﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001F1 RID: 497
public class SlicedGranularAudioClip : MonoBehaviour, IClientComponent
{
	// Token: 0x040009BD RID: 2493
	public AudioClip sourceClip;

	// Token: 0x040009BE RID: 2494
	public AudioClip granularClip;

	// Token: 0x040009BF RID: 2495
	public int sampleRate = 44100;

	// Token: 0x040009C0 RID: 2496
	public float grainAttack = 0.1f;

	// Token: 0x040009C1 RID: 2497
	public float grainSustain = 0.1f;

	// Token: 0x040009C2 RID: 2498
	public float grainRelease = 0.1f;

	// Token: 0x040009C3 RID: 2499
	public float grainFrequency = 0.1f;

	// Token: 0x040009C4 RID: 2500
	public int grainAttackSamples;

	// Token: 0x040009C5 RID: 2501
	public int grainSustainSamples;

	// Token: 0x040009C6 RID: 2502
	public int grainReleaseSamples;

	// Token: 0x040009C7 RID: 2503
	public int grainFrequencySamples;

	// Token: 0x040009C8 RID: 2504
	public int samplesUntilNextGrain;

	// Token: 0x040009C9 RID: 2505
	public List<global::SlicedGranularAudioClip.Grain> grains = new List<global::SlicedGranularAudioClip.Grain>();

	// Token: 0x040009CA RID: 2506
	public List<int> startPositions = new List<int>();

	// Token: 0x040009CB RID: 2507
	public int lastStartPositionIdx = int.MaxValue;

	// Token: 0x020001F2 RID: 498
	public class Grain
	{
		// Token: 0x1700010A RID: 266
		// (get) Token: 0x06000F50 RID: 3920 RVA: 0x0005DC18 File Offset: 0x0005BE18
		public bool finished
		{
			get
			{
				return this.currentSample >= this.endSample;
			}
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x0005DC2C File Offset: 0x0005BE2C
		public void Init(float[] source, int start, int attack, int sustain, int release)
		{
			this.sourceData = source;
			this.startSample = start;
			this.currentSample = start;
			this.attackTimeSamples = attack;
			this.sustainTimeSamples = sustain;
			this.releaseTimeSamples = release;
			this.gainPerSampleAttack = 0.5f / (float)this.attackTimeSamples;
			this.gainPerSampleRelease = -0.5f / (float)this.releaseTimeSamples;
			this.attackEndSample = this.startSample + this.attackTimeSamples;
			this.releaseStartSample = this.attackEndSample + this.sustainTimeSamples;
			this.endSample = this.releaseStartSample + this.releaseTimeSamples;
			this.gain = 0f;
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x0005DCD0 File Offset: 0x0005BED0
		public float GetSample()
		{
			if (this.currentSample >= this.sourceData.Length)
			{
				return 0f;
			}
			float num = this.sourceData[this.currentSample];
			if (this.currentSample <= this.attackEndSample)
			{
				this.gain += this.gainPerSampleAttack;
				if (this.gain > 0.5f)
				{
					this.gain = 0.5f;
				}
			}
			else if (this.currentSample >= this.releaseStartSample)
			{
				this.gain += this.gainPerSampleRelease;
				if (this.gain < 0f)
				{
					this.gain = 0f;
				}
			}
			this.currentSample++;
			return num * this.gain;
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x0005DDA0 File Offset: 0x0005BFA0
		public void FadeOut()
		{
			this.releaseStartSample = this.currentSample;
			this.endSample = this.releaseStartSample + this.releaseTimeSamples;
		}

		// Token: 0x040009CC RID: 2508
		private float[] sourceData;

		// Token: 0x040009CD RID: 2509
		private int startSample;

		// Token: 0x040009CE RID: 2510
		private int currentSample;

		// Token: 0x040009CF RID: 2511
		private int attackTimeSamples;

		// Token: 0x040009D0 RID: 2512
		private int sustainTimeSamples;

		// Token: 0x040009D1 RID: 2513
		private int releaseTimeSamples;

		// Token: 0x040009D2 RID: 2514
		private float gain;

		// Token: 0x040009D3 RID: 2515
		private float gainPerSampleAttack;

		// Token: 0x040009D4 RID: 2516
		private float gainPerSampleRelease;

		// Token: 0x040009D5 RID: 2517
		private int attackEndSample;

		// Token: 0x040009D6 RID: 2518
		private int releaseStartSample;

		// Token: 0x040009D7 RID: 2519
		private int endSample;
	}
}
