﻿using System;
using Rust;
using UnityEngine;

// Token: 0x020004F2 RID: 1266
public class ItemModSound : global::ItemMod
{
	// Token: 0x06001B02 RID: 6914 RVA: 0x00097298 File Offset: 0x00095498
	public override void OnParentChanged(global::Item item)
	{
		if (Application.isLoadingSave)
		{
			return;
		}
		if (this.actionType == global::ItemModSound.Type.OnAttachToWeapon)
		{
			if (item.parentItem == null)
			{
				return;
			}
			if (item.parentItem.info.category != global::ItemCategory.Weapon)
			{
				return;
			}
			global::BasePlayer ownerPlayer = item.parentItem.GetOwnerPlayer();
			if (ownerPlayer == null)
			{
				return;
			}
			global::Effect.server.Run(this.effect.resourcePath, ownerPlayer, 0u, Vector3.zero, Vector3.zero, null, false);
		}
	}

	// Token: 0x040015D0 RID: 5584
	public global::GameObjectRef effect = new global::GameObjectRef();

	// Token: 0x040015D1 RID: 5585
	public global::ItemModSound.Type actionType;

	// Token: 0x020004F3 RID: 1267
	public enum Type
	{
		// Token: 0x040015D3 RID: 5587
		OnAttachToWeapon
	}
}
