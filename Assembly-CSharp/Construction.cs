﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch;
using UnityEngine;

// Token: 0x0200020B RID: 523
public class Construction : global::PrefabAttribute, global::IPrefabNeedsWarming
{
	// Token: 0x06000F7C RID: 3964 RVA: 0x0005E90C File Offset: 0x0005CB0C
	public global::BaseEntity CreateConstruction(global::Construction.Target target, bool bNeedsValidPlacement = false)
	{
		GameObject gameObject = global::GameManager.server.CreatePrefab(this.fullName, Vector3.zero, Quaternion.identity, false);
		bool flag = this.UpdatePlacement(gameObject.transform, this, ref target);
		global::BaseEntity baseEntity = gameObject.ToBaseEntity();
		if (bNeedsValidPlacement && !flag)
		{
			if (baseEntity.IsValid())
			{
				baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
			}
			else
			{
				global::GameManager.Destroy(gameObject, 0f);
			}
			return null;
		}
		global::DecayEntity decayEntity = baseEntity as global::DecayEntity;
		if (decayEntity)
		{
			decayEntity.AttachToBuilding(target.entity as global::DecayEntity);
		}
		return baseEntity;
	}

	// Token: 0x06000F7D RID: 3965 RVA: 0x0005E9A4 File Offset: 0x0005CBA4
	public bool HasMaleSockets(global::Construction.Target target)
	{
		foreach (global::Socket_Base socket_Base in this.allSockets)
		{
			if (socket_Base.male && !socket_Base.maleDummy)
			{
				if (socket_Base.TestTarget(target))
				{
					return true;
				}
			}
		}
		return false;
	}

	// Token: 0x06000F7E RID: 3966 RVA: 0x0005E9FC File Offset: 0x0005CBFC
	public void FindMaleSockets(global::Construction.Target target, List<global::Socket_Base> sockets)
	{
		foreach (global::Socket_Base socket_Base in this.allSockets)
		{
			if (socket_Base.male && !socket_Base.maleDummy)
			{
				if (socket_Base.TestTarget(target))
				{
					sockets.Add(socket_Base);
				}
			}
		}
	}

	// Token: 0x06000F7F RID: 3967 RVA: 0x0005EA58 File Offset: 0x0005CC58
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		base.AttributeSetup(rootObj, name, serverside, clientside, bundling);
		this.isBuildingPrivilege = rootObj.GetComponent<global::BuildingPrivlidge>();
		this.bounds = rootObj.GetComponent<global::BaseEntity>().bounds;
		this.deployable = base.GetComponent<global::Deployable>();
		this.placeholder = base.GetComponentInChildren<global::ConstructionPlaceholder>();
		this.allSockets = base.GetComponentsInChildren<global::Socket_Base>(true);
		this.socketHandle = base.GetComponentsInChildren<global::SocketHandle>(true).FirstOrDefault<global::SocketHandle>();
		global::ConstructionGrade[] components = rootObj.GetComponents<global::ConstructionGrade>();
		this.grades = new global::ConstructionGrade[5];
		foreach (global::ConstructionGrade constructionGrade in components)
		{
			constructionGrade.construction = this;
			this.grades[(int)constructionGrade.gradeBase.type] = constructionGrade;
		}
		for (int j = 0; j < this.grades.Length; j++)
		{
			if (!(this.grades[j] == null))
			{
				this.defaultGrade = this.grades[j];
				break;
			}
		}
	}

	// Token: 0x06000F80 RID: 3968 RVA: 0x0005EB5C File Offset: 0x0005CD5C
	protected override Type GetIndexedType()
	{
		return typeof(global::Construction);
	}

	// Token: 0x06000F81 RID: 3969 RVA: 0x0005EB68 File Offset: 0x0005CD68
	public bool UpdatePlacement(Transform transform, global::Construction common, ref global::Construction.Target target)
	{
		if (!target.valid)
		{
			return false;
		}
		if (!common.canBypassBuildingPermission && !target.player.CanBuild())
		{
			global::Construction.lastPlacementError = "Player doesn't have permission";
			return false;
		}
		List<global::Socket_Base> list = Pool.GetList<global::Socket_Base>();
		common.FindMaleSockets(target, list);
		foreach (global::Socket_Base socket_Base in list)
		{
			global::Construction.Placement placement = null;
			if (!(target.entity != null) || !(target.socket != null) || !target.entity.IsOccupied(target.socket))
			{
				if (placement == null)
				{
					placement = socket_Base.DoPlacement(target);
				}
				if (placement != null)
				{
					if (!socket_Base.CheckSocketMods(placement))
					{
						transform.position = placement.position;
						transform.rotation = placement.rotation;
					}
					else if (!this.TestPlacingThroughRock(ref placement, target))
					{
						transform.position = placement.position;
						transform.rotation = placement.rotation;
						global::Construction.lastPlacementError = "Placing through rock";
					}
					else if (!global::Construction.TestPlacingThroughWall(ref placement, transform, common, target))
					{
						transform.position = placement.position;
						transform.rotation = placement.rotation;
						global::Construction.lastPlacementError = "Placing through wall";
					}
					else if (Vector3.Distance(placement.position, target.player.eyes.position) > common.maxplaceDistance + 1f)
					{
						transform.position = placement.position;
						transform.rotation = placement.rotation;
						global::Construction.lastPlacementError = "Too far away";
					}
					else
					{
						global::DeployVolume[] volumes = global::PrefabAttribute.server.FindAll<global::DeployVolume>(this.prefabID);
						if (global::DeployVolume.Check(placement.position, placement.rotation, volumes, -1))
						{
							transform.position = placement.position;
							transform.rotation = placement.rotation;
							global::Construction.lastPlacementError = "Not enough space";
						}
						else
						{
							global::BuildingProximity[] volumes2 = global::PrefabAttribute.server.FindAll<global::BuildingProximity>(this.prefabID);
							if (global::BuildingProximity.Check(target.player, this, placement.position, placement.rotation, volumes2))
							{
								transform.position = placement.position;
								transform.rotation = placement.rotation;
							}
							else if (common.isBuildingPrivilege && !target.player.CanPlaceBuildingPrivilege(placement.position, placement.rotation, common.bounds))
							{
								transform.position = placement.position;
								transform.rotation = placement.rotation;
								global::Construction.lastPlacementError = "Cannot stack building privileges";
							}
							else
							{
								bool flag = target.player.IsBuildingBlocked(placement.position, placement.rotation, common.bounds);
								if (common.canBypassBuildingPermission || !flag)
								{
									target.inBuildingPrivilege = flag;
									transform.position = placement.position;
									transform.rotation = placement.rotation;
									Pool.FreeList<global::Socket_Base>(ref list);
									return true;
								}
								transform.position = placement.position;
								transform.rotation = placement.rotation;
								global::Construction.lastPlacementError = "Building privilege";
							}
						}
					}
				}
			}
		}
		Pool.FreeList<global::Socket_Base>(ref list);
		return false;
	}

	// Token: 0x06000F82 RID: 3970 RVA: 0x0005EEE4 File Offset: 0x0005D0E4
	private bool TestPlacingThroughRock(ref global::Construction.Placement placement, global::Construction.Target target)
	{
		OBB obb;
		obb..ctor(placement.position, Vector3.one, placement.rotation, this.bounds);
		Vector3 center = target.player.GetCenter(true);
		Vector3 origin = target.ray.origin;
		if (Physics.Linecast(center, origin, 65536, 1))
		{
			return false;
		}
		RaycastHit raycastHit;
		Vector3 vector = (!obb.Trace(target.ray, ref raycastHit, float.PositiveInfinity)) ? obb.ClosestPoint(origin) : raycastHit.point;
		return !Physics.Linecast(origin, vector, 65536, 1);
	}

	// Token: 0x06000F83 RID: 3971 RVA: 0x0005EF88 File Offset: 0x0005D188
	private static bool TestPlacingThroughWall(ref global::Construction.Placement placement, Transform transform, global::Construction common, global::Construction.Target target)
	{
		Vector3 vector = placement.position - target.ray.origin;
		RaycastHit hit;
		if (!Physics.Raycast(target.ray.origin, vector.normalized, ref hit, vector.magnitude, 2097152))
		{
			return true;
		}
		global::StabilityEntity stabilityEntity = hit.GetEntity() as global::StabilityEntity;
		if (stabilityEntity != null && target.entity == stabilityEntity)
		{
			return true;
		}
		float num = vector.magnitude - hit.distance;
		if (num < 0.2f)
		{
			return true;
		}
		global::Construction.lastPlacementError = "object in placement path";
		transform.position = hit.point;
		transform.rotation = placement.rotation;
		return false;
	}

	// Token: 0x04000A30 RID: 2608
	public global::BaseEntity.Menu.Option info;

	// Token: 0x04000A31 RID: 2609
	public bool canBypassBuildingPermission;

	// Token: 0x04000A32 RID: 2610
	public bool canRotate;

	// Token: 0x04000A33 RID: 2611
	public bool checkVolumeOnRotate;

	// Token: 0x04000A34 RID: 2612
	public bool checkVolumeOnUpgrade;

	// Token: 0x04000A35 RID: 2613
	public bool canPlaceAtMaxDistance;

	// Token: 0x04000A36 RID: 2614
	public Vector3 rotationAmount = new Vector3(0f, 90f, 0f);

	// Token: 0x04000A37 RID: 2615
	[Range(0f, 10f)]
	public float healthMultiplier = 1f;

	// Token: 0x04000A38 RID: 2616
	[Range(0f, 10f)]
	public float costMultiplier = 1f;

	// Token: 0x04000A39 RID: 2617
	[Range(1f, 50f)]
	public float maxplaceDistance = 4f;

	// Token: 0x04000A3A RID: 2618
	[NonSerialized]
	public global::Socket_Base[] allSockets;

	// Token: 0x04000A3B RID: 2619
	[NonSerialized]
	public global::ConstructionGrade defaultGrade;

	// Token: 0x04000A3C RID: 2620
	[NonSerialized]
	public global::SocketHandle socketHandle;

	// Token: 0x04000A3D RID: 2621
	[NonSerialized]
	public Bounds bounds;

	// Token: 0x04000A3E RID: 2622
	[NonSerialized]
	public bool isBuildingPrivilege;

	// Token: 0x04000A3F RID: 2623
	[NonSerialized]
	public global::ConstructionGrade[] grades;

	// Token: 0x04000A40 RID: 2624
	[NonSerialized]
	public global::Deployable deployable;

	// Token: 0x04000A41 RID: 2625
	[NonSerialized]
	public global::ConstructionPlaceholder placeholder;

	// Token: 0x04000A42 RID: 2626
	public static string lastPlacementError;

	// Token: 0x0200020C RID: 524
	public class Grade
	{
		// Token: 0x1700010E RID: 270
		// (get) Token: 0x06000F85 RID: 3973 RVA: 0x0005F054 File Offset: 0x0005D254
		public PhysicMaterial physicMaterial
		{
			get
			{
				return this.grade.physicMaterial;
			}
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000F86 RID: 3974 RVA: 0x0005F064 File Offset: 0x0005D264
		public global::ProtectionProperties damageProtecton
		{
			get
			{
				return this.grade.damageProtecton;
			}
		}

		// Token: 0x04000A43 RID: 2627
		public global::BuildingGrade grade;

		// Token: 0x04000A44 RID: 2628
		public float maxHealth;

		// Token: 0x04000A45 RID: 2629
		public List<global::ItemAmount> costToBuild;
	}

	// Token: 0x0200020D RID: 525
	public struct Target
	{
		// Token: 0x06000F87 RID: 3975 RVA: 0x0005F074 File Offset: 0x0005D274
		public Quaternion GetWorldRotation(bool female)
		{
			Quaternion quaternion = this.socket.rotation;
			if (this.socket.male && this.socket.female && female)
			{
				quaternion = this.socket.rotation * Quaternion.Euler(180f, 0f, 180f);
			}
			return this.entity.transform.rotation * quaternion;
		}

		// Token: 0x06000F88 RID: 3976 RVA: 0x0005F0F0 File Offset: 0x0005D2F0
		public Vector3 GetWorldPosition()
		{
			return this.entity.transform.localToWorldMatrix.MultiplyPoint3x4(this.socket.position);
		}

		// Token: 0x04000A46 RID: 2630
		public bool valid;

		// Token: 0x04000A47 RID: 2631
		public Ray ray;

		// Token: 0x04000A48 RID: 2632
		public global::BaseEntity entity;

		// Token: 0x04000A49 RID: 2633
		public global::Socket_Base socket;

		// Token: 0x04000A4A RID: 2634
		public bool onTerrain;

		// Token: 0x04000A4B RID: 2635
		public Vector3 position;

		// Token: 0x04000A4C RID: 2636
		public Vector3 normal;

		// Token: 0x04000A4D RID: 2637
		public Vector3 rotation;

		// Token: 0x04000A4E RID: 2638
		public global::BasePlayer player;

		// Token: 0x04000A4F RID: 2639
		public bool inBuildingPrivilege;
	}

	// Token: 0x0200020E RID: 526
	public class Placement
	{
		// Token: 0x04000A50 RID: 2640
		public Vector3 position;

		// Token: 0x04000A51 RID: 2641
		public Quaternion rotation;
	}
}
