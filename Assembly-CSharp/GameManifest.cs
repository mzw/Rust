﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

// Token: 0x02000636 RID: 1590
public class GameManifest : ScriptableObject
{
	// Token: 0x1700023C RID: 572
	// (get) Token: 0x06002043 RID: 8259 RVA: 0x000B8A8C File Offset: 0x000B6C8C
	public static global::GameManifest Current
	{
		get
		{
			if (global::GameManifest.loadedManifest != null)
			{
				return global::GameManifest.loadedManifest;
			}
			global::GameManifest.Load();
			return global::GameManifest.loadedManifest;
		}
	}

	// Token: 0x06002044 RID: 8260 RVA: 0x000B8AB0 File Offset: 0x000B6CB0
	public static void Load()
	{
		if (global::GameManifest.loadedManifest != null)
		{
			return;
		}
		global::GameManifest.loadedManifest = global::FileSystem.Load<global::GameManifest>("Assets/manifest.asset", true);
		foreach (global::GameManifest.PrefabProperties prefabProperties in global::GameManifest.loadedManifest.prefabProperties)
		{
			global::GameManifest.guidToPath.Add(prefabProperties.guid, prefabProperties.name);
		}
		DebugEx.Log(global::GameManifest.GetMetadataStatus(), 0);
	}

	// Token: 0x06002045 RID: 8261 RVA: 0x000B8B24 File Offset: 0x000B6D24
	public static void LoadAssets()
	{
		if (Skinnable.All != null)
		{
			return;
		}
		Skinnable.All = global::GameManifest.LoadSkinnableAssets();
		DebugEx.Log(global::GameManifest.GetAssetStatus(), 0);
	}

	// Token: 0x06002046 RID: 8262 RVA: 0x000B8B48 File Offset: 0x000B6D48
	private static Skinnable[] LoadSkinnableAssets()
	{
		string[] array = global::GameManifest.loadedManifest.skinnables;
		Skinnable[] array2 = new Skinnable[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array2[i] = global::FileSystem.Load<Skinnable>(array[i], true);
		}
		return array2;
	}

	// Token: 0x06002047 RID: 8263 RVA: 0x000B8B8C File Offset: 0x000B6D8C
	internal static Dictionary<string, string[]> LoadEffectDictionary()
	{
		global::GameManifest.EffectCategory[] array = global::GameManifest.loadedManifest.effectCategories;
		Dictionary<string, string[]> dictionary = new Dictionary<string, string[]>();
		foreach (global::GameManifest.EffectCategory effectCategory in array)
		{
			dictionary.Add(effectCategory.folder, effectCategory.prefabs.ToArray());
		}
		return dictionary;
	}

	// Token: 0x06002048 RID: 8264 RVA: 0x000B8BE4 File Offset: 0x000B6DE4
	internal static string GUIDToPath(string guid)
	{
		if (string.IsNullOrEmpty(guid))
		{
			Debug.LogError("GUIDToPath: guid is empty");
			return string.Empty;
		}
		global::GameManifest.Load();
		string result;
		if (global::GameManifest.guidToPath.TryGetValue(guid, out result))
		{
			return result;
		}
		Debug.LogWarning("GUIDToPath: no path found for guid " + guid);
		return string.Empty;
	}

	// Token: 0x06002049 RID: 8265 RVA: 0x000B8C3C File Offset: 0x000B6E3C
	internal static Object GUIDToObject(string guid)
	{
		Object result = null;
		if (global::GameManifest.guidToObject.TryGetValue(guid, out result))
		{
			return result;
		}
		string text = global::GameManifest.GUIDToPath(guid);
		if (string.IsNullOrEmpty(text))
		{
			Debug.LogWarning("Missing file for guid " + guid);
			global::GameManifest.guidToObject.Add(guid, null);
			return null;
		}
		Object @object = global::FileSystem.Load<Object>(text, true);
		global::GameManifest.guidToObject.Add(guid, @object);
		return @object;
	}

	// Token: 0x0600204A RID: 8266 RVA: 0x000B8CA4 File Offset: 0x000B6EA4
	private static string GetMetadataStatus()
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (global::GameManifest.loadedManifest != null)
		{
			stringBuilder.Append("Manifest Metadata Loaded");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append(global::GameManifest.loadedManifest.pooledStrings.Length.ToString());
			stringBuilder.Append(" pooled strings");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append(global::GameManifest.loadedManifest.meshColliders.Length.ToString());
			stringBuilder.Append(" mesh colliders");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append(global::GameManifest.loadedManifest.prefabProperties.Length.ToString());
			stringBuilder.Append(" prefab properties");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append(global::GameManifest.loadedManifest.effectCategories.Length.ToString());
			stringBuilder.Append(" effect categories");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append(global::GameManifest.loadedManifest.entities.Length.ToString());
			stringBuilder.Append(" entity names");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append(global::GameManifest.loadedManifest.skinnables.Length.ToString());
			stringBuilder.Append(" skinnable names");
		}
		else
		{
			stringBuilder.Append("Manifest Metadata Missing");
		}
		return stringBuilder.ToString();
	}

	// Token: 0x0600204B RID: 8267 RVA: 0x000B8E70 File Offset: 0x000B7070
	private static string GetAssetStatus()
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (global::GameManifest.loadedManifest != null)
		{
			stringBuilder.Append("Manifest Assets Loaded");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.Append((Skinnable.All == null) ? "0" : Skinnable.All.Length.ToString());
			stringBuilder.Append(" skinnable objects");
		}
		else
		{
			stringBuilder.Append("Manifest Assets Missing");
		}
		return stringBuilder.ToString();
	}

	// Token: 0x04001B0F RID: 6927
	public global::GameManifest.PooledString[] pooledStrings;

	// Token: 0x04001B10 RID: 6928
	public global::GameManifest.MeshColliderInfo[] meshColliders;

	// Token: 0x04001B11 RID: 6929
	public global::GameManifest.PrefabProperties[] prefabProperties;

	// Token: 0x04001B12 RID: 6930
	public global::GameManifest.EffectCategory[] effectCategories;

	// Token: 0x04001B13 RID: 6931
	public string[] skinnables;

	// Token: 0x04001B14 RID: 6932
	public string[] entities;

	// Token: 0x04001B15 RID: 6933
	internal static global::GameManifest loadedManifest;

	// Token: 0x04001B16 RID: 6934
	internal static Dictionary<string, string> guidToPath = new Dictionary<string, string>();

	// Token: 0x04001B17 RID: 6935
	internal static Dictionary<string, Object> guidToObject = new Dictionary<string, Object>();

	// Token: 0x02000637 RID: 1591
	[Serializable]
	public struct PooledString
	{
		// Token: 0x04001B18 RID: 6936
		[HideInInspector]
		public string str;

		// Token: 0x04001B19 RID: 6937
		public uint hash;
	}

	// Token: 0x02000638 RID: 1592
	[Serializable]
	public class MeshColliderInfo
	{
		// Token: 0x04001B1A RID: 6938
		[HideInInspector]
		public string name;

		// Token: 0x04001B1B RID: 6939
		public uint hash;

		// Token: 0x04001B1C RID: 6940
		public PhysicMaterial physicMaterial;
	}

	// Token: 0x02000639 RID: 1593
	[Serializable]
	public class PrefabProperties
	{
		// Token: 0x04001B1D RID: 6941
		[HideInInspector]
		public string name;

		// Token: 0x04001B1E RID: 6942
		public string guid;

		// Token: 0x04001B1F RID: 6943
		public uint hash;

		// Token: 0x04001B20 RID: 6944
		public bool needsWarmup;
	}

	// Token: 0x0200063A RID: 1594
	[Serializable]
	public class EffectCategory
	{
		// Token: 0x04001B21 RID: 6945
		[HideInInspector]
		public string folder;

		// Token: 0x04001B22 RID: 6946
		public List<string> prefabs;
	}
}
