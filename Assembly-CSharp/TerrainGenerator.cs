﻿using System;
using UnityEngine;

// Token: 0x020005CD RID: 1485
public class TerrainGenerator : SingletonComponent<global::TerrainGenerator>
{
	// Token: 0x06001EA9 RID: 7849 RVA: 0x000AC9A0 File Offset: 0x000AABA0
	public GameObject CreateTerrain()
	{
		Debug.Log(string.Concat(new object[]
		{
			"Generating terrain of size ",
			global::World.Size,
			" with seed ",
			global::World.Seed
		}));
		Terrain component = Terrain.CreateTerrainGameObject(new TerrainData
		{
			baseMapResolution = Mathf.NextPowerOfTwo((int)(global::World.Size * 0.01f)),
			heightmapResolution = Mathf.NextPowerOfTwo((int)(global::World.Size * 0.5f)) + 1,
			alphamapResolution = Mathf.NextPowerOfTwo((int)(global::World.Size * 0.5f)),
			size = new Vector3(global::World.Size, 1000f, global::World.Size)
		}).GetComponent<Terrain>();
		component.transform.position = base.transform.position + new Vector3((float)(-(float)((ulong)global::World.Size)) * 0.5f, 0f, (float)(-(float)((ulong)global::World.Size)) * 0.5f);
		component.castShadows = this.config.CastShadows;
		component.materialType = 3;
		component.materialTemplate = this.config.Material;
		component.gameObject.tag = base.gameObject.tag;
		component.gameObject.layer = base.gameObject.layer;
		TerrainCollider component2 = component.gameObject.GetComponent<TerrainCollider>();
		component2.sharedMaterial = this.config.GenericMaterial;
		global::TerrainMeta terrainMeta = component.gameObject.AddComponent<global::TerrainMeta>();
		component.gameObject.AddComponent<global::TerrainPhysics>();
		component.gameObject.AddComponent<global::TerrainColors>();
		component.gameObject.AddComponent<global::TerrainCollision>();
		component.gameObject.AddComponent<global::TerrainBiomeMap>();
		component.gameObject.AddComponent<global::TerrainAlphaMap>();
		component.gameObject.AddComponent<global::TerrainHeightMap>();
		component.gameObject.AddComponent<global::TerrainSplatMap>();
		component.gameObject.AddComponent<global::TerrainTopologyMap>();
		component.gameObject.AddComponent<global::TerrainWaterMap>();
		component.gameObject.AddComponent<global::TerrainPath>();
		terrainMeta.terrain = component;
		terrainMeta.config = this.config;
		Object.DestroyImmediate(base.gameObject);
		return component.gameObject;
	}

	// Token: 0x04001979 RID: 6521
	public global::TerrainConfig config;

	// Token: 0x0400197A RID: 6522
	public uint EditorSize;

	// Token: 0x0400197B RID: 6523
	private const float HeightMapRes = 0.5f;

	// Token: 0x0400197C RID: 6524
	private const float SplatMapRes = 0.5f;

	// Token: 0x0400197D RID: 6525
	private const float BaseMapRes = 0.01f;
}
