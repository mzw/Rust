﻿using System;

// Token: 0x02000355 RID: 853
public class Binocular : global::AttackEntity
{
	// Token: 0x04000F30 RID: 3888
	public float[] fovs;

	// Token: 0x04000F31 RID: 3889
	public global::GameObjectRef fovChangeEffect;
}
