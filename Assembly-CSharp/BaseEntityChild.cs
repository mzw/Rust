﻿using System;
using Rust;
using Rust.Registry;
using UnityEngine;

// Token: 0x0200035B RID: 859
public class BaseEntityChild : MonoBehaviour
{
	// Token: 0x0600147A RID: 5242 RVA: 0x000775C4 File Offset: 0x000757C4
	public static void Setup(GameObject obj, global::BaseEntity parent)
	{
		using (TimeWarning.New("Registry.Entity.Register", 0.1f))
		{
			Entity.Register(obj, parent);
		}
	}

	// Token: 0x0600147B RID: 5243 RVA: 0x0007760C File Offset: 0x0007580C
	public void OnDestroy()
	{
		if (Application.isQuitting)
		{
			return;
		}
		using (TimeWarning.New("Registry.Entity.Unregister", 0.1f))
		{
			Entity.Unregister(base.gameObject);
		}
	}
}
