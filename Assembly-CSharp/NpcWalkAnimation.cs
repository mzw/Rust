﻿using System;
using UnityEngine;

// Token: 0x02000235 RID: 565
public class NpcWalkAnimation : MonoBehaviour, IClientComponent
{
	// Token: 0x04000AC6 RID: 2758
	public Vector3 HipFudge = new Vector3(-90f, 0f, 90f);

	// Token: 0x04000AC7 RID: 2759
	public global::BaseNpc Npc;

	// Token: 0x04000AC8 RID: 2760
	public Animator Animator;

	// Token: 0x04000AC9 RID: 2761
	public Transform HipBone;

	// Token: 0x04000ACA RID: 2762
	public Transform LookBone;

	// Token: 0x04000ACB RID: 2763
	public bool UpdateWalkSpeed = true;

	// Token: 0x04000ACC RID: 2764
	public bool UpdateFacingDirection = true;

	// Token: 0x04000ACD RID: 2765
	public bool UpdateGroundNormal = true;

	// Token: 0x04000ACE RID: 2766
	public Transform alignmentRoot;

	// Token: 0x04000ACF RID: 2767
	public bool LaggyAss = true;

	// Token: 0x04000AD0 RID: 2768
	public bool LookAtTarget;

	// Token: 0x04000AD1 RID: 2769
	public float MaxLaggyAssRotation = 70f;

	// Token: 0x04000AD2 RID: 2770
	public float MaxWalkAnimSpeed = 25f;

	// Token: 0x04000AD3 RID: 2771
	public bool UseDirectionBlending;
}
