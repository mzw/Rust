﻿using System;
using Facepunch;
using ProtoBuf;
using UnityEngine;

// Token: 0x020003AB RID: 939
public class PlanterBox : global::BaseCombatEntity, global::ISplashable
{
	// Token: 0x06001650 RID: 5712 RVA: 0x00081544 File Offset: 0x0007F744
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.resource = Pool.Get<BaseResource>();
		info.msg.resource.stage = this.soilSaturation;
	}

	// Token: 0x06001651 RID: 5713 RVA: 0x00081578 File Offset: 0x0007F778
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.resource != null)
		{
			this.soilSaturation = info.msg.resource.stage;
		}
	}

	// Token: 0x17000189 RID: 393
	// (get) Token: 0x06001652 RID: 5714 RVA: 0x000815AC File Offset: 0x0007F7AC
	public float soilSaturationFraction
	{
		get
		{
			return (float)this.soilSaturation / (float)this.soilSaturationMax;
		}
	}

	// Token: 0x06001653 RID: 5715 RVA: 0x000815C0 File Offset: 0x0007F7C0
	public int UseWater(int amount)
	{
		int num = Mathf.Min(amount, this.soilSaturation);
		this.soilSaturation -= num;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		return num;
	}

	// Token: 0x06001654 RID: 5716 RVA: 0x000815F0 File Offset: 0x0007F7F0
	public bool wantsSplash(global::ItemDefinition splashType, int amount)
	{
		return splashType.shortname == "water.salt" || this.soilSaturation < this.soilSaturationMax;
	}

	// Token: 0x06001655 RID: 5717 RVA: 0x00081618 File Offset: 0x0007F818
	public int DoSplash(global::ItemDefinition splashType, int amount)
	{
		if (splashType.shortname == "water.salt")
		{
			this.soilSaturation = 0;
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return amount;
		}
		int num = Mathf.Min(this.soilSaturationMax - this.soilSaturation, amount);
		this.soilSaturation += num;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		return num;
	}

	// Token: 0x040010CD RID: 4301
	public int soilSaturation;

	// Token: 0x040010CE RID: 4302
	public int soilSaturationMax = 8000;

	// Token: 0x040010CF RID: 4303
	public MeshRenderer soilRenderer;
}
