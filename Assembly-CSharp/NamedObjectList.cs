﻿using System;
using UnityEngine;

// Token: 0x0200064C RID: 1612
[CreateAssetMenu(menuName = "Rust/Named Object List")]
public class NamedObjectList : ScriptableObject
{
	// Token: 0x04001B62 RID: 7010
	public global::NamedObjectList.NamedObject[] objects;

	// Token: 0x0200064D RID: 1613
	[Serializable]
	public struct NamedObject
	{
		// Token: 0x04001B63 RID: 7011
		public string name;

		// Token: 0x04001B64 RID: 7012
		public Object obj;
	}
}
