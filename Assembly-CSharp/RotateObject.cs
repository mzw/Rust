﻿using System;
using UnityEngine;

// Token: 0x020002F7 RID: 759
public class RotateObject : MonoBehaviour
{
	// Token: 0x0600132D RID: 4909 RVA: 0x00070CC0 File Offset: 0x0006EEC0
	private void Update()
	{
		base.transform.Rotate(Vector3.up, Time.deltaTime * this.rotateSpeed_X);
		base.transform.Rotate(base.transform.forward, Time.deltaTime * this.rotateSpeed_Y);
		base.transform.Rotate(base.transform.right, Time.deltaTime * this.rotateSpeed_Z);
	}

	// Token: 0x04000DED RID: 3565
	public float rotateSpeed_X = 1f;

	// Token: 0x04000DEE RID: 3566
	public float rotateSpeed_Y = 1f;

	// Token: 0x04000DEF RID: 3567
	public float rotateSpeed_Z = 1f;
}
