﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000630 RID: 1584
[CreateAssetMenu(menuName = "Rust/Building Grade")]
public class BuildingGrade : ScriptableObject
{
	// Token: 0x04001AF3 RID: 6899
	public global::BuildingGrade.Enum type;

	// Token: 0x04001AF4 RID: 6900
	public float baseHealth;

	// Token: 0x04001AF5 RID: 6901
	public List<global::ItemAmount> baseCost;

	// Token: 0x04001AF6 RID: 6902
	public PhysicMaterial physicMaterial;

	// Token: 0x04001AF7 RID: 6903
	public global::ProtectionProperties damageProtecton;

	// Token: 0x04001AF8 RID: 6904
	public global::BaseEntity.Menu.Option upgradeMenu;

	// Token: 0x02000631 RID: 1585
	public enum Enum
	{
		// Token: 0x04001AFA RID: 6906
		None = -1,
		// Token: 0x04001AFB RID: 6907
		Twigs,
		// Token: 0x04001AFC RID: 6908
		Wood,
		// Token: 0x04001AFD RID: 6909
		Stone,
		// Token: 0x04001AFE RID: 6910
		Metal,
		// Token: 0x04001AFF RID: 6911
		TopTier,
		// Token: 0x04001B00 RID: 6912
		Count
	}
}
