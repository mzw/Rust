﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200062E RID: 1582
public static class AssetNameCache
{
	// Token: 0x06002028 RID: 8232 RVA: 0x000B8460 File Offset: 0x000B6660
	private static string LookupName(Object obj)
	{
		if (obj == null)
		{
			return string.Empty;
		}
		string name;
		if (!global::AssetNameCache.mixed.TryGetValue(obj, out name))
		{
			name = obj.name;
			global::AssetNameCache.mixed.Add(obj, name);
		}
		return name;
	}

	// Token: 0x06002029 RID: 8233 RVA: 0x000B84A8 File Offset: 0x000B66A8
	private static string LookupNameLower(Object obj)
	{
		if (obj == null)
		{
			return string.Empty;
		}
		string text;
		if (!global::AssetNameCache.lower.TryGetValue(obj, out text))
		{
			text = obj.name.ToLower();
			global::AssetNameCache.lower.Add(obj, text);
		}
		return text;
	}

	// Token: 0x0600202A RID: 8234 RVA: 0x000B84F4 File Offset: 0x000B66F4
	private static string LookupNameUpper(Object obj)
	{
		if (obj == null)
		{
			return string.Empty;
		}
		string text;
		if (!global::AssetNameCache.upper.TryGetValue(obj, out text))
		{
			text = obj.name.ToUpper();
			global::AssetNameCache.upper.Add(obj, text);
		}
		return text;
	}

	// Token: 0x0600202B RID: 8235 RVA: 0x000B8540 File Offset: 0x000B6740
	public static string GetName(this PhysicMaterial mat)
	{
		return global::AssetNameCache.LookupName(mat);
	}

	// Token: 0x0600202C RID: 8236 RVA: 0x000B8548 File Offset: 0x000B6748
	public static string GetNameLower(this PhysicMaterial mat)
	{
		return global::AssetNameCache.LookupNameLower(mat);
	}

	// Token: 0x0600202D RID: 8237 RVA: 0x000B8550 File Offset: 0x000B6750
	public static string GetNameUpper(this PhysicMaterial mat)
	{
		return global::AssetNameCache.LookupNameUpper(mat);
	}

	// Token: 0x0600202E RID: 8238 RVA: 0x000B8558 File Offset: 0x000B6758
	public static string GetName(this Material mat)
	{
		return global::AssetNameCache.LookupName(mat);
	}

	// Token: 0x0600202F RID: 8239 RVA: 0x000B8560 File Offset: 0x000B6760
	public static string GetNameLower(this Material mat)
	{
		return global::AssetNameCache.LookupNameLower(mat);
	}

	// Token: 0x06002030 RID: 8240 RVA: 0x000B8568 File Offset: 0x000B6768
	public static string GetNameUpper(this Material mat)
	{
		return global::AssetNameCache.LookupNameUpper(mat);
	}

	// Token: 0x04001AEE RID: 6894
	private static Dictionary<Object, string> mixed = new Dictionary<Object, string>();

	// Token: 0x04001AEF RID: 6895
	private static Dictionary<Object, string> lower = new Dictionary<Object, string>();

	// Token: 0x04001AF0 RID: 6896
	private static Dictionary<Object, string> upper = new Dictionary<Object, string>();
}
