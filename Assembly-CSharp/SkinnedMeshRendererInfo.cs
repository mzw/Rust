﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x0200025C RID: 604
public class SkinnedMeshRendererInfo : global::ComponentInfo<SkinnedMeshRenderer>
{
	// Token: 0x06001070 RID: 4208 RVA: 0x00063928 File Offset: 0x00061B28
	public override void Reset()
	{
		if (this.component == null)
		{
			return;
		}
		this.component.shadowCastingMode = this.shadows;
		if (this.material)
		{
			this.component.sharedMaterial = this.material;
		}
		this.component.sharedMesh = this.mesh;
		this.component.localBounds = this.bounds;
	}

	// Token: 0x06001071 RID: 4209 RVA: 0x0006399C File Offset: 0x00061B9C
	public override void Setup()
	{
		this.shadows = this.component.shadowCastingMode;
		this.material = this.component.sharedMaterial;
		this.mesh = this.component.sharedMesh;
		this.bounds = this.component.localBounds;
		this.RefreshCache();
	}

	// Token: 0x06001072 RID: 4210 RVA: 0x000639F4 File Offset: 0x00061BF4
	private void RefreshCache()
	{
		if (this.cachedMesh != this.component.sharedMesh)
		{
			if (this.cachedMesh != null)
			{
				global::SkinnedMeshRendererCache.Add(this.cachedMesh, this.cachedRig);
			}
			this.cachedMesh = this.component.sharedMesh;
			this.cachedRig = global::SkinnedMeshRendererCache.Get(this.component);
		}
	}

	// Token: 0x06001073 RID: 4211 RVA: 0x00063A60 File Offset: 0x00061C60
	public void BuildRig()
	{
		this.RefreshCache();
		Vector3 position = base.transform.position;
		Quaternion rotation = base.transform.rotation;
		base.transform.rotation = Quaternion.identity;
		base.transform.position = Vector3.zero;
		Transform[] array = new Transform[this.cachedRig.transforms.Length];
		for (int i = 0; i < this.cachedRig.transforms.Length; i++)
		{
			GameObject gameObject = new GameObject(this.cachedRig.bones[i]);
			gameObject.transform.position = this.cachedRig.transforms[i].MultiplyPoint(Vector3.zero);
			gameObject.transform.rotation = Quaternion.LookRotation(this.cachedRig.transforms[i].GetColumn(2), this.cachedRig.transforms[i].GetColumn(1));
			gameObject.transform.SetParent(base.transform, true);
			array[i] = gameObject.transform;
		}
		GameObject gameObject2 = new GameObject("root");
		gameObject2.transform.position = this.cachedRig.rootTransform.MultiplyPoint(Vector3.zero);
		gameObject2.transform.rotation = Quaternion.LookRotation(this.cachedRig.rootTransform.GetColumn(2), this.cachedRig.rootTransform.GetColumn(1));
		gameObject2.transform.SetParent(base.transform, true);
		this.component.rootBone = gameObject2.transform;
		this.component.bones = array;
		base.transform.rotation = rotation;
		base.transform.position = position;
	}

	// Token: 0x04000B3A RID: 2874
	public ShadowCastingMode shadows;

	// Token: 0x04000B3B RID: 2875
	public Material material;

	// Token: 0x04000B3C RID: 2876
	public Mesh mesh;

	// Token: 0x04000B3D RID: 2877
	public Bounds bounds;

	// Token: 0x04000B3E RID: 2878
	public Mesh cachedMesh;

	// Token: 0x04000B3F RID: 2879
	public global::SkinnedMeshRendererCache.RigInfo cachedRig;

	// Token: 0x04000B40 RID: 2880
	private Transform root;

	// Token: 0x04000B41 RID: 2881
	private Transform[] bones;
}
