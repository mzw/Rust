﻿using System;
using UnityEngine;

// Token: 0x02000546 RID: 1350
public class DecorOffset : global::DecorComponent
{
	// Token: 0x06001C8A RID: 7306 RVA: 0x0009FF18 File Offset: 0x0009E118
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		uint num = SeedEx.Seed(pos, global::World.Seed) + 1u;
		pos.x += scale.x * SeedRandom.Range(ref num, this.MinOffset.x, this.MaxOffset.x);
		pos.y += scale.y * SeedRandom.Range(ref num, this.MinOffset.y, this.MaxOffset.y);
		pos.z += scale.z * SeedRandom.Range(ref num, this.MinOffset.z, this.MaxOffset.z);
	}

	// Token: 0x04001795 RID: 6037
	public Vector3 MinOffset = new Vector3(0f, 0f, 0f);

	// Token: 0x04001796 RID: 6038
	public Vector3 MaxOffset = new Vector3(0f, 0f, 0f);
}
