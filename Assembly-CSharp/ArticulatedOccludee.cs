﻿using System;
using System.Collections.Generic;
using Facepunch;
using Rust;
using UnityEngine;

// Token: 0x020002EA RID: 746
public class ArticulatedOccludee : global::BaseMonoBehaviour
{
	// Token: 0x17000162 RID: 354
	// (get) Token: 0x060012D4 RID: 4820 RVA: 0x0006E340 File Offset: 0x0006C540
	public bool IsVisible
	{
		get
		{
			return this.isVisible;
		}
	}

	// Token: 0x17000163 RID: 355
	// (get) Token: 0x060012D5 RID: 4821 RVA: 0x0006E348 File Offset: 0x0006C548
	// (set) Token: 0x060012D6 RID: 4822 RVA: 0x0006E350 File Offset: 0x0006C550
	public SkinnedMeshRenderer FixedBoundsRef
	{
		get
		{
			return this.fixedBoundsRef;
		}
		set
		{
			this.fixedBoundsRef = value;
		}
	}

	// Token: 0x060012D7 RID: 4823 RVA: 0x0006E35C File Offset: 0x0006C55C
	protected virtual void OnEnable()
	{
		if (this.registerCullingOnStart)
		{
			List<Collider> list = Pool.GetList<Collider>();
			base.GetComponentsInChildren<Collider>(list);
			LODGroup component = base.GetComponent<LODGroup>();
			if (component != null)
			{
				this.ProcessVisibility(component, list);
			}
			this.UpdateCullingBounds();
			Pool.FreeList<Collider>(ref list);
		}
	}

	// Token: 0x060012D8 RID: 4824 RVA: 0x0006E3AC File Offset: 0x0006C5AC
	protected virtual void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.UnregisterFromCulling();
		this.ClearVisibility();
	}

	// Token: 0x060012D9 RID: 4825 RVA: 0x0006E3C8 File Offset: 0x0006C5C8
	public void ClearVisibility()
	{
		if (this.lodGroup != null)
		{
			this.lodGroup.localReferencePoint = Vector3.zero;
			this.lodGroup.RecalculateBounds();
			this.lodGroup = null;
		}
		if (this.renderers != null)
		{
			this.renderers.Clear();
		}
		if (this.colliders != null)
		{
			this.colliders.Clear();
		}
		this.fixedBoundsRef = null;
		this.localOccludee = new global::OccludeeSphere(-1);
	}

	// Token: 0x060012DA RID: 4826 RVA: 0x0006E448 File Offset: 0x0006C648
	public void ProcessVisibility(LODGroup lod, List<Collider> colliders)
	{
		this.ProcessVisibility(null, colliders);
		this.lodGroup = lod;
		if (lod != null)
		{
			this.renderers = new List<Renderer>(16);
			LOD[] lods = lod.GetLODs();
			for (int i = 0; i < lods.Length; i++)
			{
				foreach (Renderer renderer in lods[i].renderers)
				{
					if (renderer != null)
					{
						this.renderers.Add(renderer);
					}
				}
			}
		}
	}

	// Token: 0x060012DB RID: 4827 RVA: 0x0006E4D8 File Offset: 0x0006C6D8
	public void ProcessVisibility(List<Renderer> renderers, List<Collider> colliders)
	{
		this.ClearVisibility();
		if (renderers != null && renderers.Count > 0)
		{
			this.renderers = new List<Renderer>(16);
			for (int i = 0; i < renderers.Count; i++)
			{
				Renderer renderer = renderers[i];
				if (renderer != null)
				{
					this.renderers.Add(renderer);
				}
			}
		}
		if (colliders != null && colliders.Count > 0)
		{
			this.colliders = new List<Collider>(colliders.Count);
			for (int j = 0; j < colliders.Count; j++)
			{
				Collider collider = colliders[j];
				if (collider != null)
				{
					this.colliders.Add(collider);
				}
			}
		}
	}

	// Token: 0x060012DC RID: 4828 RVA: 0x0006E59C File Offset: 0x0006C79C
	private void RegisterForCulling(global::OcclusionCulling.Sphere sphere, bool visible)
	{
		if (this.localOccludee.IsRegistered)
		{
			this.UnregisterFromCulling();
		}
		int num = global::OcclusionCulling.RegisterOccludee(sphere.position, sphere.radius, visible, 0.25f, false, new global::OcclusionCulling.OnVisibilityChanged(this.OnVisibilityChanged));
		if (num >= 0)
		{
			this.localOccludee = new global::OccludeeSphere(num, this.localOccludee.sphere);
		}
		else
		{
			this.localOccludee.Invalidate();
			Debug.LogWarning("[OcclusionCulling] Occludee registration failed for " + base.name + ". Too many registered.");
		}
	}

	// Token: 0x060012DD RID: 4829 RVA: 0x0006E630 File Offset: 0x0006C830
	private void UnregisterFromCulling()
	{
		if (this.localOccludee.IsRegistered)
		{
			global::OcclusionCulling.UnregisterOccludee(this.localOccludee.id);
			this.localOccludee.Invalidate();
		}
	}

	// Token: 0x060012DE RID: 4830 RVA: 0x0006E660 File Offset: 0x0006C860
	public void UpdateCullingBounds()
	{
		Vector3 position = Vector3.zero;
		float radius = 0f;
		bool flag = true;
		if (this.fixedBoundsRef != null)
		{
			Bounds bounds = this.fixedBoundsRef.bounds;
			position = bounds.center;
			radius = Mathf.Max(Mathf.Max(bounds.extents.x, bounds.extents.y), bounds.extents.z);
		}
		else
		{
			int num = (this.renderers == null) ? 0 : this.renderers.Count;
			int num2 = (this.colliders == null) ? 0 : this.colliders.Count;
			if (num > 0 && num < num2)
			{
				Bounds bounds2 = this.renderers[0].bounds;
				for (int i = 1; i < this.renderers.Count; i++)
				{
					bounds2.Encapsulate(this.renderers[i].bounds);
				}
				position = bounds2.center;
				radius = Mathf.Max(Mathf.Max(bounds2.extents.x, bounds2.extents.y), bounds2.extents.z);
			}
			else if (num2 > 0)
			{
				Bounds bounds3 = this.colliders[0].bounds;
				for (int j = 1; j < this.colliders.Count; j++)
				{
					bounds3.Encapsulate(this.colliders[j].bounds);
				}
				position = bounds3.center;
				radius = bounds3.extents.magnitude;
			}
			else
			{
				flag = false;
			}
		}
		if (flag)
		{
			global::OcclusionCulling.Sphere sphere = new global::OcclusionCulling.Sphere(position, radius);
			if (this.localOccludee.IsRegistered)
			{
				global::OcclusionCulling.UpdateDynamicOccludee(this.localOccludee.id, sphere.position, sphere.radius);
				this.localOccludee.sphere = sphere;
			}
			else
			{
				bool visible = true;
				if (this.lodGroup != null)
				{
					visible = this.lodGroup.enabled;
				}
				this.RegisterForCulling(sphere, visible);
			}
		}
	}

	// Token: 0x060012DF RID: 4831 RVA: 0x0006E8B0 File Offset: 0x0006CAB0
	protected virtual bool CheckVisibility()
	{
		return this.localOccludee.state == null || this.localOccludee.state.isVisible;
	}

	// Token: 0x060012E0 RID: 4832 RVA: 0x0006E8D4 File Offset: 0x0006CAD4
	private void ApplyVisibility(bool vis)
	{
		if (this.lodGroup != null)
		{
			float num = (float)((!vis) ? 100000 : 0);
			if (num != this.lodGroup.localReferencePoint.x)
			{
				this.lodGroup.localReferencePoint = new Vector3(num, num, num);
			}
		}
	}

	// Token: 0x060012E1 RID: 4833 RVA: 0x0006E934 File Offset: 0x0006CB34
	protected virtual void OnVisibilityChanged(bool visible)
	{
		if (global::MainCamera.mainCamera != null)
		{
			float dist = Vector3.Distance(global::MainCamera.mainCamera.transform.position, base.transform.position);
			this.VisUpdateUsingCulling(dist, visible);
		}
	}

	// Token: 0x060012E2 RID: 4834 RVA: 0x0006E97C File Offset: 0x0006CB7C
	private void UpdateVisibility(float delay)
	{
	}

	// Token: 0x060012E3 RID: 4835 RVA: 0x0006E980 File Offset: 0x0006CB80
	private void VisUpdateUsingCulling(float dist, bool visibility)
	{
	}

	// Token: 0x060012E4 RID: 4836 RVA: 0x0006E984 File Offset: 0x0006CB84
	public virtual void TriggerUpdateVisibilityBounds()
	{
		if (base.enabled)
		{
			float sqrMagnitude = (base.transform.position - global::MainCamera.mainCamera.transform.position).sqrMagnitude;
			float num = 400f;
			float num2;
			if (sqrMagnitude < num)
			{
				num2 = 1f / Random.Range(15f, 35f);
			}
			else
			{
				float num3 = Mathf.Clamp01((Mathf.Sqrt(sqrMagnitude) - 20f) * 0.001f);
				float num4 = Mathf.Lerp(0.04f, 2f, num3);
				num2 = Random.Range(num4, num4 + 0.04f);
			}
			this.UpdateVisibility(num2);
			this.ApplyVisibility(this.isVisible);
			base.Invoke(new Action(this.TriggerUpdateVisibilityBounds), num2);
		}
	}

	// Token: 0x04000D9B RID: 3483
	private const float UpdateBoundsFadeStart = 20f;

	// Token: 0x04000D9C RID: 3484
	private const float UpdateBoundsFadeLength = 1000f;

	// Token: 0x04000D9D RID: 3485
	private const float UpdateBoundsMaxFrequency = 25f;

	// Token: 0x04000D9E RID: 3486
	private const float UpdateBoundsMinFrequency = 0.5f;

	// Token: 0x04000D9F RID: 3487
	public bool registerCullingOnStart;

	// Token: 0x04000DA0 RID: 3488
	private LODGroup lodGroup;

	// Token: 0x04000DA1 RID: 3489
	private List<Renderer> renderers;

	// Token: 0x04000DA2 RID: 3490
	private List<Collider> colliders;

	// Token: 0x04000DA3 RID: 3491
	private global::OccludeeSphere localOccludee = new global::OccludeeSphere(-1);

	// Token: 0x04000DA4 RID: 3492
	private bool isVisible = true;

	// Token: 0x04000DA5 RID: 3493
	private SkinnedMeshRenderer fixedBoundsRef;
}
