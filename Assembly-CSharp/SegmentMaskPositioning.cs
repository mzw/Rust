﻿using System;
using UnityEngine;

// Token: 0x020006F7 RID: 1783
public class SegmentMaskPositioning : MonoBehaviour
{
	// Token: 0x04001E63 RID: 7779
	public global::PlayerModel source;

	// Token: 0x04001E64 RID: 7780
	public GameObject headMask;

	// Token: 0x04001E65 RID: 7781
	public GameObject chestMask;

	// Token: 0x04001E66 RID: 7782
	public GameObject legsMask;

	// Token: 0x04001E67 RID: 7783
	public float xOffset = 0.75f;
}
