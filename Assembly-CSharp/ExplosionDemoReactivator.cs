﻿using System;
using UnityEngine;

// Token: 0x020007F3 RID: 2035
public class ExplosionDemoReactivator : MonoBehaviour
{
	// Token: 0x060025A3 RID: 9635 RVA: 0x000D048C File Offset: 0x000CE68C
	private void Start()
	{
		base.InvokeRepeating("Reactivate", 0f, this.TimeDelayToReactivate);
	}

	// Token: 0x060025A4 RID: 9636 RVA: 0x000D04A4 File Offset: 0x000CE6A4
	private void Reactivate()
	{
		Transform[] componentsInChildren = base.GetComponentsInChildren<Transform>();
		foreach (Transform transform in componentsInChildren)
		{
			transform.gameObject.SetActive(false);
			transform.gameObject.SetActive(true);
		}
	}

	// Token: 0x040021C0 RID: 8640
	public float TimeDelayToReactivate = 3f;
}
