﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200009F RID: 159
public class StorageContainer : global::DecayEntity
{
	// Token: 0x060009C8 RID: 2504 RVA: 0x000428F4 File Offset: 0x00040AF4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("StorageContainer.OnRpcMessage", 0.1f))
		{
			if (rpc == 4042031372u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_OpenLoot ");
				}
				using (TimeWarning.New("RPC_OpenLoot", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_OpenLoot", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_OpenLoot(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_OpenLoot");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009C9 RID: 2505 RVA: 0x00042AD4 File Offset: 0x00040CD4
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.yellow;
		Gizmos.DrawCube(this.dropPosition, Vector3.one * 0.1f);
		Gizmos.color = Color.white;
		Gizmos.DrawRay(this.dropPosition, this.dropVelocity);
	}

	// Token: 0x060009CA RID: 2506 RVA: 0x00042B30 File Offset: 0x00040D30
	public void MoveAllInventoryItems(global::ItemContainer source, global::ItemContainer dest)
	{
		for (int i = 0; i < Mathf.Min(source.capacity, dest.capacity); i++)
		{
			global::Item slot = source.GetSlot(i);
			if (slot != null)
			{
				slot.MoveToContainer(dest, -1, true);
			}
		}
	}

	// Token: 0x060009CB RID: 2507 RVA: 0x00042B78 File Offset: 0x00040D78
	public virtual void ReceiveInventoryFromItem(global::Item item)
	{
		if (item.contents != null)
		{
			this.MoveAllInventoryItems(item.contents, this.inventory);
		}
	}

	// Token: 0x060009CC RID: 2508 RVA: 0x00042B98 File Offset: 0x00040D98
	public override bool CanPickup(global::BasePlayer player)
	{
		bool flag = base.GetSlot(global::BaseEntity.Slot.Lock) != null;
		if (base.isClient)
		{
			return base.CanPickup(player) && !flag;
		}
		return (!this.pickup.requireEmptyInv || this.inventory == null || this.inventory.itemList.Count == 0) && base.CanPickup(player) && !flag;
	}

	// Token: 0x060009CD RID: 2509 RVA: 0x00042C18 File Offset: 0x00040E18
	public override void OnPickedUp(global::Item createdItem, global::BasePlayer player)
	{
		base.OnPickedUp(createdItem, player);
		if (createdItem != null && createdItem.contents != null)
		{
			this.MoveAllInventoryItems(this.inventory, createdItem.contents);
			return;
		}
		for (int i = 0; i < this.inventory.capacity; i++)
		{
			global::Item slot = this.inventory.GetSlot(i);
			if (slot != null)
			{
				slot.RemoveFromContainer();
				player.GiveItem(slot, global::BaseEntity.GiveItemReason.PickedUp);
			}
		}
	}

	// Token: 0x060009CE RID: 2510 RVA: 0x00042C90 File Offset: 0x00040E90
	public override void ResetState()
	{
		base.ResetState();
		if (this.inventory != null)
		{
			this.inventory.Clear();
		}
		this.inventory = null;
	}

	// Token: 0x060009CF RID: 2511 RVA: 0x00042CB8 File Offset: 0x00040EB8
	public override void ServerInit()
	{
		if (this.inventory == null)
		{
			this.CreateInventory(true);
			this.OnInventoryFirstCreated(this.inventory);
		}
		base.ServerInit();
	}

	// Token: 0x060009D0 RID: 2512 RVA: 0x00042CE0 File Offset: 0x00040EE0
	public virtual void OnInventoryFirstCreated(global::ItemContainer container)
	{
	}

	// Token: 0x060009D1 RID: 2513 RVA: 0x00042CE4 File Offset: 0x00040EE4
	public virtual void OnItemAddedOrRemoved(global::Item item, bool added)
	{
	}

	// Token: 0x060009D2 RID: 2514 RVA: 0x00042CE8 File Offset: 0x00040EE8
	public virtual bool ItemFilter(global::Item item, int targetSlot)
	{
		return this.onlyAcceptCategory == global::ItemCategory.All || item.info.category == this.onlyAcceptCategory;
	}

	// Token: 0x060009D3 RID: 2515 RVA: 0x00042D0C File Offset: 0x00040F0C
	public void CreateInventory(bool giveUID)
	{
		this.inventory = new global::ItemContainer();
		this.inventory.entityOwner = this;
		this.inventory.allowedContents = ((this.allowedContents != (global::ItemContainer.ContentsType)0) ? this.allowedContents : global::ItemContainer.ContentsType.Generic);
		this.inventory.onlyAllowedItem = this.allowedItem;
		this.inventory.maxStackSize = this.maxStackSize;
		this.inventory.ServerInitialize(null, this.inventorySlots);
		if (giveUID)
		{
			this.inventory.GiveUID();
		}
		this.inventory.onDirty += this.OnInventoryDirty;
		this.inventory.onItemAddedRemoved = new Action<global::Item, bool>(this.OnItemAddedOrRemoved);
		if (this.onlyAcceptCategory != global::ItemCategory.All)
		{
			this.inventory.canAcceptItem = new Func<global::Item, int, bool>(this.ItemFilter);
		}
	}

	// Token: 0x060009D4 RID: 2516 RVA: 0x00042DEC File Offset: 0x00040FEC
	public override void PreServerLoad()
	{
		base.PreServerLoad();
		this.CreateInventory(false);
	}

	// Token: 0x060009D5 RID: 2517 RVA: 0x00042DFC File Offset: 0x00040FFC
	protected virtual void OnInventoryDirty()
	{
		base.InvalidateNetworkCache();
	}

	// Token: 0x060009D6 RID: 2518 RVA: 0x00042E04 File Offset: 0x00041004
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		if (this.inventory != null && this.inventory.uid == 0u)
		{
			this.inventory.GiveUID();
		}
		base.SetFlag(global::BaseEntity.Flags.Open, false, false);
	}

	// Token: 0x060009D7 RID: 2519 RVA: 0x00042E3C File Offset: 0x0004103C
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		if (this.inventory != null)
		{
			this.inventory.Kill();
			this.inventory = null;
		}
	}

	// Token: 0x060009D8 RID: 2520 RVA: 0x00042E64 File Offset: 0x00041064
	public override bool HasSlot(global::BaseEntity.Slot slot)
	{
		return (this.isLockable && slot == global::BaseEntity.Slot.Lock) || base.HasSlot(slot);
	}

	// Token: 0x060009D9 RID: 2521 RVA: 0x00042E80 File Offset: 0x00041080
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_OpenLoot(global::BaseEntity.RPCMessage rpc)
	{
		if (!this.isLootable)
		{
			return;
		}
		global::BasePlayer player = rpc.player;
		this.PlayerOpenLoot(player);
	}

	// Token: 0x060009DA RID: 2522 RVA: 0x00042EAC File Offset: 0x000410AC
	public virtual string GetPanelName()
	{
		return this.panelName;
	}

	// Token: 0x060009DB RID: 2523 RVA: 0x00042EB4 File Offset: 0x000410B4
	public virtual bool CanMoveFrom(global::BasePlayer player, global::Item item)
	{
		return !this.inventory.IsLocked();
	}

	// Token: 0x060009DC RID: 2524 RVA: 0x00042EC4 File Offset: 0x000410C4
	public virtual bool CanOpenLootPanel(global::BasePlayer player, string panelName = "")
	{
		if (this.needsBuildingPrivilegeToUse && !player.CanBuild())
		{
			return false;
		}
		global::BaseLock baseLock = base.GetSlot(global::BaseEntity.Slot.Lock) as global::BaseLock;
		if (baseLock != null && !baseLock.OnTryToOpen(player))
		{
			player.ChatMessage("It is locked...");
			return false;
		}
		return true;
	}

	// Token: 0x060009DD RID: 2525 RVA: 0x00042F1C File Offset: 0x0004111C
	public virtual bool PlayerOpenLoot(global::BasePlayer player)
	{
		return this.PlayerOpenLoot(player, this.panelName);
	}

	// Token: 0x060009DE RID: 2526 RVA: 0x00042F2C File Offset: 0x0004112C
	public virtual bool PlayerOpenLoot(global::BasePlayer player, string panelToOpen)
	{
		object obj = Interface.CallHook("CanLootEntity", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		if (base.IsLocked())
		{
			player.ChatMessage("Can't loot right now");
			return false;
		}
		if (!this.CanOpenLootPanel(player, panelToOpen))
		{
			return false;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, true, false);
		using (TimeWarning.New("PlayerOpenLoot", 0.1f))
		{
			player.inventory.loot.StartLootingEntity(this, true);
			player.inventory.loot.AddContainer(this.inventory);
			player.inventory.loot.SendImmediate();
			player.ClientRPCPlayer<string>(null, player, "RPC_OpenLootPanel", panelToOpen);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
		return true;
	}

	// Token: 0x060009DF RID: 2527 RVA: 0x00043014 File Offset: 0x00041214
	public virtual void PlayerStoppedLooting(global::BasePlayer player)
	{
		Interface.CallHook("OnLootEntityEnd", new object[]
		{
			player,
			this
		});
		base.SetFlag(global::BaseEntity.Flags.Open, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060009E0 RID: 2528 RVA: 0x0004304C File Offset: 0x0004124C
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (info.forDisk)
		{
			if (this.inventory != null)
			{
				info.msg.storageBox = Facepunch.Pool.Get<StorageBox>();
				info.msg.storageBox.contents = this.inventory.Save();
			}
			else
			{
				Debug.LogWarning("Storage container without inventory: " + this.ToString());
			}
		}
	}

	// Token: 0x060009E1 RID: 2529 RVA: 0x000430C0 File Offset: 0x000412C0
	public override void OnKilled(global::HitInfo info)
	{
		this.DropItems();
		base.OnKilled(info);
	}

	// Token: 0x060009E2 RID: 2530 RVA: 0x000430D0 File Offset: 0x000412D0
	public void DropItems()
	{
		if (this.inventory == null || this.inventory.itemList == null || this.inventory.itemList.Count == 0)
		{
			return;
		}
		if (this.dropChance == 0f)
		{
			return;
		}
		if (this.ShouldDropItemsIndividually() || this.inventory.itemList.Count == 1)
		{
			global::DropUtil.DropItems(this.inventory, this.GetDropPosition(), 1f);
		}
		else
		{
			global::DroppedItemContainer droppedItemContainer = this.inventory.Drop("assets/prefabs/misc/item drop/item_drop.prefab", this.GetDropPosition(), base.transform.rotation);
			if (droppedItemContainer != null)
			{
			}
		}
	}

	// Token: 0x060009E3 RID: 2531 RVA: 0x0004318C File Offset: 0x0004138C
	public override Vector3 GetDropPosition()
	{
		return base.transform.localToWorldMatrix.MultiplyPoint(this.dropPosition);
	}

	// Token: 0x060009E4 RID: 2532 RVA: 0x000431B4 File Offset: 0x000413B4
	public override Vector3 GetDropVelocity()
	{
		return base.transform.localToWorldMatrix.MultiplyVector(this.dropPosition);
	}

	// Token: 0x060009E5 RID: 2533 RVA: 0x000431DC File Offset: 0x000413DC
	public virtual bool ShouldDropItemsIndividually()
	{
		return false;
	}

	// Token: 0x060009E6 RID: 2534 RVA: 0x000431E0 File Offset: 0x000413E0
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.storageBox != null)
		{
			if (this.inventory != null)
			{
				this.inventory.Load(info.msg.storageBox.contents);
				this.inventory.capacity = this.inventorySlots;
			}
			else
			{
				Debug.LogWarning("Storage container without inventory: " + this.ToString());
			}
		}
	}

	// Token: 0x0400048C RID: 1164
	public int inventorySlots = 6;

	// Token: 0x0400048D RID: 1165
	public float dropChance = 0.75f;

	// Token: 0x0400048E RID: 1166
	public bool isLootable = true;

	// Token: 0x0400048F RID: 1167
	public bool isLockable = true;

	// Token: 0x04000490 RID: 1168
	public string panelName = "generic";

	// Token: 0x04000491 RID: 1169
	public global::ItemContainer.ContentsType allowedContents;

	// Token: 0x04000492 RID: 1170
	public global::ItemDefinition allowedItem;

	// Token: 0x04000493 RID: 1171
	public int maxStackSize;

	// Token: 0x04000494 RID: 1172
	public bool needsBuildingPrivilegeToUse;

	// Token: 0x04000495 RID: 1173
	public global::SoundDefinition openSound;

	// Token: 0x04000496 RID: 1174
	public global::SoundDefinition closeSound;

	// Token: 0x04000497 RID: 1175
	[Header("Item Dropping")]
	public Vector3 dropPosition;

	// Token: 0x04000498 RID: 1176
	public Vector3 dropVelocity = Vector3.forward;

	// Token: 0x04000499 RID: 1177
	public global::ItemCategory onlyAcceptCategory = global::ItemCategory.All;

	// Token: 0x0400049A RID: 1178
	[NonSerialized]
	public global::ItemContainer inventory;
}
