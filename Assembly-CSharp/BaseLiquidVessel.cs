﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200002F RID: 47
public class BaseLiquidVessel : global::AttackEntity
{
	// Token: 0x06000438 RID: 1080 RVA: 0x00017750 File Offset: 0x00015950
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseLiquidVessel.OnRpcMessage", 0.1f))
		{
			if (rpc == 1829743341u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoDrink ");
				}
				using (TimeWarning.New("DoDrink", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoDrink", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoDrink(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoDrink");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 1512060663u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SendFilling ");
				}
				using (TimeWarning.New("SendFilling", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SendFilling(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in SendFilling");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 926465024u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ThrowContents ");
				}
				using (TimeWarning.New("ThrowContents", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ThrowContents(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in ThrowContents");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000439 RID: 1081 RVA: 0x00017B94 File Offset: 0x00015D94
	public override void ServerInit()
	{
		base.ServerInit();
		base.InvokeRepeating(new Action(this.FillCheck), 1f, 1f);
	}

	// Token: 0x0600043A RID: 1082 RVA: 0x00017BB8 File Offset: 0x00015DB8
	public override void OnHeldChanged()
	{
		base.OnHeldChanged();
		if (base.IsDisabled())
		{
			this.StopFilling();
		}
		if (!this.hasLid)
		{
			this.DoThrow(base.transform.position, Vector3.zero);
			global::Item item = this.GetItem();
			if (item == null)
			{
				return;
			}
			item.contents.SetLocked(base.IsDisabled());
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x0600043B RID: 1083 RVA: 0x00017C24 File Offset: 0x00015E24
	public void SetFilling(bool isFilling)
	{
		base.SetFlag(global::BaseEntity.Flags.Open, isFilling, false);
		if (isFilling)
		{
			this.StartFilling();
		}
		else
		{
			this.StopFilling();
		}
	}

	// Token: 0x0600043C RID: 1084 RVA: 0x00017C48 File Offset: 0x00015E48
	public void StartFilling()
	{
		float num = UnityEngine.Time.realtimeSinceStartup - this.lastFillTime;
		this.StopFilling();
		base.InvokeRepeating(new Action(this.FillCheck), 0f, 0.3f);
		if (num > 1f)
		{
			global::LiquidContainer facingLiquidContainer = this.GetFacingLiquidContainer();
			if (facingLiquidContainer != null && facingLiquidContainer.GetLiquidItem() != null)
			{
				global::Effect.server.Run(this.fillFromContainer.resourcePath, facingLiquidContainer.transform.position, Vector3.up, null, false);
			}
			else if (this.CanFillFromWorld())
			{
				global::Effect.server.Run(this.fillFromWorld.resourcePath, base.GetOwnerPlayer(), 0u, Vector3.zero, Vector3.up, null, false);
			}
		}
		this.lastFillTime = UnityEngine.Time.realtimeSinceStartup;
	}

	// Token: 0x0600043D RID: 1085 RVA: 0x00017D10 File Offset: 0x00015F10
	public void StopFilling()
	{
		base.CancelInvoke(new Action(this.FillCheck));
	}

	// Token: 0x0600043E RID: 1086 RVA: 0x00017D24 File Offset: 0x00015F24
	public void FillCheck()
	{
		if (base.isClient)
		{
			return;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		float num = UnityEngine.Time.realtimeSinceStartup - this.lastFillTime;
		Vector3 pos = ownerPlayer.transform.position - new Vector3(0f, 1f, 0f);
		if (this.CanFillFromWorld())
		{
			this.AddLiquid(global::WaterResource.GetAtPoint(pos), Mathf.FloorToInt(num * this.fillMlPerSec));
		}
		else
		{
			global::LiquidContainer facingLiquidContainer = this.GetFacingLiquidContainer();
			if (facingLiquidContainer != null && facingLiquidContainer.HasLiquidItem())
			{
				int num2 = Mathf.CeilToInt((1f - this.HeldFraction()) * (float)this.MaxHoldable());
				if (num2 > 0)
				{
					global::Item liquidItem = facingLiquidContainer.GetLiquidItem();
					int num3 = Mathf.Min(Mathf.CeilToInt(num * this.fillMlPerSec), Mathf.Min(liquidItem.amount, num2));
					this.AddLiquid(liquidItem.info, num3);
					liquidItem.UseItem(num3);
					facingLiquidContainer.OpenTap(2f);
				}
			}
		}
	}

	// Token: 0x0600043F RID: 1087 RVA: 0x00017E3C File Offset: 0x0001603C
	public void LoseWater(int amount)
	{
		global::Item item = this.GetItem();
		global::Item slot = item.contents.GetSlot(0);
		if (slot != null)
		{
			slot.UseItem(amount);
			slot.MarkDirty();
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x06000440 RID: 1088 RVA: 0x00017E78 File Offset: 0x00016078
	public void AddLiquid(global::ItemDefinition liquidType, int amount)
	{
		if (amount <= 0)
		{
			return;
		}
		global::Item item = this.GetItem();
		global::Item item2 = item.contents.GetSlot(0);
		global::ItemModContainer component = item.info.GetComponent<global::ItemModContainer>();
		if (item2 == null)
		{
			global::Item item3 = global::ItemManager.Create(liquidType, amount, 0UL);
			if (item3 != null)
			{
				item3.MoveToContainer(item.contents, -1, true);
			}
		}
		else
		{
			int num = Mathf.Clamp(item2.amount + amount, 0, component.maxStackSize);
			global::ItemDefinition itemDefinition = global::WaterResource.Merge(item2.info, liquidType);
			if (itemDefinition != item2.info)
			{
				item2.Remove(0f);
				item2 = global::ItemManager.Create(itemDefinition, num, 0UL);
				item2.MoveToContainer(item.contents, -1, true);
			}
			else
			{
				item2.amount = num;
			}
			item2.MarkDirty();
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x06000441 RID: 1089 RVA: 0x00017F54 File Offset: 0x00016154
	public int AmountHeld()
	{
		global::Item slot = this.GetItem().contents.GetSlot(0);
		if (slot == null)
		{
			return 0;
		}
		return slot.amount;
	}

	// Token: 0x06000442 RID: 1090 RVA: 0x00017F84 File Offset: 0x00016184
	public float HeldFraction()
	{
		return (float)this.AmountHeld() / (float)this.MaxHoldable();
	}

	// Token: 0x06000443 RID: 1091 RVA: 0x00017F98 File Offset: 0x00016198
	public int MaxHoldable()
	{
		return this.GetItem().info.GetComponent<global::ItemModContainer>().maxStackSize;
	}

	// Token: 0x06000444 RID: 1092 RVA: 0x00017FB0 File Offset: 0x000161B0
	public bool CanDrink()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return false;
		}
		if (!ownerPlayer.metabolism.CanConsume())
		{
			return false;
		}
		if (!this.canDrinkFrom)
		{
			return false;
		}
		global::Item item = this.GetItem();
		return item != null && item.contents != null && item.contents.itemList != null && item.contents.itemList.Count != 0;
	}

	// Token: 0x06000445 RID: 1093 RVA: 0x00018038 File Offset: 0x00016238
	private bool IsWeaponBusy()
	{
		return UnityEngine.Time.realtimeSinceStartup < this.nextFreeTime;
	}

	// Token: 0x06000446 RID: 1094 RVA: 0x00018048 File Offset: 0x00016248
	private void SetBusyFor(float dur)
	{
		this.nextFreeTime = UnityEngine.Time.realtimeSinceStartup + dur;
	}

	// Token: 0x06000447 RID: 1095 RVA: 0x00018058 File Offset: 0x00016258
	private void ClearBusy()
	{
		this.nextFreeTime = UnityEngine.Time.realtimeSinceStartup - 1f;
	}

	// Token: 0x06000448 RID: 1096 RVA: 0x0001806C File Offset: 0x0001626C
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void DoDrink(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		global::Item item = this.GetItem();
		if (item == null)
		{
			return;
		}
		if (item.contents == null)
		{
			return;
		}
		if (!msg.player.metabolism.CanConsume())
		{
			return;
		}
		foreach (global::Item item2 in item.contents.itemList)
		{
			global::ItemModConsume component = item2.info.GetComponent<global::ItemModConsume>();
			if (!(component == null))
			{
				if (component.CanDoAction(item2, msg.player))
				{
					component.DoAction(item2, msg.player);
					break;
				}
			}
		}
	}

	// Token: 0x06000449 RID: 1097 RVA: 0x00018154 File Offset: 0x00016354
	[global::BaseEntity.RPC_Server]
	private void ThrowContents(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		this.DoThrow(ownerPlayer.eyes.position + ownerPlayer.eyes.BodyForward() * 1f, ownerPlayer.estimatedVelocity + ownerPlayer.eyes.BodyForward() * this.throwScale);
		global::Effect.server.Run(this.ThrowEffect3P.resourcePath, ownerPlayer.transform.position, ownerPlayer.eyes.BodyForward(), ownerPlayer.net.connection, false);
	}

	// Token: 0x0600044A RID: 1098 RVA: 0x000181F4 File Offset: 0x000163F4
	public void DoThrow(Vector3 pos, Vector3 velocity)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		global::Item item = this.GetItem();
		if (item == null)
		{
			return;
		}
		if (item.contents == null)
		{
			return;
		}
		global::Item slot = item.contents.GetSlot(0);
		if (slot != null && slot.amount > 0)
		{
			Vector3 vector = ownerPlayer.eyes.position + ownerPlayer.eyes.BodyForward() * 1f;
			global::WaterBall waterBall = global::GameManager.server.CreateEntity(this.thrownWaterObject.resourcePath, vector, Quaternion.identity, true) as global::WaterBall;
			if (waterBall)
			{
				waterBall.liquidType = slot.info;
				waterBall.waterAmount = slot.amount;
				waterBall.transform.position = vector;
				waterBall.SetVelocity(velocity);
				waterBall.Spawn();
			}
			slot.UseItem(slot.amount);
			slot.MarkDirty();
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x0600044B RID: 1099 RVA: 0x000182F4 File Offset: 0x000164F4
	[global::BaseEntity.RPC_Server]
	private void SendFilling(global::BaseEntity.RPCMessage msg)
	{
		bool filling = msg.read.Bit();
		this.SetFilling(filling);
	}

	// Token: 0x0600044C RID: 1100 RVA: 0x00018318 File Offset: 0x00016518
	public bool CanFillFromWorld()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		return ownerPlayer && ownerPlayer.WaterFactor() >= 0.05f;
	}

	// Token: 0x0600044D RID: 1101 RVA: 0x0001834C File Offset: 0x0001654C
	public bool CanThrow()
	{
		return this.HeldFraction() > this.minThrowFrac;
	}

	// Token: 0x0600044E RID: 1102 RVA: 0x0001835C File Offset: 0x0001655C
	public global::LiquidContainer GetFacingLiquidContainer()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return null;
		}
		RaycastHit hit;
		if (UnityEngine.Physics.Raycast(ownerPlayer.eyes.HeadRay(), ref hit, 2f, 1101212417))
		{
			global::BaseEntity baseEntity = hit.GetEntity();
			if (baseEntity && !hit.collider.gameObject.CompareTag("Not Player Usable") && !hit.collider.gameObject.CompareTag("Usable Primary"))
			{
				baseEntity = baseEntity.ToServer<global::BaseEntity>();
				return baseEntity.GetComponent<global::LiquidContainer>();
			}
		}
		return null;
	}

	// Token: 0x04000126 RID: 294
	[Header("Liquid Vessel")]
	public global::GameObjectRef thrownWaterObject;

	// Token: 0x04000127 RID: 295
	public global::GameObjectRef ThrowEffect3P;

	// Token: 0x04000128 RID: 296
	public global::SoundDefinition throwSound3P;

	// Token: 0x04000129 RID: 297
	public global::GameObjectRef fillFromContainer;

	// Token: 0x0400012A RID: 298
	public global::GameObjectRef fillFromWorld;

	// Token: 0x0400012B RID: 299
	public bool hasLid;

	// Token: 0x0400012C RID: 300
	public float throwScale = 10f;

	// Token: 0x0400012D RID: 301
	public bool canDrinkFrom;

	// Token: 0x0400012E RID: 302
	public bool updateVMWater;

	// Token: 0x0400012F RID: 303
	public float minThrowFrac;

	// Token: 0x04000130 RID: 304
	public bool useThrowAnim;

	// Token: 0x04000131 RID: 305
	public float fillMlPerSec = 500f;

	// Token: 0x04000132 RID: 306
	private float lastFillTime;

	// Token: 0x04000133 RID: 307
	private float nextFreeTime;
}
