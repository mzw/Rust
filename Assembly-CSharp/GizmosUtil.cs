﻿using System;
using UnityEngine;

// Token: 0x02000788 RID: 1928
public static class GizmosUtil
{
	// Token: 0x060023C3 RID: 9155 RVA: 0x000C5BF0 File Offset: 0x000C3DF0
	public static void DrawWireCircleX(Vector3 pos, float radius)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix *= Matrix4x4.TRS(pos, Quaternion.identity, new Vector3(0f, 1f, 1f));
		Gizmos.DrawWireSphere(Vector3.zero, radius);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023C4 RID: 9156 RVA: 0x000C5C44 File Offset: 0x000C3E44
	public static void DrawWireCircleY(Vector3 pos, float radius)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix *= Matrix4x4.TRS(pos, Quaternion.identity, new Vector3(1f, 0f, 1f));
		Gizmos.DrawWireSphere(Vector3.zero, radius);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023C5 RID: 9157 RVA: 0x000C5C98 File Offset: 0x000C3E98
	public static void DrawWireCircleZ(Vector3 pos, float radius)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix *= Matrix4x4.TRS(pos, Quaternion.identity, new Vector3(1f, 1f, 0f));
		Gizmos.DrawWireSphere(Vector3.zero, radius);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023C6 RID: 9158 RVA: 0x000C5CEC File Offset: 0x000C3EEC
	public static void DrawCircleX(Vector3 pos, float radius)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix *= Matrix4x4.TRS(pos, Quaternion.identity, new Vector3(0f, 1f, 1f));
		Gizmos.DrawSphere(Vector3.zero, radius);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023C7 RID: 9159 RVA: 0x000C5D40 File Offset: 0x000C3F40
	public static void DrawCircleY(Vector3 pos, float radius)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix *= Matrix4x4.TRS(pos, Quaternion.identity, new Vector3(1f, 0f, 1f));
		Gizmos.DrawSphere(Vector3.zero, radius);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023C8 RID: 9160 RVA: 0x000C5D94 File Offset: 0x000C3F94
	public static void DrawCircleZ(Vector3 pos, float radius)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix *= Matrix4x4.TRS(pos, Quaternion.identity, new Vector3(1f, 1f, 0f));
		Gizmos.DrawSphere(Vector3.zero, radius);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023C9 RID: 9161 RVA: 0x000C5DE8 File Offset: 0x000C3FE8
	public static void DrawWireCylinderX(Vector3 pos, float radius, float height)
	{
		global::GizmosUtil.DrawWireCircleX(pos - new Vector3(0.5f * height, 0f, 0f), radius);
		global::GizmosUtil.DrawWireCircleX(pos + new Vector3(0.5f * height, 0f, 0f), radius);
	}

	// Token: 0x060023CA RID: 9162 RVA: 0x000C5E3C File Offset: 0x000C403C
	public static void DrawWireCylinderY(Vector3 pos, float radius, float height)
	{
		global::GizmosUtil.DrawWireCircleY(pos - new Vector3(0f, 0.5f * height, 0f), radius);
		global::GizmosUtil.DrawWireCircleY(pos + new Vector3(0f, 0.5f * height, 0f), radius);
	}

	// Token: 0x060023CB RID: 9163 RVA: 0x000C5E90 File Offset: 0x000C4090
	public static void DrawWireCylinderZ(Vector3 pos, float radius, float height)
	{
		global::GizmosUtil.DrawWireCircleZ(pos - new Vector3(0f, 0f, 0.5f * height), radius);
		global::GizmosUtil.DrawWireCircleZ(pos + new Vector3(0f, 0f, 0.5f * height), radius);
	}

	// Token: 0x060023CC RID: 9164 RVA: 0x000C5EE4 File Offset: 0x000C40E4
	public static void DrawCylinderX(Vector3 pos, float radius, float height)
	{
		global::GizmosUtil.DrawCircleX(pos - new Vector3(0.5f * height, 0f, 0f), radius);
		global::GizmosUtil.DrawCircleX(pos + new Vector3(0.5f * height, 0f, 0f), radius);
	}

	// Token: 0x060023CD RID: 9165 RVA: 0x000C5F38 File Offset: 0x000C4138
	public static void DrawCylinderY(Vector3 pos, float radius, float height)
	{
		global::GizmosUtil.DrawCircleY(pos - new Vector3(0f, 0.5f * height, 0f), radius);
		global::GizmosUtil.DrawCircleY(pos + new Vector3(0f, 0.5f * height, 0f), radius);
	}

	// Token: 0x060023CE RID: 9166 RVA: 0x000C5F8C File Offset: 0x000C418C
	public static void DrawCylinderZ(Vector3 pos, float radius, float height)
	{
		global::GizmosUtil.DrawCircleZ(pos - new Vector3(0f, 0f, 0.5f * height), radius);
		global::GizmosUtil.DrawCircleZ(pos + new Vector3(0f, 0f, 0.5f * height), radius);
	}

	// Token: 0x060023CF RID: 9167 RVA: 0x000C5FE0 File Offset: 0x000C41E0
	public static void DrawWireCapsuleX(Vector3 pos, float radius, float height)
	{
		Vector3 vector = pos - new Vector3(0.5f * height, 0f, 0f);
		Vector3 vector2 = pos + new Vector3(0.5f * height, 0f, 0f);
		Gizmos.DrawWireSphere(vector, radius);
		Gizmos.DrawWireSphere(vector2, radius);
		Gizmos.DrawLine(vector + Vector3.forward * radius, vector2 + Vector3.forward * radius);
		Gizmos.DrawLine(vector + Vector3.up * radius, vector2 + Vector3.up * radius);
		Gizmos.DrawLine(vector + Vector3.back * radius, vector2 + Vector3.back * radius);
		Gizmos.DrawLine(vector + Vector3.down * radius, vector2 + Vector3.down * radius);
	}

	// Token: 0x060023D0 RID: 9168 RVA: 0x000C60D4 File Offset: 0x000C42D4
	public static void DrawWireCapsuleY(Vector3 pos, float radius, float height)
	{
		Vector3 vector = pos - new Vector3(0f, 0.5f * height, 0f);
		Vector3 vector2 = pos + new Vector3(0f, 0.5f * height, 0f);
		Gizmos.DrawWireSphere(vector, radius);
		Gizmos.DrawWireSphere(vector2, radius);
		Gizmos.DrawLine(vector + Vector3.forward * radius, vector2 + Vector3.forward * radius);
		Gizmos.DrawLine(vector + Vector3.right * radius, vector2 + Vector3.right * radius);
		Gizmos.DrawLine(vector + Vector3.back * radius, vector2 + Vector3.back * radius);
		Gizmos.DrawLine(vector + Vector3.left * radius, vector2 + Vector3.left * radius);
	}

	// Token: 0x060023D1 RID: 9169 RVA: 0x000C61C8 File Offset: 0x000C43C8
	public static void DrawWireCapsuleZ(Vector3 pos, float radius, float height)
	{
		Vector3 vector = pos - new Vector3(0f, 0f, 0.5f * height);
		Vector3 vector2 = pos + new Vector3(0f, 0f, 0.5f * height);
		Gizmos.DrawWireSphere(vector, radius);
		Gizmos.DrawWireSphere(vector2, radius);
		Gizmos.DrawLine(vector + Vector3.up * radius, vector2 + Vector3.up * radius);
		Gizmos.DrawLine(vector + Vector3.right * radius, vector2 + Vector3.right * radius);
		Gizmos.DrawLine(vector + Vector3.down * radius, vector2 + Vector3.down * radius);
		Gizmos.DrawLine(vector + Vector3.left * radius, vector2 + Vector3.left * radius);
	}

	// Token: 0x060023D2 RID: 9170 RVA: 0x000C62BC File Offset: 0x000C44BC
	public static void DrawCapsuleX(Vector3 pos, float radius, float height)
	{
		Vector3 vector = pos - new Vector3(0.5f * height, 0f, 0f);
		Vector3 vector2 = pos + new Vector3(0.5f * height, 0f, 0f);
		Gizmos.DrawSphere(vector, radius);
		Gizmos.DrawSphere(vector2, radius);
	}

	// Token: 0x060023D3 RID: 9171 RVA: 0x000C6314 File Offset: 0x000C4514
	public static void DrawCapsuleY(Vector3 pos, float radius, float height)
	{
		Vector3 vector = pos - new Vector3(0f, 0.5f * height, 0f);
		Vector3 vector2 = pos + new Vector3(0f, 0.5f * height, 0f);
		Gizmos.DrawSphere(vector, radius);
		Gizmos.DrawSphere(vector2, radius);
	}

	// Token: 0x060023D4 RID: 9172 RVA: 0x000C636C File Offset: 0x000C456C
	public static void DrawCapsuleZ(Vector3 pos, float radius, float height)
	{
		Vector3 vector = pos - new Vector3(0f, 0f, 0.5f * height);
		Vector3 vector2 = pos + new Vector3(0f, 0f, 0.5f * height);
		Gizmos.DrawSphere(vector, radius);
		Gizmos.DrawSphere(vector2, radius);
	}

	// Token: 0x060023D5 RID: 9173 RVA: 0x000C63C4 File Offset: 0x000C45C4
	public static void DrawWireCube(Vector3 pos, Vector3 size, Quaternion rot)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(pos, rot, size);
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023D6 RID: 9174 RVA: 0x000C63FC File Offset: 0x000C45FC
	public static void DrawCube(Vector3 pos, Vector3 size, Quaternion rot)
	{
		Matrix4x4 matrix = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(pos, rot, size);
		Gizmos.DrawCube(Vector3.zero, Vector3.one);
		Gizmos.matrix = matrix;
	}

	// Token: 0x060023D7 RID: 9175 RVA: 0x000C6434 File Offset: 0x000C4634
	public static void DrawWirePath(Vector3 a, Vector3 b, float thickness)
	{
		global::GizmosUtil.DrawWireCircleY(a, thickness);
		global::GizmosUtil.DrawWireCircleY(b, thickness);
		Vector3 normalized = (b - a).normalized;
		Vector3 vector = Quaternion.Euler(0f, 90f, 0f) * normalized;
		Gizmos.DrawLine(b + vector * thickness, a + vector * thickness);
		Gizmos.DrawLine(b - vector * thickness, a - vector * thickness);
	}

	// Token: 0x060023D8 RID: 9176 RVA: 0x000C64B8 File Offset: 0x000C46B8
	public static void DrawSemiCircle(float radius)
	{
		float num = radius * 0.0174532924f * 0.5f;
		Vector3 vector = Mathf.Cos(num) * Vector3.forward + Mathf.Sin(num) * Vector3.right;
		Gizmos.DrawLine(Vector3.zero, vector);
		Vector3 vector2 = Mathf.Cos(-num) * Vector3.forward + Mathf.Sin(-num) * Vector3.right;
		Gizmos.DrawLine(Vector3.zero, vector2);
		float num2 = Mathf.Clamp(radius / 16f, 4f, 64f);
		float num3 = num / num2;
		for (float num4 = num; num4 > 0f; num4 -= num3)
		{
			Vector3 vector3 = Mathf.Cos(num4) * Vector3.forward + Mathf.Sin(num4) * Vector3.right;
			Gizmos.DrawLine(Vector3.zero, vector3);
			if (vector != Vector3.zero)
			{
				Gizmos.DrawLine(vector3, vector);
			}
			vector = vector3;
			Vector3 vector4 = Mathf.Cos(-num4) * Vector3.forward + Mathf.Sin(-num4) * Vector3.right;
			Gizmos.DrawLine(Vector3.zero, vector4);
			if (vector2 != Vector3.zero)
			{
				Gizmos.DrawLine(vector4, vector2);
			}
			vector2 = vector4;
		}
		Gizmos.DrawLine(vector, vector2);
	}

	// Token: 0x060023D9 RID: 9177 RVA: 0x000C6618 File Offset: 0x000C4818
	public static void DrawMeshes(Transform transform)
	{
		foreach (MeshRenderer meshRenderer in transform.GetComponentsInChildren<MeshRenderer>())
		{
			if (meshRenderer.enabled)
			{
				MeshFilter component = meshRenderer.GetComponent<MeshFilter>();
				if (component)
				{
					Transform transform2 = meshRenderer.transform;
					Gizmos.DrawMesh(component.sharedMesh, transform2.position, transform2.rotation, transform2.lossyScale);
				}
			}
		}
	}

	// Token: 0x060023DA RID: 9178 RVA: 0x000C6694 File Offset: 0x000C4894
	public static void DrawBounds(Transform transform)
	{
		Bounds bounds = transform.GetBounds(true, false, true);
		Vector3 lossyScale = transform.lossyScale;
		Quaternion rotation = transform.rotation;
		Vector3 pos = transform.position + rotation * Vector3.Scale(lossyScale, bounds.center);
		Vector3 size = Vector3.Scale(lossyScale, bounds.size);
		global::GizmosUtil.DrawCube(pos, size, rotation);
		global::GizmosUtil.DrawWireCube(pos, size, rotation);
	}
}
