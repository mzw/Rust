﻿using System;
using UnityEngine;

// Token: 0x020001ED RID: 493
public class MusicUtil
{
	// Token: 0x06000F3D RID: 3901 RVA: 0x0005DA8C File Offset: 0x0005BC8C
	public static double BeatsToSeconds(float tempo, float beats)
	{
		return 60.0 / (double)tempo * (double)beats;
	}

	// Token: 0x06000F3E RID: 3902 RVA: 0x0005DAA0 File Offset: 0x0005BCA0
	public static double BarsToSeconds(float tempo, float bars)
	{
		return global::MusicUtil.BeatsToSeconds(tempo, bars * 4f);
	}

	// Token: 0x06000F3F RID: 3903 RVA: 0x0005DAB0 File Offset: 0x0005BCB0
	public static int SecondsToSamples(double seconds)
	{
		return global::MusicUtil.SecondsToSamples(seconds, UnityEngine.AudioSettings.outputSampleRate);
	}

	// Token: 0x06000F40 RID: 3904 RVA: 0x0005DAC0 File Offset: 0x0005BCC0
	public static int SecondsToSamples(double seconds, int sampleRate)
	{
		return (int)((double)sampleRate * seconds);
	}

	// Token: 0x06000F41 RID: 3905 RVA: 0x0005DAC8 File Offset: 0x0005BCC8
	public static int SecondsToSamples(float seconds)
	{
		return global::MusicUtil.SecondsToSamples(seconds, UnityEngine.AudioSettings.outputSampleRate);
	}

	// Token: 0x06000F42 RID: 3906 RVA: 0x0005DAD8 File Offset: 0x0005BCD8
	public static int SecondsToSamples(float seconds, int sampleRate)
	{
		return (int)((float)sampleRate * seconds);
	}

	// Token: 0x06000F43 RID: 3907 RVA: 0x0005DAE0 File Offset: 0x0005BCE0
	public static int BarsToSamples(float tempo, float bars, int sampleRate)
	{
		return global::MusicUtil.SecondsToSamples(global::MusicUtil.BarsToSeconds(tempo, bars), sampleRate);
	}

	// Token: 0x06000F44 RID: 3908 RVA: 0x0005DAF0 File Offset: 0x0005BCF0
	public static int BarsToSamples(float tempo, float bars)
	{
		return global::MusicUtil.SecondsToSamples(global::MusicUtil.BarsToSeconds(tempo, bars));
	}

	// Token: 0x06000F45 RID: 3909 RVA: 0x0005DB00 File Offset: 0x0005BD00
	public static int BeatsToSamples(float tempo, float beats)
	{
		return global::MusicUtil.SecondsToSamples(global::MusicUtil.BeatsToSeconds(tempo, beats));
	}

	// Token: 0x06000F46 RID: 3910 RVA: 0x0005DB10 File Offset: 0x0005BD10
	public static float SecondsToBeats(float tempo, double seconds)
	{
		return tempo / 60f * (float)seconds;
	}

	// Token: 0x06000F47 RID: 3911 RVA: 0x0005DB1C File Offset: 0x0005BD1C
	public static float SecondsToBars(float tempo, double seconds)
	{
		return global::MusicUtil.SecondsToBeats(tempo, seconds) / 4f;
	}

	// Token: 0x06000F48 RID: 3912 RVA: 0x0005DB2C File Offset: 0x0005BD2C
	public static float Quantize(float position, float gridSize)
	{
		return Mathf.Round(position / gridSize) * gridSize;
	}

	// Token: 0x06000F49 RID: 3913 RVA: 0x0005DB38 File Offset: 0x0005BD38
	public static float FlooredQuantize(float position, float gridSize)
	{
		return Mathf.Floor(position / gridSize) * gridSize;
	}

	// Token: 0x040009A9 RID: 2473
	public const float OneSixteenth = 0.0625f;
}
