﻿using System;
using UnityEngine;

// Token: 0x020003C9 RID: 969
public class BaseMovement : MonoBehaviour
{
	// Token: 0x04001168 RID: 4456
	[NonSerialized]
	public bool adminCheat;

	// Token: 0x04001169 RID: 4457
	[NonSerialized]
	public float adminSpeed = 1f;
}
