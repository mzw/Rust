﻿using System;
using UnityEngine;

// Token: 0x020005D0 RID: 1488
public class ApplyTerrainModifiers : MonoBehaviour
{
	// Token: 0x06001EB7 RID: 7863 RVA: 0x000AD6B8 File Offset: 0x000AB8B8
	protected void Awake()
	{
		global::BaseEntity component = base.GetComponent<global::BaseEntity>();
		global::TerrainModifier[] modifiers = null;
		if (component.isServer)
		{
			modifiers = global::PrefabAttribute.server.FindAll<global::TerrainModifier>(component.prefabID);
		}
		base.transform.ApplyTerrainModifiers(modifiers);
		global::GameManager.Destroy(this, 0f);
	}
}
