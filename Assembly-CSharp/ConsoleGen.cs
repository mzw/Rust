﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Facepunch.Extend;
using Rust.Ai;
using UnityEngine;

// Token: 0x02000009 RID: 9
public class ConsoleGen
{
	// Token: 0x0400002F RID: 47
	public static ConsoleSystem.Command[] All = new ConsoleSystem.Command[]
	{
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "bear",
			FullName = "bear.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Bear.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Bear.Population = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "boar",
			FullName = "boar.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Boar.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Boar.Population = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "chicken",
			FullName = "chicken.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Chicken.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Chicken.Population = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "echo",
			Parent = "commands",
			FullName = "commands.echo",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				Commands.Echo(arg.FullString);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find",
			Parent = "commands",
			FullName = "commands.find",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				Commands.Find(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ban",
			Parent = "global",
			FullName = "global.ban",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.ban(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "banid",
			Parent = "global",
			FullName = "global.banid",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.banid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "banlist",
			Parent = "global",
			FullName = "global.banlist",
			ServerAdmin = true,
			Description = "List of banned users (sourceds compat)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.banlist(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "banlistex",
			Parent = "global",
			FullName = "global.banlistex",
			ServerAdmin = true,
			Description = "List of banned users - shows reasons and usernames",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.banlistex(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bans",
			Parent = "global",
			FullName = "global.bans",
			ServerAdmin = true,
			Description = "List of banned users",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				global::ServerUsers.User[] array = ConVar.Admin.Bans();
				arg.ReplyWithObject(array);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "buildinfo",
			Parent = "global",
			FullName = "global.buildinfo",
			ServerAdmin = true,
			Description = "Get information about this build",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				BuildInfo buildInfo = ConVar.Admin.BuildInfo();
				arg.ReplyWithObject(buildInfo);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "clientperf",
			Parent = "global",
			FullName = "global.clientperf",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.clientperf(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "entid",
			Parent = "global",
			FullName = "global.entid",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.entid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "kick",
			Parent = "global",
			FullName = "global.kick",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.kick(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "kickall",
			Parent = "global",
			FullName = "global.kickall",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.kickall(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "listid",
			Parent = "global",
			FullName = "global.listid",
			ServerAdmin = true,
			Description = "List of banned users, by ID (sourceds compat)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.listid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "moderatorid",
			Parent = "global",
			FullName = "global.moderatorid",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.moderatorid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "mutechat",
			Parent = "global",
			FullName = "global.mutechat",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.mutechat(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "mutevoice",
			Parent = "global",
			FullName = "global.mutevoice",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.mutevoice(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ownerid",
			Parent = "global",
			FullName = "global.ownerid",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.ownerid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "playerlist",
			Parent = "global",
			FullName = "global.playerlist",
			ServerAdmin = true,
			Description = "Get a list of players",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.PlayerInfo[] array = ConVar.Admin.playerlist();
				arg.ReplyWithObject(array);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "players",
			Parent = "global",
			FullName = "global.players",
			ServerAdmin = true,
			Description = "Print out currently connected clients etc",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.players(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "removemoderator",
			Parent = "global",
			FullName = "global.removemoderator",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.removemoderator(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "removeowner",
			Parent = "global",
			FullName = "global.removeowner",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.removeowner(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "say",
			Parent = "global",
			FullName = "global.say",
			ServerAdmin = true,
			Description = "Sends a message in chat",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.say(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "serverinfo",
			Parent = "global",
			FullName = "global.serverinfo",
			ServerAdmin = true,
			Description = "Get a list of information about the server",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.ServerInfoOutput serverInfoOutput = ConVar.Admin.ServerInfo();
				arg.ReplyWithObject(serverInfoOutput);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "skipqueue",
			Parent = "global",
			FullName = "global.skipqueue",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.skipqueue(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "stats",
			Parent = "global",
			FullName = "global.stats",
			ServerAdmin = true,
			Description = "Print out stats of currently connected clients",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.stats(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "status",
			Parent = "global",
			FullName = "global.status",
			ServerAdmin = true,
			Description = "Print out currently connected clients",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.status(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "unban",
			Parent = "global",
			FullName = "global.unban",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.unban(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "unmutechat",
			Parent = "global",
			FullName = "global.unmutechat",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.unmutechat(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "unmutevoice",
			Parent = "global",
			FullName = "global.unmutevoice",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.unmutevoice(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "users",
			Parent = "global",
			FullName = "global.users",
			ServerAdmin = true,
			Description = "Show user info for players on server.",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Admin.users(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "aidebug_loadbalanceoverduereportserver",
			Parent = "ai",
			FullName = "ai.aidebug_loadbalanceoverduereportserver",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.aiDebug_LoadBalanceOverdueReportServer(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "aidebug_toggle",
			Parent = "ai",
			FullName = "ai.aidebug_toggle",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.aiDebug_toggle(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ailoadbalancerupdateinterval",
			Parent = "ai",
			FullName = "ai.ailoadbalancerupdateinterval",
			ServerAdmin = true,
			Description = "Set the update interval for the behaviour ai of animals and npcs. (Default: 0.25)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.aiLoadBalancerUpdateInterval(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "aimanagerloadbalancerupdateinterval",
			Parent = "ai",
			FullName = "ai.aimanagerloadbalancerupdateinterval",
			ServerAdmin = true,
			Description = "Set the update interval for the agency of dormant and active animals and npcs. (Default: 2.0)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.aiManagerLoadBalancerUpdateInterval(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "animalsenseloadbalancerupdateinterval",
			Parent = "ai",
			FullName = "ai.animalsenseloadbalancerupdateinterval",
			ServerAdmin = true,
			Description = "Set the update interval for animal senses that updates the knowledge gathering of animals. (Default: 0.2)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.AnimalSenseLoadBalancerUpdateInterval(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "defaultloadbalancerupdateinterval",
			Parent = "ai",
			FullName = "ai.defaultloadbalancerupdateinterval",
			ServerAdmin = true,
			Description = "Set the update interval for the default load balancer, currently used for cover point generation. (Default: 2.5)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.defaultLoadBalancerUpdateInterval(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "frametime",
			Parent = "ai",
			FullName = "ai.frametime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AI.frametime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.frametime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ignoreplayers",
			Parent = "ai",
			FullName = "ai.ignoreplayers",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AI.ignoreplayers.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.ignoreplayers = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "move",
			Parent = "ai",
			FullName = "ai.move",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AI.move.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.move = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_alertness_drain_rate",
			Parent = "ai",
			FullName = "ai.npc_alertness_drain_rate",
			ServerAdmin = true,
			Description = "npc_alertness_drain_rate define the rate at which we drain the alertness level of an NPC when there are no enemies in sight. (Default: 0.01)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_alertness_drain_rate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_alertness_drain_rate = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_alertness_zero_detection_mod",
			Parent = "ai",
			FullName = "ai.npc_alertness_zero_detection_mod",
			ServerAdmin = true,
			Description = "npc_alertness_zero_detection_mod define the threshold of visibility required to detect an enemy when alertness is zero. (Default: 0.5)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_alertness_zero_detection_mod.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_alertness_zero_detection_mod = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_cover_compromised_cooldown",
			Parent = "ai",
			FullName = "ai.npc_cover_compromised_cooldown",
			ServerAdmin = true,
			Description = "npc_cover_compromised_cooldown defines how long a cover point is marked as compromised before it's cleared again for selection. (default: 10)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_cover_compromised_cooldown.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_cover_compromised_cooldown = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_cover_path_vs_straight_dist_max_diff",
			Parent = "ai",
			FullName = "ai.npc_cover_path_vs_straight_dist_max_diff",
			ServerAdmin = true,
			Description = "npc_cover_path_vs_straight_dist_max_diff defines what the maximum difference between straight-line distance and path distance can be when evaluating cover points. (default: 2)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_cover_path_vs_straight_dist_max_diff.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_cover_path_vs_straight_dist_max_diff = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_cover_use_path_distance",
			Parent = "ai",
			FullName = "ai.npc_cover_use_path_distance",
			ServerAdmin = true,
			Description = "If npc_cover_use_path_distance is set to true then npcs will look at the distance between the cover point and their target using the path between the two, rather than the straight-line distance.",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_cover_use_path_distance.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_cover_use_path_distance = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_door_trigger_size",
			Parent = "ai",
			FullName = "ai.npc_door_trigger_size",
			ServerAdmin = true,
			Description = "npc_door_trigger_size defines the size of the trigger box on doors that opens the door as npcs walk close to it (default: 1.5)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_door_trigger_size.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_door_trigger_size = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_enable",
			Parent = "ai",
			FullName = "ai.npc_enable",
			ServerAdmin = true,
			Description = "If npc_enable is set to false then npcs won't spawn. (default: false)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_enable.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_enable = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_families_no_hurt",
			Parent = "ai",
			FullName = "ai.npc_families_no_hurt",
			ServerAdmin = true,
			Description = "If npc_families_no_hurt is true, npcs of the same family won't be able to hurt each other. (default: true)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_families_no_hurt.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_families_no_hurt = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_junkpile_a_spawn_chance",
			Parent = "ai",
			FullName = "ai.npc_junkpile_a_spawn_chance",
			ServerAdmin = true,
			Description = "npc_junkpile_a_spawn_chance define the chance for scientists to spawn at junkpile a. (Default: 0)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_junkpile_a_spawn_chance.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_junkpile_a_spawn_chance = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_junkpile_g_spawn_chance",
			Parent = "ai",
			FullName = "ai.npc_junkpile_g_spawn_chance",
			ServerAdmin = true,
			Description = "npc_junkpile_g_spawn_chance define the chance for scientists to spawn at junkpile g. (Default: 0)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_junkpile_g_spawn_chance.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_junkpile_g_spawn_chance = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_max_junkpile_count",
			Parent = "ai",
			FullName = "ai.npc_max_junkpile_count",
			ServerAdmin = true,
			Description = "npc_max_junkpile_count define how many npcs can spawn into the world at junkpiles at the same time (does not include monuments) (Default: 20)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_max_junkpile_count.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_max_junkpile_count = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_max_population_military_tunnels",
			Parent = "ai",
			FullName = "ai.npc_max_population_military_tunnels",
			ServerAdmin = true,
			Description = "npc_max_population_military_tunnels defines the size of the npc population at military tunnels. (default: 3)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_max_population_military_tunnels.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_max_population_military_tunnels = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_patrol_point_cooldown",
			Parent = "ai",
			FullName = "ai.npc_patrol_point_cooldown",
			ServerAdmin = true,
			Description = "npc_patrol_point_cooldown defines the cooldown time on a patrol point until it's available again (default: 5)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_patrol_point_cooldown.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_patrol_point_cooldown = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_respawn_delay_max_military_tunnels",
			Parent = "ai",
			FullName = "ai.npc_respawn_delay_max_military_tunnels",
			ServerAdmin = true,
			Description = "npc_respawn_delay_max_military_tunnels defines the maximum delay between spawn ticks at military tunnels. (default: 1920)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_respawn_delay_max_military_tunnels.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_respawn_delay_max_military_tunnels = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_respawn_delay_min_military_tunnels",
			Parent = "ai",
			FullName = "ai.npc_respawn_delay_min_military_tunnels",
			ServerAdmin = true,
			Description = "npc_respawn_delay_min_military_tunnels defines the minimum delay between spawn ticks at military tunnels. (default: 480)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_respawn_delay_min_military_tunnels.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_respawn_delay_min_military_tunnels = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_spawn_per_tick_max_military_tunnels",
			Parent = "ai",
			FullName = "ai.npc_spawn_per_tick_max_military_tunnels",
			ServerAdmin = true,
			Description = "npc_spawn_per_tick_max_military_tunnels defines how many can maximum spawn at once at military tunnels. (default: 1)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_spawn_per_tick_max_military_tunnels.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_spawn_per_tick_max_military_tunnels = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_spawn_per_tick_min_military_tunnels",
			Parent = "ai",
			FullName = "ai.npc_spawn_per_tick_min_military_tunnels",
			ServerAdmin = true,
			Description = "npc_spawn_per_tick_min_military_tunnels defineshow many will minimum spawn at once at military tunnels. (default: 1)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_spawn_per_tick_min_military_tunnels.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_spawn_per_tick_min_military_tunnels = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_speed_crouch_run",
			Parent = "ai",
			FullName = "ai.npc_speed_crouch_run",
			ServerAdmin = true,
			Description = "npc_speed_crouch_run define the speed of an npc when in the crouched run state, and should be a number between 0 and 1. (Default: 0.25)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_speed_crouch_run.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_speed_crouch_run = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_speed_crouch_walk",
			Parent = "ai",
			FullName = "ai.npc_speed_crouch_walk",
			ServerAdmin = true,
			Description = "npc_speed_walk define the speed of an npc when in the crouched walk state, and should be a number between 0 and 1. (Default: 0.1)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_speed_crouch_walk.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_speed_crouch_walk = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_speed_run",
			Parent = "ai",
			FullName = "ai.npc_speed_run",
			ServerAdmin = true,
			Description = "npc_speed_walk define the speed of an npc when in the run state, and should be a number between 0 and 1. (Default: 0.4)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_speed_run.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_speed_run = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_speed_sprint",
			Parent = "ai",
			FullName = "ai.npc_speed_sprint",
			ServerAdmin = true,
			Description = "npc_speed_walk define the speed of an npc when in the sprint state, and should be a number between 0 and 1. (Default: 1.0)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_speed_sprint.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_speed_sprint = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_speed_walk",
			Parent = "ai",
			FullName = "ai.npc_speed_walk",
			ServerAdmin = true,
			Description = "npc_speed_walk define the speed of an npc when in the walk state, and should be a number between 0 and 1. (Default: 0.18)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_speed_walk.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_speed_walk = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npc_valid_aim_cone",
			Parent = "ai",
			FullName = "ai.npc_valid_aim_cone",
			ServerAdmin = true,
			Description = "npc_valid_aim_cone defines how close their aim needs to be on target in order to fire. (default: 5)",
			Variable = true,
			GetOveride = (() => ConVar.AI.npc_valid_aim_cone.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.npc_valid_aim_cone = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "npcsenseloadbalancerupdateinterval",
			Parent = "ai",
			FullName = "ai.npcsenseloadbalancerupdateinterval",
			ServerAdmin = true,
			Description = "Set the update interval for npc senses that updates the knowledge gathering of npcs. (Default: 0.2)",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.AI.NpcSenseLoadBalancerUpdateInterval(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sensetime",
			Parent = "ai",
			FullName = "ai.sensetime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AI.sensetime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.sensetime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "think",
			Parent = "ai",
			FullName = "ai.think",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AI.think.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.think = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "tickrate",
			Parent = "ai",
			FullName = "ai.tickrate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AI.tickrate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AI.tickrate = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "admincheat",
			Parent = "antihack",
			FullName = "antihack.admincheat",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.admincheat.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.admincheat = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "debuglevel",
			Parent = "antihack",
			FullName = "antihack.debuglevel",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.debuglevel.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.debuglevel = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "enforcementlevel",
			Parent = "antihack",
			FullName = "antihack.enforcementlevel",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.enforcementlevel.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.enforcementlevel = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "eye_clientframes",
			Parent = "antihack",
			FullName = "antihack.eye_clientframes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.eye_clientframes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.eye_clientframes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "eye_forgiveness",
			Parent = "antihack",
			FullName = "antihack.eye_forgiveness",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.eye_forgiveness.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.eye_forgiveness = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "eye_penalty",
			Parent = "antihack",
			FullName = "antihack.eye_penalty",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.eye_penalty.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.eye_penalty = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "eye_protection",
			Parent = "antihack",
			FullName = "antihack.eye_protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.eye_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.eye_protection = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "eye_serverframes",
			Parent = "antihack",
			FullName = "antihack.eye_serverframes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.eye_serverframes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.eye_serverframes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_extrusion",
			Parent = "antihack",
			FullName = "antihack.flyhack_extrusion",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_extrusion.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_extrusion = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_forgiveness_horizontal",
			Parent = "antihack",
			FullName = "antihack.flyhack_forgiveness_horizontal",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_forgiveness_horizontal.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_forgiveness_horizontal = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_forgiveness_vertical",
			Parent = "antihack",
			FullName = "antihack.flyhack_forgiveness_vertical",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_forgiveness_vertical.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_forgiveness_vertical = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_margin",
			Parent = "antihack",
			FullName = "antihack.flyhack_margin",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_margin.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_margin = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_maxsteps",
			Parent = "antihack",
			FullName = "antihack.flyhack_maxsteps",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_maxsteps.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_maxsteps = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_penalty",
			Parent = "antihack",
			FullName = "antihack.flyhack_penalty",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_penalty.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_penalty = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_protection",
			Parent = "antihack",
			FullName = "antihack.flyhack_protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_protection = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_reject",
			Parent = "antihack",
			FullName = "antihack.flyhack_reject",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_reject.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_reject = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flyhack_stepsize",
			Parent = "antihack",
			FullName = "antihack.flyhack_stepsize",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.flyhack_stepsize.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.flyhack_stepsize = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxdeltatime",
			Parent = "antihack",
			FullName = "antihack.maxdeltatime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.maxdeltatime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.maxdeltatime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxdesync",
			Parent = "antihack",
			FullName = "antihack.maxdesync",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.maxdesync.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.maxdesync = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxviolation",
			Parent = "antihack",
			FullName = "antihack.maxviolation",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.maxviolation.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.maxviolation = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "melee_clientframes",
			Parent = "antihack",
			FullName = "antihack.melee_clientframes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.melee_clientframes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.melee_clientframes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "melee_forgiveness",
			Parent = "antihack",
			FullName = "antihack.melee_forgiveness",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.melee_forgiveness.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.melee_forgiveness = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "melee_penalty",
			Parent = "antihack",
			FullName = "antihack.melee_penalty",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.melee_penalty.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.melee_penalty = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "melee_protection",
			Parent = "antihack",
			FullName = "antihack.melee_protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.melee_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.melee_protection = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "melee_serverframes",
			Parent = "antihack",
			FullName = "antihack.melee_serverframes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.melee_serverframes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.melee_serverframes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "modelstate",
			Parent = "antihack",
			FullName = "antihack.modelstate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.modelstate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.modelstate = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_backtracking",
			Parent = "antihack",
			FullName = "antihack.noclip_backtracking",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_backtracking.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_backtracking = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_margin",
			Parent = "antihack",
			FullName = "antihack.noclip_margin",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_margin.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_margin = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_maxsteps",
			Parent = "antihack",
			FullName = "antihack.noclip_maxsteps",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_maxsteps.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_maxsteps = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_penalty",
			Parent = "antihack",
			FullName = "antihack.noclip_penalty",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_penalty.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_penalty = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_protection",
			Parent = "antihack",
			FullName = "antihack.noclip_protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_protection = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_reject",
			Parent = "antihack",
			FullName = "antihack.noclip_reject",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_reject.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_reject = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "noclip_stepsize",
			Parent = "antihack",
			FullName = "antihack.noclip_stepsize",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.noclip_stepsize.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.noclip_stepsize = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "objectplacement",
			Parent = "antihack",
			FullName = "antihack.objectplacement",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.objectplacement.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.objectplacement = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "projectile_clientframes",
			Parent = "antihack",
			FullName = "antihack.projectile_clientframes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.projectile_clientframes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.projectile_clientframes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "projectile_forgiveness",
			Parent = "antihack",
			FullName = "antihack.projectile_forgiveness",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.projectile_forgiveness.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.projectile_forgiveness = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "projectile_penalty",
			Parent = "antihack",
			FullName = "antihack.projectile_penalty",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.projectile_penalty.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.projectile_penalty = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "projectile_protection",
			Parent = "antihack",
			FullName = "antihack.projectile_protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.projectile_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.projectile_protection = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "projectile_serverframes",
			Parent = "antihack",
			FullName = "antihack.projectile_serverframes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.projectile_serverframes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.projectile_serverframes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "relaxationpause",
			Parent = "antihack",
			FullName = "antihack.relaxationpause",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.relaxationpause.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.relaxationpause = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "relaxationrate",
			Parent = "antihack",
			FullName = "antihack.relaxationrate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.relaxationrate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.relaxationrate = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "reporting",
			Parent = "antihack",
			FullName = "antihack.reporting",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.reporting.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.reporting = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_deltatime",
			Parent = "antihack",
			FullName = "antihack.speedhack_deltatime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_deltatime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_deltatime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_forgiveness",
			Parent = "antihack",
			FullName = "antihack.speedhack_forgiveness",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_forgiveness.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_forgiveness = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_history",
			Parent = "antihack",
			FullName = "antihack.speedhack_history",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_history.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_history = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_penalty",
			Parent = "antihack",
			FullName = "antihack.speedhack_penalty",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_penalty.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_penalty = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_protection",
			Parent = "antihack",
			FullName = "antihack.speedhack_protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_protection = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_reject",
			Parent = "antihack",
			FullName = "antihack.speedhack_reject",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_reject.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_reject = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_slopespeed",
			Parent = "antihack",
			FullName = "antihack.speedhack_slopespeed",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_slopespeed.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_slopespeed = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "speedhack_tickets",
			Parent = "antihack",
			FullName = "antihack.speedhack_tickets",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.speedhack_tickets.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.speedhack_tickets = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "userlevel",
			Parent = "antihack",
			FullName = "antihack.userlevel",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.AntiHack.userlevel.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.AntiHack.userlevel = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collider_capacity",
			Parent = "batching",
			FullName = "batching.collider_capacity",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Batching.collider_capacity.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Batching.collider_capacity = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collider_submeshes",
			Parent = "batching",
			FullName = "batching.collider_submeshes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Batching.collider_submeshes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Batching.collider_submeshes = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collider_threading",
			Parent = "batching",
			FullName = "batching.collider_threading",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Batching.collider_threading.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Batching.collider_threading = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collider_vertices",
			Parent = "batching",
			FullName = "batching.collider_vertices",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Batching.collider_vertices.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Batching.collider_vertices = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "colliders",
			Parent = "batching",
			FullName = "batching.colliders",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Batching.colliders.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Batching.colliders = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "print_colliders",
			Parent = "batching",
			FullName = "batching.print_colliders",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Batching.print_colliders(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "refresh_colliders",
			Parent = "batching",
			FullName = "batching.refresh_colliders",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Batching.refresh_colliders(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "verbose",
			Parent = "batching",
			FullName = "batching.verbose",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Batching.verbose.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Batching.verbose = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "enabled",
			Parent = "bradley",
			FullName = "bradley.enabled",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Bradley.enabled.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Bradley.enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "quickrespawn",
			Parent = "bradley",
			FullName = "bradley.quickrespawn",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Bradley.quickrespawn(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawndelayminutes",
			Parent = "bradley",
			FullName = "bradley.respawndelayminutes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Bradley.respawnDelayMinutes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Bradley.respawnDelayMinutes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawndelayvariance",
			Parent = "bradley",
			FullName = "bradley.respawndelayvariance",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Bradley.respawnDelayVariance.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Bradley.respawnDelayVariance = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "enabled",
			Parent = "chat",
			FullName = "chat.enabled",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Chat.enabled.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Chat.enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "say",
			Parent = "chat",
			FullName = "chat.say",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Chat.say(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "search",
			Parent = "chat",
			FullName = "chat.search",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				IEnumerable<ConVar.Chat.ChatEntry> enumerable = ConVar.Chat.search(arg);
				arg.ReplyWithObject(enumerable);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "serverlog",
			Parent = "chat",
			FullName = "chat.serverlog",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Chat.serverlog.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Chat.serverlog = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "tail",
			Parent = "chat",
			FullName = "chat.tail",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				IEnumerable<ConVar.Chat.ChatEntry> enumerable = ConVar.Chat.tail(arg);
				arg.ReplyWithObject(enumerable);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "search",
			Parent = "console",
			FullName = "console.search",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				IEnumerable<Facepunch.Output.Entry> enumerable = ConVar.Console.search(arg);
				arg.ReplyWithObject(enumerable);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "tail",
			Parent = "console",
			FullName = "console.tail",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				IEnumerable<Facepunch.Output.Entry> enumerable = ConVar.Console.tail(arg);
				arg.ReplyWithObject(enumerable);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "frameminutes",
			Parent = "construct",
			FullName = "construct.frameminutes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Construct.frameminutes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Construct.frameminutes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "add",
			Parent = "craft",
			FullName = "craft.add",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Craft.add(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cancel",
			Parent = "craft",
			FullName = "craft.cancel",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Craft.cancel(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "canceltask",
			Parent = "craft",
			FullName = "craft.canceltask",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Craft.canceltask(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "instant",
			Parent = "craft",
			FullName = "craft.instant",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Craft.instant.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Craft.instant = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "export",
			Parent = "data",
			FullName = "data.export",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Data.export(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "breakheld",
			Parent = "debug",
			FullName = "debug.breakheld",
			ServerAdmin = true,
			Description = "Break the current held object",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.breakheld(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "breakitem",
			Parent = "debug",
			FullName = "debug.breakitem",
			ServerAdmin = true,
			Description = "Break all the items in your inventory whose name match the passed string",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.breakitem(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "checktriggers",
			Parent = "debug",
			FullName = "debug.checktriggers",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Debugging.checktriggers.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Debugging.checktriggers = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "disablecondition",
			Parent = "debug",
			FullName = "debug.disablecondition",
			ServerAdmin = true,
			Description = "Do not damage any items",
			Variable = true,
			GetOveride = (() => ConVar.Debugging.disablecondition.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Debugging.disablecondition = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "drink",
			Parent = "debug",
			FullName = "debug.drink",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.drink(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "eat",
			Parent = "debug",
			FullName = "debug.eat",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.eat(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "flushgroup",
			Parent = "debug",
			FullName = "debug.flushgroup",
			ServerAdmin = true,
			Description = "Takes you in and out of your current network group, causing you to delete and then download all entities in your PVS again",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.flushgroup(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "hurt",
			Parent = "debug",
			FullName = "debug.hurt",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.hurt(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "log",
			Parent = "debug",
			FullName = "debug.log",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Debugging.log.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Debugging.log = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "renderinfo",
			Parent = "debug",
			FullName = "debug.renderinfo",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Debugging.renderinfo(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_0_blockcount",
			Parent = "decay",
			FullName = "decay.bracket_0_blockcount",
			ServerAdmin = true,
			Description = "Between 0 and this value are considered bracket 0 and will cost bracket_0_costfraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_0_blockcount.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_0_blockcount = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_0_costfraction",
			Parent = "decay",
			FullName = "decay.bracket_0_costfraction",
			ServerAdmin = true,
			Description = "blocks within bracket 0 will cost this fraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_0_costfraction.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_0_costfraction = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_1_blockcount",
			Parent = "decay",
			FullName = "decay.bracket_1_blockcount",
			ServerAdmin = true,
			Description = "Between bracket_0_blockcount and this value are considered bracket 1 and will cost bracket_1_costfraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_1_blockcount.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_1_blockcount = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_1_costfraction",
			Parent = "decay",
			FullName = "decay.bracket_1_costfraction",
			ServerAdmin = true,
			Description = "blocks within bracket 1 will cost this fraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_1_costfraction.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_1_costfraction = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_2_blockcount",
			Parent = "decay",
			FullName = "decay.bracket_2_blockcount",
			ServerAdmin = true,
			Description = "Between bracket_1_blockcount and this value are considered bracket 2 and will cost bracket_2_costfraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_2_blockcount.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_2_blockcount = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_2_costfraction",
			Parent = "decay",
			FullName = "decay.bracket_2_costfraction",
			ServerAdmin = true,
			Description = "blocks within bracket 2 will cost this fraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_2_costfraction.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_2_costfraction = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_3_blockcount",
			Parent = "decay",
			FullName = "decay.bracket_3_blockcount",
			ServerAdmin = true,
			Description = "Between bracket_2_blockcount and this value (and beyond) are considered bracket 3 and will cost bracket_3_costfraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_3_blockcount.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_3_blockcount = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bracket_3_costfraction",
			Parent = "decay",
			FullName = "decay.bracket_3_costfraction",
			ServerAdmin = true,
			Description = "blocks within bracket 3 will cost this fraction per upkeep period to maintain",
			Variable = true,
			GetOveride = (() => ConVar.Decay.bracket_3_costfraction.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.bracket_3_costfraction = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "debug",
			Parent = "decay",
			FullName = "decay.debug",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Decay.debug.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.debug = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "delay_metal",
			Parent = "decay",
			FullName = "decay.delay_metal",
			ServerAdmin = true,
			Description = "How long should this building grade decay be delayed when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.delay_metal.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.delay_metal = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "delay_override",
			Parent = "decay",
			FullName = "decay.delay_override",
			ServerAdmin = true,
			Description = "When set to a value above 0 everything will decay with this delay",
			Variable = true,
			GetOveride = (() => ConVar.Decay.delay_override.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.delay_override = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "delay_stone",
			Parent = "decay",
			FullName = "decay.delay_stone",
			ServerAdmin = true,
			Description = "How long should this building grade decay be delayed when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.delay_stone.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.delay_stone = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "delay_toptier",
			Parent = "decay",
			FullName = "decay.delay_toptier",
			ServerAdmin = true,
			Description = "How long should this building grade decay be delayed when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.delay_toptier.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.delay_toptier = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "delay_twig",
			Parent = "decay",
			FullName = "decay.delay_twig",
			ServerAdmin = true,
			Description = "How long should this building grade decay be delayed when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.delay_twig.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.delay_twig = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "delay_wood",
			Parent = "decay",
			FullName = "decay.delay_wood",
			ServerAdmin = true,
			Description = "How long should this building grade decay be delayed when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.delay_wood.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.delay_wood = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "duration_metal",
			Parent = "decay",
			FullName = "decay.duration_metal",
			ServerAdmin = true,
			Description = "How long should this building grade take to decay when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.duration_metal.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.duration_metal = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "duration_override",
			Parent = "decay",
			FullName = "decay.duration_override",
			ServerAdmin = true,
			Description = "When set to a value above 0 everything will decay with this duration",
			Variable = true,
			GetOveride = (() => ConVar.Decay.duration_override.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.duration_override = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "duration_stone",
			Parent = "decay",
			FullName = "decay.duration_stone",
			ServerAdmin = true,
			Description = "How long should this building grade take to decay when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.duration_stone.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.duration_stone = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "duration_toptier",
			Parent = "decay",
			FullName = "decay.duration_toptier",
			ServerAdmin = true,
			Description = "How long should this building grade take to decay when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.duration_toptier.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.duration_toptier = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "duration_twig",
			Parent = "decay",
			FullName = "decay.duration_twig",
			ServerAdmin = true,
			Description = "How long should this building grade take to decay when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.duration_twig.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.duration_twig = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "duration_wood",
			Parent = "decay",
			FullName = "decay.duration_wood",
			ServerAdmin = true,
			Description = "How long should this building grade take to decay when not protected by upkeep, in hours",
			Variable = true,
			GetOveride = (() => ConVar.Decay.duration_wood.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.duration_wood = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "outside_test_range",
			Parent = "decay",
			FullName = "decay.outside_test_range",
			ServerAdmin = true,
			Description = "Maximum distance to test to see if a structure is outside, higher values are slower but accurate for huge buildings",
			Variable = true,
			GetOveride = (() => ConVar.Decay.outside_test_range.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.outside_test_range = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "scale",
			Parent = "decay",
			FullName = "decay.scale",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Decay.scale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.scale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "tick",
			Parent = "decay",
			FullName = "decay.tick",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Decay.tick.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.tick = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "upkeep",
			Parent = "decay",
			FullName = "decay.upkeep",
			ServerAdmin = true,
			Description = "Is upkeep enabled",
			Variable = true,
			GetOveride = (() => ConVar.Decay.upkeep.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.upkeep = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "upkeep_grief_protection",
			Parent = "decay",
			FullName = "decay.upkeep_grief_protection",
			ServerAdmin = true,
			Description = "How many minutes can the upkeep cost last after the cupboard was destroyed? default : 1440 (24 hours)",
			Variable = true,
			GetOveride = (() => ConVar.Decay.upkeep_grief_protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.upkeep_grief_protection = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "upkeep_heal_scale",
			Parent = "decay",
			FullName = "decay.upkeep_heal_scale",
			ServerAdmin = true,
			Description = "Scale at which objects heal when upkeep conditions are met, default of 1 is same rate at which they decay",
			Variable = true,
			GetOveride = (() => ConVar.Decay.upkeep_heal_scale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.upkeep_heal_scale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "upkeep_inside_decay_scale",
			Parent = "decay",
			FullName = "decay.upkeep_inside_decay_scale",
			ServerAdmin = true,
			Description = "Scale at which objects decay when they are inside, default of 0.1",
			Variable = true,
			GetOveride = (() => ConVar.Decay.upkeep_inside_decay_scale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.upkeep_inside_decay_scale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "upkeep_period_minutes",
			Parent = "decay",
			FullName = "decay.upkeep_period_minutes",
			ServerAdmin = true,
			Description = "How many minutes does the upkeep cost last? default : 1440 (24 hours)",
			Variable = true,
			GetOveride = (() => ConVar.Decay.upkeep_period_minutes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Decay.upkeep_period_minutes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "debug_toggle",
			Parent = "entity",
			FullName = "entity.debug_toggle",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.debug_toggle(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "deleteby",
			Parent = "entity",
			FullName = "entity.deleteby",
			ServerAdmin = true,
			Description = "Destroy all entities created by this user",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				int num = ConVar.Entity.DeleteBy(arg.GetULong(0, 0UL));
				arg.ReplyWithObject(num);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find_entity",
			Parent = "entity",
			FullName = "entity.find_entity",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.find_entity(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find_group",
			Parent = "entity",
			FullName = "entity.find_group",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.find_group(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find_id",
			Parent = "entity",
			FullName = "entity.find_id",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.find_id(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find_parent",
			Parent = "entity",
			FullName = "entity.find_parent",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.find_parent(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find_radius",
			Parent = "entity",
			FullName = "entity.find_radius",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.find_radius(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "find_self",
			Parent = "entity",
			FullName = "entity.find_self",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.find_self(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nudge",
			Parent = "entity",
			FullName = "entity.nudge",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.nudge(arg.GetInt(0, 0));
			}
		},
		new ConsoleSystem.Command
		{
			Name = "spawnlootfrom",
			Parent = "entity",
			FullName = "entity.spawnlootfrom",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Entity.spawnlootfrom(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "spawn",
			Parent = "entity",
			FullName = "entity.spawn",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Entity.svspawn(arg.GetString(0, string.Empty), arg.GetVector3(1, Vector3.zero));
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "spawnitem",
			Parent = "entity",
			FullName = "entity.spawnitem",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Entity.svspawnitem(arg.GetString(0, string.Empty), arg.GetVector3(1, Vector3.zero));
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "addtime",
			Parent = "env",
			FullName = "env.addtime",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Env.addtime(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "day",
			Parent = "env",
			FullName = "env.day",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Env.day.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Env.day = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "month",
			Parent = "env",
			FullName = "env.month",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Env.month.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Env.month = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "progresstime",
			Parent = "env",
			FullName = "env.progresstime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Env.progresstime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Env.progresstime = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "time",
			Parent = "env",
			FullName = "env.time",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Env.time.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Env.time = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "year",
			Parent = "env",
			FullName = "env.year",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Env.year.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Env.year = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "limit",
			Parent = "fps",
			FullName = "fps.limit",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.FPS.limit.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.FPS.limit = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collect",
			Parent = "gc",
			FullName = "gc.collect",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.GC.collect();
			}
		},
		new ConsoleSystem.Command
		{
			Name = "unload",
			Parent = "gc",
			FullName = "gc.unload",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.GC.unload();
			}
		},
		new ConsoleSystem.Command
		{
			Name = "breakitem",
			Parent = "global",
			FullName = "global.breakitem",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.breakitem(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "colliders",
			Parent = "global",
			FullName = "global.colliders",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.colliders(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "developer",
			Parent = "global",
			FullName = "global.developer",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Global.developer.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Global.developer = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "error",
			Parent = "global",
			FullName = "global.error",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.error(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "free",
			Parent = "global",
			FullName = "global.free",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.free(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "injure",
			Parent = "global",
			FullName = "global.injure",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.injure(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "kill",
			Parent = "global",
			FullName = "global.kill",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.kill(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxthreads",
			Parent = "global",
			FullName = "global.maxthreads",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Global.maxthreads.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Global.maxthreads = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "objects",
			Parent = "global",
			FullName = "global.objects",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.objects(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "perf",
			Parent = "global",
			FullName = "global.perf",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Global.perf.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Global.perf = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "queue",
			Parent = "global",
			FullName = "global.queue",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.queue(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "quit",
			Parent = "global",
			FullName = "global.quit",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.quit(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "report",
			Parent = "global",
			FullName = "global.report",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.report(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawn",
			Parent = "global",
			FullName = "global.respawn",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.respawn(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawn_sleepingbag",
			Parent = "global",
			FullName = "global.respawn_sleepingbag",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.respawn_sleepingbag(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawn_sleepingbag_remove",
			Parent = "global",
			FullName = "global.respawn_sleepingbag_remove",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.respawn_sleepingbag_remove(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "restart",
			Parent = "global",
			FullName = "global.restart",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.restart(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "setinfo",
			Parent = "global",
			FullName = "global.setinfo",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.setinfo(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sleep",
			Parent = "global",
			FullName = "global.sleep",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.sleep(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "spectate",
			Parent = "global",
			FullName = "global.spectate",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.spectate(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "status_sv",
			Parent = "global",
			FullName = "global.status_sv",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.status_sv(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "subscriptions",
			Parent = "global",
			FullName = "global.subscriptions",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.subscriptions(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sysinfo",
			Parent = "global",
			FullName = "global.sysinfo",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.sysinfo(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sysuid",
			Parent = "global",
			FullName = "global.sysuid",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.sysuid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "teleport",
			Parent = "global",
			FullName = "global.teleport",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.teleport(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "teleport2me",
			Parent = "global",
			FullName = "global.teleport2me",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.teleport2me(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "teleportany",
			Parent = "global",
			FullName = "global.teleportany",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.teleportany(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "teleportpos",
			Parent = "global",
			FullName = "global.teleportpos",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.teleportpos(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "textures",
			Parent = "global",
			FullName = "global.textures",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.textures(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "timewarning",
			Parent = "global",
			FullName = "global.timewarning",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Global.timewarning.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Global.timewarning = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "version",
			Parent = "global",
			FullName = "global.version",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Global.version(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "enabled",
			Parent = "halloween",
			FullName = "halloween.enabled",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Halloween.enabled.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Halloween.enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "murdererpopulation",
			Parent = "halloween",
			FullName = "halloween.murdererpopulation",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => ConVar.Halloween.murdererpopulation.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Halloween.murdererpopulation = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cd",
			Parent = "hierarchy",
			FullName = "hierarchy.cd",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Hierarchy.cd(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "del",
			Parent = "hierarchy",
			FullName = "hierarchy.del",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Hierarchy.del(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ls",
			Parent = "hierarchy",
			FullName = "hierarchy.ls",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Hierarchy.ls(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "endloot",
			Parent = "inventory",
			FullName = "inventory.endloot",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.endloot(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "give",
			Parent = "inventory",
			FullName = "inventory.give",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.give(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "giveall",
			Parent = "inventory",
			FullName = "inventory.giveall",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.giveall(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "givearm",
			Parent = "inventory",
			FullName = "inventory.givearm",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.givearm(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "giveid",
			Parent = "inventory",
			FullName = "inventory.giveid",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.giveid(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "giveto",
			Parent = "inventory",
			FullName = "inventory.giveto",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.giveto(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "lighttoggle",
			Parent = "inventory",
			FullName = "inventory.lighttoggle",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.lighttoggle(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "resetbp",
			Parent = "inventory",
			FullName = "inventory.resetbp",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.resetbp(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "unlockall",
			Parent = "inventory",
			FullName = "inventory.unlockall",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Inventory.unlockall(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "printmanifest",
			Parent = "manifest",
			FullName = "manifest.printmanifest",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				object obj = ConVar.Manifest.PrintManifest();
				arg.ReplyWithObject(obj);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "printmanifestraw",
			Parent = "manifest",
			FullName = "manifest.printmanifestraw",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				object obj = ConVar.Manifest.PrintManifestRaw();
				arg.ReplyWithObject(obj);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "visdebug",
			Parent = "net",
			FullName = "net.visdebug",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Net.visdebug.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Net.visdebug = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bulletaccuracy",
			Parent = "heli",
			FullName = "heli.bulletaccuracy",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.PatrolHelicopter.bulletAccuracy.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.PatrolHelicopter.bulletAccuracy = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bulletdamagescale",
			Parent = "heli",
			FullName = "heli.bulletdamagescale",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.PatrolHelicopter.bulletDamageScale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.PatrolHelicopter.bulletDamageScale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "call",
			Parent = "heli",
			FullName = "heli.call",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.PatrolHelicopter.call(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "calltome",
			Parent = "heli",
			FullName = "heli.calltome",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.PatrolHelicopter.calltome(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "drop",
			Parent = "heli",
			FullName = "heli.drop",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.PatrolHelicopter.drop(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "guns",
			Parent = "heli",
			FullName = "heli.guns",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.PatrolHelicopter.guns.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.PatrolHelicopter.guns = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "lifetimeminutes",
			Parent = "heli",
			FullName = "heli.lifetimeminutes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.PatrolHelicopter.lifetimeMinutes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.PatrolHelicopter.lifetimeMinutes = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "strafe",
			Parent = "heli",
			FullName = "heli.strafe",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.PatrolHelicopter.strafe(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bouncethreshold",
			Parent = "physics",
			FullName = "physics.bouncethreshold",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Physics.bouncethreshold.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.bouncethreshold = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "droppedmode",
			Parent = "physics",
			FullName = "physics.droppedmode",
			ServerAdmin = true,
			Description = "The physics mode that dropped items and corpses should use. good, tempgood or fast. fast + tempgood might cause objects to fall through other objects.",
			Variable = true,
			GetOveride = (() => ConVar.Physics.droppedmode.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.droppedmode = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "gravity",
			Parent = "physics",
			FullName = "physics.gravity",
			ServerAdmin = true,
			Description = "Gravity multiplier",
			Variable = true,
			GetOveride = (() => ConVar.Physics.gravity.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.gravity = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "minsteps",
			Parent = "physics",
			FullName = "physics.minsteps",
			ServerAdmin = true,
			Description = "The slowest physics steps will operate",
			Variable = true,
			GetOveride = (() => ConVar.Physics.minsteps.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.minsteps = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sendeffects",
			Parent = "physics",
			FullName = "physics.sendeffects",
			ServerAdmin = true,
			Description = "Send effects to clients when physics objects collide",
			Variable = true,
			GetOveride = (() => ConVar.Physics.sendeffects.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.sendeffects = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sleepthreshold",
			Parent = "physics",
			FullName = "physics.sleepthreshold",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Physics.sleepthreshold.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.sleepthreshold = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "solveriterationcount",
			Parent = "physics",
			FullName = "physics.solveriterationcount",
			ServerAdmin = true,
			Description = "The default solver iteration count permitted for any rigid bodies (default 7). Must be positive",
			Variable = true,
			GetOveride = (() => ConVar.Physics.solveriterationcount.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.solveriterationcount = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "steps",
			Parent = "physics",
			FullName = "physics.steps",
			ServerAdmin = true,
			Description = "The amount of physics steps per second",
			Variable = true,
			GetOveride = (() => ConVar.Physics.steps.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Physics.steps = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "clear_assets",
			Parent = "pool",
			FullName = "pool.clear_assets",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Pool.clear_assets(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "clear_memory",
			Parent = "pool",
			FullName = "pool.clear_memory",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Pool.clear_memory(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "clear_prefabs",
			Parent = "pool",
			FullName = "pool.clear_prefabs",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Pool.clear_prefabs(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collider_batches",
			Parent = "pool",
			FullName = "pool.collider_batches",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Pool.collider_batches.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Pool.collider_batches = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "mode",
			Parent = "pool",
			FullName = "pool.mode",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Pool.mode.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Pool.mode = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "print_assets",
			Parent = "pool",
			FullName = "pool.print_assets",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Pool.print_assets(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "print_memory",
			Parent = "pool",
			FullName = "pool.print_memory",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Pool.print_memory(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "print_prefabs",
			Parent = "pool",
			FullName = "pool.print_prefabs",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Pool.print_prefabs(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "start",
			Parent = "profile",
			FullName = "profile.start",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Profile.start(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "stop",
			Parent = "profile",
			FullName = "profile.stop",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Profile.stop(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "hostileduration",
			Parent = "sentry",
			FullName = "sentry.hostileduration",
			ServerAdmin = true,
			Description = "how long until something is considered hostile after it attacked",
			Variable = true,
			GetOveride = (() => ConVar.Sentry.hostileduration.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Sentry.hostileduration = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "targetall",
			Parent = "sentry",
			FullName = "sentry.targetall",
			ServerAdmin = true,
			Description = "target everyone regardless of authorization",
			Variable = true,
			GetOveride = (() => ConVar.Sentry.targetall.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Sentry.targetall = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "arrowarmor",
			Parent = "server",
			FullName = "server.arrowarmor",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.arrowarmor.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.arrowarmor = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "arrowdamage",
			Parent = "server",
			FullName = "server.arrowdamage",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.arrowdamage.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.arrowdamage = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "authtimeout",
			Parent = "server",
			FullName = "server.authtimeout",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.authtimeout.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.authtimeout = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "backup",
			Parent = "server",
			FullName = "server.backup",
			ServerAdmin = true,
			Description = "Backup server folder",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.backup();
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bleedingarmor",
			Parent = "server",
			FullName = "server.bleedingarmor",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.bleedingarmor.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.bleedingarmor = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bleedingdamage",
			Parent = "server",
			FullName = "server.bleedingdamage",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.bleedingdamage.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.bleedingdamage = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "branch",
			Parent = "server",
			FullName = "server.branch",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.branch.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.branch = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bulletarmor",
			Parent = "server",
			FullName = "server.bulletarmor",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.bulletarmor.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.bulletarmor = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "bulletdamage",
			Parent = "server",
			FullName = "server.bulletdamage",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.bulletdamage.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.bulletdamage = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cheatreport",
			Parent = "server",
			FullName = "server.cheatreport",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.cheatreport(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "combatlog",
			Parent = "server",
			FullName = "server.combatlog",
			ServerUser = true,
			Description = "Get the player combat log",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Server.combatlog(arg);
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "combatlogsize",
			Parent = "server",
			FullName = "server.combatlogsize",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.combatlogsize.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.combatlogsize = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "compression",
			Parent = "server",
			FullName = "server.compression",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.compression.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.compression = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "corpsedespawn",
			Parent = "server",
			FullName = "server.corpsedespawn",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.corpsedespawn.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.corpsedespawn = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "corpses",
			Parent = "server",
			FullName = "server.corpses",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.corpses.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.corpses = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cycletime",
			Parent = "server",
			FullName = "server.cycletime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.cycletime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.cycletime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "description",
			Parent = "server",
			FullName = "server.description",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.description.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.description = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "dropitems",
			Parent = "server",
			FullName = "server.dropitems",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.dropitems.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.dropitems = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "encryption",
			Parent = "server",
			FullName = "server.encryption",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.encryption.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.encryption = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "entityrate",
			Parent = "server",
			FullName = "server.entityrate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.entityrate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.entityrate = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "events",
			Parent = "server",
			FullName = "server.events",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.events.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.events = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "fps",
			Parent = "server",
			FullName = "server.fps",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.fps(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "globalchat",
			Parent = "server",
			FullName = "server.globalchat",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.globalchat.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.globalchat = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "headerimage",
			Parent = "server",
			FullName = "server.headerimage",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.headerimage.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.headerimage = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "hostname",
			Parent = "server",
			FullName = "server.hostname",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.hostname.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.hostname = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "identity",
			Parent = "server",
			FullName = "server.identity",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.identity.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.identity = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "idlekick",
			Parent = "server",
			FullName = "server.idlekick",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.idlekick.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.idlekick = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "idlekickadmins",
			Parent = "server",
			FullName = "server.idlekickadmins",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.idlekickadmins.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.idlekickadmins = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "idlekickmode",
			Parent = "server",
			FullName = "server.idlekickmode",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.idlekickmode.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.idlekickmode = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ip",
			Parent = "server",
			FullName = "server.ip",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.ip.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.ip = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ipqueriespermin",
			Parent = "server",
			FullName = "server.ipqueriespermin",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.ipQueriesPerMin.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.ipQueriesPerMin = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "itemdespawn",
			Parent = "server",
			FullName = "server.itemdespawn",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.itemdespawn.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.itemdespawn = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "level",
			Parent = "server",
			FullName = "server.level",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.level.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.level = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxcommandspersecond",
			Parent = "server",
			FullName = "server.maxcommandspersecond",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxcommandspersecond.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxcommandspersecond = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxpacketspersecond",
			Parent = "server",
			FullName = "server.maxpacketspersecond",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxpacketspersecond.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxpacketspersecond = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxplayers",
			Parent = "server",
			FullName = "server.maxplayers",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxplayers.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxplayers = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxreceivetime",
			Parent = "server",
			FullName = "server.maxreceivetime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxreceivetime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxreceivetime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxrpcspersecond",
			Parent = "server",
			FullName = "server.maxrpcspersecond",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxrpcspersecond.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxrpcspersecond = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxtickspersecond",
			Parent = "server",
			FullName = "server.maxtickspersecond",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxtickspersecond.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxtickspersecond = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxunack",
			Parent = "server",
			FullName = "server.maxunack",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.maxunack.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.maxunack = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "meleearmor",
			Parent = "server",
			FullName = "server.meleearmor",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.meleearmor.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.meleearmor = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "meleedamage",
			Parent = "server",
			FullName = "server.meleedamage",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.meleedamage.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.meleedamage = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "metabolismtick",
			Parent = "server",
			FullName = "server.metabolismtick",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.metabolismtick.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.metabolismtick = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "netcache",
			Parent = "server",
			FullName = "server.netcache",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.netcache.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.netcache = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "netcachesize",
			Parent = "server",
			FullName = "server.netcachesize",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.netcachesize.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.netcachesize = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "netlog",
			Parent = "server",
			FullName = "server.netlog",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.netlog.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.netlog = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "official",
			Parent = "server",
			FullName = "server.official",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.official.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.official = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "plantlightdetection",
			Parent = "server",
			FullName = "server.plantlightdetection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.plantlightdetection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.plantlightdetection = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "planttick",
			Parent = "server",
			FullName = "server.planttick",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.planttick.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.planttick = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "planttickscale",
			Parent = "server",
			FullName = "server.planttickscale",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.planttickscale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.planttickscale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "playertimeout",
			Parent = "server",
			FullName = "server.playertimeout",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.playertimeout.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.playertimeout = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "port",
			Parent = "server",
			FullName = "server.port",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.port.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.port = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "printeyes",
			Parent = "server",
			FullName = "server.printeyes",
			ServerAdmin = true,
			Description = "Print the current player eyes.",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Server.printeyes(arg);
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "printpos",
			Parent = "server",
			FullName = "server.printpos",
			ServerAdmin = true,
			Description = "Print the current player position.",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Server.printpos(arg);
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "printrot",
			Parent = "server",
			FullName = "server.printrot",
			ServerAdmin = true,
			Description = "Print the current player rotation.",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Server.printrot(arg);
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "pve",
			Parent = "server",
			FullName = "server.pve",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.pve.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.pve = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "queriespersecond",
			Parent = "server",
			FullName = "server.queriespersecond",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.queriesPerSecond.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.queriesPerSecond = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "queryport",
			Parent = "server",
			FullName = "server.queryport",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.queryport.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.queryport = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "radiation",
			Parent = "server",
			FullName = "server.radiation",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.radiation.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.radiation = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "readcfg",
			Parent = "server",
			FullName = "server.readcfg",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				string text = ConVar.Server.readcfg(arg);
				arg.ReplyWithObject(text);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawnresetrange",
			Parent = "server",
			FullName = "server.respawnresetrange",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.respawnresetrange.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.respawnresetrange = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "salt",
			Parent = "server",
			FullName = "server.salt",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.salt.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.salt = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "save",
			Parent = "server",
			FullName = "server.save",
			ServerAdmin = true,
			Description = "Force save the current game",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.save(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "savecachesize",
			Parent = "server",
			FullName = "server.savecachesize",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.savecachesize.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.savecachesize = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "saveinterval",
			Parent = "server",
			FullName = "server.saveinterval",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.saveinterval.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.saveinterval = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "schematime",
			Parent = "server",
			FullName = "server.schematime",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.schematime.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.schematime = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "secure",
			Parent = "server",
			FullName = "server.secure",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.secure.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.secure = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "seed",
			Parent = "server",
			FullName = "server.seed",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.seed.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.seed = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sendnetworkupdate",
			Parent = "server",
			FullName = "server.sendnetworkupdate",
			ServerAdmin = true,
			Description = "Send network update for all players",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.sendnetworkupdate(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "setshowholstereditems",
			Parent = "server",
			FullName = "server.setshowholstereditems",
			ServerAdmin = true,
			Description = "Show holstered items on player bodies",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.setshowholstereditems(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "showholstereditems",
			Parent = "server",
			FullName = "server.showholstereditems",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.showHolsteredItems.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.showHolsteredItems = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "snapshot",
			Parent = "server",
			FullName = "server.snapshot",
			ServerUser = true,
			Description = "This sends a snapshot of all the entities in the client's pvs. This is mostly redundant, but we request this when the client starts recording a demo.. so they get all the information.",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.snapshot(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "stability",
			Parent = "server",
			FullName = "server.stability",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.stability.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.stability = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "start",
			Parent = "server",
			FullName = "server.start",
			ServerAdmin = true,
			Description = "Starts a server",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.start(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "stats",
			Parent = "server",
			FullName = "server.stats",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.stats.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.stats = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "stop",
			Parent = "server",
			FullName = "server.stop",
			ServerAdmin = true,
			Description = "Stops a server",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.stop(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "tickrate",
			Parent = "server",
			FullName = "server.tickrate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.tickrate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.tickrate = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "updatebatch",
			Parent = "server",
			FullName = "server.updatebatch",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.updatebatch.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.updatebatch = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "updatebatchspawn",
			Parent = "server",
			FullName = "server.updatebatchspawn",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.updatebatchspawn.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.updatebatchspawn = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "url",
			Parent = "server",
			FullName = "server.url",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.url.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.url = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "worldsize",
			Parent = "server",
			FullName = "server.worldsize",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.worldsize.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.worldsize = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "woundingenabled",
			Parent = "server",
			FullName = "server.woundingenabled",
			ServerAdmin = true,
			Saved = true,
			Variable = true,
			GetOveride = (() => ConVar.Server.woundingenabled.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Server.woundingenabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "writecfg",
			Parent = "server",
			FullName = "server.writecfg",
			ServerAdmin = true,
			Description = "Writes config files",
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Server.writecfg(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "fill_groups",
			Parent = "spawn",
			FullName = "spawn.fill_groups",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Spawn.fill_groups(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "fill_populations",
			Parent = "spawn",
			FullName = "spawn.fill_populations",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Spawn.fill_populations(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "max_density",
			Parent = "spawn",
			FullName = "spawn.max_density",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.max_density.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.max_density = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "max_rate",
			Parent = "spawn",
			FullName = "spawn.max_rate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.max_rate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.max_rate = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "min_density",
			Parent = "spawn",
			FullName = "spawn.min_density",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.min_density.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.min_density = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "min_rate",
			Parent = "spawn",
			FullName = "spawn.min_rate",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.min_rate.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.min_rate = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "player_base",
			Parent = "spawn",
			FullName = "spawn.player_base",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.player_base.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.player_base = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "player_scale",
			Parent = "spawn",
			FullName = "spawn.player_scale",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.player_scale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.player_scale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "report",
			Parent = "spawn",
			FullName = "spawn.report",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Spawn.report(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawn_groups",
			Parent = "spawn",
			FullName = "spawn.respawn_groups",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.respawn_groups.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.respawn_groups = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "respawn_populations",
			Parent = "spawn",
			FullName = "spawn.respawn_populations",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Spawn.respawn_populations.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Spawn.respawn_populations = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "scalars",
			Parent = "spawn",
			FullName = "spawn.scalars",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Spawn.scalars(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "accuracy",
			Parent = "stability",
			FullName = "stability.accuracy",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Stability.accuracy.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Stability.accuracy = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "collapse",
			Parent = "stability",
			FullName = "stability.collapse",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Stability.collapse.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Stability.collapse = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "refresh_stability",
			Parent = "stability",
			FullName = "stability.refresh_stability",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Stability.refresh_stability(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "stabilityqueue",
			Parent = "stability",
			FullName = "stability.stabilityqueue",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Stability.stabilityqueue.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Stability.stabilityqueue = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "strikes",
			Parent = "stability",
			FullName = "stability.strikes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Stability.strikes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Stability.strikes = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "surroundingsqueue",
			Parent = "stability",
			FullName = "stability.surroundingsqueue",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Stability.surroundingsqueue.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Stability.surroundingsqueue = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "verbose",
			Parent = "stability",
			FullName = "stability.verbose",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Stability.verbose.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Stability.verbose = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "call",
			Parent = "supply",
			FullName = "supply.call",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Supply.call(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "drop",
			Parent = "supply",
			FullName = "supply.drop",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Supply.drop(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "fixeddelta",
			Parent = "time",
			FullName = "time.fixeddelta",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Time.fixeddelta.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Time.fixeddelta = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "maxdelta",
			Parent = "time",
			FullName = "time.maxdelta",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Time.maxdelta.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Time.maxdelta = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "pausewhileloading",
			Parent = "time",
			FullName = "time.pausewhileloading",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Time.pausewhileloading.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Time.pausewhileloading = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "timescale",
			Parent = "time",
			FullName = "time.timescale",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Time.timescale.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Time.timescale = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "attack",
			Parent = "vis",
			FullName = "vis.attack",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.attack.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.attack = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "damage",
			Parent = "vis",
			FullName = "vis.damage",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.damage.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.damage = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "hitboxes",
			Parent = "vis",
			FullName = "vis.hitboxes",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.hitboxes.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.hitboxes = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "lineofsight",
			Parent = "vis",
			FullName = "vis.lineofsight",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.lineofsight.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.lineofsight = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "protection",
			Parent = "vis",
			FullName = "vis.protection",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.protection.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.protection = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "sense",
			Parent = "vis",
			FullName = "vis.sense",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.sense.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.sense = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "triggers",
			Parent = "vis",
			FullName = "vis.triggers",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.triggers.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.triggers = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "weakspots",
			Parent = "vis",
			FullName = "vis.weakspots",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.Vis.weakspots.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.Vis.weakspots = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "clouds",
			Parent = "weather",
			FullName = "weather.clouds",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Weather.clouds(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "fog",
			Parent = "weather",
			FullName = "weather.fog",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Weather.fog(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "rain",
			Parent = "weather",
			FullName = "weather.rain",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Weather.rain(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "wind",
			Parent = "weather",
			FullName = "weather.wind",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Weather.wind(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "print_approved_skins",
			Parent = "workshop",
			FullName = "workshop.print_approved_skins",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.Workshop.print_approved_skins(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cache",
			Parent = "world",
			FullName = "world.cache",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.World.cache.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.World.cache = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "enabled",
			Parent = "xmas",
			FullName = "xmas.enabled",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.XMas.enabled.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.XMas.enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "giftsperplayer",
			Parent = "xmas",
			FullName = "xmas.giftsperplayer",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.XMas.giftsPerPlayer.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.XMas.giftsPerPlayer = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "refill",
			Parent = "xmas",
			FullName = "xmas.refill",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				ConVar.XMas.refill(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "spawnattempts",
			Parent = "xmas",
			FullName = "xmas.spawnattempts",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.XMas.spawnAttempts.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.XMas.spawnAttempts = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "spawnrange",
			Parent = "xmas",
			FullName = "xmas.spawnrange",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => ConVar.XMas.spawnRange.ToString()),
			SetOveride = delegate(string str)
			{
				ConVar.XMas.spawnRange = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "endtest",
			Parent = "cui",
			FullName = "cui.endtest",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				global::cui.endtest(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "test",
			Parent = "cui",
			FullName = "cui.test",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				global::cui.test(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "dump",
			Parent = "global",
			FullName = "global.dump",
			ServerAdmin = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				global::DiagnosticsConSys.dump(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ip",
			Parent = "rcon",
			FullName = "rcon.ip",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => Facepunch.RCon.Ip.ToString()),
			SetOveride = delegate(string str)
			{
				Facepunch.RCon.Ip = str;
			}
		},
		new ConsoleSystem.Command
		{
			Name = "port",
			Parent = "rcon",
			FullName = "rcon.port",
			ServerAdmin = true,
			Variable = true,
			GetOveride = (() => Facepunch.RCon.Port.ToString()),
			SetOveride = delegate(string str)
			{
				Facepunch.RCon.Port = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "print",
			Parent = "rcon",
			FullName = "rcon.print",
			ServerAdmin = true,
			Description = "If true, rcon commands etc will be printed in the console",
			Variable = true,
			GetOveride = (() => Facepunch.RCon.Print.ToString()),
			SetOveride = delegate(string str)
			{
				Facepunch.RCon.Print = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "web",
			Parent = "rcon",
			FullName = "rcon.web",
			ServerAdmin = true,
			Description = "If set to true, use websocket rcon. If set to false use legacy, source engine rcon.",
			Variable = true,
			GetOveride = (() => Facepunch.RCon.Web.ToString()),
			SetOveride = delegate(string str)
			{
				Facepunch.RCon.Web = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "horse",
			FullName = "horse.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Horse.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Horse.Population = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "update",
			Parent = "note",
			FullName = "note.update",
			ServerUser = true,
			Variable = false,
			Call = delegate(ConsoleSystem.Arg arg)
			{
				global::note.update(arg);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ai_dormant",
			Parent = "aimanager",
			FullName = "aimanager.ai_dormant",
			ServerAdmin = true,
			Description = "If ai_dormant is true, any npc outside the range of players will render itself dormant and take up less resources, but wildlife won't simulate as well.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.ai_dormant.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.ai_dormant = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ai_dormant_max_wakeup_per_tick",
			Parent = "aimanager",
			FullName = "aimanager.ai_dormant_max_wakeup_per_tick",
			ServerAdmin = true,
			Description = "ai_dormant_max_wakeup_per_tick defines the maximum number of dormant agents we will wake up in a single tick. (default: 20)",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.ai_dormant_max_wakeup_per_tick.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.ai_dormant_max_wakeup_per_tick = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "ai_to_player_distance_wakeup_range",
			Parent = "aimanager",
			FullName = "aimanager.ai_to_player_distance_wakeup_range",
			ServerAdmin = true,
			Description = "If an agent is beyond this distance to a player, it's flagged for becoming dormant.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.ai_to_player_distance_wakeup_range.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.ai_to_player_distance_wakeup_range = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_disable",
			Parent = "aimanager",
			FullName = "aimanager.nav_disable",
			ServerAdmin = true,
			Description = "If set to true the navmesh won't generate.. which means Ai that uses the navmesh won't be able to move",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_disable.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_disable = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid",
			ServerAdmin = true,
			Description = "If set to true the ai manager will control a navmesh grid, only building navmesh patches in those areas where it's needed and for those agent types. If this is false, we bake a single huge navmesh for the entire island, and can only support a single agent type.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_agents_expand_domain_enabled",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_agents_expand_domain_enabled",
			ServerAdmin = true,
			Description = "If nav_grid_agents_expand_domain_enabled is true, agents add new cells for baking as they walk around the world, as needed.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_agents_expand_domain_enabled.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_agents_expand_domain_enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_cell_height",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_cell_height",
			ServerAdmin = true,
			Description = "nav_grid_cell_height adjust how much vertical difference within a cell we can cope with when generating navmesh.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_cell_height.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_cell_height = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_cell_width",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_cell_width",
			ServerAdmin = true,
			Description = "nav_grid_cell_width adjust how large each cell in the navmesh grid is. Larger cells take longer to bake, but we need fewer of them to cover the map.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_cell_width.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_cell_width = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_kill_dormant_cells",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_kill_dormant_cells",
			ServerAdmin = true,
			Description = "If nav_grid_kill_dormant_cells is true, when a navigation grid cell is out of range from active players for a certain time, we unload it to save memory (but comes with extra performance hit since we have to regenerate that navmesh then next time it's activated again).",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_kill_dormant_cells.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_kill_dormant_cells = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_links_enabled",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_links_enabled",
			ServerAdmin = true,
			Description = "If nav_grid_links_enabled is true, agents can walk between navmesh grid cells, otherwise they can only walk on the navmesh grid cell they spawn (faster pathfinding).",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_links_enabled.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_links_enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_max_bakes_per_frame",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_max_bakes_per_frame",
			ServerAdmin = true,
			Description = "nav_grid_max_bakes_per_frame adjust how many rebakes of navmesh grid cells we will start each frame. The higher the number, the heavier performance impact.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_max_bakes_per_frame.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_max_bakes_per_frame = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_max_links_generated_per_frame",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_max_links_generated_per_frame",
			ServerAdmin = true,
			Description = "nav_grid_max_links_generated_per_frame adjust how many navmesh links can generate within a frame. The higher the number, the heavier performance impact.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_max_links_generated_per_frame.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_max_links_generated_per_frame = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_rebake_cells_enabled",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_rebake_cells_enabled",
			ServerAdmin = true,
			Description = "If nav_grid_rebake_cells_enabled is true, we rebake cells in player areas to adapt to construction and destruction.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_rebake_cells_enabled.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_rebake_cells_enabled = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_grid_rebake_cooldown",
			Parent = "aimanager",
			FullName = "aimanager.nav_grid_rebake_cooldown",
			ServerAdmin = true,
			Description = "nav_grid_rebake_cooldown defines how long a navmesh grid cell must wait between rebakes. Higher number means a cell will rebake less often.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_grid_rebake_cooldown.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_grid_rebake_cooldown = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_obstacles_carve_state",
			Parent = "aimanager",
			FullName = "aimanager.nav_obstacles_carve_state",
			ServerAdmin = true,
			Description = "nav_obstacles_carve_state defines which obstacles can carve the terrain. 0 - No carving, 1 - Only player construction carves, 2 - All obstacles carve.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_obstacles_carve_state.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_obstacles_carve_state = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "nav_wait",
			Parent = "aimanager",
			FullName = "aimanager.nav_wait",
			ServerAdmin = true,
			Description = "If true we'll wait for the navmesh to generate before completely starting the server. This might cause your server to hitch and lag as it generates in the background.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.nav_wait.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.nav_wait = StringExtensions.ToBool(str);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "pathfindingiterationsperframe",
			Parent = "aimanager",
			FullName = "aimanager.pathfindingiterationsperframe",
			ServerAdmin = true,
			Description = "The maximum amount of nodes processed each frame in the asynchronous pathfinding process. Increasing this value will cause the paths to be processed faster, but can cause some hiccups in frame rate. Default value is 100, a good range for tuning is between 50 and 500.",
			Variable = true,
			GetOveride = (() => Rust.Ai.AiManager.pathfindingIterationsPerFrame.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.AiManager.pathfindingIterationsPerFrame = StringExtensions.ToInt(str, 0);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cover_point_sample_step_height",
			Parent = "coverpointvolume",
			FullName = "coverpointvolume.cover_point_sample_step_height",
			ServerAdmin = true,
			Description = "cover_point_sample_step_height defines the height of the steps we do vertically for the cover point volume's cover point generation (smaller steps gives more accurate cover points, but at a higher processing cost). (default: 2.0)",
			Variable = true,
			GetOveride = (() => Rust.Ai.CoverPointVolume.cover_point_sample_step_height.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.CoverPointVolume.cover_point_sample_step_height = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "cover_point_sample_step_size",
			Parent = "coverpointvolume",
			FullName = "coverpointvolume.cover_point_sample_step_size",
			ServerAdmin = true,
			Description = "cover_point_sample_step_size defines the size of the steps we do horizontally for the cover point volume's cover point generation (smaller steps gives more accurate cover points, but at a higher processing cost). (default: 6.0)",
			Variable = true,
			GetOveride = (() => Rust.Ai.CoverPointVolume.cover_point_sample_step_size.ToString()),
			SetOveride = delegate(string str)
			{
				Rust.Ai.CoverPointVolume.cover_point_sample_step_size = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "stag",
			FullName = "stag.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Stag.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Stag.Population = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "wolf",
			FullName = "wolf.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Wolf.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Wolf.Population = StringExtensions.ToFloat(str, 0f);
			}
		},
		new ConsoleSystem.Command
		{
			Name = "population",
			Parent = "zombie",
			FullName = "zombie.population",
			ServerAdmin = true,
			Description = "Population active on the server, per square km",
			Variable = true,
			GetOveride = (() => global::Zombie.Population.ToString()),
			SetOveride = delegate(string str)
			{
				global::Zombie.Population = StringExtensions.ToFloat(str, 0f);
			}
		}
	};
}
