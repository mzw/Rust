﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006CA RID: 1738
public class VirtualItemIcon : MonoBehaviour
{
	// Token: 0x04001D73 RID: 7539
	public global::ItemDefinition itemDef;

	// Token: 0x04001D74 RID: 7540
	public int itemAmount;

	// Token: 0x04001D75 RID: 7541
	public bool asBlueprint;

	// Token: 0x04001D76 RID: 7542
	public Image iconImage;

	// Token: 0x04001D77 RID: 7543
	public Image bpUnderlay;

	// Token: 0x04001D78 RID: 7544
	public Text amountText;

	// Token: 0x04001D79 RID: 7545
	public CanvasGroup iconContents;

	// Token: 0x04001D7A RID: 7546
	public CanvasGroup conditionObject;

	// Token: 0x04001D7B RID: 7547
	public Image conditionFill;

	// Token: 0x04001D7C RID: 7548
	public Image maxConditionFill;

	// Token: 0x04001D7D RID: 7549
	public Image cornerIcon;
}
