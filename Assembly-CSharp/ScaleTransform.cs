﻿using System;
using UnityEngine;

// Token: 0x02000331 RID: 817
public class ScaleTransform : global::ScaleRenderer
{
	// Token: 0x060013B7 RID: 5047 RVA: 0x00073AF0 File Offset: 0x00071CF0
	public override void SetScale_Internal(float scale)
	{
		base.SetScale_Internal(scale);
		this.myRenderer.transform.localScale = this.initialScale * scale;
	}

	// Token: 0x060013B8 RID: 5048 RVA: 0x00073B18 File Offset: 0x00071D18
	public override void GatherInitialValues()
	{
		this.initialScale = this.myRenderer.transform.localScale;
		base.GatherInitialValues();
	}

	// Token: 0x04000EA2 RID: 3746
	private Vector3 initialScale;
}
