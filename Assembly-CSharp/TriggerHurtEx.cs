﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rust;
using UnityEngine;

// Token: 0x0200062A RID: 1578
public class TriggerHurtEx : global::TriggerBase, IServerComponent
{
	// Token: 0x06002018 RID: 8216 RVA: 0x000B7B30 File Offset: 0x000B5D30
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (!(baseEntity is global::BaseCombatEntity))
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x06002019 RID: 8217 RVA: 0x000B7B8C File Offset: 0x000B5D8C
	internal void DoDamage(global::BaseEntity ent, global::TriggerHurtEx.HurtType type, List<Rust.DamageTypeEntry> damage, global::GameObjectRef effect, float multiply = 1f)
	{
		if (!this.damageEnabled)
		{
			return;
		}
		using (TimeWarning.New("TriggerHurtEx.DoDamage", 0.1f))
		{
			if (damage != null && damage.Count > 0)
			{
				global::BaseCombatEntity baseCombatEntity = ent as global::BaseCombatEntity;
				if (baseCombatEntity)
				{
					global::HitInfo hitInfo = new global::HitInfo();
					hitInfo.damageTypes.Add(damage);
					hitInfo.damageTypes.ScaleAll(multiply);
					hitInfo.DoHitEffects = true;
					hitInfo.DidHit = true;
					hitInfo.Initiator = base.gameObject.ToBaseEntity();
					hitInfo.PointStart = base.transform.position;
					hitInfo.PointEnd = baseCombatEntity.transform.position;
					if (type == global::TriggerHurtEx.HurtType.Simple)
					{
						baseCombatEntity.Hurt(hitInfo);
					}
					else
					{
						baseCombatEntity.OnAttacked(hitInfo);
					}
				}
			}
			if (effect.isValid)
			{
				global::Effect.server.Run(effect.resourcePath, ent, global::StringPool.closest, base.transform.position, Vector3.up, null, false);
			}
		}
	}

	// Token: 0x0600201A RID: 8218 RVA: 0x000B7CA4 File Offset: 0x000B5EA4
	internal override void OnEntityEnter(global::BaseEntity ent)
	{
		base.OnEntityEnter(ent);
		if (ent == null)
		{
			return;
		}
		if (this.entityAddList == null)
		{
			this.entityAddList = new List<global::BaseEntity>();
		}
		this.entityAddList.Add(ent);
		base.Invoke(new Action(this.ProcessQueues), 0.1f);
	}

	// Token: 0x0600201B RID: 8219 RVA: 0x000B7D00 File Offset: 0x000B5F00
	internal override void OnEntityLeave(global::BaseEntity ent)
	{
		base.OnEntityLeave(ent);
		if (ent == null)
		{
			return;
		}
		if (this.entityLeaveList == null)
		{
			this.entityLeaveList = new List<global::BaseEntity>();
		}
		this.entityLeaveList.Add(ent);
		base.Invoke(new Action(this.ProcessQueues), 0.1f);
	}

	// Token: 0x0600201C RID: 8220 RVA: 0x000B7D5C File Offset: 0x000B5F5C
	internal override void OnObjects()
	{
		base.InvokeRepeating(new Action(this.OnTick), this.repeatRate, this.repeatRate);
	}

	// Token: 0x0600201D RID: 8221 RVA: 0x000B7D7C File Offset: 0x000B5F7C
	internal override void OnEmpty()
	{
		base.CancelInvoke(new Action(this.OnTick));
	}

	// Token: 0x0600201E RID: 8222 RVA: 0x000B7D90 File Offset: 0x000B5F90
	private void OnTick()
	{
		this.ProcessQueues();
		if (this.entityInfo != null)
		{
			foreach (KeyValuePair<global::BaseEntity, global::TriggerHurtEx.EntityTriggerInfo> keyValuePair in this.entityInfo.ToArray<KeyValuePair<global::BaseEntity, global::TriggerHurtEx.EntityTriggerInfo>>())
			{
				if (keyValuePair.Key.IsValid())
				{
					Vector3 estimatedWorldPosition = keyValuePair.Key.GetEstimatedWorldPosition();
					float magnitude = (estimatedWorldPosition - keyValuePair.Value.lastPosition).magnitude;
					if (magnitude > 0.01f)
					{
						keyValuePair.Value.lastPosition = estimatedWorldPosition;
						this.DoDamage(keyValuePair.Key, this.hurtTypeOnMove, this.damageOnMove, this.effectOnMove, magnitude);
					}
					this.DoDamage(keyValuePair.Key, this.hurtTypeOnTimer, this.damageOnTimer, this.effectOnTimer, this.repeatRate);
				}
			}
		}
	}

	// Token: 0x0600201F RID: 8223 RVA: 0x000B7E7C File Offset: 0x000B607C
	private void ProcessQueues()
	{
		if (this.entityAddList != null)
		{
			foreach (global::BaseEntity baseEntity in this.entityAddList)
			{
				if (baseEntity.IsValid())
				{
					this.DoDamage(baseEntity, this.hurtTypeOnEnter, this.damageOnEnter, this.effectOnEnter, 1f);
					if (this.entityInfo == null)
					{
						this.entityInfo = new Dictionary<global::BaseEntity, global::TriggerHurtEx.EntityTriggerInfo>();
					}
					if (!this.entityInfo.ContainsKey(baseEntity))
					{
						this.entityInfo.Add(baseEntity, new global::TriggerHurtEx.EntityTriggerInfo
						{
							lastPosition = baseEntity.GetEstimatedWorldPosition()
						});
					}
				}
			}
			this.entityAddList = null;
		}
		if (this.entityLeaveList != null)
		{
			foreach (global::BaseEntity baseEntity2 in this.entityLeaveList)
			{
				if (baseEntity2.IsValid())
				{
					this.DoDamage(baseEntity2, this.hurtTypeOnLeave, this.damageOnLeave, this.effectOnLeave, 1f);
					if (this.entityInfo != null)
					{
						this.entityInfo.Remove(baseEntity2);
						if (this.entityInfo.Count == 0)
						{
							this.entityInfo = null;
						}
					}
				}
			}
			this.entityLeaveList.Clear();
		}
	}

	// Token: 0x04001AD7 RID: 6871
	public float repeatRate = 0.1f;

	// Token: 0x04001AD8 RID: 6872
	[Header("On Enter")]
	public List<Rust.DamageTypeEntry> damageOnEnter;

	// Token: 0x04001AD9 RID: 6873
	public global::GameObjectRef effectOnEnter;

	// Token: 0x04001ADA RID: 6874
	public global::TriggerHurtEx.HurtType hurtTypeOnEnter;

	// Token: 0x04001ADB RID: 6875
	[Header("On Timer (damage per second)")]
	public List<Rust.DamageTypeEntry> damageOnTimer;

	// Token: 0x04001ADC RID: 6876
	public global::GameObjectRef effectOnTimer;

	// Token: 0x04001ADD RID: 6877
	public global::TriggerHurtEx.HurtType hurtTypeOnTimer;

	// Token: 0x04001ADE RID: 6878
	[Header("On Move (damage per meter)")]
	public List<Rust.DamageTypeEntry> damageOnMove;

	// Token: 0x04001ADF RID: 6879
	public global::GameObjectRef effectOnMove;

	// Token: 0x04001AE0 RID: 6880
	public global::TriggerHurtEx.HurtType hurtTypeOnMove;

	// Token: 0x04001AE1 RID: 6881
	[Header("On Leave")]
	public List<Rust.DamageTypeEntry> damageOnLeave;

	// Token: 0x04001AE2 RID: 6882
	public global::GameObjectRef effectOnLeave;

	// Token: 0x04001AE3 RID: 6883
	public global::TriggerHurtEx.HurtType hurtTypeOnLeave;

	// Token: 0x04001AE4 RID: 6884
	public bool damageEnabled = true;

	// Token: 0x04001AE5 RID: 6885
	internal Dictionary<global::BaseEntity, global::TriggerHurtEx.EntityTriggerInfo> entityInfo;

	// Token: 0x04001AE6 RID: 6886
	internal List<global::BaseEntity> entityAddList;

	// Token: 0x04001AE7 RID: 6887
	internal List<global::BaseEntity> entityLeaveList;

	// Token: 0x0200062B RID: 1579
	public enum HurtType
	{
		// Token: 0x04001AE9 RID: 6889
		Simple,
		// Token: 0x04001AEA RID: 6890
		IncludeBleedingAndScreenShake
	}

	// Token: 0x0200062C RID: 1580
	public class EntityTriggerInfo
	{
		// Token: 0x04001AEB RID: 6891
		public Vector3 lastPosition;
	}
}
