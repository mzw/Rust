﻿using System;

// Token: 0x02000439 RID: 1081
public interface IOnSendNetworkUpdate
{
	// Token: 0x0600186B RID: 6251
	void OnSendNetworkUpdate(global::BaseEntity entity);
}
