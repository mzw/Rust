﻿using System;
using UnityEngine;

// Token: 0x020004B4 RID: 1204
[ExecuteInEditMode]
public class LinearFog : MonoBehaviour
{
	// Token: 0x060019E0 RID: 6624 RVA: 0x00091498 File Offset: 0x0008F698
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (!this.fogMaterial)
		{
			Graphics.Blit(source, destination);
			return;
		}
		this.fogMaterial.SetColor("_FogColor", this.fogColor);
		this.fogMaterial.SetFloat("_Start", this.fogStart);
		this.fogMaterial.SetFloat("_Range", this.fogRange);
		this.fogMaterial.SetFloat("_Density", this.fogDensity);
		if (this.fogSky)
		{
			this.fogMaterial.SetFloat("_CutOff", 2f);
		}
		else
		{
			this.fogMaterial.SetFloat("_CutOff", 1f);
		}
		for (int i = 0; i < this.fogMaterial.passCount; i++)
		{
			Graphics.Blit(source, destination, this.fogMaterial, i);
		}
	}

	// Token: 0x04001496 RID: 5270
	public Material fogMaterial;

	// Token: 0x04001497 RID: 5271
	public Color fogColor = Color.white;

	// Token: 0x04001498 RID: 5272
	public float fogStart;

	// Token: 0x04001499 RID: 5273
	public float fogRange = 1f;

	// Token: 0x0400149A RID: 5274
	public float fogDensity = 1f;

	// Token: 0x0400149B RID: 5275
	public bool fogSky;
}
