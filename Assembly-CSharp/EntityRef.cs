﻿using System;
using UnityEngine;

// Token: 0x02000386 RID: 902
public struct EntityRef
{
	// Token: 0x0600156B RID: 5483 RVA: 0x0007B488 File Offset: 0x00079688
	public bool IsSet()
	{
		return this.id_cached != 0u;
	}

	// Token: 0x0600156C RID: 5484 RVA: 0x0007B498 File Offset: 0x00079698
	public bool IsValid(bool serverside)
	{
		return this.Get(serverside).IsValid();
	}

	// Token: 0x0600156D RID: 5485 RVA: 0x0007B4A8 File Offset: 0x000796A8
	public void Set(global::BaseEntity ent)
	{
		this.ent_cached = ent;
		this.id_cached = 0u;
		if (this.ent_cached.IsValid())
		{
			this.id_cached = this.ent_cached.net.ID;
		}
	}

	// Token: 0x0600156E RID: 5486 RVA: 0x0007B4E0 File Offset: 0x000796E0
	public global::BaseEntity Get(bool serverside)
	{
		if (this.ent_cached == null && this.id_cached > 0u)
		{
			if (serverside)
			{
				this.ent_cached = (global::BaseNetworkable.serverEntities.Find(this.id_cached) as global::BaseEntity);
			}
			else
			{
				Debug.LogWarning("EntityRef: Looking for clientside entities on pure server!");
			}
		}
		if (!this.ent_cached.IsValid())
		{
			this.ent_cached = null;
		}
		return this.ent_cached;
	}

	// Token: 0x17000186 RID: 390
	// (get) Token: 0x0600156F RID: 5487 RVA: 0x0007B558 File Offset: 0x00079758
	// (set) Token: 0x06001570 RID: 5488 RVA: 0x0007B588 File Offset: 0x00079788
	public uint uid
	{
		get
		{
			if (this.ent_cached.IsValid())
			{
				this.id_cached = this.ent_cached.net.ID;
			}
			return this.id_cached;
		}
		set
		{
			this.id_cached = value;
			if (this.id_cached == 0u)
			{
				this.ent_cached = null;
				return;
			}
			if (this.ent_cached.IsValid() && this.ent_cached.net.ID == this.id_cached)
			{
				return;
			}
			this.ent_cached = null;
		}
	}

	// Token: 0x04000FB6 RID: 4022
	internal global::BaseEntity ent_cached;

	// Token: 0x04000FB7 RID: 4023
	internal uint id_cached;
}
