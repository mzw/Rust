﻿using System;

// Token: 0x020004F8 RID: 1272
public class ItemModUseContent : global::ItemMod
{
	// Token: 0x06001B0C RID: 6924 RVA: 0x00097750 File Offset: 0x00095950
	public override void DoAction(global::Item item, global::BasePlayer player)
	{
		if (item.contents == null)
		{
			return;
		}
		if (item.contents.itemList.Count == 0)
		{
			return;
		}
		global::Item item2 = item.contents.itemList[0];
		item2.UseItem(this.amountToConsume);
	}

	// Token: 0x040015E3 RID: 5603
	public int amountToConsume = 1;
}
