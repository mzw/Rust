﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000436 RID: 1078
public static class OnParentDestroyingEx
{
	// Token: 0x06001866 RID: 6246 RVA: 0x0008A538 File Offset: 0x00088738
	public static void BroadcastOnParentDestroying(this GameObject go)
	{
		List<global::IOnParentDestroying> list = Pool.GetList<global::IOnParentDestroying>();
		go.GetComponentsInChildren<global::IOnParentDestroying>(list);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].OnParentDestroying();
		}
		Pool.FreeList<global::IOnParentDestroying>(ref list);
	}

	// Token: 0x06001867 RID: 6247 RVA: 0x0008A57C File Offset: 0x0008877C
	public static void SendOnParentDestroying(this GameObject go)
	{
		List<global::IOnParentDestroying> list = Pool.GetList<global::IOnParentDestroying>();
		go.GetComponents<global::IOnParentDestroying>(list);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].OnParentDestroying();
		}
		Pool.FreeList<global::IOnParentDestroying>(ref list);
	}
}
