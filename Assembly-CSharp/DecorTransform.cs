﻿using System;
using UnityEngine;

// Token: 0x0200054A RID: 1354
public class DecorTransform : global::DecorComponent
{
	// Token: 0x06001C92 RID: 7314 RVA: 0x000A0200 File Offset: 0x0009E400
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		pos += rot * Vector3.Scale(scale, this.Position);
		rot = Quaternion.Euler(this.Rotation) * rot;
		scale = Vector3.Scale(scale, this.Scale);
	}

	// Token: 0x0400179B RID: 6043
	public Vector3 Position = new Vector3(0f, 0f, 0f);

	// Token: 0x0400179C RID: 6044
	public Vector3 Rotation = new Vector3(0f, 0f, 0f);

	// Token: 0x0400179D RID: 6045
	public Vector3 Scale = new Vector3(1f, 1f, 1f);
}
