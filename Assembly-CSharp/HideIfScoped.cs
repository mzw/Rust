﻿using System;
using UnityEngine;

// Token: 0x020000D9 RID: 217
public class HideIfScoped : MonoBehaviour
{
	// Token: 0x06000B0B RID: 2827 RVA: 0x0004A390 File Offset: 0x00048590
	public void SetVisible(bool vis)
	{
		foreach (Renderer renderer in this.renderers)
		{
			renderer.enabled = vis;
		}
	}

	// Token: 0x040005C4 RID: 1476
	public Renderer[] renderers;
}
