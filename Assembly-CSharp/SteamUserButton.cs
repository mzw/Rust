﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000703 RID: 1795
public class SteamUserButton : MonoBehaviour
{
	// Token: 0x04001EB6 RID: 7862
	public Text steamName;

	// Token: 0x04001EB7 RID: 7863
	public RawImage avatar;

	// Token: 0x04001EB8 RID: 7864
	public Color textColorInGame;

	// Token: 0x04001EB9 RID: 7865
	public Color textColorOnline;

	// Token: 0x04001EBA RID: 7866
	public Color textColorNormal;
}
