﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200040F RID: 1039
public abstract class DeployVolume : global::PrefabAttribute
{
	// Token: 0x060017E1 RID: 6113 RVA: 0x000886B0 File Offset: 0x000868B0
	protected override Type GetIndexedType()
	{
		return typeof(global::DeployVolume);
	}

	// Token: 0x060017E2 RID: 6114
	protected abstract bool Check(Vector3 position, Quaternion rotation, int mask = -1);

	// Token: 0x060017E3 RID: 6115
	protected abstract bool Check(Vector3 position, Quaternion rotation, OBB test, int mask = -1);

	// Token: 0x060017E4 RID: 6116 RVA: 0x000886BC File Offset: 0x000868BC
	public static bool Check(Vector3 position, Quaternion rotation, global::DeployVolume[] volumes, int mask = -1)
	{
		for (int i = 0; i < volumes.Length; i++)
		{
			if (volumes[i].Check(position, rotation, mask))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060017E5 RID: 6117 RVA: 0x000886F0 File Offset: 0x000868F0
	public static bool Check(Vector3 position, Quaternion rotation, global::DeployVolume[] volumes, OBB test, int mask = -1)
	{
		for (int i = 0; i < volumes.Length; i++)
		{
			if (volumes[i].Check(position, rotation, test, mask))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060017E6 RID: 6118 RVA: 0x00088728 File Offset: 0x00086928
	public static bool CheckSphere(Vector3 pos, float radius, int layerMask, global::ColliderInfo.Flags ignore)
	{
		if (ignore == (global::ColliderInfo.Flags)0)
		{
			return global::GamePhysics.CheckSphere(pos, radius, layerMask, 0);
		}
		List<Collider> list = Pool.GetList<Collider>();
		global::GamePhysics.OverlapSphere(pos, radius, list, layerMask, 1);
		bool result = global::DeployVolume.CheckFlags(list, ignore);
		Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x060017E7 RID: 6119 RVA: 0x00088768 File Offset: 0x00086968
	public static bool CheckCapsule(Vector3 start, Vector3 end, float radius, int layerMask, global::ColliderInfo.Flags ignore)
	{
		if (ignore == (global::ColliderInfo.Flags)0)
		{
			return global::GamePhysics.CheckCapsule(start, end, radius, layerMask, 0);
		}
		List<Collider> list = Pool.GetList<Collider>();
		global::GamePhysics.OverlapCapsule(start, end, radius, list, layerMask, 1);
		bool result = global::DeployVolume.CheckFlags(list, ignore);
		Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x060017E8 RID: 6120 RVA: 0x000887AC File Offset: 0x000869AC
	public static bool CheckOBB(OBB obb, int layerMask, global::ColliderInfo.Flags ignore)
	{
		if (ignore == (global::ColliderInfo.Flags)0)
		{
			return global::GamePhysics.CheckOBB(obb, layerMask, 0);
		}
		List<Collider> list = Pool.GetList<Collider>();
		global::GamePhysics.OverlapOBB(obb, list, layerMask, 1);
		bool result = global::DeployVolume.CheckFlags(list, ignore);
		Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x060017E9 RID: 6121 RVA: 0x000887E8 File Offset: 0x000869E8
	public static bool CheckBounds(Bounds bounds, int layerMask, global::ColliderInfo.Flags ignore)
	{
		if (ignore == (global::ColliderInfo.Flags)0)
		{
			return global::GamePhysics.CheckBounds(bounds, layerMask, 0);
		}
		List<Collider> list = Pool.GetList<Collider>();
		global::GamePhysics.OverlapBounds(bounds, list, layerMask, 1);
		bool result = global::DeployVolume.CheckFlags(list, ignore);
		Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x060017EA RID: 6122 RVA: 0x00088824 File Offset: 0x00086A24
	private static bool CheckFlags(List<Collider> list, global::ColliderInfo.Flags ignore)
	{
		for (int i = 0; i < list.Count; i++)
		{
			global::ColliderInfo component = list[i].gameObject.GetComponent<global::ColliderInfo>();
			if (component == null)
			{
				return true;
			}
			if (!component.HasFlag(ignore))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x0400127D RID: 4733
	public LayerMask layers = 537001984;

	// Token: 0x0400127E RID: 4734
	[InspectorFlags]
	public global::ColliderInfo.Flags ignore;
}
