﻿using System;

// Token: 0x02000357 RID: 855
public class FlintStrikeWeapon : global::BaseProjectile
{
	// Token: 0x06001473 RID: 5235 RVA: 0x000774B0 File Offset: 0x000756B0
	public override global::RecoilProperties GetRecoil()
	{
		return this.strikeRecoil;
	}

	// Token: 0x04000F32 RID: 3890
	public float successFraction = 0.5f;

	// Token: 0x04000F33 RID: 3891
	public global::RecoilProperties strikeRecoil;
}
