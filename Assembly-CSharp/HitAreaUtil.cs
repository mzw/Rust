﻿using System;

// Token: 0x02000432 RID: 1074
public static class HitAreaUtil
{
	// Token: 0x06001862 RID: 6242 RVA: 0x0008A508 File Offset: 0x00088708
	public static string Format(global::HitArea area)
	{
		if (area == (global::HitArea)0)
		{
			return "None";
		}
		if (area == (global::HitArea)-1)
		{
			return "Generic";
		}
		return area.ToString();
	}
}
