﻿using System;
using UnityEngine;

// Token: 0x020007BC RID: 1980
public class RagdollEditor : SingletonComponent<global::RagdollEditor>
{
	// Token: 0x060024C3 RID: 9411 RVA: 0x000CA344 File Offset: 0x000C8544
	private void OnGUI()
	{
		GUI.Box(new Rect((float)Screen.width * 0.5f - 2f, (float)Screen.height * 0.5f - 2f, 4f, 4f), string.Empty);
	}

	// Token: 0x060024C4 RID: 9412 RVA: 0x000CA384 File Offset: 0x000C8584
	protected override void Awake()
	{
		base.Awake();
	}

	// Token: 0x060024C5 RID: 9413 RVA: 0x000CA38C File Offset: 0x000C858C
	private void Update()
	{
		Camera.main.fieldOfView = 75f;
		if (Input.GetKey(324))
		{
			this.view.y = this.view.y + Input.GetAxisRaw("Mouse X") * 3f;
			this.view.x = this.view.x - Input.GetAxisRaw("Mouse Y") * 3f;
			Cursor.lockState = 1;
			Cursor.visible = false;
		}
		else
		{
			Cursor.lockState = 0;
			Cursor.visible = true;
		}
		Camera.main.transform.rotation = Quaternion.Euler(this.view);
		Vector3 vector = Vector3.zero;
		if (Input.GetKey(119))
		{
			vector += Vector3.forward;
		}
		if (Input.GetKey(115))
		{
			vector += Vector3.back;
		}
		if (Input.GetKey(97))
		{
			vector += Vector3.left;
		}
		if (Input.GetKey(100))
		{
			vector += Vector3.right;
		}
		Camera.main.transform.position += base.transform.rotation * vector * 0.05f;
		if (Input.GetKeyDown(323))
		{
			this.StartGrab();
		}
		if (Input.GetKeyUp(323))
		{
			this.StopGrab();
		}
	}

	// Token: 0x060024C6 RID: 9414 RVA: 0x000CA4F8 File Offset: 0x000C86F8
	private void FixedUpdate()
	{
		if (Input.GetKey(323))
		{
			this.UpdateGrab();
		}
	}

	// Token: 0x060024C7 RID: 9415 RVA: 0x000CA510 File Offset: 0x000C8710
	private void StartGrab()
	{
		RaycastHit raycastHit;
		if (!Physics.Raycast(base.transform.position, base.transform.forward, ref raycastHit, 100f))
		{
			return;
		}
		this.grabbedRigid = raycastHit.collider.GetComponent<Rigidbody>();
		if (this.grabbedRigid == null)
		{
			return;
		}
		this.grabPos = this.grabbedRigid.transform.worldToLocalMatrix.MultiplyPoint(raycastHit.point);
		this.grabOffset = base.transform.worldToLocalMatrix.MultiplyPoint(raycastHit.point);
	}

	// Token: 0x060024C8 RID: 9416 RVA: 0x000CA5B0 File Offset: 0x000C87B0
	private void UpdateGrab()
	{
		if (this.grabbedRigid == null)
		{
			return;
		}
		Vector3 vector = base.transform.TransformPoint(this.grabOffset);
		Vector3 vector2 = this.grabbedRigid.transform.TransformPoint(this.grabPos);
		Vector3 vector3 = vector - vector2;
		this.grabbedRigid.AddForceAtPosition(vector3 * 100f, vector2, 5);
	}

	// Token: 0x060024C9 RID: 9417 RVA: 0x000CA618 File Offset: 0x000C8818
	private void StopGrab()
	{
		this.grabbedRigid = null;
	}

	// Token: 0x04002043 RID: 8259
	private Vector3 view = default(Vector3);

	// Token: 0x04002044 RID: 8260
	private Rigidbody grabbedRigid;

	// Token: 0x04002045 RID: 8261
	private Vector3 grabPos;

	// Token: 0x04002046 RID: 8262
	private Vector3 grabOffset;
}
