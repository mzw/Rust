﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200000A RID: 10
public class AutoTurret : global::StorageContainer
{
	// Token: 0x060002EA RID: 746 RVA: 0x0001007C File Offset: 0x0000E27C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("AutoTurret.OnRpcMessage", 0.1f))
		{
			if (rpc == 2017592092u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - AddSelfAuthorize ");
				}
				using (TimeWarning.New("AddSelfAuthorize", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("AddSelfAuthorize", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.AddSelfAuthorize(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in AddSelfAuthorize");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3024779371u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ClearList ");
				}
				using (TimeWarning.New("ClearList", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("ClearList", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ClearList(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in ClearList");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 3523413432u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - FlipAim ");
				}
				using (TimeWarning.New("FlipAim", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("FlipAim", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.FlipAim(rpc4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in FlipAim");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 2101914649u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RemoveSelfAuthorize ");
				}
				using (TimeWarning.New("RemoveSelfAuthorize", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RemoveSelfAuthorize", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RemoveSelfAuthorize(rpc5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in RemoveSelfAuthorize");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
			if (rpc == 786675805u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SERVER_AttackAll ");
				}
				using (TimeWarning.New("SERVER_AttackAll", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SERVER_AttackAll", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc6 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SERVER_AttackAll(rpc6);
						}
					}
					catch (Exception ex5)
					{
						player.Kick("RPC Error in SERVER_AttackAll");
						Debug.LogException(ex5);
					}
				}
				return true;
			}
			if (rpc == 2795490276u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SERVER_Peacekeeper ");
				}
				using (TimeWarning.New("SERVER_Peacekeeper", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SERVER_Peacekeeper", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc7 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SERVER_Peacekeeper(rpc7);
						}
					}
					catch (Exception ex6)
					{
						player.Kick("RPC Error in SERVER_Peacekeeper");
						Debug.LogException(ex6);
					}
				}
				return true;
			}
			if (rpc == 1498407638u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SERVER_TurnOff ");
				}
				using (TimeWarning.New("SERVER_TurnOff", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SERVER_TurnOff", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc8 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SERVER_TurnOff(rpc8);
						}
					}
					catch (Exception ex7)
					{
						player.Kick("RPC Error in SERVER_TurnOff");
						Debug.LogException(ex7);
					}
				}
				return true;
			}
			if (rpc == 1156714392u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SERVER_TurnOn ");
				}
				using (TimeWarning.New("SERVER_TurnOn", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SERVER_TurnOn", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc9 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SERVER_TurnOn(rpc9);
						}
					}
					catch (Exception ex8)
					{
						player.Kick("RPC Error in SERVER_TurnOn");
						Debug.LogException(ex8);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060002EB RID: 747 RVA: 0x00010D3C File Offset: 0x0000EF3C
	public bool IsOnline()
	{
		return base.IsOn();
	}

	// Token: 0x060002EC RID: 748 RVA: 0x00010D44 File Offset: 0x0000EF44
	public bool IsOffline()
	{
		return !this.IsOnline();
	}

	// Token: 0x060002ED RID: 749 RVA: 0x00010D50 File Offset: 0x0000EF50
	public float AngleToTarget(global::BaseCombatEntity potentialtarget)
	{
		Vector3 vector = this.AimOffset(potentialtarget);
		Vector3 position = this.muzzlePos.transform.position;
		Vector3 normalized = (vector - position).normalized;
		return Vector3.Angle(this.muzzlePos.forward, normalized);
	}

	// Token: 0x060002EE RID: 750 RVA: 0x00010D9C File Offset: 0x0000EF9C
	public bool InFiringArc(global::BaseCombatEntity potentialtarget)
	{
		return this.AngleToTarget(potentialtarget) <= 90f;
	}

	// Token: 0x060002EF RID: 751 RVA: 0x00010DB0 File Offset: 0x0000EFB0
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.CanPickup(player) && this.IsOffline() && this.IsAuthed(player);
	}

	// Token: 0x060002F0 RID: 752 RVA: 0x00010DD4 File Offset: 0x0000EFD4
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.autoturret = Facepunch.Pool.Get<ProtoBuf.AutoTurret>();
		info.msg.autoturret.users = this.authorizedPlayers;
	}

	// Token: 0x060002F1 RID: 753 RVA: 0x00010E08 File Offset: 0x0000F008
	public override void PostSave(global::BaseNetworkable.SaveInfo info)
	{
		base.PostSave(info);
		info.msg.autoturret.users = null;
	}

	// Token: 0x060002F2 RID: 754 RVA: 0x00010E24 File Offset: 0x0000F024
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.autoturret != null)
		{
			this.authorizedPlayers = info.msg.autoturret.users;
			info.msg.autoturret.users = null;
		}
	}

	// Token: 0x060002F3 RID: 755 RVA: 0x00010E74 File Offset: 0x0000F074
	public Vector3 AimOffset(global::BaseCombatEntity aimat)
	{
		global::BasePlayer basePlayer = aimat as global::BasePlayer;
		if (basePlayer != null)
		{
			return basePlayer.eyes.position;
		}
		return aimat.transform.position + new Vector3(0f, 0.3f, 0f);
	}

	// Token: 0x060002F4 RID: 756 RVA: 0x00010EC4 File Offset: 0x0000F0C4
	public float GetAimSpeed()
	{
		if (this.HasTarget())
		{
			return 5f;
		}
		return 1f;
	}

	// Token: 0x060002F5 RID: 757 RVA: 0x00010EDC File Offset: 0x0000F0DC
	public void UpdateAiming()
	{
		if (this.aimDir == Vector3.zero)
		{
			return;
		}
		float num = (!base.isServer) ? 5f : 8f;
		Quaternion quaternion = Quaternion.LookRotation(this.aimDir);
		Quaternion quaternion2 = Quaternion.Euler(0f, quaternion.eulerAngles.y, 0f);
		Quaternion quaternion3 = Quaternion.Euler(quaternion.eulerAngles.x, 0f, 0f);
		if (this.gun_yaw.transform.rotation != quaternion2)
		{
			this.gun_yaw.transform.rotation = Quaternion.Lerp(this.gun_yaw.transform.rotation, quaternion2, UnityEngine.Time.deltaTime * num);
		}
		if (this.gun_pitch.transform.localRotation != quaternion3)
		{
			this.gun_pitch.transform.localRotation = Quaternion.Lerp(this.gun_pitch.transform.localRotation, quaternion3, UnityEngine.Time.deltaTime * num);
		}
	}

	// Token: 0x060002F6 RID: 758 RVA: 0x00010FF8 File Offset: 0x0000F1F8
	public bool IsAuthed(global::BasePlayer player)
	{
		return this.authorizedPlayers.Any((PlayerNameID x) => x.userid == player.userID);
	}

	// Token: 0x060002F7 RID: 759 RVA: 0x0001102C File Offset: 0x0000F22C
	public bool AnyAuthed()
	{
		return this.authorizedPlayers.Count > 0;
	}

	// Token: 0x060002F8 RID: 760 RVA: 0x0001103C File Offset: 0x0000F23C
	public bool CanChangeSettings(global::BasePlayer player)
	{
		return this.IsAuthed(player) && this.IsOffline();
	}

	// Token: 0x060002F9 RID: 761 RVA: 0x00011054 File Offset: 0x0000F254
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x060002FA RID: 762 RVA: 0x00011058 File Offset: 0x0000F258
	public bool PeacekeeperMode()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x060002FB RID: 763 RVA: 0x00011068 File Offset: 0x0000F268
	public void SetOnline()
	{
		this.SetIsOnline(true);
	}

	// Token: 0x060002FC RID: 764 RVA: 0x00011074 File Offset: 0x0000F274
	public void SetIsOnline(bool online)
	{
		if (online == base.HasFlag(global::BaseEntity.Flags.On))
		{
			return;
		}
		if (Interface.CallHook("OnTurretToggle", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.On, online, false);
		this.booting = false;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		if (this.IsOffline())
		{
			this.SetTarget(null);
			this.isLootable = true;
		}
		else
		{
			this.isLootable = false;
		}
	}

	// Token: 0x060002FD RID: 765 RVA: 0x000110E8 File Offset: 0x0000F2E8
	public void InitiateShutdown()
	{
		if (this.IsOffline())
		{
			return;
		}
		if (Interface.CallHook("OnTurretShutdown", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		global::Effect.server.Run(this.offlineSound.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		this.SetIsOnline(false);
	}

	// Token: 0x060002FE RID: 766 RVA: 0x00011140 File Offset: 0x0000F340
	public void InitiateStartup()
	{
		if (this.IsOnline() || this.booting)
		{
			return;
		}
		if (Interface.CallHook("OnTurretStartup", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		global::Effect.server.Run(this.onlineSound.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		base.Invoke(new Action(this.SetOnline), 2f);
		this.booting = true;
	}

	// Token: 0x060002FF RID: 767 RVA: 0x000111BC File Offset: 0x0000F3BC
	public void SetPeacekeepermode(bool isOn)
	{
		bool flag = this.PeacekeeperMode();
		if (flag == isOn)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved1, isOn, false);
		global::Effect.server.Run(this.peacekeeperToggleSound.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		Interface.CallHook("OnTurretModeToggle", new object[]
		{
			this
		});
	}

	// Token: 0x06000300 RID: 768 RVA: 0x0001121C File Offset: 0x0000F41C
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void FlipAim(global::BaseEntity.RPCMessage rpc)
	{
		if (this.IsOnline() || !this.IsAuthed(rpc.player) || this.booting)
		{
			return;
		}
		base.transform.rotation = Quaternion.LookRotation(-base.transform.forward, base.transform.up);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000301 RID: 769 RVA: 0x00011284 File Offset: 0x0000F484
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void AddSelfAuthorize(global::BaseEntity.RPCMessage rpc)
	{
		global::BaseEntity.RPCMessage rpc = rpc2;
		if (this.IsOnline())
		{
			return;
		}
		if (Interface.CallHook("OnTurretAuthorize", new object[]
		{
			this,
			rpc2.player
		}) != null)
		{
			return;
		}
		this.authorizedPlayers.RemoveAll((PlayerNameID x) => x.userid == rpc.player.userID);
		PlayerNameID playerNameID = new PlayerNameID();
		playerNameID.userid = rpc.player.userID;
		playerNameID.username = rpc.player.displayName;
		this.authorizedPlayers.Add(playerNameID);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000302 RID: 770 RVA: 0x0001132C File Offset: 0x0000F52C
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void RemoveSelfAuthorize(global::BaseEntity.RPCMessage rpc)
	{
		global::BaseEntity.RPCMessage rpc = rpc2;
		if (this.booting || this.IsOnline() || !this.IsAuthed(rpc.player))
		{
			return;
		}
		if (Interface.CallHook("OnTurretDeauthorize", new object[]
		{
			this,
			rpc2.player
		}) != null)
		{
			return;
		}
		this.authorizedPlayers.RemoveAll((PlayerNameID x) => x.userid == rpc.player.userID);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000303 RID: 771 RVA: 0x000113B8 File Offset: 0x0000F5B8
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void ClearList(global::BaseEntity.RPCMessage rpc)
	{
		if (this.booting || this.IsOnline() || !this.IsAuthed(rpc.player))
		{
			return;
		}
		if (Interface.CallHook("OnTurretClearList", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		this.authorizedPlayers.Clear();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000304 RID: 772 RVA: 0x00011424 File Offset: 0x0000F624
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void SERVER_TurnOn(global::BaseEntity.RPCMessage rpc)
	{
		if (this.IsAuthed(rpc.player))
		{
			this.InitiateStartup();
		}
	}

	// Token: 0x06000305 RID: 773 RVA: 0x00011440 File Offset: 0x0000F640
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void SERVER_TurnOff(global::BaseEntity.RPCMessage rpc)
	{
		if (this.IsAuthed(rpc.player))
		{
			this.InitiateShutdown();
		}
	}

	// Token: 0x06000306 RID: 774 RVA: 0x0001145C File Offset: 0x0000F65C
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void SERVER_Peacekeeper(global::BaseEntity.RPCMessage rpc)
	{
		if (this.IsAuthed(rpc.player))
		{
			this.SetPeacekeepermode(true);
		}
	}

	// Token: 0x06000307 RID: 775 RVA: 0x00011478 File Offset: 0x0000F678
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void SERVER_AttackAll(global::BaseEntity.RPCMessage rpc)
	{
		if (this.IsAuthed(rpc.player))
		{
			this.SetPeacekeepermode(false);
		}
	}

	// Token: 0x06000308 RID: 776 RVA: 0x00011494 File Offset: 0x0000F694
	public override void ServerInit()
	{
		base.ServerInit();
		base.InvokeRepeating(new Action(this.ServerTick), Random.Range(0f, 1f), 0.015f);
		base.InvokeRandomized(new Action(this.SendAimDir), Random.Range(0f, 1f), 0.2f, 0.05f);
		base.InvokeRandomized(new Action(this.TargetScan), Random.Range(0f, 1f), 1f, 0.1f);
	}

	// Token: 0x06000309 RID: 777 RVA: 0x00011524 File Offset: 0x0000F724
	public void SendAimDir()
	{
		if (UnityEngine.Time.realtimeSinceStartup > this.nextForcedAimTime || this.HasTarget() || Vector3.Angle(this.lastSentAimDir, this.aimDir) > 0.03f)
		{
			this.lastSentAimDir = this.aimDir;
			base.ClientRPC<Vector3>(null, "CLIENT_ReceiveAimDir", this.aimDir);
			this.nextForcedAimTime = UnityEngine.Time.realtimeSinceStartup + 2f;
		}
	}

	// Token: 0x0600030A RID: 778 RVA: 0x00011598 File Offset: 0x0000F798
	public void SetTarget(global::BaseCombatEntity targ)
	{
		if (Interface.CallHook("OnTurretTarget", new object[]
		{
			this,
			targ
		}) != null)
		{
			return;
		}
		if (targ != this.target)
		{
			global::Effect.server.Run((!(targ == null)) ? this.targetAcquiredEffect.resourcePath : this.targetLostEffect.resourcePath, base.transform.position, Vector3.up, null, false);
		}
		this.target = targ;
	}

	// Token: 0x0600030B RID: 779 RVA: 0x0001161C File Offset: 0x0000F81C
	public bool ObjectVisible(global::BaseCombatEntity obj)
	{
		object obj2 = Interface.CallHook("CanBeTargeted", new object[]
		{
			obj,
			this
		});
		if (obj2 is bool)
		{
			return (bool)obj2;
		}
		List<RaycastHit> list = Facepunch.Pool.GetList<RaycastHit>();
		Vector3 position = this.eyePos.transform.position;
		Vector3 vector = this.AimOffset(obj);
		float num = Vector3.Distance(vector, position);
		Vector3 normalized = (vector - position).normalized;
		Vector3 vector2 = Vector3.Cross(normalized, Vector3.up);
		for (int i = 0; i < 3; i++)
		{
			Vector3 vector3 = vector + vector2 * global::AutoTurret.visibilityOffsets[i];
			Vector3 normalized2 = (vector3 - position).normalized;
			list.Clear();
			global::GamePhysics.TraceAll(new Ray(position, normalized2), 0f, list, num * 1.1f, 1084434689, 0);
			for (int j = 0; j < list.Count; j++)
			{
				RaycastHit hit = list[j];
				global::BaseEntity entity = hit.GetEntity();
				if (entity != null && (entity == obj || entity.EqualNetID(obj)))
				{
					Facepunch.Pool.FreeList<RaycastHit>(ref list);
					return true;
				}
				if (!(entity != null) || entity.ShouldBlockProjectiles())
				{
					break;
				}
			}
		}
		Facepunch.Pool.FreeList<RaycastHit>(ref list);
		return false;
	}

	// Token: 0x0600030C RID: 780 RVA: 0x00011794 File Offset: 0x0000F994
	public void FireGun(Vector3 targetPos, float aimCone)
	{
		if (this.IsOffline())
		{
			return;
		}
		Vector3 vector = this.muzzlePos.transform.position - this.muzzlePos.forward * 0.25f;
		Vector3 forward = this.muzzlePos.transform.forward;
		Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(aimCone, forward, true);
		targetPos = vector + modifiedAimConeDirection * 300f;
		List<RaycastHit> list = Facepunch.Pool.GetList<RaycastHit>();
		global::GamePhysics.TraceAll(new Ray(vector, modifiedAimConeDirection), 0f, list, 300f, 1084435201, 0);
		for (int i = 0; i < list.Count; i++)
		{
			RaycastHit hit = list[i];
			global::BaseEntity entity = hit.GetEntity();
			if (!(entity != null) || (!(entity == this) && !entity.EqualNetID(this)))
			{
				global::BaseCombatEntity baseCombatEntity = entity as global::BaseCombatEntity;
				if (baseCombatEntity != null)
				{
					this.ApplyDamage(baseCombatEntity, hit.point, modifiedAimConeDirection);
				}
				if (!(entity != null) || entity.ShouldBlockProjectiles())
				{
					targetPos = hit.point;
					break;
				}
			}
		}
		base.ClientRPC<Vector3>(null, "CLIENT_FireGun", targetPos);
		Facepunch.Pool.FreeList<RaycastHit>(ref list);
	}

	// Token: 0x0600030D RID: 781 RVA: 0x000118E8 File Offset: 0x0000FAE8
	private void ApplyDamage(global::BaseCombatEntity entity, Vector3 point, Vector3 normal)
	{
		float num = 15f * Random.Range(0.9f, 1.1f);
		if (entity is global::BasePlayer && entity != this.target)
		{
			num *= 0.5f;
		}
		if (this.PeacekeeperMode() && entity == this.target)
		{
			this.target.MarkHostileTime();
		}
		global::HitInfo info = new global::HitInfo(this, entity, Rust.DamageType.Bullet, num, point);
		entity.OnAttacked(info);
		if (entity is global::BasePlayer || entity is global::BaseNpc)
		{
			global::Effect.server.ImpactEffect(new global::HitInfo
			{
				HitPositionWorld = point,
				HitNormalWorld = -normal,
				HitMaterial = global::StringPool.Get("Flesh")
			});
		}
	}

	// Token: 0x0600030E RID: 782 RVA: 0x000119B0 File Offset: 0x0000FBB0
	public void IdleTick()
	{
		if (UnityEngine.Time.realtimeSinceStartup > this.nextIdleAimTime)
		{
			this.nextIdleAimTime = UnityEngine.Time.realtimeSinceStartup + Random.Range(4f, 5f);
			Quaternion quaternion = Quaternion.LookRotation(this.eyePos.forward, Vector3.up);
			quaternion *= Quaternion.AngleAxis(Random.Range(-45f, 45f), Vector3.up);
			this.targetAimDir = quaternion * Vector3.forward;
		}
		if (!this.HasTarget())
		{
			this.aimDir = Vector3.Lerp(this.aimDir, this.targetAimDir, UnityEngine.Time.deltaTime * 2f);
		}
	}

	// Token: 0x0600030F RID: 783 RVA: 0x00011A5C File Offset: 0x0000FC5C
	public bool HasAmmo()
	{
		return this.ammoItem != null && this.ammoItem.amount > 0 && this.ammoItem.parent == this.inventory;
	}

	// Token: 0x06000310 RID: 784 RVA: 0x00011A90 File Offset: 0x0000FC90
	public void Reload()
	{
		foreach (global::Item item in this.inventory.itemList)
		{
			if (item.info.itemid == this.ammoType.itemid && item.amount > 0)
			{
				this.ammoItem = item;
				break;
			}
		}
	}

	// Token: 0x06000311 RID: 785 RVA: 0x00011B20 File Offset: 0x0000FD20
	public void EnsureReloaded()
	{
		if (!this.HasAmmo())
		{
			this.Reload();
		}
	}

	// Token: 0x06000312 RID: 786 RVA: 0x00011B34 File Offset: 0x0000FD34
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		base.PlayerStoppedLooting(player);
		this.EnsureReloaded();
		this.nextShotTime = UnityEngine.Time.time;
	}

	// Token: 0x06000313 RID: 787 RVA: 0x00011B50 File Offset: 0x0000FD50
	public void TargetTick()
	{
		if (UnityEngine.Time.realtimeSinceStartup >= this.nextVisCheck)
		{
			this.nextVisCheck = UnityEngine.Time.realtimeSinceStartup + Random.Range(0.2f, 0.3f);
			this.targetVisible = this.ObjectVisible(this.target);
			if (this.targetVisible)
			{
				this.lastTargetSeenTime = UnityEngine.Time.realtimeSinceStartup;
			}
		}
		if (UnityEngine.Time.time >= this.nextShotTime && this.targetVisible && this.AngleToTarget(this.target) < 10f)
		{
			this.EnsureReloaded();
			if (this.HasAmmo())
			{
				this.FireGun(this.AimOffset(this.target), this.aimCone);
				this.nextShotTime = UnityEngine.Time.time + 0.115f;
				this.ammoItem.UseItem(1);
			}
			else
			{
				this.nextShotTime = UnityEngine.Time.time + 60f;
			}
		}
		if (this.target.IsDead() || UnityEngine.Time.realtimeSinceStartup - this.lastTargetSeenTime > 3f || Vector3.Distance(base.transform.position, this.target.transform.position) > this.sightRange || (this.PeacekeeperMode() && !this.IsEntityHostile(this.target)))
		{
			this.SetTarget(null);
		}
	}

	// Token: 0x06000314 RID: 788 RVA: 0x00011CB4 File Offset: 0x0000FEB4
	public bool HasTarget()
	{
		return this.target != null && this.target.IsAlive();
	}

	// Token: 0x06000315 RID: 789 RVA: 0x00011CD8 File Offset: 0x0000FED8
	public void OfflineTick()
	{
		this.aimDir = Vector3.up;
	}

	// Token: 0x06000316 RID: 790 RVA: 0x00011CE8 File Offset: 0x0000FEE8
	public bool IsEntityHostile(global::BaseCombatEntity ent)
	{
		int num = 0;
		if (ent is global::BasePlayer)
		{
			global::BasePlayer basePlayer = ent as global::BasePlayer;
			global::HeldEntity heldEntity = basePlayer.GetHeldEntity();
			if (heldEntity != null)
			{
				global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
				if (attackEntity != null && attackEntity.hostile)
				{
					num++;
				}
			}
		}
		float num2 = Vector3Ex.Distance2D(ent.GetHostilePos(), base.transform.position);
		if (num2 < this.sightRange && ent.TimeSinceHostile() < ConVar.Sentry.hostileduration)
		{
			num++;
		}
		return num > 0;
	}

	// Token: 0x06000317 RID: 791 RVA: 0x00011D7C File Offset: 0x0000FF7C
	public void TargetScan()
	{
		if (this.HasTarget() || this.IsOffline())
		{
			return;
		}
		List<global::BaseCombatEntity> list = Facepunch.Pool.GetList<global::BaseCombatEntity>();
		global::Vis.Entities<global::BaseCombatEntity>(this.eyePos.transform.position, this.sightRange, list, 133120, 2);
		for (int i = 0; i < list.Count; i++)
		{
			global::BaseCombatEntity baseCombatEntity = list[i];
			if (baseCombatEntity.IsAlive() && this.InFiringArc(baseCombatEntity) && this.ObjectVisible(baseCombatEntity))
			{
				if (!ConVar.Sentry.targetall)
				{
					global::BasePlayer basePlayer = baseCombatEntity as global::BasePlayer;
					if (basePlayer && this.IsAuthed(basePlayer))
					{
						goto IL_F8;
					}
				}
				if (!(baseCombatEntity is global::AutoTurret))
				{
					if (this.PeacekeeperMode())
					{
						if (!this.IsEntityHostile(baseCombatEntity))
						{
							goto IL_F8;
						}
						if (this.target == null)
						{
							this.nextShotTime = UnityEngine.Time.time + 1f;
						}
					}
					this.SetTarget(baseCombatEntity);
					break;
				}
			}
			IL_F8:;
		}
		Facepunch.Pool.FreeList<global::BaseCombatEntity>(ref list);
	}

	// Token: 0x06000318 RID: 792 RVA: 0x00011E98 File Offset: 0x00010098
	public void ServerTick()
	{
		if (base.isClient)
		{
			return;
		}
		if (base.IsDestroyed)
		{
			return;
		}
		if (!this.IsOnline())
		{
			this.OfflineTick();
		}
		else if (this.HasTarget())
		{
			this.TargetTick();
		}
		else
		{
			this.IdleTick();
		}
		this.UpdateFacingToTarget();
	}

	// Token: 0x06000319 RID: 793 RVA: 0x00011EF8 File Offset: 0x000100F8
	public override void OnAttacked(global::HitInfo info)
	{
		base.OnAttacked(info);
		if ((this.IsOnline() && !this.HasTarget()) || !this.targetVisible)
		{
			global::AutoTurret autoTurret = info.Initiator as global::AutoTurret;
			if (autoTurret != null)
			{
				return;
			}
			global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
			if (!basePlayer || !this.IsAuthed(basePlayer))
			{
				this.SetTarget(info.Initiator as global::BaseCombatEntity);
			}
		}
	}

	// Token: 0x0600031A RID: 794 RVA: 0x00011F7C File Offset: 0x0001017C
	public void UpdateFacingToTarget()
	{
		if (this.target != null && this.targetVisible)
		{
			Vector3 vector = this.AimOffset(this.target);
			Vector3 vector2 = this.gun_pitch.transform.InverseTransformPoint(this.muzzlePos.transform.position);
			vector2.z = (vector2.x = 0f);
			Vector3 vector3 = vector - (this.gun_pitch.position + vector2);
			this.aimDir = vector3;
		}
		this.UpdateAiming();
	}

	// Token: 0x04000030 RID: 48
	public global::GameObjectRef gun_fire_effect;

	// Token: 0x04000031 RID: 49
	public global::GameObjectRef bulletEffect;

	// Token: 0x04000032 RID: 50
	public float bulletSpeed = 200f;

	// Token: 0x04000033 RID: 51
	public global::BaseCombatEntity target;

	// Token: 0x04000034 RID: 52
	public Transform eyePos;

	// Token: 0x04000035 RID: 53
	public Transform muzzlePos;

	// Token: 0x04000036 RID: 54
	public Vector3 aimDir;

	// Token: 0x04000037 RID: 55
	public Transform gun_yaw;

	// Token: 0x04000038 RID: 56
	public Transform gun_pitch;

	// Token: 0x04000039 RID: 57
	public float sightRange = 30f;

	// Token: 0x0400003A RID: 58
	public global::SoundDefinition turnLoopDef;

	// Token: 0x0400003B RID: 59
	public global::SoundDefinition movementChangeDef;

	// Token: 0x0400003C RID: 60
	public global::SoundDefinition ambientLoopDef;

	// Token: 0x0400003D RID: 61
	public global::SoundDefinition focusCameraDef;

	// Token: 0x0400003E RID: 62
	public float focusSoundFreqMin = 2.5f;

	// Token: 0x0400003F RID: 63
	public float focusSoundFreqMax = 7f;

	// Token: 0x04000040 RID: 64
	public global::GameObjectRef peacekeeperToggleSound;

	// Token: 0x04000041 RID: 65
	public global::GameObjectRef onlineSound;

	// Token: 0x04000042 RID: 66
	public global::GameObjectRef offlineSound;

	// Token: 0x04000043 RID: 67
	public global::GameObjectRef targetAcquiredEffect;

	// Token: 0x04000044 RID: 68
	public global::GameObjectRef targetLostEffect;

	// Token: 0x04000045 RID: 69
	public float aimCone;

	// Token: 0x04000046 RID: 70
	public List<PlayerNameID> authorizedPlayers = new List<PlayerNameID>();

	// Token: 0x04000047 RID: 71
	public global::ItemDefinition ammoType;

	// Token: 0x04000048 RID: 72
	private float nextShotTime;

	// Token: 0x04000049 RID: 73
	private float nextVisCheck;

	// Token: 0x0400004A RID: 74
	private float lastTargetSeenTime;

	// Token: 0x0400004B RID: 75
	private bool targetVisible = true;

	// Token: 0x0400004C RID: 76
	private bool booting;

	// Token: 0x0400004D RID: 77
	private float nextIdleAimTime;

	// Token: 0x0400004E RID: 78
	private Vector3 targetAimDir = Vector3.forward;

	// Token: 0x0400004F RID: 79
	private global::Item ammoItem;

	// Token: 0x04000050 RID: 80
	private const float bulletDamage = 15f;

	// Token: 0x04000051 RID: 81
	private float nextForcedAimTime;

	// Token: 0x04000052 RID: 82
	private Vector3 lastSentAimDir = Vector3.zero;

	// Token: 0x04000053 RID: 83
	private static float[] visibilityOffsets = new float[]
	{
		0f,
		0.2f,
		-0.2f
	};

	// Token: 0x0200000B RID: 11
	public static class TurretFlags
	{
		// Token: 0x04000054 RID: 84
		public const global::BaseEntity.Flags Peacekeeper = global::BaseEntity.Flags.Reserved1;
	}
}
