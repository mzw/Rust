﻿using System;
using UnityEngine;

// Token: 0x02000555 RID: 1365
public class ApplyTerrainAnchors : MonoBehaviour
{
	// Token: 0x06001CA5 RID: 7333 RVA: 0x000A08CC File Offset: 0x0009EACC
	protected void Awake()
	{
		global::BaseEntity component = base.GetComponent<global::BaseEntity>();
		global::TerrainAnchor[] anchors = null;
		if (component.isServer)
		{
			anchors = global::PrefabAttribute.server.FindAll<global::TerrainAnchor>(component.prefabID);
		}
		base.transform.ApplyTerrainAnchors(anchors);
		global::GameManager.Destroy(this, 0f);
	}
}
