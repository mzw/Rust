﻿using System;
using UnityEngine;

// Token: 0x02000214 RID: 532
public class DecayPoint : global::PrefabAttribute
{
	// Token: 0x06000FA1 RID: 4001 RVA: 0x0005F9A0 File Offset: 0x0005DBA0
	public bool IsOccupied(global::BaseEntity entity)
	{
		return entity.IsOccupied(this.socket);
	}

	// Token: 0x06000FA2 RID: 4002 RVA: 0x0005F9B0 File Offset: 0x0005DBB0
	protected override Type GetIndexedType()
	{
		return typeof(global::DecayPoint);
	}

	// Token: 0x04000A70 RID: 2672
	[Tooltip("If this point is occupied this will take this % off the power of the decay")]
	public float protection = 0.25f;

	// Token: 0x04000A71 RID: 2673
	public global::Socket_Base socket;
}
