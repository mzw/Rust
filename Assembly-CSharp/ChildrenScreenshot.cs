﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;

// Token: 0x0200073E RID: 1854
public class ChildrenScreenshot : MonoBehaviour
{
	// Token: 0x060022D8 RID: 8920 RVA: 0x000C1B80 File Offset: 0x000BFD80
	[ContextMenu("Create Screenshots")]
	public void CreateScreenshots()
	{
		RenderTexture renderTexture = new RenderTexture(this.width, this.height, 0);
		GameObject gameObject = new GameObject();
		Camera camera = gameObject.AddComponent<Camera>();
		camera.targetTexture = renderTexture;
		camera.orthographic = false;
		camera.fieldOfView = this.fieldOfView;
		camera.nearClipPlane = 0.1f;
		camera.farClipPlane = 2000f;
		camera.cullingMask = LayerMask.GetMask(new string[]
		{
			"TransparentFX"
		});
		camera.clearFlags = 2;
		camera.backgroundColor = new Color(0f, 0f, 0f, 0f);
		camera.renderingPath = 3;
		Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, 5, false);
		foreach (Transform transform in base.transform.Cast<Transform>())
		{
			this.PositionCamera(camera, transform.gameObject);
			int layer = transform.gameObject.layer;
			transform.gameObject.SetLayerRecursive(1);
			camera.Render();
			transform.gameObject.SetLayerRecursive(layer);
			string text = transform.GetRecursiveName(string.Empty);
			text = text.Replace('/', '.');
			RenderTexture.active = renderTexture;
			texture2D.ReadPixels(new Rect(0f, 0f, (float)renderTexture.width, (float)renderTexture.height), 0, 0, false);
			RenderTexture.active = null;
			byte[] bytes = ImageConversion.EncodeToPNG(texture2D);
			string path = string.Format(this.folder, text, transform.name);
			string directoryName = Path.GetDirectoryName(path);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			File.WriteAllBytes(path, bytes);
		}
		Object.DestroyImmediate(texture2D, true);
		Object.DestroyImmediate(renderTexture, true);
		Object.DestroyImmediate(gameObject, true);
	}

	// Token: 0x060022D9 RID: 8921 RVA: 0x000C1D6C File Offset: 0x000BFF6C
	public void PositionCamera(Camera cam, GameObject obj)
	{
		Bounds bounds;
		bounds..ctor(obj.transform.position, Vector3.zero * 0.1f);
		bool flag = true;
		foreach (Renderer renderer in obj.GetComponentsInChildren<Renderer>())
		{
			if (flag)
			{
				bounds = renderer.bounds;
				flag = false;
			}
			else
			{
				bounds.Encapsulate(renderer.bounds);
			}
		}
		float num = bounds.size.magnitude * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * 0.0174532924f);
		cam.transform.position = bounds.center + obj.transform.TransformVector(this.offsetAngle.normalized) * num;
		cam.transform.LookAt(bounds.center);
	}

	// Token: 0x04001F44 RID: 8004
	public Vector3 offsetAngle = new Vector3(0f, 0f, 1f);

	// Token: 0x04001F45 RID: 8005
	public int width = 512;

	// Token: 0x04001F46 RID: 8006
	public int height = 512;

	// Token: 0x04001F47 RID: 8007
	public float fieldOfView = 70f;

	// Token: 0x04001F48 RID: 8008
	[Tooltip("0 = full recursive name, 1 = object name")]
	public string folder = "screenshots/{0}.png";
}
