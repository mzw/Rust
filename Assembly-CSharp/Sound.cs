﻿using System;
using UnityEngine;

// Token: 0x020001F3 RID: 499
public class Sound : MonoBehaviour, IClientComponent
{
	// Token: 0x1700010B RID: 267
	// (get) Token: 0x06000F55 RID: 3925 RVA: 0x0005DDD8 File Offset: 0x0005BFD8
	public global::SoundFade fade
	{
		get
		{
			return this._fade;
		}
	}

	// Token: 0x1700010C RID: 268
	// (get) Token: 0x06000F56 RID: 3926 RVA: 0x0005DDE0 File Offset: 0x0005BFE0
	public global::SoundModulation modulation
	{
		get
		{
			return this._modulation;
		}
	}

	// Token: 0x1700010D RID: 269
	// (get) Token: 0x06000F57 RID: 3927 RVA: 0x0005DDE8 File Offset: 0x0005BFE8
	public global::SoundOcclusion occlusion
	{
		get
		{
			return this._occlusion;
		}
	}

	// Token: 0x040009D8 RID: 2520
	public static float volumeExponent = Mathf.Log(Mathf.Sqrt(10f), 2f);

	// Token: 0x040009D9 RID: 2521
	public global::SoundDefinition definition;

	// Token: 0x040009DA RID: 2522
	public global::SoundModifier[] modifiers;

	// Token: 0x040009DB RID: 2523
	public global::SoundSource soundSource;

	// Token: 0x040009DC RID: 2524
	public AudioSource[] audioSources = new AudioSource[2];

	// Token: 0x040009DD RID: 2525
	[SerializeField]
	private global::SoundFade _fade;

	// Token: 0x040009DE RID: 2526
	[SerializeField]
	private global::SoundModulation _modulation;

	// Token: 0x040009DF RID: 2527
	[SerializeField]
	private global::SoundOcclusion _occlusion;
}
