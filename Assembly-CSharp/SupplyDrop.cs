﻿using System;
using UnityEngine;

// Token: 0x020003AE RID: 942
public class SupplyDrop : global::LootContainer
{
	// Token: 0x0600165E RID: 5726 RVA: 0x000817CC File Offset: 0x0007F9CC
	public override void ServerInit()
	{
		base.ServerInit();
		this.parachute = global::GameManager.server.CreateEntity(this.parachutePrefab.resourcePath, default(Vector3), default(Quaternion), true);
		if (this.parachute)
		{
			this.parachute.SetParent(this, "parachute_attach");
			this.parachute.Spawn();
		}
	}

	// Token: 0x0600165F RID: 5727 RVA: 0x0008183C File Offset: 0x0007FA3C
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		this.RemoveParachute();
	}

	// Token: 0x06001660 RID: 5728 RVA: 0x0008184C File Offset: 0x0007FA4C
	private void RemoveParachute()
	{
		if (this.parachute)
		{
			this.parachute.Kill(global::BaseNetworkable.DestroyMode.None);
			this.parachute = null;
		}
	}

	// Token: 0x06001661 RID: 5729 RVA: 0x00081874 File Offset: 0x0007FA74
	private void OnCollisionEnter(Collision collision)
	{
		this.RemoveParachute();
	}

	// Token: 0x040010D3 RID: 4307
	public global::GameObjectRef parachutePrefab;

	// Token: 0x040010D4 RID: 4308
	private global::BaseEntity parachute;
}
