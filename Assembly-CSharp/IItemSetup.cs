﻿using System;

// Token: 0x020004C2 RID: 1218
public interface IItemSetup
{
	// Token: 0x06001A56 RID: 6742
	void OnItemSetup(global::Item item);
}
