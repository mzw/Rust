﻿using System;
using UnityEngine;

// Token: 0x0200022A RID: 554
public class SocketMod_HotSpot : global::SocketMod
{
	// Token: 0x06000FF3 RID: 4083 RVA: 0x000611D8 File Offset: 0x0005F3D8
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = new Color(1f, 1f, 0f, 0.5f);
		Gizmos.DrawSphere(Vector3.zero, this.spotSize);
	}

	// Token: 0x06000FF4 RID: 4084 RVA: 0x00061218 File Offset: 0x0005F418
	public override void ModifyPlacement(global::Construction.Placement place)
	{
		Vector3 position = place.position + place.rotation * this.worldPosition;
		place.position = position;
	}

	// Token: 0x04000AAC RID: 2732
	public float spotSize = 0.1f;
}
