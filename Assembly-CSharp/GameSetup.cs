﻿using System;
using System.Collections;
using ConVar;
using Network;
using Rust;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000427 RID: 1063
public class GameSetup : MonoBehaviour
{
	// Token: 0x06001838 RID: 6200 RVA: 0x00089970 File Offset: 0x00087B70
	protected void Awake()
	{
		if (global::GameSetup.RunOnce)
		{
			global::GameManager.Destroy(base.gameObject, 0f);
			return;
		}
		global::GameManifest.Load();
		global::GameManifest.LoadAssets();
		global::GameSetup.RunOnce = true;
		if (global::Bootstrap.needsSetup)
		{
			global::Bootstrap.Init_Tier0();
			global::Bootstrap.Init_Systems();
			global::Bootstrap.Init_Config();
		}
		base.StartCoroutine(this.DoGameSetup());
	}

	// Token: 0x06001839 RID: 6201 RVA: 0x000899D0 File Offset: 0x00087BD0
	private IEnumerator DoGameSetup()
	{
		Application.isLoading = true;
		global::TerrainMeta.InitNoTerrain();
		global::ItemManager.Initialize();
		global::LevelManager.CurrentLevelName = SceneManager.GetActiveScene().name;
		if (this.loadLevel && !string.IsNullOrEmpty(this.loadLevelScene))
		{
			Network.Net.sv.Reset();
			ConVar.Server.level = this.loadLevelScene;
			global::LoadingScreen.Update("LOADING LEVEL " + this.loadLevelScene.ToUpper());
			Application.LoadLevelAdditive(this.loadLevelScene);
			global::LoadingScreen.Update(this.loadLevelScene.ToUpper() + " LOADED");
		}
		if (this.startServer)
		{
			yield return base.StartCoroutine(this.StartServer());
		}
		yield return null;
		Application.isLoading = false;
		yield break;
	}

	// Token: 0x0600183A RID: 6202 RVA: 0x000899EC File Offset: 0x00087BEC
	private IEnumerator StartServer()
	{
		ConVar.GC.collect();
		ConVar.GC.unload();
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return base.StartCoroutine(global::Bootstrap.StartServer(this.loadSave, this.loadSaveFile, true));
		yield break;
	}

	// Token: 0x040012C4 RID: 4804
	public static bool RunOnce;

	// Token: 0x040012C5 RID: 4805
	public bool startServer = true;

	// Token: 0x040012C6 RID: 4806
	public string clientConnectCommand = "client.connect 127.0.0.1:28015";

	// Token: 0x040012C7 RID: 4807
	public bool loadMenu = true;

	// Token: 0x040012C8 RID: 4808
	public bool loadLevel;

	// Token: 0x040012C9 RID: 4809
	public string loadLevelScene = string.Empty;

	// Token: 0x040012CA RID: 4810
	public bool loadSave;

	// Token: 0x040012CB RID: 4811
	public string loadSaveFile = string.Empty;
}
