﻿using System;
using UnityEngine;

// Token: 0x020000C3 RID: 195
public class rottest : MonoBehaviour
{
	// Token: 0x06000AA7 RID: 2727 RVA: 0x00048B38 File Offset: 0x00046D38
	private void Start()
	{
	}

	// Token: 0x06000AA8 RID: 2728 RVA: 0x00048B3C File Offset: 0x00046D3C
	private void Update()
	{
		this.aimDir = new Vector3(0f, 45f * Mathf.Sin(Time.time * 6f), 0f);
		this.UpdateAiming();
	}

	// Token: 0x06000AA9 RID: 2729 RVA: 0x00048B70 File Offset: 0x00046D70
	public void UpdateAiming()
	{
		if (this.aimDir == Vector3.zero)
		{
			return;
		}
		Quaternion quaternion = Quaternion.Euler(0f, this.aimDir.y, 0f);
		if (base.transform.localRotation != quaternion)
		{
			base.transform.localRotation = Quaternion.Lerp(base.transform.localRotation, quaternion, Time.deltaTime * 8f);
		}
	}

	// Token: 0x04000575 RID: 1397
	public Transform turretBase;

	// Token: 0x04000576 RID: 1398
	public Vector3 aimDir;
}
