﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

// Token: 0x0200079B RID: 1947
public static class ObjWriter
{
	// Token: 0x0600242E RID: 9262 RVA: 0x000C75F8 File Offset: 0x000C57F8
	public static string MeshToString(Mesh mesh)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("g ").Append(mesh.name).Append("\n");
		foreach (Vector3 vector in mesh.vertices)
		{
			stringBuilder.Append(string.Format("v {0} {1} {2}\n", -vector.x, vector.y, vector.z));
		}
		stringBuilder.Append("\n");
		foreach (Vector3 vector2 in mesh.normals)
		{
			stringBuilder.Append(string.Format("vn {0} {1} {2}\n", -vector2.x, vector2.y, vector2.z));
		}
		stringBuilder.Append("\n");
		Vector2[] uv = mesh.uv;
		for (int k = 0; k < uv.Length; k++)
		{
			Vector3 vector3 = uv[k];
			stringBuilder.Append(string.Format("vt {0} {1}\n", vector3.x, vector3.y));
		}
		stringBuilder.Append("\n");
		int[] triangles = mesh.triangles;
		for (int l = 0; l < triangles.Length; l += 3)
		{
			int num = triangles[l] + 1;
			int num2 = triangles[l + 1] + 1;
			int num3 = triangles[l + 2] + 1;
			stringBuilder.Append(string.Format("f {1}/{1}/{1} {0}/{0}/{0} {2}/{2}/{2}\n", num, num2, num3));
		}
		return stringBuilder.ToString();
	}

	// Token: 0x0600242F RID: 9263 RVA: 0x000C77E0 File Offset: 0x000C59E0
	public static void Write(Mesh mesh, string path)
	{
		using (StreamWriter streamWriter = new StreamWriter(path))
		{
			streamWriter.Write(global::ObjWriter.MeshToString(mesh));
		}
	}
}
