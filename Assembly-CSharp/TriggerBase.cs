﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Oxide.Core;
using Rust;
using UnityEngine;

// Token: 0x02000497 RID: 1175
public class TriggerBase : global::BaseMonoBehaviour
{
	// Token: 0x06001994 RID: 6548 RVA: 0x0009007C File Offset: 0x0008E27C
	internal virtual GameObject InterestedInObject(GameObject obj)
	{
		int num = 1 << obj.layer;
		if ((this.interestLayers.value & num) != num)
		{
			return null;
		}
		return obj;
	}

	// Token: 0x06001995 RID: 6549 RVA: 0x000900AC File Offset: 0x0008E2AC
	protected virtual void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (this.contents == null)
		{
			return;
		}
		foreach (GameObject targetObj in this.contents.ToArray<GameObject>())
		{
			this.OnTriggerExit(targetObj);
		}
		this.contents = null;
	}

	// Token: 0x06001996 RID: 6550 RVA: 0x00090104 File Offset: 0x0008E304
	internal virtual void OnEntityEnter(global::BaseEntity ent)
	{
		if (ent == null)
		{
			return;
		}
		if (this.entityContents == null)
		{
			this.entityContents = new HashSet<global::BaseEntity>();
		}
		Interface.CallHook("OnEntityEnter", new object[]
		{
			this,
			ent
		});
		this.entityContents.Add(ent);
	}

	// Token: 0x06001997 RID: 6551 RVA: 0x0009015C File Offset: 0x0008E35C
	internal virtual void OnEntityLeave(global::BaseEntity ent)
	{
		if (this.entityContents == null)
		{
			return;
		}
		Interface.CallHook("OnEntityLeave", new object[]
		{
			this,
			ent
		});
		this.entityContents.Remove(ent);
	}

	// Token: 0x06001998 RID: 6552 RVA: 0x000901A0 File Offset: 0x0008E3A0
	internal virtual void OnObjectAdded(GameObject obj)
	{
		if (obj == null)
		{
			return;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity)
		{
			this.OnEntityEnter(baseEntity);
			baseEntity.EnterTrigger(this);
		}
	}

	// Token: 0x06001999 RID: 6553 RVA: 0x000901DC File Offset: 0x0008E3DC
	internal virtual void OnObjectRemoved(GameObject obj)
	{
		if (obj == null)
		{
			return;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity)
		{
			this.OnEntityLeave(baseEntity);
			baseEntity.LeaveTrigger(this);
		}
	}

	// Token: 0x0600199A RID: 6554 RVA: 0x00090218 File Offset: 0x0008E418
	internal void RemoveInvalidEntities()
	{
		if (this.entityContents == null)
		{
			return;
		}
		Collider component = base.GetComponent<Collider>();
		if (component == null)
		{
			return;
		}
		Bounds bounds = component.bounds;
		bounds.Expand(1f);
		foreach (global::BaseEntity baseEntity in this.entityContents.ToArray<global::BaseEntity>())
		{
			if (baseEntity == null)
			{
				Debug.LogWarning("Trigger " + this.ToString() + " contains destroyed entity.");
			}
			else if (!bounds.Contains(baseEntity.ClosestPoint(base.transform.position)))
			{
				Debug.LogWarning("Trigger " + this.ToString() + " contains entity that is too far away: " + baseEntity.ToString());
				this.RemoveEntity(baseEntity);
			}
		}
	}

	// Token: 0x0600199B RID: 6555 RVA: 0x000902F0 File Offset: 0x0008E4F0
	internal bool CheckEntity(global::BaseEntity ent)
	{
		if (ent == null)
		{
			return true;
		}
		Collider component = base.GetComponent<Collider>();
		if (component == null)
		{
			return true;
		}
		Bounds bounds = component.bounds;
		bounds.Expand(1f);
		return bounds.Contains(ent.ClosestPoint(base.transform.position));
	}

	// Token: 0x0600199C RID: 6556 RVA: 0x0009034C File Offset: 0x0008E54C
	internal virtual void OnObjects()
	{
	}

	// Token: 0x0600199D RID: 6557 RVA: 0x00090350 File Offset: 0x0008E550
	internal virtual void OnEmpty()
	{
		this.contents = null;
		this.entityContents = null;
	}

	// Token: 0x0600199E RID: 6558 RVA: 0x00090360 File Offset: 0x0008E560
	public void RemoveObject(GameObject obj)
	{
		if (obj == null)
		{
			return;
		}
		Collider component = obj.GetComponent<Collider>();
		if (component == null)
		{
			return;
		}
		this.OnTriggerExit(component);
	}

	// Token: 0x0600199F RID: 6559 RVA: 0x00090398 File Offset: 0x0008E598
	public void RemoveEntity(global::BaseEntity obj)
	{
		this.OnTriggerExit(obj.gameObject);
	}

	// Token: 0x060019A0 RID: 6560 RVA: 0x000903A8 File Offset: 0x0008E5A8
	private void OnTriggerEnter(Collider collider)
	{
		if (this == null)
		{
			return;
		}
		using (TimeWarning.New("TriggerBase.OnTriggerEnter", 0.1f))
		{
			GameObject gameObject = this.InterestedInObject(collider.gameObject);
			if (gameObject == null)
			{
				return;
			}
			if (this.contents == null)
			{
				this.contents = new HashSet<GameObject>();
			}
			if (this.contents.Contains(gameObject))
			{
				return;
			}
			int count = this.contents.Count;
			this.contents.Add(gameObject);
			this.OnObjectAdded(gameObject);
			if (count == 0 && this.contents.Count == 1)
			{
				this.OnObjects();
			}
		}
		if (ConVar.Debugging.checktriggers)
		{
			this.RemoveInvalidEntities();
		}
	}

	// Token: 0x060019A1 RID: 6561 RVA: 0x0009048C File Offset: 0x0008E68C
	private void OnTriggerExit(Collider collider)
	{
		if (this == null)
		{
			return;
		}
		if (collider == null)
		{
			return;
		}
		GameObject gameObject = this.InterestedInObject(collider.gameObject);
		if (gameObject == null)
		{
			return;
		}
		this.OnTriggerExit(gameObject);
		if (ConVar.Debugging.checktriggers)
		{
			this.RemoveInvalidEntities();
		}
	}

	// Token: 0x060019A2 RID: 6562 RVA: 0x000904E4 File Offset: 0x0008E6E4
	private void OnTriggerExit(GameObject targetObj)
	{
		if (this.contents == null)
		{
			return;
		}
		if (!this.contents.Contains(targetObj))
		{
			return;
		}
		this.contents.Remove(targetObj);
		this.OnObjectRemoved(targetObj);
		if (this.contents == null || this.contents.Count == 0)
		{
			this.OnEmpty();
		}
	}

	// Token: 0x04001438 RID: 5176
	public LayerMask interestLayers;

	// Token: 0x04001439 RID: 5177
	[NonSerialized]
	public HashSet<GameObject> contents;

	// Token: 0x0400143A RID: 5178
	[NonSerialized]
	public HashSet<global::BaseEntity> entityContents;
}
