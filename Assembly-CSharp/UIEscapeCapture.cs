﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

// Token: 0x0200071B RID: 1819
public class UIEscapeCapture : ListComponent<global::UIEscapeCapture>
{
	// Token: 0x06002284 RID: 8836 RVA: 0x000C1078 File Offset: 0x000BF278
	public static bool EscapePressed()
	{
		using (IEnumerator<global::UIEscapeCapture> enumerator = ListComponent<global::UIEscapeCapture>.InstanceList.GetEnumerator())
		{
			if (enumerator.MoveNext())
			{
				global::UIEscapeCapture uiescapeCapture = enumerator.Current;
				uiescapeCapture.onEscape.Invoke();
				return true;
			}
		}
		return false;
	}

	// Token: 0x04001EFB RID: 7931
	public UnityEvent onEscape;
}
