﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ConVar;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x020001B2 RID: 434
public class MonumentNavMesh : FacepunchBehaviour, IServerComponent
{
	// Token: 0x170000D6 RID: 214
	// (get) Token: 0x06000E5B RID: 3675 RVA: 0x0005A8E4 File Offset: 0x00058AE4
	public bool IsBuilding
	{
		get
		{
			return !this.HasBuildOperationStarted || this.BuildingOperation != null;
		}
	}

	// Token: 0x06000E5C RID: 3676 RVA: 0x0005A900 File Offset: 0x00058B00
	private void OnEnable()
	{
		if (Rust.Ai.AiManager.nav_grid)
		{
			base.enabled = false;
			return;
		}
		this.agentTypeId = NavMesh.GetSettingsByIndex(this.NavMeshAgentTypeIndex).agentTypeID;
		this.NavMeshData = new NavMeshData(this.agentTypeId);
		this.sources = new List<NavMeshBuildSource>();
		this.terrainBakes = new List<global::AsyncTerrainNavMeshBake>();
		this.defaultArea = NavMesh.GetAreaFromName(this.DefaultAreaName);
		base.InvokeRepeating(new Action(this.FinishBuildingNavmesh), 0f, 1f);
	}

	// Token: 0x06000E5D RID: 3677 RVA: 0x0005A98C File Offset: 0x00058B8C
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (Rust.Ai.AiManager.nav_grid)
		{
			return;
		}
		base.CancelInvoke(new Action(this.FinishBuildingNavmesh));
		this.NavMeshDataInstance.Remove();
	}

	// Token: 0x06000E5E RID: 3678 RVA: 0x0005A9C4 File Offset: 0x00058BC4
	[ContextMenu("Update Monument Nav Mesh")]
	public void UpdateNavMeshAsync()
	{
		if (this.HasBuildOperationStarted)
		{
			return;
		}
		if (Rust.Ai.AiManager.nav_disable || !ConVar.AI.npc_enable)
		{
			return;
		}
		float realtimeSinceStartup = UnityEngine.Time.realtimeSinceStartup;
		Debug.Log("Starting Monument Navmesh Build with " + this.sources.Count + " sources");
		NavMeshBuildSettings settingsByIndex = NavMesh.GetSettingsByIndex(this.NavMeshAgentTypeIndex);
		settingsByIndex.overrideVoxelSize = true;
		settingsByIndex.voxelSize *= this.NavmeshResolutionModifier;
		this.BuildingOperation = NavMeshBuilder.UpdateNavMeshDataAsync(this.NavMeshData, settingsByIndex, this.sources, this.Bounds);
		this.BuildTimer.Reset();
		this.BuildTimer.Start();
		this.HasBuildOperationStarted = true;
		float num = UnityEngine.Time.realtimeSinceStartup - realtimeSinceStartup;
		if (num > 0.1f)
		{
			Debug.LogWarning("Calling UpdateNavMesh took " + num);
		}
	}

	// Token: 0x06000E5F RID: 3679 RVA: 0x0005AAA8 File Offset: 0x00058CA8
	private IEnumerator CollectSourcesAsync(Action callback)
	{
		float time = UnityEngine.Time.realtimeSinceStartup;
		Debug.Log("Starting Navmesh Source Collecting.");
		List<NavMeshBuildMarkup> markups = new List<NavMeshBuildMarkup>();
		NavMeshBuilder.CollectSources(this.Bounds, this.LayerMask, this.NavMeshCollectGeometry, this.defaultArea, markups, this.sources);
		if (global::TerrainMeta.HeightMap != null)
		{
			for (float x = -this.Bounds.extents.x; x < this.Bounds.extents.x; x += (float)this.CellSize)
			{
				for (float z = -this.Bounds.extents.z; z < this.Bounds.extents.z; z += (float)this.CellSize)
				{
					global::AsyncTerrainNavMeshBake terrainSource = new global::AsyncTerrainNavMeshBake(this.Bounds.center + new Vector3(x, 0f, z), this.CellSize, this.Height, false, true);
					yield return terrainSource;
					this.terrainBakes.Add(terrainSource);
					NavMeshBuildSource mesh = terrainSource.CreateNavMeshBuildSource(true);
					mesh.area = this.defaultArea;
					this.sources.Add(mesh);
				}
			}
		}
		this.AppendModifierVolumes(ref this.sources);
		float timeTaken = UnityEngine.Time.realtimeSinceStartup - time;
		if (timeTaken > 0.1f)
		{
			Debug.LogWarning("Calling CollectSourcesAsync took " + timeTaken);
		}
		if (callback != null)
		{
			callback();
		}
		yield break;
	}

	// Token: 0x06000E60 RID: 3680 RVA: 0x0005AACC File Offset: 0x00058CCC
	public IEnumerator UpdateNavMeshAndWait()
	{
		if (this.HasBuildOperationStarted)
		{
			yield break;
		}
		if (Rust.Ai.AiManager.nav_disable || !ConVar.AI.npc_enable)
		{
			yield break;
		}
		this.HasBuildOperationStarted = false;
		this.Bounds.center = base.transform.position;
		this.Bounds.size = new Vector3((float)(this.CellSize * this.CellCount), (float)this.Height, (float)(this.CellSize * this.CellCount));
		if (Rust.Ai.AiManager.nav_wait)
		{
			yield return this.CollectSourcesAsync(new Action(this.UpdateNavMeshAsync));
		}
		else
		{
			base.StartCoroutine(this.CollectSourcesAsync(new Action(this.UpdateNavMeshAsync)));
		}
		if (!Rust.Ai.AiManager.nav_wait)
		{
			Debug.Log("nav_wait is false, so we're not waiting for the navmesh to finish generating. This might cause your server to sputter while it's generating.");
			yield break;
		}
		int lastPct = 0;
		while (!this.HasBuildOperationStarted)
		{
			Thread.Sleep(250);
			yield return null;
		}
		while (this.BuildingOperation != null)
		{
			int pctDone = (int)(this.BuildingOperation.progress * 100f);
			if (lastPct != pctDone)
			{
				Debug.LogFormat("{0}%", new object[]
				{
					pctDone
				});
				lastPct = pctDone;
			}
			Thread.Sleep(250);
			this.FinishBuildingNavmesh();
			yield return null;
		}
		yield break;
	}

	// Token: 0x06000E61 RID: 3681 RVA: 0x0005AAE8 File Offset: 0x00058CE8
	private void AppendModifierVolumes(ref List<NavMeshBuildSource> sources)
	{
		List<NavMeshModifierVolume> activeModifiers = NavMeshModifierVolume.activeModifiers;
		foreach (NavMeshModifierVolume navMeshModifierVolume in activeModifiers)
		{
			if ((this.LayerMask & 1 << navMeshModifierVolume.gameObject.layer) != 0)
			{
				if (navMeshModifierVolume.AffectsAgentType(this.agentTypeId))
				{
					Vector3 vector = navMeshModifierVolume.transform.TransformPoint(navMeshModifierVolume.center);
					if (this.Bounds.Contains(vector))
					{
						Vector3 lossyScale = navMeshModifierVolume.transform.lossyScale;
						Vector3 size;
						size..ctor(navMeshModifierVolume.size.x * Mathf.Abs(lossyScale.x), navMeshModifierVolume.size.y * Mathf.Abs(lossyScale.y), navMeshModifierVolume.size.z * Mathf.Abs(lossyScale.z));
						NavMeshBuildSource item = default(NavMeshBuildSource);
						item.shape = 5;
						item.transform = Matrix4x4.TRS(vector, navMeshModifierVolume.transform.rotation, Vector3.one);
						item.size = size;
						item.area = navMeshModifierVolume.area;
						sources.Add(item);
					}
				}
			}
		}
	}

	// Token: 0x06000E62 RID: 3682 RVA: 0x0005AC64 File Offset: 0x00058E64
	public void FinishBuildingNavmesh()
	{
		if (this.BuildingOperation == null)
		{
			return;
		}
		if (!this.BuildingOperation.isDone)
		{
			return;
		}
		if (!this.NavMeshDataInstance.valid)
		{
			this.NavMeshDataInstance = NavMesh.AddNavMeshData(this.NavMeshData);
		}
		Debug.Log(string.Format("Monument Navmesh Build took {0:0.00} seconds", this.BuildTimer.Elapsed.TotalSeconds));
		this.BuildingOperation = null;
	}

	// Token: 0x06000E63 RID: 3683 RVA: 0x0005ACE0 File Offset: 0x00058EE0
	public void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.magenta * new Color(1f, 1f, 1f, 0.5f);
		Gizmos.DrawCube(base.transform.position, this.Bounds.size);
	}

	// Token: 0x04000867 RID: 2151
	public int NavMeshAgentTypeIndex;

	// Token: 0x04000868 RID: 2152
	[Tooltip("The default area associated with the NavMeshAgent index.")]
	public string DefaultAreaName = "HumanNPC";

	// Token: 0x04000869 RID: 2153
	public int CellCount = 1;

	// Token: 0x0400086A RID: 2154
	public int CellSize = 80;

	// Token: 0x0400086B RID: 2155
	public int Height = 100;

	// Token: 0x0400086C RID: 2156
	public float NavmeshResolutionModifier = 0.5f;

	// Token: 0x0400086D RID: 2157
	public Bounds Bounds;

	// Token: 0x0400086E RID: 2158
	public NavMeshData NavMeshData;

	// Token: 0x0400086F RID: 2159
	public NavMeshDataInstance NavMeshDataInstance;

	// Token: 0x04000870 RID: 2160
	public LayerMask LayerMask;

	// Token: 0x04000871 RID: 2161
	public NavMeshCollectGeometry NavMeshCollectGeometry;

	// Token: 0x04000872 RID: 2162
	private List<global::AsyncTerrainNavMeshBake> terrainBakes;

	// Token: 0x04000873 RID: 2163
	private List<NavMeshBuildSource> sources;

	// Token: 0x04000874 RID: 2164
	private AsyncOperation BuildingOperation;

	// Token: 0x04000875 RID: 2165
	private bool HasBuildOperationStarted;

	// Token: 0x04000876 RID: 2166
	private Stopwatch BuildTimer = new Stopwatch();

	// Token: 0x04000877 RID: 2167
	private int defaultArea;

	// Token: 0x04000878 RID: 2168
	private int agentTypeId;
}
