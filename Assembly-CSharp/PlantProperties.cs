﻿using System;
using UnityEngine;

// Token: 0x020003BA RID: 954
[CreateAssetMenu(menuName = "Rust/Plant Properties")]
public class PlantProperties : ScriptableObject
{
	// Token: 0x04001105 RID: 4357
	[ArrayIndexIsEnum(enumType = typeof(global::PlantProperties.State))]
	public global::PlantProperties.Stage[] stages = new global::PlantProperties.Stage[6];

	// Token: 0x04001106 RID: 4358
	[Header("Metabolism")]
	public AnimationCurve timeOfDayHappiness = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 0f),
		new Keyframe(12f, 1f),
		new Keyframe(24f, 0f)
	});

	// Token: 0x04001107 RID: 4359
	public AnimationCurve temperatureHappiness = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(-10f, -1f),
		new Keyframe(1f, 0f),
		new Keyframe(30f, 1f),
		new Keyframe(50f, 0f),
		new Keyframe(80f, -1f)
	});

	// Token: 0x04001108 RID: 4360
	public AnimationCurve fruitCurve = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 0f),
		new Keyframe(0.75f, 1f),
		new Keyframe(1f, 0f)
	});

	// Token: 0x04001109 RID: 4361
	public int maxSeasons = 1;

	// Token: 0x0400110A RID: 4362
	public int maxHeldWater = 1000;

	// Token: 0x0400110B RID: 4363
	public int lifetimeWaterConsumption = 5000;

	// Token: 0x0400110C RID: 4364
	public float waterConsumptionLifetime = 60f;

	// Token: 0x0400110D RID: 4365
	public int waterYieldBonus = 1;

	// Token: 0x0400110E RID: 4366
	[Header("Harvesting")]
	public global::BaseEntity.Menu.Option pickOption;

	// Token: 0x0400110F RID: 4367
	public global::ItemDefinition pickupItem;

	// Token: 0x04001110 RID: 4368
	public int pickupAmount = 1;

	// Token: 0x04001111 RID: 4369
	public global::GameObjectRef pickEffect;

	// Token: 0x04001112 RID: 4370
	public int maxHarvests = 1;

	// Token: 0x04001113 RID: 4371
	public bool disappearAfterHarvest;

	// Token: 0x04001114 RID: 4372
	[Header("Cloning")]
	public global::BaseEntity.Menu.Option cloneOption;

	// Token: 0x04001115 RID: 4373
	public global::ItemDefinition cloneItem;

	// Token: 0x04001116 RID: 4374
	public int maxClones = 1;

	// Token: 0x020003BB RID: 955
	public enum State
	{
		// Token: 0x04001118 RID: 4376
		Seed,
		// Token: 0x04001119 RID: 4377
		Seedling,
		// Token: 0x0400111A RID: 4378
		Sapling,
		// Token: 0x0400111B RID: 4379
		Mature,
		// Token: 0x0400111C RID: 4380
		Fruiting,
		// Token: 0x0400111D RID: 4381
		Dying
	}

	// Token: 0x020003BC RID: 956
	[Serializable]
	public struct Stage
	{
		// Token: 0x0400111E RID: 4382
		public global::PlantProperties.State nextState;

		// Token: 0x0400111F RID: 4383
		public float lifeLength;

		// Token: 0x04001120 RID: 4384
		public float health;

		// Token: 0x04001121 RID: 4385
		public float resources;

		// Token: 0x04001122 RID: 4386
		public global::GameObjectRef skinObject;
	}
}
