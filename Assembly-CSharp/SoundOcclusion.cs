﻿using System;
using UnityEngine;

// Token: 0x020001FC RID: 508
[RequireComponent(typeof(global::OnePoleLowpassFilter))]
public class SoundOcclusion : MonoBehaviour
{
	// Token: 0x04000A08 RID: 2568
	public LayerMask occlusionLayerMask;
}
