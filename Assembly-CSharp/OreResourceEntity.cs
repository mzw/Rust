﻿using System;
using Network;
using UnityEngine;

// Token: 0x02000080 RID: 128
public class OreResourceEntity : global::StagedResourceEntity
{
	// Token: 0x06000885 RID: 2181 RVA: 0x00037AE4 File Offset: 0x00035CE4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("OreResourceEntity.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000886 RID: 2182 RVA: 0x00037B2C File Offset: 0x00035D2C
	protected override void UpdateNetworkStage()
	{
		int stage = this.stage;
		base.UpdateNetworkStage();
		int stage2 = this.stage;
		if (stage2 != stage && this._hotSpot)
		{
			this.DelayedBonusSpawn();
		}
	}

	// Token: 0x06000887 RID: 2183 RVA: 0x00037B6C File Offset: 0x00035D6C
	public void CleanupBonus()
	{
		if (this._hotSpot)
		{
			this._hotSpot.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		this._hotSpot = null;
	}

	// Token: 0x06000888 RID: 2184 RVA: 0x00037B94 File Offset: 0x00035D94
	public override void OnKilled(global::HitInfo info)
	{
		this.CleanupBonus();
		base.OnKilled(info);
	}

	// Token: 0x06000889 RID: 2185 RVA: 0x00037BA4 File Offset: 0x00035DA4
	public override void ServerInit()
	{
		base.ServerInit();
		this._hotSpot = this.SpawnBonusSpot(Vector3.zero);
	}

	// Token: 0x0600088A RID: 2186 RVA: 0x00037BC0 File Offset: 0x00035DC0
	public void FinishBonusAssigned()
	{
		global::Effect.server.Run(this.finishEffect.resourcePath, base.GetEstimatedWorldPosition(), base.transform.up, null, false);
	}

	// Token: 0x0600088B RID: 2187 RVA: 0x00037BE8 File Offset: 0x00035DE8
	public override void OnAttacked(global::HitInfo info)
	{
		if (base.isClient)
		{
			base.OnAttacked(info);
			return;
		}
		if (!info.DidGather && info.gatherScale > 0f && this._hotSpot)
		{
			float num = Vector3.Distance(info.HitPositionWorld, this._hotSpot.transform.position);
			if (num <= this._hotSpot.GetComponent<SphereCollider>().radius * 1.5f)
			{
				this.bonusesKilled++;
				info.gatherScale = 1f + Mathf.Clamp((float)this.bonusesKilled * 0.5f, 0f, 2f);
				this._hotSpot.FireFinishEffect();
				base.ClientRPC<int, Vector3>(null, "PlayBonusLevelSound", this.bonusesKilled, this._hotSpot.transform.position);
			}
			else if (this.bonusesKilled > 0)
			{
				this.bonusesKilled = 0;
				global::Effect.server.Run(this.bonusFailEffect.resourcePath, base.GetEstimatedWorldPosition(), base.transform.up, null, false);
			}
			if (this.bonusesKilled > 0)
			{
				this.CleanupBonus();
			}
		}
		if (this._hotSpot == null)
		{
			this.DelayedBonusSpawn();
		}
		base.OnAttacked(info);
	}

	// Token: 0x0600088C RID: 2188 RVA: 0x00037D3C File Offset: 0x00035F3C
	public void DelayedBonusSpawn()
	{
		base.CancelInvoke(new Action(this.RespawnBonus));
		base.Invoke(new Action(this.RespawnBonus), 0.25f);
	}

	// Token: 0x0600088D RID: 2189 RVA: 0x00037D68 File Offset: 0x00035F68
	public void RespawnBonus()
	{
		this.CleanupBonus();
		this._hotSpot = this.SpawnBonusSpot(this.lastNodeDir);
	}

	// Token: 0x0600088E RID: 2190 RVA: 0x00037D84 File Offset: 0x00035F84
	public global::OreHotSpot SpawnBonusSpot(Vector3 lastDirection)
	{
		if (base.isClient)
		{
			return null;
		}
		if (!this.bonusPrefab.isValid)
		{
			return null;
		}
		Vector2 normalized = Random.insideUnitCircle.normalized;
		Vector3 vector = Vector3.zero;
		MeshCollider stageComponent = base.GetStageComponent<MeshCollider>();
		Vector3 vector2 = base.transform.InverseTransformPoint(stageComponent.bounds.center);
		if (lastDirection == Vector3.zero)
		{
			Vector3 vector3 = this.RandomCircle(1f, false);
			this.lastNodeDir = vector3.normalized;
			Vector3 vector4 = base.transform.TransformDirection(vector3.normalized);
			vector3 = base.transform.position + base.transform.up * (vector2.y + 0.5f) + vector4.normalized * 2.5f;
			vector = vector3;
		}
		else
		{
			Vector3 vector5 = Vector3.Cross(this.lastNodeDir, Vector3.up);
			float num = Random.Range(0.25f, 0.5f);
			float num2 = (Random.Range(0, 2) != 0) ? 1f : -1f;
			Vector3 normalized2 = (this.lastNodeDir + vector5 * num * num2).normalized;
			this.lastNodeDir = normalized2;
			vector = base.transform.position + base.transform.TransformDirection(normalized2) * 2f;
			float num3 = Random.Range(1f, 1.5f);
			vector += base.transform.up * (vector2.y + num3);
		}
		this.bonusesSpawned++;
		Vector3 normalized3 = (stageComponent.bounds.center - vector).normalized;
		RaycastHit raycastHit;
		if (stageComponent.Raycast(new Ray(vector, normalized3), ref raycastHit, 10f))
		{
			global::OreHotSpot oreHotSpot = global::GameManager.server.CreateEntity(this.bonusPrefab.resourcePath, raycastHit.point - normalized3 * 0.025f, Quaternion.LookRotation(raycastHit.normal, Vector3.up), true) as global::OreHotSpot;
			oreHotSpot.Spawn();
			oreHotSpot.SendMessage("OreOwner", this);
			return oreHotSpot;
		}
		return null;
	}

	// Token: 0x0600088F RID: 2191 RVA: 0x00037FE4 File Offset: 0x000361E4
	public Vector3 RandomCircle(float distance = 1f, bool allowInside = false)
	{
		Vector2 vector = (!allowInside) ? Random.insideUnitCircle.normalized : Random.insideUnitCircle;
		return new Vector3(vector.x, 0f, vector.y);
	}

	// Token: 0x06000890 RID: 2192 RVA: 0x00038028 File Offset: 0x00036228
	public Vector3 RandomHemisphereDirection(Vector3 input, float degreesOffset, bool allowInside = true, bool changeHeight = true)
	{
		degreesOffset = Mathf.Clamp(degreesOffset / 180f, -180f, 180f);
		Vector2 vector = (!allowInside) ? Random.insideUnitCircle.normalized : Random.insideUnitCircle;
		Vector3 vector2;
		vector2..ctor(vector.x * degreesOffset, (!changeHeight) ? 0f : (Random.Range(-1f, 1f) * degreesOffset), vector.y * degreesOffset);
		return (input + vector2).normalized;
	}

	// Token: 0x06000891 RID: 2193 RVA: 0x000380B8 File Offset: 0x000362B8
	public Vector3 ClampToHemisphere(Vector3 hemiInput, float degreesOffset, Vector3 inputVec)
	{
		degreesOffset = Mathf.Clamp(degreesOffset / 180f, -180f, 180f);
		Vector3 normalized = (hemiInput + Vector3.one * degreesOffset).normalized;
		Vector3 normalized2 = (hemiInput + Vector3.one * -degreesOffset).normalized;
		for (int i = 0; i < 3; i++)
		{
			inputVec[i] = Mathf.Clamp(inputVec[i], normalized2[i], normalized[i]);
		}
		return inputVec;
	}

	// Token: 0x06000892 RID: 2194 RVA: 0x00038154 File Offset: 0x00036354
	public static Vector3 RandomCylinderPointAroundVector(Vector3 input, float distance, float minHeight = 0f, float maxHeight = 0f, bool allowInside = false)
	{
		Vector2 vector = (!allowInside) ? Random.insideUnitCircle.normalized : Random.insideUnitCircle;
		Vector3 vector2;
		vector2..ctor(vector.x, 0f, vector.y);
		Vector3 result = vector2.normalized * distance;
		result.y = Random.Range(minHeight, maxHeight);
		return result;
	}

	// Token: 0x06000893 RID: 2195 RVA: 0x000381B8 File Offset: 0x000363B8
	public Vector3 ClampToCylinder(Vector3 localPos, Vector3 cylinderAxis, float cylinderDistance, float minHeight = 0f, float maxHeight = 0f)
	{
		return Vector3.zero;
	}

	// Token: 0x040003FA RID: 1018
	public global::GameObjectRef bonusPrefab;

	// Token: 0x040003FB RID: 1019
	public global::GameObjectRef finishEffect;

	// Token: 0x040003FC RID: 1020
	public global::GameObjectRef bonusFailEffect;

	// Token: 0x040003FD RID: 1021
	public global::OreHotSpot _hotSpot;

	// Token: 0x040003FE RID: 1022
	private int bonusesKilled;

	// Token: 0x040003FF RID: 1023
	private int bonusesSpawned;

	// Token: 0x04000400 RID: 1024
	private Vector3 lastNodeDir = Vector3.zero;
}
