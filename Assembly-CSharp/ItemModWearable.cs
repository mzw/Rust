﻿using System;
using Rust;
using UnityEngine;

// Token: 0x020004F9 RID: 1273
public class ItemModWearable : global::ItemMod
{
	// Token: 0x170001D8 RID: 472
	// (get) Token: 0x06001B0E RID: 6926 RVA: 0x000977BC File Offset: 0x000959BC
	public global::Wearable targetWearable
	{
		get
		{
			if (!this.entityPrefab.isValid)
			{
				return null;
			}
			return this.entityPrefab.Get().GetComponent<global::Wearable>();
		}
	}

	// Token: 0x06001B0F RID: 6927 RVA: 0x000977E0 File Offset: 0x000959E0
	private void DoPrepare()
	{
		if (!this.entityPrefab.isValid)
		{
			Debug.LogWarning("ItemModWearable: entityPrefab is null! " + base.gameObject, base.gameObject);
		}
		if (this.entityPrefab.isValid && this.targetWearable == null)
		{
			Debug.LogWarning("ItemModWearable: entityPrefab doesn't have a Wearable component! " + base.gameObject, this.entityPrefab.Get());
		}
	}

	// Token: 0x06001B10 RID: 6928 RVA: 0x0009785C File Offset: 0x00095A5C
	public override void ModInit()
	{
		string resourcePath = this.entityPrefab.resourcePath;
		if (string.IsNullOrEmpty(resourcePath))
		{
			Debug.LogWarning(this + " - entityPrefab is null or something.. - " + this.entityPrefab.guid);
		}
	}

	// Token: 0x06001B11 RID: 6929 RVA: 0x0009789C File Offset: 0x00095A9C
	public bool ProtectsArea(global::HitArea area)
	{
		return !(this.armorProperties == null) && this.armorProperties.Contains(area);
	}

	// Token: 0x06001B12 RID: 6930 RVA: 0x000978C0 File Offset: 0x00095AC0
	public bool HasProtections()
	{
		return this.protectionProperties != null;
	}

	// Token: 0x06001B13 RID: 6931 RVA: 0x000978D0 File Offset: 0x00095AD0
	internal float GetProtection(global::Item item, Rust.DamageType damageType)
	{
		if (this.protectionProperties == null)
		{
			return 0f;
		}
		return this.protectionProperties.Get(damageType) * this.ConditionProtectionScale(item);
	}

	// Token: 0x06001B14 RID: 6932 RVA: 0x00097900 File Offset: 0x00095B00
	public float ConditionProtectionScale(global::Item item)
	{
		return (!item.isBroken) ? 1f : 0.25f;
	}

	// Token: 0x06001B15 RID: 6933 RVA: 0x0009791C File Offset: 0x00095B1C
	public void CollectProtection(global::Item item, global::ProtectionProperties protection)
	{
		if (this.protectionProperties == null)
		{
			return;
		}
		protection.Add(this.protectionProperties, this.ConditionProtectionScale(item));
	}

	// Token: 0x06001B16 RID: 6934 RVA: 0x00097944 File Offset: 0x00095B44
	private bool IsHeadgear()
	{
		global::Wearable component = this.entityPrefab.Get().GetComponent<global::Wearable>();
		return component != null && (component.occupationOver & (global::Wearable.OccupationSlots.HeadTop | global::Wearable.OccupationSlots.Face | global::Wearable.OccupationSlots.HeadBack)) != (global::Wearable.OccupationSlots)0;
	}

	// Token: 0x06001B17 RID: 6935 RVA: 0x00097980 File Offset: 0x00095B80
	public bool IsFootwear()
	{
		global::Wearable component = this.entityPrefab.Get().GetComponent<global::Wearable>();
		return component != null && (component.occupationOver & (global::Wearable.OccupationSlots.LeftFoot | global::Wearable.OccupationSlots.RightFoot)) != (global::Wearable.OccupationSlots)0;
	}

	// Token: 0x06001B18 RID: 6936 RVA: 0x000979C0 File Offset: 0x00095BC0
	public override void OnAttacked(global::Item item, global::HitInfo info)
	{
		if (!item.hasCondition)
		{
			return;
		}
		float num = 0f;
		for (int i = 0; i < 22; i++)
		{
			Rust.DamageType damageType = (Rust.DamageType)i;
			if (info.damageTypes.Has(damageType))
			{
				num += Mathf.Clamp(info.damageTypes.types[i] * this.GetProtection(item, damageType), 0f, item.condition);
				if (num >= item.condition)
				{
					break;
				}
			}
		}
		item.LoseCondition(num);
		if (item != null && item.isBroken && item.GetOwnerPlayer() && this.IsHeadgear() && info.damageTypes.Total() >= item.GetOwnerPlayer().health)
		{
			global::BaseEntity baseEntity = item.Drop(item.GetOwnerPlayer().transform.position + new Vector3(0f, 1.8f, 0f), Vector3.up * 3f, default(Quaternion));
			baseEntity.SetAngularVelocity(Random.rotation.eulerAngles * 5f);
		}
	}

	// Token: 0x06001B19 RID: 6937 RVA: 0x00097AF4 File Offset: 0x00095CF4
	public bool CanExistWith(global::ItemModWearable wearable)
	{
		if (wearable == null)
		{
			return true;
		}
		global::Wearable targetWearable = this.targetWearable;
		global::Wearable targetWearable2 = wearable.targetWearable;
		return (targetWearable.occupationOver & targetWearable2.occupationOver) == (global::Wearable.OccupationSlots)0 && (targetWearable.occupationUnder & targetWearable2.occupationUnder) == (global::Wearable.OccupationSlots)0;
	}

	// Token: 0x040015E4 RID: 5604
	public global::GameObjectRef entityPrefab = new global::GameObjectRef();

	// Token: 0x040015E5 RID: 5605
	public global::ProtectionProperties protectionProperties;

	// Token: 0x040015E6 RID: 5606
	public global::ArmorProperties armorProperties;

	// Token: 0x040015E7 RID: 5607
	public global::ClothingMovementProperties movementProperties;

	// Token: 0x040015E8 RID: 5608
	public global::UIBlackoutOverlay.blackoutType occlusionType = global::UIBlackoutOverlay.blackoutType.NONE;

	// Token: 0x040015E9 RID: 5609
	public bool blocksAiming;

	// Token: 0x040015EA RID: 5610
	public bool emissive;
}
