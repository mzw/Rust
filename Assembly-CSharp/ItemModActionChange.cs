﻿using System;
using UnityEngine;

// Token: 0x020004D5 RID: 1237
public class ItemModActionChange : global::ItemMod
{
	// Token: 0x06001AAE RID: 6830 RVA: 0x00095908 File Offset: 0x00093B08
	public override void OnChanged(global::Item item)
	{
		if (!item.isServer)
		{
			return;
		}
		global::BasePlayer ownerPlayer = item.GetOwnerPlayer();
		foreach (global::ItemMod itemMod in this.actions)
		{
			if (itemMod.CanDoAction(item, ownerPlayer))
			{
				itemMod.DoAction(item, ownerPlayer);
			}
		}
	}

	// Token: 0x06001AAF RID: 6831 RVA: 0x00095964 File Offset: 0x00093B64
	private void OnValidate()
	{
		if (this.actions == null)
		{
			Debug.LogWarning("ItemModMenuOption: actions is null!", base.gameObject);
		}
	}

	// Token: 0x04001573 RID: 5491
	public global::ItemMod[] actions;
}
