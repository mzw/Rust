﻿using System;
using System.Collections.Generic;
using ConVar;
using Rust;
using UnityEngine;

// Token: 0x02000279 RID: 633
public class Projectile : MonoBehaviour
{
	// Token: 0x060010B3 RID: 4275 RVA: 0x000647B0 File Offset: 0x000629B0
	public void CalculateDamage(global::HitInfo info, global::Projectile.Modifier mod, float scale)
	{
		float num = this.damageMultipliers.Lerp(mod.distanceOffset + mod.distanceScale * this.damageDistances.x, mod.distanceOffset + mod.distanceScale * this.damageDistances.y, info.ProjectileDistance);
		float num2 = scale * (mod.damageOffset + mod.damageScale * num);
		foreach (Rust.DamageTypeEntry damageTypeEntry in this.damageTypes)
		{
			info.damageTypes.Add(damageTypeEntry.type, damageTypeEntry.amount * num2);
		}
		if (ConVar.Global.developer > 0)
		{
			Debug.Log(string.Concat(new object[]
			{
				" Projectile damage: ",
				info.damageTypes.Total(),
				" (scalar=",
				num2,
				")"
			}));
		}
	}

	// Token: 0x04000BA0 RID: 2976
	public const float lifeTime = 8f;

	// Token: 0x04000BA1 RID: 2977
	[Header("Attributes")]
	public Vector3 initialVelocity;

	// Token: 0x04000BA2 RID: 2978
	public float drag;

	// Token: 0x04000BA3 RID: 2979
	public float gravityModifier = 1f;

	// Token: 0x04000BA4 RID: 2980
	public float thickness;

	// Token: 0x04000BA5 RID: 2981
	[Tooltip("This projectile will raycast for this many units, and then become a projectile. This is typically done for bullets.")]
	public float initialDistance;

	// Token: 0x04000BA6 RID: 2982
	[Header("Impact Rules")]
	public bool remainInWorld;

	// Token: 0x04000BA7 RID: 2983
	[Range(0f, 1f)]
	public float stickProbability = 1f;

	// Token: 0x04000BA8 RID: 2984
	[Range(0f, 1f)]
	public float breakProbability;

	// Token: 0x04000BA9 RID: 2985
	[Range(0f, 1f)]
	public float conditionLoss;

	// Token: 0x04000BAA RID: 2986
	[Range(0f, 1f)]
	public float ricochetChance;

	// Token: 0x04000BAB RID: 2987
	public float penetrationPower = 1f;

	// Token: 0x04000BAC RID: 2988
	[Header("Damage")]
	public global::DamageProperties damageProperties;

	// Token: 0x04000BAD RID: 2989
	[Horizontal(2, -1)]
	public global::MinMax damageDistances = new global::MinMax(10f, 100f);

	// Token: 0x04000BAE RID: 2990
	[Horizontal(2, -1)]
	public global::MinMax damageMultipliers = new global::MinMax(1f, 0.8f);

	// Token: 0x04000BAF RID: 2991
	public List<Rust.DamageTypeEntry> damageTypes = new List<Rust.DamageTypeEntry>();

	// Token: 0x04000BB0 RID: 2992
	[Header("Rendering")]
	public global::ScaleRenderer rendererToScale;

	// Token: 0x04000BB1 RID: 2993
	public global::ScaleRenderer firstPersonRenderer;

	// Token: 0x04000BB2 RID: 2994
	public bool createDecals = true;

	// Token: 0x04000BB3 RID: 2995
	[Header("Audio")]
	public global::SoundDefinition flybySound;

	// Token: 0x04000BB4 RID: 2996
	public float flybySoundDistance = 7f;

	// Token: 0x04000BB5 RID: 2997
	public global::SoundDefinition closeFlybySound;

	// Token: 0x04000BB6 RID: 2998
	public float closeFlybyDistance = 3f;

	// Token: 0x04000BB7 RID: 2999
	[Header("Tumble")]
	public float tumbleSpeed;

	// Token: 0x04000BB8 RID: 3000
	public Vector3 tumbleAxis = Vector3.right;

	// Token: 0x04000BB9 RID: 3001
	[NonSerialized]
	public global::BasePlayer owner;

	// Token: 0x04000BBA RID: 3002
	[NonSerialized]
	public global::AttackEntity sourceWeaponPrefab;

	// Token: 0x04000BBB RID: 3003
	[NonSerialized]
	public global::Projectile sourceProjectilePrefab;

	// Token: 0x04000BBC RID: 3004
	[NonSerialized]
	public global::ItemModProjectile mod;

	// Token: 0x04000BBD RID: 3005
	[NonSerialized]
	public int projectileID;

	// Token: 0x04000BBE RID: 3006
	[NonSerialized]
	public int seed;

	// Token: 0x04000BBF RID: 3007
	[NonSerialized]
	public bool clientsideEffect;

	// Token: 0x04000BC0 RID: 3008
	[NonSerialized]
	public bool clientsideAttack;

	// Token: 0x04000BC1 RID: 3009
	[NonSerialized]
	public float integrity = 1f;

	// Token: 0x04000BC2 RID: 3010
	[NonSerialized]
	public float maxDistance = float.PositiveInfinity;

	// Token: 0x04000BC3 RID: 3011
	[NonSerialized]
	public global::Projectile.Modifier modifier = global::Projectile.Modifier.Default;

	// Token: 0x04000BC4 RID: 3012
	[NonSerialized]
	public bool invisible;

	// Token: 0x0200027A RID: 634
	public struct Modifier
	{
		// Token: 0x04000BC5 RID: 3013
		public float damageScale;

		// Token: 0x04000BC6 RID: 3014
		public float damageOffset;

		// Token: 0x04000BC7 RID: 3015
		public float distanceScale;

		// Token: 0x04000BC8 RID: 3016
		public float distanceOffset;

		// Token: 0x04000BC9 RID: 3017
		public static global::Projectile.Modifier Default = new global::Projectile.Modifier
		{
			damageScale = 1f,
			damageOffset = 0f,
			distanceScale = 1f,
			distanceOffset = 0f
		};
	}
}
