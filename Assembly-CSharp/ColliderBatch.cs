﻿using System;
using ConVar;
using Rust;
using UnityEngine;

// Token: 0x0200028E RID: 654
public class ColliderBatch : MonoBehaviour, IServerComponent
{
	// Token: 0x17000124 RID: 292
	// (get) Token: 0x060010D8 RID: 4312 RVA: 0x00065068 File Offset: 0x00063268
	// (set) Token: 0x060010D9 RID: 4313 RVA: 0x00065070 File Offset: 0x00063270
	public Transform BatchTransform { get; set; }

	// Token: 0x17000125 RID: 293
	// (get) Token: 0x060010DA RID: 4314 RVA: 0x0006507C File Offset: 0x0006327C
	// (set) Token: 0x060010DB RID: 4315 RVA: 0x00065084 File Offset: 0x00063284
	public MeshCollider BatchCollider { get; set; }

	// Token: 0x17000126 RID: 294
	// (get) Token: 0x060010DC RID: 4316 RVA: 0x00065090 File Offset: 0x00063290
	// (set) Token: 0x060010DD RID: 4317 RVA: 0x00065098 File Offset: 0x00063298
	public Rigidbody BatchRigidbody { get; set; }

	// Token: 0x060010DE RID: 4318 RVA: 0x000650A4 File Offset: 0x000632A4
	protected void OnEnable()
	{
		this.BatchTransform = base.transform;
		this.BatchCollider = base.GetComponent<MeshCollider>();
		this.BatchRigidbody = base.GetComponent<Rigidbody>();
		this.Add();
	}

	// Token: 0x060010DF RID: 4319 RVA: 0x000650D0 File Offset: 0x000632D0
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.Remove();
	}

	// Token: 0x060010E0 RID: 4320 RVA: 0x000650E4 File Offset: 0x000632E4
	public void Add()
	{
		if (this.batchGroup != null)
		{
			this.Remove();
		}
		if (!ConVar.Batching.colliders)
		{
			return;
		}
		if (this.BatchTransform == null)
		{
			return;
		}
		if (this.BatchCollider == null)
		{
			return;
		}
		if (this.BatchCollider.sharedMesh.subMeshCount > ConVar.Batching.collider_submeshes)
		{
			return;
		}
		if (this.BatchCollider.sharedMesh.vertexCount > ConVar.Batching.collider_vertices)
		{
			return;
		}
		global::ColliderCell colliderCell = SingletonComponent<global::ColliderGrid>.Instance[this.BatchTransform.position];
		this.batchGroup = colliderCell.FindBatchGroup(this);
		this.batchGroup.Add(this);
		this.batchInstance.mesh = this.BatchCollider.sharedMesh;
		this.batchInstance.position = this.BatchTransform.position;
		this.batchInstance.rotation = this.BatchTransform.rotation;
		this.batchInstance.scale = this.BatchTransform.lossyScale;
		this.batchInstance.transform = this.BatchTransform;
		this.batchInstance.rigidbody = this.BatchRigidbody;
		this.batchInstance.collider = this.BatchCollider;
		this.batchInstance.bounds = new OBB(this.BatchTransform, this.BatchCollider.sharedMesh.bounds);
		if (Application.isLoading)
		{
			this.BatchCollider.enabled = false;
		}
	}

	// Token: 0x060010E1 RID: 4321 RVA: 0x00065260 File Offset: 0x00063460
	public void Remove()
	{
		if (this.batchGroup == null)
		{
			return;
		}
		this.batchGroup.Invalidate();
		this.batchGroup.Remove(this);
		this.batchGroup = null;
	}

	// Token: 0x060010E2 RID: 4322 RVA: 0x0006528C File Offset: 0x0006348C
	public void Refresh()
	{
		this.Remove();
		this.Add();
	}

	// Token: 0x060010E3 RID: 4323 RVA: 0x0006529C File Offset: 0x0006349C
	public void AddBatch(global::ColliderGroup batchGroup)
	{
		batchGroup.Add(this.batchInstance);
	}

	// Token: 0x04000BFB RID: 3067
	private global::ColliderGroup batchGroup;

	// Token: 0x04000BFC RID: 3068
	private global::MeshColliderInstance batchInstance;
}
