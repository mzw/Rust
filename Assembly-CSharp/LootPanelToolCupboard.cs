﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020000AE RID: 174
public class LootPanelToolCupboard : global::LootPanel
{
	// Token: 0x040004F6 RID: 1270
	public List<global::VirtualItemIcon> costIcons;

	// Token: 0x040004F7 RID: 1271
	public Text costPerTimeText;

	// Token: 0x040004F8 RID: 1272
	public Text protectedText;

	// Token: 0x040004F9 RID: 1273
	public GameObject baseNotProtectedObj;

	// Token: 0x040004FA RID: 1274
	public GameObject baseProtectedObj;

	// Token: 0x040004FB RID: 1275
	public global::Translate.Phrase protectedPrefix;

	// Token: 0x040004FC RID: 1276
	public global::Tooltip costToolTip;

	// Token: 0x040004FD RID: 1277
	public global::Translate.Phrase blocksPhrase;
}
