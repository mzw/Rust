﻿using System;
using UnityEngine;

// Token: 0x0200075B RID: 1883
public class Screenshot : MonoBehaviour
{
	// Token: 0x04001F93 RID: 8083
	public string screenshotPath;

	// Token: 0x04001F94 RID: 8084
	public int sizeMultiplier = 4;
}
