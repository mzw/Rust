﻿using System;
using UnityEngine;

// Token: 0x020000CD RID: 205
public class PathSpeedZone : MonoBehaviour
{
	// Token: 0x06000AD5 RID: 2773 RVA: 0x000496F4 File Offset: 0x000478F4
	public OBB WorldSpaceBounds()
	{
		return new OBB(base.transform.position, base.transform.lossyScale, base.transform.rotation, this.bounds);
	}

	// Token: 0x06000AD6 RID: 2774 RVA: 0x00049724 File Offset: 0x00047924
	public float GetMaxSpeed()
	{
		return this.maxVelocityPerSec;
	}

	// Token: 0x06000AD7 RID: 2775 RVA: 0x0004972C File Offset: 0x0004792C
	public virtual void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = new Color(1f, 0.5f, 0f, 0.5f);
		Gizmos.DrawCube(this.bounds.center, this.bounds.size);
		Gizmos.color = new Color(1f, 0.7f, 0f, 1f);
		Gizmos.DrawWireCube(this.bounds.center, this.bounds.size);
	}

	// Token: 0x0400059A RID: 1434
	public Bounds bounds;

	// Token: 0x0400059B RID: 1435
	public OBB obbBounds;

	// Token: 0x0400059C RID: 1436
	public float maxVelocityPerSec = 5f;
}
