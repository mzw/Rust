﻿using System;

// Token: 0x02000642 RID: 1602
[Serializable]
public enum HairType
{
	// Token: 0x04001B45 RID: 6981
	Head,
	// Token: 0x04001B46 RID: 6982
	Eyebrow,
	// Token: 0x04001B47 RID: 6983
	Facial,
	// Token: 0x04001B48 RID: 6984
	Armpit,
	// Token: 0x04001B49 RID: 6985
	Pubic,
	// Token: 0x04001B4A RID: 6986
	Count
}
