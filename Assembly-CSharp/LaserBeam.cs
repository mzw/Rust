﻿using System;
using UnityEngine;

// Token: 0x020000D8 RID: 216
public class LaserBeam : MonoBehaviour
{
	// Token: 0x040005B7 RID: 1463
	public float scrollSpeed = 0.5f;

	// Token: 0x040005B8 RID: 1464
	public LineRenderer beamRenderer;

	// Token: 0x040005B9 RID: 1465
	public GameObject dotObject;

	// Token: 0x040005BA RID: 1466
	public Renderer dotRenderer;

	// Token: 0x040005BB RID: 1467
	public GameObject dotSpotlight;

	// Token: 0x040005BC RID: 1468
	public Vector2 scrollDir;

	// Token: 0x040005BD RID: 1469
	public float maxDistance = 100f;

	// Token: 0x040005BE RID: 1470
	public float stillBlendFactor = 0.1f;

	// Token: 0x040005BF RID: 1471
	public float movementBlendFactor = 0.5f;

	// Token: 0x040005C0 RID: 1472
	public float movementThreshhold = 0.15f;

	// Token: 0x040005C1 RID: 1473
	public bool isFirstPerson;

	// Token: 0x040005C2 RID: 1474
	public Transform emissionOverride;

	// Token: 0x040005C3 RID: 1475
	private MaterialPropertyBlock block;
}
