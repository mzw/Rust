﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000691 RID: 1681
public class MonumentMarker : MonoBehaviour
{
	// Token: 0x06002111 RID: 8465 RVA: 0x000BB62C File Offset: 0x000B982C
	public void Setup(global::MonumentInfo info)
	{
		string translated = info.displayPhrase.translated;
		this.text.text = ((!string.IsNullOrEmpty(translated)) ? translated : "Monument");
	}

	// Token: 0x04001C75 RID: 7285
	public Text text;
}
