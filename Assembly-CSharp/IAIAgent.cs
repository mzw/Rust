﻿using System;
using System.Collections.Generic;
using Apex.AI;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x020001C2 RID: 450
public interface IAIAgent
{
	// Token: 0x170000DD RID: 221
	// (get) Token: 0x06000E94 RID: 3732
	global::BaseNpc.AiStatistics GetStats { get; }

	// Token: 0x170000DE RID: 222
	// (get) Token: 0x06000E95 RID: 3733
	NavMeshAgent GetNavAgent { get; }

	// Token: 0x06000E96 RID: 3734
	IAIContext GetContext(Guid aiId);

	// Token: 0x170000DF RID: 223
	// (get) Token: 0x06000E97 RID: 3735
	// (set) Token: 0x06000E98 RID: 3736
	Vector2i CurrentCoord { get; set; }

	// Token: 0x170000E0 RID: 224
	// (get) Token: 0x06000E99 RID: 3737
	// (set) Token: 0x06000E9A RID: 3738
	Vector2i PreviousCoord { get; set; }

	// Token: 0x170000E1 RID: 225
	// (get) Token: 0x06000E9B RID: 3739
	// (set) Token: 0x06000E9C RID: 3740
	bool AgencyUpdateRequired { get; set; }

	// Token: 0x170000E2 RID: 226
	// (get) Token: 0x06000E9D RID: 3741
	// (set) Token: 0x06000E9E RID: 3742
	bool IsOnOffmeshLinkAndReachedNewCoord { get; set; }

	// Token: 0x170000E3 RID: 227
	// (get) Token: 0x06000E9F RID: 3743
	// (set) Token: 0x06000EA0 RID: 3744
	Vector3 Destination { get; set; }

	// Token: 0x170000E4 RID: 228
	// (get) Token: 0x06000EA1 RID: 3745
	// (set) Token: 0x06000EA2 RID: 3746
	bool IsStopped { get; set; }

	// Token: 0x170000E5 RID: 229
	// (get) Token: 0x06000EA3 RID: 3747
	// (set) Token: 0x06000EA4 RID: 3748
	bool AutoBraking { get; set; }

	// Token: 0x170000E6 RID: 230
	// (get) Token: 0x06000EA5 RID: 3749
	bool HasPath { get; }

	// Token: 0x06000EA6 RID: 3750
	bool IsNavRunning();

	// Token: 0x06000EA7 RID: 3751
	void Pause();

	// Token: 0x06000EA8 RID: 3752
	void Resume();

	// Token: 0x06000EA9 RID: 3753
	void UpdateDestination(Vector3 newDestination);

	// Token: 0x06000EAA RID: 3754
	void UpdateDestination(Transform tx);

	// Token: 0x06000EAB RID: 3755
	void StopMoving();

	// Token: 0x170000E7 RID: 231
	// (get) Token: 0x06000EAC RID: 3756
	float TimeAtDestination { get; }

	// Token: 0x170000E8 RID: 232
	// (get) Token: 0x06000EAD RID: 3757
	bool IsStuck { get; }

	// Token: 0x170000E9 RID: 233
	// (get) Token: 0x06000EAE RID: 3758
	// (set) Token: 0x06000EAF RID: 3759
	float TargetSpeed { get; set; }

	// Token: 0x06000EB0 RID: 3760
	bool WantsToEat(global::BaseEntity eatable);

	// Token: 0x170000EA RID: 234
	// (get) Token: 0x06000EB1 RID: 3761
	// (set) Token: 0x06000EB2 RID: 3762
	global::BaseEntity FoodTarget { get; set; }

	// Token: 0x06000EB3 RID: 3763
	void Eat();

	// Token: 0x170000EB RID: 235
	// (get) Token: 0x06000EB4 RID: 3764
	float GetAttackRate { get; }

	// Token: 0x170000EC RID: 236
	// (get) Token: 0x06000EB5 RID: 3765
	float GetAttackRange { get; }

	// Token: 0x170000ED RID: 237
	// (get) Token: 0x06000EB6 RID: 3766
	Vector3 GetAttackOffset { get; }

	// Token: 0x06000EB7 RID: 3767
	void StartAttack();

	// Token: 0x06000EB8 RID: 3768
	void StartAttack(Rust.Ai.AttackOperator.AttackType type, global::BaseCombatEntity target);

	// Token: 0x06000EB9 RID: 3769
	bool AttackReady();

	// Token: 0x170000EE RID: 238
	// (get) Token: 0x06000EBA RID: 3770
	// (set) Token: 0x06000EBB RID: 3771
	global::BaseEntity AttackTarget { get; set; }

	// Token: 0x170000EF RID: 239
	// (get) Token: 0x06000EBC RID: 3772
	float AttackTargetVisibleFor { get; }

	// Token: 0x170000F0 RID: 240
	// (get) Token: 0x06000EBD RID: 3773
	// (set) Token: 0x06000EBE RID: 3774
	Rust.Ai.Memory.SeenInfo AttackTargetMemory { get; set; }

	// Token: 0x170000F1 RID: 241
	// (get) Token: 0x06000EBF RID: 3775
	global::BaseCombatEntity CombatTarget { get; }

	// Token: 0x170000F2 RID: 242
	// (get) Token: 0x06000EC0 RID: 3776
	Vector3 AttackPosition { get; }

	// Token: 0x170000F3 RID: 243
	// (get) Token: 0x06000EC1 RID: 3777
	Vector3 CrouchedAttackPosition { get; }

	// Token: 0x170000F4 RID: 244
	// (get) Token: 0x06000EC2 RID: 3778
	Vector3 CurrentAimAngles { get; }

	// Token: 0x06000EC3 RID: 3779
	float GetWantsToAttack(global::BaseEntity target);

	// Token: 0x06000EC4 RID: 3780
	float FearLevel(global::BaseEntity ent);

	// Token: 0x170000F5 RID: 245
	// (get) Token: 0x06000EC5 RID: 3781
	// (set) Token: 0x06000EC6 RID: 3782
	Vector3 SpawnPosition { get; set; }

	// Token: 0x170000F6 RID: 246
	// (get) Token: 0x06000EC7 RID: 3783
	global::BaseCombatEntity Entity { get; }

	// Token: 0x170000F7 RID: 247
	// (get) Token: 0x06000EC8 RID: 3784
	float GetAttackCost { get; }

	// Token: 0x170000F8 RID: 248
	// (get) Token: 0x06000EC9 RID: 3785
	float GetStamina { get; }

	// Token: 0x170000F9 RID: 249
	// (get) Token: 0x06000ECA RID: 3786
	float GetEnergy { get; }

	// Token: 0x170000FA RID: 250
	// (get) Token: 0x06000ECB RID: 3787
	float GetSleep { get; }

	// Token: 0x06000ECC RID: 3788
	bool BusyTimerActive();

	// Token: 0x06000ECD RID: 3789
	void SetBusyFor(float dur);

	// Token: 0x170000FB RID: 251
	// (get) Token: 0x06000ECE RID: 3790
	float GetStuckDuration { get; }

	// Token: 0x170000FC RID: 252
	// (get) Token: 0x06000ECF RID: 3791
	float GetLastStuckTime { get; }

	// Token: 0x170000FD RID: 253
	// (get) Token: 0x06000ED0 RID: 3792
	float currentBehaviorDuration { get; }

	// Token: 0x170000FE RID: 254
	// (get) Token: 0x06000ED1 RID: 3793
	// (set) Token: 0x06000ED2 RID: 3794
	global::BaseNpc.Behaviour CurrentBehaviour { get; set; }

	// Token: 0x170000FF RID: 255
	// (get) Token: 0x06000ED3 RID: 3795
	// (set) Token: 0x06000ED4 RID: 3796
	int AgentTypeIndex { get; set; }

	// Token: 0x06000ED5 RID: 3797
	byte GetFact(global::BaseNpc.Facts fact);

	// Token: 0x06000ED6 RID: 3798
	void SetFact(global::BaseNpc.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true);

	// Token: 0x06000ED7 RID: 3799
	float ToSpeed(global::BaseNpc.SpeedEnum speed);

	// Token: 0x06000ED8 RID: 3800
	List<Rust.Ai.NavPointSample> RequestNavPointSamplesInCircle(Rust.Ai.NavPointSampler.SampleCount sampleCount, float radius, Rust.Ai.NavPointSampler.SampleFeatures features = Rust.Ai.NavPointSampler.SampleFeatures.None);

	// Token: 0x06000ED9 RID: 3801
	byte GetFact(global::NPCPlayerApex.Facts fact);

	// Token: 0x06000EDA RID: 3802
	void SetFact(global::NPCPlayerApex.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true);

	// Token: 0x06000EDB RID: 3803
	float ToSpeed(global::NPCPlayerApex.SpeedEnum speed);
}
