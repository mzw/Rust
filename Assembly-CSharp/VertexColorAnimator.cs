﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200081F RID: 2079
public class VertexColorAnimator : MonoBehaviour
{
	// Token: 0x06002699 RID: 9881 RVA: 0x000D6508 File Offset: 0x000D4708
	public void initLists()
	{
		this.animationMeshes = new List<global::MeshHolder>();
		this.animationKeyframes = new List<float>();
	}

	// Token: 0x0600269A RID: 9882 RVA: 0x000D6520 File Offset: 0x000D4720
	public void addMesh(Mesh mesh, float atPosition)
	{
		global::MeshHolder meshHolder = new global::MeshHolder();
		meshHolder.setAnimationData(mesh);
		this.animationMeshes.Add(meshHolder);
		this.animationKeyframes.Add(atPosition);
	}

	// Token: 0x0600269B RID: 9883 RVA: 0x000D6554 File Offset: 0x000D4754
	private void Start()
	{
		this.elapsedTime = 0f;
	}

	// Token: 0x0600269C RID: 9884 RVA: 0x000D6564 File Offset: 0x000D4764
	public void replaceKeyframe(int frameIndex, Mesh mesh)
	{
		this.animationMeshes[frameIndex].setAnimationData(mesh);
	}

	// Token: 0x0600269D RID: 9885 RVA: 0x000D6578 File Offset: 0x000D4778
	public void deleteKeyframe(int frameIndex)
	{
		this.animationMeshes.RemoveAt(frameIndex);
		this.animationKeyframes.RemoveAt(frameIndex);
	}

	// Token: 0x0600269E RID: 9886 RVA: 0x000D6594 File Offset: 0x000D4794
	public void scrobble(float scrobblePos)
	{
		if (this.animationMeshes.Count == 0)
		{
			return;
		}
		Color[] array = new Color[base.GetComponent<MeshFilter>().sharedMesh.colors.Length];
		int num = 0;
		for (int i = 0; i < this.animationKeyframes.Count; i++)
		{
			if (scrobblePos >= this.animationKeyframes[i])
			{
				num = i;
			}
		}
		if (num >= this.animationKeyframes.Count - 1)
		{
			base.GetComponent<global::VertexColorStream>().setColors(this.animationMeshes[num]._colors);
			return;
		}
		float num2 = this.animationKeyframes[num + 1] - this.animationKeyframes[num];
		float num3 = this.animationKeyframes[num];
		float num4 = (scrobblePos - num3) / num2;
		for (int j = 0; j < array.Length; j++)
		{
			array[j] = Color.Lerp(this.animationMeshes[num]._colors[j], this.animationMeshes[num + 1]._colors[j], num4);
		}
		base.GetComponent<global::VertexColorStream>().setColors(array);
	}

	// Token: 0x0600269F RID: 9887 RVA: 0x000D66D4 File Offset: 0x000D48D4
	private void Update()
	{
		if (this.mode == 0)
		{
			this.elapsedTime += Time.fixedDeltaTime / this.timeScale;
		}
		else if (this.mode == 1)
		{
			this.elapsedTime += Time.fixedDeltaTime / this.timeScale;
			if (this.elapsedTime > 1f)
			{
				this.elapsedTime = 0f;
			}
		}
		else if (this.mode == 2)
		{
			if (Mathf.FloorToInt(Time.fixedTime / this.timeScale) % 2 == 0)
			{
				this.elapsedTime += Time.fixedDeltaTime / this.timeScale;
			}
			else
			{
				this.elapsedTime -= Time.fixedDeltaTime / this.timeScale;
			}
		}
		Color[] array = new Color[base.GetComponent<MeshFilter>().sharedMesh.colors.Length];
		int num = 0;
		for (int i = 0; i < this.animationKeyframes.Count; i++)
		{
			if (this.elapsedTime >= this.animationKeyframes[i])
			{
				num = i;
			}
		}
		if (num < this.animationKeyframes.Count - 1)
		{
			float num2 = this.animationKeyframes[num + 1] - this.animationKeyframes[num];
			float num3 = this.animationKeyframes[num];
			float num4 = (this.elapsedTime - num3) / num2;
			for (int j = 0; j < array.Length; j++)
			{
				array[j] = Color.Lerp(this.animationMeshes[num]._colors[j], this.animationMeshes[num + 1]._colors[j], num4);
			}
		}
		else
		{
			array = this.animationMeshes[num]._colors;
		}
		base.GetComponent<global::VertexColorStream>().setColors(array);
	}

	// Token: 0x0400230F RID: 8975
	public List<global::MeshHolder> animationMeshes;

	// Token: 0x04002310 RID: 8976
	public List<float> animationKeyframes;

	// Token: 0x04002311 RID: 8977
	public float timeScale = 2f;

	// Token: 0x04002312 RID: 8978
	public int mode;

	// Token: 0x04002313 RID: 8979
	private float elapsedTime;
}
