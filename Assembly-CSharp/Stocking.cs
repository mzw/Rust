﻿using System;
using Rust;
using UnityEngine;

// Token: 0x020000BE RID: 190
public class Stocking : global::LootContainer
{
	// Token: 0x06000A98 RID: 2712 RVA: 0x000486DC File Offset: 0x000468DC
	public override void ServerInit()
	{
		base.ServerInit();
		if (global::Stocking.stockings == null)
		{
			global::Stocking.stockings = new ListHashSet<global::Stocking>(8);
		}
		global::Stocking.stockings.Add(this);
	}

	// Token: 0x06000A99 RID: 2713 RVA: 0x00048704 File Offset: 0x00046904
	internal override void DoServerDestroy()
	{
		global::Stocking.stockings.Remove(this);
		base.DoServerDestroy();
	}

	// Token: 0x06000A9A RID: 2714 RVA: 0x00048718 File Offset: 0x00046918
	public bool IsEmpty()
	{
		if (this.inventory == null)
		{
			return false;
		}
		for (int i = this.inventory.itemList.Count - 1; i >= 0; i--)
		{
			global::Item item = this.inventory.itemList[i];
			if (item != null)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000A9B RID: 2715 RVA: 0x00048770 File Offset: 0x00046970
	public override void SpawnLoot()
	{
		if (this.inventory == null)
		{
			Debug.Log("CONTACT DEVELOPERS! Stocking::PopulateLoot has null inventory!!! " + base.name);
			return;
		}
		bool flag = this.IsEmpty();
		if (flag)
		{
			base.SpawnLoot();
			base.SetFlag(global::BaseEntity.Flags.On, true, false);
			this.Hurt(this.MaxHealth() * 0.1f, Rust.DamageType.Generic, null, false);
		}
	}

	// Token: 0x06000A9C RID: 2716 RVA: 0x000487D4 File Offset: 0x000469D4
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		base.PlayerStoppedLooting(player);
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		if (this.IsEmpty() && base.healthFraction <= 0.1f)
		{
			this.Hurt(base.health, Rust.DamageType.Generic, this, false);
		}
	}

	// Token: 0x0400055A RID: 1370
	public static ListHashSet<global::Stocking> stockings;
}
