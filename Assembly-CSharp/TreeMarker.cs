﻿using System;
using Network;
using UnityEngine;

// Token: 0x02000002 RID: 2
public class TreeMarker : global::BaseEntity
{
	// Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("TreeMarker.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x04000001 RID: 1
	public global::GameObjectRef hitEffect;

	// Token: 0x04000002 RID: 2
	public global::SoundDefinition hitEffectSound;

	// Token: 0x04000003 RID: 3
	public global::GameObjectRef spawnEffect;

	// Token: 0x04000004 RID: 4
	public global::DeferredDecal myDecal;

	// Token: 0x04000005 RID: 5
	private Vector3 initialPosition;
}
