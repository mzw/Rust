﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x0200032B RID: 811
public class PhysicsEffects : MonoBehaviour
{
	// Token: 0x060013A3 RID: 5027 RVA: 0x0007367C File Offset: 0x0007187C
	public void OnEnable()
	{
		this.enabledAt = UnityEngine.Time.time;
	}

	// Token: 0x060013A4 RID: 5028 RVA: 0x0007368C File Offset: 0x0007188C
	public void OnCollisionEnter(Collision collision)
	{
		if (!ConVar.Physics.sendeffects)
		{
			return;
		}
		if (UnityEngine.Time.time < this.enabledAt + this.enableDelay)
		{
			return;
		}
		if (UnityEngine.Time.time < this.lastEffectPlayed + this.minTimeBetweenEffects)
		{
			return;
		}
		float num = collision.relativeVelocity.magnitude;
		num *= 0.055f;
		if (num <= this.ignoreImpactThreshold)
		{
			return;
		}
		if (Vector3.Distance(base.transform.position, this.lastCollisionPos) < this.minDistBetweenEffects && this.lastEffectPlayed != 0f)
		{
			return;
		}
		if (this.entity != null)
		{
			this.entity.SignalBroadcast(global::BaseEntity.Signal.PhysImpact, num.ToString(), null);
		}
		this.lastEffectPlayed = UnityEngine.Time.time;
		this.lastCollisionPos = base.transform.position;
	}

	// Token: 0x04000E80 RID: 3712
	public global::BaseEntity entity;

	// Token: 0x04000E81 RID: 3713
	public global::SoundDefinition physImpactSoundDef;

	// Token: 0x04000E82 RID: 3714
	public float minTimeBetweenEffects = 0.25f;

	// Token: 0x04000E83 RID: 3715
	public float minDistBetweenEffects = 0.1f;

	// Token: 0x04000E84 RID: 3716
	public float lowMedThreshold = 0.4f;

	// Token: 0x04000E85 RID: 3717
	public float medHardThreshold = 0.7f;

	// Token: 0x04000E86 RID: 3718
	public float enableDelay = 0.1f;

	// Token: 0x04000E87 RID: 3719
	private float lastEffectPlayed;

	// Token: 0x04000E88 RID: 3720
	private float enabledAt = float.PositiveInfinity;

	// Token: 0x04000E89 RID: 3721
	private float ignoreImpactThreshold = 0.02f;

	// Token: 0x04000E8A RID: 3722
	private Vector3 lastCollisionPos;
}
