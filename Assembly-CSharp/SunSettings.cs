﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x02000282 RID: 642
public class SunSettings : MonoBehaviour, IClientComponent
{
	// Token: 0x060010C2 RID: 4290 RVA: 0x00064BCC File Offset: 0x00062DCC
	private void OnEnable()
	{
		this.light = base.GetComponent<Light>();
	}

	// Token: 0x060010C3 RID: 4291 RVA: 0x00064BDC File Offset: 0x00062DDC
	private void Update()
	{
		LightShadows lightShadows = Mathf.Clamp(ConVar.Graphics.shadowmode, 1, 2);
		if (this.light.shadows != lightShadows)
		{
			this.light.shadows = lightShadows;
		}
	}

	// Token: 0x04000BEC RID: 3052
	private Light light;
}
