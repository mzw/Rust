﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000081 RID: 129
public class Planner : global::HeldEntity
{
	// Token: 0x06000895 RID: 2197 RVA: 0x000381C8 File Offset: 0x000363C8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Planner.OnRpcMessage", 0.1f))
		{
			if (rpc == 1840638812u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoPlace ");
				}
				using (TimeWarning.New("DoPlace", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoPlace", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoPlace(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoPlace");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000896 RID: 2198 RVA: 0x000383A4 File Offset: 0x000365A4
	public global::ItemModDeployable GetModDeployable()
	{
		global::ItemDefinition ownerItemDefinition = base.GetOwnerItemDefinition();
		if (ownerItemDefinition == null)
		{
			return null;
		}
		return ownerItemDefinition.GetComponent<global::ItemModDeployable>();
	}

	// Token: 0x06000897 RID: 2199 RVA: 0x000383CC File Offset: 0x000365CC
	public global::Deployable GetDeployable()
	{
		global::ItemModDeployable modDeployable = this.GetModDeployable();
		if (modDeployable == null)
		{
			return null;
		}
		return modDeployable.GetDeployable(this);
	}

	// Token: 0x1700005F RID: 95
	// (get) Token: 0x06000898 RID: 2200 RVA: 0x000383F8 File Offset: 0x000365F8
	public bool isTypeDeployable
	{
		get
		{
			return this.GetModDeployable() != null;
		}
	}

	// Token: 0x06000899 RID: 2201 RVA: 0x00038408 File Offset: 0x00036608
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void DoPlace(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		using (CreateBuilding createBuilding = CreateBuilding.Deserialize(msg.read))
		{
			this.DoBuild(createBuilding);
		}
	}

	// Token: 0x0600089A RID: 2202 RVA: 0x00038460 File Offset: 0x00036660
	public global::Socket_Base FindSocket(string name, uint prefabIDToFind)
	{
		global::Socket_Base[] source = global::PrefabAttribute.server.FindAll<global::Socket_Base>(prefabIDToFind);
		return source.FirstOrDefault((global::Socket_Base s) => s.socketName == name);
	}

	// Token: 0x0600089B RID: 2203 RVA: 0x00038498 File Offset: 0x00036698
	public void DoBuild(CreateBuilding msg)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		if (ConVar.AntiHack.objectplacement && ownerPlayer.TriggeredAntiHack(1f, float.PositiveInfinity))
		{
			ownerPlayer.ChatMessage("AntiHack!");
			return;
		}
		global::Construction construction = global::PrefabAttribute.server.Find<global::Construction>(msg.blockID);
		if (construction == null)
		{
			ownerPlayer.ChatMessage("Couldn't find Construction " + msg.blockID);
			return;
		}
		if (!this.CanAffordToPlace(construction))
		{
			ownerPlayer.ChatMessage("Can't afford to place!");
			return;
		}
		if (!ownerPlayer.CanBuild() && !construction.canBypassBuildingPermission)
		{
			ownerPlayer.ChatMessage("Building is blocked!");
			return;
		}
		global::Construction.Target target = default(global::Construction.Target);
		global::BaseNetworkable baseNetworkable = null;
		if (msg.entity > 0u)
		{
			baseNetworkable = global::BaseNetworkable.serverEntities.Find(msg.entity);
			if (!baseNetworkable)
			{
				ownerPlayer.ChatMessage("Couldn't find entity " + msg.entity);
				return;
			}
			target.entity = (baseNetworkable as global::BaseEntity);
			if (msg.socket > 0u)
			{
				string text = global::StringPool.Get(msg.socket);
				if (text != string.Empty && target.entity != null)
				{
					target.socket = this.FindSocket(text, target.entity.prefabID);
				}
				else
				{
					ownerPlayer.ChatMessage("Invalid Socket!");
				}
			}
			else
			{
				Debug.Log("socket was 0");
			}
		}
		target.ray = msg.ray;
		target.onTerrain = msg.onterrain;
		target.position = msg.position;
		target.normal = msg.normal;
		target.rotation = msg.rotation;
		target.player = ownerPlayer;
		target.valid = true;
		if (Interface.CallHook("CanBuild", new object[]
		{
			this,
			construction,
			target
		}) != null)
		{
			return;
		}
		global::Deployable deployable = this.GetDeployable();
		if (deployable && deployable.placeEffect.isValid)
		{
			if (baseNetworkable && msg.socket > 0u)
			{
				global::Effect.server.Run(deployable.placeEffect.resourcePath, baseNetworkable.transform.TransformPoint(target.socket.worldPosition), baseNetworkable.transform.up, null, false);
			}
			else
			{
				global::Effect.server.Run(deployable.placeEffect.resourcePath, msg.position, msg.normal, null, false);
			}
		}
		this.DoBuild(target, construction);
	}

	// Token: 0x0600089C RID: 2204 RVA: 0x00038748 File Offset: 0x00036948
	public void DoBuild(global::Construction.Target target, global::Construction component)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		if (target.ray.IsNaNOrInfinity())
		{
			return;
		}
		if (Vector3Ex.IsNaNOrInfinity(target.position))
		{
			return;
		}
		if (Vector3Ex.IsNaNOrInfinity(target.normal))
		{
			return;
		}
		if (target.socket != null)
		{
			if (!target.socket.female)
			{
				ownerPlayer.ChatMessage("Target socket is not female. (" + target.socket.socketName + ")");
				return;
			}
			if (target.entity != null && target.entity.IsOccupied(target.socket))
			{
				ownerPlayer.ChatMessage("Target socket is occupied. (" + target.socket.socketName + ")");
				return;
			}
		}
		global::Construction.lastPlacementError = "No Error";
		GameObject gameObject = this.DoPlacement(target, component);
		if (gameObject == null)
		{
			ownerPlayer.ChatMessage("Can't place: " + global::Construction.lastPlacementError);
		}
		if (gameObject != null)
		{
			Interface.CallHook("OnEntityBuilt", new object[]
			{
				this,
				gameObject
			});
			global::Deployable deployable = this.GetDeployable();
			if (deployable != null)
			{
				global::BaseEntity baseEntity = gameObject.ToBaseEntity();
				if (deployable.setSocketParent && target.entity != null && baseEntity)
				{
					baseEntity.SetParent(target.entity, 0u);
					baseEntity.transform.position = target.entity.transform.InverseTransformPoint(baseEntity.transform.position);
				}
				if (deployable.wantsInstanceData && base.GetOwnerItem().instanceData != null)
				{
					(baseEntity as global::IInstanceDataReceiver).ReceiveInstanceData(base.GetOwnerItem().instanceData);
				}
				if (deployable.copyInventoryFromItem)
				{
					global::StorageContainer component2 = baseEntity.GetComponent<global::StorageContainer>();
					if (component2)
					{
						component2.ReceiveInventoryFromItem(base.GetOwnerItem());
					}
				}
				global::ItemModDeployable modDeployable = this.GetModDeployable();
				if (modDeployable != null)
				{
					modDeployable.OnDeployed(baseEntity, ownerPlayer);
				}
			}
			this.PayForPlacement(ownerPlayer, component);
		}
	}

	// Token: 0x0600089D RID: 2205 RVA: 0x00038988 File Offset: 0x00036B88
	public GameObject DoPlacement(global::Construction.Target placement, global::Construction component)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return null;
		}
		global::BaseEntity baseEntity = component.CreateConstruction(placement, true);
		if (!baseEntity)
		{
			return null;
		}
		float num = 1f;
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem != null)
		{
			baseEntity.skinID = ownerItem.skin;
			if (ownerItem.hasCondition)
			{
				num = ownerItem.conditionNormalized;
			}
		}
		baseEntity.gameObject.AwakeFromInstantiate();
		global::BuildingBlock buildingBlock = baseEntity as global::BuildingBlock;
		if (buildingBlock)
		{
			buildingBlock.blockDefinition = global::PrefabAttribute.server.Find<global::Construction>(buildingBlock.prefabID);
			if (!buildingBlock.blockDefinition)
			{
				Debug.LogError("Placing a building block that has no block definition!");
				return null;
			}
			buildingBlock.SetGrade(buildingBlock.blockDefinition.defaultGrade.gradeBase.type);
			float num2 = buildingBlock.currentGrade.maxHealth;
		}
		global::BaseCombatEntity baseCombatEntity = baseEntity as global::BaseCombatEntity;
		if (baseCombatEntity)
		{
			float num2 = (!(buildingBlock != null)) ? baseCombatEntity.startHealth : buildingBlock.currentGrade.maxHealth;
			baseCombatEntity.ResetLifeStateOnSpawn = false;
			baseCombatEntity.InitializeHealth(num2 * num, num2);
		}
		baseEntity.gameObject.SendMessage("SetDeployedBy", ownerPlayer, 1);
		baseEntity.OwnerID = ownerPlayer.userID;
		baseEntity.Spawn();
		if (buildingBlock)
		{
			global::Effect.server.Run("assets/bundled/prefabs/fx/build/frame_place.prefab", baseEntity, 0u, Vector3.zero, Vector3.zero, null, false);
		}
		global::StabilityEntity stabilityEntity = baseEntity as global::StabilityEntity;
		if (stabilityEntity)
		{
			stabilityEntity.UpdateSurroundingEntities();
		}
		return baseEntity.gameObject;
	}

	// Token: 0x0600089E RID: 2206 RVA: 0x00038B30 File Offset: 0x00036D30
	public void PayForPlacement(global::BasePlayer player, global::Construction component)
	{
		if (this.isTypeDeployable)
		{
			this.GetItem().UseItem(1);
			return;
		}
		List<global::Item> list = new List<global::Item>();
		foreach (global::ItemAmount itemAmount in component.defaultGrade.costToBuild)
		{
			player.inventory.Take(list, itemAmount.itemDef.itemid, (int)itemAmount.amount);
			player.Command("note.inv", new object[]
			{
				itemAmount.itemDef.itemid,
				itemAmount.amount * -1f
			});
		}
		foreach (global::Item item in list)
		{
			item.Remove(0f);
		}
	}

	// Token: 0x0600089F RID: 2207 RVA: 0x00038C4C File Offset: 0x00036E4C
	public bool CanAffordToPlace(global::Construction component)
	{
		if (this.isTypeDeployable)
		{
			return true;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return false;
		}
		foreach (global::ItemAmount itemAmount in component.defaultGrade.costToBuild)
		{
			if ((float)ownerPlayer.inventory.GetAmount(itemAmount.itemDef.itemid) < itemAmount.amount)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x04000401 RID: 1025
	public global::BaseEntity[] buildableList;
}
