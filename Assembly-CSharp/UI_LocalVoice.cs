﻿using System;
using UnityEngine;

// Token: 0x02000716 RID: 1814
public class UI_LocalVoice : SingletonComponent<global::UI_LocalVoice>
{
	// Token: 0x04001EE9 RID: 7913
	public CanvasGroup voiceCanvas;

	// Token: 0x04001EEA RID: 7914
	public CanvasGroup levelImage;
}
