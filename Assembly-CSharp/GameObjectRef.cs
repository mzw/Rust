﻿using System;
using Facepunch;
using UnityEngine;

// Token: 0x020007B4 RID: 1972
[Serializable]
public class GameObjectRef : global::ResourceRef<GameObject>
{
	// Token: 0x060024B2 RID: 9394 RVA: 0x000C9F68 File Offset: 0x000C8168
	public GameObject Instantiate(Transform parent = null)
	{
		return Facepunch.Instantiate.GameObject(base.Get(), parent);
	}
}
