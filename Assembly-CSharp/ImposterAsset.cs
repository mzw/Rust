﻿using System;
using UnityEngine;

// Token: 0x0200060D RID: 1549
public class ImposterAsset : ScriptableObject
{
	// Token: 0x06001F68 RID: 8040 RVA: 0x000B2304 File Offset: 0x000B0504
	public Texture2D FindTexture(string name)
	{
		foreach (global::ImposterAsset.TextureEntry textureEntry in this.textures)
		{
			if (textureEntry.name == name)
			{
				return textureEntry.texture;
			}
		}
		return null;
	}

	// Token: 0x04001A66 RID: 6758
	public global::ImposterAsset.TextureEntry[] textures;

	// Token: 0x04001A67 RID: 6759
	public Vector2 size;

	// Token: 0x04001A68 RID: 6760
	public Vector2 pivot;

	// Token: 0x04001A69 RID: 6761
	public Mesh mesh;

	// Token: 0x0200060E RID: 1550
	[Serializable]
	public class TextureEntry
	{
		// Token: 0x06001F69 RID: 8041 RVA: 0x000B234C File Offset: 0x000B054C
		public TextureEntry(string name, Texture2D texture)
		{
			this.name = name;
			this.texture = texture;
		}

		// Token: 0x04001A6A RID: 6762
		public string name;

		// Token: 0x04001A6B RID: 6763
		public Texture2D texture;
	}
}
