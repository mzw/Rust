﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020000B1 RID: 177
public class ItemSearchEntry : MonoBehaviour
{
	// Token: 0x0400050B RID: 1291
	public Button button;

	// Token: 0x0400050C RID: 1292
	public Text text;

	// Token: 0x0400050D RID: 1293
	public RawImage image;

	// Token: 0x0400050E RID: 1294
	public RawImage bpImage;

	// Token: 0x0400050F RID: 1295
	private global::ItemDefinition itemInfo;

	// Token: 0x04000510 RID: 1296
	private global::AddSellOrderManager manager;
}
