﻿using System;
using UnityEngine;

// Token: 0x02000007 RID: 7
public class sedanAnimation : MonoBehaviour
{
	// Token: 0x0600000D RID: 13 RVA: 0x00002744 File Offset: 0x00000944
	private void Start()
	{
		this.myRigidbody = base.GetComponent<Rigidbody>();
	}

	// Token: 0x0600000E RID: 14 RVA: 0x00002754 File Offset: 0x00000954
	private void Update()
	{
		this.DoSteering();
		this.ApplyForceAtWheels();
		this.UpdateTireAnimation();
		this.InputPlayer();
	}

	// Token: 0x0600000F RID: 15 RVA: 0x00002770 File Offset: 0x00000970
	private void InputPlayer()
	{
		if (Input.GetKey(119))
		{
			this.gasPedal = Mathf.Clamp(this.gasPedal + Time.deltaTime * this.GasLerpTime, -100f, 100f);
			this.brakePedal = Mathf.Lerp(this.brakePedal, 0f, Time.deltaTime * this.GasLerpTime);
		}
		else if (Input.GetKey(115))
		{
			this.gasPedal = Mathf.Clamp(this.gasPedal - Time.deltaTime * this.GasLerpTime, -100f, 100f);
			this.brakePedal = Mathf.Lerp(this.brakePedal, 0f, Time.deltaTime * this.GasLerpTime);
		}
		else
		{
			this.gasPedal = Mathf.Lerp(this.gasPedal, 0f, Time.deltaTime * this.GasLerpTime);
			this.brakePedal = Mathf.Lerp(this.brakePedal, 100f, Time.deltaTime * this.GasLerpTime / 5f);
		}
		if (Input.GetKey(97))
		{
			this.steering = Mathf.Clamp(this.steering - Time.deltaTime * this.SteeringLerpTime, -60f, 60f);
		}
		else if (Input.GetKey(100))
		{
			this.steering = Mathf.Clamp(this.steering + Time.deltaTime * this.SteeringLerpTime, -60f, 60f);
		}
		else
		{
			this.steering = Mathf.Lerp(this.steering, 0f, Time.deltaTime * this.SteeringLerpTime);
		}
	}

	// Token: 0x06000010 RID: 16 RVA: 0x00002914 File Offset: 0x00000B14
	private void DoSteering()
	{
		this.FL_wheelCollider.steerAngle = this.steering;
		this.FR_wheelCollider.steerAngle = this.steering;
	}

	// Token: 0x06000011 RID: 17 RVA: 0x00002938 File Offset: 0x00000B38
	private void ApplyForceAtWheels()
	{
		if (this.FL_wheelCollider.isGrounded)
		{
			this.FL_wheelCollider.motorTorque = this.gasPedal * this.motorForceConstant;
			this.FL_wheelCollider.brakeTorque = this.brakePedal * this.brakeForceConstant;
		}
		if (this.FR_wheelCollider.isGrounded)
		{
			this.FR_wheelCollider.motorTorque = this.gasPedal * this.motorForceConstant;
			this.FR_wheelCollider.brakeTorque = this.brakePedal * this.brakeForceConstant;
		}
		if (this.RL_wheelCollider.isGrounded)
		{
			this.RL_wheelCollider.motorTorque = this.gasPedal * this.motorForceConstant;
			this.RL_wheelCollider.brakeTorque = this.brakePedal * this.brakeForceConstant;
		}
		if (this.RR_wheelCollider.isGrounded)
		{
			this.RR_wheelCollider.motorTorque = this.gasPedal * this.motorForceConstant;
			this.RR_wheelCollider.brakeTorque = this.brakePedal * this.brakeForceConstant;
		}
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002A48 File Offset: 0x00000C48
	private void UpdateTireAnimation()
	{
		float num = Vector3.Dot(this.myRigidbody.velocity, this.myRigidbody.transform.forward);
		if (this.FL_wheelCollider.isGrounded)
		{
			this.FL_shock.localPosition = new Vector3(this.FL_shock.localPosition.x, this.shockRestingPosY + this.GetShockHeightDelta(this.FL_wheelCollider), this.FL_shock.localPosition.z);
			this.FL_wheel.localEulerAngles = new Vector3(this.FL_wheel.localEulerAngles.x, this.FL_wheel.localEulerAngles.y, this.FL_wheel.localEulerAngles.z - num * Time.deltaTime * this.wheelSpinConstant);
		}
		else
		{
			this.FL_shock.localPosition = Vector3.Lerp(this.FL_shock.localPosition, new Vector3(this.FL_shock.localPosition.x, this.shockRestingPosY, this.FL_shock.localPosition.z), Time.deltaTime * 2f);
		}
		if (this.FR_wheelCollider.isGrounded)
		{
			this.FR_shock.localPosition = new Vector3(this.FR_shock.localPosition.x, this.shockRestingPosY + this.GetShockHeightDelta(this.FR_wheelCollider), this.FR_shock.localPosition.z);
			this.FR_wheel.localEulerAngles = new Vector3(this.FR_wheel.localEulerAngles.x, this.FR_wheel.localEulerAngles.y, this.FR_wheel.localEulerAngles.z - num * Time.deltaTime * this.wheelSpinConstant);
		}
		else
		{
			this.FR_shock.localPosition = Vector3.Lerp(this.FR_shock.localPosition, new Vector3(this.FR_shock.localPosition.x, this.shockRestingPosY, this.FR_shock.localPosition.z), Time.deltaTime * 2f);
		}
		if (this.RL_wheelCollider.isGrounded)
		{
			this.RL_shock.localPosition = new Vector3(this.RL_shock.localPosition.x, this.shockRestingPosY + this.GetShockHeightDelta(this.RL_wheelCollider), this.RL_shock.localPosition.z);
			this.RL_wheel.localEulerAngles = new Vector3(this.RL_wheel.localEulerAngles.x, this.RL_wheel.localEulerAngles.y, this.RL_wheel.localEulerAngles.z - num * Time.deltaTime * this.wheelSpinConstant);
		}
		else
		{
			this.RL_shock.localPosition = Vector3.Lerp(this.RL_shock.localPosition, new Vector3(this.RL_shock.localPosition.x, this.shockRestingPosY, this.RL_shock.localPosition.z), Time.deltaTime * 2f);
		}
		if (this.RR_wheelCollider.isGrounded)
		{
			this.RR_shock.localPosition = new Vector3(this.RR_shock.localPosition.x, this.shockRestingPosY + this.GetShockHeightDelta(this.RR_wheelCollider), this.RR_shock.localPosition.z);
			this.RR_wheel.localEulerAngles = new Vector3(this.RR_wheel.localEulerAngles.x, this.RR_wheel.localEulerAngles.y, this.RR_wheel.localEulerAngles.z - num * Time.deltaTime * this.wheelSpinConstant);
		}
		else
		{
			this.RR_shock.localPosition = Vector3.Lerp(this.RR_shock.localPosition, new Vector3(this.RR_shock.localPosition.x, this.shockRestingPosY, this.RR_shock.localPosition.z), Time.deltaTime * 2f);
		}
		foreach (Transform transform in this.frontAxles)
		{
			transform.localEulerAngles = new Vector3(this.steering, transform.localEulerAngles.y, transform.localEulerAngles.z);
		}
	}

	// Token: 0x06000013 RID: 19 RVA: 0x00002F20 File Offset: 0x00001120
	private float GetShockHeightDelta(WheelCollider wheel)
	{
		int mask = LayerMask.GetMask(new string[]
		{
			"Terrain",
			"World",
			"Construction"
		});
		RaycastHit raycastHit;
		Physics.Linecast(wheel.transform.position, wheel.transform.position - Vector3.up * 10f, ref raycastHit, mask);
		return Mathx.RemapValClamped(raycastHit.distance, this.traceDistanceNeutralPoint - this.shockDistance, this.traceDistanceNeutralPoint + this.shockDistance, this.shockDistance * 0.75f, -0.75f * this.shockDistance);
	}

	// Token: 0x04000015 RID: 21
	public Transform[] frontAxles;

	// Token: 0x04000016 RID: 22
	public Transform FL_shock;

	// Token: 0x04000017 RID: 23
	public Transform FL_wheel;

	// Token: 0x04000018 RID: 24
	public Transform FR_shock;

	// Token: 0x04000019 RID: 25
	public Transform FR_wheel;

	// Token: 0x0400001A RID: 26
	public Transform RL_shock;

	// Token: 0x0400001B RID: 27
	public Transform RL_wheel;

	// Token: 0x0400001C RID: 28
	public Transform RR_shock;

	// Token: 0x0400001D RID: 29
	public Transform RR_wheel;

	// Token: 0x0400001E RID: 30
	public WheelCollider FL_wheelCollider;

	// Token: 0x0400001F RID: 31
	public WheelCollider FR_wheelCollider;

	// Token: 0x04000020 RID: 32
	public WheelCollider RL_wheelCollider;

	// Token: 0x04000021 RID: 33
	public WheelCollider RR_wheelCollider;

	// Token: 0x04000022 RID: 34
	public Transform steeringWheel;

	// Token: 0x04000023 RID: 35
	public float motorForceConstant = 150f;

	// Token: 0x04000024 RID: 36
	public float brakeForceConstant = 500f;

	// Token: 0x04000025 RID: 37
	public float brakePedal;

	// Token: 0x04000026 RID: 38
	public float gasPedal;

	// Token: 0x04000027 RID: 39
	public float steering;

	// Token: 0x04000028 RID: 40
	private Rigidbody myRigidbody;

	// Token: 0x04000029 RID: 41
	public float GasLerpTime = 20f;

	// Token: 0x0400002A RID: 42
	public float SteeringLerpTime = 20f;

	// Token: 0x0400002B RID: 43
	private float wheelSpinConstant = 120f;

	// Token: 0x0400002C RID: 44
	private float shockRestingPosY = -0.27f;

	// Token: 0x0400002D RID: 45
	private float shockDistance = 0.3f;

	// Token: 0x0400002E RID: 46
	private float traceDistanceNeutralPoint = 0.7f;
}
