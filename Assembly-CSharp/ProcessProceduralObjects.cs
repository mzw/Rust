﻿using System;
using System.Collections.Generic;

// Token: 0x020005C9 RID: 1481
public class ProcessProceduralObjects : global::ProceduralComponent
{
	// Token: 0x06001E9F RID: 7839 RVA: 0x000AC500 File Offset: 0x000AA700
	public override void Process(uint seed)
	{
		List<global::ProceduralObject> proceduralObjects = SingletonComponent<global::WorldSetup>.Instance.ProceduralObjects;
		if (!global::World.Serialization.Cached)
		{
			for (int i = 0; i < proceduralObjects.Count; i++)
			{
				global::ProceduralObject proceduralObject = proceduralObjects[i];
				if (proceduralObject)
				{
					proceduralObject.Process();
				}
			}
		}
		proceduralObjects.Clear();
	}

	// Token: 0x1700021D RID: 541
	// (get) Token: 0x06001EA0 RID: 7840 RVA: 0x000AC560 File Offset: 0x000AA760
	public override bool RunOnCache
	{
		get
		{
			return true;
		}
	}
}
