﻿using System;
using UnityEngine;

// Token: 0x020006BD RID: 1725
public class NoticeArea : SingletonComponent<global::NoticeArea>
{
	// Token: 0x0600215A RID: 8538 RVA: 0x000BC0E0 File Offset: 0x000BA2E0
	public static void ItemPickUp(global::ItemDefinition def, int amount, string nameOverride)
	{
		if (SingletonComponent<global::NoticeArea>.Instance == null)
		{
			return;
		}
		GameObject gameObject = Object.Instantiate<GameObject>((amount <= 0) ? SingletonComponent<global::NoticeArea>.Instance.itemDroppedPrefab : SingletonComponent<global::NoticeArea>.Instance.itemPickupPrefab);
		if (gameObject == null)
		{
			return;
		}
		gameObject.transform.SetParent(SingletonComponent<global::NoticeArea>.Instance.transform, false);
		global::ItemPickupNotice component = gameObject.GetComponent<global::ItemPickupNotice>();
		if (component == null)
		{
			return;
		}
		component.itemInfo = def;
		component.amount = amount;
		if (!string.IsNullOrEmpty(nameOverride))
		{
			component.Text.text = nameOverride;
		}
	}

	// Token: 0x04001D33 RID: 7475
	public GameObject itemPickupPrefab;

	// Token: 0x04001D34 RID: 7476
	public GameObject itemDroppedPrefab;
}
