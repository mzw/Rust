﻿using System;

// Token: 0x02000437 RID: 1079
public interface IOnPostNetworkUpdate
{
	// Token: 0x06001868 RID: 6248
	void OnPostNetworkUpdate(global::BaseEntity entity);
}
