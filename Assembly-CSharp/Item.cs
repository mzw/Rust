﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020004BF RID: 1215
public class Item
{
	// Token: 0x170001C1 RID: 449
	// (get) Token: 0x06001A08 RID: 6664 RVA: 0x00092594 File Offset: 0x00090794
	// (set) Token: 0x06001A07 RID: 6663 RVA: 0x00092544 File Offset: 0x00090744
	public float condition
	{
		get
		{
			return this._condition;
		}
		set
		{
			float condition = this._condition;
			this._condition = Mathf.Clamp(value, 0f, this.maxCondition);
			if (this.isServer && Mathf.Ceil(value) != Mathf.Ceil(condition))
			{
				this.MarkDirty();
			}
		}
	}

	// Token: 0x170001C2 RID: 450
	// (get) Token: 0x06001A0A RID: 6666 RVA: 0x000925D0 File Offset: 0x000907D0
	// (set) Token: 0x06001A09 RID: 6665 RVA: 0x0009259C File Offset: 0x0009079C
	public float maxCondition
	{
		get
		{
			return this._maxCondition;
		}
		set
		{
			this._maxCondition = Mathf.Clamp(value, 0f, this.info.condition.max);
			if (this.isServer)
			{
				this.MarkDirty();
			}
		}
	}

	// Token: 0x170001C3 RID: 451
	// (get) Token: 0x06001A0B RID: 6667 RVA: 0x000925D8 File Offset: 0x000907D8
	public float maxConditionNormalized
	{
		get
		{
			return this._maxCondition / this.info.condition.max;
		}
	}

	// Token: 0x170001C4 RID: 452
	// (get) Token: 0x06001A0C RID: 6668 RVA: 0x000925F4 File Offset: 0x000907F4
	// (set) Token: 0x06001A0D RID: 6669 RVA: 0x00092614 File Offset: 0x00090814
	public float conditionNormalized
	{
		get
		{
			if (!this.hasCondition)
			{
				return 1f;
			}
			return this.condition / this.maxCondition;
		}
		set
		{
			if (!this.hasCondition)
			{
				return;
			}
			this.condition = value * this.maxCondition;
		}
	}

	// Token: 0x170001C5 RID: 453
	// (get) Token: 0x06001A0E RID: 6670 RVA: 0x00092630 File Offset: 0x00090830
	public bool hasCondition
	{
		get
		{
			return this.info != null && this.info.condition.enabled && this.info.condition.max > 0f;
		}
	}

	// Token: 0x170001C6 RID: 454
	// (get) Token: 0x06001A0F RID: 6671 RVA: 0x00092680 File Offset: 0x00090880
	public bool isBroken
	{
		get
		{
			return this.hasCondition && this.condition <= 0f;
		}
	}

	// Token: 0x06001A10 RID: 6672 RVA: 0x000926A0 File Offset: 0x000908A0
	public void LoseCondition(float amount)
	{
		if (!this.hasCondition)
		{
			return;
		}
		if (ConVar.Debugging.disablecondition)
		{
			return;
		}
		if (Interface.CallHook("IOnLoseCondition", new object[]
		{
			this,
			amount
		}) != null)
		{
			return;
		}
		float condition = this.condition;
		this.condition -= amount;
		if (ConVar.Global.developer > 0)
		{
			Debug.Log(string.Concat(new object[]
			{
				this.info.shortname,
				" was damaged by: ",
				amount,
				"cond is: ",
				this.condition,
				"/",
				this.maxCondition
			}));
		}
		if (this.condition <= 0f && this.condition < condition)
		{
			this.OnBroken();
		}
	}

	// Token: 0x06001A11 RID: 6673 RVA: 0x00092784 File Offset: 0x00090984
	public void RepairCondition(float amount)
	{
		if (!this.hasCondition)
		{
			return;
		}
		this.condition += amount;
	}

	// Token: 0x06001A12 RID: 6674 RVA: 0x000927A0 File Offset: 0x000909A0
	public void DoRepair(float maxLossFraction)
	{
		if (!this.hasCondition)
		{
			return;
		}
		if (this.info.condition.maintainMaxCondition)
		{
			maxLossFraction = 0f;
		}
		float num = 1f - this.condition / this.maxCondition;
		maxLossFraction = Mathf.Clamp(maxLossFraction, 0f, this.info.condition.max);
		this.maxCondition *= 1f - maxLossFraction * num;
		this.condition = this.maxCondition;
		global::BaseEntity baseEntity = this.GetHeldEntity();
		if (baseEntity != null)
		{
			baseEntity.SetFlag(global::BaseEntity.Flags.Broken, false, false);
		}
		if (ConVar.Global.developer > 0)
		{
			Debug.Log(string.Concat(new object[]
			{
				this.info.shortname,
				" was repaired! new cond is: ",
				this.condition,
				"/",
				this.maxCondition
			}));
		}
	}

	// Token: 0x06001A13 RID: 6675 RVA: 0x000928A0 File Offset: 0x00090AA0
	public global::ItemContainer GetRootContainer()
	{
		global::ItemContainer itemContainer = this.parent;
		int num = 0;
		while (itemContainer != null && num <= 8)
		{
			if (itemContainer.parent == null || itemContainer.parent.parent == null)
			{
				break;
			}
			itemContainer = itemContainer.parent.parent;
			num++;
		}
		if (num == 8)
		{
			Debug.LogWarning("GetRootContainer failed with 8 iterations");
		}
		return itemContainer;
	}

	// Token: 0x06001A14 RID: 6676 RVA: 0x0009290C File Offset: 0x00090B0C
	public virtual void OnBroken()
	{
		if (!this.hasCondition)
		{
			return;
		}
		global::BaseEntity baseEntity = this.GetHeldEntity();
		if (baseEntity != null)
		{
			baseEntity.SetFlag(global::BaseEntity.Flags.Broken, true, false);
		}
		global::BasePlayer ownerPlayer = this.GetOwnerPlayer();
		if (ownerPlayer && ownerPlayer.GetActiveItem() == this)
		{
			global::Effect.server.Run("assets/bundled/prefabs/fx/item_break.prefab", ownerPlayer, 0u, Vector3.zero, Vector3.zero, null, false);
			ownerPlayer.ChatMessage("Your active item was broken!");
		}
		if (!this.info.condition.repairable || this.maxCondition <= 5f)
		{
			this.Remove(0f);
		}
		else if (this.parent != null && this.parent.HasFlag(global::ItemContainer.Flag.NoBrokenItems))
		{
			global::ItemContainer rootContainer = this.GetRootContainer();
			if (rootContainer.HasFlag(global::ItemContainer.Flag.NoBrokenItems))
			{
				this.Remove(0f);
			}
			else
			{
				global::BasePlayer playerOwner = rootContainer.playerOwner;
				if (playerOwner != null && !this.MoveToContainer(playerOwner.inventory.containerMain, -1, true))
				{
					this.Drop(playerOwner.transform.position, playerOwner.eyes.BodyForward() * 1.5f, default(Quaternion));
				}
			}
		}
		this.MarkDirty();
	}

	// Token: 0x170001C7 RID: 455
	// (get) Token: 0x06001A15 RID: 6677 RVA: 0x00092A60 File Offset: 0x00090C60
	public int despawnMultiplier
	{
		get
		{
			return (!(this.info != null)) ? 1 : Mathf.Clamp((this.info.rarity - 1) * 4, 1, 100);
		}
	}

	// Token: 0x170001C8 RID: 456
	// (get) Token: 0x06001A16 RID: 6678 RVA: 0x00092A90 File Offset: 0x00090C90
	public global::ItemDefinition blueprintTargetDef
	{
		get
		{
			if (!this.IsBlueprint())
			{
				return null;
			}
			return global::ItemManager.FindItemDefinition(this.blueprintTarget);
		}
	}

	// Token: 0x170001C9 RID: 457
	// (get) Token: 0x06001A17 RID: 6679 RVA: 0x00092AAC File Offset: 0x00090CAC
	// (set) Token: 0x06001A18 RID: 6680 RVA: 0x00092AC8 File Offset: 0x00090CC8
	public int blueprintTarget
	{
		get
		{
			if (this.instanceData == null)
			{
				return 0;
			}
			return this.instanceData.blueprintTarget;
		}
		set
		{
			if (this.instanceData == null)
			{
				this.instanceData = new ProtoBuf.Item.InstanceData();
			}
			this.instanceData.ShouldPool = false;
			this.instanceData.blueprintTarget = value;
		}
	}

	// Token: 0x170001CA RID: 458
	// (get) Token: 0x06001A19 RID: 6681 RVA: 0x00092AF8 File Offset: 0x00090CF8
	// (set) Token: 0x06001A1A RID: 6682 RVA: 0x00092B00 File Offset: 0x00090D00
	public int blueprintAmount
	{
		get
		{
			return this.amount;
		}
		set
		{
			this.amount = value;
		}
	}

	// Token: 0x06001A1B RID: 6683 RVA: 0x00092B0C File Offset: 0x00090D0C
	public bool IsBlueprint()
	{
		return this.blueprintTarget != 0;
	}

	// Token: 0x14000003 RID: 3
	// (add) Token: 0x06001A1C RID: 6684 RVA: 0x00092B1C File Offset: 0x00090D1C
	// (remove) Token: 0x06001A1D RID: 6685 RVA: 0x00092B54 File Offset: 0x00090D54
	public event Action<global::Item> OnDirty;

	// Token: 0x06001A1E RID: 6686 RVA: 0x00092B8C File Offset: 0x00090D8C
	public bool HasFlag(global::Item.Flag f)
	{
		return (this.flags & f) == f;
	}

	// Token: 0x06001A1F RID: 6687 RVA: 0x00092B9C File Offset: 0x00090D9C
	public void SetFlag(global::Item.Flag f, bool b)
	{
		if (b)
		{
			this.flags |= f;
		}
		else
		{
			this.flags &= ~f;
		}
	}

	// Token: 0x06001A20 RID: 6688 RVA: 0x00092BC8 File Offset: 0x00090DC8
	public bool IsOn()
	{
		return this.HasFlag(global::Item.Flag.IsOn);
	}

	// Token: 0x06001A21 RID: 6689 RVA: 0x00092BD4 File Offset: 0x00090DD4
	public bool IsOnFire()
	{
		return this.HasFlag(global::Item.Flag.OnFire);
	}

	// Token: 0x06001A22 RID: 6690 RVA: 0x00092BE0 File Offset: 0x00090DE0
	public bool IsCooking()
	{
		return this.HasFlag(global::Item.Flag.Cooking);
	}

	// Token: 0x06001A23 RID: 6691 RVA: 0x00092BEC File Offset: 0x00090DEC
	public bool IsLocked()
	{
		return this.HasFlag(global::Item.Flag.IsLocked) || (this.parent != null && this.parent.IsLocked());
	}

	// Token: 0x170001CB RID: 459
	// (get) Token: 0x06001A24 RID: 6692 RVA: 0x00092C18 File Offset: 0x00090E18
	public global::Item parentItem
	{
		get
		{
			if (this.parent == null)
			{
				return null;
			}
			return this.parent.parent;
		}
	}

	// Token: 0x06001A25 RID: 6693 RVA: 0x00092C34 File Offset: 0x00090E34
	public void MarkDirty()
	{
		this.OnChanged();
		this.dirty = true;
		if (this.parent != null)
		{
			this.parent.MarkDirty();
		}
		if (this.OnDirty != null)
		{
			this.OnDirty(this);
		}
	}

	// Token: 0x06001A26 RID: 6694 RVA: 0x00092C70 File Offset: 0x00090E70
	public void OnChanged()
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnChanged(this);
		}
		if (this.contents != null)
		{
			this.contents.OnChanged();
		}
	}

	// Token: 0x06001A27 RID: 6695 RVA: 0x00092CC0 File Offset: 0x00090EC0
	public void CollectedForCrafting(global::BasePlayer crafter)
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.CollectedForCrafting(this, crafter);
		}
	}

	// Token: 0x06001A28 RID: 6696 RVA: 0x00092CFC File Offset: 0x00090EFC
	public void ReturnedFromCancelledCraft(global::BasePlayer crafter)
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.ReturnedFromCancelledCraft(this, crafter);
		}
	}

	// Token: 0x06001A29 RID: 6697 RVA: 0x00092D38 File Offset: 0x00090F38
	public void Initialize(global::ItemDefinition template)
	{
		this.uid = Network.Net.sv.TakeUID();
		float max = this.info.condition.max;
		this.maxCondition = max;
		this.condition = max;
		this.OnItemCreated();
	}

	// Token: 0x06001A2A RID: 6698 RVA: 0x00092D7C File Offset: 0x00090F7C
	public void OnItemCreated()
	{
		this.onCycle = null;
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnItemCreated(this);
		}
	}

	// Token: 0x06001A2B RID: 6699 RVA: 0x00092DBC File Offset: 0x00090FBC
	public void OnVirginSpawn()
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnVirginItem(this);
		}
	}

	// Token: 0x06001A2C RID: 6700 RVA: 0x00092DF4 File Offset: 0x00090FF4
	public void RemoveFromWorld()
	{
		global::BaseEntity worldEntity = this.GetWorldEntity();
		if (worldEntity == null)
		{
			return;
		}
		this.SetWorldEntity(null);
		this.OnRemovedFromWorld();
		if (this.contents != null)
		{
			this.contents.OnRemovedFromWorld();
		}
		if (!worldEntity.IsValid())
		{
			return;
		}
		worldEntity.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06001A2D RID: 6701 RVA: 0x00092E4C File Offset: 0x0009104C
	public void OnRemovedFromWorld()
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnRemovedFromWorld(this);
		}
	}

	// Token: 0x06001A2E RID: 6702 RVA: 0x00092E84 File Offset: 0x00091084
	public void RemoveFromContainer()
	{
		if (this.parent == null)
		{
			return;
		}
		this.SetParent(null);
	}

	// Token: 0x06001A2F RID: 6703 RVA: 0x00092E9C File Offset: 0x0009109C
	public void SetParent(global::ItemContainer target)
	{
		if (target == this.parent)
		{
			return;
		}
		if (this.parent != null)
		{
			this.parent.Remove(this);
			this.parent = null;
		}
		if (target == null)
		{
			this.position = 0;
		}
		else
		{
			this.parent = target;
			if (!this.parent.Insert(this))
			{
				this.Remove(0f);
				Debug.LogError("Item.SetParent caused remove - this shouldn't ever happen");
			}
		}
		this.MarkDirty();
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnParentChanged(this);
		}
	}

	// Token: 0x06001A30 RID: 6704 RVA: 0x00092F48 File Offset: 0x00091148
	public void OnAttacked(global::HitInfo hitInfo)
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnAttacked(this, hitInfo);
		}
	}

	// Token: 0x06001A31 RID: 6705 RVA: 0x00092F84 File Offset: 0x00091184
	public bool IsChildContainer(global::ItemContainer c)
	{
		if (this.contents == null)
		{
			return false;
		}
		if (this.contents == c)
		{
			return true;
		}
		foreach (global::Item item in this.contents.itemList)
		{
			if (item.IsChildContainer(c))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x06001A32 RID: 6706 RVA: 0x00093010 File Offset: 0x00091210
	public bool CanMoveTo(global::ItemContainer newcontainer, int iTargetPos = -1, bool allowStack = true)
	{
		return !this.IsChildContainer(newcontainer) && newcontainer.CanAcceptItem(this, iTargetPos) == global::ItemContainer.CanAcceptResult.CanAccept && iTargetPos < newcontainer.capacity && (this.parent == null || newcontainer != this.parent || iTargetPos != this.position);
	}

	// Token: 0x06001A33 RID: 6707 RVA: 0x00093070 File Offset: 0x00091270
	public bool MoveToContainer(global::ItemContainer newcontainer, int iTargetPos = -1, bool allowStack = true)
	{
		bool result;
		using (TimeWarning.New("MoveToContainer", 0.1f))
		{
			global::ItemContainer itemContainer = this.parent;
			if (!this.CanMoveTo(newcontainer, iTargetPos, allowStack))
			{
				result = false;
			}
			else if (iTargetPos >= 0 && newcontainer.SlotTaken(iTargetPos))
			{
				global::Item slot = newcontainer.GetSlot(iTargetPos);
				if (allowStack)
				{
					int num = slot.MaxStackable();
					if (slot.CanStack(this))
					{
						if (slot.amount >= num)
						{
							return false;
						}
						slot.amount += this.amount;
						slot.MarkDirty();
						this.RemoveFromWorld();
						this.RemoveFromContainer();
						this.Remove(0f);
						int num2 = slot.amount - num;
						if (num2 > 0)
						{
							global::Item item = slot.SplitItem(num2);
							if (item != null && !item.MoveToContainer(newcontainer, -1, false) && (itemContainer == null || !item.MoveToContainer(itemContainer, -1, true)))
							{
								item.Drop(newcontainer.dropPosition, newcontainer.dropVelocity, default(Quaternion));
							}
							slot.amount = num;
						}
						return true;
					}
				}
				if (this.parent != null)
				{
					global::ItemContainer newcontainer2 = this.parent;
					int iTargetPos2 = this.position;
					if (!slot.CanMoveTo(newcontainer2, iTargetPos2, true))
					{
						result = false;
					}
					else
					{
						this.RemoveFromContainer();
						slot.RemoveFromContainer();
						slot.MoveToContainer(newcontainer2, iTargetPos2, true);
						result = this.MoveToContainer(newcontainer, iTargetPos, true);
					}
				}
				else
				{
					result = false;
				}
			}
			else if (this.parent == newcontainer)
			{
				if (iTargetPos >= 0 && iTargetPos != this.position && !this.parent.SlotTaken(iTargetPos))
				{
					this.position = iTargetPos;
					this.MarkDirty();
					result = true;
				}
				else
				{
					result = false;
				}
			}
			else
			{
				if (iTargetPos == -1 && allowStack && this.info.stackable > 1)
				{
					global::Item item2 = (from x in newcontainer.FindItemsByItemID(this.info.itemid)
					orderby x.amount
					select x).FirstOrDefault<global::Item>();
					if (item2 != null && item2.CanStack(this))
					{
						int num3 = item2.MaxStackable();
						if (item2.amount < num3)
						{
							item2.amount += this.amount;
							item2.MarkDirty();
							int num4 = item2.amount - num3;
							if (num4 <= 0)
							{
								this.RemoveFromWorld();
								this.RemoveFromContainer();
								this.Remove(0f);
								return true;
							}
							this.amount = num4;
							this.MarkDirty();
							item2.amount = num3;
							return this.MoveToContainer(newcontainer, iTargetPos, allowStack);
						}
					}
				}
				if (newcontainer.maxStackSize > 0 && newcontainer.maxStackSize < this.amount)
				{
					global::Item item3 = this.SplitItem(newcontainer.maxStackSize);
					if (item3 != null && !item3.MoveToContainer(newcontainer, iTargetPos, false) && (itemContainer == null || !item3.MoveToContainer(itemContainer, -1, true)))
					{
						item3.Drop(newcontainer.dropPosition, newcontainer.dropVelocity, default(Quaternion));
					}
					result = true;
				}
				else if (!newcontainer.CanTake(this))
				{
					result = false;
				}
				else
				{
					this.RemoveFromContainer();
					this.RemoveFromWorld();
					this.position = iTargetPos;
					this.SetParent(newcontainer);
					result = true;
				}
			}
		}
		return result;
	}

	// Token: 0x06001A34 RID: 6708 RVA: 0x00093414 File Offset: 0x00091614
	public global::BaseEntity CreateWorldObject(Vector3 pos, Quaternion rotation = default(Quaternion))
	{
		global::BaseEntity baseEntity = this.GetWorldEntity();
		if (baseEntity != null)
		{
			return baseEntity;
		}
		baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/misc/burlap sack/generic_world.prefab", pos, rotation, true);
		if (baseEntity == null)
		{
			Debug.LogWarning("Couldn't create world object for prefab: items/generic_world");
			return null;
		}
		global::WorldItem worldItem = baseEntity as global::WorldItem;
		if (worldItem != null)
		{
			worldItem.InitializeItem(this);
		}
		baseEntity.Spawn();
		this.SetWorldEntity(baseEntity);
		return this.GetWorldEntity();
	}

	// Token: 0x06001A35 RID: 6709 RVA: 0x00093490 File Offset: 0x00091690
	public global::BaseEntity Drop(Vector3 vPos, Vector3 vVelocity, Quaternion rotation = default(Quaternion))
	{
		this.RemoveFromWorld();
		global::BaseEntity baseEntity = null;
		if (vPos != Vector3.zero && !this.info.HasFlag(global::ItemDefinition.Flag.NoDropping))
		{
			baseEntity = this.CreateWorldObject(vPos, rotation);
			if (baseEntity)
			{
				baseEntity.SetVelocity(vVelocity);
			}
		}
		else
		{
			this.Remove(0f);
		}
		Interface.CallHook("OnItemDropped", new object[]
		{
			this,
			baseEntity
		});
		this.RemoveFromContainer();
		return baseEntity;
	}

	// Token: 0x06001A36 RID: 6710 RVA: 0x00093514 File Offset: 0x00091714
	public bool IsBusy()
	{
		return this.busyTime > UnityEngine.Time.time;
	}

	// Token: 0x06001A37 RID: 6711 RVA: 0x0009352C File Offset: 0x0009172C
	public void BusyFor(float fTime)
	{
		this.busyTime = UnityEngine.Time.time + fTime;
	}

	// Token: 0x06001A38 RID: 6712 RVA: 0x0009353C File Offset: 0x0009173C
	public void Remove(float fTime = 0f)
	{
		if (this.removeTime > 0f)
		{
			return;
		}
		if (this.isServer)
		{
			foreach (global::ItemMod itemMod in this.info.itemMods)
			{
				itemMod.OnRemove(this);
			}
		}
		this.onCycle = null;
		this.removeTime = UnityEngine.Time.time + fTime;
		this.OnDirty = null;
		this.position = -1;
		if (this.isServer)
		{
			global::ItemManager.RemoveItem(this, fTime);
		}
	}

	// Token: 0x06001A39 RID: 6713 RVA: 0x000935C4 File Offset: 0x000917C4
	public void DoRemove()
	{
		this.OnDirty = null;
		this.onCycle = null;
		if (this.isServer && this.uid > 0u && Network.Net.sv != null)
		{
			Network.Net.sv.ReturnUID(this.uid);
			this.uid = 0u;
		}
		if (this.contents != null)
		{
			this.contents.Kill();
			this.contents = null;
		}
		if (this.isServer)
		{
			this.RemoveFromWorld();
			this.RemoveFromContainer();
		}
		global::BaseEntity baseEntity = this.GetHeldEntity();
		if (baseEntity.IsValid())
		{
			Debug.LogWarning(string.Concat(new object[]
			{
				"Item's Held Entity not removed!",
				this.info.displayName.english,
				" -> ",
				baseEntity
			}), baseEntity);
		}
	}

	// Token: 0x06001A3A RID: 6714 RVA: 0x00093698 File Offset: 0x00091898
	public void SwitchOnOff(bool bNewState, global::BasePlayer player)
	{
		if (this.HasFlag(global::Item.Flag.IsOn) == bNewState)
		{
			return;
		}
		this.SetFlag(global::Item.Flag.IsOn, bNewState);
		this.MarkDirty();
	}

	// Token: 0x06001A3B RID: 6715 RVA: 0x000936B8 File Offset: 0x000918B8
	public void LockUnlock(bool bNewState, global::BasePlayer player)
	{
		if (this.HasFlag(global::Item.Flag.IsLocked) == bNewState)
		{
			return;
		}
		this.SetFlag(global::Item.Flag.IsLocked, bNewState);
		this.MarkDirty();
	}

	// Token: 0x170001CC RID: 460
	// (get) Token: 0x06001A3C RID: 6716 RVA: 0x000936D8 File Offset: 0x000918D8
	public float temperature
	{
		get
		{
			if (this.parent != null)
			{
				return this.parent.temperature;
			}
			return 15f;
		}
	}

	// Token: 0x06001A3D RID: 6717 RVA: 0x000936F8 File Offset: 0x000918F8
	public global::BasePlayer GetOwnerPlayer()
	{
		if (this.parent == null)
		{
			return null;
		}
		return this.parent.GetOwnerPlayer();
	}

	// Token: 0x06001A3E RID: 6718 RVA: 0x00093714 File Offset: 0x00091914
	public global::Item SplitItem(int split_Amount)
	{
		Assert.IsTrue(split_Amount > 0, "split_Amount <= 0");
		if (split_Amount <= 0)
		{
			return null;
		}
		if (split_Amount >= this.amount)
		{
			return null;
		}
		object obj = Interface.CallHook("OnItemSplit", new object[]
		{
			this,
			split_Amount
		});
		if (obj is global::Item)
		{
			return (global::Item)obj;
		}
		this.amount -= split_Amount;
		global::Item item = global::ItemManager.CreateByItemID(this.info.itemid, 1, 0UL);
		item.amount = split_Amount;
		if (this.IsBlueprint())
		{
			item.blueprintTarget = this.blueprintTarget;
		}
		this.MarkDirty();
		return item;
	}

	// Token: 0x06001A3F RID: 6719 RVA: 0x000937C0 File Offset: 0x000919C0
	public bool CanBeHeld()
	{
		return !this.isBroken;
	}

	// Token: 0x06001A40 RID: 6720 RVA: 0x000937D0 File Offset: 0x000919D0
	public bool CanStack(global::Item item)
	{
		object obj = Interface.CallHook("CanStackItem", new object[]
		{
			this,
			item
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return item != this && this.info.stackable > 1 && item.info.stackable > 1 && item.info.itemid == this.info.itemid && (!this.hasCondition || this.condition == this.maxCondition) && (!item.hasCondition || item.condition == item.maxCondition) && this.IsValid() && (!this.IsBlueprint() || this.blueprintTarget == item.blueprintTarget);
	}

	// Token: 0x06001A41 RID: 6721 RVA: 0x000938BC File Offset: 0x00091ABC
	public bool IsValid()
	{
		return this.removeTime <= 0f;
	}

	// Token: 0x06001A42 RID: 6722 RVA: 0x000938D4 File Offset: 0x00091AD4
	public void SetWorldEntity(global::BaseEntity ent)
	{
		if (!ent.IsValid())
		{
			this.worldEnt.Set(null);
			this.MarkDirty();
			return;
		}
		if (this.worldEnt.uid == ent.net.ID)
		{
			return;
		}
		this.worldEnt.Set(ent);
		this.MarkDirty();
		this.OnMovedToWorld();
		if (this.contents != null)
		{
			this.contents.OnMovedToWorld();
		}
	}

	// Token: 0x06001A43 RID: 6723 RVA: 0x0009394C File Offset: 0x00091B4C
	public void OnMovedToWorld()
	{
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.OnMovedToWorld(this);
		}
	}

	// Token: 0x06001A44 RID: 6724 RVA: 0x00093984 File Offset: 0x00091B84
	public global::BaseEntity GetWorldEntity()
	{
		return this.worldEnt.Get(this.isServer);
	}

	// Token: 0x06001A45 RID: 6725 RVA: 0x00093998 File Offset: 0x00091B98
	public void SetHeldEntity(global::BaseEntity ent)
	{
		if (!ent.IsValid())
		{
			this.heldEntity.Set(null);
			this.MarkDirty();
			return;
		}
		if (this.heldEntity.uid == ent.net.ID)
		{
			return;
		}
		this.heldEntity.Set(ent);
		this.MarkDirty();
		if (ent.IsValid())
		{
			global::HeldEntity heldEntity = ent as global::HeldEntity;
			if (heldEntity != null)
			{
				heldEntity.SetupHeldEntity(this);
			}
		}
	}

	// Token: 0x06001A46 RID: 6726 RVA: 0x00093A18 File Offset: 0x00091C18
	public global::BaseEntity GetHeldEntity()
	{
		return this.heldEntity.Get(this.isServer);
	}

	// Token: 0x14000004 RID: 4
	// (add) Token: 0x06001A47 RID: 6727 RVA: 0x00093A2C File Offset: 0x00091C2C
	// (remove) Token: 0x06001A48 RID: 6728 RVA: 0x00093A64 File Offset: 0x00091C64
	public event Action<global::Item, float> onCycle;

	// Token: 0x06001A49 RID: 6729 RVA: 0x00093A9C File Offset: 0x00091C9C
	public void OnCycle(float delta)
	{
		if (this.onCycle != null)
		{
			this.onCycle(this, delta);
		}
	}

	// Token: 0x06001A4A RID: 6730 RVA: 0x00093AB8 File Offset: 0x00091CB8
	public void ServerCommand(string command, global::BasePlayer player)
	{
		global::HeldEntity heldEntity = this.GetHeldEntity() as global::HeldEntity;
		if (heldEntity != null)
		{
			heldEntity.ServerCommand(this, command, player);
		}
		foreach (global::ItemMod itemMod in this.info.itemMods)
		{
			itemMod.ServerCommand(this, command, player);
		}
	}

	// Token: 0x06001A4B RID: 6731 RVA: 0x00093B14 File Offset: 0x00091D14
	public void UseItem(int amountToConsume = 1)
	{
		if (amountToConsume <= 0)
		{
			return;
		}
		Interface.CallHook("OnItemUse", new object[]
		{
			this,
			amountToConsume
		});
		this.amount -= amountToConsume;
		if (this.amount <= 0)
		{
			this.amount = 0;
			this.Remove(0f);
			return;
		}
		this.MarkDirty();
	}

	// Token: 0x06001A4C RID: 6732 RVA: 0x00093B7C File Offset: 0x00091D7C
	public bool HasAmmo(AmmoTypes ammoType)
	{
		global::ItemModProjectile component = this.info.GetComponent<global::ItemModProjectile>();
		return (component && component.IsAmmo(ammoType)) || (this.contents != null && this.contents.HasAmmo(ammoType));
	}

	// Token: 0x06001A4D RID: 6733 RVA: 0x00093BC8 File Offset: 0x00091DC8
	public void FindAmmo(List<global::Item> list, AmmoTypes ammoType)
	{
		global::ItemModProjectile component = this.info.GetComponent<global::ItemModProjectile>();
		if (component && component.IsAmmo(ammoType))
		{
			list.Add(this);
			return;
		}
		if (this.contents != null)
		{
			this.contents.FindAmmo(list, ammoType);
		}
	}

	// Token: 0x06001A4E RID: 6734 RVA: 0x00093C18 File Offset: 0x00091E18
	public override string ToString()
	{
		return string.Concat(new object[]
		{
			"Item.",
			this.info.shortname,
			"x",
			this.amount,
			".",
			this.uid
		});
	}

	// Token: 0x06001A4F RID: 6735 RVA: 0x00093C74 File Offset: 0x00091E74
	public global::Item FindItem(uint iUID)
	{
		if (this.uid == iUID)
		{
			return this;
		}
		if (this.contents == null)
		{
			return null;
		}
		return this.contents.FindItemByUID(iUID);
	}

	// Token: 0x06001A50 RID: 6736 RVA: 0x00093CA0 File Offset: 0x00091EA0
	public int MaxStackable()
	{
		int num = this.info.stackable;
		if (this.parent != null && this.parent.maxStackSize > 0)
		{
			num = Mathf.Min(this.parent.maxStackSize, num);
		}
		object obj = Interface.CallHook("OnMaxStackable", new object[]
		{
			this
		});
		if (obj is int)
		{
			return (int)obj;
		}
		return num;
	}

	// Token: 0x170001CD RID: 461
	// (get) Token: 0x06001A51 RID: 6737 RVA: 0x00093D10 File Offset: 0x00091F10
	public global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return this.info.Traits;
		}
	}

	// Token: 0x06001A52 RID: 6738 RVA: 0x00093D20 File Offset: 0x00091F20
	public virtual ProtoBuf.Item Save(bool bIncludeContainer = false, bool bIncludeOwners = true)
	{
		this.dirty = false;
		ProtoBuf.Item item = Facepunch.Pool.Get<ProtoBuf.Item>();
		item.UID = this.uid;
		item.itemid = this.info.itemid;
		item.slot = this.position;
		item.amount = this.amount;
		item.flags = (int)this.flags;
		item.removetime = this.removeTime;
		item.locktime = this.busyTime;
		item.instanceData = this.instanceData;
		item.worldEntity = this.worldEnt.uid;
		item.heldEntity = this.heldEntity.uid;
		item.skinid = this.skin;
		item.name = this.name;
		item.text = this.text;
		if (this.hasCondition)
		{
			item.conditionData = Facepunch.Pool.Get<ProtoBuf.Item.ConditionData>();
			item.conditionData.maxCondition = this._maxCondition;
			item.conditionData.condition = this._condition;
		}
		if (this.contents != null && bIncludeContainer)
		{
			item.contents = this.contents.Save();
		}
		return item;
	}

	// Token: 0x06001A53 RID: 6739 RVA: 0x00093E40 File Offset: 0x00092040
	public virtual void Load(ProtoBuf.Item load)
	{
		if (this.info == null || this.info.itemid != load.itemid)
		{
			this.info = global::ItemManager.FindItemDefinition(load.itemid);
		}
		this.uid = load.UID;
		this.name = load.name;
		this.text = load.text;
		this.amount = load.amount;
		this.position = load.slot;
		this.busyTime = load.locktime;
		this.removeTime = load.removetime;
		this.flags = (global::Item.Flag)load.flags;
		this.worldEnt.uid = load.worldEntity;
		this.heldEntity.uid = load.heldEntity;
		if (this.instanceData != null)
		{
			this.instanceData.ShouldPool = true;
			this.instanceData.ResetToPool();
			this.instanceData = null;
		}
		this.instanceData = load.instanceData;
		if (this.instanceData != null)
		{
			this.instanceData.ShouldPool = false;
		}
		this.skin = load.skinid;
		if (this.info == null || this.info.itemid != load.itemid)
		{
			this.info = global::ItemManager.FindItemDefinition(load.itemid);
		}
		if (this.info == null)
		{
			return;
		}
		this._condition = 0f;
		this._maxCondition = 0f;
		if (load.conditionData != null)
		{
			this._condition = load.conditionData.condition;
			this._maxCondition = load.conditionData.maxCondition;
		}
		else if (this.info.condition.enabled)
		{
			this._condition = this.info.condition.max;
			this._maxCondition = this.info.condition.max;
		}
		if (load.contents != null)
		{
			if (this.contents == null)
			{
				this.contents = new global::ItemContainer();
				if (this.isServer)
				{
					this.contents.ServerInitialize(this, load.contents.slots);
				}
			}
			this.contents.Load(load.contents);
		}
		if (this.isServer)
		{
			this.removeTime = 0f;
			this.OnItemCreated();
		}
	}

	// Token: 0x040014D3 RID: 5331
	private float _condition;

	// Token: 0x040014D4 RID: 5332
	private float _maxCondition = 100f;

	// Token: 0x040014D5 RID: 5333
	public global::ItemDefinition info;

	// Token: 0x040014D6 RID: 5334
	public uint uid;

	// Token: 0x040014D7 RID: 5335
	public bool dirty;

	// Token: 0x040014D8 RID: 5336
	public int amount = 1;

	// Token: 0x040014D9 RID: 5337
	public int position;

	// Token: 0x040014DA RID: 5338
	public float busyTime;

	// Token: 0x040014DB RID: 5339
	public float removeTime;

	// Token: 0x040014DC RID: 5340
	public float fuel;

	// Token: 0x040014DD RID: 5341
	public bool isServer;

	// Token: 0x040014DE RID: 5342
	public ProtoBuf.Item.InstanceData instanceData;

	// Token: 0x040014DF RID: 5343
	public ulong skin;

	// Token: 0x040014E0 RID: 5344
	public string name;

	// Token: 0x040014E1 RID: 5345
	public string text;

	// Token: 0x040014E3 RID: 5347
	public global::Item.Flag flags;

	// Token: 0x040014E4 RID: 5348
	public global::ItemContainer contents;

	// Token: 0x040014E5 RID: 5349
	public global::ItemContainer parent;

	// Token: 0x040014E6 RID: 5350
	private global::EntityRef worldEnt;

	// Token: 0x040014E7 RID: 5351
	private global::EntityRef heldEntity;

	// Token: 0x020004C0 RID: 1216
	[Flags]
	public enum Flag
	{
		// Token: 0x040014EB RID: 5355
		None = 0,
		// Token: 0x040014EC RID: 5356
		Placeholder = 1,
		// Token: 0x040014ED RID: 5357
		IsOn = 2,
		// Token: 0x040014EE RID: 5358
		OnFire = 4,
		// Token: 0x040014EF RID: 5359
		IsLocked = 8,
		// Token: 0x040014F0 RID: 5360
		Cooking = 16
	}
}
