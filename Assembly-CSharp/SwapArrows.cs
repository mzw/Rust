﻿using System;
using Rust;
using UnityEngine;

// Token: 0x020007BF RID: 1983
public class SwapArrows : MonoBehaviour, IClientComponent
{
	// Token: 0x060024CF RID: 9423 RVA: 0x000CA6A8 File Offset: 0x000C88A8
	public void SelectArrowType(int iType)
	{
		this.HideAllArrowHeads();
		this.arrowModels[iType].SetActive(true);
	}

	// Token: 0x060024D0 RID: 9424 RVA: 0x000CA6C0 File Offset: 0x000C88C0
	public void HideAllArrowHeads()
	{
		foreach (GameObject gameObject in this.arrowModels)
		{
			gameObject.SetActive(false);
		}
	}

	// Token: 0x060024D1 RID: 9425 RVA: 0x000CA6F4 File Offset: 0x000C88F4
	public void UpdateAmmoType(global::ItemDefinition ammoType)
	{
		if (this.curAmmoType == ammoType.shortname)
		{
			return;
		}
		this.curAmmoType = ammoType.shortname;
		string text = this.curAmmoType;
		if (text != null && !(text == "ammo_arrow"))
		{
			if (text == "arrow.bone")
			{
				this.SelectArrowType(0);
				return;
			}
			if (text == "arrow.fire")
			{
				this.SelectArrowType(1);
				return;
			}
			if (text == "arrow.hv")
			{
				this.SelectArrowType(2);
				return;
			}
			if (text == "ammo_arrow_poison")
			{
				this.SelectArrowType(3);
				return;
			}
			if (text == "ammo_arrow_stone")
			{
				this.SelectArrowType(4);
				return;
			}
		}
		this.HideAllArrowHeads();
	}

	// Token: 0x060024D2 RID: 9426 RVA: 0x000CA7C4 File Offset: 0x000C89C4
	private void Cleanup()
	{
		this.HideAllArrowHeads();
		this.curAmmoType = string.Empty;
	}

	// Token: 0x060024D3 RID: 9427 RVA: 0x000CA7D8 File Offset: 0x000C89D8
	public void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.Cleanup();
	}

	// Token: 0x060024D4 RID: 9428 RVA: 0x000CA7EC File Offset: 0x000C89EC
	public void OnEnable()
	{
		this.Cleanup();
	}

	// Token: 0x0400204D RID: 8269
	public GameObject[] arrowModels;

	// Token: 0x0400204E RID: 8270
	[NonSerialized]
	private string curAmmoType = string.Empty;

	// Token: 0x020007C0 RID: 1984
	public enum ArrowType
	{
		// Token: 0x04002050 RID: 8272
		One,
		// Token: 0x04002051 RID: 8273
		Two,
		// Token: 0x04002052 RID: 8274
		Three,
		// Token: 0x04002053 RID: 8275
		Four
	}
}
