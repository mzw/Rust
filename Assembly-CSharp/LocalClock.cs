﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000793 RID: 1939
public class LocalClock
{
	// Token: 0x060023DF RID: 9183 RVA: 0x000C678C File Offset: 0x000C498C
	public void Add(float delta, float variance, Action action)
	{
		global::LocalClock.TimedEvent item = default(global::LocalClock.TimedEvent);
		item.time = Time.time + delta + Random.Range(-variance, variance);
		item.delta = delta;
		item.variance = variance;
		item.action = action;
		this.events.Add(item);
	}

	// Token: 0x060023E0 RID: 9184 RVA: 0x000C67DC File Offset: 0x000C49DC
	public void Tick()
	{
		for (int i = 0; i < this.events.Count; i++)
		{
			global::LocalClock.TimedEvent value = this.events[i];
			if (Time.time > value.time)
			{
				float delta = value.delta;
				float variance = value.variance;
				value.action();
				value.time = Time.time + delta + Random.Range(-variance, variance);
				this.events[i] = value;
			}
		}
	}

	// Token: 0x04001FAE RID: 8110
	public List<global::LocalClock.TimedEvent> events = new List<global::LocalClock.TimedEvent>();

	// Token: 0x02000794 RID: 1940
	public struct TimedEvent
	{
		// Token: 0x04001FAF RID: 8111
		public float time;

		// Token: 0x04001FB0 RID: 8112
		public float delta;

		// Token: 0x04001FB1 RID: 8113
		public float variance;

		// Token: 0x04001FB2 RID: 8114
		public Action action;
	}
}
