﻿using System;
using UnityEngine;

// Token: 0x02000469 RID: 1129
public class PrefabParameters : MonoBehaviour
{
	// Token: 0x04001380 RID: 4992
	public global::PrefabPriority Priority = global::PrefabPriority.Default;

	// Token: 0x04001381 RID: 4993
	public int Count = 1;
}
