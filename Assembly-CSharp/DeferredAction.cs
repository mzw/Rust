﻿using System;
using UnityEngine;

// Token: 0x02000749 RID: 1865
public class DeferredAction
{
	// Token: 0x060022F3 RID: 8947 RVA: 0x000C2404 File Offset: 0x000C0604
	public DeferredAction(Object sender, Action action, global::ActionPriority priority = global::ActionPriority.Medium)
	{
		this.sender = sender;
		this.action = action;
		this.priority = priority;
		this.Idle = true;
	}

	// Token: 0x17000278 RID: 632
	// (get) Token: 0x060022F4 RID: 8948 RVA: 0x000C2430 File Offset: 0x000C0630
	// (set) Token: 0x060022F5 RID: 8949 RVA: 0x000C2438 File Offset: 0x000C0638
	public bool Idle { get; private set; }

	// Token: 0x17000279 RID: 633
	// (get) Token: 0x060022F6 RID: 8950 RVA: 0x000C2444 File Offset: 0x000C0644
	public int Index
	{
		get
		{
			return (int)this.priority;
		}
	}

	// Token: 0x060022F7 RID: 8951 RVA: 0x000C244C File Offset: 0x000C064C
	public void Action()
	{
		if (this.Idle)
		{
			throw new Exception("Double invocation of a deferred action.");
		}
		this.Idle = true;
		if (this.sender)
		{
			this.action();
		}
	}

	// Token: 0x060022F8 RID: 8952 RVA: 0x000C2488 File Offset: 0x000C0688
	public void Invoke()
	{
		if (!this.Idle)
		{
			throw new Exception("Double invocation of a deferred action.");
		}
		global::LoadBalancer.Enqueue(this);
		this.Idle = false;
	}

	// Token: 0x060022F9 RID: 8953 RVA: 0x000C24B0 File Offset: 0x000C06B0
	public static implicit operator bool(global::DeferredAction obj)
	{
		return obj != null;
	}

	// Token: 0x060022FA RID: 8954 RVA: 0x000C24BC File Offset: 0x000C06BC
	public static void Invoke(Object sender, Action action, global::ActionPriority priority = global::ActionPriority.Medium)
	{
		new global::DeferredAction(sender, action, priority).Invoke();
	}

	// Token: 0x04001F65 RID: 8037
	private Object sender;

	// Token: 0x04001F66 RID: 8038
	private Action action;

	// Token: 0x04001F67 RID: 8039
	private global::ActionPriority priority = global::ActionPriority.Medium;
}
