﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch;
using Rust;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000426 RID: 1062
public class GameManager
{
	// Token: 0x06001823 RID: 6179 RVA: 0x000893AC File Offset: 0x000875AC
	public GameManager(bool clientside, bool serverside)
	{
		this.Clientside = clientside;
		this.Serverside = serverside;
		this.preProcessed = new global::PrefabPreProcess(clientside, serverside);
		this.pool = new global::PrefabPoolCollection();
	}

	// Token: 0x06001824 RID: 6180 RVA: 0x000893DC File Offset: 0x000875DC
	public uint AddPrefab(string strPrefab, GameObject go)
	{
		uint result = global::StringPool.Add(strPrefab);
		this.preProcessed.AddPrefab(strPrefab, go);
		return result;
	}

	// Token: 0x06001825 RID: 6181 RVA: 0x00089400 File Offset: 0x00087600
	public GameObject FindPrefab(global::BaseEntity ent)
	{
		if (ent == null)
		{
			return null;
		}
		return this.FindPrefab(ent.PrefabName);
	}

	// Token: 0x06001826 RID: 6182 RVA: 0x0008941C File Offset: 0x0008761C
	public GameObject FindPrefab(string strPrefab)
	{
		GameObject gameObject = this.preProcessed.Find(strPrefab);
		if (gameObject != null)
		{
			return gameObject;
		}
		gameObject = global::FileSystem.LoadPrefab(strPrefab);
		if (gameObject == null)
		{
			return null;
		}
		this.preProcessed.Process(strPrefab, gameObject);
		return this.preProcessed.Find(strPrefab);
	}

	// Token: 0x06001827 RID: 6183 RVA: 0x00089474 File Offset: 0x00087674
	public GameObject CreatePrefab(string strPrefab, Vector3 pos, Quaternion rot, Vector3 scale, bool active = true)
	{
		GameObject gameObject = this.Instantiate(strPrefab, pos, rot);
		if (gameObject)
		{
			gameObject.transform.localScale = scale;
			if (active)
			{
				gameObject.AwakeFromInstantiate();
			}
		}
		return gameObject;
	}

	// Token: 0x06001828 RID: 6184 RVA: 0x000894B4 File Offset: 0x000876B4
	public GameObject CreatePrefab(string strPrefab, Vector3 pos, Quaternion rot, bool active = true)
	{
		GameObject gameObject = this.Instantiate(strPrefab, pos, rot);
		if (gameObject && active)
		{
			gameObject.AwakeFromInstantiate();
		}
		return gameObject;
	}

	// Token: 0x06001829 RID: 6185 RVA: 0x000894E4 File Offset: 0x000876E4
	public GameObject CreatePrefab(string strPrefab, bool active = true)
	{
		GameObject gameObject = this.Instantiate(strPrefab, Vector3.zero, Quaternion.identity);
		if (gameObject && active)
		{
			gameObject.AwakeFromInstantiate();
		}
		return gameObject;
	}

	// Token: 0x0600182A RID: 6186 RVA: 0x0008951C File Offset: 0x0008771C
	public GameObject CreatePrefab(string strPrefab, Transform parent, bool active = true)
	{
		GameObject gameObject = this.Instantiate(strPrefab, parent.position, parent.rotation);
		if (gameObject)
		{
			gameObject.transform.SetParent(parent, false);
			gameObject.Identity();
			if (active)
			{
				gameObject.AwakeFromInstantiate();
			}
		}
		return gameObject;
	}

	// Token: 0x0600182B RID: 6187 RVA: 0x00089568 File Offset: 0x00087768
	public global::BaseEntity CreateEntity(string strPrefab, Vector3 pos = default(Vector3), Quaternion rot = default(Quaternion), bool startActive = true)
	{
		if (string.IsNullOrEmpty(strPrefab))
		{
			return null;
		}
		GameObject gameObject = this.CreatePrefab(strPrefab, pos, rot, startActive);
		if (gameObject == null)
		{
			return null;
		}
		global::BaseEntity component = gameObject.GetComponent<global::BaseEntity>();
		if (!component)
		{
			Debug.LogError("CreateEntity called on a prefab that isn't an entity! " + strPrefab);
			Object.Destroy(gameObject);
			return null;
		}
		return component;
	}

	// Token: 0x0600182C RID: 6188 RVA: 0x000895C8 File Offset: 0x000877C8
	private GameObject Instantiate(string strPrefab, Vector3 pos, Quaternion rot)
	{
		if (!StringEx.IsLower(strPrefab))
		{
			Debug.LogWarning("Converting prefab name to lowercase: " + strPrefab);
			strPrefab = strPrefab.ToLower();
		}
		GameObject gameObject = this.FindPrefab(strPrefab);
		if (!gameObject)
		{
			Debug.LogError("Couldn't find prefab \"" + strPrefab + "\"");
			return null;
		}
		GameObject gameObject2 = this.pool.Pop(global::StringPool.Get(strPrefab), pos, rot);
		if (gameObject2 == null)
		{
			gameObject2 = Facepunch.Instantiate.GameObject(gameObject, pos, rot);
			gameObject2.name = strPrefab;
		}
		else
		{
			gameObject2.transform.localScale = gameObject.transform.localScale;
		}
		if (!this.Clientside && this.Serverside && gameObject2.transform.parent == null)
		{
			SceneManager.MoveGameObjectToScene(gameObject2, Rust.Server.EntityScene);
		}
		return gameObject2;
	}

	// Token: 0x0600182D RID: 6189 RVA: 0x000896A8 File Offset: 0x000878A8
	public static void Destroy(Component component, float delay = 0f)
	{
		global::BaseEntity ent = component as global::BaseEntity;
		if (ent.IsValid())
		{
			Debug.LogError("Trying to destroy an entity without killing it first: " + component.name);
		}
		Object.Destroy(component, delay);
	}

	// Token: 0x0600182E RID: 6190 RVA: 0x000896E4 File Offset: 0x000878E4
	public static void Destroy(GameObject instance, float delay = 0f)
	{
		if (!instance)
		{
			return;
		}
		global::BaseEntity component = instance.GetComponent<global::BaseEntity>();
		if (component.IsValid())
		{
			Debug.LogError("Trying to destroy an entity without killing it first: " + instance.name);
		}
		Object.Destroy(instance, delay);
	}

	// Token: 0x0600182F RID: 6191 RVA: 0x0008972C File Offset: 0x0008792C
	public static void DestroyImmediate(Component component, bool allowDestroyingAssets = false)
	{
		global::BaseEntity ent = component as global::BaseEntity;
		if (ent.IsValid())
		{
			Debug.LogError("Trying to destroy an entity without killing it first: " + component.name);
		}
		Object.DestroyImmediate(component, allowDestroyingAssets);
	}

	// Token: 0x06001830 RID: 6192 RVA: 0x00089768 File Offset: 0x00087968
	public static void DestroyImmediate(GameObject instance, bool allowDestroyingAssets = false)
	{
		global::BaseEntity component = instance.GetComponent<global::BaseEntity>();
		if (component.IsValid())
		{
			Debug.LogError("Trying to destroy an entity without killing it first: " + instance.name);
		}
		Object.DestroyImmediate(instance, allowDestroyingAssets);
	}

	// Token: 0x06001831 RID: 6193 RVA: 0x000897A4 File Offset: 0x000879A4
	public void Retire(GameObject instance)
	{
		if (!instance)
		{
			return;
		}
		using (TimeWarning.New("GameManager.Retire", 0.1f))
		{
			global::BaseEntity component = instance.GetComponent<global::BaseEntity>();
			if (component.IsValid())
			{
				Debug.LogError("Trying to retire an entity without killing it first: " + instance.name);
			}
			if (!Application.isQuitting && instance.SupportsPooling())
			{
				this.pool.Push(instance);
			}
			else
			{
				Object.Destroy(instance);
			}
		}
	}

	// Token: 0x06001832 RID: 6194 RVA: 0x00089844 File Offset: 0x00087A44
	public static void Warmup()
	{
		global::GameManager.server.WarmupInternal();
	}

	// Token: 0x06001833 RID: 6195 RVA: 0x00089850 File Offset: 0x00087A50
	private void WarmupInternal()
	{
		IEnumerable<global::GameManifest.PrefabProperties> enumerable = from x in global::GameManifest.Current.prefabProperties
		where x.needsWarmup
		select x;
		foreach (global::GameManifest.PrefabProperties prefabProperties in enumerable)
		{
			if (this.FindPrefab(prefabProperties.name) == null)
			{
				Debug.LogError("Couldn't Warmup " + prefabProperties.name);
			}
		}
	}

	// Token: 0x06001834 RID: 6196 RVA: 0x000898F8 File Offset: 0x00087AF8
	public void Warmup(uint prefabID)
	{
		string text = global::StringPool.Get(prefabID);
		if (string.IsNullOrEmpty(text))
		{
			return;
		}
		this.FindPrefab(text);
	}

	// Token: 0x040012BE RID: 4798
	public static global::GameManager server = new global::GameManager(false, true);

	// Token: 0x040012BF RID: 4799
	internal global::PrefabPreProcess preProcessed;

	// Token: 0x040012C0 RID: 4800
	internal global::PrefabPoolCollection pool;

	// Token: 0x040012C1 RID: 4801
	private bool Clientside;

	// Token: 0x040012C2 RID: 4802
	private bool Serverside;
}
