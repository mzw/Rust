﻿using System;
using UnityEngine.SceneManagement;

namespace Rust
{
	// Token: 0x0200047D RID: 1149
	public static class Server
	{
		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x060018F1 RID: 6385 RVA: 0x0008C564 File Offset: 0x0008A764
		public static Scene EntityScene
		{
			get
			{
				if (!Server._entityScene.IsValid())
				{
					Server._entityScene = SceneManager.CreateScene("Server Entities");
				}
				return Server._entityScene;
			}
		}

		// Token: 0x040013AD RID: 5037
		public const float UseDistance = 3f;

		// Token: 0x040013AE RID: 5038
		private static Scene _entityScene;
	}
}
