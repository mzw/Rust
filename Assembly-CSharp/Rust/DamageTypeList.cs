﻿using System;
using System.Collections.Generic;

namespace Rust
{
	// Token: 0x0200040C RID: 1036
	public class DamageTypeList
	{
		// Token: 0x060017D2 RID: 6098 RVA: 0x00088458 File Offset: 0x00086658
		public void Set(DamageType index, float amount)
		{
			this.types[(int)index] = amount;
		}

		// Token: 0x060017D3 RID: 6099 RVA: 0x00088464 File Offset: 0x00086664
		public float Get(DamageType index)
		{
			return this.types[(int)index];
		}

		// Token: 0x060017D4 RID: 6100 RVA: 0x00088470 File Offset: 0x00086670
		public void Add(DamageType index, float amount)
		{
			this.Set(index, this.Get(index) + amount);
		}

		// Token: 0x060017D5 RID: 6101 RVA: 0x00088484 File Offset: 0x00086684
		public void Scale(DamageType index, float amount)
		{
			this.Set(index, this.Get(index) * amount);
		}

		// Token: 0x060017D6 RID: 6102 RVA: 0x00088498 File Offset: 0x00086698
		public bool Has(DamageType index)
		{
			return this.Get(index) > 0f;
		}

		// Token: 0x060017D7 RID: 6103 RVA: 0x000884A8 File Offset: 0x000866A8
		public float Total()
		{
			float num = 0f;
			for (int i = 0; i < this.types.Length; i++)
			{
				float num2 = this.types[i];
				if (!float.IsNaN(num2))
				{
					if (!float.IsInfinity(num2))
					{
						num += num2;
					}
				}
			}
			return num;
		}

		// Token: 0x060017D8 RID: 6104 RVA: 0x00088504 File Offset: 0x00086704
		public void Add(List<DamageTypeEntry> entries)
		{
			foreach (DamageTypeEntry damageTypeEntry in entries)
			{
				this.Add(damageTypeEntry.type, damageTypeEntry.amount);
			}
		}

		// Token: 0x060017D9 RID: 6105 RVA: 0x00088568 File Offset: 0x00086768
		public void ScaleAll(float amount)
		{
			for (int i = 0; i < this.types.Length; i++)
			{
				this.Scale((DamageType)i, amount);
			}
		}

		// Token: 0x060017DA RID: 6106 RVA: 0x00088598 File Offset: 0x00086798
		public DamageType GetMajorityDamageType()
		{
			int result = 0;
			float num = 0f;
			for (int i = 0; i < this.types.Length; i++)
			{
				float num2 = this.types[i];
				if (!float.IsNaN(num2))
				{
					if (!float.IsInfinity(num2))
					{
						if (num2 >= num)
						{
							result = i;
							num = num2;
						}
					}
				}
			}
			return (DamageType)result;
		}

		// Token: 0x060017DB RID: 6107 RVA: 0x00088600 File Offset: 0x00086800
		public bool IsMeleeType()
		{
			DamageType majorityDamageType = this.GetMajorityDamageType();
			return majorityDamageType == DamageType.Blunt || majorityDamageType == DamageType.Slash || majorityDamageType == DamageType.Stab;
		}

		// Token: 0x060017DC RID: 6108 RVA: 0x0008862C File Offset: 0x0008682C
		public bool IsBleedCausing()
		{
			DamageType majorityDamageType = this.GetMajorityDamageType();
			return majorityDamageType == DamageType.Bite || majorityDamageType == DamageType.Slash || majorityDamageType == DamageType.Stab || majorityDamageType == DamageType.Bullet || majorityDamageType == DamageType.Arrow;
		}

		// Token: 0x04001271 RID: 4721
		public float[] types = new float[22];
	}
}
