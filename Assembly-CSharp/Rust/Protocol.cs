﻿using System;

namespace Rust
{
	// Token: 0x02000478 RID: 1144
	public static class Protocol
	{
		// Token: 0x170001AF RID: 431
		// (get) Token: 0x060018EE RID: 6382 RVA: 0x0008C4F0 File Offset: 0x0008A6F0
		public static string printable
		{
			get
			{
				return string.Concat(new object[]
				{
					2065,
					".",
					158,
					".",
					1
				});
			}
		}

		// Token: 0x040013A2 RID: 5026
		public const int network = 2065;

		// Token: 0x040013A3 RID: 5027
		public const int save = 158;

		// Token: 0x040013A4 RID: 5028
		public const int report = 1;

		// Token: 0x040013A5 RID: 5029
		public const int persistance = 2;

		// Token: 0x040013A6 RID: 5030
		public const int storage = 0;
	}
}
