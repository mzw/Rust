﻿using System;
using ConVar;
using UnityEngine;

namespace Rust
{
	// Token: 0x02000425 RID: 1061
	internal static class GameInfo
	{
		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06001821 RID: 6177 RVA: 0x00089390 File Offset: 0x00087590
		internal static bool IsOfficialServer
		{
			get
			{
				return Application.isEditor || ConVar.Server.official;
			}
		}

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x06001822 RID: 6178 RVA: 0x000893A4 File Offset: 0x000875A4
		internal static bool HasAchievements
		{
			get
			{
				return GameInfo.IsOfficialServer;
			}
		}
	}
}
