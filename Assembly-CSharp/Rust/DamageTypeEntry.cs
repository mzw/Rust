﻿using System;

namespace Rust
{
	// Token: 0x0200040D RID: 1037
	[Serializable]
	public class DamageTypeEntry
	{
		// Token: 0x04001272 RID: 4722
		public DamageType type;

		// Token: 0x04001273 RID: 4723
		public float amount;
	}
}
