﻿using System;

namespace Rust
{
	// Token: 0x0200040B RID: 1035
	public enum DamageType
	{
		// Token: 0x0400125A RID: 4698
		Generic,
		// Token: 0x0400125B RID: 4699
		Hunger,
		// Token: 0x0400125C RID: 4700
		Thirst,
		// Token: 0x0400125D RID: 4701
		Cold,
		// Token: 0x0400125E RID: 4702
		Drowned,
		// Token: 0x0400125F RID: 4703
		Heat,
		// Token: 0x04001260 RID: 4704
		Bleeding,
		// Token: 0x04001261 RID: 4705
		Poison,
		// Token: 0x04001262 RID: 4706
		Suicide,
		// Token: 0x04001263 RID: 4707
		Bullet,
		// Token: 0x04001264 RID: 4708
		Slash,
		// Token: 0x04001265 RID: 4709
		Blunt,
		// Token: 0x04001266 RID: 4710
		Fall,
		// Token: 0x04001267 RID: 4711
		Radiation,
		// Token: 0x04001268 RID: 4712
		Bite,
		// Token: 0x04001269 RID: 4713
		Stab,
		// Token: 0x0400126A RID: 4714
		Explosion,
		// Token: 0x0400126B RID: 4715
		RadiationExposure,
		// Token: 0x0400126C RID: 4716
		ColdExposure,
		// Token: 0x0400126D RID: 4717
		Decay,
		// Token: 0x0400126E RID: 4718
		ElectricShock,
		// Token: 0x0400126F RID: 4719
		Arrow,
		// Token: 0x04001270 RID: 4720
		LAST
	}
}
