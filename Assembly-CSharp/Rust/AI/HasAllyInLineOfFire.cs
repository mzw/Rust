﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200016D RID: 365
	public class HasAllyInLineOfFire : BaseScorer
	{
		// Token: 0x06000D28 RID: 3368 RVA: 0x00053524 File Offset: 0x00051724
		public override float GetScore(BaseContext ctx)
		{
			global::NPCHumanContext npchumanContext = ctx as global::NPCHumanContext;
			if (npchumanContext != null)
			{
				global::Scientist scientist = npchumanContext.Human as global::Scientist;
				List<global::Scientist> list;
				if (scientist != null && scientist.GetAlliesInRange(out list) > 0)
				{
					foreach (global::Scientist scientist2 in list)
					{
						Vector3 normalized = (npchumanContext.EnemyPosition - npchumanContext.Position).normalized;
						Vector3 normalized2 = (scientist2.Entity.ServerPosition - npchumanContext.Position).normalized;
						float num = Vector3.Dot(normalized, normalized2);
						if (num > 0.8f)
						{
							return 1f;
						}
					}
				}
			}
			return 0f;
		}
	}
}
