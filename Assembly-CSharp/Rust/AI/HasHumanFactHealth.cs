﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200016B RID: 363
	public class HasHumanFactHealth : BaseScorer
	{
		// Token: 0x06000D24 RID: 3364 RVA: 0x00053428 File Offset: 0x00051628
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::NPCPlayerApex.Facts.Health);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x04000736 RID: 1846
		[ApexSerialization(defaultValue = global::NPCPlayerApex.HealthEnum.Fine)]
		public global::NPCPlayerApex.HealthEnum value;
	}
}
