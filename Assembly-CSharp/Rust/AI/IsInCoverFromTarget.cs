﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200014E RID: 334
	public class IsInCoverFromTarget : BaseScorer
	{
		// Token: 0x06000CE8 RID: 3304 RVA: 0x00052594 File Offset: 0x00050794
		public override float GetScore(BaseContext ctx)
		{
			if (SingletonComponent<AiManager>.Instance == null || !SingletonComponent<AiManager>.Instance.enabled || !SingletonComponent<AiManager>.Instance.UseCover || ctx.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			if (!(ctx is global::NPCHumanContext))
			{
				return 0f;
			}
			return 0f;
		}

		// Token: 0x04000707 RID: 1799
		[ApexSerialization]
		public float CoverArcThreshold = -0.75f;
	}
}
