﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000141 RID: 321
	public class ProximityToDanger : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CD0 RID: 3280 RVA: 0x0005202C File Offset: 0x0005022C
		public override float GetScore(BaseContext c, Vector3 position)
		{
			float num = 0f;
			for (int i = 0; i < c.Memory.All.Count; i++)
			{
				float num2 = Vector3.Distance(position, c.Memory.All[i].Position) / this.Range;
				num2 = 1f - num2;
				if (num2 >= 0f)
				{
					num += c.Memory.All[i].Danger * num2;
				}
			}
			return num;
		}

		// Token: 0x040006FD RID: 1789
		[ApexSerialization]
		public float Range = 20f;
	}
}
