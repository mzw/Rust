﻿using System;
using System.Collections.Generic;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200017A RID: 378
	public class NavigateToOperator : BaseAction
	{
		// Token: 0x06000D50 RID: 3408 RVA: 0x000542D8 File Offset: 0x000524D8
		public override void DoExecute(BaseContext c)
		{
			if (c.GetFact(global::BaseNpc.Facts.CanNotMove) == 1)
			{
				c.AIAgent.StopMoving();
				return;
			}
			if (!c.AIAgent.IsNavRunning() || c.AIAgent.GetNavAgent.pathPending)
			{
				return;
			}
			switch (this.Operator)
			{
			case NavigateToOperator.OperatorType.EnemyLoc:
				NavigateToOperator.NavigateToEnemy(c);
				break;
			case NavigateToOperator.OperatorType.RandomLoc:
				NavigateToOperator.NavigateToRandomLoc(c);
				break;
			case NavigateToOperator.OperatorType.Spawn:
				NavigateToOperator.NavigateToSpawn(c);
				break;
			case NavigateToOperator.OperatorType.FoodLoc:
				NavigateToOperator.NavigateToFood(c);
				break;
			case NavigateToOperator.OperatorType.FleeEnemy:
				NavigateToOperator.FleeEnemy(c);
				break;
			case NavigateToOperator.OperatorType.FleeHurtDir:
				NavigateToOperator.FleeHurtDir(c);
				break;
			}
		}

		// Token: 0x06000D51 RID: 3409 RVA: 0x00054394 File Offset: 0x00052594
		public static void MakeUnstuck(BaseContext c)
		{
			global::BaseNpc baseNpc = c.Entity as global::BaseNpc;
			if (baseNpc)
			{
				baseNpc.stuckDuration = 0f;
				baseNpc.IsStuck = false;
			}
		}

		// Token: 0x06000D52 RID: 3410 RVA: 0x000543CC File Offset: 0x000525CC
		public static void NavigateToEnemy(BaseContext c)
		{
			if (c.GetFact(global::BaseNpc.Facts.HasEnemy) > 0 && c.AIAgent.IsNavRunning())
			{
				NavigateToOperator.MakeUnstuck(c);
				c.AIAgent.GetNavAgent.destination = c.EnemyPosition;
			}
		}

		// Token: 0x06000D53 RID: 3411 RVA: 0x00054408 File Offset: 0x00052608
		public static void NavigateToRandomLoc(BaseContext c)
		{
			if (IsRoamReady.Evaluate(c) && c.AIAgent.IsNavRunning() && NavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.DiscourageSharpTurns, c.AIAgent.GetStats.MinRoamRange, c.AIAgent.GetStats.MaxRoamRange))
			{
				float num = c.AIAgent.GetStats.MaxRoamDelay - c.AIAgent.GetStats.MinRoamDelay;
				float num2 = Random.value * num;
				float num3 = num2 / num;
				float num4 = c.AIAgent.GetStats.RoamDelayDistribution.Evaluate(num3);
				float num5 = num4 * num;
				c.NextRoamTime = Time.realtimeSinceStartup + c.AIAgent.GetStats.MinRoamDelay + num5;
			}
		}

		// Token: 0x06000D54 RID: 3412 RVA: 0x000544E8 File Offset: 0x000526E8
		public static void NavigateToSpawn(BaseContext c)
		{
			if (c.AIAgent.IsNavRunning())
			{
				NavigateToOperator.MakeUnstuck(c);
				c.AIAgent.GetNavAgent.destination = c.AIAgent.SpawnPosition;
			}
		}

		// Token: 0x06000D55 RID: 3413 RVA: 0x0005451C File Offset: 0x0005271C
		public static void NavigateToFood(BaseContext c)
		{
			if (c.AIAgent.FoodTarget != null && !c.AIAgent.FoodTarget.IsDestroyed && c.AIAgent.FoodTarget.transform != null && c.GetFact(global::BaseNpc.Facts.FoodRange) < 2 && c.AIAgent.IsNavRunning())
			{
				NavigateToOperator.MakeUnstuck(c);
				c.AIAgent.GetNavAgent.destination = c.AIAgent.FoodTarget.ServerPosition;
			}
		}

		// Token: 0x06000D56 RID: 3414 RVA: 0x000545B4 File Offset: 0x000527B4
		public static void FleeEnemy(BaseContext c)
		{
			if (c.AIAgent.IsNavRunning() && NavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.RetreatFromTarget, c.AIAgent.GetStats.MinFleeRange, c.AIAgent.GetStats.MaxFleeRange))
			{
				c.SetFact(global::BaseNpc.Facts.IsFleeing, 1);
			}
		}

		// Token: 0x06000D57 RID: 3415 RVA: 0x00054614 File Offset: 0x00052814
		public static void FleeHurtDir(BaseContext c)
		{
			if (c.AIAgent.IsNavRunning() && NavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.RetreatFromDirection, c.AIAgent.GetStats.MinFleeRange, c.AIAgent.GetStats.MaxFleeRange))
			{
				c.SetFact(global::BaseNpc.Facts.IsFleeing, 1);
			}
		}

		// Token: 0x06000D58 RID: 3416 RVA: 0x00054674 File Offset: 0x00052874
		private static bool NavigateInDirOfBestSample(BaseContext c, NavPointSampler.SampleCount sampleCount, float radius, NavPointSampler.SampleFeatures features, float minRange, float maxRange)
		{
			List<NavPointSample> list = c.AIAgent.RequestNavPointSamplesInCircle(sampleCount, radius, features);
			if (list == null)
			{
				return false;
			}
			foreach (NavPointSample navPointSample in list)
			{
				Vector3 normalized = (navPointSample.Position - c.Position).normalized;
				Vector3 vector = c.Position + (normalized * minRange + normalized * ((maxRange - minRange) * Random.value));
				NavPointSample navPointSample2 = NavPointSampler.SamplePoint(vector, new NavPointSampler.SampleScoreParams
				{
					WaterMaxDepth = c.AIAgent.GetStats.MaxWaterDepth,
					Agent = c.AIAgent,
					Features = features
				});
				if (!Mathf.Approximately(navPointSample2.Score, 0f))
				{
					NavigateToOperator.MakeUnstuck(c);
					vector = navPointSample2.Position;
					c.AIAgent.GetNavAgent.destination = vector;
					return true;
				}
			}
			return false;
		}

		// Token: 0x04000752 RID: 1874
		[ApexSerialization]
		public NavigateToOperator.OperatorType Operator;

		// Token: 0x0200017B RID: 379
		public enum OperatorType
		{
			// Token: 0x04000754 RID: 1876
			EnemyLoc,
			// Token: 0x04000755 RID: 1877
			RandomLoc,
			// Token: 0x04000756 RID: 1878
			Spawn,
			// Token: 0x04000757 RID: 1879
			FoodLoc,
			// Token: 0x04000758 RID: 1880
			FleeEnemy,
			// Token: 0x04000759 RID: 1881
			FleeHurtDir
		}
	}
}
