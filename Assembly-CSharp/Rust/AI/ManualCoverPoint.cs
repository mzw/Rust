﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200019B RID: 411
	public class ManualCoverPoint : FacepunchBehaviour
	{
		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000DB3 RID: 3507 RVA: 0x00055AD8 File Offset: 0x00053CD8
		public Vector3 Position
		{
			get
			{
				return base.transform.position;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000DB4 RID: 3508 RVA: 0x00055AE8 File Offset: 0x00053CE8
		public float DirectionMagnitude
		{
			get
			{
				if (this.Volume != null)
				{
					return this.Volume.CoverPointRayLength;
				}
				return 1f;
			}
		}

		// Token: 0x06000DB5 RID: 3509 RVA: 0x00055B0C File Offset: 0x00053D0C
		private void Awake()
		{
			if (base.transform.parent != null)
			{
				this.Volume = base.transform.parent.GetComponent<CoverPointVolume>();
			}
		}

		// Token: 0x06000DB6 RID: 3510 RVA: 0x00055B3C File Offset: 0x00053D3C
		public CoverPoint ToCoverPoint(CoverPointVolume volume)
		{
			this.Volume = volume;
			Vector3 normalized = (base.transform.rotation * this.Normal).normalized;
			return new CoverPoint(this.Volume, this.Score)
			{
				Position = base.transform.position,
				Normal = normalized,
				NormalCoverType = this.NormalCoverType
			};
		}

		// Token: 0x040007AD RID: 1965
		public float Score = 2f;

		// Token: 0x040007AE RID: 1966
		public CoverPointVolume Volume;

		// Token: 0x040007AF RID: 1967
		public Vector3 Normal;

		// Token: 0x040007B0 RID: 1968
		public CoverPoint.CoverType NormalCoverType;
	}
}
