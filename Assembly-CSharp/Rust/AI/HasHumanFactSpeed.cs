﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000164 RID: 356
	public class HasHumanFactSpeed : BaseScorer
	{
		// Token: 0x06000D18 RID: 3352 RVA: 0x00053234 File Offset: 0x00051434
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::NPCPlayerApex.Facts.Speed);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x04000727 RID: 1831
		[ApexSerialization(defaultValue = global::NPCPlayerApex.SpeedEnum.StandStill)]
		public global::NPCPlayerApex.SpeedEnum value;
	}
}
