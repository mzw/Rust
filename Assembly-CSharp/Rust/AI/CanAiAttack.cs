﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200014B RID: 331
	public sealed class CanAiAttack : BaseScorer
	{
		// Token: 0x06000CE2 RID: 3298 RVA: 0x000524BC File Offset: 0x000506BC
		public override float GetScore(BaseContext c)
		{
			global::BasePlayer basePlayer = c.AIAgent as global::BasePlayer;
			if (basePlayer != null)
			{
				global::HeldEntity heldEntity = basePlayer.GetHeldEntity();
				global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
				if (attackEntity != null)
				{
					return 1f;
				}
			}
			return 0f;
		}
	}
}
