﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001AD RID: 429
	public class Memory
	{
		// Token: 0x06000E41 RID: 3649 RVA: 0x00059CB8 File Offset: 0x00057EB8
		public void Update(global::BaseEntity ent, float danger = 0f)
		{
			for (int i = 0; i < this.All.Count; i++)
			{
				if (this.All[i].Entity == ent)
				{
					Memory.SeenInfo value = this.All[i];
					value.Position = ent.transform.position;
					value.Timestamp = Mathf.Max(Time.realtimeSinceStartup, value.Timestamp);
					value.Danger += danger;
					this.All[i] = value;
					return;
				}
			}
			this.All.Add(new Memory.SeenInfo
			{
				Entity = ent,
				Position = ent.transform.position,
				Timestamp = Time.realtimeSinceStartup,
				Danger = danger
			});
			this.Visible.Add(ent);
		}

		// Token: 0x06000E42 RID: 3650 RVA: 0x00059DA4 File Offset: 0x00057FA4
		public void AddDanger(Vector3 position, float amount)
		{
			for (int i = 0; i < this.All.Count; i++)
			{
				if (Mathf.Approximately(this.All[i].Position.x, position.x) && Mathf.Approximately(this.All[i].Position.y, position.y) && Mathf.Approximately(this.All[i].Position.z, position.z))
				{
					Memory.SeenInfo value = this.All[i];
					value.Danger = amount;
					this.All[i] = value;
					return;
				}
			}
			this.All.Add(new Memory.SeenInfo
			{
				Position = position,
				Timestamp = Time.realtimeSinceStartup,
				Danger = amount
			});
		}

		// Token: 0x06000E43 RID: 3651 RVA: 0x00059EA4 File Offset: 0x000580A4
		public Memory.SeenInfo GetInfo(global::BaseEntity entity)
		{
			foreach (Memory.SeenInfo result in this.All)
			{
				if (result.Entity == entity)
				{
					return result;
				}
			}
			return default(Memory.SeenInfo);
		}

		// Token: 0x06000E44 RID: 3652 RVA: 0x00059F20 File Offset: 0x00058120
		internal void Forget(float secondsOld)
		{
			for (int i = 0; i < this.All.Count; i++)
			{
				if (Time.realtimeSinceStartup - this.All[i].Timestamp > secondsOld)
				{
					if (this.All[i].Entity != null)
					{
						this.Visible.Remove(this.All[i].Entity);
					}
					this.All.RemoveAt(i);
					i--;
				}
			}
		}

		// Token: 0x04000840 RID: 2112
		public List<global::BaseEntity> Visible = new List<global::BaseEntity>();

		// Token: 0x04000841 RID: 2113
		public List<Memory.SeenInfo> All = new List<Memory.SeenInfo>();

		// Token: 0x020001AE RID: 430
		public struct SeenInfo
		{
			// Token: 0x04000842 RID: 2114
			public global::BaseEntity Entity;

			// Token: 0x04000843 RID: 2115
			public Vector3 Position;

			// Token: 0x04000844 RID: 2116
			public float Timestamp;

			// Token: 0x04000845 RID: 2117
			public float Danger;
		}
	}
}
