﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000181 RID: 385
	public sealed class EntityDistance : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000D67 RID: 3431 RVA: 0x00054A20 File Offset: 0x00052C20
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			if (target == null)
			{
				return 1f;
			}
			return Vector3.Distance(target.ServerPosition, c.Position) / this.DistanceScope;
		}

		// Token: 0x04000761 RID: 1889
		[ApexSerialization(defaultValue = 10f)]
		public float DistanceScope = 10f;
	}
}
