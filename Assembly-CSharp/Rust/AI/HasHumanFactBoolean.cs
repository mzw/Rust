﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000162 RID: 354
	public class HasHumanFactBoolean : BaseScorer
	{
		// Token: 0x06000D14 RID: 3348 RVA: 0x000530B4 File Offset: 0x000512B4
		public override float GetScore(BaseContext c)
		{
			byte b = (!this.value) ? 0 : 1;
			byte b2 = c.GetFact(this.fact);
			return (b2 != b) ? 0f : 1f;
		}

		// Token: 0x04000724 RID: 1828
		[ApexSerialization]
		public global::NPCPlayerApex.Facts fact;

		// Token: 0x04000725 RID: 1829
		[ApexSerialization(defaultValue = false)]
		public bool value;
	}
}
