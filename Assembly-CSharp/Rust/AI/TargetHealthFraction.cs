﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000128 RID: 296
	public sealed class TargetHealthFraction : BaseScorer
	{
		// Token: 0x06000CA0 RID: 3232 RVA: 0x000515D0 File Offset: 0x0004F7D0
		public override float GetScore(BaseContext c)
		{
			global::BaseCombatEntity combatTarget = c.AIAgent.CombatTarget;
			return (!(combatTarget == null)) ? combatTarget.healthFraction : 0f;
		}
	}
}
