﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000166 RID: 358
	public class SetHumanFactBoolean : BaseAction
	{
		// Token: 0x06000D1C RID: 3356 RVA: 0x000532A4 File Offset: 0x000514A4
		public override void DoExecute(BaseContext c)
		{
			c.SetFact(this.fact, (!this.value) ? 0 : 1, true, true);
		}

		// Token: 0x04000729 RID: 1833
		[ApexSerialization]
		public global::NPCPlayerApex.Facts fact;

		// Token: 0x0400072A RID: 1834
		[ApexSerialization(defaultValue = false)]
		public bool value;
	}
}
