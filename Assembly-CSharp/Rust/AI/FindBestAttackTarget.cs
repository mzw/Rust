﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x020000F7 RID: 247
	public class FindBestAttackTarget : BaseActionWithOptions<global::BaseEntity>
	{
		// Token: 0x06000C35 RID: 3125 RVA: 0x0004FFF4 File Offset: 0x0004E1F4
		public override void DoExecute(BaseContext c)
		{
			global::BaseEntity baseEntity;
			float num;
			if (!base.TryGetBest(c, c.Memory.Visible, this.AllScorersMustScoreAboveZero, out baseEntity, out num) || num < this.ScoreThreshold)
			{
				global::NPCHumanContext npchumanContext = c as global::NPCHumanContext;
				if (npchumanContext != null && c.AIAgent.GetWantsToAttack(npchumanContext.LastAttacker) > 0f)
				{
					c.AIAgent.AttackTarget = npchumanContext.LastAttacker;
				}
				else
				{
					c.AIAgent.AttackTarget = null;
				}
			}
			else
			{
				if (c.AIAgent.GetWantsToAttack(baseEntity) < 0.1f)
				{
					baseEntity = null;
				}
				c.AIAgent.AttackTarget = baseEntity;
			}
			if (c.AIAgent.AttackTarget != null)
			{
				foreach (Memory.SeenInfo attackTargetMemory in c.Memory.All)
				{
					if (attackTargetMemory.Entity == baseEntity)
					{
						c.AIAgent.AttackTargetMemory = attackTargetMemory;
						break;
					}
				}
			}
		}

		// Token: 0x040006B9 RID: 1721
		[ApexSerialization]
		public float ScoreThreshold;

		// Token: 0x040006BA RID: 1722
		[ApexSerialization]
		public bool AllScorersMustScoreAboveZero;
	}
}
