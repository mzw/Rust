﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200017C RID: 380
	public class IsRoamReady : BaseScorer
	{
		// Token: 0x06000D5A RID: 3418 RVA: 0x000547B8 File Offset: 0x000529B8
		public override float GetScore(BaseContext c)
		{
			return (float)((!IsRoamReady.Evaluate(c)) ? 0 : 1);
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x000547D0 File Offset: 0x000529D0
		public static bool Evaluate(BaseContext c)
		{
			if (c is global::NPCHumanContext)
			{
				return c.GetFact(global::NPCPlayerApex.Facts.IsRoamReady) > 0;
			}
			return c.GetFact(global::BaseNpc.Facts.IsRoamReady) > 0;
		}
	}
}
