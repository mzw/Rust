﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001B6 RID: 438
	public struct NavPointSample
	{
		// Token: 0x0400088C RID: 2188
		public Vector3 Position;

		// Token: 0x0400088D RID: 2189
		public float Score;
	}
}
