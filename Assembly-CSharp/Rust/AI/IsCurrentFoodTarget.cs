﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000110 RID: 272
	public sealed class IsCurrentFoodTarget : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C6B RID: 3179 RVA: 0x00050A38 File Offset: 0x0004EC38
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			return (float)((!(c.AIAgent.FoodTarget == target)) ? 0 : 1);
		}
	}
}
