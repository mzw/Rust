﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000104 RID: 260
	public sealed class DistanceFromTargetDestination : BaseScorer
	{
		// Token: 0x06000C50 RID: 3152 RVA: 0x00050538 File Offset: 0x0004E738
		public override float GetScore(BaseContext c)
		{
			if (!c.AIAgent.IsNavRunning())
			{
				return 1f;
			}
			return Vector3.Distance(c.Entity.ServerPosition, c.AIAgent.GetNavAgent.destination) / this.MaxDistance;
		}

		// Token: 0x040006C0 RID: 1728
		[ApexSerialization(defaultValue = 10f)]
		public float MaxDistance = 10f;
	}
}
