﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200018A RID: 394
	public class EntityProximityToCover : BaseScorer
	{
		// Token: 0x06000D76 RID: 3446 RVA: 0x00054DD0 File Offset: 0x00052FD0
		public override float GetScore(BaseContext ctx)
		{
			global::NPCHumanContext npchumanContext = ctx as global::NPCHumanContext;
			if (npchumanContext != null)
			{
				float num;
				CoverPoint closestCover = ProximityToCover.GetClosestCover(npchumanContext, npchumanContext.Position, this.MaxDistance, this._coverType, out num);
				if (closestCover != null)
				{
					return this.Response.Evaluate(num / this.MaxDistance) * closestCover.Score;
				}
			}
			return 0f;
		}

		// Token: 0x04000772 RID: 1906
		[ApexSerialization]
		public float MaxDistance = 20f;

		// Token: 0x04000773 RID: 1907
		[ApexSerialization]
		public ProximityToCover.CoverType _coverType;

		// Token: 0x04000774 RID: 1908
		[ApexSerialization]
		public AnimationCurve Response = AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);
	}
}
