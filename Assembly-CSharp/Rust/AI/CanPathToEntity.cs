﻿using System;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x02000101 RID: 257
	public sealed class CanPathToEntity : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C49 RID: 3145 RVA: 0x000503CC File Offset: 0x0004E5CC
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			if (c.AIAgent.IsNavRunning() && c.AIAgent.GetNavAgent.CalculatePath(target.ServerPosition, CanPathToEntity.pathToEntity) && CanPathToEntity.pathToEntity.status == null)
			{
				return 1f;
			}
			return 0f;
		}

		// Token: 0x040006BE RID: 1726
		private static readonly NavMeshPath pathToEntity = new NavMeshPath();
	}
}
