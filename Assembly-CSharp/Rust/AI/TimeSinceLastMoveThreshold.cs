﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000174 RID: 372
	public class TimeSinceLastMoveThreshold : BaseScorer
	{
		// Token: 0x06000D42 RID: 3394 RVA: 0x00054120 File Offset: 0x00052320
		public override float GetScore(BaseContext c)
		{
			return (float)((!TimeSinceLastMoveThreshold.Evaluate(c as global::NPCHumanContext, this.minThreshold, this.maxThreshold)) ? 0 : 1);
		}

		// Token: 0x06000D43 RID: 3395 RVA: 0x00054148 File Offset: 0x00052348
		public static bool Evaluate(global::NPCHumanContext c, float minThreshold, float maxThreshold)
		{
			if (Mathf.Approximately(c.Human.TimeLastMoved, 0f))
			{
				return true;
			}
			float num = Time.realtimeSinceStartup - c.Human.TimeLastMoved;
			if (c.GetFact(global::NPCPlayerApex.Facts.IsMoving) > 0 || num < minThreshold)
			{
				return false;
			}
			if (num >= maxThreshold)
			{
				return true;
			}
			float num2 = maxThreshold - minThreshold;
			float num3 = maxThreshold - num;
			return Random.value < num3 / num2;
		}

		// Token: 0x0400074B RID: 1867
		[ApexSerialization]
		public float minThreshold;

		// Token: 0x0400074C RID: 1868
		[ApexSerialization]
		public float maxThreshold = 1f;
	}
}
