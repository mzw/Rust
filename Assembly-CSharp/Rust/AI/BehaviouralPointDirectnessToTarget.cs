﻿using System;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000184 RID: 388
	public class BehaviouralPointDirectnessToTarget : PointDirectnessToTarget
	{
		// Token: 0x06000D6D RID: 3437 RVA: 0x00054B18 File Offset: 0x00052D18
		public override float GetScore(BaseContext c, Vector3 point)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			float score = base.GetScore(c, point);
			switch (this.guide)
			{
			case BehaviouralPointDirectnessToTarget.Guide.Approach:
				if (this.minDirectness > 0f && score >= this.minDirectness)
				{
					return 1f;
				}
				break;
			case BehaviouralPointDirectnessToTarget.Guide.Retreat:
				if (this.maxDirectness < 0f && score <= this.maxDirectness)
				{
					return 1f;
				}
				break;
			case BehaviouralPointDirectnessToTarget.Guide.Flank:
				if (score >= this.minDirectness && score <= this.maxDirectness)
				{
					return 1f;
				}
				break;
			default:
				return 0f;
			}
			return 0f;
		}

		// Token: 0x04000763 RID: 1891
		[FriendlyName("Minimum Directness", "If Approach guided, this value should be greater than 0 to ensure we are approaching our target, but if Flank guided, we rather want this to be a slight negative number, -0.1 for instance.")]
		[ApexSerialization]
		private float minDirectness = -0.1f;

		// Token: 0x04000764 RID: 1892
		[ApexSerialization]
		[FriendlyName("Maximum Directness", "If Retreat guided, this value should be less than 0 to ensure we are retreating from our target, but if Flank guided, we rather want this to be a slight positive number, 0.1 for instance.")]
		private float maxDirectness = 0.1f;

		// Token: 0x04000765 RID: 1893
		[FriendlyName("Behaviour Guide", "If Approach guided, min value over 0 should be used.\nIf Retreat guided, max value under 0 should be used.\nIf Flank guided, a min and max value around 0 (min: -0.1, max: 0.1) should be used.")]
		[ApexSerialization]
		private BehaviouralPointDirectnessToTarget.Guide guide = BehaviouralPointDirectnessToTarget.Guide.Flank;

		// Token: 0x02000185 RID: 389
		public enum Guide
		{
			// Token: 0x04000767 RID: 1895
			Approach,
			// Token: 0x04000768 RID: 1896
			Retreat,
			// Token: 0x04000769 RID: 1897
			Flank
		}
	}
}
