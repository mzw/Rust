﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000175 RID: 373
	public class TimeSinceLastMoveToCoverThreshold : BaseScorer
	{
		// Token: 0x06000D45 RID: 3397 RVA: 0x000541CC File Offset: 0x000523CC
		public override float GetScore(BaseContext c)
		{
			return (float)((!TimeSinceLastMoveToCoverThreshold.Evaluate(c as global::NPCHumanContext, this.minThreshold, this.maxThreshold)) ? 0 : 1);
		}

		// Token: 0x06000D46 RID: 3398 RVA: 0x000541F4 File Offset: 0x000523F4
		public static bool Evaluate(global::NPCHumanContext c, float minThreshold, float maxThreshold)
		{
			if (Mathf.Approximately(c.Human.TimeLastMovedToCover, 0f))
			{
				return true;
			}
			float num = Time.realtimeSinceStartup - c.Human.TimeLastMovedToCover;
			if (c.GetFact(global::NPCPlayerApex.Facts.IsMovingToCover) > 0 || num < minThreshold)
			{
				return false;
			}
			if (num >= maxThreshold)
			{
				return true;
			}
			float num2 = maxThreshold - minThreshold;
			float num3 = maxThreshold - num;
			return Random.value < num3 / num2;
		}

		// Token: 0x0400074D RID: 1869
		[ApexSerialization]
		public float minThreshold;

		// Token: 0x0400074E RID: 1870
		[ApexSerialization]
		public float maxThreshold = 1f;
	}
}
