﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200015D RID: 349
	public class HasFactFoodRange : BaseScorer
	{
		// Token: 0x06000D0A RID: 3338 RVA: 0x00052FA0 File Offset: 0x000511A0
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::BaseNpc.Facts.FoodRange);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x0400071D RID: 1821
		[ApexSerialization(defaultValue = global::BaseNpc.FoodRangeEnum.EatRange)]
		public global::BaseNpc.FoodRangeEnum value;
	}
}
