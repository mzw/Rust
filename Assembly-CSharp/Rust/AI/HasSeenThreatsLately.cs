﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020000FA RID: 250
	public sealed class HasSeenThreatsLately : BaseScorer
	{
		// Token: 0x06000C3B RID: 3131 RVA: 0x000501C0 File Offset: 0x0004E3C0
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTargetMemory.Timestamp > 0f && Time.realtimeSinceStartup - c.AIAgent.AttackTargetMemory.Timestamp <= this.WithinSeconds)
			{
				return 1f;
			}
			return 0f;
		}

		// Token: 0x040006BC RID: 1724
		[ApexSerialization]
		public float WithinSeconds = 10f;
	}
}
