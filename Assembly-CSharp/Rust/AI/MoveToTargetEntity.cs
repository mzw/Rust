﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000108 RID: 264
	public class MoveToTargetEntity : BaseAction
	{
		// Token: 0x06000C58 RID: 3160 RVA: 0x00050744 File Offset: 0x0004E944
		public override void DoExecute(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return;
			}
			c.AIAgent.UpdateDestination(c.AIAgent.AttackTargetMemory.Position);
		}
	}
}
