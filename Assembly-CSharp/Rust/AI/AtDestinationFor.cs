﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200011A RID: 282
	public class AtDestinationFor : BaseScorer
	{
		// Token: 0x06000C84 RID: 3204 RVA: 0x000512EC File Offset: 0x0004F4EC
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.TimeAtDestination < this.Duration) ? 0f : 1f;
		}

		// Token: 0x040006D3 RID: 1747
		[ApexSerialization]
		public float Duration = 5f;
	}
}
