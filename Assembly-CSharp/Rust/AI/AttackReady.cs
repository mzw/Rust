﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000127 RID: 295
	public class AttackReady : BaseScorer
	{
		// Token: 0x06000C9E RID: 3230 RVA: 0x000515A4 File Offset: 0x0004F7A4
		public override float GetScore(BaseContext c)
		{
			return (!c.AIAgent.AttackReady()) ? 0f : 1f;
		}
	}
}
