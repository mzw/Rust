﻿using System;

namespace Rust.Ai
{
	// Token: 0x020000FD RID: 253
	public sealed class IsCurrentAttackEntity : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C41 RID: 3137 RVA: 0x00050278 File Offset: 0x0004E478
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			return (float)((!(c.AIAgent.AttackTarget == target)) ? 0 : 1);
		}
	}
}
