﻿using System;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200018C RID: 396
	public abstract class WeightedScorerBase<T> : OptionScorerBase<T>
	{
		// Token: 0x06000D7A RID: 3450 RVA: 0x00054F64 File Offset: 0x00053164
		public WeightedScorerBase()
		{
			this.DebugName = base.GetType().Name;
		}

		// Token: 0x06000D7B RID: 3451 RVA: 0x00054F88 File Offset: 0x00053188
		protected float ProcessScore(float s)
		{
			s = Mathf.Clamp01(s);
			if (this.InvertScore)
			{
				s = 1f - s;
			}
			return s * this.ScoreScale;
		}

		// Token: 0x06000D7C RID: 3452 RVA: 0x00054FB0 File Offset: 0x000531B0
		public override float Score(IAIContext context, T option)
		{
			return this.ProcessScore(this.GetScore((BaseContext)context, option));
		}

		// Token: 0x06000D7D RID: 3453
		public abstract float GetScore(BaseContext context, T option);

		// Token: 0x04000776 RID: 1910
		[ApexSerialization(defaultValue = false)]
		public bool InvertScore;

		// Token: 0x04000777 RID: 1911
		[ApexSerialization(defaultValue = 50f)]
		public float ScoreScale = 50f;

		// Token: 0x04000778 RID: 1912
		private string DebugName;
	}
}
