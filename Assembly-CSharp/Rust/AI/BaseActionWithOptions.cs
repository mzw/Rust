﻿using System;
using System.Collections.Generic;
using Apex.AI;

namespace Rust.Ai
{
	// Token: 0x0200010C RID: 268
	public abstract class BaseActionWithOptions<T> : ActionWithOptions<T>
	{
		// Token: 0x06000C60 RID: 3168 RVA: 0x0005081C File Offset: 0x0004EA1C
		public BaseActionWithOptions()
		{
			this.DebugName = base.GetType().Name;
		}

		// Token: 0x06000C61 RID: 3169 RVA: 0x00050838 File Offset: 0x0004EA38
		public override void Execute(IAIContext context)
		{
			BaseContext baseContext = context as BaseContext;
			if (baseContext != null)
			{
				this.DoExecute(baseContext);
			}
		}

		// Token: 0x06000C62 RID: 3170
		public abstract void DoExecute(BaseContext context);

		// Token: 0x06000C63 RID: 3171 RVA: 0x0005085C File Offset: 0x0004EA5C
		public bool TryGetBest(BaseContext context, IList<T> options, bool allScorersMustScoreAboveZero, out T best, out float bestScore)
		{
			bestScore = float.MinValue;
			best = default(T);
			for (int i = 0; i < options.Count; i++)
			{
				float num = 0f;
				bool flag = true;
				for (int j = 0; j < base.scorers.Count; j++)
				{
					if (!base.scorers[j].isDisabled)
					{
						float num2 = base.scorers[j].Score(context, options[i]);
						if (allScorersMustScoreAboveZero && num2 <= 0f)
						{
							flag = false;
							break;
						}
						num += num2;
					}
				}
				if (flag && num > bestScore)
				{
					bestScore = num;
					best = options[i];
				}
			}
			return best != null;
		}

		// Token: 0x040006C4 RID: 1732
		private string DebugName;
	}
}
