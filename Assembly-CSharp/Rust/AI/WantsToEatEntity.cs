﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000111 RID: 273
	public sealed class WantsToEatEntity : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C6D RID: 3181 RVA: 0x00050A60 File Offset: 0x0004EC60
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			return (float)((!c.AIAgent.WantsToEat(target)) ? 0 : 1);
		}
	}
}
