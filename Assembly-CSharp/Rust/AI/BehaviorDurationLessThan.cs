﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200013B RID: 315
	public sealed class BehaviorDurationLessThan : BaseScorer
	{
		// Token: 0x06000CC4 RID: 3268 RVA: 0x00051DDC File Offset: 0x0004FFDC
		public override float GetScore(BaseContext c)
		{
			return (float)((c.AIAgent.CurrentBehaviour != this.Behaviour || c.AIAgent.currentBehaviorDuration >= this.duration) ? 0 : 1);
		}

		// Token: 0x040006F3 RID: 1779
		[ApexSerialization]
		public global::BaseNpc.Behaviour Behaviour;

		// Token: 0x040006F4 RID: 1780
		[ApexSerialization]
		public float duration;
	}
}
