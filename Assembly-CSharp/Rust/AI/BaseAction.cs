﻿using System;
using Apex.AI;

namespace Rust.Ai
{
	// Token: 0x0200010B RID: 267
	public abstract class BaseAction : ActionBase
	{
		// Token: 0x06000C5D RID: 3165 RVA: 0x000507DC File Offset: 0x0004E9DC
		public BaseAction()
		{
			this.DebugName = base.GetType().Name;
		}

		// Token: 0x06000C5E RID: 3166 RVA: 0x000507F8 File Offset: 0x0004E9F8
		public override void Execute(IAIContext context)
		{
			BaseContext baseContext = context as BaseContext;
			if (baseContext != null)
			{
				this.DoExecute(baseContext);
			}
		}

		// Token: 0x06000C5F RID: 3167
		public abstract void DoExecute(BaseContext context);

		// Token: 0x040006C3 RID: 1731
		private string DebugName;
	}
}
