﻿using System;
using System.Collections.Generic;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200016E RID: 366
	public class HumanNavigateToOperator : BaseAction
	{
		// Token: 0x06000D2A RID: 3370 RVA: 0x00053618 File Offset: 0x00051818
		public override void DoExecute(BaseContext c)
		{
			global::NPCHumanContext npchumanContext = c as global::NPCHumanContext;
			if (c.GetFact(global::NPCPlayerApex.Facts.CanNotMove) == 1)
			{
				c.AIAgent.StopMoving();
				if (npchumanContext != null)
				{
					npchumanContext.Human.SetFact(global::NPCPlayerApex.Facts.PathToTargetStatus, 2, true, true);
				}
				return;
			}
			c.AIAgent.SetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover, 0, true, true);
			c.AIAgent.SetFact(global::NPCPlayerApex.Facts.SidesteppedOutOfCover, 0, true, true);
			switch (this.Operator)
			{
			case HumanNavigateToOperator.OperatorType.EnemyLoc:
				HumanNavigateToOperator.NavigateToEnemy(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.RandomLoc:
				HumanNavigateToOperator.NavigateToRandomLoc(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.SpawnLoc:
				HumanNavigateToOperator.NavigateToSpawnLoc(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.FleeEnemy:
				HumanNavigateToOperator.FleeEnemy(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.FleeHurtDir:
				HumanNavigateToOperator.FleeHurtDir(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.RetreatCover:
				HumanNavigateToOperator.NavigateToCover(npchumanContext, HumanNavigateToOperator.TakeCoverIntention.Retreat);
				break;
			case HumanNavigateToOperator.OperatorType.FlankCover:
				HumanNavigateToOperator.NavigateToCover(npchumanContext, HumanNavigateToOperator.TakeCoverIntention.Flank);
				break;
			case HumanNavigateToOperator.OperatorType.AdvanceCover:
				HumanNavigateToOperator.NavigateToCover(npchumanContext, HumanNavigateToOperator.TakeCoverIntention.Advance);
				break;
			case HumanNavigateToOperator.OperatorType.FleeExplosive:
				HumanNavigateToOperator.FleeExplosive(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.Sidestep:
				HumanNavigateToOperator.Sidestep(npchumanContext);
				break;
			case HumanNavigateToOperator.OperatorType.ClosestCover:
				HumanNavigateToOperator.NavigateToCover(npchumanContext, HumanNavigateToOperator.TakeCoverIntention.Closest);
				break;
			case HumanNavigateToOperator.OperatorType.PatrolLoc:
				HumanNavigateToOperator.NavigateToPatrolLoc(npchumanContext);
				break;
			}
		}

		// Token: 0x06000D2B RID: 3371 RVA: 0x00053748 File Offset: 0x00051948
		public static void MakeUnstuck(global::NPCHumanContext c)
		{
			c.Human.stuckDuration = 0f;
			c.Human.IsStuck = false;
		}

		// Token: 0x06000D2C RID: 3372 RVA: 0x00053768 File Offset: 0x00051968
		public static void NavigateToEnemy(global::NPCHumanContext c)
		{
			if (c.GetFact(global::NPCPlayerApex.Facts.HasEnemy) > 0 && c.AIAgent.IsNavRunning())
			{
				if (c.GetFact(global::NPCPlayerApex.Facts.HasLineOfSight) > 0)
				{
					HumanNavigateToOperator.MakeUnstuck(c);
					c.AIAgent.GetNavAgent.destination = c.EnemyPosition;
				}
				else
				{
					Memory.SeenInfo info = c.Memory.GetInfo(c.AIAgent.AttackTarget);
					if (info.Entity != null)
					{
						HumanNavigateToOperator.MakeUnstuck(c);
						c.AIAgent.GetNavAgent.destination = info.Position;
					}
				}
				c.Human.SetTargetPathStatus(0.05f);
			}
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x00053818 File Offset: 0x00051A18
		public static void NavigateToRandomLoc(global::NPCHumanContext c)
		{
			if (IsHumanRoamReady.Evaluate(c) && c.AIAgent.IsNavRunning() && HumanNavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.DiscourageSharpTurns, c.AIAgent.GetStats.MinRoamRange, c.AIAgent.GetStats.MaxRoamRange))
			{
				HumanNavigateToOperator.UpdateRoamTime(c);
				if (c.Human.OnChatter != null)
				{
					c.Human.OnChatter();
				}
			}
		}

		// Token: 0x06000D2E RID: 3374 RVA: 0x000538A0 File Offset: 0x00051AA0
		public static void NavigateToPatrolLoc(global::NPCHumanContext c)
		{
			if (c.AiLocationManager == null)
			{
				return;
			}
			if (IsHumanRoamReady.Evaluate(c) && c.AIAgent.IsNavRunning())
			{
				global::PathInterestNode randomPatrolPointInRange = c.AiLocationManager.GetRandomPatrolPointInRange(c.Position, c.AIAgent.GetStats.MinRoamRange, c.AIAgent.GetStats.MaxRoamRange, c.CurrentPatrolPoint);
				if (randomPatrolPointInRange != null)
				{
					HumanNavigateToOperator.MakeUnstuck(c);
					c.AIAgent.GetNavAgent.destination = randomPatrolPointInRange.transform.position;
					c.Human.SetTargetPathStatus(0.05f);
					c.CurrentPatrolPoint = randomPatrolPointInRange;
				}
				HumanNavigateToOperator.UpdateRoamTime(c);
				if (c.Human.OnChatter != null)
				{
					c.Human.OnChatter();
				}
			}
		}

		// Token: 0x06000D2F RID: 3375 RVA: 0x00053984 File Offset: 0x00051B84
		public static void NavigateToSpawnLoc(global::NPCHumanContext c)
		{
			if (IsHumanRoamReady.Evaluate(c) && c.AIAgent.IsNavRunning())
			{
				HumanNavigateToOperator.MakeUnstuck(c);
				c.AIAgent.GetNavAgent.destination = c.AIAgent.SpawnPosition;
				c.Human.SetTargetPathStatus(0.05f);
				HumanNavigateToOperator.UpdateRoamTime(c);
			}
		}

		// Token: 0x06000D30 RID: 3376 RVA: 0x000539E4 File Offset: 0x00051BE4
		private static void UpdateRoamTime(global::NPCHumanContext c)
		{
			float num = c.AIAgent.GetStats.MaxRoamDelay - c.AIAgent.GetStats.MinRoamDelay;
			float num2 = Random.value * num;
			float num3 = num2 / num;
			float num4 = c.AIAgent.GetStats.RoamDelayDistribution.Evaluate(num3);
			float num5 = num4 * num;
			c.NextRoamTime = Time.realtimeSinceStartup + c.AIAgent.GetStats.MinRoamDelay + num5;
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x00053A6C File Offset: 0x00051C6C
		public static void NavigateToCover(global::NPCHumanContext c, HumanNavigateToOperator.TakeCoverIntention intention)
		{
			if (!c.AIAgent.IsNavRunning())
			{
				return;
			}
			c.Human.TimeLastMovedToCover = Time.realtimeSinceStartup;
			switch (intention)
			{
			case HumanNavigateToOperator.TakeCoverIntention.Advance:
				if (c.CoverSet.Advance.ReservedCoverPoint != null)
				{
					HumanNavigateToOperator.PathToCover(c, c.CoverSet.Advance.ReservedCoverPoint.Position);
				}
				else if (c.CoverSet.Closest.ReservedCoverPoint != null)
				{
					HumanNavigateToOperator.PathToCover(c, c.CoverSet.Closest.ReservedCoverPoint.Position);
				}
				return;
			case HumanNavigateToOperator.TakeCoverIntention.Flank:
				if (c.CoverSet.Flank.ReservedCoverPoint != null)
				{
					HumanNavigateToOperator.PathToCover(c, c.CoverSet.Flank.ReservedCoverPoint.Position);
					c.SetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover, 1, true, true);
				}
				else if (c.CoverSet.Closest.ReservedCoverPoint != null)
				{
					HumanNavigateToOperator.PathToCover(c, c.CoverSet.Closest.ReservedCoverPoint.Position);
					c.SetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover, 1, true, true);
				}
				return;
			case HumanNavigateToOperator.TakeCoverIntention.Retreat:
				if (c.CoverSet.Retreat.ReservedCoverPoint != null)
				{
					HumanNavigateToOperator.PathToCover(c, c.CoverSet.Retreat.ReservedCoverPoint.Position);
					c.SetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover, 1, true, true);
				}
				else if (c.CoverSet.Closest.ReservedCoverPoint != null)
				{
					HumanNavigateToOperator.PathToCover(c, c.CoverSet.Closest.ReservedCoverPoint.Position);
					c.SetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover, 1, true, true);
				}
				return;
			}
			if (c.CoverSet.Closest.ReservedCoverPoint != null)
			{
				HumanNavigateToOperator.PathToCover(c, c.CoverSet.Closest.ReservedCoverPoint.Position);
			}
		}

		// Token: 0x06000D32 RID: 3378 RVA: 0x00053C54 File Offset: 0x00051E54
		public static void PathToCover(global::NPCHumanContext c, Vector3 coverPosition)
		{
			HumanNavigateToOperator.MakeUnstuck(c);
			c.AIAgent.GetNavAgent.destination = coverPosition;
			c.Human.SetTargetPathStatus(0.05f);
			c.SetFact(global::NPCPlayerApex.Facts.IsMovingToCover, 1, true, true);
			if (c.Human.OnTakeCover != null)
			{
				c.Human.OnTakeCover();
			}
		}

		// Token: 0x06000D33 RID: 3379 RVA: 0x00053CB4 File Offset: 0x00051EB4
		public static void FleeEnemy(global::NPCHumanContext c)
		{
			if (c.AIAgent.IsNavRunning() && HumanNavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.RetreatFromTarget, c.AIAgent.GetStats.MinFleeRange, c.AIAgent.GetStats.MaxFleeRange))
			{
				c.SetFact(global::NPCPlayerApex.Facts.IsFleeing, 1, true, true);
			}
		}

		// Token: 0x06000D34 RID: 3380 RVA: 0x00053D14 File Offset: 0x00051F14
		public static void FleeExplosive(global::NPCHumanContext c)
		{
			if (c.AIAgent.IsNavRunning() && HumanNavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.RetreatFromExplosive, c.AIAgent.GetStats.MinFleeRange, c.AIAgent.GetStats.MaxFleeRange))
			{
				c.SetFact(global::NPCPlayerApex.Facts.IsFleeing, 1, true, true);
				if (c.Human.OnFleeExplosive != null)
				{
					c.Human.OnFleeExplosive();
				}
			}
		}

		// Token: 0x06000D35 RID: 3381 RVA: 0x00053D98 File Offset: 0x00051F98
		public static void FleeHurtDir(global::NPCHumanContext c)
		{
			if (c.AIAgent.IsNavRunning() && HumanNavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.RetreatFromDirection, c.AIAgent.GetStats.MinFleeRange, c.AIAgent.GetStats.MaxFleeRange))
			{
				c.SetFact(global::NPCPlayerApex.Facts.IsFleeing, 1, true, true);
			}
		}

		// Token: 0x06000D36 RID: 3382 RVA: 0x00053DFC File Offset: 0x00051FFC
		public static void Sidestep(global::NPCHumanContext c)
		{
			if (c.AIAgent.IsNavRunning() && HumanNavigateToOperator.NavigateInDirOfBestSample(c, NavPointSampler.SampleCount.Eight, 4f, NavPointSampler.SampleFeatures.FlankTarget, 2f, 5f) && c.AIAgent.GetFact(global::NPCPlayerApex.Facts.IsInCover) == 1)
			{
				c.AIAgent.SetFact(global::NPCPlayerApex.Facts.SidesteppedOutOfCover, 1, true, true);
			}
		}

		// Token: 0x06000D37 RID: 3383 RVA: 0x00053E58 File Offset: 0x00052058
		private static bool NavigateInDirOfBestSample(global::NPCHumanContext c, NavPointSampler.SampleCount sampleCount, float radius, NavPointSampler.SampleFeatures features, float minRange, float maxRange)
		{
			List<NavPointSample> list = c.AIAgent.RequestNavPointSamplesInCircle(sampleCount, radius, features);
			if (list == null)
			{
				return false;
			}
			foreach (NavPointSample navPointSample in list)
			{
				Vector3 normalized = (navPointSample.Position - c.Position).normalized;
				Vector3 vector = c.Position + (normalized * minRange + normalized * ((maxRange - minRange) * Random.value));
				if (!(c.AIAgent.AttackTarget != null) || NavPointSampler.IsValidPointDirectness(vector, c.Position, c.EnemyPosition))
				{
					NavPointSample navPointSample2 = NavPointSampler.SamplePoint(vector, new NavPointSampler.SampleScoreParams
					{
						WaterMaxDepth = c.AIAgent.GetStats.MaxWaterDepth,
						Agent = c.AIAgent,
						Features = features
					});
					if (!Mathf.Approximately(navPointSample2.Score, 0f))
					{
						HumanNavigateToOperator.MakeUnstuck(c);
						vector = navPointSample2.Position;
						c.AIAgent.GetNavAgent.destination = vector;
						c.Human.SetTargetPathStatus(0.05f);
						c.AIAgent.SetFact(global::NPCPlayerApex.Facts.IsMoving, 1, true, false);
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x04000737 RID: 1847
		[ApexSerialization]
		public HumanNavigateToOperator.OperatorType Operator;

		// Token: 0x0200016F RID: 367
		public enum OperatorType
		{
			// Token: 0x04000739 RID: 1849
			EnemyLoc,
			// Token: 0x0400073A RID: 1850
			RandomLoc,
			// Token: 0x0400073B RID: 1851
			SpawnLoc,
			// Token: 0x0400073C RID: 1852
			FleeEnemy,
			// Token: 0x0400073D RID: 1853
			FleeHurtDir,
			// Token: 0x0400073E RID: 1854
			RetreatCover,
			// Token: 0x0400073F RID: 1855
			FlankCover,
			// Token: 0x04000740 RID: 1856
			AdvanceCover,
			// Token: 0x04000741 RID: 1857
			FleeExplosive,
			// Token: 0x04000742 RID: 1858
			Sidestep,
			// Token: 0x04000743 RID: 1859
			ClosestCover,
			// Token: 0x04000744 RID: 1860
			PatrolLoc
		}

		// Token: 0x02000170 RID: 368
		public enum TakeCoverIntention
		{
			// Token: 0x04000746 RID: 1862
			Advance,
			// Token: 0x04000747 RID: 1863
			Flank,
			// Token: 0x04000748 RID: 1864
			Retreat,
			// Token: 0x04000749 RID: 1865
			Closest
		}
	}
}
