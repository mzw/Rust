﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200013E RID: 318
	public class DistanceFromDestination : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CCA RID: 3274 RVA: 0x00051ECC File Offset: 0x000500CC
		public override float GetScore(BaseContext c, Vector3 position)
		{
			if (!c.AIAgent.IsStopped)
			{
				return 1f;
			}
			return Vector3.Distance(position, c.AIAgent.Destination) / this.Range;
		}

		// Token: 0x040006F6 RID: 1782
		[ApexSerialization]
		public float Range = 50f;
	}
}
