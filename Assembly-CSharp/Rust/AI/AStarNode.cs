﻿using System;

namespace Rust.AI
{
	// Token: 0x020000C5 RID: 197
	public class AStarNode
	{
		// Token: 0x06000AAC RID: 2732 RVA: 0x00048DD0 File Offset: 0x00046FD0
		public AStarNode(float g, float h, AStarNode parent, global::BasePathNode node)
		{
			this.G = g;
			this.H = h;
			this.Parent = parent;
			this.Node = node;
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000AAD RID: 2733 RVA: 0x00048DF8 File Offset: 0x00046FF8
		public float F
		{
			get
			{
				return this.G + this.H;
			}
		}

		// Token: 0x06000AAE RID: 2734 RVA: 0x00048E08 File Offset: 0x00047008
		public void Update(float g, float h, AStarNode parent, global::BasePathNode node)
		{
			this.G = g;
			this.H = h;
			this.Parent = parent;
			this.Node = node;
		}

		// Token: 0x06000AAF RID: 2735 RVA: 0x00048E28 File Offset: 0x00047028
		public bool Satisfies(global::BasePathNode node)
		{
			return this.Node == node;
		}

		// Token: 0x06000AB0 RID: 2736 RVA: 0x00048E38 File Offset: 0x00047038
		public static bool operator <(AStarNode lhs, AStarNode rhs)
		{
			return lhs.F < rhs.F;
		}

		// Token: 0x06000AB1 RID: 2737 RVA: 0x00048E48 File Offset: 0x00047048
		public static bool operator >(AStarNode lhs, AStarNode rhs)
		{
			return lhs.F > rhs.F;
		}

		// Token: 0x04000577 RID: 1399
		public AStarNode Parent;

		// Token: 0x04000578 RID: 1400
		public float G;

		// Token: 0x04000579 RID: 1401
		public float H;

		// Token: 0x0400057A RID: 1402
		public global::BasePathNode Node;
	}
}
