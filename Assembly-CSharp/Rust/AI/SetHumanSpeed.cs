﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000165 RID: 357
	public class SetHumanSpeed : BaseAction
	{
		// Token: 0x06000D1A RID: 3354 RVA: 0x0005326C File Offset: 0x0005146C
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.TargetSpeed = c.AIAgent.ToSpeed(this.value);
			c.SetFact(global::NPCPlayerApex.Facts.Speed, (byte)this.value, true, true);
		}

		// Token: 0x04000728 RID: 1832
		[ApexSerialization(defaultValue = global::NPCPlayerApex.SpeedEnum.StandStill)]
		public global::NPCPlayerApex.SpeedEnum value;
	}
}
