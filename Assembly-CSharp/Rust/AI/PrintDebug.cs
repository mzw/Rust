﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200014D RID: 333
	public sealed class PrintDebug : BaseAction
	{
		// Token: 0x06000CE6 RID: 3302 RVA: 0x00052570 File Offset: 0x00050770
		public override void DoExecute(BaseContext c)
		{
			Debug.Log(this.debugMessage);
		}

		// Token: 0x04000706 RID: 1798
		[ApexSerialization]
		private string debugMessage;
	}
}
