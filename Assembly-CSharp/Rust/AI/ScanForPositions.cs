﻿using System;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x02000118 RID: 280
	[FriendlyName("Scan for Positions", "Scanning positions and storing them in the context")]
	public sealed class ScanForPositions : BaseAction
	{
		// Token: 0x06000C7E RID: 3198 RVA: 0x000510A8 File Offset: 0x0004F2A8
		public override void DoExecute(BaseContext c)
		{
			if (c.sampledPositions == null)
			{
				return;
			}
			if (c.sampledPositions.Count > 0)
			{
				c.sampledPositions.Clear();
			}
			Vector3 position = c.Position;
			float num = Time.time * 1f;
			float num2 = this.SamplingRange / (float)this.SampleRings;
			for (float num3 = this.SamplingRange; num3 > 0.5f; num3 -= num2)
			{
				num += 10f;
				for (float num4 = num % 35f; num4 < 360f; num4 += 35f)
				{
					Vector3 p;
					p..ctor(position.x + Mathf.Sin(num4 * 0.0174532924f) * num3, position.y, position.z + Mathf.Cos(num4 * 0.0174532924f) * num3);
					if (this.CalculatePath && num3 < this.SamplingRange * this.CalculatePathInnerCirclePercentageThreshold)
					{
						ScanForPositions.TryAddPoint(c, p, true, this.ScanAllAreas, this.AreaName, this.SampleTerrainHeight);
					}
					else
					{
						ScanForPositions.TryAddPoint(c, p, false, this.ScanAllAreas, this.AreaName, this.SampleTerrainHeight);
					}
				}
			}
		}

		// Token: 0x06000C7F RID: 3199 RVA: 0x000511DC File Offset: 0x0004F3DC
		private static void TryAddPoint(BaseContext c, Vector3 p, bool calculatePath, bool scanAllAreas, string areaName, bool sampleTerrainHeight)
		{
			int num = (!scanAllAreas && !string.IsNullOrEmpty(areaName)) ? (1 << NavMesh.GetAreaFromName(areaName)) : -1;
			if (sampleTerrainHeight)
			{
				p.y = global::TerrainMeta.HeightMap.GetHeight(p);
			}
			NavMeshHit navMeshHit;
			if (!NavMesh.SamplePosition(p, ref navMeshHit, 4f, num))
			{
				return;
			}
			if (!navMeshHit.hit)
			{
				return;
			}
			if (calculatePath || c.AIAgent.IsStuck)
			{
				if (NavMesh.CalculatePath(navMeshHit.position, c.Position, num, ScanForPositions.reusablePath) && ScanForPositions.reusablePath.status == null)
				{
					c.sampledPositions.Add(navMeshHit.position);
				}
			}
			else
			{
				c.sampledPositions.Add(navMeshHit.position);
			}
		}

		// Token: 0x040006CB RID: 1739
		[FriendlyName("Sampling Range", "How large a range points are sampled in, in a square with the entity in the center")]
		[ApexSerialization(defaultValue = 12f)]
		public float SamplingRange = 12f;

		// Token: 0x040006CC RID: 1740
		[ApexSerialization(defaultValue = 1.5f)]
		[FriendlyName("Sampling Density", "How much distance there is between individual samples")]
		public int SampleRings = 3;

		// Token: 0x040006CD RID: 1741
		[ApexSerialization(defaultValue = false)]
		[FriendlyName("Calculate Path", "Calculating the path to each position ensures connectivity, but is expensive. Should be used for fallbacks/stuck-detection only?")]
		public bool CalculatePath;

		// Token: 0x040006CE RID: 1742
		[ApexSerialization(defaultValue = false)]
		[FriendlyName("Percentage of Inner Circle for Calculate Path", "Calculating the path to each position ensures connectivity, but is expensive. Here we can define what percentage of the sampling range (it's inner circle) we want to calculate paths for.")]
		public float CalculatePathInnerCirclePercentageThreshold = 0.1f;

		// Token: 0x040006CF RID: 1743
		[ApexSerialization]
		public bool ScanAllAreas = true;

		// Token: 0x040006D0 RID: 1744
		[ApexSerialization]
		public string AreaName;

		// Token: 0x040006D1 RID: 1745
		[ApexSerialization]
		public bool SampleTerrainHeight = true;

		// Token: 0x040006D2 RID: 1746
		private static NavMeshPath reusablePath = new NavMeshPath();
	}
}
