﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000119 RID: 281
	public class StaminaLevel : BaseScorer
	{
		// Token: 0x06000C82 RID: 3202 RVA: 0x000512C8 File Offset: 0x0004F4C8
		public override float GetScore(BaseContext c)
		{
			return c.AIAgent.GetStamina;
		}
	}
}
