﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000100 RID: 256
	public sealed class EntityDangerLevel : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C47 RID: 3143 RVA: 0x00050338 File Offset: 0x0004E538
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			foreach (Memory.SeenInfo seenInfo in c.Memory.All)
			{
				if (seenInfo.Entity == target)
				{
					return Mathf.Max(seenInfo.Danger, this.MinScore);
				}
			}
			return this.MinScore;
		}

		// Token: 0x040006BD RID: 1725
		[ApexSerialization]
		public float MinScore;
	}
}
