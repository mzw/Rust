﻿using System;
using System.Collections.Generic;
using Apex.LoadBalancing;
using UnityEngine;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x02000199 RID: 409
	public class CoverPointVolume : MonoBehaviour, IServerComponent, ILoadBalanced
	{
		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000DA5 RID: 3493 RVA: 0x00055338 File Offset: 0x00053538
		public bool repeat
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000DA6 RID: 3494 RVA: 0x0005533C File Offset: 0x0005353C
		private void OnEnable()
		{
			Apex.LoadBalancing.LoadBalancer.defaultBalancer.Add(this);
		}

		// Token: 0x06000DA7 RID: 3495 RVA: 0x0005534C File Offset: 0x0005354C
		private void OnDisable()
		{
			if (Application.isQuitting)
			{
				return;
			}
			Apex.LoadBalancing.LoadBalancer.defaultBalancer.Remove(this);
		}

		// Token: 0x06000DA8 RID: 3496 RVA: 0x00055364 File Offset: 0x00053564
		public float? ExecuteUpdate(float deltaTime, float nextInterval)
		{
			if (this.CoverPoints.Count == 0)
			{
				if (this._dynNavMeshBuildCompletionTime < 0f)
				{
					if (SingletonComponent<global::DynamicNavMesh>.Instance == null || !SingletonComponent<global::DynamicNavMesh>.Instance.enabled || !SingletonComponent<global::DynamicNavMesh>.Instance.IsBuilding)
					{
						this._dynNavMeshBuildCompletionTime = Time.realtimeSinceStartup;
					}
				}
				else if (this._genAttempts < 4 && Time.realtimeSinceStartup - this._dynNavMeshBuildCompletionTime > 0.25f)
				{
					this.GenerateCoverPoints(null);
					if (this.CoverPoints.Count != 0)
					{
						Apex.LoadBalancing.LoadBalancer.defaultBalancer.Remove(this);
						return null;
					}
					this._dynNavMeshBuildCompletionTime = Time.realtimeSinceStartup;
					this._genAttempts++;
					if (this._genAttempts >= 4)
					{
						Object.Destroy(base.gameObject);
						Apex.LoadBalancing.LoadBalancer.defaultBalancer.Remove(this);
						return null;
					}
				}
			}
			return new float?(1f + Random.value * 2f);
		}

		// Token: 0x06000DA9 RID: 3497 RVA: 0x00055480 File Offset: 0x00053680
		[ContextMenu("Clear Cover Points")]
		private void ClearCoverPoints()
		{
			this.CoverPoints.Clear();
			this._coverPointBlockers.Clear();
		}

		// Token: 0x06000DAA RID: 3498 RVA: 0x00055498 File Offset: 0x00053698
		public Bounds GetBounds()
		{
			if (Mathf.Approximately(this.bounds.center.sqrMagnitude, 0f))
			{
				this.bounds = new Bounds(base.transform.position, base.transform.localScale);
			}
			return this.bounds;
		}

		// Token: 0x06000DAB RID: 3499 RVA: 0x000554F0 File Offset: 0x000536F0
		[ContextMenu("Pre-Generate Cover Points")]
		public void PreGenerateCoverPoints()
		{
			this.GenerateCoverPoints(null);
		}

		// Token: 0x06000DAC RID: 3500 RVA: 0x000554FC File Offset: 0x000536FC
		[ContextMenu("Convert to Manual Cover Points")]
		public void ConvertToManualCoverPoints()
		{
			foreach (CoverPoint coverPoint in this.CoverPoints)
			{
				ManualCoverPoint manualCoverPoint = new GameObject("MCP").AddComponent<ManualCoverPoint>();
				manualCoverPoint.transform.localPosition = Vector3.zero;
				manualCoverPoint.transform.position = coverPoint.Position;
				manualCoverPoint.Normal = coverPoint.Normal;
				manualCoverPoint.NormalCoverType = coverPoint.NormalCoverType;
				manualCoverPoint.Volume = this;
			}
		}

		// Token: 0x06000DAD RID: 3501 RVA: 0x000555A4 File Offset: 0x000537A4
		public void GenerateCoverPoints(Transform coverPointGroup)
		{
			float realtimeSinceStartup = Time.realtimeSinceStartup;
			this.ClearCoverPoints();
			if (this.ManualCoverPointGroup == null)
			{
				this.ManualCoverPointGroup = coverPointGroup;
			}
			if (this.ManualCoverPointGroup == null)
			{
				this.ManualCoverPointGroup = base.transform;
			}
			if (this.ManualCoverPointGroup.childCount > 0)
			{
				ManualCoverPoint[] componentsInChildren = this.ManualCoverPointGroup.GetComponentsInChildren<ManualCoverPoint>();
				foreach (ManualCoverPoint manualCoverPoint in componentsInChildren)
				{
					CoverPoint item = manualCoverPoint.ToCoverPoint(this);
					this.CoverPoints.Add(item);
				}
			}
			if (this._coverPointBlockers.Count == 0 && this.BlockerGroup != null)
			{
				CoverPointBlockerVolume[] componentsInChildren2 = this.BlockerGroup.GetComponentsInChildren<CoverPointBlockerVolume>();
				if (componentsInChildren2 != null && componentsInChildren2.Length > 0)
				{
					this._coverPointBlockers.AddRange(componentsInChildren2);
				}
			}
			if (this.CoverPoints.Count == 0)
			{
				NavMeshHit navMeshHit;
				bool flag = NavMesh.SamplePosition(base.transform.position, ref navMeshHit, base.transform.localScale.y * CoverPointVolume.cover_point_sample_step_height, -1);
				if (flag)
				{
					Vector3 position = base.transform.position;
					Vector3 vector = base.transform.lossyScale * 0.5f;
					for (float num = position.x - vector.x + 1f; num < position.x + vector.x - 1f; num += CoverPointVolume.cover_point_sample_step_size)
					{
						for (float num2 = position.z - vector.z + 1f; num2 < position.z + vector.z - 1f; num2 += CoverPointVolume.cover_point_sample_step_size)
						{
							for (float num3 = position.y - vector.y; num3 < position.y + vector.y; num3 += CoverPointVolume.cover_point_sample_step_height)
							{
								Vector3 vector2;
								vector2..ctor(num, num3, num2);
								NavMeshHit info;
								if (NavMesh.FindClosestEdge(vector2, ref info, navMeshHit.mask))
								{
									if (AiManager.nav_grid)
									{
										bool flag2 = (info.position - base.transform.position).sqrMagnitude >= vector.x - 0.5f;
										if (flag2)
										{
											goto IL_2E9;
										}
									}
									info.position = new Vector3(info.position.x, info.position.y + 0.5f, info.position.z);
									bool flag3 = true;
									foreach (CoverPoint coverPoint in this.CoverPoints)
									{
										if ((coverPoint.Position - info.position).sqrMagnitude < CoverPointVolume.cover_point_sample_step_size * CoverPointVolume.cover_point_sample_step_size)
										{
											flag3 = false;
											break;
										}
									}
									if (flag3)
									{
										CoverPoint coverPoint2 = this.CalculateCoverPoint(info);
										if (coverPoint2 != null)
										{
											this.CoverPoints.Add(coverPoint2);
										}
									}
								}
								IL_2E9:;
							}
						}
					}
				}
			}
		}

		// Token: 0x06000DAE RID: 3502 RVA: 0x00055918 File Offset: 0x00053B18
		private CoverPoint CalculateCoverPoint(NavMeshHit info)
		{
			RaycastHit raycastHit;
			CoverPointVolume.CoverType coverType = this.ProvidesCoverInDir(new Ray(info.position, -info.normal), this.CoverPointRayLength, out raycastHit);
			if (coverType == CoverPointVolume.CoverType.None)
			{
				return null;
			}
			CoverPoint coverPoint = new CoverPoint(this, this.DefaultCoverPointScore)
			{
				Position = info.position,
				Normal = -info.normal
			};
			if (coverType == CoverPointVolume.CoverType.Full)
			{
				coverPoint.NormalCoverType = CoverPoint.CoverType.Full;
			}
			else if (coverType == CoverPointVolume.CoverType.Partial)
			{
				coverPoint.NormalCoverType = CoverPoint.CoverType.Partial;
			}
			return coverPoint;
		}

		// Token: 0x06000DAF RID: 3503 RVA: 0x000559A4 File Offset: 0x00053BA4
		internal CoverPointVolume.CoverType ProvidesCoverInDir(Ray ray, float maxDistance, out RaycastHit rayHit)
		{
			rayHit = default(RaycastHit);
			if (Vector3Ex.IsNaNOrInfinity(ray.origin))
			{
				return CoverPointVolume.CoverType.None;
			}
			if (Vector3Ex.IsNaNOrInfinity(ray.direction))
			{
				return CoverPointVolume.CoverType.None;
			}
			if (ray.direction == Vector3.zero)
			{
				return CoverPointVolume.CoverType.None;
			}
			ray.origin += global::PlayerEyes.EyeOffset;
			if (Physics.Raycast(ray.origin, ray.direction, ref rayHit, maxDistance, this.CoverLayerMask))
			{
				return CoverPointVolume.CoverType.Full;
			}
			ray.origin += global::PlayerEyes.DuckOffset;
			if (Physics.Raycast(ray.origin, ray.direction, ref rayHit, maxDistance, this.CoverLayerMask))
			{
				return CoverPointVolume.CoverType.Partial;
			}
			return CoverPointVolume.CoverType.None;
		}

		// Token: 0x06000DB0 RID: 3504 RVA: 0x00055A78 File Offset: 0x00053C78
		public bool Contains(Vector3 point)
		{
			Bounds bounds;
			bounds..ctor(base.transform.position, base.transform.localScale);
			return bounds.Contains(point);
		}

		// Token: 0x0400079D RID: 1949
		public float DefaultCoverPointScore = 1f;

		// Token: 0x0400079E RID: 1950
		public float CoverPointRayLength = 1f;

		// Token: 0x0400079F RID: 1951
		public LayerMask CoverLayerMask;

		// Token: 0x040007A0 RID: 1952
		public Transform BlockerGroup;

		// Token: 0x040007A1 RID: 1953
		public Transform ManualCoverPointGroup;

		// Token: 0x040007A2 RID: 1954
		[ServerVar(Help = "cover_point_sample_step_size defines the size of the steps we do horizontally for the cover point volume's cover point generation (smaller steps gives more accurate cover points, but at a higher processing cost). (default: 6.0)")]
		public static float cover_point_sample_step_size = 6f;

		// Token: 0x040007A3 RID: 1955
		[ServerVar(Help = "cover_point_sample_step_height defines the height of the steps we do vertically for the cover point volume's cover point generation (smaller steps gives more accurate cover points, but at a higher processing cost). (default: 2.0)")]
		public static float cover_point_sample_step_height = 2f;

		// Token: 0x040007A4 RID: 1956
		public readonly List<CoverPoint> CoverPoints = new List<CoverPoint>();

		// Token: 0x040007A5 RID: 1957
		private readonly List<CoverPointBlockerVolume> _coverPointBlockers = new List<CoverPointBlockerVolume>();

		// Token: 0x040007A6 RID: 1958
		private float _dynNavMeshBuildCompletionTime = -1f;

		// Token: 0x040007A7 RID: 1959
		private int _genAttempts;

		// Token: 0x040007A8 RID: 1960
		private Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

		// Token: 0x0200019A RID: 410
		internal enum CoverType
		{
			// Token: 0x040007AA RID: 1962
			None,
			// Token: 0x040007AB RID: 1963
			Partial,
			// Token: 0x040007AC RID: 1964
			Full
		}
	}
}
