﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200018F RID: 399
	public struct AiAnswer_ShareEnemyTarget : IAiAnswer
	{
		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000D8F RID: 3471 RVA: 0x00055144 File Offset: 0x00053344
		// (set) Token: 0x06000D90 RID: 3472 RVA: 0x0005514C File Offset: 0x0005334C
		public global::NPCPlayerApex Source { get; set; }

		// Token: 0x04000788 RID: 1928
		public global::BasePlayer PlayerTarget;
	}
}
