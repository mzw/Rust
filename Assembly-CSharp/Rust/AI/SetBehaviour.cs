﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000132 RID: 306
	public sealed class SetBehaviour : BaseAction
	{
		// Token: 0x06000CB4 RID: 3252 RVA: 0x0005198C File Offset: 0x0004FB8C
		public override void DoExecute(BaseContext c)
		{
			if (c.AIAgent.CurrentBehaviour == this.Behaviour)
			{
				return;
			}
			c.AIAgent.CurrentBehaviour = this.Behaviour;
			if (this.BusyTime > 0f)
			{
				c.AIAgent.SetBusyFor(this.BusyTime);
			}
		}

		// Token: 0x040006E1 RID: 1761
		[ApexSerialization]
		public global::BaseNpc.Behaviour Behaviour;

		// Token: 0x040006E2 RID: 1762
		[ApexSerialization]
		public float BusyTime;
	}
}
