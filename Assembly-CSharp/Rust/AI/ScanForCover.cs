﻿using System;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000116 RID: 278
	[FriendlyName("Scan for Cover", "Scanning for cover volumes and the cover points within the relevant ones.")]
	public sealed class ScanForCover : BaseAction
	{
		// Token: 0x06000C77 RID: 3191 RVA: 0x00050BD8 File Offset: 0x0004EDD8
		public override void DoExecute(BaseContext ctx)
		{
			if (SingletonComponent<AiManager>.Instance == null || !SingletonComponent<AiManager>.Instance.enabled || !SingletonComponent<AiManager>.Instance.UseCover || ctx.AIAgent.AttackTarget == null)
			{
				return;
			}
			global::NPCHumanContext npchumanContext = ctx as global::NPCHumanContext;
			if (npchumanContext == null)
			{
				return;
			}
			if (npchumanContext.sampledCoverPoints.Count > 0)
			{
				npchumanContext.sampledCoverPoints.Clear();
				npchumanContext.sampledCoverPointTypes.Clear();
			}
			if (!(npchumanContext.AIAgent.AttackTarget is global::BasePlayer))
			{
				return;
			}
			if (npchumanContext.CurrentCoverVolume == null || !npchumanContext.CurrentCoverVolume.Contains(npchumanContext.Position))
			{
				npchumanContext.CurrentCoverVolume = SingletonComponent<AiManager>.Instance.GetCoverVolumeContaining(npchumanContext.Position);
				if (npchumanContext.CurrentCoverVolume == null)
				{
					npchumanContext.CurrentCoverVolume = AiManager.CreateNewCoverVolume(npchumanContext.Position, null);
				}
			}
			if (npchumanContext.CurrentCoverVolume != null)
			{
				foreach (CoverPoint coverPoint in npchumanContext.CurrentCoverVolume.CoverPoints)
				{
					if (!coverPoint.IsReserved)
					{
						Vector3 position = coverPoint.Position;
						float sqrMagnitude = (npchumanContext.Position - position).sqrMagnitude;
						if (sqrMagnitude <= this.MaxDistanceToCover)
						{
							Vector3 normalized = (position - npchumanContext.AIAgent.AttackTargetMemory.Position).normalized;
							if (ScanForCover.ProvidesCoverFromDirection(coverPoint, normalized, this.CoverArcThreshold))
							{
								npchumanContext.sampledCoverPointTypes.Add(coverPoint.NormalCoverType);
								npchumanContext.sampledCoverPoints.Add(coverPoint);
							}
						}
					}
				}
			}
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x00050DC8 File Offset: 0x0004EFC8
		public static bool ProvidesCoverFromDirection(CoverPoint cp, Vector3 directionTowardCover, float arcThreshold)
		{
			float num = Vector3.Dot(cp.Normal, directionTowardCover);
			return num < arcThreshold;
		}

		// Token: 0x040006C6 RID: 1734
		[ApexSerialization]
		public float MaxDistanceToCover = 15f;

		// Token: 0x040006C7 RID: 1735
		[ApexSerialization]
		public float CoverArcThreshold = -0.75f;
	}
}
