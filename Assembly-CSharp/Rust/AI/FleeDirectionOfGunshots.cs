﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000131 RID: 305
	public sealed class FleeDirectionOfGunshots : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CB2 RID: 3250 RVA: 0x000518D0 File Offset: 0x0004FAD0
		public override float GetScore(BaseContext c, Vector3 option)
		{
			global::BaseNpc baseNpc = c.AIAgent as global::BaseNpc;
			if (baseNpc == null)
			{
				return 0f;
			}
			if (float.IsInfinity(baseNpc.SecondsSinceLastHeardGunshot) || float.IsNaN(baseNpc.SecondsSinceLastHeardGunshot))
			{
				return 0f;
			}
			float num = (this.WithinSeconds - baseNpc.SecondsSinceLastHeardGunshot) / this.WithinSeconds;
			if (num <= 0f)
			{
				return 0f;
			}
			Vector3 vector = option - baseNpc.transform.localPosition;
			float num2 = Vector3.Dot(baseNpc.LastHeardGunshotDirection, vector);
			return (this.Arc <= num2) ? 0f : 1f;
		}

		// Token: 0x040006DF RID: 1759
		[ApexSerialization]
		public float WithinSeconds = 10f;

		// Token: 0x040006E0 RID: 1760
		[ApexSerialization]
		public float Arc = -0.2f;
	}
}
