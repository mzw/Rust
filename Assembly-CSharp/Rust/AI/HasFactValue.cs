﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200015A RID: 346
	public class HasFactValue : BaseScorer
	{
		// Token: 0x06000D04 RID: 3332 RVA: 0x00052EDC File Offset: 0x000510DC
		public override float GetScore(BaseContext c)
		{
			byte b = c.GetFact(this.fact);
			return (b != this.value) ? 0f : 1f;
		}

		// Token: 0x04000718 RID: 1816
		[ApexSerialization]
		public global::BaseNpc.Facts fact;

		// Token: 0x04000719 RID: 1817
		[ApexSerialization(defaultValue = 0f)]
		public byte value;
	}
}
