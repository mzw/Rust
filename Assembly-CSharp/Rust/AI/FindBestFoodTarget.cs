﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200010D RID: 269
	public class FindBestFoodTarget : BaseActionWithOptions<global::BaseEntity>
	{
		// Token: 0x06000C65 RID: 3173 RVA: 0x00050954 File Offset: 0x0004EB54
		public override void DoExecute(BaseContext c)
		{
			global::BaseEntity baseEntity = base.GetBest(c, c.Memory.Visible);
			if (baseEntity == null || !c.AIAgent.WantsToEat(baseEntity))
			{
				baseEntity = null;
			}
			c.AIAgent.FoodTarget = baseEntity;
		}
	}
}
