﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000126 RID: 294
	public class HasIdleFor : BaseScorer
	{
		// Token: 0x06000C9C RID: 3228 RVA: 0x00051574 File Offset: 0x0004F774
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.GetStuckDuration < this.StuckSeconds) ? 0f : 1f;
		}

		// Token: 0x040006D9 RID: 1753
		[ApexSerialization]
		public float StuckSeconds = 5f;
	}
}
