﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000139 RID: 313
	public sealed class IsCrouched : BaseScorer
	{
		// Token: 0x06000CC0 RID: 3264 RVA: 0x00051D48 File Offset: 0x0004FF48
		public override float GetScore(BaseContext c)
		{
			global::NPCPlayerApex npcplayerApex = c.AIAgent as global::NPCPlayerApex;
			if (npcplayerApex != null)
			{
				return (!npcplayerApex.modelState.ducked) ? 0f : 1f;
			}
			return 0f;
		}
	}
}
