﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001C5 RID: 453
	public static class Sense
	{
		// Token: 0x06000EDC RID: 3804 RVA: 0x0005BDF8 File Offset: 0x00059FF8
		public static void Stimulate(Sensation sensation)
		{
			global::BaseEntity.Query.EntityTree server = global::BaseEntity.Query.Server;
			Vector3 position = sensation.Position;
			float radius = sensation.Radius;
			global::BaseEntity[] results = Sense.query;
			if (Sense.<>f__mg$cache0 == null)
			{
				Sense.<>f__mg$cache0 = new Func<global::BaseEntity, bool>(Sense.IsAbleToBeStimulated);
			}
			int inSphere = server.GetInSphere(position, radius, results, Sense.<>f__mg$cache0);
			for (int i = 0; i < inSphere; i++)
			{
				Sense.query[i].OnSensation(sensation);
			}
		}

		// Token: 0x06000EDD RID: 3805 RVA: 0x0005BE60 File Offset: 0x0005A060
		private static bool IsAbleToBeStimulated(global::BaseEntity ent)
		{
			return ent is global::BasePlayer || ent is global::BaseNpc;
		}

		// Token: 0x040008AE RID: 2222
		private static global::BaseEntity[] query = new global::BaseEntity[512];

		// Token: 0x040008AF RID: 2223
		[CompilerGenerated]
		private static Func<global::BaseEntity, bool> <>f__mg$cache0;
	}
}
