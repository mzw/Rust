﻿using System;
using ConVar;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001C7 RID: 455
	public class AiLocationSpawner : global::SpawnGroup
	{
		// Token: 0x06000EE7 RID: 3815 RVA: 0x0005C0D0 File Offset: 0x0005A2D0
		public override void SpawnInitial()
		{
			if (this.IsMainSpawner)
			{
				AiLocationSpawner.SquadSpawnerLocation location = this.Location;
				if (location != AiLocationSpawner.SquadSpawnerLocation.MilitaryTunnels)
				{
					this.defaultMaxPopulation = this.maxPopulation;
					this.defaultNumToSpawnPerTickMax = this.numToSpawnPerTickMax;
					this.defaultNumToSpawnPerTickMin = this.numToSpawnPerTickMin;
				}
				else
				{
					this.maxPopulation = ConVar.AI.npc_max_population_military_tunnels;
					this.numToSpawnPerTickMax = ConVar.AI.npc_spawn_per_tick_max_military_tunnels;
					this.numToSpawnPerTickMin = ConVar.AI.npc_spawn_per_tick_min_military_tunnels;
					this.respawnDelayMax = ConVar.AI.npc_respawn_delay_max_military_tunnels;
					this.respawnDelayMin = ConVar.AI.npc_respawn_delay_min_military_tunnels;
				}
			}
			else
			{
				this.defaultMaxPopulation = this.maxPopulation;
				this.defaultNumToSpawnPerTickMax = this.numToSpawnPerTickMax;
				this.defaultNumToSpawnPerTickMin = this.numToSpawnPerTickMin;
			}
			base.SpawnInitial();
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x0005C190 File Offset: 0x0005A390
		protected override void Spawn(int numToSpawn)
		{
			if (!ConVar.AI.npc_enable)
			{
				this.maxPopulation = 0;
				this.numToSpawnPerTickMax = 0;
				this.numToSpawnPerTickMin = 0;
				return;
			}
			if (numToSpawn == 0)
			{
				if (this.IsMainSpawner)
				{
					AiLocationSpawner.SquadSpawnerLocation location = this.Location;
					if (location != AiLocationSpawner.SquadSpawnerLocation.MilitaryTunnels)
					{
						this.maxPopulation = this.defaultMaxPopulation;
						this.numToSpawnPerTickMax = this.defaultNumToSpawnPerTickMax;
						this.numToSpawnPerTickMin = this.defaultNumToSpawnPerTickMin;
						numToSpawn = Random.Range(this.numToSpawnPerTickMin, this.numToSpawnPerTickMax + 1);
					}
					else
					{
						this.maxPopulation = ConVar.AI.npc_max_population_military_tunnels;
						this.numToSpawnPerTickMax = ConVar.AI.npc_spawn_per_tick_max_military_tunnels;
						this.numToSpawnPerTickMin = ConVar.AI.npc_spawn_per_tick_min_military_tunnels;
						numToSpawn = Random.Range(this.numToSpawnPerTickMin, this.numToSpawnPerTickMax + 1);
					}
				}
				else
				{
					this.maxPopulation = this.defaultMaxPopulation;
					this.numToSpawnPerTickMax = this.defaultNumToSpawnPerTickMax;
					this.numToSpawnPerTickMin = this.defaultNumToSpawnPerTickMin;
					numToSpawn = Random.Range(this.numToSpawnPerTickMin, this.numToSpawnPerTickMax + 1);
				}
			}
			float num = this.chance;
			AiLocationSpawner.SquadSpawnerLocation location2 = this.Location;
			if (location2 != AiLocationSpawner.SquadSpawnerLocation.JunkpileA)
			{
				if (location2 == AiLocationSpawner.SquadSpawnerLocation.JunkpileG)
				{
					num = ConVar.AI.npc_junkpile_g_spawn_chance;
				}
			}
			else
			{
				num = ConVar.AI.npc_junkpile_a_spawn_chance;
			}
			if (numToSpawn == 0 || Random.value > num || ((this.Location == AiLocationSpawner.SquadSpawnerLocation.JunkpileA || this.Location == AiLocationSpawner.SquadSpawnerLocation.JunkpileG) && global::NPCPlayerApex.AllJunkpileNPCs.Count >= ConVar.AI.npc_max_junkpile_count))
			{
				return;
			}
			numToSpawn = Mathf.Min(numToSpawn, this.maxPopulation - base.currentPopulation);
			for (int i = 0; i < numToSpawn; i++)
			{
				Vector3 pos;
				Quaternion rot;
				global::BaseSpawnPoint spawnPoint = this.GetSpawnPoint(out pos, out rot);
				if (spawnPoint)
				{
					global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(base.GetPrefab(), pos, rot, true);
					if (baseEntity)
					{
						if (this.Manager != null)
						{
							global::NPCPlayerApex npcplayerApex = baseEntity as global::NPCPlayerApex;
							if (npcplayerApex != null)
							{
								npcplayerApex.AiContext.AiLocationManager = this.Manager;
							}
						}
						baseEntity.Spawn();
						global::SpawnPointInstance spawnPointInstance = baseEntity.gameObject.AddComponent<global::SpawnPointInstance>();
						spawnPointInstance.parentSpawnGroup = this;
						spawnPointInstance.parentSpawnPoint = spawnPoint;
						spawnPointInstance.Notify();
					}
				}
			}
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x0005C3E0 File Offset: 0x0005A5E0
		protected override global::BaseSpawnPoint GetSpawnPoint(out Vector3 pos, out Quaternion rot)
		{
			return base.GetSpawnPoint(out pos, out rot);
		}

		// Token: 0x040008B5 RID: 2229
		public AiLocationSpawner.SquadSpawnerLocation Location;

		// Token: 0x040008B6 RID: 2230
		public AiLocationManager Manager;

		// Token: 0x040008B7 RID: 2231
		public bool IsMainSpawner = true;

		// Token: 0x040008B8 RID: 2232
		public float chance = 1f;

		// Token: 0x040008B9 RID: 2233
		private int defaultMaxPopulation;

		// Token: 0x040008BA RID: 2234
		private int defaultNumToSpawnPerTickMax;

		// Token: 0x040008BB RID: 2235
		private int defaultNumToSpawnPerTickMin;

		// Token: 0x020001C8 RID: 456
		public enum SquadSpawnerLocation
		{
			// Token: 0x040008BD RID: 2237
			MilitaryTunnels,
			// Token: 0x040008BE RID: 2238
			JunkpileA,
			// Token: 0x040008BF RID: 2239
			JunkpileG,
			// Token: 0x040008C0 RID: 2240
			None
		}
	}
}
