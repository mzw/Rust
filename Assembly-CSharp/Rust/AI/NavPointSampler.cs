﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x020001B8 RID: 440
	public static class NavPointSampler
	{
		// Token: 0x06000E74 RID: 3700 RVA: 0x0005B2F8 File Offset: 0x000594F8
		public static bool SampleCircle(NavPointSampler.SampleCount sampleCount, Vector3 center, float radius, NavPointSampler.SampleScoreParams scoreParams, ref List<NavPointSample> samples)
		{
			if (scoreParams.Agent == null || scoreParams.Agent.GetNavAgent == null)
			{
				return false;
			}
			float num = 90f;
			if (sampleCount != NavPointSampler.SampleCount.Eight)
			{
				if (sampleCount == NavPointSampler.SampleCount.Sixteen)
				{
					num = 22.5f;
				}
			}
			else
			{
				num = 45f;
			}
			int num2 = 1 + NavPointSampler.GetFeatureCount((int)scoreParams.Features);
			for (float num3 = 0f; num3 < 360f; num3 += num)
			{
				NavPointSample item = NavPointSampler.SamplePoint(NavPointSampler.GetPointOnCircle(center, radius, num3), scoreParams);
				if (item.Score > 0f)
				{
					samples.Add(item);
					if (item.Score >= (float)num2)
					{
						break;
					}
				}
			}
			samples.Sort(NavPointSampler.NavPointSampleComparer);
			return samples.Count > 0;
		}

		// Token: 0x06000E75 RID: 3701 RVA: 0x0005B3D8 File Offset: 0x000595D8
		public static int GetFeatureCount(int features)
		{
			int num = 0;
			while (features != 0)
			{
				features &= features - 1;
				num++;
			}
			return num;
		}

		// Token: 0x06000E76 RID: 3702 RVA: 0x0005B400 File Offset: 0x00059600
		public static Vector3 GetPointOnCircle(Vector3 center, float radius, float degrees)
		{
			float num = center.x + radius * Mathf.Cos(degrees * 0.0174532924f);
			float num2 = center.z + radius * Mathf.Sin(degrees * 0.0174532924f);
			return new Vector3(num, center.y, num2);
		}

		// Token: 0x06000E77 RID: 3703 RVA: 0x0005B44C File Offset: 0x0005964C
		public static NavPointSample SamplePoint(Vector3 pos, NavPointSampler.SampleScoreParams scoreParams)
		{
			float num = 0f;
			if (NavPointSampler._SampleNavMesh(ref pos, scoreParams.Agent) && NavPointSampler._WaterDepth(pos, scoreParams.WaterMaxDepth) > 0f)
			{
				num = 1f;
				if ((scoreParams.Features & NavPointSampler.SampleFeatures.DiscourageSharpTurns) > NavPointSampler.SampleFeatures.None)
				{
					num += NavPointSampler._DiscourageSharpTurns(pos, scoreParams.Agent);
				}
				if ((scoreParams.Features & NavPointSampler.SampleFeatures.RetreatFromTarget) > NavPointSampler.SampleFeatures.None)
				{
					num += NavPointSampler.RetreatPointValue(pos, scoreParams.Agent);
				}
				if ((scoreParams.Features & NavPointSampler.SampleFeatures.ApproachTarget) > NavPointSampler.SampleFeatures.None)
				{
					num += NavPointSampler.ApproachPointValue(pos, scoreParams.Agent);
				}
				if ((scoreParams.Features & NavPointSampler.SampleFeatures.FlankTarget) > NavPointSampler.SampleFeatures.None)
				{
					num += NavPointSampler.FlankPointValue(pos, scoreParams.Agent);
				}
				if ((scoreParams.Features & NavPointSampler.SampleFeatures.RetreatFromDirection) > NavPointSampler.SampleFeatures.None)
				{
					num += NavPointSampler.RetreatFromDirection(pos, scoreParams.Agent);
				}
				if ((scoreParams.Features & NavPointSampler.SampleFeatures.RetreatFromExplosive) > NavPointSampler.SampleFeatures.None)
				{
					num += NavPointSampler.RetreatPointValue(pos, scoreParams.Agent);
				}
			}
			return new NavPointSample
			{
				Position = pos,
				Score = num
			};
		}

		// Token: 0x06000E78 RID: 3704 RVA: 0x0005B568 File Offset: 0x00059768
		private static bool _SampleNavMesh(ref Vector3 pos, global::IAIAgent agent)
		{
			NavMeshHit navMeshHit;
			if (NavMesh.SamplePosition(pos, ref navMeshHit, agent.GetNavAgent.height * 2f, agent.GetNavAgent.areaMask))
			{
				pos = navMeshHit.position;
				return true;
			}
			return false;
		}

		// Token: 0x06000E79 RID: 3705 RVA: 0x0005B5B4 File Offset: 0x000597B4
		private static float _WaterDepth(Vector3 pos, float maxDepth)
		{
			float num = global::WaterLevel.GetWaterDepth(pos);
			if (Mathf.Approximately(num, 0f))
			{
				return 1f;
			}
			num = Mathf.Min(num, maxDepth);
			return 1f - num / maxDepth;
		}

		// Token: 0x06000E7A RID: 3706 RVA: 0x0005B5F0 File Offset: 0x000597F0
		private static float _DiscourageSharpTurns(Vector3 pos, global::IAIAgent agent)
		{
			Vector3 normalized = (pos - agent.Entity.ServerPosition).normalized;
			float num = Vector3.Dot(agent.Entity.transform.forward, normalized);
			if (num > 0.45f)
			{
				return 1f;
			}
			if (num > 0f)
			{
				return num;
			}
			return 0f;
		}

		// Token: 0x06000E7B RID: 3707 RVA: 0x0005B654 File Offset: 0x00059854
		public static bool IsValidPointDirectness(Vector3 point, Vector3 pos, Vector3 targetPos)
		{
			Vector3 vector = pos - targetPos;
			Vector3 vector2 = pos - point;
			float num = Vector3.Dot(vector.normalized, vector2.normalized);
			return num <= 0.5f || vector2.sqrMagnitude <= vector.sqrMagnitude;
		}

		// Token: 0x06000E7C RID: 3708 RVA: 0x0005B6A8 File Offset: 0x000598A8
		public static bool PointDirectnessToTarget(Vector3 point, Vector3 pos, Vector3 targetPos, out float value)
		{
			Vector3 vector = point - pos;
			Vector3 vector2 = targetPos - pos;
			value = Vector3.Dot(vector.normalized, vector2.normalized);
			if (value > 0.5f && vector.sqrMagnitude > vector2.sqrMagnitude)
			{
				value = 0f;
				return false;
			}
			return true;
		}

		// Token: 0x06000E7D RID: 3709 RVA: 0x0005B704 File Offset: 0x00059904
		public static float RetreatPointValue(Vector3 point, global::IAIAgent agent)
		{
			if (agent.AttackTarget == null)
			{
				return 0f;
			}
			float num = 0f;
			if (!NavPointSampler.PointDirectnessToTarget(point, agent.Entity.ServerPosition, agent.AttackTarget.ServerPosition, out num))
			{
				return 0f;
			}
			if (num <= -0.5f)
			{
				return num * -1f;
			}
			return 0f;
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x0005B770 File Offset: 0x00059970
		public static float RetreatPointValueExplosive(Vector3 point, global::IAIAgent agent)
		{
			BaseContext baseContext = agent.GetContext(Guid.Empty) as BaseContext;
			if (baseContext == null || baseContext.DeployedExplosives.Count == 0 || baseContext.DeployedExplosives[0] == null || baseContext.DeployedExplosives[0].IsDestroyed)
			{
				return 0f;
			}
			float num = 0f;
			if (!NavPointSampler.PointDirectnessToTarget(point, agent.Entity.ServerPosition, baseContext.DeployedExplosives[0].ServerPosition, out num))
			{
				return 0f;
			}
			if (num <= -0.5f)
			{
				return num * -1f;
			}
			return 0f;
		}

		// Token: 0x06000E7F RID: 3711 RVA: 0x0005B824 File Offset: 0x00059A24
		public static float ApproachPointValue(Vector3 point, global::IAIAgent agent)
		{
			if (agent.AttackTarget == null)
			{
				return 0f;
			}
			float num = 0f;
			if (!NavPointSampler.PointDirectnessToTarget(point, agent.Entity.ServerPosition, agent.AttackTarget.ServerPosition, out num))
			{
				return 0f;
			}
			if (num >= 0.5f)
			{
				return num;
			}
			return 0f;
		}

		// Token: 0x06000E80 RID: 3712 RVA: 0x0005B88C File Offset: 0x00059A8C
		public static float FlankPointValue(Vector3 point, global::IAIAgent agent)
		{
			if (agent.AttackTarget == null)
			{
				return 0f;
			}
			float num = 0f;
			if (!NavPointSampler.PointDirectnessToTarget(point, agent.Entity.ServerPosition, agent.AttackTarget.ServerPosition, out num))
			{
				return 0f;
			}
			if (num >= -0.1f && num <= 0.1f)
			{
				return 1f;
			}
			return 0f;
		}

		// Token: 0x06000E81 RID: 3713 RVA: 0x0005B900 File Offset: 0x00059B00
		public static float RetreatFromDirection(Vector3 point, global::IAIAgent agent)
		{
			if (agent.Entity.LastAttackedDir == Vector3.zero)
			{
				return 0f;
			}
			Vector3 normalized = (point - agent.Entity.ServerPosition).normalized;
			float num = Vector3.Dot(agent.Entity.LastAttackedDir, normalized);
			if (num > -0.5f)
			{
				return 0f;
			}
			return 1f;
		}

		// Token: 0x0400088E RID: 2190
		private const float HalfPI = 0.0174532924f;

		// Token: 0x0400088F RID: 2191
		private static readonly NavPointSampleComparer NavPointSampleComparer = new NavPointSampleComparer();

		// Token: 0x020001B9 RID: 441
		public enum SampleCount
		{
			// Token: 0x04000891 RID: 2193
			Four,
			// Token: 0x04000892 RID: 2194
			Eight,
			// Token: 0x04000893 RID: 2195
			Sixteen
		}

		// Token: 0x020001BA RID: 442
		[Flags]
		public enum SampleFeatures
		{
			// Token: 0x04000895 RID: 2197
			None = 0,
			// Token: 0x04000896 RID: 2198
			DiscourageSharpTurns = 1,
			// Token: 0x04000897 RID: 2199
			RetreatFromTarget = 2,
			// Token: 0x04000898 RID: 2200
			ApproachTarget = 4,
			// Token: 0x04000899 RID: 2201
			FlankTarget = 8,
			// Token: 0x0400089A RID: 2202
			RetreatFromDirection = 16,
			// Token: 0x0400089B RID: 2203
			RetreatFromExplosive = 32
		}

		// Token: 0x020001BB RID: 443
		public struct SampleScoreParams
		{
			// Token: 0x0400089C RID: 2204
			public float WaterMaxDepth;

			// Token: 0x0400089D RID: 2205
			public global::IAIAgent Agent;

			// Token: 0x0400089E RID: 2206
			public NavPointSampler.SampleFeatures Features;
		}
	}
}
