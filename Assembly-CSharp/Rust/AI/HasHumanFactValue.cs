﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000161 RID: 353
	public class HasHumanFactValue : BaseScorer
	{
		// Token: 0x06000D12 RID: 3346 RVA: 0x00053074 File Offset: 0x00051274
		public override float GetScore(BaseContext c)
		{
			byte b = c.GetFact(this.fact);
			return (b != this.value) ? 0f : 1f;
		}

		// Token: 0x04000722 RID: 1826
		[ApexSerialization]
		public global::NPCPlayerApex.Facts fact;

		// Token: 0x04000723 RID: 1827
		[ApexSerialization(defaultValue = 0f)]
		public byte value;
	}
}
