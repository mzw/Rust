﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200011B RID: 283
	public class AtDestinationForRandom : BaseScorer
	{
		// Token: 0x06000C86 RID: 3206 RVA: 0x00051334 File Offset: 0x0004F534
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.TimeAtDestination < Random.Range(this.MinDuration, this.MaxDuration)) ? 0f : 1f;
		}

		// Token: 0x040006D4 RID: 1748
		[ApexSerialization]
		public float MinDuration = 2.5f;

		// Token: 0x040006D5 RID: 1749
		[ApexSerialization]
		public float MaxDuration = 5f;
	}
}
