﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200013F RID: 319
	public class RangeFromHome : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CCC RID: 3276 RVA: 0x00051F38 File Offset: 0x00050138
		public override float GetScore(BaseContext c, Vector3 position)
		{
			float num = Mathf.Min(Vector3.Distance(position, c.AIAgent.SpawnPosition), this.Range) / this.Range;
			return (!this.UseResponseCurve) ? num : this.ResponseCurve.Evaluate(num);
		}

		// Token: 0x040006F7 RID: 1783
		[ApexSerialization]
		public float Range = 50f;

		// Token: 0x040006F8 RID: 1784
		[ApexSerialization]
		public AnimationCurve ResponseCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

		// Token: 0x040006F9 RID: 1785
		[ApexSerialization]
		public bool UseResponseCurve = true;
	}
}
