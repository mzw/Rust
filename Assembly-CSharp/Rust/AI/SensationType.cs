﻿using System;

namespace Rust.Ai
{
	// Token: 0x020001C3 RID: 451
	public enum SensationType
	{
		// Token: 0x040008A9 RID: 2217
		Gunshot,
		// Token: 0x040008AA RID: 2218
		ThrownWeapon
	}
}
