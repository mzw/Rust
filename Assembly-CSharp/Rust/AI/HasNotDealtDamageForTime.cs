﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200012F RID: 303
	public sealed class HasNotDealtDamageForTime : BaseScorer
	{
		// Token: 0x06000CAE RID: 3246 RVA: 0x000517D0 File Offset: 0x0004F9D0
		public override float GetScore(BaseContext c)
		{
			if (float.IsInfinity(c.Entity.SecondsSinceDealtDamage) || float.IsNaN(c.Entity.SecondsSinceDealtDamage))
			{
				return 0f;
			}
			return (c.Entity.SecondsSinceDealtDamage <= this.ForSeconds) ? 0f : 1f;
		}

		// Token: 0x040006DD RID: 1757
		[ApexSerialization]
		public float ForSeconds = 10f;
	}
}
