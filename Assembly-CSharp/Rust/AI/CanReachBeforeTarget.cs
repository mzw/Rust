﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000186 RID: 390
	public sealed class CanReachBeforeTarget : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000D6F RID: 3439 RVA: 0x00054BEC File Offset: 0x00052DEC
		public override float GetScore(BaseContext c, Vector3 point)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			float num = Vector3.Distance(c.AIAgent.AttackTargetMemory.Position, point);
			float num2 = Vector3.Distance(c.Position, point);
			return (num2 >= num) ? 0f : 1f;
		}
	}
}
