﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000103 RID: 259
	public sealed class AggressionRangeFromTargetEntity : BaseScorer
	{
		// Token: 0x06000C4E RID: 3150 RVA: 0x000504A0 File Offset: 0x0004E6A0
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null || Mathf.Approximately(c.AIAgent.GetStats.AggressionRange, 0f))
			{
				return 0f;
			}
			return Vector3.Distance(c.AIAgent.AttackPosition, c.AIAgent.AttackTargetMemory.Position) / c.AIAgent.GetStats.AggressionRange;
		}
	}
}
