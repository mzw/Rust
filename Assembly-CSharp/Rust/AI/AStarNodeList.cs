﻿using System;
using System.Collections.Generic;

namespace Rust.AI
{
	// Token: 0x020000C6 RID: 198
	public class AStarNodeList : List<AStarNode>
	{
		// Token: 0x06000AB3 RID: 2739 RVA: 0x00048E6C File Offset: 0x0004706C
		public bool Contains(global::BasePathNode n)
		{
			for (int i = 0; i < base.Count; i++)
			{
				AStarNode astarNode = base[i];
				if (astarNode != null && astarNode.Node.Equals(n))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000AB4 RID: 2740 RVA: 0x00048EB4 File Offset: 0x000470B4
		public AStarNode GetAStarNodeOf(global::BasePathNode n)
		{
			for (int i = 0; i < base.Count; i++)
			{
				AStarNode astarNode = base[i];
				if (astarNode != null && astarNode.Node.Equals(n))
				{
					return astarNode;
				}
			}
			return null;
		}

		// Token: 0x06000AB5 RID: 2741 RVA: 0x00048EFC File Offset: 0x000470FC
		public void AStarNodeSort()
		{
			base.Sort(this.comparer);
		}

		// Token: 0x0400057B RID: 1403
		private readonly AStarNodeList.AStarNodeComparer comparer = new AStarNodeList.AStarNodeComparer();

		// Token: 0x020000C7 RID: 199
		private class AStarNodeComparer : IComparer<AStarNode>
		{
			// Token: 0x06000AB7 RID: 2743 RVA: 0x00048F14 File Offset: 0x00047114
			int IComparer<AStarNode>.Compare(AStarNode lhs, AStarNode rhs)
			{
				if (lhs < rhs)
				{
					return -1;
				}
				if (lhs > rhs)
				{
					return 1;
				}
				return 0;
			}
		}
	}
}
