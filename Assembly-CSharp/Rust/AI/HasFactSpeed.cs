﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200015E RID: 350
	public class HasFactSpeed : BaseScorer
	{
		// Token: 0x06000D0C RID: 3340 RVA: 0x00052FDC File Offset: 0x000511DC
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::BaseNpc.Facts.Speed);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x0400071E RID: 1822
		[ApexSerialization(defaultValue = global::BaseNpc.SpeedEnum.StandStill)]
		public global::BaseNpc.SpeedEnum value;
	}
}
