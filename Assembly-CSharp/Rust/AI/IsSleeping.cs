﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200012B RID: 299
	public sealed class IsSleeping : BaseScorer
	{
		// Token: 0x06000CA6 RID: 3238 RVA: 0x0005167C File Offset: 0x0004F87C
		public override float GetScore(BaseContext c)
		{
			return (float)((c.AIAgent.CurrentBehaviour != global::BaseNpc.Behaviour.Sleep) ? 0 : 1);
		}
	}
}
