﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x020001A6 RID: 422
	public class NavMeshGridCell : FacepunchBehaviour, IServerComponent
	{
		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000E0D RID: 3597 RVA: 0x00058438 File Offset: 0x00056638
		public int LayerCount
		{
			get
			{
				return this.Layers.Count;
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000E0E RID: 3598 RVA: 0x00058448 File Offset: 0x00056648
		// (set) Token: 0x06000E0F RID: 3599 RVA: 0x00058450 File Offset: 0x00056650
		public Vector2i Coord { get; internal set; }

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000E10 RID: 3600 RVA: 0x0005845C File Offset: 0x0005665C
		// (set) Token: 0x06000E11 RID: 3601 RVA: 0x000584C8 File Offset: 0x000566C8
		public bool IsAwaitingBake
		{
			get
			{
				foreach (NavMeshGridCell.Layer layer in this.Layers)
				{
					if (layer.IsAwaitingBake)
					{
						return true;
					}
				}
				return false;
			}
			set
			{
				if (value)
				{
					foreach (NavMeshGridCell.Layer layer in this.Layers)
					{
						layer.IsAwaitingBake = true;
					}
				}
			}
		}

		// Token: 0x06000E12 RID: 3602 RVA: 0x0005852C File Offset: 0x0005672C
		public void Setup(Vector2i coord, Vector3 position, Vector3 size, LayerMask layerMask, NavMeshCollectGeometry navMeshCollectGeometry)
		{
			this.Coord = coord;
			base.transform.position = position;
			this.TimeLastSeenByPlayer = Time.realtimeSinceStartup;
			this.Bounds = new Bounds(Vector3.zero, size + new Vector3(0f, 0f, 0f));
		}

		// Token: 0x06000E13 RID: 3603 RVA: 0x00058584 File Offset: 0x00056784
		public NavMeshGridCell.Layer GetLayer(int agentTypeIndex)
		{
			foreach (NavMeshGridCell.Layer layer in this.Layers)
			{
				if (layer.AgentTypeIndex == agentTypeIndex)
				{
					return layer;
				}
			}
			return null;
		}

		// Token: 0x06000E14 RID: 3604 RVA: 0x000585F0 File Offset: 0x000567F0
		public NavMeshGridCell.Layer CreateLayer(int agentTypeIndex, int area)
		{
			NavMeshGridCell.Layer layer = new NavMeshGridCell.Layer(this, agentTypeIndex, area);
			this.Layers.Add(layer);
			return layer;
		}

		// Token: 0x06000E15 RID: 3605 RVA: 0x00058614 File Offset: 0x00056814
		public IEnumerator CreateNavMeshLayerAsync(NavMeshGridCell.Layer layer, LayerMask layerMask, Action<NavMeshGridCell.Layer> onAsyncDoneCallback)
		{
			this.LastBakeTime = Time.realtimeSinceStartup;
			if (this.terrainSource == null)
			{
				this.terrainSource = new global::AsyncTerrainNavMeshBake(base.transform.position, (int)this.Bounds.size.x, (int)this.Bounds.size.y, false, true);
				yield return this.terrainSource;
			}
			if (this.terrainSource != null && layer.terrainBuildSource.area != layer.Surface.defaultArea)
			{
				layer.terrainBuildSource = this.terrainSource.CreateNavMeshBuildSource(this.Layers.Count == 1);
				layer.terrainBuildSource.area = layer.Surface.defaultArea;
				if (this.Layers.Count > 1)
				{
					layer.terrainBuildSource.sourceObject = this.Layers[0].terrainBuildSource.sourceObject;
				}
				layer.Sources.Add(layer.terrainBuildSource);
			}
			List<NavMeshBuildSource> removeSources = null;
			foreach (NavMeshBuildSource item in layer.Sources)
			{
				if (item.sourceObject == null)
				{
					if (removeSources == null)
					{
						removeSources = Pool.GetList<NavMeshBuildSource>();
					}
					removeSources.Add(item);
				}
			}
			if (removeSources != null)
			{
				foreach (NavMeshBuildSource item2 in removeSources)
				{
					layer.Sources.Remove(item2);
				}
				Pool.FreeList<NavMeshBuildSource>(ref removeSources);
			}
			yield return layer.GenerateNavmeshAsync(this, layerMask, layer.Sources);
			if (onAsyncDoneCallback != null)
			{
				onAsyncDoneCallback(layer);
			}
			yield break;
		}

		// Token: 0x06000E16 RID: 3606 RVA: 0x00058644 File Offset: 0x00056844
		public IEnumerator WaitForBuildToFinish(int agentTypeIndex, Action<NavMeshGridCell.Layer> onAsyncDoneCallback)
		{
			NavMeshGridCell.Layer layer = this.GetLayer(agentTypeIndex);
			while (layer.BuildingOperation != null && !layer.BuildingOperation.isDone)
			{
				yield return null;
			}
			if (onAsyncDoneCallback != null)
			{
				onAsyncDoneCallback(layer);
			}
			yield break;
		}

		// Token: 0x06000E17 RID: 3607 RVA: 0x00058670 File Offset: 0x00056870
		[ContextMenu("Rebake")]
		public void RebakeCell()
		{
			base.StartCoroutine(this.RebakeCellAsync(null));
		}

		// Token: 0x06000E18 RID: 3608 RVA: 0x00058680 File Offset: 0x00056880
		public IEnumerator RebakeCellAsync(Action onAsyncDoneCallback = null)
		{
			this.LastBakeTime = Time.realtimeSinceStartup;
			int count = this.Layers.Count;
			for (int i = 0; i < count; i++)
			{
				NavMeshGridCell.Layer layer = this.Layers[i];
				if (!layer.IsBuilding)
				{
					if (this.terrainSource == null)
					{
						this.terrainSource = new global::AsyncTerrainNavMeshBake(base.transform.position, (int)(this.Bounds.size.x + 0.5f), (int)(this.Bounds.size.y + 0.5f), false, true);
						yield return this.terrainSource;
					}
					if (this.terrainSource != null && layer.terrainBuildSource.area != layer.Surface.defaultArea)
					{
						layer.terrainBuildSource = this.terrainSource.CreateNavMeshBuildSource(this.Layers.Count == 1);
						layer.terrainBuildSource.area = layer.Surface.defaultArea;
						if (this.Layers.Count > 1)
						{
							layer.terrainBuildSource.sourceObject = this.Layers[0].terrainBuildSource.sourceObject;
						}
						layer.Sources.Add(layer.terrainBuildSource);
					}
					List<NavMeshBuildSource> removeSources = null;
					foreach (NavMeshBuildSource item in layer.Sources)
					{
						if (item.sourceObject == null)
						{
							if (removeSources == null)
							{
								removeSources = Pool.GetList<NavMeshBuildSource>();
							}
							removeSources.Add(item);
						}
					}
					if (removeSources != null)
					{
						foreach (NavMeshBuildSource item2 in removeSources)
						{
							layer.Sources.Remove(item2);
						}
						Pool.FreeList<NavMeshBuildSource>(ref removeSources);
					}
					yield return layer.GenerateNavmeshAsync(this, SingletonComponent<AiManager>.Instance.LayerMask, layer.Sources);
				}
			}
			if (onAsyncDoneCallback != null)
			{
				onAsyncDoneCallback();
			}
			yield break;
		}

		// Token: 0x06000E19 RID: 3609 RVA: 0x000586A4 File Offset: 0x000568A4
		private static Vector3 Quantize(Vector3 v, Vector3 quant)
		{
			float num = quant.x * Mathf.Floor(v.x / quant.x);
			float num2 = quant.y * Mathf.Floor(v.y / quant.y);
			float num3 = quant.z * Mathf.Floor(v.z / quant.z);
			return new Vector3(num, num2, num3);
		}

		// Token: 0x06000E1A RID: 3610 RVA: 0x00058710 File Offset: 0x00056910
		public void KeepAlive()
		{
			this.TimeLastSeenByPlayer = Time.realtimeSinceStartup;
		}

		// Token: 0x06000E1B RID: 3611 RVA: 0x00058720 File Offset: 0x00056920
		public bool Timeout(float timeoutThreshold)
		{
			return Time.realtimeSinceStartup - this.TimeLastSeenByPlayer > timeoutThreshold;
		}

		// Token: 0x06000E1C RID: 3612 RVA: 0x00058734 File Offset: 0x00056934
		public void Kill()
		{
			foreach (NavMeshGridCell.Layer layer in this.Layers)
			{
				layer.Kill();
			}
			Object.Destroy(base.gameObject);
		}

		// Token: 0x06000E1D RID: 3613 RVA: 0x0005879C File Offset: 0x0005699C
		public bool IsAtBorder(Vector3 position, float threshold, bool mustHaveValidNeighbour)
		{
			Vector3 vector = position - base.transform.position;
			if (this.Bounds.extents.x - vector.sqrMagnitude >= threshold)
			{
				return false;
			}
			if (!mustHaveValidNeighbour)
			{
				return true;
			}
			if (Mathf.Abs(vector.x) < Mathf.Abs(vector.z))
			{
				if (vector.z > 0f)
				{
					return SingletonComponent<AiManager>.Instance.HasReadyNeighbour(this.Coord, Vector2i.forward);
				}
				return SingletonComponent<AiManager>.Instance.HasReadyNeighbour(this.Coord, Vector2i.back);
			}
			else
			{
				if (vector.x < 0f)
				{
					return SingletonComponent<AiManager>.Instance.HasReadyNeighbour(this.Coord, Vector2i.left);
				}
				return SingletonComponent<AiManager>.Instance.HasReadyNeighbour(this.Coord, Vector2i.right);
			}
		}

		// Token: 0x06000E1E RID: 3614 RVA: 0x0005887C File Offset: 0x00056A7C
		public NavMeshGridCell.NavMeshLinkInfo GetOrCreateLink(int agentTypeIndex, Vector3 position, float threshold)
		{
			NavMeshGridCell.Layer layer = this.GetLayer(agentTypeIndex);
			if (layer == null || layer.NavMeshLinkInfos == null || layer.IsBuilding || layer.IsAwaitingBake)
			{
				return null;
			}
			Vector3 direction = position - base.transform.position;
			foreach (NavMeshGridCell.NavMeshLinkInfo navMeshLinkInfo in layer.NavMeshLinkInfos)
			{
				Vector3 vector = base.transform.position + navMeshLinkInfo.Link.startPoint;
				if ((position - vector).sqrMagnitude < threshold)
				{
					return navMeshLinkInfo;
				}
			}
			while (layer.NavMeshLinkInfos.Count > 0)
			{
				this.RemoveLink(layer.NavMeshLinkInfos[layer.NavMeshLinkInfos.Count - 1]);
			}
			NavMeshLink link;
			if (layer.TryGenerateLinkInDirection(this, layer.Surface, position, direction, out link))
			{
				NavMeshGridCell.NavMeshLinkInfo navMeshLinkInfo2 = Pool.Get<NavMeshGridCell.NavMeshLinkInfo>();
				navMeshLinkInfo2.Cell = this;
				navMeshLinkInfo2.Layer = layer;
				navMeshLinkInfo2.Link = link;
				layer.NavMeshLinkInfos.Add(navMeshLinkInfo2);
				return navMeshLinkInfo2;
			}
			return null;
		}

		// Token: 0x06000E1F RID: 3615 RVA: 0x000589D0 File Offset: 0x00056BD0
		public bool RemoveLink(NavMeshGridCell.NavMeshLinkInfo info)
		{
			if (info.Layer.NavMeshLinkInfos.Remove(info))
			{
				info.References = 0;
				NavMeshLink link = info.Link;
				link.transform.SetParent(null);
				SingletonComponent<AiManager>.Instance.LinkPool.Push(link.gameObject);
				Pool.Free<NavMeshGridCell.NavMeshLinkInfo>(ref info);
				return true;
			}
			return false;
		}

		// Token: 0x0400080C RID: 2060
		public readonly List<NavMeshGridCell.Layer> Layers = new List<NavMeshGridCell.Layer>(1);

		// Token: 0x0400080D RID: 2061
		public float TimeLastSeenByPlayer;

		// Token: 0x0400080E RID: 2062
		public Bounds Bounds;

		// Token: 0x0400080F RID: 2063
		private global::AsyncTerrainNavMeshBake terrainSource;

		// Token: 0x04000810 RID: 2064
		public float LastBakeTime;

		// Token: 0x020001A7 RID: 423
		public class NavMeshLinkInfo
		{
			// Token: 0x04000812 RID: 2066
			public NavMeshGridCell Cell;

			// Token: 0x04000813 RID: 2067
			public NavMeshGridCell.Layer Layer;

			// Token: 0x04000814 RID: 2068
			public NavMeshLink Link;

			// Token: 0x04000815 RID: 2069
			public int References;
		}

		// Token: 0x020001A8 RID: 424
		public class Layer
		{
			// Token: 0x06000E21 RID: 3617 RVA: 0x00058A34 File Offset: 0x00056C34
			public Layer(NavMeshGridCell cell, int agentTypeIndex, int areaMask)
			{
				this.IsBuilding = false;
				this.AgentTypeIndex = agentTypeIndex;
				this.BuildSettings = NavMesh.GetSettingsByIndex(agentTypeIndex);
				this.terrainBuildSource.area = -1;
				if (agentTypeIndex == 0)
				{
					this.BuildSettings.overrideVoxelSize = true;
					this.BuildSettings.voxelSize = this.BuildSettings.voxelSize * 2f;
				}
				else if (agentTypeIndex == 1)
				{
					this.BuildSettings.overrideVoxelSize = true;
					this.BuildSettings.voxelSize = this.BuildSettings.voxelSize * 2f;
				}
				int areaFromName = NavMesh.GetAreaFromName("Walkable");
				if ((areaMask & 1 << NavMesh.GetAreaFromName("HumanNPC")) != 0)
				{
					areaFromName = NavMesh.GetAreaFromName("HumanNPC");
				}
				else if ((areaMask & 1 << NavMesh.GetAreaFromName("Road")) != 0)
				{
					areaFromName = NavMesh.GetAreaFromName("Road");
				}
				this.Surface = cell.gameObject.AddComponent<NavMeshSurface>();
				this.Surface.layerMask = SingletonComponent<AiManager>.Instance.LayerMask;
				this.Surface.agentTypeID = this.BuildSettings.agentTypeID;
				this.Surface.defaultArea = areaFromName;
				this.Surface.center = Vector3.zero;
				this.Surface.size = cell.Bounds.size;
				this.Surface.overrideVoxelSize = true;
				this.Surface.voxelSize = this.BuildSettings.voxelSize;
				this.Surface.overrideTileSize = true;
				this.Surface.tileSize = this.BuildSettings.tileSize;
				this.Surface.buildHeightMesh = false;
				this.Surface.collectObjects = 1;
				this.Surface.useGeometry = 1;
				this.Surface.UpdateOnTransformChanged = false;
				this.NavMeshLinkInfos = new List<NavMeshGridCell.NavMeshLinkInfo>();
				NavMeshBuilder.CollectSources(new Bounds(NavMeshGridCell.Quantize(cell.transform.position, cell.Bounds.size * 0.1f), cell.Bounds.size), SingletonComponent<AiManager>.Instance.LayerMask, SingletonComponent<AiManager>.Instance.NavMeshCollectGeometry, this.Surface.defaultArea, this.markups, this.Sources);
			}

			// Token: 0x170000C8 RID: 200
			// (get) Token: 0x06000E22 RID: 3618 RVA: 0x00058C84 File Offset: 0x00056E84
			// (set) Token: 0x06000E23 RID: 3619 RVA: 0x00058C8C File Offset: 0x00056E8C
			public bool IsBuilding { get; internal set; }

			// Token: 0x06000E24 RID: 3620 RVA: 0x00058C98 File Offset: 0x00056E98
			internal bool TryGenerateLinkInDirection(NavMeshGridCell cell, NavMeshSurface surface, Vector3 position, Vector3 direction, out NavMeshLink link)
			{
				link = null;
				float multiplier = 1.5f;
				bool result;
				if (Mathf.Abs(direction.x) < Mathf.Abs(direction.z))
				{
					if (direction.z > 0f)
					{
						Vector3 vector = Vector3.forward * surface.size.z / 2f;
						Vector3 vector2 = (direction.x <= 0f) ? Vector3.right : Vector3.left;
						Vector3 cellCenterOffset = vector + vector2 * surface.size.x / 2f;
						cellCenterOffset.x += direction.x - vector2.x * surface.size.x / 2f;
						result = this.TryGenerateLink(cell, surface, cellCenterOffset, Vector3.back, Vector3.forward, multiplier, out link);
					}
					else
					{
						Vector3 vector3 = Vector3.back * surface.size.z / 2f;
						Vector3 vector4 = (direction.x <= 0f) ? Vector3.right : Vector3.left;
						Vector3 cellCenterOffset2 = vector3 + vector4 * surface.size.x / 2f;
						cellCenterOffset2.x += direction.x - vector4.x * surface.size.x / 2f;
						result = this.TryGenerateLink(cell, surface, cellCenterOffset2, Vector3.forward, Vector3.back, multiplier, out link);
					}
				}
				else if (direction.x < 0f)
				{
					Vector3 vector5 = Vector3.left * surface.size.x / 2f;
					Vector3 vector6 = (direction.z <= 0f) ? Vector3.forward : Vector3.back;
					Vector3 cellCenterOffset3 = vector5 + vector6 * surface.size.z / 2f;
					cellCenterOffset3.z += direction.z - vector6.z * surface.size.z / 2f;
					result = this.TryGenerateLink(cell, surface, cellCenterOffset3, Vector3.right, Vector3.left, multiplier, out link);
				}
				else
				{
					Vector3 vector7 = Vector3.right * (surface.size.x / 2f);
					Vector3 vector8 = (direction.z <= 0f) ? Vector3.forward : Vector3.back;
					Vector3 cellCenterOffset4 = vector7 + vector8 * surface.size.z / 2f;
					cellCenterOffset4.z += direction.z - vector8.z * surface.size.z / 2f;
					result = this.TryGenerateLink(cell, surface, cellCenterOffset4, Vector3.left, Vector3.right, multiplier, out link);
				}
				return result;
			}

			// Token: 0x06000E25 RID: 3621 RVA: 0x00058FE8 File Offset: 0x000571E8
			public bool TryGenerateLink(NavMeshGridCell cell, NavMeshSurface surface, Vector3 cellCenterOffset, Vector3 start, Vector3 end, float multiplier, out NavMeshLink link)
			{
				int num = 0;
				Vector3 vector = surface.transform.position + cellCenterOffset;
				RaycastHit raycastHit;
				if (global::TransformUtil.GetGroundInfo(vector + new Vector3(0f, 50f, 0f), out raycastHit, 100f, 295772417, null))
				{
					vector = raycastHit.point;
				}
				for (int i = 0; i < 3; i++)
				{
					vector += start * multiplier;
					Vector2i coord = SingletonComponent<AiManager>.Instance.GetCoord(vector);
					NavMeshHit navMeshHit;
					if (coord == cell.Coord && NavMesh.SamplePosition(vector, ref navMeshHit, 10f, 1 << surface.defaultArea))
					{
						coord = SingletonComponent<AiManager>.Instance.GetCoord(navMeshHit.position);
						if (coord == cell.Coord)
						{
							vector = navMeshHit.position;
							num++;
							break;
						}
					}
				}
				start = vector;
				Vector3 vector2 = surface.transform.position + cellCenterOffset;
				if (global::TransformUtil.GetGroundInfo(vector2 + new Vector3(0f, 50f, 0f), out raycastHit, 100f, 295772417, null))
				{
					vector2 = raycastHit.point;
				}
				for (int j = 0; j < 3; j++)
				{
					vector2 += end * multiplier;
					Vector2i coord2 = SingletonComponent<AiManager>.Instance.GetCoord(vector2);
					NavMeshHit navMeshHit2;
					if (coord2 != cell.Coord && NavMesh.SamplePosition(vector2 + new Vector3(0f, 1.25f, 0f), ref navMeshHit2, 1.5f, 1 << surface.defaultArea))
					{
						coord2 = SingletonComponent<AiManager>.Instance.GetCoord(navMeshHit2.position);
						if (coord2 != cell.Coord)
						{
							vector2 = navMeshHit2.position;
							num++;
							break;
						}
					}
				}
				end = vector2;
				RaycastHit raycastHit2;
				if (num == 2 && !Physics.Linecast(start + Vector3.up * 0.5f, end + Vector3.up * 0.5f, ref raycastHit2, surface.layerMask))
				{
					GameObject gameObject = SingletonComponent<AiManager>.Instance.LinkPool.Pop(default(Vector3), default(Quaternion));
					if (gameObject == null)
					{
						gameObject = SingletonComponent<AiManager>.Instance.NavMeshLinkPrefab.Instantiate(null);
						gameObject.EnablePooling(SingletonComponent<AiManager>.Instance.NavMeshLinkPrefab.resourceID);
					}
					gameObject.transform.SetParent(surface.transform);
					gameObject.transform.localPosition = default(Vector3);
					link = gameObject.GetComponent<NavMeshLink>();
					link.autoUpdate = false;
					link.bidirectional = true;
					link.agentTypeID = this.BuildSettings.agentTypeID;
					link.area = surface.defaultArea;
					link.UpdateOnTransformChanged = false;
					link.startPoint = start - surface.transform.position;
					link.endPoint = end - surface.transform.position;
					link.width = 1f;
					return true;
				}
				link = null;
				return false;
			}

			// Token: 0x06000E26 RID: 3622 RVA: 0x0005934C File Offset: 0x0005754C
			internal IEnumerator GenerateNavmeshAsync(NavMeshGridCell cell, LayerMask layerMask, List<NavMeshBuildSource> sources)
			{
				if (this.IsBuilding)
				{
					yield break;
				}
				this.IsBuilding = true;
				this.IsAwaitingBake = false;
				this.Surface.layerMask = layerMask;
				this.BuildingOperation = this.Surface.UpdateNavMesh(sources, false);
				yield return this.BuildingOperation;
				this.IsBuilding = false;
				yield break;
			}

			// Token: 0x06000E27 RID: 3623 RVA: 0x00059378 File Offset: 0x00057578
			public void Kill()
			{
				if (this.Surface != null)
				{
					this.Surface.RemoveData();
				}
				if (this.Surface != null)
				{
					Object.Destroy(this.Surface);
				}
			}

			// Token: 0x04000816 RID: 2070
			public int AgentTypeIndex;

			// Token: 0x04000817 RID: 2071
			public NavMeshSurface Surface;

			// Token: 0x04000818 RID: 2072
			public List<NavMeshGridCell.NavMeshLinkInfo> NavMeshLinkInfos;

			// Token: 0x04000819 RID: 2073
			public AsyncOperation BuildingOperation;

			// Token: 0x0400081A RID: 2074
			public NavMeshBuildSettings BuildSettings;

			// Token: 0x0400081C RID: 2076
			public bool IsAwaitingBake;

			// Token: 0x0400081D RID: 2077
			internal readonly List<NavMeshBuildSource> Sources = new List<NavMeshBuildSource>();

			// Token: 0x0400081E RID: 2078
			internal readonly List<NavMeshBuildMarkup> markups = new List<NavMeshBuildMarkup>();

			// Token: 0x0400081F RID: 2079
			internal NavMeshBuildSource terrainBuildSource;
		}
	}
}
