﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200014A RID: 330
	public sealed class ReloadWeaponNeed : BaseScorer
	{
		// Token: 0x06000CE0 RID: 3296 RVA: 0x00052420 File Offset: 0x00050620
		public override float GetScore(BaseContext c)
		{
			global::BasePlayer basePlayer = c.AIAgent as global::BasePlayer;
			if (basePlayer != null)
			{
				global::HeldEntity heldEntity = basePlayer.GetHeldEntity();
				global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
				if (attackEntity != null)
				{
					global::BaseProjectile baseProjectile = attackEntity as global::BaseProjectile;
					if (baseProjectile)
					{
						float num = (float)baseProjectile.primaryMagazine.contents / (float)baseProjectile.primaryMagazine.capacity;
						return (!this.UseResponseCurve) ? num : this.ResponseCurve.Evaluate(num);
					}
				}
			}
			return 0f;
		}

		// Token: 0x04000704 RID: 1796
		[ApexSerialization]
		private AnimationCurve ResponseCurve = AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);

		// Token: 0x04000705 RID: 1797
		[ApexSerialization]
		private bool UseResponseCurve = true;
	}
}
