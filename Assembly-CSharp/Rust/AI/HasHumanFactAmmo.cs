﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000167 RID: 359
	public class HasHumanFactAmmo : BaseScorer
	{
		// Token: 0x06000D1E RID: 3358 RVA: 0x000532D0 File Offset: 0x000514D0
		public override float GetScore(BaseContext c)
		{
			if (this.requireRanged && c.GetFact(global::NPCPlayerApex.Facts.CurrentWeaponType) == 1)
			{
				if (this.Equality <= HasHumanFactAmmo.EqualityEnum.Equal)
				{
					return 0f;
				}
				return 1f;
			}
			else
			{
				byte fact = c.GetFact(global::NPCPlayerApex.Facts.CurrentAmmoState);
				switch (this.Equality)
				{
				case HasHumanFactAmmo.EqualityEnum.Greater:
					return (fact >= (byte)this.value) ? 0f : 1f;
				case HasHumanFactAmmo.EqualityEnum.Gequal:
					return (fact > (byte)this.value) ? 0f : 1f;
				default:
					return (fact != (byte)this.value) ? 0f : 1f;
				case HasHumanFactAmmo.EqualityEnum.Lequal:
					return (fact < (byte)this.value) ? 0f : 1f;
				case HasHumanFactAmmo.EqualityEnum.Lesser:
					return (fact <= (byte)this.value) ? 0f : 1f;
				}
			}
		}

		// Token: 0x0400072B RID: 1835
		[ApexSerialization(defaultValue = global::NPCPlayerApex.AmmoStateEnum.Full)]
		public global::NPCPlayerApex.AmmoStateEnum value;

		// Token: 0x0400072C RID: 1836
		[ApexSerialization]
		public bool requireRanged;

		// Token: 0x0400072D RID: 1837
		[ApexSerialization(defaultValue = HasHumanFactAmmo.EqualityEnum.Equal)]
		public HasHumanFactAmmo.EqualityEnum Equality;

		// Token: 0x02000168 RID: 360
		public enum EqualityEnum
		{
			// Token: 0x0400072F RID: 1839
			Greater,
			// Token: 0x04000730 RID: 1840
			Gequal,
			// Token: 0x04000731 RID: 1841
			Equal,
			// Token: 0x04000732 RID: 1842
			Lequal,
			// Token: 0x04000733 RID: 1843
			Lesser
		}
	}
}
