﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000193 RID: 403
	public struct AiStatement_EnemySeen : IAiStatement
	{
		// Token: 0x04000789 RID: 1929
		public global::BasePlayer Enemy;

		// Token: 0x0400078A RID: 1930
		public float Score;
	}
}
