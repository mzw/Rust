﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000122 RID: 290
	public sealed class SelfDefence : BaseScorer
	{
		// Token: 0x06000C94 RID: 3220 RVA: 0x00051454 File Offset: 0x0004F654
		public override float GetScore(BaseContext c)
		{
			if (float.IsNegativeInfinity(c.Entity.SecondsSinceAttacked) || float.IsNaN(c.Entity.SecondsSinceAttacked))
			{
				return 0f;
			}
			float num = (this.WithinSeconds - c.Entity.SecondsSinceAttacked) / this.WithinSeconds;
			return num * c.AIAgent.GetStats.Defensiveness;
		}

		// Token: 0x040006D7 RID: 1751
		[ApexSerialization]
		public float WithinSeconds = 10f;
	}
}
