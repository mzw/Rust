﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000147 RID: 327
	public sealed class HasThreats : BaseScorer
	{
		// Token: 0x06000CDA RID: 3290 RVA: 0x0005227C File Offset: 0x0005047C
		public override float GetScore(BaseContext c)
		{
			float num = 0f;
			for (int i = 0; i < c.Memory.All.Count; i++)
			{
				Memory.SeenInfo seenInfo = c.Memory.All[i];
				if (!(seenInfo.Entity == null))
				{
					num += c.AIAgent.FearLevel(seenInfo.Entity);
				}
			}
			return num;
		}
	}
}
