﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200014C RID: 332
	public sealed class ReloadWeapon : BaseAction
	{
		// Token: 0x06000CE4 RID: 3300 RVA: 0x00052510 File Offset: 0x00050710
		public override void DoExecute(BaseContext c)
		{
			global::BasePlayer basePlayer = c.AIAgent as global::BasePlayer;
			if (basePlayer != null)
			{
				global::HeldEntity heldEntity = basePlayer.GetHeldEntity();
				global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
				if (attackEntity != null)
				{
					global::BaseProjectile baseProjectile = attackEntity as global::BaseProjectile;
					if (baseProjectile)
					{
						baseProjectile.ServerReload();
					}
				}
			}
		}
	}
}
