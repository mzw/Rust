﻿using System;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000180 RID: 384
	public abstract class BaseScorer : ContextualScorerBase
	{
		// Token: 0x06000D62 RID: 3426 RVA: 0x000549A4 File Offset: 0x00052BA4
		public BaseScorer()
		{
			this.DebugName = base.GetType().Name;
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x000549C0 File Offset: 0x00052BC0
		protected float ProcessScore(float s)
		{
			s = Mathf.Clamp01(s);
			if (this.InvertScore)
			{
				s = 1f - s;
			}
			return s * this.score;
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x000549E8 File Offset: 0x00052BE8
		public override float Score(IAIContext context)
		{
			return this.ProcessScore(this.GetScore((BaseContext)context));
		}

		// Token: 0x06000D65 RID: 3429
		public abstract float GetScore(BaseContext context);

		// Token: 0x0400075F RID: 1887
		[ApexSerialization(defaultValue = false)]
		public bool InvertScore;

		// Token: 0x04000760 RID: 1888
		private string DebugName;
	}
}
