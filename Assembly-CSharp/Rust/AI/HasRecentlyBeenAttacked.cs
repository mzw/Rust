﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200012D RID: 301
	public sealed class HasRecentlyBeenAttacked : BaseScorer
	{
		// Token: 0x06000CAA RID: 3242 RVA: 0x000516C8 File Offset: 0x0004F8C8
		public override float GetScore(BaseContext c)
		{
			if (float.IsNegativeInfinity(c.Entity.SecondsSinceAttacked) || float.IsNaN(c.Entity.SecondsSinceAttacked))
			{
				return 0f;
			}
			if (this.BooleanResult)
			{
				return (c.Entity.SecondsSinceAttacked > this.WithinSeconds) ? 0f : 1f;
			}
			return (this.WithinSeconds - c.Entity.SecondsSinceAttacked) / this.WithinSeconds;
		}

		// Token: 0x040006DA RID: 1754
		[ApexSerialization]
		public float WithinSeconds = 10f;

		// Token: 0x040006DB RID: 1755
		[ApexSerialization]
		public bool BooleanResult;
	}
}
