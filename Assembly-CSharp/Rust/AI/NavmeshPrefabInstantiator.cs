﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001B5 RID: 437
	public class NavmeshPrefabInstantiator : MonoBehaviour
	{
		// Token: 0x06000E71 RID: 3697 RVA: 0x0005B284 File Offset: 0x00059484
		private void Start()
		{
			if (this.NavmeshPrefab != null)
			{
				GameObject gameObject = this.NavmeshPrefab.Instantiate(base.transform);
				gameObject.SetActive(true);
				Object.Destroy(this);
			}
		}

		// Token: 0x0400088B RID: 2187
		public global::GameObjectRef NavmeshPrefab;
	}
}
