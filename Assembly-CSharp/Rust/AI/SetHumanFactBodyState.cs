﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200016A RID: 362
	public class SetHumanFactBodyState : BaseAction
	{
		// Token: 0x06000D22 RID: 3362 RVA: 0x0005340C File Offset: 0x0005160C
		public override void DoExecute(BaseContext c)
		{
			c.SetFact(global::NPCPlayerApex.Facts.BodyState, (byte)this.value, true, false);
		}

		// Token: 0x04000735 RID: 1845
		[ApexSerialization(defaultValue = global::NPCPlayerApex.BodyState.StandingTall)]
		public global::NPCPlayerApex.BodyState value;
	}
}
