﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000194 RID: 404
	public struct AiStatement_EnemyEngaged : IAiStatement
	{
		// Token: 0x0400078B RID: 1931
		public global::BasePlayer Enemy;

		// Token: 0x0400078C RID: 1932
		public float Score;
	}
}
