﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000133 RID: 307
	public class SetBusyFor : BaseAction
	{
		// Token: 0x06000CB6 RID: 3254 RVA: 0x000519EC File Offset: 0x0004FBEC
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.SetBusyFor(this.BusyTime);
		}

		// Token: 0x040006E3 RID: 1763
		[ApexSerialization]
		public float BusyTime;
	}
}
