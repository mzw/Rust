﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000171 RID: 369
	public class IsHumanRoamReady : BaseScorer
	{
		// Token: 0x06000D39 RID: 3385 RVA: 0x00053FF8 File Offset: 0x000521F8
		public override float GetScore(BaseContext c)
		{
			return (float)((!IsHumanRoamReady.Evaluate(c as global::NPCHumanContext)) ? 0 : 1);
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x00054014 File Offset: 0x00052214
		public static bool Evaluate(global::NPCHumanContext c)
		{
			return c.GetFact(global::NPCPlayerApex.Facts.IsRoamReady) > 0;
		}
	}
}
