﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000142 RID: 322
	public class LineOfSightToTarget : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CD2 RID: 3282 RVA: 0x000520C8 File Offset: 0x000502C8
		public override float GetScore(BaseContext c, Vector3 position)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			if (this.Cover == LineOfSightToTarget.CoverType.Full)
			{
				return (!c.AIAgent.AttackTarget.IsVisible(position + new Vector3(0f, 1.8f, 0f))) ? 0f : 1f;
			}
			return (!c.AIAgent.AttackTarget.IsVisible(position + new Vector3(0f, 0.9f, 0f))) ? 0f : 1f;
		}

		// Token: 0x040006FE RID: 1790
		[ApexSerialization]
		private LineOfSightToTarget.CoverType Cover;

		// Token: 0x02000143 RID: 323
		public enum CoverType
		{
			// Token: 0x04000700 RID: 1792
			Full,
			// Token: 0x04000701 RID: 1793
			Partial
		}
	}
}
