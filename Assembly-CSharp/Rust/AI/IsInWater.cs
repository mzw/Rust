﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000187 RID: 391
	public class IsInWater : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000D71 RID: 3441 RVA: 0x00054C68 File Offset: 0x00052E68
		public override float GetScore(BaseContext c, Vector3 position)
		{
			float waterDepth = global::WaterLevel.GetWaterDepth(position);
			return waterDepth / this.MaxDepth;
		}

		// Token: 0x0400076A RID: 1898
		[ApexSerialization(defaultValue = 3f)]
		public float MaxDepth = 3f;
	}
}
