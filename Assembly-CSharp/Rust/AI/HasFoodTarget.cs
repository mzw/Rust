﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200010F RID: 271
	public class HasFoodTarget : BaseScorer
	{
		// Token: 0x06000C69 RID: 3177 RVA: 0x00050A0C File Offset: 0x0004EC0C
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.FoodTarget == null)
			{
				return 0f;
			}
			return 1f;
		}
	}
}
