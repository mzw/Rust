﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000148 RID: 328
	public sealed class HasThreatsNearby : BaseScorer
	{
		// Token: 0x06000CDC RID: 3292 RVA: 0x00052304 File Offset: 0x00050504
		public override float GetScore(BaseContext c)
		{
			float num = 0f;
			for (int i = 0; i < c.Memory.All.Count; i++)
			{
				Memory.SeenInfo seenInfo = c.Memory.All[i];
				if (!(seenInfo.Entity == null))
				{
					if (c.Entity.Distance(seenInfo.Entity) <= this.range)
					{
						num += c.AIAgent.FearLevel(seenInfo.Entity);
					}
				}
			}
			return num;
		}

		// Token: 0x04000703 RID: 1795
		[ApexSerialization]
		public float range = 20f;
	}
}
