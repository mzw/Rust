﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000154 RID: 340
	public class HumanAttackOperator : BaseAction
	{
		// Token: 0x06000CF1 RID: 3313 RVA: 0x000527C4 File Offset: 0x000509C4
		public override void DoExecute(BaseContext c)
		{
			AttackOperator.AttackTargetType target = this.Target;
			if (target == AttackOperator.AttackTargetType.Enemy)
			{
				HumanAttackOperator.AttackEnemy(c as global::NPCHumanContext, this.Type);
			}
		}

		// Token: 0x06000CF2 RID: 3314 RVA: 0x000527FC File Offset: 0x000509FC
		public static void AttackEnemy(global::NPCHumanContext c, AttackOperator.AttackType type)
		{
			if (c.GetFact(global::NPCPlayerApex.Facts.IsWeaponAttackReady) == 0)
			{
				return;
			}
			global::BaseCombatEntity baseCombatEntity = null;
			if (c.EnemyNpc != null)
			{
				baseCombatEntity = c.EnemyNpc;
			}
			if (c.EnemyPlayer != null)
			{
				baseCombatEntity = c.EnemyPlayer;
			}
			if (baseCombatEntity == null)
			{
				return;
			}
			c.AIAgent.StartAttack(type, baseCombatEntity);
			c.SetFact(global::NPCPlayerApex.Facts.IsWeaponAttackReady, 0, true, true);
			if (Random.value < 0.1f && c.Human.OnAggro != null)
			{
				c.Human.OnAggro();
			}
		}

		// Token: 0x04000711 RID: 1809
		[ApexSerialization]
		public AttackOperator.AttackType Type;

		// Token: 0x04000712 RID: 1810
		[ApexSerialization]
		public AttackOperator.AttackTargetType Target;
	}
}
