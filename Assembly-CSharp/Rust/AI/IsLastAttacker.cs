﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x020000F8 RID: 248
	public class IsLastAttacker : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C37 RID: 3127 RVA: 0x0005013C File Offset: 0x0004E33C
		public override float GetScore(BaseContext context, global::BaseEntity option)
		{
			global::NPCHumanContext npchumanContext = context as global::NPCHumanContext;
			if (npchumanContext != null)
			{
				return (!(npchumanContext.LastAttacker == option)) ? this.MinScore : 1f;
			}
			return 0f;
		}

		// Token: 0x040006BB RID: 1723
		[ApexSerialization]
		public float MinScore = 0.1f;
	}
}
