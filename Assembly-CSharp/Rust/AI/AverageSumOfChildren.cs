﻿using System;
using System.Collections.Generic;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200017D RID: 381
	public class AverageSumOfChildren : CompositeQualifier
	{
		// Token: 0x06000D5D RID: 3421 RVA: 0x00054820 File Offset: 0x00052A20
		public override float Score(IAIContext context, IList<IContextualScorer> scorers)
		{
			if (scorers.Count == 0)
			{
				return 0f;
			}
			float num = 0f;
			for (int i = 0; i < scorers.Count; i++)
			{
				float num2 = scorers[i].Score(context);
				if (this.FailIfAnyScoreZero && (num2 < 0f || Mathf.Approximately(num2, 0f)))
				{
					return 0f;
				}
				num += num2;
			}
			num /= (float)scorers.Count;
			if (this.normalize)
			{
				num /= this.MaxAverageScore;
				return num * this.postNormalizeMultiplier;
			}
			return num;
		}

		// Token: 0x0400075A RID: 1882
		[ApexSerialization]
		private bool normalize = true;

		// Token: 0x0400075B RID: 1883
		[ApexSerialization]
		private float postNormalizeMultiplier = 1f;

		// Token: 0x0400075C RID: 1884
		[ApexSerialization]
		private float MaxAverageScore = 100f;

		// Token: 0x0400075D RID: 1885
		[ApexSerialization]
		private bool FailIfAnyScoreZero = true;
	}
}
