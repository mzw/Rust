﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000123 RID: 291
	public class CanAffordAttack : BaseScorer
	{
		// Token: 0x06000C96 RID: 3222 RVA: 0x000514C8 File Offset: 0x0004F6C8
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.GetStamina < c.AIAgent.GetAttackCost) ? 0f : 1f;
		}
	}
}
