﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200016C RID: 364
	public class ShouldReturnToSpawnPoint : BaseScorer
	{
		// Token: 0x06000D26 RID: 3366 RVA: 0x00053460 File Offset: 0x00051660
		public override float GetScore(BaseContext ctx)
		{
			global::NPCHumanContext npchumanContext = ctx as global::NPCHumanContext;
			if (npchumanContext != null)
			{
				global::NPCPlayerApex.EnemyRangeEnum fact = (global::NPCPlayerApex.EnemyRangeEnum)npchumanContext.GetFact(global::NPCPlayerApex.Facts.RangeToSpawnLocation);
				if (fact >= npchumanContext.Human.GetStats.MaxRangeToSpawnLoc)
				{
					if (float.IsNaN(npchumanContext.Human.SecondsSinceLastInRangeOfSpawnPosition) || float.IsNegativeInfinity(npchumanContext.Human.SecondsSinceLastInRangeOfSpawnPosition) || float.IsInfinity(npchumanContext.Human.SecondsSinceLastInRangeOfSpawnPosition))
					{
						return 0f;
					}
					bool flag = npchumanContext.Human.SecondsSinceLastInRangeOfSpawnPosition >= npchumanContext.Human.GetStats.OutOfRangeOfSpawnPointTimeout;
					return (float)((!flag) ? 0 : 1);
				}
			}
			return 0f;
		}
	}
}
