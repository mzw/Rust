﻿using System;
using System.Runtime.CompilerServices;
using Apex.AI;
using Apex.Serialization;
using ConVar;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000117 RID: 279
	[FriendlyName("Scan for Entities", "Update Context.Entities")]
	public sealed class ScanForEntities : BaseAction
	{
		// Token: 0x06000C7A RID: 3194 RVA: 0x00050E0C File Offset: 0x0004F00C
		public override void DoExecute(BaseContext c)
		{
			if (global::BaseEntity.Query.Server == null)
			{
				return;
			}
			global::BaseEntity.Query.EntityTree server = global::BaseEntity.Query.Server;
			Vector3 position = c.Position;
			float visionRange = c.AIAgent.GetStats.VisionRange;
			global::BaseEntity[] results = this.Results;
			if (ScanForEntities.<>f__mg$cache0 == null)
			{
				ScanForEntities.<>f__mg$cache0 = new Func<global::BaseEntity, bool>(ScanForEntities.AiCaresAbout);
			}
			int inSphere = server.GetInSphere(position, visionRange, results, ScanForEntities.<>f__mg$cache0);
			if (inSphere == 0)
			{
				return;
			}
			for (int i = 0; i < inSphere; i++)
			{
				global::BaseEntity baseEntity = this.Results[i];
				if (!(baseEntity == null))
				{
					if (!(baseEntity == c.Entity))
					{
						if (baseEntity.isServer)
						{
							if (ScanForEntities.WithinVisionCone(c.AIAgent, baseEntity))
							{
								global::BasePlayer basePlayer = baseEntity as global::BasePlayer;
								if (basePlayer != null && !(baseEntity is global::NPCPlayer))
								{
									if (ConVar.AI.ignoreplayers)
									{
										goto IL_158;
									}
									Vector3 attackPosition = c.AIAgent.AttackPosition;
									if (!basePlayer.IsVisible(attackPosition, basePlayer.CenterPoint()) && !basePlayer.IsVisible(attackPosition, basePlayer.eyes.position) && !basePlayer.IsVisible(attackPosition, basePlayer.transform.position))
									{
										goto IL_158;
									}
								}
								c.Memory.Update(baseEntity, 0f);
							}
						}
					}
				}
				IL_158:;
			}
			c.Memory.Forget((float)this.forgetTime);
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x00050F90 File Offset: 0x0004F190
		private static bool WithinVisionCone(global::IAIAgent agent, global::BaseEntity other)
		{
			if (agent.GetStats.VisionCone == -1f)
			{
				return true;
			}
			global::BaseCombatEntity entity = agent.Entity;
			Vector3 vector = entity.transform.forward;
			global::BasePlayer basePlayer = entity as global::BasePlayer;
			if (basePlayer != null)
			{
				vector = basePlayer.eyes.BodyForward();
			}
			Vector3 normalized = (other.transform.position - entity.transform.position).normalized;
			float num = Vector3.Dot(entity.transform.forward, normalized);
			return num >= agent.GetStats.VisionCone;
		}

		// Token: 0x06000C7C RID: 3196 RVA: 0x0005103C File Offset: 0x0004F23C
		private static bool AiCaresAbout(global::BaseEntity ent)
		{
			return ent is global::BasePlayer || ent is global::BaseNpc || ent is global::WorldItem || ent is global::BaseCorpse;
		}

		// Token: 0x040006C8 RID: 1736
		public global::BaseEntity[] Results = new global::BaseEntity[64];

		// Token: 0x040006C9 RID: 1737
		[ApexSerialization]
		public int forgetTime = 10;

		// Token: 0x040006CA RID: 1738
		[CompilerGenerated]
		private static Func<global::BaseEntity, bool> <>f__mg$cache0;
	}
}
