﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200015B RID: 347
	public class HasFactBoolean : BaseScorer
	{
		// Token: 0x06000D06 RID: 3334 RVA: 0x00052F1C File Offset: 0x0005111C
		public override float GetScore(BaseContext c)
		{
			byte b = (!this.value) ? 0 : 1;
			byte b2 = c.GetFact(this.fact);
			return (b2 != b) ? 0f : 1f;
		}

		// Token: 0x0400071A RID: 1818
		[ApexSerialization]
		public global::BaseNpc.Facts fact;

		// Token: 0x0400071B RID: 1819
		[ApexSerialization(defaultValue = false)]
		public bool value;
	}
}
