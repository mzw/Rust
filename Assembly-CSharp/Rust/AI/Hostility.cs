﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000120 RID: 288
	public sealed class Hostility : BaseScorer
	{
		// Token: 0x06000C90 RID: 3216 RVA: 0x000513F8 File Offset: 0x0004F5F8
		public override float GetScore(BaseContext c)
		{
			return c.AIAgent.GetStats.Hostility;
		}
	}
}
