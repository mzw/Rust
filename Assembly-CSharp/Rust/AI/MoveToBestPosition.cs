﻿using System;
using Apex.AI;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000114 RID: 276
	[FriendlyName("Move To Best Position", "Sets a move target based on the scorers and moves towards it")]
	public class MoveToBestPosition : BaseActionWithOptions<Vector3>
	{
		// Token: 0x06000C73 RID: 3187 RVA: 0x00050AD8 File Offset: 0x0004ECD8
		public override void DoExecute(BaseContext c)
		{
			Vector3 best = base.GetBest(c, c.sampledPositions);
			if (best.sqrMagnitude == 0f)
			{
				return;
			}
			global::NPCHumanContext npchumanContext = c as global::NPCHumanContext;
			if (npchumanContext != null && npchumanContext.CurrentCoverVolume != null)
			{
				for (int i = 0; i < npchumanContext.sampledCoverPoints.Count; i++)
				{
					CoverPoint coverPoint = npchumanContext.sampledCoverPoints[i];
					CoverPoint.CoverType coverType = npchumanContext.sampledCoverPointTypes[i];
					Vector3 position = coverPoint.Position;
					float num = Vector3Ex.Distance2D(position, best);
					if (num < 1f)
					{
						npchumanContext.CoverSet.Update(coverPoint, coverPoint, coverPoint);
						break;
					}
				}
			}
			c.AIAgent.UpdateDestination(best);
			c.lastSampledPosition = best;
		}
	}
}
