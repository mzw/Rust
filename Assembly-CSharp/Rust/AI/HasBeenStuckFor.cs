﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000124 RID: 292
	public class HasBeenStuckFor : BaseScorer
	{
		// Token: 0x06000C98 RID: 3224 RVA: 0x00051508 File Offset: 0x0004F708
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.GetStuckDuration < this.StuckSeconds) ? 0f : 1f;
		}

		// Token: 0x040006D8 RID: 1752
		[ApexSerialization]
		public float StuckSeconds = 5f;
	}
}
