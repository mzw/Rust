﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200011D RID: 285
	public class EnergyLevel : BaseScorer
	{
		// Token: 0x06000C8A RID: 3210 RVA: 0x00051388 File Offset: 0x0004F588
		public override float GetScore(BaseContext c)
		{
			return c.AIAgent.GetEnergy;
		}
	}
}
