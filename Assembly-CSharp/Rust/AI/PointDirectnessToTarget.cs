﻿using System;
using Apex.AI;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000183 RID: 387
	public class PointDirectnessToTarget : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000D6B RID: 3435 RVA: 0x00054A84 File Offset: 0x00052C84
		public override float GetScore(BaseContext c, Vector3 point)
		{
			Vector3 vector;
			if (this.UsePerfectInfo)
			{
				vector = c.AIAgent.AttackTarget.ServerPosition;
			}
			else
			{
				vector = c.AIAgent.AttackTargetMemory.Position;
			}
			float num = Vector3.Distance(c.Position, vector);
			float num2 = Vector3.Distance(point, vector);
			float num3 = Vector3.Distance(c.Position, point);
			return (num - num2) / num3;
		}

		// Token: 0x04000762 RID: 1890
		[ApexSerialization]
		[FriendlyName("Use Perfect Position Information", "Should we apply perfect knowledge about the attack target's whereabouts, or the last memorized position.")]
		private bool UsePerfectInfo;
	}
}
