﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200011C RID: 284
	public class TimeAtDestination : BaseScorer
	{
		// Token: 0x06000C88 RID: 3208 RVA: 0x00051370 File Offset: 0x0004F570
		public override float GetScore(BaseContext c)
		{
			return c.AIAgent.TimeAtDestination;
		}
	}
}
