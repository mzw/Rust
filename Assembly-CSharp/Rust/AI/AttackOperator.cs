﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000151 RID: 337
	public class AttackOperator : BaseAction
	{
		// Token: 0x06000CEE RID: 3310 RVA: 0x00052728 File Offset: 0x00050928
		public override void DoExecute(BaseContext c)
		{
			AttackOperator.AttackTargetType target = this.Target;
			if (target == AttackOperator.AttackTargetType.Enemy)
			{
				AttackOperator.AttackEnemy(c, this.Type);
			}
		}

		// Token: 0x06000CEF RID: 3311 RVA: 0x00052758 File Offset: 0x00050958
		public static void AttackEnemy(BaseContext c, AttackOperator.AttackType type)
		{
			if (c.GetFact(global::BaseNpc.Facts.IsAttackReady) == 0)
			{
				return;
			}
			global::BaseCombatEntity target = null;
			if (c.EnemyNpc != null)
			{
				target = c.EnemyNpc;
			}
			if (c.EnemyPlayer != null)
			{
				target = c.EnemyPlayer;
			}
			c.AIAgent.StartAttack(type, target);
			c.SetFact(global::BaseNpc.Facts.IsAttackReady, 0);
		}

		// Token: 0x04000709 RID: 1801
		[ApexSerialization]
		public AttackOperator.AttackType Type;

		// Token: 0x0400070A RID: 1802
		[ApexSerialization]
		public AttackOperator.AttackTargetType Target;

		// Token: 0x02000152 RID: 338
		public enum AttackType
		{
			// Token: 0x0400070C RID: 1804
			CloseRange,
			// Token: 0x0400070D RID: 1805
			MediumRange,
			// Token: 0x0400070E RID: 1806
			LongRange
		}

		// Token: 0x02000153 RID: 339
		public enum AttackTargetType
		{
			// Token: 0x04000710 RID: 1808
			Enemy
		}
	}
}
