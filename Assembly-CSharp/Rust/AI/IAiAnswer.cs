﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200018E RID: 398
	public interface IAiAnswer
	{
		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000D8D RID: 3469
		// (set) Token: 0x06000D8E RID: 3470
		global::NPCPlayerApex Source { get; set; }
	}
}
