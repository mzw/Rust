﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000140 RID: 320
	public class DistanceFromHome : BaseScorer
	{
		// Token: 0x06000CCE RID: 3278 RVA: 0x00051FC4 File Offset: 0x000501C4
		public override float GetScore(BaseContext c)
		{
			float num = Mathf.Min(Vector3.Distance(c.Position, c.AIAgent.SpawnPosition), this.Range) / this.Range;
			return (!this.UseResponseCurve) ? num : this.ResponseCurve.Evaluate(num);
		}

		// Token: 0x040006FA RID: 1786
		[ApexSerialization]
		public float Range = 50f;

		// Token: 0x040006FB RID: 1787
		[ApexSerialization]
		public AnimationCurve ResponseCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

		// Token: 0x040006FC RID: 1788
		[ApexSerialization]
		public bool UseResponseCurve = true;
	}
}
