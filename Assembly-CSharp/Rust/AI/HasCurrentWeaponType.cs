﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000157 RID: 343
	public class HasCurrentWeaponType : BaseScorer
	{
		// Token: 0x06000CFC RID: 3324 RVA: 0x00052D10 File Offset: 0x00050F10
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::NPCPlayerApex.Facts.CurrentWeaponType);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x04000714 RID: 1812
		[ApexSerialization(defaultValue = global::NPCPlayerApex.WeaponTypeEnum.None)]
		public global::NPCPlayerApex.WeaponTypeEnum value;
	}
}
