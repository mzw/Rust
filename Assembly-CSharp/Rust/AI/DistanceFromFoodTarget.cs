﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200010E RID: 270
	public sealed class DistanceFromFoodTarget : BaseScorer
	{
		// Token: 0x06000C67 RID: 3175 RVA: 0x000509B4 File Offset: 0x0004EBB4
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.FoodTarget == null)
			{
				return 0f;
			}
			return Vector3.Distance(c.Position, c.AIAgent.FoodTarget.transform.localPosition) / this.MaxDistance;
		}

		// Token: 0x040006C5 RID: 1733
		[ApexSerialization(defaultValue = 10f)]
		public float MaxDistance = 10f;
	}
}
