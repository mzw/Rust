﻿using System;
using Apex.Serialization;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x02000172 RID: 370
	public class GetHumanPathToTargetStatus : BaseScorer
	{
		// Token: 0x06000D3C RID: 3388 RVA: 0x00054028 File Offset: 0x00052228
		public override float GetScore(BaseContext c)
		{
			return (float)((!GetHumanPathToTargetStatus.Evaluate(c as global::NPCHumanContext, this.Status)) ? 0 : 1);
		}

		// Token: 0x06000D3D RID: 3389 RVA: 0x00054048 File Offset: 0x00052248
		public static bool Evaluate(global::NPCHumanContext c, NavMeshPathStatus s)
		{
			byte fact = c.GetFact(global::NPCPlayerApex.Facts.PathToTargetStatus);
			NavMeshPathStatus navMeshPathStatus = c.Human.ToPathStatus(fact);
			return navMeshPathStatus == s;
		}

		// Token: 0x0400074A RID: 1866
		[ApexSerialization]
		public NavMeshPathStatus Status;
	}
}
