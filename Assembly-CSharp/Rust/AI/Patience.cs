﻿using System;

namespace Rust.Ai
{
	// Token: 0x020000FC RID: 252
	public sealed class Patience : BaseScorer
	{
		// Token: 0x06000C3F RID: 3135 RVA: 0x00050250 File Offset: 0x0004E450
		public override float GetScore(BaseContext c)
		{
			return c.AIAgent.GetStats.Tolerance;
		}
	}
}
