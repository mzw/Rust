﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000102 RID: 258
	public sealed class DistanceFromTargetEntity : BaseScorer
	{
		// Token: 0x06000C4C RID: 3148 RVA: 0x00050444 File Offset: 0x0004E644
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			return Vector3.Distance(c.AIAgent.AttackPosition, c.AIAgent.AttackTargetMemory.Position) / this.MaxDistance;
		}

		// Token: 0x040006BF RID: 1727
		[ApexSerialization(defaultValue = 10f)]
		public float MaxDistance = 10f;
	}
}
