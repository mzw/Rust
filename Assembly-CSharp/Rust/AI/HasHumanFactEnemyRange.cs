﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000163 RID: 355
	public class HasHumanFactEnemyRange : BaseScorer
	{
		// Token: 0x06000D16 RID: 3350 RVA: 0x00053100 File Offset: 0x00051300
		public override float GetScore(BaseContext c)
		{
			global::NPCPlayerApex.EnemyRangeEnum fact = (global::NPCPlayerApex.EnemyRangeEnum)c.GetFact(global::NPCPlayerApex.Facts.EnemyRange);
			if (fact == global::NPCPlayerApex.EnemyRangeEnum.AggroRange)
			{
				global::NPCHumanContext npchumanContext = c as global::NPCHumanContext;
				if (npchumanContext != null)
				{
					if (npchumanContext.Human.Stats.AggressionRange < npchumanContext.Human.Stats.CloseRange)
					{
						return (this.value != global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange && this.value != global::NPCPlayerApex.EnemyRangeEnum.AggroRange) ? 0f : 1f;
					}
					if (npchumanContext.Human.Stats.AggressionRange < npchumanContext.Human.Stats.MediumRange)
					{
						return (this.value != global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange && this.value != global::NPCPlayerApex.EnemyRangeEnum.AggroRange) ? 0f : 1f;
					}
					if (npchumanContext.Human.Stats.AggressionRange < npchumanContext.Human.Stats.LongRange)
					{
						return (this.value != global::NPCPlayerApex.EnemyRangeEnum.LongAttackRange && this.value != global::NPCPlayerApex.EnemyRangeEnum.AggroRange) ? 0f : 1f;
					}
				}
			}
			return (fact != this.value) ? 0f : 1f;
		}

		// Token: 0x04000726 RID: 1830
		[ApexSerialization(defaultValue = global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange)]
		public global::NPCPlayerApex.EnemyRangeEnum value;
	}
}
