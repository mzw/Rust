﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000113 RID: 275
	public sealed class Eat : BaseAction
	{
		// Token: 0x06000C71 RID: 3185 RVA: 0x00050AC0 File Offset: 0x0004ECC0
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.Eat();
		}
	}
}
