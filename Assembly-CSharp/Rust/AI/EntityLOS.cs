﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000182 RID: 386
	public sealed class EntityLOS : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000D69 RID: 3433 RVA: 0x00054A54 File Offset: 0x00052C54
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			return (!c.Entity.IsVisible(target.CenterPoint())) ? 0f : 1f;
		}
	}
}
