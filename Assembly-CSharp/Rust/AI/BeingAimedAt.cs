﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000136 RID: 310
	public sealed class BeingAimedAt : BaseScorer
	{
		// Token: 0x06000CBC RID: 3260 RVA: 0x00051B1C File Offset: 0x0004FD1C
		public override float GetScore(BaseContext c)
		{
			float num = 0f;
			int num2 = 0;
			foreach (global::BaseEntity baseEntity in c.Memory.Visible)
			{
				global::BasePlayer basePlayer = baseEntity as global::BasePlayer;
				if (basePlayer != null && !(basePlayer is global::IAIAgent))
				{
					Vector3 vector = basePlayer.eyes.BodyForward();
					float num3 = 0f;
					float num4 = Vector3.Dot(c.AIAgent.CurrentAimAngles, vector);
					switch (this.EqualityType)
					{
					case BeingAimedAt.Equality.Equal:
						num3 = ((!Mathf.Approximately(num4, this.arc)) ? 0f : 1f);
						break;
					case BeingAimedAt.Equality.LEqual:
						num3 = ((num4 > this.arc) ? 0f : 1f);
						break;
					case BeingAimedAt.Equality.GEqual:
						num3 = ((num4 < this.arc) ? 0f : 1f);
						break;
					case BeingAimedAt.Equality.NEqual:
						num3 = ((!Mathf.Approximately(num4, this.arc)) ? 1f : 0f);
						break;
					case BeingAimedAt.Equality.Less:
						num3 = ((num4 >= this.arc) ? 0f : 1f);
						break;
					case BeingAimedAt.Equality.Greater:
						num3 = ((num4 <= this.arc) ? 0f : 1f);
						break;
					}
					num += num3;
					num2++;
				}
			}
			if (num2 > 0)
			{
				num /= (float)num2;
			}
			return num;
		}

		// Token: 0x040006E7 RID: 1767
		[ApexSerialization]
		public float arc;

		// Token: 0x040006E8 RID: 1768
		[ApexSerialization]
		public BeingAimedAt.Equality EqualityType;

		// Token: 0x02000137 RID: 311
		public enum Equality
		{
			// Token: 0x040006EA RID: 1770
			Equal,
			// Token: 0x040006EB RID: 1771
			LEqual,
			// Token: 0x040006EC RID: 1772
			GEqual,
			// Token: 0x040006ED RID: 1773
			NEqual,
			// Token: 0x040006EE RID: 1774
			Less,
			// Token: 0x040006EF RID: 1775
			Greater
		}
	}
}
