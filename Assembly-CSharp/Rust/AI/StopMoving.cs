﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000115 RID: 277
	public class StopMoving : BaseAction
	{
		// Token: 0x06000C75 RID: 3189 RVA: 0x00050BA8 File Offset: 0x0004EDA8
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.StopMoving();
		}
	}
}
