﻿using System;

namespace Rust.Ai
{
	// Token: 0x020000FF RID: 255
	public sealed class EntitySizeDifference : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C45 RID: 3141 RVA: 0x000502B8 File Offset: 0x0004E4B8
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			float num = 1f;
			global::BaseNpc baseNpc = c.AIAgent as global::BaseNpc;
			if (baseNpc != null)
			{
				num = baseNpc.Stats.Size;
			}
			global::BasePlayer basePlayer = target as global::BasePlayer;
			if (basePlayer != null)
			{
				return 1f / num;
			}
			global::BaseNpc baseNpc2 = target as global::BaseNpc;
			if (baseNpc2 != null)
			{
				return baseNpc2.Stats.Size / num;
			}
			return 0f;
		}
	}
}
