﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001B7 RID: 439
	public class NavPointSampleComparer : IComparer<NavPointSample>
	{
		// Token: 0x06000E73 RID: 3699 RVA: 0x0005B2C4 File Offset: 0x000594C4
		public int Compare(NavPointSample a, NavPointSample b)
		{
			if (Mathf.Approximately(a.Score, b.Score))
			{
				return 0;
			}
			if (a.Score > b.Score)
			{
				return -1;
			}
			return 1;
		}
	}
}
