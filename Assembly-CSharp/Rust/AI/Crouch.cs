﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000138 RID: 312
	public sealed class Crouch : BaseAction
	{
		// Token: 0x06000CBE RID: 3262 RVA: 0x00051CFC File Offset: 0x0004FEFC
		public override void DoExecute(BaseContext ctx)
		{
			if (this.crouch)
			{
				global::NPCPlayerApex npcplayerApex = ctx.AIAgent as global::NPCPlayerApex;
				if (npcplayerApex != null)
				{
					npcplayerApex.modelState.SetFlag(1, this.crouch);
				}
			}
		}

		// Token: 0x040006F0 RID: 1776
		[ApexSerialization]
		public bool crouch;
	}
}
