﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000149 RID: 329
	public sealed class HasWeaponEquipped : BaseScorer
	{
		// Token: 0x06000CDE RID: 3294 RVA: 0x000523A4 File Offset: 0x000505A4
		public override float GetScore(BaseContext c)
		{
			global::BasePlayer basePlayer = c.AIAgent as global::BasePlayer;
			if (basePlayer != null)
			{
				global::HeldEntity heldEntity = basePlayer.GetHeldEntity();
				global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
				if (attackEntity != null)
				{
					return 1f;
				}
			}
			return 0f;
		}
	}
}
