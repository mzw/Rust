﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000146 RID: 326
	public class ProximityToDangers : ProximityToPeers
	{
		// Token: 0x06000CD8 RID: 3288 RVA: 0x00052248 File Offset: 0x00050448
		protected override float Test(Memory.SeenInfo memory, BaseContext c)
		{
			if (memory.Entity == null)
			{
				return 0f;
			}
			return c.AIAgent.FearLevel(memory.Entity);
		}
	}
}
