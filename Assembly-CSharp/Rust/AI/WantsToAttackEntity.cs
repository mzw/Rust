﻿using System;

namespace Rust.Ai
{
	// Token: 0x020000FE RID: 254
	public sealed class WantsToAttackEntity : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000C43 RID: 3139 RVA: 0x000502A0 File Offset: 0x0004E4A0
		public override float GetScore(BaseContext c, global::BaseEntity target)
		{
			return c.AIAgent.GetWantsToAttack(target);
		}
	}
}
