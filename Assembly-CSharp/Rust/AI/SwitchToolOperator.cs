﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000158 RID: 344
	public class SwitchToolOperator : BaseAction
	{
		// Token: 0x06000CFE RID: 3326 RVA: 0x00052D4C File Offset: 0x00050F4C
		public override void DoExecute(BaseContext c)
		{
			SwitchToolOperator.TrySwitchToolTo(c as global::NPCHumanContext, this.ToolTypeDay, this.ToolTypeNight);
		}

		// Token: 0x06000CFF RID: 3327 RVA: 0x00052D68 File Offset: 0x00050F68
		public static bool TrySwitchToolTo(global::NPCHumanContext c, global::NPCPlayerApex.ToolTypeEnum toolDay, global::NPCPlayerApex.ToolTypeEnum toolNight)
		{
			if (c != null)
			{
				global::Item item = null;
				uint svActiveItemID = c.Human.svActiveItemID;
				if (TOD_Sky.Instance != null)
				{
					if (TOD_Sky.Instance.IsDay)
					{
						item = SwitchToolOperator.FindTool(c, toolDay);
					}
					else
					{
						item = SwitchToolOperator.FindTool(c, toolNight);
					}
				}
				if (item != null)
				{
					c.Human.UpdateActiveItem(item.uid);
					if (svActiveItemID != c.Human.svActiveItemID)
					{
						c.Human.NextToolSwitchTime = Time.realtimeSinceStartup + c.Human.ToolSwitchFrequency;
						c.SetFact(global::NPCPlayerApex.Facts.CurrentWeaponType, 0, true, true);
						c.SetFact(global::NPCPlayerApex.Facts.CurrentToolType, (byte)c.Human.GetCurrentToolTypeEnum(), true, true);
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D00 RID: 3328 RVA: 0x00052E24 File Offset: 0x00051024
		public static global::Item FindTool(global::NPCHumanContext c, global::NPCPlayerApex.ToolTypeEnum tool)
		{
			global::Item[] array = c.Human.inventory.AllItems();
			foreach (global::Item item in array)
			{
				if (item.info.category == global::ItemCategory.Tool)
				{
					global::HeldEntity heldEntity = item.GetHeldEntity() as global::HeldEntity;
					if (heldEntity != null && heldEntity.toolType == tool)
					{
						return item;
					}
				}
			}
			return null;
		}

		// Token: 0x04000715 RID: 1813
		[ApexSerialization]
		private global::NPCPlayerApex.ToolTypeEnum ToolTypeDay;

		// Token: 0x04000716 RID: 1814
		[ApexSerialization]
		private global::NPCPlayerApex.ToolTypeEnum ToolTypeNight;

		// Token: 0x02000159 RID: 345
		public class HasCurrentToolType : BaseScorer
		{
			// Token: 0x06000D02 RID: 3330 RVA: 0x00052EA0 File Offset: 0x000510A0
			public override float GetScore(BaseContext c)
			{
				byte fact = c.GetFact(global::NPCPlayerApex.Facts.CurrentToolType);
				return (fact != (byte)this.value) ? 0f : 1f;
			}

			// Token: 0x04000717 RID: 1815
			[ApexSerialization(defaultValue = global::NPCPlayerApex.ToolTypeEnum.None)]
			public global::NPCPlayerApex.ToolTypeEnum value;
		}
	}
}
