﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000130 RID: 304
	public sealed class HasRecentlyHeardGunshots : BaseScorer
	{
		// Token: 0x06000CB0 RID: 3248 RVA: 0x00051848 File Offset: 0x0004FA48
		public override float GetScore(BaseContext c)
		{
			global::BaseNpc baseNpc = c.AIAgent as global::BaseNpc;
			if (baseNpc == null)
			{
				return 0f;
			}
			if (float.IsInfinity(baseNpc.SecondsSinceLastHeardGunshot) || float.IsNaN(baseNpc.SecondsSinceLastHeardGunshot))
			{
				return 0f;
			}
			return (this.WithinSeconds - baseNpc.SecondsSinceLastHeardGunshot) / this.WithinSeconds;
		}

		// Token: 0x040006DE RID: 1758
		[ApexSerialization]
		public float WithinSeconds = 10f;
	}
}
