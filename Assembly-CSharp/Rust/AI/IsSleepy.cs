﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200012C RID: 300
	public sealed class IsSleepy : BaseScorer
	{
		// Token: 0x06000CA8 RID: 3240 RVA: 0x000516A0 File Offset: 0x0004F8A0
		public override float GetScore(BaseContext c)
		{
			return 1f - c.AIAgent.GetSleep;
		}
	}
}
