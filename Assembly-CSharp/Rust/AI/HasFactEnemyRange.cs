﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200015C RID: 348
	public class HasFactEnemyRange : BaseScorer
	{
		// Token: 0x06000D08 RID: 3336 RVA: 0x00052F68 File Offset: 0x00051168
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::BaseNpc.Facts.EnemyRange);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x0400071C RID: 1820
		[ApexSerialization(defaultValue = global::BaseNpc.EnemyRangeEnum.AttackRange)]
		public global::BaseNpc.EnemyRangeEnum value;
	}
}
