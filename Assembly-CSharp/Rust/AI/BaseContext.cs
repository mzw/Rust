﻿using System;
using System.Collections.Generic;
using Apex.AI;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200018D RID: 397
	public class BaseContext : IAIContext
	{
		// Token: 0x06000D7E RID: 3454 RVA: 0x00054FD4 File Offset: 0x000531D4
		public BaseContext(global::IAIAgent agent)
		{
			this.AIAgent = agent;
			this.Entity = agent.Entity;
			this.sampledPositions = new List<Vector3>();
			this.Memory = new Memory();
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000D7F RID: 3455 RVA: 0x00055048 File Offset: 0x00053248
		// (set) Token: 0x06000D80 RID: 3456 RVA: 0x00055050 File Offset: 0x00053250
		public Vector3 lastSampledPosition { get; set; }

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000D81 RID: 3457 RVA: 0x0005505C File Offset: 0x0005325C
		// (set) Token: 0x06000D82 RID: 3458 RVA: 0x00055064 File Offset: 0x00053264
		public List<Vector3> sampledPositions { get; private set; }

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000D83 RID: 3459 RVA: 0x00055070 File Offset: 0x00053270
		// (set) Token: 0x06000D84 RID: 3460 RVA: 0x00055078 File Offset: 0x00053278
		public global::IAIAgent AIAgent { get; private set; }

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000D85 RID: 3461 RVA: 0x00055084 File Offset: 0x00053284
		// (set) Token: 0x06000D86 RID: 3462 RVA: 0x0005508C File Offset: 0x0005328C
		public global::BaseCombatEntity Entity { get; private set; }

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000D87 RID: 3463 RVA: 0x00055098 File Offset: 0x00053298
		public Vector3 Position
		{
			get
			{
				return this.Entity.ServerPosition;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000D88 RID: 3464 RVA: 0x000550A8 File Offset: 0x000532A8
		public Vector3 EnemyPosition
		{
			get
			{
				return (!(this.EnemyPlayer != null)) ? ((!(this.EnemyNpc != null)) ? Vector3.zero : this.EnemyNpc.ServerPosition) : this.EnemyPlayer.ServerPosition;
			}
		}

		// Token: 0x06000D89 RID: 3465 RVA: 0x000550FC File Offset: 0x000532FC
		public byte GetFact(global::BaseNpc.Facts fact)
		{
			return this.AIAgent.GetFact(fact);
		}

		// Token: 0x06000D8A RID: 3466 RVA: 0x0005510C File Offset: 0x0005330C
		public void SetFact(global::BaseNpc.Facts fact, byte value)
		{
			this.AIAgent.SetFact(fact, value, true, true);
		}

		// Token: 0x06000D8B RID: 3467 RVA: 0x00055120 File Offset: 0x00053320
		public byte GetFact(global::NPCPlayerApex.Facts fact)
		{
			return this.AIAgent.GetFact(fact);
		}

		// Token: 0x06000D8C RID: 3468 RVA: 0x00055130 File Offset: 0x00053330
		public void SetFact(global::NPCPlayerApex.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true)
		{
			this.AIAgent.SetFact(fact, value, triggerCallback, onlyTriggerCallbackOnDiffValue);
		}

		// Token: 0x0400077D RID: 1917
		public Memory Memory;

		// Token: 0x0400077E RID: 1918
		public List<global::BasePlayer> Players = new List<global::BasePlayer>();

		// Token: 0x0400077F RID: 1919
		public List<global::BaseNpc> Npcs = new List<global::BaseNpc>();

		// Token: 0x04000780 RID: 1920
		public List<global::BasePlayer> PlayersBehindUs = new List<global::BasePlayer>();

		// Token: 0x04000781 RID: 1921
		public List<global::BaseNpc> NpcsBehindUs = new List<global::BaseNpc>();

		// Token: 0x04000782 RID: 1922
		public List<global::TimedExplosive> DeployedExplosives = new List<global::TimedExplosive>();

		// Token: 0x04000783 RID: 1923
		public global::BasePlayer EnemyPlayer;

		// Token: 0x04000784 RID: 1924
		public global::BaseNpc EnemyNpc;

		// Token: 0x04000785 RID: 1925
		public float LastTargetScore;

		// Token: 0x04000786 RID: 1926
		public float NextRoamTime;
	}
}
