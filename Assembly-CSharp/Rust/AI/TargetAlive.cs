﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000129 RID: 297
	public sealed class TargetAlive : BaseScorer
	{
		// Token: 0x06000CA2 RID: 3234 RVA: 0x00051610 File Offset: 0x0004F810
		public override float GetScore(BaseContext c)
		{
			global::BaseCombatEntity combatTarget = c.AIAgent.CombatTarget;
			return (!(combatTarget == null) && combatTarget.IsAlive()) ? 1f : 0f;
		}
	}
}
