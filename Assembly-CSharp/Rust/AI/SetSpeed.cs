﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200015F RID: 351
	public class SetSpeed : BaseAction
	{
		// Token: 0x06000D0E RID: 3342 RVA: 0x00053014 File Offset: 0x00051214
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.TargetSpeed = c.AIAgent.ToSpeed(this.value);
			c.SetFact(global::BaseNpc.Facts.Speed, (byte)this.value);
		}

		// Token: 0x0400071F RID: 1823
		[ApexSerialization(defaultValue = global::BaseNpc.SpeedEnum.StandStill)]
		public global::BaseNpc.SpeedEnum value;
	}
}
