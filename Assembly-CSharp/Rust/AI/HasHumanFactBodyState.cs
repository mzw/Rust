﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000169 RID: 361
	public class HasHumanFactBodyState : BaseScorer
	{
		// Token: 0x06000D20 RID: 3360 RVA: 0x000533D0 File Offset: 0x000515D0
		public override float GetScore(BaseContext c)
		{
			byte fact = c.GetFact(global::NPCPlayerApex.Facts.BodyState);
			return (fact != (byte)this.value) ? 0f : 1f;
		}

		// Token: 0x04000734 RID: 1844
		[ApexSerialization(defaultValue = global::NPCPlayerApex.BodyState.StandingTall)]
		public global::NPCPlayerApex.BodyState value;
	}
}
