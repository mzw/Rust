﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200013C RID: 316
	public class DistanceFromSelf : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CC6 RID: 3270 RVA: 0x00051E28 File Offset: 0x00050028
		public override float GetScore(BaseContext c, Vector3 position)
		{
			return Mathf.Clamp(Vector3.Distance(position, c.Position), 0f, this.Range) / this.Range;
		}

		// Token: 0x040006F5 RID: 1781
		[ApexSerialization]
		public float Range = 20f;
	}
}
