﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200011E RID: 286
	public sealed class HealthFraction : BaseScorer
	{
		// Token: 0x06000C8C RID: 3212 RVA: 0x000513A0 File Offset: 0x0004F5A0
		public override float GetScore(BaseContext c)
		{
			return c.Entity.healthFraction;
		}
	}
}
