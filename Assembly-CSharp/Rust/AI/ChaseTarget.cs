﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000109 RID: 265
	public class ChaseTarget : BaseAction
	{
		// Token: 0x06000C5A RID: 3162 RVA: 0x00050790 File Offset: 0x0004E990
		public override void DoExecute(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return;
			}
			c.AIAgent.UpdateDestination(c.AIAgent.AttackTarget.transform);
		}
	}
}
