﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200013A RID: 314
	public sealed class BehaviorDuration : BaseScorer
	{
		// Token: 0x06000CC2 RID: 3266 RVA: 0x00051D9C File Offset: 0x0004FF9C
		public override float GetScore(BaseContext c)
		{
			return (float)((c.AIAgent.CurrentBehaviour != this.Behaviour || c.AIAgent.currentBehaviorDuration < this.duration) ? 0 : 1);
		}

		// Token: 0x040006F1 RID: 1777
		[ApexSerialization]
		public global::BaseNpc.Behaviour Behaviour;

		// Token: 0x040006F2 RID: 1778
		[ApexSerialization]
		public float duration;
	}
}
