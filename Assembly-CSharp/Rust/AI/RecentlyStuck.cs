﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000125 RID: 293
	public class RecentlyStuck : BaseScorer
	{
		// Token: 0x06000C9A RID: 3226 RVA: 0x00051538 File Offset: 0x0004F738
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.GetLastStuckTime == 0f) ? 0f : 1f;
		}
	}
}
