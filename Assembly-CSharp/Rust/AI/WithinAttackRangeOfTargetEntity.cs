﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000105 RID: 261
	public sealed class WithinAttackRangeOfTargetEntity : BaseScorer
	{
		// Token: 0x06000C52 RID: 3154 RVA: 0x00050580 File Offset: 0x0004E780
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			return (Vector3.Distance(c.AIAgent.AttackPosition, c.AIAgent.AttackTarget.ClosestPoint(c.AIAgent.AttackPosition)) > c.AIAgent.GetAttackRange) ? 0f : 1f;
		}
	}
}
