﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000121 RID: 289
	public sealed class Defensiveness : BaseScorer
	{
		// Token: 0x06000C92 RID: 3218 RVA: 0x00051420 File Offset: 0x0004F620
		public override float GetScore(BaseContext c)
		{
			return c.AIAgent.GetStats.Defensiveness;
		}
	}
}
