﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200012E RID: 302
	public sealed class HasRecentlyDealtDamage : BaseScorer
	{
		// Token: 0x06000CAC RID: 3244 RVA: 0x00051764 File Offset: 0x0004F964
		public override float GetScore(BaseContext c)
		{
			if (float.IsInfinity(c.Entity.SecondsSinceDealtDamage) || float.IsNaN(c.Entity.SecondsSinceDealtDamage))
			{
				return 0f;
			}
			return (this.WithinSeconds - c.Entity.SecondsSinceDealtDamage) / this.WithinSeconds;
		}

		// Token: 0x040006DC RID: 1756
		[ApexSerialization]
		public float WithinSeconds = 10f;
	}
}
