﻿using System;

namespace Rust.Ai
{
	// Token: 0x020000FB RID: 251
	public sealed class FeelsThreatenedByAttackTarget : BaseScorer
	{
		// Token: 0x06000C3D RID: 3133 RVA: 0x00050224 File Offset: 0x0004E424
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			return 1f;
		}
	}
}
