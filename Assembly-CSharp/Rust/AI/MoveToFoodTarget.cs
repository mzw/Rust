﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000112 RID: 274
	public class MoveToFoodTarget : BaseAction
	{
		// Token: 0x06000C6F RID: 3183 RVA: 0x00050A84 File Offset: 0x0004EC84
		public override void DoExecute(BaseContext c)
		{
			if (c.AIAgent.FoodTarget == null)
			{
				return;
			}
			c.AIAgent.UpdateDestination(c.AIAgent.FoodTarget.transform);
		}
	}
}
