﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200012A RID: 298
	public sealed class IsBusy : BaseScorer
	{
		// Token: 0x06000CA4 RID: 3236 RVA: 0x00051658 File Offset: 0x0004F858
		public override float GetScore(BaseContext c)
		{
			return (float)((!c.AIAgent.BusyTimerActive()) ? 0 : 1);
		}
	}
}
