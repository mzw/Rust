﻿using System;
using System.Collections.Generic;
using ConVar;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001C6 RID: 454
	public class AiLocationManager : FacepunchBehaviour, IServerComponent
	{
		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000EE0 RID: 3808 RVA: 0x0005BE9C File Offset: 0x0005A09C
		public AiLocationSpawner.SquadSpawnerLocation LocationType
		{
			get
			{
				if (this.MainSpawner != null)
				{
					return this.MainSpawner.Location;
				}
				return AiLocationSpawner.SquadSpawnerLocation.None;
			}
		}

		// Token: 0x06000EE1 RID: 3809 RVA: 0x0005BEBC File Offset: 0x0005A0BC
		private void Awake()
		{
			AiLocationManager.Managers.Add(this);
		}

		// Token: 0x06000EE2 RID: 3810 RVA: 0x0005BECC File Offset: 0x0005A0CC
		private void OnDestroy()
		{
			AiLocationManager.Managers.Remove(this);
		}

		// Token: 0x06000EE3 RID: 3811 RVA: 0x0005BEDC File Offset: 0x0005A0DC
		public global::PathInterestNode GetFirstPatrolPointInRange(Vector3 from, float minRange = 10f, float maxRange = 100f)
		{
			if (this.PatrolPointGroup == null)
			{
				return null;
			}
			if (this.patrolPoints == null)
			{
				this.patrolPoints = new List<global::PathInterestNode>(this.PatrolPointGroup.GetComponentsInChildren<global::PathInterestNode>());
			}
			if (this.patrolPoints.Count == 0)
			{
				return null;
			}
			float num = minRange * minRange;
			float num2 = maxRange * maxRange;
			foreach (global::PathInterestNode pathInterestNode in this.patrolPoints)
			{
				float sqrMagnitude = (pathInterestNode.transform.position - from).sqrMagnitude;
				if (sqrMagnitude >= num && sqrMagnitude <= num2)
				{
					return pathInterestNode;
				}
			}
			return null;
		}

		// Token: 0x06000EE4 RID: 3812 RVA: 0x0005BFB8 File Offset: 0x0005A1B8
		public global::PathInterestNode GetRandomPatrolPointInRange(Vector3 from, float minRange = 10f, float maxRange = 100f, global::PathInterestNode currentPatrolPoint = null)
		{
			if (this.PatrolPointGroup == null)
			{
				return null;
			}
			if (this.patrolPoints == null)
			{
				this.patrolPoints = new List<global::PathInterestNode>(this.PatrolPointGroup.GetComponentsInChildren<global::PathInterestNode>());
			}
			if (this.patrolPoints.Count == 0)
			{
				return null;
			}
			float num = minRange * minRange;
			float num2 = maxRange * maxRange;
			for (int i = 0; i < 20; i++)
			{
				global::PathInterestNode pathInterestNode = this.patrolPoints[Random.Range(0, this.patrolPoints.Count)];
				if (UnityEngine.Time.time < pathInterestNode.NextVisitTime)
				{
					if (pathInterestNode == currentPatrolPoint)
					{
						return null;
					}
				}
				else
				{
					float sqrMagnitude = (pathInterestNode.transform.position - from).sqrMagnitude;
					if (sqrMagnitude >= num && sqrMagnitude <= num2)
					{
						pathInterestNode.NextVisitTime = UnityEngine.Time.time + ConVar.AI.npc_patrol_point_cooldown;
						return pathInterestNode;
					}
				}
			}
			return null;
		}

		// Token: 0x040008B0 RID: 2224
		public static List<AiLocationManager> Managers = new List<AiLocationManager>();

		// Token: 0x040008B1 RID: 2225
		[SerializeField]
		public AiLocationSpawner MainSpawner;

		// Token: 0x040008B2 RID: 2226
		public Transform CoverPointGroup;

		// Token: 0x040008B3 RID: 2227
		public Transform PatrolPointGroup;

		// Token: 0x040008B4 RID: 2228
		private List<global::PathInterestNode> patrolPoints;
	}
}
