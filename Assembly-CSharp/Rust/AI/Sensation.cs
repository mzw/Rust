﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x020001C4 RID: 452
	public struct Sensation
	{
		// Token: 0x040008AB RID: 2219
		public SensationType Type;

		// Token: 0x040008AC RID: 2220
		public Vector3 Position;

		// Token: 0x040008AD RID: 2221
		public float Radius;
	}
}
