﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000106 RID: 262
	public sealed class LineOfSightToTargetEntity : BaseScorer
	{
		// Token: 0x06000C54 RID: 3156 RVA: 0x000505FC File Offset: 0x0004E7FC
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			global::BasePlayer basePlayer = c.AIAgent.AttackTarget as global::BasePlayer;
			if (basePlayer)
			{
				Vector3 attackPosition = c.AIAgent.AttackPosition;
				bool flag = basePlayer.IsVisible(attackPosition, basePlayer.CenterPoint()) || basePlayer.IsVisible(attackPosition, basePlayer.eyes.position) || basePlayer.IsVisible(attackPosition, basePlayer.transform.position);
				return (!flag) ? 0f : 1f;
			}
			if (this.Cover == CoverPoint.CoverType.Full)
			{
				return (!c.AIAgent.AttackTarget.IsVisible(c.AIAgent.AttackPosition)) ? 0f : 1f;
			}
			return (!c.AIAgent.AttackTarget.IsVisible(c.AIAgent.CrouchedAttackPosition)) ? 0f : 1f;
		}

		// Token: 0x040006C1 RID: 1729
		[ApexSerialization]
		private CoverPoint.CoverType Cover;
	}
}
