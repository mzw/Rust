﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200011F RID: 287
	public sealed class HealthFractionCurve : BaseScorer
	{
		// Token: 0x06000C8E RID: 3214 RVA: 0x000513D8 File Offset: 0x0004F5D8
		public override float GetScore(BaseContext c)
		{
			return this.ResponseCurve.Evaluate(c.Entity.healthFraction);
		}

		// Token: 0x040006D6 RID: 1750
		[ApexSerialization]
		private AnimationCurve ResponseCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
	}
}
