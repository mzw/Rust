﻿using System;
using System.Collections;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000195 RID: 405
	public class CoverPoint
	{
		// Token: 0x06000D91 RID: 3473 RVA: 0x00055158 File Offset: 0x00053358
		public CoverPoint(CoverPointVolume volume, float score)
		{
			this.Volume = volume;
			this.Score = score;
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000D92 RID: 3474 RVA: 0x00055170 File Offset: 0x00053370
		// (set) Token: 0x06000D93 RID: 3475 RVA: 0x00055178 File Offset: 0x00053378
		public CoverPointVolume Volume { get; private set; }

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000D94 RID: 3476 RVA: 0x00055184 File Offset: 0x00053384
		// (set) Token: 0x06000D95 RID: 3477 RVA: 0x0005518C File Offset: 0x0005338C
		public global::BaseEntity ReservedFor { get; set; }

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000D96 RID: 3478 RVA: 0x00055198 File Offset: 0x00053398
		public bool IsReserved
		{
			get
			{
				return this.ReservedFor != null;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000D97 RID: 3479 RVA: 0x000551A8 File Offset: 0x000533A8
		// (set) Token: 0x06000D98 RID: 3480 RVA: 0x000551B0 File Offset: 0x000533B0
		public bool IsCompromised { get; set; }

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000D99 RID: 3481 RVA: 0x000551BC File Offset: 0x000533BC
		// (set) Token: 0x06000D9A RID: 3482 RVA: 0x000551C4 File Offset: 0x000533C4
		public float Score { get; set; }

		// Token: 0x06000D9B RID: 3483 RVA: 0x000551D0 File Offset: 0x000533D0
		public void CoverIsCompromised(float cooldown)
		{
			if (this.IsCompromised)
			{
				return;
			}
			if (this.Volume != null)
			{
				this.Volume.StartCoroutine(this.StartCooldown(cooldown));
			}
		}

		// Token: 0x06000D9C RID: 3484 RVA: 0x00055204 File Offset: 0x00053404
		private IEnumerator StartCooldown(float cooldown)
		{
			this.IsCompromised = true;
			yield return UnityEngine.CoroutineEx.waitForSeconds(cooldown);
			this.IsCompromised = false;
			yield break;
		}

		// Token: 0x0400078E RID: 1934
		public Vector3 Position;

		// Token: 0x0400078F RID: 1935
		public Vector3 Normal;

		// Token: 0x04000790 RID: 1936
		public CoverPoint.CoverType NormalCoverType;

		// Token: 0x02000196 RID: 406
		public enum CoverType
		{
			// Token: 0x04000795 RID: 1941
			Full,
			// Token: 0x04000796 RID: 1942
			Partial,
			// Token: 0x04000797 RID: 1943
			None
		}
	}
}
