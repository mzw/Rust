﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200013D RID: 317
	public class PositionInAttackRangeOfTarget : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CC8 RID: 3272 RVA: 0x00051E58 File Offset: 0x00050058
		public override float GetScore(BaseContext c, Vector3 position)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			if (Vector3.Distance(position, c.AIAgent.AttackTargetMemory.Position) < c.AIAgent.GetAttackRange)
			{
				return 1f;
			}
			return 0f;
		}
	}
}
