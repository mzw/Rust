﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200018B RID: 395
	public class ProximityToPeers : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000D78 RID: 3448 RVA: 0x00054E40 File Offset: 0x00053040
		public override float GetScore(BaseContext c, Vector3 position)
		{
			float num = float.MaxValue;
			Vector3 vector = Vector3.zero;
			for (int i = 0; i < c.Memory.All.Count; i++)
			{
				Memory.SeenInfo memory = c.Memory.All[i];
				if (!(memory.Entity == null))
				{
					float num2 = this.Test(memory, c);
					if (num2 > 0f)
					{
						float num3 = (position - memory.Position).sqrMagnitude * num2;
						if (num3 < num)
						{
							num = num3;
							vector = memory.Position;
						}
					}
				}
			}
			if (vector == Vector3.zero)
			{
				return 0f;
			}
			num = Vector3.Distance(vector, position);
			return 1f - num / this.desiredRange;
		}

		// Token: 0x06000D79 RID: 3449 RVA: 0x00054F18 File Offset: 0x00053118
		protected virtual float Test(Memory.SeenInfo memory, BaseContext c)
		{
			if (memory.Entity == null)
			{
				return 0f;
			}
			global::BaseNpc baseNpc = memory.Entity as global::BaseNpc;
			if (baseNpc == null)
			{
				return 0f;
			}
			return 1f;
		}

		// Token: 0x04000775 RID: 1909
		[ApexSerialization(defaultValue = 14f)]
		public float desiredRange = 14f;
	}
}
