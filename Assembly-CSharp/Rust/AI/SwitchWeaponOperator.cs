﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000156 RID: 342
	public class SwitchWeaponOperator : BaseAction
	{
		// Token: 0x06000CF7 RID: 3319 RVA: 0x00052940 File Offset: 0x00050B40
		public override void DoExecute(BaseContext c)
		{
			SwitchWeaponOperator.TrySwitchWeaponTo(c as global::NPCHumanContext, this.WeaponType);
		}

		// Token: 0x06000CF8 RID: 3320 RVA: 0x00052954 File Offset: 0x00050B54
		public static bool TrySwitchWeaponTo(global::NPCHumanContext c, global::NPCPlayerApex.WeaponTypeEnum WeaponType)
		{
			if (c != null)
			{
				if (Time.realtimeSinceStartup < c.Human.NextWeaponSwitchTime)
				{
					return false;
				}
				uint svActiveItemID = c.Human.svActiveItemID;
				global::Item item;
				switch (WeaponType)
				{
				default:
					c.Human.UpdateActiveItem(0u);
					if (svActiveItemID != c.Human.svActiveItemID)
					{
						c.Human.NextWeaponSwitchTime = Time.realtimeSinceStartup + c.Human.WeaponSwitchFrequency;
						c.SetFact(global::NPCPlayerApex.Facts.CurrentWeaponType, (byte)c.Human.GetCurrentWeaponTypeEnum(), true, true);
						c.SetFact(global::NPCPlayerApex.Facts.CurrentAmmoState, (byte)c.Human.GetCurrentAmmoStateEnum(), true, true);
					}
					if (c.AIAgent.IsNavRunning())
					{
						c.AIAgent.GetNavAgent.stoppingDistance = 1f;
					}
					return true;
				case global::NPCPlayerApex.WeaponTypeEnum.CloseRange:
					item = SwitchWeaponOperator.FindBestMelee(c);
					if (item == null)
					{
						item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.None, global::NPCPlayerApex.WeaponTypeEnum.CloseRange, c);
						if (item == null)
						{
							item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.CloseRange, global::NPCPlayerApex.WeaponTypeEnum.MediumRange, c);
							if (item == null)
							{
								item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.MediumRange, global::NPCPlayerApex.WeaponTypeEnum.LongRange, c);
							}
						}
					}
					if (c.AIAgent.IsNavRunning())
					{
						c.AIAgent.GetNavAgent.stoppingDistance = 1f;
					}
					break;
				case global::NPCPlayerApex.WeaponTypeEnum.MediumRange:
					item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.CloseRange, global::NPCPlayerApex.WeaponTypeEnum.MediumRange, c);
					if (item == null)
					{
						item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.MediumRange, global::NPCPlayerApex.WeaponTypeEnum.LongRange, c);
					}
					if (c.AIAgent.IsNavRunning())
					{
						c.AIAgent.GetNavAgent.stoppingDistance = 0.1f;
					}
					break;
				case global::NPCPlayerApex.WeaponTypeEnum.LongRange:
					item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.MediumRange, global::NPCPlayerApex.WeaponTypeEnum.LongRange, c);
					if (item == null)
					{
						item = SwitchWeaponOperator.FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum.CloseRange, global::NPCPlayerApex.WeaponTypeEnum.MediumRange, c);
					}
					if (c.AIAgent.IsNavRunning())
					{
						c.AIAgent.GetNavAgent.stoppingDistance = 0.1f;
					}
					break;
				}
				if (item != null)
				{
					c.Human.UpdateActiveItem(item.uid);
					if (svActiveItemID != c.Human.svActiveItemID)
					{
						c.Human.NextWeaponSwitchTime = Time.realtimeSinceStartup + c.Human.WeaponSwitchFrequency;
						c.SetFact(global::NPCPlayerApex.Facts.CurrentWeaponType, (byte)c.Human.GetCurrentWeaponTypeEnum(), true, true);
						c.SetFact(global::NPCPlayerApex.Facts.CurrentAmmoState, (byte)c.Human.GetCurrentAmmoStateEnum(), true, true);
						c.SetFact(global::NPCPlayerApex.Facts.CurrentToolType, 0, true, true);
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000CF9 RID: 3321 RVA: 0x00052B90 File Offset: 0x00050D90
		private static global::Item FindBestMelee(global::NPCHumanContext c)
		{
			if (c.Human.GetPathStatus() != 0)
			{
				return null;
			}
			global::Item item = null;
			global::BaseMelee baseMelee = null;
			global::Item[] array = c.Human.inventory.AllItems();
			foreach (global::Item item2 in array)
			{
				if (item2.info.category == global::ItemCategory.Weapon && !item2.isBroken)
				{
					global::BaseMelee baseMelee2 = item2.GetHeldEntity() as global::BaseMelee;
					if (baseMelee2)
					{
						if (item == null)
						{
							item = item2;
							baseMelee = baseMelee2;
						}
						else if (baseMelee2.hostileScore > baseMelee.hostileScore)
						{
							item = item2;
							baseMelee = baseMelee2;
						}
					}
				}
			}
			return item;
		}

		// Token: 0x06000CFA RID: 3322 RVA: 0x00052C48 File Offset: 0x00050E48
		private static global::Item FindBestProjInRange(global::NPCPlayerApex.WeaponTypeEnum from, global::NPCPlayerApex.WeaponTypeEnum to, global::NPCHumanContext c)
		{
			global::Item item = null;
			global::BaseProjectile baseProjectile = null;
			global::Item[] array = c.Human.inventory.AllItems();
			foreach (global::Item item2 in array)
			{
				if (item2.info.category == global::ItemCategory.Weapon && !item2.isBroken)
				{
					global::BaseProjectile baseProjectile2 = item2.GetHeldEntity() as global::BaseProjectile;
					if (baseProjectile2 != null && baseProjectile2.effectiveRangeType <= to && baseProjectile2.effectiveRangeType > from)
					{
						if (item == null)
						{
							item = item2;
							baseProjectile = baseProjectile2;
						}
						else if (baseProjectile2.hostileScore > baseProjectile.hostileScore)
						{
							item = item2;
							baseProjectile = baseProjectile2;
						}
					}
				}
			}
			return item;
		}

		// Token: 0x04000713 RID: 1811
		[ApexSerialization]
		private global::NPCPlayerApex.WeaponTypeEnum WeaponType;
	}
}
