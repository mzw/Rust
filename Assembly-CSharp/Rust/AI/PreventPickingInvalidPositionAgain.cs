﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x02000150 RID: 336
	public class PreventPickingInvalidPositionAgain : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CEC RID: 3308 RVA: 0x00052690 File Offset: 0x00050890
		public override float GetScore(BaseContext c, Vector3 option)
		{
			if (c.AIAgent.IsNavRunning())
			{
				NavMeshAgent getNavAgent = c.AIAgent.GetNavAgent;
				if (getNavAgent != null && (!getNavAgent.hasPath || getNavAgent.isPathStale || getNavAgent.pathStatus == 1 || getNavAgent.pathStatus == 2))
				{
					float sqrMagnitude = (c.lastSampledPosition - option).sqrMagnitude;
					if (sqrMagnitude < 0.1f)
					{
						return 0f;
					}
				}
			}
			return 1f;
		}
	}
}
