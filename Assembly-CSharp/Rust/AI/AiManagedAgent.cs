﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200019C RID: 412
	[DefaultExecutionOrder(-102)]
	public class AiManagedAgent : FacepunchBehaviour, IServerComponent
	{
		// Token: 0x06000DB8 RID: 3512 RVA: 0x00055BB0 File Offset: 0x00053DB0
		private void OnEnable()
		{
			this.isRegistered = false;
			if (SingletonComponent<AiManager>.Instance == null || !SingletonComponent<AiManager>.Instance.enabled || AiManager.nav_disable)
			{
				base.enabled = false;
				return;
			}
			this.agent = base.GetComponent<global::IAIAgent>();
			if (this.agent != null)
			{
				if (this.agent.Entity.isClient)
				{
					base.enabled = false;
					return;
				}
				this.agent.AgentTypeIndex = this.AgentTypeIndex;
				float num = SeedRandom.Value((uint)Mathf.Abs(base.GetInstanceID()));
				this.registrationTimeOffset = Time.realtimeSinceStartup + num * 3f;
			}
		}

		// Token: 0x06000DB9 RID: 3513 RVA: 0x00055C60 File Offset: 0x00053E60
		private void FixedUpdate()
		{
			if (!this.isRegistered && this.registrationTimeOffset > 0f && Time.realtimeSinceStartup > this.registrationTimeOffset)
			{
				SingletonComponent<AiManager>.Instance.Add(this.agent);
				this.isRegistered = true;
				if (SingletonComponent<AiManager>.Instance.UseNavMesh)
				{
					this.NavmeshGridCoord = SingletonComponent<AiManager>.Instance.GetCoord(base.transform.localPosition);
				}
			}
		}

		// Token: 0x06000DBA RID: 3514 RVA: 0x00055CDC File Offset: 0x00053EDC
		private void OnDisable()
		{
			if (Application.isQuitting)
			{
				return;
			}
			if (SingletonComponent<AiManager>.Instance == null || !SingletonComponent<AiManager>.Instance.enabled || this.agent == null || this.agent.Entity == null || this.agent.Entity.isClient || !this.isRegistered)
			{
				return;
			}
			SingletonComponent<AiManager>.Instance.Remove(this.agent);
		}

		// Token: 0x040007B1 RID: 1969
		[Tooltip("TODO: Replace with actual agent type id on the NavMeshAgent when we upgrade to 5.6.1 or above.")]
		public int AgentTypeIndex;

		// Token: 0x040007B2 RID: 1970
		[ReadOnly]
		public Vector2i NavmeshGridCoord;

		// Token: 0x040007B3 RID: 1971
		private global::IAIAgent agent;

		// Token: 0x040007B4 RID: 1972
		private bool isRegistered;

		// Token: 0x040007B5 RID: 1973
		private float registrationTimeOffset;
	}
}
