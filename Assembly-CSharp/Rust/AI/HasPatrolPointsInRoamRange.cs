﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000173 RID: 371
	public class HasPatrolPointsInRoamRange : BaseScorer
	{
		// Token: 0x06000D3F RID: 3391 RVA: 0x00054078 File Offset: 0x00052278
		public override float GetScore(BaseContext c)
		{
			return (float)((!HasPatrolPointsInRoamRange.Evaluate(c as global::NPCHumanContext)) ? 0 : 1);
		}

		// Token: 0x06000D40 RID: 3392 RVA: 0x00054094 File Offset: 0x00052294
		public static bool Evaluate(global::NPCHumanContext c)
		{
			if (c.AiLocationManager != null)
			{
				global::PathInterestNode firstPatrolPointInRange = c.AiLocationManager.GetFirstPatrolPointInRange(c.Position, c.AIAgent.GetStats.MinRoamRange, c.AIAgent.GetStats.MaxRoamRange);
				return firstPatrolPointInRange != null && Time.time >= firstPatrolPointInRange.NextVisitTime;
			}
			return false;
		}
	}
}
