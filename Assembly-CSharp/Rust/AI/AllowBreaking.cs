﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000145 RID: 325
	public sealed class AllowBreaking : BaseAction
	{
		// Token: 0x06000CD6 RID: 3286 RVA: 0x0005222C File Offset: 0x0005042C
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.AutoBraking = this.Allow;
		}

		// Token: 0x04000702 RID: 1794
		[ApexSerialization]
		public bool Allow;
	}
}
