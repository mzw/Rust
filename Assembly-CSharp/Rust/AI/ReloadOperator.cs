﻿using System;

namespace Rust.Ai
{
	// Token: 0x02000155 RID: 341
	public class ReloadOperator : BaseAction
	{
		// Token: 0x06000CF4 RID: 3316 RVA: 0x000528A4 File Offset: 0x00050AA4
		public override void DoExecute(BaseContext c)
		{
			ReloadOperator.Reload(c as global::NPCHumanContext);
		}

		// Token: 0x06000CF5 RID: 3317 RVA: 0x000528B4 File Offset: 0x00050AB4
		public static void Reload(global::NPCHumanContext c)
		{
			if (c == null)
			{
				return;
			}
			global::HeldEntity heldEntity = c.Human.GetHeldEntity();
			global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
			if (attackEntity == null)
			{
				return;
			}
			global::BaseProjectile baseProjectile = attackEntity as global::BaseProjectile;
			if (baseProjectile && baseProjectile.primaryMagazine.CanAiReload(c.Human))
			{
				baseProjectile.ServerReload();
				if (c.Human.OnReload != null)
				{
					c.Human.OnReload();
				}
			}
		}
	}
}
