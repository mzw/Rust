﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000135 RID: 309
	public sealed class AimingAtTarget : BaseScorer
	{
		// Token: 0x06000CBA RID: 3258 RVA: 0x00051A30 File Offset: 0x0004FC30
		public override float GetScore(BaseContext c)
		{
			if (!(c.AIAgent.AttackTarget != null))
			{
				return 0f;
			}
			if (this.PerfectKnowledge)
			{
				return (Vector3.Dot(c.AIAgent.CurrentAimAngles, (c.AIAgent.AttackTarget.transform.position - c.AIAgent.AttackPosition).normalized) < this.arc) ? 0f : 1f;
			}
			return (Vector3.Dot(c.AIAgent.CurrentAimAngles, (c.AIAgent.AttackTargetMemory.Position - c.AIAgent.AttackPosition).normalized) < this.arc) ? 0f : 1f;
		}

		// Token: 0x040006E5 RID: 1765
		[ApexSerialization]
		public float arc;

		// Token: 0x040006E6 RID: 1766
		[ApexSerialization]
		public bool PerfectKnowledge;
	}
}
