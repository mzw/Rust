﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x0200014F RID: 335
	public class InInCoverFromEntity : WeightedScorerBase<global::BaseEntity>
	{
		// Token: 0x06000CEA RID: 3306 RVA: 0x00052618 File Offset: 0x00050818
		public override float GetScore(BaseContext ctx, global::BaseEntity option)
		{
			if (SingletonComponent<AiManager>.Instance == null || !SingletonComponent<AiManager>.Instance.enabled || !SingletonComponent<AiManager>.Instance.UseCover || ctx.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			if (!(ctx is global::NPCHumanContext))
			{
				return 0f;
			}
			return 0f;
		}

		// Token: 0x04000708 RID: 1800
		[ApexSerialization]
		public float CoverArcThreshold = -0.75f;
	}
}
