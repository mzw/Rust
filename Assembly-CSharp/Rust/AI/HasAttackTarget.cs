﻿using System;

namespace Rust.Ai
{
	// Token: 0x020000F9 RID: 249
	public class HasAttackTarget : BaseScorer
	{
		// Token: 0x06000C39 RID: 3129 RVA: 0x00050188 File Offset: 0x0004E388
		public override float GetScore(BaseContext c)
		{
			if (c.AIAgent.AttackTarget == null)
			{
				return 0f;
			}
			return this.score;
		}
	}
}
