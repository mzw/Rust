﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000107 RID: 263
	public sealed class TargetEntityVisibleFor : BaseScorer
	{
		// Token: 0x06000C56 RID: 3158 RVA: 0x00050714 File Offset: 0x0004E914
		public override float GetScore(BaseContext c)
		{
			return (c.AIAgent.AttackTargetVisibleFor < this.duration) ? 0f : 1f;
		}

		// Token: 0x040006C2 RID: 1730
		[ApexSerialization]
		public float duration;
	}
}
