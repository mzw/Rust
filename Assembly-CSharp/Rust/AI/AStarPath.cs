﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rust.AI
{
	// Token: 0x020000C4 RID: 196
	public static class AStarPath
	{
		// Token: 0x06000AAA RID: 2730 RVA: 0x00048BEC File Offset: 0x00046DEC
		private static float Heuristic(global::BasePathNode from, global::BasePathNode to)
		{
			return Vector3.Distance(from.transform.position, to.transform.position);
		}

		// Token: 0x06000AAB RID: 2731 RVA: 0x00048C0C File Offset: 0x00046E0C
		public static bool FindPath(global::BasePathNode start, global::BasePathNode goal, out Stack<global::BasePathNode> path, out float pathCost)
		{
			path = null;
			pathCost = -1f;
			bool result = false;
			if (start == goal)
			{
				return false;
			}
			AStarNodeList astarNodeList = new AStarNodeList();
			HashSet<global::BasePathNode> hashSet = new HashSet<global::BasePathNode>();
			AStarNode item = new AStarNode(0f, AStarPath.Heuristic(start, goal), null, start);
			astarNodeList.Add(item);
			while (astarNodeList.Count > 0)
			{
				AStarNode astarNode = astarNodeList[0];
				astarNodeList.RemoveAt(0);
				hashSet.Add(astarNode.Node);
				if (astarNode.Satisfies(goal))
				{
					path = new Stack<global::BasePathNode>();
					pathCost = 0f;
					while (astarNode.Parent != null)
					{
						pathCost += astarNode.F;
						path.Push(astarNode.Node);
						astarNode = astarNode.Parent;
					}
					if (astarNode != null)
					{
						path.Push(astarNode.Node);
					}
					result = true;
					break;
				}
				foreach (global::BasePathNode basePathNode in astarNode.Node.linked)
				{
					if (!hashSet.Contains(basePathNode))
					{
						float num = astarNode.G + AStarPath.Heuristic(astarNode.Node, basePathNode);
						AStarNode astarNode2 = astarNodeList.GetAStarNodeOf(basePathNode);
						if (astarNode2 == null)
						{
							astarNode2 = new AStarNode(num, AStarPath.Heuristic(basePathNode, goal), astarNode, basePathNode);
							astarNodeList.Add(astarNode2);
							astarNodeList.AStarNodeSort();
						}
						else if (num < astarNode2.G)
						{
							astarNode2.Update(num, astarNode2.H, astarNode, basePathNode);
							astarNodeList.AStarNodeSort();
						}
					}
				}
			}
			return result;
		}
	}
}
