﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000144 RID: 324
	public class FacesAwayFromDanger : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000CD4 RID: 3284 RVA: 0x00052184 File Offset: 0x00050384
		public override float GetScore(BaseContext c, Vector3 position)
		{
			float num = 0f;
			Vector3 vector = position - c.Entity.transform.position.normalized;
			for (int i = 0; i < c.Memory.All.Count; i++)
			{
				Vector3 normalized = (c.Memory.All[i].Position - c.Entity.transform.position).normalized;
				float num2 = Vector3.Dot(vector, normalized);
				num += -num2;
			}
			return num;
		}
	}
}
