﻿using System;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x0200017F RID: 383
	public sealed class AimingAtPoint : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000D61 RID: 3425 RVA: 0x00054968 File Offset: 0x00052B68
		public override float GetScore(BaseContext context, Vector3 position)
		{
			return Vector3.Dot(context.Entity.transform.forward, (position - context.Position).normalized);
		}
	}
}
