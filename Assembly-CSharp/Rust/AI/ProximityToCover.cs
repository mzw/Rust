﻿using System;
using Apex.Serialization;
using UnityEngine;

namespace Rust.Ai
{
	// Token: 0x02000188 RID: 392
	public class ProximityToCover : WeightedScorerBase<Vector3>
	{
		// Token: 0x06000D73 RID: 3443 RVA: 0x00054CB8 File Offset: 0x00052EB8
		public override float GetScore(BaseContext ctx, Vector3 option)
		{
			global::NPCHumanContext npchumanContext = ctx as global::NPCHumanContext;
			if (npchumanContext != null)
			{
				float num;
				CoverPoint closestCover = ProximityToCover.GetClosestCover(npchumanContext, option, this.MaxDistance, this._coverType, out num);
				if (closestCover != null)
				{
					return this.Response.Evaluate(num / this.MaxDistance) * closestCover.Score;
				}
			}
			return 0f;
		}

		// Token: 0x06000D74 RID: 3444 RVA: 0x00054D10 File Offset: 0x00052F10
		internal static CoverPoint GetClosestCover(global::NPCHumanContext c, Vector3 point, float MaxDistance, ProximityToCover.CoverType _coverType, out float bestDistance)
		{
			bestDistance = MaxDistance;
			CoverPoint result = null;
			for (int i = 0; i < c.sampledCoverPoints.Count; i++)
			{
				CoverPoint coverPoint = c.sampledCoverPoints[i];
				CoverPoint.CoverType coverType = c.sampledCoverPointTypes[i];
				if ((_coverType != ProximityToCover.CoverType.Full || coverType == CoverPoint.CoverType.Full) && (_coverType != ProximityToCover.CoverType.Partial || coverType == CoverPoint.CoverType.Partial))
				{
					float num = Vector3.Distance(coverPoint.Position, point);
					if (num < bestDistance)
					{
						bestDistance = num;
						result = coverPoint;
					}
				}
			}
			return result;
		}

		// Token: 0x0400076B RID: 1899
		[ApexSerialization]
		public float MaxDistance = 20f;

		// Token: 0x0400076C RID: 1900
		[ApexSerialization]
		public ProximityToCover.CoverType _coverType;

		// Token: 0x0400076D RID: 1901
		[ApexSerialization]
		public AnimationCurve Response = AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);

		// Token: 0x02000189 RID: 393
		public enum CoverType
		{
			// Token: 0x0400076F RID: 1903
			All,
			// Token: 0x04000770 RID: 1904
			Full,
			// Token: 0x04000771 RID: 1905
			Partial
		}
	}
}
