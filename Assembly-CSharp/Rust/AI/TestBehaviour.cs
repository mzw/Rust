﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000134 RID: 308
	public sealed class TestBehaviour : BaseScorer
	{
		// Token: 0x06000CB8 RID: 3256 RVA: 0x00051A08 File Offset: 0x0004FC08
		public override float GetScore(BaseContext c)
		{
			return (float)((c.AIAgent.CurrentBehaviour != this.Behaviour) ? 0 : 1);
		}

		// Token: 0x040006E4 RID: 1764
		[ApexSerialization]
		public global::BaseNpc.Behaviour Behaviour;
	}
}
