﻿using System;
using Apex.Serialization;

namespace Rust.Ai
{
	// Token: 0x02000160 RID: 352
	public class SetFactBoolean : BaseAction
	{
		// Token: 0x06000D10 RID: 3344 RVA: 0x00053048 File Offset: 0x00051248
		public override void DoExecute(BaseContext c)
		{
			c.SetFact(this.fact, (!this.value) ? 0 : 1);
		}

		// Token: 0x04000720 RID: 1824
		[ApexSerialization]
		public global::BaseNpc.Facts fact;

		// Token: 0x04000721 RID: 1825
		[ApexSerialization(defaultValue = false)]
		public bool value;
	}
}
