﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Apex.DataStructures;
using Apex.LoadBalancing;
using Facepunch;
using UnityEngine;
using UnityEngine.AI;

namespace Rust.Ai
{
	// Token: 0x0200019D RID: 413
	[DefaultExecutionOrder(-103)]
	public class AiManager : SingletonComponent<AiManager>, IServerComponent, ILoadBalanced
	{
		// Token: 0x06000DBB RID: 3515 RVA: 0x00055D68 File Offset: 0x00053F68
		public AiManager()
		{
			if (AiManager.<>f__mg$cache0 == null)
			{
				AiManager.<>f__mg$cache0 = new Func<global::BasePlayer, bool>(AiManager.InterestedInPlayersOnly);
			}
			this.filter = AiManager.<>f__mg$cache0;
			this.IntensityCellSize = 80f;
			this.HitResponse = AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);
			this.hitResponseOutputMultiplier = 1f;
			this.HurtResponse = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
			this.maxHurtResponseInput = 5.5f;
			this.hurtResponseOutputMultiplier = 1f;
			this.OnDeathIntensityBonus = 1f;
			this.IntensityDecayRate = 0.1f;
			this.NoEngagementToDecayTime = 5f;
			this.DebugIntensityHeightMultiplier = 100f;
			this.DebugMinColor = Color.green;
			this.DebugMaxColor = Color.red;
			this.UseNavMesh = true;
			this.CellTimeoutThreshold = 30f;
			base..ctor();
		}

		// Token: 0x06000DBC RID: 3516 RVA: 0x00055ED4 File Offset: 0x000540D4
		internal void OnEnableAgency()
		{
		}

		// Token: 0x06000DBD RID: 3517 RVA: 0x00055ED8 File Offset: 0x000540D8
		internal void OnDisableAgency()
		{
		}

		// Token: 0x06000DBE RID: 3518 RVA: 0x00055EDC File Offset: 0x000540DC
		public void Add(global::IAIAgent agent)
		{
			if (AiManager.ai_dormant)
			{
				if (this.IsAgentCloseToPlayers(agent))
				{
					this.InternalAdd(agent);
				}
				else
				{
					this.AddDormantAgency(agent);
				}
			}
			else
			{
				this.InternalAdd(agent);
			}
		}

		// Token: 0x06000DBF RID: 3519 RVA: 0x00055F14 File Offset: 0x00054114
		internal void InternalAdd(global::IAIAgent agent)
		{
			if (AiManager.nav_grid && this.UseNavMesh && this.NavMeshAddAgent(agent, delegate(global::IAIAgent a)
			{
				if (a != null)
				{
					this.InternalAddPlusAdjacents(agent);
				}
			}) == AiManager.NavMeshAddResult.Async)
			{
				return;
			}
			this.InternalAddPlusAdjacents(agent);
		}

		// Token: 0x06000DC0 RID: 3520 RVA: 0x00055F78 File Offset: 0x00054178
		internal void InternalAddPlusAdjacents(global::IAIAgent agent)
		{
			this.AddActiveAgency(agent);
			if (AiManager.nav_grid && this.UseNavMesh)
			{
				Vector2i vector2i = this.NavMeshGrid.WorldToGridCoords(agent.Entity.ServerPosition);
				for (int i = 1; i < AiManager.Directions.Length; i++)
				{
					this.NavMeshAddAtCoord(agent, vector2i + AiManager.Directions[i], agent.Entity.ServerPosition.y, agent.AgentTypeIndex);
				}
			}
		}

		// Token: 0x06000DC1 RID: 3521 RVA: 0x00056008 File Offset: 0x00054208
		internal void UpdateAgentSurroundings()
		{
			if (AiManager.nav_grid && this.UseNavMesh)
			{
				foreach (global::IAIAgent iaiagent in this.activeAgents)
				{
					if (iaiagent.AgencyUpdateRequired)
					{
						iaiagent.AgencyUpdateRequired = false;
						for (int i = 1; i < AiManager.Directions.Length; i++)
						{
							this.NavMeshAddAtCoord(iaiagent, iaiagent.CurrentCoord + AiManager.Directions[i], iaiagent.Entity.ServerPosition.y, iaiagent.AgentTypeIndex);
						}
					}
				}
			}
		}

		// Token: 0x06000DC2 RID: 3522 RVA: 0x000560D8 File Offset: 0x000542D8
		public void Remove(global::IAIAgent agent)
		{
			this.RemoveActiveAgency(agent);
			if (AiManager.ai_dormant)
			{
				this.RemoveDormantAgency(agent);
			}
		}

		// Token: 0x06000DC3 RID: 3523 RVA: 0x000560F4 File Offset: 0x000542F4
		internal void AddActiveAgency(global::IAIAgent agent)
		{
			if (this.pendingAddToActive.Contains(agent))
			{
				return;
			}
			this.pendingAddToActive.Add(agent);
		}

		// Token: 0x06000DC4 RID: 3524 RVA: 0x00056118 File Offset: 0x00054318
		internal void AddDormantAgency(global::IAIAgent agent)
		{
			if (this.pendingAddToDormant.Contains(agent))
			{
				return;
			}
			this.pendingAddToDormant.Add(agent);
		}

		// Token: 0x06000DC5 RID: 3525 RVA: 0x0005613C File Offset: 0x0005433C
		internal void RemoveActiveAgency(global::IAIAgent agent)
		{
			if (this.pendingRemoveFromActive.Contains(agent))
			{
				return;
			}
			this.pendingRemoveFromActive.Add(agent);
		}

		// Token: 0x06000DC6 RID: 3526 RVA: 0x00056160 File Offset: 0x00054360
		internal void RemoveDormantAgency(global::IAIAgent agent)
		{
			if (this.pendingRemoveFromDormant.Contains(agent))
			{
				return;
			}
			this.pendingRemoveFromDormant.Add(agent);
		}

		// Token: 0x06000DC7 RID: 3527 RVA: 0x00056184 File Offset: 0x00054384
		internal void UpdateAgency()
		{
			this.AgencyCleanup();
			this.AgencyAddPending();
			if (AiManager.ai_dormant)
			{
				this.TryWakeUpDormantAgents();
				this.TryMakeAgentsDormant();
			}
			if (!AiManager.nav_disable && AiManager.nav_grid && AiManager.nav_grid_agents_expand_domain_enabled)
			{
				this.UpdateAgentSurroundings();
			}
		}

		// Token: 0x06000DC8 RID: 3528 RVA: 0x000561D8 File Offset: 0x000543D8
		private void AgencyCleanup()
		{
			if (AiManager.ai_dormant)
			{
				foreach (global::IAIAgent iaiagent in this.pendingRemoveFromDormant)
				{
					if (iaiagent != null)
					{
						this.dormantAgents.Remove(iaiagent);
					}
				}
				this.pendingRemoveFromDormant.Clear();
			}
			foreach (global::IAIAgent iaiagent2 in this.pendingRemoveFromActive)
			{
				if (iaiagent2 != null)
				{
					this.activeAgents.Remove(iaiagent2);
					if (AiManager.nav_grid && this.UseNavMesh)
					{
						this.RemoveAgentLinkReference(iaiagent2, true);
					}
				}
			}
			this.pendingRemoveFromActive.Clear();
		}

		// Token: 0x06000DC9 RID: 3529 RVA: 0x000562E0 File Offset: 0x000544E0
		private void AgencyAddPending()
		{
			if (AiManager.ai_dormant)
			{
				foreach (global::IAIAgent iaiagent in this.pendingAddToDormant)
				{
					if (iaiagent != null && !(iaiagent.Entity == null) && !iaiagent.Entity.IsDestroyed)
					{
						this.dormantAgents.Add(iaiagent);
						iaiagent.Pause();
					}
				}
				this.pendingAddToDormant.Clear();
			}
			foreach (global::IAIAgent iaiagent2 in this.pendingAddToActive)
			{
				if (iaiagent2 != null && !(iaiagent2.Entity == null) && !iaiagent2.Entity.IsDestroyed)
				{
					if (this.activeAgents.Add(iaiagent2))
					{
						iaiagent2.Resume();
					}
				}
			}
			this.pendingAddToActive.Clear();
		}

		// Token: 0x06000DCA RID: 3530 RVA: 0x0005641C File Offset: 0x0005461C
		private void TryWakeUpDormantAgents()
		{
			if (!AiManager.ai_dormant || this.dormantAgents.Count == 0)
			{
				return;
			}
			if (this.lastWakeUpDormantIndex >= this.dormantAgents.Count)
			{
				this.lastWakeUpDormantIndex = 0;
			}
			int num = this.lastWakeUpDormantIndex;
			int i = 0;
			while (i < AiManager.ai_dormant_max_wakeup_per_tick)
			{
				if (this.lastWakeUpDormantIndex >= this.dormantAgents.Count)
				{
					this.lastWakeUpDormantIndex = 0;
				}
				if (this.lastWakeUpDormantIndex == num && i > 0)
				{
					break;
				}
				global::IAIAgent iaiagent = this.dormantAgents[this.lastWakeUpDormantIndex];
				this.lastWakeUpDormantIndex++;
				i++;
				if (iaiagent.Entity.IsDestroyed)
				{
					this.RemoveDormantAgency(iaiagent);
				}
				else if (this.IsAgentCloseToPlayers(iaiagent))
				{
					this.InternalAdd(iaiagent);
					this.RemoveDormantAgency(iaiagent);
				}
			}
		}

		// Token: 0x06000DCB RID: 3531 RVA: 0x0005650C File Offset: 0x0005470C
		private void TryMakeAgentsDormant()
		{
			if (!AiManager.ai_dormant)
			{
				return;
			}
			foreach (global::IAIAgent iaiagent in this.activeAgents)
			{
				if (iaiagent.Entity.IsDestroyed)
				{
					this.RemoveActiveAgency(iaiagent);
				}
				else if (!this.IsAgentCloseToPlayers(iaiagent))
				{
					this.AddDormantAgency(iaiagent);
					this.RemoveActiveAgency(iaiagent);
				}
			}
		}

		// Token: 0x06000DCC RID: 3532 RVA: 0x000565A4 File Offset: 0x000547A4
		internal void ForceAgentsDormant(Vector2i coord)
		{
			if (!AiManager.nav_grid || !this.UseNavMesh || !AiManager.ai_dormant)
			{
				return;
			}
			foreach (global::IAIAgent iaiagent in this.activeAgents)
			{
				Vector2i coord2 = this.GetCoord(iaiagent.Entity.ServerPosition);
				if (coord2 == coord)
				{
					this.AddDormantAgency(iaiagent);
					this.RemoveActiveAgency(iaiagent);
				}
			}
		}

		// Token: 0x06000DCD RID: 3533 RVA: 0x00056648 File Offset: 0x00054848
		internal void OnEnableCover()
		{
			if (this.coverPointVolumeGrid == null)
			{
				this.coverPointVolumeGrid = new WorldSpaceGrid<CoverPointVolume>(global::TerrainMeta.Size.x, this.CoverPointVolumeCellSize);
			}
		}

		// Token: 0x06000DCE RID: 3534 RVA: 0x00056680 File Offset: 0x00054880
		internal void OnDisableCover()
		{
			if (this.coverPointVolumeGrid == null || this.coverPointVolumeGrid.Cells == null)
			{
				return;
			}
			for (int i = 0; i < this.coverPointVolumeGrid.Cells.Length; i++)
			{
				Object.Destroy(this.coverPointVolumeGrid.Cells[i]);
			}
		}

		// Token: 0x06000DCF RID: 3535 RVA: 0x000566DC File Offset: 0x000548DC
		public static CoverPointVolume CreateNewCoverVolume(Vector3 point, Transform coverPointGroup)
		{
			if (SingletonComponent<AiManager>.Instance != null && SingletonComponent<AiManager>.Instance.enabled && SingletonComponent<AiManager>.Instance.UseCover)
			{
				CoverPointVolume coverPointVolume = SingletonComponent<AiManager>.Instance.GetCoverVolumeContaining(point);
				if (coverPointVolume == null)
				{
					Vector2i vector2i = SingletonComponent<AiManager>.Instance.coverPointVolumeGrid.WorldToGridCoords(point);
					if (SingletonComponent<AiManager>.Instance.cpvPrefab != null)
					{
						coverPointVolume = Object.Instantiate<CoverPointVolume>(SingletonComponent<AiManager>.Instance.cpvPrefab);
					}
					else
					{
						coverPointVolume = new GameObject("CoverPointVolume").AddComponent<CoverPointVolume>();
					}
					coverPointVolume.transform.localPosition = default(Vector3);
					coverPointVolume.transform.position = SingletonComponent<AiManager>.Instance.coverPointVolumeGrid.GridToWorldCoords(vector2i) + Vector3.up * point.y;
					coverPointVolume.transform.localScale = new Vector3(SingletonComponent<AiManager>.Instance.CoverPointVolumeCellSize, SingletonComponent<AiManager>.Instance.CoverPointVolumeCellHeight, SingletonComponent<AiManager>.Instance.CoverPointVolumeCellSize);
					coverPointVolume.CoverLayerMask = SingletonComponent<AiManager>.Instance.DynamicCoverPointVolumeLayerMask;
					coverPointVolume.CoverPointRayLength = SingletonComponent<AiManager>.Instance.CoverPointRayLength;
					SingletonComponent<AiManager>.Instance.coverPointVolumeGrid[vector2i] = coverPointVolume;
					coverPointVolume.GenerateCoverPoints(coverPointGroup);
				}
				return coverPointVolume;
			}
			return null;
		}

		// Token: 0x06000DD0 RID: 3536 RVA: 0x00056828 File Offset: 0x00054A28
		public CoverPointVolume GetCoverVolumeContaining(Vector3 point)
		{
			Vector2i vector2i = this.coverPointVolumeGrid.WorldToGridCoords(point);
			return this.coverPointVolumeGrid[vector2i];
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000DD1 RID: 3537 RVA: 0x00056850 File Offset: 0x00054A50
		public bool repeat
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000DD2 RID: 3538 RVA: 0x00056854 File Offset: 0x00054A54
		public void Initialize()
		{
			if (AiManager.nav_grid && !AiManager.nav_disable)
			{
				this.UseNavMesh = true;
			}
			else
			{
				this.UseNavMesh = false;
			}
			this.OnEnableAgency();
			if (this.UseNavMesh)
			{
				this.OnEnableNavMeshGrid();
			}
			if (this.UseCover)
			{
				this.OnEnableCover();
			}
			if (this.UseIntensity)
			{
				this.OnEnableIntensity();
			}
			global::AiManagerLoadBalancer.aiManagerLoadBalancer.Add(this);
		}

		// Token: 0x06000DD3 RID: 3539 RVA: 0x000568D0 File Offset: 0x00054AD0
		private void OnDisable()
		{
			if (Application.isQuitting)
			{
				return;
			}
			this.OnDisableAgency();
			if (this.UseNavMesh)
			{
				this.OnDisableNavMeshGrid();
			}
			if (this.UseCover)
			{
				this.OnDisableCover();
			}
			if (this.UseIntensity)
			{
				this.OnDisableIntensity();
			}
			global::AiManagerLoadBalancer.aiManagerLoadBalancer.Remove(this);
		}

		// Token: 0x06000DD4 RID: 3540 RVA: 0x0005692C File Offset: 0x00054B2C
		public float? ExecuteUpdate(float deltaTime, float nextInterval)
		{
			if (AiManager.nav_disable)
			{
				return new float?(nextInterval);
			}
			this.UpdateAgency();
			if (this.UseNavMesh)
			{
				this.UpdateNavMeshGridCell();
			}
			if (this.UseIntensity)
			{
				this.UpdateIntensityGridCellLifetimes();
			}
			return new float?(Random.value + 1f);
		}

		// Token: 0x06000DD5 RID: 3541 RVA: 0x00056984 File Offset: 0x00054B84
		private bool IsAgentCloseToPlayers(global::IAIAgent agent)
		{
			bool result;
			if (AiManager.nav_grid && this.UseNavMesh)
			{
				Vector2i coord = this.GetCoord(agent.Entity.ServerPosition);
				result = this.IsCoordCloseToPlayers(coord);
			}
			else
			{
				int playersInSphere = global::BaseEntity.Query.Server.GetPlayersInSphere(agent.Entity.ServerPosition, AiManager.ai_to_player_distance_wakeup_range, this.playerVicinityQuery, this.filter);
				result = (playersInSphere > 0);
			}
			return result;
		}

		// Token: 0x06000DD6 RID: 3542 RVA: 0x000569F4 File Offset: 0x00054BF4
		private static bool InterestedInPlayersOnly(global::BaseEntity entity)
		{
			global::BasePlayer basePlayer = entity as global::BasePlayer;
			return !(basePlayer == null) && !(basePlayer is global::IAIAgent) && !basePlayer.IsSleeping() && basePlayer.IsConnected;
		}

		// Token: 0x06000DD7 RID: 3543 RVA: 0x00056A3C File Offset: 0x00054C3C
		private bool IsCoordCloseToPlayers(Vector2i coord)
		{
			if (this.UseNavMesh)
			{
				foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
				{
					Vector2i coord2 = this.GetCoord(basePlayer.ServerPosition);
					if (this.IsCoordEqualOrAdjacentToOther(coord2, coord))
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000DD8 RID: 3544 RVA: 0x00056AC0 File Offset: 0x00054CC0
		internal void OnEnableIntensity()
		{
			if (this.intensityGrid == null)
			{
				this.intensityGrid = new WorldSpaceGrid<AiManager.EngagementInfo>(global::TerrainMeta.Size.x, this.IntensityCellSize);
			}
		}

		// Token: 0x06000DD9 RID: 3545 RVA: 0x00056AF8 File Offset: 0x00054CF8
		internal void OnDisableIntensity()
		{
		}

		// Token: 0x06000DDA RID: 3546 RVA: 0x00056AFC File Offset: 0x00054CFC
		public void UpdateIntensityGridCellLifetimes()
		{
			for (int i = 0; i < this.intensityGrid.Cells.Length; i++)
			{
				AiManager.EngagementInfo engagementInfo = this.intensityGrid.Cells[i];
				if (engagementInfo != null && Time.realtimeSinceStartup - engagementInfo.LastEngagementTime > this.NoEngagementToDecayTime)
				{
					engagementInfo.Intensity = Mathf.Max(engagementInfo.Intensity - this.IntensityDecayRate, 0f);
					if (Mathf.Approximately(engagementInfo.Intensity, 0f) && engagementInfo.Timeout(this.CellTimeoutThreshold))
					{
						Pool.Free<AiManager.EngagementInfo>(ref engagementInfo);
					}
				}
			}
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				if (!(basePlayer is global::IAIAgent))
				{
					Vector2i vector2i = this.intensityGrid.WorldToGridCoords(basePlayer.ServerPosition);
					foreach (Vector2i vector2i2 in AiManager.Directions)
					{
						Vector2i vector2i3 = vector2i + vector2i2;
						if (vector2i3.x >= 0 && vector2i3.y >= 0 && vector2i3.x < this.intensityGrid.CellCount && vector2i3.y < this.intensityGrid.CellCount)
						{
							AiManager.EngagementInfo engagementInfo2 = this.intensityGrid[vector2i3];
							if (engagementInfo2 != null)
							{
								engagementInfo2.KeepAlive();
							}
						}
					}
				}
			}
		}

		// Token: 0x06000DDB RID: 3547 RVA: 0x00056CAC File Offset: 0x00054EAC
		public void OnHit(global::HitInfo info)
		{
			global::BasePlayer basePlayer = info.HitEntity as global::BasePlayer;
			if (basePlayer != null && !(basePlayer is global::IAIAgent))
			{
				Vector2i vector2i = this.intensityGrid.WorldToGridCoords(basePlayer.ServerPosition);
				AiManager.EngagementInfo engagementInfo = this.intensityGrid[vector2i];
				if (engagementInfo == null)
				{
					engagementInfo = Pool.Get<AiManager.EngagementInfo>();
					engagementInfo.Coord = vector2i;
					this.intensityGrid[vector2i] = engagementInfo;
				}
				engagementInfo.Intensity += this.HitResponse.Evaluate(basePlayer.healthFraction) * this.hitResponseOutputMultiplier;
				if (basePlayer.IsDead())
				{
					engagementInfo.Intensity += this.OnDeathIntensityBonus;
				}
				engagementInfo.LastEngagementTime = Time.realtimeSinceStartup;
			}
			global::BasePlayer basePlayer2 = info.Initiator as global::BasePlayer;
			if (basePlayer2 != null && !(basePlayer2 is global::IAIAgent))
			{
				Vector2i vector2i2 = this.intensityGrid.WorldToGridCoords(basePlayer2.ServerPosition);
				AiManager.EngagementInfo engagementInfo2 = this.intensityGrid[vector2i2];
				if (engagementInfo2 == null)
				{
					engagementInfo2 = Pool.Get<AiManager.EngagementInfo>();
					engagementInfo2.Coord = vector2i2;
					this.intensityGrid[vector2i2] = engagementInfo2;
				}
				global::BaseCombatEntity baseCombatEntity = (!(basePlayer != null)) ? (info.HitEntity as global::BaseCombatEntity) : basePlayer;
				float num = Mathf.Clamp(this.fearEstimate(basePlayer2, baseCombatEntity), 0f, this.maxHurtResponseInput);
				engagementInfo2.Intensity += this.HurtResponse.Evaluate(num) * this.hurtResponseOutputMultiplier;
				if (baseCombatEntity != null && baseCombatEntity.IsDead())
				{
					engagementInfo2.Intensity += this.OnDeathIntensityBonus;
				}
				engagementInfo2.LastEngagementTime = Time.realtimeSinceStartup;
			}
		}

		// Token: 0x06000DDC RID: 3548 RVA: 0x00056E6C File Offset: 0x0005506C
		private float fearEstimate(global::BasePlayer initiator, global::BaseCombatEntity victim)
		{
			float num = 0f;
			if (victim == null)
			{
				return num;
			}
			global::BaseNpc baseNpc = victim as global::BaseNpc;
			if (baseNpc != null)
			{
				if (baseNpc.Stats.Size > 0.5f)
				{
					if (baseNpc.WantsToAttack(initiator) > 0.25f)
					{
						num += 0.3f;
					}
					if (baseNpc.AttackTarget == initiator)
					{
						num += 0.5f;
					}
					if (baseNpc.CurrentBehaviour == global::BaseNpc.Behaviour.Attack)
					{
						num *= 1.5f;
					}
					if (baseNpc.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
					{
						num *= 0.1f;
					}
				}
			}
			else
			{
				global::NPCPlayerApex npcplayerApex = victim as global::NPCPlayerApex;
				if (npcplayerApex != null)
				{
					if (npcplayerApex.AttackTarget == initiator)
					{
						num += 1f;
					}
					if (npcplayerApex.CurrentBehaviour == global::BaseNpc.Behaviour.Attack)
					{
						num *= 1.5f;
					}
					if (npcplayerApex.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
					{
						num *= 0.1f;
					}
				}
			}
			num += this.HitResponse.Evaluate(initiator.healthFraction);
			num += 1f - this.HitResponse.Evaluate(victim.healthFraction);
			global::BasePlayer basePlayer = victim as global::BasePlayer;
			if (basePlayer != null)
			{
				num += ((!(basePlayer is global::IAIAgent)) ? 2f : 1f);
			}
			return num;
		}

		// Token: 0x06000DDD RID: 3549 RVA: 0x00056FC8 File Offset: 0x000551C8
		internal void OnEnableNavMeshGrid()
		{
			if (this.NavMeshGrid == null)
			{
				this.NavMeshGrid = new WorldSpaceGrid<NavMeshGridCell>(global::TerrainMeta.Size.x, (float)AiManager.nav_grid_cell_width);
			}
			this.AgentLinks = new Dictionary<int, NavMeshGridCell.NavMeshLinkInfo>();
			this.LinkPool = new global::PrefabPool();
			this.bakeHeap = new BinaryHeap<AiManager.BakeInfo>(256, new AiManager.BakeInfo.Comparer());
			NavMesh.pathfindingIterationsPerFrame = AiManager.pathfindingIterationsPerFrame;
		}

		// Token: 0x06000DDE RID: 3550 RVA: 0x00057034 File Offset: 0x00055234
		internal void OnDisableNavMeshGrid()
		{
		}

		// Token: 0x06000DDF RID: 3551 RVA: 0x00057038 File Offset: 0x00055238
		internal AiManager.NavMeshAddResult NavMeshAddAgent(global::IAIAgent agent, Action<global::IAIAgent> onAsyncDoneCallback)
		{
			if (this.NavMeshGrid == null)
			{
				this.NavMeshGrid = new WorldSpaceGrid<NavMeshGridCell>(global::TerrainMeta.Size.x, (float)AiManager.nav_grid_cell_width);
			}
			Vector2i vector2i = this.NavMeshGrid.WorldToGridCoords(agent.Entity.ServerPosition);
			NavMeshGridCell cell = this.NavMeshGrid[vector2i];
			if (cell == null)
			{
				string text = "cell";
				cell = new GameObject(text).AddComponent<NavMeshGridCell>();
				cell.Setup(vector2i, this.NavMeshGrid.GridToWorldCoords(vector2i) + Vector3.up * agent.Entity.ServerPosition.y, new Vector3((float)AiManager.nav_grid_cell_width, (float)AiManager.nav_grid_cell_height, (float)AiManager.nav_grid_cell_width), this.LayerMask, this.NavMeshCollectGeometry);
				cell.transform.SetParent(base.transform);
				this.NavMeshGrid[vector2i] = cell;
			}
			NavMeshGridCell.Layer layer = cell.GetLayer(agent.AgentTypeIndex);
			if (layer == null)
			{
				layer = cell.CreateLayer(agent.AgentTypeIndex, agent.GetNavAgent.areaMask);
				layer.IsAwaitingBake = true;
				this.bakeHeap.Add(new AiManager.BakeInfo
				{
					Cell = cell,
					BakeFunc = cell.CreateNavMeshLayerAsync(layer, this.LayerMask, delegate(NavMeshGridCell.Layer l)
					{
						this.DecreaseNumCellsCurrentlyBaking();
						this.OnNavMeshBuildDoneCallback(l, agent, cell, onAsyncDoneCallback);
					})
				});
				return AiManager.NavMeshAddResult.Async;
			}
			if (this.IsNavmeshBuilding(layer))
			{
				base.StartCoroutine(cell.WaitForBuildToFinish(agent.AgentTypeIndex, delegate(NavMeshGridCell.Layer l)
				{
					this.OnNavMeshBuildDoneCallback(l, agent, cell, onAsyncDoneCallback);
				}));
				return AiManager.NavMeshAddResult.Async;
			}
			return AiManager.NavMeshAddResult.True;
		}

		// Token: 0x06000DE0 RID: 3552 RVA: 0x0005723C File Offset: 0x0005543C
		internal AiManager.NavMeshAddResult NavMeshAddAtCoord(global::IAIAgent agent, Vector2i coord, float y, int agentTypeIndex)
		{
			if (this.NavMeshGrid == null || this.NavMeshGrid.Cells == null || this.NavMeshGrid.Cells.Length == 0 || coord.x < 0 || coord.y < 0 || coord.x >= this.NavMeshGrid.CellCount || coord.y >= this.NavMeshGrid.CellCount)
			{
				return AiManager.NavMeshAddResult.False;
			}
			NavMeshGridCell navMeshGridCell = this.NavMeshGrid[coord];
			if (navMeshGridCell == null)
			{
				string text = "cell";
				navMeshGridCell = new GameObject(text).AddComponent<NavMeshGridCell>();
				navMeshGridCell.Setup(coord, this.NavMeshGrid.GridToWorldCoords(coord) + Vector3.up * y, new Vector3((float)AiManager.nav_grid_cell_width, (float)AiManager.nav_grid_cell_height, (float)AiManager.nav_grid_cell_width), this.LayerMask, this.NavMeshCollectGeometry);
				navMeshGridCell.transform.SetParent(base.transform);
				this.NavMeshGrid[coord] = navMeshGridCell;
			}
			NavMeshGridCell.Layer layer = navMeshGridCell.GetLayer(agentTypeIndex);
			if (layer == null)
			{
				layer = navMeshGridCell.CreateLayer(agentTypeIndex, agent.GetNavAgent.areaMask);
				layer.IsAwaitingBake = true;
				this.bakeHeap.Add(new AiManager.BakeInfo
				{
					Cell = navMeshGridCell,
					BakeFunc = navMeshGridCell.CreateNavMeshLayerAsync(layer, this.LayerMask, new Action<NavMeshGridCell.Layer>(this.DecreaseNumCellsCurrentlyBaking))
				});
				return AiManager.NavMeshAddResult.Async;
			}
			if (this.IsNavmeshBuilding(layer))
			{
				return AiManager.NavMeshAddResult.Async;
			}
			return AiManager.NavMeshAddResult.True;
		}

		// Token: 0x06000DE1 RID: 3553 RVA: 0x000573C8 File Offset: 0x000555C8
		private void OnNavMeshBuildDoneCallback(NavMeshGridCell.Layer layer, global::IAIAgent agent, NavMeshGridCell cell, Action<global::IAIAgent> onAsyncDoneCallback)
		{
			if (agent.Entity == null || agent.Entity.IsDestroyed)
			{
				if (onAsyncDoneCallback != null)
				{
					onAsyncDoneCallback(null);
				}
				return;
			}
			if (onAsyncDoneCallback != null)
			{
				onAsyncDoneCallback(agent);
			}
		}

		// Token: 0x06000DE2 RID: 3554 RVA: 0x00057418 File Offset: 0x00055618
		internal void UpdateNavMeshGridCell()
		{
			this.TryBake();
			this.TryGenerateLinks();
			this.UpdateCellTimeouts();
			this.UpdateCellPlayerTimestamps();
			this.TryQueueCellRebakes();
		}

		// Token: 0x06000DE3 RID: 3555 RVA: 0x00057438 File Offset: 0x00055638
		private void TryBake()
		{
			while (this.bakeHeap.count > 0 && this.NumCellsCurrentlyBaking < AiManager.nav_grid_max_bakes_per_frame)
			{
				AiManager.BakeInfo bakeInfo = this.bakeHeap.Remove();
				this.NumCellsCurrentlyBaking++;
				base.StartCoroutine(bakeInfo.BakeFunc);
			}
		}

		// Token: 0x06000DE4 RID: 3556 RVA: 0x00057494 File Offset: 0x00055694
		private void TryGenerateLinks()
		{
			if (AiManager.nav_grid_links_enabled && this.NumLinksCurrentlyGenerating < AiManager.nav_grid_max_links_generated_per_frame)
			{
				foreach (global::IAIAgent iaiagent in this.activeAgents)
				{
					if (iaiagent.Entity == null)
					{
						this.RemoveActiveAgency(iaiagent);
					}
					else
					{
						Vector2i coord = this.GetCoord(iaiagent.Entity.ServerPosition);
						NavMeshGridCell navMeshGridCell = this.NavMeshGrid[coord];
						if (this.NumLinksCurrentlyGenerating < AiManager.nav_grid_max_links_generated_per_frame && navMeshGridCell != null && !navMeshGridCell.Timeout(this.CellTimeoutThreshold) && navMeshGridCell.IsAtBorder(iaiagent.Entity.ServerPosition, (float)AiManager.nav_grid_cell_width * 0.05f, true))
						{
							this.NumLinksCurrentlyGenerating++;
							base.StartCoroutine(this.LinkGeneration(iaiagent, navMeshGridCell));
						}
						else
						{
							this.RemoveAgentLinkReference(iaiagent, false);
						}
					}
				}
			}
		}

		// Token: 0x06000DE5 RID: 3557 RVA: 0x000575BC File Offset: 0x000557BC
		private void UpdateCellTimeouts()
		{
			if (AiManager.ai_dormant)
			{
				for (int i = 0; i < this.NavMeshGrid.Cells.Length; i++)
				{
					NavMeshGridCell navMeshGridCell = this.NavMeshGrid.Cells[i];
					if (navMeshGridCell != null && navMeshGridCell.Timeout(this.CellTimeoutThreshold))
					{
						Vector2i coord = this.NavMeshGrid.WorldToGridCoords(navMeshGridCell.Bounds.center);
						this.ForceAgentsDormant(coord);
						if (AiManager.nav_grid_kill_dormant_cells)
						{
							navMeshGridCell.Kill();
							this.NavMeshGrid.Cells[i] = null;
						}
					}
				}
			}
		}

		// Token: 0x06000DE6 RID: 3558 RVA: 0x00057658 File Offset: 0x00055858
		private void UpdateCellPlayerTimestamps()
		{
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				if (!(basePlayer is global::IAIAgent))
				{
					Vector2i vector2i = this.NavMeshGrid.WorldToGridCoords(basePlayer.ServerPosition);
					foreach (Vector2i vector2i2 in AiManager.Directions)
					{
						Vector2i vector2i3 = vector2i + vector2i2;
						if (vector2i3.x >= 0 && vector2i3.y >= 0 && vector2i3.x < this.NavMeshGrid.CellCount && vector2i3.y < this.NavMeshGrid.CellCount)
						{
							NavMeshGridCell navMeshGridCell = this.NavMeshGrid[vector2i3];
							if (navMeshGridCell != null)
							{
								navMeshGridCell.KeepAlive();
							}
						}
					}
				}
			}
		}

		// Token: 0x06000DE7 RID: 3559 RVA: 0x00057778 File Offset: 0x00055978
		private void TryQueueCellRebakes()
		{
			if (this.bakeHeap.count == 0 && AiManager.nav_grid_rebake_cells_enabled)
			{
				HashSet<Vector2i> hashSet = Pool.Get<HashSet<Vector2i>>();
				foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
				{
					if (!(basePlayer == null) && basePlayer.IsConnected && !(basePlayer is global::IAIAgent))
					{
						Vector2i vector2i = this.NavMeshGrid.WorldToGridCoords(basePlayer.ServerPosition);
						foreach (Vector2i vector2i2 in AiManager.Directions)
						{
							Vector2i vector2i3 = vector2i + vector2i2;
							if (vector2i3.x >= 0 && vector2i3.y >= 0 && vector2i3.x < this.NavMeshGrid.CellCount && vector2i3.y < this.NavMeshGrid.CellCount && !hashSet.Contains(vector2i3))
							{
								NavMeshGridCell navMeshGridCell = this.NavMeshGrid[vector2i3];
								if (navMeshGridCell != null && navMeshGridCell.Layers.Count > 0 && !this.IsAnyNavmeshBuilding(vector2i3) && Time.realtimeSinceStartup - navMeshGridCell.LastBakeTime >= AiManager.nav_grid_rebake_cooldown)
								{
									hashSet.Add(vector2i3);
									navMeshGridCell.IsAwaitingBake = true;
									this.bakeHeap.Add(new AiManager.BakeInfo
									{
										Cell = navMeshGridCell,
										BakeFunc = navMeshGridCell.RebakeCellAsync(new Action(this.DecreaseNumCellsCurrentlyBaking))
									});
								}
							}
						}
					}
				}
				hashSet.Clear();
				Pool.Free<HashSet<Vector2i>>(ref hashSet);
			}
		}

		// Token: 0x06000DE8 RID: 3560 RVA: 0x00057970 File Offset: 0x00055B70
		private void DecreaseNumCellsCurrentlyBaking(NavMeshGridCell.Layer layer)
		{
			this.DecreaseNumCellsCurrentlyBaking();
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x00057978 File Offset: 0x00055B78
		private void DecreaseNumCellsCurrentlyBaking()
		{
			this.NumCellsCurrentlyBaking--;
			if (this.NumCellsCurrentlyBaking < 0)
			{
				this.NumCellsCurrentlyBaking = 0;
			}
		}

		// Token: 0x06000DEA RID: 3562 RVA: 0x0005799C File Offset: 0x00055B9C
		private IEnumerator LinkGeneration(global::IAIAgent agent, NavMeshGridCell cell)
		{
			if (agent != null && agent.Entity != null && cell != null && cell.IsAtBorder(agent.Entity.ServerPosition, (float)AiManager.nav_grid_cell_width * 0.05f, false))
			{
				NavMeshGridCell.NavMeshLinkInfo link = cell.GetOrCreateLink(agent.AgentTypeIndex, agent.Entity.ServerPosition, (float)AiManager.nav_grid_cell_width * 0.1f);
				if (link != null)
				{
					NavMeshGridCell.NavMeshLinkInfo navMeshLinkInfo;
					if (this.AgentLinks.TryGetValue(agent.Entity.GetInstanceID(), out navMeshLinkInfo) && navMeshLinkInfo != null && navMeshLinkInfo.Link.GetInstanceID() != link.Link.GetInstanceID())
					{
						this.RemoveAgentLinkReference(agent, navMeshLinkInfo, false);
					}
					link.References++;
					this.AgentLinks[agent.Entity.GetInstanceID()] = link;
				}
				else
				{
					this.RemoveAgentLinkReference(agent, false);
				}
				yield return null;
			}
			this.NumLinksCurrentlyGenerating--;
			if (this.NumLinksCurrentlyGenerating < 0)
			{
				this.NumLinksCurrentlyGenerating = 0;
			}
			yield break;
		}

		// Token: 0x06000DEB RID: 3563 RVA: 0x000579C8 File Offset: 0x00055BC8
		public bool HasReadyNeighbour(Vector2i coord, Vector2i direction)
		{
			Vector2i vector2i = coord + direction;
			if (vector2i.x < 0 || vector2i.y < 0 || vector2i.x >= SingletonComponent<AiManager>.Instance.NavMeshGrid.CellCount || vector2i.y >= SingletonComponent<AiManager>.Instance.NavMeshGrid.CellCount)
			{
				return false;
			}
			NavMeshGridCell navMeshGridCell = this.NavMeshGrid[vector2i];
			return !(navMeshGridCell == null) && !this.IsAnyNavmeshBuilding(vector2i) && navMeshGridCell.Layers.Count > 0;
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x00057A68 File Offset: 0x00055C68
		private void RemoveAgentLinkReference(global::IAIAgent agent, bool removeAgentFromCache = false)
		{
			if (this.AgentLinks == null || agent.Entity == null)
			{
				return;
			}
			NavMeshGridCell.NavMeshLinkInfo info;
			if (this.AgentLinks.TryGetValue(agent.Entity.GetInstanceID(), out info))
			{
				this.RemoveAgentLinkReference(agent, info, removeAgentFromCache);
			}
		}

		// Token: 0x06000DED RID: 3565 RVA: 0x00057AB8 File Offset: 0x00055CB8
		private void RemoveAgentLinkReference(global::IAIAgent agent, NavMeshGridCell.NavMeshLinkInfo info, bool removeAgentFromCache = false)
		{
			if (info != null)
			{
				info.References--;
				if (info.References == 0 && (info.Cell == null || !info.Cell.RemoveLink(info)))
				{
					info.Link.transform.SetParent(null);
					this.LinkPool.Push(info.Link.gameObject);
				}
			}
			if (this.AgentLinks != null && agent.Entity != null)
			{
				if (removeAgentFromCache)
				{
					this.AgentLinks.Remove(agent.Entity.GetInstanceID());
				}
				else
				{
					this.AgentLinks[agent.Entity.GetInstanceID()] = null;
				}
			}
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x00057B84 File Offset: 0x00055D84
		internal bool HasActiveAgents(NavMeshGridCell cell)
		{
			if (cell.LayerCount == 0)
			{
				return false;
			}
			foreach (global::IAIAgent iaiagent in this.activeAgents)
			{
				if (!(iaiagent.Entity == null))
				{
					if (cell.Bounds.Contains(iaiagent.Entity.ServerPosition))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x00057C24 File Offset: 0x00055E24
		public Vector2i GetCoord(Vector3 position)
		{
			return this.NavMeshGrid.WorldToGridCoords(position);
		}

		// Token: 0x06000DF0 RID: 3568 RVA: 0x00057C40 File Offset: 0x00055E40
		public bool IsAnyNavmeshBuilding(Vector3 position)
		{
			return this.IsAnyNavmeshBuilding(this.GetCoord(position));
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x00057C50 File Offset: 0x00055E50
		public bool IsAnyNavmeshBuilding(Vector2i coord)
		{
			NavMeshGridCell navMeshGridCell = this.NavMeshGrid[coord];
			if (navMeshGridCell != null)
			{
				foreach (NavMeshGridCell.Layer layer in navMeshGridCell.Layers)
				{
					if (layer != null)
					{
						return layer.IsAwaitingBake || layer.IsBuilding || (layer.BuildingOperation != null && !layer.BuildingOperation.isDone);
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000DF2 RID: 3570 RVA: 0x00057D04 File Offset: 0x00055F04
		public bool IsNavmeshBuilding(int agentTypeIndex, Vector3 position)
		{
			return this.IsNavmeshBuilding(agentTypeIndex, this.GetCoord(position));
		}

		// Token: 0x06000DF3 RID: 3571 RVA: 0x00057D14 File Offset: 0x00055F14
		public bool IsNavmeshBuilding(int agentTypeIndex, Vector2i coord)
		{
			NavMeshGridCell navMeshGridCell = this.NavMeshGrid[coord];
			if (navMeshGridCell != null)
			{
				NavMeshGridCell.Layer layer = navMeshGridCell.GetLayer(agentTypeIndex);
				if (layer != null)
				{
					return layer.IsAwaitingBake || layer.IsBuilding || (layer.BuildingOperation != null && !layer.BuildingOperation.isDone);
				}
			}
			return false;
		}

		// Token: 0x06000DF4 RID: 3572 RVA: 0x00057D80 File Offset: 0x00055F80
		public bool IsNavmeshBuilding(NavMeshGridCell.Layer layer)
		{
			return layer.IsAwaitingBake || layer.IsBuilding || (layer.BuildingOperation != null && !layer.BuildingOperation.isDone);
		}

		// Token: 0x06000DF5 RID: 3573 RVA: 0x00057DB8 File Offset: 0x00055FB8
		public bool IsCoordEqualOrAdjacentToOther(Vector2i coord, Vector2i other)
		{
			for (int i = 0; i < AiManager.Directions.Length; i++)
			{
				if (coord.x + AiManager.Directions[i].x == other.x && coord.y + AiManager.Directions[i].y == other.y)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x040007B6 RID: 1974
		private readonly HashSet<global::IAIAgent> activeAgents = new HashSet<global::IAIAgent>();

		// Token: 0x040007B7 RID: 1975
		private readonly List<global::IAIAgent> dormantAgents = new List<global::IAIAgent>();

		// Token: 0x040007B8 RID: 1976
		private readonly HashSet<global::IAIAgent> pendingAddToActive = new HashSet<global::IAIAgent>();

		// Token: 0x040007B9 RID: 1977
		private readonly HashSet<global::IAIAgent> pendingAddToDormant = new HashSet<global::IAIAgent>();

		// Token: 0x040007BA RID: 1978
		private readonly HashSet<global::IAIAgent> pendingRemoveFromActive = new HashSet<global::IAIAgent>();

		// Token: 0x040007BB RID: 1979
		private readonly HashSet<global::IAIAgent> pendingRemoveFromDormant = new HashSet<global::IAIAgent>();

		// Token: 0x040007BC RID: 1980
		private int lastWakeUpDormantIndex;

		// Token: 0x040007BD RID: 1981
		[Header("Cover System")]
		[SerializeField]
		public bool UseCover = true;

		// Token: 0x040007BE RID: 1982
		public float CoverPointVolumeCellSize = 20f;

		// Token: 0x040007BF RID: 1983
		public float CoverPointVolumeCellHeight = 8f;

		// Token: 0x040007C0 RID: 1984
		public float CoverPointRayLength = 1f;

		// Token: 0x040007C1 RID: 1985
		public CoverPointVolume cpvPrefab;

		// Token: 0x040007C2 RID: 1986
		[SerializeField]
		public LayerMask DynamicCoverPointVolumeLayerMask;

		// Token: 0x040007C3 RID: 1987
		private WorldSpaceGrid<CoverPointVolume> coverPointVolumeGrid;

		// Token: 0x040007C4 RID: 1988
		[ServerVar(Help = "If true we'll wait for the navmesh to generate before completely starting the server. This might cause your server to hitch and lag as it generates in the background.")]
		public static bool nav_wait = true;

		// Token: 0x040007C5 RID: 1989
		[ServerVar(Help = "If set to true the navmesh won't generate.. which means Ai that uses the navmesh won't be able to move")]
		public static bool nav_disable = false;

		// Token: 0x040007C6 RID: 1990
		[ServerVar(Help = "If set to true the ai manager will control a navmesh grid, only building navmesh patches in those areas where it's needed and for those agent types. If this is false, we bake a single huge navmesh for the entire island, and can only support a single agent type.")]
		public static bool nav_grid = false;

		// Token: 0x040007C7 RID: 1991
		[ServerVar(Help = "If ai_dormant is true, any npc outside the range of players will render itself dormant and take up less resources, but wildlife won't simulate as well.")]
		public static bool ai_dormant = true;

		// Token: 0x040007C8 RID: 1992
		[ServerVar(Help = "If nav_grid_kill_dormant_cells is true, when a navigation grid cell is out of range from active players for a certain time, we unload it to save memory (but comes with extra performance hit since we have to regenerate that navmesh then next time it's activated again).")]
		public static bool nav_grid_kill_dormant_cells = false;

		// Token: 0x040007C9 RID: 1993
		[ServerVar(Help = "nav_grid_max_bakes_per_frame adjust how many rebakes of navmesh grid cells we will start each frame. The higher the number, the heavier performance impact.")]
		public static int nav_grid_max_bakes_per_frame = 1;

		// Token: 0x040007CA RID: 1994
		[ServerVar(Help = "nav_grid_max_links_generated_per_frame adjust how many navmesh links can generate within a frame. The higher the number, the heavier performance impact.")]
		public static int nav_grid_max_links_generated_per_frame = 4;

		// Token: 0x040007CB RID: 1995
		[ServerVar(Help = "If nav_grid_links_enabled is true, agents can walk between navmesh grid cells, otherwise they can only walk on the navmesh grid cell they spawn (faster pathfinding).")]
		public static bool nav_grid_links_enabled = true;

		// Token: 0x040007CC RID: 1996
		[ServerVar(Help = "If nav_grid_rebake_cells_enabled is true, we rebake cells in player areas to adapt to construction and destruction.")]
		public static bool nav_grid_rebake_cells_enabled = true;

		// Token: 0x040007CD RID: 1997
		[ServerVar(Help = "If nav_grid_agents_expand_domain_enabled is true, agents add new cells for baking as they walk around the world, as needed.")]
		public static bool nav_grid_agents_expand_domain_enabled = true;

		// Token: 0x040007CE RID: 1998
		[ServerVar(Help = "nav_grid_rebake_cooldown defines how long a navmesh grid cell must wait between rebakes. Higher number means a cell will rebake less often.")]
		public static float nav_grid_rebake_cooldown = 30f;

		// Token: 0x040007CF RID: 1999
		[ServerVar(Help = "nav_grid_cell_width adjust how large each cell in the navmesh grid is. Larger cells take longer to bake, but we need fewer of them to cover the map.")]
		public static int nav_grid_cell_width = 80;

		// Token: 0x040007D0 RID: 2000
		[ServerVar(Help = "nav_grid_cell_height adjust how much vertical difference within a cell we can cope with when generating navmesh.")]
		public static int nav_grid_cell_height = 100;

		// Token: 0x040007D1 RID: 2001
		[ServerVar(Help = "The maximum amount of nodes processed each frame in the asynchronous pathfinding process. Increasing this value will cause the paths to be processed faster, but can cause some hiccups in frame rate. Default value is 100, a good range for tuning is between 50 and 500.")]
		public static int pathfindingIterationsPerFrame = 100;

		// Token: 0x040007D2 RID: 2002
		[ServerVar(Help = "If an agent is beyond this distance to a player, it's flagged for becoming dormant.")]
		public static float ai_to_player_distance_wakeup_range = 160f;

		// Token: 0x040007D3 RID: 2003
		[ServerVar(Help = "nav_obstacles_carve_state defines which obstacles can carve the terrain. 0 - No carving, 1 - Only player construction carves, 2 - All obstacles carve.")]
		public static int nav_obstacles_carve_state = 2;

		// Token: 0x040007D4 RID: 2004
		[ServerVar(Help = "ai_dormant_max_wakeup_per_tick defines the maximum number of dormant agents we will wake up in a single tick. (default: 20)")]
		public static int ai_dormant_max_wakeup_per_tick = 20;

		// Token: 0x040007D5 RID: 2005
		private readonly global::BasePlayer[] playerVicinityQuery = new global::BasePlayer[1];

		// Token: 0x040007D6 RID: 2006
		private readonly Func<global::BasePlayer, bool> filter;

		// Token: 0x040007D7 RID: 2007
		[Header("Intensity System")]
		[SerializeField]
		public bool UseIntensity;

		// Token: 0x040007D8 RID: 2008
		public float IntensityCellSize;

		// Token: 0x040007D9 RID: 2009
		public AnimationCurve HitResponse;

		// Token: 0x040007DA RID: 2010
		public float hitResponseOutputMultiplier;

		// Token: 0x040007DB RID: 2011
		public AnimationCurve HurtResponse;

		// Token: 0x040007DC RID: 2012
		public float maxHurtResponseInput;

		// Token: 0x040007DD RID: 2013
		public float hurtResponseOutputMultiplier;

		// Token: 0x040007DE RID: 2014
		public float OnDeathIntensityBonus;

		// Token: 0x040007DF RID: 2015
		public float IntensityDecayRate;

		// Token: 0x040007E0 RID: 2016
		public float NoEngagementToDecayTime;

		// Token: 0x040007E1 RID: 2017
		public float DebugIntensityHeightMultiplier;

		// Token: 0x040007E2 RID: 2018
		public Color DebugMinColor;

		// Token: 0x040007E3 RID: 2019
		public Color DebugMaxColor;

		// Token: 0x040007E4 RID: 2020
		private WorldSpaceGrid<AiManager.EngagementInfo> intensityGrid;

		// Token: 0x040007E5 RID: 2021
		[Header("NavMesh Grid")]
		[SerializeField]
		public bool UseNavMesh;

		// Token: 0x040007E6 RID: 2022
		public LayerMask LayerMask;

		// Token: 0x040007E7 RID: 2023
		public NavMeshCollectGeometry NavMeshCollectGeometry;

		// Token: 0x040007E8 RID: 2024
		public float CellTimeoutThreshold;

		// Token: 0x040007E9 RID: 2025
		public global::GameObjectRef NavMeshLinkPrefab;

		// Token: 0x040007EA RID: 2026
		internal WorldSpaceGrid<NavMeshGridCell> NavMeshGrid;

		// Token: 0x040007EB RID: 2027
		private Dictionary<int, NavMeshGridCell.NavMeshLinkInfo> AgentLinks;

		// Token: 0x040007EC RID: 2028
		internal global::PrefabPool LinkPool;

		// Token: 0x040007ED RID: 2029
		internal int NumCellsCurrentlyBaking;

		// Token: 0x040007EE RID: 2030
		internal int NumLinksCurrentlyGenerating;

		// Token: 0x040007EF RID: 2031
		private BinaryHeap<AiManager.BakeInfo> bakeHeap;

		// Token: 0x040007F0 RID: 2032
		public static Vector2i[] Directions = new Vector2i[]
		{
			Vector2i.zero,
			Vector2i.forward,
			Vector2i.right,
			Vector2i.back,
			Vector2i.left,
			Vector2i.forward + Vector2i.right,
			Vector2i.back + Vector2i.right,
			Vector2i.forward + Vector2i.left,
			Vector2i.back + Vector2i.left
		};

		// Token: 0x040007F1 RID: 2033
		[CompilerGenerated]
		private static Func<global::BasePlayer, bool> <>f__mg$cache0;

		// Token: 0x0200019E RID: 414
		private class EngagementInfo : Pool.IPooled
		{
			// Token: 0x06000DF8 RID: 3576 RVA: 0x00057F7C File Offset: 0x0005617C
			public void EnterPool()
			{
				this.LastEngagementTime = 0f;
				this.Intensity = 0f;
			}

			// Token: 0x06000DF9 RID: 3577 RVA: 0x00057F94 File Offset: 0x00056194
			public void LeavePool()
			{
			}

			// Token: 0x06000DFA RID: 3578 RVA: 0x00057F98 File Offset: 0x00056198
			public void KeepAlive()
			{
				this.TimeLastSeenByPlayer = Time.realtimeSinceStartup;
			}

			// Token: 0x06000DFB RID: 3579 RVA: 0x00057FA8 File Offset: 0x000561A8
			public bool Timeout(float timeoutThreshold)
			{
				return Time.realtimeSinceStartup - this.TimeLastSeenByPlayer > timeoutThreshold;
			}

			// Token: 0x040007F2 RID: 2034
			internal Vector2i Coord;

			// Token: 0x040007F3 RID: 2035
			internal float LastEngagementTime;

			// Token: 0x040007F4 RID: 2036
			internal float Intensity;

			// Token: 0x040007F5 RID: 2037
			internal float TimeLastSeenByPlayer;
		}

		// Token: 0x0200019F RID: 415
		private struct BakeInfo
		{
			// Token: 0x040007F6 RID: 2038
			public IEnumerator BakeFunc;

			// Token: 0x040007F7 RID: 2039
			public NavMeshGridCell Cell;

			// Token: 0x020001A0 RID: 416
			public class Comparer : IComparer<AiManager.BakeInfo>
			{
				// Token: 0x06000DFD RID: 3581 RVA: 0x00057FC4 File Offset: 0x000561C4
				public int Compare(AiManager.BakeInfo a, AiManager.BakeInfo b)
				{
					if (a.Cell == null && b.Cell == null)
					{
						return 0;
					}
					if (a.Cell == null)
					{
						return -1;
					}
					if (b.Cell == null)
					{
						return 1;
					}
					int distanceToNearestPlayer = this.GetDistanceToNearestPlayer(a.Cell.Coord);
					if (distanceToNearestPlayer == 0)
					{
						return 1;
					}
					int distanceToNearestPlayer2 = this.GetDistanceToNearestPlayer(b.Cell.Coord);
					if (distanceToNearestPlayer2 == 0)
					{
						return -1;
					}
					if (distanceToNearestPlayer < distanceToNearestPlayer2)
					{
						return 1;
					}
					if (distanceToNearestPlayer > distanceToNearestPlayer2)
					{
						return -1;
					}
					return 0;
				}

				// Token: 0x06000DFE RID: 3582 RVA: 0x00058068 File Offset: 0x00056268
				private int GetDistanceToNearestPlayer(Vector2i coord)
				{
					int num = int.MaxValue;
					foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
					{
						if (!(basePlayer == null) && basePlayer.IsConnected)
						{
							Vector2i coord2 = SingletonComponent<AiManager>.Instance.GetCoord(basePlayer.ServerPosition);
							for (int i = 0; i < AiManager.Directions.Length; i++)
							{
								Vector2i vector2i = coord2 + AiManager.Directions[i];
								if (coord.x == vector2i.x && coord.y == vector2i.y)
								{
									return 0;
								}
							}
							Vector2i a = coord - coord2;
							int num2 = this.MagnitudeSquared(a);
							if (num2 < num)
							{
								num = num2;
							}
						}
					}
					return num;
				}

				// Token: 0x06000DFF RID: 3583 RVA: 0x00058174 File Offset: 0x00056374
				private int MagnitudeSquared(Vector2i a)
				{
					return this.Dot(a, a);
				}

				// Token: 0x06000E00 RID: 3584 RVA: 0x00058180 File Offset: 0x00056380
				private int Dot(Vector2i a, Vector2i b)
				{
					return a.x * b.x + a.y * b.y;
				}
			}
		}

		// Token: 0x020001A1 RID: 417
		private struct LinkInfo
		{
			// Token: 0x040007F8 RID: 2040
			public IEnumerator LinkGenAction;

			// Token: 0x040007F9 RID: 2041
			public global::IAIAgent Agent;

			// Token: 0x040007FA RID: 2042
			public NavMeshGridCell Cell;
		}

		// Token: 0x020001A2 RID: 418
		internal enum NavMeshAddResult
		{
			// Token: 0x040007FC RID: 2044
			Async,
			// Token: 0x040007FD RID: 2045
			True,
			// Token: 0x040007FE RID: 2046
			False
		}
	}
}
