﻿using System;

namespace Rust.Ai
{
	// Token: 0x0200010A RID: 266
	public sealed class StartAttack : BaseAction
	{
		// Token: 0x06000C5C RID: 3164 RVA: 0x000507CC File Offset: 0x0004E9CC
		public override void DoExecute(BaseContext c)
		{
			c.AIAgent.StartAttack();
		}
	}
}
