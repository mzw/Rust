﻿using System;

namespace Rust
{
	// Token: 0x02000479 RID: 1145
	public static class Defines
	{
		// Token: 0x040013A7 RID: 5031
		public static uint appID = 252490u;

		// Token: 0x040013A8 RID: 5032
		public const string resourceFolder = "assets/bundled";

		// Token: 0x0200047A RID: 1146
		public static class Connection
		{
			// Token: 0x040013A9 RID: 5033
			public const byte mode_steam = 228;
		}

		// Token: 0x0200047B RID: 1147
		public static class Tags
		{
			// Token: 0x040013AA RID: 5034
			public const string NotPlayerUsable = "Not Player Usable";

			// Token: 0x040013AB RID: 5035
			public const string MeshColliderBatch = "MeshColliderBatch";
		}
	}
}
