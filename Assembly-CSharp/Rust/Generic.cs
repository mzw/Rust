﻿using System;
using UnityEngine.SceneManagement;

namespace Rust
{
	// Token: 0x0200047C RID: 1148
	public static class Generic
	{
		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x060018F0 RID: 6384 RVA: 0x0008C53C File Offset: 0x0008A73C
		public static Scene BatchingScene
		{
			get
			{
				if (!Generic._batchingScene.IsValid())
				{
					Generic._batchingScene = SceneManager.CreateScene("Batching");
				}
				return Generic._batchingScene;
			}
		}

		// Token: 0x040013AC RID: 5036
		private static Scene _batchingScene;
	}
}
