﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Rust.UI.Utility
{
	// Token: 0x02000683 RID: 1667
	[RequireComponent(typeof(Toggle))]
	internal class ForceWeather : MonoBehaviour
	{
		// Token: 0x060020E8 RID: 8424 RVA: 0x000BAC98 File Offset: 0x000B8E98
		public void OnEnable()
		{
			this.component = base.GetComponent<Toggle>();
		}

		// Token: 0x060020E9 RID: 8425 RVA: 0x000BACA8 File Offset: 0x000B8EA8
		public void Update()
		{
			if (SingletonComponent<global::Climate>.Instance == null)
			{
				return;
			}
			if (this.Rain)
			{
				SingletonComponent<global::Climate>.Instance.Overrides.Rain = Mathf.MoveTowards(SingletonComponent<global::Climate>.Instance.Overrides.Rain, (float)((!this.component.isOn) ? 0 : 1), Time.deltaTime / 2f);
			}
			if (this.Fog)
			{
				SingletonComponent<global::Climate>.Instance.Overrides.Fog = Mathf.MoveTowards(SingletonComponent<global::Climate>.Instance.Overrides.Fog, (float)((!this.component.isOn) ? 0 : 1), Time.deltaTime / 2f);
			}
			if (this.Wind)
			{
				SingletonComponent<global::Climate>.Instance.Overrides.Wind = Mathf.MoveTowards(SingletonComponent<global::Climate>.Instance.Overrides.Wind, (float)((!this.component.isOn) ? 0 : 1), Time.deltaTime / 2f);
			}
			if (this.Clouds)
			{
				SingletonComponent<global::Climate>.Instance.Overrides.Clouds = Mathf.MoveTowards(SingletonComponent<global::Climate>.Instance.Overrides.Clouds, (float)((!this.component.isOn) ? 0 : 1), Time.deltaTime / 2f);
			}
		}

		// Token: 0x04001C39 RID: 7225
		private Toggle component;

		// Token: 0x04001C3A RID: 7226
		public bool Rain;

		// Token: 0x04001C3B RID: 7227
		public bool Fog;

		// Token: 0x04001C3C RID: 7228
		public bool Wind;

		// Token: 0x04001C3D RID: 7229
		public bool Clouds;
	}
}
