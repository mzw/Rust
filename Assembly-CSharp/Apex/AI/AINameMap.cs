﻿using System;

namespace Apex.AI
{
	// Token: 0x020007D0 RID: 2000
	public static class AINameMap
	{
		// Token: 0x040020EF RID: 8431
		public static readonly Guid AnimalAction = new Guid("fbbca198-2370-403a-a9ff-46ebd266cc4c");

		// Token: 0x040020F0 RID: 8432
		public static readonly Guid AnimalMovement = new Guid("6dca0a5b-d417-4ee1-9780-86ef5721fa04");

		// Token: 0x040020F1 RID: 8433
		public static readonly Guid AnimalThink = new Guid("288b4d89-ac3d-4e3c-b927-b3193af53d9e");

		// Token: 0x040020F2 RID: 8434
		public static readonly Guid HTNAllShoot = new Guid("fd7e613e-149b-428f-ae72-1483cbe5b29c");

		// Token: 0x040020F3 RID: 8435
		public static readonly Guid HTNOneAttackFromCover = new Guid("a641deaf-34cc-4427-81d8-ec2ecd4e55bb");

		// Token: 0x040020F4 RID: 8436
		public static readonly Guid HTNOneIdle = new Guid("ed311b16-a8c6-46f1-9fe2-9badc411ae95");

		// Token: 0x040020F5 RID: 8437
		public static readonly Guid HTNOneKillPrimaryThreat = new Guid("d0494bf8-f36f-4854-b2b2-92b0e4665036");

		// Token: 0x040020F6 RID: 8438
		public static readonly Guid HTNOneReloadOrSwitchWeapon = new Guid("75bf2577-4f66-4cdc-89c2-c358816af226");

		// Token: 0x040020F7 RID: 8439
		public static readonly Guid HTNRootScientist = new Guid("ffc9820f-5a80-4c46-8ecf-d38d3d45be2f");

		// Token: 0x040020F8 RID: 8440
		public static readonly Guid MurdererAction = new Guid("6e8a0667-bed9-424a-9128-6f49e9196ea9");

		// Token: 0x040020F9 RID: 8441
		public static readonly Guid MurdererMove = new Guid("84177090-95b1-4434-a4b5-c480785487f4");

		// Token: 0x040020FA RID: 8442
		public static readonly Guid MurdererThink = new Guid("21161e69-f640-4c69-b9c8-0e061807ff5e");

		// Token: 0x040020FB RID: 8443
		public static readonly Guid NpcAnimalGeneric = new Guid("0ce6945b-b709-487d-b69d-047f13447107");

		// Token: 0x040020FC RID: 8444
		public static readonly Guid NpcHumanScientist = new Guid("157ef1da-b58a-4092-9267-4458c850fc48");

		// Token: 0x040020FD RID: 8445
		public static readonly Guid NpcHumanScientistMelee = new Guid("96f12289-c968-4d53-afc7-d64897dd85c6");

		// Token: 0x040020FE RID: 8446
		public static readonly Guid NpcHumanScientistTactical = new Guid("fba3b625-d850-4312-b328-ef9ae20f44bc");

		// Token: 0x040020FF RID: 8447
		public static readonly Guid NPCPlayerAction = new Guid("8d135d51-8591-4977-a0ea-34be0e175ab0");

		// Token: 0x04002100 RID: 8448
		public static readonly Guid NPCPlayerIdle = new Guid("d9c089f7-6067-42b6-bb7a-49c1d1687867");

		// Token: 0x04002101 RID: 8449
		public static readonly Guid NPCPlayerMove = new Guid("594ce0f7-e8f7-4fcc-bb7b-f5619fb6c36e");

		// Token: 0x04002102 RID: 8450
		public static readonly Guid NPCPlayerThink = new Guid("fc6aef4e-0128-4c29-8082-9f8098dade72");

		// Token: 0x04002103 RID: 8451
		public static readonly Guid ZombieAction = new Guid("0b91e09e-b490-4412-898f-62235e69aabe");
	}
}
