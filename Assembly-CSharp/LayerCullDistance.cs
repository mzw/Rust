﻿using System;
using UnityEngine;

// Token: 0x02000745 RID: 1861
public class LayerCullDistance : MonoBehaviour
{
	// Token: 0x060022E9 RID: 8937 RVA: 0x000C216C File Offset: 0x000C036C
	protected void OnEnable()
	{
		Camera component = base.GetComponent<Camera>();
		float[] layerCullDistances = component.layerCullDistances;
		layerCullDistances[LayerMask.NameToLayer(this.Layer)] = this.Distance;
		component.layerCullDistances = layerCullDistances;
	}

	// Token: 0x04001F53 RID: 8019
	public string Layer = "Default";

	// Token: 0x04001F54 RID: 8020
	public float Distance = 1000f;
}
