﻿using System;
using UnityEngine;

// Token: 0x020004B2 RID: 1202
public class OutlineObject : MonoBehaviour
{
	// Token: 0x04001494 RID: 5268
	public Mesh[] meshes;

	// Token: 0x04001495 RID: 5269
	public Transform[] meshTransforms;
}
