﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006F4 RID: 1780
public class PieOption : MonoBehaviour
{
	// Token: 0x060021CF RID: 8655 RVA: 0x000BDD3C File Offset: 0x000BBF3C
	public void UpdateOption(float startSlice, float sliceSize, float border, string optionTitle, float outerSize, float innerSize, float imageSize, Sprite sprite)
	{
		if (this.background == null)
		{
			return;
		}
		float num = this.background.rectTransform.rect.height * 0.5f;
		float num2 = num * (innerSize + (outerSize - innerSize) * 0.5f);
		float num3 = num * (outerSize - innerSize);
		this.background.startRadius = startSlice;
		this.background.endRadius = startSlice + sliceSize;
		this.background.border = border;
		this.background.outerSize = outerSize;
		this.background.innerSize = innerSize;
		this.background.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 0f);
		float num4 = startSlice + sliceSize * 0.5f;
		float num5 = Mathf.Sin(num4 * 0.0174532924f) * num2;
		float num6 = Mathf.Cos(num4 * 0.0174532924f) * num2;
		this.imageIcon.rectTransform.localPosition = new Vector3(num5, num6);
		this.imageIcon.rectTransform.sizeDelta = new Vector2(num3 * imageSize, num3 * imageSize);
		this.imageIcon.sprite = sprite;
	}

	// Token: 0x04001E4F RID: 7759
	public global::PieShape background;

	// Token: 0x04001E50 RID: 7760
	public Image imageIcon;
}
