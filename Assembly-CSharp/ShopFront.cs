﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000090 RID: 144
public class ShopFront : global::StorageContainer
{
	// Token: 0x0600095A RID: 2394 RVA: 0x0003F638 File Offset: 0x0003D838
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("ShopFront.OnRpcMessage", 0.1f))
		{
			if (rpc == 1913037343u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - AcceptClicked ");
				}
				using (TimeWarning.New("AcceptClicked", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("AcceptClicked", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.AcceptClicked(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in AcceptClicked");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 519046253u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - CancelClicked ");
				}
				using (TimeWarning.New("CancelClicked", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("CancelClicked", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.CancelClicked(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in CancelClicked");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x17000065 RID: 101
	// (get) Token: 0x0600095B RID: 2395 RVA: 0x0003F9A4 File Offset: 0x0003DBA4
	public global::ItemContainer vendorInventory
	{
		get
		{
			return this.inventory;
		}
	}

	// Token: 0x0600095C RID: 2396 RVA: 0x0003F9AC File Offset: 0x0003DBAC
	public bool TradeLocked()
	{
		return false;
	}

	// Token: 0x0600095D RID: 2397 RVA: 0x0003F9B0 File Offset: 0x0003DBB0
	public bool IsTradingPlayer(global::BasePlayer player)
	{
		return player != null && (this.IsPlayerCustomer(player) || this.IsPlayerVendor(player));
	}

	// Token: 0x0600095E RID: 2398 RVA: 0x0003F9D8 File Offset: 0x0003DBD8
	public bool IsPlayerCustomer(global::BasePlayer player)
	{
		return player == this.customerPlayer;
	}

	// Token: 0x0600095F RID: 2399 RVA: 0x0003F9E8 File Offset: 0x0003DBE8
	public bool IsPlayerVendor(global::BasePlayer player)
	{
		return player == this.vendorPlayer;
	}

	// Token: 0x06000960 RID: 2400 RVA: 0x0003F9F8 File Offset: 0x0003DBF8
	public bool PlayerInVendorPos(global::BasePlayer player)
	{
		return Vector3.Dot(base.transform.right, (player.GetEstimatedWorldPosition() - base.transform.position).normalized) <= -0.7f;
	}

	// Token: 0x06000961 RID: 2401 RVA: 0x0003FA40 File Offset: 0x0003DC40
	public bool PlayerInCustomerPos(global::BasePlayer player)
	{
		return Vector3.Dot(base.transform.right, (player.GetEstimatedWorldPosition() - base.transform.position).normalized) >= 0.7f;
	}

	// Token: 0x06000962 RID: 2402 RVA: 0x0003FA88 File Offset: 0x0003DC88
	public bool LootEligable(global::BasePlayer player)
	{
		return !(player == null) && ((this.PlayerInVendorPos(player) && this.vendorPlayer == null) || (this.PlayerInCustomerPos(player) && this.customerPlayer == null));
	}

	// Token: 0x06000963 RID: 2403 RVA: 0x0003FAE4 File Offset: 0x0003DCE4
	public void ResetTrade()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved3, false, false);
		this.vendorInventory.SetLocked(false);
		this.customerInventory.SetLocked(false);
		base.CancelInvoke(new Action(this.CompleteTrade));
	}

	// Token: 0x06000964 RID: 2404 RVA: 0x0003FB44 File Offset: 0x0003DD44
	public void CompleteTrade()
	{
		if (this.vendorPlayer != null && this.customerPlayer != null && base.HasFlag(global::BaseEntity.Flags.Reserved1) && base.HasFlag(global::BaseEntity.Flags.Reserved2))
		{
			if (Interface.CallHook("OnShopCompleteTrade", new object[]
			{
				this
			}) != null)
			{
				return;
			}
			for (int i = this.vendorInventory.capacity - 1; i >= 0; i--)
			{
				global::Item slot = this.vendorInventory.GetSlot(i);
				global::Item slot2 = this.customerInventory.GetSlot(i);
				if (this.customerPlayer && slot != null)
				{
					this.customerPlayer.GiveItem(slot, global::BaseEntity.GiveItemReason.Generic);
				}
				if (this.vendorPlayer && slot2 != null)
				{
					this.vendorPlayer.GiveItem(slot2, global::BaseEntity.GiveItemReason.Generic);
				}
			}
			global::Effect.server.Run(this.transactionCompleteEffect.resourcePath, this, 0u, new Vector3(0f, 1f, 0f), Vector3.zero, null, false);
		}
		this.ResetTrade();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000965 RID: 2405 RVA: 0x0003FC68 File Offset: 0x0003DE68
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void AcceptClicked(global::BaseEntity.RPCMessage msg)
	{
		if (!this.IsTradingPlayer(msg.player))
		{
			return;
		}
		if (this.vendorPlayer == null || this.customerPlayer == null)
		{
			return;
		}
		if (this.IsPlayerVendor(msg.player))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved1, true, false);
			this.vendorInventory.SetLocked(true);
		}
		else if (this.IsPlayerCustomer(msg.player))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved2, true, false);
			this.customerInventory.SetLocked(true);
		}
		if (base.HasFlag(global::BaseEntity.Flags.Reserved1) && base.HasFlag(global::BaseEntity.Flags.Reserved2))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved3, true, false);
			base.Invoke(new Action(this.CompleteTrade), 2f);
		}
	}

	// Token: 0x06000966 RID: 2406 RVA: 0x0003FD4C File Offset: 0x0003DF4C
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void CancelClicked(global::BaseEntity.RPCMessage msg)
	{
		if (!this.IsTradingPlayer(msg.player))
		{
			return;
		}
		if (this.vendorPlayer)
		{
		}
		if (this.customerPlayer)
		{
		}
		this.ResetTrade();
	}

	// Token: 0x06000967 RID: 2407 RVA: 0x0003FD88 File Offset: 0x0003DF88
	public override void PreServerLoad()
	{
		base.PreServerLoad();
	}

	// Token: 0x06000968 RID: 2408 RVA: 0x0003FD90 File Offset: 0x0003DF90
	public override void ServerInit()
	{
		base.ServerInit();
		global::ItemContainer vendorInventory = this.vendorInventory;
		vendorInventory.canAcceptItem = (Func<global::Item, int, bool>)Delegate.Combine(vendorInventory.canAcceptItem, new Func<global::Item, int, bool>(this.CanAcceptVendorItem));
		if (this.customerInventory == null)
		{
			this.customerInventory = new global::ItemContainer();
			this.customerInventory.allowedContents = ((this.allowedContents != (global::ItemContainer.ContentsType)0) ? this.allowedContents : global::ItemContainer.ContentsType.Generic);
			this.customerInventory.onlyAllowedItem = this.allowedItem;
			this.customerInventory.entityOwner = this;
			this.customerInventory.maxStackSize = this.maxStackSize;
			this.customerInventory.ServerInitialize(null, this.inventorySlots);
			this.customerInventory.GiveUID();
			this.customerInventory.onDirty += this.OnInventoryDirty;
			this.customerInventory.onItemAddedRemoved = new Action<global::Item, bool>(this.OnItemAddedOrRemoved);
			global::ItemContainer itemContainer = this.customerInventory;
			itemContainer.canAcceptItem = (Func<global::Item, int, bool>)Delegate.Combine(itemContainer.canAcceptItem, new Func<global::Item, int, bool>(this.CanAcceptCustomerItem));
			this.OnInventoryFirstCreated(this.customerInventory);
		}
	}

	// Token: 0x06000969 RID: 2409 RVA: 0x0003FEB0 File Offset: 0x0003E0B0
	public override void OnItemAddedOrRemoved(global::Item item, bool added)
	{
		base.OnItemAddedOrRemoved(item, added);
		this.ResetTrade();
	}

	// Token: 0x0600096A RID: 2410 RVA: 0x0003FEC0 File Offset: 0x0003E0C0
	private bool CanAcceptVendorItem(global::Item item, int targetSlot)
	{
		return (this.vendorPlayer != null && item.GetOwnerPlayer() == this.vendorPlayer) || this.vendorInventory.itemList.Contains(item) || item.parent == null;
	}

	// Token: 0x0600096B RID: 2411 RVA: 0x0003FF18 File Offset: 0x0003E118
	private bool CanAcceptCustomerItem(global::Item item, int targetSlot)
	{
		return (this.customerPlayer != null && item.GetOwnerPlayer() == this.customerPlayer) || this.customerInventory.itemList.Contains(item) || item.parent == null;
	}

	// Token: 0x0600096C RID: 2412 RVA: 0x0003FF70 File Offset: 0x0003E170
	public override bool CanMoveFrom(global::BasePlayer player, global::Item item)
	{
		if (this.TradeLocked())
		{
			return false;
		}
		if (this.IsTradingPlayer(player))
		{
			if (this.IsPlayerCustomer(player) && this.customerInventory.itemList.Contains(item) && !this.customerInventory.IsLocked())
			{
				return true;
			}
			if (this.IsPlayerVendor(player) && this.vendorInventory.itemList.Contains(item) && !this.vendorInventory.IsLocked())
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x0600096D RID: 2413 RVA: 0x00040000 File Offset: 0x0003E200
	public override bool CanOpenLootPanel(global::BasePlayer player, string panelName = "")
	{
		return base.CanOpenLootPanel(player, panelName) && this.LootEligable(player);
	}

	// Token: 0x0600096E RID: 2414 RVA: 0x0004001C File Offset: 0x0003E21C
	public void ReturnPlayerItems(global::BasePlayer player)
	{
		if (this.IsTradingPlayer(player))
		{
			global::ItemContainer itemContainer = null;
			if (this.IsPlayerVendor(player))
			{
				itemContainer = this.vendorInventory;
			}
			else if (this.IsPlayerCustomer(player))
			{
				itemContainer = this.customerInventory;
			}
			if (itemContainer != null)
			{
				for (int i = itemContainer.itemList.Count - 1; i >= 0; i--)
				{
					global::Item item = itemContainer.itemList[i];
					player.GiveItem(item, global::BaseEntity.GiveItemReason.Generic);
				}
			}
		}
	}

	// Token: 0x0600096F RID: 2415 RVA: 0x0004009C File Offset: 0x0003E29C
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		if (!this.IsTradingPlayer(player))
		{
			return;
		}
		this.ReturnPlayerItems(player);
		if (player == this.vendorPlayer)
		{
			this.vendorPlayer = null;
		}
		if (player == this.customerPlayer)
		{
			this.customerPlayer = null;
		}
		this.UpdatePlayers();
		this.ResetTrade();
		base.PlayerStoppedLooting(player);
	}

	// Token: 0x06000970 RID: 2416 RVA: 0x00040100 File Offset: 0x0003E300
	public override bool PlayerOpenLoot(global::BasePlayer player, string panelToOpen)
	{
		bool flag = base.PlayerOpenLoot(player, panelToOpen);
		if (flag)
		{
			player.inventory.loot.AddContainer(this.customerInventory);
			player.inventory.loot.SendImmediate();
		}
		if (this.PlayerInVendorPos(player) && this.vendorPlayer == null)
		{
			this.vendorPlayer = player;
		}
		else
		{
			if (!this.PlayerInCustomerPos(player) || !(this.customerPlayer == null))
			{
				return false;
			}
			this.customerPlayer = player;
		}
		this.ResetTrade();
		this.UpdatePlayers();
		return flag;
	}

	// Token: 0x06000971 RID: 2417 RVA: 0x000401A4 File Offset: 0x0003E3A4
	public void UpdatePlayers()
	{
		base.ClientRPC<uint, uint>(null, "CLIENT_ReceivePlayers", (!(this.vendorPlayer == null)) ? this.vendorPlayer.net.ID : 0u, (!(this.customerPlayer == null)) ? this.customerPlayer.net.ID : 0u);
	}

	// Token: 0x04000453 RID: 1107
	public global::BasePlayer vendorPlayer;

	// Token: 0x04000454 RID: 1108
	public global::BasePlayer customerPlayer;

	// Token: 0x04000455 RID: 1109
	public global::GameObjectRef transactionCompleteEffect;

	// Token: 0x04000456 RID: 1110
	public global::ItemContainer customerInventory;

	// Token: 0x02000091 RID: 145
	public static class ShopFrontFlags
	{
		// Token: 0x04000457 RID: 1111
		public const global::BaseEntity.Flags VendorAccepted = global::BaseEntity.Flags.Reserved1;

		// Token: 0x04000458 RID: 1112
		public const global::BaseEntity.Flags CustomerAccepted = global::BaseEntity.Flags.Reserved2;

		// Token: 0x04000459 RID: 1113
		public const global::BaseEntity.Flags Exchanging = global::BaseEntity.Flags.Reserved3;
	}
}
