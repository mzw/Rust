﻿using System;
using UnityEngine.UI;

// Token: 0x020006A9 RID: 1705
public class ExplosiveInformationPanel : global::ItemInformationPanel
{
	// Token: 0x04001CE1 RID: 7393
	public global::ItemTextValue explosiveDmgDisplay;

	// Token: 0x04001CE2 RID: 7394
	public global::ItemTextValue lethalDmgDisplay;

	// Token: 0x04001CE3 RID: 7395
	public global::ItemTextValue throwDistanceDisplay;

	// Token: 0x04001CE4 RID: 7396
	public global::ItemTextValue projectileDistanceDisplay;

	// Token: 0x04001CE5 RID: 7397
	public global::ItemTextValue fuseLengthDisplay;

	// Token: 0x04001CE6 RID: 7398
	public global::ItemTextValue blastRadiusDisplay;

	// Token: 0x04001CE7 RID: 7399
	public Text unreliableText;
}
