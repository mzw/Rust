﻿using System;
using UnityEngine;

// Token: 0x0200052C RID: 1324
[Serializable]
public class SpawnFilter
{
	// Token: 0x06001BF4 RID: 7156 RVA: 0x0009DA30 File Offset: 0x0009BC30
	public bool Test(Vector3 worldPos)
	{
		return this.GetFactor(worldPos) > 0.5f;
	}

	// Token: 0x06001BF5 RID: 7157 RVA: 0x0009DA40 File Offset: 0x0009BC40
	public bool Test(float normX, float normZ)
	{
		return this.GetFactor(normX, normZ) > 0.5f;
	}

	// Token: 0x06001BF6 RID: 7158 RVA: 0x0009DA54 File Offset: 0x0009BC54
	public float GetFactor(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetFactor(normX, normZ);
	}

	// Token: 0x06001BF7 RID: 7159 RVA: 0x0009DA84 File Offset: 0x0009BC84
	public float GetFactor(float normX, float normZ)
	{
		if (global::TerrainMeta.TopologyMap == null)
		{
			return 0f;
		}
		int splatType = (int)this.SplatType;
		int biomeType = (int)this.BiomeType;
		int topologyAny = (int)this.TopologyAny;
		int topologyAll = (int)this.TopologyAll;
		int topologyNot = (int)this.TopologyNot;
		if (topologyAny == 0)
		{
			Debug.LogError("Empty topology filter is invalid.");
		}
		else if (topologyAny != -1 || topologyAll != 0 || topologyNot != 0)
		{
			int topology = global::TerrainMeta.TopologyMap.GetTopology(normX, normZ);
			if (topologyAny != -1 && (topology & topologyAny) == 0)
			{
				return 0f;
			}
			if (topologyNot != 0 && (topology & topologyNot) != 0)
			{
				return 0f;
			}
			if (topologyAll != 0 && (topology & topologyAll) != topologyAll)
			{
				return 0f;
			}
		}
		if (biomeType == 0)
		{
			Debug.LogError("Empty biome filter is invalid.");
		}
		else if (biomeType != -1)
		{
			int biomeMaxType = global::TerrainMeta.BiomeMap.GetBiomeMaxType(normX, normZ, -1);
			if ((biomeMaxType & biomeType) == 0)
			{
				return 0f;
			}
		}
		if (splatType == 0)
		{
			Debug.LogError("Empty splat filter is invalid.");
		}
		else if (splatType != -1)
		{
			return global::TerrainMeta.SplatMap.GetSplat(normX, normZ, splatType);
		}
		return 1f;
	}

	// Token: 0x040016B7 RID: 5815
	[InspectorFlags]
	public global::TerrainSplat.Enum SplatType = (global::TerrainSplat.Enum)(-1);

	// Token: 0x040016B8 RID: 5816
	[InspectorFlags]
	public global::TerrainBiome.Enum BiomeType = (global::TerrainBiome.Enum)(-1);

	// Token: 0x040016B9 RID: 5817
	[InspectorFlags]
	public global::TerrainTopology.Enum TopologyAny = (global::TerrainTopology.Enum)(-1);

	// Token: 0x040016BA RID: 5818
	[InspectorFlags]
	public global::TerrainTopology.Enum TopologyAll;

	// Token: 0x040016BB RID: 5819
	[InspectorFlags]
	public global::TerrainTopology.Enum TopologyNot;
}
