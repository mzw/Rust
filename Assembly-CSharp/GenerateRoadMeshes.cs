﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005B6 RID: 1462
public class GenerateRoadMeshes : global::ProceduralComponent
{
	// Token: 0x06001E6D RID: 7789 RVA: 0x000AA408 File Offset: 0x000A8608
	public override void Process(uint seed)
	{
		List<global::PathList> roads = global::TerrainMeta.Path.Roads;
		foreach (global::PathList pathList in roads)
		{
			foreach (Mesh sharedMesh in pathList.CreateMesh())
			{
				GameObject gameObject = new GameObject("Road Mesh");
				MeshCollider meshCollider = gameObject.AddComponent<MeshCollider>();
				meshCollider.sharedMaterial = this.RoadPhysicMaterial;
				meshCollider.sharedMesh = sharedMesh;
				gameObject.AddComponent<global::AddToHeightMap>();
				gameObject.layer = 16;
				gameObject.SetHierarchyGroup(pathList.Name, true, false);
			}
		}
	}

	// Token: 0x17000219 RID: 537
	// (get) Token: 0x06001E6E RID: 7790 RVA: 0x000AA4F4 File Offset: 0x000A86F4
	public override bool RunOnCache
	{
		get
		{
			return true;
		}
	}

	// Token: 0x04001940 RID: 6464
	public Material RoadMaterial;

	// Token: 0x04001941 RID: 6465
	public PhysicMaterial RoadPhysicMaterial;
}
