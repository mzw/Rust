﻿using System;
using UnityEngine;

// Token: 0x020004FD RID: 1277
[Serializable]
public class ItemAmountRandom
{
	// Token: 0x06001B23 RID: 6947 RVA: 0x00097C2C File Offset: 0x00095E2C
	public int RandomAmount()
	{
		return Mathf.RoundToInt(this.amount.Evaluate(Random.Range(0f, 1f)));
	}

	// Token: 0x040015F1 RID: 5617
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition itemDef;

	// Token: 0x040015F2 RID: 5618
	public AnimationCurve amount = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 0f),
		new Keyframe(1f, 1f)
	});
}
