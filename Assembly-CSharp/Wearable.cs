﻿using System;
using UnityEngine;

// Token: 0x020004A4 RID: 1188
public class Wearable : MonoBehaviour, global::IItemSetup, global::IPrefabPreProcess
{
	// Token: 0x060019D0 RID: 6608 RVA: 0x000913A8 File Offset: 0x0008F5A8
	public void OnItemSetup(global::Item item)
	{
	}

	// Token: 0x060019D1 RID: 6609 RVA: 0x000913AC File Offset: 0x0008F5AC
	public virtual void PreProcess(global::IPrefabProcessor preProcess, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		foreach (LODGroup lodgroup in base.GetComponentsInChildren<LODGroup>(true))
		{
			lodgroup.SetLODs(global::Wearable.emptyLOD);
			preProcess.RemoveComponent(lodgroup);
		}
	}

	// Token: 0x060019D2 RID: 6610 RVA: 0x000913EC File Offset: 0x0008F5EC
	public void SetupRendererCache(global::IPrefabProcessor preProcess)
	{
	}

	// Token: 0x04001456 RID: 5206
	[InspectorFlags]
	public global::Wearable.RemoveSkin removeSkin;

	// Token: 0x04001457 RID: 5207
	[InspectorFlags]
	public global::Wearable.RemoveHair removeHair;

	// Token: 0x04001458 RID: 5208
	public global::Wearable.DeformHair deformHair;

	// Token: 0x04001459 RID: 5209
	public bool showCensorshipCube;

	// Token: 0x0400145A RID: 5210
	public bool showCensorshipCubeBreasts;

	// Token: 0x0400145B RID: 5211
	[InspectorFlags]
	public global::Wearable.OccupationSlots occupationUnder;

	// Token: 0x0400145C RID: 5212
	[InspectorFlags]
	public global::Wearable.OccupationSlots occupationOver;

	// Token: 0x0400145D RID: 5213
	public string followBone;

	// Token: 0x0400145E RID: 5214
	private static LOD[] emptyLOD = new LOD[1];

	// Token: 0x020004A5 RID: 1189
	[Flags]
	public enum RemoveSkin
	{
		// Token: 0x04001460 RID: 5216
		Torso = 1,
		// Token: 0x04001461 RID: 5217
		Feet = 2,
		// Token: 0x04001462 RID: 5218
		Hands = 4,
		// Token: 0x04001463 RID: 5219
		Legs = 8,
		// Token: 0x04001464 RID: 5220
		Head = 16
	}

	// Token: 0x020004A6 RID: 1190
	[Flags]
	public enum RemoveHair
	{
		// Token: 0x04001466 RID: 5222
		Head = 1,
		// Token: 0x04001467 RID: 5223
		Eyebrow = 2,
		// Token: 0x04001468 RID: 5224
		Facial = 4,
		// Token: 0x04001469 RID: 5225
		Armpit = 8,
		// Token: 0x0400146A RID: 5226
		Pubic = 16
	}

	// Token: 0x020004A7 RID: 1191
	[Flags]
	public enum DeformHair
	{
		// Token: 0x0400146C RID: 5228
		None = 0,
		// Token: 0x0400146D RID: 5229
		BaseballCap = 1,
		// Token: 0x0400146E RID: 5230
		BoonieHat = 2,
		// Token: 0x0400146F RID: 5231
		CandleHat = 3,
		// Token: 0x04001470 RID: 5232
		MinersHat = 4,
		// Token: 0x04001471 RID: 5233
		WoodHelmet = 5
	}

	// Token: 0x020004A8 RID: 1192
	[Flags]
	public enum OccupationSlots
	{
		// Token: 0x04001473 RID: 5235
		HeadTop = 1,
		// Token: 0x04001474 RID: 5236
		Face = 2,
		// Token: 0x04001475 RID: 5237
		HeadBack = 4,
		// Token: 0x04001476 RID: 5238
		TorsoFront = 8,
		// Token: 0x04001477 RID: 5239
		TorsoBack = 16,
		// Token: 0x04001478 RID: 5240
		LeftShoulder = 32,
		// Token: 0x04001479 RID: 5241
		RightShoulder = 64,
		// Token: 0x0400147A RID: 5242
		LeftArm = 128,
		// Token: 0x0400147B RID: 5243
		RightArm = 256,
		// Token: 0x0400147C RID: 5244
		LeftHand = 512,
		// Token: 0x0400147D RID: 5245
		RightHand = 1024,
		// Token: 0x0400147E RID: 5246
		Groin = 2048,
		// Token: 0x0400147F RID: 5247
		Bum = 4096,
		// Token: 0x04001480 RID: 5248
		LeftKnee = 8192,
		// Token: 0x04001481 RID: 5249
		RightKnee = 16384,
		// Token: 0x04001482 RID: 5250
		LeftLeg = 32768,
		// Token: 0x04001483 RID: 5251
		RightLeg = 65536,
		// Token: 0x04001484 RID: 5252
		LeftFoot = 131072,
		// Token: 0x04001485 RID: 5253
		RightFoot = 262144
	}
}
