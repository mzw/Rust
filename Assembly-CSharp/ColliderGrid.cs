﻿using System;
using System.Collections;
using System.Diagnostics;
using ConVar;
using Rust;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000291 RID: 657
public class ColliderGrid : SingletonComponent<global::ColliderGrid>, IServerComponent
{
	// Token: 0x060010F4 RID: 4340 RVA: 0x00065AC8 File Offset: 0x00063CC8
	protected void OnEnable()
	{
		base.StartCoroutine(this.UpdateCoroutine());
	}

	// Token: 0x060010F5 RID: 4341 RVA: 0x00065AD8 File Offset: 0x00063CD8
	public static void RefreshAll()
	{
		if (SingletonComponent<global::ColliderGrid>.Instance)
		{
			SingletonComponent<global::ColliderGrid>.Instance.Refresh();
		}
	}

	// Token: 0x060010F6 RID: 4342 RVA: 0x00065AF4 File Offset: 0x00063CF4
	public void Refresh()
	{
		if (this.grid == null)
		{
			this.Init();
		}
		for (int i = 0; i < this.grid.CellCount; i++)
		{
			for (int j = 0; j < this.grid.CellCount; j++)
			{
				this.grid[i, j].Refresh();
			}
		}
	}

	// Token: 0x17000129 RID: 297
	public global::ColliderCell this[Vector3 worldPos]
	{
		get
		{
			if (this.grid == null)
			{
				this.Init();
			}
			return this.grid[worldPos];
		}
	}

	// Token: 0x060010F8 RID: 4344 RVA: 0x00065B7C File Offset: 0x00063D7C
	private void Init()
	{
		this.grid = new WorldSpaceGrid<global::ColliderCell>(global::TerrainMeta.Size.x, this.CellSize);
		for (int i = 0; i < this.grid.CellCount; i++)
		{
			for (int j = 0; j < this.grid.CellCount; j++)
			{
				this.grid[i, j] = new global::ColliderCell(this, this.grid.GridToWorldCoords(new Vector2i(i, j)));
			}
		}
		this.pool = new global::PrefabPool();
	}

	// Token: 0x060010F9 RID: 4345 RVA: 0x00065C10 File Offset: 0x00063E10
	public global::MeshColliderBatch CreateInstance()
	{
		GameObject gameObject = this.pool.Pop(default(Vector3), default(Quaternion));
		if (gameObject)
		{
			gameObject.AwakeFromInstantiate();
		}
		else
		{
			gameObject = global::MeshColliderBatch.CreateInstance();
			if (ConVar.Pool.collider_batches)
			{
				gameObject.EnablePooling(0u);
			}
			SceneManager.MoveGameObjectToScene(gameObject, Rust.Generic.BatchingScene);
		}
		return gameObject.GetComponent<global::MeshColliderBatch>();
	}

	// Token: 0x060010FA RID: 4346 RVA: 0x00065C7C File Offset: 0x00063E7C
	public void RecycleInstance(global::MeshColliderBatch instance)
	{
		if (instance.gameObject.SupportsPooling())
		{
			this.pool.Push(instance.gameObject);
		}
		else
		{
			global::GameManager.Destroy(instance.gameObject, 0f);
		}
	}

	// Token: 0x060010FB RID: 4347 RVA: 0x00065CB4 File Offset: 0x00063EB4
	public int MeshCount()
	{
		if (this.grid == null)
		{
			return 0;
		}
		int num = 0;
		for (int i = 0; i < this.grid.CellCount; i++)
		{
			for (int j = 0; j < this.grid.CellCount; j++)
			{
				num += this.grid[i, j].MeshCount();
			}
		}
		return num;
	}

	// Token: 0x060010FC RID: 4348 RVA: 0x00065D20 File Offset: 0x00063F20
	public int BatchedMeshCount()
	{
		if (this.grid == null)
		{
			return 0;
		}
		int num = 0;
		for (int i = 0; i < this.grid.CellCount; i++)
		{
			for (int j = 0; j < this.grid.CellCount; j++)
			{
				num += this.grid[i, j].BatchedMeshCount();
			}
		}
		return num;
	}

	// Token: 0x1700012A RID: 298
	// (get) Token: 0x060010FD RID: 4349 RVA: 0x00065D8C File Offset: 0x00063F8C
	public bool NeedsTimeout
	{
		get
		{
			return this.watch.Elapsed.TotalMilliseconds > (double)this.MaxMilliseconds;
		}
	}

	// Token: 0x060010FE RID: 4350 RVA: 0x00065DB8 File Offset: 0x00063FB8
	public void ResetTimeout()
	{
		this.watch.Reset();
		this.watch.Start();
	}

	// Token: 0x060010FF RID: 4351 RVA: 0x00065DD0 File Offset: 0x00063FD0
	private IEnumerator UpdateCoroutine()
	{
		for (;;)
		{
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			if (!Application.isReceiving)
			{
				if (!Application.isLoading)
				{
					if (!global::ColliderGrid.Paused)
					{
						if (this.grid != null)
						{
							this.ResetTimeout();
							for (int x = 0; x < this.grid.CellCount; x++)
							{
								for (int z = 0; z < this.grid.CellCount; z++)
								{
									global::ColliderCell cell = this.grid[x, z];
									if (cell.NeedsRefresh())
									{
										IEnumerator enumerator = cell.RefreshAsync();
										while (enumerator.MoveNext())
										{
											object obj = enumerator.Current;
											yield return obj;
										}
									}
									if (this.NeedsTimeout)
									{
										yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
										this.ResetTimeout();
									}
								}
							}
						}
					}
				}
			}
		}
		yield break;
	}

	// Token: 0x04000C11 RID: 3089
	public static bool Paused;

	// Token: 0x04000C12 RID: 3090
	public float CellSize = 50f;

	// Token: 0x04000C13 RID: 3091
	public float MaxMilliseconds = 0.1f;

	// Token: 0x04000C14 RID: 3092
	private WorldSpaceGrid<global::ColliderCell> grid;

	// Token: 0x04000C15 RID: 3093
	private global::PrefabPool pool;

	// Token: 0x04000C16 RID: 3094
	private Stopwatch watch = Stopwatch.StartNew();
}
