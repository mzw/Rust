﻿using System;
using System.Collections.Generic;
using Network;
using UnityEngine;

// Token: 0x02000337 RID: 823
public static class ConsoleNetwork
{
	// Token: 0x060013CD RID: 5069 RVA: 0x000740A8 File Offset: 0x000722A8
	internal static void Init()
	{
	}

	// Token: 0x060013CE RID: 5070 RVA: 0x000740AC File Offset: 0x000722AC
	internal static void OnClientCommand(Message packet)
	{
		string text = packet.read.String();
		if (packet.connection == null || !packet.connection.connected)
		{
			Debug.LogWarning("Client without connection tried to run command: " + text);
			return;
		}
		string text2 = ConsoleSystem.Run(ConsoleSystem.Option.Server.FromConnection(packet.connection).Quiet(), text, new object[0]);
		if (!string.IsNullOrEmpty(text2))
		{
			global::ConsoleNetwork.SendClientReply(packet.connection, text2);
		}
	}

	// Token: 0x060013CF RID: 5071 RVA: 0x00074130 File Offset: 0x00072330
	internal static void SendClientReply(Connection cn, string strCommand)
	{
		if (!Net.sv.IsConnected())
		{
			return;
		}
		Net.sv.write.Start();
		Net.sv.write.PacketID(11);
		Net.sv.write.String(strCommand);
		Net.sv.write.Send(new SendInfo(cn));
	}

	// Token: 0x060013D0 RID: 5072 RVA: 0x00074194 File Offset: 0x00072394
	public static void SendClientCommand(Connection cn, string strCommand, params object[] args)
	{
		if (!Net.sv.IsConnected())
		{
			return;
		}
		Net.sv.write.Start();
		Net.sv.write.PacketID(12);
		Net.sv.write.String(ConsoleSystem.BuildCommand(strCommand, args));
		Net.sv.write.Send(new SendInfo(cn));
	}

	// Token: 0x060013D1 RID: 5073 RVA: 0x00074200 File Offset: 0x00072400
	public static void SendClientCommand(List<Connection> cn, string strCommand, params object[] args)
	{
		if (!Net.sv.IsConnected())
		{
			return;
		}
		Net.sv.write.Start();
		Net.sv.write.PacketID(12);
		Net.sv.write.String(ConsoleSystem.BuildCommand(strCommand, args));
		Net.sv.write.Send(new SendInfo(cn));
	}

	// Token: 0x060013D2 RID: 5074 RVA: 0x0007426C File Offset: 0x0007246C
	public static void BroadcastToAllClients(string strCommand, params object[] args)
	{
		if (!Net.sv.IsConnected())
		{
			return;
		}
		Net.sv.write.Start();
		Net.sv.write.PacketID(12);
		Net.sv.write.String(ConsoleSystem.BuildCommand(strCommand, args));
		Net.sv.write.Send(new SendInfo(Net.sv.connections));
	}
}
