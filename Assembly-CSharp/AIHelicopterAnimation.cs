﻿using System;
using UnityEngine;

// Token: 0x020003A2 RID: 930
public class AIHelicopterAnimation : MonoBehaviour
{
	// Token: 0x060015E5 RID: 5605 RVA: 0x0007E64C File Offset: 0x0007C84C
	public void Awake()
	{
		this.lastPosition = base.transform.position;
	}

	// Token: 0x060015E6 RID: 5606 RVA: 0x0007E660 File Offset: 0x0007C860
	public Vector3 GetMoveDirection()
	{
		return this._ai.GetMoveDirection();
	}

	// Token: 0x060015E7 RID: 5607 RVA: 0x0007E670 File Offset: 0x0007C870
	public float GetMoveSpeed()
	{
		return this._ai.GetMoveSpeed();
	}

	// Token: 0x060015E8 RID: 5608 RVA: 0x0007E680 File Offset: 0x0007C880
	public void Update()
	{
		this.lastPosition = base.transform.position;
		Vector3 moveDirection = this.GetMoveDirection();
		float moveSpeed = this.GetMoveSpeed();
		float num = 0.25f + Mathf.Clamp01(moveSpeed / this._ai.maxSpeed) * 0.75f;
		this.smoothRateOfChange = Mathf.Lerp(this.smoothRateOfChange, moveSpeed - this.oldMoveSpeed, Time.deltaTime * 5f);
		this.oldMoveSpeed = moveSpeed;
		float num2 = Vector3.Angle(moveDirection, base.transform.forward);
		float num3 = Vector3.Angle(moveDirection, -base.transform.forward);
		float num4 = 1f - Mathf.Clamp01(num2 / this.degreeMax);
		float num5 = 1f - Mathf.Clamp01(num3 / this.degreeMax);
		float num6 = (num4 - num5) * num;
		float num7 = Mathf.Lerp(this.lastForwardBackScalar, num6, Time.deltaTime * 2f);
		this.lastForwardBackScalar = num7;
		float num8 = Vector3.Angle(moveDirection, base.transform.right);
		float num9 = Vector3.Angle(moveDirection, -base.transform.right);
		float num10 = 1f - Mathf.Clamp01(num8 / this.degreeMax);
		float num11 = 1f - Mathf.Clamp01(num9 / this.degreeMax);
		float num12 = (num10 - num11) * num;
		float num13 = Mathf.Lerp(this.lastStrafeScalar, num12, Time.deltaTime * 2f);
		this.lastStrafeScalar = num13;
		Vector3 zero = Vector3.zero;
		zero.x += num7 * this.swayAmount;
		zero.z -= num13 * this.swayAmount;
		Quaternion localRotation = Quaternion.identity;
		localRotation = Quaternion.Euler(zero.x, zero.y, zero.z);
		this._ai.helicopterBase.rotorPivot.transform.localRotation = localRotation;
	}

	// Token: 0x04001060 RID: 4192
	public global::PatrolHelicopterAI _ai;

	// Token: 0x04001061 RID: 4193
	public float swayAmount = 1f;

	// Token: 0x04001062 RID: 4194
	public float lastStrafeScalar;

	// Token: 0x04001063 RID: 4195
	public float lastForwardBackScalar;

	// Token: 0x04001064 RID: 4196
	public float degreeMax = 90f;

	// Token: 0x04001065 RID: 4197
	public Vector3 lastPosition = Vector3.zero;

	// Token: 0x04001066 RID: 4198
	public float oldMoveSpeed;

	// Token: 0x04001067 RID: 4199
	public float smoothRateOfChange;

	// Token: 0x04001068 RID: 4200
	public float flareAmount;
}
