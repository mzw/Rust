﻿using System;
using UnityEngine;

// Token: 0x02000550 RID: 1360
public class RandomStaticObject : MonoBehaviour
{
	// Token: 0x06001C9A RID: 7322 RVA: 0x000A03B4 File Offset: 0x0009E5B4
	protected void Start()
	{
		uint num = SeedEx.Seed(base.transform.position, global::World.Seed + this.Seed);
		if (SeedRandom.Value(ref num) > this.Probability)
		{
			for (int i = 0; i < this.Candidates.Length; i++)
			{
				global::GameManager.Destroy(this.Candidates[i], 0f);
			}
			global::GameManager.Destroy(this, 0f);
		}
		else
		{
			int num2 = SeedRandom.Range(num, 0, base.transform.childCount);
			for (int j = 0; j < this.Candidates.Length; j++)
			{
				GameObject gameObject = this.Candidates[j];
				if (j == num2)
				{
					gameObject.SetActive(true);
				}
				else
				{
					global::GameManager.Destroy(gameObject, 0f);
				}
			}
			global::GameManager.Destroy(this, 0f);
		}
	}

	// Token: 0x040017B4 RID: 6068
	public uint Seed;

	// Token: 0x040017B5 RID: 6069
	public float Probability = 0.5f;

	// Token: 0x040017B6 RID: 6070
	public GameObject[] Candidates;
}
