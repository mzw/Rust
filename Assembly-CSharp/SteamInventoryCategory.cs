﻿using System;
using UnityEngine;

// Token: 0x02000657 RID: 1623
[CreateAssetMenu(menuName = "Rust/Generic Steam Inventory Category")]
public class SteamInventoryCategory : ScriptableObject
{
	// Token: 0x04001B89 RID: 7049
	[Header("Steam Inventory")]
	public bool canBeSoldToOtherUsers;

	// Token: 0x04001B8A RID: 7050
	public bool canBeTradedWithOtherUsers;

	// Token: 0x04001B8B RID: 7051
	public bool isCommodity;

	// Token: 0x04001B8C RID: 7052
	public global::SteamInventoryCategory.Price price;

	// Token: 0x04001B8D RID: 7053
	public global::SteamInventoryCategory.DropChance dropChance;

	// Token: 0x04001B8E RID: 7054
	public bool CanBeInCrates = true;

	// Token: 0x02000658 RID: 1624
	public enum Price
	{
		// Token: 0x04001B90 RID: 7056
		CannotBuy,
		// Token: 0x04001B91 RID: 7057
		VLV25,
		// Token: 0x04001B92 RID: 7058
		VLV50,
		// Token: 0x04001B93 RID: 7059
		VLV75,
		// Token: 0x04001B94 RID: 7060
		VLV100,
		// Token: 0x04001B95 RID: 7061
		VLV150,
		// Token: 0x04001B96 RID: 7062
		VLV200,
		// Token: 0x04001B97 RID: 7063
		VLV250,
		// Token: 0x04001B98 RID: 7064
		VLV300,
		// Token: 0x04001B99 RID: 7065
		VLV350,
		// Token: 0x04001B9A RID: 7066
		VLV400,
		// Token: 0x04001B9B RID: 7067
		VLV450,
		// Token: 0x04001B9C RID: 7068
		VLV500,
		// Token: 0x04001B9D RID: 7069
		VLV550,
		// Token: 0x04001B9E RID: 7070
		VLV600,
		// Token: 0x04001B9F RID: 7071
		VLV650,
		// Token: 0x04001BA0 RID: 7072
		VLV700,
		// Token: 0x04001BA1 RID: 7073
		VLV750,
		// Token: 0x04001BA2 RID: 7074
		VLV800,
		// Token: 0x04001BA3 RID: 7075
		VLV850,
		// Token: 0x04001BA4 RID: 7076
		VLV900,
		// Token: 0x04001BA5 RID: 7077
		VLV950,
		// Token: 0x04001BA6 RID: 7078
		VLV1000,
		// Token: 0x04001BA7 RID: 7079
		VLV1100,
		// Token: 0x04001BA8 RID: 7080
		VLV1200,
		// Token: 0x04001BA9 RID: 7081
		VLV1300,
		// Token: 0x04001BAA RID: 7082
		VLV1400,
		// Token: 0x04001BAB RID: 7083
		VLV1500,
		// Token: 0x04001BAC RID: 7084
		VLV1600,
		// Token: 0x04001BAD RID: 7085
		VLV1700,
		// Token: 0x04001BAE RID: 7086
		VLV1800,
		// Token: 0x04001BAF RID: 7087
		VLV1900,
		// Token: 0x04001BB0 RID: 7088
		VLV2000,
		// Token: 0x04001BB1 RID: 7089
		VLV2500,
		// Token: 0x04001BB2 RID: 7090
		VLV3000,
		// Token: 0x04001BB3 RID: 7091
		VLV3500,
		// Token: 0x04001BB4 RID: 7092
		VLV4000,
		// Token: 0x04001BB5 RID: 7093
		VLV4500,
		// Token: 0x04001BB6 RID: 7094
		VLV5000,
		// Token: 0x04001BB7 RID: 7095
		VLV6000,
		// Token: 0x04001BB8 RID: 7096
		VLV7000,
		// Token: 0x04001BB9 RID: 7097
		VLV8000,
		// Token: 0x04001BBA RID: 7098
		VLV9000,
		// Token: 0x04001BBB RID: 7099
		VLV10000
	}

	// Token: 0x02000659 RID: 1625
	public enum DropChance
	{
		// Token: 0x04001BBD RID: 7101
		NeverDrop,
		// Token: 0x04001BBE RID: 7102
		VeryRare,
		// Token: 0x04001BBF RID: 7103
		Rare,
		// Token: 0x04001BC0 RID: 7104
		Common,
		// Token: 0x04001BC1 RID: 7105
		VeryCommon,
		// Token: 0x04001BC2 RID: 7106
		ExtremelyRare
	}
}
