﻿using System;
using UnityEngine;

// Token: 0x020005D1 RID: 1489
public class TerrainCarve : global::TerrainModifier
{
	// Token: 0x06001EB9 RID: 7865 RVA: 0x000AD70C File Offset: 0x000AB90C
	protected override void Apply(Vector3 position, float opacity, float radius, float fade)
	{
		if (!global::TerrainMeta.AlphaMap)
		{
			return;
		}
		global::TerrainMeta.AlphaMap.SetAlpha(position, 0f, opacity, radius, fade);
	}
}
