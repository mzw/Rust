﻿using System;
using UnityEngine;

// Token: 0x020004F0 RID: 1264
public class ItemModRecycleInto : global::ItemMod
{
	// Token: 0x06001AFE RID: 6910 RVA: 0x0009709C File Offset: 0x0009529C
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (command == "recycle_item")
		{
			int num = Random.Range(this.numRecycledItemMin, this.numRecycledItemMax + 1);
			item.UseItem(1);
			if (num > 0)
			{
				global::Item item2 = global::ItemManager.Create(this.recycleIntoItem, num, 0UL);
				if (!item2.MoveToContainer(player.inventory.containerMain, -1, true))
				{
					item2.Drop(player.eyes.position, player.eyes.BodyForward() * 2f, default(Quaternion));
				}
				if (this.successEffect.isValid)
				{
					global::Effect.server.Run(this.successEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
				}
			}
		}
	}

	// Token: 0x040015C7 RID: 5575
	public global::ItemDefinition recycleIntoItem;

	// Token: 0x040015C8 RID: 5576
	public int numRecycledItemMin = 1;

	// Token: 0x040015C9 RID: 5577
	public int numRecycledItemMax = 1;

	// Token: 0x040015CA RID: 5578
	public global::GameObjectRef successEffect;
}
