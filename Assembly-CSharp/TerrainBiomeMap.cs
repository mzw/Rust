﻿using System;
using UnityEngine;

// Token: 0x02000569 RID: 1385
public class TerrainBiomeMap : global::TerrainMap3D<byte>
{
	// Token: 0x06001CE4 RID: 7396 RVA: 0x000A1A1C File Offset: 0x0009FC1C
	public override void Setup()
	{
		if (this.BiomeTexture != null)
		{
			if (this.BiomeTexture.width == this.BiomeTexture.height)
			{
				this.res = this.BiomeTexture.width;
				this.num = 4;
				this.src = (this.dst = new byte[this.num, this.res, this.res]);
				Color32[] pixels = this.BiomeTexture.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						Color32 color = pixels[num];
						this.dst[0, i, j] = color.r;
						this.dst[1, i, j] = color.g;
						this.dst[2, i, j] = color.b;
						this.dst[3, i, j] = color.a;
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid biome texture: " + this.BiomeTexture.name);
			}
		}
		else
		{
			this.res = this.terrain.terrainData.alphamapResolution;
			this.num = 4;
			this.src = (this.dst = new byte[this.num, this.res, this.res]);
		}
	}

	// Token: 0x06001CE5 RID: 7397 RVA: 0x000A1BA4 File Offset: 0x0009FDA4
	public void GenerateTextures()
	{
		this.BiomeTexture = new Texture2D(this.res, this.res, 4, true, true);
		this.BiomeTexture.name = "BiomeTexture";
		this.BiomeTexture.wrapMode = 1;
		Color32[] col = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				byte b = this.src[0, z, i];
				byte b2 = this.src[1, z, i];
				byte b3 = this.src[2, z, i];
				byte b4 = this.src[3, z, i];
				col[z * this.res + i] = new Color32(b, b2, b3, b4);
			}
		});
		this.BiomeTexture.SetPixels32(col);
	}

	// Token: 0x06001CE6 RID: 7398 RVA: 0x000A1C38 File Offset: 0x0009FE38
	public void ApplyTextures()
	{
		this.BiomeTexture.Apply(true, false);
		this.BiomeTexture.Compress(false);
		this.BiomeTexture.Apply(false, true);
	}

	// Token: 0x06001CE7 RID: 7399 RVA: 0x000A1C60 File Offset: 0x0009FE60
	public float GetBiomeMax(Vector3 worldPos, int mask = -1)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetBiomeMax(normX, normZ, mask);
	}

	// Token: 0x06001CE8 RID: 7400 RVA: 0x000A1C90 File Offset: 0x0009FE90
	public float GetBiomeMax(float normX, float normZ, int mask = -1)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetBiomeMax(x, z, mask);
	}

	// Token: 0x06001CE9 RID: 7401 RVA: 0x000A1CB8 File Offset: 0x0009FEB8
	public float GetBiomeMax(int x, int z, int mask = -1)
	{
		byte b = 0;
		for (int i = 0; i < this.num; i++)
		{
			if ((global::TerrainBiome.IndexToType(i) & mask) != 0)
			{
				byte b2 = this.src[i, z, x];
				if (b2 >= b)
				{
					b = b2;
				}
			}
		}
		return (float)b;
	}

	// Token: 0x06001CEA RID: 7402 RVA: 0x000A1D10 File Offset: 0x0009FF10
	public int GetBiomeMaxIndex(Vector3 worldPos, int mask = -1)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetBiomeMaxIndex(normX, normZ, mask);
	}

	// Token: 0x06001CEB RID: 7403 RVA: 0x000A1D40 File Offset: 0x0009FF40
	public int GetBiomeMaxIndex(float normX, float normZ, int mask = -1)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetBiomeMaxIndex(x, z, mask);
	}

	// Token: 0x06001CEC RID: 7404 RVA: 0x000A1D68 File Offset: 0x0009FF68
	public int GetBiomeMaxIndex(int x, int z, int mask = -1)
	{
		byte b = 0;
		int result = 0;
		for (int i = 0; i < this.num; i++)
		{
			if ((global::TerrainBiome.IndexToType(i) & mask) != 0)
			{
				byte b2 = this.src[i, z, x];
				if (b2 >= b)
				{
					b = b2;
					result = i;
				}
			}
		}
		return result;
	}

	// Token: 0x06001CED RID: 7405 RVA: 0x000A1DC4 File Offset: 0x0009FFC4
	public int GetBiomeMaxType(Vector3 worldPos, int mask = -1)
	{
		return global::TerrainBiome.IndexToType(this.GetBiomeMaxIndex(worldPos, mask));
	}

	// Token: 0x06001CEE RID: 7406 RVA: 0x000A1DD4 File Offset: 0x0009FFD4
	public int GetBiomeMaxType(float normX, float normZ, int mask = -1)
	{
		return global::TerrainBiome.IndexToType(this.GetBiomeMaxIndex(normX, normZ, mask));
	}

	// Token: 0x06001CEF RID: 7407 RVA: 0x000A1DE4 File Offset: 0x0009FFE4
	public int GetBiomeMaxType(int x, int z, int mask = -1)
	{
		return global::TerrainBiome.IndexToType(this.GetBiomeMaxIndex(x, z, mask));
	}

	// Token: 0x06001CF0 RID: 7408 RVA: 0x000A1DF4 File Offset: 0x0009FFF4
	public float GetBiome(Vector3 worldPos, int mask)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetBiome(normX, normZ, mask);
	}

	// Token: 0x06001CF1 RID: 7409 RVA: 0x000A1E24 File Offset: 0x000A0024
	public float GetBiome(float normX, float normZ, int mask)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetBiome(x, z, mask);
	}

	// Token: 0x06001CF2 RID: 7410 RVA: 0x000A1E4C File Offset: 0x000A004C
	public float GetBiome(int x, int z, int mask)
	{
		if (Mathf.IsPowerOfTwo(mask))
		{
			return global::TextureData.Byte2Float((int)this.src[global::TerrainBiome.TypeToIndex(mask), z, x]);
		}
		int num = 0;
		for (int i = 0; i < this.num; i++)
		{
			if ((global::TerrainBiome.IndexToType(i) & mask) != 0)
			{
				num += (int)this.src[i, z, x];
			}
		}
		return Mathf.Clamp01(global::TextureData.Byte2Float(num));
	}

	// Token: 0x06001CF3 RID: 7411 RVA: 0x000A1EC4 File Offset: 0x000A00C4
	public void SetBiome(Vector3 worldPos, int id)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetBiome(normX, normZ, id);
	}

	// Token: 0x06001CF4 RID: 7412 RVA: 0x000A1EF4 File Offset: 0x000A00F4
	public void SetBiome(float normX, float normZ, int id)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetBiome(x, z, id);
	}

	// Token: 0x06001CF5 RID: 7413 RVA: 0x000A1F1C File Offset: 0x000A011C
	public void SetBiome(int x, int z, int id)
	{
		int num = global::TerrainBiome.TypeToIndex(id);
		for (int i = 0; i < this.num; i++)
		{
			if (i == num)
			{
				this.dst[i, z, x] = byte.MaxValue;
			}
			else
			{
				this.dst[i, z, x] = 0;
			}
		}
	}

	// Token: 0x06001CF6 RID: 7414 RVA: 0x000A1F78 File Offset: 0x000A0178
	public void SetBiome(Vector3 worldPos, int id, float v)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetBiome(normX, normZ, id, v);
	}

	// Token: 0x06001CF7 RID: 7415 RVA: 0x000A1FAC File Offset: 0x000A01AC
	public void SetBiome(float normX, float normZ, int id, float v)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetBiome(x, z, id, v);
	}

	// Token: 0x06001CF8 RID: 7416 RVA: 0x000A1FD4 File Offset: 0x000A01D4
	public void SetBiome(int x, int z, int id, float v)
	{
		this.SetBiome(x, z, id, this.GetBiome(x, z, id), v);
	}

	// Token: 0x06001CF9 RID: 7417 RVA: 0x000A1FEC File Offset: 0x000A01EC
	public void SetBiomeRaw(int x, int z, Vector4 v, float opacity)
	{
		if (opacity == 0f)
		{
			return;
		}
		float num = Mathf.Clamp01(v.x + v.y + v.z + v.w);
		if (num == 0f)
		{
			return;
		}
		float num2 = 1f - opacity * num;
		if (num2 == 0f && opacity == 1f)
		{
			this.dst[0, z, x] = global::TextureData.Float2Byte(v.x);
			this.dst[1, z, x] = global::TextureData.Float2Byte(v.y);
			this.dst[2, z, x] = global::TextureData.Float2Byte(v.z);
			this.dst[3, z, x] = global::TextureData.Float2Byte(v.w);
		}
		else
		{
			this.dst[0, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[0, z, x]) * num2 + v.x * opacity);
			this.dst[1, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[1, z, x]) * num2 + v.y * opacity);
			this.dst[2, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[2, z, x]) * num2 + v.z * opacity);
			this.dst[3, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[3, z, x]) * num2 + v.w * opacity);
		}
	}

	// Token: 0x06001CFA RID: 7418 RVA: 0x000A2190 File Offset: 0x000A0390
	private void SetBiome(int x, int z, int id, float old_val, float new_val)
	{
		int num = global::TerrainBiome.TypeToIndex(id);
		if (old_val >= 1f)
		{
			return;
		}
		float num2 = (1f - new_val) / (1f - old_val);
		for (int i = 0; i < this.num; i++)
		{
			if (i == num)
			{
				this.dst[i, z, x] = global::TextureData.Float2Byte(new_val);
			}
			else
			{
				this.dst[i, z, x] = global::TextureData.Float2Byte(num2 * global::TextureData.Byte2Float((int)this.dst[i, z, x]));
			}
		}
	}

	// Token: 0x04001805 RID: 6149
	public Texture2D BiomeTexture;

	// Token: 0x04001806 RID: 6150
	internal int num;
}
