﻿using System;
using UnityEngine;

// Token: 0x02000746 RID: 1862
public class LightLOD : MonoBehaviour, global::ILOD, IClientComponent
{
	// Token: 0x060022EB RID: 8939 RVA: 0x000C21B4 File Offset: 0x000C03B4
	protected void OnValidate()
	{
		global::LightEx.CheckConflict(base.gameObject);
	}

	// Token: 0x04001F55 RID: 8021
	public float DistanceBias;

	// Token: 0x04001F56 RID: 8022
	public bool ToggleLight;

	// Token: 0x04001F57 RID: 8023
	public bool ToggleShadows = true;
}
