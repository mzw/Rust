﻿using System;
using Rust;
using UnityEngine;

// Token: 0x020004DF RID: 1247
[RequireComponent(typeof(global::ItemModConsumable))]
public class ItemModConsume : global::ItemMod
{
	// Token: 0x06001AC3 RID: 6851 RVA: 0x00095D58 File Offset: 0x00093F58
	public virtual global::ItemModConsumable GetConsumable()
	{
		if (this.primaryConsumable)
		{
			return this.primaryConsumable;
		}
		return base.GetComponent<global::ItemModConsumable>();
	}

	// Token: 0x06001AC4 RID: 6852 RVA: 0x00095D78 File Offset: 0x00093F78
	public virtual global::GameObjectRef GetConsumeEffect()
	{
		return this.consumeEffect;
	}

	// Token: 0x06001AC5 RID: 6853 RVA: 0x00095D80 File Offset: 0x00093F80
	public override void DoAction(global::Item item, global::BasePlayer player)
	{
		if (item.amount < 1)
		{
			return;
		}
		global::GameObjectRef gameObjectRef = this.GetConsumeEffect();
		if (gameObjectRef.isValid)
		{
			Vector3 posLocal = (!player.IsDucked()) ? new Vector3(0f, 2f, 0f) : new Vector3(0f, 1f, 0f);
			global::Effect.server.Run(gameObjectRef.resourcePath, player, 0u, posLocal, Vector3.zero, null, false);
		}
		player.metabolism.MarkConsumption();
		global::ItemModConsumable consumable = this.GetConsumable();
		float num = (float)Mathf.Max(consumable.amountToConsume, 1);
		float num2 = Mathf.Min((float)item.amount, num);
		float num3 = num2 / num;
		float num4 = item.conditionNormalized;
		if (consumable.conditionFractionToLose > 0f)
		{
			num4 = consumable.conditionFractionToLose;
		}
		foreach (global::ItemModConsumable.ConsumableEffect consumableEffect in consumable.effects)
		{
			if (player.healthFraction <= consumableEffect.onlyIfHealthLessThan)
			{
				if (consumableEffect.type == global::MetabolismAttribute.Type.Health)
				{
					if (consumableEffect.amount < 0f)
					{
						player.OnAttacked(new global::HitInfo(player, player, Rust.DamageType.Generic, -consumableEffect.amount * num3 * num4, player.transform.position + player.transform.forward * 1f));
					}
					else
					{
						player.health += consumableEffect.amount * num3 * num4;
					}
				}
				else
				{
					player.metabolism.ApplyChange(consumableEffect.type, consumableEffect.amount * num3 * num4, consumableEffect.time * num3 * num4);
				}
			}
		}
		if (this.product != null)
		{
			foreach (global::ItemAmountRandom itemAmountRandom in this.product)
			{
				int num5 = Mathf.RoundToInt((float)itemAmountRandom.RandomAmount() * num4);
				if (num5 > 0)
				{
					global::Item item2 = global::ItemManager.Create(itemAmountRandom.itemDef, num5, 0UL);
					player.GiveItem(item2, global::BaseEntity.GiveItemReason.Generic);
				}
			}
		}
		if (string.IsNullOrEmpty(this.eatGesture))
		{
			player.SignalBroadcast(global::BaseEntity.Signal.Gesture, this.eatGesture, null);
		}
		if (consumable.conditionFractionToLose > 0f)
		{
			item.LoseCondition(consumable.conditionFractionToLose * item.maxCondition);
		}
		else
		{
			item.UseItem((int)num2);
		}
	}

	// Token: 0x06001AC6 RID: 6854 RVA: 0x00096020 File Offset: 0x00094220
	public override bool CanDoAction(global::Item item, global::BasePlayer player)
	{
		return player.metabolism.CanConsume();
	}

	// Token: 0x04001588 RID: 5512
	public global::GameObjectRef consumeEffect;

	// Token: 0x04001589 RID: 5513
	public string eatGesture = "eat_2hand";

	// Token: 0x0400158A RID: 5514
	[Tooltip("Items that are given on consumption of this item")]
	public global::ItemAmountRandom[] product;

	// Token: 0x0400158B RID: 5515
	public global::ItemModConsumable primaryConsumable;
}
