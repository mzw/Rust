﻿using System;
using Facepunch;
using ProtoBuf;

// Token: 0x020003D2 RID: 978
public class PlayerCorpse : global::LootableCorpse
{
	// Token: 0x060016D4 RID: 5844 RVA: 0x00083A18 File Offset: 0x00081C18
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (base.isServer && this.containers != null && this.containers.Length > 1 && !info.forDisk)
		{
			info.msg.storageBox = Pool.Get<StorageBox>();
			info.msg.storageBox.contents = this.containers[1].Save();
		}
	}

	// Token: 0x060016D5 RID: 5845 RVA: 0x00083A8C File Offset: 0x00081C8C
	public override string Categorize()
	{
		return "playercorpse";
	}
}
