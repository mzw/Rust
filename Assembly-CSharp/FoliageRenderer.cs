﻿using System;
using UnityEngine;

// Token: 0x02000423 RID: 1059
public class FoliageRenderer : MonoBehaviour, IClientComponent
{
	// Token: 0x040012BA RID: 4794
	public Material material;

	// Token: 0x040012BB RID: 4795
	public Mesh LOD0;

	// Token: 0x040012BC RID: 4796
	public Mesh LOD1;
}
