﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200071A RID: 1818
public class UIDeathScreen : SingletonComponent<global::UIDeathScreen>, global::IUIScreen
{
	// Token: 0x04001EF5 RID: 7925
	public GameObject sleepingBagIconPrefab;

	// Token: 0x04001EF6 RID: 7926
	public GameObject sleepingBagContainer;

	// Token: 0x04001EF7 RID: 7927
	public global::LifeInfographic previousLifeInfographic;

	// Token: 0x04001EF8 RID: 7928
	public Animator screenAnimator;

	// Token: 0x04001EF9 RID: 7929
	public bool fadeIn;

	// Token: 0x04001EFA RID: 7930
	public Button ReportCheatButton;
}
