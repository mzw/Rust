﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000756 RID: 1878
public abstract class PrefabAttribute : MonoBehaviour, global::IPrefabPreProcess
{
	// Token: 0x1700027A RID: 634
	// (get) Token: 0x0600231A RID: 8986 RVA: 0x000C2A30 File Offset: 0x000C0C30
	public bool isClient
	{
		get
		{
			return !this.isServer;
		}
	}

	// Token: 0x0600231B RID: 8987 RVA: 0x000C2A3C File Offset: 0x000C0C3C
	public virtual void PreProcess(global::IPrefabProcessor preProcess, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		if (bundling)
		{
			return;
		}
		this.fullName = name;
		this.hierachyName = base.transform.GetRecursiveName(string.Empty);
		this.prefabID = global::StringPool.Get(name);
		this.instanceID = base.GetInstanceID();
		this.worldPosition = base.transform.position;
		this.worldRotation = base.transform.rotation;
		this.worldForward = base.transform.forward;
		this.localPosition = base.transform.localPosition;
		this.localScale = base.transform.localScale;
		this.localRotation = base.transform.localRotation;
		if (serverside)
		{
			this.prefabAttribute = global::PrefabAttribute.server;
			this.gameManager = global::GameManager.server;
			this.isServer = true;
		}
		this.AttributeSetup(rootObj, name, serverside, clientside, bundling);
		if (serverside)
		{
			global::PrefabAttribute.server.Add(this.prefabID, this);
		}
		preProcess.RemoveComponent(this);
		preProcess.NominateForDeletion(base.gameObject);
	}

	// Token: 0x0600231C RID: 8988 RVA: 0x000C2B4C File Offset: 0x000C0D4C
	protected virtual void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
	}

	// Token: 0x0600231D RID: 8989
	protected abstract Type GetIndexedType();

	// Token: 0x0600231E RID: 8990 RVA: 0x000C2B50 File Offset: 0x000C0D50
	public static bool operator ==(global::PrefabAttribute x, global::PrefabAttribute y)
	{
		return global::PrefabAttribute.ComparePrefabAttribute(x, y);
	}

	// Token: 0x0600231F RID: 8991 RVA: 0x000C2B5C File Offset: 0x000C0D5C
	public static bool operator !=(global::PrefabAttribute x, global::PrefabAttribute y)
	{
		return !global::PrefabAttribute.ComparePrefabAttribute(x, y);
	}

	// Token: 0x06002320 RID: 8992 RVA: 0x000C2B68 File Offset: 0x000C0D68
	public override bool Equals(object o)
	{
		return global::PrefabAttribute.ComparePrefabAttribute(this, (global::PrefabAttribute)o);
	}

	// Token: 0x06002321 RID: 8993 RVA: 0x000C2B78 File Offset: 0x000C0D78
	public override int GetHashCode()
	{
		return (this.hierachyName == null) ? base.GetHashCode() : this.hierachyName.GetHashCode();
	}

	// Token: 0x06002322 RID: 8994 RVA: 0x000C2B9C File Offset: 0x000C0D9C
	public static implicit operator bool(global::PrefabAttribute exists)
	{
		return !object.ReferenceEquals(exists, null);
	}

	// Token: 0x06002323 RID: 8995 RVA: 0x000C2BA8 File Offset: 0x000C0DA8
	internal static bool ComparePrefabAttribute(global::PrefabAttribute x, global::PrefabAttribute y)
	{
		bool flag = object.ReferenceEquals(x, null);
		bool flag2 = object.ReferenceEquals(y, null);
		return (flag && flag2) || (!flag && !flag2 && x.instanceID == y.instanceID);
	}

	// Token: 0x06002324 RID: 8996 RVA: 0x000C2BF0 File Offset: 0x000C0DF0
	public override string ToString()
	{
		if (object.ReferenceEquals(this, null))
		{
			return "null";
		}
		return this.hierachyName;
	}

	// Token: 0x04001F7F RID: 8063
	[NonSerialized]
	public Vector3 worldPosition;

	// Token: 0x04001F80 RID: 8064
	[NonSerialized]
	public Quaternion worldRotation;

	// Token: 0x04001F81 RID: 8065
	[NonSerialized]
	public Vector3 worldForward;

	// Token: 0x04001F82 RID: 8066
	[NonSerialized]
	public Vector3 localPosition;

	// Token: 0x04001F83 RID: 8067
	[NonSerialized]
	public Vector3 localScale;

	// Token: 0x04001F84 RID: 8068
	[NonSerialized]
	public Quaternion localRotation;

	// Token: 0x04001F85 RID: 8069
	[NonSerialized]
	public string fullName;

	// Token: 0x04001F86 RID: 8070
	[NonSerialized]
	public string hierachyName;

	// Token: 0x04001F87 RID: 8071
	[NonSerialized]
	public uint prefabID;

	// Token: 0x04001F88 RID: 8072
	[NonSerialized]
	public int instanceID;

	// Token: 0x04001F89 RID: 8073
	[NonSerialized]
	public global::PrefabAttribute.Library prefabAttribute;

	// Token: 0x04001F8A RID: 8074
	[NonSerialized]
	public global::GameManager gameManager;

	// Token: 0x04001F8B RID: 8075
	[NonSerialized]
	public bool isServer;

	// Token: 0x04001F8C RID: 8076
	public static global::PrefabAttribute.Library server = new global::PrefabAttribute.Library();

	// Token: 0x02000757 RID: 1879
	public class AttributeCollection
	{
		// Token: 0x06002327 RID: 8999 RVA: 0x000C2C38 File Offset: 0x000C0E38
		internal List<global::PrefabAttribute> Find(Type t)
		{
			List<global::PrefabAttribute> list;
			if (this.attributes.TryGetValue(t, out list))
			{
				return list;
			}
			list = new List<global::PrefabAttribute>();
			this.attributes.Add(t, list);
			return list;
		}

		// Token: 0x06002328 RID: 9000 RVA: 0x000C2C70 File Offset: 0x000C0E70
		public T[] Find<T>()
		{
			if (this.cache == null)
			{
				this.cache = new Dictionary<Type, object>();
			}
			object obj;
			if (this.cache.TryGetValue(typeof(T), out obj))
			{
				return (T[])obj;
			}
			obj = this.Find(typeof(T)).Cast<T>().ToArray<T>();
			this.cache.Add(typeof(T), obj);
			return (T[])obj;
		}

		// Token: 0x06002329 RID: 9001 RVA: 0x000C2CF0 File Offset: 0x000C0EF0
		public void Add(global::PrefabAttribute attribute)
		{
			List<global::PrefabAttribute> list = this.Find(attribute.GetIndexedType());
			Assert.IsTrue(!list.Contains(attribute), "AttributeCollection.Add: Adding twice to list");
			list.Add(attribute);
			this.cache = null;
		}

		// Token: 0x04001F8D RID: 8077
		private Dictionary<Type, List<global::PrefabAttribute>> attributes = new Dictionary<Type, List<global::PrefabAttribute>>();

		// Token: 0x04001F8E RID: 8078
		private Dictionary<Type, object> cache = new Dictionary<Type, object>();
	}

	// Token: 0x02000758 RID: 1880
	public class Library
	{
		// Token: 0x0600232B RID: 9003 RVA: 0x000C2D4C File Offset: 0x000C0F4C
		public global::PrefabAttribute.AttributeCollection Find(uint prefabID)
		{
			global::PrefabAttribute.AttributeCollection attributeCollection;
			if (this.prefabs.TryGetValue(prefabID, out attributeCollection))
			{
				return attributeCollection;
			}
			attributeCollection = new global::PrefabAttribute.AttributeCollection();
			this.prefabs.Add(prefabID, attributeCollection);
			return attributeCollection;
		}

		// Token: 0x0600232C RID: 9004 RVA: 0x000C2D84 File Offset: 0x000C0F84
		public T Find<T>(uint prefabID) where T : global::PrefabAttribute
		{
			T[] array = this.Find(prefabID).Find<T>();
			if (array.Length == 0)
			{
				return (T)((object)null);
			}
			return array[0];
		}

		// Token: 0x0600232D RID: 9005 RVA: 0x000C2DB4 File Offset: 0x000C0FB4
		public T[] FindAll<T>(uint prefabID) where T : global::PrefabAttribute
		{
			return this.Find(prefabID).Find<T>();
		}

		// Token: 0x0600232E RID: 9006 RVA: 0x000C2DC4 File Offset: 0x000C0FC4
		public T[] GetAll<T>() where T : global::PrefabAttribute
		{
			return this.all.Find<T>();
		}

		// Token: 0x0600232F RID: 9007 RVA: 0x000C2DD4 File Offset: 0x000C0FD4
		public void Add(uint prefabID, global::PrefabAttribute attribute)
		{
			global::PrefabAttribute.AttributeCollection attributeCollection = this.Find(prefabID);
			attributeCollection.Add(attribute);
			this.all.Add(attribute);
		}

		// Token: 0x04001F8F RID: 8079
		private global::PrefabAttribute.AttributeCollection all = new global::PrefabAttribute.AttributeCollection();

		// Token: 0x04001F90 RID: 8080
		private Dictionary<uint, global::PrefabAttribute.AttributeCollection> prefabs = new Dictionary<uint, global::PrefabAttribute.AttributeCollection>();
	}
}
