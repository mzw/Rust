﻿using System;

// Token: 0x0200051D RID: 1309
[Serializable]
public struct NoiseParameters
{
	// Token: 0x06001BD2 RID: 7122 RVA: 0x0009BB5C File Offset: 0x00099D5C
	public NoiseParameters(int octaves, float frequency, float amplitude, float offset)
	{
		this.Octaves = octaves;
		this.Frequency = frequency;
		this.Amplitude = amplitude;
		this.Offset = offset;
	}

	// Token: 0x0400165C RID: 5724
	public int Octaves;

	// Token: 0x0400165D RID: 5725
	public float Frequency;

	// Token: 0x0400165E RID: 5726
	public float Amplitude;

	// Token: 0x0400165F RID: 5727
	public float Offset;
}
