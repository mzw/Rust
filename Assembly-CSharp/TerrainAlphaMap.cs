﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

// Token: 0x02000566 RID: 1382
public class TerrainAlphaMap : global::TerrainMap2D<byte>
{
	// Token: 0x06001CD3 RID: 7379 RVA: 0x000A1568 File Offset: 0x0009F768
	public override void Setup()
	{
		if (this.AlphaTexture != null)
		{
			if (this.AlphaTexture.width == this.AlphaTexture.height)
			{
				this.res = this.AlphaTexture.width;
				this.src = (this.dst = new byte[this.res, this.res]);
				Color32[] pixels = this.AlphaTexture.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						this.dst[i, j] = pixels[num].a;
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid alpha texture: " + this.AlphaTexture.name);
			}
		}
		else
		{
			this.res = this.terrain.terrainData.alphamapResolution;
			this.src = (this.dst = new byte[this.res, this.res]);
			for (int k = 0; k < this.res; k++)
			{
				for (int l = 0; l < this.res; l++)
				{
					this.dst[k, l] = byte.MaxValue;
				}
			}
		}
	}

	// Token: 0x06001CD4 RID: 7380 RVA: 0x000A16D4 File Offset: 0x0009F8D4
	public void GenerateTextures()
	{
		this.AlphaTexture = new Texture2D(this.res, this.res, 1, false, true);
		this.AlphaTexture.name = "AlphaTexture";
		this.AlphaTexture.wrapMode = 1;
		Color32[] col = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				byte b = this.src[z, i];
				col[z * this.res + i] = new Color32(b, b, b, b);
			}
		});
		this.AlphaTexture.SetPixels32(col);
	}

	// Token: 0x06001CD5 RID: 7381 RVA: 0x000A1768 File Offset: 0x0009F968
	public void ApplyTextures()
	{
		this.AlphaTexture.Apply(true, false);
		this.AlphaTexture.Compress(false);
		this.AlphaTexture.Apply(false, true);
	}

	// Token: 0x06001CD6 RID: 7382 RVA: 0x000A1790 File Offset: 0x0009F990
	public float GetAlpha(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetAlpha(normX, normZ);
	}

	// Token: 0x06001CD7 RID: 7383 RVA: 0x000A17C0 File Offset: 0x0009F9C0
	public float GetAlpha(float normX, float normZ)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		float num6 = Mathf.Lerp(this.GetAlpha(num4, num5), this.GetAlpha(x, num5), num2 - (float)num4);
		float num7 = Mathf.Lerp(this.GetAlpha(num4, z), this.GetAlpha(x, z), num2 - (float)num4);
		return Mathf.Lerp(num6, num7, num3 - (float)num5);
	}

	// Token: 0x06001CD8 RID: 7384 RVA: 0x000A1858 File Offset: 0x0009FA58
	public float GetAlpha(int x, int z)
	{
		return global::TextureData.Byte2Float((int)this.src[z, x]);
	}

	// Token: 0x06001CD9 RID: 7385 RVA: 0x000A186C File Offset: 0x0009FA6C
	public void SetAlpha(Vector3 worldPos, float a)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetAlpha(normX, normZ, a);
	}

	// Token: 0x06001CDA RID: 7386 RVA: 0x000A189C File Offset: 0x0009FA9C
	public void SetAlpha(float normX, float normZ, float a)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetAlpha(x, z, a);
	}

	// Token: 0x06001CDB RID: 7387 RVA: 0x000A18C4 File Offset: 0x0009FAC4
	public void SetAlpha(int x, int z, float a)
	{
		this.dst[z, x] = global::TextureData.Float2Byte(a);
	}

	// Token: 0x06001CDC RID: 7388 RVA: 0x000A18DC File Offset: 0x0009FADC
	public void SetAlpha(int x, int z, float a, float opacity)
	{
		this.SetAlpha(x, z, Mathf.Lerp(this.GetAlpha(x, z), a, opacity));
	}

	// Token: 0x06001CDD RID: 7389 RVA: 0x000A18F8 File Offset: 0x0009FAF8
	public void SetAlpha(Vector3 worldPos, float a, float opacity, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetAlpha(normX, normZ, a, opacity, radius, fade);
	}

	// Token: 0x06001CDE RID: 7390 RVA: 0x000A1930 File Offset: 0x0009FB30
	public void SetAlpha(float normX, float normZ, float a, float opacity, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			lerp *= opacity;
			if (lerp > 0f)
			{
				this.SetAlpha(x, z, a, lerp);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x040017FF RID: 6143
	[FormerlySerializedAs("ColorTexture")]
	public Texture2D AlphaTexture;
}
