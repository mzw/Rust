﻿using System;
using UnityEngine;

// Token: 0x020003D6 RID: 982
public class ServerProjectile : global::EntityComponent<global::BaseEntity>, IServerComponent
{
	// Token: 0x060016E5 RID: 5861 RVA: 0x00083C94 File Offset: 0x00081E94
	private void Update()
	{
		if (base.baseEntity.isServer)
		{
			this.DoMovement();
		}
	}

	// Token: 0x060016E6 RID: 5862 RVA: 0x00083CAC File Offset: 0x00081EAC
	public void DoMovement()
	{
		this._currentVelocity += Physics.gravity * this.gravityModifier * Time.deltaTime * Time.timeScale;
		float num = this._currentVelocity.magnitude * Time.deltaTime;
		RaycastHit raycastHit;
		if (global::GamePhysics.Trace(new Ray(base.transform.position, this._currentVelocity.normalized), 0f, out raycastHit, num + this.scanRange, 1101212417, 0))
		{
			global::BaseEntity entity = raycastHit.GetEntity();
			if (!entity.IsValid() || !base.baseEntity.creatorEntity.IsValid() || entity.net.ID != base.baseEntity.creatorEntity.net.ID)
			{
				base.transform.position += base.transform.forward * raycastHit.distance;
				base.SendMessage("ProjectileImpact", raycastHit, 1);
				return;
			}
		}
		base.transform.position += base.transform.forward * num;
		base.transform.rotation = Quaternion.LookRotation(this._currentVelocity.normalized);
	}

	// Token: 0x060016E7 RID: 5863 RVA: 0x00083E08 File Offset: 0x00082008
	public void InitializeVelocity(Vector3 overrideVel)
	{
		Vector3 normalized = overrideVel.normalized;
		base.transform.rotation = Quaternion.LookRotation(normalized);
		this.initialVelocity = normalized * this.speed;
		this._currentVelocity = normalized * this.speed;
	}

	// Token: 0x0400119D RID: 4509
	public Vector3 initialVelocity;

	// Token: 0x0400119E RID: 4510
	public float drag;

	// Token: 0x0400119F RID: 4511
	public float gravityModifier = 1f;

	// Token: 0x040011A0 RID: 4512
	public float speed = 15f;

	// Token: 0x040011A1 RID: 4513
	public float scanRange;

	// Token: 0x040011A2 RID: 4514
	private Vector3 _currentVelocity = Vector3.zero;
}
