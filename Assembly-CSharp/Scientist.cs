﻿using System;
using System.Collections.Generic;
using Rust.Ai;
using UnityEngine;

// Token: 0x020000D2 RID: 210
public class Scientist : global::NPCPlayerApex
{
	// Token: 0x06000AE8 RID: 2792 RVA: 0x00049A7C File Offset: 0x00047C7C
	public override string Categorize()
	{
		return "scientist";
	}

	// Token: 0x06000AE9 RID: 2793 RVA: 0x00049A84 File Offset: 0x00047C84
	public override float StartHealth()
	{
		return Random.Range(150f, 150f);
	}

	// Token: 0x06000AEA RID: 2794 RVA: 0x00049A98 File Offset: 0x00047C98
	public override float StartMaxHealth()
	{
		return 150f;
	}

	// Token: 0x06000AEB RID: 2795 RVA: 0x00049AA0 File Offset: 0x00047CA0
	public override float MaxHealth()
	{
		return 150f;
	}

	// Token: 0x1700006D RID: 109
	// (get) Token: 0x06000AEC RID: 2796 RVA: 0x00049AA8 File Offset: 0x00047CA8
	public override global::BaseNpc.AiStatistics.FamilyEnum Family
	{
		get
		{
			return global::BaseNpc.AiStatistics.FamilyEnum.Scientist;
		}
	}

	// Token: 0x06000AED RID: 2797 RVA: 0x00049AAC File Offset: 0x00047CAC
	public override void ServerInit()
	{
		if (base.isClient)
		{
			return;
		}
		base.ServerInit();
		global::Scientist.AllScientists.Add(this);
		this.InitComm();
	}

	// Token: 0x06000AEE RID: 2798 RVA: 0x00049AD4 File Offset: 0x00047CD4
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		global::Scientist.AllScientists.Remove(this);
		this.OnDestroyComm();
	}

	// Token: 0x06000AEF RID: 2799 RVA: 0x00049AF0 File Offset: 0x00047CF0
	public override bool ShouldDropActiveItem()
	{
		return false;
	}

	// Token: 0x06000AF0 RID: 2800 RVA: 0x00049AF4 File Offset: 0x00047CF4
	public override void CreateCorpse()
	{
		using (TimeWarning.New("Create corpse", 0.1f))
		{
			global::NPCPlayerCorpse npcplayerCorpse = base.DropCorpse("assets/prefabs/npc/scientist/scientist_corpse.prefab") as global::NPCPlayerCorpse;
			if (npcplayerCorpse)
			{
				npcplayerCorpse.SetLootableIn(2f);
				npcplayerCorpse.SetFlag(global::BaseEntity.Flags.Reserved5, base.HasPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash), false);
				npcplayerCorpse.SetFlag(global::BaseEntity.Flags.Reserved2, true, false);
				npcplayerCorpse.TakeFrom(new global::ItemContainer[]
				{
					this.inventory.containerMain,
					this.inventory.containerWear,
					this.inventory.containerBelt
				});
				npcplayerCorpse.playerName = base.displayName;
				npcplayerCorpse.playerSteamID = this.userID;
				npcplayerCorpse.Spawn();
				npcplayerCorpse.TakeChildren(this);
				foreach (global::ItemContainer itemContainer in npcplayerCorpse.containers)
				{
					itemContainer.Clear();
				}
				if (this.LootSpawnSlots.Length > 0)
				{
					foreach (global::LootContainer.LootSpawnSlot lootSpawnSlot in this.LootSpawnSlots)
					{
						for (int k = 0; k < lootSpawnSlot.numberToSpawn; k++)
						{
							float num = Random.Range(0f, 1f);
							if (num <= lootSpawnSlot.probability)
							{
								lootSpawnSlot.definition.SpawnIntoContainer(npcplayerCorpse.containers[0]);
							}
						}
					}
				}
			}
		}
	}

	// Token: 0x06000AF1 RID: 2801 RVA: 0x00049CA0 File Offset: 0x00047EA0
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		this._displayName = string.Format("Scientist {0}", (this.net == null) ? "scientist".GetHashCode() : ((int)this.net.ID));
	}

	// Token: 0x06000AF2 RID: 2802 RVA: 0x00049CF0 File Offset: 0x00047EF0
	private void InitComm()
	{
		base.OnAggro = (global::NPCPlayerApex.ActionCallback)Delegate.Combine(base.OnAggro, new global::NPCPlayerApex.ActionCallback(this.OnAggroComm));
	}

	// Token: 0x06000AF3 RID: 2803 RVA: 0x00049D14 File Offset: 0x00047F14
	private void OnDestroyComm()
	{
		base.OnAggro = (global::NPCPlayerApex.ActionCallback)Delegate.Remove(base.OnAggro, new global::NPCPlayerApex.ActionCallback(this.OnAggroComm));
	}

	// Token: 0x06000AF4 RID: 2804 RVA: 0x00049D38 File Offset: 0x00047F38
	public override int GetAlliesInRange(out List<global::Scientist> allies)
	{
		global::Scientist.CommQueryCache.Clear();
		foreach (global::Scientist scientist in global::Scientist.AllScientists)
		{
			if (base.IsInCommunicationRange(scientist))
			{
				global::Scientist.CommQueryCache.Add(scientist);
			}
		}
		allies = global::Scientist.CommQueryCache;
		return global::Scientist.CommQueryCache.Count;
	}

	// Token: 0x06000AF5 RID: 2805 RVA: 0x00049DC0 File Offset: 0x00047FC0
	public override void SendStatement(Rust.Ai.AiStatement_EnemyEngaged statement)
	{
		foreach (global::Scientist scientist in global::Scientist.AllScientists)
		{
			if (base.IsInCommunicationRange(scientist))
			{
				scientist.OnAiStatement(this, statement);
			}
		}
	}

	// Token: 0x06000AF6 RID: 2806 RVA: 0x00049E28 File Offset: 0x00048028
	public override void SendStatement(Rust.Ai.AiStatement_EnemySeen statement)
	{
		foreach (global::Scientist scientist in global::Scientist.AllScientists)
		{
			if (base.IsInCommunicationRange(scientist))
			{
				scientist.OnAiStatement(this, statement);
			}
		}
	}

	// Token: 0x06000AF7 RID: 2807 RVA: 0x00049E90 File Offset: 0x00048090
	public override void OnAiStatement(global::NPCPlayerApex source, Rust.Ai.AiStatement_EnemyEngaged statement)
	{
		if (statement.Enemy == null)
		{
			return;
		}
		if (base.AiContext.EnemyPlayer == null || base.AiContext.EnemyPlayer == statement.Enemy)
		{
			if (source.GetFact(global::NPCPlayerApex.Facts.AttackedRecently) > 0)
			{
				base.SetFact(global::NPCPlayerApex.Facts.AllyAttackedRecently, 1, true, true);
				this.AllyAttackedRecentlyTimeout = Time.realtimeSinceStartup + 7f;
			}
			float sqrMagnitude = (statement.Enemy.ServerPosition - this.ServerPosition).sqrMagnitude;
			bool flag = base.IsVisibleStanding(statement.Enemy);
			bool flag2 = false;
			if (!flag)
			{
				flag2 = base.IsVisibleCrouched(statement.Enemy);
			}
			if (flag || flag2)
			{
				base.AiContext.Memory.Update(statement.Enemy, 0f);
			}
			base.SetAttackTarget(statement.Enemy, statement.Score, sqrMagnitude, flag, flag2);
			if (base.GetFact(global::NPCPlayerApex.Facts.IsAggro) == 0)
			{
				base.StartAggro(this.Stats.DeaggroChaseTime, false);
			}
		}
	}

	// Token: 0x06000AF8 RID: 2808 RVA: 0x00049FB0 File Offset: 0x000481B0
	public override void OnAiStatement(global::NPCPlayerApex source, Rust.Ai.AiStatement_EnemySeen statement)
	{
	}

	// Token: 0x06000AF9 RID: 2809 RVA: 0x00049FB4 File Offset: 0x000481B4
	public override int AskQuestion(Rust.Ai.AiQuestion_ShareEnemyTarget question, out List<Rust.Ai.AiAnswer_ShareEnemyTarget> answers)
	{
		global::Scientist.CommTargetCache.Clear();
		List<global::Scientist> list;
		if (this.GetAlliesInRange(out list) > 0)
		{
			foreach (global::Scientist scientist in list)
			{
				Rust.Ai.AiAnswer_ShareEnemyTarget item = scientist.OnAiQuestion(this, question);
				if (item.PlayerTarget != null)
				{
					global::Scientist.CommTargetCache.Add(item);
				}
			}
		}
		answers = global::Scientist.CommTargetCache;
		return global::Scientist.CommTargetCache.Count;
	}

	// Token: 0x06000AFA RID: 2810 RVA: 0x0004A054 File Offset: 0x00048254
	private void OnAggroComm()
	{
		this.SendStatement(new Rust.Ai.AiStatement_EnemyEngaged
		{
			Enemy = base.AiContext.EnemyPlayer,
			Score = base.AiContext.LastTargetScore
		});
	}

	// Token: 0x040005AB RID: 1451
	public global::LootContainer.LootSpawnSlot[] LootSpawnSlots;

	// Token: 0x040005AC RID: 1452
	private static readonly HashSet<global::Scientist> AllScientists = new HashSet<global::Scientist>();

	// Token: 0x040005AD RID: 1453
	private static readonly List<global::Scientist> CommQueryCache = new List<global::Scientist>();

	// Token: 0x040005AE RID: 1454
	private static readonly List<Rust.Ai.AiAnswer_ShareEnemyTarget> CommTargetCache = new List<Rust.Ai.AiAnswer_ShareEnemyTarget>(10);
}
