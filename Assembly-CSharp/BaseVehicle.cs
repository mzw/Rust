﻿using System;

// Token: 0x020003E6 RID: 998
public class BaseVehicle : global::BaseMountable
{
	// Token: 0x0600173F RID: 5951 RVA: 0x000857C8 File Offset: 0x000839C8
	public virtual bool DirectlyMountable()
	{
		return false;
	}
}
