﻿using System;

namespace UnityEngine
{
	// Token: 0x02000762 RID: 1890
	public static class CollisionEx
	{
		// Token: 0x06002344 RID: 9028 RVA: 0x000C3250 File Offset: 0x000C1450
		public static global::BaseEntity GetEntity(this Collision col)
		{
			if (col.transform.CompareTag("MeshColliderBatch"))
			{
				global::MeshColliderBatch component = col.gameObject.GetComponent<global::MeshColliderBatch>();
				if (component)
				{
					for (int i = 0; i < col.contacts.Length; i++)
					{
						ContactPoint contactPoint = col.contacts[i];
						Ray ray;
						ray..ctor(contactPoint.point + contactPoint.normal * 0.01f, -contactPoint.normal);
						RaycastHit hit;
						if (col.collider.Raycast(ray, ref hit, 1f))
						{
							return hit.GetEntity();
						}
					}
				}
			}
			return col.gameObject.ToBaseEntity();
		}
	}
}
