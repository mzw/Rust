﻿using System;
using Rust;
using Rust.Registry;

namespace UnityEngine
{
	// Token: 0x02000768 RID: 1896
	public static class GameObjectEx
	{
		// Token: 0x06002353 RID: 9043 RVA: 0x000C382C File Offset: 0x000C1A2C
		public static global::BaseEntity ToBaseEntity(this GameObject go)
		{
			IEntity entity = GameObjectEx.GetEntityFromRegistry(go);
			if (entity == null && !go.transform.gameObject.activeSelf)
			{
				entity = GameObjectEx.GetEntityFromComponent(go);
			}
			return entity as global::BaseEntity;
		}

		// Token: 0x06002354 RID: 9044 RVA: 0x000C3868 File Offset: 0x000C1A68
		private static IEntity GetEntityFromRegistry(GameObject go)
		{
			Transform transform = go.transform;
			IEntity entity = Entity.Get(transform.gameObject);
			while (entity == null && transform.parent != null)
			{
				transform = transform.parent;
				entity = Entity.Get(transform.gameObject);
			}
			return entity;
		}

		// Token: 0x06002355 RID: 9045 RVA: 0x000C38B8 File Offset: 0x000C1AB8
		private static IEntity GetEntityFromComponent(GameObject go)
		{
			Transform transform = go.transform;
			IEntity component = transform.GetComponent<IEntity>();
			while (component == null && transform.parent != null)
			{
				transform = transform.parent;
				component = transform.GetComponent<IEntity>();
			}
			return component;
		}

		// Token: 0x06002356 RID: 9046 RVA: 0x000C3900 File Offset: 0x000C1B00
		public static void SetHierarchyGroup(this GameObject obj, string strRoot, bool groupActive = true, bool persistant = false)
		{
			obj.transform.SetParent(global::HierarchyUtil.GetRoot(strRoot, groupActive, persistant).transform, true);
		}
	}
}
