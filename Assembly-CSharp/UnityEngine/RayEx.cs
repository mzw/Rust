﻿using System;

namespace UnityEngine
{
	// Token: 0x02000770 RID: 1904
	public static class RayEx
	{
		// Token: 0x06002363 RID: 9059 RVA: 0x000C3F40 File Offset: 0x000C2140
		public static Vector3 ClosestPoint(this Ray ray, Vector3 pos)
		{
			return ray.origin + Vector3.Dot(pos - ray.origin, ray.direction) * ray.direction;
		}

		// Token: 0x06002364 RID: 9060 RVA: 0x000C3F74 File Offset: 0x000C2174
		public static float Distance(this Ray ray, Vector3 pos)
		{
			return Vector3.Cross(ray.direction, pos - ray.origin).magnitude;
		}

		// Token: 0x06002365 RID: 9061 RVA: 0x000C3FA4 File Offset: 0x000C21A4
		public static float SqrDistance(this Ray ray, Vector3 pos)
		{
			return Vector3.Cross(ray.direction, pos - ray.origin).sqrMagnitude;
		}

		// Token: 0x06002366 RID: 9062 RVA: 0x000C3FD4 File Offset: 0x000C21D4
		public static bool IsNaNOrInfinity(this Ray r)
		{
			return Vector3Ex.IsNaNOrInfinity(r.origin) || Vector3Ex.IsNaNOrInfinity(r.direction);
		}
	}
}
