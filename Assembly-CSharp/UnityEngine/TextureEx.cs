﻿using System;

namespace UnityEngine
{
	// Token: 0x02000773 RID: 1907
	public static class TextureEx
	{
		// Token: 0x0600236A RID: 9066 RVA: 0x000C40A4 File Offset: 0x000C22A4
		public static void Clear(this Texture2D tex, Color32 color)
		{
			if (tex.width > TextureEx.buffer.Length)
			{
				Debug.LogError("Trying to clear texture that is too big: " + tex.width);
				return;
			}
			for (int i = 0; i < tex.width; i++)
			{
				TextureEx.buffer[i] = color;
			}
			for (int j = 0; j < tex.height; j++)
			{
				tex.SetPixels32(0, j, tex.width, 1, TextureEx.buffer);
			}
			tex.Apply();
		}

		// Token: 0x0600236B RID: 9067 RVA: 0x000C4138 File Offset: 0x000C2338
		public static int GetSizeInBytes(this Texture texture)
		{
			int num = texture.width;
			int num2 = texture.height;
			if (texture is Texture2D)
			{
				Texture2D texture2D = texture as Texture2D;
				int bitsPerPixel = TextureEx.GetBitsPerPixel(texture2D.format);
				int mipmapCount = texture2D.mipmapCount;
				int i = 1;
				int num3 = 0;
				while (i <= mipmapCount)
				{
					num3 += num * num2 * bitsPerPixel / 8;
					num /= 2;
					num2 /= 2;
					i++;
				}
				return num3;
			}
			if (texture is Texture2DArray)
			{
				Texture2DArray texture2DArray = texture as Texture2DArray;
				int bitsPerPixel2 = TextureEx.GetBitsPerPixel(texture2DArray.format);
				int num4 = 10;
				int j = 1;
				int num5 = 0;
				int depth = texture2DArray.depth;
				while (j <= num4)
				{
					num5 += num * num2 * bitsPerPixel2 / 8;
					num /= 2;
					num2 /= 2;
					j++;
				}
				return num5 * depth;
			}
			if (texture is Cubemap)
			{
				Cubemap cubemap = texture as Cubemap;
				int bitsPerPixel3 = TextureEx.GetBitsPerPixel(cubemap.format);
				int num6 = num * num2 * bitsPerPixel3 / 8;
				int num7 = 6;
				return num6 * num7;
			}
			return 0;
		}

		// Token: 0x0600236C RID: 9068 RVA: 0x000C4244 File Offset: 0x000C2444
		public static int GetBitsPerPixel(TextureFormat format)
		{
			switch (format)
			{
			case 1:
				return 8;
			case 2:
				return 16;
			case 3:
				return 24;
			case 4:
				return 32;
			case 5:
				return 32;
			default:
				switch (format)
				{
				case 30:
					return 2;
				case 31:
					return 2;
				case 32:
					return 4;
				case 33:
					return 4;
				case 34:
					return 4;
				case 35:
					return 4;
				case 36:
					return 8;
				default:
					if (format != 25)
					{
						return 0;
					}
					break;
				}
				break;
			case 7:
				return 16;
			case 10:
				return 4;
			case 12:
				break;
			case 13:
				return 16;
			case 14:
				return 32;
			}
			return 8;
		}

		// Token: 0x04001F99 RID: 8089
		private static Color32[] buffer = new Color32[8192];
	}
}
