﻿using System;

namespace UnityEngine
{
	// Token: 0x0200076E RID: 1902
	public static class QuaternionEx
	{
		// Token: 0x0600235A RID: 9050 RVA: 0x000C3C40 File Offset: 0x000C1E40
		public static Quaternion AlignToNormal(this Quaternion rot, Vector3 normal)
		{
			return Quaternion.FromToRotation(Vector3.up, normal) * rot;
		}

		// Token: 0x0600235B RID: 9051 RVA: 0x000C3C54 File Offset: 0x000C1E54
		public static Quaternion LookRotationWithOffset(Vector3 offset, Vector3 forward, Vector3 up)
		{
			Quaternion quaternion = Quaternion.LookRotation(forward, Vector3.up);
			return quaternion * Quaternion.Inverse(Quaternion.LookRotation(offset, Vector3.up));
		}

		// Token: 0x0600235C RID: 9052 RVA: 0x000C3C88 File Offset: 0x000C1E88
		public static Quaternion LookRotationForcedUp(Vector3 forward, Vector3 up)
		{
			if (forward == up)
			{
				return Quaternion.LookRotation(up);
			}
			Vector3 vector = Vector3.Cross(forward, up);
			forward = Vector3.Cross(up, vector);
			return Quaternion.LookRotation(forward, up);
		}

		// Token: 0x0600235D RID: 9053 RVA: 0x000C3CC0 File Offset: 0x000C1EC0
		public static Quaternion LookRotationGradient(Vector3 normal, Vector3 up)
		{
			Vector3 vector = (!(normal == Vector3.up)) ? Vector3.Cross(normal, Vector3.up) : Vector3.forward;
			Vector3 forward = Vector3.Cross(normal, vector);
			return QuaternionEx.LookRotationForcedUp(forward, up);
		}

		// Token: 0x0600235E RID: 9054 RVA: 0x000C3D04 File Offset: 0x000C1F04
		public static Quaternion LookRotationNormal(Vector3 normal, Vector3 up = default(Vector3))
		{
			if (up != Vector3.zero)
			{
				return QuaternionEx.LookRotationForcedUp(up, normal);
			}
			if (normal == Vector3.up)
			{
				return QuaternionEx.LookRotationForcedUp(Vector3.forward, normal);
			}
			if (normal == Vector3.down)
			{
				return QuaternionEx.LookRotationForcedUp(Vector3.back, normal);
			}
			if (normal.y == 0f)
			{
				return QuaternionEx.LookRotationForcedUp(Vector3.up, normal);
			}
			Vector3 vector = Vector3.Cross(normal, Vector3.up);
			Vector3 vector2 = Vector3.Cross(normal, vector);
			return QuaternionEx.LookRotationForcedUp(-vector2, normal);
		}
	}
}
