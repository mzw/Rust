﻿using System;

namespace UnityEngine
{
	// Token: 0x02000771 RID: 1905
	public static class SkinnedMeshRendererEx
	{
		// Token: 0x06002367 RID: 9063 RVA: 0x000C3FF8 File Offset: 0x000C21F8
		public static Transform FindRig(this SkinnedMeshRenderer renderer)
		{
			Transform parent = renderer.transform.parent;
			Transform transform = renderer.rootBone;
			while (transform != null && transform.parent != null && transform.parent != parent)
			{
				transform = transform.parent;
			}
			return transform;
		}
	}
}
