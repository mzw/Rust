﻿using System;
using Network;

namespace UnityEngine
{
	// Token: 0x0200076B RID: 1899
	public static class NetworkPacketEx
	{
		// Token: 0x06002358 RID: 9048 RVA: 0x000C39A4 File Offset: 0x000C1BA4
		public static global::BasePlayer Player(this Message v)
		{
			if (v.connection == null)
			{
				return null;
			}
			return v.connection.player as global::BasePlayer;
		}
	}
}
