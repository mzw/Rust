﻿using System;
using UnityEngine.UI;

namespace UnityEngine
{
	// Token: 0x02000777 RID: 1911
	public static class UIEx
	{
		// Token: 0x06002389 RID: 9097 RVA: 0x000C4C18 File Offset: 0x000C2E18
		public static Vector2 Unpivot(this RectTransform rect, Vector2 localPos)
		{
			localPos.x += rect.pivot.x * rect.rect.width;
			localPos.y += rect.pivot.y * rect.rect.height;
			return localPos;
		}

		// Token: 0x0600238A RID: 9098 RVA: 0x000C4C7C File Offset: 0x000C2E7C
		public static void CenterOnPosition(this ScrollRect scrollrect, Vector2 pos)
		{
			RectTransform rectTransform = scrollrect.transform as RectTransform;
			Vector2 vector;
			vector..ctor(scrollrect.content.localScale.x, scrollrect.content.localScale.y);
			pos.x *= vector.x;
			pos.y *= vector.y;
			Vector2 vector2;
			vector2..ctor(scrollrect.content.rect.width * vector.x - rectTransform.rect.width, scrollrect.content.rect.height * vector.y - rectTransform.rect.height);
			pos.x = pos.x / vector2.x + scrollrect.content.pivot.x;
			pos.y = pos.y / vector2.y + scrollrect.content.pivot.y;
			if (scrollrect.movementType != null)
			{
				pos.x = Mathf.Clamp(pos.x, 0f, 1f);
				pos.y = Mathf.Clamp(pos.y, 0f, 1f);
			}
			scrollrect.normalizedPosition = pos;
		}
	}
}
