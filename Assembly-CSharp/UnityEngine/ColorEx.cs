﻿using System;

namespace UnityEngine
{
	// Token: 0x02000763 RID: 1891
	public static class ColorEx
	{
		// Token: 0x06002345 RID: 9029 RVA: 0x000C3310 File Offset: 0x000C1510
		public static string ToHex(this Color32 color)
		{
			return color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		}

		// Token: 0x06002346 RID: 9030 RVA: 0x000C334C File Offset: 0x000C154C
		public static Color Parse(string str)
		{
			string[] array = str.Split(new char[]
			{
				' '
			});
			if (array.Length == 3)
			{
				return new Color(float.Parse(array[0]), float.Parse(array[1]), float.Parse(array[2]));
			}
			if (array.Length == 4)
			{
				return new Color(float.Parse(array[0]), float.Parse(array[1]), float.Parse(array[2]), float.Parse(array[3]));
			}
			return Color.white;
		}
	}
}
