﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Facepunch;

namespace UnityEngine
{
	// Token: 0x02000775 RID: 1909
	public static class TransformEx
	{
		// Token: 0x0600236F RID: 9071 RVA: 0x000C4338 File Offset: 0x000C2538
		public static string GetRecursiveName(this Transform transform, string strEndName = "")
		{
			string text = transform.name;
			if (!string.IsNullOrEmpty(strEndName))
			{
				text = text + "/" + strEndName;
			}
			if (transform.parent != null)
			{
				text = transform.parent.GetRecursiveName(text);
			}
			return text;
		}

		// Token: 0x06002370 RID: 9072 RVA: 0x000C4384 File Offset: 0x000C2584
		public static void RemoveComponent<T>(this Transform transform) where T : Component
		{
			T component = transform.GetComponent<T>();
			if (component == null)
			{
				return;
			}
			global::GameManager.Destroy(component, 0f);
		}

		// Token: 0x06002371 RID: 9073 RVA: 0x000C43BC File Offset: 0x000C25BC
		public static void DestroyAllChildren(this Transform transform, bool immediate = false)
		{
			List<GameObject> list = Pool.GetList<GameObject>();
			IEnumerator enumerator = transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform2 = (Transform)obj;
					if (!transform2.CompareTag("persist"))
					{
						list.Add(transform2.gameObject);
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			if (immediate)
			{
				foreach (GameObject instance in list)
				{
					global::GameManager.DestroyImmediate(instance, false);
				}
			}
			else
			{
				foreach (GameObject instance2 in list)
				{
					global::GameManager.Destroy(instance2, 0f);
				}
			}
			Pool.FreeList<GameObject>(ref list);
		}

		// Token: 0x06002372 RID: 9074 RVA: 0x000C44E4 File Offset: 0x000C26E4
		public static void RetireAllChildren(this Transform transform, global::GameManager gameManager)
		{
			List<GameObject> list = Pool.GetList<GameObject>();
			IEnumerator enumerator = transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform2 = (Transform)obj;
					if (!transform2.CompareTag("persist"))
					{
						list.Add(transform2.gameObject);
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			foreach (GameObject instance in list)
			{
				gameManager.Retire(instance);
			}
			Pool.FreeList<GameObject>(ref list);
		}

		// Token: 0x06002373 RID: 9075 RVA: 0x000C45B4 File Offset: 0x000C27B4
		public static List<Transform> GetChildren(this Transform transform)
		{
			return transform.Cast<Transform>().ToList<Transform>();
		}

		// Token: 0x06002374 RID: 9076 RVA: 0x000C45C4 File Offset: 0x000C27C4
		public static void OrderChildren(this Transform tx, Func<Transform, object> selector)
		{
			foreach (Transform transform in tx.Cast<Transform>().OrderBy(selector))
			{
				transform.SetAsLastSibling();
			}
		}

		// Token: 0x06002375 RID: 9077 RVA: 0x000C4624 File Offset: 0x000C2824
		public static List<Transform> GetAllChildren(this Transform transform)
		{
			List<Transform> list = new List<Transform>();
			if (transform != null)
			{
				transform.AddAllChildren(list);
			}
			return list;
		}

		// Token: 0x06002376 RID: 9078 RVA: 0x000C464C File Offset: 0x000C284C
		public static void AddAllChildren(this Transform transform, List<Transform> list)
		{
			list.Add(transform);
			for (int i = 0; i < transform.childCount; i++)
			{
				Transform child = transform.GetChild(i);
				if (!(child == null))
				{
					child.AddAllChildren(list);
				}
			}
		}

		// Token: 0x06002377 RID: 9079 RVA: 0x000C4698 File Offset: 0x000C2898
		public static Transform[] GetChildrenWithTag(this Transform transform, string strTag)
		{
			List<Transform> allChildren = transform.GetAllChildren();
			return (from x in allChildren
			where x.CompareTag(strTag)
			select x).ToArray<Transform>();
		}

		// Token: 0x06002378 RID: 9080 RVA: 0x000C46D0 File Offset: 0x000C28D0
		public static void Identity(this GameObject go)
		{
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
		}

		// Token: 0x06002379 RID: 9081 RVA: 0x000C4704 File Offset: 0x000C2904
		public static GameObject CreateChild(this GameObject go)
		{
			GameObject gameObject = new GameObject();
			gameObject.transform.parent = go.transform;
			gameObject.Identity();
			return gameObject;
		}

		// Token: 0x0600237A RID: 9082 RVA: 0x000C4730 File Offset: 0x000C2930
		public static GameObject InstantiateChild(this GameObject go, GameObject prefab)
		{
			GameObject gameObject = Instantiate.GameObject(prefab, null);
			gameObject.transform.SetParent(go.transform, false);
			gameObject.Identity();
			return gameObject;
		}

		// Token: 0x0600237B RID: 9083 RVA: 0x000C4760 File Offset: 0x000C2960
		public static void SetLayerRecursive(this GameObject go, int Layer)
		{
			if (go.layer != Layer)
			{
				go.layer = Layer;
			}
			for (int i = 0; i < go.transform.childCount; i++)
			{
				go.transform.GetChild(i).gameObject.SetLayerRecursive(Layer);
			}
		}

		// Token: 0x0600237C RID: 9084 RVA: 0x000C47B4 File Offset: 0x000C29B4
		public static bool DropToGround(this Transform transform, bool alignToNormal = false, float fRange = 100f)
		{
			Vector3 position;
			Vector3 vector;
			if (transform.GetGroundInfo(out position, out vector, fRange))
			{
				transform.position = position;
				if (alignToNormal)
				{
					transform.rotation = Quaternion.LookRotation(transform.forward, vector);
				}
				return true;
			}
			return false;
		}

		// Token: 0x0600237D RID: 9085 RVA: 0x000C47F4 File Offset: 0x000C29F4
		public static bool GetGroundInfo(this Transform transform, out Vector3 pos, out Vector3 normal, float range = 100f)
		{
			return global::TransformUtil.GetGroundInfo(transform.position, out pos, out normal, range, transform);
		}

		// Token: 0x0600237E RID: 9086 RVA: 0x000C4808 File Offset: 0x000C2A08
		public static bool GetGroundInfoTerrainOnly(this Transform transform, out Vector3 pos, out Vector3 normal, float range = 100f)
		{
			return global::TransformUtil.GetGroundInfoTerrainOnly(transform.position, out pos, out normal, range);
		}

		// Token: 0x0600237F RID: 9087 RVA: 0x000C4818 File Offset: 0x000C2A18
		public static Bounds WorkoutRenderBounds(this Transform tx)
		{
			Bounds bounds;
			bounds..ctor(Vector3.zero, Vector3.zero);
			Renderer[] componentsInChildren = tx.GetComponentsInChildren<Renderer>();
			foreach (Renderer renderer in componentsInChildren)
			{
				if (!(renderer is ParticleRenderer))
				{
					if (!(renderer is ParticleSystemRenderer))
					{
						if (bounds.center == Vector3.zero)
						{
							bounds = renderer.bounds;
						}
						else
						{
							bounds.Encapsulate(renderer.bounds);
						}
					}
				}
			}
			return bounds;
		}

		// Token: 0x06002380 RID: 9088 RVA: 0x000C48AC File Offset: 0x000C2AAC
		public static List<T> GetSiblings<T>(this Transform transform, bool includeSelf = false)
		{
			List<T> list = new List<T>();
			if (transform.parent == null)
			{
				return list;
			}
			for (int i = 0; i < transform.parent.childCount; i++)
			{
				Transform child = transform.parent.GetChild(i);
				if (includeSelf || !(child == transform))
				{
					T component = child.GetComponent<T>();
					if (component != null)
					{
						list.Add(component);
					}
				}
			}
			return list;
		}

		// Token: 0x06002381 RID: 9089 RVA: 0x000C4934 File Offset: 0x000C2B34
		public static void DestroyChildren(this Transform transform)
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				global::GameManager.Destroy(transform.GetChild(i).gameObject, 0f);
			}
		}

		// Token: 0x06002382 RID: 9090 RVA: 0x000C4970 File Offset: 0x000C2B70
		public static void SetChildrenActive(this Transform transform, bool b)
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(b);
			}
		}

		// Token: 0x06002383 RID: 9091 RVA: 0x000C49A8 File Offset: 0x000C2BA8
		public static Transform ActiveChild(this Transform transform, string name, bool bDisableOthers)
		{
			Transform result = null;
			for (int i = 0; i < transform.childCount; i++)
			{
				Transform child = transform.GetChild(i);
				if (child.name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
				{
					result = child;
					child.gameObject.SetActive(true);
				}
				else if (bDisableOthers)
				{
					child.gameObject.SetActive(false);
				}
			}
			return result;
		}

		// Token: 0x06002384 RID: 9092 RVA: 0x000C4A10 File Offset: 0x000C2C10
		public static T GetComponentInChildrenIncludeDisabled<T>(this Transform transform) where T : Component
		{
			List<T> list = Pool.GetList<T>();
			transform.GetComponentsInChildren<T>(true, list);
			T result = (list.Count <= 0) ? ((T)((object)null)) : list[0];
			Pool.FreeList<T>(ref list);
			return result;
		}

		// Token: 0x06002385 RID: 9093 RVA: 0x000C4A54 File Offset: 0x000C2C54
		public static void SetHierarchyGroup(this Transform transform, string strRoot, bool groupActive = true, bool persistant = false)
		{
			transform.SetParent(global::HierarchyUtil.GetRoot(strRoot, groupActive, persistant).transform, true);
		}

		// Token: 0x06002386 RID: 9094 RVA: 0x000C4A6C File Offset: 0x000C2C6C
		public static Bounds GetBounds(this Transform transform, bool includeRenderers = true, bool includeColliders = true, bool includeInactive = true)
		{
			Bounds result;
			result..ctor(Vector3.zero, Vector3.zero);
			if (includeRenderers)
			{
				foreach (MeshFilter meshFilter in transform.GetComponentsInChildren<MeshFilter>(includeInactive))
				{
					if (meshFilter.sharedMesh)
					{
						Matrix4x4 matrix = transform.worldToLocalMatrix * meshFilter.transform.localToWorldMatrix;
						Bounds bounds = meshFilter.sharedMesh.bounds;
						result.Encapsulate(bounds.Transform(matrix));
					}
				}
				foreach (SkinnedMeshRenderer skinnedMeshRenderer in transform.GetComponentsInChildren<SkinnedMeshRenderer>(includeInactive))
				{
					if (skinnedMeshRenderer.sharedMesh)
					{
						Matrix4x4 matrix2 = transform.worldToLocalMatrix * skinnedMeshRenderer.transform.localToWorldMatrix;
						Bounds bounds2 = skinnedMeshRenderer.sharedMesh.bounds;
						result.Encapsulate(bounds2.Transform(matrix2));
					}
				}
			}
			if (includeColliders)
			{
				foreach (MeshCollider meshCollider in transform.GetComponentsInChildren<MeshCollider>(includeInactive))
				{
					if (meshCollider.sharedMesh)
					{
						if (!meshCollider.isTrigger)
						{
							Matrix4x4 matrix3 = transform.worldToLocalMatrix * meshCollider.transform.localToWorldMatrix;
							Bounds bounds3 = meshCollider.sharedMesh.bounds;
							result.Encapsulate(bounds3.Transform(matrix3));
						}
					}
				}
			}
			return result;
		}
	}
}
