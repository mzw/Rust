﻿using System;
using System.Collections.Generic;

namespace UnityEngine
{
	// Token: 0x02000765 RID: 1893
	public static class CoroutineEx
	{
		// Token: 0x06002348 RID: 9032 RVA: 0x000C33E4 File Offset: 0x000C15E4
		public static WaitForSeconds waitForSeconds(float seconds)
		{
			WaitForSeconds waitForSeconds;
			if (!CoroutineEx.waitForSecondsBuffer.TryGetValue(seconds, out waitForSeconds))
			{
				waitForSeconds = new WaitForSeconds(seconds);
				CoroutineEx.waitForSecondsBuffer.Add(seconds, waitForSeconds);
			}
			return waitForSeconds;
		}

		// Token: 0x06002349 RID: 9033 RVA: 0x000C3418 File Offset: 0x000C1618
		public static WaitForSecondsRealtime waitForSecondsRealtime(float seconds)
		{
			return new WaitForSecondsRealtime(seconds);
		}

		// Token: 0x04001F95 RID: 8085
		public static WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

		// Token: 0x04001F96 RID: 8086
		public static WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();

		// Token: 0x04001F97 RID: 8087
		private static Dictionary<float, WaitForSeconds> waitForSecondsBuffer = new Dictionary<float, WaitForSeconds>();
	}
}
