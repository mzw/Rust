﻿using System;

namespace UnityEngine
{
	// Token: 0x0200075E RID: 1886
	public static class ArgEx
	{
		// Token: 0x06002337 RID: 9015 RVA: 0x000C2EC0 File Offset: 0x000C10C0
		public static global::BasePlayer Player(this ConsoleSystem.Arg arg)
		{
			if (arg.Connection == null)
			{
				return null;
			}
			return arg.Connection.player as global::BasePlayer;
		}

		// Token: 0x06002338 RID: 9016 RVA: 0x000C2EE0 File Offset: 0x000C10E0
		public static global::BasePlayer GetPlayer(this ConsoleSystem.Arg arg, int iArgNum)
		{
			string @string = arg.GetString(iArgNum, string.Empty);
			if (@string == null)
			{
				return null;
			}
			return global::BasePlayer.Find(@string);
		}

		// Token: 0x06002339 RID: 9017 RVA: 0x000C2F0C File Offset: 0x000C110C
		public static global::BasePlayer GetSleeper(this ConsoleSystem.Arg arg, int iArgNum)
		{
			string @string = arg.GetString(iArgNum, string.Empty);
			if (@string == null)
			{
				return null;
			}
			return global::BasePlayer.FindSleeping(@string);
		}

		// Token: 0x0600233A RID: 9018 RVA: 0x000C2F38 File Offset: 0x000C1138
		public static global::BasePlayer GetPlayerOrSleeper(this ConsoleSystem.Arg arg, int iArgNum)
		{
			string @string = arg.GetString(iArgNum, string.Empty);
			if (@string == null)
			{
				return null;
			}
			global::BasePlayer basePlayer = global::BasePlayer.Find(@string);
			return (!basePlayer) ? global::BasePlayer.FindSleeping(@string) : basePlayer;
		}
	}
}
