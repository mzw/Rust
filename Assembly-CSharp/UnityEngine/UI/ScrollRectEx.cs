﻿using System;
using Rust;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x020006FB RID: 1787
	[SelectionBase]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	[AddComponentMenu("UI/Scroll Rect Ex", 37)]
	public class ScrollRectEx : UIBehaviour, IInitializePotentialDragHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IScrollHandler, ICanvasElement, ILayoutGroup, IEventSystemHandler, ILayoutController
	{
		// Token: 0x060021D6 RID: 8662 RVA: 0x000BE154 File Offset: 0x000BC354
		protected ScrollRectEx()
		{
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x060021D7 RID: 8663 RVA: 0x000BE1DC File Offset: 0x000BC3DC
		// (set) Token: 0x060021D8 RID: 8664 RVA: 0x000BE1E4 File Offset: 0x000BC3E4
		public RectTransform content
		{
			get
			{
				return this.m_Content;
			}
			set
			{
				this.m_Content = value;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x060021D9 RID: 8665 RVA: 0x000BE1F0 File Offset: 0x000BC3F0
		// (set) Token: 0x060021DA RID: 8666 RVA: 0x000BE1F8 File Offset: 0x000BC3F8
		public bool horizontal
		{
			get
			{
				return this.m_Horizontal;
			}
			set
			{
				this.m_Horizontal = value;
			}
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x060021DB RID: 8667 RVA: 0x000BE204 File Offset: 0x000BC404
		// (set) Token: 0x060021DC RID: 8668 RVA: 0x000BE20C File Offset: 0x000BC40C
		public bool vertical
		{
			get
			{
				return this.m_Vertical;
			}
			set
			{
				this.m_Vertical = value;
			}
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x060021DD RID: 8669 RVA: 0x000BE218 File Offset: 0x000BC418
		// (set) Token: 0x060021DE RID: 8670 RVA: 0x000BE220 File Offset: 0x000BC420
		public ScrollRectEx.MovementType movementType
		{
			get
			{
				return this.m_MovementType;
			}
			set
			{
				this.m_MovementType = value;
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x060021DF RID: 8671 RVA: 0x000BE22C File Offset: 0x000BC42C
		// (set) Token: 0x060021E0 RID: 8672 RVA: 0x000BE234 File Offset: 0x000BC434
		public float elasticity
		{
			get
			{
				return this.m_Elasticity;
			}
			set
			{
				this.m_Elasticity = value;
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x060021E1 RID: 8673 RVA: 0x000BE240 File Offset: 0x000BC440
		// (set) Token: 0x060021E2 RID: 8674 RVA: 0x000BE248 File Offset: 0x000BC448
		public bool inertia
		{
			get
			{
				return this.m_Inertia;
			}
			set
			{
				this.m_Inertia = value;
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x060021E3 RID: 8675 RVA: 0x000BE254 File Offset: 0x000BC454
		// (set) Token: 0x060021E4 RID: 8676 RVA: 0x000BE25C File Offset: 0x000BC45C
		public float decelerationRate
		{
			get
			{
				return this.m_DecelerationRate;
			}
			set
			{
				this.m_DecelerationRate = value;
			}
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x060021E5 RID: 8677 RVA: 0x000BE268 File Offset: 0x000BC468
		// (set) Token: 0x060021E6 RID: 8678 RVA: 0x000BE270 File Offset: 0x000BC470
		public float scrollSensitivity
		{
			get
			{
				return this.m_ScrollSensitivity;
			}
			set
			{
				this.m_ScrollSensitivity = value;
			}
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x060021E7 RID: 8679 RVA: 0x000BE27C File Offset: 0x000BC47C
		// (set) Token: 0x060021E8 RID: 8680 RVA: 0x000BE284 File Offset: 0x000BC484
		public RectTransform viewport
		{
			get
			{
				return this.m_Viewport;
			}
			set
			{
				this.m_Viewport = value;
				this.SetDirtyCaching();
			}
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x060021E9 RID: 8681 RVA: 0x000BE294 File Offset: 0x000BC494
		// (set) Token: 0x060021EA RID: 8682 RVA: 0x000BE29C File Offset: 0x000BC49C
		public Scrollbar horizontalScrollbar
		{
			get
			{
				return this.m_HorizontalScrollbar;
			}
			set
			{
				if (this.m_HorizontalScrollbar)
				{
					this.m_HorizontalScrollbar.onValueChanged.RemoveListener(new UnityAction<float>(this.SetHorizontalNormalizedPosition));
				}
				this.m_HorizontalScrollbar = value;
				if (this.m_HorizontalScrollbar)
				{
					this.m_HorizontalScrollbar.onValueChanged.AddListener(new UnityAction<float>(this.SetHorizontalNormalizedPosition));
				}
				this.SetDirtyCaching();
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x060021EB RID: 8683 RVA: 0x000BE310 File Offset: 0x000BC510
		// (set) Token: 0x060021EC RID: 8684 RVA: 0x000BE318 File Offset: 0x000BC518
		public Scrollbar verticalScrollbar
		{
			get
			{
				return this.m_VerticalScrollbar;
			}
			set
			{
				if (this.m_VerticalScrollbar)
				{
					this.m_VerticalScrollbar.onValueChanged.RemoveListener(new UnityAction<float>(this.SetVerticalNormalizedPosition));
				}
				this.m_VerticalScrollbar = value;
				if (this.m_VerticalScrollbar)
				{
					this.m_VerticalScrollbar.onValueChanged.AddListener(new UnityAction<float>(this.SetVerticalNormalizedPosition));
				}
				this.SetDirtyCaching();
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x060021ED RID: 8685 RVA: 0x000BE38C File Offset: 0x000BC58C
		// (set) Token: 0x060021EE RID: 8686 RVA: 0x000BE394 File Offset: 0x000BC594
		public ScrollRectEx.ScrollbarVisibility horizontalScrollbarVisibility
		{
			get
			{
				return this.m_HorizontalScrollbarVisibility;
			}
			set
			{
				this.m_HorizontalScrollbarVisibility = value;
				this.SetDirtyCaching();
			}
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x060021EF RID: 8687 RVA: 0x000BE3A4 File Offset: 0x000BC5A4
		// (set) Token: 0x060021F0 RID: 8688 RVA: 0x000BE3AC File Offset: 0x000BC5AC
		public ScrollRectEx.ScrollbarVisibility verticalScrollbarVisibility
		{
			get
			{
				return this.m_VerticalScrollbarVisibility;
			}
			set
			{
				this.m_VerticalScrollbarVisibility = value;
				this.SetDirtyCaching();
			}
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x060021F1 RID: 8689 RVA: 0x000BE3BC File Offset: 0x000BC5BC
		// (set) Token: 0x060021F2 RID: 8690 RVA: 0x000BE3C4 File Offset: 0x000BC5C4
		public float horizontalScrollbarSpacing
		{
			get
			{
				return this.m_HorizontalScrollbarSpacing;
			}
			set
			{
				this.m_HorizontalScrollbarSpacing = value;
				this.SetDirty();
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x060021F3 RID: 8691 RVA: 0x000BE3D4 File Offset: 0x000BC5D4
		// (set) Token: 0x060021F4 RID: 8692 RVA: 0x000BE3DC File Offset: 0x000BC5DC
		public float verticalScrollbarSpacing
		{
			get
			{
				return this.m_VerticalScrollbarSpacing;
			}
			set
			{
				this.m_VerticalScrollbarSpacing = value;
				this.SetDirty();
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x060021F5 RID: 8693 RVA: 0x000BE3EC File Offset: 0x000BC5EC
		// (set) Token: 0x060021F6 RID: 8694 RVA: 0x000BE3F4 File Offset: 0x000BC5F4
		public ScrollRectEx.ScrollRectEvent onValueChanged
		{
			get
			{
				return this.m_OnValueChanged;
			}
			set
			{
				this.m_OnValueChanged = value;
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x060021F7 RID: 8695 RVA: 0x000BE400 File Offset: 0x000BC600
		protected RectTransform viewRect
		{
			get
			{
				if (this.m_ViewRect == null)
				{
					this.m_ViewRect = this.m_Viewport;
				}
				if (this.m_ViewRect == null)
				{
					this.m_ViewRect = (RectTransform)base.transform;
				}
				return this.m_ViewRect;
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x060021F8 RID: 8696 RVA: 0x000BE454 File Offset: 0x000BC654
		// (set) Token: 0x060021F9 RID: 8697 RVA: 0x000BE45C File Offset: 0x000BC65C
		public Vector2 velocity
		{
			get
			{
				return this.m_Velocity;
			}
			set
			{
				this.m_Velocity = value;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x060021FA RID: 8698 RVA: 0x000BE468 File Offset: 0x000BC668
		private RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x060021FB RID: 8699 RVA: 0x000BE490 File Offset: 0x000BC690
		public virtual void Rebuild(CanvasUpdate executing)
		{
			if (executing == null)
			{
				this.UpdateCachedData();
			}
			if (executing == 2)
			{
				this.UpdateBounds();
				this.UpdateScrollbars(Vector2.zero);
				this.UpdatePrevData();
				this.m_HasRebuiltLayout = true;
			}
		}

		// Token: 0x060021FC RID: 8700 RVA: 0x000BE4C4 File Offset: 0x000BC6C4
		private void UpdateCachedData()
		{
			Transform transform = base.transform;
			this.m_HorizontalScrollbarRect = ((!(this.m_HorizontalScrollbar == null)) ? (this.m_HorizontalScrollbar.transform as RectTransform) : null);
			this.m_VerticalScrollbarRect = ((!(this.m_VerticalScrollbar == null)) ? (this.m_VerticalScrollbar.transform as RectTransform) : null);
			bool flag = this.viewRect.parent == transform;
			bool flag2 = !this.m_HorizontalScrollbarRect || this.m_HorizontalScrollbarRect.parent == transform;
			bool flag3 = !this.m_VerticalScrollbarRect || this.m_VerticalScrollbarRect.parent == transform;
			bool flag4 = flag && flag2 && flag3;
			this.m_HSliderExpand = (flag4 && this.m_HorizontalScrollbarRect && this.horizontalScrollbarVisibility == ScrollRectEx.ScrollbarVisibility.AutoHideAndExpandViewport);
			this.m_VSliderExpand = (flag4 && this.m_VerticalScrollbarRect && this.verticalScrollbarVisibility == ScrollRectEx.ScrollbarVisibility.AutoHideAndExpandViewport);
			this.m_HSliderHeight = ((!(this.m_HorizontalScrollbarRect == null)) ? this.m_HorizontalScrollbarRect.rect.height : 0f);
			this.m_VSliderWidth = ((!(this.m_VerticalScrollbarRect == null)) ? this.m_VerticalScrollbarRect.rect.width : 0f);
		}

		// Token: 0x060021FD RID: 8701 RVA: 0x000BE65C File Offset: 0x000BC85C
		protected override void OnEnable()
		{
			base.OnEnable();
			if (this.m_HorizontalScrollbar)
			{
				this.m_HorizontalScrollbar.onValueChanged.AddListener(new UnityAction<float>(this.SetHorizontalNormalizedPosition));
			}
			if (this.m_VerticalScrollbar)
			{
				this.m_VerticalScrollbar.onValueChanged.AddListener(new UnityAction<float>(this.SetVerticalNormalizedPosition));
			}
			CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);
		}

		// Token: 0x060021FE RID: 8702 RVA: 0x000BE6D0 File Offset: 0x000BC8D0
		protected override void OnDisable()
		{
			if (Application.isQuitting)
			{
				return;
			}
			CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild(this);
			if (this.m_HorizontalScrollbar)
			{
				this.m_HorizontalScrollbar.onValueChanged.RemoveListener(new UnityAction<float>(this.SetHorizontalNormalizedPosition));
			}
			if (this.m_VerticalScrollbar)
			{
				this.m_VerticalScrollbar.onValueChanged.RemoveListener(new UnityAction<float>(this.SetVerticalNormalizedPosition));
			}
			this.m_HasRebuiltLayout = false;
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x060021FF RID: 8703 RVA: 0x000BE76C File Offset: 0x000BC96C
		public override bool IsActive()
		{
			return base.IsActive() && this.m_Content != null;
		}

		// Token: 0x06002200 RID: 8704 RVA: 0x000BE788 File Offset: 0x000BC988
		private void EnsureLayoutHasRebuilt()
		{
			if (!this.m_HasRebuiltLayout && !CanvasUpdateRegistry.IsRebuildingLayout())
			{
				Canvas.ForceUpdateCanvases();
			}
		}

		// Token: 0x06002201 RID: 8705 RVA: 0x000BE7A4 File Offset: 0x000BC9A4
		public virtual void StopMovement()
		{
			this.m_Velocity = Vector2.zero;
		}

		// Token: 0x06002202 RID: 8706 RVA: 0x000BE7B4 File Offset: 0x000BC9B4
		public virtual void OnScroll(PointerEventData data)
		{
			if (!this.IsActive())
			{
				return;
			}
			this.EnsureLayoutHasRebuilt();
			this.UpdateBounds();
			Vector2 scrollDelta = data.scrollDelta;
			scrollDelta.y *= -1f;
			if (this.vertical && !this.horizontal)
			{
				if (Mathf.Abs(scrollDelta.x) > Mathf.Abs(scrollDelta.y))
				{
					scrollDelta.y = scrollDelta.x;
				}
				scrollDelta.x = 0f;
			}
			if (this.horizontal && !this.vertical)
			{
				if (Mathf.Abs(scrollDelta.y) > Mathf.Abs(scrollDelta.x))
				{
					scrollDelta.x = scrollDelta.y;
				}
				scrollDelta.y = 0f;
			}
			Vector2 vector = this.m_Content.anchoredPosition;
			vector += scrollDelta * this.m_ScrollSensitivity;
			if (this.m_MovementType == ScrollRectEx.MovementType.Clamped)
			{
				vector += this.CalculateOffset(vector - this.m_Content.anchoredPosition);
			}
			this.SetContentAnchoredPosition(vector);
			this.UpdateBounds();
		}

		// Token: 0x06002203 RID: 8707 RVA: 0x000BE8E4 File Offset: 0x000BCAE4
		public virtual void OnInitializePotentialDrag(PointerEventData eventData)
		{
			if (eventData.button != this.scrollButton)
			{
				return;
			}
			this.m_Velocity = Vector2.zero;
		}

		// Token: 0x06002204 RID: 8708 RVA: 0x000BE904 File Offset: 0x000BCB04
		public virtual void OnBeginDrag(PointerEventData eventData)
		{
			if (eventData.button != this.scrollButton)
			{
				return;
			}
			if (!this.IsActive())
			{
				return;
			}
			this.UpdateBounds();
			this.m_PointerStartLocalCursor = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(this.viewRect, eventData.position, eventData.pressEventCamera, ref this.m_PointerStartLocalCursor);
			this.m_ContentStartPosition = this.m_Content.anchoredPosition;
			this.m_Dragging = true;
		}

		// Token: 0x06002205 RID: 8709 RVA: 0x000BE978 File Offset: 0x000BCB78
		public virtual void OnEndDrag(PointerEventData eventData)
		{
			if (eventData.button != this.scrollButton)
			{
				return;
			}
			this.m_Dragging = false;
		}

		// Token: 0x06002206 RID: 8710 RVA: 0x000BE994 File Offset: 0x000BCB94
		public virtual void OnDrag(PointerEventData eventData)
		{
			if (eventData.button != this.scrollButton)
			{
				return;
			}
			if (!this.IsActive())
			{
				return;
			}
			Vector2 vector;
			if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(this.viewRect, eventData.position, eventData.pressEventCamera, ref vector))
			{
				return;
			}
			this.UpdateBounds();
			Vector2 vector2 = vector - this.m_PointerStartLocalCursor;
			Vector2 vector3 = this.m_ContentStartPosition + vector2;
			Vector2 vector4 = this.CalculateOffset(vector3 - this.m_Content.anchoredPosition);
			vector3 += vector4;
			if (this.m_MovementType == ScrollRectEx.MovementType.Elastic)
			{
				if (vector4.x != 0f)
				{
					vector3.x -= ScrollRectEx.RubberDelta(vector4.x, this.m_ViewBounds.size.x);
				}
				if (vector4.y != 0f)
				{
					vector3.y -= ScrollRectEx.RubberDelta(vector4.y, this.m_ViewBounds.size.y);
				}
			}
			this.SetContentAnchoredPosition(vector3);
		}

		// Token: 0x06002207 RID: 8711 RVA: 0x000BEAB4 File Offset: 0x000BCCB4
		protected virtual void SetContentAnchoredPosition(Vector2 position)
		{
			if (!this.m_Horizontal)
			{
				position.x = this.m_Content.anchoredPosition.x;
			}
			if (!this.m_Vertical)
			{
				position.y = this.m_Content.anchoredPosition.y;
			}
			if (position != this.m_Content.anchoredPosition)
			{
				this.m_Content.anchoredPosition = position;
				this.UpdateBounds();
			}
		}

		// Token: 0x06002208 RID: 8712 RVA: 0x000BEB34 File Offset: 0x000BCD34
		protected virtual void LateUpdate()
		{
			if (!this.m_Content)
			{
				return;
			}
			this.EnsureLayoutHasRebuilt();
			this.UpdateScrollbarVisibility();
			this.UpdateBounds();
			float unscaledDeltaTime = Time.unscaledDeltaTime;
			Vector2 vector = this.CalculateOffset(Vector2.zero);
			if (!this.m_Dragging && (vector != Vector2.zero || this.m_Velocity != Vector2.zero))
			{
				Vector2 vector2 = this.m_Content.anchoredPosition;
				for (int i = 0; i < 2; i++)
				{
					if (this.m_MovementType == ScrollRectEx.MovementType.Elastic && vector[i] != 0f)
					{
						float num = this.m_Velocity[i];
						vector2[i] = Mathf.SmoothDamp(this.m_Content.anchoredPosition[i], this.m_Content.anchoredPosition[i] + vector[i], ref num, this.m_Elasticity, float.PositiveInfinity, unscaledDeltaTime);
						this.m_Velocity[i] = num;
					}
					else if (this.m_Inertia)
					{
						int num2;
						this.m_Velocity[num2 = i] = this.m_Velocity[num2] * Mathf.Pow(this.m_DecelerationRate, unscaledDeltaTime);
						if (Mathf.Abs(this.m_Velocity[i]) < 1f)
						{
							this.m_Velocity[i] = 0f;
						}
						int num3;
						vector2[num3 = i] = vector2[num3] + this.m_Velocity[i] * unscaledDeltaTime;
					}
					else
					{
						this.m_Velocity[i] = 0f;
					}
				}
				if (this.m_Velocity != Vector2.zero)
				{
					if (this.m_MovementType == ScrollRectEx.MovementType.Clamped)
					{
						vector = this.CalculateOffset(vector2 - this.m_Content.anchoredPosition);
						vector2 += vector;
					}
					this.SetContentAnchoredPosition(vector2);
				}
			}
			if (this.m_Dragging && this.m_Inertia)
			{
				Vector3 vector3 = (this.m_Content.anchoredPosition - this.m_PrevPosition) / unscaledDeltaTime;
				this.m_Velocity = Vector3.Lerp(this.m_Velocity, vector3, unscaledDeltaTime * 10f);
			}
			if (this.m_ViewBounds != this.m_PrevViewBounds || this.m_ContentBounds != this.m_PrevContentBounds || this.m_Content.anchoredPosition != this.m_PrevPosition)
			{
				this.UpdateScrollbars(vector);
				this.m_OnValueChanged.Invoke(this.normalizedPosition);
				this.UpdatePrevData();
			}
		}

		// Token: 0x06002209 RID: 8713 RVA: 0x000BEDF8 File Offset: 0x000BCFF8
		private void UpdatePrevData()
		{
			if (this.m_Content == null)
			{
				this.m_PrevPosition = Vector2.zero;
			}
			else
			{
				this.m_PrevPosition = this.m_Content.anchoredPosition;
			}
			this.m_PrevViewBounds = this.m_ViewBounds;
			this.m_PrevContentBounds = this.m_ContentBounds;
		}

		// Token: 0x0600220A RID: 8714 RVA: 0x000BEE50 File Offset: 0x000BD050
		private void UpdateScrollbars(Vector2 offset)
		{
			if (this.m_HorizontalScrollbar)
			{
				if (this.m_ContentBounds.size.x > 0f)
				{
					this.m_HorizontalScrollbar.size = Mathf.Clamp01((this.m_ViewBounds.size.x - Mathf.Abs(offset.x)) / this.m_ContentBounds.size.x);
				}
				else
				{
					this.m_HorizontalScrollbar.size = 1f;
				}
				this.m_HorizontalScrollbar.value = this.horizontalNormalizedPosition;
			}
			if (this.m_VerticalScrollbar)
			{
				if (this.m_ContentBounds.size.y > 0f)
				{
					this.m_VerticalScrollbar.size = Mathf.Clamp01((this.m_ViewBounds.size.y - Mathf.Abs(offset.y)) / this.m_ContentBounds.size.y);
				}
				else
				{
					this.m_VerticalScrollbar.size = 1f;
				}
				this.m_VerticalScrollbar.value = this.verticalNormalizedPosition;
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x0600220B RID: 8715 RVA: 0x000BEF90 File Offset: 0x000BD190
		// (set) Token: 0x0600220C RID: 8716 RVA: 0x000BEFA4 File Offset: 0x000BD1A4
		public Vector2 normalizedPosition
		{
			get
			{
				return new Vector2(this.horizontalNormalizedPosition, this.verticalNormalizedPosition);
			}
			set
			{
				this.SetNormalizedPosition(value.x, 0);
				this.SetNormalizedPosition(value.y, 1);
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x0600220D RID: 8717 RVA: 0x000BEFC4 File Offset: 0x000BD1C4
		// (set) Token: 0x0600220E RID: 8718 RVA: 0x000BF08C File Offset: 0x000BD28C
		public float horizontalNormalizedPosition
		{
			get
			{
				this.UpdateBounds();
				if (this.m_ContentBounds.size.x <= this.m_ViewBounds.size.x)
				{
					return (float)((this.m_ViewBounds.min.x <= this.m_ContentBounds.min.x) ? 0 : 1);
				}
				return (this.m_ViewBounds.min.x - this.m_ContentBounds.min.x) / (this.m_ContentBounds.size.x - this.m_ViewBounds.size.x);
			}
			set
			{
				this.SetNormalizedPosition(value, 0);
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x0600220F RID: 8719 RVA: 0x000BF098 File Offset: 0x000BD298
		// (set) Token: 0x06002210 RID: 8720 RVA: 0x000BF160 File Offset: 0x000BD360
		public float verticalNormalizedPosition
		{
			get
			{
				this.UpdateBounds();
				if (this.m_ContentBounds.size.y <= this.m_ViewBounds.size.y)
				{
					return (float)((this.m_ViewBounds.min.y <= this.m_ContentBounds.min.y) ? 0 : 1);
				}
				return (this.m_ViewBounds.min.y - this.m_ContentBounds.min.y) / (this.m_ContentBounds.size.y - this.m_ViewBounds.size.y);
			}
			set
			{
				this.SetNormalizedPosition(value, 1);
			}
		}

		// Token: 0x06002211 RID: 8721 RVA: 0x000BF16C File Offset: 0x000BD36C
		private void SetHorizontalNormalizedPosition(float value)
		{
			this.SetNormalizedPosition(value, 0);
		}

		// Token: 0x06002212 RID: 8722 RVA: 0x000BF178 File Offset: 0x000BD378
		private void SetVerticalNormalizedPosition(float value)
		{
			this.SetNormalizedPosition(value, 1);
		}

		// Token: 0x06002213 RID: 8723 RVA: 0x000BF184 File Offset: 0x000BD384
		private void SetNormalizedPosition(float value, int axis)
		{
			this.EnsureLayoutHasRebuilt();
			this.UpdateBounds();
			float num = this.m_ContentBounds.size[axis] - this.m_ViewBounds.size[axis];
			float num2 = this.m_ViewBounds.min[axis] - value * num;
			float num3 = this.m_Content.localPosition[axis] + num2 - this.m_ContentBounds.min[axis];
			Vector3 localPosition = this.m_Content.localPosition;
			if (Mathf.Abs(localPosition[axis] - num3) > 0.01f)
			{
				localPosition[axis] = num3;
				this.m_Content.localPosition = localPosition;
				this.m_Velocity[axis] = 0f;
				this.UpdateBounds();
			}
		}

		// Token: 0x06002214 RID: 8724 RVA: 0x000BF268 File Offset: 0x000BD468
		private static float RubberDelta(float overStretching, float viewSize)
		{
			return (1f - 1f / (Mathf.Abs(overStretching) * 0.55f / viewSize + 1f)) * viewSize * Mathf.Sign(overStretching);
		}

		// Token: 0x06002215 RID: 8725 RVA: 0x000BF294 File Offset: 0x000BD494
		protected override void OnRectTransformDimensionsChange()
		{
			this.SetDirty();
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06002216 RID: 8726 RVA: 0x000BF29C File Offset: 0x000BD49C
		private bool hScrollingNeeded
		{
			get
			{
				return !Application.isPlaying || this.m_ContentBounds.size.x > this.m_ViewBounds.size.x + 0.01f;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06002217 RID: 8727 RVA: 0x000BF2E4 File Offset: 0x000BD4E4
		private bool vScrollingNeeded
		{
			get
			{
				return !Application.isPlaying || this.m_ContentBounds.size.y > this.m_ViewBounds.size.y + 0.01f;
			}
		}

		// Token: 0x06002218 RID: 8728 RVA: 0x000BF32C File Offset: 0x000BD52C
		public virtual void SetLayoutHorizontal()
		{
			this.m_Tracker.Clear();
			if (this.m_HSliderExpand || this.m_VSliderExpand)
			{
				this.m_Tracker.Add(this, this.viewRect, 16134);
				this.viewRect.anchorMin = Vector2.zero;
				this.viewRect.anchorMax = Vector2.one;
				this.viewRect.sizeDelta = Vector2.zero;
				this.viewRect.anchoredPosition = Vector2.zero;
				LayoutRebuilder.ForceRebuildLayoutImmediate(this.content);
				this.m_ViewBounds = new Bounds(this.viewRect.rect.center, this.viewRect.rect.size);
				this.m_ContentBounds = this.GetBounds();
			}
			if (this.m_VSliderExpand && this.vScrollingNeeded)
			{
				this.viewRect.sizeDelta = new Vector2(-(this.m_VSliderWidth + this.m_VerticalScrollbarSpacing), this.viewRect.sizeDelta.y);
				LayoutRebuilder.ForceRebuildLayoutImmediate(this.content);
				this.m_ViewBounds = new Bounds(this.viewRect.rect.center, this.viewRect.rect.size);
				this.m_ContentBounds = this.GetBounds();
			}
			if (this.m_HSliderExpand && this.hScrollingNeeded)
			{
				this.viewRect.sizeDelta = new Vector2(this.viewRect.sizeDelta.x, -(this.m_HSliderHeight + this.m_HorizontalScrollbarSpacing));
				this.m_ViewBounds = new Bounds(this.viewRect.rect.center, this.viewRect.rect.size);
				this.m_ContentBounds = this.GetBounds();
			}
			if (this.m_VSliderExpand && this.vScrollingNeeded && this.viewRect.sizeDelta.x == 0f && this.viewRect.sizeDelta.y < 0f)
			{
				this.viewRect.sizeDelta = new Vector2(-(this.m_VSliderWidth + this.m_VerticalScrollbarSpacing), this.viewRect.sizeDelta.y);
			}
		}

		// Token: 0x06002219 RID: 8729 RVA: 0x000BF5B4 File Offset: 0x000BD7B4
		public virtual void SetLayoutVertical()
		{
			this.UpdateScrollbarLayout();
			this.m_ViewBounds = new Bounds(this.viewRect.rect.center, this.viewRect.rect.size);
			this.m_ContentBounds = this.GetBounds();
		}

		// Token: 0x0600221A RID: 8730 RVA: 0x000BF610 File Offset: 0x000BD810
		private void UpdateScrollbarVisibility()
		{
			if (this.m_VerticalScrollbar && this.m_VerticalScrollbarVisibility != ScrollRectEx.ScrollbarVisibility.Permanent && this.m_VerticalScrollbar.gameObject.activeSelf != this.vScrollingNeeded)
			{
				this.m_VerticalScrollbar.gameObject.SetActive(this.vScrollingNeeded);
			}
			if (this.m_HorizontalScrollbar && this.m_HorizontalScrollbarVisibility != ScrollRectEx.ScrollbarVisibility.Permanent && this.m_HorizontalScrollbar.gameObject.activeSelf != this.hScrollingNeeded)
			{
				this.m_HorizontalScrollbar.gameObject.SetActive(this.hScrollingNeeded);
			}
		}

		// Token: 0x0600221B RID: 8731 RVA: 0x000BF6B8 File Offset: 0x000BD8B8
		private void UpdateScrollbarLayout()
		{
			if (this.m_VSliderExpand && this.m_HorizontalScrollbar)
			{
				this.m_Tracker.Add(this, this.m_HorizontalScrollbarRect, 5378);
				this.m_HorizontalScrollbarRect.anchorMin = new Vector2(0f, this.m_HorizontalScrollbarRect.anchorMin.y);
				this.m_HorizontalScrollbarRect.anchorMax = new Vector2(1f, this.m_HorizontalScrollbarRect.anchorMax.y);
				this.m_HorizontalScrollbarRect.anchoredPosition = new Vector2(0f, this.m_HorizontalScrollbarRect.anchoredPosition.y);
				if (this.vScrollingNeeded)
				{
					this.m_HorizontalScrollbarRect.sizeDelta = new Vector2(-(this.m_VSliderWidth + this.m_VerticalScrollbarSpacing), this.m_HorizontalScrollbarRect.sizeDelta.y);
				}
				else
				{
					this.m_HorizontalScrollbarRect.sizeDelta = new Vector2(0f, this.m_HorizontalScrollbarRect.sizeDelta.y);
				}
			}
			if (this.m_HSliderExpand && this.m_VerticalScrollbar)
			{
				this.m_Tracker.Add(this, this.m_VerticalScrollbarRect, 10756);
				this.m_VerticalScrollbarRect.anchorMin = new Vector2(this.m_VerticalScrollbarRect.anchorMin.x, 0f);
				this.m_VerticalScrollbarRect.anchorMax = new Vector2(this.m_VerticalScrollbarRect.anchorMax.x, 1f);
				this.m_VerticalScrollbarRect.anchoredPosition = new Vector2(this.m_VerticalScrollbarRect.anchoredPosition.x, 0f);
				if (this.hScrollingNeeded)
				{
					this.m_VerticalScrollbarRect.sizeDelta = new Vector2(this.m_VerticalScrollbarRect.sizeDelta.x, -(this.m_HSliderHeight + this.m_HorizontalScrollbarSpacing));
				}
				else
				{
					this.m_VerticalScrollbarRect.sizeDelta = new Vector2(this.m_VerticalScrollbarRect.sizeDelta.x, 0f);
				}
			}
		}

		// Token: 0x0600221C RID: 8732 RVA: 0x000BF8F4 File Offset: 0x000BDAF4
		private void UpdateBounds()
		{
			this.m_ViewBounds = new Bounds(this.viewRect.rect.center, this.viewRect.rect.size);
			this.m_ContentBounds = this.GetBounds();
			if (this.m_Content == null)
			{
				return;
			}
			Vector3 size = this.m_ContentBounds.size;
			Vector3 center = this.m_ContentBounds.center;
			Vector3 vector = this.m_ViewBounds.size - size;
			if (vector.x > 0f)
			{
				center.x -= vector.x * (this.m_Content.pivot.x - 0.5f);
				size.x = this.m_ViewBounds.size.x;
			}
			if (vector.y > 0f)
			{
				center.y -= vector.y * (this.m_Content.pivot.y - 0.5f);
				size.y = this.m_ViewBounds.size.y;
			}
			this.m_ContentBounds.size = size;
			this.m_ContentBounds.center = center;
		}

		// Token: 0x0600221D RID: 8733 RVA: 0x000BFA58 File Offset: 0x000BDC58
		private Bounds GetBounds()
		{
			if (this.m_Content == null)
			{
				return default(Bounds);
			}
			Vector3 vector;
			vector..ctor(float.MaxValue, float.MaxValue, float.MaxValue);
			Vector3 vector2;
			vector2..ctor(float.MinValue, float.MinValue, float.MinValue);
			Matrix4x4 worldToLocalMatrix = this.viewRect.worldToLocalMatrix;
			this.m_Content.GetWorldCorners(this.m_Corners);
			for (int i = 0; i < 4; i++)
			{
				Vector3 vector3 = worldToLocalMatrix.MultiplyPoint3x4(this.m_Corners[i]);
				vector = Vector3.Min(vector3, vector);
				vector2 = Vector3.Max(vector3, vector2);
			}
			Bounds result;
			result..ctor(vector, Vector3.zero);
			result.Encapsulate(vector2);
			return result;
		}

		// Token: 0x0600221E RID: 8734 RVA: 0x000BFB24 File Offset: 0x000BDD24
		private Vector2 CalculateOffset(Vector2 delta)
		{
			Vector2 zero = Vector2.zero;
			if (this.m_MovementType == ScrollRectEx.MovementType.Unrestricted)
			{
				return zero;
			}
			Vector2 vector = this.m_ContentBounds.min;
			Vector2 vector2 = this.m_ContentBounds.max;
			if (this.m_Horizontal)
			{
				vector.x += delta.x;
				vector2.x += delta.x;
				if (vector.x > this.m_ViewBounds.min.x)
				{
					zero.x = this.m_ViewBounds.min.x - vector.x;
				}
				else if (vector2.x < this.m_ViewBounds.max.x)
				{
					zero.x = this.m_ViewBounds.max.x - vector2.x;
				}
			}
			if (this.m_Vertical)
			{
				vector.y += delta.y;
				vector2.y += delta.y;
				if (vector2.y < this.m_ViewBounds.max.y)
				{
					zero.y = this.m_ViewBounds.max.y - vector2.y;
				}
				else if (vector.y > this.m_ViewBounds.min.y)
				{
					zero.y = this.m_ViewBounds.min.y - vector.y;
				}
			}
			return zero;
		}

		// Token: 0x0600221F RID: 8735 RVA: 0x000BFCE8 File Offset: 0x000BDEE8
		protected void SetDirty()
		{
			if (!this.IsActive())
			{
				return;
			}
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
		}

		// Token: 0x06002220 RID: 8736 RVA: 0x000BFD04 File Offset: 0x000BDF04
		protected void SetDirtyCaching()
		{
			if (!this.IsActive())
			{
				return;
			}
			CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
		}

		// Token: 0x06002221 RID: 8737 RVA: 0x000BFD24 File Offset: 0x000BDF24
		public void CenterOnPosition(Vector2 pos)
		{
			RectTransform rectTransform = base.transform as RectTransform;
			Vector2 vector;
			vector..ctor(this.content.localScale.x, this.content.localScale.y);
			pos.x *= vector.x;
			pos.y *= vector.y;
			Vector2 vector2;
			vector2..ctor(this.content.rect.width * vector.x - rectTransform.rect.width, this.content.rect.height * vector.y - rectTransform.rect.height);
			pos.x = pos.x / vector2.x + this.content.pivot.x;
			pos.y = pos.y / vector2.y + this.content.pivot.y;
			if (this.movementType != ScrollRectEx.MovementType.Unrestricted)
			{
				pos.x = Mathf.Clamp(pos.x, 0f, 1f);
				pos.y = Mathf.Clamp(pos.y, 0f, 1f);
			}
			this.normalizedPosition = pos;
		}

		// Token: 0x06002222 RID: 8738 RVA: 0x000BFE98 File Offset: 0x000BE098
		public void LayoutComplete()
		{
		}

		// Token: 0x06002223 RID: 8739 RVA: 0x000BFE9C File Offset: 0x000BE09C
		public void GraphicUpdateComplete()
		{
		}

		// Token: 0x06002224 RID: 8740 RVA: 0x000BFEA0 File Offset: 0x000BE0A0
		Transform ICanvasElement.get_transform()
		{
			return base.transform;
		}

		// Token: 0x06002225 RID: 8741 RVA: 0x000BFEA8 File Offset: 0x000BE0A8
		bool ICanvasElement.IsDestroyed()
		{
			return base.IsDestroyed();
		}

		// Token: 0x04001E78 RID: 7800
		public PointerEventData.InputButton scrollButton;

		// Token: 0x04001E79 RID: 7801
		[SerializeField]
		private RectTransform m_Content;

		// Token: 0x04001E7A RID: 7802
		[SerializeField]
		private bool m_Horizontal = true;

		// Token: 0x04001E7B RID: 7803
		[SerializeField]
		private bool m_Vertical = true;

		// Token: 0x04001E7C RID: 7804
		[SerializeField]
		private ScrollRectEx.MovementType m_MovementType = ScrollRectEx.MovementType.Elastic;

		// Token: 0x04001E7D RID: 7805
		[SerializeField]
		private float m_Elasticity = 0.1f;

		// Token: 0x04001E7E RID: 7806
		[SerializeField]
		private bool m_Inertia = true;

		// Token: 0x04001E7F RID: 7807
		[SerializeField]
		private float m_DecelerationRate = 0.135f;

		// Token: 0x04001E80 RID: 7808
		[SerializeField]
		private float m_ScrollSensitivity = 1f;

		// Token: 0x04001E81 RID: 7809
		[SerializeField]
		private RectTransform m_Viewport;

		// Token: 0x04001E82 RID: 7810
		[SerializeField]
		private Scrollbar m_HorizontalScrollbar;

		// Token: 0x04001E83 RID: 7811
		[SerializeField]
		private Scrollbar m_VerticalScrollbar;

		// Token: 0x04001E84 RID: 7812
		[SerializeField]
		private ScrollRectEx.ScrollbarVisibility m_HorizontalScrollbarVisibility;

		// Token: 0x04001E85 RID: 7813
		[SerializeField]
		private ScrollRectEx.ScrollbarVisibility m_VerticalScrollbarVisibility;

		// Token: 0x04001E86 RID: 7814
		[SerializeField]
		private float m_HorizontalScrollbarSpacing;

		// Token: 0x04001E87 RID: 7815
		[SerializeField]
		private float m_VerticalScrollbarSpacing;

		// Token: 0x04001E88 RID: 7816
		[SerializeField]
		private ScrollRectEx.ScrollRectEvent m_OnValueChanged = new ScrollRectEx.ScrollRectEvent();

		// Token: 0x04001E89 RID: 7817
		private Vector2 m_PointerStartLocalCursor = Vector2.zero;

		// Token: 0x04001E8A RID: 7818
		private Vector2 m_ContentStartPosition = Vector2.zero;

		// Token: 0x04001E8B RID: 7819
		private RectTransform m_ViewRect;

		// Token: 0x04001E8C RID: 7820
		private Bounds m_ContentBounds;

		// Token: 0x04001E8D RID: 7821
		private Bounds m_ViewBounds;

		// Token: 0x04001E8E RID: 7822
		private Vector2 m_Velocity;

		// Token: 0x04001E8F RID: 7823
		private bool m_Dragging;

		// Token: 0x04001E90 RID: 7824
		private Vector2 m_PrevPosition = Vector2.zero;

		// Token: 0x04001E91 RID: 7825
		private Bounds m_PrevContentBounds;

		// Token: 0x04001E92 RID: 7826
		private Bounds m_PrevViewBounds;

		// Token: 0x04001E93 RID: 7827
		[NonSerialized]
		private bool m_HasRebuiltLayout;

		// Token: 0x04001E94 RID: 7828
		private bool m_HSliderExpand;

		// Token: 0x04001E95 RID: 7829
		private bool m_VSliderExpand;

		// Token: 0x04001E96 RID: 7830
		private float m_HSliderHeight;

		// Token: 0x04001E97 RID: 7831
		private float m_VSliderWidth;

		// Token: 0x04001E98 RID: 7832
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x04001E99 RID: 7833
		private RectTransform m_HorizontalScrollbarRect;

		// Token: 0x04001E9A RID: 7834
		private RectTransform m_VerticalScrollbarRect;

		// Token: 0x04001E9B RID: 7835
		private DrivenRectTransformTracker m_Tracker;

		// Token: 0x04001E9C RID: 7836
		private readonly Vector3[] m_Corners = new Vector3[4];

		// Token: 0x020006FC RID: 1788
		public enum MovementType
		{
			// Token: 0x04001E9E RID: 7838
			Unrestricted,
			// Token: 0x04001E9F RID: 7839
			Elastic,
			// Token: 0x04001EA0 RID: 7840
			Clamped
		}

		// Token: 0x020006FD RID: 1789
		public enum ScrollbarVisibility
		{
			// Token: 0x04001EA2 RID: 7842
			Permanent,
			// Token: 0x04001EA3 RID: 7843
			AutoHide,
			// Token: 0x04001EA4 RID: 7844
			AutoHideAndExpandViewport
		}

		// Token: 0x020006FE RID: 1790
		[Serializable]
		public class ScrollRectEvent : UnityEvent<Vector2>
		{
		}
	}
}
