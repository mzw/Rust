﻿using System;

namespace UnityEngine
{
	// Token: 0x02000761 RID: 1889
	public static class ColliderEx
	{
		// Token: 0x06002343 RID: 9027 RVA: 0x000C3230 File Offset: 0x000C1430
		public static PhysicMaterial GetMaterialAt(this Collider obj, Vector3 pos)
		{
			if (obj is TerrainCollider)
			{
				return global::TerrainMeta.Physics.GetMaterial(pos);
			}
			return obj.sharedMaterial;
		}
	}
}
