﻿using System;
using Facepunch;

namespace UnityEngine
{
	// Token: 0x02000764 RID: 1892
	public static class ComponentEx
	{
		// Token: 0x06002347 RID: 9031 RVA: 0x000C33C8 File Offset: 0x000C15C8
		public static T Instantiate<T>(this T component) where T : Component
		{
			return Facepunch.Instantiate.GameObject(component.gameObject, null).GetComponent<T>();
		}
	}
}
