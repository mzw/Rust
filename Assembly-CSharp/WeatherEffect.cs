﻿using System;
using UnityEngine;

// Token: 0x020004AB RID: 1195
public abstract class WeatherEffect : MonoBehaviour, IClientComponent
{
	// Token: 0x0400148B RID: 5259
	public ParticleSystem[] emitOnStart;

	// Token: 0x0400148C RID: 5260
	public ParticleSystem[] emitOnStop;

	// Token: 0x0400148D RID: 5261
	public ParticleSystem[] emitOnLoop;
}
