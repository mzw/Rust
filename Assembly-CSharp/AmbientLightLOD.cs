﻿using System;
using UnityEngine;

// Token: 0x0200073B RID: 1851
public class AmbientLightLOD : MonoBehaviour, global::ILOD, IClientComponent
{
	// Token: 0x060022CA RID: 8906 RVA: 0x000C18F8 File Offset: 0x000BFAF8
	protected void OnValidate()
	{
		global::LightEx.CheckConflict(base.gameObject);
	}

	// Token: 0x04001F3D RID: 7997
	public float enabledRadius = 20f;
}
