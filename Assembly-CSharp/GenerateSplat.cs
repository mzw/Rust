﻿using System;
using UnityEngine;

// Token: 0x020005BA RID: 1466
public class GenerateSplat : global::ProceduralComponent
{
	// Token: 0x06001E75 RID: 7797 RVA: 0x000AA6D4 File Offset: 0x000A88D4
	public override void Process(uint seed)
	{
		byte[,,] map = global::TerrainMeta.SplatMap.dst;
		int res = global::TerrainMeta.SplatMap.res;
		global::TerrainSplatMap splatmap = global::TerrainMeta.SplatMap;
		global::TerrainTopologyMap topomap = global::TerrainMeta.TopologyMap;
		global::TerrainBiomeMap biomemap = global::TerrainMeta.BiomeMap;
		global::TerrainHeightMap heightmap = global::TerrainMeta.HeightMap;
		float noiseX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float noiseZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		Parallel.For(0, res, delegate(int z)
		{
			for (int i = 0; i < res; i++)
			{
				float normX = splatmap.Coordinate(i);
				float normZ = splatmap.Coordinate(z);
				float num = global::TerrainMeta.DenormalizeX(normX);
				float num2 = global::TerrainMeta.DenormalizeZ(normZ);
				float height = heightmap.GetHeight01(normX, normZ);
				float slope = heightmap.GetSlope(normX, normZ);
				int topology = topomap.GetTopology(normX, normZ);
				float biome = biomemap.GetBiome(normX, normZ, 8);
				float biome2 = biomemap.GetBiome(normX, normZ, 4);
				float biome3 = biomemap.GetBiome(normX, normZ, 2);
				float biome4 = biomemap.GetBiome(normX, normZ, 1);
				float num3 = 0f;
				float num4 = 0f;
				float num5 = 0f;
				float num6 = 0f;
				float num7 = 0f;
				float num8 = 0f;
				float num9 = 0f;
				float num10 = 0f;
				float num11 = 0f;
				if ((topology & 32) != 0)
				{
					num11 = 1f;
				}
				else if ((topology & 64) != 0)
				{
					num11 = 0.25f + 0.5f * Mathf.Clamp01(global::Noise.Turbulence((double)(num + noiseX), (double)(num2 + noiseZ), 4, 0.02500000037252903, 1.25, 2.0, 0.5) - 0.25f);
				}
				if (slope > 40f)
				{
					float num12 = (1f - num3) * Mathf.InverseLerp(40f, 60f, slope);
					num4 += num12;
					num3 = num4 + num5 + num6 + num7 + num8 + num9 + num10;
				}
				if (biome > 0f)
				{
					float num13 = (1f - num3) * biome;
					num10 += num13;
					num3 = num4 + num5 + num6 + num7 + num8 + num9 + num10;
				}
				float num14 = (1f - num3) * Mathf.InverseLerp(0.502f, 0.501f, height);
				float num15 = Mathf.Clamp01(global::Noise.Billow((double)(num + noiseX), (double)(num2 + noiseZ), 4, 0.039999999105930328, 0.44999998807907104, 2.0, 0.5) * Mathf.InverseLerp(0.499f, 0.501f, height));
				num9 += num14 * num15;
				num6 += num14 * (1f - num15);
				num3 = num4 + num5 + num6 + num7 + num8 + num9 + num10;
				if (biome2 > 0f)
				{
					float num16 = (1f - num3) * biome2;
					num8 += num16 * num11;
					num5 += num16 * (1f - num11);
					num3 = num4 + num5 + num6 + num7 + num8 + num9 + num10;
				}
				if (biome3 > 0f)
				{
					float num17 = (1f - num3) * biome3;
					num8 += num17 * num11;
					num5 += num17 * (1f - num11);
					num3 = num4 + num5 + num6 + num7 + num8 + num9 + num10;
				}
				if (biome4 > 0f)
				{
					float num18 = (1f - num3) * biome4;
					float num19 = Mathf.Clamp01(global::Noise.Turbulence((double)(num + noiseX), (double)(num2 + noiseZ), 4, 0.0099999997764825821, 1.5, 2.0, 0.5));
					num8 += num18 * num11;
					num5 += num18 * (1f - num11) * num19;
					num6 += num18 * (1f - num11) * (1f - num19);
					num3 = num4 + num5 + num5 + num7 + num8 + num9 + num10;
				}
				map[3, z, i] = global::TextureData.Float2Byte(num4);
				map[4, z, i] = global::TextureData.Float2Byte(num5);
				map[2, z, i] = global::TextureData.Float2Byte(num6);
				map[0, z, i] = global::TextureData.Float2Byte(num7);
				map[5, z, i] = global::TextureData.Float2Byte(num8);
				map[6, z, i] = global::TextureData.Float2Byte(num9);
				map[1, z, i] = global::TextureData.Float2Byte(num10);
			}
		});
	}
}
