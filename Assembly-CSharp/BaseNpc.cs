﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Apex.AI;
using Apex.AI.Components;
using Apex.LoadBalancing;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

// Token: 0x02000034 RID: 52
public class BaseNpc : global::BaseCombatEntity, IContextProvider, global::IAIAgent, ILoadBalanced
{
	// Token: 0x0600047F RID: 1151 RVA: 0x0001A3EC File Offset: 0x000185EC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseNpc.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x17000013 RID: 19
	// (get) Token: 0x06000480 RID: 1152 RVA: 0x0001A434 File Offset: 0x00018634
	// (set) Token: 0x06000481 RID: 1153 RVA: 0x0001A43C File Offset: 0x0001863C
	public int AgentTypeIndex
	{
		get
		{
			return this.agentTypeIndex;
		}
		set
		{
			this.agentTypeIndex = value;
		}
	}

	// Token: 0x17000014 RID: 20
	// (get) Token: 0x06000482 RID: 1154 RVA: 0x0001A448 File Offset: 0x00018648
	// (set) Token: 0x06000483 RID: 1155 RVA: 0x0001A450 File Offset: 0x00018650
	public bool IsStuck { get; set; }

	// Token: 0x17000015 RID: 21
	// (get) Token: 0x06000484 RID: 1156 RVA: 0x0001A45C File Offset: 0x0001865C
	// (set) Token: 0x06000485 RID: 1157 RVA: 0x0001A464 File Offset: 0x00018664
	public Vector2i CurrentCoord { get; set; }

	// Token: 0x17000016 RID: 22
	// (get) Token: 0x06000486 RID: 1158 RVA: 0x0001A470 File Offset: 0x00018670
	// (set) Token: 0x06000487 RID: 1159 RVA: 0x0001A478 File Offset: 0x00018678
	public Vector2i PreviousCoord { get; set; }

	// Token: 0x17000017 RID: 23
	// (get) Token: 0x06000488 RID: 1160 RVA: 0x0001A484 File Offset: 0x00018684
	// (set) Token: 0x06000489 RID: 1161 RVA: 0x0001A48C File Offset: 0x0001868C
	public bool AgencyUpdateRequired { get; set; }

	// Token: 0x17000018 RID: 24
	// (get) Token: 0x0600048A RID: 1162 RVA: 0x0001A498 File Offset: 0x00018698
	// (set) Token: 0x0600048B RID: 1163 RVA: 0x0001A4A0 File Offset: 0x000186A0
	public bool IsOnOffmeshLinkAndReachedNewCoord { get; set; }

	// Token: 0x0600048C RID: 1164 RVA: 0x0001A4AC File Offset: 0x000186AC
	public override string DebugText()
	{
		string text = base.DebugText();
		text += string.Format("\nBehaviour: {0}", this.CurrentBehaviour);
		text += string.Format("\nAttackTarget: {0}", this.AttackTarget);
		text += string.Format("\nFoodTarget: {0}", this.FoodTarget);
		text += string.Format("\nSleep: {0:0.00}", this.Sleep);
		if (this.AiContext != null)
		{
			text += string.Format("\nVisible Ents: {0}", this.AiContext.Memory.Visible.Count);
		}
		return text;
	}

	// Token: 0x0600048D RID: 1165 RVA: 0x0001A560 File Offset: 0x00018760
	public void TickAi()
	{
		if (!ConVar.AI.think)
		{
			return;
		}
		using (TimeWarning.New("TickNavigation", 0.1f))
		{
			this.TickNavigation();
		}
		if (!Rust.Ai.AiManager.ai_dormant || this.GetNavAgent.enabled)
		{
			using (TimeWarning.New("TickMetabolism", 0.1f))
			{
				this.TickSleep();
				this.TickMetabolism();
				this.TickSpeed();
			}
		}
	}

	// Token: 0x0600048E RID: 1166 RVA: 0x0001A60C File Offset: 0x0001880C
	private void TickSpeed()
	{
		float num = this.Stats.Speed;
		if (this.NewAI)
		{
			num = this.TargetSpeed;
			num *= 0.5f + base.healthFraction * 0.5f;
			this.NavAgent.speed = Mathf.Lerp(this.NavAgent.speed, num, 0.5f);
			this.NavAgent.angularSpeed = this.Stats.TurnSpeed;
			this.NavAgent.acceleration = this.Stats.Acceleration;
			return;
		}
		num *= 0.5f + base.healthFraction * 0.5f;
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Idle)
		{
			num *= 0.2f;
		}
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Eat)
		{
			num *= 0.3f;
		}
		float num2 = Mathf.Min(this.NavAgent.speed / this.Stats.Speed, 1f);
		num2 = global::BaseNpc.speedFractionResponse.Evaluate(num2);
		float num3 = 1f - 0.9f * Vector3.Angle(base.transform.forward, (this.NavAgent.nextPosition - this.ServerPosition).normalized) / 180f * num2 * num2;
		num *= num3;
		this.NavAgent.speed = Mathf.Lerp(this.NavAgent.speed, num, 0.5f);
		this.NavAgent.angularSpeed = this.Stats.TurnSpeed * (1.1f - num2);
		this.NavAgent.acceleration = this.Stats.Acceleration;
	}

	// Token: 0x0600048F RID: 1167 RVA: 0x0001A7A4 File Offset: 0x000189A4
	protected virtual void TickMetabolism()
	{
		float num = 0.000166666665f;
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
		{
			num *= 0.01f;
		}
		if (this.NavAgent.velocity.sqrMagnitude > 0.1f)
		{
			num *= 2f;
		}
		this.Energy.Add(num * 0.1f * -1f);
		if (this.Stamina.TimeSinceUsed > 5f)
		{
			float num2 = 0.06666667f;
			this.Stamina.Add(0.1f * num2);
		}
		if (base.SecondsSinceAttacked > 60f)
		{
		}
	}

	// Token: 0x06000490 RID: 1168 RVA: 0x0001A848 File Offset: 0x00018A48
	public virtual bool WantsToEat(global::BaseEntity best)
	{
		object obj = Interface.CallHook("CanNpcEat", new object[]
		{
			this,
			best
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return best.HasTrait(global::BaseEntity.TraitFlag.Food) && !best.HasTrait(global::BaseEntity.TraitFlag.Alive);
	}

	// Token: 0x06000491 RID: 1169 RVA: 0x0001A8A0 File Offset: 0x00018AA0
	public virtual float FearLevel(global::BaseEntity ent)
	{
		float num = 0f;
		global::BaseNpc baseNpc = ent as global::BaseNpc;
		if (baseNpc != null && baseNpc.Stats.Size > this.Stats.Size)
		{
			if (baseNpc.WantsToAttack(this) > 0.25f)
			{
				num += 0.2f;
			}
			if (baseNpc.AttackTarget == this)
			{
				num += 0.3f;
			}
			if (baseNpc.CurrentBehaviour == global::BaseNpc.Behaviour.Attack)
			{
				num *= 1.5f;
			}
			if (baseNpc.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
			{
				num *= 0.1f;
			}
		}
		global::BasePlayer basePlayer = ent as global::BasePlayer;
		if (basePlayer != null)
		{
			num += 1f;
		}
		return num;
	}

	// Token: 0x06000492 RID: 1170 RVA: 0x0001A958 File Offset: 0x00018B58
	public virtual float HateLevel(global::BaseEntity ent)
	{
		return 0f;
	}

	// Token: 0x06000493 RID: 1171 RVA: 0x0001A960 File Offset: 0x00018B60
	protected virtual void TickSleep()
	{
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
		{
			this.IsSleeping = true;
			this.Sleep += 0.000333333359f;
		}
		else
		{
			this.IsSleeping = false;
			this.Sleep -= 2.77777781E-05f;
		}
		this.Sleep = Mathf.Clamp01(this.Sleep);
	}

	// Token: 0x06000494 RID: 1172 RVA: 0x0001A9C4 File Offset: 0x00018BC4
	public void TickNavigation()
	{
		if (!ConVar.AI.move)
		{
			return;
		}
		if (!this.IsNavRunning())
		{
			return;
		}
		Vector3 position = base.transform.position;
		this.stepDirection = Vector3.zero;
		if (Rust.Ai.AiManager.nav_grid)
		{
			this.UpdateCoords();
		}
		if (this.ChaseTransform)
		{
			this.TickChase();
		}
		if (this.NavAgent.isOnOffMeshLink)
		{
			this.TickNavMeshLinkTraversal(ref position);
		}
		else if (this.NavAgent.hasPath)
		{
			this.TickFollowPath(ref position);
		}
		if (!this.ValidateNextPosition(ref position))
		{
			return;
		}
		this.UpdatePositionAndRotation(position);
		this.TickIdle();
		this.TickStuck();
	}

	// Token: 0x06000495 RID: 1173 RVA: 0x0001AA7C File Offset: 0x00018C7C
	private void UpdateCoords()
	{
		this.PreviousCoord = this.CurrentCoord;
		this.CurrentCoord = SingletonComponent<Rust.Ai.AiManager>.Instance.GetCoord(this.ServerPosition);
		if (this.CurrentCoord.x != this.PreviousCoord.x || this.CurrentCoord.y != this.PreviousCoord.y)
		{
			this.AgencyUpdateRequired = true;
			if (this.NavAgent.isOnOffMeshLink)
			{
				this.IsOnOffmeshLinkAndReachedNewCoord = true;
			}
		}
	}

	// Token: 0x06000496 RID: 1174 RVA: 0x0001AB0C File Offset: 0x00018D0C
	private void TickChase()
	{
		Vector3 vector = this.ChaseTransform.position;
		Vector3 vector2 = base.transform.position - vector;
		if ((double)vector2.magnitude < 5.0)
		{
			vector += vector2.normalized * this.AttackOffset.z;
		}
		if ((this.NavAgent.destination - vector).sqrMagnitude > 0.0100000007f)
		{
			this.NavAgent.SetDestination(vector);
		}
	}

	// Token: 0x06000497 RID: 1175 RVA: 0x0001AB9C File Offset: 0x00018D9C
	private void TickNavMeshLinkTraversal(ref Vector3 moveToPosition)
	{
		OffMeshLinkData currentOffMeshLinkData = this.NavAgent.currentOffMeshLinkData;
		if (!currentOffMeshLinkData.valid)
		{
			this.CompleteNavMeshLinkTraversal(true, ref moveToPosition);
		}
		else
		{
			Vector3 vector = currentOffMeshLinkData.endPos + Vector3.up * this.NavAgent.baseOffset;
			NavMeshHit navMeshHit;
			if (NavMesh.SamplePosition(vector, ref navMeshHit, this.NavAgent.height * 2f, this.NavAgent.areaMask))
			{
				vector = navMeshHit.position;
				if (this.IsNavMeshLinkTraversalComplete(moveToPosition, vector))
				{
					this.CompleteNavMeshLinkTraversal(false, ref moveToPosition);
				}
				else
				{
					this.TraverseLink(ref moveToPosition, vector);
				}
			}
			else
			{
				this.CompleteNavMeshLinkTraversal(true, ref moveToPosition);
			}
		}
	}

	// Token: 0x06000498 RID: 1176 RVA: 0x0001AC58 File Offset: 0x00018E58
	private bool IsNavMeshLinkTraversalComplete(Vector3 moveToPosition, Vector3 targetPosition)
	{
		return (Rust.Ai.AiManager.nav_grid && this.IsOnOffmeshLinkAndReachedNewCoord) || (!Rust.Ai.AiManager.nav_grid && Vector3Ex.Distance2D(moveToPosition, targetPosition) < 0.15f);
	}

	// Token: 0x06000499 RID: 1177 RVA: 0x0001AC90 File Offset: 0x00018E90
	private void TraverseLink(ref Vector3 moveToPosition, Vector3 targetPosition)
	{
		float num = Mathf.Clamp01(this.NavAgent.speed * 0.1f);
		moveToPosition = Vector3.MoveTowards(moveToPosition, targetPosition, num);
		RaycastHit raycastHit;
		if (global::TransformUtil.GetGroundInfo(moveToPosition + Vector3.up * this.NavAgent.height * 0.5f, out raycastHit, this.NavAgent.height, this.movementMask, base.transform))
		{
			moveToPosition = raycastHit.point;
		}
		this.stepDirection = (moveToPosition - base.transform.position).normalized;
	}

	// Token: 0x0600049A RID: 1178 RVA: 0x0001AD48 File Offset: 0x00018F48
	private void CompleteNavMeshLinkTraversal(bool failed, ref Vector3 moveToPosition)
	{
		this.NavAgent.CompleteOffMeshLink();
		this.IsOnOffmeshLinkAndReachedNewCoord = false;
		if (failed)
		{
			this.NavAgent.ResetPath();
			this.stepDirection = Vector3.zero;
		}
		else if (this.NavAgent.hasPath)
		{
			moveToPosition = this.NavAgent.nextPosition;
			this.stepDirection = this.NavAgent.velocity.normalized;
		}
		this.Resume();
	}

	// Token: 0x0600049B RID: 1179 RVA: 0x0001ADC8 File Offset: 0x00018FC8
	private void TickFollowPath(ref Vector3 moveToPosition)
	{
		moveToPosition = this.NavAgent.nextPosition;
		this.stepDirection = this.NavAgent.velocity.normalized;
	}

	// Token: 0x0600049C RID: 1180 RVA: 0x0001AE00 File Offset: 0x00019000
	private bool ValidateNextPosition(ref Vector3 moveToPosition)
	{
		if (!global::ValidBounds.Test(moveToPosition))
		{
			Debug.Log(string.Concat(new object[]
			{
				"Invalid NavAgent Position: ",
				this,
				" ",
				moveToPosition,
				" (destroying)"
			}));
			base.Kill(global::BaseNetworkable.DestroyMode.None);
			return false;
		}
		return true;
	}

	// Token: 0x0600049D RID: 1181 RVA: 0x0001AE64 File Offset: 0x00019064
	private void UpdatePositionAndRotation(Vector3 moveToPosition)
	{
		this.ServerPosition = moveToPosition;
		this.UpdateAiRotation();
	}

	// Token: 0x0600049E RID: 1182 RVA: 0x0001AE74 File Offset: 0x00019074
	private void TickIdle()
	{
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Idle)
		{
			this.idleDuration += 0.1f;
		}
		else
		{
			this.idleDuration = 0f;
		}
	}

	// Token: 0x0600049F RID: 1183 RVA: 0x0001AEA4 File Offset: 0x000190A4
	public void TickStuck()
	{
		if (this.IsNavRunning() && !this.NavAgent.isStopped && (this.lastStuckPos - this.ServerPosition).sqrMagnitude < 0.0625f && this.AttackReady())
		{
			this.stuckDuration += 0.1f;
			if (this.stuckDuration >= 5f && Mathf.Approximately(this.lastStuckTime, 0f))
			{
				this.lastStuckTime = UnityEngine.Time.time;
				this.OnBecomeStuck();
			}
		}
		else
		{
			this.stuckDuration = 0f;
			this.lastStuckPos = this.ServerPosition;
			if (UnityEngine.Time.time - this.lastStuckTime > 5f)
			{
				this.lastStuckTime = 0f;
				this.OnBecomeUnStuck();
			}
		}
	}

	// Token: 0x060004A0 RID: 1184 RVA: 0x0001AF88 File Offset: 0x00019188
	public void OnBecomeStuck()
	{
		this.IsStuck = true;
	}

	// Token: 0x060004A1 RID: 1185 RVA: 0x0001AF94 File Offset: 0x00019194
	public void OnBecomeUnStuck()
	{
		this.IsStuck = false;
	}

	// Token: 0x060004A2 RID: 1186 RVA: 0x0001AFA0 File Offset: 0x000191A0
	public void UpdateAiRotation()
	{
		if (!this.IsNavRunning())
		{
			return;
		}
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
		{
			return;
		}
		if ((this.NavAgent.destination - this.ServerPosition).sqrMagnitude > 1f)
		{
			Vector3 vector = this.stepDirection;
			float sqrMagnitude = vector.sqrMagnitude;
			if (sqrMagnitude > 0.001f)
			{
				this.ServerRotation = Quaternion.LookRotation(vector);
				return;
			}
		}
		if (this.ChaseTransform && this.CurrentBehaviour == global::BaseNpc.Behaviour.Attack)
		{
			Vector3 vector2 = this.ChaseTransform.localPosition - this.ServerPosition;
			float magnitude = vector2.magnitude;
			if (magnitude < 3f && magnitude > 0.001f)
			{
				this.ServerRotation = Quaternion.LookRotation(vector2.normalized);
				return;
			}
		}
		if (this.AttackTarget && this.CurrentBehaviour == global::BaseNpc.Behaviour.Attack)
		{
			Vector3 vector3 = this.AttackTarget.ServerPosition - this.ServerPosition;
			float magnitude2 = vector3.magnitude;
			if (magnitude2 < 3f && magnitude2 > 0.001f)
			{
				this.ServerRotation = Quaternion.LookRotation(vector3.normalized);
				return;
			}
		}
	}

	// Token: 0x17000019 RID: 25
	// (get) Token: 0x060004A3 RID: 1187 RVA: 0x0001B0E4 File Offset: 0x000192E4
	public float GetAttackRate
	{
		get
		{
			return this.AttackRate;
		}
	}

	// Token: 0x060004A4 RID: 1188 RVA: 0x0001B0EC File Offset: 0x000192EC
	public bool AttackReady()
	{
		return UnityEngine.Time.realtimeSinceStartup >= this.nextAttackTime;
	}

	// Token: 0x060004A5 RID: 1189 RVA: 0x0001B100 File Offset: 0x00019300
	public virtual void StartAttack()
	{
		if (!this.AttackTarget)
		{
			return;
		}
		if (!this.AttackReady())
		{
			return;
		}
		if (Interface.CallHook("CanNpcAttack", new object[]
		{
			this,
			this.AttackTarget
		}) != null)
		{
			return;
		}
		float magnitude = (this.AttackTarget.ServerPosition - this.ServerPosition).magnitude;
		if (magnitude > this.AttackRange)
		{
			return;
		}
		this.nextAttackTime = UnityEngine.Time.realtimeSinceStartup + this.AttackRate;
		global::BaseCombatEntity combatTarget = this.CombatTarget;
		if (!combatTarget)
		{
			return;
		}
		combatTarget.Hurt(this.AttackDamage, this.AttackDamageType, this, true);
		this.Stamina.Use(this.AttackCost);
		this.BusyTimer.Activate(0.5f, null);
		base.SignalBroadcast(global::BaseEntity.Signal.Attack, null);
		base.ClientRPC<Vector3>(null, "Attack", this.AttackTarget.ServerPosition);
	}

	// Token: 0x060004A6 RID: 1190 RVA: 0x0001B1F4 File Offset: 0x000193F4
	public void StartAttack(Rust.Ai.AttackOperator.AttackType type, global::BaseCombatEntity target)
	{
		if (target == null)
		{
			return;
		}
		if (this.GetFact(global::BaseNpc.Facts.IsAttackReady) == 0)
		{
			return;
		}
		Vector3 vector = target.ServerPosition - this.ServerPosition;
		float magnitude = vector.magnitude;
		if (magnitude > this.AttackRange)
		{
			return;
		}
		if (magnitude > 0.001f)
		{
			this.ServerRotation = Quaternion.LookRotation(vector.normalized);
		}
		this.nextAttackTime = UnityEngine.Time.realtimeSinceStartup + this.AttackRate;
		target.Hurt(this.AttackDamage, this.AttackDamageType, this, true);
		this.Stamina.Use(this.AttackCost);
		base.SignalBroadcast(global::BaseEntity.Signal.Attack, null);
		base.ClientRPC<Vector3>(null, "Attack", target.ServerPosition);
	}

	// Token: 0x060004A7 RID: 1191 RVA: 0x0001B2B0 File Offset: 0x000194B0
	public virtual void Eat()
	{
		if (!this.FoodTarget)
		{
			return;
		}
		this.BusyTimer.Activate(0.5f, null);
		this.FoodTarget.Eat(this, 0.5f);
		this.StartEating(Random.value * 5f + 0.5f);
		base.ClientRPC<Vector3>(null, "Eat", this.FoodTarget.transform.position);
	}

	// Token: 0x060004A8 RID: 1192 RVA: 0x0001B324 File Offset: 0x00019524
	public virtual void AddCalories(float amount)
	{
		this.Energy.Add(amount / 1000f);
	}

	// Token: 0x060004A9 RID: 1193 RVA: 0x0001B338 File Offset: 0x00019538
	public virtual void Startled()
	{
		base.ClientRPC<Vector3>(null, "Startled", base.transform.position);
	}

	// Token: 0x060004AA RID: 1194 RVA: 0x0001B354 File Offset: 0x00019554
	private bool IsAfraid()
	{
		if (this.GetFact(global::BaseNpc.Facts.AfraidRange) == 0)
		{
			if (this.AiContext.EnemyNpc != null && this.IsAfraidOf(this.AiContext.EnemyNpc.Stats.Family))
			{
				this.SetFact(global::BaseNpc.Facts.IsAfraid, 1, true, true);
				return true;
			}
			if (this.AiContext.EnemyPlayer != null && this.IsAfraidOf(this.AiContext.EnemyPlayer.Family))
			{
				this.SetFact(global::BaseNpc.Facts.IsAfraid, 1, true, true);
				return true;
			}
		}
		this.SetFact(global::BaseNpc.Facts.IsAfraid, 0, true, true);
		return false;
	}

	// Token: 0x060004AB RID: 1195 RVA: 0x0001B3FC File Offset: 0x000195FC
	private bool IsAfraidOf(global::BaseNpc.AiStatistics.FamilyEnum family)
	{
		foreach (global::BaseNpc.AiStatistics.FamilyEnum familyEnum in this.Stats.IsAfraidOf)
		{
			if (family == familyEnum)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060004AC RID: 1196 RVA: 0x0001B438 File Offset: 0x00019638
	private bool CheckHealthThresholdToFlee()
	{
		if (base.healthFraction > this.Stats.HealthThresholdForFleeing)
		{
			if (this.Stats.HealthThresholdForFleeing < 1f)
			{
				this.SetFact(global::BaseNpc.Facts.IsUnderHealthThreshold, 0, true, true);
				return false;
			}
			if (this.GetFact(global::BaseNpc.Facts.HasEnemy) == 1)
			{
				this.SetFact(global::BaseNpc.Facts.IsUnderHealthThreshold, 0, true, true);
				return false;
			}
		}
		bool flag = Random.value < this.Stats.HealthThresholdFleeChance;
		this.SetFact(global::BaseNpc.Facts.IsUnderHealthThreshold, (!flag) ? 0 : 1, true, true);
		return flag;
	}

	// Token: 0x060004AD RID: 1197 RVA: 0x0001B4C4 File Offset: 0x000196C4
	private void TickBehaviourState()
	{
		if (this.GetFact(global::BaseNpc.Facts.WantsToFlee) == 1 && this.IsNavRunning() && this.NavAgent.pathStatus == null && UnityEngine.Time.realtimeSinceStartup - (this.maxFleeTime - this.Stats.MaxFleeTime) > 0.5f)
		{
			this.TickFlee();
		}
		if (this.GetFact(global::BaseNpc.Facts.CanTargetEnemies) == 0)
		{
			this.TickBlockEnemyTargeting();
		}
		if (this.GetFact(global::BaseNpc.Facts.CanTargetFood) == 0)
		{
			this.TickBlockFoodTargeting();
		}
		if (this.GetFact(global::BaseNpc.Facts.IsAggro) == 1)
		{
			this.TickAggro();
		}
		if (this.GetFact(global::BaseNpc.Facts.IsEating) == 1)
		{
			this.TickEating();
		}
		if (this.GetFact(global::BaseNpc.Facts.CanNotMove) == 1)
		{
			this.TickWakeUpBlockMove();
		}
	}

	// Token: 0x060004AE RID: 1198 RVA: 0x0001B584 File Offset: 0x00019784
	private void WantsToFlee()
	{
		if (this.GetFact(global::BaseNpc.Facts.WantsToFlee) == 1 || !this.IsNavRunning())
		{
			return;
		}
		this.SetFact(global::BaseNpc.Facts.WantsToFlee, 1, true, true);
		this.maxFleeTime = UnityEngine.Time.realtimeSinceStartup + this.Stats.MaxFleeTime;
	}

	// Token: 0x060004AF RID: 1199 RVA: 0x0001B5C4 File Offset: 0x000197C4
	private void TickFlee()
	{
		bool flag = UnityEngine.Time.realtimeSinceStartup > this.maxFleeTime;
		if (flag || (this.IsNavRunning() && this.NavAgent.remainingDistance <= this.NavAgent.stoppingDistance + 1f))
		{
			if (!flag && this.IsAfraid())
			{
				Rust.Ai.NavigateToOperator.FleeEnemy(this.AiContext);
			}
			else
			{
				this.SetFact(global::BaseNpc.Facts.WantsToFlee, 0, true, true);
				this.SetFact(global::BaseNpc.Facts.IsFleeing, 0, true, true);
				this.Stats.HealthThresholdForFleeing = base.healthFraction * this.fleeHealthThresholdPercentage;
			}
		}
	}

	// Token: 0x060004B0 RID: 1200 RVA: 0x0001B660 File Offset: 0x00019860
	public bool BlockEnemyTargeting(float timeout)
	{
		if (this.GetFact(global::BaseNpc.Facts.CanTargetEnemies) == 0)
		{
			return false;
		}
		this.SetFact(global::BaseNpc.Facts.CanTargetEnemies, 0, true, true);
		this.blockEnemyTargetingTimeout = UnityEngine.Time.realtimeSinceStartup + timeout;
		this.blockTargetingThisEnemy = this.AttackTarget;
		return true;
	}

	// Token: 0x060004B1 RID: 1201 RVA: 0x0001B694 File Offset: 0x00019894
	private void TickBlockEnemyTargeting()
	{
		if (this.GetFact(global::BaseNpc.Facts.CanTargetEnemies) == 1)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup > this.blockEnemyTargetingTimeout)
		{
			this.SetFact(global::BaseNpc.Facts.CanTargetEnemies, 1, true, true);
		}
	}

	// Token: 0x060004B2 RID: 1202 RVA: 0x0001B6C0 File Offset: 0x000198C0
	public bool BlockFoodTargeting(float timeout)
	{
		if (this.GetFact(global::BaseNpc.Facts.CanTargetFood) == 0)
		{
			return false;
		}
		this.SetFact(global::BaseNpc.Facts.CanTargetFood, 0, true, true);
		this.blockFoodTargetingTimeout = UnityEngine.Time.realtimeSinceStartup + timeout;
		return true;
	}

	// Token: 0x060004B3 RID: 1203 RVA: 0x0001B6EC File Offset: 0x000198EC
	private void TickBlockFoodTargeting()
	{
		if (this.GetFact(global::BaseNpc.Facts.CanTargetFood) == 1)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup > this.blockFoodTargetingTimeout)
		{
			this.SetFact(global::BaseNpc.Facts.CanTargetFood, 1, true, true);
		}
	}

	// Token: 0x060004B4 RID: 1204 RVA: 0x0001B718 File Offset: 0x00019918
	public bool TryAggro(global::BaseNpc.EnemyRangeEnum range)
	{
		if (Mathf.Approximately(this.Stats.Hostility, 0f) && Mathf.Approximately(this.Stats.Defensiveness, 0f))
		{
			return false;
		}
		if (this.GetFact(global::BaseNpc.Facts.IsAggro) == 0 && (range == global::BaseNpc.EnemyRangeEnum.AggroRange || range == global::BaseNpc.EnemyRangeEnum.AttackRange))
		{
			float num = (range != global::BaseNpc.EnemyRangeEnum.AttackRange) ? this.Stats.Defensiveness : 1f;
			num = Mathf.Max(num, this.Stats.Hostility);
			if (UnityEngine.Time.realtimeSinceStartup > this.lastAggroChanceCalcTime + 5f)
			{
				this.lastAggroChanceResult = Random.value;
				this.lastAggroChanceCalcTime = UnityEngine.Time.realtimeSinceStartup;
			}
			if (this.lastAggroChanceResult < num)
			{
				return this.StartAggro(this.Stats.DeaggroChaseTime);
			}
		}
		return false;
	}

	// Token: 0x060004B5 RID: 1205 RVA: 0x0001B7F0 File Offset: 0x000199F0
	public bool StartAggro(float timeout)
	{
		if (this.GetFact(global::BaseNpc.Facts.IsAggro) == 1)
		{
			return false;
		}
		this.SetFact(global::BaseNpc.Facts.IsAggro, 1, true, true);
		this.aggroTimeout = UnityEngine.Time.realtimeSinceStartup + timeout;
		return true;
	}

	// Token: 0x060004B6 RID: 1206 RVA: 0x0001B81C File Offset: 0x00019A1C
	private void TickAggro()
	{
		bool triggerCallback = true;
		bool flag;
		if (float.IsInfinity(base.SecondsSinceDealtDamage))
		{
			flag = (UnityEngine.Time.realtimeSinceStartup > this.aggroTimeout);
		}
		else
		{
			global::BaseCombatEntity baseCombatEntity = this.AttackTarget as global::BaseCombatEntity;
			if (baseCombatEntity != null && baseCombatEntity.lastAttacker != null && this.net != null && baseCombatEntity.lastAttacker.net != null)
			{
				flag = (baseCombatEntity.lastAttacker.net.ID == this.net.ID && base.SecondsSinceDealtDamage > this.Stats.DeaggroChaseTime);
			}
			else
			{
				flag = (UnityEngine.Time.realtimeSinceStartup > this.aggroTimeout);
			}
		}
		if (!flag)
		{
			if (this.AiContext.EnemyNpc != null && (this.AiContext.EnemyNpc.IsDead() || this.AiContext.EnemyNpc.IsDestroyed))
			{
				flag = true;
				triggerCallback = false;
			}
			else if (this.AiContext.EnemyPlayer != null && (this.AiContext.EnemyPlayer.IsDead() || this.AiContext.EnemyPlayer.IsDestroyed))
			{
				flag = true;
				triggerCallback = false;
			}
		}
		if (flag)
		{
			this.SetFact(global::BaseNpc.Facts.IsAggro, 0, triggerCallback, true);
		}
	}

	// Token: 0x060004B7 RID: 1207 RVA: 0x0001B984 File Offset: 0x00019B84
	public bool StartEating(float timeout)
	{
		if (this.GetFact(global::BaseNpc.Facts.IsEating) == 1)
		{
			return false;
		}
		this.SetFact(global::BaseNpc.Facts.IsEating, 1, true, true);
		this.eatTimeout = UnityEngine.Time.realtimeSinceStartup + timeout;
		return true;
	}

	// Token: 0x060004B8 RID: 1208 RVA: 0x0001B9B0 File Offset: 0x00019BB0
	private void TickEating()
	{
		if (this.GetFact(global::BaseNpc.Facts.IsEating) == 0)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup > this.eatTimeout)
		{
			this.SetFact(global::BaseNpc.Facts.IsEating, 0, true, true);
		}
	}

	// Token: 0x060004B9 RID: 1209 RVA: 0x0001B9DC File Offset: 0x00019BDC
	public bool WakeUpBlockMove(float timeout)
	{
		if (this.GetFact(global::BaseNpc.Facts.CanNotMove) == 1)
		{
			return false;
		}
		this.SetFact(global::BaseNpc.Facts.CanNotMove, 1, true, true);
		this.wakeUpBlockMoveTimeout = UnityEngine.Time.realtimeSinceStartup + timeout;
		return true;
	}

	// Token: 0x060004BA RID: 1210 RVA: 0x0001BA08 File Offset: 0x00019C08
	private void TickWakeUpBlockMove()
	{
		if (this.GetFact(global::BaseNpc.Facts.CanNotMove) == 0)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup > this.wakeUpBlockMoveTimeout)
		{
			this.SetFact(global::BaseNpc.Facts.CanNotMove, 0, true, true);
		}
	}

	// Token: 0x060004BB RID: 1211 RVA: 0x0001BA34 File Offset: 0x00019C34
	private void OnFactChanged(global::BaseNpc.Facts fact, byte oldValue, byte newValue)
	{
		switch (fact)
		{
		case global::BaseNpc.Facts.CanTargetEnemies:
			if (newValue == 1)
			{
				this.blockTargetingThisEnemy = null;
			}
			break;
		default:
			if (fact != global::BaseNpc.Facts.FoodRange)
			{
				if (fact == global::BaseNpc.Facts.IsEating)
				{
					if (newValue == 0)
					{
						this.FoodTarget = null;
					}
				}
			}
			else if (newValue == 0)
			{
				this.CurrentBehaviour = global::BaseNpc.Behaviour.Eat;
			}
			break;
		case global::BaseNpc.Facts.Speed:
			if (newValue != 0)
			{
				if (newValue != 1)
				{
					this.IsStopped = false;
				}
				else
				{
					this.IsStopped = false;
					this.CurrentBehaviour = global::BaseNpc.Behaviour.Wander;
				}
			}
			else
			{
				this.StopMoving();
				this.CurrentBehaviour = global::BaseNpc.Behaviour.Idle;
			}
			break;
		case global::BaseNpc.Facts.IsSleeping:
			if (newValue > 0)
			{
				this.CurrentBehaviour = global::BaseNpc.Behaviour.Sleep;
				this.SetFact(global::BaseNpc.Facts.CanTargetEnemies, 0, false, true);
				this.SetFact(global::BaseNpc.Facts.CanTargetFood, 0, true, true);
			}
			else
			{
				this.CurrentBehaviour = global::BaseNpc.Behaviour.Idle;
				this.SetFact(global::BaseNpc.Facts.CanTargetEnemies, 1, true, true);
				this.SetFact(global::BaseNpc.Facts.CanTargetFood, 1, true, true);
				this.WakeUpBlockMove(this.Stats.WakeupBlockMoveTime);
				this.TickSenses();
			}
			break;
		case global::BaseNpc.Facts.IsAggro:
			if (newValue > 0)
			{
				this.CurrentBehaviour = global::BaseNpc.Behaviour.Attack;
			}
			else
			{
				this.BlockEnemyTargeting(this.Stats.DeaggroCooldown);
			}
			break;
		}
	}

	// Token: 0x060004BC RID: 1212 RVA: 0x0001BB8C File Offset: 0x00019D8C
	public void UpdateDestination(Vector3 position)
	{
		if (this.IsStopped)
		{
			this.IsStopped = false;
		}
		if ((this.Destination - position).sqrMagnitude > 0.0100000007f)
		{
			this.Destination = position;
		}
		this.ChaseTransform = null;
	}

	// Token: 0x060004BD RID: 1213 RVA: 0x0001BBD8 File Offset: 0x00019DD8
	public void UpdateDestination(Transform tx)
	{
		this.IsStopped = false;
		this.ChaseTransform = tx;
	}

	// Token: 0x060004BE RID: 1214 RVA: 0x0001BBE8 File Offset: 0x00019DE8
	public void StopMoving()
	{
		this.IsStopped = true;
		this.ChaseTransform = null;
	}

	// Token: 0x1700001A RID: 26
	// (get) Token: 0x060004BF RID: 1215 RVA: 0x0001BBF8 File Offset: 0x00019DF8
	// (set) Token: 0x060004C0 RID: 1216 RVA: 0x0001BC1C File Offset: 0x00019E1C
	public Vector3 Destination
	{
		get
		{
			if (this.IsNavRunning())
			{
				return this.GetNavAgent.destination;
			}
			return this.Entity.ServerPosition;
		}
		set
		{
			if (this.IsNavRunning())
			{
				this.GetNavAgent.destination = value;
			}
		}
	}

	// Token: 0x1700001B RID: 27
	// (get) Token: 0x060004C1 RID: 1217 RVA: 0x0001BC38 File Offset: 0x00019E38
	// (set) Token: 0x060004C2 RID: 1218 RVA: 0x0001BC54 File Offset: 0x00019E54
	public bool IsStopped
	{
		get
		{
			return !this.IsNavRunning() || this.GetNavAgent.isStopped;
		}
		set
		{
			if (this.IsNavRunning())
			{
				this.GetNavAgent.isStopped = value;
			}
		}
	}

	// Token: 0x1700001C RID: 28
	// (get) Token: 0x060004C3 RID: 1219 RVA: 0x0001BC70 File Offset: 0x00019E70
	// (set) Token: 0x060004C4 RID: 1220 RVA: 0x0001BC8C File Offset: 0x00019E8C
	public bool AutoBraking
	{
		get
		{
			return this.IsNavRunning() && this.GetNavAgent.autoBraking;
		}
		set
		{
			if (this.IsNavRunning())
			{
				this.GetNavAgent.autoBraking = value;
			}
		}
	}

	// Token: 0x1700001D RID: 29
	// (get) Token: 0x060004C5 RID: 1221 RVA: 0x0001BCA8 File Offset: 0x00019EA8
	public bool HasPath
	{
		get
		{
			return this.IsNavRunning() && this.GetNavAgent.hasPath;
		}
	}

	// Token: 0x060004C6 RID: 1222 RVA: 0x0001BCC4 File Offset: 0x00019EC4
	public bool IsNavRunning()
	{
		return !Rust.Ai.AiManager.nav_disable && this.GetNavAgent != null && this.GetNavAgent.enabled && this.GetNavAgent.isOnNavMesh;
	}

	// Token: 0x060004C7 RID: 1223 RVA: 0x0001BD00 File Offset: 0x00019F00
	public void Pause()
	{
		this.IsDormant = true;
		if (this.GetNavAgent != null && this.GetNavAgent.enabled)
		{
			this.GetNavAgent.enabled = false;
		}
		if (this.utilityAiComponent == null)
		{
			this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
		}
		if (this.utilityAiComponent != null)
		{
			this.utilityAiComponent.Pause();
			this.utilityAiComponent.enabled = false;
		}
	}

	// Token: 0x060004C8 RID: 1224 RVA: 0x0001BD8C File Offset: 0x00019F8C
	public void Resume()
	{
		if (this.GetNavAgent == null || Rust.Ai.AiManager.nav_disable)
		{
			this.Pause();
			return;
		}
		if (!this.GetNavAgent.isOnNavMesh)
		{
			base.StartCoroutine(this.TryForceToNavmesh());
		}
		else
		{
			this.IsDormant = false;
			this.GetNavAgent.enabled = true;
			if (this.utilityAiComponent == null)
			{
				this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
			}
			if (this.utilityAiComponent != null)
			{
				this.utilityAiComponent.enabled = true;
				this.utilityAiComponent.Resume();
			}
		}
	}

	// Token: 0x060004C9 RID: 1225 RVA: 0x0001BE3C File Offset: 0x0001A03C
	private IEnumerator TryForceToNavmesh()
	{
		yield return null;
		int numTries = 0;
		float waitForRetryTime = 1f;
		float maxDistanceMultiplier = 2f;
		if (SingletonComponent<Rust.Ai.AiManager>.Instance != null && SingletonComponent<Rust.Ai.AiManager>.Instance.enabled && SingletonComponent<Rust.Ai.AiManager>.Instance.UseNavMesh)
		{
			while (SingletonComponent<Rust.Ai.AiManager>.Instance.IsNavmeshBuilding(this.AgentTypeIndex, this.ServerPosition))
			{
				yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(waitForRetryTime);
				waitForRetryTime += 0.5f;
			}
		}
		else if (!Rust.Ai.AiManager.nav_grid && SingletonComponent<global::DynamicNavMesh>.Instance != null)
		{
			while (SingletonComponent<global::DynamicNavMesh>.Instance.IsBuilding)
			{
				yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(waitForRetryTime);
				waitForRetryTime += 0.5f;
			}
		}
		waitForRetryTime = 1f;
		while (numTries < 4)
		{
			if (this.GetNavAgent.isOnNavMesh)
			{
				this.GetNavAgent.enabled = true;
				if (this.utilityAiComponent == null)
				{
					this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
				}
				if (this.utilityAiComponent != null)
				{
					this.utilityAiComponent.enabled = true;
					this.utilityAiComponent.Resume();
				}
				this.IsDormant = false;
				yield break;
			}
			NavMeshHit navMeshHit;
			if (NavMesh.SamplePosition(this.ServerPosition, ref navMeshHit, this.GetNavAgent.height * maxDistanceMultiplier, this.GetNavAgent.areaMask))
			{
				this.ServerPosition = navMeshHit.position;
				this.GetNavAgent.Warp(this.ServerPosition);
				this.GetNavAgent.enabled = true;
				if (this.utilityAiComponent == null)
				{
					this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
				}
				if (this.utilityAiComponent != null)
				{
					this.utilityAiComponent.enabled = true;
					this.utilityAiComponent.Resume();
				}
				this.IsDormant = false;
				yield break;
			}
			yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(waitForRetryTime);
			maxDistanceMultiplier *= 1.5f;
			waitForRetryTime *= 1.5f;
			numTries++;
		}
		base.DieInstantly();
		yield break;
	}

	// Token: 0x1700001E RID: 30
	// (get) Token: 0x060004CA RID: 1226 RVA: 0x0001BE58 File Offset: 0x0001A058
	// (set) Token: 0x060004CB RID: 1227 RVA: 0x0001BE60 File Offset: 0x0001A060
	public global::BaseEntity AttackTarget { get; set; }

	// Token: 0x1700001F RID: 31
	// (get) Token: 0x060004CC RID: 1228 RVA: 0x0001BE6C File Offset: 0x0001A06C
	// (set) Token: 0x060004CD RID: 1229 RVA: 0x0001BE74 File Offset: 0x0001A074
	public Rust.Ai.Memory.SeenInfo AttackTargetMemory { get; set; }

	// Token: 0x17000020 RID: 32
	// (get) Token: 0x060004CE RID: 1230 RVA: 0x0001BE80 File Offset: 0x0001A080
	// (set) Token: 0x060004CF RID: 1231 RVA: 0x0001BE88 File Offset: 0x0001A088
	public global::BaseEntity FoodTarget { get; set; }

	// Token: 0x17000021 RID: 33
	// (get) Token: 0x060004D0 RID: 1232 RVA: 0x0001BE94 File Offset: 0x0001A094
	public global::BaseCombatEntity CombatTarget
	{
		get
		{
			return this.AttackTarget as global::BaseCombatEntity;
		}
	}

	// Token: 0x17000022 RID: 34
	// (get) Token: 0x060004D1 RID: 1233 RVA: 0x0001BEA4 File Offset: 0x0001A0A4
	// (set) Token: 0x060004D2 RID: 1234 RVA: 0x0001BEAC File Offset: 0x0001A0AC
	public Vector3 SpawnPosition { get; set; }

	// Token: 0x17000023 RID: 35
	// (get) Token: 0x060004D3 RID: 1235 RVA: 0x0001BEB8 File Offset: 0x0001A0B8
	public float AttackTargetVisibleFor
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x17000024 RID: 36
	// (get) Token: 0x060004D4 RID: 1236 RVA: 0x0001BEC0 File Offset: 0x0001A0C0
	public float TimeAtDestination
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x17000025 RID: 37
	// (get) Token: 0x060004D5 RID: 1237 RVA: 0x0001BEC8 File Offset: 0x0001A0C8
	public global::BaseCombatEntity Entity
	{
		get
		{
			return this;
		}
	}

	// Token: 0x17000026 RID: 38
	// (get) Token: 0x060004D6 RID: 1238 RVA: 0x0001BECC File Offset: 0x0001A0CC
	public NavMeshAgent GetNavAgent
	{
		get
		{
			if (base.isClient)
			{
				return null;
			}
			if (this.NavAgent == null)
			{
				this.NavAgent = base.GetComponent<NavMeshAgent>();
				if (this.NavAgent == null)
				{
					Debug.LogErrorFormat("{0} has no nav agent!", new object[]
					{
						base.name
					});
				}
			}
			return this.NavAgent;
		}
	}

	// Token: 0x060004D7 RID: 1239 RVA: 0x0001BF34 File Offset: 0x0001A134
	public float GetWantsToAttack(global::BaseEntity target)
	{
		object obj = Interface.CallHook("OnNpcTarget", new object[]
		{
			this,
			target
		});
		if (obj is float)
		{
			return (float)obj;
		}
		return this.WantsToAttack(target);
	}

	// Token: 0x17000027 RID: 39
	// (get) Token: 0x060004D8 RID: 1240 RVA: 0x0001BF74 File Offset: 0x0001A174
	public global::BaseNpc.AiStatistics GetStats
	{
		get
		{
			return this.Stats;
		}
	}

	// Token: 0x17000028 RID: 40
	// (get) Token: 0x060004D9 RID: 1241 RVA: 0x0001BF7C File Offset: 0x0001A17C
	public float GetAttackRange
	{
		get
		{
			return this.AttackRange;
		}
	}

	// Token: 0x17000029 RID: 41
	// (get) Token: 0x060004DA RID: 1242 RVA: 0x0001BF84 File Offset: 0x0001A184
	public Vector3 GetAttackOffset
	{
		get
		{
			return this.AttackOffset;
		}
	}

	// Token: 0x1700002A RID: 42
	// (get) Token: 0x060004DB RID: 1243 RVA: 0x0001BF8C File Offset: 0x0001A18C
	public float GetStamina
	{
		get
		{
			return this.Stamina.Level;
		}
	}

	// Token: 0x1700002B RID: 43
	// (get) Token: 0x060004DC RID: 1244 RVA: 0x0001BF9C File Offset: 0x0001A19C
	public float GetEnergy
	{
		get
		{
			return this.Energy.Level;
		}
	}

	// Token: 0x1700002C RID: 44
	// (get) Token: 0x060004DD RID: 1245 RVA: 0x0001BFAC File Offset: 0x0001A1AC
	public float GetAttackCost
	{
		get
		{
			return this.AttackCost;
		}
	}

	// Token: 0x1700002D RID: 45
	// (get) Token: 0x060004DE RID: 1246 RVA: 0x0001BFB4 File Offset: 0x0001A1B4
	public float GetSleep
	{
		get
		{
			return this.Sleep;
		}
	}

	// Token: 0x1700002E RID: 46
	// (get) Token: 0x060004DF RID: 1247 RVA: 0x0001BFBC File Offset: 0x0001A1BC
	public Vector3 CurrentAimAngles
	{
		get
		{
			return base.transform.forward;
		}
	}

	// Token: 0x1700002F RID: 47
	// (get) Token: 0x060004E0 RID: 1248 RVA: 0x0001BFCC File Offset: 0x0001A1CC
	public float GetStuckDuration
	{
		get
		{
			return this.stuckDuration;
		}
	}

	// Token: 0x17000030 RID: 48
	// (get) Token: 0x060004E1 RID: 1249 RVA: 0x0001BFD4 File Offset: 0x0001A1D4
	public float GetLastStuckTime
	{
		get
		{
			return this.lastStuckTime;
		}
	}

	// Token: 0x060004E2 RID: 1250 RVA: 0x0001BFDC File Offset: 0x0001A1DC
	public bool BusyTimerActive()
	{
		return this.BusyTimer.IsActive;
	}

	// Token: 0x060004E3 RID: 1251 RVA: 0x0001BFEC File Offset: 0x0001A1EC
	public void SetBusyFor(float dur)
	{
		this.BusyTimer.Activate(dur, null);
	}

	// Token: 0x17000031 RID: 49
	// (get) Token: 0x060004E4 RID: 1252 RVA: 0x0001BFFC File Offset: 0x0001A1FC
	public Vector3 AttackPosition
	{
		get
		{
			return this.ServerPosition + base.transform.TransformDirection(this.AttackOffset);
		}
	}

	// Token: 0x17000032 RID: 50
	// (get) Token: 0x060004E5 RID: 1253 RVA: 0x0001C01C File Offset: 0x0001A21C
	public Vector3 CrouchedAttackPosition
	{
		get
		{
			return this.AttackPosition;
		}
	}

	// Token: 0x060004E6 RID: 1254 RVA: 0x0001C024 File Offset: 0x0001A224
	internal float WantsToAttack(global::BaseEntity target)
	{
		if (target == null)
		{
			return 0f;
		}
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Sleep)
		{
			return 0f;
		}
		if (!target.HasAnyTrait(global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Human))
		{
			return 0f;
		}
		if (target.GetType() == base.GetType())
		{
			return 1f - this.Stats.Tolerance;
		}
		return 1f;
	}

	// Token: 0x060004E7 RID: 1255 RVA: 0x0001C090 File Offset: 0x0001A290
	protected virtual void SetupAiContext()
	{
		this.AiContext = new Rust.Ai.BaseContext(this);
	}

	// Token: 0x060004E8 RID: 1256 RVA: 0x0001C0A0 File Offset: 0x0001A2A0
	public IAIContext GetContext(Guid aiId)
	{
		if (this.AiContext == null)
		{
			this.SetupAiContext();
		}
		return this.AiContext;
	}

	// Token: 0x17000033 RID: 51
	// (get) Token: 0x060004E9 RID: 1257 RVA: 0x0001C0BC File Offset: 0x0001A2BC
	public float currentBehaviorDuration
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x17000034 RID: 52
	// (get) Token: 0x060004EA RID: 1258 RVA: 0x0001C0C4 File Offset: 0x0001A2C4
	// (set) Token: 0x060004EB RID: 1259 RVA: 0x0001C0CC File Offset: 0x0001A2CC
	public global::BaseNpc.Behaviour CurrentBehaviour { get; set; }

	// Token: 0x060004EC RID: 1260 RVA: 0x0001C0D8 File Offset: 0x0001A2D8
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.baseNPC = Facepunch.Pool.Get<BaseNPC>();
		info.msg.baseNPC.flags = (int)this.aiFlags;
	}

	// Token: 0x060004ED RID: 1261 RVA: 0x0001C10C File Offset: 0x0001A30C
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.baseNPC != null)
		{
			this.aiFlags = (global::BaseNpc.AiFlags)info.msg.baseNPC.flags;
		}
	}

	// Token: 0x060004EE RID: 1262 RVA: 0x0001C140 File Offset: 0x0001A340
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x060004EF RID: 1263 RVA: 0x0001C144 File Offset: 0x0001A344
	public override float MaxVelocity()
	{
		return this.Stats.Speed;
	}

	// Token: 0x060004F0 RID: 1264 RVA: 0x0001C154 File Offset: 0x0001A354
	public bool HasAiFlag(global::BaseNpc.AiFlags f)
	{
		return (this.aiFlags & f) == f;
	}

	// Token: 0x060004F1 RID: 1265 RVA: 0x0001C164 File Offset: 0x0001A364
	public void SetAiFlag(global::BaseNpc.AiFlags f, bool set)
	{
		global::BaseNpc.AiFlags aiFlags = this.aiFlags;
		if (set)
		{
			this.aiFlags |= f;
		}
		else
		{
			this.aiFlags &= ~f;
		}
		if (aiFlags != this.aiFlags && base.isServer)
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x17000035 RID: 53
	// (get) Token: 0x060004F2 RID: 1266 RVA: 0x0001C1C0 File Offset: 0x0001A3C0
	// (set) Token: 0x060004F3 RID: 1267 RVA: 0x0001C1CC File Offset: 0x0001A3CC
	public bool IsSitting
	{
		get
		{
			return this.HasAiFlag(global::BaseNpc.AiFlags.Sitting);
		}
		set
		{
			this.SetAiFlag(global::BaseNpc.AiFlags.Sitting, value);
		}
	}

	// Token: 0x17000036 RID: 54
	// (get) Token: 0x060004F4 RID: 1268 RVA: 0x0001C1D8 File Offset: 0x0001A3D8
	// (set) Token: 0x060004F5 RID: 1269 RVA: 0x0001C1E4 File Offset: 0x0001A3E4
	public bool IsChasing
	{
		get
		{
			return this.HasAiFlag(global::BaseNpc.AiFlags.Chasing);
		}
		set
		{
			this.SetAiFlag(global::BaseNpc.AiFlags.Chasing, value);
		}
	}

	// Token: 0x17000037 RID: 55
	// (get) Token: 0x060004F6 RID: 1270 RVA: 0x0001C1F0 File Offset: 0x0001A3F0
	// (set) Token: 0x060004F7 RID: 1271 RVA: 0x0001C1FC File Offset: 0x0001A3FC
	public bool IsSleeping
	{
		get
		{
			return this.HasAiFlag(global::BaseNpc.AiFlags.Sleeping);
		}
		set
		{
			this.SetAiFlag(global::BaseNpc.AiFlags.Sleeping, value);
		}
	}

	// Token: 0x060004F8 RID: 1272 RVA: 0x0001C208 File Offset: 0x0001A408
	public void InitFacts()
	{
		this.SetFact(global::BaseNpc.Facts.CanTargetEnemies, 1, true, true);
		this.SetFact(global::BaseNpc.Facts.CanTargetFood, 1, true, true);
	}

	// Token: 0x060004F9 RID: 1273 RVA: 0x0001C220 File Offset: 0x0001A420
	public byte GetFact(global::BaseNpc.Facts fact)
	{
		return this.CurrentFacts[(int)fact];
	}

	// Token: 0x060004FA RID: 1274 RVA: 0x0001C22C File Offset: 0x0001A42C
	public void SetFact(global::BaseNpc.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true)
	{
		byte b = this.CurrentFacts[(int)fact];
		this.CurrentFacts[(int)fact] = value;
		if (triggerCallback && value != b)
		{
			this.OnFactChanged(fact, b, value);
		}
	}

	// Token: 0x060004FB RID: 1275 RVA: 0x0001C264 File Offset: 0x0001A464
	public byte GetFact(global::NPCPlayerApex.Facts fact)
	{
		return 0;
	}

	// Token: 0x060004FC RID: 1276 RVA: 0x0001C268 File Offset: 0x0001A468
	public void SetFact(global::NPCPlayerApex.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true)
	{
	}

	// Token: 0x060004FD RID: 1277 RVA: 0x0001C26C File Offset: 0x0001A46C
	public float ToSpeed(global::NPCPlayerApex.SpeedEnum speed)
	{
		return 0f;
	}

	// Token: 0x060004FE RID: 1278 RVA: 0x0001C274 File Offset: 0x0001A474
	public global::BaseNpc.EnemyRangeEnum ToEnemyRangeEnum(float range)
	{
		if (range <= this.AttackRange)
		{
			return global::BaseNpc.EnemyRangeEnum.AttackRange;
		}
		if (range <= this.Stats.AggressionRange)
		{
			return global::BaseNpc.EnemyRangeEnum.AggroRange;
		}
		if (range >= this.Stats.DeaggroRange && this.GetFact(global::BaseNpc.Facts.IsAggro) > 0)
		{
			return global::BaseNpc.EnemyRangeEnum.OutOfRange;
		}
		if (range <= this.Stats.VisionRange)
		{
			return global::BaseNpc.EnemyRangeEnum.AwareRange;
		}
		return global::BaseNpc.EnemyRangeEnum.OutOfRange;
	}

	// Token: 0x060004FF RID: 1279 RVA: 0x0001C2D8 File Offset: 0x0001A4D8
	public global::BaseNpc.FoodRangeEnum ToFoodRangeEnum(float range)
	{
		if (range <= 0.5f)
		{
			return global::BaseNpc.FoodRangeEnum.EatRange;
		}
		if (range <= this.Stats.VisionRange)
		{
			return global::BaseNpc.FoodRangeEnum.AwareRange;
		}
		return global::BaseNpc.FoodRangeEnum.OutOfRange;
	}

	// Token: 0x06000500 RID: 1280 RVA: 0x0001C2FC File Offset: 0x0001A4FC
	public global::BaseNpc.AfraidRangeEnum ToAfraidRangeEnum(float range)
	{
		if (range <= this.Stats.AfraidRange)
		{
			return global::BaseNpc.AfraidRangeEnum.InAfraidRange;
		}
		return global::BaseNpc.AfraidRangeEnum.OutOfRange;
	}

	// Token: 0x06000501 RID: 1281 RVA: 0x0001C314 File Offset: 0x0001A514
	public global::BaseNpc.HealthEnum ToHealthEnum(float healthNormalized)
	{
		if (healthNormalized >= 0.75f)
		{
			return global::BaseNpc.HealthEnum.Fine;
		}
		if (healthNormalized >= 0.25f)
		{
			return global::BaseNpc.HealthEnum.Medium;
		}
		return global::BaseNpc.HealthEnum.Low;
	}

	// Token: 0x06000502 RID: 1282 RVA: 0x0001C334 File Offset: 0x0001A534
	public byte ToIsTired(float energyNormalized)
	{
		bool flag = this.GetFact(global::BaseNpc.Facts.IsSleeping) == 1;
		if (!flag && energyNormalized < 0.1f)
		{
			return 1;
		}
		if (flag && energyNormalized < 0.5f)
		{
			return 1;
		}
		return 0;
	}

	// Token: 0x06000503 RID: 1283 RVA: 0x0001C374 File Offset: 0x0001A574
	public global::BaseNpc.SpeedEnum ToSpeedEnum(float speed)
	{
		if (speed <= 0.01f)
		{
			return global::BaseNpc.SpeedEnum.StandStill;
		}
		if (speed <= 0.18f)
		{
			return global::BaseNpc.SpeedEnum.Walk;
		}
		return global::BaseNpc.SpeedEnum.Run;
	}

	// Token: 0x06000504 RID: 1284 RVA: 0x0001C394 File Offset: 0x0001A594
	public float ToSpeed(global::BaseNpc.SpeedEnum speed)
	{
		if (speed == global::BaseNpc.SpeedEnum.StandStill)
		{
			return 0f;
		}
		if (speed != global::BaseNpc.SpeedEnum.Walk)
		{
			return this.Stats.Speed;
		}
		return 0.18f * this.Stats.Speed;
	}

	// Token: 0x06000505 RID: 1285 RVA: 0x0001C3CC File Offset: 0x0001A5CC
	private void TickSenses()
	{
		if (global::BaseEntity.Query.Server == null || this.AiContext == null || this.IsDormant)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup > this.lastTickTime + this.SensesTickRate)
		{
			this.TickVision();
			this.TickHearing();
			this.TickSmell();
			this.AiContext.Memory.Forget((float)this.ForgetUnseenEntityTime);
			this.lastTickTime = UnityEngine.Time.realtimeSinceStartup;
		}
		this.TickEnemyAwareness();
		this.TickFoodAwareness();
		this.UpdateSelfFacts();
	}

	// Token: 0x06000506 RID: 1286 RVA: 0x0001C458 File Offset: 0x0001A658
	private void TickVision()
	{
		this.AiContext.Players.Clear();
		this.AiContext.Npcs.Clear();
		this.AiContext.PlayersBehindUs.Clear();
		this.AiContext.NpcsBehindUs.Clear();
		if (global::BaseEntity.Query.Server == null)
		{
			return;
		}
		if (this.GetFact(global::BaseNpc.Facts.IsSleeping) == 1)
		{
			return;
		}
		global::BaseEntity.Query.EntityTree server = global::BaseEntity.Query.Server;
		Vector3 serverPosition = this.ServerPosition;
		float visionRange = this.Stats.VisionRange;
		global::BaseEntity[] sensesResults = this.SensesResults;
		if (global::BaseNpc.<>f__mg$cache0 == null)
		{
			global::BaseNpc.<>f__mg$cache0 = new Func<global::BaseEntity, bool>(global::BaseNpc.AiCaresAbout);
		}
		int inSphere = server.GetInSphere(serverPosition, visionRange, sensesResults, global::BaseNpc.<>f__mg$cache0);
		if (inSphere == 0)
		{
			return;
		}
		for (int i = 0; i < inSphere; i++)
		{
			global::BaseEntity baseEntity = this.SensesResults[i];
			if (!(baseEntity == null))
			{
				if (!(baseEntity == this))
				{
					if (baseEntity.isServer)
					{
						if (!(baseEntity.transform == null))
						{
							if (!baseEntity.IsDestroyed)
							{
								if (!global::BaseNpc.WithinVisionCone(this, baseEntity))
								{
									global::BasePlayer basePlayer = baseEntity as global::BasePlayer;
									if (basePlayer != null)
									{
										if (!ConVar.AI.ignoreplayers)
										{
											if ((basePlayer.ServerPosition - this.ServerPosition).sqrMagnitude <= (this.AttackRange + 2f) * (this.AttackRange + 2f))
											{
												this.AiContext.PlayersBehindUs.Add(basePlayer);
											}
										}
									}
									else
									{
										global::BaseNpc baseNpc = baseEntity as global::BaseNpc;
										if (baseNpc != null && (baseNpc.ServerPosition - this.ServerPosition).sqrMagnitude <= (this.AttackRange + 2f) * (this.AttackRange + 2f))
										{
											this.AiContext.NpcsBehindUs.Add(baseNpc);
										}
									}
								}
								else
								{
									global::BasePlayer basePlayer2 = baseEntity as global::BasePlayer;
									if (basePlayer2 != null)
									{
										if (ConVar.AI.ignoreplayers)
										{
											goto IL_2CB;
										}
										Vector3 attackPosition = this.AiContext.AIAgent.AttackPosition;
										if (!basePlayer2.IsVisible(attackPosition, basePlayer2.CenterPoint()) && !basePlayer2.IsVisible(attackPosition, basePlayer2.eyes.position) && !basePlayer2.IsVisible(attackPosition, basePlayer2.transform.position))
										{
											goto IL_2CB;
										}
										this.AiContext.Players.Add(baseEntity as global::BasePlayer);
									}
									else
									{
										global::BaseNpc baseNpc2 = baseEntity as global::BaseNpc;
										if (baseNpc2 != null)
										{
											this.AiContext.Npcs.Add(baseNpc2);
										}
									}
									this.AiContext.Memory.Update(baseEntity, 0f);
								}
							}
						}
					}
				}
			}
			IL_2CB:;
		}
	}

	// Token: 0x06000507 RID: 1287 RVA: 0x0001C73C File Offset: 0x0001A93C
	private void TickHearing()
	{
		this.SetFact(global::BaseNpc.Facts.LoudNoiseNearby, 0, true, true);
	}

	// Token: 0x06000508 RID: 1288 RVA: 0x0001C74C File Offset: 0x0001A94C
	private void TickSmell()
	{
	}

	// Token: 0x06000509 RID: 1289 RVA: 0x0001C750 File Offset: 0x0001A950
	private void TickEnemyAwareness()
	{
		if (this.GetFact(global::BaseNpc.Facts.CanTargetEnemies) == 0 && this.blockTargetingThisEnemy == null)
		{
			this.AiContext.EnemyNpc = null;
			this.AiContext.EnemyPlayer = null;
			this.SetFact(global::BaseNpc.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::BaseNpc.Facts.EnemyRange, 3, true, true);
			this.SetFact(global::BaseNpc.Facts.IsAggro, 0, false, true);
			return;
		}
		this.SelectEnemy();
	}

	// Token: 0x0600050A RID: 1290 RVA: 0x0001C7B8 File Offset: 0x0001A9B8
	private void SelectEnemy()
	{
		if (this.AiContext.Players.Count == 0 && this.AiContext.Npcs.Count == 0 && this.AiContext.PlayersBehindUs.Count == 0 && this.AiContext.NpcsBehindUs.Count == 0)
		{
			this.AiContext.EnemyNpc = null;
			this.AiContext.EnemyPlayer = null;
			this.SetFact(global::BaseNpc.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::BaseNpc.Facts.EnemyRange, 3, true, true);
			this.SetFact(global::BaseNpc.Facts.IsAggro, 0, false, true);
			return;
		}
		this.AggroClosestEnemy();
	}

	// Token: 0x0600050B RID: 1291 RVA: 0x0001C858 File Offset: 0x0001AA58
	private void AggroClosestEnemy()
	{
		float num = float.MaxValue;
		global::BasePlayer basePlayer = null;
		global::BaseNpc baseNpc = null;
		this.AiContext.AIAgent.AttackTarget = null;
		Vector3 vector = Vector3.zero;
		foreach (global::BasePlayer basePlayer2 in this.AiContext.Players)
		{
			if (!basePlayer2.IsDead() && !basePlayer2.IsDestroyed && (!(this.blockTargetingThisEnemy != null) || basePlayer2.net == null || this.blockTargetingThisEnemy.net == null || basePlayer2.net.ID != this.blockTargetingThisEnemy.net.ID))
			{
				Vector3 vector2 = basePlayer2.ServerPosition - this.ServerPosition;
				float sqrMagnitude = vector2.sqrMagnitude;
				if (sqrMagnitude < num)
				{
					num = sqrMagnitude;
					basePlayer = basePlayer2;
					baseNpc = null;
					vector = vector2;
					if (num <= this.AttackRange)
					{
						break;
					}
				}
			}
		}
		if (num > this.AttackRange)
		{
			foreach (global::BaseNpc baseNpc2 in this.AiContext.Npcs)
			{
				if (!baseNpc2.IsDead() && !baseNpc2.IsDestroyed && this.Stats.Family != baseNpc2.Stats.Family)
				{
					Vector3 vector3 = baseNpc2.ServerPosition - this.ServerPosition;
					float sqrMagnitude2 = vector3.sqrMagnitude;
					if (sqrMagnitude2 < num)
					{
						num = sqrMagnitude2;
						baseNpc = baseNpc2;
						basePlayer = null;
						vector = vector3;
						if (num < this.AttackRange)
						{
							break;
						}
					}
				}
			}
		}
		if (num > this.AttackRange)
		{
			if (this.AiContext.PlayersBehindUs.Count > 0)
			{
				basePlayer = this.AiContext.PlayersBehindUs[0];
				baseNpc = null;
			}
			else if (this.AiContext.NpcsBehindUs.Count > 0)
			{
				basePlayer = null;
				baseNpc = this.AiContext.NpcsBehindUs[0];
			}
		}
		this.AiContext.EnemyPlayer = basePlayer;
		this.AiContext.EnemyNpc = baseNpc;
		if (basePlayer != null || baseNpc != null)
		{
			this.SetFact(global::BaseNpc.Facts.HasEnemy, 1, true, true);
			if (basePlayer != null)
			{
				this.AiContext.AIAgent.AttackTarget = basePlayer;
			}
			else
			{
				this.AiContext.AIAgent.AttackTarget = baseNpc;
			}
			if (Interface.CallHook("IOnNpcTarget", new object[]
			{
				this,
				this.AiContext.AIAgent.AttackTarget
			}) != null)
			{
				return;
			}
			float magnitude = vector.magnitude;
			global::BaseNpc.EnemyRangeEnum enemyRangeEnum = this.ToEnemyRangeEnum(magnitude);
			global::BaseNpc.AfraidRangeEnum value = this.ToAfraidRangeEnum(magnitude);
			this.SetFact(global::BaseNpc.Facts.EnemyRange, (byte)enemyRangeEnum, true, true);
			this.SetFact(global::BaseNpc.Facts.AfraidRange, (byte)value, true, true);
			this.TryAggro(enemyRangeEnum);
		}
		else
		{
			this.SetFact(global::BaseNpc.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::BaseNpc.Facts.EnemyRange, 3, true, true);
			this.SetFact(global::BaseNpc.Facts.AfraidRange, 1, true, true);
		}
	}

	// Token: 0x0600050C RID: 1292 RVA: 0x0001CBB8 File Offset: 0x0001ADB8
	private void TickFoodAwareness()
	{
		if (this.GetFact(global::BaseNpc.Facts.CanTargetFood) == 0)
		{
			this.FoodTarget = null;
			this.SetFact(global::BaseNpc.Facts.FoodRange, 2, true, true);
			return;
		}
		this.SelectFood();
	}

	// Token: 0x0600050D RID: 1293 RVA: 0x0001CBE0 File Offset: 0x0001ADE0
	private void SelectFood()
	{
		if (this.AiContext.Memory.Visible.Count == 0)
		{
			this.FoodTarget = null;
			this.SetFact(global::BaseNpc.Facts.FoodRange, 2, true, true);
			return;
		}
		this.SelectClosestFood();
	}

	// Token: 0x0600050E RID: 1294 RVA: 0x0001CC18 File Offset: 0x0001AE18
	private void SelectClosestFood()
	{
		float num = float.MaxValue;
		Vector3 vector = Vector3.zero;
		bool flag = false;
		foreach (global::BaseEntity baseEntity in this.AiContext.Memory.Visible)
		{
			if (!baseEntity.IsDestroyed)
			{
				if (this.WantsToEat(baseEntity))
				{
					Vector3 vector2 = baseEntity.ServerPosition - this.ServerPosition;
					float sqrMagnitude = vector2.sqrMagnitude;
					if (sqrMagnitude < num)
					{
						num = sqrMagnitude;
						this.FoodTarget = baseEntity;
						vector = vector2;
						flag = true;
						if (num <= 0.1f)
						{
							break;
						}
					}
				}
			}
		}
		if (flag)
		{
			global::BaseNpc.FoodRangeEnum value = this.ToFoodRangeEnum(vector.magnitude);
			this.SetFact(global::BaseNpc.Facts.FoodRange, (byte)value, true, true);
		}
		else
		{
			this.FoodTarget = null;
			this.SetFact(global::BaseNpc.Facts.FoodRange, 2, true, true);
		}
	}

	// Token: 0x0600050F RID: 1295 RVA: 0x0001CD20 File Offset: 0x0001AF20
	private void UpdateSelfFacts()
	{
		this.SetFact(global::BaseNpc.Facts.Health, (byte)this.ToHealthEnum(base.healthFraction), true, true);
		this.SetFact(global::BaseNpc.Facts.IsTired, this.ToIsTired(this.Sleep), true, true);
		this.SetFact(global::BaseNpc.Facts.IsAttackReady, (UnityEngine.Time.realtimeSinceStartup < this.nextAttackTime) ? 0 : 1, true, true);
		this.SetFact(global::BaseNpc.Facts.IsRoamReady, (UnityEngine.Time.realtimeSinceStartup < this.AiContext.NextRoamTime || !this.IsNavRunning()) ? 0 : 1, true, true);
		this.SetFact(global::BaseNpc.Facts.Speed, (byte)this.ToSpeedEnum(this.TargetSpeed / this.Stats.Speed), true, true);
		this.SetFact(global::BaseNpc.Facts.IsHungry, (this.Energy.Level >= 0.25f) ? 0 : 1, true, true);
		this.SetFact(global::BaseNpc.Facts.AttackedLately, (float.IsNegativeInfinity(base.SecondsSinceAttacked) || base.SecondsSinceAttacked >= this.Stats.AttackedMemoryTime) ? 0 : 1, true, true);
		this.SetFact(global::BaseNpc.Facts.IsMoving, this.IsMoving(), true, true);
		if (this.CheckHealthThresholdToFlee() || this.IsAfraid())
		{
			this.WantsToFlee();
		}
	}

	// Token: 0x06000510 RID: 1296 RVA: 0x0001CE58 File Offset: 0x0001B058
	private byte IsMoving()
	{
		return (!this.IsNavRunning() || !this.NavAgent.hasPath || this.NavAgent.remainingDistance <= this.NavAgent.stoppingDistance || this.IsStuck || this.GetFact(global::BaseNpc.Facts.Speed) == 0) ? 0 : 1;
	}

	// Token: 0x06000511 RID: 1297 RVA: 0x0001CEBC File Offset: 0x0001B0BC
	private static bool AiCaresAbout(global::BaseEntity ent)
	{
		return ent is global::BasePlayer || ent is global::BaseNpc || ent is global::WorldItem || ent is global::BaseCorpse || ent is global::CollectibleEntity;
	}

	// Token: 0x06000512 RID: 1298 RVA: 0x0001CF0C File Offset: 0x0001B10C
	private static bool WithinVisionCone(global::BaseNpc npc, global::BaseEntity other)
	{
		if (Mathf.Approximately(npc.Stats.VisionCone, -1f))
		{
			return true;
		}
		Vector3 normalized = (other.ServerPosition - npc.ServerPosition).normalized;
		float num = Vector3.Dot(npc.transform.forward, normalized);
		return num >= npc.Stats.VisionCone;
	}

	// Token: 0x06000513 RID: 1299 RVA: 0x0001CF78 File Offset: 0x0001B178
	public List<Rust.Ai.NavPointSample> RequestNavPointSamplesInCircle(Rust.Ai.NavPointSampler.SampleCount sampleCount, float radius, Rust.Ai.NavPointSampler.SampleFeatures features = Rust.Ai.NavPointSampler.SampleFeatures.None)
	{
		this.navPointSamples.Clear();
		Rust.Ai.NavPointSampler.SampleCircle(sampleCount, this.ServerPosition, radius, new Rust.Ai.NavPointSampler.SampleScoreParams
		{
			WaterMaxDepth = this.Stats.MaxWaterDepth,
			Agent = this,
			Features = features
		}, ref this.navPointSamples);
		return this.navPointSamples;
	}

	// Token: 0x06000514 RID: 1300 RVA: 0x0001CFD8 File Offset: 0x0001B1D8
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.NavAgent == null)
		{
			this.NavAgent = base.GetComponent<NavMeshAgent>();
		}
		if (this.NavAgent != null)
		{
			this.NavAgent.updateRotation = false;
			this.NavAgent.updatePosition = false;
		}
		this.IsDormant = false;
		this.IsStuck = false;
		this.AgencyUpdateRequired = false;
		this.IsOnOffmeshLinkAndReachedNewCoord = false;
		base.InvokeRandomized(new Action(this.TickAi), 0.1f, 0.1f, 0.00500000035f);
		this.Sleep = Random.Range(0.5f, 1f);
		this.Stamina.Level = Random.Range(0.1f, 1f);
		this.Energy.Level = Random.Range(0.5f, 1f);
		this.Hydration.Level = Random.Range(0.5f, 1f);
		if (this.NewAI)
		{
			this.InitFacts();
			this.fleeHealthThresholdPercentage = this.Stats.HealthThresholdForFleeing;
			global::AnimalSensesLoadBalancer.animalSensesLoadBalancer.Add(this);
		}
	}

	// Token: 0x06000515 RID: 1301 RVA: 0x0001D100 File Offset: 0x0001B300
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		if (this.NewAI)
		{
			global::AnimalSensesLoadBalancer.animalSensesLoadBalancer.Remove(this);
		}
	}

	// Token: 0x17000012 RID: 18
	// (get) Token: 0x06000516 RID: 1302 RVA: 0x0001D120 File Offset: 0x0001B320
	bool ILoadBalanced.repeat
	{
		get
		{
			return true;
		}
	}

	// Token: 0x06000517 RID: 1303 RVA: 0x0001D124 File Offset: 0x0001B324
	float? ILoadBalanced.ExecuteUpdate(float deltaTime, float nextInterval)
	{
		if (base.IsDestroyed || this == null || base.transform == null)
		{
			global::AnimalSensesLoadBalancer.animalSensesLoadBalancer.Remove(this);
			return new float?(nextInterval);
		}
		using (TimeWarning.New("Animal.TickSenses", 0.1f))
		{
			this.TickSenses();
		}
		using (TimeWarning.New("Animal.TickBehaviourState", 0.1f))
		{
			this.TickBehaviourState();
		}
		return new float?(Random.value * 0.1f + 0.1f);
	}

	// Token: 0x06000518 RID: 1304 RVA: 0x0001D1F0 File Offset: 0x0001B3F0
	public override void Hurt(global::HitInfo info)
	{
		if (info.Initiator != null && this.AiContext != null)
		{
			this.AiContext.Memory.Update(info.Initiator, 0f);
			if (this.blockTargetingThisEnemy != null && this.blockTargetingThisEnemy.net != null && info.Initiator.net != null && this.blockTargetingThisEnemy.net.ID == info.Initiator.net.ID)
			{
				this.SetFact(global::BaseNpc.Facts.CanTargetEnemies, 1, true, true);
			}
			if (this.GetFact(global::BaseNpc.Facts.HasEnemy) == 0)
			{
				this.WantsToFlee();
			}
			else
			{
				this.TryAggro(global::BaseNpc.EnemyRangeEnum.AggroRange);
			}
		}
		base.Hurt(info);
	}

	// Token: 0x06000519 RID: 1305 RVA: 0x0001D2BC File Offset: 0x0001B4BC
	public override void OnKilled(global::HitInfo hitInfo = null)
	{
		Assert.IsTrue(base.isServer, "OnKilled called on client!");
		global::BaseCorpse baseCorpse = base.DropCorpse(this.CorpsePrefab.resourcePath);
		if (baseCorpse)
		{
			baseCorpse.Spawn();
			baseCorpse.TakeChildren(this);
		}
		base.Invoke(new Action(base.KillMessage), 0.5f);
	}

	// Token: 0x0600051A RID: 1306 RVA: 0x0001D31C File Offset: 0x0001B51C
	public override void OnSensation(Rust.Ai.Sensation sensation)
	{
		if (this.AiContext == null)
		{
			return;
		}
		Rust.Ai.SensationType type = sensation.Type;
		if (type == Rust.Ai.SensationType.Gunshot || type == Rust.Ai.SensationType.ThrownWeapon)
		{
			this.OnSenseGunshot(sensation);
		}
	}

	// Token: 0x0600051B RID: 1307 RVA: 0x0001D35C File Offset: 0x0001B55C
	protected virtual void OnSenseGunshot(Rust.Ai.Sensation sensation)
	{
		this.AiContext.Memory.AddDanger(sensation.Position, 1f);
		this._lastHeardGunshotTime = UnityEngine.Time.time;
		this.LastHeardGunshotDirection = (sensation.Position - base.transform.localPosition).normalized;
		if (this.CurrentBehaviour != global::BaseNpc.Behaviour.Attack)
		{
			this.CurrentBehaviour = global::BaseNpc.Behaviour.Flee;
		}
	}

	// Token: 0x17000038 RID: 56
	// (get) Token: 0x0600051C RID: 1308 RVA: 0x0001D3C8 File Offset: 0x0001B5C8
	public float SecondsSinceLastHeardGunshot
	{
		get
		{
			return UnityEngine.Time.time - this._lastHeardGunshotTime;
		}
	}

	// Token: 0x17000039 RID: 57
	// (get) Token: 0x0600051D RID: 1309 RVA: 0x0001D3D8 File Offset: 0x0001B5D8
	// (set) Token: 0x0600051E RID: 1310 RVA: 0x0001D3E0 File Offset: 0x0001B5E0
	public Vector3 LastHeardGunshotDirection { get; set; }

	// Token: 0x1700003A RID: 58
	// (get) Token: 0x0600051F RID: 1311 RVA: 0x0001D3EC File Offset: 0x0001B5EC
	// (set) Token: 0x06000520 RID: 1312 RVA: 0x0001D3F4 File Offset: 0x0001B5F4
	public float TargetSpeed { get; set; }

	// Token: 0x0400014A RID: 330
	public int agentTypeIndex;

	// Token: 0x0400014B RID: 331
	public bool NewAI;

	// Token: 0x0400014D RID: 333
	private Vector3 stepDirection;

	// Token: 0x04000152 RID: 338
	private float maxFleeTime;

	// Token: 0x04000153 RID: 339
	private float fleeHealthThresholdPercentage = 1f;

	// Token: 0x04000154 RID: 340
	private float blockEnemyTargetingTimeout = float.NegativeInfinity;

	// Token: 0x04000155 RID: 341
	private float blockFoodTargetingTimeout = float.NegativeInfinity;

	// Token: 0x04000156 RID: 342
	private float aggroTimeout = float.NegativeInfinity;

	// Token: 0x04000157 RID: 343
	private float lastAggroChanceResult;

	// Token: 0x04000158 RID: 344
	private float lastAggroChanceCalcTime;

	// Token: 0x04000159 RID: 345
	private const float aggroChanceRecalcTimeout = 5f;

	// Token: 0x0400015A RID: 346
	private float eatTimeout = float.NegativeInfinity;

	// Token: 0x0400015B RID: 347
	private float wakeUpBlockMoveTimeout = float.NegativeInfinity;

	// Token: 0x0400015C RID: 348
	private global::BaseEntity blockTargetingThisEnemy;

	// Token: 0x0400015D RID: 349
	private static readonly AnimationCurve speedFractionResponse = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

	// Token: 0x0400015E RID: 350
	private float nextAttackTime;

	// Token: 0x0400015F RID: 351
	[NonSerialized]
	public Transform ChaseTransform;

	// Token: 0x04000160 RID: 352
	[Header("BaseNpc")]
	public global::GameObjectRef CorpsePrefab;

	// Token: 0x04000161 RID: 353
	public global::BaseNpc.AiStatistics Stats;

	// Token: 0x04000162 RID: 354
	public Vector3 AttackOffset;

	// Token: 0x04000163 RID: 355
	public float AttackDamage = 20f;

	// Token: 0x04000164 RID: 356
	public Rust.DamageType AttackDamageType = Rust.DamageType.Bite;

	// Token: 0x04000165 RID: 357
	[Tooltip("Stamina to use per attack")]
	public float AttackCost = 0.1f;

	// Token: 0x04000166 RID: 358
	[Tooltip("How often can we attack")]
	public float AttackRate = 1f;

	// Token: 0x04000167 RID: 359
	[Tooltip("Maximum Distance for an attack")]
	public float AttackRange = 1f;

	// Token: 0x04000168 RID: 360
	public NavMeshAgent NavAgent;

	// Token: 0x04000169 RID: 361
	[SerializeField]
	private UtilityAIComponent utilityAiComponent;

	// Token: 0x0400016A RID: 362
	public LayerMask movementMask = 295772417;

	// Token: 0x0400016B RID: 363
	[NonSerialized]
	public Rust.Ai.BaseContext AiContext;

	// Token: 0x0400016C RID: 364
	public bool IsDormant;

	// Token: 0x04000171 RID: 369
	public global::StateTimer BusyTimer;

	// Token: 0x04000172 RID: 370
	public float Sleep;

	// Token: 0x04000173 RID: 371
	public global::VitalLevel Stamina;

	// Token: 0x04000174 RID: 372
	public global::VitalLevel Energy;

	// Token: 0x04000175 RID: 373
	public global::VitalLevel Hydration;

	// Token: 0x04000177 RID: 375
	[InspectorFlags]
	public global::BaseNpc.AiFlags aiFlags;

	// Token: 0x04000178 RID: 376
	[NonSerialized]
	public byte[] CurrentFacts = new byte[Enum.GetValues(typeof(global::BaseNpc.Facts)).Length];

	// Token: 0x04000179 RID: 377
	[Header("NPC Senses")]
	public int ForgetUnseenEntityTime = 10;

	// Token: 0x0400017A RID: 378
	public float SensesTickRate = 0.5f;

	// Token: 0x0400017B RID: 379
	[NonSerialized]
	public global::BaseEntity[] SensesResults = new global::BaseEntity[64];

	// Token: 0x0400017C RID: 380
	private List<Rust.Ai.NavPointSample> navPointSamples = new List<Rust.Ai.NavPointSample>(8);

	// Token: 0x0400017D RID: 381
	private float lastTickTime;

	// Token: 0x0400017E RID: 382
	public const float TickRate = 0.1f;

	// Token: 0x0400017F RID: 383
	private Vector3 lastStuckPos;

	// Token: 0x04000180 RID: 384
	public float stuckDuration;

	// Token: 0x04000181 RID: 385
	public float lastStuckTime;

	// Token: 0x04000182 RID: 386
	public float idleDuration;

	// Token: 0x04000183 RID: 387
	private float nextFlinchTime;

	// Token: 0x04000184 RID: 388
	private float _lastHeardGunshotTime = float.NegativeInfinity;

	// Token: 0x04000187 RID: 391
	[CompilerGenerated]
	private static Func<global::BaseEntity, bool> <>f__mg$cache0;

	// Token: 0x02000035 RID: 53
	public enum Behaviour
	{
		// Token: 0x04000189 RID: 393
		Idle,
		// Token: 0x0400018A RID: 394
		Wander,
		// Token: 0x0400018B RID: 395
		Attack,
		// Token: 0x0400018C RID: 396
		Flee,
		// Token: 0x0400018D RID: 397
		Eat,
		// Token: 0x0400018E RID: 398
		Sleep,
		// Token: 0x0400018F RID: 399
		RetreatingToCover
	}

	// Token: 0x02000036 RID: 54
	[Flags]
	public enum AiFlags
	{
		// Token: 0x04000191 RID: 401
		Sitting = 2,
		// Token: 0x04000192 RID: 402
		Chasing = 4,
		// Token: 0x04000193 RID: 403
		Sleeping = 8
	}

	// Token: 0x02000037 RID: 55
	public enum Facts
	{
		// Token: 0x04000195 RID: 405
		HasEnemy,
		// Token: 0x04000196 RID: 406
		EnemyRange,
		// Token: 0x04000197 RID: 407
		CanTargetEnemies,
		// Token: 0x04000198 RID: 408
		Health,
		// Token: 0x04000199 RID: 409
		Speed,
		// Token: 0x0400019A RID: 410
		IsTired,
		// Token: 0x0400019B RID: 411
		IsSleeping,
		// Token: 0x0400019C RID: 412
		IsAttackReady,
		// Token: 0x0400019D RID: 413
		IsRoamReady,
		// Token: 0x0400019E RID: 414
		IsAggro,
		// Token: 0x0400019F RID: 415
		WantsToFlee,
		// Token: 0x040001A0 RID: 416
		IsHungry,
		// Token: 0x040001A1 RID: 417
		FoodRange,
		// Token: 0x040001A2 RID: 418
		AttackedLately,
		// Token: 0x040001A3 RID: 419
		LoudNoiseNearby,
		// Token: 0x040001A4 RID: 420
		CanTargetFood,
		// Token: 0x040001A5 RID: 421
		IsMoving,
		// Token: 0x040001A6 RID: 422
		IsFleeing,
		// Token: 0x040001A7 RID: 423
		IsEating,
		// Token: 0x040001A8 RID: 424
		IsAfraid,
		// Token: 0x040001A9 RID: 425
		AfraidRange,
		// Token: 0x040001AA RID: 426
		IsUnderHealthThreshold,
		// Token: 0x040001AB RID: 427
		CanNotMove
	}

	// Token: 0x02000038 RID: 56
	public enum EnemyRangeEnum : byte
	{
		// Token: 0x040001AD RID: 429
		AttackRange,
		// Token: 0x040001AE RID: 430
		AggroRange,
		// Token: 0x040001AF RID: 431
		AwareRange,
		// Token: 0x040001B0 RID: 432
		OutOfRange
	}

	// Token: 0x02000039 RID: 57
	public enum FoodRangeEnum : byte
	{
		// Token: 0x040001B2 RID: 434
		EatRange,
		// Token: 0x040001B3 RID: 435
		AwareRange,
		// Token: 0x040001B4 RID: 436
		OutOfRange
	}

	// Token: 0x0200003A RID: 58
	public enum AfraidRangeEnum : byte
	{
		// Token: 0x040001B6 RID: 438
		InAfraidRange,
		// Token: 0x040001B7 RID: 439
		OutOfRange
	}

	// Token: 0x0200003B RID: 59
	public enum HealthEnum : byte
	{
		// Token: 0x040001B9 RID: 441
		Fine,
		// Token: 0x040001BA RID: 442
		Medium,
		// Token: 0x040001BB RID: 443
		Low
	}

	// Token: 0x0200003C RID: 60
	public enum SpeedEnum : byte
	{
		// Token: 0x040001BD RID: 445
		StandStill,
		// Token: 0x040001BE RID: 446
		Walk,
		// Token: 0x040001BF RID: 447
		Run
	}

	// Token: 0x0200003D RID: 61
	[Serializable]
	public struct AiStatistics
	{
		// Token: 0x040001C0 RID: 448
		[Tooltip("Ai will be less likely to fight animals that are larger than them, and more likely to flee from them.")]
		[Range(0f, 1f)]
		public float Size;

		// Token: 0x040001C1 RID: 449
		[Tooltip("How fast we can move")]
		public float Speed;

		// Token: 0x040001C2 RID: 450
		[Tooltip("How fast can we accelerate")]
		public float Acceleration;

		// Token: 0x040001C3 RID: 451
		[Tooltip("How fast can we turn around")]
		public float TurnSpeed;

		// Token: 0x040001C4 RID: 452
		[Range(0f, 1f)]
		[Tooltip("Determines things like how near we'll allow other species to get")]
		public float Tolerance;

		// Token: 0x040001C5 RID: 453
		[Tooltip("How far this NPC can see")]
		public float VisionRange;

		// Token: 0x040001C6 RID: 454
		[Tooltip("Our vision cone for dot product - a value of -1 means we can see all around us, 0 = only infront ")]
		public float VisionCone;

		// Token: 0x040001C7 RID: 455
		[Tooltip("NPCs use distance visibility to basically make closer enemies easier to detect than enemies further away")]
		public AnimationCurve DistanceVisibility;

		// Token: 0x040001C8 RID: 456
		[Tooltip("How likely are we to be offensive without being threatened")]
		public float Hostility;

		// Token: 0x040001C9 RID: 457
		[Tooltip("How likely are we to defend ourselves when attacked")]
		public float Defensiveness;

		// Token: 0x040001CA RID: 458
		[Tooltip("The range at which we will engage targets")]
		public float AggressionRange;

		// Token: 0x040001CB RID: 459
		[Tooltip("The range at which an aggrified npc will disengage it's current target")]
		public float DeaggroRange;

		// Token: 0x040001CC RID: 460
		[Tooltip("For how long will we chase a target until we give up")]
		public float DeaggroChaseTime;

		// Token: 0x040001CD RID: 461
		[Tooltip("When we deaggro, how long do we wait until we can aggro again.")]
		public float DeaggroCooldown;

		// Token: 0x040001CE RID: 462
		[Tooltip("The threshold of our health fraction where there's a chance that we want to flee")]
		public float HealthThresholdForFleeing;

		// Token: 0x040001CF RID: 463
		[Tooltip("The chance that we will flee when our health threshold is triggered")]
		public float HealthThresholdFleeChance;

		// Token: 0x040001D0 RID: 464
		[Tooltip("When we flee, what is the minimum distance we should flee?")]
		public float MinFleeRange;

		// Token: 0x040001D1 RID: 465
		[Tooltip("When we flee, what is the maximum distance we should flee?")]
		public float MaxFleeRange;

		// Token: 0x040001D2 RID: 466
		[Tooltip("When we flee, what is the maximum time that can pass until we stop?")]
		public float MaxFleeTime;

		// Token: 0x040001D3 RID: 467
		[Tooltip("At what range we are afraid of a target that is in our Is Afraid Of list.")]
		public float AfraidRange;

		// Token: 0x040001D4 RID: 468
		[Tooltip("The family this npc belong to. Npcs in the same family will not attack each other.")]
		public global::BaseNpc.AiStatistics.FamilyEnum Family;

		// Token: 0x040001D5 RID: 469
		[Tooltip("List of the types of Npc that we are afraid of.")]
		public global::BaseNpc.AiStatistics.FamilyEnum[] IsAfraidOf;

		// Token: 0x040001D6 RID: 470
		[Tooltip("The minimum distance this npc will wander when idle.")]
		public float MinRoamRange;

		// Token: 0x040001D7 RID: 471
		[Tooltip("The maximum distance this npc will wander when idle.")]
		public float MaxRoamRange;

		// Token: 0x040001D8 RID: 472
		[Tooltip("The minimum amount of time between each time we seek a new roam destination (when idle)")]
		public float MinRoamDelay;

		// Token: 0x040001D9 RID: 473
		[Tooltip("The maximum amount of time between each time we seek a new roam destination (when idle)")]
		public float MaxRoamDelay;

		// Token: 0x040001DA RID: 474
		[Tooltip("In the range between min and max roam delay, we evaluate the random value through this curve")]
		public AnimationCurve RoamDelayDistribution;

		// Token: 0x040001DB RID: 475
		[Tooltip("For how long do we remember that someone attacked us")]
		public float AttackedMemoryTime;

		// Token: 0x040001DC RID: 476
		[Tooltip("How long should we block movement to make the wakeup animation not look whack?")]
		public float WakeupBlockMoveTime;

		// Token: 0x040001DD RID: 477
		[Tooltip("The maximum water depth this npc willingly will walk into.")]
		public float MaxWaterDepth;

		// Token: 0x040001DE RID: 478
		[Tooltip("The range we consider using close range weapons.")]
		public float CloseRange;

		// Token: 0x040001DF RID: 479
		[Tooltip("The range we consider using medium range weapons.")]
		public float MediumRange;

		// Token: 0x040001E0 RID: 480
		[Tooltip("The range we consider using long range weapons.")]
		public float LongRange;

		// Token: 0x040001E1 RID: 481
		[Tooltip("How long can we be out of range of our spawn point before we time out and make our way back home (when idle).")]
		public float OutOfRangeOfSpawnPointTimeout;

		// Token: 0x040001E2 RID: 482
		[Tooltip("What is the maximum distance we are allowed to have to our spawn location before we are being encourraged to go back home.")]
		public global::NPCPlayerApex.EnemyRangeEnum MaxRangeToSpawnLoc;

		// Token: 0x0200003E RID: 62
		public enum FamilyEnum
		{
			// Token: 0x040001E4 RID: 484
			Bear,
			// Token: 0x040001E5 RID: 485
			Wolf,
			// Token: 0x040001E6 RID: 486
			Deer,
			// Token: 0x040001E7 RID: 487
			Boar,
			// Token: 0x040001E8 RID: 488
			Chicken,
			// Token: 0x040001E9 RID: 489
			Horse,
			// Token: 0x040001EA RID: 490
			Zombie,
			// Token: 0x040001EB RID: 491
			Scientist,
			// Token: 0x040001EC RID: 492
			Murderer,
			// Token: 0x040001ED RID: 493
			Player
		}
	}
}
