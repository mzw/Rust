﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000662 RID: 1634
public class StringPool
{
	// Token: 0x060020A9 RID: 8361 RVA: 0x000BA474 File Offset: 0x000B8674
	private static void Init()
	{
		if (global::StringPool.initialized)
		{
			return;
		}
		global::StringPool.toString = new Dictionary<uint, string>();
		global::StringPool.toNumber = new Dictionary<string, uint>(StringComparer.OrdinalIgnoreCase);
		global::GameManifest gameManifest = global::FileSystem.Load<global::GameManifest>("Assets/manifest.asset", true);
		uint num = 0u;
		while ((ulong)num < (ulong)((long)gameManifest.pooledStrings.Length))
		{
			global::StringPool.toString.Add(gameManifest.pooledStrings[(int)((UIntPtr)num)].hash, gameManifest.pooledStrings[(int)((UIntPtr)num)].str);
			global::StringPool.toNumber.Add(gameManifest.pooledStrings[(int)((UIntPtr)num)].str, gameManifest.pooledStrings[(int)((UIntPtr)num)].hash);
			num += 1u;
		}
		global::StringPool.initialized = true;
		global::StringPool.closest = global::StringPool.Get("closest");
	}

	// Token: 0x060020AA RID: 8362 RVA: 0x000BA540 File Offset: 0x000B8740
	public static string Get(uint i)
	{
		if (i == 0u)
		{
			return string.Empty;
		}
		global::StringPool.Init();
		string result;
		if (global::StringPool.toString.TryGetValue(i, out result))
		{
			return result;
		}
		Debug.LogWarning("StringPool.GetString - no string for ID" + i);
		return string.Empty;
	}

	// Token: 0x060020AB RID: 8363 RVA: 0x000BA58C File Offset: 0x000B878C
	public static uint Get(string i)
	{
		if (string.IsNullOrEmpty(i))
		{
			return 0u;
		}
		global::StringPool.Init();
		uint result;
		if (global::StringPool.toNumber.TryGetValue(i, out result))
		{
			return result;
		}
		Debug.LogWarning("StringPool.GetNumber - no number for string " + i);
		return 0u;
	}

	// Token: 0x060020AC RID: 8364 RVA: 0x000BA5D0 File Offset: 0x000B87D0
	public static uint Add(string str)
	{
		uint num = 0u;
		if (!global::StringPool.toNumber.TryGetValue(str, out num))
		{
			num = (uint)str.GetHashCode();
			global::StringPool.toString.Add(num, str);
			global::StringPool.toNumber.Add(str, num);
		}
		return num;
	}

	// Token: 0x04001BF2 RID: 7154
	private static Dictionary<uint, string> toString;

	// Token: 0x04001BF3 RID: 7155
	private static Dictionary<string, uint> toNumber;

	// Token: 0x04001BF4 RID: 7156
	private static bool initialized;

	// Token: 0x04001BF5 RID: 7157
	public static uint closest;
}
