﻿using System;
using UnityEngine;

// Token: 0x0200036D RID: 877
public class EntityItem_RotateWhenOn : global::EntityComponent<global::BaseEntity>
{
	// Token: 0x04000F62 RID: 3938
	public global::EntityItem_RotateWhenOn.State on;

	// Token: 0x04000F63 RID: 3939
	public global::EntityItem_RotateWhenOn.State off;

	// Token: 0x04000F64 RID: 3940
	internal bool currentlyOn;

	// Token: 0x04000F65 RID: 3941
	internal bool stateInitialized;

	// Token: 0x04000F66 RID: 3942
	public global::BaseEntity.Flags targetFlag = global::BaseEntity.Flags.On;

	// Token: 0x0200036E RID: 878
	[Serializable]
	public class State
	{
		// Token: 0x04000F67 RID: 3943
		public Vector3 rotation = default(Vector3);

		// Token: 0x04000F68 RID: 3944
		public float initialDelay;

		// Token: 0x04000F69 RID: 3945
		public float timeToTake = 2f;

		// Token: 0x04000F6A RID: 3946
		public AnimationCurve animationCurve = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, 0f),
			new Keyframe(1f, 1f)
		});

		// Token: 0x04000F6B RID: 3947
		public string effectOnStart = string.Empty;

		// Token: 0x04000F6C RID: 3948
		public string effectOnFinish = string.Empty;

		// Token: 0x04000F6D RID: 3949
		public global::SoundDefinition movementLoop;

		// Token: 0x04000F6E RID: 3950
		public float movementLoopFadeOutTime = 0.1f;

		// Token: 0x04000F6F RID: 3951
		public global::SoundDefinition startSound;

		// Token: 0x04000F70 RID: 3952
		public global::SoundDefinition stopSound;
	}
}
