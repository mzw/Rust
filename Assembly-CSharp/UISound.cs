﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x020006F3 RID: 1779
public static class UISound
{
	// Token: 0x060021CC RID: 8652 RVA: 0x000BDCAC File Offset: 0x000BBEAC
	private static AudioSource GetAudioSource()
	{
		if (global::UISound.source != null)
		{
			return global::UISound.source;
		}
		GameObject gameObject = new GameObject("UISound");
		global::UISound.source = gameObject.AddComponent<AudioSource>();
		global::UISound.source.spatialBlend = 0f;
		global::UISound.source.volume = 1f;
		return global::UISound.source;
	}

	// Token: 0x060021CD RID: 8653 RVA: 0x000BDD08 File Offset: 0x000BBF08
	public static void Play(AudioClip clip, float volume = 1f)
	{
		if (clip == null)
		{
			return;
		}
		global::UISound.GetAudioSource().volume = volume * ConVar.Audio.master;
		global::UISound.GetAudioSource().PlayOneShot(clip);
	}

	// Token: 0x04001E4E RID: 7758
	private static AudioSource source;
}
