﻿using System;
using ConVar;
using UnityEngine;
using UnityEngine.Audio;

// Token: 0x02000238 RID: 568
public class AudioSettings : MonoBehaviour
{
	// Token: 0x0600101B RID: 4123 RVA: 0x00061C5C File Offset: 0x0005FE5C
	private void Update()
	{
		if (this.mixer == null)
		{
			return;
		}
		this.mixer.SetFloat("MasterVol", this.LinearToDecibel(ConVar.Audio.master));
		float num;
		this.mixer.GetFloat("MusicVol", ref num);
		if (!global::LevelManager.isLoaded || !global::MainCamera.isValid)
		{
			this.mixer.SetFloat("MusicVol", Mathf.Lerp(num, this.LinearToDecibel(ConVar.Audio.musicvolumemenu), UnityEngine.Time.deltaTime));
		}
		else
		{
			this.mixer.SetFloat("MusicVol", Mathf.Lerp(num, this.LinearToDecibel(ConVar.Audio.musicvolume), UnityEngine.Time.deltaTime));
		}
		this.mixer.SetFloat("WorldVol", this.LinearToDecibel(ConVar.Audio.game));
		this.mixer.SetFloat("VoiceVol", this.LinearToDecibel(ConVar.Audio.voices));
	}

	// Token: 0x0600101C RID: 4124 RVA: 0x00061D4C File Offset: 0x0005FF4C
	private float LinearToDecibel(float linear)
	{
		float result;
		if (linear > 0f)
		{
			result = 20f * Mathf.Log10(linear);
		}
		else
		{
			result = -144f;
		}
		return result;
	}

	// Token: 0x04000ADA RID: 2778
	public AudioMixer mixer;
}
