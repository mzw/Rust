﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200007D RID: 125
public class MapEntity : global::HeldEntity
{
	// Token: 0x06000878 RID: 2168 RVA: 0x00036FFC File Offset: 0x000351FC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("MapEntity.OnRpcMessage", 0.1f))
		{
			if (rpc == 3144626660u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ImageUpdate ");
				}
				using (TimeWarning.New("ImageUpdate", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("ImageUpdate", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ImageUpdate(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in ImageUpdate");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000879 RID: 2169 RVA: 0x000371D8 File Offset: 0x000353D8
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.mapEntity != null)
		{
			if (info.msg.mapEntity.fogImages.Count == this.fogImages.Length)
			{
				this.fogImages = info.msg.mapEntity.fogImages.ToArray();
			}
			if (info.msg.mapEntity.paintImages.Count == this.paintImages.Length)
			{
				this.paintImages = info.msg.mapEntity.paintImages.ToArray();
			}
		}
	}

	// Token: 0x0600087A RID: 2170 RVA: 0x0003727C File Offset: 0x0003547C
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.mapEntity = Facepunch.Pool.Get<ProtoBuf.MapEntity>();
		info.msg.mapEntity.fogImages = Facepunch.Pool.Get<List<uint>>();
		info.msg.mapEntity.fogImages.AddRange(this.fogImages);
		info.msg.mapEntity.paintImages = Facepunch.Pool.Get<List<uint>>();
		info.msg.mapEntity.paintImages.AddRange(this.paintImages);
	}

	// Token: 0x0600087B RID: 2171 RVA: 0x00037308 File Offset: 0x00035508
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.FromOwner]
	public void ImageUpdate(global::BaseEntity.RPCMessage msg)
	{
		byte b = msg.read.UInt8();
		byte b2 = msg.read.UInt8();
		uint num = msg.read.UInt32();
		if (b == 0 && this.fogImages[(int)b2] == num)
		{
			return;
		}
		if (b == 1 && this.paintImages[(int)b2] == num)
		{
			return;
		}
		uint num2 = (uint)b * 1000u + (uint)b2;
		byte[] array = msg.read.BytesWithSize();
		if (array == null)
		{
			return;
		}
		global::FileStorage.server.RemoveEntityNum(this.net.ID, num2);
		uint num3 = global::FileStorage.server.Store(array, global::FileStorage.Type.png, this.net.ID, num2);
		if (b == 0)
		{
			this.fogImages[(int)b2] = num3;
		}
		if (b == 1)
		{
			this.paintImages[(int)b2] = num3;
		}
		base.InvalidateNetworkCache();
		Interface.CallHook("OnMapImageUpdated", null);
	}

	// Token: 0x040003EF RID: 1007
	[NonSerialized]
	public uint[] fogImages = new uint[1];

	// Token: 0x040003F0 RID: 1008
	[NonSerialized]
	public uint[] paintImages = new uint[144];
}
