﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using UnityEngine;

// Token: 0x02000295 RID: 661
public static class GamePhysics
{
	// Token: 0x06001126 RID: 4390 RVA: 0x000666E0 File Offset: 0x000648E0
	public static bool CheckSphere(Vector3 position, float radius, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(position, layerMask);
		return UnityEngine.Physics.CheckSphere(position, radius, layerMask, triggerInteraction);
	}

	// Token: 0x06001127 RID: 4391 RVA: 0x000666F4 File Offset: 0x000648F4
	public static bool CheckCapsule(Vector3 start, Vector3 end, float radius, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision((start + end) * 0.5f, layerMask);
		return UnityEngine.Physics.CheckCapsule(start, end, radius, layerMask, triggerInteraction);
	}

	// Token: 0x06001128 RID: 4392 RVA: 0x0006671C File Offset: 0x0006491C
	public static bool CheckOBB(OBB obb, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(obb.position, layerMask);
		return UnityEngine.Physics.CheckBox(obb.position, obb.extents, obb.rotation, layerMask, triggerInteraction);
	}

	// Token: 0x06001129 RID: 4393 RVA: 0x0006674C File Offset: 0x0006494C
	public static bool CheckBounds(Bounds bounds, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(bounds.center, layerMask);
		return UnityEngine.Physics.CheckBox(bounds.center, bounds.extents, Quaternion.identity, layerMask, triggerInteraction);
	}

	// Token: 0x0600112A RID: 4394 RVA: 0x00066778 File Offset: 0x00064978
	public static void OverlapSphere(Vector3 position, float radius, List<Collider> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(position, layerMask);
		int count = UnityEngine.Physics.OverlapSphereNonAlloc(position, radius, global::GamePhysics.colBuffer, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList(count, list);
	}

	// Token: 0x0600112B RID: 4395 RVA: 0x000667A8 File Offset: 0x000649A8
	public static void OverlapCapsule(Vector3 point0, Vector3 point1, float radius, List<Collider> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(point0, layerMask);
		layerMask = global::GamePhysics.HandleTerrainCollision(point1, layerMask);
		int count = UnityEngine.Physics.OverlapCapsuleNonAlloc(point0, point1, radius, global::GamePhysics.colBuffer, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList(count, list);
	}

	// Token: 0x0600112C RID: 4396 RVA: 0x000667E4 File Offset: 0x000649E4
	public static void OverlapOBB(OBB obb, List<Collider> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(obb.position, layerMask);
		int count = UnityEngine.Physics.OverlapBoxNonAlloc(obb.position, obb.extents, global::GamePhysics.colBuffer, obb.rotation, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList(count, list);
	}

	// Token: 0x0600112D RID: 4397 RVA: 0x0006682C File Offset: 0x00064A2C
	public static void OverlapBounds(Bounds bounds, List<Collider> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1)
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(bounds.center, layerMask);
		int count = UnityEngine.Physics.OverlapBoxNonAlloc(bounds.center, bounds.extents, global::GamePhysics.colBuffer, Quaternion.identity, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList(count, list);
	}

	// Token: 0x0600112E RID: 4398 RVA: 0x00066870 File Offset: 0x00064A70
	private static void BufferToList(int count, List<Collider> list)
	{
		if (count >= global::GamePhysics.colBuffer.Length)
		{
			Debug.LogWarning("Physics query is exceeding collider buffer length.");
		}
		for (int i = 0; i < count; i++)
		{
			list.Add(global::GamePhysics.colBuffer[i]);
			global::GamePhysics.colBuffer[i] = null;
		}
	}

	// Token: 0x0600112F RID: 4399 RVA: 0x000668BC File Offset: 0x00064ABC
	public static bool CheckSphere<T>(Vector3 pos, float radius, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		List<Collider> list = Facepunch.Pool.GetList<Collider>();
		global::GamePhysics.OverlapSphere(pos, radius, list, layerMask, triggerInteraction);
		bool result = global::GamePhysics.CheckComponent<T>(list);
		Facepunch.Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x06001130 RID: 4400 RVA: 0x000668E8 File Offset: 0x00064AE8
	public static bool CheckCapsule<T>(Vector3 start, Vector3 end, float radius, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		List<Collider> list = Facepunch.Pool.GetList<Collider>();
		global::GamePhysics.OverlapCapsule(start, end, radius, list, layerMask, triggerInteraction);
		bool result = global::GamePhysics.CheckComponent<T>(list);
		Facepunch.Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x06001131 RID: 4401 RVA: 0x00066918 File Offset: 0x00064B18
	public static bool CheckOBB<T>(OBB obb, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		List<Collider> list = Facepunch.Pool.GetList<Collider>();
		global::GamePhysics.OverlapOBB(obb, list, layerMask, triggerInteraction);
		bool result = global::GamePhysics.CheckComponent<T>(list);
		Facepunch.Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x06001132 RID: 4402 RVA: 0x00066944 File Offset: 0x00064B44
	public static bool CheckBounds<T>(Bounds bounds, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		List<Collider> list = Facepunch.Pool.GetList<Collider>();
		global::GamePhysics.OverlapBounds(bounds, list, layerMask, triggerInteraction);
		bool result = global::GamePhysics.CheckComponent<T>(list);
		Facepunch.Pool.FreeList<Collider>(ref list);
		return result;
	}

	// Token: 0x06001133 RID: 4403 RVA: 0x00066970 File Offset: 0x00064B70
	private static bool CheckComponent<T>(List<Collider> list)
	{
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].gameObject.GetComponent<T>() != null)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x06001134 RID: 4404 RVA: 0x000669B4 File Offset: 0x00064BB4
	public static void OverlapSphere<T>(Vector3 position, float radius, List<T> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(position, layerMask);
		int count = UnityEngine.Physics.OverlapSphereNonAlloc(position, radius, global::GamePhysics.colBuffer, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList<T>(count, list);
	}

	// Token: 0x06001135 RID: 4405 RVA: 0x000669E4 File Offset: 0x00064BE4
	public static void OverlapCapsule<T>(Vector3 point0, Vector3 point1, float radius, List<T> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(point0, layerMask);
		layerMask = global::GamePhysics.HandleTerrainCollision(point1, layerMask);
		int count = UnityEngine.Physics.OverlapCapsuleNonAlloc(point0, point1, radius, global::GamePhysics.colBuffer, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList<T>(count, list);
	}

	// Token: 0x06001136 RID: 4406 RVA: 0x00066A20 File Offset: 0x00064C20
	public static void OverlapOBB<T>(OBB obb, List<T> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(obb.position, layerMask);
		int count = UnityEngine.Physics.OverlapBoxNonAlloc(obb.position, obb.extents, global::GamePhysics.colBuffer, obb.rotation, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList<T>(count, list);
	}

	// Token: 0x06001137 RID: 4407 RVA: 0x00066A68 File Offset: 0x00064C68
	public static void OverlapBounds<T>(Bounds bounds, List<T> list, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 1) where T : Component
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(bounds.center, layerMask);
		int count = UnityEngine.Physics.OverlapBoxNonAlloc(bounds.center, bounds.extents, global::GamePhysics.colBuffer, Quaternion.identity, layerMask, triggerInteraction);
		global::GamePhysics.BufferToList<T>(count, list);
	}

	// Token: 0x06001138 RID: 4408 RVA: 0x00066AAC File Offset: 0x00064CAC
	private static void BufferToList<T>(int count, List<T> list) where T : Component
	{
		if (count >= global::GamePhysics.colBuffer.Length)
		{
			Debug.LogWarning("Physics query is exceeding collider buffer length.");
		}
		for (int i = 0; i < count; i++)
		{
			T component = global::GamePhysics.colBuffer[i].gameObject.GetComponent<T>();
			if (component)
			{
				list.Add(component);
			}
			global::GamePhysics.colBuffer[i] = null;
		}
	}

	// Token: 0x06001139 RID: 4409 RVA: 0x00066B14 File Offset: 0x00064D14
	public static bool Trace(Ray ray, float radius, out RaycastHit hitInfo, float maxDistance = float.PositiveInfinity, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		List<RaycastHit> list = Facepunch.Pool.GetList<RaycastHit>();
		global::GamePhysics.TraceAllUnordered(ray, radius, list, maxDistance, layerMask, triggerInteraction);
		if (list.Count == 0)
		{
			hitInfo = default(RaycastHit);
			Facepunch.Pool.FreeList<RaycastHit>(ref list);
			return false;
		}
		global::GamePhysics.Sort(list);
		hitInfo = list[0];
		Facepunch.Pool.FreeList<RaycastHit>(ref list);
		return true;
	}

	// Token: 0x0600113A RID: 4410 RVA: 0x00066B6C File Offset: 0x00064D6C
	public static void TraceAll(Ray ray, float radius, List<RaycastHit> hits, float maxDistance = float.PositiveInfinity, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		global::GamePhysics.TraceAllUnordered(ray, radius, hits, maxDistance, layerMask, triggerInteraction);
		global::GamePhysics.Sort(hits);
	}

	// Token: 0x0600113B RID: 4411 RVA: 0x00066B84 File Offset: 0x00064D84
	public static void TraceAllUnordered(Ray ray, float radius, List<RaycastHit> hits, float maxDistance = float.PositiveInfinity, int layerMask = -5, QueryTriggerInteraction triggerInteraction = 0)
	{
		int num;
		if (radius == 0f)
		{
			num = UnityEngine.Physics.RaycastNonAlloc(ray, global::GamePhysics.hitBuffer, maxDistance, layerMask, triggerInteraction);
		}
		else
		{
			num = UnityEngine.Physics.SphereCastNonAlloc(ray, radius, global::GamePhysics.hitBuffer, maxDistance, layerMask, triggerInteraction);
		}
		if (num == 0)
		{
			return;
		}
		if (num >= global::GamePhysics.hitBuffer.Length)
		{
			Debug.LogWarning("Physics query is exceeding hit buffer length.");
		}
		for (int i = 0; i < num; i++)
		{
			RaycastHit raycastHit = global::GamePhysics.hitBuffer[i];
			if (global::GamePhysics.Verify(raycastHit))
			{
				hits.Add(raycastHit);
			}
		}
	}

	// Token: 0x0600113C RID: 4412 RVA: 0x00066C1C File Offset: 0x00064E1C
	public static bool LineOfSight(Vector3 p0, Vector3 p1, int layerMask, float padding = 0f)
	{
		Vector3 vector = p1 - p0;
		float magnitude = vector.magnitude;
		if (magnitude <= padding + padding)
		{
			return true;
		}
		Vector3 vector2 = vector / magnitude;
		Vector3 vector3 = vector2 * padding;
		RaycastHit raycastHit;
		if (!UnityEngine.Physics.Linecast(p0 + vector3, p1 - vector3, ref raycastHit, layerMask, 1))
		{
			if (ConVar.Vis.lineofsight)
			{
				global::ConsoleNetwork.BroadcastToAllClients("ddraw.line", new object[]
				{
					60f,
					Color.green,
					p0,
					p1
				});
			}
			return true;
		}
		if (ConVar.Vis.lineofsight)
		{
			global::ConsoleNetwork.BroadcastToAllClients("ddraw.line", new object[]
			{
				60f,
				Color.red,
				p0,
				p1
			});
			global::ConsoleNetwork.BroadcastToAllClients("ddraw.text", new object[]
			{
				60f,
				Color.white,
				raycastHit.point,
				raycastHit.collider.name
			});
		}
		return false;
	}

	// Token: 0x0600113D RID: 4413 RVA: 0x00066D48 File Offset: 0x00064F48
	public static bool LineOfSight(Vector3 p0, Vector3 p1, Vector3 p2, int layerMask, float padding = 0f)
	{
		return global::GamePhysics.LineOfSight(p0, p1, layerMask, padding) && global::GamePhysics.LineOfSight(p1, p2, layerMask, padding);
	}

	// Token: 0x0600113E RID: 4414 RVA: 0x00066D68 File Offset: 0x00064F68
	public static bool LineOfSight(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, int layerMask, float padding = 0f)
	{
		return global::GamePhysics.LineOfSight(p0, p1, layerMask, padding) && global::GamePhysics.LineOfSight(p1, p2, layerMask, padding) && global::GamePhysics.LineOfSight(p2, p3, layerMask, padding);
	}

	// Token: 0x0600113F RID: 4415 RVA: 0x00066D98 File Offset: 0x00064F98
	public static bool Verify(RaycastHit hitInfo)
	{
		return global::GamePhysics.Verify(hitInfo.collider, hitInfo.point);
	}

	// Token: 0x06001140 RID: 4416 RVA: 0x00066DB0 File Offset: 0x00064FB0
	public static bool Verify(Collider collider, Vector3 point)
	{
		return (!(collider is TerrainCollider) || !global::TerrainMeta.Collision || !global::TerrainMeta.Collision.GetIgnore(point, 0.01f)) && collider.enabled;
	}

	// Token: 0x06001141 RID: 4417 RVA: 0x00066DEC File Offset: 0x00064FEC
	public static int HandleTerrainCollision(Vector3 position, int layerMask)
	{
		int num = 8388608;
		if ((layerMask & num) != 0 && global::TerrainMeta.Collision && global::TerrainMeta.Collision.GetIgnore(position, 0.01f))
		{
			layerMask &= ~num;
		}
		return layerMask;
	}

	// Token: 0x06001142 RID: 4418 RVA: 0x00066E34 File Offset: 0x00065034
	public static void Sort(List<RaycastHit> hits)
	{
		hits.Sort((RaycastHit a, RaycastHit b) => a.distance.CompareTo(b.distance));
	}

	// Token: 0x06001143 RID: 4419 RVA: 0x00066E5C File Offset: 0x0006505C
	public static void Sort(RaycastHit[] hits)
	{
		Array.Sort<RaycastHit>(hits, (RaycastHit a, RaycastHit b) => a.distance.CompareTo(b.distance));
	}

	// Token: 0x04000C2F RID: 3119
	public const int BufferLength = 8192;

	// Token: 0x04000C30 RID: 3120
	private static RaycastHit[] hitBuffer = new RaycastHit[8192];

	// Token: 0x04000C31 RID: 3121
	private static Collider[] colBuffer = new Collider[8192];
}
