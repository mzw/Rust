﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000485 RID: 1157
[CreateAssetMenu(menuName = "Rust/Convar Controlled Spawn Population")]
public class ConvarControlledSpawnPopulation : global::SpawnPopulation
{
	// Token: 0x170001B9 RID: 441
	// (get) Token: 0x06001920 RID: 6432 RVA: 0x0008DBE0 File Offset: 0x0008BDE0
	protected ConsoleSystem.Command Command
	{
		get
		{
			if (this._command == null)
			{
				this._command = ConsoleSystem.Index.Server.Find(this.PopulationConvar);
				Assert.IsNotNull<ConsoleSystem.Command>(this._command, string.Format("{0} has missing convar {1}", this, this.PopulationConvar));
			}
			return this._command;
		}
	}

	// Token: 0x170001BA RID: 442
	// (get) Token: 0x06001921 RID: 6433 RVA: 0x0008DC20 File Offset: 0x0008BE20
	public override float TargetDensity
	{
		get
		{
			return this.Command.AsFloat;
		}
	}

	// Token: 0x040013DA RID: 5082
	[Header("Convars")]
	public string PopulationConvar;

	// Token: 0x040013DB RID: 5083
	private ConsoleSystem.Command _command;
}
