﻿using System;
using UnityEngine;

// Token: 0x02000268 RID: 616
public class IconSkinPicker : MonoBehaviour
{
	// Token: 0x04000B4A RID: 2890
	public global::GameObjectRef pickerIcon;

	// Token: 0x04000B4B RID: 2891
	public GameObject container;

	// Token: 0x04000B4C RID: 2892
	public Action skinChangedEvent;
}
