﻿using System;
using UnityEngine;

// Token: 0x0200070A RID: 1802
public class Tooltip : global::BaseMonoBehaviour
{
	// Token: 0x1700026F RID: 623
	// (get) Token: 0x0600224E RID: 8782 RVA: 0x000C07AC File Offset: 0x000BE9AC
	public string english
	{
		get
		{
			return this.Text;
		}
	}

	// Token: 0x04001EC1 RID: 7873
	public static GameObject Current;

	// Token: 0x04001EC2 RID: 7874
	[TextArea]
	public string Text;

	// Token: 0x04001EC3 RID: 7875
	public GameObject TooltipObject;

	// Token: 0x04001EC4 RID: 7876
	public string token = string.Empty;
}
