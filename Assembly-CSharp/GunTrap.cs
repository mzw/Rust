﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using Oxide.Core;
using Rust;
using UnityEngine;

// Token: 0x0200006B RID: 107
public class GunTrap : global::StorageContainer
{
	// Token: 0x060007DE RID: 2014 RVA: 0x000336D4 File Offset: 0x000318D4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("GunTrap.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060007DF RID: 2015 RVA: 0x0003371C File Offset: 0x0003191C
	public bool UseAmmo()
	{
		foreach (global::Item item in this.inventory.itemList)
		{
			if (item.info == this.ammoType && item.amount > 0)
			{
				item.UseItem(1);
				return true;
			}
		}
		return false;
	}

	// Token: 0x060007E0 RID: 2016 RVA: 0x000337AC File Offset: 0x000319AC
	public void FireWeapon()
	{
		if (!this.UseAmmo())
		{
			return;
		}
		global::Effect.server.Run(this.gun_fire_effect.resourcePath, this, global::StringPool.Get(this.muzzlePos.gameObject.name), Vector3.zero, Vector3.zero, null, false);
		for (int i = 0; i < this.numPellets; i++)
		{
			this.FireBullet();
		}
	}

	// Token: 0x060007E1 RID: 2017 RVA: 0x00033814 File Offset: 0x00031A14
	public void FireBullet()
	{
		float damageAmount = 10f;
		Vector3 vector = this.muzzlePos.transform.position - this.muzzlePos.forward * 0.25f;
		Vector3 forward = this.muzzlePos.transform.forward;
		Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection((float)this.aimCone, forward, true);
		Vector3 arg = vector + modifiedAimConeDirection * 300f;
		base.ClientRPC<Vector3>(null, "CLIENT_FireGun", arg);
		List<RaycastHit> list = Pool.GetList<RaycastHit>();
		global::GamePhysics.TraceAll(new Ray(vector, modifiedAimConeDirection), 0.1f, list, 300f, 1084435201, 0);
		for (int i = 0; i < list.Count; i++)
		{
			RaycastHit hit = list[i];
			global::BaseEntity entity = hit.GetEntity();
			if (!(entity != null) || (!(entity == this) && !entity.EqualNetID(this)))
			{
				global::BaseCombatEntity baseCombatEntity = entity as global::BaseCombatEntity;
				if (baseCombatEntity != null)
				{
					global::HitInfo info = new global::HitInfo(this, entity, Rust.DamageType.Bullet, damageAmount, hit.point);
					entity.OnAttacked(info);
					if (entity is global::BasePlayer || entity is global::BaseNpc)
					{
						global::Effect.server.ImpactEffect(new global::HitInfo
						{
							HitPositionWorld = hit.point,
							HitNormalWorld = -hit.normal,
							HitMaterial = global::StringPool.Get("Flesh")
						});
					}
				}
				if (!(entity != null) || entity.ShouldBlockProjectiles())
				{
					arg = hit.point;
					break;
				}
			}
		}
	}

	// Token: 0x060007E2 RID: 2018 RVA: 0x000339CC File Offset: 0x00031BCC
	public override void ServerInit()
	{
		base.ServerInit();
		base.InvokeRandomized(new Action(this.TriggerCheck), Random.Range(0f, 1f), 0.5f, 0.1f);
	}

	// Token: 0x060007E3 RID: 2019 RVA: 0x00033A00 File Offset: 0x00031C00
	public void TriggerCheck()
	{
		bool flag = this.CheckTrigger(3f, this.sensorRadius) || this.CheckTrigger(1.25f, this.sensorRadius * 0.4f);
		if (flag)
		{
			this.FireWeapon();
		}
	}

	// Token: 0x060007E4 RID: 2020 RVA: 0x00033A4C File Offset: 0x00031C4C
	public bool CheckTrigger(float offset, float radius)
	{
		List<RaycastHit> list = Pool.GetList<RaycastHit>();
		List<global::BasePlayer> list2 = Pool.GetList<global::BasePlayer>();
		global::Vis.Entities<global::BasePlayer>(this.GetEyePosition() + base.transform.forward * offset, radius, list2, 131072, 2);
		bool flag = false;
		foreach (global::BasePlayer basePlayer in list2)
		{
			if (!basePlayer.IsSleeping() && basePlayer.IsAlive() && !basePlayer.IsBuildingAuthed())
			{
				object obj = Interface.CallHook("CanBeTargeted", new object[]
				{
					basePlayer,
					this
				});
				if (obj is bool)
				{
					Pool.FreeList<RaycastHit>(ref list);
					Pool.FreeList<global::BasePlayer>(ref list2);
					return (bool)obj;
				}
				list.Clear();
				global::GamePhysics.TraceAll(new Ray(basePlayer.eyes.position, (this.GetEyePosition() - basePlayer.eyes.position).normalized), 0f, list, 9f, 1075904769, 0);
				for (int i = 0; i < list.Count; i++)
				{
					RaycastHit hit = list[i];
					global::BaseEntity entity = hit.GetEntity();
					if (entity != null && (entity == this || entity.EqualNetID(this)))
					{
						flag = true;
						break;
					}
					if (!(entity != null) || entity.ShouldBlockProjectiles())
					{
						break;
					}
				}
				if (flag)
				{
					break;
				}
			}
		}
		Pool.FreeList<RaycastHit>(ref list);
		Pool.FreeList<global::BasePlayer>(ref list2);
		return flag;
	}

	// Token: 0x060007E5 RID: 2021 RVA: 0x00033C28 File Offset: 0x00031E28
	public bool IsTriggered()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x060007E6 RID: 2022 RVA: 0x00033C38 File Offset: 0x00031E38
	public Vector3 GetEyePosition()
	{
		return this.eyeTransform.position;
	}

	// Token: 0x040003AA RID: 938
	public global::GameObjectRef gun_fire_effect;

	// Token: 0x040003AB RID: 939
	public global::GameObjectRef bulletEffect;

	// Token: 0x040003AC RID: 940
	public global::GameObjectRef triggeredEffect;

	// Token: 0x040003AD RID: 941
	public Transform muzzlePos;

	// Token: 0x040003AE RID: 942
	public Transform eyeTransform;

	// Token: 0x040003AF RID: 943
	public int numPellets = 15;

	// Token: 0x040003B0 RID: 944
	public int aimCone = 30;

	// Token: 0x040003B1 RID: 945
	public float sensorRadius = 1.25f;

	// Token: 0x040003B2 RID: 946
	public global::ItemDefinition ammoType;

	// Token: 0x0200006C RID: 108
	public static class GunTrapFlags
	{
		// Token: 0x040003B3 RID: 947
		public const global::BaseEntity.Flags Triggered = global::BaseEntity.Flags.Reserved1;
	}
}
