﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x020006F1 RID: 1777
[ExecuteInEditMode]
public class PieMenu : UIBehaviour
{
	// Token: 0x060021BC RID: 8636 RVA: 0x000BD2AC File Offset: 0x000BB4AC
	protected override void Start()
	{
		base.Start();
		global::PieMenu.Instance = this;
		this.canvasGroup = base.GetComponentInChildren<CanvasGroup>();
		this.canvasGroup.alpha = 0f;
		this.canvasGroup.interactable = false;
		this.canvasGroup.blocksRaycasts = false;
		this.IsOpen = false;
		this.isClosing = true;
	}

	// Token: 0x060021BD RID: 8637 RVA: 0x000BD308 File Offset: 0x000BB508
	public void Clear()
	{
		this.options = new global::PieMenu.MenuOption[0];
	}

	// Token: 0x060021BE RID: 8638 RVA: 0x000BD318 File Offset: 0x000BB518
	public void AddOption(global::PieMenu.MenuOption option)
	{
		List<global::PieMenu.MenuOption> list = this.options.ToList<global::PieMenu.MenuOption>();
		list.Add(option);
		this.options = list.ToArray();
	}

	// Token: 0x060021BF RID: 8639 RVA: 0x000BD344 File Offset: 0x000BB544
	public void FinishAndOpen()
	{
		this.IsOpen = true;
		this.isClosing = false;
		this.SetDefaultOption();
		this.Rebuild();
		this.UpdateInteraction(false);
		this.PlayOpenSound();
		LeanTween.cancel(base.gameObject);
		LeanTween.cancel(this.scaleTarget);
		base.GetComponent<CanvasGroup>().alpha = 0f;
		LeanTween.alphaCanvas(base.GetComponent<CanvasGroup>(), 1f, 0.1f).setEase(21);
		this.scaleTarget.transform.localScale = Vector3.one * 1.5f;
		LeanTween.scale(this.scaleTarget, Vector3.one, 0.1f).setEase(24);
	}

	// Token: 0x060021C0 RID: 8640 RVA: 0x000BD3F8 File Offset: 0x000BB5F8
	protected override void OnEnable()
	{
		base.OnEnable();
		this.Rebuild();
	}

	// Token: 0x060021C1 RID: 8641 RVA: 0x000BD408 File Offset: 0x000BB608
	public void SetDefaultOption()
	{
		this.defaultOption = null;
		for (int i = 0; i < this.options.Length; i++)
		{
			if (!this.options[i].disabled)
			{
				if (this.defaultOption == null)
				{
					this.defaultOption = this.options[i];
				}
				if (this.options[i].selected)
				{
					this.defaultOption = this.options[i];
					return;
				}
			}
		}
	}

	// Token: 0x060021C2 RID: 8642 RVA: 0x000BD48C File Offset: 0x000BB68C
	public void PlayOpenSound()
	{
		global::UISound.Play(this.clipOpen, 1f);
	}

	// Token: 0x060021C3 RID: 8643 RVA: 0x000BD4A0 File Offset: 0x000BB6A0
	public void PlayCancelSound()
	{
		global::UISound.Play(this.clipCancel, 1f);
	}

	// Token: 0x060021C4 RID: 8644 RVA: 0x000BD4B4 File Offset: 0x000BB6B4
	public void Close(bool success = false)
	{
		if (this.isClosing)
		{
			return;
		}
		this.isClosing = true;
		global::NeedsCursor component = base.GetComponent<global::NeedsCursor>();
		if (component != null)
		{
			component.enabled = false;
		}
		LeanTween.cancel(base.gameObject);
		LeanTween.cancel(this.scaleTarget);
		LeanTween.alphaCanvas(base.GetComponent<CanvasGroup>(), 0f, 0.2f).setEase(21);
		LeanTween.scale(this.scaleTarget, Vector3.one * ((!success) ? 0.5f : 1.5f), 0.2f).setEase(21);
		this.IsOpen = false;
	}

	// Token: 0x060021C5 RID: 8645 RVA: 0x000BD560 File Offset: 0x000BB760
	private void Update()
	{
		if (!Application.isPlaying)
		{
			this.Rebuild();
		}
		if (this.pieBackground.innerSize != this.innerSize || this.pieBackground.outerSize != this.outerSize || this.pieBackground.startRadius != this.startRadius || this.pieBackground.endRadius != this.startRadius + this.radiusSize)
		{
			this.pieBackground.startRadius = this.startRadius;
			this.pieBackground.endRadius = this.startRadius + this.radiusSize;
			this.pieBackground.innerSize = this.innerSize;
			this.pieBackground.outerSize = this.outerSize;
			this.pieBackground.SetVerticesDirty();
		}
		this.UpdateInteraction(true);
		if (this.IsOpen)
		{
			global::CursorManager.HoldOpen(false);
			global::IngameMenuBackground.Enabled = true;
		}
	}

	// Token: 0x060021C6 RID: 8646 RVA: 0x000BD650 File Offset: 0x000BB850
	public void Rebuild()
	{
		this.options = (from x in this.options
		orderby x.order
		select x).ToArray<global::PieMenu.MenuOption>();
		while (this.optionsCanvas.transform.childCount > 0)
		{
			global::GameManager.DestroyImmediate(this.optionsCanvas.transform.GetChild(0).gameObject, true);
		}
		float num = this.radiusSize / (float)this.options.Length;
		for (int i = 0; i < this.options.Length; i++)
		{
			GameObject gameObject = Instantiate.GameObject(this.pieOptionPrefab, null);
			gameObject.transform.SetParent(this.optionsCanvas.transform, false);
			this.options[i].option = gameObject.GetComponent<global::PieOption>();
			this.options[i].option.UpdateOption(this.startRadius + (float)i * num - num * 0.25f, num, this.sliceGaps, this.options[i].name, this.outerSize, this.innerSize, this.iconSize, this.options[i].sprite);
		}
		this.selectedOption = null;
	}

	// Token: 0x060021C7 RID: 8647 RVA: 0x000BD788 File Offset: 0x000BB988
	public void UpdateInteraction(bool allowLerp = true)
	{
		if (this.isClosing)
		{
			return;
		}
		Vector3 vector = Input.mousePosition - new Vector3((float)Screen.width / 2f, (float)Screen.height / 2f, 0f);
		float num = Mathf.Atan2(vector.x, vector.y) * 57.29578f;
		if (num < 0f)
		{
			num += 360f;
		}
		float num2 = this.radiusSize / (float)this.options.Length;
		for (int i = 0; i < this.options.Length; i++)
		{
			float num3 = this.startRadius + (float)i * num2 + num2 * 0.5f - num2 * 0.25f;
			if ((vector.magnitude < 32f && this.options[i] == this.defaultOption) || (vector.magnitude >= 32f && Mathf.Abs(Mathf.DeltaAngle(num, num3)) < num2 * 0.5f))
			{
				if (allowLerp)
				{
					this.pieSelection.startRadius = Mathf.MoveTowardsAngle(this.pieSelection.startRadius, this.options[i].option.background.startRadius, Time.deltaTime * Mathf.Abs(Mathf.DeltaAngle(this.pieSelection.startRadius, this.options[i].option.background.startRadius) * 30f + 10f));
					this.pieSelection.endRadius = Mathf.MoveTowardsAngle(this.pieSelection.endRadius, this.options[i].option.background.endRadius, Time.deltaTime * Mathf.Abs(Mathf.DeltaAngle(this.pieSelection.endRadius, this.options[i].option.background.endRadius) * 30f + 10f));
				}
				else
				{
					this.pieSelection.startRadius = this.options[i].option.background.startRadius;
					this.pieSelection.endRadius = this.options[i].option.background.endRadius;
				}
				this.pieSelection.SetVerticesDirty();
				this.middleImage.sprite = this.options[i].sprite;
				this.middleTitle.text = this.options[i].name;
				this.middleDesc.text = this.options[i].desc;
				this.middleRequired.text = string.Empty;
				string text = this.options[i].requirements;
				if (text != null)
				{
					text = text.Replace("[e]", "<color=#CD412B>");
					text = text.Replace("[/e]", "</color>");
					this.middleRequired.text = text;
				}
				this.options[i].option.imageIcon.color = this.colorIconHovered;
				if (this.selectedOption != this.options[i])
				{
					if (this.selectedOption != null && !this.options[i].disabled)
					{
						global::UISound.Play(this.clipChanged, 1f);
						this.scaleTarget.transform.localScale = Vector3.one;
						LeanTween.scale(this.scaleTarget, Vector3.one * 1.03f, 0.2f).setEase(global::PieMenu.easePunch);
					}
					this.selectedOption = this.options[i];
				}
			}
			else
			{
				this.options[i].option.imageIcon.color = this.colorIconActive;
			}
			if (this.options[i].disabled)
			{
				this.options[i].option.imageIcon.color = this.colorIconDisabled;
				this.options[i].option.background.color = this.colorBackgroundDisabled;
			}
		}
	}

	// Token: 0x060021C8 RID: 8648 RVA: 0x000BDB84 File Offset: 0x000BBD84
	public bool DoSelect()
	{
		return true;
	}

	// Token: 0x04001E23 RID: 7715
	public static global::PieMenu Instance;

	// Token: 0x04001E24 RID: 7716
	public Image middleBox;

	// Token: 0x04001E25 RID: 7717
	public global::PieShape pieBackgroundBlur;

	// Token: 0x04001E26 RID: 7718
	public global::PieShape pieBackground;

	// Token: 0x04001E27 RID: 7719
	public global::PieShape pieSelection;

	// Token: 0x04001E28 RID: 7720
	public GameObject pieOptionPrefab;

	// Token: 0x04001E29 RID: 7721
	public GameObject optionsCanvas;

	// Token: 0x04001E2A RID: 7722
	public global::PieMenu.MenuOption[] options;

	// Token: 0x04001E2B RID: 7723
	public GameObject scaleTarget;

	// Token: 0x04001E2C RID: 7724
	public float sliceGaps = 10f;

	// Token: 0x04001E2D RID: 7725
	[Range(0f, 1f)]
	public float outerSize = 1f;

	// Token: 0x04001E2E RID: 7726
	[Range(0f, 1f)]
	public float innerSize = 0.5f;

	// Token: 0x04001E2F RID: 7727
	[Range(0f, 1f)]
	public float iconSize = 0.8f;

	// Token: 0x04001E30 RID: 7728
	[Range(0f, 360f)]
	public float startRadius;

	// Token: 0x04001E31 RID: 7729
	[Range(0f, 360f)]
	public float radiusSize = 360f;

	// Token: 0x04001E32 RID: 7730
	public Image middleImage;

	// Token: 0x04001E33 RID: 7731
	public Text middleTitle;

	// Token: 0x04001E34 RID: 7732
	public Text middleDesc;

	// Token: 0x04001E35 RID: 7733
	public Text middleRequired;

	// Token: 0x04001E36 RID: 7734
	public Color colorIconActive;

	// Token: 0x04001E37 RID: 7735
	public Color colorIconHovered;

	// Token: 0x04001E38 RID: 7736
	public Color colorIconDisabled;

	// Token: 0x04001E39 RID: 7737
	public Color colorBackgroundDisabled;

	// Token: 0x04001E3A RID: 7738
	public AudioClip clipOpen;

	// Token: 0x04001E3B RID: 7739
	public AudioClip clipCancel;

	// Token: 0x04001E3C RID: 7740
	public AudioClip clipChanged;

	// Token: 0x04001E3D RID: 7741
	public AudioClip clipSelected;

	// Token: 0x04001E3E RID: 7742
	public global::PieMenu.MenuOption defaultOption;

	// Token: 0x04001E3F RID: 7743
	private bool isClosing;

	// Token: 0x04001E40 RID: 7744
	private CanvasGroup canvasGroup;

	// Token: 0x04001E41 RID: 7745
	public bool IsOpen;

	// Token: 0x04001E42 RID: 7746
	internal global::PieMenu.MenuOption selectedOption;

	// Token: 0x04001E43 RID: 7747
	private static AnimationCurve easePunch = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 0f),
		new Keyframe(0.112586f, 0.9976035f),
		new Keyframe(0.3120486f, 0.01720615f),
		new Keyframe(0.4316337f, 0.170306817f),
		new Keyframe(0.5524869f, 0.03141804f),
		new Keyframe(0.6549395f, 0.002909959f),
		new Keyframe(0.770987f, 0.009817753f),
		new Keyframe(0.8838775f, 0.001939224f),
		new Keyframe(1f, 0f)
	});

	// Token: 0x020006F2 RID: 1778
	[Serializable]
	public class MenuOption
	{
		// Token: 0x04001E45 RID: 7749
		public string name;

		// Token: 0x04001E46 RID: 7750
		public string desc;

		// Token: 0x04001E47 RID: 7751
		public string requirements;

		// Token: 0x04001E48 RID: 7752
		public Sprite sprite;

		// Token: 0x04001E49 RID: 7753
		public bool disabled;

		// Token: 0x04001E4A RID: 7754
		public int order;

		// Token: 0x04001E4B RID: 7755
		[NonSerialized]
		public Action<global::BasePlayer> action;

		// Token: 0x04001E4C RID: 7756
		[NonSerialized]
		public global::PieOption option;

		// Token: 0x04001E4D RID: 7757
		[NonSerialized]
		public bool selected;
	}
}
