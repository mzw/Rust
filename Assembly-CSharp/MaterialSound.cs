﻿using System;
using UnityEngine;

// Token: 0x02000506 RID: 1286
[CreateAssetMenu(menuName = "Rust/MaterialSound")]
public class MaterialSound : ScriptableObject
{
	// Token: 0x0400160B RID: 5643
	public global::SoundDefinition DefaultSound;

	// Token: 0x0400160C RID: 5644
	public global::MaterialSound.Entry[] Entries;

	// Token: 0x02000507 RID: 1287
	[Serializable]
	public class Entry
	{
		// Token: 0x0400160D RID: 5645
		public PhysicMaterial Material;

		// Token: 0x0400160E RID: 5646
		public global::SoundDefinition Sound;
	}
}
