﻿using System;

// Token: 0x020006E1 RID: 1761
public class ServerBrowserList : global::BaseMonoBehaviour
{
	// Token: 0x04001DE6 RID: 7654
	public global::ServerBrowserCategory categoryButton;

	// Token: 0x04001DE7 RID: 7655
	public bool startActive;

	// Token: 0x04001DE8 RID: 7656
	public global::ServerBrowserItem itemTemplate;

	// Token: 0x04001DE9 RID: 7657
	public int refreshOrder;

	// Token: 0x04001DEA RID: 7658
	public bool UseOfficialServers;

	// Token: 0x04001DEB RID: 7659
	public global::ServerBrowserList.Rules[] rules;

	// Token: 0x04001DEC RID: 7660
	public global::ServerBrowserList.QueryType queryType;

	// Token: 0x04001DED RID: 7661
	public static string VersionTag = "v" + 2065;

	// Token: 0x04001DEE RID: 7662
	public global::ServerBrowserList.ServerKeyvalues[] keyValues = new global::ServerBrowserList.ServerKeyvalues[0];

	// Token: 0x020006E2 RID: 1762
	[Serializable]
	public struct Rules
	{
		// Token: 0x04001DEF RID: 7663
		public string tag;

		// Token: 0x04001DF0 RID: 7664
		public global::ServerBrowserList serverList;
	}

	// Token: 0x020006E3 RID: 1763
	public enum QueryType
	{
		// Token: 0x04001DF2 RID: 7666
		RegularInternet,
		// Token: 0x04001DF3 RID: 7667
		Friends,
		// Token: 0x04001DF4 RID: 7668
		History,
		// Token: 0x04001DF5 RID: 7669
		LAN,
		// Token: 0x04001DF6 RID: 7670
		Favourites,
		// Token: 0x04001DF7 RID: 7671
		None
	}

	// Token: 0x020006E4 RID: 1764
	[Serializable]
	public struct ServerKeyvalues
	{
		// Token: 0x04001DF8 RID: 7672
		public string key;

		// Token: 0x04001DF9 RID: 7673
		public string value;
	}
}
