﻿using System;
using UnityEngine;

// Token: 0x0200056E RID: 1390
public class TerrainHeightMap : global::TerrainMap2D<short>
{
	// Token: 0x06001D0F RID: 7439 RVA: 0x000A2788 File Offset: 0x000A0988
	public override void Setup()
	{
		if (this.HeightTexture != null)
		{
			if (this.HeightTexture.width == this.HeightTexture.height)
			{
				this.res = this.HeightTexture.width;
				this.src = (this.dst = new short[this.res, this.res]);
				Color32[] pixels = this.HeightTexture.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						Color32 c = pixels[num];
						this.dst[i, j] = global::TextureData.DecodeShort(c);
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid height texture: " + this.HeightTexture.name);
			}
		}
		else
		{
			this.res = this.terrain.terrainData.heightmapResolution;
			this.src = (this.dst = new short[this.res, this.res]);
		}
		this.normY = global::TerrainMeta.Size.x / global::TerrainMeta.Size.y / (float)this.res;
	}

	// Token: 0x06001D10 RID: 7440 RVA: 0x000A28E0 File Offset: 0x000A0AE0
	public void ApplyToTerrain()
	{
		float[,] heights = this.terrain.terrainData.GetHeights(0, 0, this.res, this.res);
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				heights[z, i] = this.GetHeight01(i, z);
			}
		});
		this.terrain.terrainData.SetHeights(0, 0, heights);
		TerrainCollider component = this.terrain.GetComponent<TerrainCollider>();
		if (component)
		{
			component.enabled = false;
			component.enabled = true;
		}
	}

	// Token: 0x06001D11 RID: 7441 RVA: 0x000A2974 File Offset: 0x000A0B74
	public void GenerateTextures(bool heightTexture = true, bool normalTexture = true)
	{
		if (heightTexture)
		{
			Color32[] heights = new Color32[this.res * this.res];
			Parallel.For(0, this.res, delegate(int z)
			{
				for (int i = 0; i < this.res; i++)
				{
					heights[z * this.res + i] = global::TextureData.EncodeShort(this.src[z, i]);
				}
			});
			this.HeightTexture = new Texture2D(this.res, this.res, 4, true, true);
			this.HeightTexture.name = "HeightTexture";
			this.HeightTexture.wrapMode = 1;
			this.HeightTexture.SetPixels32(heights);
		}
		if (normalTexture)
		{
			int normalres = this.res - 1;
			Color32[] normals = new Color32[normalres * normalres];
			Parallel.For(0, normalres, delegate(int z)
			{
				float normZ = ((float)z + 0.5f) / (float)normalres;
				for (int i = 0; i < normalres; i++)
				{
					float normX = ((float)i + 0.5f) / (float)normalres;
					Vector3 normal = this.GetNormal(normX, normZ);
					normals[z * normalres + i] = global::TextureData.EncodeNormal(normal);
				}
			});
			this.NormalTexture = new Texture2D(normalres, normalres, 4, true, true);
			this.NormalTexture.name = "NormalTexture";
			this.NormalTexture.wrapMode = 1;
			this.NormalTexture.SetPixels32(normals);
		}
	}

	// Token: 0x06001D12 RID: 7442 RVA: 0x000A2AA4 File Offset: 0x000A0CA4
	public void ApplyTextures()
	{
		this.HeightTexture.Apply(true, false);
		this.NormalTexture.Apply(true, false);
		this.NormalTexture.Compress(false);
		this.HeightTexture.Apply(false, true);
		this.NormalTexture.Apply(false, true);
	}

	// Token: 0x06001D13 RID: 7443 RVA: 0x000A2AF4 File Offset: 0x000A0CF4
	public float GetHeight(Vector3 worldPos)
	{
		return global::TerrainMeta.Position.y + this.GetHeight01(worldPos) * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001D14 RID: 7444 RVA: 0x000A2B24 File Offset: 0x000A0D24
	public float GetHeight(float normX, float normZ)
	{
		return global::TerrainMeta.Position.y + this.GetHeight01(normX, normZ) * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001D15 RID: 7445 RVA: 0x000A2B58 File Offset: 0x000A0D58
	public float GetHeight(int x, int z)
	{
		return global::TerrainMeta.Position.y + this.GetHeight01(x, z) * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001D16 RID: 7446 RVA: 0x000A2B8C File Offset: 0x000A0D8C
	public float GetHeight01(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetHeight01(normX, normZ);
	}

	// Token: 0x06001D17 RID: 7447 RVA: 0x000A2BBC File Offset: 0x000A0DBC
	public float GetHeight01(float normX, float normZ)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		float height = this.GetHeight01(num4, num5);
		float height2 = this.GetHeight01(x, num5);
		float height3 = this.GetHeight01(num4, z);
		float height4 = this.GetHeight01(x, z);
		float num6 = num2 - (float)num4;
		float num7 = num3 - (float)num5;
		float num8 = Mathf.Lerp(height, height2, num6);
		float num9 = Mathf.Lerp(height3, height4, num6);
		return Mathf.Lerp(num8, num9, num7);
	}

	// Token: 0x06001D18 RID: 7448 RVA: 0x000A2C6C File Offset: 0x000A0E6C
	public float GetHeight01(int x, int z)
	{
		return global::TextureData.Short2Float((int)this.src[z, x]);
	}

	// Token: 0x06001D19 RID: 7449 RVA: 0x000A2C80 File Offset: 0x000A0E80
	private float GetSrcHeight01(int x, int z)
	{
		return global::TextureData.Short2Float((int)this.src[z, x]);
	}

	// Token: 0x06001D1A RID: 7450 RVA: 0x000A2C94 File Offset: 0x000A0E94
	private float GetDstHeight01(int x, int z)
	{
		return global::TextureData.Short2Float((int)this.dst[z, x]);
	}

	// Token: 0x06001D1B RID: 7451 RVA: 0x000A2CA8 File Offset: 0x000A0EA8
	public Vector3 GetNormal(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetNormal(normX, normZ);
	}

	// Token: 0x06001D1C RID: 7452 RVA: 0x000A2CD8 File Offset: 0x000A0ED8
	public Vector3 GetNormal(float normX, float normZ)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		Vector3 normal = this.GetNormal(num4, num5);
		Vector3 normal2 = this.GetNormal(x, num5);
		Vector3 normal3 = this.GetNormal(num4, z);
		Vector3 normal4 = this.GetNormal(x, z);
		float num6 = num2 - (float)num4;
		float num7 = num3 - (float)num5;
		Vector3 vector = Vector3.Lerp(normal, normal2, num6);
		Vector3 vector2 = Vector3.Lerp(normal3, normal4, num6);
		return Vector3.Lerp(vector, vector2, num7).normalized;
	}

	// Token: 0x06001D1D RID: 7453 RVA: 0x000A2D90 File Offset: 0x000A0F90
	public Vector3 GetNormal(int x, int z)
	{
		int num = this.res - 1;
		int x2 = Mathf.Clamp(x - 1, 0, num);
		int z2 = Mathf.Clamp(z - 1, 0, num);
		int x3 = Mathf.Clamp(x + 1, 0, num);
		int z3 = Mathf.Clamp(z + 1, 0, num);
		float num2 = (this.GetHeight01(x3, z2) - this.GetHeight01(x2, z2)) * 0.5f;
		float num3 = (this.GetHeight01(x2, z3) - this.GetHeight01(x2, z2)) * 0.5f;
		Vector3 vector;
		vector..ctor(-num2, this.normY, -num3);
		return vector.normalized;
	}

	// Token: 0x06001D1E RID: 7454 RVA: 0x000A2E20 File Offset: 0x000A1020
	private Vector3 GetNormalSobel(int x, int z)
	{
		int num = this.res - 1;
		Vector3 vector;
		vector..ctor(global::TerrainMeta.Size.x / (float)num, global::TerrainMeta.Size.y, global::TerrainMeta.Size.z / (float)num);
		int x2 = Mathf.Clamp(x - 1, 0, num);
		int z2 = Mathf.Clamp(z - 1, 0, num);
		int x3 = Mathf.Clamp(x + 1, 0, num);
		int z3 = Mathf.Clamp(z + 1, 0, num);
		float num2 = this.GetHeight01(x2, z2) * -1f;
		num2 += this.GetHeight01(x2, z) * -2f;
		num2 += this.GetHeight01(x2, z3) * -1f;
		num2 += this.GetHeight01(x3, z2) * 1f;
		num2 += this.GetHeight01(x3, z) * 2f;
		num2 += this.GetHeight01(x3, z3) * 1f;
		num2 *= vector.y;
		num2 /= vector.x;
		float num3 = this.GetHeight01(x2, z2) * -1f;
		num3 += this.GetHeight01(x, z2) * -2f;
		num3 += this.GetHeight01(x3, z2) * -1f;
		num3 += this.GetHeight01(x2, z3) * 1f;
		num3 += this.GetHeight01(x, z3) * 2f;
		num3 += this.GetHeight01(x3, z3) * 1f;
		num3 *= vector.y;
		num3 /= vector.z;
		Vector3 vector2;
		vector2..ctor(-num2, 8f, -num3);
		return vector2.normalized;
	}

	// Token: 0x06001D1F RID: 7455 RVA: 0x000A2FE0 File Offset: 0x000A11E0
	public float GetSlope(Vector3 worldPos)
	{
		return Vector3.Angle(Vector3.up, this.GetNormal(worldPos));
	}

	// Token: 0x06001D20 RID: 7456 RVA: 0x000A2FF4 File Offset: 0x000A11F4
	public float GetSlope(float normX, float normZ)
	{
		return Vector3.Angle(Vector3.up, this.GetNormal(normX, normZ));
	}

	// Token: 0x06001D21 RID: 7457 RVA: 0x000A3008 File Offset: 0x000A1208
	public float GetSlope(int x, int z)
	{
		return Vector3.Angle(Vector3.up, this.GetNormal(x, z));
	}

	// Token: 0x06001D22 RID: 7458 RVA: 0x000A301C File Offset: 0x000A121C
	public float GetSlope01(Vector3 worldPos)
	{
		return this.GetSlope(worldPos) * 0.0111111114f;
	}

	// Token: 0x06001D23 RID: 7459 RVA: 0x000A302C File Offset: 0x000A122C
	public float GetSlope01(float normX, float normZ)
	{
		return this.GetSlope(normX, normZ) * 0.0111111114f;
	}

	// Token: 0x06001D24 RID: 7460 RVA: 0x000A303C File Offset: 0x000A123C
	public float GetSlope01(int x, int z)
	{
		return this.GetSlope(x, z) * 0.0111111114f;
	}

	// Token: 0x06001D25 RID: 7461 RVA: 0x000A304C File Offset: 0x000A124C
	public void SetHeight(Vector3 worldPos, float height)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetHeight(normX, normZ, height);
	}

	// Token: 0x06001D26 RID: 7462 RVA: 0x000A307C File Offset: 0x000A127C
	public void SetHeight(float normX, float normZ, float height)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetHeight(x, z, height);
	}

	// Token: 0x06001D27 RID: 7463 RVA: 0x000A30A4 File Offset: 0x000A12A4
	public void SetHeight(int x, int z, float height)
	{
		this.dst[z, x] = global::TextureData.Float2Short(height);
	}

	// Token: 0x06001D28 RID: 7464 RVA: 0x000A30BC File Offset: 0x000A12BC
	public void SetHeight(Vector3 worldPos, float height, float opacity)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetHeight(normX, normZ, height, opacity);
	}

	// Token: 0x06001D29 RID: 7465 RVA: 0x000A30F0 File Offset: 0x000A12F0
	public void SetHeight(float normX, float normZ, float height, float opacity)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetHeight(x, z, height, opacity);
	}

	// Token: 0x06001D2A RID: 7466 RVA: 0x000A3118 File Offset: 0x000A1318
	public void SetHeight(int x, int z, float height, float opacity)
	{
		float height2 = Mathf.SmoothStep(this.GetDstHeight01(x, z), height, opacity);
		this.SetHeight(x, z, height2);
	}

	// Token: 0x06001D2B RID: 7467 RVA: 0x000A3140 File Offset: 0x000A1340
	public void AddHeight(Vector3 worldPos, float delta)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.AddHeight(normX, normZ, delta);
	}

	// Token: 0x06001D2C RID: 7468 RVA: 0x000A3170 File Offset: 0x000A1370
	public void AddHeight(float normX, float normZ, float delta)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.AddHeight(x, z, delta);
	}

	// Token: 0x06001D2D RID: 7469 RVA: 0x000A3198 File Offset: 0x000A1398
	public void AddHeight(int x, int z, float delta)
	{
		float height = Mathf.Clamp01(this.GetDstHeight01(x, z) + delta);
		this.SetHeight(x, z, height);
	}

	// Token: 0x06001D2E RID: 7470 RVA: 0x000A31C0 File Offset: 0x000A13C0
	public void LowerHeight(Vector3 worldPos, float height, float opacity)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.LowerHeight(normX, normZ, height, opacity);
	}

	// Token: 0x06001D2F RID: 7471 RVA: 0x000A31F4 File Offset: 0x000A13F4
	public void LowerHeight(float normX, float normZ, float height, float opacity)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.LowerHeight(x, z, height, opacity);
	}

	// Token: 0x06001D30 RID: 7472 RVA: 0x000A321C File Offset: 0x000A141C
	public void LowerHeight(int x, int z, float height, float opacity)
	{
		float height2 = Mathf.Min(this.GetDstHeight01(x, z), Mathf.SmoothStep(this.GetSrcHeight01(x, z), height, opacity));
		this.SetHeight(x, z, height2);
	}

	// Token: 0x06001D31 RID: 7473 RVA: 0x000A3250 File Offset: 0x000A1450
	public void RaiseHeight(Vector3 worldPos, float height, float opacity)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.RaiseHeight(normX, normZ, height, opacity);
	}

	// Token: 0x06001D32 RID: 7474 RVA: 0x000A3284 File Offset: 0x000A1484
	public void RaiseHeight(float normX, float normZ, float height, float opacity)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.RaiseHeight(x, z, height, opacity);
	}

	// Token: 0x06001D33 RID: 7475 RVA: 0x000A32AC File Offset: 0x000A14AC
	public void RaiseHeight(int x, int z, float height, float opacity)
	{
		float height2 = Mathf.Max(this.GetDstHeight01(x, z), Mathf.SmoothStep(this.GetSrcHeight01(x, z), height, opacity));
		this.SetHeight(x, z, height2);
	}

	// Token: 0x06001D34 RID: 7476 RVA: 0x000A32E0 File Offset: 0x000A14E0
	public void SetHeight(Vector3 worldPos, float opacity, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		float height = global::TerrainMeta.NormalizeY(worldPos.y);
		this.SetHeight(normX, normZ, height, opacity, radius, fade);
	}

	// Token: 0x06001D35 RID: 7477 RVA: 0x000A3324 File Offset: 0x000A1524
	public void SetHeight(float normX, float normZ, float height, float opacity, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if (lerp > 0f)
			{
				this.SetHeight(x, z, height, lerp * opacity);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x06001D36 RID: 7478 RVA: 0x000A3368 File Offset: 0x000A1568
	public void LowerHeight(Vector3 worldPos, float opacity, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		float height = global::TerrainMeta.NormalizeY(worldPos.y);
		this.LowerHeight(normX, normZ, height, opacity, radius, fade);
	}

	// Token: 0x06001D37 RID: 7479 RVA: 0x000A33AC File Offset: 0x000A15AC
	public void LowerHeight(float normX, float normZ, float height, float opacity, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if (lerp > 0f)
			{
				this.LowerHeight(x, z, height, lerp * opacity);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x06001D38 RID: 7480 RVA: 0x000A33F0 File Offset: 0x000A15F0
	public void RaiseHeight(Vector3 worldPos, float opacity, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		float height = global::TerrainMeta.NormalizeY(worldPos.y);
		this.RaiseHeight(normX, normZ, height, opacity, radius, fade);
	}

	// Token: 0x06001D39 RID: 7481 RVA: 0x000A3434 File Offset: 0x000A1634
	public void RaiseHeight(float normX, float normZ, float height, float opacity, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if (lerp > 0f)
			{
				this.RaiseHeight(x, z, height, lerp * opacity);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x06001D3A RID: 7482 RVA: 0x000A3478 File Offset: 0x000A1678
	public void AddHeight(Vector3 worldPos, float delta, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.AddHeight(normX, normZ, delta, radius, fade);
	}

	// Token: 0x06001D3B RID: 7483 RVA: 0x000A34AC File Offset: 0x000A16AC
	public void AddHeight(float normX, float normZ, float delta, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if (lerp > 0f)
			{
				this.AddHeight(x, z, lerp * delta);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x0400180F RID: 6159
	public Texture2D HeightTexture;

	// Token: 0x04001810 RID: 6160
	public Texture2D NormalTexture;

	// Token: 0x04001811 RID: 6161
	private float normY;
}
