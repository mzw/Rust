﻿using System;
using UnityEngine;

// Token: 0x02000320 RID: 800
public class EmissionToggle : MonoBehaviour, IClientComponent
{
	// Token: 0x04000E4D RID: 3661
	private Color emissionColor;

	// Token: 0x04000E4E RID: 3662
	public Renderer[] targetRenderers;

	// Token: 0x04000E4F RID: 3663
	public int materialIndex = -1;

	// Token: 0x04000E50 RID: 3664
	private static MaterialPropertyBlock block;
}
