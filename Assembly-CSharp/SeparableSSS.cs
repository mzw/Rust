﻿using System;
using UnityEngine;

// Token: 0x020005FF RID: 1535
public class SeparableSSS
{
	// Token: 0x06001F42 RID: 8002 RVA: 0x000B1738 File Offset: 0x000AF938
	private static Vector3 Gaussian(float variance, float r, Color falloffColor)
	{
		Vector3 zero = Vector3.zero;
		for (int i = 0; i < 3; i++)
		{
			float num = r / (0.001f + falloffColor[i]);
			zero[i] = Mathf.Exp(-(num * num) / (2f * variance)) / (6.28f * variance);
		}
		return zero;
	}

	// Token: 0x06001F43 RID: 8003 RVA: 0x000B1790 File Offset: 0x000AF990
	private static Vector3 Profile(float r, Color falloffColor)
	{
		return 0.1f * global::SeparableSSS.Gaussian(0.0484f, r, falloffColor) + 0.118f * global::SeparableSSS.Gaussian(0.187f, r, falloffColor) + 0.113f * global::SeparableSSS.Gaussian(0.567f, r, falloffColor) + 0.358f * global::SeparableSSS.Gaussian(1.99f, r, falloffColor) + 0.078f * global::SeparableSSS.Gaussian(7.41f, r, falloffColor);
	}

	// Token: 0x06001F44 RID: 8004 RVA: 0x000B1820 File Offset: 0x000AFA20
	public static void CalculateKernel(Color[] target, int targetStart, int targetSize, Color subsurfaceColor, Color falloffColor)
	{
		int num = targetSize * 2 - 1;
		float num2 = (num <= 20) ? 2f : 3f;
		float num3 = 2f;
		Color[] array = new Color[num];
		float num4 = 2f * num2 / (float)(num - 1);
		for (int i = 0; i < num; i++)
		{
			float num5 = -num2 + (float)i * num4;
			float num6 = (num5 >= 0f) ? 1f : -1f;
			array[i].a = num2 * num6 * Mathf.Abs(Mathf.Pow(num5, num3)) / Mathf.Pow(num2, num3);
		}
		for (int j = 0; j < num; j++)
		{
			float num7 = (j <= 0) ? 0f : Mathf.Abs(array[j].a - array[j - 1].a);
			float num8 = (j >= num - 1) ? 0f : Mathf.Abs(array[j].a - array[j + 1].a);
			float num9 = (num7 + num8) / 2f;
			Vector3 vector = num9 * global::SeparableSSS.Profile(array[j].a, falloffColor);
			array[j].r = vector.x;
			array[j].g = vector.y;
			array[j].b = vector.z;
		}
		Color color = array[num / 2];
		for (int k = num / 2; k > 0; k--)
		{
			array[k] = array[k - 1];
		}
		array[0] = color;
		Vector3 zero = Vector3.zero;
		for (int l = 0; l < num; l++)
		{
			zero.x += array[l].r;
			zero.y += array[l].g;
			zero.z += array[l].b;
		}
		for (int m = 0; m < num; m++)
		{
			Color[] array2 = array;
			int num10 = m;
			array2[num10].r = array2[num10].r / zero.x;
			Color[] array3 = array;
			int num11 = m;
			array3[num11].g = array3[num11].g / zero.y;
			Color[] array4 = array;
			int num12 = m;
			array4[num12].b = array4[num12].b / zero.z;
		}
		target[targetStart] = array[0];
		uint num13 = 0u;
		while ((ulong)num13 < (ulong)((long)(targetSize - 1)))
		{
			checked
			{
				target[(int)((IntPtr)(unchecked((long)targetStart + (long)((ulong)num13) + 1L)))] = array[(int)((IntPtr)(unchecked((long)targetSize + (long)((ulong)num13))))];
			}
			num13 += 1u;
		}
	}
}
