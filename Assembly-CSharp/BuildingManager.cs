﻿using System;

// Token: 0x020003F9 RID: 1017
public class BuildingManager
{
	// Token: 0x06001793 RID: 6035 RVA: 0x000870E8 File Offset: 0x000852E8
	public global::BuildingManager.Building GetBuilding(uint buildingID)
	{
		global::BuildingManager.Building result = null;
		this.buildingDictionary.TryGetValue(buildingID, ref result);
		return result;
	}

	// Token: 0x06001794 RID: 6036 RVA: 0x00087108 File Offset: 0x00085308
	public void Add(global::DecayEntity ent)
	{
		if (ent.buildingID == 0u)
		{
			if (!this.decayEntities.Contains(ent))
			{
				this.decayEntities.Add(ent);
			}
			return;
		}
		global::BuildingManager.Building building = this.GetBuilding(ent.buildingID);
		if (building == null)
		{
			this.buildingDictionary.Add(ent.buildingID, building = new global::BuildingManager.Building(ent.buildingID));
		}
		building.Add(ent);
		building.Dirty();
	}

	// Token: 0x06001795 RID: 6037 RVA: 0x0008717C File Offset: 0x0008537C
	public void Remove(global::DecayEntity ent)
	{
		if (ent.buildingID == 0u)
		{
			this.decayEntities.Remove(ent);
			return;
		}
		global::BuildingManager.Building building = this.GetBuilding(ent.buildingID);
		if (building == null)
		{
			return;
		}
		building.Remove(ent);
		if (building.IsEmpty())
		{
			this.buildingDictionary.Remove(ent.buildingID);
		}
		else
		{
			building.Dirty();
		}
	}

	// Token: 0x06001796 RID: 6038 RVA: 0x000871E8 File Offset: 0x000853E8
	public void Clear()
	{
		this.buildingDictionary.Clear();
	}

	// Token: 0x0400121D RID: 4637
	public static global::ServerBuildingManager server = new global::ServerBuildingManager();

	// Token: 0x0400121E RID: 4638
	protected ListHashSet<global::DecayEntity> decayEntities = new ListHashSet<global::DecayEntity>(8);

	// Token: 0x0400121F RID: 4639
	protected ListDictionary<uint, global::BuildingManager.Building> buildingDictionary = new ListDictionary<uint, global::BuildingManager.Building>(8);

	// Token: 0x020003FA RID: 1018
	public class Building
	{
		// Token: 0x06001798 RID: 6040 RVA: 0x00087204 File Offset: 0x00085404
		public Building(uint id)
		{
			this.ID = id;
		}

		// Token: 0x06001799 RID: 6041 RVA: 0x00087214 File Offset: 0x00085414
		public bool IsEmpty()
		{
			return !this.HasBuildingPrivileges() && !this.HasBuildingBlocks() && !this.HasDecayEntities();
		}

		// Token: 0x0600179A RID: 6042 RVA: 0x00087240 File Offset: 0x00085440
		public global::BuildingPrivlidge GetDominatingBuildingPrivilege()
		{
			global::BuildingPrivlidge buildingPrivlidge = null;
			if (this.HasBuildingPrivileges())
			{
				for (int i = 0; i < this.buildingPrivileges.Count; i++)
				{
					global::BuildingPrivlidge buildingPrivlidge2 = this.buildingPrivileges[i];
					if (!(buildingPrivlidge2 == null))
					{
						if (buildingPrivlidge2.IsOlderThan(buildingPrivlidge))
						{
							buildingPrivlidge = buildingPrivlidge2;
						}
					}
				}
			}
			return buildingPrivlidge;
		}

		// Token: 0x0600179B RID: 6043 RVA: 0x000872A8 File Offset: 0x000854A8
		public bool HasBuildingPrivileges()
		{
			return this.buildingPrivileges != null && this.buildingPrivileges.Count > 0;
		}

		// Token: 0x0600179C RID: 6044 RVA: 0x000872C8 File Offset: 0x000854C8
		public bool HasBuildingBlocks()
		{
			return this.buildingBlocks != null && this.buildingBlocks.Count > 0;
		}

		// Token: 0x0600179D RID: 6045 RVA: 0x000872E8 File Offset: 0x000854E8
		public bool HasDecayEntities()
		{
			return this.decayEntities != null && this.decayEntities.Count > 0;
		}

		// Token: 0x0600179E RID: 6046 RVA: 0x00087308 File Offset: 0x00085508
		public void AddBuildingPrivilege(global::BuildingPrivlidge ent)
		{
			if (ent == null)
			{
				return;
			}
			if (this.buildingPrivileges == null)
			{
				this.buildingPrivileges = new ListHashSet<global::BuildingPrivlidge>(8);
			}
			if (!this.buildingPrivileges.Contains(ent))
			{
				this.buildingPrivileges.Add(ent);
			}
		}

		// Token: 0x0600179F RID: 6047 RVA: 0x00087358 File Offset: 0x00085558
		public void RemoveBuildingPrivilege(global::BuildingPrivlidge ent)
		{
			if (ent == null)
			{
				return;
			}
			this.buildingPrivileges.Remove(ent);
			if (this.buildingPrivileges.Count == 0)
			{
				this.buildingPrivileges = null;
			}
		}

		// Token: 0x060017A0 RID: 6048 RVA: 0x0008738C File Offset: 0x0008558C
		public void AddBuildingBlock(global::BuildingBlock ent)
		{
			if (ent == null)
			{
				return;
			}
			if (this.buildingBlocks == null)
			{
				this.buildingBlocks = new ListHashSet<global::BuildingBlock>(8);
			}
			if (!this.buildingBlocks.Contains(ent))
			{
				this.buildingBlocks.Add(ent);
			}
		}

		// Token: 0x060017A1 RID: 6049 RVA: 0x000873DC File Offset: 0x000855DC
		public void RemoveBuildingBlock(global::BuildingBlock ent)
		{
			if (ent == null)
			{
				return;
			}
			this.buildingBlocks.Remove(ent);
			if (this.buildingBlocks.Count == 0)
			{
				this.buildingBlocks = null;
			}
		}

		// Token: 0x060017A2 RID: 6050 RVA: 0x00087410 File Offset: 0x00085610
		public void AddDecayEntity(global::DecayEntity ent)
		{
			if (ent == null)
			{
				return;
			}
			if (this.decayEntities == null)
			{
				this.decayEntities = new ListHashSet<global::DecayEntity>(8);
			}
			if (!this.decayEntities.Contains(ent))
			{
				this.decayEntities.Add(ent);
			}
		}

		// Token: 0x060017A3 RID: 6051 RVA: 0x00087460 File Offset: 0x00085660
		public void RemoveDecayEntity(global::DecayEntity ent)
		{
			if (ent == null)
			{
				return;
			}
			this.decayEntities.Remove(ent);
			if (this.decayEntities.Count == 0)
			{
				this.decayEntities = null;
			}
		}

		// Token: 0x060017A4 RID: 6052 RVA: 0x00087494 File Offset: 0x00085694
		public void Add(global::DecayEntity ent)
		{
			this.AddDecayEntity(ent);
			this.AddBuildingBlock(ent as global::BuildingBlock);
			this.AddBuildingPrivilege(ent as global::BuildingPrivlidge);
		}

		// Token: 0x060017A5 RID: 6053 RVA: 0x000874B8 File Offset: 0x000856B8
		public void Remove(global::DecayEntity ent)
		{
			this.RemoveDecayEntity(ent);
			this.RemoveBuildingBlock(ent as global::BuildingBlock);
			this.RemoveBuildingPrivilege(ent as global::BuildingPrivlidge);
		}

		// Token: 0x060017A6 RID: 6054 RVA: 0x000874DC File Offset: 0x000856DC
		public void Dirty()
		{
			global::BuildingPrivlidge dominatingBuildingPrivilege = this.GetDominatingBuildingPrivilege();
			if (dominatingBuildingPrivilege != null)
			{
				dominatingBuildingPrivilege.BuildingDirty();
			}
		}

		// Token: 0x04001220 RID: 4640
		public uint ID;

		// Token: 0x04001221 RID: 4641
		public ListHashSet<global::BuildingPrivlidge> buildingPrivileges;

		// Token: 0x04001222 RID: 4642
		public ListHashSet<global::BuildingBlock> buildingBlocks;

		// Token: 0x04001223 RID: 4643
		public ListHashSet<global::DecayEntity> decayEntities;
	}
}
