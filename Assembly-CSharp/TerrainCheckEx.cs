﻿using System;
using UnityEngine;

// Token: 0x0200055B RID: 1371
public static class TerrainCheckEx
{
	// Token: 0x06001CB2 RID: 7346 RVA: 0x000A0C70 File Offset: 0x0009EE70
	public static bool ApplyTerrainChecks(this Transform transform, global::TerrainCheck[] anchors, Vector3 pos, Quaternion rot, Vector3 scale, global::SpawnFilter filter = null)
	{
		if (anchors.Length <= 0)
		{
			return true;
		}
		foreach (global::TerrainCheck terrainCheck in anchors)
		{
			Vector3 vector = Vector3.Scale(terrainCheck.worldPosition, scale);
			Vector3 vector2 = pos + rot * vector;
			if (global::TerrainMeta.OutOfBounds(vector2))
			{
				return false;
			}
			if (filter != null && filter.GetFactor(vector2) == 0f)
			{
				return false;
			}
			if (!terrainCheck.Check(vector2))
			{
				return false;
			}
		}
		return true;
	}
}
