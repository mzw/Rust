﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005AA RID: 1450
public class GenerateRiverLayout : global::ProceduralComponent
{
	// Token: 0x06001E52 RID: 7762 RVA: 0x000A91AC File Offset: 0x000A73AC
	public override void Process(uint seed)
	{
		List<global::PathList> list = new List<global::PathList>();
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		List<Vector3> list2 = new List<Vector3>();
		for (float num = global::TerrainMeta.Position.z; num < global::TerrainMeta.Position.z + global::TerrainMeta.Size.z; num += 50f)
		{
			for (float num2 = global::TerrainMeta.Position.x; num2 < global::TerrainMeta.Position.x + global::TerrainMeta.Size.x; num2 += 50f)
			{
				Vector3 vector;
				vector..ctor(num2, 0f, num);
				float num3 = vector.y = heightMap.GetHeight(vector);
				if (vector.y > 5f)
				{
					Vector3 normal = heightMap.GetNormal(vector);
					if (normal.y > 0.01f)
					{
						Vector2 vector2;
						vector2..ctor(normal.x, normal.z);
						Vector2 normalized = vector2.normalized;
						list2.Add(vector);
						float radius = 12f;
						int num4 = 12;
						for (int i = 0; i < 10000; i++)
						{
							vector.x += normalized.x;
							vector.z += normalized.y;
							float slope = heightMap.GetSlope(vector);
							if (slope > 30f)
							{
								break;
							}
							float height = heightMap.GetHeight(vector);
							if (height > num3 + 10f)
							{
								break;
							}
							vector.y = Mathf.Min(height, num3);
							list2.Add(vector);
							int topology = topologyMap.GetTopology(vector, radius);
							int topology2 = topologyMap.GetTopology(vector);
							int num5 = 2694148;
							int num6 = 128;
							if ((topology & num5) != 0)
							{
								break;
							}
							if ((topology2 & num6) != 0 && --num4 <= 0)
							{
								if (list2.Count >= 300)
								{
									list.Add(new global::PathList("River " + list.Count, list2.ToArray())
									{
										Width = 24f,
										InnerPadding = 0.5f,
										OuterPadding = 0.5f,
										InnerFade = 8f,
										OuterFade = 16f,
										RandomScale = 0.75f,
										MeshOffset = -0.4f,
										TerrainOffset = -2f,
										Topology = 16384,
										Splat = 64,
										Start = true,
										End = true
									});
								}
								break;
							}
							normal = heightMap.GetNormal(vector);
							Vector2 vector3;
							vector3..ctor(normalized.x + 0.15f * normal.x, normalized.y + 0.15f * normal.z);
							normalized = vector3.normalized;
							num3 = vector.y;
						}
						list2.Clear();
					}
				}
			}
		}
		list.Sort((global::PathList a, global::PathList b) => b.Path.Points.Length.CompareTo(a.Path.Points.Length));
		int num7 = Mathf.RoundToInt(10f * global::TerrainMeta.Size.x * global::TerrainMeta.Size.z * 1E-06f);
		int num8 = Mathf.NextPowerOfTwo((int)(global::World.Size / 24f));
		bool[,] array = new bool[num8, num8];
		for (int j = 0; j < list.Count; j++)
		{
			if (j >= num7)
			{
				ListEx.RemoveUnordered<global::PathList>(list, j--);
			}
			else
			{
				global::PathList pathList = list[j];
				for (int k = 0; k < j; k++)
				{
					global::PathList pathList2 = list[k];
					if (Vector3.Distance(pathList2.Path.GetStartPoint(), pathList.Path.GetStartPoint()) < 100f)
					{
						ListEx.RemoveUnordered<global::PathList>(list, j--);
					}
				}
				int num9 = -1;
				int num10 = -1;
				for (int l = 0; l < pathList.Path.Points.Length; l++)
				{
					Vector3 vector4 = pathList.Path.Points[l];
					int num11 = Mathf.Clamp((int)(global::TerrainMeta.NormalizeX(vector4.x) * (float)num8), 0, num8 - 1);
					int num12 = Mathf.Clamp((int)(global::TerrainMeta.NormalizeZ(vector4.z) * (float)num8), 0, num8 - 1);
					if (num9 != num11 || num10 != num12)
					{
						if (array[num12, num11])
						{
							ListEx.RemoveUnordered<global::PathList>(list, j--);
							break;
						}
						num9 = num11;
						num10 = num12;
						array[num12, num11] = true;
					}
				}
			}
		}
		global::TerrainMeta.Path.Rivers.AddRange(list);
	}

	// Token: 0x0400191A RID: 6426
	public const float Width = 24f;

	// Token: 0x0400191B RID: 6427
	public const float InnerPadding = 0.5f;

	// Token: 0x0400191C RID: 6428
	public const float OuterPadding = 0.5f;

	// Token: 0x0400191D RID: 6429
	public const float InnerFade = 8f;

	// Token: 0x0400191E RID: 6430
	public const float OuterFade = 16f;

	// Token: 0x0400191F RID: 6431
	public const float RandomScale = 0.75f;

	// Token: 0x04001920 RID: 6432
	public const float MeshOffset = -0.4f;

	// Token: 0x04001921 RID: 6433
	public const float TerrainOffset = -2f;
}
