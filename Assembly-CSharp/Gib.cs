﻿using System;
using UnityEngine;

// Token: 0x02000266 RID: 614
public class Gib : MonoBehaviour
{
	// Token: 0x0600107E RID: 4222 RVA: 0x00063CF4 File Offset: 0x00061EF4
	public static string GetEffect(PhysicMaterial physicMaterial)
	{
		string nameLower = physicMaterial.GetNameLower();
		if (nameLower != null)
		{
			if (nameLower == "wood")
			{
				return "assets/bundled/prefabs/fx/building/wood_gib.prefab";
			}
			if (nameLower == "concrete")
			{
				return "assets/bundled/prefabs/fx/building/stone_gib.prefab";
			}
			if (nameLower == "metal")
			{
				return "assets/bundled/prefabs/fx/building/metal_sheet_gib.prefab";
			}
			if (nameLower == "rock")
			{
				return "assets/bundled/prefabs/fx/building/stone_gib.prefab";
			}
			if (nameLower == "flesh")
			{
				return "assets/bundled/prefabs/fx/building/wood_gib.prefab";
			}
		}
		return "assets/bundled/prefabs/fx/building/wood_gib.prefab";
	}

	// Token: 0x04000B47 RID: 2887
	public static int gibCount;
}
