﻿using System;
using System.Collections.Generic;
using System.IO;

// Token: 0x020007A2 RID: 1954
public static class RawWriter
{
	// Token: 0x0600246B RID: 9323 RVA: 0x000C8BB8 File Offset: 0x000C6DB8
	public static void Write(IEnumerable<byte> data, string path)
	{
		using (FileStream fileStream = File.Open(path, FileMode.Create))
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
			{
				foreach (byte value in data)
				{
					binaryWriter.Write(value);
				}
			}
		}
	}

	// Token: 0x0600246C RID: 9324 RVA: 0x000C8C58 File Offset: 0x000C6E58
	public static void Write(IEnumerable<int> data, string path)
	{
		using (FileStream fileStream = File.Open(path, FileMode.Create))
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
			{
				foreach (int value in data)
				{
					binaryWriter.Write(value);
				}
			}
		}
	}

	// Token: 0x0600246D RID: 9325 RVA: 0x000C8CF8 File Offset: 0x000C6EF8
	public static void Write(IEnumerable<short> data, string path)
	{
		using (FileStream fileStream = File.Open(path, FileMode.Create))
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
			{
				foreach (short value in data)
				{
					binaryWriter.Write(value);
				}
			}
		}
	}

	// Token: 0x0600246E RID: 9326 RVA: 0x000C8D98 File Offset: 0x000C6F98
	public static void Write(IEnumerable<float> data, string path)
	{
		using (FileStream fileStream = File.Open(path, FileMode.Create))
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
			{
				foreach (float value in data)
				{
					binaryWriter.Write(value);
				}
			}
		}
	}
}
