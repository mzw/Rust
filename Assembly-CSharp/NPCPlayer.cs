﻿using System;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x020000E6 RID: 230
public class NPCPlayer : global::BasePlayer
{
	// Token: 0x1700007A RID: 122
	// (get) Token: 0x06000B46 RID: 2886 RVA: 0x0004AF54 File Offset: 0x00049154
	// (set) Token: 0x06000B47 RID: 2887 RVA: 0x0004AF5C File Offset: 0x0004915C
	public Vector2i CurrentCoord { get; set; }

	// Token: 0x1700007B RID: 123
	// (get) Token: 0x06000B48 RID: 2888 RVA: 0x0004AF68 File Offset: 0x00049168
	// (set) Token: 0x06000B49 RID: 2889 RVA: 0x0004AF70 File Offset: 0x00049170
	public Vector2i PreviousCoord { get; set; }

	// Token: 0x1700007C RID: 124
	// (get) Token: 0x06000B4A RID: 2890 RVA: 0x0004AF7C File Offset: 0x0004917C
	// (set) Token: 0x06000B4B RID: 2891 RVA: 0x0004AF84 File Offset: 0x00049184
	public bool AgencyUpdateRequired { get; set; }

	// Token: 0x1700007D RID: 125
	// (get) Token: 0x06000B4C RID: 2892 RVA: 0x0004AF90 File Offset: 0x00049190
	// (set) Token: 0x06000B4D RID: 2893 RVA: 0x0004AF98 File Offset: 0x00049198
	public bool IsOnOffmeshLinkAndReachedNewCoord { get; set; }

	// Token: 0x1700007E RID: 126
	// (get) Token: 0x06000B4E RID: 2894 RVA: 0x0004AFA4 File Offset: 0x000491A4
	protected override float PositionTickRate
	{
		get
		{
			return 0.05f;
		}
	}

	// Token: 0x06000B4F RID: 2895 RVA: 0x0004AFAC File Offset: 0x000491AC
	public override void ServerInit()
	{
		if (base.isClient)
		{
			return;
		}
		this.spawnPos = this.GetPosition();
		this.randomOffset = Random.Range(0f, 1f);
		base.ServerInit();
		this.UpdateNetworkGroup();
		this.loadouts[Random.Range(0, this.loadouts.Length)].GiveToPlayer(this);
		base.InvokeRepeating(new Action(this.ServerThink_Internal), 0f, 0.1f);
		this.lastThinkTime = Time.time;
		base.Invoke(new Action(this.EquipTest), 1f);
		this.finalDestination = base.transform.position;
		this.AgencyUpdateRequired = false;
		this.IsOnOffmeshLinkAndReachedNewCoord = false;
		if (this.NavAgent == null)
		{
			this.NavAgent = base.GetComponent<NavMeshAgent>();
		}
		if (this.NavAgent)
		{
			this.NavAgent.updateRotation = false;
			this.NavAgent.updatePosition = false;
		}
	}

	// Token: 0x06000B50 RID: 2896 RVA: 0x0004B0B0 File Offset: 0x000492B0
	public void RandomMove()
	{
		float num = 8f;
		Vector2 vector = Random.insideUnitCircle * num;
		this.SetDestination(this.spawnPos + new Vector3(vector.x, 0f, vector.y));
	}

	// Token: 0x06000B51 RID: 2897 RVA: 0x0004B0F8 File Offset: 0x000492F8
	public virtual void SetDestination(Vector3 newDestination)
	{
		this.finalDestination = newDestination;
	}

	// Token: 0x06000B52 RID: 2898 RVA: 0x0004B104 File Offset: 0x00049304
	public global::AttackEntity GetAttackEntity()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		return heldEntity as global::AttackEntity;
	}

	// Token: 0x06000B53 RID: 2899 RVA: 0x0004B120 File Offset: 0x00049320
	public void ShotTest()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
		if (attackEntity == null)
		{
			return;
		}
		global::BaseProjectile baseProjectile = attackEntity as global::BaseProjectile;
		if (baseProjectile && baseProjectile.primaryMagazine.contents <= 0)
		{
			baseProjectile.ServerReload();
			global::NPCPlayerApex npcplayerApex = this as global::NPCPlayerApex;
			if (npcplayerApex && npcplayerApex.OnReload != null)
			{
				npcplayerApex.OnReload();
			}
		}
		if (Mathf.Approximately(attackEntity.attackLengthMin, -1f))
		{
			attackEntity.ServerUse();
			return;
		}
		if (base.IsInvoking(new Action(this.TriggerDown)))
		{
			return;
		}
		if (Time.time < this.nextTriggerTime)
		{
			return;
		}
		base.InvokeRepeating(new Action(this.TriggerDown), 0f, 0.01f);
		this.triggerEndTime = Time.time + Random.Range(attackEntity.attackLengthMin, attackEntity.attackLengthMax);
		this.TriggerDown();
	}

	// Token: 0x06000B54 RID: 2900 RVA: 0x0004B220 File Offset: 0x00049420
	public bool MeleeAttack()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
		if (attackEntity == null)
		{
			return false;
		}
		global::BaseMelee baseMelee = attackEntity as global::BaseMelee;
		if (baseMelee == null)
		{
			return false;
		}
		baseMelee.ServerUse();
		return true;
	}

	// Token: 0x06000B55 RID: 2901 RVA: 0x0004B268 File Offset: 0x00049468
	public void TriggerDown()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
		if (attackEntity != null)
		{
			attackEntity.ServerUse();
		}
		if (Time.time > this.triggerEndTime)
		{
			base.CancelInvoke(new Action(this.TriggerDown));
			this.nextTriggerTime = Time.time + ((!(attackEntity != null)) ? 1f : attackEntity.attackSpacing);
		}
	}

	// Token: 0x06000B56 RID: 2902 RVA: 0x0004B2E0 File Offset: 0x000494E0
	public void EquipTest()
	{
		base.UpdateActiveItem(this.inventory.containerBelt.GetSlot(0).uid);
	}

	// Token: 0x06000B57 RID: 2903 RVA: 0x0004B300 File Offset: 0x00049500
	public void ServerThink_Internal()
	{
		float delta = Time.time - this.lastThinkTime;
		this.ServerThink(delta);
		this.lastThinkTime = Time.time;
	}

	// Token: 0x06000B58 RID: 2904 RVA: 0x0004B32C File Offset: 0x0004952C
	public virtual void ServerThink(float delta)
	{
		this.TickAi(delta);
	}

	// Token: 0x06000B59 RID: 2905 RVA: 0x0004B338 File Offset: 0x00049538
	public virtual void Resume()
	{
	}

	// Token: 0x06000B5A RID: 2906 RVA: 0x0004B33C File Offset: 0x0004953C
	public virtual bool IsNavRunning()
	{
		return false;
	}

	// Token: 0x06000B5B RID: 2907 RVA: 0x0004B340 File Offset: 0x00049540
	public virtual void TickAi(float delta)
	{
		this.MovementUpdate(delta);
	}

	// Token: 0x06000B5C RID: 2908 RVA: 0x0004B34C File Offset: 0x0004954C
	public virtual void MovementUpdate(float delta)
	{
		if (base.isClient)
		{
			return;
		}
		if (!this.IsAlive() || base.IsWounded() || !this.IsNavRunning())
		{
			return;
		}
		Vector3 moveToPosition = base.transform.position;
		if (this.NavAgent.isOnOffMeshLink)
		{
			this.TickNavMeshLinkTraversal(delta, ref moveToPosition);
		}
		else if (this.NavAgent.hasPath)
		{
			moveToPosition = this.NavAgent.nextPosition;
		}
		if (!this.ValidateNextPosition(ref moveToPosition))
		{
			return;
		}
		this.UpdateSpeed(delta);
		this.UpdatePositionAndRotation(moveToPosition);
	}

	// Token: 0x06000B5D RID: 2909 RVA: 0x0004B3EC File Offset: 0x000495EC
	private void TickNavMeshLinkTraversal(float delta, ref Vector3 moveToPosition)
	{
		OffMeshLinkData currentOffMeshLinkData = this.NavAgent.currentOffMeshLinkData;
		if (!currentOffMeshLinkData.valid)
		{
			this.CompleteNavMeshLinkTraversal(true, ref moveToPosition);
		}
		else
		{
			Vector3 vector = currentOffMeshLinkData.endPos + Vector3.up * this.NavAgent.baseOffset;
			NavMeshHit navMeshHit;
			if (NavMesh.SamplePosition(vector, ref navMeshHit, this.NavAgent.height * 2f, this.NavAgent.areaMask))
			{
				vector = navMeshHit.position;
				if (this.IsNavMeshLinkTraversalComplete(moveToPosition, vector))
				{
					this.CompleteNavMeshLinkTraversal(false, ref moveToPosition);
				}
				else
				{
					this.TraverseLink(delta, ref moveToPosition, vector);
				}
			}
			else
			{
				this.CompleteNavMeshLinkTraversal(true, ref moveToPosition);
			}
		}
	}

	// Token: 0x06000B5E RID: 2910 RVA: 0x0004B4A8 File Offset: 0x000496A8
	private bool IsNavMeshLinkTraversalComplete(Vector3 moveToPosition, Vector3 targetPosition)
	{
		return (Rust.Ai.AiManager.nav_grid && this.IsOnOffmeshLinkAndReachedNewCoord) || (!Rust.Ai.AiManager.nav_grid && Vector3Ex.Distance2D(moveToPosition, targetPosition) < 0.15f);
	}

	// Token: 0x06000B5F RID: 2911 RVA: 0x0004B4E0 File Offset: 0x000496E0
	private void TraverseLink(float delta, ref Vector3 moveToPosition, Vector3 targetPosition)
	{
		float num = Mathf.Clamp01(this.NavAgent.speed * delta);
		moveToPosition = Vector3.MoveTowards(moveToPosition, targetPosition, num);
		RaycastHit raycastHit;
		if (global::TransformUtil.GetGroundInfo(moveToPosition + new Vector3(0f, 1.25f, 0f), out raycastHit, this.NavAgent.height, this.movementMask, base.transform))
		{
			moveToPosition = raycastHit.point;
		}
	}

	// Token: 0x06000B60 RID: 2912 RVA: 0x0004B564 File Offset: 0x00049764
	private void CompleteNavMeshLinkTraversal(bool failed, ref Vector3 moveToPosition)
	{
		this.NavAgent.CompleteOffMeshLink();
		this.IsOnOffmeshLinkAndReachedNewCoord = false;
		if (failed)
		{
			this.NavAgent.ResetPath();
		}
		else if (this.NavAgent.hasPath)
		{
			moveToPosition = this.NavAgent.nextPosition;
		}
		this.Resume();
	}

	// Token: 0x06000B61 RID: 2913 RVA: 0x0004B5C0 File Offset: 0x000497C0
	private bool ValidateNextPosition(ref Vector3 moveToPosition)
	{
		if (!global::ValidBounds.Test(moveToPosition))
		{
			Debug.Log(string.Concat(new object[]
			{
				"Invalid NavAgent Position: ",
				this,
				" ",
				moveToPosition,
				" (destroying)"
			}));
			base.Kill(global::BaseNetworkable.DestroyMode.None);
			return false;
		}
		return true;
	}

	// Token: 0x06000B62 RID: 2914 RVA: 0x0004B624 File Offset: 0x00049824
	private void UpdateSpeed(float delta)
	{
		float num = this.DesiredMoveSpeed();
		this.NavAgent.speed = Mathf.Lerp(this.NavAgent.speed, num, delta * 1f);
	}

	// Token: 0x06000B63 RID: 2915 RVA: 0x0004B65C File Offset: 0x0004985C
	private void UpdatePositionAndRotation(Vector3 moveToPosition)
	{
		this.ServerPosition = moveToPosition;
		this.SetAimDirection(this.GetAimDirection());
	}

	// Token: 0x06000B64 RID: 2916 RVA: 0x0004B674 File Offset: 0x00049874
	public Vector3 GetPosition()
	{
		return base.transform.position;
	}

	// Token: 0x06000B65 RID: 2917 RVA: 0x0004B684 File Offset: 0x00049884
	public virtual float DesiredMoveSpeed()
	{
		float running = Mathf.Sin(Time.time + this.randomOffset);
		return base.GetSpeed(running, 0f);
	}

	// Token: 0x06000B66 RID: 2918 RVA: 0x0004B6B4 File Offset: 0x000498B4
	public override bool EligibleForWounding(global::HitInfo info)
	{
		return false;
	}

	// Token: 0x06000B67 RID: 2919 RVA: 0x0004B6B8 File Offset: 0x000498B8
	public virtual Vector3 GetAimDirection()
	{
		float num = Vector3Ex.Distance2D(this.finalDestination, this.GetPosition());
		if (num >= 1f)
		{
			Vector3 normalized = (this.finalDestination - this.GetPosition()).normalized;
			return new Vector3(normalized.x, 0f, normalized.z);
		}
		return this.eyes.BodyForward();
	}

	// Token: 0x06000B68 RID: 2920 RVA: 0x0004B720 File Offset: 0x00049920
	public virtual void SetAimDirection(Vector3 newAim)
	{
		if (newAim == Vector3.zero)
		{
			return;
		}
		global::AttackEntity attackEntity = this.GetAttackEntity();
		if (attackEntity)
		{
			newAim = attackEntity.ModifyAIAim(newAim, 1f);
		}
		this.eyes.rotation = Quaternion.LookRotation(newAim, Vector3.up);
		this.viewAngles = this.eyes.rotation.eulerAngles;
		this.ServerRotation = this.eyes.rotation;
	}

	// Token: 0x040005FA RID: 1530
	public Vector3 finalDestination;

	// Token: 0x040005FB RID: 1531
	[NonSerialized]
	private float randomOffset;

	// Token: 0x040005FC RID: 1532
	[NonSerialized]
	private Vector3 spawnPos;

	// Token: 0x040005FD RID: 1533
	public global::PlayerInventoryProperties[] loadouts;

	// Token: 0x040005FE RID: 1534
	public float stoppingDistance = 0.1f;

	// Token: 0x040005FF RID: 1535
	public LayerMask movementMask = 295772417;

	// Token: 0x04000600 RID: 1536
	public NavMeshAgent NavAgent;

	// Token: 0x04000605 RID: 1541
	public bool IsDormant;

	// Token: 0x04000606 RID: 1542
	private float triggerEndTime;

	// Token: 0x04000607 RID: 1543
	private float nextTriggerTime;

	// Token: 0x04000608 RID: 1544
	private float lastThinkTime;

	// Token: 0x04000609 RID: 1545
	private Vector3 lastPos;
}
