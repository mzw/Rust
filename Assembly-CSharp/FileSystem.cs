﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000339 RID: 825
public static class FileSystem
{
	// Token: 0x060013D6 RID: 5078 RVA: 0x000742E0 File Offset: 0x000724E0
	public static GameObject[] LoadPrefabs(string folder)
	{
		if (!folder.EndsWith("/"))
		{
			Debug.LogWarning("FileSystem.LoadPrefabs - folder should end in '/' - " + folder);
		}
		if (!folder.StartsWith("assets/"))
		{
			Debug.LogWarning("FileSystem.LoadPrefabs - should start with assets/ - " + folder);
		}
		return global::FileSystem.LoadAll<GameObject>(folder, ".prefab");
	}

	// Token: 0x060013D7 RID: 5079 RVA: 0x00074338 File Offset: 0x00072538
	public static GameObject LoadPrefab(string filePath)
	{
		if (!filePath.StartsWith("assets/", StringComparison.CurrentCultureIgnoreCase))
		{
			Debug.LogWarning("FileSystem.LoadPrefab - should start with assets/ - " + filePath);
		}
		return global::FileSystem.Load<GameObject>(filePath, true);
	}

	// Token: 0x060013D8 RID: 5080 RVA: 0x00074364 File Offset: 0x00072564
	public static string[] FindAll(string folder, string search = "")
	{
		return global::FileSystem.iface.FindAll(folder, search);
	}

	// Token: 0x060013D9 RID: 5081 RVA: 0x00074380 File Offset: 0x00072580
	public static T[] LoadAll<T>(string folder, string search = "") where T : Object
	{
		List<T> list = new List<T>();
		foreach (string filePath in global::FileSystem.FindAll(folder, search))
		{
			T t = global::FileSystem.Load<T>(filePath, true);
			if (t != null)
			{
				list.Add(t);
			}
		}
		return list.ToArray();
	}

	// Token: 0x060013DA RID: 5082 RVA: 0x000743DC File Offset: 0x000725DC
	public static T Load<T>(string filePath, bool bComplain = true) where T : Object
	{
		filePath = filePath.ToLower();
		T t = (T)((object)null);
		if (global::FileSystem.cache.ContainsKey(filePath))
		{
			t = (global::FileSystem.cache[filePath] as T);
		}
		else
		{
			t = global::FileSystem.iface.Load<T>(filePath, bComplain);
			if (t != null)
			{
				global::FileSystem.cache.Add(filePath, t);
			}
		}
		return t;
	}

	// Token: 0x060013DB RID: 5083 RVA: 0x00074454 File Offset: 0x00072654
	public static global::FileSystem.Operation LoadAsync(string filePath)
	{
		filePath = filePath.ToLower();
		global::FileSystem.Operation result;
		if (global::FileSystem.cache.ContainsKey(filePath))
		{
			result = new global::FileSystem.Operation(filePath, null);
		}
		else
		{
			result = new global::FileSystem.Operation(filePath, global::FileSystem.iface.LoadAsync(filePath));
		}
		return result;
	}

	// Token: 0x04000EBB RID: 3771
	public static global::IFileSystem iface = null;

	// Token: 0x04000EBC RID: 3772
	public static Dictionary<string, Object> cache = new Dictionary<string, Object>();

	// Token: 0x0200033A RID: 826
	public struct Operation
	{
		// Token: 0x060013DD RID: 5085 RVA: 0x000744B0 File Offset: 0x000726B0
		public Operation(string path, AsyncOperation request)
		{
			this.path = path;
			this.request = request;
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x060013DE RID: 5086 RVA: 0x000744C0 File Offset: 0x000726C0
		public bool isDone
		{
			get
			{
				return this.request == null || this.request.isDone;
			}
		}

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x060013DF RID: 5087 RVA: 0x000744E0 File Offset: 0x000726E0
		public float progress
		{
			get
			{
				return (this.request == null) ? 1f : this.request.progress;
			}
		}

		// Token: 0x060013E0 RID: 5088 RVA: 0x00074504 File Offset: 0x00072704
		public static implicit operator AsyncOperation(global::FileSystem.Operation op)
		{
			return op.request;
		}

		// Token: 0x060013E1 RID: 5089 RVA: 0x00074510 File Offset: 0x00072710
		public T Load<T>() where T : Object
		{
			T result = (T)((object)null);
			if (!this.isDone)
			{
				return result;
			}
			if (global::FileSystem.cache.ContainsKey(this.path))
			{
				result = (global::FileSystem.cache[this.path] as T);
			}
			else
			{
				AssetBundleRequest assetBundleRequest = this.request as AssetBundleRequest;
				if (assetBundleRequest != null && assetBundleRequest.asset != null)
				{
					global::FileSystem.cache.Add(this.path, assetBundleRequest.asset);
					result = (assetBundleRequest.asset as T);
				}
			}
			return result;
		}

		// Token: 0x04000EBD RID: 3773
		private string path;

		// Token: 0x04000EBE RID: 3774
		private AsyncOperation request;
	}
}
