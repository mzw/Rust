﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200020F RID: 527
public class ConstructionGrade : global::PrefabAttribute
{
	// Token: 0x17000110 RID: 272
	// (get) Token: 0x06000F8B RID: 3979 RVA: 0x0005F130 File Offset: 0x0005D330
	public float maxHealth
	{
		get
		{
			return (!this.gradeBase || !this.construction) ? 0f : (this.gradeBase.baseHealth * this.construction.healthMultiplier);
		}
	}

	// Token: 0x17000111 RID: 273
	// (get) Token: 0x06000F8C RID: 3980 RVA: 0x0005F180 File Offset: 0x0005D380
	public List<global::ItemAmount> costToBuild
	{
		get
		{
			if (this._costToBuild != null)
			{
				return this._costToBuild;
			}
			this._costToBuild = new List<global::ItemAmount>();
			foreach (global::ItemAmount itemAmount in this.gradeBase.baseCost)
			{
				this._costToBuild.Add(new global::ItemAmount(itemAmount.itemDef, Mathf.Ceil(itemAmount.amount * this.construction.costMultiplier)));
			}
			return this._costToBuild;
		}
	}

	// Token: 0x06000F8D RID: 3981 RVA: 0x0005F22C File Offset: 0x0005D42C
	protected override Type GetIndexedType()
	{
		return typeof(global::ConstructionGrade);
	}

	// Token: 0x04000A52 RID: 2642
	[NonSerialized]
	public global::Construction construction;

	// Token: 0x04000A53 RID: 2643
	public global::BuildingGrade gradeBase;

	// Token: 0x04000A54 RID: 2644
	public global::GameObjectRef skinObject;

	// Token: 0x04000A55 RID: 2645
	internal List<global::ItemAmount> _costToBuild;
}
