﻿using System;
using UnityEngine;

// Token: 0x020005D4 RID: 1492
public abstract class TerrainModifier : global::PrefabAttribute
{
	// Token: 0x06001EBF RID: 7871 RVA: 0x000AD7CC File Offset: 0x000AB9CC
	protected void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, this.Opacity);
		global::GizmosUtil.DrawWireCircleY(base.transform.position, base.transform.lossyScale.y * this.Radius);
	}

	// Token: 0x06001EC0 RID: 7872 RVA: 0x000AD824 File Offset: 0x000ABA24
	public void Apply(Vector3 pos, float scale)
	{
		this.Apply(pos, this.Opacity, scale * this.Radius, scale * this.Fade);
	}

	// Token: 0x06001EC1 RID: 7873
	protected abstract void Apply(Vector3 position, float opacity, float radius, float fade);

	// Token: 0x06001EC2 RID: 7874 RVA: 0x000AD844 File Offset: 0x000ABA44
	protected override Type GetIndexedType()
	{
		return typeof(global::TerrainModifier);
	}

	// Token: 0x04001999 RID: 6553
	public float Opacity = 1f;

	// Token: 0x0400199A RID: 6554
	public float Radius;

	// Token: 0x0400199B RID: 6555
	public float Fade;
}
