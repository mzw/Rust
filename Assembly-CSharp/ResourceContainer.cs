﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200008D RID: 141
public class ResourceContainer : global::EntityComponent<global::BaseEntity>
{
	// Token: 0x06000946 RID: 2374 RVA: 0x0003ECB0 File Offset: 0x0003CEB0
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("ResourceContainer.OnRpcMessage", 0.1f))
		{
			if (rpc == 384777705u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - StartLootingContainer ");
				}
				using (TimeWarning.New("StartLootingContainer", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("StartLootingContainer", this.GetBaseEntity(), player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.StartLootingContainer(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in StartLootingContainer");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x17000064 RID: 100
	// (get) Token: 0x06000947 RID: 2375 RVA: 0x0003EE94 File Offset: 0x0003D094
	public int accessedSecondsAgo
	{
		get
		{
			return (int)(UnityEngine.Time.realtimeSinceStartup - this.lastAccessTime);
		}
	}

	// Token: 0x06000948 RID: 2376 RVA: 0x0003EEA4 File Offset: 0x0003D0A4
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	private void StartLootingContainer(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!player || player.IsDead() || player.IsSleeping())
		{
			return;
		}
		if (!this.lootable)
		{
			return;
		}
		if (Interface.CallHook("CanLootEntity", new object[]
		{
			this,
			player
		}) != null)
		{
			return;
		}
		this.lastAccessTime = UnityEngine.Time.realtimeSinceStartup;
		player.inventory.loot.StartLootingEntity(base.baseEntity, true);
		player.inventory.loot.AddContainer(this.container);
	}

	// Token: 0x04000446 RID: 1094
	public bool lootable = true;

	// Token: 0x04000447 RID: 1095
	[NonSerialized]
	public global::ItemContainer container;

	// Token: 0x04000448 RID: 1096
	[NonSerialized]
	public float lastAccessTime;
}
