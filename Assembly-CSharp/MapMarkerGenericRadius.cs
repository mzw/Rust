﻿using System;
using Network;
using UnityEngine;

// Token: 0x0200007E RID: 126
public class MapMarkerGenericRadius : global::MapMarker
{
	// Token: 0x0600087D RID: 2173 RVA: 0x000373F4 File Offset: 0x000355F4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("MapMarkerGenericRadius.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600087E RID: 2174 RVA: 0x0003743C File Offset: 0x0003563C
	public void SendUpdate(bool fullUpdate = true)
	{
		float a = this.color1.a;
		Vector3 arg;
		arg..ctor(this.color1.r, this.color1.g, this.color1.b);
		Vector3 arg2;
		arg2..ctor(this.color2.r, this.color2.g, this.color2.b);
		base.ClientRPC<Vector3, float, Vector3, float, float>(null, "MarkerUpdate", arg, a, arg2, this.alpha, this.radius);
	}

	// Token: 0x040003F1 RID: 1009
	public float radius;

	// Token: 0x040003F2 RID: 1010
	public Color color1;

	// Token: 0x040003F3 RID: 1011
	public Color color2;

	// Token: 0x040003F4 RID: 1012
	public float alpha;
}
