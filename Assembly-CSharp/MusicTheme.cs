﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001E9 RID: 489
[CreateAssetMenu(menuName = "Rust/MusicTheme")]
public class MusicTheme : ScriptableObject
{
	// Token: 0x17000106 RID: 262
	// (get) Token: 0x06000F2A RID: 3882 RVA: 0x0005D4BC File Offset: 0x0005B6BC
	public int layerCount
	{
		get
		{
			return this.layers.Count;
		}
	}

	// Token: 0x17000107 RID: 263
	// (get) Token: 0x06000F2B RID: 3883 RVA: 0x0005D4CC File Offset: 0x0005B6CC
	public int samplesPerBar
	{
		get
		{
			return global::MusicUtil.BarsToSamples(this.tempo, 1f, 44100);
		}
	}

	// Token: 0x06000F2C RID: 3884 RVA: 0x0005D4E4 File Offset: 0x0005B6E4
	private void OnValidate()
	{
		this.audioClipDict.Clear();
		this.activeClips.Clear();
		this.UpdateLengthInBars();
		for (int i = 0; i < this.clips.Count; i++)
		{
			global::MusicTheme.PositionedClip positionedClip = this.clips[i];
			int num = this.ActiveClipCollectionID(positionedClip.startingBar - 8);
			int num2 = this.ActiveClipCollectionID(positionedClip.endingBar);
			for (int j = num; j <= num2; j++)
			{
				if (!this.activeClips.ContainsKey(j))
				{
					this.activeClips.Add(j, new List<global::MusicTheme.PositionedClip>());
				}
				if (!this.activeClips[j].Contains(positionedClip))
				{
					this.activeClips[j].Add(positionedClip);
				}
			}
			if (positionedClip.musicClip != null)
			{
				AudioClip audioClip = positionedClip.musicClip.audioClip;
				if (!this.audioClipDict.ContainsKey(audioClip))
				{
					this.audioClipDict.Add(audioClip, true);
				}
				if (positionedClip.startingBar < 8 && !this.firstAudioClips.Contains(audioClip))
				{
					this.firstAudioClips.Add(audioClip);
				}
				positionedClip.musicClip.lengthInBarsWithTail = Mathf.CeilToInt(global::MusicUtil.SecondsToBars(this.tempo, (double)positionedClip.musicClip.audioClip.length));
			}
		}
	}

	// Token: 0x06000F2D RID: 3885 RVA: 0x0005D64C File Offset: 0x0005B84C
	public List<global::MusicTheme.PositionedClip> GetActiveClipsForBar(int bar)
	{
		int key = this.ActiveClipCollectionID(bar);
		if (!this.activeClips.ContainsKey(key))
		{
			return null;
		}
		return this.activeClips[key];
	}

	// Token: 0x06000F2E RID: 3886 RVA: 0x0005D680 File Offset: 0x0005B880
	private int ActiveClipCollectionID(int bar)
	{
		return Mathf.FloorToInt(Mathf.Max((float)(bar / 4), 0f));
	}

	// Token: 0x06000F2F RID: 3887 RVA: 0x0005D698 File Offset: 0x0005B898
	public global::MusicTheme.Layer LayerById(int id)
	{
		if (this.layers.Count <= id)
		{
			return null;
		}
		return this.layers[id];
	}

	// Token: 0x06000F30 RID: 3888 RVA: 0x0005D6BC File Offset: 0x0005B8BC
	public void AddLayer()
	{
		global::MusicTheme.Layer layer = new global::MusicTheme.Layer();
		layer.name = "layer " + this.layers.Count;
		this.layers.Add(layer);
	}

	// Token: 0x06000F31 RID: 3889 RVA: 0x0005D6FC File Offset: 0x0005B8FC
	private void UpdateLengthInBars()
	{
		int num = 0;
		for (int i = 0; i < this.clips.Count; i++)
		{
			global::MusicTheme.PositionedClip positionedClip = this.clips[i];
			if (!(positionedClip.musicClip == null))
			{
				int num2 = positionedClip.startingBar + positionedClip.musicClip.lengthInBars;
				if (num2 > num)
				{
					num = num2;
				}
			}
		}
		this.lengthInBars = num;
	}

	// Token: 0x06000F32 RID: 3890 RVA: 0x0005D770 File Offset: 0x0005B970
	public bool CanPlayInEnvironment(int currentBiome, int currentTopology, float currentRain, float currentSnow, float currentWind)
	{
		return (!TOD_Sky.Instance || this.time.Evaluate(TOD_Sky.Instance.Cycle.Hour) >= 0f) && (this.biomes == (global::TerrainBiome.Enum)(-1) || (this.biomes & (global::TerrainBiome.Enum)currentBiome) != (global::TerrainBiome.Enum)0) && (this.topologies == (global::TerrainTopology.Enum)(-1) || (this.topologies & (global::TerrainTopology.Enum)currentTopology) == (global::TerrainTopology.Enum)0) && ((this.rain.min <= 0f && this.rain.max >= 1f) || currentRain >= this.rain.min) && currentRain <= this.rain.max && ((this.snow.min <= 0f && this.snow.max >= 1f) || currentSnow >= this.snow.min) && currentSnow <= this.snow.max && ((this.wind.min <= 0f && this.wind.max >= 1f) || currentWind >= this.wind.min) && currentWind <= this.wind.max;
	}

	// Token: 0x06000F33 RID: 3891 RVA: 0x0005D8D8 File Offset: 0x0005BAD8
	public bool FirstClipsLoaded()
	{
		for (int i = 0; i < this.firstAudioClips.Count; i++)
		{
			if (this.firstAudioClips[i].loadState != 2)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000F34 RID: 3892 RVA: 0x0005D91C File Offset: 0x0005BB1C
	public bool ContainsAudioClip(AudioClip clip)
	{
		return this.audioClipDict.ContainsKey(clip);
	}

	// Token: 0x0400098C RID: 2444
	[Header("Basic info")]
	public float tempo = 80f;

	// Token: 0x0400098D RID: 2445
	public int intensityHoldBars = 4;

	// Token: 0x0400098E RID: 2446
	public int lengthInBars;

	// Token: 0x0400098F RID: 2447
	[Header("Playback restrictions")]
	public bool canPlayInMenus = true;

	// Token: 0x04000990 RID: 2448
	[Horizontal(2, -1)]
	public global::MusicTheme.ValueRange rain = new global::MusicTheme.ValueRange(0f, 1f);

	// Token: 0x04000991 RID: 2449
	[Horizontal(2, -1)]
	public global::MusicTheme.ValueRange wind = new global::MusicTheme.ValueRange(0f, 1f);

	// Token: 0x04000992 RID: 2450
	[Horizontal(2, -1)]
	public global::MusicTheme.ValueRange snow = new global::MusicTheme.ValueRange(0f, 1f);

	// Token: 0x04000993 RID: 2451
	[InspectorFlags]
	public global::TerrainBiome.Enum biomes = (global::TerrainBiome.Enum)(-1);

	// Token: 0x04000994 RID: 2452
	[InspectorFlags]
	public global::TerrainTopology.Enum topologies = (global::TerrainTopology.Enum)(-1);

	// Token: 0x04000995 RID: 2453
	public AnimationCurve time = AnimationCurve.Linear(0f, 0f, 24f, 0f);

	// Token: 0x04000996 RID: 2454
	[Header("Clip data")]
	public List<global::MusicTheme.PositionedClip> clips = new List<global::MusicTheme.PositionedClip>();

	// Token: 0x04000997 RID: 2455
	public List<global::MusicTheme.Layer> layers = new List<global::MusicTheme.Layer>();

	// Token: 0x04000998 RID: 2456
	private Dictionary<int, List<global::MusicTheme.PositionedClip>> activeClips = new Dictionary<int, List<global::MusicTheme.PositionedClip>>();

	// Token: 0x04000999 RID: 2457
	private List<AudioClip> firstAudioClips = new List<AudioClip>();

	// Token: 0x0400099A RID: 2458
	private Dictionary<AudioClip, bool> audioClipDict = new Dictionary<AudioClip, bool>();

	// Token: 0x020001EA RID: 490
	[Serializable]
	public class Layer
	{
		// Token: 0x0400099B RID: 2459
		public string name = "layer";
	}

	// Token: 0x020001EB RID: 491
	[Serializable]
	public class PositionedClip
	{
		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06000F37 RID: 3895 RVA: 0x0005D978 File Offset: 0x0005BB78
		public int endingBar
		{
			get
			{
				return (!(this.musicClip == null)) ? (this.startingBar + this.musicClip.lengthInBarsWithTail) : this.startingBar;
			}
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x0005D9A8 File Offset: 0x0005BBA8
		public bool CanPlay(float intensity)
		{
			return (intensity > this.minIntensity || (this.minIntensity == 0f && intensity == 0f)) && intensity <= this.maxIntensity;
		}

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x06000F39 RID: 3897 RVA: 0x0005D9E0 File Offset: 0x0005BBE0
		public bool isControlClip
		{
			get
			{
				return this.musicClip == null;
			}
		}

		// Token: 0x06000F3A RID: 3898 RVA: 0x0005D9F0 File Offset: 0x0005BBF0
		public void CopySettingsFrom(global::MusicTheme.PositionedClip otherClip)
		{
			if (this.isControlClip != otherClip.isControlClip)
			{
				return;
			}
			if (otherClip == this)
			{
				return;
			}
			this.allowFadeIn = otherClip.allowFadeIn;
			this.fadeInTime = otherClip.fadeInTime;
			this.allowFadeOut = otherClip.allowFadeOut;
			this.fadeOutTime = otherClip.fadeOutTime;
			this.maxIntensity = otherClip.maxIntensity;
			this.minIntensity = otherClip.minIntensity;
			this.intensityReduction = otherClip.intensityReduction;
		}

		// Token: 0x0400099C RID: 2460
		public global::MusicTheme theme;

		// Token: 0x0400099D RID: 2461
		public global::MusicClip musicClip;

		// Token: 0x0400099E RID: 2462
		public int startingBar;

		// Token: 0x0400099F RID: 2463
		public int layerId;

		// Token: 0x040009A0 RID: 2464
		public float minIntensity;

		// Token: 0x040009A1 RID: 2465
		public float maxIntensity = 1f;

		// Token: 0x040009A2 RID: 2466
		public bool allowFadeIn = true;

		// Token: 0x040009A3 RID: 2467
		public bool allowFadeOut = true;

		// Token: 0x040009A4 RID: 2468
		public float fadeInTime = 1f;

		// Token: 0x040009A5 RID: 2469
		public float fadeOutTime = 0.5f;

		// Token: 0x040009A6 RID: 2470
		public float intensityReduction;
	}

	// Token: 0x020001EC RID: 492
	[Serializable]
	public class ValueRange
	{
		// Token: 0x06000F3B RID: 3899 RVA: 0x0005DA6C File Offset: 0x0005BC6C
		public ValueRange(float min, float max)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x040009A7 RID: 2471
		public float min;

		// Token: 0x040009A8 RID: 2472
		public float max;
	}
}
