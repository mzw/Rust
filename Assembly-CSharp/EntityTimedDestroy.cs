﻿using System;
using UnityEngine;

// Token: 0x0200036F RID: 879
public class EntityTimedDestroy : global::EntityComponent<global::BaseEntity>
{
	// Token: 0x060014F4 RID: 5364 RVA: 0x00079384 File Offset: 0x00077584
	private void OnEnable()
	{
		base.Invoke(new Action(this.TimedDestroy), this.secondsTillDestroy);
	}

	// Token: 0x060014F5 RID: 5365 RVA: 0x000793A0 File Offset: 0x000775A0
	private void TimedDestroy()
	{
		if (base.baseEntity != null)
		{
			base.baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		else
		{
			Debug.LogWarning("EntityTimedDestroy failed, baseEntity was already null!");
		}
	}

	// Token: 0x04000F71 RID: 3953
	public float secondsTillDestroy = 1f;
}
