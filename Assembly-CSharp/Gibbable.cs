﻿using System;
using UnityEngine;

// Token: 0x0200042A RID: 1066
public class Gibbable : MonoBehaviour, IClientComponent
{
	// Token: 0x040012D4 RID: 4820
	public GameObject gibSource;

	// Token: 0x040012D5 RID: 4821
	public GameObject materialSource;

	// Token: 0x040012D6 RID: 4822
	public bool copyMaterialBlock = true;

	// Token: 0x040012D7 RID: 4823
	public PhysicMaterial physicsMaterial;

	// Token: 0x040012D8 RID: 4824
	public global::GameObjectRef fxPrefab;

	// Token: 0x040012D9 RID: 4825
	public bool spawnFxPrefab = true;

	// Token: 0x040012DA RID: 4826
	[Tooltip("If enabled, gibs will spawn even though we've hit a gib limit")]
	public bool important;

	// Token: 0x040012DB RID: 4827
	public float explodeScale;
}
