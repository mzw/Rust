﻿using System;
using UnityEngine.EventSystems;

// Token: 0x020006EC RID: 1772
public class PaintableImageGrid : UIBehaviour
{
	// Token: 0x04001E09 RID: 7689
	public global::UIPaintableImage templateImage;

	// Token: 0x04001E0A RID: 7690
	public int cols = 4;

	// Token: 0x04001E0B RID: 7691
	public int rows = 4;
}
