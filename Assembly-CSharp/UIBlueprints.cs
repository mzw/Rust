﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006A7 RID: 1703
public class UIBlueprints : ListComponent<global::UIBlueprints>
{
	// Token: 0x04001CDA RID: 7386
	public global::GameObjectRef buttonPrefab;

	// Token: 0x04001CDB RID: 7387
	public ScrollRect scrollRect;

	// Token: 0x04001CDC RID: 7388
	public InputField searchField;

	// Token: 0x04001CDD RID: 7389
	public GameObject listAvailable;

	// Token: 0x04001CDE RID: 7390
	public GameObject listLocked;

	// Token: 0x04001CDF RID: 7391
	public GameObject Categories;
}
