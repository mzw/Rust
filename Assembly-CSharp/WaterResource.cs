﻿using System;
using UnityEngine;

// Token: 0x020004A3 RID: 1187
public class WaterResource
{
	// Token: 0x060019CC RID: 6604 RVA: 0x000912EC File Offset: 0x0008F4EC
	public static global::ItemDefinition GetAtPoint(Vector3 pos)
	{
		bool flag = global::WaterResource.IsFreshWater(pos);
		return global::ItemManager.FindItemDefinition((!flag) ? "water.salt" : "water");
	}

	// Token: 0x060019CD RID: 6605 RVA: 0x0009131C File Offset: 0x0008F51C
	public static bool IsFreshWater(Vector3 pos)
	{
		return !(global::TerrainMeta.TopologyMap == null) && global::TerrainMeta.TopologyMap.GetTopology(pos, 245760);
	}

	// Token: 0x060019CE RID: 6606 RVA: 0x00091340 File Offset: 0x0008F540
	public static global::ItemDefinition Merge(global::ItemDefinition first, global::ItemDefinition second)
	{
		if (first == second)
		{
			return first;
		}
		bool flag = first.shortname == "water.salt" || second.shortname == "water.salt";
		if (flag)
		{
			return global::ItemManager.FindItemDefinition("water.salt");
		}
		return global::ItemManager.FindItemDefinition("water");
	}
}
