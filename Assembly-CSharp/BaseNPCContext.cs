﻿using System;
using Rust.Ai;

// Token: 0x020000E0 RID: 224
public class BaseNPCContext : Rust.Ai.BaseContext
{
	// Token: 0x06000B25 RID: 2853 RVA: 0x0004AA9C File Offset: 0x00048C9C
	public BaseNPCContext(global::IAIAgent agent) : base(agent)
	{
		this.Human = (agent as global::NPCPlayerApex);
	}

	// Token: 0x040005E9 RID: 1513
	public global::NPCPlayerApex Human;

	// Token: 0x040005EA RID: 1514
	public Rust.Ai.AiLocationManager AiLocationManager;
}
