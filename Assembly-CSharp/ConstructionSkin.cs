﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000211 RID: 529
public class ConstructionSkin : global::BasePrefab
{
	// Token: 0x06000F92 RID: 3986 RVA: 0x0005F304 File Offset: 0x0005D504
	private void RefreshColliderBatching()
	{
		if (this.colliderBatches == null)
		{
			this.colliderBatches = base.GetComponentsInChildren<global::ColliderBatch>(true);
		}
		for (int i = 0; i < this.colliderBatches.Length; i++)
		{
			this.colliderBatches[i].Refresh();
		}
	}

	// Token: 0x06000F93 RID: 3987 RVA: 0x0005F350 File Offset: 0x0005D550
	public int DetermineConditionalModelState(global::BuildingBlock parent)
	{
		global::ConditionalModel[] array = global::PrefabAttribute.server.FindAll<global::ConditionalModel>(this.prefabID);
		int num = 0;
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i].RunTests(parent))
			{
				num |= 1 << i;
			}
		}
		return num;
	}

	// Token: 0x06000F94 RID: 3988 RVA: 0x0005F39C File Offset: 0x0005D59C
	private void CreateConditionalModels(global::BuildingBlock parent)
	{
		global::ConditionalModel[] array = global::PrefabAttribute.server.FindAll<global::ConditionalModel>(this.prefabID);
		for (int i = 0; i < array.Length; i++)
		{
			if (parent.GetConditionalModel(i))
			{
				GameObject gameObject = array[i].InstantiateSkin(parent);
				if (!(gameObject == null))
				{
					if (this.conditionals == null)
					{
						this.conditionals = new List<GameObject>();
					}
					this.conditionals.Add(gameObject);
				}
			}
		}
	}

	// Token: 0x06000F95 RID: 3989 RVA: 0x0005F41C File Offset: 0x0005D61C
	private void DestroyConditionalModels(global::BuildingBlock parent)
	{
		if (this.conditionals == null)
		{
			return;
		}
		for (int i = 0; i < this.conditionals.Count; i++)
		{
			parent.gameManager.Retire(this.conditionals[i]);
		}
		this.conditionals.Clear();
	}

	// Token: 0x06000F96 RID: 3990 RVA: 0x0005F474 File Offset: 0x0005D674
	public void Refresh(global::BuildingBlock parent)
	{
		this.DestroyConditionalModels(parent);
		if (parent.isServer)
		{
			this.RefreshColliderBatching();
		}
		this.CreateConditionalModels(parent);
	}

	// Token: 0x06000F97 RID: 3991 RVA: 0x0005F498 File Offset: 0x0005D698
	public void Destroy(global::BuildingBlock parent)
	{
		this.DestroyConditionalModels(parent);
		parent.gameManager.Retire(base.gameObject);
	}

	// Token: 0x04000A5A RID: 2650
	private global::ColliderBatch[] colliderBatches;

	// Token: 0x04000A5B RID: 2651
	private List<GameObject> conditionals;
}
