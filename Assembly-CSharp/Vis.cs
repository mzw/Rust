﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x020004A0 RID: 1184
public static class Vis
{
	// Token: 0x060019C1 RID: 6593 RVA: 0x00090C64 File Offset: 0x0008EE64
	public static void Colliders<T>(Vector3 position, float radius, List<T> list, int layerMask = -1, QueryTriggerInteraction triggerInteraction = 2) where T : Collider
	{
		layerMask = global::GamePhysics.HandleTerrainCollision(position, layerMask);
		int num = Physics.OverlapSphereNonAlloc(position, radius, global::Vis.colBuffer, layerMask, triggerInteraction);
		if (num >= global::Vis.colBuffer.Length)
		{
			Debug.LogWarning("Vis query is exceeding collider buffer length.");
		}
		for (int i = 0; i < num; i++)
		{
			T t = global::Vis.colBuffer[i] as T;
			global::Vis.colBuffer[i] = null;
			if (!(t == null) && t.enabled)
			{
				if (t.transform.CompareTag("MeshColliderBatch"))
				{
					global::MeshColliderBatch component = t.transform.GetComponent<global::MeshColliderBatch>();
					component.LookupColliders<T>(position, radius, list);
				}
				else
				{
					list.Add(t);
				}
			}
		}
	}

	// Token: 0x060019C2 RID: 6594 RVA: 0x00090D3C File Offset: 0x0008EF3C
	public static void Components<T>(Vector3 position, float radius, List<T> list, int layerMask = -1, QueryTriggerInteraction triggerInteraction = 2) where T : Component
	{
		List<Collider> list2 = Pool.GetList<Collider>();
		global::Vis.Colliders<Collider>(position, radius, list2, layerMask, triggerInteraction);
		for (int i = 0; i < list2.Count; i++)
		{
			T component = list2[i].gameObject.GetComponent<T>();
			if (!(component == null))
			{
				list.Add(component);
			}
		}
		Pool.FreeList<Collider>(ref list2);
	}

	// Token: 0x060019C3 RID: 6595 RVA: 0x00090DA8 File Offset: 0x0008EFA8
	public static void Entities<T>(Vector3 position, float radius, List<T> list, int layerMask = -1, QueryTriggerInteraction triggerInteraction = 2) where T : global::BaseEntity
	{
		List<Collider> list2 = Pool.GetList<Collider>();
		global::Vis.Colliders<Collider>(position, radius, list2, layerMask, triggerInteraction);
		for (int i = 0; i < list2.Count; i++)
		{
			T t = list2[i].gameObject.ToBaseEntity() as T;
			if (!(t == null))
			{
				list.Add(t);
			}
		}
		Pool.FreeList<Collider>(ref list2);
	}

	// Token: 0x060019C4 RID: 6596 RVA: 0x00090E20 File Offset: 0x0008F020
	public static void EntityComponents<T>(Vector3 position, float radius, List<T> list, int layerMask = -1, QueryTriggerInteraction triggerInteraction = 2) where T : global::EntityComponentBase
	{
		List<Collider> list2 = Pool.GetList<Collider>();
		global::Vis.Colliders<Collider>(position, radius, list2, layerMask, triggerInteraction);
		for (int i = 0; i < list2.Count; i++)
		{
			global::BaseEntity baseEntity = list2[i].gameObject.ToBaseEntity();
			if (!(baseEntity == null))
			{
				T component = baseEntity.gameObject.GetComponent<T>();
				if (!(component == null))
				{
					list.Add(component);
				}
			}
		}
		Pool.FreeList<Collider>(ref list2);
	}

	// Token: 0x04001451 RID: 5201
	private static Collider[] colBuffer = new Collider[8192];
}
