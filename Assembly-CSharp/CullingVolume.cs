﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200040A RID: 1034
public class CullingVolume : MonoBehaviour, IClientComponent
{
	// Token: 0x04001257 RID: 4695
	public bool Portal;

	// Token: 0x04001258 RID: 4696
	public List<global::CullingVolume> Connections = new List<global::CullingVolume>();
}
