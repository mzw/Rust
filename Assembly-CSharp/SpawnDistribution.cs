﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000487 RID: 1159
public class SpawnDistribution
{
	// Token: 0x0600192B RID: 6443 RVA: 0x0008DDEC File Offset: 0x0008BFEC
	public SpawnDistribution(global::SpawnHandler handler, byte[] baseValues, Vector3 origin, Vector3 area)
	{
		this.Handler = handler;
		this.quadtree.UpdateValues(baseValues);
		this.origin = origin;
		float num = 0f;
		for (int i = 0; i < baseValues.Length; i++)
		{
			num += (float)baseValues[i];
		}
		this.Density = num / (float)(255 * baseValues.Length);
		this.Count = 0;
		this.area = new Vector3(area.x / (float)this.quadtree.Size, area.y, area.z / (float)this.quadtree.Size);
		this.grid = new WorldSpaceGrid<int>(area.x, 20f);
	}

	// Token: 0x0600192C RID: 6444 RVA: 0x0008DEBC File Offset: 0x0008C0BC
	public bool Sample(out Vector3 spawnPos, out Quaternion spawnRot, bool alignToNormal = false, float dithering = 0f)
	{
		return this.Sample(out spawnPos, out spawnRot, this.SampleNode(), alignToNormal, dithering);
	}

	// Token: 0x0600192D RID: 6445 RVA: 0x0008DED0 File Offset: 0x0008C0D0
	public bool Sample(out Vector3 spawnPos, out Quaternion spawnRot, global::ByteQuadtree.Element node, bool alignToNormal = false, float dithering = 0f)
	{
		if (this.Handler == null || global::TerrainMeta.HeightMap == null)
		{
			spawnPos = Vector3.zero;
			spawnRot = Quaternion.identity;
			return false;
		}
		LayerMask placementMask = this.Handler.PlacementMask;
		LayerMask placementCheckMask = this.Handler.PlacementCheckMask;
		float placementCheckHeight = this.Handler.PlacementCheckHeight;
		LayerMask radiusCheckMask = this.Handler.RadiusCheckMask;
		float radiusCheckDistance = this.Handler.RadiusCheckDistance;
		for (int i = 0; i < 15; i++)
		{
			spawnPos = this.origin;
			spawnPos.x += node.Coords.x * this.area.x;
			spawnPos.z += node.Coords.y * this.area.z;
			spawnPos.x += Random.value * this.area.x;
			spawnPos.z += Random.value * this.area.z;
			spawnPos.x += Random.Range(-dithering, dithering);
			spawnPos.z += Random.Range(-dithering, dithering);
			Vector3 vector;
			vector..ctor(spawnPos.x, global::TerrainMeta.HeightMap.GetHeight(spawnPos), spawnPos.z);
			if (vector.y > spawnPos.y)
			{
				RaycastHit raycastHit;
				if (placementCheckMask != 0 && Physics.Raycast(vector + Vector3.up * placementCheckHeight, Vector3.down, ref raycastHit, placementCheckHeight, placementCheckMask))
				{
					int num = 1 << raycastHit.transform.gameObject.layer;
					if ((num & placementMask) == 0)
					{
						goto IL_289;
					}
					vector.y = raycastHit.point.y;
				}
				if (radiusCheckMask == 0 || !Physics.CheckSphere(vector, radiusCheckDistance, radiusCheckMask))
				{
					spawnPos.y = vector.y;
					spawnRot = Quaternion.Euler(new Vector3(0f, Random.Range(0f, 360f), 0f));
					if (alignToNormal)
					{
						Vector3 normal = global::TerrainMeta.HeightMap.GetNormal(spawnPos);
						spawnRot = UnityEngine.QuaternionEx.LookRotationForcedUp(spawnRot * Vector3.forward, normal);
					}
					return true;
				}
			}
			IL_289:;
		}
		spawnPos = Vector3.zero;
		spawnRot = Quaternion.identity;
		return false;
	}

	// Token: 0x0600192E RID: 6446 RVA: 0x0008E18C File Offset: 0x0008C38C
	public global::ByteQuadtree.Element SampleNode()
	{
		global::ByteQuadtree.Element result = this.quadtree.Root;
		while (!result.IsLeaf)
		{
			result = result.RandChild;
		}
		return result;
	}

	// Token: 0x0600192F RID: 6447 RVA: 0x0008E1C0 File Offset: 0x0008C3C0
	public void AddInstance(global::Spawnable spawnable)
	{
		this.UpdateCount(spawnable, 1);
	}

	// Token: 0x06001930 RID: 6448 RVA: 0x0008E1CC File Offset: 0x0008C3CC
	public void RemoveInstance(global::Spawnable spawnable)
	{
		this.UpdateCount(spawnable, -1);
	}

	// Token: 0x06001931 RID: 6449 RVA: 0x0008E1D8 File Offset: 0x0008C3D8
	private void UpdateCount(global::Spawnable spawnable, int delta)
	{
		this.Count += delta;
		WorldSpaceGrid<int> worldSpaceGrid;
		Vector3 spawnPosition;
		(worldSpaceGrid = this.grid)[spawnPosition = spawnable.SpawnPosition] = worldSpaceGrid[spawnPosition] + delta;
		global::BaseEntity component = spawnable.GetComponent<global::BaseEntity>();
		if (component)
		{
			int num;
			if (this.dict.TryGetValue(component.prefabID, out num))
			{
				this.dict[component.prefabID] = num + delta;
			}
			else
			{
				num = delta;
				this.dict.Add(component.prefabID, num);
			}
		}
	}

	// Token: 0x06001932 RID: 6450 RVA: 0x0008E268 File Offset: 0x0008C468
	public int GetCount(uint prefabID)
	{
		int result;
		this.dict.TryGetValue(prefabID, out result);
		return result;
	}

	// Token: 0x06001933 RID: 6451 RVA: 0x0008E288 File Offset: 0x0008C488
	public int GetCount(Vector3 position)
	{
		return this.grid[position];
	}

	// Token: 0x06001934 RID: 6452 RVA: 0x0008E298 File Offset: 0x0008C498
	public float GetGridCellArea()
	{
		return this.grid.CellArea;
	}

	// Token: 0x040013DE RID: 5086
	internal global::SpawnHandler Handler;

	// Token: 0x040013DF RID: 5087
	internal float Density;

	// Token: 0x040013E0 RID: 5088
	internal int Count;

	// Token: 0x040013E1 RID: 5089
	private WorldSpaceGrid<int> grid;

	// Token: 0x040013E2 RID: 5090
	private Dictionary<uint, int> dict = new Dictionary<uint, int>();

	// Token: 0x040013E3 RID: 5091
	private global::ByteQuadtree quadtree = new global::ByteQuadtree();

	// Token: 0x040013E4 RID: 5092
	private Vector3 origin;

	// Token: 0x040013E5 RID: 5093
	private Vector3 area;
}
