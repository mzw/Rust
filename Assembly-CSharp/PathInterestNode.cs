﻿using System;
using UnityEngine;

// Token: 0x020000CC RID: 204
public class PathInterestNode : MonoBehaviour
{
	// Token: 0x1700006B RID: 107
	// (get) Token: 0x06000AD1 RID: 2769 RVA: 0x00049694 File Offset: 0x00047894
	// (set) Token: 0x06000AD2 RID: 2770 RVA: 0x0004969C File Offset: 0x0004789C
	public float NextVisitTime { get; set; }

	// Token: 0x06000AD3 RID: 2771 RVA: 0x000496A8 File Offset: 0x000478A8
	public void OnDrawGizmos()
	{
		Gizmos.color = new Color(0f, 1f, 1f, 0.5f);
		Gizmos.DrawSphere(base.transform.position, 0.5f);
	}
}
