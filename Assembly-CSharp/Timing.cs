﻿using System;
using System.Diagnostics;
using UnityEngine;

// Token: 0x020007A7 RID: 1959
public struct Timing
{
	// Token: 0x0600247B RID: 9339 RVA: 0x000C916C File Offset: 0x000C736C
	public Timing(string name)
	{
		this.sw = Stopwatch.StartNew();
		this.name = name;
	}

	// Token: 0x0600247C RID: 9340 RVA: 0x000C9180 File Offset: 0x000C7380
	public static global::Timing Start(string name)
	{
		return new global::Timing(name);
	}

	// Token: 0x0600247D RID: 9341 RVA: 0x000C9188 File Offset: 0x000C7388
	public void End()
	{
		if (this.sw.Elapsed.TotalSeconds > 0.30000001192092896)
		{
			Debug.Log("[" + this.sw.Elapsed.TotalSeconds.ToString("0.0") + "s] " + this.name);
		}
	}

	// Token: 0x04001FEE RID: 8174
	private Stopwatch sw;

	// Token: 0x04001FEF RID: 8175
	private string name;
}
