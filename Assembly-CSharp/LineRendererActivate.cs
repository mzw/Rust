﻿using System;
using UnityEngine;

// Token: 0x020000D6 RID: 214
public class LineRendererActivate : MonoBehaviour, IClientComponent
{
	// Token: 0x06000B04 RID: 2820 RVA: 0x0004A254 File Offset: 0x00048454
	private void OnEnable()
	{
		base.GetComponent<LineRenderer>().enabled = true;
	}
}
