﻿using System;

// Token: 0x020005C0 RID: 1472
public class GenerateWireMeshes : global::ProceduralComponent
{
	// Token: 0x06001E8D RID: 7821 RVA: 0x000AB51C File Offset: 0x000A971C
	public override void Process(uint seed)
	{
		global::TerrainMeta.Path.CreateWires();
	}

	// Token: 0x1700021C RID: 540
	// (get) Token: 0x06001E8E RID: 7822 RVA: 0x000AB528 File Offset: 0x000A9728
	public override bool RunOnCache
	{
		get
		{
			return true;
		}
	}
}
