﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x020001E5 RID: 485
public class MusicClipLoader
{
	// Token: 0x06000F20 RID: 3872 RVA: 0x0005D118 File Offset: 0x0005B318
	public void Update()
	{
		for (int i = this.clipsToLoad.Count - 1; i >= 0; i--)
		{
			AudioClip audioClip = this.clipsToLoad[i];
			if (audioClip.loadState != 2 && audioClip.loadState != 1)
			{
				audioClip.LoadAudioData();
				this.clipsToLoad.RemoveAt(i);
				return;
			}
		}
		for (int j = this.clipsToUnload.Count - 1; j >= 0; j--)
		{
			AudioClip audioClip2 = this.clipsToUnload[j];
			if (audioClip2.loadState == 2)
			{
				audioClip2.UnloadAudioData();
				this.clipsToUnload.RemoveAt(j);
				return;
			}
		}
	}

	// Token: 0x06000F21 RID: 3873 RVA: 0x0005D1C8 File Offset: 0x0005B3C8
	public void Refresh()
	{
		for (int i = 0; i < global::MusicManager.instance.activeMusicClips.Count; i++)
		{
			global::MusicTheme.PositionedClip positionedClip = global::MusicManager.instance.activeMusicClips[i];
			global::MusicClipLoader.LoadedAudioClip loadedAudioClip = this.FindLoadedClip(positionedClip.musicClip.audioClip);
			if (loadedAudioClip == null)
			{
				loadedAudioClip = Pool.Get<global::MusicClipLoader.LoadedAudioClip>();
				loadedAudioClip.clip = positionedClip.musicClip.audioClip;
				loadedAudioClip.unloadTime = (float)UnityEngine.AudioSettings.dspTime + loadedAudioClip.clip.length + 1f;
				this.loadedClips.Add(loadedAudioClip);
				this.loadedClipDict.Add(loadedAudioClip.clip, loadedAudioClip);
				this.clipsToLoad.Add(loadedAudioClip.clip);
			}
			else
			{
				loadedAudioClip.unloadTime = (float)UnityEngine.AudioSettings.dspTime + loadedAudioClip.clip.length + 1f;
				this.clipsToUnload.Remove(loadedAudioClip.clip);
			}
		}
		for (int j = this.loadedClips.Count - 1; j >= 0; j--)
		{
			global::MusicClipLoader.LoadedAudioClip loadedAudioClip2 = this.loadedClips[j];
			if (UnityEngine.AudioSettings.dspTime > (double)loadedAudioClip2.unloadTime)
			{
				this.clipsToUnload.Add(loadedAudioClip2.clip);
				this.loadedClips.Remove(loadedAudioClip2);
				this.loadedClipDict.Remove(loadedAudioClip2.clip);
				Pool.Free<global::MusicClipLoader.LoadedAudioClip>(ref loadedAudioClip2);
			}
		}
	}

	// Token: 0x06000F22 RID: 3874 RVA: 0x0005D330 File Offset: 0x0005B530
	private global::MusicClipLoader.LoadedAudioClip FindLoadedClip(AudioClip clip)
	{
		if (this.loadedClipDict.ContainsKey(clip))
		{
			return this.loadedClipDict[clip];
		}
		return null;
	}

	// Token: 0x04000967 RID: 2407
	public List<global::MusicClipLoader.LoadedAudioClip> loadedClips = new List<global::MusicClipLoader.LoadedAudioClip>();

	// Token: 0x04000968 RID: 2408
	public Dictionary<AudioClip, global::MusicClipLoader.LoadedAudioClip> loadedClipDict = new Dictionary<AudioClip, global::MusicClipLoader.LoadedAudioClip>();

	// Token: 0x04000969 RID: 2409
	public List<AudioClip> clipsToLoad = new List<AudioClip>();

	// Token: 0x0400096A RID: 2410
	public List<AudioClip> clipsToUnload = new List<AudioClip>();

	// Token: 0x020001E6 RID: 486
	public class LoadedAudioClip
	{
		// Token: 0x0400096B RID: 2411
		public AudioClip clip;

		// Token: 0x0400096C RID: 2412
		public float unloadTime;
	}
}
