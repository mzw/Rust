﻿using System;
using UnityEngine;

// Token: 0x02000220 RID: 544
public class Socket_Specific_Female : global::Socket_Base
{
	// Token: 0x06000FD1 RID: 4049 RVA: 0x000606C0 File Offset: 0x0005E8C0
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.red;
		Gizmos.DrawLine(Vector3.zero, Vector3.forward * 0.2f);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(Vector3.zero, Vector3.right * 0.1f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(Vector3.zero, Vector3.up * 0.1f);
		Gizmos.DrawIcon(base.transform.position, "light_circle_green.png", false);
	}

	// Token: 0x06000FD2 RID: 4050 RVA: 0x0006075C File Offset: 0x0005E95C
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.DrawWireCube(this.selectCenter, this.selectSize);
	}

	// Token: 0x06000FD3 RID: 4051 RVA: 0x00060780 File Offset: 0x0005E980
	public bool CanAccept(global::Socket_Specific socket)
	{
		foreach (string b in this.allowedMaleSockets)
		{
			if (socket.targetSocketName == b)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x04000A92 RID: 2706
	public int rotationDegrees;

	// Token: 0x04000A93 RID: 2707
	public int rotationOffset;

	// Token: 0x04000A94 RID: 2708
	public string[] allowedMaleSockets;
}
