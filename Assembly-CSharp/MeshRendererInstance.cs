﻿using System;
using UnityEngine;

// Token: 0x02000255 RID: 597
public struct MeshRendererInstance
{
	// Token: 0x17000116 RID: 278
	// (get) Token: 0x0600105A RID: 4186 RVA: 0x00063608 File Offset: 0x00061808
	// (set) Token: 0x0600105B RID: 4187 RVA: 0x00063618 File Offset: 0x00061818
	public Mesh mesh
	{
		get
		{
			return this.data.mesh;
		}
		set
		{
			this.data = global::MeshCache.Get(value);
		}
	}

	// Token: 0x04000B26 RID: 2854
	public Renderer renderer;

	// Token: 0x04000B27 RID: 2855
	public OBB bounds;

	// Token: 0x04000B28 RID: 2856
	public Vector3 position;

	// Token: 0x04000B29 RID: 2857
	public Quaternion rotation;

	// Token: 0x04000B2A RID: 2858
	public Vector3 scale;

	// Token: 0x04000B2B RID: 2859
	public global::MeshCache.Data data;
}
