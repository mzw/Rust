﻿using System;
using UnityEngine;

// Token: 0x020007B6 RID: 1974
public class BaseViewModel : MonoBehaviour
{
	// Token: 0x04002020 RID: 8224
	[Header("BaseViewModel")]
	public global::LazyAimProperties lazyaimRegular;

	// Token: 0x04002021 RID: 8225
	public global::LazyAimProperties lazyaimIronsights;

	// Token: 0x04002022 RID: 8226
	public Transform pivot;

	// Token: 0x04002023 RID: 8227
	public bool wantsHeldItemFlags;

	// Token: 0x04002024 RID: 8228
	public GameObject[] hideSightMeshes;

	// Token: 0x04002025 RID: 8229
	public Transform MuzzlePoint;

	// Token: 0x04002026 RID: 8230
	[Header("Skin")]
	public global::SubsurfaceProfile subsurfaceProfile;
}
