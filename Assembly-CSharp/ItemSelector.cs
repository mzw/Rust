﻿using System;
using UnityEngine;

// Token: 0x020004D3 RID: 1235
public class ItemSelector : PropertyAttribute
{
	// Token: 0x06001A9C RID: 6812 RVA: 0x00095860 File Offset: 0x00093A60
	public ItemSelector(global::ItemCategory category = global::ItemCategory.All)
	{
		this.category = category;
	}

	// Token: 0x04001571 RID: 5489
	public global::ItemCategory category = global::ItemCategory.All;
}
