﻿using System;
using UnityEngine;

// Token: 0x0200038C RID: 908
[Serializable]
public class MetabolismAttribute
{
	// Token: 0x17000187 RID: 391
	// (get) Token: 0x06001593 RID: 5523 RVA: 0x0007C18C File Offset: 0x0007A38C
	public float greatFraction
	{
		get
		{
			return Mathf.Floor(this.Fraction() / 0.1f) / 10f;
		}
	}

	// Token: 0x06001594 RID: 5524 RVA: 0x0007C1A8 File Offset: 0x0007A3A8
	public void Reset()
	{
		this.value = Mathf.Clamp(Random.Range(this.startMin, this.startMax), this.min, this.max);
	}

	// Token: 0x06001595 RID: 5525 RVA: 0x0007C1D4 File Offset: 0x0007A3D4
	public float Fraction()
	{
		return Mathf.InverseLerp(this.min, this.max, this.value);
	}

	// Token: 0x06001596 RID: 5526 RVA: 0x0007C1F0 File Offset: 0x0007A3F0
	public float InverseFraction()
	{
		return 1f - this.Fraction();
	}

	// Token: 0x06001597 RID: 5527 RVA: 0x0007C200 File Offset: 0x0007A400
	public void Add(float val)
	{
		this.value = Mathf.Clamp(this.value + val, this.min, this.max);
	}

	// Token: 0x06001598 RID: 5528 RVA: 0x0007C224 File Offset: 0x0007A424
	public void Subtract(float val)
	{
		this.value = Mathf.Clamp(this.value - val, this.min, this.max);
	}

	// Token: 0x06001599 RID: 5529 RVA: 0x0007C248 File Offset: 0x0007A448
	public void Increase(float fTarget)
	{
		fTarget = Mathf.Clamp(fTarget, this.min, this.max);
		if (fTarget <= this.value)
		{
			return;
		}
		this.value = fTarget;
	}

	// Token: 0x0600159A RID: 5530 RVA: 0x0007C274 File Offset: 0x0007A474
	public void MoveTowards(float fTarget, float fRate)
	{
		if (fRate == 0f)
		{
			return;
		}
		this.value = Mathf.Clamp(Mathf.MoveTowards(this.value, fTarget, fRate), this.min, this.max);
	}

	// Token: 0x0600159B RID: 5531 RVA: 0x0007C2A8 File Offset: 0x0007A4A8
	public bool HasChanged()
	{
		bool result = this.lastValue != this.value;
		this.lastValue = this.value;
		return result;
	}

	// Token: 0x0600159C RID: 5532 RVA: 0x0007C2D4 File Offset: 0x0007A4D4
	public bool HasGreatlyChanged()
	{
		float greatFraction = this.greatFraction;
		bool result = this.lastGreatFraction != greatFraction;
		this.lastGreatFraction = greatFraction;
		return result;
	}

	// Token: 0x0600159D RID: 5533 RVA: 0x0007C300 File Offset: 0x0007A500
	public void SetValue(float newValue)
	{
		this.value = newValue;
	}

	// Token: 0x04000FD3 RID: 4051
	public float startMin;

	// Token: 0x04000FD4 RID: 4052
	public float startMax;

	// Token: 0x04000FD5 RID: 4053
	public float min;

	// Token: 0x04000FD6 RID: 4054
	public float max;

	// Token: 0x04000FD7 RID: 4055
	public float value;

	// Token: 0x04000FD8 RID: 4056
	public float lastValue;

	// Token: 0x04000FD9 RID: 4057
	internal float lastGreatFraction;

	// Token: 0x04000FDA RID: 4058
	private const float greatInterval = 0.1f;

	// Token: 0x0200038D RID: 909
	public enum Type
	{
		// Token: 0x04000FDC RID: 4060
		Calories,
		// Token: 0x04000FDD RID: 4061
		Hydration,
		// Token: 0x04000FDE RID: 4062
		Heartrate,
		// Token: 0x04000FDF RID: 4063
		Poison,
		// Token: 0x04000FE0 RID: 4064
		Radiation,
		// Token: 0x04000FE1 RID: 4065
		Bleeding,
		// Token: 0x04000FE2 RID: 4066
		Health,
		// Token: 0x04000FE3 RID: 4067
		HealthOverTime
	}
}
