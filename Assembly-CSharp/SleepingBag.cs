﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000093 RID: 147
public class SleepingBag : global::DecayEntity
{
	// Token: 0x06000980 RID: 2432 RVA: 0x00040A40 File Offset: 0x0003EC40
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("SleepingBag.OnRpcMessage", 0.1f))
		{
			if (rpc == 4124455624u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - AssignToFriend ");
				}
				using (TimeWarning.New("AssignToFriend", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("AssignToFriend", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.AssignToFriend(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in AssignToFriend");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3360372542u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Rename ");
				}
				using (TimeWarning.New("Rename", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("Rename", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Rename(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in Rename");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 1117795193u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_MakeBed ");
				}
				using (TimeWarning.New("RPC_MakeBed", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_MakeBed", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_MakeBed(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in RPC_MakeBed");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 1770780785u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_MakePublic ");
				}
				using (TimeWarning.New("RPC_MakePublic", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_MakePublic", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_MakePublic(msg5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in RPC_MakePublic");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000981 RID: 2433 RVA: 0x000410C8 File Offset: 0x0003F2C8
	public bool IsPublic()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved3);
	}

	// Token: 0x17000066 RID: 102
	// (get) Token: 0x06000982 RID: 2434 RVA: 0x000410D8 File Offset: 0x0003F2D8
	public float unlockSeconds
	{
		get
		{
			if (this.unlockTime < UnityEngine.Time.realtimeSinceStartup)
			{
				return 0f;
			}
			return this.unlockTime - UnityEngine.Time.realtimeSinceStartup;
		}
	}

	// Token: 0x06000983 RID: 2435 RVA: 0x000410FC File Offset: 0x0003F2FC
	public static global::SleepingBag[] FindForPlayer(ulong playerID, bool ignoreTimers)
	{
		return (from x in global::SleepingBag.sleepingBags
		where x.deployerUserID == playerID && (ignoreTimers || x.unlockTime < UnityEngine.Time.realtimeSinceStartup)
		select x).ToArray<global::SleepingBag>();
	}

	// Token: 0x06000984 RID: 2436 RVA: 0x00041138 File Offset: 0x0003F338
	public static global::SleepingBag FindForPlayer(ulong playerID, uint sleepingBagID, bool ignoreTimers)
	{
		return global::SleepingBag.sleepingBags.FirstOrDefault((global::SleepingBag x) => x.deployerUserID == playerID && x.net.ID == sleepingBagID && (ignoreTimers || x.unlockTime < UnityEngine.Time.realtimeSinceStartup));
	}

	// Token: 0x06000985 RID: 2437 RVA: 0x00041178 File Offset: 0x0003F378
	public static bool SpawnPlayer(global::BasePlayer player, uint sleepingBag)
	{
		global::SleepingBag[] array = global::SleepingBag.FindForPlayer(player.userID, true);
		global::SleepingBag sleepingBag2 = array.FirstOrDefault((global::SleepingBag x) => x.deployerUserID == player.userID && x.net.ID == sleepingBag && x.unlockTime < UnityEngine.Time.realtimeSinceStartup);
		if (sleepingBag2 == null)
		{
			return false;
		}
		Vector3 vector = sleepingBag2.transform.position + sleepingBag2.spawnOffset;
		Quaternion rotation = Quaternion.Euler(0f, sleepingBag2.transform.rotation.eulerAngles.y, 0f);
		player.RespawnAt(vector, rotation);
		foreach (global::SleepingBag sleepingBag3 in array)
		{
			if (Vector3.Distance(vector, sleepingBag3.transform.position) <= ConVar.Server.respawnresetrange)
			{
				sleepingBag3.unlockTime = UnityEngine.Time.realtimeSinceStartup + sleepingBag3.secondsBetweenReuses;
			}
		}
		return true;
	}

	// Token: 0x06000986 RID: 2438 RVA: 0x00041274 File Offset: 0x0003F474
	public static bool DestroyBag(global::BasePlayer player, uint sleepingBag)
	{
		global::SleepingBag sleepingBag2 = global::SleepingBag.FindForPlayer(player.userID, sleepingBag, false);
		if (sleepingBag2 == null)
		{
			return false;
		}
		if (sleepingBag2.canBePublic)
		{
			sleepingBag2.SetPublic(true);
			sleepingBag2.deployerUserID = 0UL;
		}
		else
		{
			sleepingBag2.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		player.SendRespawnOptions();
		return true;
	}

	// Token: 0x06000987 RID: 2439 RVA: 0x000412CC File Offset: 0x0003F4CC
	public void SetPublic(bool isPublic)
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved3, isPublic, false);
	}

	// Token: 0x06000988 RID: 2440 RVA: 0x000412DC File Offset: 0x0003F4DC
	private void SetDeployedBy(global::BasePlayer player)
	{
		if (player == null)
		{
			return;
		}
		this.deployerUserID = player.userID;
		float realtimeSinceStartup = UnityEngine.Time.realtimeSinceStartup;
		global::SleepingBag[] array = (from x in global::SleepingBag.sleepingBags
		where x.deployerUserID == player.userID && x.unlockTime > UnityEngine.Time.realtimeSinceStartup
		select x).ToArray<global::SleepingBag>();
		foreach (global::SleepingBag sleepingBag in array)
		{
			if (sleepingBag.unlockTime > realtimeSinceStartup && Vector3.Distance(sleepingBag.transform.position, base.transform.position) <= ConVar.Server.respawnresetrange)
			{
				realtimeSinceStartup = sleepingBag.unlockTime;
			}
		}
		this.unlockTime = Mathf.Max(realtimeSinceStartup, UnityEngine.Time.realtimeSinceStartup + this.secondsBetweenReuses);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000989 RID: 2441 RVA: 0x000413B4 File Offset: 0x0003F5B4
	public override void ServerInit()
	{
		base.ServerInit();
		if (!global::SleepingBag.sleepingBags.Contains(this))
		{
			global::SleepingBag.sleepingBags.Add(this);
		}
	}

	// Token: 0x0600098A RID: 2442 RVA: 0x000413D8 File Offset: 0x0003F5D8
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		global::SleepingBag.sleepingBags.RemoveAll((global::SleepingBag x) => x == this);
	}

	// Token: 0x0600098B RID: 2443 RVA: 0x000413F8 File Offset: 0x0003F5F8
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.sleepingBag = Facepunch.Pool.Get<ProtoBuf.SleepingBag>();
		info.msg.sleepingBag.name = this.niceName;
		info.msg.sleepingBag.deployerID = this.deployerUserID;
	}

	// Token: 0x0600098C RID: 2444 RVA: 0x0004144C File Offset: 0x0003F64C
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void Rename(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		string text = msg.read.String();
		text = global::WordFilter.Filter(text);
		if (string.IsNullOrEmpty(text))
		{
			text = "Unnamed Sleeping Bag";
		}
		if (text.Length > 24)
		{
			text = text.Substring(0, 22) + "..";
		}
		this.niceName = text;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600098D RID: 2445 RVA: 0x000414C0 File Offset: 0x0003F6C0
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void AssignToFriend(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (this.deployerUserID != msg.player.userID)
		{
			return;
		}
		ulong num = msg.read.UInt64();
		if (num == 0UL)
		{
			return;
		}
		if (Interface.CallHook("CanAssignBed", new object[]
		{
			this,
			msg.player,
			num
		}) != null)
		{
			return;
		}
		this.deployerUserID = num;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600098E RID: 2446 RVA: 0x00041548 File Offset: 0x0003F748
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_MakePublic(global::BaseEntity.RPCMessage msg)
	{
		if (!this.canBePublic)
		{
			return;
		}
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (this.deployerUserID != msg.player.userID && !msg.player.CanBuild())
		{
			return;
		}
		bool flag = msg.read.Bit();
		if (flag == this.IsPublic())
		{
			return;
		}
		if (Interface.CallHook("CanSetBedPublic", new object[]
		{
			this,
			msg.player
		}) != null)
		{
			return;
		}
		this.SetPublic(flag);
		if (!this.IsPublic())
		{
			this.deployerUserID = msg.player.userID;
		}
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600098F RID: 2447 RVA: 0x00041604 File Offset: 0x0003F804
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_MakeBed(global::BaseEntity.RPCMessage msg)
	{
		if (!this.canBePublic || !this.IsPublic())
		{
			return;
		}
		if (!msg.player.CanInteract())
		{
			return;
		}
		this.deployerUserID = msg.player.userID;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000990 RID: 2448 RVA: 0x00041654 File Offset: 0x0003F854
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.sleepingBag != null)
		{
			this.niceName = info.msg.sleepingBag.name;
			this.deployerUserID = info.msg.sleepingBag.deployerID;
		}
	}

	// Token: 0x06000991 RID: 2449 RVA: 0x000416A8 File Offset: 0x0003F8A8
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.CanPickup(player) && player.userID == this.deployerUserID;
	}

	// Token: 0x06000992 RID: 2450 RVA: 0x000416C8 File Offset: 0x0003F8C8
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x0400045D RID: 1117
	[NonSerialized]
	public ulong deployerUserID;

	// Token: 0x0400045E RID: 1118
	public GameObject renameDialog;

	// Token: 0x0400045F RID: 1119
	public GameObject assignDialog;

	// Token: 0x04000460 RID: 1120
	public float secondsBetweenReuses = 300f;

	// Token: 0x04000461 RID: 1121
	public string niceName = "Unnamed Bag";

	// Token: 0x04000462 RID: 1122
	public Vector3 spawnOffset = Vector3.zero;

	// Token: 0x04000463 RID: 1123
	public bool canBePublic;

	// Token: 0x04000464 RID: 1124
	internal float unlockTime;

	// Token: 0x04000465 RID: 1125
	private static List<global::SleepingBag> sleepingBags = new List<global::SleepingBag>();
}
