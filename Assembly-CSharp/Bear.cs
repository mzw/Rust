﻿using System;

// Token: 0x020000DE RID: 222
public class Bear : global::BaseAnimalNPC
{
	// Token: 0x1700006F RID: 111
	// (get) Token: 0x06000B1C RID: 2844 RVA: 0x0004A9D4 File Offset: 0x00048BD4
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000B1D RID: 2845 RVA: 0x0004A9D8 File Offset: 0x00048BD8
	public override bool WantsToEat(global::BaseEntity best)
	{
		return !best.HasTrait(global::BaseEntity.TraitFlag.Alive) && base.WantsToEat(best);
	}

	// Token: 0x06000B1E RID: 2846 RVA: 0x0004A9F0 File Offset: 0x00048BF0
	public override string Categorize()
	{
		return "Bear";
	}

	// Token: 0x040005E7 RID: 1511
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population = 2f;
}
