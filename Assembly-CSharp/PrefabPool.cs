﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020007B0 RID: 1968
public class PrefabPool
{
	// Token: 0x17000295 RID: 661
	// (get) Token: 0x0600249E RID: 9374 RVA: 0x000C9CB0 File Offset: 0x000C7EB0
	public int Count
	{
		get
		{
			return this.stack.Count;
		}
	}

	// Token: 0x0600249F RID: 9375 RVA: 0x000C9CC0 File Offset: 0x000C7EC0
	public void Push(global::PrefabInfo info)
	{
		this.stack.Push(info);
		info.EnterPool();
	}

	// Token: 0x060024A0 RID: 9376 RVA: 0x000C9CD4 File Offset: 0x000C7ED4
	public void Push(GameObject instance)
	{
		global::PrefabInfo component = instance.GetComponent<global::PrefabInfo>();
		this.Push(component);
	}

	// Token: 0x060024A1 RID: 9377 RVA: 0x000C9CF0 File Offset: 0x000C7EF0
	public GameObject Pop(Vector3 pos = default(Vector3), Quaternion rot = default(Quaternion))
	{
		while (this.stack.Count > 0)
		{
			global::PrefabInfo prefabInfo = this.stack.Pop();
			if (prefabInfo)
			{
				prefabInfo.transform.position = pos;
				prefabInfo.transform.rotation = rot;
				prefabInfo.LeavePool();
				return prefabInfo.gameObject;
			}
		}
		return null;
	}

	// Token: 0x060024A2 RID: 9378 RVA: 0x000C9D50 File Offset: 0x000C7F50
	public void Clear()
	{
		foreach (global::PrefabInfo prefabInfo in this.stack)
		{
			if (prefabInfo)
			{
				Object.Destroy(prefabInfo);
			}
		}
		this.stack.Clear();
	}

	// Token: 0x04002018 RID: 8216
	public Stack<global::PrefabInfo> stack = new Stack<global::PrefabInfo>();
}
