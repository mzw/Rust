﻿using System;
using UnityEngine;

// Token: 0x020003B7 RID: 951
public class FruitScale : MonoBehaviour, IClientComponent
{
	// Token: 0x06001686 RID: 5766 RVA: 0x0008235C File Offset: 0x0008055C
	public void SetProgress(float progress)
	{
		base.transform.localScale = Vector3.one * progress;
	}
}
