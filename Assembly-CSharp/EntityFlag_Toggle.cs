﻿using System;
using UnityEngine;
using UnityEngine.Events;

// Token: 0x0200036C RID: 876
public class EntityFlag_Toggle : global::EntityComponent<global::BaseEntity>, global::IOnPostNetworkUpdate, global::IOnSendNetworkUpdate, global::IPrefabPreProcess
{
	// Token: 0x060014ED RID: 5357 RVA: 0x000791F4 File Offset: 0x000773F4
	public void DoUpdate(global::BaseEntity entity)
	{
		bool flag = entity.HasFlag(this.flag);
		if (this.hasRunOnce && flag == this.lastHasFlag)
		{
			return;
		}
		this.hasRunOnce = true;
		this.lastHasFlag = flag;
		if (flag)
		{
			this.onFlagEnabled.Invoke();
		}
		else
		{
			this.onFlagDisabled.Invoke();
		}
	}

	// Token: 0x060014EE RID: 5358 RVA: 0x00079258 File Offset: 0x00077458
	public void OnPostNetworkUpdate(global::BaseEntity entity)
	{
		if (!this.runClientside)
		{
			return;
		}
		this.DoUpdate(entity);
	}

	// Token: 0x060014EF RID: 5359 RVA: 0x00079270 File Offset: 0x00077470
	public void OnSendNetworkUpdate(global::BaseEntity entity)
	{
		if (!this.runServerside)
		{
			return;
		}
		this.DoUpdate(entity);
	}

	// Token: 0x060014F0 RID: 5360 RVA: 0x00079288 File Offset: 0x00077488
	public void PreProcess(global::IPrefabProcessor process, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		if ((!clientside || !this.runClientside) && (!serverside || !this.runServerside))
		{
			process.RemoveComponent(this);
		}
	}

	// Token: 0x04000F5B RID: 3931
	public bool runClientside = true;

	// Token: 0x04000F5C RID: 3932
	public bool runServerside = true;

	// Token: 0x04000F5D RID: 3933
	public global::BaseEntity.Flags flag;

	// Token: 0x04000F5E RID: 3934
	[SerializeField]
	private UnityEvent onFlagEnabled = new UnityEvent();

	// Token: 0x04000F5F RID: 3935
	[SerializeField]
	private UnityEvent onFlagDisabled = new UnityEvent();

	// Token: 0x04000F60 RID: 3936
	internal bool hasRunOnce;

	// Token: 0x04000F61 RID: 3937
	internal bool lastHasFlag;
}
