﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using ProtoBuf;
using UnityEngine;

// Token: 0x02000099 RID: 153
public class StagedResourceEntity : global::ResourceEntity
{
	// Token: 0x060009A8 RID: 2472 RVA: 0x00041DC8 File Offset: 0x0003FFC8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("StagedResourceEntity.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009A9 RID: 2473 RVA: 0x00041E10 File Offset: 0x00040010
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.resource == null)
		{
			return;
		}
		int num = info.msg.resource.stage;
		bool flag = info.fromDisk && base.isServer;
		if (flag)
		{
			this.health = this.startHealth;
			num = 0;
		}
		if (num != this.stage)
		{
			this.stage = num;
			this.UpdateStage();
		}
	}

	// Token: 0x060009AA RID: 2474 RVA: 0x00041E8C File Offset: 0x0004008C
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (info.msg.resource == null)
		{
			info.msg.resource = Pool.Get<BaseResource>();
		}
		info.msg.resource.health = this.Health();
		info.msg.resource.stage = this.stage;
	}

	// Token: 0x060009AB RID: 2475 RVA: 0x00041EF0 File Offset: 0x000400F0
	protected override void OnHealthChanged()
	{
		base.Invoke(new Action(this.UpdateNetworkStage), 0.1f);
	}

	// Token: 0x060009AC RID: 2476 RVA: 0x00041F0C File Offset: 0x0004010C
	protected virtual void UpdateNetworkStage()
	{
		int num = this.FindBestStage();
		if (num != this.stage)
		{
			this.stage = this.FindBestStage();
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			this.UpdateStage();
		}
	}

	// Token: 0x060009AD RID: 2477 RVA: 0x00041F48 File Offset: 0x00040148
	private int FindBestStage()
	{
		float num = Mathf.InverseLerp(0f, this.MaxHealth(), this.Health());
		for (int i = 0; i < this.stages.Count; i++)
		{
			if (num >= this.stages[i].health)
			{
				return i;
			}
		}
		return this.stages.Count - 1;
	}

	// Token: 0x060009AE RID: 2478 RVA: 0x00041FB0 File Offset: 0x000401B0
	public T GetStageComponent<T>() where T : Component
	{
		return this.stages[this.stage].instance.GetComponentInChildren<T>();
	}

	// Token: 0x060009AF RID: 2479 RVA: 0x00041FD0 File Offset: 0x000401D0
	private void UpdateStage()
	{
		if (this.stages.Count == 0)
		{
			return;
		}
		for (int i = 0; i < this.stages.Count; i++)
		{
			this.stages[i].instance.SetActive(i == this.stage);
		}
		global::GroundWatch.PhysicsChanged(base.gameObject);
	}

	// Token: 0x04000478 RID: 1144
	public List<global::StagedResourceEntity.ResourceStage> stages = new List<global::StagedResourceEntity.ResourceStage>();

	// Token: 0x04000479 RID: 1145
	public int stage;

	// Token: 0x0400047A RID: 1146
	public global::GameObjectRef changeStageEffect;

	// Token: 0x0400047B RID: 1147
	public GameObject gibSourceTest;

	// Token: 0x0200009A RID: 154
	[Serializable]
	public class ResourceStage
	{
		// Token: 0x0400047C RID: 1148
		public float health;

		// Token: 0x0400047D RID: 1149
		public GameObject instance;
	}
}
