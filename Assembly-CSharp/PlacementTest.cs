﻿using System;
using UnityEngine;

// Token: 0x020000BB RID: 187
public class PlacementTest : MonoBehaviour
{
	// Token: 0x06000A8F RID: 2703 RVA: 0x00048398 File Offset: 0x00046598
	public Vector3 RandomHemisphereDirection(Vector3 input, float degreesOffset)
	{
		degreesOffset = Mathf.Clamp(degreesOffset / 180f, -180f, 180f);
		Vector2 insideUnitCircle = Random.insideUnitCircle;
		Vector3 vector;
		vector..ctor(insideUnitCircle.x * degreesOffset, Random.Range(-1f, 1f) * degreesOffset, insideUnitCircle.y * degreesOffset);
		return (input + vector).normalized;
	}

	// Token: 0x06000A90 RID: 2704 RVA: 0x000483FC File Offset: 0x000465FC
	public Vector3 RandomCylinderPointAroundVector(Vector3 input, float distance, float minHeight = 0f, float maxHeight = 0f)
	{
		Vector2 insideUnitCircle = Random.insideUnitCircle;
		Vector3 vector;
		vector..ctor(insideUnitCircle.x, 0f, insideUnitCircle.y);
		Vector3 normalized = vector.normalized;
		return new Vector3(normalized.x * distance, Random.Range(minHeight, maxHeight), normalized.z * distance);
	}

	// Token: 0x06000A91 RID: 2705 RVA: 0x00048450 File Offset: 0x00046650
	public Vector3 ClampToHemisphere(Vector3 hemiInput, float degreesOffset, Vector3 inputVec)
	{
		degreesOffset = Mathf.Clamp(degreesOffset / 180f, -180f, 180f);
		Vector3 normalized = (hemiInput + Vector3.one * degreesOffset).normalized;
		Vector3 normalized2 = (hemiInput + Vector3.one * -degreesOffset).normalized;
		for (int i = 0; i < 3; i++)
		{
			inputVec[i] = Mathf.Clamp(inputVec[i], normalized2[i], normalized[i]);
		}
		return inputVec.normalized;
	}

	// Token: 0x06000A92 RID: 2706 RVA: 0x000484F0 File Offset: 0x000466F0
	private void Update()
	{
		if (Time.realtimeSinceStartup < this.nextTest)
		{
			return;
		}
		this.nextTest = Time.realtimeSinceStartup;
		Vector3 vector = this.RandomCylinderPointAroundVector(Vector3.up, 0.5f, 0.25f, 0.5f);
		vector = base.transform.TransformPoint(vector);
		this.testTransform.transform.position = vector;
		if (this.testTransform != null && this.visualTest != null)
		{
			Vector3 position = base.transform.position;
			RaycastHit raycastHit;
			if (this.myMeshCollider.Raycast(new Ray(this.testTransform.position, (base.transform.position - this.testTransform.position).normalized), ref raycastHit, 5f))
			{
				position = raycastHit.point;
			}
			else
			{
				Debug.LogError("Missed");
			}
			this.visualTest.transform.position = position;
		}
	}

	// Token: 0x06000A93 RID: 2707 RVA: 0x000485F4 File Offset: 0x000467F4
	public void OnDrawGizmos()
	{
	}

	// Token: 0x0400053E RID: 1342
	public MeshCollider myMeshCollider;

	// Token: 0x0400053F RID: 1343
	public Transform testTransform;

	// Token: 0x04000540 RID: 1344
	public Transform visualTest;

	// Token: 0x04000541 RID: 1345
	public float hemisphere = 45f;

	// Token: 0x04000542 RID: 1346
	public float clampTest = 45f;

	// Token: 0x04000543 RID: 1347
	public float testDist = 2f;

	// Token: 0x04000544 RID: 1348
	private float nextTest;
}
