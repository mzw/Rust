﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000092 RID: 146
public class Signage : global::BaseCombatEntity, global::ILOD
{
	// Token: 0x06000973 RID: 2419 RVA: 0x00040214 File Offset: 0x0003E414
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Signage.OnRpcMessage", 0.1f))
		{
			if (rpc == 1910248808u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - LockSign ");
				}
				using (TimeWarning.New("LockSign", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("LockSign", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.LockSign(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in LockSign");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3436477185u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - UnLockSign ");
				}
				using (TimeWarning.New("UnLockSign", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("UnLockSign", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.UnLockSign(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in UnLockSign");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 3999959558u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - UpdateSign ");
				}
				using (TimeWarning.New("UpdateSign", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("UpdateSign", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.UpdateSign(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in UpdateSign");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000974 RID: 2420 RVA: 0x00040710 File Offset: 0x0003E910
	public bool CanUpdateSign(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanUpdateSign", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return player.IsAdmin || player.IsDeveloper || (player.CanBuild() && (!base.IsLocked() || player.userID == base.OwnerID));
	}

	// Token: 0x06000975 RID: 2421 RVA: 0x00040788 File Offset: 0x0003E988
	public bool CanUnlockSign(global::BasePlayer player)
	{
		return base.IsLocked() && this.CanUpdateSign(player);
	}

	// Token: 0x06000976 RID: 2422 RVA: 0x000407A0 File Offset: 0x0003E9A0
	public bool CanLockSign(global::BasePlayer player)
	{
		return !base.IsLocked() && this.CanUpdateSign(player);
	}

	// Token: 0x06000977 RID: 2423 RVA: 0x000407B8 File Offset: 0x0003E9B8
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.sign != null && info.msg.sign.imageid != this.textureID)
		{
			this.textureID = info.msg.sign.imageid;
		}
		if (base.isServer)
		{
			if (this.textureID != 0u && global::FileStorage.server.Get(this.textureID, global::FileStorage.Type.png, this.net.ID) == null)
			{
				this.textureID = 0u;
			}
			if (this.textureID == 0u)
			{
				base.SetFlag(global::BaseEntity.Flags.Locked, false, false);
			}
		}
	}

	// Token: 0x06000978 RID: 2424 RVA: 0x00040864 File Offset: 0x0003EA64
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void LockSign(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (!this.CanUpdateSign(msg.player))
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Locked, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		base.OwnerID = msg.player.userID;
		Interface.CallHook("OnSignLocked", new object[]
		{
			this,
			msg.player
		});
	}

	// Token: 0x06000979 RID: 2425 RVA: 0x000408DC File Offset: 0x0003EADC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void UnLockSign(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (!this.CanUnlockSign(msg.player))
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Locked, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600097A RID: 2426 RVA: 0x00040914 File Offset: 0x0003EB14
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.sign = Facepunch.Pool.Get<Sign>();
		info.msg.sign.imageid = this.textureID;
	}

	// Token: 0x0600097B RID: 2427 RVA: 0x00040948 File Offset: 0x0003EB48
	public override void OnKilled(global::HitInfo info)
	{
		if (this.net != null)
		{
			global::FileStorage.server.RemoveAllByEntity(this.net.ID);
		}
		this.textureID = 0u;
		base.OnKilled(info);
	}

	// Token: 0x0600097C RID: 2428 RVA: 0x00040978 File Offset: 0x0003EB78
	public override bool ShouldNetworkOwnerInfo()
	{
		return true;
	}

	// Token: 0x0600097D RID: 2429 RVA: 0x0004097C File Offset: 0x0003EB7C
	public override string Categorize()
	{
		return "sign";
	}

	// Token: 0x0600097E RID: 2430 RVA: 0x00040984 File Offset: 0x0003EB84
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void UpdateSign(global::BaseEntity.RPCMessage msg)
	{
		if (!this.CanUpdateSign(msg.player))
		{
			return;
		}
		byte[] array = msg.read.BytesWithSize();
		if (array == null)
		{
			return;
		}
		global::FileStorage.server.RemoveAllByEntity(this.net.ID);
		this.textureID = global::FileStorage.server.Store(array, global::FileStorage.Type.png, this.net.ID, 0u);
		Interface.CallHook("OnSignUpdated", new object[]
		{
			this,
			msg.player
		});
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0400045A RID: 1114
	public GameObject changeTextDialog;

	// Token: 0x0400045B RID: 1115
	public global::MeshPaintableSource paintableSource;

	// Token: 0x0400045C RID: 1116
	[NonSerialized]
	public uint textureID;
}
