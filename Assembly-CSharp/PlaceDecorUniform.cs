﻿using System;
using UnityEngine;

// Token: 0x020005C1 RID: 1473
public class PlaceDecorUniform : global::ProceduralComponent
{
	// Token: 0x06001E90 RID: 7824 RVA: 0x000AB558 File Offset: 0x000A9758
	public override void Process(uint seed)
	{
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + this.ResourceFolder, null, null, true);
		if (array == null || array.Length == 0)
		{
			return;
		}
		Vector3 position = global::TerrainMeta.Position;
		Vector3 size = global::TerrainMeta.Size;
		float x = position.x;
		float z = position.z;
		float num = position.x + size.x;
		float num2 = position.z + size.z;
		for (float num3 = z; num3 < num2; num3 += this.ObjectDistance)
		{
			for (float num4 = x; num4 < num; num4 += this.ObjectDistance)
			{
				float num5 = num4 + SeedRandom.Range(ref seed, -this.ObjectDithering, this.ObjectDithering);
				float num6 = num3 + SeedRandom.Range(ref seed, -this.ObjectDithering, this.ObjectDithering);
				float normX = global::TerrainMeta.NormalizeX(num5);
				float normZ = global::TerrainMeta.NormalizeZ(num6);
				float num7 = SeedRandom.Value(ref seed);
				float factor = this.Filter.GetFactor(normX, normZ);
				global::Prefab random = array.GetRandom(ref seed);
				if (factor * factor >= num7)
				{
					float height = heightMap.GetHeight(normX, normZ);
					Vector3 vector;
					vector..ctor(num5, height, num6);
					Quaternion localRotation = random.Object.transform.localRotation;
					Vector3 localScale = random.Object.transform.localScale;
					random.ApplyDecorComponents(ref vector, ref localRotation, ref localScale);
					if (random.ApplyTerrainAnchors(ref vector, localRotation, localScale, this.Filter))
					{
						if (random.ApplyTerrainChecks(vector, localRotation, localScale, this.Filter))
						{
							if (random.ApplyTerrainFilters(vector, localRotation, localScale, null))
							{
								random.ApplyTerrainModifiers(vector, localRotation, localScale);
								global::World.Serialization.AddPrefab("Decor", random.ID, vector, localRotation, localScale);
							}
						}
					}
				}
			}
		}
	}

	// Token: 0x04001958 RID: 6488
	public global::SpawnFilter Filter;

	// Token: 0x04001959 RID: 6489
	public string ResourceFolder = string.Empty;

	// Token: 0x0400195A RID: 6490
	public float ObjectDistance = 10f;

	// Token: 0x0400195B RID: 6491
	public float ObjectDithering = 5f;
}
