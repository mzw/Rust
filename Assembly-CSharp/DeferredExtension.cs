﻿using System;
using UnityEngine;

// Token: 0x020005FD RID: 1533
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(global::CommandBufferManager))]
public class DeferredExtension : MonoBehaviour
{
	// Token: 0x04001A42 RID: 6722
	public global::ExtendGBufferParams extendGBuffer = global::ExtendGBufferParams.Default;

	// Token: 0x04001A43 RID: 6723
	public global::SubsurfaceScatteringParams subsurfaceScattering = global::SubsurfaceScatteringParams.Default;

	// Token: 0x04001A44 RID: 6724
	public Texture2D blueNoise;

	// Token: 0x04001A45 RID: 6725
	public float depthScale = 100f;

	// Token: 0x04001A46 RID: 6726
	public bool debug;
}
