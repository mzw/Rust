﻿using System;
using Kino;
using Smaa;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.ImageEffects;

// Token: 0x0200026C RID: 620
[ExecuteInEditMode]
public class MainCamera : SingletonComponent<global::MainCamera>
{
	// Token: 0x17000117 RID: 279
	// (get) Token: 0x06001089 RID: 4233 RVA: 0x00063F80 File Offset: 0x00062180
	public static bool isValid
	{
		get
		{
			return global::MainCamera.mainCamera != null && global::MainCamera.mainCamera.enabled;
		}
	}

	// Token: 0x17000118 RID: 280
	// (get) Token: 0x0600108A RID: 4234 RVA: 0x00063FA0 File Offset: 0x000621A0
	// (set) Token: 0x0600108B RID: 4235 RVA: 0x00063FB4 File Offset: 0x000621B4
	public static Vector3 position
	{
		get
		{
			return global::MainCamera.mainCamera.transform.position;
		}
		set
		{
			global::MainCamera.mainCamera.transform.position = value;
		}
	}

	// Token: 0x17000119 RID: 281
	// (get) Token: 0x0600108C RID: 4236 RVA: 0x00063FC8 File Offset: 0x000621C8
	// (set) Token: 0x0600108D RID: 4237 RVA: 0x00063FDC File Offset: 0x000621DC
	public static Vector3 forward
	{
		get
		{
			return global::MainCamera.mainCamera.transform.forward;
		}
		set
		{
			if (value.sqrMagnitude > 0f)
			{
				global::MainCamera.mainCamera.transform.forward = value;
			}
		}
	}

	// Token: 0x1700011A RID: 282
	// (get) Token: 0x0600108E RID: 4238 RVA: 0x00064000 File Offset: 0x00062200
	// (set) Token: 0x0600108F RID: 4239 RVA: 0x00064014 File Offset: 0x00062214
	public static Vector3 right
	{
		get
		{
			return global::MainCamera.mainCamera.transform.right;
		}
		set
		{
			if (value.sqrMagnitude > 0f)
			{
				global::MainCamera.mainCamera.transform.right = value;
			}
		}
	}

	// Token: 0x1700011B RID: 283
	// (get) Token: 0x06001090 RID: 4240 RVA: 0x00064038 File Offset: 0x00062238
	// (set) Token: 0x06001091 RID: 4241 RVA: 0x0006404C File Offset: 0x0006224C
	public static Vector3 up
	{
		get
		{
			return global::MainCamera.mainCamera.transform.up;
		}
		set
		{
			if (value.sqrMagnitude > 0f)
			{
				global::MainCamera.mainCamera.transform.up = value;
			}
		}
	}

	// Token: 0x1700011C RID: 284
	// (get) Token: 0x06001092 RID: 4242 RVA: 0x00064070 File Offset: 0x00062270
	public static Quaternion rotation
	{
		get
		{
			return global::MainCamera.mainCamera.transform.rotation;
		}
	}

	// Token: 0x1700011D RID: 285
	// (get) Token: 0x06001093 RID: 4243 RVA: 0x00064084 File Offset: 0x00062284
	public static Ray Ray
	{
		get
		{
			return new Ray(global::MainCamera.position, global::MainCamera.forward);
		}
	}

	// Token: 0x1700011E RID: 286
	// (get) Token: 0x06001094 RID: 4244 RVA: 0x00064098 File Offset: 0x00062298
	public static RaycastHit Raycast
	{
		get
		{
			RaycastHit result;
			Physics.Raycast(global::MainCamera.Ray, ref result, 1024f, 94464769);
			return result;
		}
	}

	// Token: 0x04000B65 RID: 2917
	public static Camera mainCamera;

	// Token: 0x04000B66 RID: 2918
	public DepthOfField dof;

	// Token: 0x04000B67 RID: 2919
	public global::AmplifyOcclusionEffect ssao;

	// Token: 0x04000B68 RID: 2920
	public Motion motionBlur;

	// Token: 0x04000B69 RID: 2921
	public TOD_Rays shafts;

	// Token: 0x04000B6A RID: 2922
	public UnityStandardAssets.CinematicEffects.TonemappingColorGrading tonemappingColorGrading;

	// Token: 0x04000B6B RID: 2923
	public global::FXAA fxaa;

	// Token: 0x04000B6C RID: 2924
	public Smaa.SMAA smaa;

	// Token: 0x04000B6D RID: 2925
	public PostProcessLayer post;

	// Token: 0x04000B6E RID: 2926
	public CC_SharpenAndVignette sharpenAndVignette;

	// Token: 0x04000B6F RID: 2927
	public global::SEScreenSpaceShadows contactShadows;

	// Token: 0x04000B70 RID: 2928
	public global::VisualizeTexelDensity visualizeTexelDensity;

	// Token: 0x04000B71 RID: 2929
	public global::EnvironmentVolumePropertiesCollection environmentVolumeProperties;
}
