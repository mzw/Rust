﻿using System;
using Apex.LoadBalancing;

// Token: 0x02000178 RID: 376
public sealed class AnimalSensesLoadBalancer : Apex.LoadBalancing.LoadBalancer
{
	// Token: 0x06000D4B RID: 3403 RVA: 0x0005428C File Offset: 0x0005248C
	private AnimalSensesLoadBalancer()
	{
	}

	// Token: 0x04000750 RID: 1872
	public static readonly ILoadBalancer animalSensesLoadBalancer = new LoadBalancedQueue(300, 0.1f, 50, 4);
}
