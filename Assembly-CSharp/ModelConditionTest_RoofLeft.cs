﻿using System;
using UnityEngine;

// Token: 0x02000219 RID: 537
public class ModelConditionTest_RoofLeft : global::ModelConditionTest
{
	// Token: 0x06000FB2 RID: 4018 RVA: 0x0005FDF0 File Offset: 0x0005DFF0
	protected void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.gray;
		Gizmos.DrawWireCube(new Vector3(3f, 1.5f, 0f), new Vector3(3f, 3f, 3f));
	}

	// Token: 0x06000FB3 RID: 4019 RVA: 0x0005FE44 File Offset: 0x0005E044
	public override bool DoTest(global::BaseEntity ent)
	{
		global::EntityLink entityLink = ent.FindLink("roof/sockets/neighbour/2");
		return entityLink != null && entityLink.IsEmpty();
	}

	// Token: 0x04000A7F RID: 2687
	private const string socket = "roof/sockets/neighbour/2";
}
