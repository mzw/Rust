﻿using System;

// Token: 0x020000DF RID: 223
public class Boar : global::BaseAnimalNPC
{
	// Token: 0x17000070 RID: 112
	// (get) Token: 0x06000B21 RID: 2849 RVA: 0x0004AA0C File Offset: 0x00048C0C
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000B22 RID: 2850 RVA: 0x0004AA10 File Offset: 0x00048C10
	public override bool WantsToEat(global::BaseEntity best)
	{
		if (best.HasTrait(global::BaseEntity.TraitFlag.Alive))
		{
			return false;
		}
		if (best.HasTrait(global::BaseEntity.TraitFlag.Meat))
		{
			return false;
		}
		global::CollectibleEntity collectibleEntity = best as global::CollectibleEntity;
		if (collectibleEntity != null)
		{
			foreach (global::ItemAmount itemAmount in collectibleEntity.itemList)
			{
				if (itemAmount.itemDef.category == global::ItemCategory.Food)
				{
					return true;
				}
			}
		}
		return base.WantsToEat(best);
	}

	// Token: 0x06000B23 RID: 2851 RVA: 0x0004AA88 File Offset: 0x00048C88
	public override string Categorize()
	{
		return "Boar";
	}

	// Token: 0x040005E8 RID: 1512
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population = 5f;
}
