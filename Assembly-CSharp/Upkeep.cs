﻿using System;

// Token: 0x02000383 RID: 899
public class Upkeep : global::PrefabAttribute
{
	// Token: 0x06001559 RID: 5465 RVA: 0x0007B21C File Offset: 0x0007941C
	protected override Type GetIndexedType()
	{
		return typeof(global::Upkeep);
	}

	// Token: 0x04000FB1 RID: 4017
	public float upkeepMultiplier = 1f;
}
