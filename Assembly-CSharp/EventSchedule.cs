﻿using System;
using ConVar;
using Rust;
using UnityEngine;

// Token: 0x020003EF RID: 1007
public class EventSchedule : global::BaseMonoBehaviour
{
	// Token: 0x06001757 RID: 5975 RVA: 0x00086048 File Offset: 0x00084248
	private void OnEnable()
	{
		this.hoursRemaining = Random.Range(this.minimumHoursBetween, this.maxmumHoursBetween);
		base.InvokeRepeating(new Action(this.RunSchedule), 1f, 1f);
	}

	// Token: 0x06001758 RID: 5976 RVA: 0x00086080 File Offset: 0x00084280
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		base.CancelInvoke(new Action(this.RunSchedule));
	}

	// Token: 0x06001759 RID: 5977 RVA: 0x000860A0 File Offset: 0x000842A0
	private void RunSchedule()
	{
		if (Application.isLoading)
		{
			return;
		}
		if (!ConVar.Server.events)
		{
			return;
		}
		this.CountHours();
		if (this.hoursRemaining > 0f)
		{
			return;
		}
		this.Trigger();
	}

	// Token: 0x0600175A RID: 5978 RVA: 0x000860D8 File Offset: 0x000842D8
	private void Trigger()
	{
		this.hoursRemaining = Random.Range(this.minimumHoursBetween, this.maxmumHoursBetween);
		global::TriggeredEvent[] components = base.GetComponents<global::TriggeredEvent>();
		if (components.Length == 0)
		{
			return;
		}
		global::TriggeredEvent triggeredEvent = components[Random.Range(0, components.Length)];
		if (triggeredEvent == null)
		{
			return;
		}
		triggeredEvent.SendMessage("RunEvent", 1);
	}

	// Token: 0x0600175B RID: 5979 RVA: 0x00086134 File Offset: 0x00084334
	private void CountHours()
	{
		if (!TOD_Sky.Instance)
		{
			return;
		}
		if (this.lastRun != 0L)
		{
			TimeSpan timeSpan = TOD_Sky.Instance.Cycle.DateTime.Subtract(DateTime.FromBinary(this.lastRun));
			this.hoursRemaining -= (float)timeSpan.TotalSeconds / 60f / 60f;
		}
		this.lastRun = TOD_Sky.Instance.Cycle.DateTime.ToBinary();
	}

	// Token: 0x040011F5 RID: 4597
	[Tooltip("The minimum amount of hours between events")]
	public float minimumHoursBetween = 12f;

	// Token: 0x040011F6 RID: 4598
	[Tooltip("The maximum amount of hours between events")]
	public float maxmumHoursBetween = 24f;

	// Token: 0x040011F7 RID: 4599
	private float hoursRemaining;

	// Token: 0x040011F8 RID: 4600
	private long lastRun;
}
