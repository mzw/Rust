﻿using System;
using ConVar;
using Facepunch;
using Network;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200005F RID: 95
public class Chainsaw : global::BaseMelee
{
	// Token: 0x06000746 RID: 1862 RVA: 0x0002DDF8 File Offset: 0x0002BFF8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Chainsaw.OnRpcMessage", 0.1f))
		{
			if (rpc == 1276361188u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoReload ");
				}
				using (TimeWarning.New("DoReload", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoReload", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoReload(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoReload");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 682694132u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Server_SetAttacking ");
				}
				using (TimeWarning.New("Server_SetAttacking", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("Server_SetAttacking", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Server_SetAttacking(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in Server_SetAttacking");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 3683462696u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Server_StartEngine ");
				}
				using (TimeWarning.New("Server_StartEngine", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("Server_StartEngine", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Server_StartEngine(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in Server_StartEngine");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 4033578816u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Server_StopEngine ");
				}
				using (TimeWarning.New("Server_StopEngine", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("Server_StopEngine", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Server_StopEngine(msg5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in Server_StopEngine");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000747 RID: 1863 RVA: 0x0002E46C File Offset: 0x0002C66C
	public bool EngineOn()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000748 RID: 1864 RVA: 0x0002E478 File Offset: 0x0002C678
	public bool IsAttacking()
	{
		return base.HasFlag(global::BaseEntity.Flags.Busy);
	}

	// Token: 0x06000749 RID: 1865 RVA: 0x0002E488 File Offset: 0x0002C688
	protected override bool VerifyClientAttack(global::BasePlayer player)
	{
		return this.EngineOn() && base.VerifyClientAttack(player);
	}

	// Token: 0x0600074A RID: 1866 RVA: 0x0002E4A0 File Offset: 0x0002C6A0
	public override void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
		this.ServerCommand(item, "unload_ammo", crafter);
	}

	// Token: 0x0600074B RID: 1867 RVA: 0x0002E4B0 File Offset: 0x0002C6B0
	public override void SetHeld(bool bHeld)
	{
		if (!bHeld)
		{
			this.SetEngineStatus(false);
		}
		base.SetHeld(bHeld);
	}

	// Token: 0x0600074C RID: 1868 RVA: 0x0002E4C8 File Offset: 0x0002C6C8
	public void ReduceAmmo(float firingTime)
	{
		this.ammoRemainder += this.fuelPerSec * firingTime;
		if (this.ammoRemainder >= 1f)
		{
			int num = Mathf.FloorToInt(this.ammoRemainder);
			this.ammoRemainder -= (float)num;
			if (this.ammoRemainder >= 1f)
			{
				num++;
				this.ammoRemainder -= 1f;
			}
			this.ammo -= num;
			if (this.ammo <= 0)
			{
				this.ammo = 0;
			}
		}
		if ((float)this.ammo <= 0f)
		{
			this.SetEngineStatus(false);
		}
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600074D RID: 1869 RVA: 0x0002E57C File Offset: 0x0002C77C
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	public void DoReload(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		global::Item item;
		while (this.ammo < this.maxAmmo && (item = this.GetAmmo()) != null && item.amount > 0)
		{
			int num = Mathf.Min(this.maxAmmo - this.ammo, item.amount);
			this.ammo += num;
			item.UseItem(num);
		}
		base.SendNetworkUpdateImmediate(false);
		global::ItemManager.DoRemoves();
		ownerPlayer.inventory.ServerUpdate(0f);
	}

	// Token: 0x0600074E RID: 1870 RVA: 0x0002E618 File Offset: 0x0002C818
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.baseProjectile = new ProtoBuf.BaseProjectile();
		info.msg.baseProjectile.primaryMagazine = Facepunch.Pool.Get<Magazine>();
		info.msg.baseProjectile.primaryMagazine.contents = this.ammo;
	}

	// Token: 0x0600074F RID: 1871 RVA: 0x0002E670 File Offset: 0x0002C870
	public void SetEngineStatus(bool status)
	{
		base.SetFlag(global::BaseEntity.Flags.On, status, false);
		if (!status)
		{
			this.SetAttackStatus(false);
		}
		base.CancelInvoke(new Action(this.EngineTick));
		if (status)
		{
			base.InvokeRepeating(new Action(this.EngineTick), 0f, 1f);
		}
	}

	// Token: 0x06000750 RID: 1872 RVA: 0x0002E6C8 File Offset: 0x0002C8C8
	public void SetAttackStatus(bool status)
	{
		if (!this.EngineOn())
		{
			status = false;
		}
		base.SetFlag(global::BaseEntity.Flags.Busy, status, false);
		base.CancelInvoke(new Action(this.AttackTick));
		if (status)
		{
			base.InvokeRepeating(new Action(this.AttackTick), 0f, 1f);
		}
	}

	// Token: 0x06000751 RID: 1873 RVA: 0x0002E724 File Offset: 0x0002C924
	public void EngineTick()
	{
		this.ReduceAmmo(0.05f);
	}

	// Token: 0x06000752 RID: 1874 RVA: 0x0002E734 File Offset: 0x0002C934
	public void AttackTick()
	{
		this.ReduceAmmo(this.fuelPerSec);
	}

	// Token: 0x06000753 RID: 1875 RVA: 0x0002E744 File Offset: 0x0002C944
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	public void Server_StartEngine(global::BaseEntity.RPCMessage msg)
	{
		if (this.ammo <= 0 || this.EngineOn())
		{
			return;
		}
		this.ReduceAmmo(0.25f);
		bool flag = Random.Range(0f, 1f) <= this.engineStartChance;
		if (flag)
		{
			this.SetEngineStatus(true);
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x06000754 RID: 1876 RVA: 0x0002E7A4 File Offset: 0x0002C9A4
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	public void Server_StopEngine(global::BaseEntity.RPCMessage msg)
	{
		this.SetEngineStatus(false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000755 RID: 1877 RVA: 0x0002E7B4 File Offset: 0x0002C9B4
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	public void Server_SetAttacking(global::BaseEntity.RPCMessage msg)
	{
		bool flag = msg.read.Bit();
		if (this.IsAttacking() == flag)
		{
			return;
		}
		if (!this.EngineOn())
		{
			return;
		}
		this.SetAttackStatus(flag);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000756 RID: 1878 RVA: 0x0002E7F8 File Offset: 0x0002C9F8
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (item == null)
		{
			return;
		}
		if (command == "unload_ammo")
		{
			int num = this.ammo;
			if (num > 0)
			{
				this.ammo = 0;
				base.SendNetworkUpdateImmediate(false);
				global::Item item2 = global::ItemManager.Create(this.fuelType, num, 0UL);
				if (!item2.MoveToContainer(player.inventory.containerMain, -1, true))
				{
					item2.Drop(player.eyes.position, player.eyes.BodyForward() * 2f, default(Quaternion));
				}
			}
		}
	}

	// Token: 0x06000757 RID: 1879 RVA: 0x0002E890 File Offset: 0x0002CA90
	public void DisableHitEffects()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved6, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved7, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved8, false, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000758 RID: 1880 RVA: 0x0002E8C0 File Offset: 0x0002CAC0
	public void EnableHitEffect(uint hitMaterial)
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved6, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved7, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved8, false, false);
		if (hitMaterial == global::StringPool.Get("Flesh"))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved8, true, false);
		}
		else if (hitMaterial == global::StringPool.Get("Wood"))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved7, true, false);
		}
		else
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved6, true, false);
		}
		base.SendNetworkUpdateImmediate(false);
		base.CancelInvoke(new Action(this.DisableHitEffects));
		base.Invoke(new Action(this.DisableHitEffects), 0.5f);
	}

	// Token: 0x06000759 RID: 1881 RVA: 0x0002E978 File Offset: 0x0002CB78
	public override void DoAttackShared(global::HitInfo info)
	{
		base.DoAttackShared(info);
		if (base.isServer)
		{
			this.EnableHitEffect(info.HitMaterial);
		}
	}

	// Token: 0x0600075A RID: 1882 RVA: 0x0002E998 File Offset: 0x0002CB98
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.baseProjectile != null && info.msg.baseProjectile.primaryMagazine != null)
		{
			this.ammo = info.msg.baseProjectile.primaryMagazine.contents;
		}
	}

	// Token: 0x0600075B RID: 1883 RVA: 0x0002E9F0 File Offset: 0x0002CBF0
	public bool HasAmmo()
	{
		return this.GetAmmo() != null;
	}

	// Token: 0x0600075C RID: 1884 RVA: 0x0002EA00 File Offset: 0x0002CC00
	public global::Item GetAmmo()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return null;
		}
		global::Item item = ownerPlayer.inventory.containerMain.FindItemsByItemName(this.fuelType.shortname);
		if (item == null)
		{
			item = ownerPlayer.inventory.containerBelt.FindItemsByItemName(this.fuelType.shortname);
		}
		return item;
	}

	// Token: 0x04000347 RID: 839
	[Header("Chainsaw")]
	public float fuelPerSec = 1f;

	// Token: 0x04000348 RID: 840
	public int maxAmmo = 100;

	// Token: 0x04000349 RID: 841
	public int ammo = 100;

	// Token: 0x0400034A RID: 842
	public global::ItemDefinition fuelType;

	// Token: 0x0400034B RID: 843
	public float reloadDuration = 2.5f;

	// Token: 0x0400034C RID: 844
	[Header("Sounds")]
	public global::SoundPlayer idleLoop;

	// Token: 0x0400034D RID: 845
	public global::SoundPlayer attackLoopAir;

	// Token: 0x0400034E RID: 846
	public global::SoundPlayer revUp;

	// Token: 0x0400034F RID: 847
	public global::SoundPlayer revDown;

	// Token: 0x04000350 RID: 848
	public global::SoundPlayer offSound;

	// Token: 0x04000351 RID: 849
	public float engineStartChance = 0.33f;

	// Token: 0x04000352 RID: 850
	private float ammoRemainder;

	// Token: 0x04000353 RID: 851
	public Renderer chainRenderer;

	// Token: 0x04000354 RID: 852
	private MaterialPropertyBlock block;

	// Token: 0x04000355 RID: 853
	private Vector2 saveST;
}
