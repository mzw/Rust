﻿using System;
using UnityEngine;

// Token: 0x02000239 RID: 569
public class BoneFollower : ListComponent<global::BoneFollower>
{
	// Token: 0x04000ADB RID: 2779
	public Transform bone;

	// Token: 0x04000ADC RID: 2780
	public Vector3 offset;

	// Token: 0x04000ADD RID: 2781
	public Quaternion rotationOffset;
}
