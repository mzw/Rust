﻿using System;
using UnityEngine;

// Token: 0x02000237 RID: 567
public class AnimationEventForward : MonoBehaviour
{
	// Token: 0x06001019 RID: 4121 RVA: 0x00061C44 File Offset: 0x0005FE44
	public void Event(string type)
	{
		this.targetObject.SendMessage(type);
	}

	// Token: 0x04000AD9 RID: 2777
	public GameObject targetObject;
}
