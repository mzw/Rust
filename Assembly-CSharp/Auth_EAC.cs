﻿using System;
using System.Collections;
using Network;

// Token: 0x02000618 RID: 1560
public static class Auth_EAC
{
	// Token: 0x06001F8A RID: 8074 RVA: 0x000B3318 File Offset: 0x000B1518
	public static IEnumerator Run(Connection connection)
	{
		if (!connection.active)
		{
			yield break;
		}
		if (connection.rejected)
		{
			yield break;
		}
		connection.authStatus = string.Empty;
		global::EACServer.OnJoinGame(connection);
		while (connection.active && !connection.rejected && connection.authStatus == string.Empty)
		{
			yield return null;
		}
		yield break;
	}
}
