﻿using System;
using UnityEngine;

// Token: 0x020004F5 RID: 1269
public class ItemModSwap : global::ItemMod
{
	// Token: 0x06001B06 RID: 6918 RVA: 0x00097454 File Offset: 0x00095654
	public override void DoAction(global::Item item, global::BasePlayer player)
	{
		if (item.amount < 1)
		{
			return;
		}
		foreach (global::ItemAmount itemAmount in this.becomeItem)
		{
			global::Item item2 = global::ItemManager.Create(itemAmount.itemDef, (int)itemAmount.amount, 0UL);
			if (item2 != null)
			{
				if (!item2.MoveToContainer(item.parent, -1, true))
				{
					player.GiveItem(item2, global::BaseEntity.GiveItemReason.Generic);
				}
				if (this.sendPlayerPickupNotification)
				{
					player.Command("note.inv", new object[]
					{
						item2.info.itemid,
						item2.amount
					});
				}
			}
		}
		if (this.sendPlayerDropNotification)
		{
			player.Command("note.inv", new object[]
			{
				item.info.itemid,
				-1
			});
		}
		if (this.actionEffect.isValid)
		{
			global::Effect.server.Run(this.actionEffect.resourcePath, player.transform.position, Vector3.up, null, false);
		}
		item.UseItem(1);
	}

	// Token: 0x040015D5 RID: 5589
	public global::GameObjectRef actionEffect;

	// Token: 0x040015D6 RID: 5590
	public global::ItemAmount[] becomeItem;

	// Token: 0x040015D7 RID: 5591
	public bool sendPlayerPickupNotification;

	// Token: 0x040015D8 RID: 5592
	public bool sendPlayerDropNotification;

	// Token: 0x040015D9 RID: 5593
	public float xpScale = 1f;
}
