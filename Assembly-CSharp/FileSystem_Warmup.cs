﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using UnityEngine;

// Token: 0x0200033E RID: 830
public class FileSystem_Warmup : MonoBehaviour
{
	// Token: 0x060013F1 RID: 5105 RVA: 0x00074BDC File Offset: 0x00072DDC
	public static void Run()
	{
		if (!global::FileSystem_Warmup.run || global::FileSystem_Warmup.running)
		{
			return;
		}
		global::FileSystem_Warmup.running = true;
		string[] assetList = global::FileSystem_Warmup.GetAssetList();
		for (int i = 0; i < assetList.Length; i++)
		{
			global::FileSystem.Load<Object>(assetList[i], false);
			global::FileSystem_Warmup.PrefabWarmup(assetList[i]);
		}
		global::FileSystem_Warmup.running = (global::FileSystem_Warmup.run = false);
	}

	// Token: 0x060013F2 RID: 5106 RVA: 0x00074C3C File Offset: 0x00072E3C
	public static IEnumerator Run(float deltaTime, Action<string> statusFunction = null)
	{
		if (!global::FileSystem_Warmup.run || global::FileSystem_Warmup.running)
		{
			yield break;
		}
		global::FileSystem_Warmup.running = true;
		string[] prewarmAssets = global::FileSystem_Warmup.GetAssetList();
		Stopwatch sw = Stopwatch.StartNew();
		for (int i = 0; i < prewarmAssets.Length; i++)
		{
			if (sw.Elapsed.TotalSeconds > (double)deltaTime || i == 0 || i == prewarmAssets.Length - 1)
			{
				global::FileSystem_Warmup.Status(statusFunction, "Asset Warmup ({0}/{1})", i + 1, prewarmAssets.Length);
				yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
				sw.Reset();
				sw.Start();
			}
			global::FileSystem.Load<Object>(prewarmAssets[i], false);
			global::FileSystem_Warmup.PrefabWarmup(prewarmAssets[i]);
		}
		global::FileSystem_Warmup.running = (global::FileSystem_Warmup.run = false);
		yield break;
	}

	// Token: 0x060013F3 RID: 5107 RVA: 0x00074C60 File Offset: 0x00072E60
	private static bool ShouldPreProcess(string path)
	{
		for (int i = 0; i < global::FileSystem_Warmup.noPreProcessing.Length; i++)
		{
			if (StringEx.Contains(path, global::FileSystem_Warmup.noPreProcessing[i], CompareOptions.IgnoreCase))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x060013F4 RID: 5108 RVA: 0x00074C9C File Offset: 0x00072E9C
	private static bool ShouldIgnore(string path)
	{
		for (int i = 0; i < global::FileSystem_Warmup.excludeFilter.Length; i++)
		{
			if (StringEx.Contains(path, global::FileSystem_Warmup.excludeFilter[i], CompareOptions.IgnoreCase))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060013F5 RID: 5109 RVA: 0x00074CD8 File Offset: 0x00072ED8
	private static string[] GetAssetList()
	{
		return (from x in global::FileSystem_Warmup.includeFilter.SelectMany((string x) => global::FileSystem.FindAll("assets", x))
		where !global::FileSystem_Warmup.ShouldIgnore(x)
		select x).ToArray<string>();
	}

	// Token: 0x060013F6 RID: 5110 RVA: 0x00074D34 File Offset: 0x00072F34
	private static void PrefabWarmup(string path)
	{
		if (!global::FileSystem_Warmup.ShouldPreProcess(path))
		{
			return;
		}
		global::GameManager.server.FindPrefab(path);
	}

	// Token: 0x060013F7 RID: 5111 RVA: 0x00074D50 File Offset: 0x00072F50
	private static void Status(Action<string> statusFunction, string status, object obj1)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, obj1));
		}
	}

	// Token: 0x060013F8 RID: 5112 RVA: 0x00074D68 File Offset: 0x00072F68
	private static void Status(Action<string> statusFunction, string status, object obj1, object obj2)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, obj1, obj2));
		}
	}

	// Token: 0x060013F9 RID: 5113 RVA: 0x00074D80 File Offset: 0x00072F80
	private static void Status(Action<string> statusFunction, string status, object obj1, object obj2, object obj3)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, obj1, obj2, obj3));
		}
	}

	// Token: 0x060013FA RID: 5114 RVA: 0x00074D98 File Offset: 0x00072F98
	private static void Status(Action<string> statusFunction, string status, params object[] objs)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, objs));
		}
	}

	// Token: 0x04000EC8 RID: 3784
	private static bool run = true;

	// Token: 0x04000EC9 RID: 3785
	private static bool running = false;

	// Token: 0x04000ECA RID: 3786
	private static string[] includeFilter = new string[]
	{
		".prefab"
	};

	// Token: 0x04000ECB RID: 3787
	private static string[] excludeFilter = new string[]
	{
		"/bundled/prefabs/autospawn/monument",
		"/bundled/prefabs/autospawn/mountain",
		"/bundled/prefabs/autospawn/canyon",
		"/bundled/prefabs/autospawn/decor",
		"/bundled/prefabs/navmesh",
		"/prefabs/world/",
		"/prefabs/system/",
		"/third party/"
	};

	// Token: 0x04000ECC RID: 3788
	private static string[] noPreProcessing = new string[]
	{
		"/content/ui/",
		"/prefabs/ui/"
	};
}
