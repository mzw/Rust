﻿using System;
using UnityEngine;

// Token: 0x02000231 RID: 561
public class AnimalSkin : MonoBehaviour, IClientComponent
{
	// Token: 0x06001009 RID: 4105 RVA: 0x00061800 File Offset: 0x0005FA00
	private void Start()
	{
		this.model = base.gameObject.GetComponent<global::Model>();
		if (!this.dontRandomizeOnStart)
		{
			int iSkin = Mathf.FloorToInt((float)Random.Range(0, this.animalSkins.Length));
			this.ChangeSkin(iSkin);
		}
	}

	// Token: 0x0600100A RID: 4106 RVA: 0x00061848 File Offset: 0x0005FA48
	public void ChangeSkin(int iSkin)
	{
		if (this.animalSkins.Length <= 0)
		{
			return;
		}
		iSkin = Mathf.Clamp(iSkin, 0, this.animalSkins.Length - 1);
		foreach (SkinnedMeshRenderer skinnedMeshRenderer in this.animalMesh)
		{
			Material[] sharedMaterials = skinnedMeshRenderer.sharedMaterials;
			if (sharedMaterials != null)
			{
				for (int j = 0; j < sharedMaterials.Length; j++)
				{
					sharedMaterials[j] = this.animalSkins[iSkin].multiSkin[j];
				}
				skinnedMeshRenderer.sharedMaterials = sharedMaterials;
			}
		}
		if (this.model != null)
		{
			this.model.skin = iSkin;
		}
	}

	// Token: 0x04000AB7 RID: 2743
	public SkinnedMeshRenderer[] animalMesh;

	// Token: 0x04000AB8 RID: 2744
	public global::AnimalMultiSkin[] animalSkins;

	// Token: 0x04000AB9 RID: 2745
	private global::Model model;

	// Token: 0x04000ABA RID: 2746
	public bool dontRandomizeOnStart;
}
