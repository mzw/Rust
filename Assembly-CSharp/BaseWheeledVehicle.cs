﻿using System;
using UnityEngine;

// Token: 0x020003E8 RID: 1000
public class BaseWheeledVehicle : global::BaseVehicle
{
	// Token: 0x040011E3 RID: 4579
	[Header("Wheels")]
	public global::BaseWheeledVehicle.VehicleWheel[] wheels;

	// Token: 0x020003E9 RID: 1001
	[Serializable]
	public class VehicleWheel
	{
		// Token: 0x040011E4 RID: 4580
		public Transform shock;

		// Token: 0x040011E5 RID: 4581
		public WheelCollider wheelCollider;

		// Token: 0x040011E6 RID: 4582
		public Transform wheel;

		// Token: 0x040011E7 RID: 4583
		public Transform axle;

		// Token: 0x040011E8 RID: 4584
		public bool steerWheel;

		// Token: 0x040011E9 RID: 4585
		public bool brakeWheel = true;

		// Token: 0x040011EA RID: 4586
		public bool powerWheel = true;
	}
}
