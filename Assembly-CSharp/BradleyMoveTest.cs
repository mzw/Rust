﻿using System;
using UnityEngine;

// Token: 0x020000CA RID: 202
public class BradleyMoveTest : MonoBehaviour
{
	// Token: 0x06000AC0 RID: 2752 RVA: 0x00049148 File Offset: 0x00047348
	public void Awake()
	{
		this.Initialize();
	}

	// Token: 0x06000AC1 RID: 2753 RVA: 0x00049150 File Offset: 0x00047350
	public void Initialize()
	{
		this.myRigidBody.centerOfMass = this.centerOfMass.localPosition;
		this.destination = base.transform.position;
	}

	// Token: 0x06000AC2 RID: 2754 RVA: 0x0004917C File Offset: 0x0004737C
	public void SetDestination(Vector3 dest)
	{
		this.destination = dest;
	}

	// Token: 0x06000AC3 RID: 2755 RVA: 0x00049188 File Offset: 0x00047388
	public void FixedUpdate()
	{
		Vector3 velocity = this.myRigidBody.velocity;
		this.SetDestination(this.followTest.transform.position);
		float num = Vector3.Distance(base.transform.position, this.destination);
		if (num > this.stoppingDist)
		{
			Vector3 zero = Vector3.zero;
			float num2 = Vector3.Dot(zero, base.transform.right);
			float num3 = Vector3.Dot(zero, -base.transform.right);
			float num4 = Vector3.Dot(zero, base.transform.right);
			float num5 = Vector3.Dot(zero, -base.transform.forward);
			if (num5 > num4)
			{
				if (num2 >= num3)
				{
					this.turning = 1f;
				}
				else
				{
					this.turning = -1f;
				}
			}
			else
			{
				this.turning = num4;
			}
			this.throttle = Mathf.InverseLerp(this.stoppingDist, 30f, num);
		}
		this.throttle = Mathf.Clamp(this.throttle, -1f, 1f);
		float num6 = this.throttle;
		float num7 = this.throttle;
		if (this.turning > 0f)
		{
			num7 = -this.turning;
			num6 = this.turning;
		}
		else if (this.turning < 0f)
		{
			num6 = this.turning;
			num7 = this.turning * -1f;
		}
		this.ApplyBrakes((!this.brake) ? 0f : 1f);
		float num8 = this.throttle;
		num6 = Mathf.Clamp(num6 + num8, -1f, 1f);
		num7 = Mathf.Clamp(num7 + num8, -1f, 1f);
		this.AdjustFriction();
		float num9 = Mathf.InverseLerp(3f, 1f, velocity.magnitude * Mathf.Abs(Vector3.Dot(velocity.normalized, base.transform.forward)));
		float torqueAmount = Mathf.Lerp(this.moveForceMax, this.turnForce, num9);
		this.SetMotorTorque(num6, false, torqueAmount);
		this.SetMotorTorque(num7, true, torqueAmount);
	}

	// Token: 0x06000AC4 RID: 2756 RVA: 0x000493B8 File Offset: 0x000475B8
	public void ApplyBrakes(float amount)
	{
		this.ApplyBrakeTorque(amount, true);
		this.ApplyBrakeTorque(amount, false);
	}

	// Token: 0x06000AC5 RID: 2757 RVA: 0x000493CC File Offset: 0x000475CC
	public float GetMotorTorque(bool rightSide)
	{
		float num = 0f;
		foreach (WheelCollider wheelCollider in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			num += wheelCollider.motorTorque;
		}
		return num / (float)this.rightWheels.Length;
	}

	// Token: 0x06000AC6 RID: 2758 RVA: 0x00049428 File Offset: 0x00047628
	public void SetMotorTorque(float newThrottle, bool rightSide, float torqueAmount)
	{
		newThrottle = Mathf.Clamp(newThrottle, -1f, 1f);
		float motorTorque = torqueAmount * newThrottle;
		foreach (WheelCollider wheelCollider in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			wheelCollider.motorTorque = motorTorque;
		}
	}

	// Token: 0x06000AC7 RID: 2759 RVA: 0x00049484 File Offset: 0x00047684
	public void ApplyBrakeTorque(float amount, bool rightSide)
	{
		foreach (WheelCollider wheelCollider in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			wheelCollider.brakeTorque = this.brakeForce * amount;
		}
	}

	// Token: 0x06000AC8 RID: 2760 RVA: 0x000494D0 File Offset: 0x000476D0
	public void AdjustFriction()
	{
	}

	// Token: 0x04000582 RID: 1410
	public WheelCollider[] leftWheels;

	// Token: 0x04000583 RID: 1411
	public WheelCollider[] rightWheels;

	// Token: 0x04000584 RID: 1412
	public float moveForceMax = 2000f;

	// Token: 0x04000585 RID: 1413
	public float brakeForce = 100f;

	// Token: 0x04000586 RID: 1414
	public float throttle = 1f;

	// Token: 0x04000587 RID: 1415
	public float turnForce = 2000f;

	// Token: 0x04000588 RID: 1416
	public float sideStiffnessMax = 1f;

	// Token: 0x04000589 RID: 1417
	public float sideStiffnessMin = 0.5f;

	// Token: 0x0400058A RID: 1418
	public Transform centerOfMass;

	// Token: 0x0400058B RID: 1419
	public float turning;

	// Token: 0x0400058C RID: 1420
	public bool brake;

	// Token: 0x0400058D RID: 1421
	public Rigidbody myRigidBody;

	// Token: 0x0400058E RID: 1422
	public Vector3 destination;

	// Token: 0x0400058F RID: 1423
	public float stoppingDist = 5f;

	// Token: 0x04000590 RID: 1424
	public GameObject followTest;
}
