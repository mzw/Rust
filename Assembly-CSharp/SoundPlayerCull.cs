﻿using System;
using UnityEngine;

// Token: 0x020001FE RID: 510
public class SoundPlayerCull : MonoBehaviour, IClientComponent, global::ILOD
{
	// Token: 0x04000A0E RID: 2574
	public global::SoundPlayer soundPlayer;

	// Token: 0x04000A0F RID: 2575
	public float cullDistance = 100f;
}
