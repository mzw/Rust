﻿using System;

// Token: 0x020000F5 RID: 245
public class Wolf : global::BaseAnimalNPC
{
	// Token: 0x170000AE RID: 174
	// (get) Token: 0x06000C2A RID: 3114 RVA: 0x0004FF6C File Offset: 0x0004E16C
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000C2B RID: 3115 RVA: 0x0004FF70 File Offset: 0x0004E170
	public override bool WantsToEat(global::BaseEntity best)
	{
		return !best.HasTrait(global::BaseEntity.TraitFlag.Alive) && (best.HasTrait(global::BaseEntity.TraitFlag.Meat) || base.WantsToEat(best));
	}

	// Token: 0x06000C2C RID: 3116 RVA: 0x0004FF98 File Offset: 0x0004E198
	public override string Categorize()
	{
		return "Wolf";
	}

	// Token: 0x040006B7 RID: 1719
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population = 2f;
}
