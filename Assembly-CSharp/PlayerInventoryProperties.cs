﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200064F RID: 1615
[CreateAssetMenu(menuName = "Rust/Player Inventory Properties")]
public class PlayerInventoryProperties : ScriptableObject
{
	// Token: 0x06002075 RID: 8309 RVA: 0x000B984C File Offset: 0x000B7A4C
	public void GiveToPlayer(global::BasePlayer player)
	{
		if (player == null)
		{
			return;
		}
		player.inventory.Strip();
		foreach (global::ItemAmount itemAmount in this.belt)
		{
			player.inventory.GiveItem(global::ItemManager.Create(itemAmount.itemDef, (int)itemAmount.amount, 0UL), player.inventory.containerBelt);
		}
		foreach (global::ItemAmount itemAmount2 in this.main)
		{
			player.inventory.GiveItem(global::ItemManager.Create(itemAmount2.itemDef, (int)itemAmount2.amount, 0UL), player.inventory.containerMain);
		}
		if (this.skinnedWear.Count > 0)
		{
			foreach (global::PlayerInventoryProperties.ItemAmountSkinned itemAmountSkinned in this.skinnedWear)
			{
				player.inventory.GiveItem(global::ItemManager.Create(itemAmountSkinned.itemDef, (int)itemAmountSkinned.amount, itemAmountSkinned.GetRandomSkin()), player.inventory.containerWear);
			}
		}
		foreach (global::ItemAmount itemAmount3 in this.wear)
		{
			player.inventory.GiveItem(global::ItemManager.Create(itemAmount3.itemDef, (int)itemAmount3.amount, 0UL), player.inventory.containerWear);
		}
	}

	// Token: 0x04001B66 RID: 7014
	public string niceName;

	// Token: 0x04001B67 RID: 7015
	public int order = 100;

	// Token: 0x04001B68 RID: 7016
	public List<global::ItemAmount> belt;

	// Token: 0x04001B69 RID: 7017
	public List<global::ItemAmount> main;

	// Token: 0x04001B6A RID: 7018
	public List<global::ItemAmount> wear;

	// Token: 0x04001B6B RID: 7019
	public List<global::PlayerInventoryProperties.ItemAmountSkinned> skinnedWear;

	// Token: 0x02000650 RID: 1616
	[Serializable]
	public class ItemAmountSkinned : global::ItemAmount
	{
		// Token: 0x06002076 RID: 8310 RVA: 0x000B9A50 File Offset: 0x000B7C50
		public ItemAmountSkinned() : base(null, 0f)
		{
		}

		// Token: 0x06002077 RID: 8311 RVA: 0x000B9A60 File Offset: 0x000B7C60
		public ulong GetRandomSkin()
		{
			return this.skinOverride;
		}

		// Token: 0x04001B6C RID: 7020
		public ulong skinOverride;
	}
}
