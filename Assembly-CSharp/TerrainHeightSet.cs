﻿using System;
using UnityEngine;

// Token: 0x020005D3 RID: 1491
public class TerrainHeightSet : global::TerrainModifier
{
	// Token: 0x06001EBD RID: 7869 RVA: 0x000AD794 File Offset: 0x000AB994
	protected override void Apply(Vector3 position, float opacity, float radius, float fade)
	{
		if (!global::TerrainMeta.HeightMap)
		{
			return;
		}
		global::TerrainMeta.HeightMap.SetHeight(position, opacity, radius, fade);
	}
}
