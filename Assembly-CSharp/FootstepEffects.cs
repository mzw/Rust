﻿using System;
using UnityEngine;

// Token: 0x02000325 RID: 805
public class FootstepEffects : global::BaseFootstepEffect
{
	// Token: 0x04000E69 RID: 3689
	public Transform leftFoot;

	// Token: 0x04000E6A RID: 3690
	public Transform rightFoot;

	// Token: 0x04000E6B RID: 3691
	public string footstepEffectName = "footstep/barefoot";

	// Token: 0x04000E6C RID: 3692
	public string jumpStartEffectName = "jump-start/barefoot";

	// Token: 0x04000E6D RID: 3693
	public string jumpLandEffectName = "jump-land/barefoot";
}
