﻿using System;
using System.Linq;
using Facepunch;
using Facepunch.Models;

// Token: 0x02000300 RID: 768
public static class DeveloperList
{
	// Token: 0x0600134F RID: 4943 RVA: 0x00071BD0 File Offset: 0x0006FDD0
	public static bool Contains(ulong steamid)
	{
		return Application.Manifest != null && Application.Manifest.Administrators != null && Application.Manifest.Administrators.Any((Manifest.Administrator x) => x.UserId == steamid.ToString());
	}

	// Token: 0x06001350 RID: 4944 RVA: 0x00071C24 File Offset: 0x0006FE24
	public static bool IsDeveloper(global::BasePlayer ply)
	{
		return ply != null && global::DeveloperList.Contains(ply.userID);
	}
}
