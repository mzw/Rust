﻿using System;
using System.IO;
using Network;

// Token: 0x0200050C RID: 1292
public class NetworkCryptographyServer : global::NetworkCryptography
{
	// Token: 0x06001B59 RID: 7001 RVA: 0x00098724 File Offset: 0x00096924
	protected override void EncryptionHandler(Connection connection, MemoryStream src, int srcOffset, MemoryStream dst, int dstOffset)
	{
		if (connection.encryptionLevel <= 1u)
		{
			global::Craptography.XOR(2065u, src, srcOffset, dst, dstOffset);
		}
		else
		{
			global::EACServer.Encrypt(connection, src, srcOffset, dst, dstOffset);
		}
	}

	// Token: 0x06001B5A RID: 7002 RVA: 0x00098754 File Offset: 0x00096954
	protected override void DecryptionHandler(Connection connection, MemoryStream src, int srcOffset, MemoryStream dst, int dstOffset)
	{
		if (connection.encryptionLevel <= 1u)
		{
			global::Craptography.XOR(2065u, src, srcOffset, dst, dstOffset);
		}
		else
		{
			global::EACServer.Decrypt(connection, src, srcOffset, dst, dstOffset);
		}
	}
}
