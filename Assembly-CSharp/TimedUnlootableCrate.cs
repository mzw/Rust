﻿using System;

// Token: 0x020003AA RID: 938
public class TimedUnlootableCrate : global::LootContainer
{
	// Token: 0x0600164C RID: 5708 RVA: 0x000814C8 File Offset: 0x0007F6C8
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.unlootableOnSpawn)
		{
			this.SetUnlootableFor(this.unlootableDuration);
		}
	}

	// Token: 0x0600164D RID: 5709 RVA: 0x000814E8 File Offset: 0x0007F6E8
	public void SetUnlootableFor(float duration)
	{
		base.SetFlag(global::BaseEntity.Flags.OnFire, true, false);
		base.SetFlag(global::BaseEntity.Flags.Locked, true, false);
		this.unlootableDuration = duration;
		base.Invoke(new Action(this.MakeLootable), duration);
	}

	// Token: 0x0600164E RID: 5710 RVA: 0x00081518 File Offset: 0x0007F718
	public void MakeLootable()
	{
		base.SetFlag(global::BaseEntity.Flags.OnFire, false, false);
		base.SetFlag(global::BaseEntity.Flags.Locked, false, false);
	}

	// Token: 0x040010CB RID: 4299
	public bool unlootableOnSpawn = true;

	// Token: 0x040010CC RID: 4300
	public float unlootableDuration = 300f;
}
