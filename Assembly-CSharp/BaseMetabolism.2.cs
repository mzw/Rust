﻿using System;
using ConVar;
using Rust;
using UnityEngine;

// Token: 0x0200038B RID: 907
public abstract class BaseMetabolism<T> : global::EntityComponent<T> where T : global::BaseCombatEntity
{
	// Token: 0x06001589 RID: 5513 RVA: 0x0007BE6C File Offset: 0x0007A06C
	public virtual void Reset()
	{
		this.calories.Reset();
		this.hydration.Reset();
		this.heartrate.Reset();
	}

	// Token: 0x0600158A RID: 5514 RVA: 0x0007BE90 File Offset: 0x0007A090
	protected virtual void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.owner = (T)((object)null);
	}

	// Token: 0x0600158B RID: 5515 RVA: 0x0007BEAC File Offset: 0x0007A0AC
	public virtual void ServerInit(T owner)
	{
		this.Reset();
		this.owner = owner;
	}

	// Token: 0x0600158C RID: 5516 RVA: 0x0007BEBC File Offset: 0x0007A0BC
	public virtual void ServerUpdate(global::BaseCombatEntity ownerEntity, float delta)
	{
		this.timeSinceLastMetabolism += delta;
		if (this.timeSinceLastMetabolism <= ConVar.Server.metabolismtick)
		{
			return;
		}
		if (this.owner && !this.owner.IsDead())
		{
			this.RunMetabolism(ownerEntity, this.timeSinceLastMetabolism);
			this.DoMetabolismDamage(ownerEntity, this.timeSinceLastMetabolism);
		}
		this.timeSinceLastMetabolism = 0f;
	}

	// Token: 0x0600158D RID: 5517 RVA: 0x0007BF38 File Offset: 0x0007A138
	protected virtual void DoMetabolismDamage(global::BaseCombatEntity ownerEntity, float delta)
	{
		if (this.calories.value <= 20f)
		{
			using (TimeWarning.New("Calories Hurt", 0.1f))
			{
				ownerEntity.Hurt(Mathf.InverseLerp(20f, 0f, this.calories.value) * delta * 0.0833333358f, Rust.DamageType.Hunger, null, true);
			}
		}
		if (this.hydration.value <= 20f)
		{
			using (TimeWarning.New("Hyration Hurt", 0.1f))
			{
				ownerEntity.Hurt(Mathf.InverseLerp(20f, 0f, this.hydration.value) * delta * 0.13333334f, Rust.DamageType.Thirst, null, true);
			}
		}
	}

	// Token: 0x0600158E RID: 5518 RVA: 0x0007C028 File Offset: 0x0007A228
	protected virtual void RunMetabolism(global::BaseCombatEntity ownerEntity, float delta)
	{
		if (this.calories.value > 200f)
		{
			ownerEntity.Heal(Mathf.InverseLerp(200f, 1000f, this.calories.value) * delta * 0.0166666675f);
		}
		if (this.hydration.value > 200f)
		{
			ownerEntity.Heal(Mathf.InverseLerp(200f, 1000f, this.hydration.value) * delta * 0.0166666675f);
		}
		this.hydration.MoveTowards(0f, delta * 0.008333334f);
		this.calories.MoveTowards(0f, delta * 0.0166666675f);
		this.heartrate.MoveTowards(0.05f, delta * 0.0166666675f);
	}

	// Token: 0x0600158F RID: 5519 RVA: 0x0007C0F4 File Offset: 0x0007A2F4
	public void ApplyChange(global::MetabolismAttribute.Type type, float amount, float time)
	{
		global::MetabolismAttribute metabolismAttribute = this.FindAttribute(type);
		if (metabolismAttribute == null)
		{
			return;
		}
		metabolismAttribute.Add(amount);
	}

	// Token: 0x06001590 RID: 5520 RVA: 0x0007C118 File Offset: 0x0007A318
	public bool ShouldDie()
	{
		return this.owner && this.owner.Health() <= 0f;
	}

	// Token: 0x06001591 RID: 5521 RVA: 0x0007C150 File Offset: 0x0007A350
	public virtual global::MetabolismAttribute FindAttribute(global::MetabolismAttribute.Type type)
	{
		if (type == global::MetabolismAttribute.Type.Calories)
		{
			return this.calories;
		}
		if (type == global::MetabolismAttribute.Type.Hydration)
		{
			return this.hydration;
		}
		if (type != global::MetabolismAttribute.Type.Heartrate)
		{
			return null;
		}
		return this.heartrate;
	}

	// Token: 0x04000FCE RID: 4046
	protected T owner;

	// Token: 0x04000FCF RID: 4047
	public global::MetabolismAttribute calories = new global::MetabolismAttribute();

	// Token: 0x04000FD0 RID: 4048
	public global::MetabolismAttribute hydration = new global::MetabolismAttribute();

	// Token: 0x04000FD1 RID: 4049
	public global::MetabolismAttribute heartrate = new global::MetabolismAttribute();

	// Token: 0x04000FD2 RID: 4050
	protected float timeSinceLastMetabolism;
}
