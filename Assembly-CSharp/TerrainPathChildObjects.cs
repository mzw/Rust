﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000553 RID: 1363
public class TerrainPathChildObjects : MonoBehaviour
{
	// Token: 0x06001CA0 RID: 7328 RVA: 0x000A0580 File Offset: 0x0009E780
	protected void Awake()
	{
		List<Vector3> list = new List<Vector3>();
		IEnumerator enumerator = base.transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform = (Transform)obj;
				list.Add(transform.position);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		if (list.Count >= 2)
		{
			global::InfrastructureType type = this.Type;
			if (type != global::InfrastructureType.Road)
			{
				if (type == global::InfrastructureType.Power)
				{
					global::PathList pathList = new global::PathList("Powerline " + global::TerrainMeta.Path.Powerlines.Count, list.ToArray());
					pathList.Width = this.Width;
					pathList.InnerFade = this.Fade * 0.5f;
					pathList.OuterFade = this.Fade * 0.5f;
					pathList.MeshOffset = this.Offset * 0.3f;
					pathList.TerrainOffset = this.Offset;
					pathList.Topology = (int)this.Topology;
					pathList.Splat = (int)this.Splat;
					pathList.Spline = this.Spline;
					global::TerrainMeta.Path.Powerlines.Add(pathList);
				}
			}
			else
			{
				global::PathList pathList2 = new global::PathList("Road " + global::TerrainMeta.Path.Roads.Count, list.ToArray());
				pathList2.Width = this.Width;
				pathList2.InnerFade = this.Fade * 0.5f;
				pathList2.OuterFade = this.Fade * 0.5f;
				pathList2.MeshOffset = this.Offset * 0.3f;
				pathList2.TerrainOffset = this.Offset;
				pathList2.Topology = (int)this.Topology;
				pathList2.Splat = (int)this.Splat;
				pathList2.Spline = this.Spline;
				global::TerrainMeta.Path.Roads.Add(pathList2);
			}
		}
		global::GameManager.Destroy(base.gameObject, 0f);
	}

	// Token: 0x06001CA1 RID: 7329 RVA: 0x000A07A0 File Offset: 0x0009E9A0
	protected void OnDrawGizmos()
	{
		bool flag = false;
		Vector3 a = Vector3.zero;
		IEnumerator enumerator = base.transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform = (Transform)obj;
				Vector3 position = transform.position;
				if (flag)
				{
					Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
					global::GizmosUtil.DrawWirePath(a, position, 0.5f * this.Width);
				}
				a = position;
				flag = true;
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	// Token: 0x040017BC RID: 6076
	public bool Spline = true;

	// Token: 0x040017BD RID: 6077
	public float Width;

	// Token: 0x040017BE RID: 6078
	public float Offset;

	// Token: 0x040017BF RID: 6079
	public float Fade;

	// Token: 0x040017C0 RID: 6080
	[InspectorFlags]
	public global::TerrainSplat.Enum Splat = global::TerrainSplat.Enum.Dirt;

	// Token: 0x040017C1 RID: 6081
	[InspectorFlags]
	public global::TerrainTopology.Enum Topology = global::TerrainTopology.Enum.Road;

	// Token: 0x040017C2 RID: 6082
	public global::InfrastructureType Type;
}
