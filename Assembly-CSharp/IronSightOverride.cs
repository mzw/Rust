﻿using System;
using UnityEngine;

// Token: 0x020007BA RID: 1978
public class IronSightOverride : MonoBehaviour
{
	// Token: 0x04002036 RID: 8246
	public global::IronsightAimPoint aimPoint;

	// Token: 0x04002037 RID: 8247
	public float fieldOfViewOffset = -20f;

	// Token: 0x04002038 RID: 8248
	[Tooltip("If set to 1, the FOV is set to what this override is set to. If set to 0.5 it's half way between the weapon iconsights default and this scope.")]
	public float fovBias = 0.5f;
}
