﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006AE RID: 1710
public class ItemOptionButton : MonoBehaviour
{
	// Token: 0x04001D07 RID: 7431
	public Text name;

	// Token: 0x04001D08 RID: 7432
	public Image icon;
}
