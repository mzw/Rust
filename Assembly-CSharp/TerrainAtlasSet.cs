﻿using System;
using UnityEngine;

// Token: 0x0200055E RID: 1374
[CreateAssetMenu(menuName = "Rust/Terrain Atlas Set")]
public class TerrainAtlasSet : ScriptableObject
{
	// Token: 0x06001CB7 RID: 7351 RVA: 0x000A0DB0 File Offset: 0x0009EFB0
	public void CheckReset()
	{
		if (this.splatNames == null)
		{
			this.splatNames = new string[]
			{
				"Dirt",
				"Snow",
				"Sand",
				"Rock",
				"Grass",
				"Forest",
				"Stones",
				"Gravel"
			};
		}
		else if (this.splatNames.Length != 8)
		{
			Array.Resize<string>(ref this.splatNames, 8);
		}
		if (this.albedoHighpass == null)
		{
			this.albedoHighpass = new bool[8];
		}
		else if (this.albedoHighpass.Length != 8)
		{
			Array.Resize<bool>(ref this.albedoHighpass, 8);
		}
		if (this.albedoPaths == null)
		{
			this.albedoPaths = new string[8];
		}
		else if (this.albedoPaths.Length != 8)
		{
			Array.Resize<string>(ref this.albedoPaths, 8);
		}
		if (this.defaultValues == null)
		{
			this.defaultValues = new Color[]
			{
				new Color(1f, 1f, 1f, 0.5f),
				new Color(0.5f, 0.5f, 1f, 0f),
				new Color(0.5f, 0.5f, 0.5f, 0.5f),
				Color.black
			};
		}
		else if (this.defaultValues.Length != 4)
		{
			Array.Resize<Color>(ref this.defaultValues, 4);
		}
		if (this.sourceMaps == null)
		{
			this.sourceMaps = new global::TerrainAtlasSet.SourceMapSet[4];
		}
		else if (this.sourceMaps.Length != 4)
		{
			Array.Resize<global::TerrainAtlasSet.SourceMapSet>(ref this.sourceMaps, 4);
		}
		for (int i = 0; i < 4; i++)
		{
			this.sourceMaps[i] = ((this.sourceMaps[i] == null) ? new global::TerrainAtlasSet.SourceMapSet() : this.sourceMaps[i]);
			this.sourceMaps[i].CheckReset();
		}
	}

	// Token: 0x040017D9 RID: 6105
	public const int SplatCount = 8;

	// Token: 0x040017DA RID: 6106
	public const int SplatSize = 2048;

	// Token: 0x040017DB RID: 6107
	public const int MaxSplatSize = 2047;

	// Token: 0x040017DC RID: 6108
	public const int SplatPadding = 256;

	// Token: 0x040017DD RID: 6109
	public const int AtlasSize = 8192;

	// Token: 0x040017DE RID: 6110
	public const int RegionSize = 2560;

	// Token: 0x040017DF RID: 6111
	public const int SplatsPerLine = 3;

	// Token: 0x040017E0 RID: 6112
	public const int SourceTypeCount = 4;

	// Token: 0x040017E1 RID: 6113
	public const int AtlasMipCount = 10;

	// Token: 0x040017E2 RID: 6114
	public static string[] sourceTypeNames = new string[]
	{
		"Albedo",
		"Normal",
		"Specular",
		"Height"
	};

	// Token: 0x040017E3 RID: 6115
	public static string[] sourceTypeNamesExt = new string[]
	{
		"Albedo (rgb)",
		"Normal (rgb)",
		"Specular (rgba)",
		"Height (gray)"
	};

	// Token: 0x040017E4 RID: 6116
	public static string[] sourceTypePostfix = new string[]
	{
		"_albedo",
		"_normal",
		"_specular",
		"_height"
	};

	// Token: 0x040017E5 RID: 6117
	public string[] splatNames;

	// Token: 0x040017E6 RID: 6118
	public bool[] albedoHighpass;

	// Token: 0x040017E7 RID: 6119
	public string[] albedoPaths;

	// Token: 0x040017E8 RID: 6120
	public Color[] defaultValues;

	// Token: 0x040017E9 RID: 6121
	public global::TerrainAtlasSet.SourceMapSet[] sourceMaps;

	// Token: 0x040017EA RID: 6122
	public bool generateTextureAtlases = true;

	// Token: 0x040017EB RID: 6123
	public bool generateTextureArrays;

	// Token: 0x040017EC RID: 6124
	public string splatSearchPrefix = "terrain_";

	// Token: 0x040017ED RID: 6125
	public string splatSearchFolder = "Assets/Content/Nature/Terrain";

	// Token: 0x040017EE RID: 6126
	public string splatAtlasSavePath = "Assets/Content/Nature/Terrain/Atlas/terrain_splat_atlas";

	// Token: 0x040017EF RID: 6127
	public string normalAtlasSavePath = "Assets/Content/Nature/Terrain/Atlas/terrain_normal_atlas";

	// Token: 0x0200055F RID: 1375
	public enum SourceType
	{
		// Token: 0x040017F1 RID: 6129
		ALBEDO,
		// Token: 0x040017F2 RID: 6130
		NORMAL,
		// Token: 0x040017F3 RID: 6131
		SPECULAR,
		// Token: 0x040017F4 RID: 6132
		HEIGHT,
		// Token: 0x040017F5 RID: 6133
		COUNT
	}

	// Token: 0x02000560 RID: 1376
	[Serializable]
	public class SourceMapSet
	{
		// Token: 0x06001CBA RID: 7354 RVA: 0x000A106C File Offset: 0x0009F26C
		internal void CheckReset()
		{
			if (this.maps == null)
			{
				this.maps = new Texture2D[8];
			}
			else if (this.maps.Length != 8)
			{
				Array.Resize<Texture2D>(ref this.maps, 8);
			}
		}

		// Token: 0x040017F6 RID: 6134
		public Texture2D[] maps;
	}
}
