﻿using System;
using UnityEngine;

// Token: 0x0200026A RID: 618
public class LocalPositionAnimation : MonoBehaviour, IClientComponent
{
	// Token: 0x04000B59 RID: 2905
	public Vector3 centerPosition;

	// Token: 0x04000B5A RID: 2906
	public bool worldSpace;

	// Token: 0x04000B5B RID: 2907
	public float scaleX = 1f;

	// Token: 0x04000B5C RID: 2908
	public float timeScaleX = 1f;

	// Token: 0x04000B5D RID: 2909
	public AnimationCurve movementX = new AnimationCurve();

	// Token: 0x04000B5E RID: 2910
	public float scaleY = 1f;

	// Token: 0x04000B5F RID: 2911
	public float timeScaleY = 1f;

	// Token: 0x04000B60 RID: 2912
	public AnimationCurve movementY = new AnimationCurve();

	// Token: 0x04000B61 RID: 2913
	public float scaleZ = 1f;

	// Token: 0x04000B62 RID: 2914
	public float timeScaleZ = 1f;

	// Token: 0x04000B63 RID: 2915
	public AnimationCurve movementZ = new AnimationCurve();
}
