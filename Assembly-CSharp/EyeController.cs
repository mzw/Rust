﻿using System;
using UnityEngine;

// Token: 0x02000322 RID: 802
public class EyeController : MonoBehaviour
{
	// Token: 0x06001393 RID: 5011 RVA: 0x0007315C File Offset: 0x0007135C
	public void UpdateEyes()
	{
		Vector3 vector = this.EyeTransform.position + this.EyeTransform.forward * 100f;
		Vector3 vector2 = vector;
		this.UpdateFocus(vector);
		this.UpdateFlicker();
		if (this.Focus != null)
		{
			vector2 = this.Focus.position;
			Vector3 vector3 = this.EyeTransform.position - vector;
			Vector3 vector4 = this.EyeTransform.position - vector2;
			float num = Vector3.Dot(vector3.normalized, vector4.normalized);
			if (num < 0.8f)
			{
				this.Focus = null;
			}
		}
		this.UpdateEye(this.LeftEye, vector2);
		this.UpdateEye(this.RightEye, vector2);
	}

	// Token: 0x06001394 RID: 5012 RVA: 0x00073224 File Offset: 0x00071424
	private void UpdateEye(Transform eye, Vector3 LookAt)
	{
		eye.rotation = Quaternion.LookRotation((LookAt - eye.position).normalized, this.EyeTransform.up) * Quaternion.Euler(this.Fudge) * Quaternion.Euler(this.Flicker);
	}

	// Token: 0x06001395 RID: 5013 RVA: 0x0007327C File Offset: 0x0007147C
	private void UpdateFlicker()
	{
		this.TimeToUpdateFlicker -= Time.deltaTime;
		this.Flicker = Vector3.Lerp(this.Flicker, this.FlickerTarget, Time.deltaTime * this.FlickerSpeed);
		if (this.TimeToUpdateFlicker < 0f)
		{
			this.TimeToUpdateFlicker = Random.Range(0.2f, 2f);
			this.FlickerTarget = new Vector3(Random.Range(-this.FlickerRange.x, this.FlickerRange.x), Random.Range(-this.FlickerRange.y, this.FlickerRange.y), Random.Range(-this.FlickerRange.z, this.FlickerRange.z)) * ((!this.Focus) ? 1f : 0.01f);
			this.FlickerSpeed = Random.Range(10f, 30f);
		}
	}

	// Token: 0x06001396 RID: 5014 RVA: 0x0007337C File Offset: 0x0007157C
	private void UpdateFocus(Vector3 defaultLookAtPos)
	{
	}

	// Token: 0x04000E57 RID: 3671
	public const float MaxLookDot = 0.8f;

	// Token: 0x04000E58 RID: 3672
	public bool debug;

	// Token: 0x04000E59 RID: 3673
	public Transform LeftEye;

	// Token: 0x04000E5A RID: 3674
	public Transform RightEye;

	// Token: 0x04000E5B RID: 3675
	public Transform EyeTransform;

	// Token: 0x04000E5C RID: 3676
	public Vector3 Fudge = new Vector3(0f, 90f, 0f);

	// Token: 0x04000E5D RID: 3677
	public Vector3 FlickerRange;

	// Token: 0x04000E5E RID: 3678
	private Transform Focus;

	// Token: 0x04000E5F RID: 3679
	private float FocusUpdateTime;

	// Token: 0x04000E60 RID: 3680
	private Vector3 Flicker;

	// Token: 0x04000E61 RID: 3681
	private Vector3 FlickerTarget;

	// Token: 0x04000E62 RID: 3682
	private float TimeToUpdateFlicker;

	// Token: 0x04000E63 RID: 3683
	private float FlickerSpeed;
}
