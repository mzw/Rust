﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020007B1 RID: 1969
public class PrefabPoolCollection
{
	// Token: 0x060024A4 RID: 9380 RVA: 0x000C9DD8 File Offset: 0x000C7FD8
	public void Push(GameObject instance)
	{
		global::PrefabInfo component = instance.GetComponent<global::PrefabInfo>();
		global::PrefabPool prefabPool;
		if (!this.storage.TryGetValue(component.prefabID, out prefabPool))
		{
			prefabPool = new global::PrefabPool();
			this.storage.Add(component.prefabID, prefabPool);
		}
		prefabPool.Push(component);
	}

	// Token: 0x060024A5 RID: 9381 RVA: 0x000C9E24 File Offset: 0x000C8024
	public GameObject Pop(uint id, Vector3 pos = default(Vector3), Quaternion rot = default(Quaternion))
	{
		global::PrefabPool prefabPool;
		if (this.storage.TryGetValue(id, out prefabPool))
		{
			return prefabPool.Pop(pos, rot);
		}
		return null;
	}

	// Token: 0x060024A6 RID: 9382 RVA: 0x000C9E50 File Offset: 0x000C8050
	public void Clear()
	{
		foreach (KeyValuePair<uint, global::PrefabPool> keyValuePair in this.storage)
		{
			keyValuePair.Value.Clear();
		}
	}

	// Token: 0x04002019 RID: 8217
	public Dictionary<uint, global::PrefabPool> storage = new Dictionary<uint, global::PrefabPool>();
}
