﻿using System;
using System.Collections.Generic;

// Token: 0x0200065D RID: 1629
public static class StringFormatCache
{
	// Token: 0x06002093 RID: 8339 RVA: 0x000BA014 File Offset: 0x000B8214
	public static string Get(string format, string value1)
	{
		global::StringFormatCache.Key1 key = new global::StringFormatCache.Key1(format, value1);
		string text;
		if (!global::StringFormatCache.dict1.TryGetValue(key, out text))
		{
			text = string.Format(format, value1);
			global::StringFormatCache.dict1.Add(key, text);
		}
		return text;
	}

	// Token: 0x06002094 RID: 8340 RVA: 0x000BA054 File Offset: 0x000B8254
	public static string Get(string format, string value1, string value2)
	{
		global::StringFormatCache.Key2 key = new global::StringFormatCache.Key2(format, value1, value2);
		string text;
		if (!global::StringFormatCache.dict2.TryGetValue(key, out text))
		{
			text = string.Format(format, value1, value2);
			global::StringFormatCache.dict2.Add(key, text);
		}
		return text;
	}

	// Token: 0x06002095 RID: 8341 RVA: 0x000BA094 File Offset: 0x000B8294
	public static string Get(string format, string value1, string value2, string value3)
	{
		global::StringFormatCache.Key3 key = new global::StringFormatCache.Key3(format, value1, value2, value3);
		string text;
		if (!global::StringFormatCache.dict3.TryGetValue(key, out text))
		{
			text = string.Format(format, value1, value2, value3);
			global::StringFormatCache.dict3.Add(key, text);
		}
		return text;
	}

	// Token: 0x06002096 RID: 8342 RVA: 0x000BA0D8 File Offset: 0x000B82D8
	public static string Get(string format, string value1, string value2, string value3, string value4)
	{
		global::StringFormatCache.Key4 key = new global::StringFormatCache.Key4(format, value1, value2, value3, value4);
		string text;
		if (!global::StringFormatCache.dict4.TryGetValue(key, out text))
		{
			text = string.Format(format, new object[]
			{
				value1,
				value2,
				value3,
				value4
			});
			global::StringFormatCache.dict4.Add(key, text);
		}
		return text;
	}

	// Token: 0x04001BE0 RID: 7136
	private static Dictionary<global::StringFormatCache.Key1, string> dict1 = new Dictionary<global::StringFormatCache.Key1, string>();

	// Token: 0x04001BE1 RID: 7137
	private static Dictionary<global::StringFormatCache.Key2, string> dict2 = new Dictionary<global::StringFormatCache.Key2, string>();

	// Token: 0x04001BE2 RID: 7138
	private static Dictionary<global::StringFormatCache.Key3, string> dict3 = new Dictionary<global::StringFormatCache.Key3, string>();

	// Token: 0x04001BE3 RID: 7139
	private static Dictionary<global::StringFormatCache.Key4, string> dict4 = new Dictionary<global::StringFormatCache.Key4, string>();

	// Token: 0x0200065E RID: 1630
	private struct Key1 : IEquatable<global::StringFormatCache.Key1>
	{
		// Token: 0x06002098 RID: 8344 RVA: 0x000BA15C File Offset: 0x000B835C
		public Key1(string format, string value1)
		{
			this.format = format;
			this.value1 = value1;
		}

		// Token: 0x06002099 RID: 8345 RVA: 0x000BA16C File Offset: 0x000B836C
		public override int GetHashCode()
		{
			return this.format.GetHashCode() ^ this.value1.GetHashCode();
		}

		// Token: 0x0600209A RID: 8346 RVA: 0x000BA188 File Offset: 0x000B8388
		public override bool Equals(object other)
		{
			return other is global::StringFormatCache.Key1 && this.Equals((global::StringFormatCache.Key1)other);
		}

		// Token: 0x0600209B RID: 8347 RVA: 0x000BA1A8 File Offset: 0x000B83A8
		public bool Equals(global::StringFormatCache.Key1 other)
		{
			return this.format == other.format && this.value1 == other.value1;
		}

		// Token: 0x04001BE4 RID: 7140
		public string format;

		// Token: 0x04001BE5 RID: 7141
		public string value1;
	}

	// Token: 0x0200065F RID: 1631
	private struct Key2 : IEquatable<global::StringFormatCache.Key2>
	{
		// Token: 0x0600209C RID: 8348 RVA: 0x000BA1D8 File Offset: 0x000B83D8
		public Key2(string format, string value1, string value2)
		{
			this.format = format;
			this.value1 = value1;
			this.value2 = value2;
		}

		// Token: 0x0600209D RID: 8349 RVA: 0x000BA1F0 File Offset: 0x000B83F0
		public override int GetHashCode()
		{
			return this.format.GetHashCode() ^ this.value1.GetHashCode() ^ this.value2.GetHashCode();
		}

		// Token: 0x0600209E RID: 8350 RVA: 0x000BA218 File Offset: 0x000B8418
		public override bool Equals(object other)
		{
			return other is global::StringFormatCache.Key2 && this.Equals((global::StringFormatCache.Key2)other);
		}

		// Token: 0x0600209F RID: 8351 RVA: 0x000BA238 File Offset: 0x000B8438
		public bool Equals(global::StringFormatCache.Key2 other)
		{
			return this.format == other.format && this.value1 == other.value1 && this.value2 == other.value2;
		}

		// Token: 0x04001BE6 RID: 7142
		public string format;

		// Token: 0x04001BE7 RID: 7143
		public string value1;

		// Token: 0x04001BE8 RID: 7144
		public string value2;
	}

	// Token: 0x02000660 RID: 1632
	private struct Key3 : IEquatable<global::StringFormatCache.Key3>
	{
		// Token: 0x060020A0 RID: 8352 RVA: 0x000BA288 File Offset: 0x000B8488
		public Key3(string format, string value1, string value2, string value3)
		{
			this.format = format;
			this.value1 = value1;
			this.value2 = value2;
			this.value3 = value3;
		}

		// Token: 0x060020A1 RID: 8353 RVA: 0x000BA2A8 File Offset: 0x000B84A8
		public override int GetHashCode()
		{
			return this.format.GetHashCode() ^ this.value1.GetHashCode() ^ this.value2.GetHashCode() ^ this.value3.GetHashCode();
		}

		// Token: 0x060020A2 RID: 8354 RVA: 0x000BA2DC File Offset: 0x000B84DC
		public override bool Equals(object other)
		{
			return other is global::StringFormatCache.Key3 && this.Equals((global::StringFormatCache.Key3)other);
		}

		// Token: 0x060020A3 RID: 8355 RVA: 0x000BA2FC File Offset: 0x000B84FC
		public bool Equals(global::StringFormatCache.Key3 other)
		{
			return this.format == other.format && this.value1 == other.value1 && this.value2 == other.value2 && this.value3 == other.value3;
		}

		// Token: 0x04001BE9 RID: 7145
		public string format;

		// Token: 0x04001BEA RID: 7146
		public string value1;

		// Token: 0x04001BEB RID: 7147
		public string value2;

		// Token: 0x04001BEC RID: 7148
		public string value3;
	}

	// Token: 0x02000661 RID: 1633
	private struct Key4 : IEquatable<global::StringFormatCache.Key4>
	{
		// Token: 0x060020A4 RID: 8356 RVA: 0x000BA364 File Offset: 0x000B8564
		public Key4(string format, string value1, string value2, string value3, string value4)
		{
			this.format = format;
			this.value1 = value1;
			this.value2 = value2;
			this.value3 = value3;
			this.value4 = value4;
		}

		// Token: 0x060020A5 RID: 8357 RVA: 0x000BA38C File Offset: 0x000B858C
		public override int GetHashCode()
		{
			return this.format.GetHashCode() ^ this.value1.GetHashCode() ^ this.value2.GetHashCode() ^ this.value3.GetHashCode() ^ this.value4.GetHashCode();
		}

		// Token: 0x060020A6 RID: 8358 RVA: 0x000BA3CC File Offset: 0x000B85CC
		public override bool Equals(object other)
		{
			return other is global::StringFormatCache.Key4 && this.Equals((global::StringFormatCache.Key4)other);
		}

		// Token: 0x060020A7 RID: 8359 RVA: 0x000BA3EC File Offset: 0x000B85EC
		public bool Equals(global::StringFormatCache.Key4 other)
		{
			return this.format == other.format && this.value1 == other.value1 && this.value2 == other.value2 && this.value3 == other.value3 && this.value4 == other.value4;
		}

		// Token: 0x04001BED RID: 7149
		public string format;

		// Token: 0x04001BEE RID: 7150
		public string value1;

		// Token: 0x04001BEF RID: 7151
		public string value2;

		// Token: 0x04001BF0 RID: 7152
		public string value3;

		// Token: 0x04001BF1 RID: 7153
		public string value4;
	}
}
