﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006D0 RID: 1744
public class LookatTooltip : MonoBehaviour
{
	// Token: 0x04001D98 RID: 7576
	public static bool Enabled = true;

	// Token: 0x04001D99 RID: 7577
	public Animator tooltipAnimator;

	// Token: 0x04001D9A RID: 7578
	public global::BaseEntity currentlyLookingAt;

	// Token: 0x04001D9B RID: 7579
	public Text textLabel;

	// Token: 0x04001D9C RID: 7580
	public Image icon;
}
