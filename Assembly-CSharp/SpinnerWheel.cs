﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000098 RID: 152
public class SpinnerWheel : global::Signage
{
	// Token: 0x0600099E RID: 2462 RVA: 0x00041814 File Offset: 0x0003FA14
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("SpinnerWheel.OnRpcMessage", 0.1f))
		{
			if (rpc == 949990102u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_AnyoneSpin ");
				}
				using (TimeWarning.New("RPC_AnyoneSpin", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_AnyoneSpin", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_AnyoneSpin(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_AnyoneSpin");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 358637148u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Spin ");
				}
				using (TimeWarning.New("RPC_Spin", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Spin", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Spin(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_Spin");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600099F RID: 2463 RVA: 0x00041B80 File Offset: 0x0003FD80
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.spinnerWheel = Facepunch.Pool.Get<ProtoBuf.SpinnerWheel>();
		info.msg.spinnerWheel.spin = this.wheel.rotation.eulerAngles;
	}

	// Token: 0x060009A0 RID: 2464 RVA: 0x00041BCC File Offset: 0x0003FDCC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.spinnerWheel != null)
		{
			Quaternion rotation = Quaternion.Euler(info.msg.spinnerWheel.spin);
			if (base.isServer)
			{
				this.wheel.transform.rotation = rotation;
			}
		}
	}

	// Token: 0x060009A1 RID: 2465 RVA: 0x00041C24 File Offset: 0x0003FE24
	public void Update_Server()
	{
		if (this.velocity > 0f)
		{
			float num = Mathf.Clamp(720f * this.velocity, 0f, 720f);
			this.velocity -= UnityEngine.Time.deltaTime * Mathf.Clamp(this.velocity / 2f, 0.1f, 1f);
			if (this.velocity < 0f)
			{
				this.velocity = 0f;
			}
			this.wheel.Rotate(Vector3.up, num * UnityEngine.Time.deltaTime, 1);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x060009A2 RID: 2466 RVA: 0x00041CC8 File Offset: 0x0003FEC8
	public void Update_Client()
	{
	}

	// Token: 0x060009A3 RID: 2467 RVA: 0x00041CCC File Offset: 0x0003FECC
	public void Update()
	{
		if (base.isClient)
		{
			this.Update_Client();
		}
		if (base.isServer)
		{
			this.Update_Server();
		}
	}

	// Token: 0x060009A4 RID: 2468 RVA: 0x00041CF0 File Offset: 0x0003FEF0
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_Spin(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (this.AnyoneSpin() || rpc.player.CanBuild())
		{
			Interface.CallHook("OnSpinWheel", new object[]
			{
				rpc.player,
				this
			});
			if (this.velocity > 15f)
			{
				return;
			}
			this.velocity += Random.Range(4f, 7f);
		}
	}

	// Token: 0x060009A5 RID: 2469 RVA: 0x00041D78 File Offset: 0x0003FF78
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_AnyoneSpin(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved3, rpc.read.Bit(), false);
	}

	// Token: 0x060009A6 RID: 2470 RVA: 0x00041DA4 File Offset: 0x0003FFA4
	public bool AnyoneSpin()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved3);
	}

	// Token: 0x0400046E RID: 1134
	public Transform wheel;

	// Token: 0x0400046F RID: 1135
	public float velocity;

	// Token: 0x04000470 RID: 1136
	public Quaternion targetRotation;

	// Token: 0x04000471 RID: 1137
	[Header("Sound")]
	public global::SoundDefinition spinLoopSoundDef;

	// Token: 0x04000472 RID: 1138
	public global::SoundDefinition spinStartSoundDef;

	// Token: 0x04000473 RID: 1139
	public global::SoundDefinition spinAccentSoundDef;

	// Token: 0x04000474 RID: 1140
	public float minTimeBetweenSpinAccentSounds = 0.3f;

	// Token: 0x04000475 RID: 1141
	public float spinAccentAngleDelta = 180f;

	// Token: 0x04000476 RID: 1142
	private global::Sound spinSound;

	// Token: 0x04000477 RID: 1143
	private global::SoundModulation.Modulator spinSoundGain;
}
