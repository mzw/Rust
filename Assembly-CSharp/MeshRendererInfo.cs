﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000259 RID: 601
public class MeshRendererInfo : global::ComponentInfo<MeshRenderer>
{
	// Token: 0x06001067 RID: 4199 RVA: 0x0006370C File Offset: 0x0006190C
	public override void Reset()
	{
		this.component.shadowCastingMode = this.shadows;
		if (this.material)
		{
			this.component.sharedMaterial = this.material;
		}
		this.component.GetComponent<MeshFilter>().sharedMesh = this.mesh;
	}

	// Token: 0x06001068 RID: 4200 RVA: 0x00063764 File Offset: 0x00061964
	public override void Setup()
	{
		this.shadows = this.component.shadowCastingMode;
		this.material = this.component.sharedMaterial;
		this.mesh = this.component.GetComponent<MeshFilter>().sharedMesh;
	}

	// Token: 0x04000B30 RID: 2864
	public ShadowCastingMode shadows;

	// Token: 0x04000B31 RID: 2865
	public Material material;

	// Token: 0x04000B32 RID: 2866
	public Mesh mesh;
}
