﻿using System;
using UnityEngine;

// Token: 0x02000656 RID: 1622
[CreateAssetMenu(menuName = "Rust/Skin Set Collection")]
public class SkinSetCollection : ScriptableObject
{
	// Token: 0x0600208E RID: 8334 RVA: 0x000B9FB8 File Offset: 0x000B81B8
	public int GetIndex(float MeshNumber)
	{
		return Mathf.Clamp(Mathf.FloorToInt(MeshNumber * (float)this.Skins.Length), 0, this.Skins.Length - 1);
	}

	// Token: 0x0600208F RID: 8335 RVA: 0x000B9FDC File Offset: 0x000B81DC
	public global::SkinSet Get(float MeshNumber)
	{
		return this.Skins[this.GetIndex(MeshNumber)];
	}

	// Token: 0x04001B88 RID: 7048
	public global::SkinSet[] Skins;
}
