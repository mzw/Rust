﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006ED RID: 1773
public class UIPaintableImage : MonoBehaviour
{
	// Token: 0x17000251 RID: 593
	// (get) Token: 0x060021B4 RID: 8628 RVA: 0x000BCE84 File Offset: 0x000BB084
	public RectTransform rectTransform
	{
		get
		{
			return base.transform as RectTransform;
		}
	}

	// Token: 0x04001E0C RID: 7692
	public RawImage image;

	// Token: 0x04001E0D RID: 7693
	public int texSize = 64;

	// Token: 0x04001E0E RID: 7694
	public Color clearColor = Color.clear;

	// Token: 0x04001E0F RID: 7695
	public FilterMode filterMode = 1;

	// Token: 0x04001E10 RID: 7696
	public bool mipmaps;

	// Token: 0x020006EE RID: 1774
	public enum DrawMode
	{
		// Token: 0x04001E12 RID: 7698
		AlphaBlended,
		// Token: 0x04001E13 RID: 7699
		Additive,
		// Token: 0x04001E14 RID: 7700
		Lighten,
		// Token: 0x04001E15 RID: 7701
		Erase
	}
}
