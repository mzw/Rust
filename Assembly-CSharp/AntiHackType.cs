﻿using System;

// Token: 0x02000616 RID: 1558
public enum AntiHackType
{
	// Token: 0x04001A7F RID: 6783
	None,
	// Token: 0x04001A80 RID: 6784
	NoClip,
	// Token: 0x04001A81 RID: 6785
	SpeedHack,
	// Token: 0x04001A82 RID: 6786
	FlyHack,
	// Token: 0x04001A83 RID: 6787
	ProjectileHack,
	// Token: 0x04001A84 RID: 6788
	MeleeHack,
	// Token: 0x04001A85 RID: 6789
	EyeHack,
	// Token: 0x04001A86 RID: 6790
	AttackHack,
	// Token: 0x04001A87 RID: 6791
	ReloadHack,
	// Token: 0x04001A88 RID: 6792
	CooldownHack
}
