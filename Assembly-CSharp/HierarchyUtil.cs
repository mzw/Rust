﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000792 RID: 1938
public static class HierarchyUtil
{
	// Token: 0x060023DC RID: 9180 RVA: 0x000C6704 File Offset: 0x000C4904
	public static GameObject GetRoot(string strName, bool groupActive = true, bool persistant = false)
	{
		GameObject gameObject;
		if (global::HierarchyUtil.rootDict.TryGetValue(strName, out gameObject))
		{
			if (gameObject != null)
			{
				return gameObject;
			}
			global::HierarchyUtil.rootDict.Remove(strName);
		}
		gameObject = new GameObject(strName);
		gameObject.SetActive(groupActive);
		global::HierarchyUtil.rootDict.Add(strName, gameObject);
		if (persistant)
		{
			Object.DontDestroyOnLoad(gameObject);
		}
		return gameObject;
	}

	// Token: 0x04001FAD RID: 8109
	public static Dictionary<string, GameObject> rootDict = new Dictionary<string, GameObject>(StringComparer.OrdinalIgnoreCase);
}
