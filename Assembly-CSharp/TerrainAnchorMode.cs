﻿using System;

// Token: 0x02000557 RID: 1367
public enum TerrainAnchorMode
{
	// Token: 0x040017C7 RID: 6087
	MinimizeError,
	// Token: 0x040017C8 RID: 6088
	MinimizeMovement
}
