﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x0200045B RID: 1115
public class RendererLOD : global::LODComponent, global::IBatchingHandler
{
	// Token: 0x04001350 RID: 4944
	[Horizontal(1, 0)]
	public global::RendererLOD.State[] States;

	// Token: 0x0200045C RID: 1116
	[Serializable]
	public class State
	{
		// Token: 0x04001351 RID: 4945
		public float distance;

		// Token: 0x04001352 RID: 4946
		public Renderer renderer;

		// Token: 0x04001353 RID: 4947
		[NonSerialized]
		public MeshFilter filter;

		// Token: 0x04001354 RID: 4948
		[NonSerialized]
		public ShadowCastingMode shadowMode;
	}
}
