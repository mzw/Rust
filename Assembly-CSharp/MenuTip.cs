﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000698 RID: 1688
public class MenuTip : MonoBehaviour
{
	// Token: 0x06002121 RID: 8481 RVA: 0x000BB9D0 File Offset: 0x000B9BD0
	public void OnEnable()
	{
		this.currentTipIndex = Random.Range(0, global::MenuTip.MenuTips.Length);
	}

	// Token: 0x06002122 RID: 8482 RVA: 0x000BB9E8 File Offset: 0x000B9BE8
	public void Update()
	{
		if (!global::LoadingScreen.isOpen)
		{
			return;
		}
		if (Time.realtimeSinceStartup >= this.nextTipTime)
		{
			this.currentTipIndex++;
			if (this.currentTipIndex >= global::MenuTip.MenuTips.Length)
			{
				this.currentTipIndex = 0;
			}
			this.nextTipTime = Time.realtimeSinceStartup + 6f;
			this.UpdateTip();
		}
	}

	// Token: 0x06002123 RID: 8483 RVA: 0x000BBA50 File Offset: 0x000B9C50
	public void UpdateTip()
	{
		this.text.text = global::MenuTip.MenuTips[this.currentTipIndex].translated;
		base.GetComponent<HorizontalLayoutGroup>().enabled = false;
		base.GetComponent<HorizontalLayoutGroup>().enabled = true;
	}

	// Token: 0x04001C88 RID: 7304
	public Text text;

	// Token: 0x04001C89 RID: 7305
	public global::LoadingScreen screen;

	// Token: 0x04001C8A RID: 7306
	public static global::Translate.TokenisedPhrase[] MenuTips = new global::Translate.TokenisedPhrase[]
	{
		new global::Translate.TokenisedPhrase("menutip_bag", "Don't forget to create a sleeping bag! You can pick which one to respawn at on the death screen."),
		new global::Translate.TokenisedPhrase("menutip_baggive", "You can give a sleeping bag to a steam friend."),
		new global::Translate.TokenisedPhrase("menutip_sneakanimal", "Some animals have blind spots. Sneak up from behind to get close enough to make the kill."),
		new global::Translate.TokenisedPhrase("menutip_humanmeat", "Human meat will severely dehydrate you."),
		new global::Translate.TokenisedPhrase("menutip_hammerpickup", "You can use the Hammer tool to pick up objects. Providing they are unlocked and/or opened."),
		new global::Translate.TokenisedPhrase("menutip_seedsun", "Ensure seeds are placed in full sunlight for faster growth."),
		new global::Translate.TokenisedPhrase("menutip_lakeriverdrink", "You can drink from lakes and rivers to recover a portion of your health."),
		new global::Translate.TokenisedPhrase("menutip_cookmeat", "Cook meat in a campfire to increase its healing abilities."),
		new global::Translate.TokenisedPhrase("menutip_rotatedeployables", "Rotate deployables before placing them by pressing [R]"),
		new global::Translate.TokenisedPhrase("menutip_repairblocked", "You cannot repair or upgrade building parts for 30 seconds after they've been damaged."),
		new global::Translate.TokenisedPhrase("menutip_hammerrepair", "Hit objects with your hammer to repair them, providing you have the necessary resources."),
		new global::Translate.TokenisedPhrase("menutip_altlook", "Hold [+altlook] to check your surroundings."),
		new global::Translate.TokenisedPhrase("menutip_upkeepwarning", "The larger you expand your base the more it'll cost to upkeep"),
		new global::Translate.TokenisedPhrase("menutip_report", "If you wish to report any in-game issues try pressing F7"),
		new global::Translate.TokenisedPhrase("menutip_radwash", "Submerge yourself in water and slosh around to remove radiation"),
		new global::Translate.TokenisedPhrase("menutip_switchammo", "Switch between ammo types by holding the [+reload] key"),
		new global::Translate.TokenisedPhrase("menutip_riverplants", "Edible plants are commonly found on river sides."),
		new global::Translate.TokenisedPhrase("menutip_buildwarnmonument", "Building near monuments may attract unwanted attention."),
		new global::Translate.TokenisedPhrase("menutip_vending", "Sell your unwanted items safely by crafting a vending machine."),
		new global::Translate.TokenisedPhrase("menutip_switchammo", "Switch between ammo types by holding the [+reload] key."),
		new global::Translate.TokenisedPhrase("menutip_oretip", "Stone and Ore Nodes are most commonly found around cliffs, mountains and other rock formations."),
		new global::Translate.TokenisedPhrase("menutip_crouchwalk", "Crouching allows you to move silently."),
		new global::Translate.TokenisedPhrase("menutip_accuracy", "Standing still or crouching while shooting increases accuracy."),
		new global::Translate.TokenisedPhrase("menutip_crashharvest", "You can harvest metal from helicopter and apc crash sites."),
		new global::Translate.TokenisedPhrase("menutip_canmelt", "You can melt Empty Cans in a campfire to receive Metal Fragments."),
		new global::Translate.TokenisedPhrase("menutip_stacksplit", "You can split a stack of items in half by holding [Middle Mouse] and dragging")
	};

	// Token: 0x04001C8B RID: 7307
	private int currentTipIndex;

	// Token: 0x04001C8C RID: 7308
	private float nextTipTime;
}
