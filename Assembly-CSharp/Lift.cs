﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000076 RID: 118
public class Lift : global::AnimatedBuildingBlock
{
	// Token: 0x06000835 RID: 2101 RVA: 0x00035490 File Offset: 0x00033690
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Lift.OnRpcMessage", 0.1f))
		{
			if (rpc == 137888952u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_UseLift ");
				}
				using (TimeWarning.New("RPC_UseLift", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_UseLift", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_UseLift(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_UseLift");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000836 RID: 2102 RVA: 0x00035670 File Offset: 0x00033870
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_UseLift(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (Interface.CallHook("OnLiftUse", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		this.MoveUp();
	}

	// Token: 0x06000837 RID: 2103 RVA: 0x000356B8 File Offset: 0x000338B8
	private void MoveUp()
	{
		if (base.IsOpen())
		{
			return;
		}
		if (base.IsBusy())
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, true, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000838 RID: 2104 RVA: 0x000356E4 File Offset: 0x000338E4
	private void MoveDown()
	{
		if (!base.IsOpen())
		{
			return;
		}
		if (base.IsBusy())
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, false, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000839 RID: 2105 RVA: 0x00035710 File Offset: 0x00033910
	protected override void OnAnimatorDisabled()
	{
		if (base.isServer && base.IsOpen())
		{
			base.Invoke(new Action(this.MoveDown), this.resetDelay);
		}
	}

	// Token: 0x040003DE RID: 990
	public float resetDelay = 5f;
}
