﻿using System;
using UnityEngine;

// Token: 0x020004B5 RID: 1205
[ExecuteInEditMode]
public class MaterialOverlay : MonoBehaviour
{
	// Token: 0x060019E2 RID: 6626 RVA: 0x00091584 File Offset: 0x0008F784
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (!this.material)
		{
			Graphics.Blit(source, destination);
			return;
		}
		for (int i = 0; i < this.material.passCount; i++)
		{
			Graphics.Blit(source, destination, this.material, i);
		}
	}

	// Token: 0x0400149C RID: 5276
	public Material material;
}
