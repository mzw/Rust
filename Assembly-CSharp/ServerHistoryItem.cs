﻿using System;
using Facepunch.Steamworks;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006E6 RID: 1766
public class ServerHistoryItem : MonoBehaviour
{
	// Token: 0x04001DFD RID: 7677
	private ServerList.Server serverInfo;

	// Token: 0x04001DFE RID: 7678
	public Text serverName;

	// Token: 0x04001DFF RID: 7679
	public Text players;

	// Token: 0x04001E00 RID: 7680
	public Text lastJoinDate;

	// Token: 0x04001E01 RID: 7681
	public uint order;
}
