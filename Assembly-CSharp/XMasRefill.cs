﻿using System;
using System.Collections.Generic;
using ConVar;
using Network;
using UnityEngine;

// Token: 0x020000AB RID: 171
public class XMasRefill : global::BaseEntity
{
	// Token: 0x06000A6C RID: 2668 RVA: 0x00047D08 File Offset: 0x00045F08
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("XMasRefill.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000A6D RID: 2669 RVA: 0x00047D50 File Offset: 0x00045F50
	public float GiftRadius()
	{
		return ConVar.XMas.spawnRange;
	}

	// Token: 0x06000A6E RID: 2670 RVA: 0x00047D58 File Offset: 0x00045F58
	public int GiftsPerPlayer()
	{
		return ConVar.XMas.giftsPerPlayer;
	}

	// Token: 0x06000A6F RID: 2671 RVA: 0x00047D60 File Offset: 0x00045F60
	public int GiftSpawnAttempts()
	{
		return ConVar.XMas.giftsPerPlayer * ConVar.XMas.spawnAttempts;
	}

	// Token: 0x06000A70 RID: 2672 RVA: 0x00047D70 File Offset: 0x00045F70
	public override void ServerInit()
	{
		base.ServerInit();
		if (!ConVar.XMas.enabled)
		{
			base.Invoke(new Action(this.RemoveMe), 0.1f);
			return;
		}
		this.goodKids = ((global::BasePlayer.activePlayerList == null) ? new List<global::BasePlayer>() : new List<global::BasePlayer>(global::BasePlayer.activePlayerList));
		this.stockings = ((global::Stocking.stockings == null) ? new List<global::Stocking>() : new List<global::Stocking>(global::Stocking.stockings.Values));
		base.Invoke(new Action(this.RemoveMe), 60f);
		base.InvokeRepeating(new Action(this.DistributeLoot), 3f, 0.02f);
		base.Invoke(new Action(this.SendBells), 0.5f);
	}

	// Token: 0x06000A71 RID: 2673 RVA: 0x00047E3C File Offset: 0x0004603C
	public void SendBells()
	{
		base.ClientRPC(null, "PlayBells");
	}

	// Token: 0x06000A72 RID: 2674 RVA: 0x00047E4C File Offset: 0x0004604C
	public void RemoveMe()
	{
		if (this.goodKids.Count == 0 && this.stockings.Count == 0)
		{
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		else
		{
			base.Invoke(new Action(this.RemoveMe), 60f);
		}
	}

	// Token: 0x06000A73 RID: 2675 RVA: 0x00047E9C File Offset: 0x0004609C
	public void DistributeLoot()
	{
		if (this.goodKids.Count > 0)
		{
			global::BasePlayer basePlayer = null;
			foreach (global::BasePlayer basePlayer2 in this.goodKids)
			{
				if (!basePlayer2.IsSleeping() && !basePlayer2.IsWounded() && basePlayer2.IsAlive())
				{
					basePlayer = basePlayer2;
					break;
				}
			}
			if (basePlayer)
			{
				this.DistributeGiftsForPlayer(basePlayer);
				this.goodKids.Remove(basePlayer);
			}
		}
		if (this.stockings.Count > 0)
		{
			global::Stocking stocking = this.stockings[0];
			if (stocking != null)
			{
				stocking.SpawnLoot();
			}
			this.stockings.RemoveAt(0);
		}
	}

	// Token: 0x06000A74 RID: 2676 RVA: 0x00047F88 File Offset: 0x00046188
	protected bool DropToGround(ref Vector3 pos)
	{
		int num = 1101070337;
		int num2 = 8454144;
		if (global::TerrainMeta.TopologyMap && (global::TerrainMeta.TopologyMap.GetTopology(pos) & 82048) != 0)
		{
			return false;
		}
		if (global::TerrainMeta.HeightMap && global::TerrainMeta.Collision && !global::TerrainMeta.Collision.GetIgnore(pos, 0.01f))
		{
			float height = global::TerrainMeta.HeightMap.GetHeight(pos);
			pos.y = Mathf.Max(pos.y, height);
		}
		RaycastHit raycastHit;
		if (!global::TransformUtil.GetGroundInfo(pos, out raycastHit, 80f, num, null))
		{
			return false;
		}
		int num3 = 1 << raycastHit.transform.gameObject.layer;
		if ((num3 & num2) == 0)
		{
			return false;
		}
		pos = raycastHit.point;
		return true;
	}

	// Token: 0x06000A75 RID: 2677 RVA: 0x0004807C File Offset: 0x0004627C
	public bool DistributeGiftsForPlayer(global::BasePlayer player)
	{
		int num = this.GiftsPerPlayer();
		int num2 = this.GiftSpawnAttempts();
		int num3 = 0;
		while (num3 < num2 && num > 0)
		{
			Vector2 vector = Random.insideUnitCircle * this.GiftRadius();
			Vector3 pos = player.transform.position + new Vector3(vector.x, 10f, vector.y);
			Quaternion rot = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
			if (this.DropToGround(ref pos))
			{
				string resourcePath = this.giftPrefabs[Random.Range(0, this.giftPrefabs.Length)].resourcePath;
				global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(resourcePath, pos, rot, true);
				if (baseEntity)
				{
					baseEntity.Spawn();
					num--;
				}
			}
			num3++;
		}
		return true;
	}

	// Token: 0x040004E8 RID: 1256
	public global::GameObjectRef[] giftPrefabs;

	// Token: 0x040004E9 RID: 1257
	public List<global::BasePlayer> goodKids;

	// Token: 0x040004EA RID: 1258
	public List<global::Stocking> stockings;

	// Token: 0x040004EB RID: 1259
	public AudioSource bells;
}
