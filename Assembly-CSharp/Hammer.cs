﻿using System;
using Oxide.Core;

// Token: 0x0200035A RID: 858
public class Hammer : global::BaseMelee
{
	// Token: 0x06001477 RID: 5239 RVA: 0x000774D0 File Offset: 0x000756D0
	public override bool CanHit(global::HitTest info)
	{
		return !(info.HitEntity == null) && !(info.HitEntity is global::BasePlayer) && info.HitEntity is global::BaseCombatEntity;
	}

	// Token: 0x06001478 RID: 5240 RVA: 0x00077508 File Offset: 0x00075708
	public override void DoAttackShared(global::HitInfo info)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		global::BaseCombatEntity baseCombatEntity = info.HitEntity as global::BaseCombatEntity;
		if (baseCombatEntity != null)
		{
			if (Interface.CallHook("OnHammerHit", new object[]
			{
				ownerPlayer,
				info
			}) != null)
			{
				return;
			}
			if (ownerPlayer != null && base.isServer)
			{
				using (TimeWarning.New("DoRepair", 50L))
				{
					baseCombatEntity.DoRepair(ownerPlayer);
				}
			}
		}
		if (base.isServer)
		{
			global::Effect.server.ImpactEffect(info);
		}
		else
		{
			global::Effect.client.ImpactEffect(info);
		}
	}
}
