﻿using System;

// Token: 0x02000447 RID: 1095
public class RendererGrid : SingletonComponent<global::RendererGrid>, IClientComponent
{
	// Token: 0x0400132D RID: 4909
	public static bool Paused;

	// Token: 0x0400132E RID: 4910
	public float CellSize = 50f;

	// Token: 0x0400132F RID: 4911
	public float MaxMilliseconds = 0.1f;
}
