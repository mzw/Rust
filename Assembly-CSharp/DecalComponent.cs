﻿using System;

// Token: 0x0200025D RID: 605
public abstract class DecalComponent : global::PrefabAttribute
{
	// Token: 0x06001075 RID: 4213 RVA: 0x00063C38 File Offset: 0x00061E38
	protected override Type GetIndexedType()
	{
		return typeof(global::DecalComponent);
	}
}
