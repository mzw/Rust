﻿using System;
using System.IO;
using System.Text;

// Token: 0x0200079A RID: 1946
public static class MurmurHashEx
{
	// Token: 0x0600242B RID: 9259 RVA: 0x000C75B8 File Offset: 0x000C57B8
	public static int MurmurHashSigned(this string str)
	{
		return global::MurmurHash.Signed(global::MurmurHashEx.StringToStream(str));
	}

	// Token: 0x0600242C RID: 9260 RVA: 0x000C75C8 File Offset: 0x000C57C8
	public static uint MurmurHashUnsigned(this string str)
	{
		return global::MurmurHash.Unsigned(global::MurmurHashEx.StringToStream(str));
	}

	// Token: 0x0600242D RID: 9261 RVA: 0x000C75D8 File Offset: 0x000C57D8
	private static MemoryStream StringToStream(string str)
	{
		return new MemoryStream(Encoding.UTF8.GetBytes(str ?? string.Empty));
	}
}
