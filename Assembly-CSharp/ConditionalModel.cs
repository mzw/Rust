﻿using System;
using UnityEngine;

// Token: 0x0200020A RID: 522
public class ConditionalModel : global::PrefabAttribute
{
	// Token: 0x06000F77 RID: 3959 RVA: 0x0005E770 File Offset: 0x0005C970
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		base.AttributeSetup(rootObj, name, serverside, clientside, bundling);
		this.conditions = base.GetComponentsInChildren<global::ModelConditionTest>(true);
		this.prefabObject = base.gameObject;
		this.conditionalName = (name + "/" + base.gameObject.name).ToLower();
		this.conditionalID = this.gameManager.AddPrefab(this.conditionalName, this.prefabObject);
	}

	// Token: 0x06000F78 RID: 3960 RVA: 0x0005E7E4 File Offset: 0x0005C9E4
	public bool RunTests(global::BaseEntity parent)
	{
		for (int i = 0; i < this.conditions.Length; i++)
		{
			if (!this.conditions[i].DoTest(parent))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000F79 RID: 3961 RVA: 0x0005E820 File Offset: 0x0005CA20
	public GameObject InstantiateSkin(global::BaseEntity parent)
	{
		if (!this.prefabObject)
		{
			return null;
		}
		if (!this.onServer && this.isServer)
		{
			return null;
		}
		GameObject gameObject = this.gameManager.CreatePrefab(this.conditionalName, parent.transform, false);
		gameObject.transform.localPosition = this.prefabObject.transform.localPosition;
		gameObject.transform.localRotation = this.prefabObject.transform.localRotation;
		gameObject.AwakeFromInstantiate();
		return gameObject;
	}

	// Token: 0x06000F7A RID: 3962 RVA: 0x0005E8B0 File Offset: 0x0005CAB0
	protected override Type GetIndexedType()
	{
		return typeof(global::ConditionalModel);
	}

	// Token: 0x04000A2A RID: 2602
	public bool onClient = true;

	// Token: 0x04000A2B RID: 2603
	public bool onServer = true;

	// Token: 0x04000A2C RID: 2604
	[NonSerialized]
	public global::ModelConditionTest[] conditions;

	// Token: 0x04000A2D RID: 2605
	[NonSerialized]
	public GameObject prefabObject;

	// Token: 0x04000A2E RID: 2606
	[NonSerialized]
	public string conditionalName;

	// Token: 0x04000A2F RID: 2607
	[NonSerialized]
	public uint conditionalID;
}
