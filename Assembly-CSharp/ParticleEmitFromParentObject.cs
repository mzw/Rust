﻿using System;
using UnityEngine;

// Token: 0x0200032A RID: 810
public class ParticleEmitFromParentObject : MonoBehaviour
{
	// Token: 0x04000E7B RID: 3707
	public string bonename;

	// Token: 0x04000E7C RID: 3708
	private Bounds bounds;

	// Token: 0x04000E7D RID: 3709
	private Transform bone;

	// Token: 0x04000E7E RID: 3710
	private global::BaseEntity entity;

	// Token: 0x04000E7F RID: 3711
	private float lastBoundsUpdate;
}
