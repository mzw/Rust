﻿using System;
using System.IO;
using System.Threading;

// Token: 0x02000767 RID: 1895
public static class FileEx
{
	// Token: 0x06002350 RID: 9040 RVA: 0x000C36D8 File Offset: 0x000C18D8
	public static void Backup(DirectoryInfo parent, params string[] names)
	{
		for (int i = 0; i < names.Length; i++)
		{
			names[i] = Path.Combine(parent.FullName, names[i]);
		}
		global::FileEx.Backup(names);
	}

	// Token: 0x06002351 RID: 9041 RVA: 0x000C3710 File Offset: 0x000C1910
	public static bool MoveToSafe(this FileInfo parent, string target, int retries = 10)
	{
		for (int i = 0; i < retries; i++)
		{
			try
			{
				parent.MoveTo(target);
			}
			catch (Exception)
			{
				Thread.Sleep(5);
				goto IL_21;
			}
			return true;
			IL_21:;
		}
		return false;
	}

	// Token: 0x06002352 RID: 9042 RVA: 0x000C375C File Offset: 0x000C195C
	public static void Backup(params string[] names)
	{
		for (int i = names.Length - 2; i >= 0; i--)
		{
			FileInfo fileInfo = new FileInfo(names[i]);
			FileInfo fileInfo2 = new FileInfo(names[i + 1]);
			if (fileInfo.Exists)
			{
				if (fileInfo2.Exists)
				{
					double totalHours = (DateTime.Now - fileInfo2.LastWriteTime).TotalHours;
					int num = (i != 0) ? (1 << i - 1) : 0;
					if (totalHours >= (double)num)
					{
						fileInfo2.Delete();
						fileInfo.MoveToSafe(fileInfo2.FullName, 10);
					}
				}
				else
				{
					if (!fileInfo2.Directory.Exists)
					{
						fileInfo2.Directory.Create();
					}
					fileInfo.MoveToSafe(fileInfo2.FullName, 10);
				}
			}
		}
	}
}
