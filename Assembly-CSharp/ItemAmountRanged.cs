﻿using System;
using UnityEngine;

// Token: 0x020004FE RID: 1278
[Serializable]
public class ItemAmountRanged : global::ItemAmount
{
	// Token: 0x06001B24 RID: 6948 RVA: 0x00097C50 File Offset: 0x00095E50
	public ItemAmountRanged(global::ItemDefinition item = null, float amt = 0f, float max = -1f) : base(item, amt)
	{
		this.maxAmount = max;
	}

	// Token: 0x06001B25 RID: 6949 RVA: 0x00097C6C File Offset: 0x00095E6C
	public override void OnAfterDeserialize()
	{
		base.OnAfterDeserialize();
	}

	// Token: 0x06001B26 RID: 6950 RVA: 0x00097C74 File Offset: 0x00095E74
	public override float GetAmount()
	{
		if (this.maxAmount > 0f && this.maxAmount > this.amount)
		{
			return Random.Range(this.amount, this.maxAmount);
		}
		return this.amount;
	}

	// Token: 0x040015F3 RID: 5619
	public float maxAmount = -1f;
}
