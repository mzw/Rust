﻿using System;
using Network;
using SilentOrbit.ProtocolBuffers;
using UnityEngine;

// Token: 0x0200076C RID: 1900
public static class NetworkWriteEx
{
	// Token: 0x06002359 RID: 9049 RVA: 0x000C39C4 File Offset: 0x000C1BC4
	public static void WriteObject<T>(this Write write, T obj)
	{
		if (typeof(T) == typeof(Vector3))
		{
			write.Vector3(global::GenericsUtil.Cast<T, Vector3>(obj));
			return;
		}
		if (typeof(T) == typeof(Ray))
		{
			write.Ray(global::GenericsUtil.Cast<T, Ray>(obj));
			return;
		}
		if (typeof(T) == typeof(float))
		{
			write.Float(global::GenericsUtil.Cast<T, float>(obj));
			return;
		}
		if (typeof(T) == typeof(short))
		{
			write.Int16(global::GenericsUtil.Cast<T, short>(obj));
			return;
		}
		if (typeof(T) == typeof(ushort))
		{
			write.UInt16(global::GenericsUtil.Cast<T, ushort>(obj));
			return;
		}
		if (typeof(T) == typeof(int))
		{
			write.Int32(global::GenericsUtil.Cast<T, int>(obj));
			return;
		}
		if (typeof(T) == typeof(uint))
		{
			write.UInt32(global::GenericsUtil.Cast<T, uint>(obj));
			return;
		}
		if (typeof(T) == typeof(byte[]))
		{
			write.Bytes(global::GenericsUtil.Cast<T, byte[]>(obj));
			return;
		}
		if (typeof(T) == typeof(long))
		{
			write.Int64(global::GenericsUtil.Cast<T, long>(obj));
			return;
		}
		if (typeof(T) == typeof(ulong))
		{
			write.UInt64(global::GenericsUtil.Cast<T, ulong>(obj));
			return;
		}
		if (typeof(T) == typeof(string))
		{
			write.String(global::GenericsUtil.Cast<T, string>(obj));
			return;
		}
		if (typeof(T) == typeof(sbyte))
		{
			write.Int8(global::GenericsUtil.Cast<T, sbyte>(obj));
			return;
		}
		if (typeof(T) == typeof(byte))
		{
			write.UInt8(global::GenericsUtil.Cast<T, byte>(obj));
			return;
		}
		if (typeof(T) == typeof(bool))
		{
			write.Bool(global::GenericsUtil.Cast<T, bool>(obj));
			return;
		}
		if (obj is IProto)
		{
			((IProto)((object)obj)).WriteToStream(write);
			return;
		}
		Debug.LogError(string.Concat(new object[]
		{
			"NetworkData.Write - no handler to write ",
			obj,
			" -> ",
			obj.GetType()
		}));
	}
}
