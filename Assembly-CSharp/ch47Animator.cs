﻿using System;
using UnityEngine;

// Token: 0x020000C2 RID: 194
public class ch47Animator : MonoBehaviour
{
	// Token: 0x06000AA2 RID: 2722 RVA: 0x0004890C File Offset: 0x00046B0C
	private void Start()
	{
		this.EnableBlurredRotorBlades(false);
		this.animator.SetBool("rotorblade_stop", false);
	}

	// Token: 0x06000AA3 RID: 2723 RVA: 0x00048928 File Offset: 0x00046B28
	private void Update()
	{
		this.animator.SetBool("bottomdoor", this.bottomDoorOpen);
		this.animator.SetBool("landinggear", this.landingGearDown);
		this.animator.SetBool("leftdoor", this.leftDoorOpen);
		this.animator.SetBool("rightdoor", this.rightDoorOpen);
		this.animator.SetBool("reardoor", this.rearDoorOpen);
		this.animator.SetBool("reardoor_extension", this.rearDoorExtensionOpen);
		if (this.rotorBladeSpeed >= this.blurSpeedThreshold && !this.blurredRotorBladesEnabled)
		{
			this.EnableBlurredRotorBlades(true);
		}
		else if (this.rotorBladeSpeed < this.blurSpeedThreshold && this.blurredRotorBladesEnabled)
		{
			this.EnableBlurredRotorBlades(false);
		}
		if (this.rotorBladeSpeed <= 0f)
		{
			this.animator.SetBool("rotorblade_stop", true);
		}
		else
		{
			this.animator.SetBool("rotorblade_stop", false);
		}
	}

	// Token: 0x06000AA4 RID: 2724 RVA: 0x00048A3C File Offset: 0x00046C3C
	private void LateUpdate()
	{
		float num = Time.deltaTime * this.rotorBladeSpeed * 15f;
		Vector3 localEulerAngles = this.frontRotorBlade.localEulerAngles;
		this.frontRotorBlade.localEulerAngles = new Vector3(localEulerAngles.x, localEulerAngles.y + num, localEulerAngles.z);
		localEulerAngles = this.rearRotorBlade.localEulerAngles;
		this.rearRotorBlade.localEulerAngles = new Vector3(localEulerAngles.x, localEulerAngles.y - num, localEulerAngles.z);
	}

	// Token: 0x06000AA5 RID: 2725 RVA: 0x00048AC4 File Offset: 0x00046CC4
	private void EnableBlurredRotorBlades(bool enabled)
	{
		this.blurredRotorBladesEnabled = enabled;
		foreach (SkinnedMeshRenderer skinnedMeshRenderer in this.blurredRotorBlades)
		{
			skinnedMeshRenderer.enabled = enabled;
		}
		foreach (SkinnedMeshRenderer skinnedMeshRenderer2 in this.RotorBlades)
		{
			skinnedMeshRenderer2.enabled = !enabled;
		}
	}

	// Token: 0x04000565 RID: 1381
	public Animator animator;

	// Token: 0x04000566 RID: 1382
	public bool bottomDoorOpen;

	// Token: 0x04000567 RID: 1383
	public bool landingGearDown;

	// Token: 0x04000568 RID: 1384
	public bool leftDoorOpen;

	// Token: 0x04000569 RID: 1385
	public bool rightDoorOpen;

	// Token: 0x0400056A RID: 1386
	public bool rearDoorOpen;

	// Token: 0x0400056B RID: 1387
	public bool rearDoorExtensionOpen;

	// Token: 0x0400056C RID: 1388
	public Transform rearRotorBlade;

	// Token: 0x0400056D RID: 1389
	public Transform frontRotorBlade;

	// Token: 0x0400056E RID: 1390
	public float rotorBladeSpeed;

	// Token: 0x0400056F RID: 1391
	public float wheelTurnSpeed;

	// Token: 0x04000570 RID: 1392
	public float wheelTurnAngle;

	// Token: 0x04000571 RID: 1393
	public SkinnedMeshRenderer[] blurredRotorBlades;

	// Token: 0x04000572 RID: 1394
	public SkinnedMeshRenderer[] RotorBlades;

	// Token: 0x04000573 RID: 1395
	private bool blurredRotorBladesEnabled;

	// Token: 0x04000574 RID: 1396
	public float blurSpeedThreshold = 100f;
}
