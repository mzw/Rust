﻿using System;
using Painting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

// Token: 0x0200068C RID: 1676
public class ImagePainter : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IInitializePotentialDragHandler, IEventSystemHandler
{
	// Token: 0x17000245 RID: 581
	// (get) Token: 0x06002102 RID: 8450 RVA: 0x000BB380 File Offset: 0x000B9580
	public RectTransform rectTransform
	{
		get
		{
			return base.transform as RectTransform;
		}
	}

	// Token: 0x06002103 RID: 8451 RVA: 0x000BB390 File Offset: 0x000B9590
	public virtual void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.button == 1)
		{
			return;
		}
		Vector2 position;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(this.rectTransform, eventData.position, eventData.pressEventCamera, ref position);
		this.DrawAt(position, eventData.button);
		this.pointerState[eventData.button].isDown = true;
	}

	// Token: 0x06002104 RID: 8452 RVA: 0x000BB3E4 File Offset: 0x000B95E4
	public virtual void OnPointerUp(PointerEventData eventData)
	{
		this.pointerState[eventData.button].isDown = false;
	}

	// Token: 0x06002105 RID: 8453 RVA: 0x000BB3FC File Offset: 0x000B95FC
	public virtual void OnDrag(PointerEventData eventData)
	{
		if (eventData.button == 1)
		{
			if (this.redirectRightClick)
			{
				this.redirectRightClick.SendMessage("OnDrag", eventData);
			}
			return;
		}
		Vector2 position;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(this.rectTransform, eventData.position, eventData.pressEventCamera, ref position);
		this.DrawAt(position, eventData.button);
	}

	// Token: 0x06002106 RID: 8454 RVA: 0x000BB460 File Offset: 0x000B9660
	public virtual void OnBeginDrag(PointerEventData eventData)
	{
		if (eventData.button == 1)
		{
			if (this.redirectRightClick)
			{
				this.redirectRightClick.SendMessage("OnBeginDrag", eventData);
			}
			return;
		}
	}

	// Token: 0x06002107 RID: 8455 RVA: 0x000BB490 File Offset: 0x000B9690
	public virtual void OnEndDrag(PointerEventData eventData)
	{
		if (eventData.button == 1)
		{
			if (this.redirectRightClick)
			{
				this.redirectRightClick.SendMessage("OnEndDrag", eventData);
			}
			return;
		}
	}

	// Token: 0x06002108 RID: 8456 RVA: 0x000BB4C0 File Offset: 0x000B96C0
	public virtual void OnInitializePotentialDrag(PointerEventData eventData)
	{
		if (eventData.button == 1)
		{
			if (this.redirectRightClick)
			{
				this.redirectRightClick.SendMessage("OnInitializePotentialDrag", eventData);
			}
			return;
		}
	}

	// Token: 0x06002109 RID: 8457 RVA: 0x000BB4F0 File Offset: 0x000B96F0
	private void DrawAt(Vector2 position, PointerEventData.InputButton button)
	{
		if (this.brush == null)
		{
			return;
		}
		global::ImagePainter.PointerState pointerState = this.pointerState[button];
		Vector2 vector = this.rectTransform.Unpivot(position);
		if (pointerState.isDown)
		{
			Vector2 vector2 = pointerState.lastPos - vector;
			Vector2 normalized = vector2.normalized;
			for (float num = 0f; num < vector2.magnitude; num += Mathf.Max(this.brush.spacing, 1f) * Mathf.Max(this.spacingScale, 0.1f))
			{
				this.onDrawing.Invoke(vector + num * normalized, this.brush);
			}
			pointerState.lastPos = vector;
		}
		else
		{
			this.onDrawing.Invoke(vector, this.brush);
			pointerState.lastPos = vector;
		}
	}

	// Token: 0x0600210A RID: 8458 RVA: 0x000BB5C8 File Offset: 0x000B97C8
	private void Start()
	{
	}

	// Token: 0x0600210B RID: 8459 RVA: 0x000BB5CC File Offset: 0x000B97CC
	public void UpdateBrush(Painting.Brush brush)
	{
		this.brush = brush;
	}

	// Token: 0x04001C5D RID: 7261
	public global::ImagePainter.OnDrawingEvent onDrawing = new global::ImagePainter.OnDrawingEvent();

	// Token: 0x04001C5E RID: 7262
	public MonoBehaviour redirectRightClick;

	// Token: 0x04001C5F RID: 7263
	[Tooltip("Spacing scale will depend on your texel size, tweak to what's right.")]
	public float spacingScale = 1f;

	// Token: 0x04001C60 RID: 7264
	internal Painting.Brush brush;

	// Token: 0x04001C61 RID: 7265
	internal global::ImagePainter.PointerState[] pointerState = new global::ImagePainter.PointerState[]
	{
		new global::ImagePainter.PointerState(),
		new global::ImagePainter.PointerState(),
		new global::ImagePainter.PointerState()
	};

	// Token: 0x0200068D RID: 1677
	[Serializable]
	public class OnDrawingEvent : UnityEvent<Vector2, Painting.Brush>
	{
	}

	// Token: 0x0200068E RID: 1678
	internal class PointerState
	{
		// Token: 0x04001C62 RID: 7266
		public Vector2 lastPos;

		// Token: 0x04001C63 RID: 7267
		public bool isDown;
	}
}
