﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

// Token: 0x0200057A RID: 1402
public abstract class TerrainMap3D<T> : global::TerrainMap where T : struct
{
	// Token: 0x06001D68 RID: 7528 RVA: 0x000A4854 File Offset: 0x000A2A54
	public void Push()
	{
		if (this.src != this.dst)
		{
			return;
		}
		this.dst = (T[,,])this.src.Clone();
	}

	// Token: 0x06001D69 RID: 7529 RVA: 0x000A4880 File Offset: 0x000A2A80
	public void Pop()
	{
		if (this.src == this.dst)
		{
			return;
		}
		Array.Copy(this.dst, this.src, this.src.Length);
		this.dst = this.src;
	}

	// Token: 0x06001D6A RID: 7530 RVA: 0x000A48BC File Offset: 0x000A2ABC
	public IEnumerable<T> ToEnumerable()
	{
		return this.src.Cast<T>();
	}

	// Token: 0x06001D6B RID: 7531 RVA: 0x000A48CC File Offset: 0x000A2ACC
	public int BytesPerElement()
	{
		return Marshal.SizeOf(typeof(T));
	}

	// Token: 0x06001D6C RID: 7532 RVA: 0x000A48E0 File Offset: 0x000A2AE0
	public long GetMemoryUsage()
	{
		return (long)this.BytesPerElement() * (long)this.src.Length;
	}

	// Token: 0x06001D6D RID: 7533 RVA: 0x000A48F8 File Offset: 0x000A2AF8
	public byte[] ToByteArray()
	{
		byte[] array = new byte[this.BytesPerElement() * this.src.Length];
		Buffer.BlockCopy(this.src, 0, array, 0, array.Length);
		return array;
	}

	// Token: 0x06001D6E RID: 7534 RVA: 0x000A4930 File Offset: 0x000A2B30
	public void FromByteArray(byte[] dat)
	{
		Buffer.BlockCopy(dat, 0, this.dst, 0, dat.Length);
	}

	// Token: 0x04001837 RID: 6199
	internal T[,,] src;

	// Token: 0x04001838 RID: 6200
	internal T[,,] dst;
}
