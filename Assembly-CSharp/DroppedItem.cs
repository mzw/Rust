﻿using System;
using ConVar;
using Oxide.Core;
using UnityEngine;

// Token: 0x020003ED RID: 1005
public class DroppedItem : global::WorldItem
{
	// Token: 0x06001749 RID: 5961 RVA: 0x00085824 File Offset: 0x00083A24
	public override void ServerInit()
	{
		base.ServerInit();
		base.Invoke(new Action(this.IdleDestroy), this.GetDespawnDuration());
		base.ReceiveCollisionMessages(true);
	}

	// Token: 0x0600174A RID: 5962 RVA: 0x0008584C File Offset: 0x00083A4C
	public float GetDespawnDuration()
	{
		if (this.item != null && this.item.info.quickDespawn)
		{
			return 30f;
		}
		int num = (this.item == null) ? 1 : this.item.despawnMultiplier;
		return ConVar.Server.itemdespawn * (float)num;
	}

	// Token: 0x0600174B RID: 5963 RVA: 0x000858A4 File Offset: 0x00083AA4
	public void IdleDestroy()
	{
		base.DestroyItem();
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x0600174C RID: 5964 RVA: 0x000858B4 File Offset: 0x00083AB4
	public override void OnCollision(Collision collision, global::BaseEntity hitEntity)
	{
		if (this.item == null)
		{
			return;
		}
		global::DroppedItem droppedItem = hitEntity as global::DroppedItem;
		if (droppedItem == null)
		{
			return;
		}
		if (droppedItem.item == null)
		{
			return;
		}
		if (droppedItem.item.info != this.item.info)
		{
			return;
		}
		droppedItem.OnDroppedOn(this);
	}

	// Token: 0x0600174D RID: 5965 RVA: 0x00085918 File Offset: 0x00083B18
	public void OnDroppedOn(global::DroppedItem di)
	{
		if (this.item == null)
		{
			return;
		}
		if (di.item == null)
		{
			return;
		}
		if (Interface.CallHook("CanCombineDroppedItem", new object[]
		{
			this,
			di
		}) != null)
		{
			return;
		}
		if (this.item.info.stackable <= 1)
		{
			return;
		}
		if (di.item.info != this.item.info)
		{
			return;
		}
		if (di.item.IsBlueprint() && di.item.blueprintTarget != this.item.blueprintTarget)
		{
			return;
		}
		int num = di.item.amount + this.item.amount;
		if (num > this.item.info.stackable)
		{
			return;
		}
		if (num == 0)
		{
			return;
		}
		di.DestroyItem();
		di.Kill(global::BaseNetworkable.DestroyMode.None);
		this.item.amount = num;
		this.item.MarkDirty();
		base.Invoke(new Action(this.IdleDestroy), this.GetDespawnDuration());
		global::Effect.server.Run("assets/bundled/prefabs/fx/notice/stack.world.fx.prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
	}

	// Token: 0x0600174E RID: 5966 RVA: 0x00085A4C File Offset: 0x00083C4C
	internal override void OnParentRemoved()
	{
		Rigidbody component = base.GetComponent<Rigidbody>();
		if (component == null)
		{
			base.OnParentRemoved();
			return;
		}
		Vector3 vector = base.GetEstimatedWorldPosition();
		Quaternion estimatedWorldRotation = base.GetEstimatedWorldRotation();
		base.SetParent(null, 0u);
		RaycastHit raycastHit;
		if (UnityEngine.Physics.Raycast(vector + Vector3.up * 2f, Vector3.down, ref raycastHit, 2f, 27328512) && vector.y < raycastHit.point.y)
		{
			vector += Vector3.up * 1.5f;
		}
		base.transform.position = vector;
		base.transform.rotation = estimatedWorldRotation;
		component.collisionDetectionMode = 2;
		component.isKinematic = false;
		component.useGravity = true;
		component.WakeUp();
		base.Invoke(new Action(this.IdleDestroy), this.GetDespawnDuration());
		base.Invoke(new Action(base.SwitchToFastPhysics), 5f);
	}

	// Token: 0x0600174F RID: 5967 RVA: 0x00085B50 File Offset: 0x00083D50
	public override void PostInitShared()
	{
		base.PostInitShared();
		GameObject gameObject;
		if (this.item != null && this.item.info.worldModelPrefab.isValid)
		{
			gameObject = this.item.info.worldModelPrefab.Instantiate(null);
		}
		else
		{
			gameObject = Object.Instantiate<GameObject>(this.itemModel);
		}
		gameObject.transform.SetParent(base.transform, false);
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localRotation = Quaternion.identity;
		gameObject.SetLayerRecursive(base.gameObject.layer);
		if (base.isServer)
		{
			global::WorldModel component = gameObject.GetComponent<global::WorldModel>();
			float mass = (!component) ? 1f : component.mass;
			float drag = 0.1f;
			float angularDrag = 0.1f;
			Rigidbody rigidbody = base.gameObject.AddComponent<Rigidbody>();
			rigidbody.mass = mass;
			rigidbody.drag = drag;
			rigidbody.angularDrag = angularDrag;
			rigidbody.interpolation = 0;
			ConVar.Physics.ApplyDropped(rigidbody, this);
			foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>(true))
			{
				renderer.enabled = false;
			}
		}
		if (this.item != null)
		{
			global::PhysicsEffects component2 = base.gameObject.GetComponent<global::PhysicsEffects>();
			if (component2 != null)
			{
				component2.entity = this;
				if (this.item.info.physImpactSoundDef != null)
				{
					component2.physImpactSoundDef = this.item.info.physImpactSoundDef;
				}
			}
		}
		gameObject.SetActive(true);
	}

	// Token: 0x040011EC RID: 4588
	[Header("DroppedItem")]
	public GameObject itemModel;
}
