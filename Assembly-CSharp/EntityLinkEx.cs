﻿using System;
using System.Collections.Generic;
using Facepunch;

// Token: 0x02000385 RID: 901
public static class EntityLinkEx
{
	// Token: 0x06001568 RID: 5480 RVA: 0x0007B3DC File Offset: 0x000795DC
	public static void FreeLinks(this List<global::EntityLink> links)
	{
		for (int i = 0; i < links.Count; i++)
		{
			global::EntityLink entityLink = links[i];
			entityLink.Clear();
			Pool.Free<global::EntityLink>(ref entityLink);
		}
		links.Clear();
	}

	// Token: 0x06001569 RID: 5481 RVA: 0x0007B41C File Offset: 0x0007961C
	public static void ClearLinks(this List<global::EntityLink> links)
	{
		for (int i = 0; i < links.Count; i++)
		{
			links[i].Clear();
		}
	}

	// Token: 0x0600156A RID: 5482 RVA: 0x0007B44C File Offset: 0x0007964C
	public static void AddLinks(this List<global::EntityLink> links, global::BaseEntity entity, global::Socket_Base[] sockets)
	{
		foreach (global::Socket_Base socket in sockets)
		{
			global::EntityLink entityLink = Pool.Get<global::EntityLink>();
			entityLink.Setup(entity, socket);
			links.Add(entityLink);
		}
	}
}
