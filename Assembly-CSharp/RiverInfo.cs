﻿using System;
using UnityEngine;

// Token: 0x02000477 RID: 1143
public class RiverInfo : MonoBehaviour
{
	// Token: 0x060018ED RID: 6381 RVA: 0x0008C4CC File Offset: 0x0008A6CC
	protected void Awake()
	{
		if (global::TerrainMeta.Path)
		{
			global::TerrainMeta.Path.RiverObjs.Add(this);
		}
	}
}
