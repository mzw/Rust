﻿using System;
using UnityEngine;

// Token: 0x02000470 RID: 1136
[CreateAssetMenu(menuName = "Rust/LazyAim Properties")]
public class LazyAimProperties : ScriptableObject
{
	// Token: 0x04001391 RID: 5009
	[Range(0f, 10f)]
	public float snapStrength = 6f;

	// Token: 0x04001392 RID: 5010
	[Range(0f, 45f)]
	public float deadzoneAngle = 1f;
}
