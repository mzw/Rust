﻿using System;
using UnityEngine;

// Token: 0x02000294 RID: 660
public struct ColliderKey : IEquatable<global::ColliderKey>
{
	// Token: 0x06001120 RID: 4384 RVA: 0x00066618 File Offset: 0x00064818
	public ColliderKey(PhysicMaterial material, int layer)
	{
		this.material = material;
		this.layer = layer;
	}

	// Token: 0x06001121 RID: 4385 RVA: 0x00066628 File Offset: 0x00064828
	public ColliderKey(Collider collider)
	{
		this.material = collider.sharedMaterial;
		this.layer = collider.gameObject.layer;
	}

	// Token: 0x06001122 RID: 4386 RVA: 0x00066648 File Offset: 0x00064848
	public ColliderKey(global::ColliderBatch batch)
	{
		this.material = batch.BatchCollider.sharedMaterial;
		this.layer = batch.BatchCollider.gameObject.layer;
	}

	// Token: 0x06001123 RID: 4387 RVA: 0x00066674 File Offset: 0x00064874
	public override int GetHashCode()
	{
		return this.material.GetHashCode() ^ this.layer.GetHashCode();
	}

	// Token: 0x06001124 RID: 4388 RVA: 0x00066694 File Offset: 0x00064894
	public override bool Equals(object other)
	{
		return other is global::ColliderKey && this.Equals((global::ColliderKey)other);
	}

	// Token: 0x06001125 RID: 4389 RVA: 0x000666B4 File Offset: 0x000648B4
	public bool Equals(global::ColliderKey other)
	{
		return this.material == other.material && this.layer == other.layer;
	}

	// Token: 0x04000C2D RID: 3117
	public PhysicMaterial material;

	// Token: 0x04000C2E RID: 3118
	public int layer;
}
