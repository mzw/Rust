﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020004FF RID: 1279
public class ItemManager
{
	// Token: 0x06001B28 RID: 6952 RVA: 0x00097CB8 File Offset: 0x00095EB8
	public static void InvalidateWorkshopSkinCache()
	{
		if (global::ItemManager.itemList == null)
		{
			return;
		}
		foreach (global::ItemDefinition itemDefinition in global::ItemManager.itemList)
		{
			itemDefinition.InvalidateWorkshopSkinCache();
		}
	}

	// Token: 0x06001B29 RID: 6953 RVA: 0x00097D20 File Offset: 0x00095F20
	public static void Initialize()
	{
		if (global::ItemManager.itemList != null)
		{
			return;
		}
		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();
		IEnumerable<GameObject> source = global::FileSystem.Load<global::ObjectList>("Assets/items.asset", true).objects.Cast<GameObject>();
		if (stopwatch.Elapsed.TotalSeconds > 1.0)
		{
			Debug.Log("Loading Items Took: " + (stopwatch.Elapsed.TotalMilliseconds / 1000.0).ToString() + " seconds");
		}
		List<global::ItemDefinition> list = (from x in source
		select x.GetComponent<global::ItemDefinition>() into x
		where x != null
		select x).ToList<global::ItemDefinition>();
		List<global::ItemBlueprint> list2 = (from x in source
		select x.GetComponent<global::ItemBlueprint>() into x
		where x != null && x.userCraftable
		select x).ToList<global::ItemBlueprint>();
		Dictionary<int, global::ItemDefinition> dictionary = new Dictionary<int, global::ItemDefinition>();
		foreach (global::ItemDefinition itemDefinition in list)
		{
			itemDefinition.Initialize(list);
			if (dictionary.ContainsKey(itemDefinition.itemid))
			{
				global::ItemDefinition itemDefinition2 = dictionary[itemDefinition.itemid];
				Debug.LogWarning(string.Concat(new object[]
				{
					"Item ID duplicate ",
					itemDefinition.itemid,
					" (",
					itemDefinition.name,
					") - have you given your items unique shortnames?"
				}), itemDefinition.gameObject);
				Debug.LogWarning("Other item is " + itemDefinition2.name, itemDefinition2);
			}
			else
			{
				dictionary.Add(itemDefinition.itemid, itemDefinition);
			}
		}
		stopwatch.Stop();
		if (stopwatch.Elapsed.TotalSeconds > 1.0)
		{
			Debug.Log(string.Concat(new string[]
			{
				"Building Items Took: ",
				(stopwatch.Elapsed.TotalMilliseconds / 1000.0).ToString(),
				" seconds / Items: ",
				list.Count.ToString(),
				" / Blueprints: ",
				list2.Count.ToString()
			}));
		}
		global::ItemManager.defaultBlueprints = (from x in list2
		where !x.NeedsSteamItem && x.defaultBlueprint
		select x.targetItem.itemid).ToArray<int>();
		global::ItemManager.itemList = list;
		global::ItemManager.bpList = list2;
		global::ItemManager.itemDictionary = dictionary;
	}

	// Token: 0x06001B2A RID: 6954 RVA: 0x00098044 File Offset: 0x00096244
	public static global::Item CreateByName(string strName, int iAmount = 1, ulong skin = 0UL)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.itemList.Find((global::ItemDefinition x) => x.shortname == strName);
		if (itemDefinition == null)
		{
			return null;
		}
		return global::ItemManager.CreateByItemID(itemDefinition.itemid, iAmount, skin);
	}

	// Token: 0x06001B2B RID: 6955 RVA: 0x00098090 File Offset: 0x00096290
	public static global::Item CreateByPartialName(string strName, int iAmount = 1)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.itemList.Find((global::ItemDefinition x) => x.shortname == strName);
		if (itemDefinition == null)
		{
			itemDefinition = global::ItemManager.itemList.Find((global::ItemDefinition x) => StringEx.Contains(x.shortname, strName, CompareOptions.IgnoreCase));
		}
		if (itemDefinition == null)
		{
			return null;
		}
		return global::ItemManager.CreateByItemID(itemDefinition.itemid, iAmount, 0UL);
	}

	// Token: 0x06001B2C RID: 6956 RVA: 0x00098100 File Offset: 0x00096300
	public static global::Item CreateByItemID(int itemID, int iAmount = 1, ulong skin = 0UL)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(itemID);
		if (itemDefinition == null)
		{
			return null;
		}
		return global::ItemManager.Create(itemDefinition, iAmount, skin);
	}

	// Token: 0x06001B2D RID: 6957 RVA: 0x0009812C File Offset: 0x0009632C
	public static global::Item Create(global::ItemDefinition template, int iAmount = 1, ulong skin = 0UL)
	{
		if (template == null)
		{
			Debug.LogWarning("Creating invalid/missing item!");
			return null;
		}
		global::Item item = new global::Item();
		item.isServer = true;
		if (iAmount <= 0)
		{
			Debug.LogError("Creating item with less than 1 amount! (" + template.displayName.english + ")");
			return null;
		}
		item.info = template;
		item.amount = iAmount;
		item.skin = skin;
		item.Initialize(template);
		return item;
	}

	// Token: 0x06001B2E RID: 6958 RVA: 0x000981A4 File Offset: 0x000963A4
	public static global::Item Load(ProtoBuf.Item load, global::Item created, bool isServer)
	{
		if (created == null)
		{
			created = new global::Item();
		}
		created.isServer = isServer;
		created.Load(load);
		if (created.info == null)
		{
			Debug.LogWarning("Item loading failed - item is invalid");
			return null;
		}
		return created;
	}

	// Token: 0x06001B2F RID: 6959 RVA: 0x000981E0 File Offset: 0x000963E0
	public static global::ItemDefinition FindItemDefinition(int itemID)
	{
		global::ItemDefinition result = null;
		global::ItemManager.itemDictionary.TryGetValue(itemID, out result);
		return result;
	}

	// Token: 0x06001B30 RID: 6960 RVA: 0x00098200 File Offset: 0x00096400
	public static global::ItemDefinition FindItemDefinition(string shortName)
	{
		global::ItemManager.Initialize();
		for (int i = 0; i < global::ItemManager.itemList.Count; i++)
		{
			if (global::ItemManager.itemList[i].shortname == shortName)
			{
				return global::ItemManager.itemList[i];
			}
		}
		return null;
	}

	// Token: 0x06001B31 RID: 6961 RVA: 0x00098258 File Offset: 0x00096458
	public static global::ItemBlueprint FindBlueprint(global::ItemDefinition item)
	{
		return item.GetComponent<global::ItemBlueprint>();
	}

	// Token: 0x06001B32 RID: 6962 RVA: 0x00098260 File Offset: 0x00096460
	public static List<global::ItemDefinition> GetItemDefinitions()
	{
		global::ItemManager.Initialize();
		return global::ItemManager.itemList;
	}

	// Token: 0x06001B33 RID: 6963 RVA: 0x0009826C File Offset: 0x0009646C
	public static List<global::ItemBlueprint> GetBlueprints()
	{
		global::ItemManager.Initialize();
		return global::ItemManager.bpList;
	}

	// Token: 0x06001B34 RID: 6964 RVA: 0x00098278 File Offset: 0x00096478
	public static void DoRemoves()
	{
		using (TimeWarning.New("DoRemoves", 0.1f))
		{
			for (int i = 0; i < global::ItemManager.ItemRemoves.Count; i++)
			{
				if (global::ItemManager.ItemRemoves[i].time <= Time.time)
				{
					global::Item item = global::ItemManager.ItemRemoves[i].item;
					global::ItemManager.ItemRemoves.RemoveAt(i);
					item.DoRemove();
				}
			}
		}
	}

	// Token: 0x06001B35 RID: 6965 RVA: 0x0009831C File Offset: 0x0009651C
	public static void Heartbeat()
	{
		global::ItemManager.DoRemoves();
	}

	// Token: 0x06001B36 RID: 6966 RVA: 0x00098324 File Offset: 0x00096524
	public static void RemoveItem(global::Item item, float fTime = 0f)
	{
		Assert.IsTrue(item.isServer, "RemoveItem: Removing a client item!");
		global::ItemManager.ItemRemove item2 = default(global::ItemManager.ItemRemove);
		item2.item = item;
		item2.time = Time.time + fTime;
		global::ItemManager.ItemRemoves.Add(item2);
	}

	// Token: 0x040015F4 RID: 5620
	public static List<global::ItemDefinition> itemList;

	// Token: 0x040015F5 RID: 5621
	public static Dictionary<int, global::ItemDefinition> itemDictionary;

	// Token: 0x040015F6 RID: 5622
	public static List<global::ItemBlueprint> bpList;

	// Token: 0x040015F7 RID: 5623
	public static int[] defaultBlueprints;

	// Token: 0x040015F8 RID: 5624
	private static List<global::ItemManager.ItemRemove> ItemRemoves = new List<global::ItemManager.ItemRemove>();

	// Token: 0x02000500 RID: 1280
	private struct ItemRemove
	{
		// Token: 0x040015FF RID: 5631
		public global::Item item;

		// Token: 0x04001600 RID: 5632
		public float time;
	}
}
