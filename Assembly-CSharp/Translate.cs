﻿using System;
using Facepunch;
using UnityEngine;

// Token: 0x02000286 RID: 646
public static class Translate
{
	// Token: 0x060010CA RID: 4298 RVA: 0x00064D60 File Offset: 0x00062F60
	public static string TranslateMouseButton(string mouseButton)
	{
		if (mouseButton == "mouse0")
		{
			return "Left Mouse";
		}
		if (mouseButton == "mouse1")
		{
			return "Right Mouse";
		}
		if (mouseButton == "mouse2")
		{
			return "Center Mouse";
		}
		return mouseButton;
	}

	// Token: 0x02000287 RID: 647
	[Serializable]
	public class Phrase
	{
		// Token: 0x060010CB RID: 4299 RVA: 0x00064DB0 File Offset: 0x00062FB0
		public Phrase(string t = "", string eng = "")
		{
			this.token = t;
			this.english = eng;
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x060010CC RID: 4300 RVA: 0x00064DC8 File Offset: 0x00062FC8
		public virtual string translated
		{
			get
			{
				if (string.IsNullOrEmpty(this.token))
				{
					return this.english;
				}
				return this.english;
			}
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x00064DE8 File Offset: 0x00062FE8
		public bool IsValid()
		{
			return !string.IsNullOrEmpty(this.token);
		}

		// Token: 0x04000BF1 RID: 3057
		public string token;

		// Token: 0x04000BF2 RID: 3058
		[TextArea]
		public string english;
	}

	// Token: 0x02000288 RID: 648
	[Serializable]
	public class TokenisedPhrase : global::Translate.Phrase
	{
		// Token: 0x060010CE RID: 4302 RVA: 0x00064DF8 File Offset: 0x00062FF8
		public TokenisedPhrase(string t = "", string eng = "") : base(t, eng)
		{
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x060010CF RID: 4303 RVA: 0x00064E04 File Offset: 0x00063004
		public override string translated
		{
			get
			{
				string text = base.translated;
				text = text.Replace("[inventory.toggle]", string.Format("[{0}]", Input.GetButtonWithBind("inventory.toggle").ToUpper()));
				text = text.Replace("[inventory.togglecrafting]", string.Format("[{0}]", Input.GetButtonWithBind("inventory.togglecrafting").ToUpper()));
				text = text.Replace("[+map]", string.Format("[{0}]", Input.GetButtonWithBind("+map").ToUpper()));
				text = text.Replace("[inventory.examineheld]", string.Format("[{0}]", Input.GetButtonWithBind("inventory.examineheld").ToUpper()));
				text = text.Replace("[slot2]", string.Format("[{0}]", Input.GetButtonWithBind("+slot2").ToUpper()));
				text = text.Replace("[attack]", string.Format("[{0}]", global::Translate.TranslateMouseButton(Input.GetButtonWithBind("+attack")).ToUpper()));
				text = text.Replace("[attack2]", string.Format("[{0}]", global::Translate.TranslateMouseButton(Input.GetButtonWithBind("+attack2")).ToUpper()));
				text = text.Replace("[+use]", string.Format("[{0}]", global::Translate.TranslateMouseButton(Input.GetButtonWithBind("+use")).ToUpper()));
				text = text.Replace("[+altlook]", string.Format("[{0}]", global::Translate.TranslateMouseButton(Input.GetButtonWithBind("+altlook")).ToUpper()));
				text = text.Replace("[+reload]", string.Format("[{0}]", global::Translate.TranslateMouseButton(Input.GetButtonWithBind("+reload")).ToUpper()));
				return text.Replace("[+voice]", string.Format("[{0}]", global::Translate.TranslateMouseButton(Input.GetButtonWithBind("+voice")).ToUpper()));
			}
		}
	}
}
