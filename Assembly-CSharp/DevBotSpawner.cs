﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020002FB RID: 763
public class DevBotSpawner : FacepunchBehaviour
{
	// Token: 0x06001344 RID: 4932 RVA: 0x000718B8 File Offset: 0x0006FAB8
	public bool HasFreePopulation()
	{
		for (int i = this._spawned.Count - 1; i >= 0; i--)
		{
			global::BaseEntity baseEntity = this._spawned[i];
			if (baseEntity == null || baseEntity.Health() <= 0f)
			{
				this._spawned.Remove(baseEntity);
			}
		}
		return this._spawned.Count < this.maxPopulation;
	}

	// Token: 0x06001345 RID: 4933 RVA: 0x00071934 File Offset: 0x0006FB34
	public void SpawnBot()
	{
		while (this.HasFreePopulation())
		{
			Vector3 position = this.waypoints[0].position;
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.bot.resourcePath, position, default(Quaternion), true);
			if (baseEntity == null)
			{
				return;
			}
			this._spawned.Add(baseEntity);
			baseEntity.SendMessage("SetWaypoints", this.waypoints, 1);
			baseEntity.Spawn();
		}
	}

	// Token: 0x06001346 RID: 4934 RVA: 0x000719B4 File Offset: 0x0006FBB4
	public void Start()
	{
		this.waypoints = this.waypointParent.GetComponentsInChildren<Transform>();
		base.InvokeRepeating(new Action(this.SpawnBot), 5f, this.spawnRate);
	}

	// Token: 0x04000DF4 RID: 3572
	public global::GameObjectRef bot;

	// Token: 0x04000DF5 RID: 3573
	public Transform waypointParent;

	// Token: 0x04000DF6 RID: 3574
	public bool autoSelectLatestSpawnedGameObject = true;

	// Token: 0x04000DF7 RID: 3575
	public float spawnRate = 1f;

	// Token: 0x04000DF8 RID: 3576
	public int maxPopulation = 1;

	// Token: 0x04000DF9 RID: 3577
	private Transform[] waypoints;

	// Token: 0x04000DFA RID: 3578
	private List<global::BaseEntity> _spawned = new List<global::BaseEntity>();
}
