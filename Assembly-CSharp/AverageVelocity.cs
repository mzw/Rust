﻿using System;
using UnityEngine;

// Token: 0x02000234 RID: 564
public class AverageVelocity
{
	// Token: 0x0600100E RID: 4110 RVA: 0x00061910 File Offset: 0x0005FB10
	public void Record(Vector3 newPos)
	{
		float num = Time.time - this.time;
		if (num < 0.1f)
		{
			return;
		}
		if (this.pos.sqrMagnitude > 0f)
		{
			Vector3 vector = newPos - this.pos;
			this.averageVelocity = vector * (1f / num);
			this.averageSpeed = this.averageVelocity.magnitude;
		}
		this.time = Time.time;
		this.pos = newPos;
	}

	// Token: 0x17000112 RID: 274
	// (get) Token: 0x0600100F RID: 4111 RVA: 0x00061990 File Offset: 0x0005FB90
	public float Speed
	{
		get
		{
			return this.averageSpeed;
		}
	}

	// Token: 0x17000113 RID: 275
	// (get) Token: 0x06001010 RID: 4112 RVA: 0x00061998 File Offset: 0x0005FB98
	public Vector3 Average
	{
		get
		{
			return this.averageVelocity;
		}
	}

	// Token: 0x04000AC1 RID: 2753
	private Vector3 pos;

	// Token: 0x04000AC2 RID: 2754
	private float time;

	// Token: 0x04000AC3 RID: 2755
	private float lastEntry;

	// Token: 0x04000AC4 RID: 2756
	private float averageSpeed;

	// Token: 0x04000AC5 RID: 2757
	private Vector3 averageVelocity;
}
