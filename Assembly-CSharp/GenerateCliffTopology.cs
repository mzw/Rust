﻿using System;
using UnityEngine;

// Token: 0x0200059A RID: 1434
public class GenerateCliffTopology : global::ProceduralComponent
{
	// Token: 0x06001E2E RID: 7726 RVA: 0x000A7B14 File Offset: 0x000A5D14
	public static void Process(int x, int z)
	{
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		float normZ = topologyMap.Coordinate(z);
		float normX = topologyMap.Coordinate(x);
		int topology = topologyMap.GetTopology(x, z);
		if ((topology & 8389632) == 0)
		{
			float slope = global::TerrainMeta.HeightMap.GetSlope(normX, normZ);
			float splat = global::TerrainMeta.SplatMap.GetSplat(normX, normZ, 8);
			if (slope > 40f || splat > 0.4f)
			{
				topologyMap.AddTopology(x, z, 2);
			}
			else if (slope < 20f && splat < 0.2f)
			{
				topologyMap.RemoveTopology(x, z, 2);
			}
		}
	}

	// Token: 0x06001E2F RID: 7727 RVA: 0x000A7BB0 File Offset: 0x000A5DB0
	private static void Process(int x, int z, bool keepExisting)
	{
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		float normZ = topologyMap.Coordinate(z);
		float normX = topologyMap.Coordinate(x);
		int topology = topologyMap.GetTopology(x, z);
		if ((topology & 8389632) == 0)
		{
			float slope = global::TerrainMeta.HeightMap.GetSlope(normX, normZ);
			float splat = global::TerrainMeta.SplatMap.GetSplat(normX, normZ, 8);
			if (slope > 40f || splat > 0.4f)
			{
				topologyMap.AddTopology(x, z, 2);
			}
			else if (!keepExisting && slope < 20f && splat < 0.2f && (topology & 55296) != 0)
			{
				topologyMap.RemoveTopology(x, z, 2);
			}
		}
	}

	// Token: 0x06001E30 RID: 7728 RVA: 0x000A7C60 File Offset: 0x000A5E60
	public override void Process(uint seed)
	{
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		int topores = topologyMap.res;
		Parallel.For(0, topores, delegate(int z)
		{
			for (int i = 0; i < topores; i++)
			{
				global::GenerateCliffTopology.Process(i, z, this.KeepExisting);
			}
		});
		int[,] map = topologyMap.dst;
		global::ImageProcessing.Dilate2D(map, 4194306, 1, delegate(int x, int y)
		{
			if ((map[x, y] & 2) == 0)
			{
				map[x, y] |= 4194304;
			}
		});
	}

	// Token: 0x040018C2 RID: 6338
	public bool KeepExisting = true;

	// Token: 0x040018C3 RID: 6339
	private const int filter = 8389632;

	// Token: 0x040018C4 RID: 6340
	private const int filter_del = 55296;
}
