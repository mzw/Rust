﻿using System;
using UnityEngine;

// Token: 0x02000484 RID: 1156
public class SceneToPrefab : MonoBehaviour, IEditorComponent
{
	// Token: 0x040013D8 RID: 5080
	public bool flattenHierarchy;

	// Token: 0x040013D9 RID: 5081
	public GameObject outputPrefab;
}
