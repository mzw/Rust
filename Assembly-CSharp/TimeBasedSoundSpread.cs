﻿using System;
using UnityEngine;

// Token: 0x02000205 RID: 517
public class TimeBasedSoundSpread : global::SoundModifier
{
	// Token: 0x04000A1C RID: 2588
	public AnimationCurve spreadCurve;

	// Token: 0x04000A1D RID: 2589
	public AnimationCurve wanderIntensityCurve;
}
