﻿using System;
using UnityEngine;

// Token: 0x020007BE RID: 1982
public class RandomParameterNumberFloat : StateMachineBehaviour
{
	// Token: 0x060024CD RID: 9421 RVA: 0x000CA654 File Offset: 0x000C8854
	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (string.IsNullOrEmpty(this.parameterName))
		{
			return;
		}
		animator.SetFloat(this.parameterName, Mathf.Floor(Random.Range((float)this.min, (float)this.max + 0.5f)));
	}

	// Token: 0x0400204A RID: 8266
	public string parameterName;

	// Token: 0x0400204B RID: 8267
	public int min;

	// Token: 0x0400204C RID: 8268
	public int max;
}
