﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x020006FF RID: 1791
public class ScrollRectZoom : MonoBehaviour, IScrollHandler, IEventSystemHandler
{
	// Token: 0x1700026A RID: 618
	// (get) Token: 0x06002228 RID: 8744 RVA: 0x000BFEE8 File Offset: 0x000BE0E8
	public RectTransform rectTransform
	{
		get
		{
			return this.scrollRect.transform as RectTransform;
		}
	}

	// Token: 0x06002229 RID: 8745 RVA: 0x000BFEFC File Offset: 0x000BE0FC
	private void OnEnable()
	{
		this.SetZoom(this.zoom);
	}

	// Token: 0x0600222A RID: 8746 RVA: 0x000BFF0C File Offset: 0x000BE10C
	public void OnScroll(PointerEventData data)
	{
		this.velocity += data.scrollDelta.y * 0.001f;
		this.velocity = Mathf.Clamp(this.velocity, -1f, 1f);
	}

	// Token: 0x0600222B RID: 8747 RVA: 0x000BFF58 File Offset: 0x000BE158
	private void Update()
	{
		this.velocity = Mathf.Lerp(this.velocity, 0f, Time.deltaTime * 10f);
		this.SetZoom(this.zoom + this.velocity);
	}

	// Token: 0x0600222C RID: 8748 RVA: 0x000BFF90 File Offset: 0x000BE190
	private void SetZoom(float z)
	{
		z = Mathf.Clamp(z, this.min, this.max);
		if (z == this.zoom)
		{
			return;
		}
		this.zoom = z;
		Rect rect = (this.scrollRect.transform as RectTransform).rect;
		Vector2 vector = this.scrollRect.content.rect.size * this.zoom;
		Vector2 normalizedPosition = this.scrollRect.normalizedPosition;
		if (vector.x < rect.width)
		{
			this.zoom = rect.width / this.scrollRect.content.rect.size.x;
		}
		if (vector.y < rect.height)
		{
			this.zoom = rect.height / this.scrollRect.content.rect.size.y;
		}
		this.scrollRect.content.localScale = Vector3.one * this.zoom;
		this.scrollRect.normalizedPosition = normalizedPosition;
	}

	// Token: 0x04001EA5 RID: 7845
	public UnityEngine.UI.ScrollRectEx scrollRect;

	// Token: 0x04001EA6 RID: 7846
	public float zoom = 1f;

	// Token: 0x04001EA7 RID: 7847
	public bool smooth = true;

	// Token: 0x04001EA8 RID: 7848
	public float max = 1.5f;

	// Token: 0x04001EA9 RID: 7849
	public float min = 0.5f;

	// Token: 0x04001EAA RID: 7850
	public float velocity;
}
