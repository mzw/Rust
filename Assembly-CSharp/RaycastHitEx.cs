﻿using System;
using UnityEngine;

// Token: 0x0200076F RID: 1903
public static class RaycastHitEx
{
	// Token: 0x0600235F RID: 9055 RVA: 0x000C3DA0 File Offset: 0x000C1FA0
	public static Transform GetTransform(this RaycastHit hit)
	{
		if (hit.triangleIndex < 0)
		{
			return hit.transform;
		}
		if (!hit.transform.CompareTag("MeshColliderBatch"))
		{
			return hit.transform;
		}
		global::MeshColliderBatch component = hit.transform.GetComponent<global::MeshColliderBatch>();
		if (!component)
		{
			return hit.transform;
		}
		Transform transform = component.LookupTransform(hit.triangleIndex);
		if (!transform)
		{
			return hit.transform;
		}
		return transform;
	}

	// Token: 0x06002360 RID: 9056 RVA: 0x000C3E24 File Offset: 0x000C2024
	public static Rigidbody GetRigidbody(this RaycastHit hit)
	{
		if (hit.triangleIndex < 0)
		{
			return hit.rigidbody;
		}
		if (!hit.transform.CompareTag("MeshColliderBatch"))
		{
			return hit.rigidbody;
		}
		global::MeshColliderBatch component = hit.transform.GetComponent<global::MeshColliderBatch>();
		if (!component)
		{
			return hit.rigidbody;
		}
		Rigidbody rigidbody = component.LookupRigidbody(hit.triangleIndex);
		if (!rigidbody)
		{
			return hit.rigidbody;
		}
		return rigidbody;
	}

	// Token: 0x06002361 RID: 9057 RVA: 0x000C3EA8 File Offset: 0x000C20A8
	public static Collider GetCollider(this RaycastHit hit)
	{
		if (hit.triangleIndex < 0)
		{
			return hit.collider;
		}
		if (!hit.transform.CompareTag("MeshColliderBatch"))
		{
			return hit.collider;
		}
		global::MeshColliderBatch component = hit.transform.GetComponent<global::MeshColliderBatch>();
		if (!component)
		{
			return hit.collider;
		}
		Collider collider = component.LookupCollider(hit.triangleIndex);
		if (!collider)
		{
			return hit.collider;
		}
		return collider;
	}

	// Token: 0x06002362 RID: 9058 RVA: 0x000C3F2C File Offset: 0x000C212C
	public static global::BaseEntity GetEntity(this RaycastHit hit)
	{
		return hit.GetTransform().gameObject.ToBaseEntity();
	}
}
