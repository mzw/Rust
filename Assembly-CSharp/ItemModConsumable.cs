﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020004DD RID: 1245
public class ItemModConsumable : MonoBehaviour
{
	// Token: 0x04001581 RID: 5505
	public int amountToConsume = 1;

	// Token: 0x04001582 RID: 5506
	public float conditionFractionToLose;

	// Token: 0x04001583 RID: 5507
	public List<global::ItemModConsumable.ConsumableEffect> effects = new List<global::ItemModConsumable.ConsumableEffect>();

	// Token: 0x020004DE RID: 1246
	[Serializable]
	public class ConsumableEffect
	{
		// Token: 0x04001584 RID: 5508
		public global::MetabolismAttribute.Type type;

		// Token: 0x04001585 RID: 5509
		public float amount;

		// Token: 0x04001586 RID: 5510
		public float time;

		// Token: 0x04001587 RID: 5511
		public float onlyIfHealthLessThan = 1f;
	}
}
