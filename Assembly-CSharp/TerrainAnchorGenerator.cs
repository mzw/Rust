﻿using System;
using UnityEngine;

// Token: 0x02000559 RID: 1369
public class TerrainAnchorGenerator : MonoBehaviour, IEditorComponent
{
	// Token: 0x040017C9 RID: 6089
	public float PlacementRadius = 32f;

	// Token: 0x040017CA RID: 6090
	public float PlacementPadding;

	// Token: 0x040017CB RID: 6091
	public float PlacementFade = 16f;

	// Token: 0x040017CC RID: 6092
	public float PlacementDistance = 8f;

	// Token: 0x040017CD RID: 6093
	public float AnchorExtentsMin = 8f;

	// Token: 0x040017CE RID: 6094
	public float AnchorExtentsMax = 16f;

	// Token: 0x040017CF RID: 6095
	public float AnchorOffsetMin;

	// Token: 0x040017D0 RID: 6096
	public float AnchorOffsetMax;
}
