﻿using System;
using Network;
using Rust;
using UnityEngine;

// Token: 0x02000315 RID: 789
public class Effect : EffectData
{
	// Token: 0x06001377 RID: 4983 RVA: 0x00072710 File Offset: 0x00070910
	public Effect()
	{
	}

	// Token: 0x06001378 RID: 4984 RVA: 0x00072718 File Offset: 0x00070918
	public Effect(string effectName, Vector3 posWorld, Vector3 normWorld, Connection sourceConnection = null)
	{
		this.Init(global::Effect.Type.Generic, posWorld, normWorld, sourceConnection);
		this.pooledString = effectName;
	}

	// Token: 0x06001379 RID: 4985 RVA: 0x00072734 File Offset: 0x00070934
	public Effect(string effectName, global::BaseEntity ent, uint boneID, Vector3 posLocal, Vector3 normLocal, Connection sourceConnection = null)
	{
		this.Init(global::Effect.Type.Generic, ent, boneID, posLocal, normLocal, sourceConnection);
		this.pooledString = effectName;
	}

	// Token: 0x0600137A RID: 4986 RVA: 0x00072754 File Offset: 0x00070954
	public void Init(global::Effect.Type fxtype, global::BaseEntity ent, uint boneID, Vector3 posLocal, Vector3 normLocal, Connection sourceConnection = null)
	{
		this.Clear();
		this.type = (uint)fxtype;
		this.attached = true;
		this.origin = posLocal;
		this.normal = normLocal;
		this.gameObject = null;
		this.Up = Vector3.zero;
		if (ent != null && !ent.IsValid())
		{
			Debug.LogWarning("Effect.Init - invalid entity");
		}
		this.entity = ((!ent.IsValid()) ? 0u : ent.net.ID);
		this.source = ((sourceConnection == null) ? 0UL : sourceConnection.userid);
		this.bone = boneID;
	}

	// Token: 0x0600137B RID: 4987 RVA: 0x000727FC File Offset: 0x000709FC
	public void Init(global::Effect.Type fxtype, Vector3 posWorld, Vector3 normWorld, Connection sourceConnection = null)
	{
		this.Clear();
		this.type = (uint)fxtype;
		this.attached = false;
		this.worldPos = posWorld;
		this.worldNrm = normWorld;
		this.gameObject = null;
		this.Up = Vector3.zero;
		this.entity = 0u;
		this.origin = this.worldPos;
		this.normal = this.worldNrm;
		this.bone = 0u;
		this.source = ((sourceConnection == null) ? 0UL : sourceConnection.userid);
	}

	// Token: 0x0600137C RID: 4988 RVA: 0x00072880 File Offset: 0x00070A80
	public void Clear()
	{
		this.worldPos = Vector3.zero;
		this.worldNrm = Vector3.zero;
		this.attached = false;
		this.transform = null;
		this.gameObject = null;
		this.pooledString = null;
		this.broadcast = false;
	}

	// Token: 0x04000E40 RID: 3648
	public Vector3 Up;

	// Token: 0x04000E41 RID: 3649
	public Vector3 worldPos;

	// Token: 0x04000E42 RID: 3650
	public Vector3 worldNrm;

	// Token: 0x04000E43 RID: 3651
	public bool attached;

	// Token: 0x04000E44 RID: 3652
	public Transform transform;

	// Token: 0x04000E45 RID: 3653
	public GameObject gameObject;

	// Token: 0x04000E46 RID: 3654
	public string pooledString;

	// Token: 0x04000E47 RID: 3655
	public bool broadcast;

	// Token: 0x04000E48 RID: 3656
	private static global::Effect reusableInstace = new global::Effect();

	// Token: 0x02000316 RID: 790
	public enum Type : uint
	{
		// Token: 0x04000E4A RID: 3658
		Generic,
		// Token: 0x04000E4B RID: 3659
		Projectile
	}

	// Token: 0x02000317 RID: 791
	public static class client
	{
		// Token: 0x0600137E RID: 4990 RVA: 0x000728C8 File Offset: 0x00070AC8
		public static void Run(global::Effect.Type fxtype, global::BaseEntity ent, uint boneID, Vector3 posLocal, Vector3 normLocal)
		{
		}

		// Token: 0x0600137F RID: 4991 RVA: 0x000728CC File Offset: 0x00070ACC
		public static void Run(string strName, global::BaseEntity ent, uint boneID, Vector3 posLocal, Vector3 normLocal)
		{
			if (string.IsNullOrEmpty(strName))
			{
				return;
			}
		}

		// Token: 0x06001380 RID: 4992 RVA: 0x000728DC File Offset: 0x00070ADC
		public static void Run(global::Effect.Type fxtype, Vector3 posWorld, Vector3 normWorld, Vector3 up = default(Vector3))
		{
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x000728E0 File Offset: 0x00070AE0
		public static void Run(string strName, Vector3 posWorld = default(Vector3), Vector3 normWorld = default(Vector3), Vector3 up = default(Vector3))
		{
			if (string.IsNullOrEmpty(strName))
			{
				return;
			}
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x000728F0 File Offset: 0x00070AF0
		public static void Run(string strName, GameObject obj)
		{
			if (string.IsNullOrEmpty(strName))
			{
				return;
			}
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x00072900 File Offset: 0x00070B00
		public static void DoAdditiveImpactEffect(global::HitInfo info, string effectName)
		{
			if (info.HitEntity.IsValid())
			{
				global::Effect.client.Run(effectName, info.HitEntity, info.HitBone, info.HitPositionLocal + info.HitNormalLocal * 0.1f, info.HitNormalLocal);
			}
			else
			{
				global::Effect.client.Run(effectName, info.HitPositionWorld + info.HitNormalWorld * 0.1f, info.HitNormalWorld, default(Vector3));
			}
		}

		// Token: 0x06001384 RID: 4996 RVA: 0x00072988 File Offset: 0x00070B88
		public static void ImpactEffect(global::HitInfo info)
		{
			if (info.HitMaterial == 0u)
			{
				return;
			}
			string text = global::StringPool.Get(info.HitMaterial);
			if (text == string.Empty)
			{
				return;
			}
			string strName = global::EffectDictionary.GetParticle(info.damageTypes.GetMajorityDamageType(), text);
			string decal = global::EffectDictionary.GetDecal(info.damageTypes.GetMajorityDamageType(), text);
			if (info.HitEntity.IsValid())
			{
				global::GameObjectRef impactEffect = info.HitEntity.GetImpactEffect(info);
				if (impactEffect.isValid)
				{
					strName = impactEffect.resourcePath;
				}
				global::Effect.client.Run(strName, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal);
				if (info.DoDecals)
				{
					global::Effect.client.Run(decal, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal);
				}
			}
			else
			{
				global::Effect.client.Run(strName, info.HitPositionWorld, info.HitNormalWorld, default(Vector3));
				global::Effect.client.Run(decal, info.HitPositionWorld, info.HitNormalWorld, default(Vector3));
			}
			if (info.WeaponPrefab)
			{
				global::BaseMelee baseMelee = info.WeaponPrefab as global::BaseMelee;
				if (baseMelee != null)
				{
					string strikeEffectPath = baseMelee.GetStrikeEffectPath(text);
					if (info.HitEntity.IsValid())
					{
						global::Effect.client.Run(strikeEffectPath, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal);
					}
					else
					{
						global::Effect.client.Run(strikeEffectPath, info.HitPositionWorld, info.HitNormalWorld, default(Vector3));
					}
				}
			}
			if (info.damageTypes.Has(Rust.DamageType.Explosion))
			{
				global::Effect.client.DoAdditiveImpactEffect(info, "assets/bundled/prefabs/fx/impacts/additive/explosion.prefab");
			}
			if (info.damageTypes.Has(Rust.DamageType.Heat))
			{
				global::Effect.client.DoAdditiveImpactEffect(info, "assets/bundled/prefabs/fx/impacts/additive/fire.prefab");
			}
		}
	}

	// Token: 0x02000318 RID: 792
	public static class server
	{
		// Token: 0x06001385 RID: 4997 RVA: 0x00072B54 File Offset: 0x00070D54
		public static void Run(global::Effect.Type fxtype, global::BaseEntity ent, uint boneID, Vector3 posLocal, Vector3 normLocal, Connection sourceConnection = null, bool broadcast = false)
		{
			global::Effect.reusableInstace.Init(fxtype, ent, boneID, posLocal, normLocal, sourceConnection);
			global::Effect.reusableInstace.broadcast = broadcast;
			global::EffectNetwork.Send(global::Effect.reusableInstace);
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x00072B80 File Offset: 0x00070D80
		public static void Run(string strName, global::BaseEntity ent, uint boneID, Vector3 posLocal, Vector3 normLocal, Connection sourceConnection = null, bool broadcast = false)
		{
			if (string.IsNullOrEmpty(strName))
			{
				return;
			}
			global::Effect.reusableInstace.Init(global::Effect.Type.Generic, ent, boneID, posLocal, normLocal, sourceConnection);
			global::Effect.reusableInstace.pooledString = strName;
			global::Effect.reusableInstace.broadcast = broadcast;
			global::EffectNetwork.Send(global::Effect.reusableInstace);
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x00072BCC File Offset: 0x00070DCC
		public static void Run(global::Effect.Type fxtype, Vector3 posWorld, Vector3 normWorld, Connection sourceConnection = null, bool broadcast = false)
		{
			global::Effect.reusableInstace.Init(fxtype, posWorld, normWorld, sourceConnection);
			global::Effect.reusableInstace.broadcast = broadcast;
			global::EffectNetwork.Send(global::Effect.reusableInstace);
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x00072BF4 File Offset: 0x00070DF4
		public static void Run(string strName, Vector3 posWorld = default(Vector3), Vector3 normWorld = default(Vector3), Connection sourceConnection = null, bool broadcast = false)
		{
			if (string.IsNullOrEmpty(strName))
			{
				return;
			}
			global::Effect.reusableInstace.Init(global::Effect.Type.Generic, posWorld, normWorld, sourceConnection);
			global::Effect.reusableInstace.pooledString = strName;
			global::Effect.reusableInstace.broadcast = broadcast;
			global::EffectNetwork.Send(global::Effect.reusableInstace);
		}

		// Token: 0x06001389 RID: 5001 RVA: 0x00072C34 File Offset: 0x00070E34
		public static void DoAdditiveImpactEffect(global::HitInfo info, string effectName)
		{
			if (info.HitEntity.IsValid())
			{
				global::Effect.server.Run(effectName, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal, info.Predicted, false);
			}
			else
			{
				global::Effect.server.Run(effectName, info.HitPositionWorld, info.HitNormalWorld, info.Predicted, false);
			}
		}

		// Token: 0x0600138A RID: 5002 RVA: 0x00072C94 File Offset: 0x00070E94
		public static void ImpactEffect(global::HitInfo info)
		{
			if (info.HitMaterial == 0u)
			{
				return;
			}
			string text = global::StringPool.Get(info.HitMaterial);
			if (text == string.Empty)
			{
				return;
			}
			string strName = global::EffectDictionary.GetParticle(info.damageTypes.GetMajorityDamageType(), text);
			string decal = global::EffectDictionary.GetDecal(info.damageTypes.GetMajorityDamageType(), text);
			if (info.HitEntity.IsValid())
			{
				global::GameObjectRef impactEffect = info.HitEntity.GetImpactEffect(info);
				if (impactEffect.isValid)
				{
					strName = impactEffect.resourcePath;
				}
				global::Effect.server.Run(strName, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal, info.Predicted, false);
				global::Effect.server.Run(decal, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal, info.Predicted, false);
			}
			else
			{
				global::Effect.server.Run(strName, info.HitPositionWorld, info.HitNormalWorld, info.Predicted, false);
				global::Effect.server.Run(decal, info.HitPositionWorld, info.HitNormalWorld, info.Predicted, false);
			}
			if (info.WeaponPrefab)
			{
				global::BaseMelee baseMelee = info.WeaponPrefab as global::BaseMelee;
				if (baseMelee != null)
				{
					string strikeEffectPath = baseMelee.GetStrikeEffectPath(text);
					if (info.HitEntity.IsValid())
					{
						global::Effect.server.Run(strikeEffectPath, info.HitEntity, info.HitBone, info.HitPositionLocal, info.HitNormalLocal, info.Predicted, false);
					}
					else
					{
						global::Effect.server.Run(strikeEffectPath, info.HitPositionWorld, info.HitNormalWorld, info.Predicted, false);
					}
				}
			}
			if (info.damageTypes.Has(Rust.DamageType.Explosion))
			{
				global::Effect.server.DoAdditiveImpactEffect(info, "assets/bundled/prefabs/fx/impacts/additive/explosion.prefab");
			}
			if (info.damageTypes.Has(Rust.DamageType.Heat))
			{
				global::Effect.server.DoAdditiveImpactEffect(info, "assets/bundled/prefabs/fx/impacts/additive/fire.prefab");
			}
		}
	}
}
