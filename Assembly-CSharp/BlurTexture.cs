﻿using System;
using UnityEngine;

// Token: 0x02000779 RID: 1913
public class BlurTexture : global::ProcessedTexture
{
	// Token: 0x0600238E RID: 9102 RVA: 0x000C4E70 File Offset: 0x000C3070
	public BlurTexture(int width, int height, bool linear = true)
	{
		this.material = base.CreateMaterial("Hidden/Rust/SeparableBlur");
		this.result = base.CreateRenderTexture("Blur Texture", width, height, linear);
	}

	// Token: 0x0600238F RID: 9103 RVA: 0x000C4EA0 File Offset: 0x000C30A0
	public void Blur(float radius)
	{
		this.Blur(this.result, radius);
	}

	// Token: 0x06002390 RID: 9104 RVA: 0x000C4EB0 File Offset: 0x000C30B0
	public void Blur(Texture source, float radius)
	{
		RenderTexture renderTexture = base.CreateTemporary();
		this.material.SetVector("offsets", new Vector4(radius / (float)Screen.width, 0f, 0f, 0f));
		Graphics.Blit(source, renderTexture, this.material, 0);
		this.material.SetVector("offsets", new Vector4(0f, radius / (float)Screen.height, 0f, 0f));
		Graphics.Blit(renderTexture, this.result, this.material, 0);
		base.ReleaseTemporary(renderTexture);
	}
}
