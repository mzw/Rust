﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x020006AC RID: 1708
public class ItemIcon : global::BaseMonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, global::IDraggable, global::IInventoryChanged, global::IItemAmountChanged, global::IItemIconChanged, IEventSystemHandler
{
	// Token: 0x0600213E RID: 8510 RVA: 0x000BBE58 File Offset: 0x000BA058
	public virtual void OnPointerClick(PointerEventData eventData)
	{
	}

	// Token: 0x0600213F RID: 8511 RVA: 0x000BBE5C File Offset: 0x000BA05C
	public void OnPointerEnter(PointerEventData eventData)
	{
	}

	// Token: 0x06002140 RID: 8512 RVA: 0x000BBE60 File Offset: 0x000BA060
	public void OnPointerExit(PointerEventData eventData)
	{
	}

	// Token: 0x04001CEF RID: 7407
	public static Color defaultBackgroundColor = new Color(0.968627453f, 0.921568632f, 0.882352948f, 0.03529412f);

	// Token: 0x04001CF0 RID: 7408
	public static Color selectedBackgroundColor = new Color(0.121568628f, 0.419607848f, 0.627451f, 0.784313738f);

	// Token: 0x04001CF1 RID: 7409
	public global::ItemContainerSource containerSource;

	// Token: 0x04001CF2 RID: 7410
	public int slotOffset;

	// Token: 0x04001CF3 RID: 7411
	[Range(0f, 64f)]
	public int slot;

	// Token: 0x04001CF4 RID: 7412
	public bool setSlotFromSiblingIndex = true;

	// Token: 0x04001CF5 RID: 7413
	public GameObject slots;

	// Token: 0x04001CF6 RID: 7414
	public CanvasGroup iconContents;

	// Token: 0x04001CF7 RID: 7415
	public Image iconImage;

	// Token: 0x04001CF8 RID: 7416
	public Image underlayImage;

	// Token: 0x04001CF9 RID: 7417
	public Text amountText;

	// Token: 0x04001CFA RID: 7418
	public Image hoverOutline;

	// Token: 0x04001CFB RID: 7419
	public Image cornerIcon;

	// Token: 0x04001CFC RID: 7420
	public Image lockedImage;

	// Token: 0x04001CFD RID: 7421
	public Image progressImage;

	// Token: 0x04001CFE RID: 7422
	public Image backgroundImage;

	// Token: 0x04001CFF RID: 7423
	public CanvasGroup conditionObject;

	// Token: 0x04001D00 RID: 7424
	public Image conditionFill;

	// Token: 0x04001D01 RID: 7425
	public Image maxConditionFill;

	// Token: 0x04001D02 RID: 7426
	public bool allowSelection = true;

	// Token: 0x04001D03 RID: 7427
	public bool allowDropping = true;

	// Token: 0x04001D04 RID: 7428
	[NonSerialized]
	public global::Item item;

	// Token: 0x04001D05 RID: 7429
	[NonSerialized]
	public bool invalidSlot;

	// Token: 0x04001D06 RID: 7430
	public AudioClip hoverSound;
}
