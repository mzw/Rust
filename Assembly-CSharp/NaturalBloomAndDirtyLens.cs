﻿using System;
using UnityEngine;

// Token: 0x020004B6 RID: 1206
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[AddComponentMenu("Image Effects/Natural Bloom and Dirty Lens")]
public class NaturalBloomAndDirtyLens : MonoBehaviour
{
	// Token: 0x0400149D RID: 5277
	public Shader shader;

	// Token: 0x0400149E RID: 5278
	public Texture2D lensDirtTexture;

	// Token: 0x0400149F RID: 5279
	public float range = 10000f;

	// Token: 0x040014A0 RID: 5280
	public float cutoff = 1f;

	// Token: 0x040014A1 RID: 5281
	[Range(0f, 1f)]
	public float bloomIntensity = 0.05f;

	// Token: 0x040014A2 RID: 5282
	[Range(0f, 1f)]
	public float lensDirtIntensity = 0.05f;

	// Token: 0x040014A3 RID: 5283
	[Range(0f, 4f)]
	public float spread = 1f;

	// Token: 0x040014A4 RID: 5284
	[Range(0f, 4f)]
	public int iterations = 1;

	// Token: 0x040014A5 RID: 5285
	[Range(1f, 10f)]
	public int mips = 6;

	// Token: 0x040014A6 RID: 5286
	public float[] mipWeights = new float[]
	{
		0.5f,
		0.6f,
		0.6f,
		0.45f,
		0.35f,
		0.23f
	};

	// Token: 0x040014A7 RID: 5287
	public bool highPrecision;

	// Token: 0x040014A8 RID: 5288
	public bool downscaleSource;

	// Token: 0x040014A9 RID: 5289
	public bool debug;
}
