﻿using System;
using System.Collections.Generic;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000078 RID: 120
public class LiquidVessel : global::HeldEntity
{
	// Token: 0x06000848 RID: 2120 RVA: 0x00035C10 File Offset: 0x00033E10
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("LiquidVessel.OnRpcMessage", 0.1f))
		{
			if (rpc == 1830524834u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoEmpty ");
				}
				using (TimeWarning.New("DoEmpty", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoEmpty", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoEmpty(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoEmpty");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000849 RID: 2121 RVA: 0x00035DEC File Offset: 0x00033FEC
	public bool CanDrink()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return false;
		}
		if (!ownerPlayer.metabolism.CanConsume())
		{
			return false;
		}
		global::Item item = this.GetItem();
		return item != null && item.contents != null && item.contents.itemList != null && item.contents.itemList.Count != 0;
	}

	// Token: 0x0600084A RID: 2122 RVA: 0x00035E68 File Offset: 0x00034068
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void DoEmpty(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		global::Item item = this.GetItem();
		if (item == null)
		{
			return;
		}
		if (item.contents == null)
		{
			return;
		}
		if (!msg.player.metabolism.CanConsume())
		{
			return;
		}
		using (List<global::Item>.Enumerator enumerator = item.contents.itemList.GetEnumerator())
		{
			if (enumerator.MoveNext())
			{
				global::Item item2 = enumerator.Current;
				item2.UseItem(50);
			}
		}
	}

	// Token: 0x0600084B RID: 2123 RVA: 0x00035F14 File Offset: 0x00034114
	public void AddLiquid(global::ItemDefinition liquidType, int amount)
	{
		if (amount <= 0)
		{
			return;
		}
		global::Item item = this.GetItem();
		global::Item item2 = item.contents.GetSlot(0);
		global::ItemModContainer component = item.info.GetComponent<global::ItemModContainer>();
		if (item2 == null)
		{
			global::Item item3 = global::ItemManager.Create(liquidType, amount, 0UL);
			if (item3 != null)
			{
				item3.MoveToContainer(item.contents, -1, true);
			}
		}
		else
		{
			int num = Mathf.Clamp(item2.amount + amount, 0, component.maxStackSize);
			global::ItemDefinition itemDefinition = global::WaterResource.Merge(item2.info, liquidType);
			if (itemDefinition != item2.info)
			{
				item2.Remove(0f);
				item2 = global::ItemManager.Create(itemDefinition, num, 0UL);
				item2.MoveToContainer(item.contents, -1, true);
			}
			else
			{
				item2.amount = num;
			}
			item2.MarkDirty();
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x0600084C RID: 2124 RVA: 0x00035FF0 File Offset: 0x000341F0
	public bool CanFillHere(Vector3 pos)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		return ownerPlayer && (double)ownerPlayer.WaterFactor() > 0.05;
	}

	// Token: 0x0600084D RID: 2125 RVA: 0x0003602C File Offset: 0x0003422C
	public int AmountHeld()
	{
		global::Item slot = this.GetItem().contents.GetSlot(0);
		if (slot == null)
		{
			return 0;
		}
		return slot.amount;
	}

	// Token: 0x0600084E RID: 2126 RVA: 0x0003605C File Offset: 0x0003425C
	public float HeldFraction()
	{
		return (float)this.AmountHeld() / (float)this.MaxHoldable();
	}

	// Token: 0x0600084F RID: 2127 RVA: 0x00036070 File Offset: 0x00034270
	public bool IsFull()
	{
		return this.HeldFraction() >= 1f;
	}

	// Token: 0x06000850 RID: 2128 RVA: 0x00036084 File Offset: 0x00034284
	public int MaxHoldable()
	{
		return this.GetItem().info.GetComponent<global::ItemModContainer>().maxStackSize;
	}
}
