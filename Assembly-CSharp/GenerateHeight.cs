﻿using System;
using UnityEngine;

// Token: 0x020005A1 RID: 1441
public class GenerateHeight : global::ProceduralComponent
{
	// Token: 0x06001E3E RID: 7742 RVA: 0x000A7F78 File Offset: 0x000A6178
	public override void Process(uint seed)
	{
		global::TerrainHeightMap map = global::TerrainMeta.HeightMap;
		int res = global::TerrainMeta.HeightMap.res;
		Vector3 size = global::TerrainMeta.Size;
		float centerFalloff = 200f / (0.5f * size.x);
		float centerPadding = 0.2f;
		float borderFalloff = 400f / (0.5f * size.x);
		float borderPadding = 0f;
		float num = SeedRandom.Range(ref seed, 0.0009f, 0.0011f);
		float shapeX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float shapeZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int shapeO = this.Shape.Octaves;
		float shapeK = this.Shape.Offset;
		float shapeF = this.Shape.Frequency * num;
		float shapeA = this.Shape.Amplitude * borderFalloff;
		float baseX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float baseZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int baseO = this.Base.Octaves;
		float baseK = this.Base.Offset;
		float baseF = this.Base.Frequency * num;
		float baseA = this.Base.Amplitude;
		float lakeX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float lakeZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int lakeO = this.Lake.Octaves;
		float lakeK = this.Lake.Offset;
		float lakeF = this.Lake.Frequency * num;
		float lakeA = this.Lake.Amplitude;
		float duneX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float duneZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int duneO = this.Dune.Octaves;
		float duneK = this.Dune.Offset;
		float duneF = this.Dune.Frequency * num;
		float duneA = this.Dune.Amplitude;
		float hillX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float hillZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int hillO = this.Hill.Octaves;
		float hillK = this.Hill.Offset;
		float hillF = this.Hill.Frequency * num;
		float hillA = this.Hill.Amplitude;
		float mountX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float mountZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int mountO = this.Mountain.Octaves;
		float mountK = this.Mountain.Offset;
		float mountF = this.Mountain.Frequency * num;
		float mountA = this.Mountain.Amplitude;
		float plainX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float plainZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int plainO = this.Plain.Octaves;
		float plainK = this.Plain.Offset;
		float plainF = this.Plain.Frequency * num;
		float plainA = this.Plain.Amplitude;
		float distortX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float distortZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int distortO = 4;
		float distortF = 0.01f;
		float distortA = 5f;
		float oceanLevel0 = global::TerrainMeta.NormalizeY(-0.1f);
		float oceanLevel1 = global::TerrainMeta.NormalizeY(0.1f);
		float lootAngle = global::TerrainMeta.LootAxisAngle;
		float biomeAngle = global::TerrainMeta.BiomeAxisAngle;
		Parallel.For(0, res, delegate(int z)
		{
			for (int i = 0; i < res; i++)
			{
				float num2 = map.Coordinate(i);
				float num3 = map.Coordinate(z);
				float num4 = num2 * size.x;
				float num5 = num3 * size.z;
				num4 += global::Noise.Turbulence((double)(distortX + num4), (double)(distortZ + num5), distortO, (double)distortF, (double)distortA, 2.0, 0.5);
				num5 += global::Noise.Turbulence((double)(distortX + num4), (double)(distortZ + num5), distortO, (double)distortF, (double)distortA, 2.0, 0.5);
				Vector2 vector;
				vector..ctor(num2 * 2f - 1f, num3 * 2f - 1f);
				float num6 = (Vector2Ex.Rotate(vector, lootAngle).y + 1f) * 0.5f;
				float num7 = (Vector2Ex.Rotate(vector, biomeAngle).y + 1f) * 0.5f;
				float num8 = Mathf.InverseLerp(0.35f, 0.25f, num6);
				float num9 = Mathf.Lerp(num7, 0.5f, num8);
				float num10 = Mathf.Clamp01(num9);
				float num11 = num2 * 2f - 1f;
				float num12 = num3 * 2f - 1f;
				float num13 = (num11 * num11 + num12 * num12) * 0.75f;
				float num14 = Mathf.Max(Mathf.Abs(num11), Mathf.Abs(num12));
				float num15 = shapeK + global::Noise.Sharp((double)(shapeX + num4), (double)(shapeZ + num5), shapeO, (double)shapeF, (double)shapeA, 2.0, 0.5);
				float num16 = Mathf.Max(num13, num14) + num15;
				float num17 = 0f;
				float num18 = 0f;
				float num19 = 0f;
				float num20 = 0f;
				float num21 = Mathf.InverseLerp(1f - borderPadding - borderFalloff, 1f - borderPadding, num16);
				float num22 = Mathf.InverseLerp(1f - borderPadding - borderFalloff - centerPadding, 1f - borderPadding - borderFalloff - centerPadding - centerFalloff, num16);
				num17 -= num21;
				num17 += num22 * 0.75f;
				num17 += (1f - num21) * (baseK + global::Noise.Jordan((double)(baseX + num4), (double)(baseZ + num5), baseO, (double)baseF, (double)baseA, 2.0, 0.5, 1.0, 1.0, 1.0));
				num17 += (1f - num21) * (lakeK + global::Noise.Ridge((double)(lakeX + num4), (double)(lakeZ + num5), lakeO, (double)lakeF, (double)lakeA, 2.0, 0.5));
				float num23 = Mathf.SmoothStep(0.5f, 0.45f, -num17);
				if (num17 < 0f)
				{
					map.SetHeight(i, z, num23);
				}
				else
				{
					float num24 = Mathf.InverseLerp(0f, 0.05f, num17);
					num24 = 0.002f * num24 * num24;
					if (num17 < 0.05f)
					{
						map.SetHeight(i, z, num23 + num24);
					}
					else
					{
						float num25 = Mathx.Above(num10, 0.7f, 0.1f);
						float num26 = Mathx.Below(num10, 0.3f, 0.1f);
						float num27 = 1f - num25 - num26;
						float num28 = Mathf.InverseLerp(0.05f, 0.3f, num17);
						float num29 = Mathf.InverseLerp(0.5f, 1.15f, num17);
						float num30 = Mathf.InverseLerp(0f, 2.75f, num17);
						if (num28 > 0f)
						{
							if (num26 > 0f)
							{
								float num31 = num26;
								float num32 = duneK + global::Noise.Ridge((double)(duneX + num4), (double)(duneZ + num5), duneO, (double)duneF, (double)duneA, 2.0, 0.5);
								if (num32 > 0f)
								{
									num18 += num32 * num31;
								}
							}
							if (num25 + num27 > 0f)
							{
								float num33 = num25 + num27;
								float num34 = plainK + global::Noise.Billow((double)(plainX + num4), (double)(plainZ + num5), plainO, (double)plainF, (double)plainA, 2.0, 0.5);
								if (num34 > 0f)
								{
									num18 += num34 * num33;
								}
							}
							if (num18 > 0f)
							{
								num18 = Mathf.SmoothStep(0f, num18, num28);
							}
						}
						if (num29 > 0f)
						{
							float num35 = hillK + global::Noise.TurbulenceWarp((double)(hillX + num4), (double)(hillZ + num5), hillO, (double)hillF, (double)hillA, 2.0, 0.5, 0.25);
							if (num35 > 0f)
							{
								num19 = Mathf.SmoothStep(0f, num35, num29);
							}
						}
						if (num30 > 0f)
						{
							float num36 = mountK + global::Noise.RidgeWarp((double)(mountX + num4), (double)(mountZ + num5), mountO, (double)mountF, (double)mountA, 2.0, 0.5, 0.25);
							if (num36 > 0f)
							{
								num20 = Mathf.SmoothStep(0f, num36, num30);
							}
						}
						float num37 = num23 + num24 + Mathx.SmoothMax(num18 + num19, num20, 0.1f);
						if (num37 > oceanLevel0 && num37 < oceanLevel1)
						{
							num37 = Mathf.SmoothStep(oceanLevel0, oceanLevel1, Mathf.InverseLerp(oceanLevel0, oceanLevel1, num37));
						}
						map.SetHeight(i, z, num37);
					}
				}
			}
		});
	}

	// Token: 0x040018CD RID: 6349
	private global::NoiseParameters Shape = new global::NoiseParameters(6, 0.5f, 0.5f, -0f);

	// Token: 0x040018CE RID: 6350
	private global::NoiseParameters Base = new global::NoiseParameters(6, 1f, 5f, -0.4f);

	// Token: 0x040018CF RID: 6351
	private global::NoiseParameters Lake = new global::NoiseParameters(6, 0.5f, 0.4f, -0.2f);

	// Token: 0x040018D0 RID: 6352
	private global::NoiseParameters Dune = new global::NoiseParameters(8, 1f, 0.03f, -0.02f);

	// Token: 0x040018D1 RID: 6353
	private global::NoiseParameters Plain = new global::NoiseParameters(8, 1f, 0.05f, -0.01f);

	// Token: 0x040018D2 RID: 6354
	private global::NoiseParameters Hill = new global::NoiseParameters(8, 1.2f, 0.05f, -0f);

	// Token: 0x040018D3 RID: 6355
	private global::NoiseParameters Mountain = new global::NoiseParameters(8, 1f, 0.07f, -0f);
}
