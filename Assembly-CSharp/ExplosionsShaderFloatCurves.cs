﻿using System;
using UnityEngine;

// Token: 0x020007FC RID: 2044
public class ExplosionsShaderFloatCurves : MonoBehaviour
{
	// Token: 0x060025C6 RID: 9670 RVA: 0x000D0CD8 File Offset: 0x000CEED8
	private void Start()
	{
		Material[] materials = base.GetComponent<Renderer>().materials;
		if (this.MaterialID >= materials.Length)
		{
			Debug.Log("ShaderColorGradient: Material ID more than shader materials count.");
		}
		this.matInstance = materials[this.MaterialID];
		if (!this.matInstance.HasProperty(this.ShaderProperty))
		{
			Debug.Log("ShaderColorGradient: Shader not have \"" + this.ShaderProperty + "\" property");
		}
		this.propertyID = Shader.PropertyToID(this.ShaderProperty);
	}

	// Token: 0x060025C7 RID: 9671 RVA: 0x000D0D58 File Offset: 0x000CEF58
	private void OnEnable()
	{
		this.startTime = Time.time;
		this.canUpdate = true;
	}

	// Token: 0x060025C8 RID: 9672 RVA: 0x000D0D6C File Offset: 0x000CEF6C
	private void Update()
	{
		float num = Time.time - this.startTime;
		if (this.canUpdate)
		{
			float num2 = this.FloatPropertyCurve.Evaluate(num / this.GraphTimeMultiplier) * this.GraphScaleMultiplier;
			this.matInstance.SetFloat(this.propertyID, num2);
		}
		if (num >= this.GraphTimeMultiplier)
		{
			this.canUpdate = false;
		}
	}

	// Token: 0x040021EF RID: 8687
	public string ShaderProperty = "_BumpAmt";

	// Token: 0x040021F0 RID: 8688
	public int MaterialID;

	// Token: 0x040021F1 RID: 8689
	public AnimationCurve FloatPropertyCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

	// Token: 0x040021F2 RID: 8690
	public float GraphTimeMultiplier = 1f;

	// Token: 0x040021F3 RID: 8691
	public float GraphScaleMultiplier = 1f;

	// Token: 0x040021F4 RID: 8692
	private bool canUpdate;

	// Token: 0x040021F5 RID: 8693
	private Material matInstance;

	// Token: 0x040021F6 RID: 8694
	private int propertyID;

	// Token: 0x040021F7 RID: 8695
	private float startTime;
}
