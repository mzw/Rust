﻿using System;

// Token: 0x020004D2 RID: 1234
public enum ItemSelectionPanel
{
	// Token: 0x0400156D RID: 5485
	None,
	// Token: 0x0400156E RID: 5486
	Vessel,
	// Token: 0x0400156F RID: 5487
	Modifications,
	// Token: 0x04001570 RID: 5488
	GunInformation
}
