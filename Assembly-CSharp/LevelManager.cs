﻿using System;
using System.Collections;
using Network;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000442 RID: 1090
public static class LevelManager
{
	// Token: 0x170001AA RID: 426
	// (get) Token: 0x06001876 RID: 6262 RVA: 0x0008A738 File Offset: 0x00088938
	public static bool isLoaded
	{
		get
		{
			return global::LevelManager.CurrentLevelName != null && !(global::LevelManager.CurrentLevelName == string.Empty) && !(global::LevelManager.CurrentLevelName == "UIScene") && !(global::LevelManager.CurrentLevelName == "Empty") && !(global::LevelManager.CurrentLevelName == "MenuBackground") && !(global::LevelManager.CurrentLevelName == "UIWorkshop");
		}
	}

	// Token: 0x06001877 RID: 6263 RVA: 0x0008A7C0 File Offset: 0x000889C0
	public static bool IsValid(string strName)
	{
		return Application.CanStreamedLevelBeLoaded(strName);
	}

	// Token: 0x06001878 RID: 6264 RVA: 0x0008A7C8 File Offset: 0x000889C8
	public static void LoadLevel(string strName, bool keepLoadingScreenOpen = true)
	{
		if (strName == "proceduralmap")
		{
			strName = "Procedural Map";
		}
		global::LevelManager.CurrentLevelName = strName;
		Net.sv.Reset();
		SceneManager.LoadScene(strName, 0);
	}

	// Token: 0x06001879 RID: 6265 RVA: 0x0008A7F8 File Offset: 0x000889F8
	public static IEnumerator LoadLevelAsync(string strName, bool keepLoadingScreenOpen = true)
	{
		global::LevelManager.CurrentLevelName = strName;
		Net.sv.Reset();
		yield return null;
		yield return SceneManager.LoadSceneAsync(strName, 0);
		yield return null;
		yield return null;
		yield break;
	}

	// Token: 0x0600187A RID: 6266 RVA: 0x0008A814 File Offset: 0x00088A14
	public static void UnloadLevel()
	{
		global::LevelManager.CurrentLevelName = null;
		Application.LoadLevel("Empty");
	}

	// Token: 0x04001328 RID: 4904
	public static string CurrentLevelName;
}
