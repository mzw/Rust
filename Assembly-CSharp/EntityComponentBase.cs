﻿using System;
using Network;

// Token: 0x0200036A RID: 874
public class EntityComponentBase : global::BaseMonoBehaviour
{
	// Token: 0x060014E8 RID: 5352 RVA: 0x000790FC File Offset: 0x000772FC
	protected virtual global::BaseEntity GetBaseEntity()
	{
		return null;
	}

	// Token: 0x060014E9 RID: 5353 RVA: 0x00079100 File Offset: 0x00077300
	public virtual bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		return false;
	}
}
