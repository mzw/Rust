﻿using System;
using UnityEngine;

// Token: 0x02000805 RID: 2053
public class Occludee : MonoBehaviour
{
	// Token: 0x060025F2 RID: 9714 RVA: 0x000D1A50 File Offset: 0x000CFC50
	protected virtual void Awake()
	{
		this.renderer = base.GetComponent<Renderer>();
		this.collider = base.GetComponent<Collider>();
	}

	// Token: 0x060025F3 RID: 9715 RVA: 0x000D1A6C File Offset: 0x000CFC6C
	public void OnEnable()
	{
		if (this.autoRegister && this.collider != null)
		{
			this.Register();
		}
	}

	// Token: 0x060025F4 RID: 9716 RVA: 0x000D1A90 File Offset: 0x000CFC90
	public void OnDisable()
	{
		if (this.autoRegister && this.occludeeId >= 0)
		{
			this.Unregister();
		}
	}

	// Token: 0x060025F5 RID: 9717 RVA: 0x000D1AB0 File Offset: 0x000CFCB0
	public void Register()
	{
		this.center = this.collider.bounds.center;
		this.radius = Mathf.Max(Mathf.Max(this.collider.bounds.extents.x, this.collider.bounds.extents.y), this.collider.bounds.extents.z);
		this.occludeeId = global::OcclusionCulling.RegisterOccludee(this.center, this.radius, this.renderer.enabled, this.minTimeVisible, this.isStatic, new global::OcclusionCulling.OnVisibilityChanged(this.OnVisibilityChanged));
		if (this.occludeeId < 0)
		{
			Debug.LogWarning("[OcclusionCulling] Occludee registration failed for " + base.name + ". Too many registered.");
		}
		this.state = global::OcclusionCulling.GetStateById(this.occludeeId);
	}

	// Token: 0x060025F6 RID: 9718 RVA: 0x000D1BAC File Offset: 0x000CFDAC
	public void Unregister()
	{
		global::OcclusionCulling.UnregisterOccludee(this.occludeeId);
	}

	// Token: 0x060025F7 RID: 9719 RVA: 0x000D1BBC File Offset: 0x000CFDBC
	protected virtual void OnVisibilityChanged(bool visible)
	{
		if (this.renderer != null)
		{
			this.renderer.enabled = visible;
		}
	}

	// Token: 0x04002235 RID: 8757
	public float minTimeVisible = 0.1f;

	// Token: 0x04002236 RID: 8758
	public bool isStatic = true;

	// Token: 0x04002237 RID: 8759
	public bool autoRegister;

	// Token: 0x04002238 RID: 8760
	public bool stickyGizmos;

	// Token: 0x04002239 RID: 8761
	public global::OccludeeState state;

	// Token: 0x0400223A RID: 8762
	protected int occludeeId = -1;

	// Token: 0x0400223B RID: 8763
	protected Vector3 center;

	// Token: 0x0400223C RID: 8764
	protected float radius;

	// Token: 0x0400223D RID: 8765
	protected Renderer renderer;

	// Token: 0x0400223E RID: 8766
	protected Collider collider;
}
