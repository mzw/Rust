﻿using System;
using UnityEngine;

// Token: 0x0200037B RID: 891
public class WaterCatcher : global::LiquidContainer
{
	// Token: 0x0600151C RID: 5404 RVA: 0x0007A0E8 File Offset: 0x000782E8
	public override void ServerInit()
	{
		base.ServerInit();
		this.AddResource(1);
		base.InvokeRandomized(new Action(this.CollectWater), 60f, 60f, 6f);
	}

	// Token: 0x0600151D RID: 5405 RVA: 0x0007A118 File Offset: 0x00078318
	private void CollectWater()
	{
		if (this.IsFull())
		{
			return;
		}
		float num = 0.25f;
		num += global::Climate.GetFog(base.transform.position) * 2f;
		if (this.TestIsOutside())
		{
			num += global::Climate.GetRain(base.transform.position);
			num += global::Climate.GetSnow(base.transform.position) * 0.5f;
		}
		this.AddResource(Mathf.CeilToInt(this.maxItemToCreate * num));
	}

	// Token: 0x0600151E RID: 5406 RVA: 0x0007A19C File Offset: 0x0007839C
	private bool IsFull()
	{
		return this.inventory.itemList.Count != 0 && this.inventory.itemList[0].amount >= this.inventory.maxStackSize;
	}

	// Token: 0x0600151F RID: 5407 RVA: 0x0007A1EC File Offset: 0x000783EC
	private bool TestIsOutside()
	{
		return !Physics.SphereCast(new Ray(base.transform.localToWorldMatrix.MultiplyPoint3x4(this.rainTestPosition), Vector3.up), this.rainTestSize, 256f, 1101070337);
	}

	// Token: 0x06001520 RID: 5408 RVA: 0x0007A238 File Offset: 0x00078438
	private void AddResource(int iAmount)
	{
		this.inventory.AddItem(this.itemToCreate, iAmount);
		base.UpdateOnFlag();
	}

	// Token: 0x04000F95 RID: 3989
	[Header("Water Catcher")]
	public global::ItemDefinition itemToCreate;

	// Token: 0x04000F96 RID: 3990
	public float maxItemToCreate = 10f;

	// Token: 0x04000F97 RID: 3991
	[Header("Outside Test")]
	public Vector3 rainTestPosition = new Vector3(0f, 1f, 0f);

	// Token: 0x04000F98 RID: 3992
	public float rainTestSize = 1f;

	// Token: 0x04000F99 RID: 3993
	private const float collectInterval = 60f;
}
