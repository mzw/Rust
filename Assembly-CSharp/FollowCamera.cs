﻿using System;
using UnityEngine;

// Token: 0x02000265 RID: 613
public class FollowCamera : MonoBehaviour, IClientComponent
{
	// Token: 0x0600107C RID: 4220 RVA: 0x00063CC8 File Offset: 0x00061EC8
	private void LateUpdate()
	{
		if (global::MainCamera.mainCamera == null)
		{
			return;
		}
		base.transform.position = global::MainCamera.position;
	}
}
