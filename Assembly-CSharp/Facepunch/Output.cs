﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Facepunch.Math;
using UnityEngine;

namespace Facepunch
{
	// Token: 0x02000343 RID: 835
	public static class Output
	{
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x0600140F RID: 5135 RVA: 0x00075328 File Offset: 0x00073528
		// (remove) Token: 0x06001410 RID: 5136 RVA: 0x0007535C File Offset: 0x0007355C
		public static event Action<string, string, LogType> OnMessage;

		// Token: 0x06001411 RID: 5137 RVA: 0x00075390 File Offset: 0x00073590
		public static void Install()
		{
			if (Output.installed)
			{
				return;
			}
			if (Output.<>f__mg$cache0 == null)
			{
				Output.<>f__mg$cache0 = new Application.LogCallback(Output.LogHandler);
			}
			Application.logMessageReceived += Output.<>f__mg$cache0;
			Output.installed = true;
		}

		// Token: 0x06001412 RID: 5138 RVA: 0x000753C8 File Offset: 0x000735C8
		internal static void LogHandler(string log, string stacktrace, LogType type)
		{
			if (Output.OnMessage == null)
			{
				return;
			}
			if (log.StartsWith("Skipped frame because GfxDevice"))
			{
				return;
			}
			if (log.Contains("HandleD3DDeviceLost"))
			{
				return;
			}
			if (log.Contains("ResetD3DDevice"))
			{
				return;
			}
			if (log.Contains("dev->Reset"))
			{
				return;
			}
			if (log.Contains("D3Dwindow device not lost anymore"))
			{
				return;
			}
			if (log.Contains("D3D device reset"))
			{
				return;
			}
			if (log.Contains("group < 0xfff"))
			{
				return;
			}
			if (log.Contains("Mesh can not have more than 65000 vert"))
			{
				return;
			}
			if (log.Contains("Trying to add (Layout Rebuilder for)"))
			{
				return;
			}
			if (log.Contains("Coroutine continue failure"))
			{
				return;
			}
			if (log.Contains("No texture data available to upload"))
			{
				return;
			}
			if (log.Contains("Trying to reload asset from disk that is not"))
			{
				return;
			}
			if (log.Contains("Unable to find shaders used for the terrain engine."))
			{
				return;
			}
			if (log.Contains("Canvas element contains more than 65535 vertices"))
			{
				return;
			}
			if (log.Contains("RectTransform.set_anchorMin"))
			{
				return;
			}
			if (log.Contains("FMOD failed to initialize the output device"))
			{
				return;
			}
			if (log.Contains("Cannot create FMOD::Sound"))
			{
				return;
			}
			if (log.Contains("invalid utf-16 sequence"))
			{
				return;
			}
			if (log.Contains("missing surrogate tail"))
			{
				return;
			}
			if (log.Contains("Failed to create agent because it is not close enough to the Nav"))
			{
				return;
			}
			if (log.Contains("user-provided triangle mesh descriptor is invalid"))
			{
				return;
			}
			if (log.Contains("Releasing render texture that is set as"))
			{
				return;
			}
			Output.OnMessage(log, stacktrace, type);
			Output.HistoryOutput.Add(new Output.Entry
			{
				Message = log,
				Stacktrace = stacktrace,
				Type = type.ToString(),
				Time = Epoch.Current
			});
			while (Output.HistoryOutput.Count > 65536)
			{
				Output.HistoryOutput.RemoveAt(0);
			}
		}

		// Token: 0x04000EDD RID: 3805
		public static bool installed = false;

		// Token: 0x04000EDE RID: 3806
		public static List<Output.Entry> HistoryOutput = new List<Output.Entry>();

		// Token: 0x04000EDF RID: 3807
		[CompilerGenerated]
		private static Application.LogCallback <>f__mg$cache0;

		// Token: 0x02000344 RID: 836
		public struct Entry
		{
			// Token: 0x04000EE0 RID: 3808
			public string Message;

			// Token: 0x04000EE1 RID: 3809
			public string Stacktrace;

			// Token: 0x04000EE2 RID: 3810
			public string Type;

			// Token: 0x04000EE3 RID: 3811
			public int Time;
		}
	}
}
