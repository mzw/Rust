﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using ConVar;
using Facepunch.Rcon;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Oxide.Core;
using UnityEngine;

namespace Facepunch
{
	// Token: 0x02000346 RID: 838
	public class RCon
	{
		// Token: 0x06001418 RID: 5144 RVA: 0x000756B4 File Offset: 0x000738B4
		public static void Initialize()
		{
			if (Interface.CallHook("IOnRconInitialize", null) != null)
			{
				return;
			}
			if (RCon.Port == 0)
			{
				RCon.Port = ConVar.Server.port;
			}
			RCon.Password = CommandLine.GetSwitch("-rcon.password", CommandLine.GetSwitch("+rcon.password", string.Empty));
			if (RCon.Password == "password")
			{
				return;
			}
			if (RCon.Password == string.Empty)
			{
				return;
			}
			if (RCon.<>f__mg$cache0 == null)
			{
				RCon.<>f__mg$cache0 = new Action<string, string, UnityEngine.LogType>(RCon.OnMessage);
			}
			Output.OnMessage += RCon.<>f__mg$cache0;
			if (RCon.Web)
			{
				RCon.listenerNew = new Listener();
				if (!string.IsNullOrEmpty(RCon.Ip))
				{
					RCon.listenerNew.Address = RCon.Ip;
				}
				RCon.listenerNew.Password = RCon.Password;
				RCon.listenerNew.Port = RCon.Port;
				RCon.listenerNew.SslCertificate = CommandLine.GetSwitch("-rcon.ssl", null);
				RCon.listenerNew.SslCertificatePassword = CommandLine.GetSwitch("-rcon.sslpwd", null);
				RCon.listenerNew.OnMessage = delegate(IPEndPoint ip, string id, string msg)
				{
					object commands = RCon.Commands;
					lock (commands)
					{
						RCon.Command item = JsonConvert.DeserializeObject<RCon.Command>(msg);
						item.Ip = ip;
						item.ConnectionId = id;
						RCon.Commands.Enqueue(item);
					}
				};
				RCon.listenerNew.Start();
				Debug.Log("WebSocket RCon Started on " + RCon.Port);
			}
			else
			{
				RCon.listener = new RCon.RConListener();
				Debug.Log("RCon Started on " + RCon.Port);
				Debug.Log("Source style TCP Rcon is deprecated. Please switch to Websocket Rcon before it goes away.");
			}
		}

		// Token: 0x06001419 RID: 5145 RVA: 0x00075844 File Offset: 0x00073A44
		public static void Shutdown()
		{
			if (RCon.listenerNew != null)
			{
				RCon.listenerNew.Shutdown();
				RCon.listenerNew = null;
			}
			if (RCon.listener != null)
			{
				RCon.listener.Shutdown();
				RCon.listener = null;
			}
		}

		// Token: 0x0600141A RID: 5146 RVA: 0x0007587C File Offset: 0x00073A7C
		public static void Broadcast(RCon.LogType type, object obj)
		{
			if (RCon.listenerNew == null)
			{
				return;
			}
			RCon.Response response = default(RCon.Response);
			response.Identifier = -1;
			response.Message = JsonConvert.SerializeObject(obj, 1);
			response.Type = type;
			if (string.IsNullOrEmpty(RCon.responseConnection))
			{
				RCon.listenerNew.BroadcastMessage(JsonConvert.SerializeObject(response, 1));
			}
			else
			{
				RCon.listenerNew.SendMessage(RCon.responseConnection, JsonConvert.SerializeObject(response, 1));
			}
		}

		// Token: 0x0600141B RID: 5147 RVA: 0x00075900 File Offset: 0x00073B00
		public static void Update()
		{
			object commands = RCon.Commands;
			lock (commands)
			{
				while (RCon.Commands.Count > 0)
				{
					RCon.Command cmd = RCon.Commands.Dequeue();
					RCon.OnCommand(cmd);
				}
			}
			if (RCon.listener == null)
			{
				return;
			}
			if (RCon.lastRunTime + 0.02f >= UnityEngine.Time.realtimeSinceStartup)
			{
				return;
			}
			RCon.lastRunTime = UnityEngine.Time.realtimeSinceStartup;
			try
			{
				RCon.bannedAddresses.RemoveAll((RCon.BannedAddresses x) => x.banTime < UnityEngine.Time.realtimeSinceStartup);
				RCon.listener.Cycle();
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Rcon Exception");
				Debug.LogException(ex);
			}
		}

		// Token: 0x0600141C RID: 5148 RVA: 0x000759E0 File Offset: 0x00073BE0
		public static void BanIP(IPAddress addr, float seconds)
		{
			RCon.bannedAddresses.RemoveAll((RCon.BannedAddresses x) => x.addr == addr);
			RCon.BannedAddresses bannedAddresses = default(RCon.BannedAddresses);
			bannedAddresses.addr = addr;
			bannedAddresses.banTime = UnityEngine.Time.realtimeSinceStartup + seconds;
		}

		// Token: 0x0600141D RID: 5149 RVA: 0x00075A34 File Offset: 0x00073C34
		public static bool IsBanned(IPAddress addr)
		{
			return RCon.bannedAddresses.Count((RCon.BannedAddresses x) => x.addr == addr && x.banTime > UnityEngine.Time.realtimeSinceStartup) > 0;
		}

		// Token: 0x0600141E RID: 5150 RVA: 0x00075A68 File Offset: 0x00073C68
		private static void OnCommand(RCon.Command cmd)
		{
			try
			{
				RCon.responseIdentifier = cmd.Identifier;
				RCon.responseConnection = cmd.ConnectionId;
				RCon.isInput = true;
				if (RCon.Print)
				{
					Debug.Log(string.Concat(new object[]
					{
						"[rcon] ",
						cmd.Ip,
						": ",
						cmd.Message
					}));
				}
				RCon.isInput = false;
				string text = ConsoleSystem.Run(ConsoleSystem.Option.Server.Quiet(), cmd.Message, new object[0]);
				if (text != null)
				{
					RCon.OnMessage(text, string.Empty, 3);
				}
			}
			finally
			{
				RCon.responseIdentifier = 0;
				RCon.responseConnection = string.Empty;
			}
		}

		// Token: 0x0600141F RID: 5151 RVA: 0x00075B30 File Offset: 0x00073D30
		private static void OnMessage(string message, string stacktrace, UnityEngine.LogType type)
		{
			if (RCon.isInput)
			{
				return;
			}
			if (RCon.listenerNew != null)
			{
				RCon.Response response = default(RCon.Response);
				response.Identifier = RCon.responseIdentifier;
				response.Message = message;
				response.Stacktrace = stacktrace;
				response.Type = RCon.LogType.Generic;
				if (type == null || type == 4)
				{
					response.Type = RCon.LogType.Error;
				}
				if (type == 1 || type == 2)
				{
					response.Type = RCon.LogType.Warning;
				}
				if (string.IsNullOrEmpty(RCon.responseConnection))
				{
					RCon.listenerNew.BroadcastMessage(JsonConvert.SerializeObject(response, 1));
				}
				else
				{
					RCon.listenerNew.SendMessage(RCon.responseConnection, JsonConvert.SerializeObject(response, 1));
				}
			}
		}

		// Token: 0x04000EE8 RID: 3816
		public static string Password = string.Empty;

		// Token: 0x04000EE9 RID: 3817
		[ServerVar]
		public static int Port = 0;

		// Token: 0x04000EEA RID: 3818
		[ServerVar]
		public static string Ip = string.Empty;

		// Token: 0x04000EEB RID: 3819
		[ServerVar(Help = "If set to true, use websocket rcon. If set to false use legacy, source engine rcon.")]
		public static bool Web = true;

		// Token: 0x04000EEC RID: 3820
		[ServerVar(Help = "If true, rcon commands etc will be printed in the console")]
		public static bool Print = false;

		// Token: 0x04000EED RID: 3821
		internal static RCon.RConListener listener = null;

		// Token: 0x04000EEE RID: 3822
		internal static Listener listenerNew = null;

		// Token: 0x04000EEF RID: 3823
		private static Queue<RCon.Command> Commands = new Queue<RCon.Command>();

		// Token: 0x04000EF0 RID: 3824
		private static float lastRunTime = 0f;

		// Token: 0x04000EF1 RID: 3825
		internal static List<RCon.BannedAddresses> bannedAddresses = new List<RCon.BannedAddresses>();

		// Token: 0x04000EF2 RID: 3826
		private static int responseIdentifier;

		// Token: 0x04000EF3 RID: 3827
		private static string responseConnection;

		// Token: 0x04000EF4 RID: 3828
		private static bool isInput;

		// Token: 0x04000EF5 RID: 3829
		internal static int SERVERDATA_AUTH = 3;

		// Token: 0x04000EF6 RID: 3830
		internal static int SERVERDATA_EXECCOMMAND = 2;

		// Token: 0x04000EF7 RID: 3831
		internal static int SERVERDATA_AUTH_RESPONSE = 2;

		// Token: 0x04000EF8 RID: 3832
		internal static int SERVERDATA_RESPONSE_VALUE = 0;

		// Token: 0x04000EF9 RID: 3833
		internal static int SERVERDATA_CONSOLE_LOG = 4;

		// Token: 0x04000EFA RID: 3834
		internal static int SERVERDATA_SWITCH_UTF8 = 5;

		// Token: 0x04000EFB RID: 3835
		[CompilerGenerated]
		private static Action<string, string, UnityEngine.LogType> <>f__mg$cache0;

		// Token: 0x02000347 RID: 839
		public struct Command
		{
			// Token: 0x04000EFE RID: 3838
			public IPEndPoint Ip;

			// Token: 0x04000EFF RID: 3839
			public string ConnectionId;

			// Token: 0x04000F00 RID: 3840
			public string Name;

			// Token: 0x04000F01 RID: 3841
			public string Message;

			// Token: 0x04000F02 RID: 3842
			public int Identifier;
		}

		// Token: 0x02000348 RID: 840
		public enum LogType
		{
			// Token: 0x04000F04 RID: 3844
			Generic,
			// Token: 0x04000F05 RID: 3845
			Error,
			// Token: 0x04000F06 RID: 3846
			Warning,
			// Token: 0x04000F07 RID: 3847
			Chat
		}

		// Token: 0x02000349 RID: 841
		public struct Response
		{
			// Token: 0x04000F08 RID: 3848
			public string Message;

			// Token: 0x04000F09 RID: 3849
			public int Identifier;

			// Token: 0x04000F0A RID: 3850
			[JsonConverter(typeof(StringEnumConverter))]
			public RCon.LogType Type;

			// Token: 0x04000F0B RID: 3851
			public string Stacktrace;
		}

		// Token: 0x0200034A RID: 842
		internal struct BannedAddresses
		{
			// Token: 0x04000F0C RID: 3852
			public IPAddress addr;

			// Token: 0x04000F0D RID: 3853
			public float banTime;
		}

		// Token: 0x0200034B RID: 843
		internal class RConClient
		{
			// Token: 0x06001423 RID: 5155 RVA: 0x00075CDC File Offset: 0x00073EDC
			internal RConClient(Socket cl)
			{
				this.socket = cl;
				this.socket.NoDelay = true;
				this.connectionName = this.socket.RemoteEndPoint.ToString();
			}

			// Token: 0x06001424 RID: 5156 RVA: 0x00075D14 File Offset: 0x00073F14
			internal bool IsValid()
			{
				return this.socket != null;
			}

			// Token: 0x06001425 RID: 5157 RVA: 0x00075D24 File Offset: 0x00073F24
			internal void Update()
			{
				if (this.socket == null)
				{
					return;
				}
				if (!this.socket.Connected)
				{
					this.Close("Disconnected");
					return;
				}
				int available = this.socket.Available;
				if (available < 14)
				{
					return;
				}
				if (available > 4096)
				{
					this.Close("overflow");
					return;
				}
				byte[] buffer = new byte[available];
				this.socket.Receive(buffer);
				using (BinaryReader binaryReader = new BinaryReader(new MemoryStream(buffer, false), (!this.utf8Mode) ? Encoding.ASCII : Encoding.UTF8))
				{
					int num = binaryReader.ReadInt32();
					if (available < num)
					{
						this.Close("invalid packet");
					}
					else
					{
						this.lastMessageID = binaryReader.ReadInt32();
						int type = binaryReader.ReadInt32();
						string msg = this.ReadNullTerminatedString(binaryReader);
						this.ReadNullTerminatedString(binaryReader);
						if (!this.HandleMessage(type, msg))
						{
							this.Close("invalid packet");
						}
						else
						{
							this.lastMessageID = -1;
						}
					}
				}
			}

			// Token: 0x06001426 RID: 5158 RVA: 0x00075E4C File Offset: 0x0007404C
			internal bool HandleMessage(int type, string msg)
			{
				if (!this.isAuthorised)
				{
					return this.HandleMessage_UnAuthed(type, msg);
				}
				if (type == RCon.SERVERDATA_SWITCH_UTF8)
				{
					this.utf8Mode = true;
					return true;
				}
				if (type == RCon.SERVERDATA_EXECCOMMAND)
				{
					Debug.Log("[RCON][" + this.connectionName + "] " + msg);
					this.runningConsoleCommand = true;
					ConsoleSystem.Run(ConsoleSystem.Option.Server, msg, new object[0]);
					this.runningConsoleCommand = false;
					this.Reply(-1, RCon.SERVERDATA_RESPONSE_VALUE, string.Empty);
					return true;
				}
				if (type == RCon.SERVERDATA_RESPONSE_VALUE)
				{
					this.Reply(this.lastMessageID, RCon.SERVERDATA_RESPONSE_VALUE, string.Empty);
					return true;
				}
				Debug.Log(string.Concat(new object[]
				{
					"[RCON][",
					this.connectionName,
					"] Unhandled: ",
					this.lastMessageID,
					" -> ",
					type,
					" -> ",
					msg
				}));
				return false;
			}

			// Token: 0x06001427 RID: 5159 RVA: 0x00075F54 File Offset: 0x00074154
			internal bool HandleMessage_UnAuthed(int type, string msg)
			{
				if (type != RCon.SERVERDATA_AUTH)
				{
					IPEndPoint ipendPoint = this.socket.RemoteEndPoint as IPEndPoint;
					RCon.BanIP(ipendPoint.Address, 60f);
					this.Close("Invalid Command - Not Authed");
					return false;
				}
				this.Reply(this.lastMessageID, RCon.SERVERDATA_RESPONSE_VALUE, string.Empty);
				this.isAuthorised = (RCon.Password == msg);
				if (!this.isAuthorised)
				{
					this.Reply(-1, RCon.SERVERDATA_AUTH_RESPONSE, string.Empty);
					IPEndPoint ipendPoint2 = this.socket.RemoteEndPoint as IPEndPoint;
					RCon.BanIP(ipendPoint2.Address, 60f);
					this.Close("Invalid Password");
					return true;
				}
				this.Reply(this.lastMessageID, RCon.SERVERDATA_AUTH_RESPONSE, string.Empty);
				Debug.Log("[RCON] Auth: " + this.connectionName);
				Output.OnMessage += this.Output_OnMessage;
				return true;
			}

			// Token: 0x06001428 RID: 5160 RVA: 0x00076048 File Offset: 0x00074248
			private void Output_OnMessage(string message, string stacktrace, UnityEngine.LogType type)
			{
				if (!this.isAuthorised)
				{
					return;
				}
				if (!this.IsValid())
				{
					return;
				}
				if (this.lastMessageID != -1 && this.runningConsoleCommand)
				{
					this.Reply(this.lastMessageID, RCon.SERVERDATA_RESPONSE_VALUE, message);
				}
				this.Reply(0, RCon.SERVERDATA_CONSOLE_LOG, message);
			}

			// Token: 0x06001429 RID: 5161 RVA: 0x000760A4 File Offset: 0x000742A4
			internal void Reply(int id, int type, string msg)
			{
				MemoryStream memoryStream = new MemoryStream(1024);
				using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
				{
					if (this.utf8Mode)
					{
						byte[] bytes = Encoding.UTF8.GetBytes(msg);
						int value = 10 + bytes.Length;
						binaryWriter.Write(value);
						binaryWriter.Write(id);
						binaryWriter.Write(type);
						binaryWriter.Write(bytes);
					}
					else
					{
						int value2 = 10 + msg.Length;
						binaryWriter.Write(value2);
						binaryWriter.Write(id);
						binaryWriter.Write(type);
						foreach (char c in msg)
						{
							binaryWriter.Write((sbyte)c);
						}
					}
					binaryWriter.Write(0);
					binaryWriter.Write(0);
					binaryWriter.Flush();
					try
					{
						this.socket.Send(memoryStream.GetBuffer(), (int)memoryStream.Position, SocketFlags.None);
					}
					catch (Exception arg)
					{
						Debug.LogWarning("Error sending rcon reply: " + arg);
						this.Close("Exception");
					}
				}
			}

			// Token: 0x0600142A RID: 5162 RVA: 0x000761DC File Offset: 0x000743DC
			internal void Close(string strReasn)
			{
				Output.OnMessage -= this.Output_OnMessage;
				if (this.socket == null)
				{
					return;
				}
				Debug.Log("[RCON][" + this.connectionName + "] Disconnected: " + strReasn);
				this.socket.Close();
				this.socket = null;
			}

			// Token: 0x0600142B RID: 5163 RVA: 0x00076234 File Offset: 0x00074434
			internal string ReadNullTerminatedString(BinaryReader read)
			{
				string text = string.Empty;
				while (read.BaseStream.Position != read.BaseStream.Length)
				{
					char c = read.ReadChar();
					if (c == '\0')
					{
						return text;
					}
					text += c;
					if (text.Length > 8192)
					{
						return string.Empty;
					}
				}
				return string.Empty;
			}

			// Token: 0x04000F0E RID: 3854
			private Socket socket;

			// Token: 0x04000F0F RID: 3855
			private bool isAuthorised;

			// Token: 0x04000F10 RID: 3856
			private string connectionName;

			// Token: 0x04000F11 RID: 3857
			private int lastMessageID = -1;

			// Token: 0x04000F12 RID: 3858
			private bool runningConsoleCommand;

			// Token: 0x04000F13 RID: 3859
			private bool utf8Mode;
		}

		// Token: 0x0200034C RID: 844
		internal class RConListener
		{
			// Token: 0x0600142C RID: 5164 RVA: 0x000762A0 File Offset: 0x000744A0
			internal RConListener()
			{
				IPAddress any = IPAddress.Any;
				if (!IPAddress.TryParse(RCon.Ip, out any))
				{
					any = IPAddress.Any;
				}
				this.server = new TcpListener(any, RCon.Port);
				try
				{
					this.server.Start();
				}
				catch (Exception ex)
				{
					Debug.LogWarning("Couldn't start RCON Listener: " + ex.Message);
					this.server = null;
				}
			}

			// Token: 0x0600142D RID: 5165 RVA: 0x00076330 File Offset: 0x00074530
			internal void Shutdown()
			{
				if (this.server != null)
				{
					this.server.Stop();
					this.server = null;
				}
			}

			// Token: 0x0600142E RID: 5166 RVA: 0x00076350 File Offset: 0x00074550
			internal void Cycle()
			{
				if (this.server == null)
				{
					return;
				}
				this.ProcessConnections();
				this.RemoveDeadClients();
				this.UpdateClients();
			}

			// Token: 0x0600142F RID: 5167 RVA: 0x00076370 File Offset: 0x00074570
			private void ProcessConnections()
			{
				if (!this.server.Pending())
				{
					return;
				}
				Socket socket = this.server.AcceptSocket();
				if (socket == null)
				{
					return;
				}
				IPEndPoint ipendPoint = socket.RemoteEndPoint as IPEndPoint;
				if (Interface.CallHook("OnRconConnection", null) != null)
				{
					socket.Close();
					return;
				}
				if (RCon.IsBanned(ipendPoint.Address))
				{
					Debug.Log("[RCON] Ignoring connection - banned. " + ipendPoint.Address.ToString());
					socket.Close();
					return;
				}
				this.clients.Add(new RCon.RConClient(socket));
			}

			// Token: 0x06001430 RID: 5168 RVA: 0x00076404 File Offset: 0x00074604
			private void UpdateClients()
			{
				foreach (RCon.RConClient rconClient in this.clients)
				{
					rconClient.Update();
				}
			}

			// Token: 0x06001431 RID: 5169 RVA: 0x00076460 File Offset: 0x00074660
			private void RemoveDeadClients()
			{
				this.clients.RemoveAll((RCon.RConClient x) => !x.IsValid());
			}

			// Token: 0x04000F14 RID: 3860
			private TcpListener server;

			// Token: 0x04000F15 RID: 3861
			private List<RCon.RConClient> clients = new List<RCon.RConClient>();
		}
	}
}
