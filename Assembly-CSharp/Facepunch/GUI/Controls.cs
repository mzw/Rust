﻿using System;
using UnityEngine;

namespace Facepunch.GUI
{
	// Token: 0x02000340 RID: 832
	public static class Controls
	{
		// Token: 0x06001404 RID: 5124 RVA: 0x00074FFC File Offset: 0x000731FC
		public static float FloatSlider(string strLabel, float value, float low, float high, string format = "0.00")
		{
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			GUILayout.Label(strLabel, new GUILayoutOption[]
			{
				GUILayout.Width(Controls.labelWidth)
			});
			float num = float.Parse(GUILayout.TextField(value.ToString(format), new GUILayoutOption[]
			{
				GUILayout.ExpandWidth(true)
			}));
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			num = GUILayout.HorizontalSlider(num, low, high, new GUILayoutOption[0]);
			GUILayout.EndHorizontal();
			return num;
		}

		// Token: 0x06001405 RID: 5125 RVA: 0x00075078 File Offset: 0x00073278
		public static int IntSlider(string strLabel, int value, int low, int high, string format = "0")
		{
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			GUILayout.Label(strLabel, new GUILayoutOption[]
			{
				GUILayout.Width(Controls.labelWidth)
			});
			int num = int.Parse(GUILayout.TextField(value.ToString(format), new GUILayoutOption[]
			{
				GUILayout.ExpandWidth(true)
			}));
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			num = (int)GUILayout.HorizontalSlider((float)num, (float)low, (float)high, new GUILayoutOption[0]);
			GUILayout.EndHorizontal();
			return num;
		}

		// Token: 0x06001406 RID: 5126 RVA: 0x000750F8 File Offset: 0x000732F8
		public static string TextArea(string strName, string value)
		{
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			GUILayout.Label(strName, new GUILayoutOption[]
			{
				GUILayout.Width(Controls.labelWidth)
			});
			string result = GUILayout.TextArea(value, new GUILayoutOption[0]);
			GUILayout.EndHorizontal();
			return result;
		}

		// Token: 0x06001407 RID: 5127 RVA: 0x0007513C File Offset: 0x0007333C
		public static bool Checkbox(string strName, bool value)
		{
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			GUILayout.Label(strName, new GUILayoutOption[]
			{
				GUILayout.Width(Controls.labelWidth)
			});
			bool result = GUILayout.Toggle(value, string.Empty, new GUILayoutOption[0]);
			GUILayout.EndHorizontal();
			return result;
		}

		// Token: 0x06001408 RID: 5128 RVA: 0x00075188 File Offset: 0x00073388
		public static bool Button(string strName)
		{
			GUILayout.BeginHorizontal(new GUILayoutOption[0]);
			bool result = GUILayout.Button(strName, new GUILayoutOption[0]);
			GUILayout.EndHorizontal();
			return result;
		}

		// Token: 0x04000ED7 RID: 3799
		public static float labelWidth = 100f;
	}
}
