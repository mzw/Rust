﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Facepunch.GUI
{
	// Token: 0x02000341 RID: 833
	internal class TabbedPanel
	{
		// Token: 0x17000170 RID: 368
		// (get) Token: 0x0600140B RID: 5131 RVA: 0x000751D4 File Offset: 0x000733D4
		public TabbedPanel.Tab selectedTab
		{
			get
			{
				return this.tabs[this.selectedTabID];
			}
		}

		// Token: 0x0600140C RID: 5132 RVA: 0x000751E8 File Offset: 0x000733E8
		public void Add(TabbedPanel.Tab tab)
		{
			this.tabs.Add(tab);
		}

		// Token: 0x0600140D RID: 5133 RVA: 0x000751F8 File Offset: 0x000733F8
		internal void DrawVertical(float width)
		{
			GUILayout.BeginVertical(new GUILayoutOption[]
			{
				GUILayout.Width(width),
				GUILayout.ExpandHeight(true)
			});
			for (int i = 0; i < this.tabs.Count; i++)
			{
				if (GUILayout.Toggle(this.selectedTabID == i, this.tabs[i].name, new GUIStyle("devtab"), new GUILayoutOption[0]))
				{
					this.selectedTabID = i;
				}
			}
			if (GUILayout.Toggle(false, string.Empty, new GUIStyle("devtab"), new GUILayoutOption[]
			{
				GUILayout.ExpandHeight(true)
			}))
			{
				this.selectedTabID = -1;
			}
			GUILayout.EndVertical();
		}

		// Token: 0x0600140E RID: 5134 RVA: 0x000752BC File Offset: 0x000734BC
		internal void DrawContents()
		{
			if (this.selectedTabID < 0)
			{
				return;
			}
			TabbedPanel.Tab selectedTab = this.selectedTab;
			GUILayout.BeginVertical(new GUIStyle("devtabcontents"), new GUILayoutOption[]
			{
				GUILayout.ExpandHeight(true),
				GUILayout.ExpandWidth(true)
			});
			if (selectedTab.drawFunc != null)
			{
				selectedTab.drawFunc();
			}
			GUILayout.EndVertical();
		}

		// Token: 0x04000ED8 RID: 3800
		private int selectedTabID;

		// Token: 0x04000ED9 RID: 3801
		private List<TabbedPanel.Tab> tabs = new List<TabbedPanel.Tab>();

		// Token: 0x02000342 RID: 834
		public struct Tab
		{
			// Token: 0x04000EDA RID: 3802
			public string name;

			// Token: 0x04000EDB RID: 3803
			public Action drawFunc;
		}
	}
}
