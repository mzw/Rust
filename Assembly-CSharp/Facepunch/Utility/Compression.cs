﻿using System;
using Ionic.Zlib;

namespace Facepunch.Utility
{
	// Token: 0x0200034F RID: 847
	public class Compression
	{
		// Token: 0x06001438 RID: 5176 RVA: 0x000764EC File Offset: 0x000746EC
		public static byte[] Compress(byte[] data)
		{
			byte[] result;
			try
			{
				byte[] array = GZipStream.CompressBuffer(data);
				result = array;
			}
			catch (Exception)
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06001439 RID: 5177 RVA: 0x00076520 File Offset: 0x00074720
		public static byte[] Uncompress(byte[] data)
		{
			return GZipStream.UncompressBuffer(data);
		}
	}
}
