﻿using System;
using ConVar;

namespace Facepunch.Rust
{
	// Token: 0x020001CB RID: 459
	public static class Analytics
	{
		// Token: 0x06000EEE RID: 3822 RVA: 0x0005C4A4 File Offset: 0x0005A6A4
		internal static void Death(string v)
		{
			if (!ConVar.Server.official)
			{
				return;
			}
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x0005C4B4 File Offset: 0x0005A6B4
		public static void Crafting(string targetItemShortname, int taskSkinId)
		{
			if (!ConVar.Server.official)
			{
				return;
			}
		}
	}
}
