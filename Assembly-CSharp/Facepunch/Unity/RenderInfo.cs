﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Facepunch.Unity
{
	// Token: 0x020002F5 RID: 757
	public static class RenderInfo
	{
		// Token: 0x06001329 RID: 4905 RVA: 0x000709B0 File Offset: 0x0006EBB0
		public static void GenerateReport()
		{
			Renderer[] array = Object.FindObjectsOfType<Renderer>();
			List<RenderInfo.RendererInstance> list = new List<RenderInfo.RendererInstance>();
			foreach (Renderer renderer in array)
			{
				RenderInfo.RendererInstance item = RenderInfo.RendererInstance.From(renderer);
				list.Add(item);
			}
			string text = string.Format(Application.dataPath + "/../RenderInfo-{0:yyyy-MM-dd_hh-mm-ss-tt}.txt", DateTime.Now);
			string contents = JsonConvert.SerializeObject(list, 1);
			File.WriteAllText(text, contents);
			string text2 = Application.streamingAssetsPath + "/RenderInfo.exe";
			string text3 = "\"" + text + "\"";
			Debug.Log("Launching " + text2 + " " + text3);
			Process.Start(text2, text3);
		}

		// Token: 0x020002F6 RID: 758
		public struct RendererInstance
		{
			// Token: 0x0600132A RID: 4906 RVA: 0x00070A70 File Offset: 0x0006EC70
			public static RenderInfo.RendererInstance From(Renderer renderer)
			{
				RenderInfo.RendererInstance result = default(RenderInfo.RendererInstance);
				result.IsVisible = renderer.isVisible;
				result.CastShadows = (renderer.shadowCastingMode != 0);
				result.RecieveShadows = renderer.receiveShadows;
				result.Enabled = (renderer.enabled && renderer.gameObject.activeInHierarchy);
				result.Size = renderer.bounds.size.magnitude;
				result.Distance = Vector3.Distance(renderer.bounds.center, Camera.main.transform.position);
				result.MaterialCount = renderer.sharedMaterials.Length;
				result.RenderType = renderer.GetType().Name;
				global::BaseEntity baseEntity = renderer.gameObject.ToBaseEntity();
				if (baseEntity)
				{
					result.EntityName = baseEntity.PrefabName;
					if (baseEntity.net != null)
					{
						result.EntityId = baseEntity.net.ID;
					}
				}
				else
				{
					result.ObjectName = renderer.transform.GetRecursiveName(string.Empty);
				}
				if (renderer is MeshRenderer)
				{
					result.BoneCount = 0;
					MeshFilter component = renderer.GetComponent<MeshFilter>();
					if (component)
					{
						result.ReadMesh(component.sharedMesh);
					}
				}
				if (renderer is SkinnedMeshRenderer)
				{
					SkinnedMeshRenderer skinnedMeshRenderer = renderer as SkinnedMeshRenderer;
					result.ReadMesh(skinnedMeshRenderer.sharedMesh);
					result.UpdateWhenOffscreen = skinnedMeshRenderer.updateWhenOffscreen;
				}
				if (renderer is ParticleSystemRenderer)
				{
					ParticleSystem component2 = renderer.GetComponent<ParticleSystem>();
					if (component2)
					{
						result.MeshName = component2.name;
						result.ParticleCount = component2.particleCount;
					}
				}
				return result;
			}

			// Token: 0x0600132B RID: 4907 RVA: 0x00070C3C File Offset: 0x0006EE3C
			public void ReadMesh(Mesh mesh)
			{
				if (mesh == null)
				{
					this.MeshName = "<NULL>";
					return;
				}
				this.VertexCount = mesh.vertexCount;
				this.SubMeshCount = mesh.subMeshCount;
				this.BlendShapeCount = mesh.blendShapeCount;
				this.MeshName = mesh.name;
			}

			// Token: 0x04000DDA RID: 3546
			public bool IsVisible;

			// Token: 0x04000DDB RID: 3547
			public bool CastShadows;

			// Token: 0x04000DDC RID: 3548
			public bool Enabled;

			// Token: 0x04000DDD RID: 3549
			public bool RecieveShadows;

			// Token: 0x04000DDE RID: 3550
			public float Size;

			// Token: 0x04000DDF RID: 3551
			public float Distance;

			// Token: 0x04000DE0 RID: 3552
			public int BoneCount;

			// Token: 0x04000DE1 RID: 3553
			public int MaterialCount;

			// Token: 0x04000DE2 RID: 3554
			public int VertexCount;

			// Token: 0x04000DE3 RID: 3555
			public int TriangleCount;

			// Token: 0x04000DE4 RID: 3556
			public int SubMeshCount;

			// Token: 0x04000DE5 RID: 3557
			public int BlendShapeCount;

			// Token: 0x04000DE6 RID: 3558
			public string RenderType;

			// Token: 0x04000DE7 RID: 3559
			public string MeshName;

			// Token: 0x04000DE8 RID: 3560
			public string ObjectName;

			// Token: 0x04000DE9 RID: 3561
			public string EntityName;

			// Token: 0x04000DEA RID: 3562
			public uint EntityId;

			// Token: 0x04000DEB RID: 3563
			public bool UpdateWhenOffscreen;

			// Token: 0x04000DEC RID: 3564
			public int ParticleCount;
		}
	}
}
