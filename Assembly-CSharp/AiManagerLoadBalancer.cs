﻿using System;
using Apex.LoadBalancing;

// Token: 0x02000177 RID: 375
public sealed class AiManagerLoadBalancer : Apex.LoadBalancing.LoadBalancer
{
	// Token: 0x06000D49 RID: 3401 RVA: 0x00054270 File Offset: 0x00052470
	private AiManagerLoadBalancer()
	{
	}

	// Token: 0x0400074F RID: 1871
	public static readonly ILoadBalancer aiManagerLoadBalancer = new LoadBalancedQueue(1, 2.5f, 1, 4);
}
