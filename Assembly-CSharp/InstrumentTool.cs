﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000072 RID: 114
public class InstrumentTool : global::HeldEntity
{
	// Token: 0x0600080B RID: 2059 RVA: 0x00034200 File Offset: 0x00032400
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("InstrumentTool.OnRpcMessage", 0.1f))
		{
			if (rpc == 2224061577u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SVPlayNote ");
				}
				using (TimeWarning.New("SVPlayNote", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("SVPlayNote", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SVPlayNote(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in SVPlayNote");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600080C RID: 2060 RVA: 0x000343DC File Offset: 0x000325DC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void SVPlayNote(global::BaseEntity.RPCMessage msg)
	{
		byte b = msg.read.UInt8();
		float scale = msg.read.Float();
		global::EffectNetwork.Send(new global::Effect(this.soundEffect[(int)b].resourcePath, this, 0u, Vector3.zero, Vector3.forward, msg.connection)
		{
			scale = scale
		});
	}

	// Token: 0x040003D0 RID: 976
	public global::GameObjectRef[] soundEffect = new global::GameObjectRef[2];
}
