﻿using System;
using UnityEngine;

// Token: 0x0200046C RID: 1132
[CreateAssetMenu(menuName = "Rust/Armor Properties")]
public class ArmorProperties : ScriptableObject
{
	// Token: 0x060018D8 RID: 6360 RVA: 0x0008BF98 File Offset: 0x0008A198
	public bool Contains(global::HitArea hitArea)
	{
		return (this.area & hitArea) != (global::HitArea)0;
	}

	// Token: 0x0400138A RID: 5002
	[InspectorFlags]
	public global::HitArea area;
}
