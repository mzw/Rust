﻿using System;
using System.Collections.Generic;
using ProtoBuf;

// Token: 0x020003B0 RID: 944
public class VendingMachineMapMarker : global::MapMarker
{
	// Token: 0x06001666 RID: 5734 RVA: 0x00081950 File Offset: 0x0007FB50
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.vendingMachine = new ProtoBuf.VendingMachine();
		info.msg.vendingMachine.shopName = this.markerShopName;
		if (this.server_vendingMachine != null)
		{
			info.msg.vendingMachine.sellOrderContainer = new ProtoBuf.VendingMachine.SellOrderContainer();
			info.msg.vendingMachine.sellOrderContainer.ShouldPool = false;
			info.msg.vendingMachine.sellOrderContainer.sellOrders = new List<ProtoBuf.VendingMachine.SellOrder>();
			foreach (ProtoBuf.VendingMachine.SellOrder sellOrder in this.server_vendingMachine.sellOrders.sellOrders)
			{
				ProtoBuf.VendingMachine.SellOrder sellOrder2 = new ProtoBuf.VendingMachine.SellOrder();
				sellOrder2.ShouldPool = false;
				sellOrder.CopyTo(sellOrder2);
				info.msg.vendingMachine.sellOrderContainer.sellOrders.Add(sellOrder2);
			}
		}
	}

	// Token: 0x040010D8 RID: 4312
	public string markerShopName;

	// Token: 0x040010D9 RID: 4313
	public global::VendingMachine server_vendingMachine;

	// Token: 0x040010DA RID: 4314
	public ProtoBuf.VendingMachine.SellOrderContainer client_sellOrders;
}
