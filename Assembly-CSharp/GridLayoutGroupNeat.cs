﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200072B RID: 1835
public class GridLayoutGroupNeat : GridLayoutGroup
{
	// Token: 0x060022A6 RID: 8870 RVA: 0x000C137C File Offset: 0x000BF57C
	private float IdealCellWidth(float cellSize)
	{
		float num = base.rectTransform.rect.x + (float)(base.padding.left + base.padding.right) * 0.5f;
		float num2 = Mathf.Floor(num / cellSize);
		return num / num2 - this.m_Spacing.x;
	}

	// Token: 0x060022A7 RID: 8871 RVA: 0x000C13D4 File Offset: 0x000BF5D4
	public override void SetLayoutHorizontal()
	{
		Vector2 cellSize = this.m_CellSize;
		this.m_CellSize.x = this.IdealCellWidth(cellSize.x);
		base.SetLayoutHorizontal();
		this.m_CellSize = cellSize;
	}

	// Token: 0x060022A8 RID: 8872 RVA: 0x000C1410 File Offset: 0x000BF610
	public override void SetLayoutVertical()
	{
		Vector2 cellSize = this.m_CellSize;
		this.m_CellSize.x = this.IdealCellWidth(cellSize.x);
		base.SetLayoutVertical();
		this.m_CellSize = cellSize;
	}
}
