﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000693 RID: 1683
public class UIMapGenericRadius : MonoBehaviour
{
	// Token: 0x04001C76 RID: 7286
	public Image radialImage;

	// Token: 0x04001C77 RID: 7287
	public Image outlineImage;

	// Token: 0x04001C78 RID: 7288
	public float radius;

	// Token: 0x04001C79 RID: 7289
	public CanvasGroup fade;

	// Token: 0x04001C7A RID: 7290
	public RectTransform rect;
}
