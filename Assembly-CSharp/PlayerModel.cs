﻿using System;
using UnityEngine;

// Token: 0x020003C3 RID: 963
public class PlayerModel : ListComponent<global::PlayerModel>
{
	// Token: 0x1700018C RID: 396
	// (get) Token: 0x060016AA RID: 5802 RVA: 0x00082DE0 File Offset: 0x00080FE0
	public bool IsFemale
	{
		get
		{
			return this.skinType == 1;
		}
	}

	// Token: 0x1700018D RID: 397
	// (get) Token: 0x060016AB RID: 5803 RVA: 0x00082DEC File Offset: 0x00080FEC
	public global::SkinSetCollection SkinSet
	{
		get
		{
			if (this.IsFemale)
			{
				return this.FemaleSkin;
			}
			return this.MaleSkin;
		}
	}

	// Token: 0x1700018E RID: 398
	// (get) Token: 0x060016AC RID: 5804 RVA: 0x00082E08 File Offset: 0x00081008
	// (set) Token: 0x060016AD RID: 5805 RVA: 0x00082E10 File Offset: 0x00081010
	public Quaternion LookAngles { get; set; }

	// Token: 0x04001130 RID: 4400
	protected static int speed = Animator.StringToHash("speed");

	// Token: 0x04001131 RID: 4401
	protected static int acceleration = Animator.StringToHash("acceleration");

	// Token: 0x04001132 RID: 4402
	protected static int rotationYaw = Animator.StringToHash("rotationYaw");

	// Token: 0x04001133 RID: 4403
	protected static int forward = Animator.StringToHash("forward");

	// Token: 0x04001134 RID: 4404
	protected static int right = Animator.StringToHash("right");

	// Token: 0x04001135 RID: 4405
	protected static int up = Animator.StringToHash("up");

	// Token: 0x04001136 RID: 4406
	protected static int ducked = Animator.StringToHash("ducked");

	// Token: 0x04001137 RID: 4407
	protected static int grounded = Animator.StringToHash("grounded");

	// Token: 0x04001138 RID: 4408
	protected static int waterlevel = Animator.StringToHash("waterlevel");

	// Token: 0x04001139 RID: 4409
	protected static int attack = Animator.StringToHash("attack");

	// Token: 0x0400113A RID: 4410
	protected static int attack_alt = Animator.StringToHash("attack_alt");

	// Token: 0x0400113B RID: 4411
	protected static int deploy = Animator.StringToHash("deploy");

	// Token: 0x0400113C RID: 4412
	protected static int reload = Animator.StringToHash("reload");

	// Token: 0x0400113D RID: 4413
	protected static int throwWeapon = Animator.StringToHash("throw");

	// Token: 0x0400113E RID: 4414
	protected static int holster = Animator.StringToHash("holster");

	// Token: 0x0400113F RID: 4415
	protected static int aiming = Animator.StringToHash("aiming");

	// Token: 0x04001140 RID: 4416
	protected static int onLadder = Animator.StringToHash("onLadder");

	// Token: 0x04001141 RID: 4417
	protected static int posing = Animator.StringToHash("posing");

	// Token: 0x04001142 RID: 4418
	protected static int poseType = Animator.StringToHash("poseType");

	// Token: 0x04001143 RID: 4419
	protected static int relaxGunPose = Animator.StringToHash("relaxGunPose");

	// Token: 0x04001144 RID: 4420
	protected static int leftFootIK = Animator.StringToHash("leftFootIK");

	// Token: 0x04001145 RID: 4421
	protected static int rightFootIK = Animator.StringToHash("rightFootIK");

	// Token: 0x04001146 RID: 4422
	public BoxCollider collision;

	// Token: 0x04001147 RID: 4423
	public GameObject censorshipCube;

	// Token: 0x04001148 RID: 4424
	public GameObject censorshipCubeBreasts;

	// Token: 0x04001149 RID: 4425
	public GameObject jawBone;

	// Token: 0x0400114A RID: 4426
	public GameObject neckBone;

	// Token: 0x0400114B RID: 4427
	public GameObject headBone;

	// Token: 0x0400114C RID: 4428
	public global::SkeletonScale skeletonScale;

	// Token: 0x0400114D RID: 4429
	public global::EyeController eyeController;

	// Token: 0x0400114E RID: 4430
	public Transform[] SpineBones;

	// Token: 0x0400114F RID: 4431
	public Transform leftFootBone;

	// Token: 0x04001150 RID: 4432
	public Transform rightFootBone;

	// Token: 0x04001151 RID: 4433
	public RuntimeAnimatorController DefaultHoldType;

	// Token: 0x04001152 RID: 4434
	public RuntimeAnimatorController SleepGesture;

	// Token: 0x04001153 RID: 4435
	public RuntimeAnimatorController WoundedGesture;

	// Token: 0x04001154 RID: 4436
	public RuntimeAnimatorController CurrentGesture;

	// Token: 0x04001155 RID: 4437
	[Header("Skin")]
	public global::SkinSetCollection MaleSkin;

	// Token: 0x04001156 RID: 4438
	public global::SkinSetCollection FemaleSkin;

	// Token: 0x04001157 RID: 4439
	public global::SubsurfaceProfile subsurfaceProfile;

	// Token: 0x04001158 RID: 4440
	[Range(0f, 1f)]
	[Header("Parameters")]
	public float voiceVolume;

	// Token: 0x04001159 RID: 4441
	[Range(0f, 1f)]
	public float skinColor = 1f;

	// Token: 0x0400115A RID: 4442
	[Range(0f, 1f)]
	public float skinNumber = 1f;

	// Token: 0x0400115B RID: 4443
	[Range(0f, 1f)]
	public float meshNumber;

	// Token: 0x0400115C RID: 4444
	[Range(0f, 1f)]
	public float hairNumber;

	// Token: 0x0400115D RID: 4445
	[Range(0f, 1f)]
	public int skinType;
}
