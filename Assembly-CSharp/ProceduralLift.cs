﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000088 RID: 136
public class ProceduralLift : global::BaseEntity
{
	// Token: 0x06000907 RID: 2311 RVA: 0x0003CA50 File Offset: 0x0003AC50
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("ProceduralLift.OnRpcMessage", 0.1f))
		{
			if (rpc == 137888952u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_UseLift ");
				}
				using (TimeWarning.New("RPC_UseLift", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_UseLift", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_UseLift(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_UseLift");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000908 RID: 2312 RVA: 0x0003CC30 File Offset: 0x0003AE30
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_UseLift(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (Interface.CallHook("OnLiftUse", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		if (base.IsBusy())
		{
			return;
		}
		this.MoveToFloor((this.floorIndex + 1) % this.stops.Length);
	}

	// Token: 0x06000909 RID: 2313 RVA: 0x0003CC98 File Offset: 0x0003AE98
	public override void ServerInit()
	{
		base.ServerInit();
		this.SnapToFloor(0);
	}

	// Token: 0x0600090A RID: 2314 RVA: 0x0003CCA8 File Offset: 0x0003AEA8
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.lift = Facepunch.Pool.Get<ProtoBuf.Lift>();
		info.msg.lift.floor = this.floorIndex;
	}

	// Token: 0x0600090B RID: 2315 RVA: 0x0003CCDC File Offset: 0x0003AEDC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		if (info.msg.lift != null)
		{
			if (this.floorIndex == -1)
			{
				this.SnapToFloor(info.msg.lift.floor);
			}
			else
			{
				this.MoveToFloor(info.msg.lift.floor);
			}
		}
		base.Load(info);
	}

	// Token: 0x0600090C RID: 2316 RVA: 0x0003CD40 File Offset: 0x0003AF40
	private void ResetLift()
	{
		this.MoveToFloor(0);
	}

	// Token: 0x0600090D RID: 2317 RVA: 0x0003CD4C File Offset: 0x0003AF4C
	private void MoveToFloor(int floor)
	{
		this.floorIndex = Mathf.Clamp(floor, 0, this.stops.Length - 1);
		if (base.isServer)
		{
			base.SetFlag(global::BaseEntity.Flags.Busy, true, false);
			base.SendNetworkUpdateImmediate(false);
			base.CancelInvoke(new Action(this.ResetLift));
		}
	}

	// Token: 0x0600090E RID: 2318 RVA: 0x0003CDA4 File Offset: 0x0003AFA4
	private void SnapToFloor(int floor)
	{
		this.floorIndex = Mathf.Clamp(floor, 0, this.stops.Length - 1);
		global::ProceduralLiftStop proceduralLiftStop = this.stops[this.floorIndex];
		this.cabin.transform.position = proceduralLiftStop.transform.position;
		if (base.isServer)
		{
			base.SetFlag(global::BaseEntity.Flags.Busy, false, false);
			base.SendNetworkUpdateImmediate(false);
			base.CancelInvoke(new Action(this.ResetLift));
		}
	}

	// Token: 0x0600090F RID: 2319 RVA: 0x0003CE24 File Offset: 0x0003B024
	private void OnFinishedMoving()
	{
		if (base.isServer)
		{
			base.SetFlag(global::BaseEntity.Flags.Busy, false, false);
			base.SendNetworkUpdateImmediate(false);
			if (this.floorIndex != 0)
			{
				base.Invoke(new Action(this.ResetLift), this.resetDelay);
			}
		}
	}

	// Token: 0x06000910 RID: 2320 RVA: 0x0003CE74 File Offset: 0x0003B074
	protected void Update()
	{
		if (this.floorIndex < 0 || this.floorIndex > this.stops.Length - 1)
		{
			return;
		}
		global::ProceduralLiftStop proceduralLiftStop = this.stops[this.floorIndex];
		if (this.cabin.transform.position == proceduralLiftStop.transform.position)
		{
			return;
		}
		this.cabin.transform.position = Vector3.MoveTowards(this.cabin.transform.position, proceduralLiftStop.transform.position, this.movementSpeed * UnityEngine.Time.deltaTime);
		if (this.cabin.transform.position == proceduralLiftStop.transform.position)
		{
			this.OnFinishedMoving();
		}
	}

	// Token: 0x0400042B RID: 1067
	public float movementSpeed = 1f;

	// Token: 0x0400042C RID: 1068
	public float resetDelay = 5f;

	// Token: 0x0400042D RID: 1069
	public global::ProceduralLiftCabin cabin;

	// Token: 0x0400042E RID: 1070
	public global::ProceduralLiftStop[] stops;

	// Token: 0x0400042F RID: 1071
	private int floorIndex = -1;
}
