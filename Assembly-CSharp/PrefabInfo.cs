﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x020007AE RID: 1966
public class PrefabInfo : MonoBehaviour
{
	// Token: 0x06002495 RID: 9365 RVA: 0x000C97BC File Offset: 0x000C79BC
	public void EnterPool()
	{
		if (base.transform.parent != null)
		{
			base.transform.SetParent(null, false);
		}
		if (ConVar.Pool.mode <= 1)
		{
			if (base.gameObject.activeSelf)
			{
				base.gameObject.SetActive(false);
			}
		}
		else
		{
			this.SetBehaviourEnabled(false);
			this.SetComponentEnabled(false);
			if (!base.gameObject.activeSelf)
			{
				base.gameObject.SetActive(true);
			}
		}
	}

	// Token: 0x06002496 RID: 9366 RVA: 0x000C9844 File Offset: 0x000C7A44
	public void LeavePool()
	{
		if (ConVar.Pool.mode > 1)
		{
			this.SetComponentEnabled(true);
		}
	}

	// Token: 0x06002497 RID: 9367 RVA: 0x000C9860 File Offset: 0x000C7A60
	public void Initialize()
	{
		this.behaviourStates = new bool[this.behaviours.Length];
		this.rigidbodyStates = new bool[this.rigidbodies.Length];
		this.colliderStates = new bool[this.colliders.Length];
		this.lodgroupStates = new bool[this.lodgroups.Length];
		this.rendererStates = new bool[this.renderers.Length];
	}

	// Token: 0x06002498 RID: 9368 RVA: 0x000C98CC File Offset: 0x000C7ACC
	public void SetBehaviourEnabled(bool state)
	{
		if (!state)
		{
			for (int i = 0; i < this.behaviours.Length; i++)
			{
				Behaviour behaviour = this.behaviours[i];
				this.behaviourStates[i] = behaviour.enabled;
				behaviour.enabled = false;
			}
			for (int j = 0; j < this.particles.Length; j++)
			{
				ParticleSystem particleSystem = this.particles[j];
				particleSystem.Stop();
				particleSystem.Clear();
			}
		}
		else
		{
			for (int k = 0; k < this.particles.Length; k++)
			{
				ParticleSystem particleSystem2 = this.particles[k];
				if (particleSystem2.playOnAwake)
				{
					particleSystem2.Play();
				}
			}
			for (int l = 0; l < this.behaviours.Length; l++)
			{
				Behaviour behaviour2 = this.behaviours[l];
				behaviour2.enabled = this.behaviourStates[l];
			}
		}
	}

	// Token: 0x06002499 RID: 9369 RVA: 0x000C99BC File Offset: 0x000C7BBC
	public void SetComponentEnabled(bool state)
	{
		if (!state)
		{
			for (int i = 0; i < this.renderers.Length; i++)
			{
				Renderer renderer = this.renderers[i];
				this.rendererStates[i] = renderer.enabled;
				renderer.enabled = false;
			}
			for (int j = 0; j < this.lodgroups.Length; j++)
			{
				LODGroup lodgroup = this.lodgroups[j];
				this.lodgroupStates[j] = lodgroup.enabled;
				lodgroup.enabled = false;
			}
			for (int k = 0; k < this.colliders.Length; k++)
			{
				Collider collider = this.colliders[k];
				this.colliderStates[k] = collider.enabled;
				collider.enabled = false;
			}
			for (int l = 0; l < this.rigidbodies.Length; l++)
			{
				Rigidbody rigidbody = this.rigidbodies[l];
				this.rigidbodyStates[l] = rigidbody.isKinematic;
				rigidbody.isKinematic = true;
				rigidbody.detectCollisions = false;
			}
		}
		else
		{
			for (int m = 0; m < this.renderers.Length; m++)
			{
				Renderer renderer2 = this.renderers[m];
				renderer2.enabled = this.rendererStates[m];
			}
			for (int n = 0; n < this.lodgroups.Length; n++)
			{
				LODGroup lodgroup2 = this.lodgroups[n];
				lodgroup2.enabled = this.lodgroupStates[n];
			}
			for (int num = 0; num < this.colliders.Length; num++)
			{
				Collider collider2 = this.colliders[num];
				collider2.enabled = this.colliderStates[num];
			}
			for (int num2 = 0; num2 < this.rigidbodies.Length; num2++)
			{
				Rigidbody rigidbody2 = this.rigidbodies[num2];
				rigidbody2.isKinematic = this.rigidbodyStates[num2];
				rigidbody2.detectCollisions = true;
			}
		}
	}

	// Token: 0x0400200C RID: 8204
	internal uint prefabID;

	// Token: 0x0400200D RID: 8205
	internal Behaviour[] behaviours;

	// Token: 0x0400200E RID: 8206
	internal Rigidbody[] rigidbodies;

	// Token: 0x0400200F RID: 8207
	internal Collider[] colliders;

	// Token: 0x04002010 RID: 8208
	internal LODGroup[] lodgroups;

	// Token: 0x04002011 RID: 8209
	internal Renderer[] renderers;

	// Token: 0x04002012 RID: 8210
	internal ParticleSystem[] particles;

	// Token: 0x04002013 RID: 8211
	internal bool[] behaviourStates;

	// Token: 0x04002014 RID: 8212
	internal bool[] rigidbodyStates;

	// Token: 0x04002015 RID: 8213
	internal bool[] colliderStates;

	// Token: 0x04002016 RID: 8214
	internal bool[] lodgroupStates;

	// Token: 0x04002017 RID: 8215
	internal bool[] rendererStates;
}
