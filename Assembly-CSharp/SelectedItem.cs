﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C6 RID: 1734
public class SelectedItem : SingletonComponent<global::SelectedItem>, global::IInventoryChanged
{
	// Token: 0x04001D61 RID: 7521
	public Image icon;

	// Token: 0x04001D62 RID: 7522
	public Image iconSplitter;

	// Token: 0x04001D63 RID: 7523
	public Text title;

	// Token: 0x04001D64 RID: 7524
	public Text description;

	// Token: 0x04001D65 RID: 7525
	public GameObject splitPanel;

	// Token: 0x04001D66 RID: 7526
	public GameObject itemProtection;

	// Token: 0x04001D67 RID: 7527
	public GameObject menuOption;

	// Token: 0x04001D68 RID: 7528
	public GameObject optionsParent;

	// Token: 0x04001D69 RID: 7529
	public GameObject innerPanelContainer;
}
