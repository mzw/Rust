﻿using System;
using UnityEngine;

// Token: 0x02000554 RID: 1364
public class TerrainPathConnect : MonoBehaviour
{
	// Token: 0x06001CA3 RID: 7331 RVA: 0x000A0858 File Offset: 0x0009EA58
	public global::PathFinder.Point GetPoint(int res)
	{
		Vector3 position = base.transform.position;
		float num = global::TerrainMeta.NormalizeX(position.x);
		float num2 = global::TerrainMeta.NormalizeZ(position.z);
		return new global::PathFinder.Point
		{
			x = Mathf.Clamp((int)(num * (float)res), 0, res - 1),
			y = Mathf.Clamp((int)(num2 * (float)res), 0, res - 1)
		};
	}

	// Token: 0x040017C3 RID: 6083
	public global::InfrastructureType Type;
}
