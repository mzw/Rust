﻿using System;
using UnityEngine;

// Token: 0x02000005 RID: 5
public class Achievements : SingletonComponent<global::Achievements>
{
	// Token: 0x0400000C RID: 12
	public AudioClip listcomplete;

	// Token: 0x0400000D RID: 13
	public AudioClip itemcomplete;

	// Token: 0x0400000E RID: 14
	public AudioClip popup;
}
