﻿using System;
using UnityEngine;

// Token: 0x0200060C RID: 1548
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class Imposter : MonoBehaviour
{
	// Token: 0x04001A61 RID: 6753
	public global::ImposterAsset asset;

	// Token: 0x04001A62 RID: 6754
	[Header("Baking")]
	public GameObject reference;

	// Token: 0x04001A63 RID: 6755
	public float angle;

	// Token: 0x04001A64 RID: 6756
	public int resolution = 1024;

	// Token: 0x04001A65 RID: 6757
	public int padding = 32;
}
