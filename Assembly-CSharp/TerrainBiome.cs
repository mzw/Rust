﻿using System;
using System.Collections.Generic;

// Token: 0x02000531 RID: 1329
public static class TerrainBiome
{
	// Token: 0x06001C06 RID: 7174 RVA: 0x0009DFF4 File Offset: 0x0009C1F4
	public static int TypeToIndex(int id)
	{
		return global::TerrainBiome.type2index[id];
	}

	// Token: 0x06001C07 RID: 7175 RVA: 0x0009E004 File Offset: 0x0009C204
	public static int IndexToType(int idx)
	{
		return 1 << idx;
	}

	// Token: 0x040016F1 RID: 5873
	public const int COUNT = 4;

	// Token: 0x040016F2 RID: 5874
	public const int EVERYTHING = -1;

	// Token: 0x040016F3 RID: 5875
	public const int NOTHING = 0;

	// Token: 0x040016F4 RID: 5876
	public const int ARID = 1;

	// Token: 0x040016F5 RID: 5877
	public const int TEMPERATE = 2;

	// Token: 0x040016F6 RID: 5878
	public const int TUNDRA = 4;

	// Token: 0x040016F7 RID: 5879
	public const int ARCTIC = 8;

	// Token: 0x040016F8 RID: 5880
	public const int ARID_IDX = 0;

	// Token: 0x040016F9 RID: 5881
	public const int TEMPERATE_IDX = 1;

	// Token: 0x040016FA RID: 5882
	public const int TUNDRA_IDX = 2;

	// Token: 0x040016FB RID: 5883
	public const int ARCTIC_IDX = 3;

	// Token: 0x040016FC RID: 5884
	private static Dictionary<int, int> type2index = new Dictionary<int, int>
	{
		{
			1,
			0
		},
		{
			2,
			1
		},
		{
			4,
			2
		},
		{
			8,
			3
		}
	};

	// Token: 0x02000532 RID: 1330
	public enum Enum
	{
		// Token: 0x040016FE RID: 5886
		Arid = 1,
		// Token: 0x040016FF RID: 5887
		Temperate,
		// Token: 0x04001700 RID: 5888
		Tundra = 4,
		// Token: 0x04001701 RID: 5889
		Arctic = 8
	}
}
