﻿using System;
using UnityEngine;

// Token: 0x02000263 RID: 611
public class DynamicZNear : MonoBehaviour
{
	// Token: 0x04000B45 RID: 2885
	public float minimum = 0.05f;

	// Token: 0x04000B46 RID: 2886
	public float maximum = 1f;
}
