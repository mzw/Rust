﻿using System;
using UnityEngine;

// Token: 0x02000755 RID: 1877
public class ParticleSystemPlayer : MonoBehaviour, global::IOnParentDestroying
{
	// Token: 0x06002317 RID: 8983 RVA: 0x000C2A08 File Offset: 0x000C0C08
	protected void OnEnable()
	{
		base.GetComponent<ParticleSystem>().enableEmission = true;
	}

	// Token: 0x06002318 RID: 8984 RVA: 0x000C2A18 File Offset: 0x000C0C18
	public void OnParentDestroying()
	{
		base.GetComponent<ParticleSystem>().enableEmission = false;
	}
}
