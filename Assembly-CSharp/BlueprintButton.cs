﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200069F RID: 1695
public class BlueprintButton : MonoBehaviour, global::IInventoryChanged
{
	// Token: 0x04001CA6 RID: 7334
	public Text name;

	// Token: 0x04001CA7 RID: 7335
	public Text subtitle;

	// Token: 0x04001CA8 RID: 7336
	public Image image;

	// Token: 0x04001CA9 RID: 7337
	public Button button;

	// Token: 0x04001CAA RID: 7338
	public CanvasGroup group;

	// Token: 0x04001CAB RID: 7339
	public GameObject newNotification;

	// Token: 0x04001CAC RID: 7340
	public string gotColor = "#ffffff";

	// Token: 0x04001CAD RID: 7341
	public string notGotColor = "#ff0000";

	// Token: 0x04001CAE RID: 7342
	public float craftableFraction;

	// Token: 0x04001CAF RID: 7343
	public GameObject lockedOverlay;

	// Token: 0x04001CB0 RID: 7344
	[Header("Locked")]
	public CanvasGroup LockedGroup;

	// Token: 0x04001CB1 RID: 7345
	public Text LockedPrice;

	// Token: 0x04001CB2 RID: 7346
	public Image LockedImageBackground;

	// Token: 0x04001CB3 RID: 7347
	public Color LockedCannotUnlockColor;

	// Token: 0x04001CB4 RID: 7348
	public Color LockedCanUnlockColor;

	// Token: 0x04001CB5 RID: 7349
	[Header("Unlock Level")]
	public GameObject LockedLevel;
}
