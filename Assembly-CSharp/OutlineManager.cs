﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020004B1 RID: 1201
public class OutlineManager : MonoBehaviour, IClientComponent
{
	// Token: 0x0400148F RID: 5263
	public static Material blurMat;

	// Token: 0x04001490 RID: 5264
	public List<global::OutlineObject> objectsToRender;

	// Token: 0x04001491 RID: 5265
	public float blurAmount = 2f;

	// Token: 0x04001492 RID: 5266
	public Material glowSolidMaterial;

	// Token: 0x04001493 RID: 5267
	public Material blendGlowMaterial;
}
