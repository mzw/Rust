﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Apex.AI;
using Apex.AI.Components;
using Apex.LoadBalancing;
using ConVar;
using Oxide.Core;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x020000E7 RID: 231
public class NPCPlayerApex : global::NPCPlayer, IContextProvider, global::IAIAgent, ILoadBalanced
{
	// Token: 0x17000080 RID: 128
	// (get) Token: 0x06000B6A RID: 2922 RVA: 0x0004B8B4 File Offset: 0x00049AB4
	public override global::BaseNpc.AiStatistics.FamilyEnum Family
	{
		get
		{
			return global::BaseNpc.AiStatistics.FamilyEnum.Scientist;
		}
	}

	// Token: 0x17000081 RID: 129
	// (get) Token: 0x06000B6B RID: 2923 RVA: 0x0004B8B8 File Offset: 0x00049AB8
	// (set) Token: 0x06000B6C RID: 2924 RVA: 0x0004B8C0 File Offset: 0x00049AC0
	public int AgentTypeIndex
	{
		get
		{
			return this.agentTypeIndex;
		}
		set
		{
			this.agentTypeIndex = value;
		}
	}

	// Token: 0x17000082 RID: 130
	// (get) Token: 0x06000B6D RID: 2925 RVA: 0x0004B8CC File Offset: 0x00049ACC
	// (set) Token: 0x06000B6E RID: 2926 RVA: 0x0004B8D4 File Offset: 0x00049AD4
	public bool IsStuck { get; set; }

	// Token: 0x06000B6F RID: 2927 RVA: 0x0004B8E0 File Offset: 0x00049AE0
	public override void ServerInit()
	{
		if (base.isClient)
		{
			return;
		}
		base.ServerInit();
		this.SpawnPosition = base.GetPosition();
		this.IsStuck = false;
		if (this.NewAI)
		{
			this.InitFacts();
			this.fleeHealthThresholdPercentage = this.Stats.HealthThresholdForFleeing;
			this.coverPointComparer = new global::NPCPlayerApex.CoverPointComparer(this);
			this.DelayedReloadOnInit();
			global::NPCSensesLoadBalancer.NpcSensesLoadBalancer.Add(this);
			if (this.AiContext.AiLocationManager == null)
			{
				float num = float.PositiveInfinity;
				Rust.Ai.AiLocationManager aiLocationManager = null;
				if (Rust.Ai.AiLocationManager.Managers != null && Rust.Ai.AiLocationManager.Managers.Count > 0)
				{
					foreach (Rust.Ai.AiLocationManager aiLocationManager2 in Rust.Ai.AiLocationManager.Managers)
					{
						float sqrMagnitude = (aiLocationManager2.transform.position - this.ServerPosition).sqrMagnitude;
						if (sqrMagnitude < num)
						{
							num = sqrMagnitude;
							aiLocationManager = aiLocationManager2;
						}
					}
				}
				if (aiLocationManager != null && num <= this.Stats.VisionRange * this.Stats.VisionRange)
				{
					this.AiContext.AiLocationManager = aiLocationManager;
					if (this.AiContext.AiLocationManager.LocationType == Rust.Ai.AiLocationSpawner.SquadSpawnerLocation.JunkpileA || this.AiContext.AiLocationManager.LocationType == Rust.Ai.AiLocationSpawner.SquadSpawnerLocation.JunkpileG)
					{
						global::NPCPlayerApex.AllJunkpileNPCs.Add(this);
					}
				}
			}
		}
		base.InvokeRandomized(new Action(this.RadioChatter), 10f, 10f, 5f);
	}

	// Token: 0x06000B70 RID: 2928 RVA: 0x0004BA90 File Offset: 0x00049C90
	private void DelayedReloadOnInit()
	{
		Rust.Ai.ReloadOperator.Reload(this.AiContext);
	}

	// Token: 0x06000B71 RID: 2929 RVA: 0x0004BAA0 File Offset: 0x00049CA0
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		if (this.NewAI)
		{
			if (this.AiContext.AiLocationManager.LocationType == Rust.Ai.AiLocationSpawner.SquadSpawnerLocation.JunkpileA || this.AiContext.AiLocationManager.LocationType == Rust.Ai.AiLocationSpawner.SquadSpawnerLocation.JunkpileG)
			{
				global::NPCPlayerApex.AllJunkpileNPCs.Remove(this);
			}
			global::NPCSensesLoadBalancer.NpcSensesLoadBalancer.Remove(this);
		}
		base.CancelInvoke(new Action(this.RadioChatter));
	}

	// Token: 0x1700007F RID: 127
	// (get) Token: 0x06000B72 RID: 2930 RVA: 0x0004BB14 File Offset: 0x00049D14
	bool ILoadBalanced.repeat
	{
		get
		{
			return true;
		}
	}

	// Token: 0x06000B73 RID: 2931 RVA: 0x0004BB18 File Offset: 0x00049D18
	float? ILoadBalanced.ExecuteUpdate(float deltaTime, float nextInterval)
	{
		using (TimeWarning.New("NPC.TickSenses", 0.1f))
		{
			this.TickSenses();
		}
		using (TimeWarning.New("NPC.TickBehaviourState", 0.1f))
		{
			this.TickBehaviourState();
		}
		return new float?(Random.value * 0.1f + 0.1f);
	}

	// Token: 0x06000B74 RID: 2932 RVA: 0x0004BBA8 File Offset: 0x00049DA8
	public void RadioChatter()
	{
		if (base.IsDestroyed || base.transform == null)
		{
			base.CancelInvoke(new Action(this.RadioChatter));
			return;
		}
		if (this.RadioEffect.isValid)
		{
			global::Effect.server.Run(this.RadioEffect.resourcePath, this, global::StringPool.Get("head"), Vector3.zero, Vector3.zero, null, false);
		}
	}

	// Token: 0x06000B75 RID: 2933 RVA: 0x0004BC1C File Offset: 0x00049E1C
	public override void OnKilled(global::HitInfo info)
	{
		base.OnKilled(info);
		if (this.OnDeath != null)
		{
			this.OnDeath();
		}
		if (this.NewAI)
		{
			if (this.AiContext.AiLocationManager.LocationType == Rust.Ai.AiLocationSpawner.SquadSpawnerLocation.JunkpileA || this.AiContext.AiLocationManager.LocationType == Rust.Ai.AiLocationSpawner.SquadSpawnerLocation.JunkpileG)
			{
				global::NPCPlayerApex.AllJunkpileNPCs.Remove(this);
			}
			global::NPCSensesLoadBalancer.NpcSensesLoadBalancer.Remove(this);
		}
		base.CancelInvoke(new Action(this.RadioChatter));
		if (this.DeathEffect.isValid)
		{
			global::Effect.server.Run(this.DeathEffect.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		}
	}

	// Token: 0x06000B76 RID: 2934 RVA: 0x0004BCD4 File Offset: 0x00049ED4
	public override void Hurt(global::HitInfo info)
	{
		if (ConVar.AI.npc_families_no_hurt)
		{
			global::NPCPlayerApex npcplayerApex = info.Initiator as global::NPCPlayerApex;
			if (npcplayerApex != null && npcplayerApex.Family == this.Family)
			{
				return;
			}
		}
		if (info.Initiator != null && this.AiContext != null)
		{
			float danger = info.damageTypes.Total();
			this.AiContext.Memory.Update(info.Initiator, danger);
			this.AiContext.LastAttacker = info.Initiator;
			if (this.AiContext.CoverSet.Closest.ReservedCoverPoint != null && this.GetFact(global::NPCPlayerApex.Facts.IsInCover) > 0)
			{
				this.AiContext.CoverSet.Closest.ReservedCoverPoint.CoverIsCompromised(ConVar.AI.npc_cover_compromised_cooldown);
			}
			this.StartAggro(this.Stats.DeaggroChaseTime, true);
		}
		base.Hurt(info);
	}

	// Token: 0x06000B77 RID: 2935 RVA: 0x0004BDC8 File Offset: 0x00049FC8
	public override void TickAi(float delta)
	{
		this.MovementUpdate(delta);
	}

	// Token: 0x06000B78 RID: 2936 RVA: 0x0004BDD4 File Offset: 0x00049FD4
	public override void MovementUpdate(float delta)
	{
		if (!ConVar.AI.move)
		{
			return;
		}
		if (!this.IsNavRunning())
		{
			return;
		}
		if (Rust.Ai.AiManager.nav_grid)
		{
			base.PreviousCoord = base.CurrentCoord;
			base.CurrentCoord = SingletonComponent<Rust.Ai.AiManager>.Instance.GetCoord(this.ServerPosition);
			if (base.CurrentCoord.x != base.PreviousCoord.x || base.CurrentCoord.y != base.PreviousCoord.y)
			{
				base.AgencyUpdateRequired = true;
				if (this.NavAgent.isOnOffMeshLink)
				{
					base.IsOnOffmeshLinkAndReachedNewCoord = true;
				}
			}
		}
		base.MovementUpdate(delta);
		if ((this.IsNavRunning() && !this.NavAgent.hasPath) || Vector3Ex.Distance2D(this.finalDestination, base.GetPosition()) < 1f)
		{
			this.timeAtDestination += delta;
		}
		else
		{
			this.timeAtDestination = 0f;
		}
		this.modelState.aiming = (this.timeAtDestination > 0.25f && this.AttackTarget != null && this.GetFact(global::NPCPlayerApex.Facts.HasLineOfSight) > 0 && this.GetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover) == 0);
		this.TickStuck(delta);
	}

	// Token: 0x06000B79 RID: 2937 RVA: 0x0004BF30 File Offset: 0x0004A130
	public void TickStuck(float delta)
	{
		if (this.IsNavRunning() && !this.NavAgent.isStopped && (this.lastStuckPos - this.ServerPosition).sqrMagnitude < 0.0625f && this.AttackReady())
		{
			this.stuckDuration += delta;
			if (this.stuckDuration >= 5f && Mathf.Approximately(this.lastStuckTime, 0f))
			{
				this.lastStuckTime = UnityEngine.Time.time;
				this.OnBecomeStuck();
			}
		}
		else
		{
			this.stuckDuration = 0f;
			this.lastStuckPos = this.ServerPosition;
			if (UnityEngine.Time.time - this.lastStuckTime > 5f)
			{
				this.lastStuckTime = 0f;
				this.OnBecomeUnStuck();
			}
		}
	}

	// Token: 0x06000B7A RID: 2938 RVA: 0x0004C010 File Offset: 0x0004A210
	public void OnBecomeStuck()
	{
		this.IsStuck = true;
	}

	// Token: 0x06000B7B RID: 2939 RVA: 0x0004C01C File Offset: 0x0004A21C
	public void OnBecomeUnStuck()
	{
		this.IsStuck = false;
	}

	// Token: 0x06000B7C RID: 2940 RVA: 0x0004C028 File Offset: 0x0004A228
	public void BehaviourChanged()
	{
		this.currentBehaviorDuration = 0f;
	}

	// Token: 0x06000B7D RID: 2941 RVA: 0x0004C038 File Offset: 0x0004A238
	public override void ServerThink(float delta)
	{
		base.ServerThink(delta);
		this.currentBehaviorDuration += delta;
		this.UpdateAttackTargetVisibility(delta);
		base.SetFlag(global::BaseEntity.Flags.Reserved3, this.AttackTarget != null && this.IsAlive(), false);
	}

	// Token: 0x06000B7E RID: 2942 RVA: 0x0004C088 File Offset: 0x0004A288
	public void UpdateAttackTargetVisibility(float delta)
	{
		if (this.AttackTarget == null || (this.lastAttackTarget != null && this.lastAttackTarget != this.AttackTarget) || this.GetFact(global::NPCPlayerApex.Facts.HasLineOfSight) == 0)
		{
			this.attackTargetVisibleFor = 0f;
		}
		else
		{
			this.attackTargetVisibleFor += delta;
		}
		this.lastAttackTarget = this.AttackTarget;
	}

	// Token: 0x06000B7F RID: 2943 RVA: 0x0004C104 File Offset: 0x0004A304
	public void UpdateDestination(Vector3 newDest)
	{
		this.SetDestination(newDest);
	}

	// Token: 0x06000B80 RID: 2944 RVA: 0x0004C110 File Offset: 0x0004A310
	public void UpdateDestination(Transform tx)
	{
		this.SetDestination(tx.position);
	}

	// Token: 0x06000B81 RID: 2945 RVA: 0x0004C120 File Offset: 0x0004A320
	public override void SetDestination(Vector3 newDestination)
	{
		base.SetDestination(newDestination);
		this.IsStopped = false;
		this.Destination = newDestination;
	}

	// Token: 0x06000B82 RID: 2946 RVA: 0x0004C138 File Offset: 0x0004A338
	public float WeaponAttackRange()
	{
		global::AttackEntity attackEntity = base.GetHeldEntity() as global::AttackEntity;
		if (attackEntity == null)
		{
			return 0f;
		}
		return attackEntity.effectiveRange;
	}

	// Token: 0x06000B83 RID: 2947 RVA: 0x0004C16C File Offset: 0x0004A36C
	public void StopMoving()
	{
		this.IsStopped = true;
		this.finalDestination = base.GetPosition();
	}

	// Token: 0x06000B84 RID: 2948 RVA: 0x0004C184 File Offset: 0x0004A384
	public override float DesiredMoveSpeed()
	{
		float running = 0f;
		float ducking = (!this.modelState.ducked) ? 0f : 1f;
		float num;
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Wander)
		{
			num = 0.6f;
		}
		else
		{
			num = 1f;
			float num2 = Vector3.Dot(this.NavAgent.desiredVelocity.normalized, this.eyes.BodyForward());
			if (num2 > 0.75f)
			{
				num2 = Mathf.Clamp01((num2 - 0.75f) / 0.25f);
			}
			else
			{
				num2 = 0f;
			}
			running = num2;
		}
		return base.GetSpeed(running, ducking) * num;
	}

	// Token: 0x06000B85 RID: 2949 RVA: 0x0004C234 File Offset: 0x0004A434
	public override Vector3 GetAimDirection()
	{
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Wander || this.CurrentBehaviour == global::BaseNpc.Behaviour.RetreatingToCover)
		{
			if (this.IsNavRunning() && this.NavAgent.desiredVelocity.sqrMagnitude > 0.01f)
			{
				return this.NavAgent.desiredVelocity.normalized;
			}
			return base.transform.rotation * Vector3.forward;
		}
		else
		{
			if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Attack && this.AttackTarget != null)
			{
				Vector3 vector = Vector3.zero;
				global::BasePlayer basePlayer = this.AttackTarget as global::BasePlayer;
				if (basePlayer != null)
				{
					if (basePlayer.IsDucked())
					{
						vector = global::PlayerEyes.DuckOffset;
					}
					else if (basePlayer.IsSleeping())
					{
						vector..ctor(0f, -1f, 0f);
					}
				}
				else
				{
					global::BaseNpc baseNpc = this.AttackTarget as global::BaseNpc;
					if (baseNpc != null)
					{
						vector..ctor(0f, -0.5f, 0f);
					}
				}
				Vector3 vector2 = base.CenterPoint() + new Vector3(0f, 0f, 0f);
				Vector3 vector3 = this.AttackTarget.CenterPoint();
				if (!this.AttackTarget.IsVisible(this.eyes.position, this.AttackTarget.CenterPoint()))
				{
					Rust.Ai.Memory.SeenInfo info = this.AiContext.Memory.GetInfo(this.AttackTarget);
					if (!(info.Entity != null) || (info.Position - this.ServerPosition).sqrMagnitude <= 4f)
					{
						return base.transform.rotation * Vector3.forward;
					}
					vector3 = info.Position;
				}
				Vector3 vector4 = vector3 + vector;
				return (vector4 - vector2).normalized;
			}
			if (this.IsNavRunning() && this.NavAgent.desiredVelocity.sqrMagnitude > 0.01f)
			{
				return this.NavAgent.desiredVelocity.normalized;
			}
			return base.transform.rotation * Vector3.forward;
		}
	}

	// Token: 0x06000B86 RID: 2950 RVA: 0x0004C48C File Offset: 0x0004A68C
	public override void SetAimDirection(Vector3 newAim)
	{
		if (newAim == Vector3.zero)
		{
			return;
		}
		global::AttackEntity attackEntity = base.GetAttackEntity();
		if (attackEntity && this.AttackTarget && this.GetFact(global::NPCPlayerApex.Facts.HasLineOfSight) > 0 && this.CurrentBehaviour == global::BaseNpc.Behaviour.Attack)
		{
			float swayModifier = 1f;
			newAim = attackEntity.ModifyAIAim(newAim, swayModifier);
		}
		this.eyes.rotation = Quaternion.LookRotation(newAim, Vector3.up);
		this.viewAngles = this.eyes.rotation.eulerAngles;
		this.ServerRotation = this.eyes.rotation;
	}

	// Token: 0x06000B87 RID: 2951 RVA: 0x0004C534 File Offset: 0x0004A734
	public void StartAttack()
	{
		if (!this.IsAlive())
		{
			return;
		}
		base.ShotTest();
		base.MeleeAttack();
	}

	// Token: 0x06000B88 RID: 2952 RVA: 0x0004C550 File Offset: 0x0004A750
	public void StartAttack(Rust.Ai.AttackOperator.AttackType type, global::BaseCombatEntity target)
	{
		if (!this.IsAlive())
		{
			return;
		}
		this.AttackTarget = target;
		if (type != Rust.Ai.AttackOperator.AttackType.CloseRange)
		{
			base.ShotTest();
		}
		else if (!base.MeleeAttack())
		{
			base.ShotTest();
		}
	}

	// Token: 0x06000B89 RID: 2953 RVA: 0x0004C59C File Offset: 0x0004A79C
	public bool AttackReady()
	{
		return true;
	}

	// Token: 0x06000B8A RID: 2954 RVA: 0x0004C5A0 File Offset: 0x0004A7A0
	public override string Categorize()
	{
		return "scientist";
	}

	// Token: 0x17000083 RID: 131
	// (get) Token: 0x06000B8B RID: 2955 RVA: 0x0004C5A8 File Offset: 0x0004A7A8
	public global::NPCHumanContext AiContext
	{
		get
		{
			if (this._aiContext == null)
			{
				this.SetupAiContext();
			}
			return this._aiContext;
		}
	}

	// Token: 0x06000B8C RID: 2956 RVA: 0x0004C5C4 File Offset: 0x0004A7C4
	protected virtual void SetupAiContext()
	{
		this._aiContext = new global::NPCHumanContext(this);
	}

	// Token: 0x06000B8D RID: 2957 RVA: 0x0004C5D4 File Offset: 0x0004A7D4
	public IAIContext GetContext(Guid aiId)
	{
		return this.AiContext;
	}

	// Token: 0x17000084 RID: 132
	// (get) Token: 0x06000B8E RID: 2958 RVA: 0x0004C5DC File Offset: 0x0004A7DC
	public float TimeAtDestination
	{
		get
		{
			return this.timeAtDestination;
		}
	}

	// Token: 0x17000085 RID: 133
	// (get) Token: 0x06000B8F RID: 2959 RVA: 0x0004C5E4 File Offset: 0x0004A7E4
	// (set) Token: 0x06000B90 RID: 2960 RVA: 0x0004C608 File Offset: 0x0004A808
	public Vector3 Destination
	{
		get
		{
			if (this.IsNavRunning())
			{
				return this.GetNavAgent.destination;
			}
			return this.Entity.ServerPosition;
		}
		set
		{
			if (this.IsNavRunning())
			{
				this.GetNavAgent.destination = value;
			}
		}
	}

	// Token: 0x17000086 RID: 134
	// (get) Token: 0x06000B91 RID: 2961 RVA: 0x0004C624 File Offset: 0x0004A824
	// (set) Token: 0x06000B92 RID: 2962 RVA: 0x0004C640 File Offset: 0x0004A840
	public bool IsStopped
	{
		get
		{
			return !this.IsNavRunning() || this.GetNavAgent.isStopped;
		}
		set
		{
			if (this.IsNavRunning())
			{
				this.GetNavAgent.isStopped = value;
			}
		}
	}

	// Token: 0x17000087 RID: 135
	// (get) Token: 0x06000B93 RID: 2963 RVA: 0x0004C65C File Offset: 0x0004A85C
	// (set) Token: 0x06000B94 RID: 2964 RVA: 0x0004C678 File Offset: 0x0004A878
	public bool AutoBraking
	{
		get
		{
			return this.IsNavRunning() && this.GetNavAgent.autoBraking;
		}
		set
		{
			if (this.IsNavRunning())
			{
				this.GetNavAgent.autoBraking = value;
			}
		}
	}

	// Token: 0x17000088 RID: 136
	// (get) Token: 0x06000B95 RID: 2965 RVA: 0x0004C694 File Offset: 0x0004A894
	public bool HasPath
	{
		get
		{
			return this.IsNavRunning() && this.GetNavAgent.hasPath;
		}
	}

	// Token: 0x06000B96 RID: 2966 RVA: 0x0004C6B0 File Offset: 0x0004A8B0
	public override bool IsNavRunning()
	{
		return base.isServer && !Rust.Ai.AiManager.nav_disable && this.GetNavAgent != null && this.GetNavAgent.enabled && this.GetNavAgent.isOnNavMesh;
	}

	// Token: 0x06000B97 RID: 2967 RVA: 0x0004C704 File Offset: 0x0004A904
	public void Pause()
	{
		if (this.GetNavAgent != null && this.GetNavAgent.enabled)
		{
			this.GetNavAgent.enabled = false;
		}
		if (this.utilityAiComponent == null)
		{
			this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
		}
		if (this.utilityAiComponent != null)
		{
			this.utilityAiComponent.Pause();
			this.utilityAiComponent.enabled = false;
		}
		base.CancelInvoke(new Action(this.RadioChatter));
	}

	// Token: 0x06000B98 RID: 2968 RVA: 0x0004C79C File Offset: 0x0004A99C
	public override void Resume()
	{
		if (this.GetNavAgent == null || Rust.Ai.AiManager.nav_disable)
		{
			this.Pause();
			return;
		}
		if (!this.GetNavAgent.isOnNavMesh)
		{
			base.StartCoroutine(this.TryForceToNavmesh());
		}
		else
		{
			this.GetNavAgent.enabled = true;
			if (this.utilityAiComponent == null)
			{
				this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
			}
			if (this.utilityAiComponent != null)
			{
				this.utilityAiComponent.enabled = true;
				this.utilityAiComponent.Resume();
			}
			base.InvokeRandomized(new Action(this.RadioChatter), 10f, 10f, 5f);
		}
	}

	// Token: 0x06000B99 RID: 2969 RVA: 0x0004C864 File Offset: 0x0004AA64
	private IEnumerator TryForceToNavmesh()
	{
		yield return null;
		int numTries = 0;
		float waitForRetryTime = 1f;
		float maxDistanceMultiplier = 2f;
		if (SingletonComponent<Rust.Ai.AiManager>.Instance != null && SingletonComponent<Rust.Ai.AiManager>.Instance.enabled && SingletonComponent<Rust.Ai.AiManager>.Instance.UseNavMesh)
		{
			while (SingletonComponent<Rust.Ai.AiManager>.Instance.IsNavmeshBuilding(this.AgentTypeIndex, this.ServerPosition))
			{
				yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(waitForRetryTime);
				waitForRetryTime += 0.5f;
			}
		}
		else if (!Rust.Ai.AiManager.nav_grid && SingletonComponent<global::DynamicNavMesh>.Instance != null)
		{
			while (SingletonComponent<global::DynamicNavMesh>.Instance.IsBuilding)
			{
				yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(waitForRetryTime);
				waitForRetryTime += 0.5f;
			}
		}
		waitForRetryTime = 1f;
		while (numTries < 3)
		{
			if (this.GetNavAgent.isOnNavMesh)
			{
				this.GetNavAgent.enabled = true;
				if (this.utilityAiComponent == null)
				{
					this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
				}
				if (this.utilityAiComponent != null)
				{
					this.utilityAiComponent.enabled = true;
					this.utilityAiComponent.Resume();
				}
				base.InvokeRandomized(new Action(this.RadioChatter), 10f, 10f, 5f);
				yield break;
			}
			NavMeshHit navMeshHit;
			if (NavMesh.SamplePosition(this.ServerPosition, ref navMeshHit, this.GetNavAgent.height * maxDistanceMultiplier, this.GetNavAgent.areaMask))
			{
				this.ServerPosition = navMeshHit.position;
				this.GetNavAgent.Warp(this.ServerPosition);
				this.GetNavAgent.enabled = true;
				if (this.utilityAiComponent == null)
				{
					this.utilityAiComponent = this.Entity.GetComponent<UtilityAIComponent>();
				}
				if (this.utilityAiComponent != null)
				{
					this.utilityAiComponent.enabled = true;
					this.utilityAiComponent.Resume();
				}
				base.InvokeRandomized(new Action(this.RadioChatter), 10f, 10f, 5f);
				yield break;
			}
			yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(waitForRetryTime);
			maxDistanceMultiplier *= 1.5f;
			numTries++;
		}
		int walkableArea = NavMesh.GetAreaFromName("Walkable");
		if ((this.GetNavAgent.areaMask & 1 << walkableArea) == 0)
		{
			NavMeshBuildSettings animalSettings = NavMesh.GetSettingsByIndex(1);
			this.GetNavAgent.agentTypeID = animalSettings.agentTypeID;
			this.GetNavAgent.areaMask = 1 << walkableArea;
			yield return this.TryForceToNavmesh();
			yield break;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
		yield break;
	}

	// Token: 0x17000089 RID: 137
	// (get) Token: 0x06000B9A RID: 2970 RVA: 0x0004C880 File Offset: 0x0004AA80
	// (set) Token: 0x06000B9B RID: 2971 RVA: 0x0004C888 File Offset: 0x0004AA88
	public Vector3 SpawnPosition { get; set; }

	// Token: 0x1700008A RID: 138
	// (get) Token: 0x06000B9C RID: 2972 RVA: 0x0004C894 File Offset: 0x0004AA94
	public float AttackTargetVisibleFor
	{
		get
		{
			return this.attackTargetVisibleFor;
		}
	}

	// Token: 0x1700008B RID: 139
	// (get) Token: 0x06000B9D RID: 2973 RVA: 0x0004C89C File Offset: 0x0004AA9C
	// (set) Token: 0x06000B9E RID: 2974 RVA: 0x0004C8A4 File Offset: 0x0004AAA4
	public global::BaseEntity AttackTarget { get; set; }

	// Token: 0x1700008C RID: 140
	// (get) Token: 0x06000B9F RID: 2975 RVA: 0x0004C8B0 File Offset: 0x0004AAB0
	// (set) Token: 0x06000BA0 RID: 2976 RVA: 0x0004C8B8 File Offset: 0x0004AAB8
	public Rust.Ai.Memory.SeenInfo AttackTargetMemory { get; set; }

	// Token: 0x1700008D RID: 141
	// (get) Token: 0x06000BA1 RID: 2977 RVA: 0x0004C8C4 File Offset: 0x0004AAC4
	public global::BaseCombatEntity CombatTarget
	{
		get
		{
			return this.AttackTarget as global::BaseCombatEntity;
		}
	}

	// Token: 0x1700008E RID: 142
	// (get) Token: 0x06000BA2 RID: 2978 RVA: 0x0004C8D4 File Offset: 0x0004AAD4
	public Vector3 AttackPosition
	{
		get
		{
			return this.eyes.position;
		}
	}

	// Token: 0x1700008F RID: 143
	// (get) Token: 0x06000BA3 RID: 2979 RVA: 0x0004C8E4 File Offset: 0x0004AAE4
	public Vector3 CrouchedAttackPosition
	{
		get
		{
			if (base.IsDucked())
			{
				return this.AttackPosition;
			}
			return this.AttackPosition - Vector3.down * 1f;
		}
	}

	// Token: 0x06000BA4 RID: 2980 RVA: 0x0004C914 File Offset: 0x0004AB14
	public float FearLevel(global::BaseEntity ent)
	{
		return 0f;
	}

	// Token: 0x17000090 RID: 144
	// (get) Token: 0x06000BA5 RID: 2981 RVA: 0x0004C91C File Offset: 0x0004AB1C
	// (set) Token: 0x06000BA6 RID: 2982 RVA: 0x0004C924 File Offset: 0x0004AB24
	public global::BaseNpc.Behaviour CurrentBehaviour
	{
		get
		{
			return this._currentBehavior;
		}
		set
		{
			this._currentBehavior = value;
			this.BehaviourChanged();
		}
	}

	// Token: 0x17000091 RID: 145
	// (get) Token: 0x06000BA7 RID: 2983 RVA: 0x0004C934 File Offset: 0x0004AB34
	// (set) Token: 0x06000BA8 RID: 2984 RVA: 0x0004C93C File Offset: 0x0004AB3C
	public float currentBehaviorDuration { get; set; }

	// Token: 0x17000092 RID: 146
	// (get) Token: 0x06000BA9 RID: 2985 RVA: 0x0004C948 File Offset: 0x0004AB48
	public global::BaseCombatEntity Entity
	{
		get
		{
			return this;
		}
	}

	// Token: 0x17000093 RID: 147
	// (get) Token: 0x06000BAA RID: 2986 RVA: 0x0004C94C File Offset: 0x0004AB4C
	public NavMeshAgent GetNavAgent
	{
		get
		{
			if (this.NavAgent == null)
			{
				this.NavAgent = base.GetComponent<NavMeshAgent>();
				if (this.NavAgent == null)
				{
					Debug.LogErrorFormat("{0} has no nav agent!", new object[]
					{
						base.name
					});
				}
			}
			return this.NavAgent;
		}
	}

	// Token: 0x06000BAB RID: 2987 RVA: 0x0004C9A8 File Offset: 0x0004ABA8
	public float GetWantsToAttack(global::BaseEntity target)
	{
		if (target == null)
		{
			return 0f;
		}
		object obj = Interface.CallHook("IOnNpcPlayerTarget", new object[]
		{
			this,
			target
		});
		if (obj is float)
		{
			return (float)obj;
		}
		if (!target.HasAnyTrait(global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Human))
		{
			return 0f;
		}
		if (target.GetType() == base.GetType())
		{
			return 0f;
		}
		if (target.Health() <= 0f)
		{
			return 0f;
		}
		return 1f;
	}

	// Token: 0x17000094 RID: 148
	// (get) Token: 0x06000BAC RID: 2988 RVA: 0x0004CA38 File Offset: 0x0004AC38
	public global::BaseNpc.AiStatistics GetStats
	{
		get
		{
			return this.Stats;
		}
	}

	// Token: 0x17000095 RID: 149
	// (get) Token: 0x06000BAD RID: 2989 RVA: 0x0004CA40 File Offset: 0x0004AC40
	public float GetAttackRate
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x17000096 RID: 150
	// (get) Token: 0x06000BAE RID: 2990 RVA: 0x0004CA48 File Offset: 0x0004AC48
	public float GetAttackRange
	{
		get
		{
			return this.WeaponAttackRange();
		}
	}

	// Token: 0x17000097 RID: 151
	// (get) Token: 0x06000BAF RID: 2991 RVA: 0x0004CA50 File Offset: 0x0004AC50
	public Vector3 GetAttackOffset
	{
		get
		{
			return new Vector3(0f, 1.8f, 0f);
		}
	}

	// Token: 0x17000098 RID: 152
	// (get) Token: 0x06000BB0 RID: 2992 RVA: 0x0004CA68 File Offset: 0x0004AC68
	public Vector3 CurrentAimAngles
	{
		get
		{
			return this.eyes.BodyForward();
		}
	}

	// Token: 0x17000099 RID: 153
	// (get) Token: 0x06000BB1 RID: 2993 RVA: 0x0004CA78 File Offset: 0x0004AC78
	public float GetStamina
	{
		get
		{
			return 1f;
		}
	}

	// Token: 0x1700009A RID: 154
	// (get) Token: 0x06000BB2 RID: 2994 RVA: 0x0004CA80 File Offset: 0x0004AC80
	public float GetEnergy
	{
		get
		{
			return 1f;
		}
	}

	// Token: 0x1700009B RID: 155
	// (get) Token: 0x06000BB3 RID: 2995 RVA: 0x0004CA88 File Offset: 0x0004AC88
	public float GetAttackCost
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x1700009C RID: 156
	// (get) Token: 0x06000BB4 RID: 2996 RVA: 0x0004CA90 File Offset: 0x0004AC90
	public float GetSleep
	{
		get
		{
			return 1f;
		}
	}

	// Token: 0x1700009D RID: 157
	// (get) Token: 0x06000BB5 RID: 2997 RVA: 0x0004CA98 File Offset: 0x0004AC98
	public float GetStuckDuration
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x1700009E RID: 158
	// (get) Token: 0x06000BB6 RID: 2998 RVA: 0x0004CAA0 File Offset: 0x0004ACA0
	public float GetLastStuckTime
	{
		get
		{
			return 0f;
		}
	}

	// Token: 0x06000BB7 RID: 2999 RVA: 0x0004CAA8 File Offset: 0x0004ACA8
	public bool BusyTimerActive()
	{
		return this.BusyTimer.IsActive;
	}

	// Token: 0x06000BB8 RID: 3000 RVA: 0x0004CAB8 File Offset: 0x0004ACB8
	public void SetBusyFor(float dur)
	{
		this.BusyTimer.Activate(dur, null);
	}

	// Token: 0x06000BB9 RID: 3001 RVA: 0x0004CAC8 File Offset: 0x0004ACC8
	public bool WantsToEat(global::BaseEntity ent)
	{
		return false;
	}

	// Token: 0x1700009F RID: 159
	// (get) Token: 0x06000BBA RID: 3002 RVA: 0x0004CACC File Offset: 0x0004ACCC
	// (set) Token: 0x06000BBB RID: 3003 RVA: 0x0004CAD4 File Offset: 0x0004ACD4
	public global::BaseEntity FoodTarget { get; set; }

	// Token: 0x06000BBC RID: 3004 RVA: 0x0004CAE0 File Offset: 0x0004ACE0
	public void Eat()
	{
	}

	// Token: 0x06000BBD RID: 3005 RVA: 0x0004CAE4 File Offset: 0x0004ACE4
	public byte GetFact(global::BaseNpc.Facts fact)
	{
		return 0;
	}

	// Token: 0x06000BBE RID: 3006 RVA: 0x0004CAE8 File Offset: 0x0004ACE8
	public void SetFact(global::BaseNpc.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true)
	{
	}

	// Token: 0x06000BBF RID: 3007 RVA: 0x0004CAEC File Offset: 0x0004ACEC
	public float ToSpeed(global::BaseNpc.SpeedEnum speed)
	{
		return 0f;
	}

	// Token: 0x170000A0 RID: 160
	// (get) Token: 0x06000BC0 RID: 3008 RVA: 0x0004CAF4 File Offset: 0x0004ACF4
	// (set) Token: 0x06000BC1 RID: 3009 RVA: 0x0004CAFC File Offset: 0x0004ACFC
	public float TargetSpeed { get; set; }

	// Token: 0x06000BC2 RID: 3010 RVA: 0x0004CB08 File Offset: 0x0004AD08
	public List<Rust.Ai.NavPointSample> RequestNavPointSamplesInCircle(Rust.Ai.NavPointSampler.SampleCount sampleCount, float radius, Rust.Ai.NavPointSampler.SampleFeatures features = Rust.Ai.NavPointSampler.SampleFeatures.None)
	{
		this.navPointSamples.Clear();
		Rust.Ai.NavPointSampler.SampleCircle(sampleCount, this.ServerPosition, radius, new Rust.Ai.NavPointSampler.SampleScoreParams
		{
			WaterMaxDepth = this.Stats.MaxWaterDepth,
			Agent = this,
			Features = features
		}, ref this.navPointSamples);
		return this.navPointSamples;
	}

	// Token: 0x06000BC3 RID: 3011 RVA: 0x0004CB68 File Offset: 0x0004AD68
	private void OnFactChanged(global::NPCPlayerApex.Facts fact, byte oldValue, byte newValue)
	{
		switch (fact)
		{
		case global::NPCPlayerApex.Facts.CanTargetEnemies:
			if (newValue == 1)
			{
				this.blockTargetingThisEnemy = null;
			}
			break;
		default:
			if (fact != global::NPCPlayerApex.Facts.IsAggro)
			{
				if (fact != global::NPCPlayerApex.Facts.IsMoving)
				{
					if (fact != global::NPCPlayerApex.Facts.BodyState)
					{
						if (fact == global::NPCPlayerApex.Facts.IsRetreatingToCover)
						{
							if (newValue == 1)
							{
								this.CurrentBehaviour = global::BaseNpc.Behaviour.RetreatingToCover;
								if (newValue != oldValue)
								{
									base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, true);
								}
							}
							else if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) > 0)
							{
								this.CurrentBehaviour = global::BaseNpc.Behaviour.Attack;
								if (newValue != oldValue)
								{
									base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, false);
								}
							}
							else
							{
								this.CurrentBehaviour = global::BaseNpc.Behaviour.Idle;
								if (newValue != oldValue)
								{
									base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, true);
								}
							}
						}
					}
					else if (newValue == 0)
					{
						this.modelState.ducked = false;
					}
					else if (newValue == 1)
					{
						this.modelState.ducked = true;
					}
				}
				else if (newValue == 1)
				{
					this.TimeLastMoved = UnityEngine.Time.realtimeSinceStartup;
				}
			}
			else if (newValue > 0 && this.GetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover) == 0)
			{
				this.CurrentBehaviour = global::BaseNpc.Behaviour.Attack;
				if (newValue != oldValue)
				{
					base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, false);
				}
			}
			else
			{
				base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, true);
				this.SetFact(global::NPCPlayerApex.Facts.Speed, 0, true, true);
			}
			break;
		case global::NPCPlayerApex.Facts.Speed:
			if (newValue != 0)
			{
				if (newValue != 2)
				{
					this.IsStopped = false;
					if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) > 0)
					{
						if (newValue != oldValue)
						{
							base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, false);
						}
					}
					else if (newValue != oldValue)
					{
						base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, true);
					}
				}
				else
				{
					this.IsStopped = false;
					if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) == 0 && this.GetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover) == 0)
					{
						this.CurrentBehaviour = global::BaseNpc.Behaviour.Wander;
						if (newValue != oldValue)
						{
							base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, true);
						}
					}
				}
			}
			else
			{
				this.StopMoving();
				if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) == 0 && this.GetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover) == 0)
				{
					this.CurrentBehaviour = global::BaseNpc.Behaviour.Idle;
					if (newValue != oldValue)
					{
						base.SetPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed, true);
					}
				}
			}
			break;
		}
	}

	// Token: 0x06000BC4 RID: 3012 RVA: 0x0004CDA4 File Offset: 0x0004AFA4
	private void TickBehaviourState()
	{
		if (this.GetFact(global::NPCPlayerApex.Facts.WantsToFlee) == 1 && this.ToPathStatus(this.GetPathStatus()) == null && UnityEngine.Time.realtimeSinceStartup - (this.maxFleeTime - this.Stats.MaxFleeTime) > 0.5f)
		{
			this.TickFlee();
		}
		if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) == 1)
		{
			this.TickAggro();
		}
		if (this.GetFact(global::NPCPlayerApex.Facts.AllyAttackedRecently) == 1 && UnityEngine.Time.realtimeSinceStartup >= this.AllyAttackedRecentlyTimeout)
		{
			this.SetFact(global::NPCPlayerApex.Facts.AllyAttackedRecently, 0, true, true);
		}
	}

	// Token: 0x06000BC5 RID: 3013 RVA: 0x0004CE38 File Offset: 0x0004B038
	public bool TryAggro(global::NPCPlayerApex.EnemyRangeEnum range)
	{
		if (Mathf.Approximately(this.Stats.Hostility, 0f) && Mathf.Approximately(this.Stats.Defensiveness, 0f))
		{
			this.wasAggro = false;
			return false;
		}
		if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) == 0 && this.IsWithinAggroRange(range))
		{
			float num = (range > global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange) ? this.Stats.Defensiveness : 1f;
			num = Mathf.Max(num, this.Stats.Hostility);
			if (UnityEngine.Time.realtimeSinceStartup > this.lastAggroChanceCalcTime + 5f)
			{
				this.lastAggroChanceResult = Random.value;
				this.lastAggroChanceCalcTime = UnityEngine.Time.realtimeSinceStartup;
			}
			if (this.lastAggroChanceResult < num)
			{
				return this.StartAggro(this.Stats.DeaggroChaseTime, true);
			}
		}
		this.wasAggro = this.IsWithinAggroRange(range);
		return false;
	}

	// Token: 0x06000BC6 RID: 3014 RVA: 0x0004CF24 File Offset: 0x0004B124
	public bool StartAggro(float timeout, bool broadcastEvent = true)
	{
		if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) == 1)
		{
			this.wasAggro = true;
			return false;
		}
		this.SetFact(global::NPCPlayerApex.Facts.IsAggro, 1, true, true);
		this.aggroTimeout = UnityEngine.Time.realtimeSinceStartup + timeout;
		if (!this.wasAggro && broadcastEvent && this.OnAggro != null && this.GetFact(global::NPCPlayerApex.Facts.HasLineOfSight) > 0)
		{
			this.OnAggro();
		}
		this.wasAggro = true;
		return true;
	}

	// Token: 0x06000BC7 RID: 3015 RVA: 0x0004CFA0 File Offset: 0x0004B1A0
	private void TickAggro()
	{
		bool triggerCallback = true;
		bool flag;
		if (float.IsInfinity(base.SecondsSinceDealtDamage) || float.IsNegativeInfinity(base.SecondsSinceDealtDamage) || float.IsNaN(base.SecondsSinceDealtDamage))
		{
			flag = (UnityEngine.Time.realtimeSinceStartup > this.aggroTimeout);
		}
		else
		{
			global::BaseCombatEntity baseCombatEntity = this.AttackTarget as global::BaseCombatEntity;
			if (baseCombatEntity != null && baseCombatEntity.lastAttacker != null && this.net != null && baseCombatEntity.lastAttacker.net != null)
			{
				flag = (baseCombatEntity.lastAttacker.net.ID == this.net.ID && base.SecondsSinceDealtDamage > this.Stats.DeaggroChaseTime);
			}
			else
			{
				flag = (UnityEngine.Time.realtimeSinceStartup > this.aggroTimeout);
			}
		}
		if (!flag)
		{
			if (this.AiContext.EnemyNpc != null && (this.AiContext.EnemyNpc.IsDead() || this.AiContext.EnemyNpc.IsDestroyed))
			{
				flag = true;
				triggerCallback = false;
			}
			else if (this.AiContext.EnemyPlayer != null && (this.AiContext.EnemyPlayer.IsDead() || this.AiContext.EnemyPlayer.IsDestroyed))
			{
				flag = true;
				triggerCallback = false;
			}
		}
		if (flag)
		{
			this.SetFact(global::NPCPlayerApex.Facts.IsAggro, 0, triggerCallback, true);
		}
	}

	// Token: 0x06000BC8 RID: 3016 RVA: 0x0004D128 File Offset: 0x0004B328
	private bool CheckHealthThresholdToFlee()
	{
		if (base.healthFraction > this.Stats.HealthThresholdForFleeing)
		{
			if (this.Stats.HealthThresholdForFleeing < 1f)
			{
				this.SetFact(global::NPCPlayerApex.Facts.IsUnderHealthThreshold, 0, true, true);
				return false;
			}
			if (this.GetFact(global::NPCPlayerApex.Facts.HasEnemy) == 1)
			{
				this.SetFact(global::NPCPlayerApex.Facts.IsUnderHealthThreshold, 0, true, true);
				return false;
			}
		}
		bool flag = Random.value < this.Stats.HealthThresholdFleeChance;
		this.SetFact(global::NPCPlayerApex.Facts.IsUnderHealthThreshold, (!flag) ? 0 : 1, true, true);
		return flag;
	}

	// Token: 0x06000BC9 RID: 3017 RVA: 0x0004D1B4 File Offset: 0x0004B3B4
	private void WantsToFlee()
	{
		if (this.GetFact(global::NPCPlayerApex.Facts.WantsToFlee) == 1 || !this.IsNavRunning())
		{
			return;
		}
		this.SetFact(global::NPCPlayerApex.Facts.WantsToFlee, 1, true, true);
		this.maxFleeTime = UnityEngine.Time.realtimeSinceStartup + this.Stats.MaxFleeTime;
	}

	// Token: 0x06000BCA RID: 3018 RVA: 0x0004D1F4 File Offset: 0x0004B3F4
	private void TickFlee()
	{
		if (UnityEngine.Time.realtimeSinceStartup > this.maxFleeTime || (this.IsNavRunning() && this.NavAgent.remainingDistance <= this.NavAgent.stoppingDistance + 1f))
		{
			this.SetFact(global::NPCPlayerApex.Facts.WantsToFlee, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.IsFleeing, 0, true, true);
			this.Stats.HealthThresholdForFleeing = base.healthFraction * this.fleeHealthThresholdPercentage;
		}
	}

	// Token: 0x170000A1 RID: 161
	// (get) Token: 0x06000BCB RID: 3019 RVA: 0x0004D270 File Offset: 0x0004B470
	public float SecondsSinceLastInRangeOfSpawnPosition
	{
		get
		{
			return UnityEngine.Time.time - this.lastInRangeOfSpawnPositionTime;
		}
	}

	// Token: 0x06000BCC RID: 3020 RVA: 0x0004D280 File Offset: 0x0004B480
	private void FindCoverFromEnemy()
	{
		this.AiContext.CoverSet.Reset();
		if (this.AttackTarget != null)
		{
			this.FindCoverFromPosition(this.AiContext.EnemyPosition);
		}
	}

	// Token: 0x06000BCD RID: 3021 RVA: 0x0004D2B4 File Offset: 0x0004B4B4
	private void FindCoverFromPosition(Vector3 position)
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		Rust.Ai.CoverPoint retreat = null;
		Rust.Ai.CoverPoint flank = null;
		Rust.Ai.CoverPoint advance = null;
		this.AiContext.CoverSet.Reset();
		foreach (Rust.Ai.CoverPoint coverPoint in this.AiContext.sampledCoverPoints)
		{
			if (!coverPoint.IsReserved && !coverPoint.IsCompromised)
			{
				if (global::NPCPlayerApex.ProvidesCoverFromPoint(coverPoint, position, -0.8f))
				{
					Vector3 vector = coverPoint.Position - this.ServerPosition;
					Vector3 vector2 = position - this.ServerPosition;
					float num4 = Vector3.Dot(vector.normalized, vector2.normalized);
					if (num4 <= 0.5f || vector.sqrMagnitude <= vector2.sqrMagnitude)
					{
						if (num4 <= -0.5f)
						{
							float sqrMagnitude = vector.sqrMagnitude;
							if (sqrMagnitude < this.MinDistanceToRetreatCover * this.MinDistanceToRetreatCover)
							{
								num4 = 0.1f;
							}
							else
							{
								float num5 = num4 * -1f;
								if (num5 > num)
								{
									num = num5;
									retreat = coverPoint;
								}
							}
						}
						if (num4 >= 0.5f)
						{
							float sqrMagnitude2 = vector.sqrMagnitude;
							if (sqrMagnitude2 > vector2.sqrMagnitude)
							{
								continue;
							}
							float num6 = num4;
							if (num6 > num3)
							{
								if (ConVar.AI.npc_cover_use_path_distance && this.IsNavRunning() && this.AttackTarget != null && !this.CoverToTargetPathDistanceIsValid(coverPoint))
								{
									continue;
								}
								float sqrMagnitude3 = (coverPoint.Position - position).sqrMagnitude;
								if (sqrMagnitude3 < sqrMagnitude2)
								{
									num6 *= 0.9f;
								}
								num3 = num6;
								advance = coverPoint;
							}
						}
						if (num4 >= -0.1f && num4 <= 0.1f)
						{
							float num7 = 1f - Mathf.Abs(num4);
							if (num7 > num2)
							{
								if (!ConVar.AI.npc_cover_use_path_distance || !this.IsNavRunning() || !(this.AttackTarget != null) || this.CoverToTargetPathDistanceIsValid(coverPoint))
								{
									num2 = 0.1f - Mathf.Abs(num7);
									flank = coverPoint;
								}
							}
						}
					}
				}
			}
		}
		this.AiContext.CoverSet.Update(retreat, flank, advance);
	}

	// Token: 0x06000BCE RID: 3022 RVA: 0x0004D54C File Offset: 0x0004B74C
	private bool CoverToTargetPathDistanceIsValid(Rust.Ai.CoverPoint coverPoint)
	{
		if (global::NPCPlayerApex._pathCache == null)
		{
			global::NPCPlayerApex._pathCache = new NavMeshPath();
		}
		bool flag = NavMesh.CalculatePath(this.AttackTarget.ServerPosition, coverPoint.Position, this.GetNavAgent.areaMask, global::NPCPlayerApex._pathCache);
		if (flag)
		{
			int cornersNonAlloc = global::NPCPlayerApex._pathCache.GetCornersNonAlloc(global::NPCPlayerApex.pathCornerCache);
			if (global::NPCPlayerApex._pathCache.status == null && cornersNonAlloc > 1)
			{
				float num = this.PathDistance(cornersNonAlloc, ref global::NPCPlayerApex.pathCornerCache);
				float num2 = Vector3.Distance(this.AttackTarget.ServerPosition, coverPoint.Position);
				float num3 = num2 - num;
				bool flag2 = Mathf.Abs(num3) > ConVar.AI.npc_cover_path_vs_straight_dist_max_diff;
				if (flag2)
				{
					return false;
				}
			}
		}
		return true;
	}

	// Token: 0x06000BCF RID: 3023 RVA: 0x0004D604 File Offset: 0x0004B804
	private float PathDistance(int count, ref Vector3[] path)
	{
		if (count < 2)
		{
			return 0f;
		}
		Vector3 vector = path[0];
		float num = 0f;
		for (int i = 0; i < count; i++)
		{
			Vector3 vector2 = path[i];
			num += Vector3.Distance(vector, vector2);
			vector = vector2;
		}
		return num;
	}

	// Token: 0x06000BD0 RID: 3024 RVA: 0x0004D660 File Offset: 0x0004B860
	private void FindClosestCoverToUs()
	{
		float num = float.MaxValue;
		Rust.Ai.CoverPoint coverPoint = null;
		this.AiContext.CoverSet.Reset();
		foreach (Rust.Ai.CoverPoint coverPoint2 in this.AiContext.sampledCoverPoints)
		{
			if (!coverPoint2.IsReserved && !coverPoint2.IsCompromised)
			{
				float sqrMagnitude = (coverPoint2.Position - this.ServerPosition).sqrMagnitude;
				if (sqrMagnitude < num)
				{
					num = sqrMagnitude;
					coverPoint = coverPoint2;
				}
			}
		}
		if (coverPoint != null)
		{
			this.AiContext.CoverSet.Closest.ReservedCoverPoint = coverPoint;
		}
	}

	// Token: 0x06000BD1 RID: 3025 RVA: 0x0004D734 File Offset: 0x0004B934
	public static bool ProvidesCoverFromPoint(Rust.Ai.CoverPoint cp, Vector3 point, float arcThreshold)
	{
		Vector3 normalized = (cp.Position - point).normalized;
		float num = Vector3.Dot(cp.Normal, normalized);
		return num < arcThreshold;
	}

	// Token: 0x170000A2 RID: 162
	// (get) Token: 0x06000BD2 RID: 3026 RVA: 0x0004D768 File Offset: 0x0004B968
	// (set) Token: 0x06000BD3 RID: 3027 RVA: 0x0004D770 File Offset: 0x0004B970
	public global::NPCPlayerApex.ActionCallback OnFleeExplosive { get; set; }

	// Token: 0x170000A3 RID: 163
	// (get) Token: 0x06000BD4 RID: 3028 RVA: 0x0004D77C File Offset: 0x0004B97C
	// (set) Token: 0x06000BD5 RID: 3029 RVA: 0x0004D784 File Offset: 0x0004B984
	public global::NPCPlayerApex.ActionCallback OnTakeCover { get; set; }

	// Token: 0x170000A4 RID: 164
	// (get) Token: 0x06000BD6 RID: 3030 RVA: 0x0004D790 File Offset: 0x0004B990
	// (set) Token: 0x06000BD7 RID: 3031 RVA: 0x0004D798 File Offset: 0x0004B998
	public global::NPCPlayerApex.ActionCallback OnAggro { get; set; }

	// Token: 0x170000A5 RID: 165
	// (get) Token: 0x06000BD8 RID: 3032 RVA: 0x0004D7A4 File Offset: 0x0004B9A4
	// (set) Token: 0x06000BD9 RID: 3033 RVA: 0x0004D7AC File Offset: 0x0004B9AC
	public global::NPCPlayerApex.ActionCallback OnChatter { get; set; }

	// Token: 0x170000A6 RID: 166
	// (get) Token: 0x06000BDA RID: 3034 RVA: 0x0004D7B8 File Offset: 0x0004B9B8
	// (set) Token: 0x06000BDB RID: 3035 RVA: 0x0004D7C0 File Offset: 0x0004B9C0
	public global::NPCPlayerApex.ActionCallback OnDeath { get; set; }

	// Token: 0x170000A7 RID: 167
	// (get) Token: 0x06000BDC RID: 3036 RVA: 0x0004D7CC File Offset: 0x0004B9CC
	// (set) Token: 0x06000BDD RID: 3037 RVA: 0x0004D7D4 File Offset: 0x0004B9D4
	public global::NPCPlayerApex.ActionCallback OnReload { get; set; }

	// Token: 0x06000BDE RID: 3038 RVA: 0x0004D7E0 File Offset: 0x0004B9E0
	public bool IsInCommunicationRange(global::NPCPlayerApex npc)
	{
		if (npc != null && !npc.IsDestroyed && npc.transform != null && npc.Health() > 0f)
		{
			float sqrMagnitude = (npc.ServerPosition - this.ServerPosition).sqrMagnitude;
			return sqrMagnitude <= this.CommunicationRadius * this.CommunicationRadius;
		}
		return false;
	}

	// Token: 0x06000BDF RID: 3039 RVA: 0x0004D854 File Offset: 0x0004BA54
	public virtual int GetAlliesInRange(out List<global::Scientist> allies)
	{
		allies = null;
		return 0;
	}

	// Token: 0x06000BE0 RID: 3040 RVA: 0x0004D85C File Offset: 0x0004BA5C
	public virtual void SendStatement(Rust.Ai.AiStatement_EnemyEngaged statement)
	{
	}

	// Token: 0x06000BE1 RID: 3041 RVA: 0x0004D860 File Offset: 0x0004BA60
	public virtual void SendStatement(Rust.Ai.AiStatement_EnemySeen statement)
	{
	}

	// Token: 0x06000BE2 RID: 3042 RVA: 0x0004D864 File Offset: 0x0004BA64
	public virtual void OnAiStatement(global::NPCPlayerApex source, Rust.Ai.AiStatement_EnemyEngaged statement)
	{
	}

	// Token: 0x06000BE3 RID: 3043 RVA: 0x0004D868 File Offset: 0x0004BA68
	public virtual void OnAiStatement(global::NPCPlayerApex source, Rust.Ai.AiStatement_EnemySeen statement)
	{
	}

	// Token: 0x06000BE4 RID: 3044 RVA: 0x0004D86C File Offset: 0x0004BA6C
	public virtual int AskQuestion(Rust.Ai.AiQuestion_ShareEnemyTarget question, out List<Rust.Ai.AiAnswer_ShareEnemyTarget> answers)
	{
		answers = null;
		return 0;
	}

	// Token: 0x06000BE5 RID: 3045 RVA: 0x0004D874 File Offset: 0x0004BA74
	public Rust.Ai.AiAnswer_ShareEnemyTarget OnAiQuestion(global::NPCPlayerApex source, Rust.Ai.AiQuestion_ShareEnemyTarget question)
	{
		return new Rust.Ai.AiAnswer_ShareEnemyTarget
		{
			Source = this,
			PlayerTarget = this.AiContext.EnemyPlayer
		};
	}

	// Token: 0x06000BE6 RID: 3046 RVA: 0x0004D8A4 File Offset: 0x0004BAA4
	public void InitFacts()
	{
		this.SetFact(global::NPCPlayerApex.Facts.CanTargetEnemies, 1, true, true);
		this.sortedSqrRange.Clear();
		this.sortedSqrRange.Add(new KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>(global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange, this.Stats.CloseRange * this.Stats.CloseRange));
		this.sortedSqrRange.Add(new KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>(global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange, this.Stats.MediumRange * this.Stats.MediumRange));
		this.sortedSqrRange.Add(new KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>(global::NPCPlayerApex.EnemyRangeEnum.LongAttackRange, this.Stats.LongRange * this.Stats.LongRange));
		this.sortedSqrRange.Add(new KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>(global::NPCPlayerApex.EnemyRangeEnum.AggroRange, this.Stats.AggressionRange * this.Stats.AggressionRange));
		this.sortedSqrRange.Add(new KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>(global::NPCPlayerApex.EnemyRangeEnum.AwareRange, this.Stats.VisionRange * this.Stats.VisionRange));
		this.sortedSqrRange.Sort(delegate(KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float> kvpA, KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float> kvpB)
		{
			if (kvpA.Value > kvpB.Value)
			{
				return 1;
			}
			if (kvpA.Value < kvpB.Value)
			{
				return -1;
			}
			return 0;
		});
	}

	// Token: 0x06000BE7 RID: 3047 RVA: 0x0004D9B8 File Offset: 0x0004BBB8
	public byte GetFact(global::NPCPlayerApex.Facts fact)
	{
		return this.CurrentFacts[(int)fact];
	}

	// Token: 0x06000BE8 RID: 3048 RVA: 0x0004D9C4 File Offset: 0x0004BBC4
	public void SetFact(global::NPCPlayerApex.Facts fact, byte value, bool triggerCallback = true, bool onlyTriggerCallbackOnDiffValue = true)
	{
		byte b = this.CurrentFacts[(int)fact];
		this.CurrentFacts[(int)fact] = value;
		if (triggerCallback && (!onlyTriggerCallbackOnDiffValue || value != b))
		{
			this.OnFactChanged(fact, b, value);
		}
	}

	// Token: 0x06000BE9 RID: 3049 RVA: 0x0004DA00 File Offset: 0x0004BC00
	public global::NPCPlayerApex.EnemyRangeEnum ToEnemyRangeEnum(float sqrRange)
	{
		foreach (KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float> kvp in this.sortedSqrRange)
		{
			if (sqrRange <= kvp.Value)
			{
				if (this.GetFact(global::NPCPlayerApex.Facts.IsAggro) > 0 && this.GetFact(global::NPCPlayerApex.Facts.AttackedRecently) == 0 && this.GetFact(global::NPCPlayerApex.Facts.AllyAttackedRecently) == 0 && this.IsBeyondDeaggroRange(kvp))
				{
					return global::NPCPlayerApex.EnemyRangeEnum.OutOfRange;
				}
				return kvp.Key;
			}
		}
		return global::NPCPlayerApex.EnemyRangeEnum.OutOfRange;
	}

	// Token: 0x06000BEA RID: 3050 RVA: 0x0004DAB0 File Offset: 0x0004BCB0
	public float ToSqrRange(global::NPCPlayerApex.EnemyRangeEnum range)
	{
		foreach (KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float> keyValuePair in this.sortedSqrRange)
		{
			if (keyValuePair.Key == range)
			{
				return keyValuePair.Value;
			}
		}
		return float.PositiveInfinity;
	}

	// Token: 0x06000BEB RID: 3051 RVA: 0x0004DB28 File Offset: 0x0004BD28
	public bool IsWithinAggroRange(global::NPCPlayerApex.EnemyRangeEnum range)
	{
		switch (range)
		{
		case global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange:
			return this.Stats.CloseRange < this.Stats.AggressionRange;
		case global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange:
			return this.Stats.MediumRange < this.Stats.AggressionRange;
		case global::NPCPlayerApex.EnemyRangeEnum.LongAttackRange:
			return this.Stats.LongRange < this.Stats.AggressionRange;
		case global::NPCPlayerApex.EnemyRangeEnum.AggroRange:
			return true;
		case global::NPCPlayerApex.EnemyRangeEnum.AwareRange:
			return this.Stats.VisionRange < this.Stats.AggressionRange;
		default:
			return false;
		}
	}

	// Token: 0x06000BEC RID: 3052 RVA: 0x0004DBBC File Offset: 0x0004BDBC
	public bool IsBeyondDeaggroRange(global::NPCPlayerApex.EnemyRangeEnum range)
	{
		foreach (KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float> kvp in this.sortedSqrRange)
		{
			if (kvp.Key == range)
			{
				return this.IsBeyondDeaggroRange(kvp);
			}
		}
		return false;
	}

	// Token: 0x06000BED RID: 3053 RVA: 0x0004DC30 File Offset: 0x0004BE30
	public bool IsBeyondDeaggroRange(KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float> kvp)
	{
		return kvp.Value >= this.Stats.DeaggroRange * this.Stats.DeaggroRange;
	}

	// Token: 0x06000BEE RID: 3054 RVA: 0x0004DC58 File Offset: 0x0004BE58
	public global::NPCPlayerApex.AfraidRangeEnum ToAfraidRangeEnum(float sqrRange)
	{
		if (sqrRange <= this.Stats.AfraidRange * this.Stats.AfraidRange)
		{
			return global::NPCPlayerApex.AfraidRangeEnum.InAfraidRange;
		}
		return global::NPCPlayerApex.AfraidRangeEnum.OutOfRange;
	}

	// Token: 0x06000BEF RID: 3055 RVA: 0x0004DC7C File Offset: 0x0004BE7C
	public global::NPCPlayerApex.HealthEnum ToHealthEnum(float healthNormalized)
	{
		if (healthNormalized >= 0.75f)
		{
			return global::NPCPlayerApex.HealthEnum.Fine;
		}
		if (healthNormalized >= 0.25f)
		{
			return global::NPCPlayerApex.HealthEnum.Medium;
		}
		return global::NPCPlayerApex.HealthEnum.Low;
	}

	// Token: 0x06000BF0 RID: 3056 RVA: 0x0004DC9C File Offset: 0x0004BE9C
	public global::NPCPlayerApex.SpeedEnum ToSpeedEnum(float speed)
	{
		if (speed <= 0.01f)
		{
			return global::NPCPlayerApex.SpeedEnum.StandStill;
		}
		if (speed <= ConVar.AI.npc_speed_crouch_walk)
		{
			return global::NPCPlayerApex.SpeedEnum.CrouchWalk;
		}
		if (speed <= ConVar.AI.npc_speed_walk)
		{
			return global::NPCPlayerApex.SpeedEnum.Walk;
		}
		if (speed <= ConVar.AI.npc_speed_crouch_run)
		{
			return global::NPCPlayerApex.SpeedEnum.CrouchRun;
		}
		if (speed <= ConVar.AI.npc_speed_run)
		{
			return global::NPCPlayerApex.SpeedEnum.Run;
		}
		return global::NPCPlayerApex.SpeedEnum.Sprint;
	}

	// Token: 0x06000BF1 RID: 3057 RVA: 0x0004DCEC File Offset: 0x0004BEEC
	public float ToSpeed(global::NPCPlayerApex.SpeedEnum speed)
	{
		switch (speed)
		{
		case global::NPCPlayerApex.SpeedEnum.StandStill:
			return 0f;
		case global::NPCPlayerApex.SpeedEnum.CrouchWalk:
			return ConVar.AI.npc_speed_crouch_walk * this.Stats.Speed;
		case global::NPCPlayerApex.SpeedEnum.Walk:
			return ConVar.AI.npc_speed_walk * this.Stats.Speed;
		case global::NPCPlayerApex.SpeedEnum.Run:
			return ConVar.AI.npc_speed_run * this.Stats.Speed;
		case global::NPCPlayerApex.SpeedEnum.CrouchRun:
			return ConVar.AI.npc_speed_crouch_run * this.Stats.Speed;
		default:
			return ConVar.AI.npc_speed_sprint * this.Stats.Speed;
		}
	}

	// Token: 0x06000BF2 RID: 3058 RVA: 0x0004DD78 File Offset: 0x0004BF78
	public global::NPCPlayerApex.AmmoStateEnum GetCurrentAmmoStateEnum()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
		if (attackEntity == null)
		{
			return global::NPCPlayerApex.AmmoStateEnum.Empty;
		}
		global::BaseProjectile baseProjectile = attackEntity as global::BaseProjectile;
		if (!baseProjectile)
		{
			return global::NPCPlayerApex.AmmoStateEnum.Full;
		}
		if (baseProjectile.primaryMagazine.contents == 0)
		{
			return global::NPCPlayerApex.AmmoStateEnum.Empty;
		}
		float num = (float)baseProjectile.primaryMagazine.contents / (float)baseProjectile.primaryMagazine.capacity;
		if (num < 0.3f)
		{
			return global::NPCPlayerApex.AmmoStateEnum.Low;
		}
		if (num < 0.65f)
		{
			return global::NPCPlayerApex.AmmoStateEnum.Medium;
		}
		if (num < 1f)
		{
			return global::NPCPlayerApex.AmmoStateEnum.High;
		}
		return global::NPCPlayerApex.AmmoStateEnum.Full;
	}

	// Token: 0x06000BF3 RID: 3059 RVA: 0x0004DE0C File Offset: 0x0004C00C
	public global::NPCPlayerApex.WeaponTypeEnum GetCurrentWeaponTypeEnum()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		if (heldEntity == null)
		{
			return global::NPCPlayerApex.WeaponTypeEnum.None;
		}
		global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
		if (attackEntity == null)
		{
			return global::NPCPlayerApex.WeaponTypeEnum.None;
		}
		return attackEntity.effectiveRangeType;
	}

	// Token: 0x06000BF4 RID: 3060 RVA: 0x0004DE4C File Offset: 0x0004C04C
	public global::NPCPlayerApex.WeaponTypeEnum GetWeaponTypeEnum(global::BaseProjectile proj)
	{
		if (proj)
		{
			return proj.effectiveRangeType;
		}
		return global::NPCPlayerApex.WeaponTypeEnum.None;
	}

	// Token: 0x06000BF5 RID: 3061 RVA: 0x0004DE64 File Offset: 0x0004C064
	public global::NPCPlayerApex.EnemyRangeEnum WeaponToEnemyRange(global::NPCPlayerApex.WeaponTypeEnum weapon)
	{
		switch (weapon)
		{
		case global::NPCPlayerApex.WeaponTypeEnum.None:
		case global::NPCPlayerApex.WeaponTypeEnum.CloseRange:
			return global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange;
		case global::NPCPlayerApex.WeaponTypeEnum.MediumRange:
			return global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange;
		case global::NPCPlayerApex.WeaponTypeEnum.LongRange:
			return global::NPCPlayerApex.EnemyRangeEnum.LongAttackRange;
		default:
			return global::NPCPlayerApex.EnemyRangeEnum.OutOfRange;
		}
	}

	// Token: 0x06000BF6 RID: 3062 RVA: 0x0004DE88 File Offset: 0x0004C088
	public global::NPCPlayerApex.EnemyRangeEnum CurrentWeaponToEnemyRange()
	{
		global::NPCPlayerApex.WeaponTypeEnum currentWeaponTypeEnum = this.GetCurrentWeaponTypeEnum();
		return this.WeaponToEnemyRange(currentWeaponTypeEnum);
	}

	// Token: 0x06000BF7 RID: 3063 RVA: 0x0004DEA4 File Offset: 0x0004C0A4
	public byte GetPathStatus()
	{
		if (!this.IsNavRunning())
		{
			return 2;
		}
		return this.NavAgent.pathStatus;
	}

	// Token: 0x06000BF8 RID: 3064 RVA: 0x0004DEC0 File Offset: 0x0004C0C0
	public NavMeshPathStatus ToPathStatus(byte value)
	{
		return value;
	}

	// Token: 0x06000BF9 RID: 3065 RVA: 0x0004DEC4 File Offset: 0x0004C0C4
	public global::NPCPlayerApex.ToolTypeEnum GetCurrentToolTypeEnum()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		if (heldEntity == null)
		{
			return global::NPCPlayerApex.ToolTypeEnum.None;
		}
		return heldEntity.toolType;
	}

	// Token: 0x170000A8 RID: 168
	// (get) Token: 0x06000BFA RID: 3066 RVA: 0x0004DEEC File Offset: 0x0004C0EC
	public float SecondsSinceSeenPlayer
	{
		get
		{
			return UnityEngine.Time.time - this.lastSeenPlayerTime;
		}
	}

	// Token: 0x06000BFB RID: 3067 RVA: 0x0004DEFC File Offset: 0x0004C0FC
	private void TickSenses()
	{
		if (global::BaseEntity.Query.Server == null || this.AiContext == null || this.IsDormant)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup > this.lastTickTime + this.SensesTickRate)
		{
			this.TickVision();
			this.TickHearing();
			this.TickSmell();
			this.AiContext.Memory.Forget((float)this.ForgetUnseenEntityTime);
			this.lastTickTime = UnityEngine.Time.realtimeSinceStartup;
		}
		this.TickEnemyAwareness();
		this.UpdateSelfFacts();
	}

	// Token: 0x06000BFC RID: 3068 RVA: 0x0004DF84 File Offset: 0x0004C184
	private void TickVision()
	{
		this.AiContext.Players.Clear();
		this.AiContext.Npcs.Clear();
		this.AiContext.DeployedExplosives.Clear();
		if (global::BaseEntity.Query.Server == null)
		{
			return;
		}
		global::BaseEntity.Query.EntityTree server = global::BaseEntity.Query.Server;
		Vector3 serverPosition = this.ServerPosition;
		float visionRange = this.Stats.VisionRange;
		global::BaseEntity[] sensesResults = this.SensesResults;
		if (global::NPCPlayerApex.<>f__mg$cache0 == null)
		{
			global::NPCPlayerApex.<>f__mg$cache0 = new Func<global::BaseEntity, bool>(global::NPCPlayerApex.AiCaresAbout);
		}
		int inSphere = server.GetInSphere(serverPosition, visionRange, sensesResults, global::NPCPlayerApex.<>f__mg$cache0);
		if (inSphere == 0)
		{
			return;
		}
		for (int i = 0; i < inSphere; i++)
		{
			global::BaseEntity baseEntity = this.SensesResults[i];
			if (!(baseEntity == null))
			{
				if (!(baseEntity == this))
				{
					if (baseEntity.isServer)
					{
						global::BasePlayer basePlayer = baseEntity as global::BasePlayer;
						if (basePlayer != null)
						{
							if (!ConVar.AI.ignoreplayers)
							{
								if (!basePlayer.IsSleeping())
								{
									this.AiContext.Players.Add(baseEntity as global::BasePlayer);
								}
							}
						}
						else if (baseEntity is global::BaseNpc)
						{
							this.AiContext.Npcs.Add(baseEntity as global::BaseNpc);
						}
						else if (baseEntity is global::TimedExplosive)
						{
							global::TimedExplosive timedExplosive = baseEntity as global::TimedExplosive;
							if ((this.ServerPosition - timedExplosive.ServerPosition).sqrMagnitude < (timedExplosive.explosionRadius + 2f) * (timedExplosive.explosionRadius + 2f))
							{
								this.AiContext.DeployedExplosives.Add(timedExplosive);
							}
						}
					}
				}
			}
		}
		this.sensesTicksSinceLastCoverSweep++;
		if (this.sensesTicksSinceLastCoverSweep > 5)
		{
			this.FindCoverPoints();
			this.sensesTicksSinceLastCoverSweep = 0;
		}
	}

	// Token: 0x06000BFD RID: 3069 RVA: 0x0004E15C File Offset: 0x0004C35C
	protected bool IsVisibleStanding(global::BasePlayer player)
	{
		Vector3 vector = this.eyes.transform.position + this.eyes.transform.up * global::PlayerEyes.EyeOffset.y;
		return (player.IsVisible(vector, player.CenterPoint()) || player.IsVisible(vector, player.transform.position + global::NPCPlayerApex.feetOffset) || player.IsVisible(vector, player.eyes.position)) && (base.IsVisible(player.CenterPoint(), vector) || base.IsVisible(player.transform.position + global::NPCPlayerApex.feetOffset, vector) || base.IsVisible(player.eyes.position, vector));
	}

	// Token: 0x06000BFE RID: 3070 RVA: 0x0004E248 File Offset: 0x0004C448
	protected bool IsVisibleCrouched(global::BasePlayer player)
	{
		Vector3 vector = this.eyes.transform.position + this.eyes.transform.up * (global::PlayerEyes.EyeOffset.y + global::PlayerEyes.DuckOffset.y);
		return (player.IsVisible(vector, player.CenterPoint()) || player.IsVisible(vector, player.transform.position + global::NPCPlayerApex.feetOffset) || player.IsVisible(vector, player.eyes.position)) && (base.IsVisible(player.CenterPoint(), vector) || base.IsVisible(player.transform.position + global::NPCPlayerApex.feetOffset, vector) || base.IsVisible(player.eyes.position, vector));
	}

	// Token: 0x06000BFF RID: 3071 RVA: 0x0004E348 File Offset: 0x0004C548
	private bool IsVisibleStanding(global::BaseNpc npc)
	{
		Vector3 vector = this.eyes.transform.position + this.eyes.transform.up * global::PlayerEyes.EyeOffset.y;
		return npc.IsVisible(vector, npc.CenterPoint()) && base.IsVisible(npc.CenterPoint(), vector);
	}

	// Token: 0x06000C00 RID: 3072 RVA: 0x0004E3BC File Offset: 0x0004C5BC
	private bool IsVisibleCrouched(global::BaseNpc npc)
	{
		Vector3 vector = this.eyes.transform.position + this.eyes.transform.up * (global::PlayerEyes.EyeOffset.y + global::PlayerEyes.DuckOffset.y);
		return npc.IsVisible(vector, npc.CenterPoint()) && base.IsVisible(npc.CenterPoint(), vector);
	}

	// Token: 0x06000C01 RID: 3073 RVA: 0x0004E440 File Offset: 0x0004C640
	private void FindCoverPoints()
	{
		if (SingletonComponent<Rust.Ai.AiManager>.Instance == null || !SingletonComponent<Rust.Ai.AiManager>.Instance.enabled || !SingletonComponent<Rust.Ai.AiManager>.Instance.UseCover)
		{
			return;
		}
		if (this.AiContext.sampledCoverPoints.Count > 0)
		{
			this.AiContext.sampledCoverPoints.Clear();
		}
		if (this.AiContext.CurrentCoverVolume == null || !this.AiContext.CurrentCoverVolume.Contains(this.AiContext.Position))
		{
			this.AiContext.CurrentCoverVolume = SingletonComponent<Rust.Ai.AiManager>.Instance.GetCoverVolumeContaining(this.AiContext.Position);
			if (this.AiContext.CurrentCoverVolume == null)
			{
			}
		}
		if (this.AiContext.CurrentCoverVolume != null)
		{
			foreach (Rust.Ai.CoverPoint coverPoint in this.AiContext.CurrentCoverVolume.CoverPoints)
			{
				if (!coverPoint.IsReserved)
				{
					Vector3 position = coverPoint.Position;
					float sqrMagnitude = (this.AiContext.Position - position).sqrMagnitude;
					if (sqrMagnitude <= this.MaxDistanceToCover * this.MaxDistanceToCover)
					{
						this.AiContext.sampledCoverPoints.Add(coverPoint);
					}
				}
			}
			if (this.AiContext.sampledCoverPoints.Count > 0)
			{
				this.AiContext.sampledCoverPoints.Sort(this.coverPointComparer);
			}
		}
	}

	// Token: 0x06000C02 RID: 3074 RVA: 0x0004E600 File Offset: 0x0004C800
	private void TickHearing()
	{
		this.SetFact(global::NPCPlayerApex.Facts.LoudNoiseNearby, 0, true, true);
	}

	// Token: 0x06000C03 RID: 3075 RVA: 0x0004E610 File Offset: 0x0004C810
	private void TickSmell()
	{
	}

	// Token: 0x06000C04 RID: 3076 RVA: 0x0004E614 File Offset: 0x0004C814
	private void TickEnemyAwareness()
	{
		if (this.GetFact(global::NPCPlayerApex.Facts.CanTargetEnemies) == 0 && this.blockTargetingThisEnemy == null)
		{
			this.AiContext.EnemyNpc = null;
			this.AiContext.EnemyPlayer = null;
			this.SetFact(global::NPCPlayerApex.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.EnemyRange, 5, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.IsAggro, 0, false, true);
			return;
		}
		this.SelectEnemy();
	}

	// Token: 0x06000C05 RID: 3077 RVA: 0x0004E67C File Offset: 0x0004C87C
	private void SelectEnemy()
	{
		if (this.AiContext.Players.Count == 0 && this.AiContext.Npcs.Count == 0)
		{
			this.AiContext.EnemyNpc = null;
			this.AiContext.EnemyPlayer = null;
			this.SetFact(global::NPCPlayerApex.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.EnemyRange, 5, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.IsAggro, 0, false, true);
			return;
		}
		this.AggroBestScorePlayerOrClosestAnimal();
	}

	// Token: 0x06000C06 RID: 3078 RVA: 0x0004E6F4 File Offset: 0x0004C8F4
	private void AggroBestScorePlayerOrClosestAnimal()
	{
		float num = float.MaxValue;
		float num2 = 0f;
		bool flag = false;
		bool flag2 = false;
		global::BasePlayer basePlayer = null;
		global::BaseNpc baseNpc = null;
		this.AiContext.AIAgent.AttackTarget = null;
		Vector3 vector = Vector3.zero;
		float sqrRange = float.MaxValue;
		foreach (global::BasePlayer basePlayer2 in this.AiContext.Players)
		{
			if (!basePlayer2.IsDead() && !basePlayer2.IsDestroyed && (!(this.blockTargetingThisEnemy != null) || basePlayer2.net == null || this.blockTargetingThisEnemy.net == null || basePlayer2.net.ID != this.blockTargetingThisEnemy.net.ID))
			{
				global::NPCPlayerApex npcplayerApex = basePlayer2 as global::NPCPlayerApex;
				if (!(npcplayerApex != null) || this.Stats.Family != npcplayerApex.Stats.Family)
				{
					float num3 = 0f;
					Vector3 dir = basePlayer2.ServerPosition - this.ServerPosition;
					float sqrMagnitude = dir.sqrMagnitude;
					if (sqrMagnitude < num)
					{
						num = sqrMagnitude;
					}
					if (sqrMagnitude < this.Stats.VisionRange * this.Stats.VisionRange)
					{
						num3 += this.VisionRangeScore;
					}
					if (sqrMagnitude < this.Stats.AggressionRange * this.Stats.AggressionRange)
					{
						num3 += this.AggroRangeScore;
					}
					global::NPCPlayerApex.EnemyRangeEnum enemyRangeEnum = this.ToEnemyRangeEnum(sqrMagnitude);
					if (enemyRangeEnum == global::NPCPlayerApex.EnemyRangeEnum.LongAttackRange)
					{
						num3 += this.LongRangeScore;
					}
					else if (enemyRangeEnum == global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange)
					{
						num3 += this.MediumRangeScore;
					}
					else if (enemyRangeEnum == global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange)
					{
						num3 += this.CloseRangeScore;
					}
					bool flag3 = this.IsVisibleStanding(basePlayer2);
					bool flag4 = false;
					if (!flag3)
					{
						flag4 = this.IsVisibleCrouched(basePlayer2);
					}
					if (!flag3 && !flag4)
					{
						if (this.AiContext.Memory.GetInfo(basePlayer2).Entity == null || enemyRangeEnum > global::NPCPlayerApex.EnemyRangeEnum.AggroRange)
						{
							continue;
						}
						num3 *= 0.75f;
					}
					else
					{
						this.AiContext.Memory.Update(basePlayer2, 0f);
					}
					float dist = Mathf.Sqrt(sqrMagnitude);
					num3 *= this.VisibilityScoreModifier(basePlayer2, dir, dist, flag3, flag4);
					if (num3 > num2)
					{
						basePlayer = basePlayer2;
						baseNpc = null;
						sqrRange = sqrMagnitude;
						num2 = num3;
						flag = flag3;
						flag2 = flag4;
					}
				}
			}
		}
		List<Rust.Ai.AiAnswer_ShareEnemyTarget> list;
		if (basePlayer == null && this.AskQuestion(default(Rust.Ai.AiQuestion_ShareEnemyTarget), out list) > 0)
		{
			foreach (Rust.Ai.AiAnswer_ShareEnemyTarget aiAnswer_ShareEnemyTarget in list)
			{
				if (aiAnswer_ShareEnemyTarget.PlayerTarget != null)
				{
					basePlayer = aiAnswer_ShareEnemyTarget.PlayerTarget;
					baseNpc = null;
					vector = basePlayer.ServerPosition - this.ServerPosition;
					sqrRange = vector.sqrMagnitude;
					num2 = 100f;
					num = vector.sqrMagnitude;
					flag = this.IsVisibleStanding(basePlayer);
					flag2 = false;
					if (!flag)
					{
						flag2 = this.IsVisibleCrouched(basePlayer);
					}
					if (flag || flag2)
					{
						this.AiContext.Memory.Update(basePlayer, 0f);
					}
					break;
				}
			}
		}
		if (num > 0.1f && num2 < 10f)
		{
			bool flag5 = basePlayer != null && num <= this.Stats.AggressionRange;
			foreach (global::BaseNpc baseNpc2 in this.AiContext.Npcs)
			{
				if (!baseNpc2.IsDead() && !baseNpc2.IsDestroyed && this.Stats.Family != baseNpc2.Stats.Family)
				{
					float sqrMagnitude2 = (baseNpc2.ServerPosition - this.ServerPosition).sqrMagnitude;
					if (sqrMagnitude2 < num)
					{
						global::NPCPlayerApex.EnemyRangeEnum enemyRangeEnum2 = this.ToEnemyRangeEnum(sqrMagnitude2);
						if (!flag5 || enemyRangeEnum2 <= global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange)
						{
							if (enemyRangeEnum2 <= global::NPCPlayerApex.EnemyRangeEnum.MediumAttackRange)
							{
								num = sqrMagnitude2;
								baseNpc = baseNpc2;
								basePlayer = null;
								sqrRange = sqrMagnitude2;
								flag2 = false;
								flag = this.IsVisibleStanding(baseNpc2);
								if (!flag)
								{
									flag2 = this.IsVisibleCrouched(baseNpc2);
								}
								if (flag || flag2)
								{
									this.AiContext.Memory.Update(baseNpc2, 0f);
								}
								if (num < 0.1f)
								{
									break;
								}
							}
						}
					}
				}
			}
		}
		this.AiContext.EnemyPlayer = basePlayer;
		this.AiContext.EnemyNpc = baseNpc;
		this.AiContext.LastTargetScore = num2;
		if (basePlayer != null || baseNpc != null)
		{
			this.SetFact(global::NPCPlayerApex.Facts.HasEnemy, 1, true, true);
			if (basePlayer != null)
			{
				this.AiContext.AIAgent.AttackTarget = basePlayer;
			}
			else
			{
				this.AiContext.AIAgent.AttackTarget = baseNpc;
			}
			if (Interface.CallHook("IOnNpcPlayerTarget", new object[]
			{
				this,
				this.AiContext.AIAgent.AttackTarget
			}) != null)
			{
				return;
			}
			global::NPCPlayerApex.EnemyRangeEnum enemyRangeEnum3 = this.ToEnemyRangeEnum(sqrRange);
			global::NPCPlayerApex.AfraidRangeEnum value = this.ToAfraidRangeEnum(sqrRange);
			this.SetFact(global::NPCPlayerApex.Facts.EnemyRange, (byte)enemyRangeEnum3, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AfraidRange, (byte)value, true, true);
			bool flag6 = flag || flag2;
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSight, (!flag6) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightCrouched, (!flag2) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightStanding, (!flag) ? 0 : 1, true, true);
			if (basePlayer != null && flag6)
			{
				this.lastSeenPlayerTime = UnityEngine.Time.time;
			}
			this.TryAggro(enemyRangeEnum3);
		}
		else
		{
			this.SetFact(global::NPCPlayerApex.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.EnemyRange, 5, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AfraidRange, 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSight, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightCrouched, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightStanding, 0, true, true);
		}
	}

	// Token: 0x06000C07 RID: 3079 RVA: 0x0004EDD8 File Offset: 0x0004CFD8
	protected void SetAttackTarget(global::BasePlayer player, float score, float sqrDistance, bool lineOfSightStanding, bool lineOfSightCrouched)
	{
		if (player != null)
		{
			this.AiContext.EnemyPlayer = player;
			this.AiContext.EnemyNpc = null;
			this.AiContext.LastTargetScore = score;
			this.SetFact(global::NPCPlayerApex.Facts.HasEnemy, 1, true, true);
			this.AiContext.AIAgent.AttackTarget = player;
			global::NPCPlayerApex.EnemyRangeEnum enemyRangeEnum = this.ToEnemyRangeEnum(sqrDistance);
			global::NPCPlayerApex.AfraidRangeEnum value = this.ToAfraidRangeEnum(sqrDistance);
			this.SetFact(global::NPCPlayerApex.Facts.EnemyRange, (byte)enemyRangeEnum, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AfraidRange, (byte)value, true, true);
			bool flag = lineOfSightStanding || lineOfSightCrouched;
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSight, (!flag) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightCrouched, (!lineOfSightCrouched) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightStanding, (!lineOfSightStanding) ? 0 : 1, true, true);
			if (flag)
			{
				this.lastSeenPlayerTime = UnityEngine.Time.time;
			}
			this.TryAggro(enemyRangeEnum);
		}
		else
		{
			this.SetFact(global::NPCPlayerApex.Facts.HasEnemy, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.EnemyRange, 5, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AfraidRange, 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSight, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightCrouched, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.HasLineOfSightStanding, 0, true, true);
		}
	}

	// Token: 0x06000C08 RID: 3080 RVA: 0x0004EF0C File Offset: 0x0004D10C
	private float VisibilityScoreModifier(global::BasePlayer target, Vector3 dir, float dist, bool losStand, bool losCrouch)
	{
		float num = (!target.IsDucked()) ? 1f : 0.5f;
		num *= ((!target.IsRunning()) ? 1f : 1.5f);
		num *= ((target.estimatedSpeed > 0.01f) ? 1f : 0.5f);
		float num2 = 1f;
		bool flag = false;
		global::Item activeItem = target.GetActiveItem();
		if (activeItem != null)
		{
			global::HeldEntity heldEntity = activeItem.GetHeldEntity() as global::HeldEntity;
			if (heldEntity != null)
			{
				flag = heldEntity.LightsOn();
			}
		}
		if (!flag)
		{
			num2 = this.Stats.DistanceVisibility.Evaluate(Mathf.Clamp01(dist / this.Stats.VisionRange));
			if (!losStand && losCrouch)
			{
				num2 *= 0.75f;
			}
			else if (losStand && !losCrouch)
			{
				num2 *= 0.9f;
			}
			if (num < 1f)
			{
				float num3 = Vector3.Dot(dir.normalized, this.eyes.HeadForward().normalized);
				if (num3 > Mathf.Abs(this.Stats.VisionCone))
				{
					num2 *= 1.5f;
				}
				else if (num3 > 0f)
				{
					num2 *= Mathf.Clamp01(num3 + num);
				}
				else
				{
					num2 *= 0.25f * num;
				}
			}
			else
			{
				num2 *= num;
			}
		}
		num2 = Mathf.Clamp01(num2);
		float result;
		if (this.alertness > 0.5f)
		{
			result = ((Random.value >= num2) ? 0f : num2);
		}
		else if (this.alertness > 0.01f)
		{
			result = ((Random.value >= num2 * this.alertness) ? 0f : num2);
		}
		else
		{
			result = ((num2 <= ConVar.AI.npc_alertness_zero_detection_mod) ? 0f : num2);
		}
		return result;
	}

	// Token: 0x06000C09 RID: 3081 RVA: 0x0004F110 File Offset: 0x0004D310
	private void UpdateSelfFacts()
	{
		if ((!float.IsNegativeInfinity(base.SecondsSinceAttacked) && base.SecondsSinceAttacked < this.Stats.AttackedMemoryTime) || (!float.IsNegativeInfinity(this.SecondsSinceSeenPlayer) && this.SecondsSinceSeenPlayer < this.Stats.AttackedMemoryTime))
		{
			this.alertness = 1f;
		}
		else if (this.alertness > 0f)
		{
			this.alertness = Mathf.Clamp01(this.alertness - ConVar.AI.npc_alertness_drain_rate);
		}
		this.SetFact(global::NPCPlayerApex.Facts.Health, (byte)this.ToHealthEnum(base.healthFraction), true, true);
		this.SetFact(global::NPCPlayerApex.Facts.IsWeaponAttackReady, (UnityEngine.Time.realtimeSinceStartup < this.NextAttackTime()) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.IsRoamReady, (UnityEngine.Time.realtimeSinceStartup < this.AiContext.NextRoamTime || !this.IsNavRunning()) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.Speed, (byte)this.ToSpeedEnum(this.TargetSpeed / this.Stats.Speed), true, true);
		this.SetFact(global::NPCPlayerApex.Facts.AttackedLately, (base.SecondsSinceAttacked >= this.Stats.AttackedMemoryTime) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.AttackedVeryRecently, (base.SecondsSinceAttacked >= 2f) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.AttackedRecently, (base.SecondsSinceAttacked >= 7f) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.IsMoving, this.IsMoving(), true, false);
		this.SetFact(global::NPCPlayerApex.Facts.CanSwitchWeapon, (UnityEngine.Time.realtimeSinceStartup <= this.NextWeaponSwitchTime) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.CanSwitchTool, (UnityEngine.Time.realtimeSinceStartup <= this.NextToolSwitchTime) ? 0 : 1, true, true);
		this.SetFact(global::NPCPlayerApex.Facts.CurrentAmmoState, (byte)this.GetCurrentAmmoStateEnum(), true, true);
		this.SetFact(global::NPCPlayerApex.Facts.CurrentWeaponType, (byte)this.GetCurrentWeaponTypeEnum(), true, true);
		this.SetFact(global::NPCPlayerApex.Facts.CurrentToolType, (byte)this.GetCurrentToolTypeEnum(), true, true);
		this.SetFact(global::NPCPlayerApex.Facts.ExplosiveInRange, (this.AiContext.DeployedExplosives.Count <= 0) ? 0 : 1, true, true);
		global::NPCPlayerApex.EnemyRangeEnum rangeToSpawnPoint = this.GetRangeToSpawnPoint();
		this.SetFact(global::NPCPlayerApex.Facts.RangeToSpawnLocation, (byte)rangeToSpawnPoint, true, true);
		if (rangeToSpawnPoint < global::NPCPlayerApex.EnemyRangeEnum.OutOfRange)
		{
			this.lastInRangeOfSpawnPositionTime = UnityEngine.Time.time;
		}
		if (this.CheckHealthThresholdToFlee())
		{
			this.WantsToFlee();
		}
		if (this.GetFact(global::NPCPlayerApex.Facts.HasEnemy) == 1)
		{
			this.FindCoverFromEnemy();
			this.SetFact(global::NPCPlayerApex.Facts.RetreatCoverInRange, (this.AiContext.CoverSet.Retreat.ReservedCoverPoint == null) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.FlankCoverInRange, (this.AiContext.CoverSet.Flank.ReservedCoverPoint == null) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AdvanceCoverInRange, (this.AiContext.CoverSet.Advance.ReservedCoverPoint == null) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.CoverInRange, (this.AiContext.CoverSet.Closest.ReservedCoverPoint == null) ? 0 : 1, true, true);
			if (this.GetFact(global::NPCPlayerApex.Facts.IsMovingToCover) == 1)
			{
				this.SetFact(global::NPCPlayerApex.Facts.IsMovingToCover, this.IsMoving(), true, true);
			}
			Vector3 normalized = (this.AttackTarget.ServerPosition - this.ServerPosition).normalized;
			float num = Vector3.Dot(this.eyes.BodyForward(), normalized);
			this.SetFact(global::NPCPlayerApex.Facts.AimsAtTarget, (num <= ConVar.AI.npc_valid_aim_cone) ? 0 : 1, true, true);
		}
		else
		{
			this.FindClosestCoverToUs();
			this.SetFact(global::NPCPlayerApex.Facts.RetreatCoverInRange, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.FlankCoverInRange, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AdvanceCoverInRange, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.CoverInRange, (this.AiContext.CoverSet.Closest.ReservedCoverPoint == null) ? 0 : 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.IsMovingToCover, 0, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.AimsAtTarget, 0, true, true);
		}
		if (this.AiContext.CoverSet.Closest.ReservedCoverPoint != null)
		{
			float sqrMagnitude = (this.AiContext.CoverSet.Closest.ReservedCoverPoint.Position - this.ServerPosition).sqrMagnitude;
			byte b = (sqrMagnitude >= 0.5625f) ? 0 : 1;
			this.SetFact(global::NPCPlayerApex.Facts.IsInCover, b, true, true);
			if (b == 1)
			{
				this.SetFact(global::NPCPlayerApex.Facts.IsCoverCompromised, (!this.AiContext.CoverSet.Closest.ReservedCoverPoint.IsCompromised) ? 0 : 1, true, true);
			}
		}
		if (this.GetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover) == 1)
		{
			this.SetFact(global::NPCPlayerApex.Facts.IsRetreatingToCover, this.IsMoving(), true, true);
		}
	}

	// Token: 0x06000C0A RID: 3082 RVA: 0x0004F5D8 File Offset: 0x0004D7D8
	private global::NPCPlayerApex.EnemyRangeEnum GetRangeToSpawnPoint()
	{
		float num = this.ToSqrRange(this.Stats.MaxRangeToSpawnLoc) * 2f;
		float sqrMagnitude = (this.ServerPosition - this.SpawnPosition).sqrMagnitude;
		if (sqrMagnitude > num)
		{
			return global::NPCPlayerApex.EnemyRangeEnum.CloseAttackRange;
		}
		return this.ToEnemyRangeEnum(sqrMagnitude);
	}

	// Token: 0x06000C0B RID: 3083 RVA: 0x0004F628 File Offset: 0x0004D828
	private byte IsMoving()
	{
		return (!this.IsNavRunning() || !this.NavAgent.hasPath || this.NavAgent.remainingDistance <= this.NavAgent.stoppingDistance || this.IsStuck || this.GetFact(global::NPCPlayerApex.Facts.Speed) == 0) ? 0 : 1;
	}

	// Token: 0x06000C0C RID: 3084 RVA: 0x0004F68C File Offset: 0x0004D88C
	private float NextAttackTime()
	{
		global::HeldEntity heldEntity = base.GetHeldEntity();
		global::AttackEntity attackEntity = heldEntity as global::AttackEntity;
		if (attackEntity == null)
		{
			return float.PositiveInfinity;
		}
		return attackEntity.NextAttackTime;
	}

	// Token: 0x06000C0D RID: 3085 RVA: 0x0004F6C0 File Offset: 0x0004D8C0
	public void SetTargetPathStatus(float pendingDelay = 0.05f)
	{
		if (this.isAlreadyCheckingPathPending)
		{
			return;
		}
		if (this.NavAgent.pathPending && this.numPathPendingAttempts < 10)
		{
			this.isAlreadyCheckingPathPending = true;
			base.Invoke(new Action(this.DelayedTargetPathStatus), pendingDelay);
		}
		else
		{
			this.numPathPendingAttempts = 0;
			this.accumPathPendingDelay = 0f;
			this.SetFact(global::NPCPlayerApex.Facts.PathToTargetStatus, this.GetPathStatus(), true, true);
		}
	}

	// Token: 0x06000C0E RID: 3086 RVA: 0x0004F738 File Offset: 0x0004D938
	private void DelayedTargetPathStatus()
	{
		this.accumPathPendingDelay += 0.1f;
		this.isAlreadyCheckingPathPending = false;
		this.SetTargetPathStatus(this.accumPathPendingDelay);
	}

	// Token: 0x06000C0F RID: 3087 RVA: 0x0004F760 File Offset: 0x0004D960
	private static bool AiCaresAbout(global::BaseEntity ent)
	{
		return ent is global::BasePlayer || ent is global::BaseNpc || ent is global::WorldItem || ent is global::BaseCorpse || ent is global::TimedExplosive;
	}

	// Token: 0x06000C10 RID: 3088 RVA: 0x0004F7B0 File Offset: 0x0004D9B0
	private static bool WithinVisionCone(global::NPCPlayerApex npc, global::BaseEntity other)
	{
		if (Mathf.Approximately(npc.Stats.VisionCone, -1f))
		{
			return true;
		}
		Vector3 normalized = (other.ServerPosition - npc.ServerPosition).normalized;
		float num = Vector3.Dot(npc.ServerRotation * Vector3.forward, normalized);
		return num >= npc.Stats.VisionCone;
	}

	// Token: 0x06000C11 RID: 3089 RVA: 0x0004F820 File Offset: 0x0004DA20
	public override void OnSensation(Rust.Ai.Sensation sensation)
	{
		if (this.AiContext == null)
		{
			return;
		}
		Rust.Ai.SensationType type = sensation.Type;
		if (type == Rust.Ai.SensationType.Gunshot || type == Rust.Ai.SensationType.ThrownWeapon)
		{
			this.OnSenseGunshot(sensation);
		}
	}

	// Token: 0x06000C12 RID: 3090 RVA: 0x0004F860 File Offset: 0x0004DA60
	protected virtual void OnSenseGunshot(Rust.Ai.Sensation sensation)
	{
		this.AiContext.Memory.AddDanger(sensation.Position, 1f);
		this._lastHeardGunshotTime = UnityEngine.Time.time;
		this.LastHeardGunshotDirection = (sensation.Position - base.transform.localPosition).normalized;
		if (this.CurrentBehaviour == global::BaseNpc.Behaviour.Idle || this.CurrentBehaviour == global::BaseNpc.Behaviour.Wander)
		{
			this.FindCoverFromPosition(sensation.Position);
			this.SetFact(global::NPCPlayerApex.Facts.SeekingCover, 1, true, true);
			this.SetFact(global::NPCPlayerApex.Facts.CoverInRange, (this.AiContext.CoverSet.Closest.ReservedCoverPoint == null) ? 0 : 1, true, true);
		}
	}

	// Token: 0x170000A9 RID: 169
	// (get) Token: 0x06000C13 RID: 3091 RVA: 0x0004F914 File Offset: 0x0004DB14
	public float SecondsSinceLastHeardGunshot
	{
		get
		{
			return UnityEngine.Time.time - this._lastHeardGunshotTime;
		}
	}

	// Token: 0x170000AA RID: 170
	// (get) Token: 0x06000C14 RID: 3092 RVA: 0x0004F924 File Offset: 0x0004DB24
	// (set) Token: 0x06000C15 RID: 3093 RVA: 0x0004F92C File Offset: 0x0004DB2C
	public Vector3 LastHeardGunshotDirection { get; set; }

	// Token: 0x0400060A RID: 1546
	public global::GameObjectRef RadioEffect;

	// Token: 0x0400060B RID: 1547
	public global::GameObjectRef DeathEffect;

	// Token: 0x0400060C RID: 1548
	public int agentTypeIndex;

	// Token: 0x0400060E RID: 1550
	private Vector3 lastStuckPos;

	// Token: 0x0400060F RID: 1551
	public float stuckDuration;

	// Token: 0x04000610 RID: 1552
	public float lastStuckTime;

	// Token: 0x04000611 RID: 1553
	private float timeAtDestination;

	// Token: 0x04000612 RID: 1554
	public const float TickRate = 0.1f;

	// Token: 0x04000613 RID: 1555
	public static readonly HashSet<global::NPCPlayerApex> AllJunkpileNPCs = new HashSet<global::NPCPlayerApex>();

	// Token: 0x04000614 RID: 1556
	private float attackTargetVisibleFor;

	// Token: 0x04000615 RID: 1557
	private global::BaseEntity lastAttackTarget;

	// Token: 0x04000616 RID: 1558
	public global::BaseNpc.AiStatistics Stats;

	// Token: 0x04000617 RID: 1559
	[SerializeField]
	private UtilityAIComponent utilityAiComponent;

	// Token: 0x04000618 RID: 1560
	public bool NewAI;

	// Token: 0x04000619 RID: 1561
	public float WeaponSwitchFrequency = 5f;

	// Token: 0x0400061A RID: 1562
	public float ToolSwitchFrequency = 5f;

	// Token: 0x0400061B RID: 1563
	private global::NPCHumanContext _aiContext;

	// Token: 0x0400061C RID: 1564
	public global::StateTimer BusyTimer;

	// Token: 0x0400061D RID: 1565
	private float maxFleeTime;

	// Token: 0x0400061E RID: 1566
	private float fleeHealthThresholdPercentage = 1f;

	// Token: 0x0400061F RID: 1567
	private float aggroTimeout = float.NegativeInfinity;

	// Token: 0x04000620 RID: 1568
	private float lastAggroChanceResult;

	// Token: 0x04000621 RID: 1569
	private float lastAggroChanceCalcTime;

	// Token: 0x04000622 RID: 1570
	private const float aggroChanceRecalcTimeout = 5f;

	// Token: 0x04000623 RID: 1571
	private global::BaseEntity blockTargetingThisEnemy;

	// Token: 0x04000624 RID: 1572
	[ReadOnly]
	public float NextWeaponSwitchTime;

	// Token: 0x04000625 RID: 1573
	[ReadOnly]
	public float NextToolSwitchTime;

	// Token: 0x04000626 RID: 1574
	private bool wasAggro;

	// Token: 0x04000627 RID: 1575
	[NonSerialized]
	public float TimeLastMoved;

	// Token: 0x04000628 RID: 1576
	[NonSerialized]
	public float TimeLastMovedToCover;

	// Token: 0x04000629 RID: 1577
	[NonSerialized]
	public float AllyAttackedRecentlyTimeout;

	// Token: 0x0400062D RID: 1581
	public global::BaseNpc.Behaviour _currentBehavior;

	// Token: 0x04000631 RID: 1585
	protected float lastInRangeOfSpawnPositionTime = float.NegativeInfinity;

	// Token: 0x04000632 RID: 1586
	private static Vector3[] pathCornerCache = new Vector3[128];

	// Token: 0x04000633 RID: 1587
	private static NavMeshPath _pathCache = null;

	// Token: 0x0400063A RID: 1594
	[Header("Npc Communication")]
	public float CommunicationRadius = -1f;

	// Token: 0x0400063B RID: 1595
	[NonSerialized]
	public byte[] CurrentFacts = new byte[Enum.GetValues(typeof(global::NPCPlayerApex.Facts)).Length];

	// Token: 0x0400063C RID: 1596
	private List<KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>> sortedSqrRange = new List<KeyValuePair<global::NPCPlayerApex.EnemyRangeEnum, float>>();

	// Token: 0x0400063D RID: 1597
	[Header("NPC Player Senses")]
	public int ForgetUnseenEntityTime = 10;

	// Token: 0x0400063E RID: 1598
	public float SensesTickRate = 0.5f;

	// Token: 0x0400063F RID: 1599
	public float MaxDistanceToCover = 15f;

	// Token: 0x04000640 RID: 1600
	public float MinDistanceToRetreatCover = 6f;

	// Token: 0x04000641 RID: 1601
	[Header("NPC Player Senses Target Scoring")]
	public float VisionRangeScore = 1f;

	// Token: 0x04000642 RID: 1602
	public float AggroRangeScore = 5f;

	// Token: 0x04000643 RID: 1603
	public float LongRangeScore = 1f;

	// Token: 0x04000644 RID: 1604
	public float MediumRangeScore = 5f;

	// Token: 0x04000645 RID: 1605
	public float CloseRangeScore = 10f;

	// Token: 0x04000646 RID: 1606
	[NonSerialized]
	public global::BaseEntity[] SensesResults = new global::BaseEntity[128];

	// Token: 0x04000647 RID: 1607
	private List<Rust.Ai.NavPointSample> navPointSamples = new List<Rust.Ai.NavPointSample>(8);

	// Token: 0x04000648 RID: 1608
	private global::NPCPlayerApex.CoverPointComparer coverPointComparer;

	// Token: 0x04000649 RID: 1609
	private float lastTickTime;

	// Token: 0x0400064A RID: 1610
	private const int sensesTicksPerCoverSweep = 5;

	// Token: 0x0400064B RID: 1611
	private int sensesTicksSinceLastCoverSweep = 5;

	// Token: 0x0400064C RID: 1612
	private float alertness;

	// Token: 0x0400064D RID: 1613
	protected float lastSeenPlayerTime = float.NegativeInfinity;

	// Token: 0x0400064E RID: 1614
	private static Vector3 feetOffset = new Vector3(0f, 0.15f, 0f);

	// Token: 0x0400064F RID: 1615
	private bool isAlreadyCheckingPathPending;

	// Token: 0x04000650 RID: 1616
	private int numPathPendingAttempts;

	// Token: 0x04000651 RID: 1617
	private float accumPathPendingDelay;

	// Token: 0x04000652 RID: 1618
	private float _lastHeardGunshotTime = float.NegativeInfinity;

	// Token: 0x04000655 RID: 1621
	[CompilerGenerated]
	private static Func<global::BaseEntity, bool> <>f__mg$cache0;

	// Token: 0x020000E8 RID: 232
	public class CoverPointComparer : IComparer<Rust.Ai.CoverPoint>
	{
		// Token: 0x06000C18 RID: 3096 RVA: 0x0004F9A4 File Offset: 0x0004DBA4
		public CoverPointComparer(global::BaseEntity compareTo)
		{
			this.compareTo = compareTo;
		}

		// Token: 0x06000C19 RID: 3097 RVA: 0x0004F9B4 File Offset: 0x0004DBB4
		public int Compare(Rust.Ai.CoverPoint a, Rust.Ai.CoverPoint b)
		{
			if (this.compareTo == null || a == null || b == null)
			{
				return 0;
			}
			float sqrMagnitude = (this.compareTo.ServerPosition - a.Position).sqrMagnitude;
			if (sqrMagnitude < 0.01f)
			{
				return -1;
			}
			float sqrMagnitude2 = (this.compareTo.ServerPosition - b.Position).sqrMagnitude;
			if (sqrMagnitude < sqrMagnitude2)
			{
				return -1;
			}
			if (sqrMagnitude > sqrMagnitude2)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x04000656 RID: 1622
		private readonly global::BaseEntity compareTo;
	}

	// Token: 0x020000E9 RID: 233
	// (Invoke) Token: 0x06000C1B RID: 3099
	public delegate void ActionCallback();

	// Token: 0x020000EA RID: 234
	public enum WeaponTypeEnum : byte
	{
		// Token: 0x04000658 RID: 1624
		None,
		// Token: 0x04000659 RID: 1625
		CloseRange,
		// Token: 0x0400065A RID: 1626
		MediumRange,
		// Token: 0x0400065B RID: 1627
		LongRange
	}

	// Token: 0x020000EB RID: 235
	public enum EnemyRangeEnum : byte
	{
		// Token: 0x0400065D RID: 1629
		CloseAttackRange,
		// Token: 0x0400065E RID: 1630
		MediumAttackRange,
		// Token: 0x0400065F RID: 1631
		LongAttackRange,
		// Token: 0x04000660 RID: 1632
		AggroRange,
		// Token: 0x04000661 RID: 1633
		AwareRange,
		// Token: 0x04000662 RID: 1634
		OutOfRange
	}

	// Token: 0x020000EC RID: 236
	public enum ToolTypeEnum : byte
	{
		// Token: 0x04000664 RID: 1636
		None,
		// Token: 0x04000665 RID: 1637
		Research,
		// Token: 0x04000666 RID: 1638
		Lightsource
	}

	// Token: 0x020000ED RID: 237
	public enum Facts
	{
		// Token: 0x04000668 RID: 1640
		HasEnemy,
		// Token: 0x04000669 RID: 1641
		HasSecondaryEnemies,
		// Token: 0x0400066A RID: 1642
		EnemyRange,
		// Token: 0x0400066B RID: 1643
		CanTargetEnemies,
		// Token: 0x0400066C RID: 1644
		Health,
		// Token: 0x0400066D RID: 1645
		Speed,
		// Token: 0x0400066E RID: 1646
		IsWeaponAttackReady,
		// Token: 0x0400066F RID: 1647
		CanReload,
		// Token: 0x04000670 RID: 1648
		IsRoamReady,
		// Token: 0x04000671 RID: 1649
		IsAggro,
		// Token: 0x04000672 RID: 1650
		WantsToFlee,
		// Token: 0x04000673 RID: 1651
		AttackedLately,
		// Token: 0x04000674 RID: 1652
		LoudNoiseNearby,
		// Token: 0x04000675 RID: 1653
		IsMoving,
		// Token: 0x04000676 RID: 1654
		IsFleeing,
		// Token: 0x04000677 RID: 1655
		IsAfraid,
		// Token: 0x04000678 RID: 1656
		AfraidRange,
		// Token: 0x04000679 RID: 1657
		IsUnderHealthThreshold,
		// Token: 0x0400067A RID: 1658
		CanNotMove,
		// Token: 0x0400067B RID: 1659
		SeekingCover,
		// Token: 0x0400067C RID: 1660
		IsInCover,
		// Token: 0x0400067D RID: 1661
		IsCrouched,
		// Token: 0x0400067E RID: 1662
		CurrentAmmoState,
		// Token: 0x0400067F RID: 1663
		CurrentWeaponType,
		// Token: 0x04000680 RID: 1664
		BodyState,
		// Token: 0x04000681 RID: 1665
		HasLineOfSight,
		// Token: 0x04000682 RID: 1666
		CanSwitchWeapon,
		// Token: 0x04000683 RID: 1667
		CoverInRange,
		// Token: 0x04000684 RID: 1668
		IsMovingToCover,
		// Token: 0x04000685 RID: 1669
		ExplosiveInRange,
		// Token: 0x04000686 RID: 1670
		HasLineOfSightCrouched,
		// Token: 0x04000687 RID: 1671
		HasLineOfSightStanding,
		// Token: 0x04000688 RID: 1672
		PathToTargetStatus,
		// Token: 0x04000689 RID: 1673
		AimsAtTarget,
		// Token: 0x0400068A RID: 1674
		RetreatCoverInRange,
		// Token: 0x0400068B RID: 1675
		FlankCoverInRange,
		// Token: 0x0400068C RID: 1676
		AdvanceCoverInRange,
		// Token: 0x0400068D RID: 1677
		IsRetreatingToCover,
		// Token: 0x0400068E RID: 1678
		SidesteppedOutOfCover,
		// Token: 0x0400068F RID: 1679
		IsCoverCompromised,
		// Token: 0x04000690 RID: 1680
		AttackedVeryRecently,
		// Token: 0x04000691 RID: 1681
		RangeToSpawnLocation,
		// Token: 0x04000692 RID: 1682
		AttackedRecently,
		// Token: 0x04000693 RID: 1683
		CurrentToolType,
		// Token: 0x04000694 RID: 1684
		CanSwitchTool,
		// Token: 0x04000695 RID: 1685
		AllyAttackedRecently
	}

	// Token: 0x020000EE RID: 238
	public enum AfraidRangeEnum : byte
	{
		// Token: 0x04000697 RID: 1687
		InAfraidRange,
		// Token: 0x04000698 RID: 1688
		OutOfRange
	}

	// Token: 0x020000EF RID: 239
	public enum HealthEnum : byte
	{
		// Token: 0x0400069A RID: 1690
		Fine,
		// Token: 0x0400069B RID: 1691
		Medium,
		// Token: 0x0400069C RID: 1692
		Low
	}

	// Token: 0x020000F0 RID: 240
	public enum SpeedEnum : byte
	{
		// Token: 0x0400069E RID: 1694
		StandStill,
		// Token: 0x0400069F RID: 1695
		CrouchWalk,
		// Token: 0x040006A0 RID: 1696
		Walk,
		// Token: 0x040006A1 RID: 1697
		Run,
		// Token: 0x040006A2 RID: 1698
		CrouchRun,
		// Token: 0x040006A3 RID: 1699
		Sprint
	}

	// Token: 0x020000F1 RID: 241
	public enum AmmoStateEnum : byte
	{
		// Token: 0x040006A5 RID: 1701
		Full,
		// Token: 0x040006A6 RID: 1702
		High,
		// Token: 0x040006A7 RID: 1703
		Medium,
		// Token: 0x040006A8 RID: 1704
		Low,
		// Token: 0x040006A9 RID: 1705
		Empty
	}

	// Token: 0x020000F2 RID: 242
	public enum BodyState : byte
	{
		// Token: 0x040006AB RID: 1707
		StandingTall,
		// Token: 0x040006AC RID: 1708
		Crouched
	}
}
