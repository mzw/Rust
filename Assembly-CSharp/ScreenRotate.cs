﻿using System;
using UnityEngine;

// Token: 0x02000336 RID: 822
public class ScreenRotate : global::BaseScreenShake
{
	// Token: 0x060013CB RID: 5067 RVA: 0x00073FDC File Offset: 0x000721DC
	public override void Setup()
	{
	}

	// Token: 0x060013CC RID: 5068 RVA: 0x00073FE0 File Offset: 0x000721E0
	public override void Run(float delta, ref global::CachedTransform<Camera> cam, ref global::CachedTransform<global::BaseViewModel> vm)
	{
		Vector3 zero = Vector3.zero;
		zero.x = this.Pitch.Evaluate(delta);
		zero.y = this.Yaw.Evaluate(delta);
		zero.z = this.Roll.Evaluate(delta);
		if (cam)
		{
			cam.rotation *= Quaternion.Euler(zero);
		}
		if (vm && this.useViewModelEffect)
		{
			vm.rotation *= Quaternion.Euler(zero * -1f * (1f - this.ViewmodelEffect.Evaluate(delta)));
		}
	}

	// Token: 0x04000EB6 RID: 3766
	public AnimationCurve Pitch;

	// Token: 0x04000EB7 RID: 3767
	public AnimationCurve Yaw;

	// Token: 0x04000EB8 RID: 3768
	public AnimationCurve Roll;

	// Token: 0x04000EB9 RID: 3769
	public AnimationCurve ViewmodelEffect;

	// Token: 0x04000EBA RID: 3770
	public bool useViewModelEffect = true;
}
