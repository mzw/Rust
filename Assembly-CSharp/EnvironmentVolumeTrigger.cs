﻿using System;
using UnityEngine;

// Token: 0x02000420 RID: 1056
public class EnvironmentVolumeTrigger : MonoBehaviour
{
	// Token: 0x1700019C RID: 412
	// (get) Token: 0x06001819 RID: 6169 RVA: 0x00089254 File Offset: 0x00087454
	// (set) Token: 0x0600181A RID: 6170 RVA: 0x0008925C File Offset: 0x0008745C
	public global::EnvironmentVolume volume { get; private set; }

	// Token: 0x0600181B RID: 6171 RVA: 0x00089268 File Offset: 0x00087468
	protected void Awake()
	{
		this.volume = base.gameObject.GetComponent<global::EnvironmentVolume>();
		if (this.volume == null)
		{
			this.volume = base.gameObject.AddComponent<global::EnvironmentVolume>();
			this.volume.Center = this.Center;
			this.volume.Size = this.Size;
			this.volume.UpdateTrigger();
		}
	}

	// Token: 0x040012A2 RID: 4770
	[HideInInspector]
	public Vector3 Center = Vector3.zero;

	// Token: 0x040012A3 RID: 4771
	[HideInInspector]
	public Vector3 Size = Vector3.one;
}
