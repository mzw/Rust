﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using Oxide.Core;
using Rust;
using UnityEngine;

// Token: 0x0200006A RID: 106
public class FlameTurret : global::StorageContainer
{
	// Token: 0x060007CA RID: 1994 RVA: 0x00032D5C File Offset: 0x00030F5C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("FlameTurret.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060007CB RID: 1995 RVA: 0x00032DA4 File Offset: 0x00030FA4
	public bool IsTriggered()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved4);
	}

	// Token: 0x060007CC RID: 1996 RVA: 0x00032DB4 File Offset: 0x00030FB4
	public Vector3 GetEyePosition()
	{
		return this.eyeTransform.position;
	}

	// Token: 0x060007CD RID: 1997 RVA: 0x00032DC4 File Offset: 0x00030FC4
	public void Update()
	{
		if (base.isServer)
		{
			this.ServerThink();
		}
	}

	// Token: 0x060007CE RID: 1998 RVA: 0x00032DD8 File Offset: 0x00030FD8
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.CanPickup(player) && !this.IsTriggered();
	}

	// Token: 0x060007CF RID: 1999 RVA: 0x00032DF4 File Offset: 0x00030FF4
	public void SetTriggered(bool triggered)
	{
		if (triggered && this.HasFuel())
		{
			this.triggeredTime = Time.realtimeSinceStartup;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved4, triggered && this.HasFuel(), false);
	}

	// Token: 0x060007D0 RID: 2000 RVA: 0x00032E30 File Offset: 0x00031030
	public override void ServerInit()
	{
		base.ServerInit();
		base.InvokeRepeating(new Action(this.SendAimDir), 0f, 0.1f);
	}

	// Token: 0x060007D1 RID: 2001 RVA: 0x00032E54 File Offset: 0x00031054
	public void SendAimDir()
	{
		base.ClientRPC<Vector3>(null, "CLIENT_ReceiveAimDir", this.aimDir);
	}

	// Token: 0x060007D2 RID: 2002 RVA: 0x00032E68 File Offset: 0x00031068
	public float GetSpinSpeed()
	{
		return (float)((!this.IsTriggered()) ? 45 : 180);
	}

	// Token: 0x060007D3 RID: 2003 RVA: 0x00032E84 File Offset: 0x00031084
	public override void OnAttacked(global::HitInfo info)
	{
		if (base.isClient)
		{
			return;
		}
		if (info.damageTypes.IsMeleeType())
		{
			this.SetTriggered(true);
		}
		base.OnAttacked(info);
	}

	// Token: 0x060007D4 RID: 2004 RVA: 0x00032EB0 File Offset: 0x000310B0
	public void MovementUpdate(float delta)
	{
		this.aimDir += new Vector3(0f, delta * this.GetSpinSpeed(), 0f) * (float)this.turnDir;
		if (this.aimDir.y >= this.arc || this.aimDir.y <= -this.arc)
		{
			this.turnDir *= -1;
			this.aimDir.y = Mathf.Clamp(this.aimDir.y, -this.arc, this.arc);
		}
	}

	// Token: 0x060007D5 RID: 2005 RVA: 0x00032F54 File Offset: 0x00031154
	public void ServerThink()
	{
		float num = Time.realtimeSinceStartup - this.lastServerThink;
		if (num < 0.1f)
		{
			return;
		}
		bool flag = this.IsTriggered();
		this.lastServerThink = Time.realtimeSinceStartup;
		this.MovementUpdate(num);
		if (this.IsTriggered() && (Time.realtimeSinceStartup - this.triggeredTime > this.triggeredDuration || !this.HasFuel()))
		{
			this.SetTriggered(false);
		}
		if (!this.IsTriggered() && this.HasFuel() && this.CheckTrigger())
		{
			this.SetTriggered(true);
			global::Effect.server.Run(this.triggeredEffect.resourcePath, base.transform.position, Vector3.up, null, false);
		}
		if (flag != this.IsTriggered())
		{
			base.SendNetworkUpdateImmediate(false);
		}
		if (this.IsTriggered())
		{
			this.DoFlame(num);
		}
	}

	// Token: 0x060007D6 RID: 2006 RVA: 0x00033038 File Offset: 0x00031238
	public bool CheckTrigger()
	{
		if (Time.realtimeSinceStartup < this.nextTriggerCheckTime)
		{
			return false;
		}
		this.nextTriggerCheckTime = Time.realtimeSinceStartup + 1f / this.triggerCheckRate;
		List<RaycastHit> list = Pool.GetList<RaycastHit>();
		List<global::BasePlayer> list2 = Pool.GetList<global::BasePlayer>();
		global::Vis.Entities<global::BasePlayer>(this.GetEyePosition() + base.transform.forward * 3f, 2.5f, list2, 131072, 2);
		bool flag = false;
		foreach (global::BasePlayer basePlayer in list2)
		{
			if (!basePlayer.IsSleeping() && basePlayer.IsAlive() && !basePlayer.IsBuildingAuthed())
			{
				object obj = Interface.CallHook("CanBeTargeted", new object[]
				{
					basePlayer,
					this
				});
				if (obj is bool)
				{
					Pool.FreeList<RaycastHit>(ref list);
					Pool.FreeList<global::BasePlayer>(ref list2);
					return (bool)obj;
				}
				if (basePlayer.GetEstimatedWorldPosition().y <= this.GetEyePosition().y + 0.5f)
				{
					list.Clear();
					global::GamePhysics.TraceAll(new Ray(basePlayer.eyes.position, (this.GetEyePosition() - basePlayer.eyes.position).normalized), 0f, list, 9f, 1075904769, 0);
					for (int i = 0; i < list.Count; i++)
					{
						RaycastHit hit = list[i];
						global::BaseEntity entity = hit.GetEntity();
						if (entity != null && (entity == this || entity.EqualNetID(this)))
						{
							flag = true;
							break;
						}
						if (!(entity != null) || entity.ShouldBlockProjectiles())
						{
							break;
						}
					}
					if (flag)
					{
						break;
					}
				}
			}
		}
		Pool.FreeList<RaycastHit>(ref list);
		Pool.FreeList<global::BasePlayer>(ref list2);
		return flag;
	}

	// Token: 0x060007D7 RID: 2007 RVA: 0x00033274 File Offset: 0x00031474
	public override void OnKilled(global::HitInfo info)
	{
		float num = (float)this.GetFuelAmount() / 500f;
		global::DamageUtil.RadiusDamage(this, base.LookupPrefab(), this.GetEyePosition(), 2f, 6f, this.damagePerSec, 133120, true);
		global::Effect.server.Run(this.explosionEffect.resourcePath, base.transform.position, Vector3.up, null, false);
		int num2 = Mathf.CeilToInt(Mathf.Clamp(num * 8f, 1f, 8f));
		for (int i = 0; i < num2; i++)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.fireballPrefab.resourcePath, base.transform.position, base.transform.rotation, true);
			if (baseEntity)
			{
				Vector3 onUnitSphere = Random.onUnitSphere;
				baseEntity.transform.position = base.transform.position + new Vector3(0f, 1.5f, 0f) + onUnitSphere * Random.Range(-1f, 1f);
				baseEntity.Spawn();
				baseEntity.SetVelocity(onUnitSphere * (float)Random.Range(3, 10));
			}
		}
		base.OnKilled(info);
	}

	// Token: 0x060007D8 RID: 2008 RVA: 0x000333B4 File Offset: 0x000315B4
	public int GetFuelAmount()
	{
		global::Item slot = this.inventory.GetSlot(0);
		if (slot == null || slot.amount < 1)
		{
			return 0;
		}
		return slot.amount;
	}

	// Token: 0x060007D9 RID: 2009 RVA: 0x000333E8 File Offset: 0x000315E8
	public bool HasFuel()
	{
		return this.GetFuelAmount() > 0;
	}

	// Token: 0x060007DA RID: 2010 RVA: 0x000333F4 File Offset: 0x000315F4
	public bool UseFuel(float seconds)
	{
		global::Item slot = this.inventory.GetSlot(0);
		if (slot == null || slot.amount < 1)
		{
			return false;
		}
		this.pendingFuel += seconds * this.fuelPerSec;
		if (this.pendingFuel >= 1f)
		{
			int num = Mathf.FloorToInt(this.pendingFuel);
			slot.UseItem(num);
			this.pendingFuel -= (float)num;
		}
		return true;
	}

	// Token: 0x060007DB RID: 2011 RVA: 0x0003346C File Offset: 0x0003166C
	public void DoFlame(float delta)
	{
		if (!this.UseFuel(delta))
		{
			return;
		}
		Ray ray;
		ray..ctor(this.GetEyePosition(), base.transform.TransformDirection(Quaternion.Euler(this.aimDir) * Vector3.forward));
		Vector3 origin = ray.origin;
		RaycastHit raycastHit;
		bool flag = Physics.SphereCast(ray, 0.4f, ref raycastHit, this.flameRange, 1084434689);
		if (!flag)
		{
			raycastHit.point = origin + ray.direction * this.flameRange;
		}
		float amount = this.damagePerSec[0].amount;
		this.damagePerSec[0].amount = amount * delta;
		global::DamageUtil.RadiusDamage(this, base.LookupPrefab(), raycastHit.point - ray.direction * 0.1f, this.flameRadius * 0.5f, this.flameRadius, this.damagePerSec, 2230272, true);
		global::DamageUtil.RadiusDamage(this, base.LookupPrefab(), base.GetEstimatedWorldPosition() + new Vector3(0f, 1.25f, 0f), 0.25f, 0.25f, this.damagePerSec, 133120, false);
		this.damagePerSec[0].amount = amount;
		if (Time.realtimeSinceStartup >= this.nextFireballTime)
		{
			this.nextFireballTime = Time.realtimeSinceStartup + Random.Range(1f, 2f);
			bool flag2 = Random.Range(0, 10) <= 7;
			Vector3 vector = (!flag2 || !flag) ? (ray.origin + ray.direction * ((!flag) ? this.flameRange : raycastHit.distance) * Random.Range(0.4f, 1f)) : raycastHit.point;
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.fireballPrefab.resourcePath, vector - ray.direction * 0.25f, default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.creatorEntity = this;
				baseEntity.Spawn();
			}
		}
	}

	// Token: 0x060007DC RID: 2012 RVA: 0x000336AC File Offset: 0x000318AC
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04000397 RID: 919
	public Transform upper;

	// Token: 0x04000398 RID: 920
	public Vector3 aimDir;

	// Token: 0x04000399 RID: 921
	public float arc = 45f;

	// Token: 0x0400039A RID: 922
	public float triggeredDuration = 5f;

	// Token: 0x0400039B RID: 923
	public float flameRange = 7f;

	// Token: 0x0400039C RID: 924
	public float flameRadius = 4f;

	// Token: 0x0400039D RID: 925
	public float fuelPerSec = 1f;

	// Token: 0x0400039E RID: 926
	public Transform eyeTransform;

	// Token: 0x0400039F RID: 927
	public List<Rust.DamageTypeEntry> damagePerSec;

	// Token: 0x040003A0 RID: 928
	public global::GameObjectRef triggeredEffect;

	// Token: 0x040003A1 RID: 929
	public global::GameObjectRef fireballPrefab;

	// Token: 0x040003A2 RID: 930
	public global::GameObjectRef explosionEffect;

	// Token: 0x040003A3 RID: 931
	private float nextFireballTime;

	// Token: 0x040003A4 RID: 932
	private int turnDir = 1;

	// Token: 0x040003A5 RID: 933
	private float lastServerThink;

	// Token: 0x040003A6 RID: 934
	private float triggeredTime;

	// Token: 0x040003A7 RID: 935
	private float triggerCheckRate = 2f;

	// Token: 0x040003A8 RID: 936
	private float nextTriggerCheckTime;

	// Token: 0x040003A9 RID: 937
	private float pendingFuel;
}
