﻿using System;
using UnityEngine;

// Token: 0x02000752 RID: 1874
public class NetworkSleep : MonoBehaviour
{
	// Token: 0x04001F79 RID: 8057
	public static int totalBehavioursDisabled;

	// Token: 0x04001F7A RID: 8058
	public static int totalCollidersDisabled;

	// Token: 0x04001F7B RID: 8059
	public Behaviour[] behaviours;

	// Token: 0x04001F7C RID: 8060
	public Collider[] colliders;

	// Token: 0x04001F7D RID: 8061
	internal int BehavioursDisabled;

	// Token: 0x04001F7E RID: 8062
	internal int CollidersDisabled;
}
