﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000690 RID: 1680
public class MapInterface : SingletonComponent<global::MapInterface>
{
	// Token: 0x04001C6B RID: 7275
	public RawImage mapImage;

	// Token: 0x04001C6C RID: 7276
	public Image cameraPositon;

	// Token: 0x04001C6D RID: 7277
	public UnityEngine.UI.ScrollRectEx scrollRect;

	// Token: 0x04001C6E RID: 7278
	public global::PaintableImageGrid paintGrid;

	// Token: 0x04001C6F RID: 7279
	public global::UIPaintBox paintBox;

	// Token: 0x04001C70 RID: 7280
	public Toggle showGridToggle;

	// Token: 0x04001C71 RID: 7281
	public global::NeedsCursor needsCursor;

	// Token: 0x04001C72 RID: 7282
	public GameObject monumentMarkerContainer;

	// Token: 0x04001C73 RID: 7283
	public GameObject monumentMarkerPrefab;

	// Token: 0x04001C74 RID: 7284
	public bool followingPlayer = true;
}
