﻿using System;
using UnityEngine;

// Token: 0x02000544 RID: 1348
public class DecorFlip : global::DecorComponent
{
	// Token: 0x06001C88 RID: 7304 RVA: 0x0009FE28 File Offset: 0x0009E028
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		uint num = SeedEx.Seed(pos, global::World.Seed) + 4u;
		if (SeedRandom.Value(ref num) > 0.5f)
		{
			return;
		}
		global::DecorFlip.AxisType flipAxis = this.FlipAxis;
		if (flipAxis != global::DecorFlip.AxisType.X && flipAxis != global::DecorFlip.AxisType.Z)
		{
			if (flipAxis == global::DecorFlip.AxisType.Y)
			{
				rot = Quaternion.AngleAxis(180f, rot * Vector3.forward) * rot;
			}
		}
		else
		{
			rot = Quaternion.AngleAxis(180f, rot * Vector3.up) * rot;
		}
	}

	// Token: 0x04001790 RID: 6032
	public global::DecorFlip.AxisType FlipAxis = global::DecorFlip.AxisType.Y;

	// Token: 0x02000545 RID: 1349
	public enum AxisType
	{
		// Token: 0x04001792 RID: 6034
		X,
		// Token: 0x04001793 RID: 6035
		Y,
		// Token: 0x04001794 RID: 6036
		Z
	}
}
