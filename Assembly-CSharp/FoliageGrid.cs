﻿using System;
using UnityEngine.Rendering;

// Token: 0x02000421 RID: 1057
public class FoliageGrid : SingletonComponent<global::FoliageGrid>, IClientComponent
{
	// Token: 0x040012A4 RID: 4772
	public static bool Paused;

	// Token: 0x040012A5 RID: 4773
	public float CellSize = 50f;

	// Token: 0x040012A6 RID: 4774
	public float MaxMilliseconds = 0.1f;

	// Token: 0x040012A7 RID: 4775
	public global::LayerSelect FoliageLayer = 0;

	// Token: 0x040012A8 RID: 4776
	public ShadowCastingMode FoliageShadows;

	// Token: 0x040012A9 RID: 4777
	public const float MaxRefreshDistance = 100f;
}
