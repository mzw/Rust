﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x020003DB RID: 987
public class TimedExplosive : global::BaseEntity
{
	// Token: 0x06001702 RID: 5890 RVA: 0x0008476C File Offset: 0x0008296C
	public override void ServerInit()
	{
		this.lastBounceTime = Time.time;
		base.ServerInit();
		this.SetFuse(this.GetRandomTimerTime());
		base.ReceiveCollisionMessages(true);
		if (this.waterCausesExplosion)
		{
			base.InvokeRepeating(new Action(this.WaterCheck), 0f, 0.5f);
		}
	}

	// Token: 0x06001703 RID: 5891 RVA: 0x000847C4 File Offset: 0x000829C4
	public void WaterCheck()
	{
		if (this.waterCausesExplosion && base.WaterFactor() >= 0.5f)
		{
			this.Explode();
		}
	}

	// Token: 0x06001704 RID: 5892 RVA: 0x000847E8 File Offset: 0x000829E8
	public virtual void SetFuse(float fuseLength)
	{
		if (base.isServer)
		{
			base.Invoke(new Action(this.Explode), fuseLength);
		}
	}

	// Token: 0x06001705 RID: 5893 RVA: 0x0008480C File Offset: 0x00082A0C
	public virtual float GetRandomTimerTime()
	{
		return Random.Range(this.timerAmountMin, this.timerAmountMax);
	}

	// Token: 0x06001706 RID: 5894 RVA: 0x00084820 File Offset: 0x00082A20
	public virtual void ProjectileImpact(RaycastHit info)
	{
		this.Explode();
	}

	// Token: 0x06001707 RID: 5895 RVA: 0x00084828 File Offset: 0x00082A28
	public virtual void Explode()
	{
		base.GetComponent<Collider>().enabled = false;
		if (this.explosionEffect.isValid)
		{
			global::Effect.server.Run(this.explosionEffect.resourcePath, base.PivotPoint(), (!this.explosionUsesForward) ? Vector3.up : base.transform.forward, null, true);
		}
		if (this.damageTypes.Count > 0)
		{
			if (this.onlyDamageParent)
			{
				global::DamageUtil.RadiusDamage(this.creatorEntity, base.LookupPrefab(), base.CenterPoint(), this.minExplosionRadius, this.explosionRadius, this.damageTypes, 141568, true);
				global::BaseCombatEntity baseCombatEntity = base.GetParentEntity() as global::BaseCombatEntity;
				if (baseCombatEntity)
				{
					global::HitInfo hitInfo = new global::HitInfo();
					hitInfo.Initiator = this.creatorEntity;
					hitInfo.WeaponPrefab = base.LookupPrefab();
					hitInfo.damageTypes.Add(this.damageTypes);
					baseCombatEntity.Hurt(hitInfo);
				}
			}
			else
			{
				global::DamageUtil.RadiusDamage(this.creatorEntity, base.LookupPrefab(), base.CenterPoint(), this.minExplosionRadius, this.explosionRadius, this.damageTypes, 1075980544, true);
			}
		}
		if (base.IsDestroyed)
		{
			return;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06001708 RID: 5896 RVA: 0x00084968 File Offset: 0x00082B68
	public override void OnCollision(Collision collision, global::BaseEntity hitEntity)
	{
		if (this.canStick && !this.IsStuck())
		{
			bool flag = true;
			if (hitEntity)
			{
				flag = this.CanStickTo(hitEntity);
				if (!flag)
				{
					Collider component = base.GetComponent<Collider>();
					if (collision.collider != null && component != null)
					{
						Physics.IgnoreCollision(collision.collider, component);
					}
				}
			}
			if (flag)
			{
				this.DoCollisionStick(collision, hitEntity);
			}
		}
		this.DoBounceEffect();
	}

	// Token: 0x06001709 RID: 5897 RVA: 0x000849EC File Offset: 0x00082BEC
	public virtual bool CanStickTo(global::BaseEntity entity)
	{
		return entity.GetComponent<global::DecorDeployable>() == null;
	}

	// Token: 0x0600170A RID: 5898 RVA: 0x000849FC File Offset: 0x00082BFC
	private void DoBounceEffect()
	{
		if (!this.bounceEffect.isValid)
		{
			return;
		}
		if (Time.time - this.lastBounceTime < 0.2f)
		{
			return;
		}
		Rigidbody component = base.GetComponent<Rigidbody>();
		if (component && component.velocity.magnitude < 1f)
		{
			return;
		}
		if (this.bounceEffect.isValid)
		{
			global::Effect.server.Run(this.bounceEffect.resourcePath, base.GetEstimatedWorldPosition(), Vector3.up, null, true);
		}
		this.lastBounceTime = Time.time;
	}

	// Token: 0x0600170B RID: 5899 RVA: 0x00084A94 File Offset: 0x00082C94
	private void DoCollisionStick(Collision collision, global::BaseEntity ent)
	{
		this.DoStick(collision.contacts[0].point, collision.contacts[0].normal, ent);
	}

	// Token: 0x0600170C RID: 5900 RVA: 0x00084AC0 File Offset: 0x00082CC0
	public virtual void SetMotionEnabled(bool wantsMotion)
	{
		Rigidbody component = base.GetComponent<Rigidbody>();
		if (component)
		{
			component.useGravity = wantsMotion;
			component.isKinematic = !wantsMotion;
		}
	}

	// Token: 0x0600170D RID: 5901 RVA: 0x00084AF0 File Offset: 0x00082CF0
	public bool IsStuck()
	{
		return this.parentEntity.IsValid(true);
	}

	// Token: 0x0600170E RID: 5902 RVA: 0x00084B00 File Offset: 0x00082D00
	public void DoStick(Vector3 position, Vector3 normal, global::BaseEntity ent)
	{
		if (ent == null)
		{
			return;
		}
		if (ent is global::TimedExplosive)
		{
			if (ent.HasParent())
			{
				ent = ent.parentEntity.Get(true);
			}
			position = ent.GetEstimatedWorldPosition();
		}
		this.SetMotionEnabled(false);
		this.SetCollisionEnabled(false);
		if (base.HasChild(ent))
		{
			return;
		}
		base.transform.position = ent.transform.worldToLocalMatrix.MultiplyPoint3x4(position);
		base.transform.rotation = Quaternion.Inverse(ent.transform.rotation) * Quaternion.LookRotation(normal, base.transform.up);
		base.SetParent(ent, global::StringPool.closest);
		Vector3 estimatedWorldPosition = base.GetEstimatedWorldPosition();
		if (this.stickEffect.isValid)
		{
			global::Effect.server.Run(this.stickEffect.resourcePath, estimatedWorldPosition, Vector3.up, null, true);
		}
		base.ReceiveCollisionMessages(false);
	}

	// Token: 0x0600170F RID: 5903 RVA: 0x00084BF4 File Offset: 0x00082DF4
	private void UnStick()
	{
		if (!base.GetParentEntity())
		{
			return;
		}
		Vector3 estimatedWorldPosition = base.GetEstimatedWorldPosition();
		base.SetParent(null, 0u);
		base.transform.position = estimatedWorldPosition;
		this.SetMotionEnabled(true);
		this.SetCollisionEnabled(true);
		base.SendNetworkUpdate_Position();
		base.ReceiveCollisionMessages(true);
	}

	// Token: 0x06001710 RID: 5904 RVA: 0x00084C48 File Offset: 0x00082E48
	internal override void OnParentRemoved()
	{
		this.UnStick();
	}

	// Token: 0x06001711 RID: 5905 RVA: 0x00084C50 File Offset: 0x00082E50
	public virtual void SetCollisionEnabled(bool wantsCollision)
	{
		Collider component = base.GetComponent<Collider>();
		if (component.enabled != wantsCollision)
		{
			component.enabled = wantsCollision;
		}
	}

	// Token: 0x040011AE RID: 4526
	public float timerAmountMin = 10f;

	// Token: 0x040011AF RID: 4527
	public float timerAmountMax = 20f;

	// Token: 0x040011B0 RID: 4528
	public float minExplosionRadius;

	// Token: 0x040011B1 RID: 4529
	public float explosionRadius = 10f;

	// Token: 0x040011B2 RID: 4530
	public bool canStick;

	// Token: 0x040011B3 RID: 4531
	public bool onlyDamageParent;

	// Token: 0x040011B4 RID: 4532
	public global::GameObjectRef explosionEffect;

	// Token: 0x040011B5 RID: 4533
	public global::GameObjectRef stickEffect;

	// Token: 0x040011B6 RID: 4534
	public global::GameObjectRef bounceEffect;

	// Token: 0x040011B7 RID: 4535
	public bool explosionUsesForward;

	// Token: 0x040011B8 RID: 4536
	public bool waterCausesExplosion;

	// Token: 0x040011B9 RID: 4537
	public List<Rust.DamageTypeEntry> damageTypes = new List<Rust.DamageTypeEntry>();

	// Token: 0x040011BA RID: 4538
	[NonSerialized]
	private float lastBounceTime;
}
