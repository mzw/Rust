﻿using System;
using Oxide.Core;
using UnityEngine;

// Token: 0x020003BF RID: 959
public class PlayerBelt
{
	// Token: 0x06001690 RID: 5776 RVA: 0x00082658 File Offset: 0x00080858
	public PlayerBelt(global::BasePlayer player)
	{
		this.player = player;
	}

	// Token: 0x1700018A RID: 394
	// (get) Token: 0x06001691 RID: 5777 RVA: 0x00082668 File Offset: 0x00080868
	public static int MaxBeltSlots
	{
		get
		{
			return 6;
		}
	}

	// Token: 0x06001692 RID: 5778 RVA: 0x0008266C File Offset: 0x0008086C
	public void DropActive(Vector3 velocity)
	{
		global::Item activeItem = this.player.GetActiveItem();
		if (activeItem == null)
		{
			return;
		}
		if (Interface.CallHook("OnPlayerDropActiveItem", new object[]
		{
			this.player,
			activeItem
		}) != null)
		{
			return;
		}
		using (TimeWarning.New("PlayerBelt.DropActive", 0.1f))
		{
			activeItem.Drop(this.player.eyes.position, velocity, default(Quaternion));
			this.player.svActiveItemID = 0u;
			this.player.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x04001123 RID: 4387
	public static int SelectedSlot = -1;

	// Token: 0x04001124 RID: 4388
	protected global::BasePlayer player;
}
