﻿using System;
using UnityEngine;

// Token: 0x02000410 RID: 1040
public class DeployVolumeCapsule : global::DeployVolume
{
	// Token: 0x060017EC RID: 6124 RVA: 0x000888A4 File Offset: 0x00086AA4
	protected override bool Check(Vector3 position, Quaternion rotation, int mask = -1)
	{
		position += rotation * (this.worldRotation * this.center + this.worldPosition);
		Vector3 start = position + rotation * this.worldRotation * Vector3.up * this.height * 0.5f;
		Vector3 end = position + rotation * this.worldRotation * Vector3.down * this.height * 0.5f;
		return global::DeployVolume.CheckCapsule(start, end, this.radius, this.layers & mask, this.ignore);
	}

	// Token: 0x060017ED RID: 6125 RVA: 0x00088968 File Offset: 0x00086B68
	protected override bool Check(Vector3 position, Quaternion rotation, OBB obb, int mask = -1)
	{
		return false;
	}

	// Token: 0x0400127F RID: 4735
	public Vector3 center = Vector3.zero;

	// Token: 0x04001280 RID: 4736
	public float radius = 0.5f;

	// Token: 0x04001281 RID: 4737
	public float height = 1f;
}
