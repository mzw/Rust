﻿using System;
using UnityEngine;

// Token: 0x0200055D RID: 1373
public class TerrainCheckGeneratorVolumes : MonoBehaviour, IEditorComponent
{
	// Token: 0x06001CB5 RID: 7349 RVA: 0x000A0D3C File Offset: 0x0009EF3C
	protected void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		global::GizmosUtil.DrawWireCircleY(base.transform.position, this.PlacementRadius);
	}

	// Token: 0x040017D8 RID: 6104
	public float PlacementRadius;
}
