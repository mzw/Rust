﻿using System;
using System.Collections.Generic;

// Token: 0x020001D1 RID: 465
public class AmbienceManager : SingletonComponent<global::AmbienceManager>, IClientComponent
{
	// Token: 0x040008DF RID: 2271
	public List<global::AmbienceManager.EmitterTypeLimit> localEmitterLimits = new List<global::AmbienceManager.EmitterTypeLimit>();

	// Token: 0x040008E0 RID: 2272
	public global::AmbienceManager.EmitterTypeLimit catchallEmitterLimit = new global::AmbienceManager.EmitterTypeLimit();

	// Token: 0x040008E1 RID: 2273
	public int maxActiveLocalEmitters = 5;

	// Token: 0x040008E2 RID: 2274
	public List<global::AmbienceEmitter> baseEmitters = new List<global::AmbienceEmitter>();

	// Token: 0x040008E3 RID: 2275
	public List<global::AmbienceEmitter> emittersInRange = new List<global::AmbienceEmitter>();

	// Token: 0x040008E4 RID: 2276
	public List<global::AmbienceEmitter> activeEmitters = new List<global::AmbienceEmitter>();

	// Token: 0x040008E5 RID: 2277
	public List<global::AmbienceEmitter> emitters = new List<global::AmbienceEmitter>();

	// Token: 0x040008E6 RID: 2278
	public float localEmitterRange = 30f;

	// Token: 0x020001D2 RID: 466
	[Serializable]
	public class EmitterTypeLimit
	{
		// Token: 0x040008E7 RID: 2279
		public List<global::AmbienceDefinitionList> ambience;

		// Token: 0x040008E8 RID: 2280
		public int limit = 1;

		// Token: 0x040008E9 RID: 2281
		public int active;
	}
}
