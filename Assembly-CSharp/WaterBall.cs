﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x020003B4 RID: 948
public class WaterBall : global::BaseEntity
{
	// Token: 0x06001674 RID: 5748 RVA: 0x00081E98 File Offset: 0x00080098
	public override void ServerInit()
	{
		base.ServerInit();
		base.Invoke(new Action(this.Extinguish), 10f);
	}

	// Token: 0x06001675 RID: 5749 RVA: 0x00081EB8 File Offset: 0x000800B8
	public void Extinguish()
	{
		base.CancelInvoke(new Action(this.Extinguish));
		if (!base.IsDestroyed)
		{
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
	}

	// Token: 0x06001676 RID: 5750 RVA: 0x00081EE0 File Offset: 0x000800E0
	public void FixedUpdate()
	{
		if (base.isServer)
		{
			base.GetComponent<Rigidbody>().AddForce(Physics.gravity, 5);
		}
	}

	// Token: 0x06001677 RID: 5751 RVA: 0x00081F00 File Offset: 0x00080100
	public void DoSplash()
	{
		float num = 2.5f;
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(base.transform.position + new Vector3(0f, num * 0.75f, 0f), num, list, 1084435201, 2);
		int num2 = 0;
		while (this.waterAmount > 0 && num2 < 3)
		{
			List<global::ISplashable> list2 = Pool.GetList<global::ISplashable>();
			foreach (global::BaseEntity baseEntity in list)
			{
				if (!baseEntity.isClient)
				{
					global::ISplashable splashable = baseEntity as global::ISplashable;
					if (splashable != null && !list2.Contains(splashable) && splashable.wantsSplash(this.liquidType, this.waterAmount))
					{
						list2.Add(splashable);
					}
				}
			}
			if (list2.Count == 0)
			{
				break;
			}
			int num3 = Mathf.CeilToInt((float)(this.waterAmount / list2.Count));
			foreach (global::ISplashable splashable2 in list2)
			{
				int num4 = splashable2.DoSplash(this.liquidType, Mathf.Min(this.waterAmount, num3));
				this.waterAmount -= num4;
				if (this.waterAmount <= 0)
				{
					break;
				}
			}
			Pool.FreeList<global::ISplashable>(ref list2);
			num2++;
		}
		Pool.FreeList<global::BaseEntity>(ref list);
	}

	// Token: 0x06001678 RID: 5752 RVA: 0x000820B4 File Offset: 0x000802B4
	private void OnCollisionEnter(Collision collision)
	{
		if (base.isClient)
		{
			return;
		}
		if (this.myRigidBody.isKinematic)
		{
			return;
		}
		this.DoSplash();
		global::Effect.server.Run(this.waterExplosion.resourcePath, base.transform.position + new Vector3(0f, 0f, 0f), Vector3.up, null, false);
		this.myRigidBody.isKinematic = true;
		base.Invoke(new Action(this.Extinguish), 2f);
	}

	// Token: 0x040010E2 RID: 4322
	public global::ItemDefinition liquidType;

	// Token: 0x040010E3 RID: 4323
	public int waterAmount;

	// Token: 0x040010E4 RID: 4324
	public global::GameObjectRef waterExplosion;

	// Token: 0x040010E5 RID: 4325
	public Rigidbody myRigidBody;
}
