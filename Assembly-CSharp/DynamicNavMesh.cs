﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x020001AF RID: 431
public class DynamicNavMesh : SingletonComponent<global::DynamicNavMesh>, IServerComponent
{
	// Token: 0x170000D1 RID: 209
	// (get) Token: 0x06000E46 RID: 3654 RVA: 0x00059FF0 File Offset: 0x000581F0
	public bool IsBuilding
	{
		get
		{
			return !this.HasBuildOperationStarted || this.BuildingOperation != null;
		}
	}

	// Token: 0x06000E47 RID: 3655 RVA: 0x0005A00C File Offset: 0x0005820C
	private void OnEnable()
	{
		if (Rust.Ai.AiManager.nav_grid)
		{
			base.enabled = false;
			return;
		}
		this.agentTypeId = NavMesh.GetSettingsByIndex(this.NavMeshAgentTypeIndex).agentTypeID;
		this.NavMeshData = new NavMeshData(this.agentTypeId);
		this.sources = new List<NavMeshBuildSource>();
		this.terrainBakes = new List<global::AsyncTerrainNavMeshBake>();
		this.defaultArea = NavMesh.GetAreaFromName(this.DefaultAreaName);
		base.InvokeRepeating(new Action(this.FinishBuildingNavmesh), 0f, 1f);
	}

	// Token: 0x06000E48 RID: 3656 RVA: 0x0005A098 File Offset: 0x00058298
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (Rust.Ai.AiManager.nav_grid)
		{
			return;
		}
		base.CancelInvoke(new Action(this.FinishBuildingNavmesh));
		this.NavMeshDataInstance.Remove();
	}

	// Token: 0x06000E49 RID: 3657 RVA: 0x0005A0D0 File Offset: 0x000582D0
	[ContextMenu("Update Nav Mesh")]
	public void UpdateNavMeshAsync()
	{
		if (this.HasBuildOperationStarted)
		{
			return;
		}
		if (Rust.Ai.AiManager.nav_disable)
		{
			return;
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		Debug.Log("Starting Navmesh Build with " + this.sources.Count + " sources");
		NavMeshBuildSettings settingsByIndex = NavMesh.GetSettingsByIndex(this.NavMeshAgentTypeIndex);
		settingsByIndex.overrideVoxelSize = true;
		settingsByIndex.voxelSize *= 2f;
		this.BuildingOperation = NavMeshBuilder.UpdateNavMeshDataAsync(this.NavMeshData, settingsByIndex, this.sources, this.Bounds);
		this.BuildTimer.Reset();
		this.BuildTimer.Start();
		this.HasBuildOperationStarted = true;
		float num = Time.realtimeSinceStartup - realtimeSinceStartup;
		if (num > 0.1f)
		{
			Debug.LogWarning("Calling UpdateNavMesh took " + num);
		}
	}

	// Token: 0x06000E4A RID: 3658 RVA: 0x0005A1AC File Offset: 0x000583AC
	private IEnumerator CollectSourcesAsync(Action callback)
	{
		float time = Time.realtimeSinceStartup;
		Debug.Log("Starting Navmesh Source Collecting.");
		List<NavMeshBuildMarkup> markups = new List<NavMeshBuildMarkup>();
		NavMeshBuilder.CollectSources(this.Bounds, this.LayerMask, this.NavMeshCollectGeometry, this.defaultArea, markups, this.sources);
		if (global::TerrainMeta.HeightMap != null)
		{
			for (float x = -this.Bounds.extents.x; x < this.Bounds.extents.x; x += (float)this.AsyncTerrainNavMeshBakeCellSize)
			{
				for (float z = -this.Bounds.extents.z; z < this.Bounds.extents.z; z += (float)this.AsyncTerrainNavMeshBakeCellSize)
				{
					global::AsyncTerrainNavMeshBake terrainSource = new global::AsyncTerrainNavMeshBake(new Vector3(x, 0f, z), this.AsyncTerrainNavMeshBakeCellSize, this.AsyncTerrainNavMeshBakeCellHeight, false, true);
					yield return terrainSource;
					this.terrainBakes.Add(terrainSource);
					this.sources.Add(terrainSource.CreateNavMeshBuildSource(true));
				}
			}
		}
		this.AppendModifierVolumes(ref this.sources);
		float timeTaken = Time.realtimeSinceStartup - time;
		if (timeTaken > 0.1f)
		{
			Debug.LogWarning("Calling CollectSourcesAsync took " + timeTaken);
		}
		if (callback != null)
		{
			callback();
		}
		yield break;
	}

	// Token: 0x06000E4B RID: 3659 RVA: 0x0005A1D0 File Offset: 0x000583D0
	public IEnumerator UpdateNavMeshAndWait()
	{
		if (this.HasBuildOperationStarted)
		{
			yield break;
		}
		if (Rust.Ai.AiManager.nav_disable)
		{
			yield break;
		}
		this.HasBuildOperationStarted = false;
		this.Bounds.size = global::TerrainMeta.Size;
		this.AsyncTerrainNavMeshBakeCellSize = Rust.Ai.AiManager.nav_grid_cell_width;
		this.AsyncTerrainNavMeshBakeCellHeight = Rust.Ai.AiManager.nav_grid_cell_height;
		NavMesh.pathfindingIterationsPerFrame = Rust.Ai.AiManager.pathfindingIterationsPerFrame;
		if (Rust.Ai.AiManager.nav_wait)
		{
			yield return this.CollectSourcesAsync(new Action(this.UpdateNavMeshAsync));
		}
		else
		{
			base.StartCoroutine(this.CollectSourcesAsync(new Action(this.UpdateNavMeshAsync)));
		}
		if (!Rust.Ai.AiManager.nav_wait)
		{
			Debug.Log("nav_wait is false, so we're not waiting for the navmesh to finish generating. This might cause your server to sputter while it's generating.");
			yield break;
		}
		int lastPct = 0;
		while (!this.HasBuildOperationStarted)
		{
			Thread.Sleep(250);
			yield return null;
		}
		while (this.BuildingOperation != null)
		{
			int pctDone = (int)(this.BuildingOperation.progress * 100f);
			if (lastPct != pctDone)
			{
				Debug.LogFormat("{0}%", new object[]
				{
					pctDone
				});
				lastPct = pctDone;
			}
			Thread.Sleep(250);
			this.FinishBuildingNavmesh();
			yield return null;
		}
		yield break;
	}

	// Token: 0x06000E4C RID: 3660 RVA: 0x0005A1EC File Offset: 0x000583EC
	private void AppendModifierVolumes(ref List<NavMeshBuildSource> sources)
	{
		List<NavMeshModifierVolume> activeModifiers = NavMeshModifierVolume.activeModifiers;
		foreach (NavMeshModifierVolume navMeshModifierVolume in activeModifiers)
		{
			if ((this.LayerMask & 1 << navMeshModifierVolume.gameObject.layer) != 0)
			{
				if (navMeshModifierVolume.AffectsAgentType(this.agentTypeId))
				{
					Vector3 vector = navMeshModifierVolume.transform.TransformPoint(navMeshModifierVolume.center);
					Vector3 lossyScale = navMeshModifierVolume.transform.lossyScale;
					Vector3 size;
					size..ctor(navMeshModifierVolume.size.x * Mathf.Abs(lossyScale.x), navMeshModifierVolume.size.y * Mathf.Abs(lossyScale.y), navMeshModifierVolume.size.z * Mathf.Abs(lossyScale.z));
					NavMeshBuildSource item = default(NavMeshBuildSource);
					item.shape = 5;
					item.transform = Matrix4x4.TRS(vector, navMeshModifierVolume.transform.rotation, Vector3.one);
					item.size = size;
					item.area = navMeshModifierVolume.area;
					sources.Add(item);
				}
			}
		}
	}

	// Token: 0x06000E4D RID: 3661 RVA: 0x0005A350 File Offset: 0x00058550
	public void FinishBuildingNavmesh()
	{
		if (this.BuildingOperation == null)
		{
			return;
		}
		if (!this.BuildingOperation.isDone)
		{
			return;
		}
		if (!this.NavMeshDataInstance.valid)
		{
			this.NavMeshDataInstance = NavMesh.AddNavMeshData(this.NavMeshData);
		}
		Debug.Log(string.Format("Navmesh Build took {0:0.00} seconds", this.BuildTimer.Elapsed.TotalSeconds));
		this.BuildingOperation = null;
	}

	// Token: 0x04000846 RID: 2118
	public int NavMeshAgentTypeIndex;

	// Token: 0x04000847 RID: 2119
	[Tooltip("The default area associated with the NavMeshAgent index.")]
	public string DefaultAreaName = "Walkable";

	// Token: 0x04000848 RID: 2120
	public int AsyncTerrainNavMeshBakeCellSize = 80;

	// Token: 0x04000849 RID: 2121
	public int AsyncTerrainNavMeshBakeCellHeight = 100;

	// Token: 0x0400084A RID: 2122
	public Bounds Bounds;

	// Token: 0x0400084B RID: 2123
	public NavMeshData NavMeshData;

	// Token: 0x0400084C RID: 2124
	public NavMeshDataInstance NavMeshDataInstance;

	// Token: 0x0400084D RID: 2125
	public LayerMask LayerMask;

	// Token: 0x0400084E RID: 2126
	public NavMeshCollectGeometry NavMeshCollectGeometry;

	// Token: 0x0400084F RID: 2127
	private List<global::AsyncTerrainNavMeshBake> terrainBakes;

	// Token: 0x04000850 RID: 2128
	private List<NavMeshBuildSource> sources;

	// Token: 0x04000851 RID: 2129
	private AsyncOperation BuildingOperation;

	// Token: 0x04000852 RID: 2130
	private bool HasBuildOperationStarted;

	// Token: 0x04000853 RID: 2131
	private Stopwatch BuildTimer = new Stopwatch();

	// Token: 0x04000854 RID: 2132
	private int defaultArea;

	// Token: 0x04000855 RID: 2133
	private int agentTypeId;
}
