﻿using System;
using UnityEngine;

// Token: 0x020002A1 RID: 673
public class Buttons
{
	// Token: 0x020002A2 RID: 674
	public class ConButton : ConsoleSystem.IConsoleCommand
	{
		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06001184 RID: 4484 RVA: 0x00068FF8 File Offset: 0x000671F8
		// (set) Token: 0x06001185 RID: 4485 RVA: 0x00069000 File Offset: 0x00067200
		public bool IsDown { get; set; }

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06001186 RID: 4486 RVA: 0x0006900C File Offset: 0x0006720C
		public bool JustPressed
		{
			get
			{
				return this.IsDown && this.frame == Time.frameCount;
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06001187 RID: 4487 RVA: 0x0006902C File Offset: 0x0006722C
		public bool JustReleased
		{
			get
			{
				return !this.IsDown && this.frame == Time.frameCount;
			}
		}

		// Token: 0x06001188 RID: 4488 RVA: 0x0006904C File Offset: 0x0006724C
		public void Call(ConsoleSystem.Arg arg)
		{
			this.IsDown = arg.GetBool(0, false);
			this.frame = Time.frameCount;
		}

		// Token: 0x04000CB9 RID: 3257
		private int frame;
	}
}
