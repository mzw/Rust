﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Facepunch.Rust;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;

// Token: 0x020004BB RID: 1211
public class ItemCrafter : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x060019F3 RID: 6643 RVA: 0x00091864 File Offset: 0x0008FA64
	public void AddContainer(global::ItemContainer container)
	{
		this.containers.Add(container);
	}

	// Token: 0x060019F4 RID: 6644 RVA: 0x00091874 File Offset: 0x0008FA74
	public static float GetScaledDuration(global::ItemBlueprint bp, float workbenchLevel)
	{
		float num = workbenchLevel - (float)bp.workbenchLevelRequired;
		if (num == 1f)
		{
			return bp.time * 0.5f;
		}
		if (num >= 2f)
		{
			return bp.time * 0.25f;
		}
		return bp.time;
	}

	// Token: 0x060019F5 RID: 6645 RVA: 0x000918C4 File Offset: 0x0008FAC4
	public void ServerUpdate(float delta)
	{
		if (this.queue.Count == 0)
		{
			return;
		}
		global::ItemCraftTask itemCraftTask = this.queue.Peek();
		if (itemCraftTask.cancelled)
		{
			itemCraftTask.owner.Command("note.craft_done", new object[]
			{
				itemCraftTask.taskUID,
				0
			});
			this.queue.Dequeue();
			return;
		}
		float currentCraftLevel = itemCraftTask.owner.currentCraftLevel;
		if (itemCraftTask.endTime > UnityEngine.Time.realtimeSinceStartup)
		{
			return;
		}
		if (itemCraftTask.endTime == 0f)
		{
			float scaledDuration = global::ItemCrafter.GetScaledDuration(itemCraftTask.blueprint, currentCraftLevel);
			itemCraftTask.endTime = UnityEngine.Time.realtimeSinceStartup + scaledDuration;
			if (itemCraftTask.owner != null)
			{
				itemCraftTask.owner.Command("note.craft_start", new object[]
				{
					itemCraftTask.taskUID,
					scaledDuration,
					itemCraftTask.amount
				});
				if (itemCraftTask.owner.IsAdmin && ConVar.Craft.instant)
				{
					itemCraftTask.endTime = UnityEngine.Time.realtimeSinceStartup + 1f;
				}
			}
			return;
		}
		this.FinishCrafting(itemCraftTask);
		if (itemCraftTask.amount <= 0)
		{
			this.queue.Dequeue();
		}
		else
		{
			itemCraftTask.endTime = 0f;
		}
	}

	// Token: 0x060019F6 RID: 6646 RVA: 0x00091A24 File Offset: 0x0008FC24
	private void CollectIngredient(int item, int amount, List<global::Item> collect)
	{
		foreach (global::ItemContainer itemContainer in this.containers)
		{
			amount -= itemContainer.Take(collect, item, amount);
			if (amount <= 0)
			{
				break;
			}
		}
	}

	// Token: 0x060019F7 RID: 6647 RVA: 0x00091A94 File Offset: 0x0008FC94
	private void CollectIngredients(global::ItemBlueprint bp, global::ItemCraftTask task, int amount = 1, global::BasePlayer player = null)
	{
		List<global::Item> list = new List<global::Item>();
		foreach (global::ItemAmount itemAmount in bp.ingredients)
		{
			this.CollectIngredient(itemAmount.itemid, (int)itemAmount.amount * amount, list);
		}
		task.potentialOwners = new List<ulong>();
		foreach (global::Item item in list)
		{
			item.CollectedForCrafting(player);
			if (!task.potentialOwners.Contains(player.userID))
			{
				task.potentialOwners.Add(player.userID);
			}
		}
		task.takenItems = list;
	}

	// Token: 0x060019F8 RID: 6648 RVA: 0x00091B88 File Offset: 0x0008FD88
	public bool CraftItem(global::ItemBlueprint bp, global::BasePlayer owner, ProtoBuf.Item.InstanceData instanceData = null, int amount = 1, int skinID = 0, global::Item fromTempBlueprint = null)
	{
		if (!this.CanCraft(bp, amount))
		{
			return false;
		}
		this.taskUID++;
		global::ItemCraftTask itemCraftTask = Facepunch.Pool.Get<global::ItemCraftTask>();
		itemCraftTask.blueprint = bp;
		this.CollectIngredients(bp, itemCraftTask, amount, owner);
		itemCraftTask.endTime = 0f;
		itemCraftTask.taskUID = this.taskUID;
		itemCraftTask.owner = owner;
		itemCraftTask.instanceData = instanceData;
		if (itemCraftTask.instanceData != null)
		{
			itemCraftTask.instanceData.ShouldPool = false;
		}
		itemCraftTask.amount = amount;
		itemCraftTask.skinID = skinID;
		if (fromTempBlueprint != null && itemCraftTask.takenItems != null)
		{
			fromTempBlueprint.RemoveFromContainer();
			itemCraftTask.takenItems.Add(fromTempBlueprint);
			itemCraftTask.conditionScale = 0.5f;
		}
		object obj = Interface.CallHook("OnItemCraft", new object[]
		{
			itemCraftTask,
			owner,
			fromTempBlueprint
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		this.queue.Enqueue(itemCraftTask);
		if (itemCraftTask.owner != null)
		{
			itemCraftTask.owner.Command("note.craft_add", new object[]
			{
				itemCraftTask.taskUID,
				itemCraftTask.blueprint.targetItem.itemid,
				amount,
				itemCraftTask.skinID
			});
		}
		return true;
	}

	// Token: 0x060019F9 RID: 6649 RVA: 0x00091CEC File Offset: 0x0008FEEC
	private void FinishCrafting(global::ItemCraftTask task)
	{
		task.amount--;
		task.numCrafted++;
		ulong skin = global::ItemDefinition.FindSkin(task.blueprint.targetItem.itemid, task.skinID);
		global::Item item = global::ItemManager.CreateByItemID(task.blueprint.targetItem.itemid, 1, skin);
		item.amount = task.blueprint.amountToCreate;
		if (item.hasCondition && task.conditionScale != 1f)
		{
			item.maxCondition *= task.conditionScale;
			item.condition = item.maxCondition;
		}
		item.OnVirginSpawn();
		foreach (global::ItemAmount itemAmount in task.blueprint.ingredients)
		{
			int num = (int)itemAmount.amount;
			if (task.takenItems != null)
			{
				foreach (global::Item item2 in task.takenItems)
				{
					if (item2.info == itemAmount.itemDef)
					{
						int amount = item2.amount;
						int num2 = Mathf.Min(amount, num);
						item2.UseItem(num);
						num -= num2;
					}
					if (num <= 0)
					{
					}
				}
			}
		}
		Facepunch.Rust.Analytics.Crafting(task.blueprint.targetItem.shortname, task.skinID);
		task.owner.Command("note.craft_done", new object[]
		{
			task.taskUID,
			1,
			task.amount
		});
		Interface.CallHook("OnItemCraftFinished", new object[]
		{
			task,
			item
		});
		item.instanceData = task.instanceData;
		if (!string.IsNullOrEmpty(task.blueprint.UnlockAchievment))
		{
			task.owner.GiveAchievement(task.blueprint.UnlockAchievment);
		}
		if (task.owner.inventory.GiveItem(item, null))
		{
			task.owner.Command("note.inv", new object[]
			{
				item.info.itemid,
				item.amount
			});
			return;
		}
		global::ItemContainer itemContainer = this.containers.First<global::ItemContainer>();
		task.owner.Command("note.inv", new object[]
		{
			item.info.itemid,
			item.amount
		});
		task.owner.Command("note.inv", new object[]
		{
			item.info.itemid,
			-item.amount
		});
		item.Drop(itemContainer.dropPosition, itemContainer.dropVelocity, default(Quaternion));
	}

	// Token: 0x060019FA RID: 6650 RVA: 0x00092020 File Offset: 0x00090220
	public bool CancelTask(int iID, bool ReturnItems)
	{
		if (this.queue.Count == 0)
		{
			return false;
		}
		global::ItemCraftTask itemCraftTask = this.queue.FirstOrDefault((global::ItemCraftTask x) => x.taskUID == iID && !x.cancelled);
		if (itemCraftTask == null)
		{
			return false;
		}
		itemCraftTask.cancelled = true;
		if (itemCraftTask.owner == null)
		{
			return true;
		}
		Interface.CallHook("OnItemCraftCancelled", new object[]
		{
			itemCraftTask
		});
		itemCraftTask.owner.Command("note.craft_done", new object[]
		{
			itemCraftTask.taskUID,
			0
		});
		if (itemCraftTask.takenItems != null && itemCraftTask.takenItems.Count > 0 && ReturnItems)
		{
			foreach (global::Item item in itemCraftTask.takenItems)
			{
				if (item != null && item.amount > 0)
				{
					if (item.IsBlueprint() && item.blueprintTargetDef == itemCraftTask.blueprint.targetItem)
					{
						item.UseItem(itemCraftTask.numCrafted);
					}
					if (item.amount > 0 && !item.MoveToContainer(itemCraftTask.owner.inventory.containerMain, -1, true))
					{
						item.Drop(itemCraftTask.owner.inventory.containerMain.dropPosition, itemCraftTask.owner.inventory.containerMain.dropVelocity, default(Quaternion));
						itemCraftTask.owner.Command("note.inv", new object[]
						{
							item.info.itemid,
							-item.amount
						});
					}
				}
			}
		}
		return true;
	}

	// Token: 0x060019FB RID: 6651 RVA: 0x0009222C File Offset: 0x0009042C
	public bool CancelBlueprint(int itemid)
	{
		if (this.queue.Count == 0)
		{
			return false;
		}
		global::ItemCraftTask itemCraftTask = this.queue.FirstOrDefault((global::ItemCraftTask x) => x.blueprint.targetItem.itemid == itemid && !x.cancelled);
		return itemCraftTask != null && this.CancelTask(itemCraftTask.taskUID, true);
	}

	// Token: 0x060019FC RID: 6652 RVA: 0x00092288 File Offset: 0x00090488
	public void CancelAll(bool returnItems)
	{
		foreach (global::ItemCraftTask itemCraftTask in this.queue)
		{
			this.CancelTask(itemCraftTask.taskUID, returnItems);
		}
	}

	// Token: 0x060019FD RID: 6653 RVA: 0x000922EC File Offset: 0x000904EC
	private bool DoesHaveUsableItem(int item, int iAmount)
	{
		int num = 0;
		foreach (global::ItemContainer itemContainer in this.containers)
		{
			num += itemContainer.GetAmount(item, true);
		}
		return num >= iAmount;
	}

	// Token: 0x060019FE RID: 6654 RVA: 0x00092358 File Offset: 0x00090558
	public bool CanCraft(global::ItemBlueprint bp, int amount = 1)
	{
		if (amount < 1 || amount > 9000)
		{
			return false;
		}
		object obj = Interface.CallHook("CanCraft", new object[]
		{
			this,
			bp,
			amount
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		foreach (global::ItemAmount itemAmount in bp.ingredients)
		{
			if (!this.DoesHaveUsableItem(itemAmount.itemid, (int)itemAmount.amount * amount))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x060019FF RID: 6655 RVA: 0x0009241C File Offset: 0x0009061C
	public bool CanCraft(global::ItemDefinition def, int amount = 1)
	{
		global::ItemBlueprint component = def.GetComponent<global::ItemBlueprint>();
		return this.CanCraft(component, amount);
	}

	// Token: 0x040014C9 RID: 5321
	public List<global::ItemContainer> containers = new List<global::ItemContainer>();

	// Token: 0x040014CA RID: 5322
	public Queue<global::ItemCraftTask> queue = new Queue<global::ItemCraftTask>();

	// Token: 0x040014CB RID: 5323
	public int taskUID;
}
