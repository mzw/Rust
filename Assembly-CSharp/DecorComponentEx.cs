﻿using System;
using UnityEngine;

// Token: 0x02000542 RID: 1346
public static class DecorComponentEx
{
	// Token: 0x06001C82 RID: 7298 RVA: 0x0009FB50 File Offset: 0x0009DD50
	public static void ApplyDecorComponents(this Transform transform, global::DecorComponent[] components, ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		for (int i = 0; i < components.Length; i++)
		{
			components[i].Apply(ref pos, ref rot, ref scale);
		}
	}

	// Token: 0x06001C83 RID: 7299 RVA: 0x0009FB80 File Offset: 0x0009DD80
	public static void ApplyDecorComponents(this Transform transform, global::DecorComponent[] components)
	{
		Vector3 position = transform.position;
		Quaternion rotation = transform.rotation;
		Vector3 localScale = transform.localScale;
		transform.ApplyDecorComponents(components, ref position, ref rotation, ref localScale);
		transform.position = position;
		transform.rotation = rotation;
		transform.localScale = localScale;
	}

	// Token: 0x06001C84 RID: 7300 RVA: 0x0009FBC4 File Offset: 0x0009DDC4
	public static void ApplyDecorComponentsScaleOnly(this Transform transform, global::DecorComponent[] components)
	{
		Vector3 position = transform.position;
		Quaternion rotation = transform.rotation;
		Vector3 localScale = transform.localScale;
		transform.ApplyDecorComponents(components, ref position, ref rotation, ref localScale);
		transform.localScale = localScale;
	}
}
