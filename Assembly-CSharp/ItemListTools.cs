﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020002F1 RID: 753
public class ItemListTools : MonoBehaviour
{
	// Token: 0x0600131A RID: 4890 RVA: 0x000704C0 File Offset: 0x0006E6C0
	public void OnPanelOpened()
	{
		this.Refresh();
	}

	// Token: 0x0600131B RID: 4891 RVA: 0x000704C8 File Offset: 0x0006E6C8
	public void Refresh()
	{
		this.RebuildCategories();
	}

	// Token: 0x0600131C RID: 4892 RVA: 0x000704D0 File Offset: 0x0006E6D0
	private void RebuildCategories()
	{
		for (int i = 0; i < this.categoryButton.transform.parent.childCount; i++)
		{
			Transform child = this.categoryButton.transform.parent.GetChild(i);
			if (!(child == this.categoryButton.transform))
			{
				global::GameManager.Destroy(child.gameObject, 0f);
			}
		}
		this.categoryButton.SetActive(true);
		IEnumerable<IGrouping<global::ItemCategory, global::ItemDefinition>> source = from x in global::ItemManager.GetItemDefinitions()
		group x by x.category;
		foreach (IGrouping<global::ItemCategory, global::ItemDefinition> source2 in from x in source
		orderby x.First<global::ItemDefinition>().category
		select x)
		{
			GameObject gameObject = Object.Instantiate<GameObject>(this.categoryButton);
			gameObject.transform.SetParent(this.categoryButton.transform.parent, false);
			Text componentInChildren = gameObject.GetComponentInChildren<Text>();
			componentInChildren.text = source2.First<global::ItemDefinition>().category.ToString();
			Button btn = gameObject.GetComponentInChildren<Button>();
			global::ItemDefinition[] itemArray = source2.ToArray<global::ItemDefinition>();
			btn.onClick.AddListener(delegate
			{
				if (this.lastCategory)
				{
					this.lastCategory.interactable = true;
				}
				this.lastCategory = btn;
				this.lastCategory.interactable = false;
				this.SwitchItemCategory(itemArray);
			});
			if (this.lastCategory == null)
			{
				this.lastCategory = btn;
				this.lastCategory.interactable = false;
				this.SwitchItemCategory(itemArray);
			}
		}
		this.categoryButton.SetActive(false);
	}

	// Token: 0x0600131D RID: 4893 RVA: 0x000706BC File Offset: 0x0006E8BC
	private void SwitchItemCategory(global::ItemDefinition[] defs)
	{
		for (int i = 0; i < this.itemButton.transform.parent.childCount; i++)
		{
			Transform child = this.itemButton.transform.parent.GetChild(i);
			if (!(child == this.itemButton.transform))
			{
				global::GameManager.Destroy(child.gameObject, 0f);
			}
		}
		this.itemButton.SetActive(true);
		foreach (global::ItemDefinition itemDefinition in from x in defs
		orderby x.displayName.translated
		select x)
		{
			if (!itemDefinition.hidden)
			{
				GameObject gameObject = Object.Instantiate<GameObject>(this.itemButton);
				gameObject.transform.SetParent(this.itemButton.transform.parent, false);
				Text componentInChildren = gameObject.GetComponentInChildren<Text>();
				componentInChildren.text = itemDefinition.displayName.translated;
				gameObject.GetComponentInChildren<global::ItemButtonTools>().itemDef = itemDefinition;
				gameObject.GetComponentInChildren<global::ItemButtonTools>().image.sprite = itemDefinition.iconSprite;
			}
		}
		this.itemButton.SetActive(false);
	}

	// Token: 0x04000DCD RID: 3533
	public GameObject categoryButton;

	// Token: 0x04000DCE RID: 3534
	public GameObject itemButton;

	// Token: 0x04000DCF RID: 3535
	internal Button lastCategory;
}
