﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005F1 RID: 1521
[Serializable]
public class WaterRendering
{
	// Token: 0x17000228 RID: 552
	// (get) Token: 0x06001F26 RID: 7974 RVA: 0x000B1070 File Offset: 0x000AF270
	public bool IsInitialized
	{
		get
		{
			return this.initialized;
		}
	}

	// Token: 0x04001A1A RID: 6682
	public float MaxDisplacementDistance = 50f;

	// Token: 0x04001A1B RID: 6683
	public global::SSRControlParams SSRControl;

	// Token: 0x04001A1C RID: 6684
	public global::CausticsAnimation CausticsAnimation = new global::CausticsAnimation();

	// Token: 0x04001A1D RID: 6685
	private bool initialized;

	// Token: 0x020005F2 RID: 1522
	private struct RenderState
	{
		// Token: 0x04001A1E RID: 6686
		public Camera camera;

		// Token: 0x04001A1F RID: 6687
		public int layer;

		// Token: 0x04001A20 RID: 6688
		public MaterialPropertyBlock propertyBlock;

		// Token: 0x04001A21 RID: 6689
		public bool simulation;

		// Token: 0x04001A22 RID: 6690
		public bool displacement;

		// Token: 0x04001A23 RID: 6691
		public int reflections;

		// Token: 0x04001A24 RID: 6692
		public bool caustics;

		// Token: 0x04001A25 RID: 6693
		public int visibilityMask;

		// Token: 0x04001A26 RID: 6694
		public List<global::WaterCullingVolume> cullingVolumes;
	}
}
