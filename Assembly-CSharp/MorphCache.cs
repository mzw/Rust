﻿using System;
using UnityEngine;

// Token: 0x02000611 RID: 1553
public class MorphCache : MonoBehaviour
{
	// Token: 0x04001A76 RID: 6774
	public bool fallback;

	// Token: 0x04001A77 RID: 6775
	public int blendShape = -1;

	// Token: 0x04001A78 RID: 6776
	[Range(0f, 1f)]
	public float[] blendWeights;
}
