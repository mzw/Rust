﻿using System;
using UnityEngine;

// Token: 0x020000B3 RID: 179
public class SellOrderEntry : MonoBehaviour, global::IInventoryChanged
{
	// Token: 0x04000515 RID: 1301
	public global::VirtualItemIcon MerchandiseIcon;

	// Token: 0x04000516 RID: 1302
	public global::VirtualItemIcon CurrencyIcon;

	// Token: 0x04000517 RID: 1303
	private global::ItemDefinition merchandiseInfo;

	// Token: 0x04000518 RID: 1304
	private global::ItemDefinition currencyInfo;

	// Token: 0x04000519 RID: 1305
	public GameObject buyButton;

	// Token: 0x0400051A RID: 1306
	public GameObject cantaffordNotification;

	// Token: 0x0400051B RID: 1307
	public GameObject outOfStockNotification;

	// Token: 0x0400051C RID: 1308
	private global::LootPanelVendingMachine vendingPanel;

	// Token: 0x0400051D RID: 1309
	public global::UIIntegerEntry intEntry;
}
