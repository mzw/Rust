﻿using System;
using UnityEngine;

// Token: 0x0200027E RID: 638
public class ScaleByIntensity : MonoBehaviour
{
	// Token: 0x060010B8 RID: 4280 RVA: 0x00064954 File Offset: 0x00062B54
	private void Start()
	{
		this.initialScale = base.transform.localScale;
	}

	// Token: 0x060010B9 RID: 4281 RVA: 0x00064968 File Offset: 0x00062B68
	private void Update()
	{
		base.transform.localScale = ((!this.intensitySource.enabled) ? Vector3.zero : (this.initialScale * this.intensitySource.intensity / this.maxIntensity));
	}

	// Token: 0x04000BD1 RID: 3025
	public Vector3 initialScale = Vector3.zero;

	// Token: 0x04000BD2 RID: 3026
	public Light intensitySource;

	// Token: 0x04000BD3 RID: 3027
	public float maxIntensity = 1f;
}
