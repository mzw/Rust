﻿using System;
using Painting;
using UnityEngine;
using UnityEngine.Events;

// Token: 0x02000695 RID: 1685
public class UIPaintBox : MonoBehaviour
{
	// Token: 0x06002118 RID: 8472 RVA: 0x000BB8C0 File Offset: 0x000B9AC0
	public void UpdateBrushSize(int size)
	{
		this.brush.brushSize = Vector2.one * (float)size;
		this.brush.spacing = Mathf.Clamp((float)size * 0.1f, 1f, 3f);
		this.OnChanged();
	}

	// Token: 0x06002119 RID: 8473 RVA: 0x000BB90C File Offset: 0x000B9B0C
	public void UpdateBrushTexture(Texture2D tex)
	{
		this.brush.texture = tex;
		this.OnChanged();
	}

	// Token: 0x0600211A RID: 8474 RVA: 0x000BB920 File Offset: 0x000B9B20
	public void UpdateBrushColor(Color col)
	{
		this.brush.color.r = col.r;
		this.brush.color.g = col.g;
		this.brush.color.b = col.b;
		this.OnChanged();
	}

	// Token: 0x0600211B RID: 8475 RVA: 0x000BB978 File Offset: 0x000B9B78
	public void UpdateBrushAlpha(float a)
	{
		this.brush.color.a = a;
		this.OnChanged();
	}

	// Token: 0x0600211C RID: 8476 RVA: 0x000BB994 File Offset: 0x000B9B94
	public void UpdateBrushEraser(bool b)
	{
		this.brush.erase = b;
	}

	// Token: 0x0600211D RID: 8477 RVA: 0x000BB9A4 File Offset: 0x000B9BA4
	private void OnChanged()
	{
		this.onBrushChanged.Invoke(this.brush);
	}

	// Token: 0x04001C81 RID: 7297
	public global::UIPaintBox.OnBrushChanged onBrushChanged = new global::UIPaintBox.OnBrushChanged();

	// Token: 0x04001C82 RID: 7298
	public Painting.Brush brush;

	// Token: 0x02000696 RID: 1686
	[Serializable]
	public class OnBrushChanged : UnityEvent<Painting.Brush>
	{
	}
}
