﻿using System;
using UnityEngine;

// Token: 0x02000564 RID: 1380
public class TerrainColors : global::TerrainExtension
{
	// Token: 0x06001CCA RID: 7370 RVA: 0x000A1418 File Offset: 0x0009F618
	public override void Setup()
	{
		this.splatMap = this.terrain.GetComponent<global::TerrainSplatMap>();
		this.biomeMap = this.terrain.GetComponent<global::TerrainBiomeMap>();
	}

	// Token: 0x06001CCB RID: 7371 RVA: 0x000A143C File Offset: 0x0009F63C
	public Color GetColor(Vector3 worldPos, int mask = -1)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetColor(normX, normZ, mask);
	}

	// Token: 0x06001CCC RID: 7372 RVA: 0x000A146C File Offset: 0x0009F66C
	public Color GetColor(float normX, float normZ, int mask = -1)
	{
		float biome = this.biomeMap.GetBiome(normX, normZ, 1);
		float biome2 = this.biomeMap.GetBiome(normX, normZ, 2);
		float biome3 = this.biomeMap.GetBiome(normX, normZ, 4);
		float biome4 = this.biomeMap.GetBiome(normX, normZ, 8);
		int num = global::TerrainSplat.TypeToIndex(this.splatMap.GetSplatMaxType(normX, normZ, mask));
		global::TerrainConfig.SplatType splatType = this.config.Splats[num];
		return biome * splatType.AridColor + biome2 * splatType.TemperateColor + biome3 * splatType.TundraColor + biome4 * splatType.ArcticColor;
	}

	// Token: 0x040017FA RID: 6138
	private global::TerrainSplatMap splatMap;

	// Token: 0x040017FB RID: 6139
	private global::TerrainBiomeMap biomeMap;
}
