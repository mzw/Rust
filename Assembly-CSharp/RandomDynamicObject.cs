﻿using System;
using UnityEngine;

// Token: 0x0200054E RID: 1358
public class RandomDynamicObject : MonoBehaviour, IClientComponent, global::ILOD
{
	// Token: 0x040017AC RID: 6060
	public uint Seed;

	// Token: 0x040017AD RID: 6061
	public float Distance = 100f;

	// Token: 0x040017AE RID: 6062
	public float Probability = 0.5f;

	// Token: 0x040017AF RID: 6063
	public GameObject[] Candidates;
}
