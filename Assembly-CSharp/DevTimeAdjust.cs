﻿using System;
using UnityEngine;

// Token: 0x02000306 RID: 774
public class DevTimeAdjust : MonoBehaviour
{
	// Token: 0x0600135C RID: 4956 RVA: 0x00071FDC File Offset: 0x000701DC
	private void Start()
	{
		if (!TOD_Sky.Instance)
		{
			return;
		}
		TOD_Sky.Instance.Cycle.Hour = PlayerPrefs.GetFloat("DevTime");
	}

	// Token: 0x0600135D RID: 4957 RVA: 0x00072008 File Offset: 0x00070208
	private void OnGUI()
	{
		if (!TOD_Sky.Instance)
		{
			return;
		}
		float num = (float)Screen.width * 0.2f;
		Rect rect;
		rect..ctor((float)Screen.width - (num + 20f), (float)Screen.height - 30f, num, 20f);
		float num2 = TOD_Sky.Instance.Cycle.Hour;
		num2 = GUI.HorizontalSlider(rect, num2, 0f, 24f);
		rect.y -= 20f;
		GUI.Label(rect, "Time Of Day");
		if (num2 != TOD_Sky.Instance.Cycle.Hour)
		{
			TOD_Sky.Instance.Cycle.Hour = num2;
			PlayerPrefs.SetFloat("DevTime", num2);
		}
	}
}
