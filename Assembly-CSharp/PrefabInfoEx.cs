﻿using System;
using UnityEngine;

// Token: 0x020007AF RID: 1967
public static class PrefabInfoEx
{
	// Token: 0x0600249A RID: 9370 RVA: 0x000C9BB4 File Offset: 0x000C7DB4
	public static bool SupportsPooling(this GameObject gameObject)
	{
		global::PrefabInfo component = gameObject.GetComponent<global::PrefabInfo>();
		return component != null;
	}

	// Token: 0x0600249B RID: 9371 RVA: 0x000C9BD4 File Offset: 0x000C7DD4
	public static void EnablePooling(this GameObject gameObject, uint prefabID)
	{
		if (gameObject.SupportsPooling())
		{
			return;
		}
		Behaviour[] componentsInChildren = gameObject.GetComponentsInChildren<Behaviour>(true);
		Rigidbody[] componentsInChildren2 = gameObject.GetComponentsInChildren<Rigidbody>(true);
		Collider[] componentsInChildren3 = gameObject.GetComponentsInChildren<Collider>(true);
		LODGroup[] componentsInChildren4 = gameObject.GetComponentsInChildren<LODGroup>(true);
		Renderer[] componentsInChildren5 = gameObject.GetComponentsInChildren<Renderer>(true);
		ParticleSystem[] componentsInChildren6 = gameObject.GetComponentsInChildren<ParticleSystem>(true);
		global::PrefabInfo prefabInfo = gameObject.AddComponent<global::PrefabInfo>();
		prefabInfo.prefabID = prefabID;
		prefabInfo.behaviours = componentsInChildren;
		prefabInfo.rigidbodies = componentsInChildren2;
		prefabInfo.colliders = componentsInChildren3;
		prefabInfo.lodgroups = componentsInChildren4;
		prefabInfo.renderers = componentsInChildren5;
		prefabInfo.particles = componentsInChildren6;
		prefabInfo.Initialize();
	}

	// Token: 0x0600249C RID: 9372 RVA: 0x000C9C68 File Offset: 0x000C7E68
	public static void AwakeFromInstantiate(this GameObject gameObject)
	{
		if (gameObject.activeSelf)
		{
			global::PrefabInfo component = gameObject.GetComponent<global::PrefabInfo>();
			component.SetBehaviourEnabled(true);
		}
		else
		{
			gameObject.SetActive(true);
		}
	}
}
