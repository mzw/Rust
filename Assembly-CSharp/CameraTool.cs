﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200005D RID: 93
public class CameraTool : global::HeldEntity
{
	// Token: 0x0600073A RID: 1850 RVA: 0x0002D9BC File Offset: 0x0002BBBC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("CameraTool.OnRpcMessage", 0.1f))
		{
			if (rpc == 1504515067u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SVNoteScreenshot ");
				}
				using (TimeWarning.New("SVNoteScreenshot", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("SVNoteScreenshot", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SVNoteScreenshot(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in SVNoteScreenshot");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600073B RID: 1851 RVA: 0x0002DB98 File Offset: 0x0002BD98
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	private void SVNoteScreenshot(global::BaseEntity.RPCMessage msg)
	{
	}

	// Token: 0x04000345 RID: 837
	public global::GameObjectRef screenshotEffect;
}
