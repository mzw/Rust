﻿using System;
using UnityEngine.UI;

// Token: 0x020006D1 RID: 1745
public class AboutYou : global::BaseMonoBehaviour
{
	// Token: 0x04001D9D RID: 7581
	public Text username;

	// Token: 0x04001D9E RID: 7582
	public RawImage avatar;

	// Token: 0x04001D9F RID: 7583
	public Text subtitle;
}
