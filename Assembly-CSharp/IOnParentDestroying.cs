﻿using System;

// Token: 0x02000435 RID: 1077
public interface IOnParentDestroying
{
	// Token: 0x06001865 RID: 6245
	void OnParentDestroying();
}
