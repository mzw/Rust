﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000498 RID: 1176
public class TriggerComfort : global::TriggerBase
{
	// Token: 0x060019A4 RID: 6564 RVA: 0x00090570 File Offset: 0x0008E770
	private void OnValidate()
	{
		this.triggerSize = base.GetComponent<SphereCollider>().radius * base.transform.localScale.y;
	}

	// Token: 0x060019A5 RID: 6565 RVA: 0x000905A4 File Offset: 0x0008E7A4
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x060019A6 RID: 6566 RVA: 0x000905F0 File Offset: 0x0008E7F0
	public float CalculateComfort(Vector3 position, global::BasePlayer forPlayer = null)
	{
		float num = Vector3.Distance(base.gameObject.transform.position, position);
		float num2 = 1f - Mathf.Clamp(num - this.minComfortRange, 0f, num / (this.triggerSize - this.minComfortRange));
		float num3 = 0f;
		foreach (global::BasePlayer basePlayer in this._players)
		{
			if (!(basePlayer == forPlayer))
			{
				num3 += 0.25f * ((!basePlayer.IsSleeping()) ? 1f : 0.5f) * ((!basePlayer.IsAlive()) ? 0f : 1f);
			}
		}
		float num4 = num3;
		return (this.baseComfort + num4) * num2;
	}

	// Token: 0x060019A7 RID: 6567 RVA: 0x000906EC File Offset: 0x0008E8EC
	internal override void OnEntityEnter(global::BaseEntity ent)
	{
		global::BasePlayer basePlayer = ent as global::BasePlayer;
		if (!basePlayer)
		{
			return;
		}
		this._players.Add(basePlayer);
	}

	// Token: 0x060019A8 RID: 6568 RVA: 0x00090718 File Offset: 0x0008E918
	internal override void OnEntityLeave(global::BaseEntity ent)
	{
		global::BasePlayer basePlayer = ent as global::BasePlayer;
		if (!basePlayer)
		{
			return;
		}
		this._players.Remove(basePlayer);
	}

	// Token: 0x0400143B RID: 5179
	public float triggerSize;

	// Token: 0x0400143C RID: 5180
	public float baseComfort = 0.5f;

	// Token: 0x0400143D RID: 5181
	public float minComfortRange = 2.5f;

	// Token: 0x0400143E RID: 5182
	private const float perPlayerComfortBonus = 0.25f;

	// Token: 0x0400143F RID: 5183
	private const float bonusComfort = 0f;

	// Token: 0x04001440 RID: 5184
	private List<global::BasePlayer> _players = new List<global::BasePlayer>();
}
