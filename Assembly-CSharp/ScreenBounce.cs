﻿using System;
using UnityEngine;

// Token: 0x02000333 RID: 819
public class ScreenBounce : global::BaseScreenShake
{
	// Token: 0x060013C2 RID: 5058 RVA: 0x00073C78 File Offset: 0x00071E78
	public override void Setup()
	{
		this.bounceTime = Random.Range(0f, 1000f);
	}

	// Token: 0x060013C3 RID: 5059 RVA: 0x00073C90 File Offset: 0x00071E90
	public override void Run(float delta, ref global::CachedTransform<Camera> cam, ref global::CachedTransform<global::BaseViewModel> vm)
	{
		this.bounceTime += Time.deltaTime * this.bounceSpeed.Evaluate(delta);
		float num = this.bounceScale.Evaluate(delta) * 0.1f;
		this.bounceVelocity.x = Mathf.Sin(this.bounceTime * 20f) * num;
		this.bounceVelocity.y = Mathf.Cos(this.bounceTime * 25f) * num;
		this.bounceVelocity.z = 0f;
		Vector3 vector = Vector3.zero;
		vector += this.bounceVelocity.x * cam.right;
		vector += this.bounceVelocity.y * cam.up;
		if (cam)
		{
			cam.position += vector;
		}
		if (vm)
		{
			vm.position += vector * -1f * this.bounceViewmodel.Evaluate(delta);
		}
	}

	// Token: 0x04000EA7 RID: 3751
	public AnimationCurve bounceScale;

	// Token: 0x04000EA8 RID: 3752
	public AnimationCurve bounceSpeed;

	// Token: 0x04000EA9 RID: 3753
	public AnimationCurve bounceViewmodel;

	// Token: 0x04000EAA RID: 3754
	private float bounceTime;

	// Token: 0x04000EAB RID: 3755
	private Vector3 bounceVelocity = Vector3.zero;
}
