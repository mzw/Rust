﻿using System;
using System.Collections.Generic;

// Token: 0x0200052F RID: 1327
public static class TerrainSplat
{
	// Token: 0x06001C03 RID: 7171 RVA: 0x0009DF7C File Offset: 0x0009C17C
	public static int TypeToIndex(int id)
	{
		return global::TerrainSplat.type2index[id];
	}

	// Token: 0x06001C04 RID: 7172 RVA: 0x0009DF8C File Offset: 0x0009C18C
	public static int IndexToType(int idx)
	{
		return 1 << idx;
	}

	// Token: 0x040016D4 RID: 5844
	public const int COUNT = 8;

	// Token: 0x040016D5 RID: 5845
	public const int EVERYTHING = -1;

	// Token: 0x040016D6 RID: 5846
	public const int NOTHING = 0;

	// Token: 0x040016D7 RID: 5847
	public const int DIRT = 1;

	// Token: 0x040016D8 RID: 5848
	public const int SNOW = 2;

	// Token: 0x040016D9 RID: 5849
	public const int SAND = 4;

	// Token: 0x040016DA RID: 5850
	public const int ROCK = 8;

	// Token: 0x040016DB RID: 5851
	public const int GRASS = 16;

	// Token: 0x040016DC RID: 5852
	public const int FOREST = 32;

	// Token: 0x040016DD RID: 5853
	public const int STONES = 64;

	// Token: 0x040016DE RID: 5854
	public const int GRAVEL = 128;

	// Token: 0x040016DF RID: 5855
	public const int DIRT_IDX = 0;

	// Token: 0x040016E0 RID: 5856
	public const int SNOW_IDX = 1;

	// Token: 0x040016E1 RID: 5857
	public const int SAND_IDX = 2;

	// Token: 0x040016E2 RID: 5858
	public const int ROCK_IDX = 3;

	// Token: 0x040016E3 RID: 5859
	public const int GRASS_IDX = 4;

	// Token: 0x040016E4 RID: 5860
	public const int FOREST_IDX = 5;

	// Token: 0x040016E5 RID: 5861
	public const int STONES_IDX = 6;

	// Token: 0x040016E6 RID: 5862
	public const int GRAVEL_IDX = 7;

	// Token: 0x040016E7 RID: 5863
	private static Dictionary<int, int> type2index = new Dictionary<int, int>
	{
		{
			8,
			3
		},
		{
			16,
			4
		},
		{
			4,
			2
		},
		{
			1,
			0
		},
		{
			32,
			5
		},
		{
			64,
			6
		},
		{
			2,
			1
		},
		{
			128,
			7
		}
	};

	// Token: 0x02000530 RID: 1328
	public enum Enum
	{
		// Token: 0x040016E9 RID: 5865
		Dirt = 1,
		// Token: 0x040016EA RID: 5866
		Snow,
		// Token: 0x040016EB RID: 5867
		Sand = 4,
		// Token: 0x040016EC RID: 5868
		Rock = 8,
		// Token: 0x040016ED RID: 5869
		Grass = 16,
		// Token: 0x040016EE RID: 5870
		Forest = 32,
		// Token: 0x040016EF RID: 5871
		Stones = 64,
		// Token: 0x040016F0 RID: 5872
		Gravel = 128
	}
}
