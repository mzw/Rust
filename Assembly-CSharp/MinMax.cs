﻿using System;
using UnityEngine;

// Token: 0x02000739 RID: 1849
[Serializable]
public class MinMax
{
	// Token: 0x060022C4 RID: 8900 RVA: 0x000C1864 File Offset: 0x000BFA64
	public MinMax(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	// Token: 0x060022C5 RID: 8901 RVA: 0x000C1888 File Offset: 0x000BFA88
	public float Random()
	{
		return UnityEngine.Random.Range(this.x, this.y);
	}

	// Token: 0x060022C6 RID: 8902 RVA: 0x000C189C File Offset: 0x000BFA9C
	public float Lerp(float t)
	{
		return Mathf.Lerp(this.x, this.y, t);
	}

	// Token: 0x060022C7 RID: 8903 RVA: 0x000C18B0 File Offset: 0x000BFAB0
	public float Lerp(float a, float b, float t)
	{
		return Mathf.Lerp(this.x, this.y, Mathf.InverseLerp(a, b, t));
	}

	// Token: 0x04001F39 RID: 7993
	public float x;

	// Token: 0x04001F3A RID: 7994
	public float y = 1f;
}
