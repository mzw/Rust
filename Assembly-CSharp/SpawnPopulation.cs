﻿using System;
using System.Linq;
using ConVar;
using UnityEngine;
using UnityEngine.Serialization;

// Token: 0x02000488 RID: 1160
[CreateAssetMenu(menuName = "Rust/Spawn Population")]
public class SpawnPopulation : global::BaseScriptableObject
{
	// Token: 0x170001BB RID: 443
	// (get) Token: 0x06001936 RID: 6454 RVA: 0x0008E314 File Offset: 0x0008C514
	public virtual float TargetDensity
	{
		get
		{
			return this._targetDensity;
		}
	}

	// Token: 0x06001937 RID: 6455 RVA: 0x0008E31C File Offset: 0x0008C51C
	public bool Initialize()
	{
		if (this.Prefabs == null || this.Prefabs.Length == 0)
		{
			if (!string.IsNullOrEmpty(this.ResourceFolder))
			{
				this.Prefabs = global::Prefab.Load<global::Spawnable>("assets/bundled/prefabs/autospawn/" + this.ResourceFolder, global::GameManager.server, global::PrefabAttribute.server, false);
			}
			if (this.ResourceList != null && this.ResourceList.Length > 0)
			{
				this.Prefabs = global::Prefab.Load<global::Spawnable>((from x in this.ResourceList
				select x.resourcePath).ToArray<string>(), global::GameManager.server, global::PrefabAttribute.server);
			}
			if (this.Prefabs == null || this.Prefabs.Length == 0)
			{
				return false;
			}
			this.numToSpawn = new int[this.Prefabs.Length];
		}
		return true;
	}

	// Token: 0x06001938 RID: 6456 RVA: 0x0008E400 File Offset: 0x0008C600
	public void UpdateWeights(global::SpawnDistribution distribution, int targetCount)
	{
		int num = 0;
		for (int i = 0; i < this.Prefabs.Length; i++)
		{
			global::Prefab<global::Spawnable> prefab = this.Prefabs[i];
			int num2 = (!prefab.Parameters) ? 1 : prefab.Parameters.Count;
			num += num2;
		}
		int num3 = Mathf.CeilToInt((float)targetCount / (float)num);
		this.sumToSpawn = 0;
		for (int j = 0; j < this.Prefabs.Length; j++)
		{
			global::Prefab<global::Spawnable> prefab2 = this.Prefabs[j];
			int num4 = (!prefab2.Parameters) ? 1 : prefab2.Parameters.Count;
			int count = distribution.GetCount(prefab2.ID);
			int num5 = Mathf.Max(num4 * num3 - count, 0);
			this.numToSpawn[j] = num5;
			this.sumToSpawn += num5;
		}
	}

	// Token: 0x06001939 RID: 6457 RVA: 0x0008E4F0 File Offset: 0x0008C6F0
	public global::Prefab<global::Spawnable> GetRandomPrefab()
	{
		int num = Random.Range(0, this.sumToSpawn);
		for (int i = 0; i < this.Prefabs.Length; i++)
		{
			if ((num -= this.numToSpawn[i]) < 0)
			{
				this.numToSpawn[i]--;
				this.sumToSpawn--;
				return this.Prefabs[i];
			}
		}
		return null;
	}

	// Token: 0x0600193A RID: 6458 RVA: 0x0008E560 File Offset: 0x0008C760
	public float GetCurrentSpawnRate()
	{
		if (this.ScaleWithServerPopulation)
		{
			return this.SpawnRate * global::SpawnHandler.PlayerLerp(ConVar.Spawn.min_rate, ConVar.Spawn.max_rate);
		}
		return this.SpawnRate * ConVar.Spawn.max_rate;
	}

	// Token: 0x0600193B RID: 6459 RVA: 0x0008E590 File Offset: 0x0008C790
	public float GetCurrentSpawnDensity()
	{
		if (this.ScaleWithServerPopulation)
		{
			return this.TargetDensity * global::SpawnHandler.PlayerLerp(ConVar.Spawn.min_density, ConVar.Spawn.max_density) * 1E-06f;
		}
		return this.TargetDensity * ConVar.Spawn.max_density * 1E-06f;
	}

	// Token: 0x0600193C RID: 6460 RVA: 0x0008E5CC File Offset: 0x0008C7CC
	public float GetMaximumSpawnDensity()
	{
		if (this.ScaleWithServerPopulation)
		{
			return 2f * this.TargetDensity * global::SpawnHandler.PlayerLerp(ConVar.Spawn.min_density, ConVar.Spawn.max_density) * 1E-06f;
		}
		return 2f * this.TargetDensity * ConVar.Spawn.max_density * 1E-06f;
	}

	// Token: 0x040013E6 RID: 5094
	[Header("Spawnables")]
	public string ResourceFolder = string.Empty;

	// Token: 0x040013E7 RID: 5095
	public global::GameObjectRef[] ResourceList;

	// Token: 0x040013E8 RID: 5096
	[Header("Spawn Info")]
	[Tooltip("Usually per square km")]
	[SerializeField]
	[FormerlySerializedAs("TargetDensity")]
	private float _targetDensity = 1f;

	// Token: 0x040013E9 RID: 5097
	public float SpawnRate = 1f;

	// Token: 0x040013EA RID: 5098
	public int ClusterSizeMin = 1;

	// Token: 0x040013EB RID: 5099
	public int ClusterSizeMax = 1;

	// Token: 0x040013EC RID: 5100
	public int ClusterDithering;

	// Token: 0x040013ED RID: 5101
	public int SpawnAttemptsInitial = 20;

	// Token: 0x040013EE RID: 5102
	public int SpawnAttemptsRepeating = 10;

	// Token: 0x040013EF RID: 5103
	public bool EnforcePopulationLimits = true;

	// Token: 0x040013F0 RID: 5104
	public bool ScaleWithSpawnFilter = true;

	// Token: 0x040013F1 RID: 5105
	public bool ScaleWithServerPopulation;

	// Token: 0x040013F2 RID: 5106
	public bool AlignToNormal;

	// Token: 0x040013F3 RID: 5107
	public global::SpawnFilter Filter = new global::SpawnFilter();

	// Token: 0x040013F4 RID: 5108
	internal global::Prefab<global::Spawnable>[] Prefabs;

	// Token: 0x040013F5 RID: 5109
	private int[] numToSpawn;

	// Token: 0x040013F6 RID: 5110
	private int sumToSpawn;
}
