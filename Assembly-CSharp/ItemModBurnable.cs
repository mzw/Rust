﻿using System;

// Token: 0x020004D9 RID: 1241
public class ItemModBurnable : global::ItemMod
{
	// Token: 0x06001AB8 RID: 6840 RVA: 0x00095BFC File Offset: 0x00093DFC
	public override void OnItemCreated(global::Item item)
	{
		item.fuel = this.fuelAmount;
	}

	// Token: 0x04001577 RID: 5495
	public float fuelAmount = 10f;

	// Token: 0x04001578 RID: 5496
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition byproductItem;

	// Token: 0x04001579 RID: 5497
	public int byproductAmount = 1;

	// Token: 0x0400157A RID: 5498
	public float byproductChance = 0.5f;
}
