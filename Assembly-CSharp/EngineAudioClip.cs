﻿using System;
using System.Collections.Generic;
using JSON;
using UnityEngine;

// Token: 0x020001D6 RID: 470
public class EngineAudioClip : MonoBehaviour, IClientComponent
{
	// Token: 0x06000F00 RID: 3840 RVA: 0x0005C7C0 File Offset: 0x0005A9C0
	private int GetBucketRPM(int RPM)
	{
		return Mathf.RoundToInt((float)(RPM / 25)) * 25;
	}

	// Token: 0x04000900 RID: 2304
	public AudioClip granularClip;

	// Token: 0x04000901 RID: 2305
	public AudioClip accelerationClip;

	// Token: 0x04000902 RID: 2306
	public TextAsset accelerationCyclesJson;

	// Token: 0x04000903 RID: 2307
	public List<global::EngineAudioClip.EngineCycle> accelerationCycles = new List<global::EngineAudioClip.EngineCycle>();

	// Token: 0x04000904 RID: 2308
	public List<global::EngineAudioClip.EngineCycleBucket> cycleBuckets = new List<global::EngineAudioClip.EngineCycleBucket>();

	// Token: 0x04000905 RID: 2309
	public Dictionary<int, global::EngineAudioClip.EngineCycleBucket> accelerationCyclesByRPM = new Dictionary<int, global::EngineAudioClip.EngineCycleBucket>();

	// Token: 0x04000906 RID: 2310
	public Dictionary<int, int> rpmBucketLookup = new Dictionary<int, int>();

	// Token: 0x04000907 RID: 2311
	public int sampleRate = 44100;

	// Token: 0x04000908 RID: 2312
	public int samplesUntilNextGrain;

	// Token: 0x04000909 RID: 2313
	public int lastCycleId;

	// Token: 0x0400090A RID: 2314
	public List<global::EngineAudioClip.Grain> grains = new List<global::EngineAudioClip.Grain>();

	// Token: 0x0400090B RID: 2315
	public int currentRPM;

	// Token: 0x0400090C RID: 2316
	public int targetRPM = 1500;

	// Token: 0x0400090D RID: 2317
	public int minRPM;

	// Token: 0x0400090E RID: 2318
	public int maxRPM;

	// Token: 0x0400090F RID: 2319
	public int cyclePadding;

	// Token: 0x04000910 RID: 2320
	[Range(0f, 1f)]
	public float RPMControl;

	// Token: 0x04000911 RID: 2321
	public AudioSource source;

	// Token: 0x020001D7 RID: 471
	[Serializable]
	public class EngineCycle
	{
		// Token: 0x06000F01 RID: 3841 RVA: 0x0005C7D0 File Offset: 0x0005A9D0
		public EngineCycle(int RPM, int startSample, int endSample, float period, int id)
		{
			this.RPM = RPM;
			this.startSample = startSample;
			this.endSample = endSample;
			this.period = period;
			this.id = id;
		}

		// Token: 0x04000912 RID: 2322
		public int RPM;

		// Token: 0x04000913 RID: 2323
		public int startSample;

		// Token: 0x04000914 RID: 2324
		public int endSample;

		// Token: 0x04000915 RID: 2325
		public float period;

		// Token: 0x04000916 RID: 2326
		public int id;
	}

	// Token: 0x020001D8 RID: 472
	public class EngineCycleBucket
	{
		// Token: 0x06000F02 RID: 3842 RVA: 0x0005C800 File Offset: 0x0005AA00
		public EngineCycleBucket(int RPM)
		{
			this.RPM = RPM;
		}

		// Token: 0x06000F03 RID: 3843 RVA: 0x0005C828 File Offset: 0x0005AA28
		public global::EngineAudioClip.EngineCycle GetCycle(Random random, int lastCycleId)
		{
			if (this.remainingCycles.Count == 0)
			{
				this.ResetRemainingCycles(random);
			}
			int index = Extensions.Pop<int>(this.remainingCycles);
			if (this.cycles[index].id == lastCycleId)
			{
				if (this.remainingCycles.Count == 0)
				{
					this.ResetRemainingCycles(random);
				}
				index = Extensions.Pop<int>(this.remainingCycles);
			}
			return this.cycles[index];
		}

		// Token: 0x06000F04 RID: 3844 RVA: 0x0005C8A0 File Offset: 0x0005AAA0
		private void ResetRemainingCycles(Random random)
		{
			for (int i = 0; i < this.cycles.Count; i++)
			{
				this.remainingCycles.Add(i);
			}
			ListEx.Shuffle<int>(this.remainingCycles, (uint)random.Next());
		}

		// Token: 0x06000F05 RID: 3845 RVA: 0x0005C8E8 File Offset: 0x0005AAE8
		public void Add(global::EngineAudioClip.EngineCycle cycle)
		{
			if (!this.cycles.Contains(cycle))
			{
				this.cycles.Add(cycle);
			}
		}

		// Token: 0x04000917 RID: 2327
		public int RPM;

		// Token: 0x04000918 RID: 2328
		public List<global::EngineAudioClip.EngineCycle> cycles = new List<global::EngineAudioClip.EngineCycle>();

		// Token: 0x04000919 RID: 2329
		public List<int> remainingCycles = new List<int>();
	}

	// Token: 0x020001D9 RID: 473
	public class Grain
	{
		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000F07 RID: 3847 RVA: 0x0005C910 File Offset: 0x0005AB10
		public bool finished
		{
			get
			{
				return this.currentSample >= this.endSample;
			}
		}

		// Token: 0x06000F08 RID: 3848 RVA: 0x0005C924 File Offset: 0x0005AB24
		public void Init(float[] source, global::EngineAudioClip.EngineCycle cycle, int cyclePadding)
		{
			this.sourceData = source;
			this.startSample = cycle.startSample - cyclePadding;
			this.currentSample = this.startSample;
			this.attackTimeSamples = cyclePadding;
			this.sustainTimeSamples = cycle.endSample - cycle.startSample;
			this.releaseTimeSamples = cyclePadding;
			this.gainPerSampleAttack = 1f / (float)this.attackTimeSamples;
			this.gainPerSampleRelease = -1f / (float)this.releaseTimeSamples;
			this.attackEndSample = this.startSample + this.attackTimeSamples;
			this.releaseStartSample = this.attackEndSample + this.sustainTimeSamples;
			this.endSample = this.releaseStartSample + this.releaseTimeSamples;
			this.gain = 0f;
		}

		// Token: 0x06000F09 RID: 3849 RVA: 0x0005C9E0 File Offset: 0x0005ABE0
		public float GetSample()
		{
			if (this.currentSample >= this.sourceData.Length)
			{
				return 0f;
			}
			float num = this.sourceData[this.currentSample];
			if (this.currentSample <= this.attackEndSample)
			{
				this.gain += this.gainPerSampleAttack;
				if (this.gain > 0.8f)
				{
					this.gain = 0.8f;
				}
			}
			else if (this.currentSample >= this.releaseStartSample)
			{
				this.gain += this.gainPerSampleRelease;
				if (this.gain < 0f)
				{
					this.gain = 0f;
				}
			}
			this.currentSample++;
			return num * this.gain;
		}

		// Token: 0x0400091A RID: 2330
		private float[] sourceData;

		// Token: 0x0400091B RID: 2331
		private int startSample;

		// Token: 0x0400091C RID: 2332
		private int currentSample;

		// Token: 0x0400091D RID: 2333
		private int attackTimeSamples;

		// Token: 0x0400091E RID: 2334
		private int sustainTimeSamples;

		// Token: 0x0400091F RID: 2335
		private int releaseTimeSamples;

		// Token: 0x04000920 RID: 2336
		private float gain;

		// Token: 0x04000921 RID: 2337
		private float gainPerSampleAttack;

		// Token: 0x04000922 RID: 2338
		private float gainPerSampleRelease;

		// Token: 0x04000923 RID: 2339
		private int attackEndSample;

		// Token: 0x04000924 RID: 2340
		private int releaseStartSample;

		// Token: 0x04000925 RID: 2341
		private int endSample;
	}
}
