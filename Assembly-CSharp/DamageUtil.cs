﻿using System;
using System.Collections.Generic;
using Facepunch;
using Rust;
using UnityEngine;

// Token: 0x020002EB RID: 747
public static class DamageUtil
{
	// Token: 0x060012E5 RID: 4837 RVA: 0x0006EA50 File Offset: 0x0006CC50
	public static void RadiusDamage(global::BaseEntity attackingPlayer, global::BaseEntity weaponPrefab, Vector3 pos, float minradius, float radius, List<Rust.DamageTypeEntry> damage, int layers, bool useLineOfSight)
	{
		using (TimeWarning.New("DamageUtil.RadiusDamage", 0.1f))
		{
			List<global::HitInfo> list = Pool.GetList<global::HitInfo>();
			List<global::BaseEntity> list2 = Pool.GetList<global::BaseEntity>();
			List<global::BaseEntity> list3 = Pool.GetList<global::BaseEntity>();
			global::Vis.Entities<global::BaseEntity>(pos, radius, list3, layers, 2);
			for (int i = 0; i < list3.Count; i++)
			{
				global::BaseEntity baseEntity = list3[i];
				if (baseEntity.isServer)
				{
					if (!list2.Contains(baseEntity))
					{
						Vector3 vector = baseEntity.ClosestPoint(pos);
						float num = Vector3.Distance(vector, pos);
						float num2 = Mathf.Clamp01((num - minradius) / (radius - minradius));
						if (num2 <= 1f)
						{
							float amount = 1f - num2;
							if (!useLineOfSight || baseEntity.IsVisible(pos))
							{
								global::HitInfo hitInfo = new global::HitInfo();
								hitInfo.Initiator = attackingPlayer;
								hitInfo.WeaponPrefab = weaponPrefab;
								hitInfo.damageTypes.Add(damage);
								hitInfo.damageTypes.ScaleAll(amount);
								hitInfo.HitPositionWorld = vector;
								hitInfo.HitNormalWorld = (pos - vector).normalized;
								hitInfo.PointStart = pos;
								hitInfo.PointEnd = hitInfo.HitPositionWorld;
								list.Add(hitInfo);
								list2.Add(baseEntity);
							}
						}
					}
				}
			}
			for (int j = 0; j < list2.Count; j++)
			{
				global::BaseEntity baseEntity2 = list2[j];
				global::HitInfo info = list[j];
				baseEntity2.OnAttacked(info);
			}
			Pool.FreeList<global::BaseEntity>(ref list2);
			Pool.FreeList<global::BaseEntity>(ref list3);
		}
	}
}
