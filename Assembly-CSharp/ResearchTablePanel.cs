﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C4 RID: 1732
public class ResearchTablePanel : global::LootPanel
{
	// Token: 0x04001D52 RID: 7506
	public Button researchButton;

	// Token: 0x04001D53 RID: 7507
	public Text timerText;

	// Token: 0x04001D54 RID: 7508
	public GameObject itemDescNoItem;

	// Token: 0x04001D55 RID: 7509
	public GameObject itemDescTooBroken;

	// Token: 0x04001D56 RID: 7510
	public GameObject itemDescNotResearchable;

	// Token: 0x04001D57 RID: 7511
	public GameObject itemDescTooMany;

	// Token: 0x04001D58 RID: 7512
	public GameObject itemTakeBlueprint;

	// Token: 0x04001D59 RID: 7513
	public Text successChanceText;

	// Token: 0x04001D5A RID: 7514
	public global::ItemIcon scrapIcon;

	// Token: 0x04001D5B RID: 7515
	[NonSerialized]
	public bool wasResearching;

	// Token: 0x04001D5C RID: 7516
	public GameObject[] workbenchReqs;
}
