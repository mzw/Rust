﻿using System;
using UnityEngine;

// Token: 0x020007EE RID: 2030
[AddComponentMenu("Image Effects/FXAA")]
public class FXAA : global::FXAAPostEffectsBase, IImageEffect
{
	// Token: 0x0600257E RID: 9598 RVA: 0x000CF494 File Offset: 0x000CD694
	private void CreateMaterials()
	{
		if (this.mat == null)
		{
			this.mat = base.CheckShaderAndCreateMaterial(this.shader, this.mat);
		}
	}

	// Token: 0x0600257F RID: 9599 RVA: 0x000CF4C0 File Offset: 0x000CD6C0
	private void Start()
	{
		this.CreateMaterials();
		base.CheckSupport(false);
	}

	// Token: 0x06002580 RID: 9600 RVA: 0x000CF4D0 File Offset: 0x000CD6D0
	public bool IsActive()
	{
		return base.enabled;
	}

	// Token: 0x06002581 RID: 9601 RVA: 0x000CF4D8 File Offset: 0x000CD6D8
	public void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		this.CreateMaterials();
		float num = 1f / (float)Screen.width;
		float num2 = 1f / (float)Screen.height;
		this.mat.SetVector("_rcpFrame", new Vector4(num, num2, 0f, 0f));
		this.mat.SetVector("_rcpFrameOpt", new Vector4(num * 2f, num2 * 2f, num * 0.5f, num2 * 0.5f));
		Graphics.Blit(source, destination, this.mat);
	}

	// Token: 0x040021A2 RID: 8610
	public Shader shader;

	// Token: 0x040021A3 RID: 8611
	private Material mat;
}
