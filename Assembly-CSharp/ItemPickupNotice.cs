﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006B0 RID: 1712
public class ItemPickupNotice : MonoBehaviour
{
	// Token: 0x17000247 RID: 583
	// (set) Token: 0x06002148 RID: 8520 RVA: 0x000BBEE0 File Offset: 0x000BA0E0
	public global::ItemDefinition itemInfo
	{
		set
		{
			this.Text.text = value.displayName.translated;
		}
	}

	// Token: 0x17000248 RID: 584
	// (set) Token: 0x06002149 RID: 8521 RVA: 0x000BBEF8 File Offset: 0x000BA0F8
	public int amount
	{
		set
		{
			this.Amount.text = ((value <= 0) ? value.ToString("0") : value.ToString("+0"));
		}
	}

	// Token: 0x0600214A RID: 8522 RVA: 0x000BBF2C File Offset: 0x000BA12C
	public void PopupNoticeEnd()
	{
		global::GameManager.Destroy(this.objectDeleteOnFinish, 0f);
	}

	// Token: 0x04001D09 RID: 7433
	public GameObject objectDeleteOnFinish;

	// Token: 0x04001D0A RID: 7434
	public Text Text;

	// Token: 0x04001D0B RID: 7435
	public Text Amount;
}
