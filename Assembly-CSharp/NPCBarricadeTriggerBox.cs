﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x020001BC RID: 444
public class NPCBarricadeTriggerBox : MonoBehaviour
{
	// Token: 0x06000E84 RID: 3716 RVA: 0x0005B984 File Offset: 0x00059B84
	public void Setup(global::Barricade t)
	{
		this.target = t;
		base.transform.SetParent(this.target.transform, false);
		base.gameObject.layer = 18;
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		boxCollider.isTrigger = true;
		boxCollider.center = Vector3.zero;
		boxCollider.size = Vector3.one * ConVar.AI.npc_door_trigger_size + Vector3.right * this.target.bounds.size.x;
	}

	// Token: 0x06000E85 RID: 3717 RVA: 0x0005BA18 File Offset: 0x00059C18
	private void OnTriggerEnter(Collider other)
	{
		if (this.target == null || this.target.isClient)
		{
			return;
		}
		if (global::NPCBarricadeTriggerBox.playerServerLayer < 0)
		{
			global::NPCBarricadeTriggerBox.playerServerLayer = LayerMask.NameToLayer("Player (Server)");
		}
		if ((other.gameObject.layer & global::NPCBarricadeTriggerBox.playerServerLayer) > 0)
		{
			global::BasePlayer component = other.gameObject.GetComponent<global::BasePlayer>();
			if (component != null && component is global::NPCPlayer)
			{
				this.target.Kill(global::BaseNetworkable.DestroyMode.Gib);
			}
		}
	}

	// Token: 0x0400089F RID: 2207
	private global::Barricade target;

	// Token: 0x040008A0 RID: 2208
	private static int playerServerLayer = -1;
}
