﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200008A RID: 138
public class Recycler : global::StorageContainer
{
	// Token: 0x0600091D RID: 2333 RVA: 0x0003D458 File Offset: 0x0003B658
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Recycler.OnRpcMessage", 0.1f))
		{
			if (rpc == 3611438935u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SVSwitch ");
				}
				using (TimeWarning.New("SVSwitch", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SVSwitch", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SVSwitch(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in SVSwitch");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600091E RID: 2334 RVA: 0x0003D638 File Offset: 0x0003B838
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void SVSwitch(global::BaseEntity.RPCMessage msg)
	{
		bool flag = msg.read.Bit();
		if (flag == base.IsOn())
		{
			return;
		}
		if (msg.player == null)
		{
			return;
		}
		if (Interface.CallHook("OnRecyclerToggle", new object[]
		{
			this,
			msg.player
		}) != null)
		{
			return;
		}
		if (flag && !this.HasRecyclable())
		{
			return;
		}
		if (flag)
		{
			foreach (global::Item item in this.inventory.itemList)
			{
				item.CollectedForCrafting(msg.player);
			}
			this.StartRecycling();
		}
		else
		{
			this.StopRecycling();
		}
	}

	// Token: 0x0600091F RID: 2335 RVA: 0x0003D718 File Offset: 0x0003B918
	public bool MoveItemToOutput(global::Item newItem)
	{
		int num = -1;
		for (int i = 6; i < 12; i++)
		{
			global::Item slot = this.inventory.GetSlot(i);
			if (slot == null)
			{
				num = i;
				break;
			}
			if (slot.CanStack(newItem))
			{
				if (slot.amount + newItem.amount <= slot.info.stackable)
				{
					num = i;
					break;
				}
				int num2 = Mathf.Min(slot.info.stackable - slot.amount, newItem.amount);
				newItem.UseItem(num2);
				slot.amount += num2;
				slot.MarkDirty();
				newItem.MarkDirty();
			}
			if (newItem.amount <= 0)
			{
				return true;
			}
		}
		if (num != -1 && newItem.MoveToContainer(this.inventory, num, true))
		{
			return true;
		}
		newItem.Drop(base.transform.position + new Vector3(0f, 2f, 0f), base.transform.forward * 2f, default(Quaternion));
		return false;
	}

	// Token: 0x06000920 RID: 2336 RVA: 0x0003D838 File Offset: 0x0003BA38
	public bool HasRecyclable()
	{
		for (int i = 0; i < 6; i++)
		{
			global::Item slot = this.inventory.GetSlot(i);
			if (slot != null)
			{
				object obj = Interface.CallHook("CanRecycle", new object[]
				{
					this,
					slot
				});
				if (obj is bool)
				{
					return (bool)obj;
				}
				bool flag = slot.info.Blueprint != null;
				if (flag)
				{
					return true;
				}
			}
		}
		return false;
	}

	// Token: 0x06000921 RID: 2337 RVA: 0x0003D8B8 File Offset: 0x0003BAB8
	public void RecycleThink()
	{
		bool flag = false;
		float num = this.recycleEfficiency;
		for (int i = 0; i < 6; i++)
		{
			global::Item slot = this.inventory.GetSlot(i);
			if (slot != null)
			{
				if (Interface.CallHook("OnRecycleItem", new object[]
				{
					this,
					slot
				}) != null)
				{
					if (!this.HasRecyclable())
					{
						this.StopRecycling();
					}
					return;
				}
				if (slot.info.Blueprint != null)
				{
					if (slot.hasCondition)
					{
						num = Mathf.Clamp01(num * Mathf.Clamp(slot.conditionNormalized * slot.maxConditionNormalized, 0.1f, 1f));
					}
					int num2 = 1;
					if (slot.amount > 1)
					{
						num2 = Mathf.CeilToInt(Mathf.Min((float)slot.amount, (float)slot.info.stackable * 0.1f));
					}
					if (slot.info.Blueprint.scrapFromRecycle > 0)
					{
						global::Item newItem = global::ItemManager.CreateByName("scrap", slot.info.Blueprint.scrapFromRecycle * num2, 0UL);
						this.MoveItemToOutput(newItem);
					}
					slot.UseItem(num2);
					foreach (global::ItemAmount itemAmount in slot.info.Blueprint.ingredients)
					{
						if (!(itemAmount.itemDef.shortname == "scrap"))
						{
							float num3 = itemAmount.amount / (float)slot.info.Blueprint.amountToCreate;
							int num4 = 0;
							if (num3 <= 1f)
							{
								for (int j = 0; j < num2; j++)
								{
									if (Random.Range(0f, 1f) <= num)
									{
										num4++;
									}
								}
							}
							else
							{
								num4 = Mathf.CeilToInt(Mathf.Clamp(num3 * num * Random.Range(1f, 1f), 0f, itemAmount.amount) * (float)num2);
							}
							if (num4 > 0)
							{
								int num5 = Mathf.CeilToInt((float)num4 / (float)itemAmount.itemDef.stackable);
								for (int k = 0; k < num5; k++)
								{
									int num6 = (num4 <= itemAmount.itemDef.stackable) ? num4 : itemAmount.itemDef.stackable;
									global::Item newItem2 = global::ItemManager.Create(itemAmount.itemDef, num6, 0UL);
									if (!this.MoveItemToOutput(newItem2))
									{
										flag = true;
									}
									num4 -= num6;
									if (num4 <= 0)
									{
										break;
									}
								}
							}
						}
					}
					break;
				}
			}
		}
		if (flag || !this.HasRecyclable())
		{
			this.StopRecycling();
		}
	}

	// Token: 0x06000922 RID: 2338 RVA: 0x0003DBBC File Offset: 0x0003BDBC
	public void StartRecycling()
	{
		if (base.IsOn())
		{
			return;
		}
		base.InvokeRepeating(new Action(this.RecycleThink), 5f, 5f);
		global::Effect.server.Run(this.startSound.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000923 RID: 2339 RVA: 0x0003DC20 File Offset: 0x0003BE20
	public void StopRecycling()
	{
		base.CancelInvoke(new Action(this.RecycleThink));
		if (!base.IsOn())
		{
			return;
		}
		global::Effect.server.Run(this.stopSound.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000924 RID: 2340 RVA: 0x0003DC7C File Offset: 0x0003BE7C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04000435 RID: 1077
	public float recycleEfficiency = 0.5f;

	// Token: 0x04000436 RID: 1078
	public global::SoundDefinition grindingLoopDef;

	// Token: 0x04000437 RID: 1079
	public global::GameObjectRef startSound;

	// Token: 0x04000438 RID: 1080
	public global::GameObjectRef stopSound;
}
