﻿using System;

// Token: 0x020004DA RID: 1242
public class ItemModConditionContainerFlag : global::ItemMod
{
	// Token: 0x06001ABA RID: 6842 RVA: 0x00095C14 File Offset: 0x00093E14
	public override bool Passes(global::Item item)
	{
		if (item.parent == null)
		{
			return !this.requiredState;
		}
		if (!item.parent.HasFlag(this.flag))
		{
			return !this.requiredState;
		}
		return this.requiredState;
	}

	// Token: 0x0400157B RID: 5499
	public global::ItemContainer.Flag flag;

	// Token: 0x0400157C RID: 5500
	public bool requiredState;
}
