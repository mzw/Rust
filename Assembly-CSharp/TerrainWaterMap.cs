﻿using System;
using UnityEngine;

// Token: 0x02000584 RID: 1412
public class TerrainWaterMap : global::TerrainMap2D<short>
{
	// Token: 0x06001DB3 RID: 7603 RVA: 0x000A5F5C File Offset: 0x000A415C
	public override void Setup()
	{
		if (this.WaterTexture != null)
		{
			if (this.WaterTexture.width == this.WaterTexture.height)
			{
				this.res = this.WaterTexture.width;
				this.src = (this.dst = new short[this.res, this.res]);
				Color32[] pixels = this.WaterTexture.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						Color32 c = pixels[num];
						this.dst[i, j] = global::TextureData.DecodeShort(c);
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid water texture: " + this.WaterTexture.name);
			}
		}
		else
		{
			this.res = this.terrain.terrainData.heightmapResolution;
			this.src = (this.dst = new short[this.res, this.res]);
		}
		this.normY = global::TerrainMeta.Size.x / global::TerrainMeta.Size.y / (float)this.res;
	}

	// Token: 0x06001DB4 RID: 7604 RVA: 0x000A60B4 File Offset: 0x000A42B4
	public void GenerateTextures()
	{
		Color32[] heights = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				heights[z * this.res + i] = global::TextureData.EncodeShort(this.src[z, i]);
			}
		});
		this.WaterTexture = new Texture2D(this.res, this.res, 4, true, true);
		this.WaterTexture.name = "WaterTexture";
		this.WaterTexture.wrapMode = 1;
		this.WaterTexture.SetPixels32(heights);
	}

	// Token: 0x06001DB5 RID: 7605 RVA: 0x000A6148 File Offset: 0x000A4348
	public void ApplyTextures()
	{
		this.WaterTexture.Apply(true, true);
	}

	// Token: 0x06001DB6 RID: 7606 RVA: 0x000A6158 File Offset: 0x000A4358
	public float GetHeight(Vector3 worldPos)
	{
		return global::TerrainMeta.Position.y + this.GetHeight01(worldPos) * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001DB7 RID: 7607 RVA: 0x000A6188 File Offset: 0x000A4388
	public float GetHeight(float normX, float normZ)
	{
		return global::TerrainMeta.Position.y + this.GetHeight01(normX, normZ) * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001DB8 RID: 7608 RVA: 0x000A61BC File Offset: 0x000A43BC
	public float GetHeight(int x, int z)
	{
		return global::TerrainMeta.Position.y + this.GetHeight01(x, z) * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001DB9 RID: 7609 RVA: 0x000A61F0 File Offset: 0x000A43F0
	public float GetHeight01(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetHeight01(normX, normZ);
	}

	// Token: 0x06001DBA RID: 7610 RVA: 0x000A6220 File Offset: 0x000A4420
	public float GetHeight01(float normX, float normZ)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		float num6 = Mathf.Lerp(this.GetHeight01(num4, num5), this.GetHeight01(x, num5), num2 - (float)num4);
		float num7 = Mathf.Lerp(this.GetHeight01(num4, z), this.GetHeight01(x, z), num2 - (float)num4);
		return Mathf.Lerp(num6, num7, num3 - (float)num5);
	}

	// Token: 0x06001DBB RID: 7611 RVA: 0x000A62B8 File Offset: 0x000A44B8
	public float GetHeight01(int x, int z)
	{
		return global::TextureData.Short2Float((int)this.src[z, x]);
	}

	// Token: 0x06001DBC RID: 7612 RVA: 0x000A62CC File Offset: 0x000A44CC
	public Vector3 GetNormal(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetNormal(normX, normZ);
	}

	// Token: 0x06001DBD RID: 7613 RVA: 0x000A62FC File Offset: 0x000A44FC
	public Vector3 GetNormal(float normX, float normZ)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		float num6 = this.GetHeight01(x, num5) - this.GetHeight01(num4, num5);
		float num7 = this.GetHeight01(num4, z) - this.GetHeight01(num4, num5);
		Vector3 vector;
		vector..ctor(-num6, this.normY, -num7);
		return vector.normalized;
	}

	// Token: 0x06001DBE RID: 7614 RVA: 0x000A6390 File Offset: 0x000A4590
	public Vector3 GetNormal(int x, int z)
	{
		int num = this.res - 1;
		int x2 = Mathf.Clamp(x - 1, 0, num);
		int z2 = Mathf.Clamp(z - 1, 0, num);
		int x3 = Mathf.Clamp(x + 1, 0, num);
		int z3 = Mathf.Clamp(z + 1, 0, num);
		float num2 = (this.GetHeight01(x3, z2) - this.GetHeight01(x2, z2)) * 0.5f;
		float num3 = (this.GetHeight01(x2, z3) - this.GetHeight01(x2, z2)) * 0.5f;
		Vector3 vector;
		vector..ctor(-num2, this.normY, -num3);
		return vector.normalized;
	}

	// Token: 0x06001DBF RID: 7615 RVA: 0x000A6420 File Offset: 0x000A4620
	public float GetSlope(Vector3 worldPos)
	{
		return Vector3.Angle(Vector3.up, this.GetNormal(worldPos));
	}

	// Token: 0x06001DC0 RID: 7616 RVA: 0x000A6434 File Offset: 0x000A4634
	public float GetSlope(float normX, float normZ)
	{
		return Vector3.Angle(Vector3.up, this.GetNormal(normX, normZ));
	}

	// Token: 0x06001DC1 RID: 7617 RVA: 0x000A6448 File Offset: 0x000A4648
	public float GetSlope(int x, int z)
	{
		return Vector3.Angle(Vector3.up, this.GetNormal(x, z));
	}

	// Token: 0x06001DC2 RID: 7618 RVA: 0x000A645C File Offset: 0x000A465C
	public float GetSlope01(Vector3 worldPos)
	{
		return this.GetSlope(worldPos) * 0.0111111114f;
	}

	// Token: 0x06001DC3 RID: 7619 RVA: 0x000A646C File Offset: 0x000A466C
	public float GetSlope01(float normX, float normZ)
	{
		return this.GetSlope(normX, normZ) * 0.0111111114f;
	}

	// Token: 0x06001DC4 RID: 7620 RVA: 0x000A647C File Offset: 0x000A467C
	public float GetSlope01(int x, int z)
	{
		return this.GetSlope(x, z) * 0.0111111114f;
	}

	// Token: 0x06001DC5 RID: 7621 RVA: 0x000A648C File Offset: 0x000A468C
	public float GetDepth(Vector3 worldPos)
	{
		return this.GetHeight(worldPos) - global::TerrainMeta.HeightMap.GetHeight(worldPos);
	}

	// Token: 0x06001DC6 RID: 7622 RVA: 0x000A64A4 File Offset: 0x000A46A4
	public float GetDepth(float normX, float normZ)
	{
		return this.GetHeight(normX, normZ) - global::TerrainMeta.HeightMap.GetHeight(normX, normZ);
	}

	// Token: 0x06001DC7 RID: 7623 RVA: 0x000A64BC File Offset: 0x000A46BC
	public void SetHeight(Vector3 worldPos, float height)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetHeight(normX, normZ, height);
	}

	// Token: 0x06001DC8 RID: 7624 RVA: 0x000A64EC File Offset: 0x000A46EC
	public void SetHeight(float normX, float normZ, float height)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetHeight(x, z, height);
	}

	// Token: 0x06001DC9 RID: 7625 RVA: 0x000A6514 File Offset: 0x000A4714
	public void SetHeight(int x, int z, float height)
	{
		this.dst[z, x] = global::TextureData.Float2Short(height);
	}

	// Token: 0x0400184F RID: 6223
	public Texture2D WaterTexture;

	// Token: 0x04001850 RID: 6224
	private float normY;
}
