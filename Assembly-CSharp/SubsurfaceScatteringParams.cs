﻿using System;

// Token: 0x020005FA RID: 1530
[Serializable]
public struct SubsurfaceScatteringParams
{
	// Token: 0x04001A37 RID: 6711
	public bool enabled;

	// Token: 0x04001A38 RID: 6712
	public global::SubsurfaceScatteringParams.Quality quality;

	// Token: 0x04001A39 RID: 6713
	public bool halfResolution;

	// Token: 0x04001A3A RID: 6714
	public float radiusScale;

	// Token: 0x04001A3B RID: 6715
	public static global::SubsurfaceScatteringParams Default = new global::SubsurfaceScatteringParams
	{
		enabled = true,
		quality = global::SubsurfaceScatteringParams.Quality.Medium,
		halfResolution = true,
		radiusScale = 1f
	};

	// Token: 0x020005FB RID: 1531
	public enum Quality
	{
		// Token: 0x04001A3D RID: 6717
		Low,
		// Token: 0x04001A3E RID: 6718
		Medium,
		// Token: 0x04001A3F RID: 6719
		High
	}
}
