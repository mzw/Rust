﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000058 RID: 88
public class BuildingPrivlidge : global::StorageContainer
{
	// Token: 0x0600070E RID: 1806 RVA: 0x0002C634 File Offset: 0x0002A834
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BuildingPrivlidge.OnRpcMessage", 0.1f))
		{
			if (rpc == 2017592092u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - AddSelfAuthorize ");
				}
				using (TimeWarning.New("AddSelfAuthorize", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("AddSelfAuthorize", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.AddSelfAuthorize(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in AddSelfAuthorize");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3024779371u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ClearList ");
				}
				using (TimeWarning.New("ClearList", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("ClearList", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ClearList(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in ClearList");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 2101914649u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RemoveSelfAuthorize ");
				}
				using (TimeWarning.New("RemoveSelfAuthorize", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RemoveSelfAuthorize", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RemoveSelfAuthorize(rpc4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in RemoveSelfAuthorize");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 1023681781u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Rotate ");
				}
				using (TimeWarning.New("RPC_Rotate", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_Rotate", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Rotate(msg2);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in RPC_Rotate");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600070F RID: 1807 RVA: 0x0002CCBC File Offset: 0x0002AEBC
	public override void ResetState()
	{
		base.ResetState();
		this.authorizedPlayers.Clear();
	}

	// Token: 0x06000710 RID: 1808 RVA: 0x0002CCD0 File Offset: 0x0002AED0
	public bool IsAuthed(global::BasePlayer player)
	{
		return this.authorizedPlayers.Any((PlayerNameID x) => x.userid == player.userID);
	}

	// Token: 0x06000711 RID: 1809 RVA: 0x0002CD04 File Offset: 0x0002AF04
	public bool AnyAuthed()
	{
		return this.authorizedPlayers.Count > 0;
	}

	// Token: 0x06000712 RID: 1810 RVA: 0x0002CD14 File Offset: 0x0002AF14
	public override bool ItemFilter(global::Item item, int targetSlot)
	{
		return base.ItemFilter(item, targetSlot);
	}

	// Token: 0x06000713 RID: 1811 RVA: 0x0002CD20 File Offset: 0x0002AF20
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.buildingPrivilege = Facepunch.Pool.Get<BuildingPrivilege>();
		info.msg.buildingPrivilege.users = this.authorizedPlayers;
		if (!info.forDisk)
		{
			info.msg.buildingPrivilege.upkeepPeriodMinutes = this.CalculateUpkeepPeriodMinutes();
			info.msg.buildingPrivilege.costFraction = this.CalculateUpkeepCostFraction();
			info.msg.buildingPrivilege.protectedMinutes = this.GetProtectedMinutes(false);
		}
	}

	// Token: 0x06000714 RID: 1812 RVA: 0x0002CDB0 File Offset: 0x0002AFB0
	public override void PostSave(global::BaseNetworkable.SaveInfo info)
	{
		info.msg.buildingPrivilege.users = null;
	}

	// Token: 0x06000715 RID: 1813 RVA: 0x0002CDC4 File Offset: 0x0002AFC4
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		this.authorizedPlayers.Clear();
		if (info.msg.buildingPrivilege != null && info.msg.buildingPrivilege.users != null)
		{
			this.authorizedPlayers = info.msg.buildingPrivilege.users;
			if (!info.fromDisk)
			{
				this.cachedProtectedMinutes = info.msg.buildingPrivilege.protectedMinutes;
			}
			info.msg.buildingPrivilege.users = null;
		}
	}

	// Token: 0x06000716 RID: 1814 RVA: 0x0002CE58 File Offset: 0x0002B058
	public void BuildingDirty()
	{
		if (base.isServer)
		{
			this.AddDelayedUpdate();
		}
	}

	// Token: 0x06000717 RID: 1815 RVA: 0x0002CE6C File Offset: 0x0002B06C
	protected override void OnInventoryDirty()
	{
		base.OnInventoryDirty();
		this.AddDelayedUpdate();
	}

	// Token: 0x06000718 RID: 1816 RVA: 0x0002CE7C File Offset: 0x0002B07C
	public override void OnItemAddedOrRemoved(global::Item item, bool bAdded)
	{
		base.OnItemAddedOrRemoved(item, bAdded);
		this.AddDelayedUpdate();
	}

	// Token: 0x06000719 RID: 1817 RVA: 0x0002CE8C File Offset: 0x0002B08C
	public void AddDelayedUpdate()
	{
		if (base.IsInvoking(new Action(this.DelayedUpdate)))
		{
			base.CancelInvoke(new Action(this.DelayedUpdate));
		}
		base.Invoke(new Action(this.DelayedUpdate), 1f);
	}

	// Token: 0x0600071A RID: 1818 RVA: 0x0002CEDC File Offset: 0x0002B0DC
	public void DelayedUpdate()
	{
		this.MarkProtectedMinutesDirty(0f);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600071B RID: 1819 RVA: 0x0002CEF0 File Offset: 0x0002B0F0
	private bool CanAdministrate(global::BasePlayer player)
	{
		global::BaseLock baseLock = base.GetSlot(global::BaseEntity.Slot.Lock) as global::BaseLock;
		return baseLock == null || baseLock.OnTryToOpen(player);
	}

	// Token: 0x0600071C RID: 1820 RVA: 0x0002CF20 File Offset: 0x0002B120
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void AddSelfAuthorize(global::BaseEntity.RPCMessage rpc)
	{
		global::BaseEntity.RPCMessage rpc = rpc2;
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.CanAdministrate(rpc.player))
		{
			return;
		}
		if (Interface.CallHook("OnCupboardAuthorize", new object[]
		{
			this,
			rpc2.player
		}) != null)
		{
			return;
		}
		this.authorizedPlayers.RemoveAll((PlayerNameID x) => x.userid == rpc.player.userID);
		PlayerNameID playerNameID = new PlayerNameID();
		playerNameID.userid = rpc.player.userID;
		playerNameID.username = rpc.player.displayName;
		this.authorizedPlayers.Add(playerNameID);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600071D RID: 1821 RVA: 0x0002CFE8 File Offset: 0x0002B1E8
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RemoveSelfAuthorize(global::BaseEntity.RPCMessage rpc)
	{
		global::BaseEntity.RPCMessage rpc = rpc2;
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.CanAdministrate(rpc.player))
		{
			return;
		}
		if (Interface.CallHook("OnCupboardDeauthorize", new object[]
		{
			this,
			rpc2.player
		}) != null)
		{
			return;
		}
		this.authorizedPlayers.RemoveAll((PlayerNameID x) => x.userid == rpc.player.userID);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600071E RID: 1822 RVA: 0x0002D074 File Offset: 0x0002B274
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void ClearList(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.CanAdministrate(rpc.player))
		{
			return;
		}
		if (Interface.CallHook("OnCupboardClearList", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		this.authorizedPlayers.Clear();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600071F RID: 1823 RVA: 0x0002D0DC File Offset: 0x0002B2DC
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_Rotate(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (player.CanBuild() && player.GetHeldEntity() && player.GetHeldEntity().GetComponent<global::Hammer>() != null && (base.GetSlot(global::BaseEntity.Slot.Lock) == null || !base.GetSlot(global::BaseEntity.Slot.Lock).IsLocked()))
		{
			base.transform.rotation = Quaternion.LookRotation(-base.transform.forward, base.transform.up);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			global::Deployable component = base.GetComponent<global::Deployable>();
			if (component != null && component.placeEffect.isValid)
			{
				global::Effect.server.Run(component.placeEffect.resourcePath, base.transform.position, Vector3.up, null, false);
			}
		}
		global::BaseEntity slot = base.GetSlot(global::BaseEntity.Slot.Lock);
		if (slot != null)
		{
			slot.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x06000720 RID: 1824 RVA: 0x0002D1D8 File Offset: 0x0002B3D8
	public override bool HasSlot(global::BaseEntity.Slot slot)
	{
		return slot == global::BaseEntity.Slot.Lock || base.HasSlot(slot);
	}

	// Token: 0x06000721 RID: 1825 RVA: 0x0002D1EC File Offset: 0x0002B3EC
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x06000722 RID: 1826 RVA: 0x0002D1F0 File Offset: 0x0002B3F0
	public float CalculateUpkeepPeriodMinutes()
	{
		if (base.isServer)
		{
			return ConVar.Decay.upkeep_period_minutes;
		}
		return 0f;
	}

	// Token: 0x06000723 RID: 1827 RVA: 0x0002D208 File Offset: 0x0002B408
	public float CalculateUpkeepCostFraction()
	{
		if (base.isServer)
		{
			return this.CalculateBuildingTaxRate();
		}
		return 0f;
	}

	// Token: 0x06000724 RID: 1828 RVA: 0x0002D224 File Offset: 0x0002B424
	public void CalculateUpkeepCostAmounts(List<global::ItemAmount> itemAmounts)
	{
		global::BuildingManager.Building building = base.GetBuilding();
		if (building == null)
		{
			return;
		}
		if (!building.HasDecayEntities())
		{
			return;
		}
		float multiplier = this.CalculateUpkeepCostFraction();
		foreach (global::DecayEntity decayEntity in building.decayEntities)
		{
			decayEntity.CalculateUpkeepCostAmounts(itemAmounts, multiplier);
		}
	}

	// Token: 0x06000725 RID: 1829 RVA: 0x0002D2A0 File Offset: 0x0002B4A0
	public float GetProtectedMinutes(bool force = false)
	{
		if (!base.isServer)
		{
			return 0f;
		}
		if (!force && UnityEngine.Time.realtimeSinceStartup < this.nextProtectedCalcTime)
		{
			return this.cachedProtectedMinutes;
		}
		this.nextProtectedCalcTime = UnityEngine.Time.realtimeSinceStartup + 60f;
		List<global::ItemAmount> list = Facepunch.Pool.GetList<global::ItemAmount>();
		this.CalculateUpkeepCostAmounts(list);
		float num = this.CalculateUpkeepPeriodMinutes();
		float num2 = -1f;
		if (this.inventory != null)
		{
			foreach (global::ItemAmount itemAmount in list)
			{
				List<global::Item> source = this.inventory.FindItemsByItemID(itemAmount.itemid);
				int num3 = source.Sum((global::Item x) => x.amount);
				if (num3 > 0 && itemAmount.amount > 0f)
				{
					float num4 = (float)num3 / itemAmount.amount * num;
					if (num2 == -1f || num4 < num2)
					{
						num2 = num4;
					}
				}
				else
				{
					num2 = 0f;
				}
			}
			if (num2 == -1f)
			{
				num2 = 0f;
			}
		}
		Facepunch.Pool.FreeList<global::ItemAmount>(ref list);
		this.cachedProtectedMinutes = num2;
		return this.cachedProtectedMinutes;
	}

	// Token: 0x06000726 RID: 1830 RVA: 0x0002D3FC File Offset: 0x0002B5FC
	public override void OnKilled(global::HitInfo info)
	{
		if (ConVar.Decay.upkeep_grief_protection > 0f)
		{
			this.PurchaseUpkeepTime(ConVar.Decay.upkeep_grief_protection * 60f);
		}
		base.OnKilled(info);
	}

	// Token: 0x06000727 RID: 1831 RVA: 0x0002D428 File Offset: 0x0002B628
	public override void DecayTick()
	{
		if (this.EnsurePrimary())
		{
			base.DecayTick();
		}
	}

	// Token: 0x06000728 RID: 1832 RVA: 0x0002D43C File Offset: 0x0002B63C
	private bool EnsurePrimary()
	{
		global::BuildingManager.Building building = base.GetBuilding();
		if (building != null)
		{
			global::BuildingPrivlidge dominatingBuildingPrivilege = building.GetDominatingBuildingPrivilege();
			if (dominatingBuildingPrivilege != null && dominatingBuildingPrivilege != this)
			{
				base.Kill(global::BaseNetworkable.DestroyMode.Gib);
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000729 RID: 1833 RVA: 0x0002D480 File Offset: 0x0002B680
	public void MarkProtectedMinutesDirty(float delay = 0f)
	{
		this.nextProtectedCalcTime = UnityEngine.Time.realtimeSinceStartup + delay;
	}

	// Token: 0x0600072A RID: 1834 RVA: 0x0002D490 File Offset: 0x0002B690
	private float CalculateBuildingTaxRate()
	{
		global::BuildingManager.Building building = base.GetBuilding();
		if (building == null)
		{
			return ConVar.Decay.bracket_0_costfraction;
		}
		if (!building.HasBuildingBlocks())
		{
			return ConVar.Decay.bracket_0_costfraction;
		}
		int count = building.buildingBlocks.Count;
		int num = count;
		for (int i = 0; i < global::BuildingPrivlidge.upkeepBrackets.Length; i++)
		{
			global::BuildingPrivlidge.UpkeepBracket upkeepBracket = global::BuildingPrivlidge.upkeepBrackets[i];
			upkeepBracket.blocksTaxPaid = 0f;
			if (num > 0)
			{
				int num2;
				if (i == global::BuildingPrivlidge.upkeepBrackets.Length - 1)
				{
					num2 = num;
				}
				else
				{
					num2 = Mathf.Min(num, global::BuildingPrivlidge.upkeepBrackets[i].objectsUpTo);
				}
				num -= num2;
				upkeepBracket.blocksTaxPaid = (float)num2 * upkeepBracket.fraction;
			}
		}
		float num3 = 0f;
		for (int j = 0; j < global::BuildingPrivlidge.upkeepBrackets.Length; j++)
		{
			global::BuildingPrivlidge.UpkeepBracket upkeepBracket2 = global::BuildingPrivlidge.upkeepBrackets[j];
			if (upkeepBracket2.blocksTaxPaid <= 0f)
			{
				break;
			}
			num3 += upkeepBracket2.blocksTaxPaid;
		}
		return num3 / (float)count;
	}

	// Token: 0x0600072B RID: 1835 RVA: 0x0002D5AC File Offset: 0x0002B7AC
	private void ApplyUpkeepPayment()
	{
		List<global::Item> list = Facepunch.Pool.GetList<global::Item>();
		for (int i = 0; i < this.upkeepBuffer.Count; i++)
		{
			global::ItemAmount itemAmount = this.upkeepBuffer[i];
			int num = (int)itemAmount.amount;
			if (num >= 1)
			{
				this.inventory.Take(list, itemAmount.itemid, num);
				foreach (global::Item item in list)
				{
					item.UseItem(item.amount);
				}
				list.Clear();
				itemAmount.amount -= (float)num;
				this.upkeepBuffer[i] = itemAmount;
			}
		}
		Facepunch.Pool.FreeList<global::Item>(ref list);
	}

	// Token: 0x0600072C RID: 1836 RVA: 0x0002D68C File Offset: 0x0002B88C
	private void QueueUpkeepPayment(List<global::ItemAmount> itemAmounts)
	{
		for (int i = 0; i < itemAmounts.Count; i++)
		{
			global::ItemAmount itemAmount = itemAmounts[i];
			bool flag = false;
			foreach (global::ItemAmount itemAmount2 in this.upkeepBuffer)
			{
				if (itemAmount2.itemDef == itemAmount.itemDef)
				{
					itemAmount2.amount += itemAmount.amount;
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				this.upkeepBuffer.Add(new global::ItemAmount(itemAmount.itemDef, itemAmount.amount));
			}
		}
	}

	// Token: 0x0600072D RID: 1837 RVA: 0x0002D758 File Offset: 0x0002B958
	private bool CanAffordUpkeepPayment(List<global::ItemAmount> itemAmounts)
	{
		for (int i = 0; i < itemAmounts.Count; i++)
		{
			global::ItemAmount itemAmount = itemAmounts[i];
			int amount = this.inventory.GetAmount(itemAmount.itemid, true);
			if ((float)amount < itemAmount.amount)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x0600072E RID: 1838 RVA: 0x0002D7A8 File Offset: 0x0002B9A8
	public float PurchaseUpkeepTime(global::DecayEntity entity, float deltaTime)
	{
		float num = this.CalculateUpkeepCostFraction();
		float num2 = this.CalculateUpkeepPeriodMinutes() * 60f;
		float multiplier = num * deltaTime / num2;
		List<global::ItemAmount> list = Facepunch.Pool.GetList<global::ItemAmount>();
		entity.CalculateUpkeepCostAmounts(list, multiplier);
		bool flag = this.CanAffordUpkeepPayment(list);
		this.QueueUpkeepPayment(list);
		Facepunch.Pool.FreeList<global::ItemAmount>(ref list);
		this.ApplyUpkeepPayment();
		return (!flag) ? 0f : deltaTime;
	}

	// Token: 0x0600072F RID: 1839 RVA: 0x0002D80C File Offset: 0x0002BA0C
	public void PurchaseUpkeepTime(float deltaTime)
	{
		global::BuildingManager.Building building = base.GetBuilding();
		if (building != null && building.HasDecayEntities())
		{
			float num = this.GetProtectedMinutes(true) * 60f;
			float num2 = Mathf.Min(num, deltaTime);
			if (num2 > 0f)
			{
				foreach (global::DecayEntity decayEntity in building.decayEntities)
				{
					float protectedSeconds = decayEntity.GetProtectedSeconds();
					if (num2 > protectedSeconds)
					{
						decayEntity.AddUpkeepTime(this.PurchaseUpkeepTime(decayEntity, num2 - protectedSeconds));
					}
				}
			}
		}
	}

	// Token: 0x04000339 RID: 825
	public List<PlayerNameID> authorizedPlayers = new List<PlayerNameID>();

	// Token: 0x0400033A RID: 826
	private float cachedProtectedMinutes;

	// Token: 0x0400033B RID: 827
	private float nextProtectedCalcTime;

	// Token: 0x0400033C RID: 828
	private static global::BuildingPrivlidge.UpkeepBracket[] upkeepBrackets = new global::BuildingPrivlidge.UpkeepBracket[]
	{
		new global::BuildingPrivlidge.UpkeepBracket(ConVar.Decay.bracket_0_blockcount, ConVar.Decay.bracket_0_costfraction),
		new global::BuildingPrivlidge.UpkeepBracket(ConVar.Decay.bracket_1_blockcount, ConVar.Decay.bracket_1_costfraction),
		new global::BuildingPrivlidge.UpkeepBracket(ConVar.Decay.bracket_2_blockcount, ConVar.Decay.bracket_2_costfraction),
		new global::BuildingPrivlidge.UpkeepBracket(ConVar.Decay.bracket_3_blockcount, ConVar.Decay.bracket_3_costfraction)
	};

	// Token: 0x0400033D RID: 829
	private List<global::ItemAmount> upkeepBuffer = new List<global::ItemAmount>();

	// Token: 0x02000059 RID: 89
	public class UpkeepBracket
	{
		// Token: 0x06000732 RID: 1842 RVA: 0x0002D928 File Offset: 0x0002BB28
		public UpkeepBracket(int numObjs, float frac)
		{
			this.objectsUpTo = numObjs;
			this.fraction = frac;
			this.blocksTaxPaid = 0f;
		}

		// Token: 0x0400033F RID: 831
		public int objectsUpTo;

		// Token: 0x04000340 RID: 832
		public float fraction;

		// Token: 0x04000341 RID: 833
		public float blocksTaxPaid;
	}
}
