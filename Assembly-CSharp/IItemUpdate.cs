﻿using System;

// Token: 0x020004C1 RID: 1217
public interface IItemUpdate
{
	// Token: 0x06001A55 RID: 6741
	void OnItemUpdate(global::Item item);
}
