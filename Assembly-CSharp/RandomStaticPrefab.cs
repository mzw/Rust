﻿using System;
using UnityEngine;

// Token: 0x02000551 RID: 1361
public class RandomStaticPrefab : MonoBehaviour
{
	// Token: 0x06001C9C RID: 7324 RVA: 0x000A04AC File Offset: 0x0009E6AC
	protected void Start()
	{
		uint num = SeedEx.Seed(base.transform.position, global::World.Seed + this.Seed);
		if (SeedRandom.Value(ref num) > this.Probability)
		{
			global::GameManager.Destroy(this, 0f);
		}
		else
		{
			global::Prefab prefab = global::Prefab.LoadRandom("assets/bundled/prefabs/autospawn/" + this.ResourceFolder, ref num, null, null, true);
			prefab.Spawn(base.transform);
			global::GameManager.Destroy(this, 0f);
		}
	}

	// Token: 0x040017B7 RID: 6071
	public uint Seed;

	// Token: 0x040017B8 RID: 6072
	public float Probability = 0.5f;

	// Token: 0x040017B9 RID: 6073
	public string ResourceFolder = string.Empty;
}
