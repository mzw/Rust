﻿using System;
using System.IO;
using Network;

// Token: 0x0200050B RID: 1291
public abstract class NetworkCryptography : INetworkCryptocraphy
{
	// Token: 0x06001B50 RID: 6992 RVA: 0x0009861C File Offset: 0x0009681C
	public MemoryStream EncryptCopy(Connection connection, MemoryStream stream, int offset)
	{
		this.buffer.Position = 0L;
		this.buffer.SetLength(0L);
		this.buffer.Write(stream.GetBuffer(), 0, offset);
		this.EncryptionHandler(connection, stream, offset, this.buffer, offset);
		return this.buffer;
	}

	// Token: 0x06001B51 RID: 6993 RVA: 0x0009866C File Offset: 0x0009686C
	public MemoryStream DecryptCopy(Connection connection, MemoryStream stream, int offset)
	{
		this.buffer.Position = 0L;
		this.buffer.SetLength(0L);
		this.buffer.Write(stream.GetBuffer(), 0, offset);
		this.DecryptionHandler(connection, stream, offset, this.buffer, offset);
		return this.buffer;
	}

	// Token: 0x06001B52 RID: 6994 RVA: 0x000986BC File Offset: 0x000968BC
	public void Encrypt(Connection connection, MemoryStream stream, int offset)
	{
		this.EncryptionHandler(connection, stream, offset, stream, offset);
	}

	// Token: 0x06001B53 RID: 6995 RVA: 0x000986CC File Offset: 0x000968CC
	public void Decrypt(Connection connection, MemoryStream stream, int offset)
	{
		this.DecryptionHandler(connection, stream, offset, stream, offset);
	}

	// Token: 0x06001B54 RID: 6996 RVA: 0x000986DC File Offset: 0x000968DC
	public bool IsEnabledIncoming(Connection connection)
	{
		return connection != null && connection.encryptionLevel > 0u && connection.decryptIncoming;
	}

	// Token: 0x06001B55 RID: 6997 RVA: 0x000986FC File Offset: 0x000968FC
	public bool IsEnabledOutgoing(Connection connection)
	{
		return connection != null && connection.encryptionLevel > 0u && connection.encryptOutgoing;
	}

	// Token: 0x06001B56 RID: 6998
	protected abstract void EncryptionHandler(Connection connection, MemoryStream src, int srcOffset, MemoryStream dst, int dstOffset);

	// Token: 0x06001B57 RID: 6999
	protected abstract void DecryptionHandler(Connection connection, MemoryStream src, int srcOffset, MemoryStream dst, int dstOffset);

	// Token: 0x0400161A RID: 5658
	private MemoryStream buffer = new MemoryStream();
}
