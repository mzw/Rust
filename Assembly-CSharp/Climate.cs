﻿using System;
using UnityEngine;

// Token: 0x020003FE RID: 1022
public class Climate : SingletonComponent<global::Climate>
{
	// Token: 0x060017B5 RID: 6069 RVA: 0x00087B18 File Offset: 0x00085D18
	protected void Update()
	{
		if (!global::TerrainMeta.BiomeMap || !TOD_Sky.Instance)
		{
			return;
		}
		TOD_Sky instance = TOD_Sky.Instance;
		long num = 36000000000L;
		long num2 = (long)((ulong)global::World.Seed + (ulong)instance.Cycle.Ticks);
		long num3 = 18L * num;
		long num4 = 6L * num;
		long num5 = num2 / num3;
		float t = Mathf.InverseLerp(0f, (float)num4, (float)(num2 % num3));
		global::Climate.WeatherState weatherState = this.GetWeatherState((uint)(num5 % (long)((ulong)-1)));
		global::Climate.WeatherState weatherState2 = this.GetWeatherState((uint)((num5 + 1L) % (long)((ulong)-1)));
		this.state = global::Climate.WeatherState.Fade(weatherState, weatherState2, t);
		this.state.Override(this.Overrides);
	}

	// Token: 0x060017B6 RID: 6070 RVA: 0x00087BCC File Offset: 0x00085DCC
	public static float GetClouds(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 0f;
		}
		return Mathf.Max(SingletonComponent<global::Climate>.Instance.clamps.Clouds, SingletonComponent<global::Climate>.Instance.state.Clouds);
	}

	// Token: 0x060017B7 RID: 6071 RVA: 0x00087C08 File Offset: 0x00085E08
	public static float GetCloudOpacity(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 1f;
		}
		return Mathf.InverseLerp(0.9f, 0.8f, global::Climate.GetFog(position));
	}

	// Token: 0x060017B8 RID: 6072 RVA: 0x00087C34 File Offset: 0x00085E34
	public static float GetFog(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 0f;
		}
		return Mathf.Max(SingletonComponent<global::Climate>.Instance.clamps.Fog, SingletonComponent<global::Climate>.Instance.state.Fog);
	}

	// Token: 0x060017B9 RID: 6073 RVA: 0x00087C70 File Offset: 0x00085E70
	public static float GetWind(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 0f;
		}
		return Mathf.Max(SingletonComponent<global::Climate>.Instance.clamps.Wind, SingletonComponent<global::Climate>.Instance.state.Wind);
	}

	// Token: 0x060017BA RID: 6074 RVA: 0x00087CAC File Offset: 0x00085EAC
	public static float GetRain(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 0f;
		}
		float num = (!global::TerrainMeta.BiomeMap) ? 0f : global::TerrainMeta.BiomeMap.GetBiome(position, 1);
		float num2 = (!global::TerrainMeta.BiomeMap) ? 0f : global::TerrainMeta.BiomeMap.GetBiome(position, 8);
		return Mathf.Max(SingletonComponent<global::Climate>.Instance.clamps.Rain, SingletonComponent<global::Climate>.Instance.state.Rain) * Mathf.Lerp(1f, 0.5f, num) * (1f - num2);
	}

	// Token: 0x060017BB RID: 6075 RVA: 0x00087D58 File Offset: 0x00085F58
	public static float GetSnow(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 0f;
		}
		float num = (!global::TerrainMeta.BiomeMap) ? 0f : global::TerrainMeta.BiomeMap.GetBiome(position, 8);
		return Mathf.Max(SingletonComponent<global::Climate>.Instance.clamps.Rain, SingletonComponent<global::Climate>.Instance.state.Rain) * num;
	}

	// Token: 0x060017BC RID: 6076 RVA: 0x00087DC8 File Offset: 0x00085FC8
	public static float GetTemperature(Vector3 position)
	{
		if (!SingletonComponent<global::Climate>.Instance)
		{
			return 15f;
		}
		if (!TOD_Sky.Instance)
		{
			return 15f;
		}
		global::Climate.ClimateParameters climateParameters;
		global::Climate.ClimateParameters climateParameters2;
		float num = SingletonComponent<global::Climate>.Instance.FindBlendParameters(position, out climateParameters, out climateParameters2);
		if (climateParameters == null || climateParameters2 == null)
		{
			return 15f;
		}
		float hour = TOD_Sky.Instance.Cycle.Hour;
		float num2 = climateParameters.Temperature.Evaluate(hour);
		float num3 = climateParameters2.Temperature.Evaluate(hour);
		return Mathf.Lerp(num2, num3, num);
	}

	// Token: 0x060017BD RID: 6077 RVA: 0x00087E58 File Offset: 0x00086058
	private global::Climate.WeatherState GetWeatherState(uint seed)
	{
		SeedRandom.Wanghash(ref seed);
		bool flag = SeedRandom.Value(ref seed) < this.Weather.CloudChance;
		bool flag2 = SeedRandom.Value(ref seed) < this.Weather.FogChance;
		bool flag3 = SeedRandom.Value(ref seed) < this.Weather.RainChance;
		bool flag4 = SeedRandom.Value(ref seed) < this.Weather.StormChance;
		float num = (!flag) ? 0f : SeedRandom.Value(ref seed);
		float num2 = (float)((!flag2) ? 0 : 1);
		float num3 = (float)((!flag3) ? 0 : 1);
		float wind = (!flag4) ? 0f : SeedRandom.Value(ref seed);
		if (num3 > 0f)
		{
			num3 = Mathf.Max(num3, 0.5f);
			num2 = Mathf.Max(num2, num3);
			num = Mathf.Max(num, num3);
		}
		return new global::Climate.WeatherState
		{
			Clouds = num,
			Fog = num2,
			Wind = wind,
			Rain = num3
		};
	}

	// Token: 0x060017BE RID: 6078 RVA: 0x00087F74 File Offset: 0x00086174
	private float FindBlendParameters(Vector3 pos, out global::Climate.ClimateParameters src, out global::Climate.ClimateParameters dst)
	{
		if (this.climates == null)
		{
			this.climates = new global::Climate.ClimateParameters[]
			{
				this.Arid,
				this.Temperate,
				this.Tundra,
				this.Arctic
			};
		}
		if (global::TerrainMeta.BiomeMap == null)
		{
			src = null;
			dst = null;
			return 0.5f;
		}
		int biomeMaxType = global::TerrainMeta.BiomeMap.GetBiomeMaxType(pos, -1);
		int biomeMaxType2 = global::TerrainMeta.BiomeMap.GetBiomeMaxType(pos, ~biomeMaxType);
		src = this.climates[global::TerrainBiome.TypeToIndex(biomeMaxType)];
		dst = this.climates[global::TerrainBiome.TypeToIndex(biomeMaxType2)];
		return global::TerrainMeta.BiomeMap.GetBiome(pos, biomeMaxType2);
	}

	// Token: 0x0400122B RID: 4651
	private const float fadeAngle = 20f;

	// Token: 0x0400122C RID: 4652
	private const float defaultTemp = 15f;

	// Token: 0x0400122D RID: 4653
	private const int weatherDurationHours = 18;

	// Token: 0x0400122E RID: 4654
	private const int weatherFadeHours = 6;

	// Token: 0x0400122F RID: 4655
	[Range(0f, 1f)]
	public float BlendingSpeed = 1f;

	// Token: 0x04001230 RID: 4656
	[Range(1f, 9f)]
	public float FogMultiplier = 5f;

	// Token: 0x04001231 RID: 4657
	public float FogDarknessDistance = 200f;

	// Token: 0x04001232 RID: 4658
	public bool DebugLUTBlending;

	// Token: 0x04001233 RID: 4659
	public global::Climate.WeatherParameters Weather;

	// Token: 0x04001234 RID: 4660
	public global::Climate.ClimateParameters Arid;

	// Token: 0x04001235 RID: 4661
	public global::Climate.ClimateParameters Temperate;

	// Token: 0x04001236 RID: 4662
	public global::Climate.ClimateParameters Tundra;

	// Token: 0x04001237 RID: 4663
	public global::Climate.ClimateParameters Arctic;

	// Token: 0x04001238 RID: 4664
	private global::Climate.ClimateParameters[] climates;

	// Token: 0x04001239 RID: 4665
	private global::Climate.WeatherState state = new global::Climate.WeatherState
	{
		Clouds = 0f,
		Fog = 0f,
		Wind = 0f,
		Rain = 0f
	};

	// Token: 0x0400123A RID: 4666
	private global::Climate.WeatherState clamps = new global::Climate.WeatherState
	{
		Clouds = -1f,
		Fog = -1f,
		Wind = -1f,
		Rain = -1f
	};

	// Token: 0x0400123B RID: 4667
	public global::Climate.WeatherState Overrides = new global::Climate.WeatherState
	{
		Clouds = -1f,
		Fog = -1f,
		Wind = -1f,
		Rain = -1f
	};

	// Token: 0x020003FF RID: 1023
	[Serializable]
	public class ClimateParameters
	{
		// Token: 0x0400123C RID: 4668
		public AnimationCurve Temperature;

		// Token: 0x0400123D RID: 4669
		[Horizontal(4, -1)]
		public global::Climate.Float4 AerialDensity;

		// Token: 0x0400123E RID: 4670
		[Horizontal(4, -1)]
		public global::Climate.Float4 FogDensity;

		// Token: 0x0400123F RID: 4671
		[Horizontal(4, -1)]
		public global::Climate.Texture2D4 LUT;
	}

	// Token: 0x02000400 RID: 1024
	[Serializable]
	public class WeatherParameters
	{
		// Token: 0x04001240 RID: 4672
		[Range(0f, 1f)]
		public float RainChance = 0.5f;

		// Token: 0x04001241 RID: 4673
		[Range(0f, 1f)]
		public float FogChance = 0.5f;

		// Token: 0x04001242 RID: 4674
		[Range(0f, 1f)]
		public float CloudChance = 0.5f;

		// Token: 0x04001243 RID: 4675
		[Range(0f, 1f)]
		public float StormChance = 0.5f;
	}

	// Token: 0x02000401 RID: 1025
	public struct WeatherState
	{
		// Token: 0x060017C1 RID: 6081 RVA: 0x0008805C File Offset: 0x0008625C
		public static global::Climate.WeatherState Fade(global::Climate.WeatherState a, global::Climate.WeatherState b, float t)
		{
			return new global::Climate.WeatherState
			{
				Clouds = Mathf.SmoothStep(a.Clouds, b.Clouds, t),
				Fog = Mathf.SmoothStep(a.Fog, b.Fog, t),
				Wind = Mathf.SmoothStep(a.Wind, b.Wind, t),
				Rain = Mathf.SmoothStep(a.Rain, b.Rain, t)
			};
		}

		// Token: 0x060017C2 RID: 6082 RVA: 0x000880E0 File Offset: 0x000862E0
		public void Override(global::Climate.WeatherState other)
		{
			if (other.Clouds >= 0f)
			{
				this.Clouds = Mathf.Clamp01(other.Clouds);
			}
			if (other.Fog >= 0f)
			{
				this.Fog = Mathf.Clamp01(other.Fog);
			}
			if (other.Wind >= 0f)
			{
				this.Wind = Mathf.Clamp01(other.Wind);
			}
			if (other.Rain >= 0f)
			{
				this.Rain = Mathf.Clamp01(other.Rain);
			}
		}

		// Token: 0x060017C3 RID: 6083 RVA: 0x0008817C File Offset: 0x0008637C
		public void Max(global::Climate.WeatherState other)
		{
			this.Clouds = Mathf.Max(this.Clouds, other.Clouds);
			this.Fog = Mathf.Max(this.Fog, other.Fog);
			this.Wind = Mathf.Max(this.Wind, other.Wind);
			this.Rain = Mathf.Max(this.Rain, other.Rain);
		}

		// Token: 0x04001244 RID: 4676
		public float Clouds;

		// Token: 0x04001245 RID: 4677
		public float Fog;

		// Token: 0x04001246 RID: 4678
		public float Wind;

		// Token: 0x04001247 RID: 4679
		public float Rain;
	}

	// Token: 0x02000402 RID: 1026
	public class Value4<T>
	{
		// Token: 0x060017C5 RID: 6085 RVA: 0x000881F4 File Offset: 0x000863F4
		public float FindBlendParameters(TOD_Sky sky, out T src, out T dst)
		{
			float num = Mathf.Abs(sky.SunriseTime - sky.Cycle.Hour);
			float num2 = Mathf.Abs(sky.SunsetTime - sky.Cycle.Hour);
			float num3 = (180f - sky.SunZenith) / 180f;
			float num4 = 0.111111112f;
			if (num < num2)
			{
				if (num3 < 0.5f)
				{
					src = this.Night;
					dst = this.Dawn;
					return Mathf.InverseLerp(0.5f - num4, 0.5f, num3);
				}
				src = this.Dawn;
				dst = this.Noon;
				return Mathf.InverseLerp(0.5f, 0.5f + num4, num3);
			}
			else
			{
				if (num3 > 0.5f)
				{
					src = this.Noon;
					dst = this.Dusk;
					return Mathf.InverseLerp(0.5f + num4, 0.5f, num3);
				}
				src = this.Dusk;
				dst = this.Night;
				return Mathf.InverseLerp(0.5f, 0.5f - num4, num3);
			}
		}

		// Token: 0x04001248 RID: 4680
		public T Dawn;

		// Token: 0x04001249 RID: 4681
		public T Noon;

		// Token: 0x0400124A RID: 4682
		public T Dusk;

		// Token: 0x0400124B RID: 4683
		public T Night;
	}

	// Token: 0x02000403 RID: 1027
	[Serializable]
	public class Float4 : global::Climate.Value4<float>
	{
	}

	// Token: 0x02000404 RID: 1028
	[Serializable]
	public class Color4 : global::Climate.Value4<Color>
	{
	}

	// Token: 0x02000405 RID: 1029
	[Serializable]
	public class Texture2D4 : global::Climate.Value4<Texture2D>
	{
	}
}
