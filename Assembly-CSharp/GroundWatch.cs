﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200042B RID: 1067
public class GroundWatch : MonoBehaviour, IServerComponent
{
	// Token: 0x0600184A RID: 6218 RVA: 0x00089CCC File Offset: 0x00087ECC
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(this.groundPosition, this.radius);
	}

	// Token: 0x0600184B RID: 6219 RVA: 0x00089CFC File Offset: 0x00087EFC
	public static void PhysicsChanged(GameObject obj)
	{
		Collider component = obj.GetComponent<Collider>();
		if (!component)
		{
			return;
		}
		Bounds bounds = component.bounds;
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(bounds.center, bounds.extents.magnitude + 1f, list, 2097408, 2);
		foreach (global::BaseEntity baseEntity in list)
		{
			if (!baseEntity.IsDestroyed)
			{
				if (!baseEntity.isClient)
				{
					if (!(baseEntity is global::BuildingBlock))
					{
						baseEntity.BroadcastMessage("OnPhysicsNeighbourChanged", 1);
					}
				}
			}
		}
		Pool.FreeList<global::BaseEntity>(ref list);
	}

	// Token: 0x0600184C RID: 6220 RVA: 0x00089DE0 File Offset: 0x00087FE0
	private void OnPhysicsNeighbourChanged()
	{
		if (!this.OnGround())
		{
			base.transform.root.BroadcastMessage("OnGroundMissing", 1);
		}
	}

	// Token: 0x0600184D RID: 6221 RVA: 0x00089E10 File Offset: 0x00088010
	private bool OnGround()
	{
		List<Collider> list = Pool.GetList<Collider>();
		global::Vis.Colliders<Collider>(base.transform.TransformPoint(this.groundPosition), this.radius, list, this.layers, 2);
		foreach (Collider collider in list)
		{
			if (!(collider.transform.root == base.gameObject.transform.root))
			{
				global::BaseEntity baseEntity = collider.gameObject.ToBaseEntity();
				if (baseEntity)
				{
					if (baseEntity.IsDestroyed)
					{
						continue;
					}
					if (baseEntity.isClient)
					{
						continue;
					}
				}
				return true;
			}
		}
		Pool.FreeList<Collider>(ref list);
		return false;
	}

	// Token: 0x040012DC RID: 4828
	public Vector3 groundPosition = Vector3.zero;

	// Token: 0x040012DD RID: 4829
	public LayerMask layers = 27328512;

	// Token: 0x040012DE RID: 4830
	public float radius = 0.1f;
}
