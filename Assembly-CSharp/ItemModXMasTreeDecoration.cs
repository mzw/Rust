﻿using System;

// Token: 0x020000C0 RID: 192
public class ItemModXMasTreeDecoration : global::ItemMod
{
	// Token: 0x0400055C RID: 1372
	public global::ItemModXMasTreeDecoration.xmasFlags flagsToChange;

	// Token: 0x020000C1 RID: 193
	public enum xmasFlags
	{
		// Token: 0x0400055E RID: 1374
		pineCones = 128,
		// Token: 0x0400055F RID: 1375
		candyCanes = 256,
		// Token: 0x04000560 RID: 1376
		gingerbreadMen = 512,
		// Token: 0x04000561 RID: 1377
		Tinsel = 1024,
		// Token: 0x04000562 RID: 1378
		Balls = 2048,
		// Token: 0x04000563 RID: 1379
		Star = 16384,
		// Token: 0x04000564 RID: 1380
		Lights = 32768
	}
}
