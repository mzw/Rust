﻿using System;
using System.IO;

// Token: 0x02000799 RID: 1945
public static class MurmurHash
{
	// Token: 0x06002427 RID: 9255 RVA: 0x000C73EC File Offset: 0x000C55EC
	public static int Signed(Stream stream)
	{
		return (int)global::MurmurHash.Unsigned(stream);
	}

	// Token: 0x06002428 RID: 9256 RVA: 0x000C73F4 File Offset: 0x000C55F4
	public static uint Unsigned(Stream stream)
	{
		uint num = 1337u;
		uint num2 = 0u;
		using (BinaryReader binaryReader = new BinaryReader(stream))
		{
			byte[] array = binaryReader.ReadBytes(4);
			while (array.Length > 0)
			{
				num2 += (uint)array.Length;
				switch (array.Length)
				{
				case 1:
				{
					uint num3 = (uint)array[0];
					num3 *= 3432918353u;
					num3 = global::MurmurHash.rot(num3, 15);
					num3 *= 461845907u;
					num ^= num3;
					break;
				}
				case 2:
				{
					uint num3 = (uint)((int)array[0] | (int)array[1] << 8);
					num3 *= 3432918353u;
					num3 = global::MurmurHash.rot(num3, 15);
					num3 *= 461845907u;
					num ^= num3;
					break;
				}
				case 3:
				{
					uint num3 = (uint)((int)array[0] | (int)array[1] << 8 | (int)array[2] << 16);
					num3 *= 3432918353u;
					num3 = global::MurmurHash.rot(num3, 15);
					num3 *= 461845907u;
					num ^= num3;
					break;
				}
				case 4:
				{
					uint num3 = (uint)((int)array[0] | (int)array[1] << 8 | (int)array[2] << 16 | (int)array[3] << 24);
					num3 *= 3432918353u;
					num3 = global::MurmurHash.rot(num3, 15);
					num3 *= 461845907u;
					num ^= num3;
					num = global::MurmurHash.rot(num, 13);
					num = num * 5u + 3864292196u;
					break;
				}
				}
				array = binaryReader.ReadBytes(4);
			}
		}
		num ^= num2;
		num = global::MurmurHash.mix(num);
		return num;
	}

	// Token: 0x06002429 RID: 9257 RVA: 0x000C7574 File Offset: 0x000C5774
	private static uint rot(uint x, byte r)
	{
		return x << (int)r | x >> (int)(32 - r);
	}

	// Token: 0x0600242A RID: 9258 RVA: 0x000C7588 File Offset: 0x000C5788
	private static uint mix(uint h)
	{
		h ^= h >> 16;
		h *= 2246822507u;
		h ^= h >> 13;
		h *= 3266489909u;
		h ^= h >> 16;
		return h;
	}

	// Token: 0x04001FCA RID: 8138
	private const uint seed = 1337u;
}
