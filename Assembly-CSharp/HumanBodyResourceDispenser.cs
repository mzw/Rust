﻿using System;

// Token: 0x02000371 RID: 881
public class HumanBodyResourceDispenser : global::ResourceDispenser
{
	// Token: 0x060014F8 RID: 5368 RVA: 0x000793E0 File Offset: 0x000775E0
	public override bool OverrideOwnership(global::Item item, global::AttackEntity weapon)
	{
		if (item.info.shortname == "skull.human")
		{
			global::PlayerCorpse component = base.GetComponent<global::PlayerCorpse>();
			if (component)
			{
				item.name = "Skull of \"" + component.playerName + "\"";
				return true;
			}
		}
		return false;
	}
}
