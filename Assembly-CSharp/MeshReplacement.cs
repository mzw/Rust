﻿using System;
using UnityEngine;

// Token: 0x02000460 RID: 1120
public class MeshReplacement : MonoBehaviour
{
	// Token: 0x0600189C RID: 6300 RVA: 0x0008AAFC File Offset: 0x00088CFC
	internal static void Process(GameObject go, bool IsFemale)
	{
		if (!IsFemale)
		{
			return;
		}
		foreach (global::MeshReplacement meshReplacement in go.GetComponentsInChildren<global::MeshReplacement>(true))
		{
			SkinnedMeshRenderer component = meshReplacement.GetComponent<SkinnedMeshRenderer>();
			component.sharedMesh = meshReplacement.Female.sharedMesh;
			component.rootBone = meshReplacement.Female.rootBone;
			component.bones = meshReplacement.Female.bones;
		}
	}

	// Token: 0x0400135B RID: 4955
	public SkinnedMeshRenderer Female;
}
