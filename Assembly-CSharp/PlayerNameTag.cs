﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020003C4 RID: 964
public class PlayerNameTag : MonoBehaviour
{
	// Token: 0x0400115F RID: 4447
	public CanvasGroup canvasGroup;

	// Token: 0x04001160 RID: 4448
	public Text text;

	// Token: 0x04001161 RID: 4449
	public Gradient color;

	// Token: 0x04001162 RID: 4450
	public float minDistance = 3f;

	// Token: 0x04001163 RID: 4451
	public float maxDistance = 10f;

	// Token: 0x04001164 RID: 4452
	public Vector3 positionOffset;

	// Token: 0x04001165 RID: 4453
	public Transform parentBone;
}
