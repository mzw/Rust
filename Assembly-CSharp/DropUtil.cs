﻿using System;
using ConVar;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000399 RID: 921
public class DropUtil
{
	// Token: 0x060015C0 RID: 5568 RVA: 0x0007D2DC File Offset: 0x0007B4DC
	public static void DropItems(global::ItemContainer container, Vector3 position, float chance = 1f)
	{
		if (!ConVar.Server.dropitems)
		{
			return;
		}
		if (container == null)
		{
			return;
		}
		if (container.itemList == null)
		{
			return;
		}
		if (Interface.CallHook("OnContainerDropItems", new object[]
		{
			container
		}) != null)
		{
			return;
		}
		float num = 0.25f;
		foreach (global::Item item in container.itemList.ToArray())
		{
			if (Random.Range(0f, 1f) <= chance)
			{
				float num2 = Random.Range(0f, 2f);
				item.RemoveFromContainer();
				global::BaseEntity baseEntity = item.CreateWorldObject(position + new Vector3(Random.Range(-num, num), 1f, Random.Range(-num, num)), default(Quaternion));
				if (baseEntity == null)
				{
					item.Remove(0f);
				}
				else if (num2 > 0f)
				{
					baseEntity.SetVelocity(new Vector3(Random.Range(-1f, 1f), Random.Range(0f, 1f), Random.Range(-1f, 1f)) * num2);
					baseEntity.SetAngularVelocity(new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f)) * num2);
				}
			}
		}
	}
}
