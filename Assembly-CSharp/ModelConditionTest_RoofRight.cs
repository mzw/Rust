﻿using System;
using UnityEngine;

// Token: 0x0200021A RID: 538
public class ModelConditionTest_RoofRight : global::ModelConditionTest
{
	// Token: 0x06000FB5 RID: 4021 RVA: 0x0005FE74 File Offset: 0x0005E074
	protected void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.gray;
		Gizmos.DrawWireCube(new Vector3(-3f, 1.5f, 0f), new Vector3(3f, 3f, 3f));
	}

	// Token: 0x06000FB6 RID: 4022 RVA: 0x0005FEC8 File Offset: 0x0005E0C8
	public override bool DoTest(global::BaseEntity ent)
	{
		global::EntityLink entityLink = ent.FindLink("roof/sockets/neighbour/1");
		return entityLink != null && entityLink.IsEmpty();
	}

	// Token: 0x04000A80 RID: 2688
	private const string socket = "roof/sockets/neighbour/1";
}
