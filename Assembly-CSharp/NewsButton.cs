﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006D7 RID: 1751
public class NewsButton : MonoBehaviour
{
	// Token: 0x04001DAF RID: 7599
	public int storyNumber;

	// Token: 0x04001DB0 RID: 7600
	public global::NewsSource.Story story;

	// Token: 0x04001DB1 RID: 7601
	public CanvasGroup canvasGroup;

	// Token: 0x04001DB2 RID: 7602
	public Text text;

	// Token: 0x04001DB3 RID: 7603
	public Text author;

	// Token: 0x04001DB4 RID: 7604
	public RawImage image;

	// Token: 0x04001DB5 RID: 7605
	private float randomness;
}
