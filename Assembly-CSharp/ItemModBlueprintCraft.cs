﻿using System;
using UnityEngine;

// Token: 0x020004D8 RID: 1240
public class ItemModBlueprintCraft : global::ItemMod
{
	// Token: 0x06001AB6 RID: 6838 RVA: 0x00095A68 File Offset: 0x00093C68
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (item.GetOwnerPlayer() != player)
		{
			return;
		}
		if (command == "craft")
		{
			if (!item.IsBlueprint())
			{
				return;
			}
			if (!player.inventory.crafting.CanCraft(item.blueprintTargetDef.Blueprint, 1))
			{
				return;
			}
			global::Item fromTempBlueprint = item;
			if (item.amount > 1)
			{
				fromTempBlueprint = item.SplitItem(1);
			}
			player.inventory.crafting.CraftItem(item.blueprintTargetDef.Blueprint, player, null, 1, 0, fromTempBlueprint);
			if (this.successEffect.isValid)
			{
				global::Effect.server.Run(this.successEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
			}
		}
		if (command == "craft_all")
		{
			if (!item.IsBlueprint())
			{
				return;
			}
			if (!player.inventory.crafting.CanCraft(item.blueprintTargetDef.Blueprint, item.amount))
			{
				return;
			}
			player.inventory.crafting.CraftItem(item.blueprintTargetDef.Blueprint, player, null, item.amount, 0, item);
			if (this.successEffect.isValid)
			{
				global::Effect.server.Run(this.successEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
			}
		}
	}

	// Token: 0x04001576 RID: 5494
	public global::GameObjectRef successEffect;
}
