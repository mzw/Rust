﻿using System;
using UnityEngine;

// Token: 0x02000600 RID: 1536
[Serializable]
public struct SubsurfaceProfileData
{
	// Token: 0x17000229 RID: 553
	// (get) Token: 0x06001F45 RID: 8005 RVA: 0x000B1B50 File Offset: 0x000AFD50
	public static global::SubsurfaceProfileData Default
	{
		get
		{
			return new global::SubsurfaceProfileData
			{
				ScatterRadius = 1.2f,
				SubsurfaceColor = new Color(0.48f, 0.41f, 0.28f),
				FalloffColor = new Color(1f, 0.37f, 0.3f)
			};
		}
	}

	// Token: 0x1700022A RID: 554
	// (get) Token: 0x06001F46 RID: 8006 RVA: 0x000B1BA8 File Offset: 0x000AFDA8
	public static global::SubsurfaceProfileData Invalid
	{
		get
		{
			return new global::SubsurfaceProfileData
			{
				ScatterRadius = 0f,
				SubsurfaceColor = Color.clear,
				FalloffColor = Color.clear
			};
		}
	}

	// Token: 0x04001A48 RID: 6728
	[Range(0.1f, 50f)]
	public float ScatterRadius;

	// Token: 0x04001A49 RID: 6729
	[ColorUsage(false, true, 1f, 1f, 1f, 1f)]
	public Color SubsurfaceColor;

	// Token: 0x04001A4A RID: 6730
	[ColorUsage(false, true, 1f, 1f, 1f, 1f)]
	public Color FalloffColor;
}
