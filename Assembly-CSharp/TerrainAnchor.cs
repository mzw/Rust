﻿using System;
using UnityEngine;

// Token: 0x02000556 RID: 1366
public class TerrainAnchor : global::PrefabAttribute
{
	// Token: 0x06001CA7 RID: 7335 RVA: 0x000A092C File Offset: 0x0009EB2C
	protected void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		Gizmos.DrawLine(base.transform.position + Vector3.up * this.Offset - Vector3.up * this.Extents, base.transform.position + Vector3.up * this.Offset + Vector3.up * this.Extents);
	}

	// Token: 0x06001CA8 RID: 7336 RVA: 0x000A09C8 File Offset: 0x0009EBC8
	public void Apply(out float height, out float min, out float max, Vector3 pos)
	{
		height = global::TerrainMeta.HeightMap.GetHeight(pos);
		min = height - this.Offset - this.Extents;
		max = height - this.Offset + this.Extents;
	}

	// Token: 0x06001CA9 RID: 7337 RVA: 0x000A09FC File Offset: 0x0009EBFC
	protected override Type GetIndexedType()
	{
		return typeof(global::TerrainAnchor);
	}

	// Token: 0x040017C4 RID: 6084
	public float Extents = 1f;

	// Token: 0x040017C5 RID: 6085
	public float Offset;
}
