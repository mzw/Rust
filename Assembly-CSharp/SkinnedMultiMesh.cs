﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000280 RID: 640
public class SkinnedMultiMesh : MonoBehaviour
{
	// Token: 0x17000120 RID: 288
	// (get) Token: 0x060010BF RID: 4287 RVA: 0x00064BB4 File Offset: 0x00062DB4
	public List<Renderer> Renderers
	{
		get
		{
			return this.renderers;
		}
	}

	// Token: 0x17000121 RID: 289
	// (get) Token: 0x060010C0 RID: 4288 RVA: 0x00064BBC File Offset: 0x00062DBC
	public List<Animator> Animators
	{
		get
		{
			return this.animators;
		}
	}

	// Token: 0x04000BD9 RID: 3033
	public bool shadowOnly;

	// Token: 0x04000BDA RID: 3034
	public List<global::SkinnedMultiMesh.Part> parts = new List<global::SkinnedMultiMesh.Part>();

	// Token: 0x04000BDB RID: 3035
	public Dictionary<string, Transform> boneDict = new Dictionary<string, Transform>(StringComparer.OrdinalIgnoreCase);

	// Token: 0x04000BDC RID: 3036
	[NonSerialized]
	public List<global::SkinnedMultiMesh.Part> createdParts = new List<global::SkinnedMultiMesh.Part>();

	// Token: 0x04000BDD RID: 3037
	[NonSerialized]
	public long lastBuildHash;

	// Token: 0x04000BDE RID: 3038
	[NonSerialized]
	public MaterialPropertyBlock sharedBlockProperty;

	// Token: 0x04000BDF RID: 3039
	[NonSerialized]
	public MaterialPropertyBlock hairBlockProperty;

	// Token: 0x04000BE0 RID: 3040
	public float skinNumber;

	// Token: 0x04000BE1 RID: 3041
	public float meshNumber;

	// Token: 0x04000BE2 RID: 3042
	public float hairNumber;

	// Token: 0x04000BE3 RID: 3043
	public int skinType;

	// Token: 0x04000BE4 RID: 3044
	public global::SkinSetCollection SkinCollection;

	// Token: 0x04000BE5 RID: 3045
	private global::OptimizeAnimator optimizeAnimator;

	// Token: 0x04000BE6 RID: 3046
	private LODGroup lodGroup;

	// Token: 0x04000BE7 RID: 3047
	private List<Renderer> renderers = new List<Renderer>(32);

	// Token: 0x04000BE8 RID: 3048
	private List<Animator> animators = new List<Animator>(8);

	// Token: 0x02000281 RID: 641
	public struct Part
	{
		// Token: 0x04000BE9 RID: 3049
		public GameObject gameObject;

		// Token: 0x04000BEA RID: 3050
		public string name;

		// Token: 0x04000BEB RID: 3051
		public global::Item item;
	}
}
