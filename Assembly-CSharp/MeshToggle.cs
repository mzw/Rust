﻿using System;
using UnityEngine;

// Token: 0x02000750 RID: 1872
public class MeshToggle : MonoBehaviour
{
	// Token: 0x06002305 RID: 8965 RVA: 0x000C282C File Offset: 0x000C0A2C
	public void SwitchRenderer(int index)
	{
		if (this.RendererMeshes.Length == 0)
		{
			return;
		}
		MeshFilter component = base.GetComponent<MeshFilter>();
		if (!component)
		{
			return;
		}
		component.sharedMesh = this.RendererMeshes[Mathf.Clamp(index, 0, this.RendererMeshes.Length - 1)];
	}

	// Token: 0x06002306 RID: 8966 RVA: 0x000C2878 File Offset: 0x000C0A78
	public void SwitchRenderer(float factor)
	{
		int index = Mathf.RoundToInt(factor * (float)this.RendererMeshes.Length);
		this.SwitchRenderer(index);
	}

	// Token: 0x06002307 RID: 8967 RVA: 0x000C28A0 File Offset: 0x000C0AA0
	public void SwitchCollider(int index)
	{
		if (this.ColliderMeshes.Length == 0)
		{
			return;
		}
		MeshCollider component = base.GetComponent<MeshCollider>();
		if (!component)
		{
			return;
		}
		component.sharedMesh = this.ColliderMeshes[Mathf.Clamp(index, 0, this.ColliderMeshes.Length - 1)];
	}

	// Token: 0x06002308 RID: 8968 RVA: 0x000C28EC File Offset: 0x000C0AEC
	public void SwitchCollider(float factor)
	{
		int index = Mathf.RoundToInt(factor * (float)this.ColliderMeshes.Length);
		this.SwitchCollider(index);
	}

	// Token: 0x06002309 RID: 8969 RVA: 0x000C2914 File Offset: 0x000C0B14
	public void SwitchAll(int index)
	{
		this.SwitchRenderer(index);
		this.SwitchCollider(index);
	}

	// Token: 0x0600230A RID: 8970 RVA: 0x000C2924 File Offset: 0x000C0B24
	public void SwitchAll(float factor)
	{
		this.SwitchRenderer(factor);
		this.SwitchCollider(factor);
	}

	// Token: 0x04001F76 RID: 8054
	public Mesh[] RendererMeshes;

	// Token: 0x04001F77 RID: 8055
	public Mesh[] ColliderMeshes;
}
