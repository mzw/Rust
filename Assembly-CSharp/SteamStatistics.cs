﻿using System;
using System.Collections.Generic;
using ConVar;
using Rust;
using UnityEngine;

// Token: 0x020003D1 RID: 977
public class SteamStatistics
{
	// Token: 0x060016CE RID: 5838 RVA: 0x00083860 File Offset: 0x00081A60
	public SteamStatistics(global::BasePlayer p)
	{
		this.player = p;
	}

	// Token: 0x060016CF RID: 5839 RVA: 0x0008387C File Offset: 0x00081A7C
	public void Init()
	{
		if (Rust.Global.SteamServer == null)
		{
			return;
		}
		Rust.Global.SteamServer.Stats.Refresh(this.player.userID, new Action<ulong, bool>(this.OnStatsRefreshed));
		this.intStats.Clear();
	}

	// Token: 0x060016D0 RID: 5840 RVA: 0x000838BC File Offset: 0x00081ABC
	public void Save()
	{
		if (Rust.Global.SteamServer == null)
		{
			return;
		}
		Rust.Global.SteamServer.Stats.Commit(this.player.userID, null);
	}

	// Token: 0x060016D1 RID: 5841 RVA: 0x000838E4 File Offset: 0x00081AE4
	public void OnStatsRefreshed(ulong steamid, bool state)
	{
		this.hasRefreshed = true;
	}

	// Token: 0x060016D2 RID: 5842 RVA: 0x000838F0 File Offset: 0x00081AF0
	public void Add(string name, int var)
	{
		if (Rust.Global.SteamServer == null)
		{
			return;
		}
		if (!this.hasRefreshed)
		{
			return;
		}
		using (TimeWarning.New("PlayerStats.Add", 0.1f))
		{
			int num = 0;
			if (this.intStats.TryGetValue(name, out num))
			{
				Dictionary<string, int> dictionary;
				(dictionary = this.intStats)[name] = dictionary[name] + var;
				Rust.Global.SteamServer.Stats.SetInt(this.player.userID, name, this.intStats[name]);
			}
			else
			{
				num = Rust.Global.SteamServer.Stats.GetInt(this.player.userID, name, 0);
				if (!Rust.Global.SteamServer.Stats.SetInt(this.player.userID, name, num + var))
				{
					if (ConVar.Global.developer > 0)
					{
						Debug.LogWarning("[STEAMWORKS] Couldn't SetUserStat: " + name);
					}
				}
				else
				{
					this.intStats.Add(name, num + var);
				}
			}
		}
	}

	// Token: 0x04001195 RID: 4501
	private global::BasePlayer player;

	// Token: 0x04001196 RID: 4502
	public Dictionary<string, int> intStats = new Dictionary<string, int>();

	// Token: 0x04001197 RID: 4503
	private bool hasRefreshed;
}
