﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000031 RID: 49
public class BaseMelee : global::AttackEntity
{
	// Token: 0x06000459 RID: 1113 RVA: 0x000186DC File Offset: 0x000168DC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseMelee.OnRpcMessage", 0.1f))
		{
			if (rpc == 386279056u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - CLProject ");
				}
				using (TimeWarning.New("CLProject", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("CLProject", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.CLProject(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in CLProject");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 148642921u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - PlayerAttack ");
				}
				using (TimeWarning.New("PlayerAttack", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("PlayerAttack", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.PlayerAttack(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in PlayerAttack");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600045A RID: 1114 RVA: 0x00018A40 File Offset: 0x00016C40
	public override void GetAttackStats(global::HitInfo info)
	{
		info.damageTypes.Add(this.damageTypes);
		info.CanGather = this.gathering.Any();
	}

	// Token: 0x0600045B RID: 1115 RVA: 0x00018A64 File Offset: 0x00016C64
	public virtual void DoAttackShared(global::HitInfo info)
	{
		if (Interface.CallHook("OnPlayerAttack", new object[]
		{
			this.GetOwnerPlayer(),
			info
		}) != null)
		{
			return;
		}
		this.GetAttackStats(info);
		if (info.HitEntity != null)
		{
			using (TimeWarning.New("OnAttacked", 50L))
			{
				info.HitEntity.OnAttacked(info);
			}
		}
		if (base.isServer)
		{
			using (TimeWarning.New("ImpactEffect", 20L))
			{
				global::Effect.server.ImpactEffect(info);
			}
		}
		else
		{
			using (TimeWarning.New("ImpactEffect", 20L))
			{
				global::Effect.client.ImpactEffect(info);
			}
		}
		if (base.isServer && !base.IsDestroyed)
		{
			using (TimeWarning.New("UpdateItemCondition", 50L))
			{
				this.UpdateItemCondition(info);
			}
			base.StartAttackCooldown(this.repeatDelay * 0.5f);
		}
	}

	// Token: 0x0600045C RID: 1116 RVA: 0x00018BBC File Offset: 0x00016DBC
	public global::ResourceDispenser.GatherPropertyEntry GetGatherInfoFromIndex(global::ResourceDispenser.GatherType index)
	{
		return this.gathering.GetFromIndex(index);
	}

	// Token: 0x0600045D RID: 1117 RVA: 0x00018BCC File Offset: 0x00016DCC
	public virtual bool CanHit(global::HitTest info)
	{
		return true;
	}

	// Token: 0x0600045E RID: 1118 RVA: 0x00018BD0 File Offset: 0x00016DD0
	public float TotalDamage()
	{
		float num = 0f;
		foreach (Rust.DamageTypeEntry damageTypeEntry in this.damageTypes)
		{
			if (damageTypeEntry.amount > 0f)
			{
				num += damageTypeEntry.amount;
			}
		}
		return num;
	}

	// Token: 0x0600045F RID: 1119 RVA: 0x00018C4C File Offset: 0x00016E4C
	public bool IsItemBroken()
	{
		global::Item ownerItem = base.GetOwnerItem();
		return ownerItem == null || ownerItem.isBroken;
	}

	// Token: 0x06000460 RID: 1120 RVA: 0x00018C70 File Offset: 0x00016E70
	public void LoseCondition(float amount)
	{
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem == null)
		{
			return;
		}
		ownerItem.LoseCondition(amount);
	}

	// Token: 0x06000461 RID: 1121 RVA: 0x00018C94 File Offset: 0x00016E94
	public virtual float GetConditionLoss()
	{
		return 1f;
	}

	// Token: 0x06000462 RID: 1122 RVA: 0x00018C9C File Offset: 0x00016E9C
	public void UpdateItemCondition(global::HitInfo info)
	{
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem == null || !ownerItem.hasCondition)
		{
			return;
		}
		if (info != null && info.DidHit && !info.DidGather)
		{
			float num = this.GetConditionLoss();
			float num2 = 0f;
			foreach (Rust.DamageTypeEntry damageTypeEntry in this.damageTypes)
			{
				if (damageTypeEntry.amount > 0f)
				{
					num2 += Mathf.Clamp(damageTypeEntry.amount - info.damageTypes.Get(damageTypeEntry.type), 0f, damageTypeEntry.amount);
				}
			}
			num += num2 * 0.2f;
			ownerItem.LoseCondition(num);
		}
	}

	// Token: 0x06000463 RID: 1123 RVA: 0x00018D88 File Offset: 0x00016F88
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	public void PlayerAttack(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.VerifyClientAttack(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		using (TimeWarning.New("PlayerAttack", 50L))
		{
			using (PlayerAttack playerAttack = ProtoBuf.PlayerAttack.Deserialize(msg.read))
			{
				if (playerAttack != null)
				{
					global::HitInfo hitInfo = Facepunch.Pool.Get<global::HitInfo>();
					hitInfo.LoadFromAttack(playerAttack.attack, true);
					hitInfo.Initiator = player;
					hitInfo.Weapon = this;
					hitInfo.WeaponPrefab = this;
					hitInfo.Predicted = msg.connection;
					hitInfo.damageProperties = this.damageProperties;
					if (Interface.CallHook("OnMeleeAttack", new object[]
					{
						player,
						hitInfo
					}) != null)
					{
						return;
					}
					if (hitInfo.IsNaNOrInfinity())
					{
						string shortPrefabName = base.ShortPrefabName;
						global::AntiHack.Log(player, global::AntiHackType.MeleeHack, "Contains NaN (" + shortPrefabName + ")");
						player.stats.combat.Log(hitInfo, "melee_nan");
					}
					else
					{
						if (ConVar.AntiHack.melee_protection > 0 && hitInfo.HitEntity)
						{
							bool flag = true;
							float num = 1f + ConVar.AntiHack.melee_forgiveness;
							float melee_clientframes = ConVar.AntiHack.melee_clientframes;
							float melee_serverframes = ConVar.AntiHack.melee_serverframes;
							float num2 = melee_clientframes / 60f;
							float num3 = melee_serverframes * Mathx.Max(UnityEngine.Time.deltaTime, UnityEngine.Time.smoothDeltaTime, UnityEngine.Time.fixedDeltaTime);
							float num4 = (player.desyncTime + num2 + num3) * num;
							if (ConVar.AntiHack.projectile_protection >= 2)
							{
								float num5 = hitInfo.HitEntity.MaxVelocity();
								float num6 = hitInfo.HitEntity.BoundsPadding() + num4 * num5;
								float num7 = hitInfo.HitEntity.Distance(hitInfo.HitPositionWorld);
								if (num7 > num6)
								{
									string shortPrefabName2 = base.ShortPrefabName;
									string shortPrefabName3 = hitInfo.HitEntity.ShortPrefabName;
									global::AntiHack.Log(player, global::AntiHackType.MeleeHack, string.Concat(new object[]
									{
										"Entity too far away (",
										shortPrefabName2,
										" on ",
										shortPrefabName3,
										" with ",
										num7,
										"m > ",
										num6,
										"m in ",
										num4,
										"s)"
									}));
									player.stats.combat.Log(hitInfo, "melee_distance");
									flag = false;
								}
							}
							if (ConVar.AntiHack.melee_protection >= 1)
							{
								float num8 = hitInfo.Initiator.MaxVelocity();
								float num9 = hitInfo.Initiator.BoundsPadding() + num4 * num8 + num * this.maxDistance;
								float num10 = hitInfo.Initiator.Distance(hitInfo.HitPositionWorld);
								if (num10 > num9)
								{
									string shortPrefabName4 = base.ShortPrefabName;
									string shortPrefabName5 = hitInfo.HitEntity.ShortPrefabName;
									global::AntiHack.Log(player, global::AntiHackType.MeleeHack, string.Concat(new object[]
									{
										"Initiator too far away (",
										shortPrefabName4,
										" on ",
										shortPrefabName5,
										" with ",
										num10,
										"m > ",
										num9,
										"m in ",
										num4,
										"s)"
									}));
									player.stats.combat.Log(hitInfo, "melee_distance");
									flag = false;
								}
							}
							if (ConVar.AntiHack.melee_protection >= 3)
							{
								Vector3 pointStart = hitInfo.PointStart;
								Vector3 vector = hitInfo.HitPositionWorld + hitInfo.HitNormalWorld.normalized * 0.001f;
								Vector3 position = player.eyes.position;
								Vector3 vector2 = pointStart;
								Vector3 vector3 = hitInfo.PositionOnRay(vector);
								Vector3 vector4 = vector;
								bool flag2 = global::GamePhysics.LineOfSight(position, vector2, vector3, vector4, 2162688, 0f);
								if (!flag2)
								{
									player.stats.Add("hit_" + hitInfo.HitEntity.Categorize() + "_indirect_los", 1, global::Stats.Server);
								}
								else
								{
									player.stats.Add("hit_" + hitInfo.HitEntity.Categorize() + "_direct_los", 1, global::Stats.Server);
								}
								if (!flag2)
								{
									string shortPrefabName6 = base.ShortPrefabName;
									string shortPrefabName7 = hitInfo.HitEntity.ShortPrefabName;
									global::AntiHack.Log(player, global::AntiHackType.MeleeHack, string.Concat(new object[]
									{
										"Line of sight (",
										shortPrefabName6,
										" on ",
										shortPrefabName7,
										") ",
										position,
										" ",
										vector2,
										" ",
										vector3,
										" ",
										vector4
									}));
									player.stats.combat.Log(hitInfo, "melee_los");
									flag = false;
								}
							}
							if (!flag)
							{
								global::AntiHack.AddViolation(player, global::AntiHackType.MeleeHack, ConVar.AntiHack.melee_penalty);
								return;
							}
						}
						player.metabolism.UseHeart(this.heartStress * 0.2f);
						using (TimeWarning.New("DoAttackShared", 50L))
						{
							this.DoAttackShared(hitInfo);
						}
					}
				}
			}
		}
	}

	// Token: 0x06000464 RID: 1124 RVA: 0x000192F8 File Offset: 0x000174F8
	public override bool CanBeUsedInWater()
	{
		return true;
	}

	// Token: 0x06000465 RID: 1125 RVA: 0x000192FC File Offset: 0x000174FC
	public string GetStrikeEffectPath(string materialName)
	{
		for (int i = 0; i < this.materialStrikeFX.Count; i++)
		{
			if (this.materialStrikeFX[i].materialName == materialName && this.materialStrikeFX[i].fx.isValid)
			{
				return this.materialStrikeFX[i].fx.resourcePath;
			}
		}
		return this.strikeFX.resourcePath;
	}

	// Token: 0x06000466 RID: 1126 RVA: 0x00019380 File Offset: 0x00017580
	public override void ServerUse()
	{
		if (base.isClient)
		{
			return;
		}
		if (base.HasAttackCooldown())
		{
			return;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		base.StartAttackCooldown(this.repeatDelay * 2f);
		ownerPlayer.SignalBroadcast(global::BaseEntity.Signal.Attack, string.Empty, null);
		if (this.swingEffect.isValid)
		{
			global::Effect.server.Run(this.swingEffect.resourcePath, base.transform.position, Vector3.forward, ownerPlayer.net.connection, false);
		}
		if (base.IsInvoking(new Action(this.ServerUse_Strike)))
		{
			base.CancelInvoke(new Action(this.ServerUse_Strike));
		}
		base.Invoke(new Action(this.ServerUse_Strike), this.aiStrikeDelay);
	}

	// Token: 0x06000467 RID: 1127 RVA: 0x00019458 File Offset: 0x00017658
	public void ServerUse_Strike()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		Vector3 position = ownerPlayer.eyes.position;
		Vector3 vector = ownerPlayer.eyes.BodyForward();
		for (int i = 0; i < 2; i++)
		{
			List<RaycastHit> list = Facepunch.Pool.GetList<RaycastHit>();
			global::GamePhysics.TraceAll(new Ray(position - vector * ((i != 0) ? 0.2f : 0f), vector), (i != 0) ? this.attackRadius : 0f, list, this.effectiveRange + 0.2f, 1084435201, 0);
			bool flag = false;
			for (int j = 0; j < list.Count; j++)
			{
				RaycastHit hit = list[j];
				global::BaseEntity entity = hit.GetEntity();
				if (!(entity == null))
				{
					if (!(entity != null) || (!(entity == ownerPlayer) && !entity.EqualNetID(ownerPlayer)))
					{
						if (!(entity != null) || !entity.isClient)
						{
							if (!(entity.Categorize() == ownerPlayer.Categorize()))
							{
								float num = 0f;
								foreach (Rust.DamageTypeEntry damageTypeEntry in this.damageTypes)
								{
									num += damageTypeEntry.amount;
								}
								entity.OnAttacked(new global::HitInfo(this, entity, Rust.DamageType.Slash, num * this.npcDamageScale));
								global::HitInfo hitInfo = Facepunch.Pool.Get<global::HitInfo>();
								hitInfo.HitPositionWorld = hit.point;
								hitInfo.HitNormalWorld = -vector;
								if (entity is global::BaseNpc || entity is global::BasePlayer)
								{
									hitInfo.HitMaterial = global::StringPool.Get("Flesh");
								}
								else
								{
									hitInfo.HitMaterial = global::StringPool.Get((!(hit.GetCollider().sharedMaterial != null)) ? "generic" : hit.GetCollider().sharedMaterial.GetName());
								}
								global::Effect.server.ImpactEffect(hitInfo);
								Facepunch.Pool.Free<global::HitInfo>(ref hitInfo);
								flag = true;
								if (!(entity != null) || entity.ShouldBlockProjectiles())
								{
									break;
								}
							}
						}
					}
				}
			}
			Facepunch.Pool.FreeList<RaycastHit>(ref list);
			if (flag)
			{
				break;
			}
		}
	}

	// Token: 0x06000468 RID: 1128 RVA: 0x000196F8 File Offset: 0x000178F8
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.FromOwner]
	private void CLProject(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.VerifyClientAttack(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		if (!this.canThrowAsProjectile)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Not throwable (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "not_throwable");
			return;
		}
		global::Item item = this.GetItem();
		if (item == null)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Item not found (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "item_missing");
			return;
		}
		global::ItemModProjectile component = item.info.GetComponent<global::ItemModProjectile>();
		if (component == null)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Item mod not found (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "mod_missing");
			return;
		}
		ProjectileShoot projectileShoot = ProjectileShoot.Deserialize(msg.read);
		if (projectileShoot.projectiles.Count != 1)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Projectile count mismatch (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "count_mismatch");
			return;
		}
		player.CleanupExpiredProjectiles();
		foreach (ProjectileShoot.Projectile projectile in projectileShoot.projectiles)
		{
			if (player.HasFiredProjectile(projectile.projectileID))
			{
				global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Duplicate ID (" + projectile.projectileID + ")");
				player.stats.combat.Log(this, "duplicate_id");
			}
			else
			{
				if (!base.ValidateEyePos(player, projectile.startPos))
				{
					projectile.startPos = player.eyes.position;
				}
				player.NoteFiredProjectile(projectile.projectileID, projectile.startPos, projectile.startVel, this, item.info, item);
				global::Effect effect = new global::Effect();
				effect.Init(global::Effect.Type.Projectile, projectile.startPos, projectile.startVel, msg.connection);
				effect.scale = 1f;
				effect.pooledString = component.projectileObject.resourcePath;
				effect.number = projectile.seed;
				global::EffectNetwork.Send(effect);
			}
		}
		item.SetParent(null);
		Interface.CallHook("OnMeleeThrown", new object[]
		{
			player,
			item
		});
		if (player != null)
		{
			Rust.Ai.Sense.Stimulate(new Rust.Ai.Sensation
			{
				Type = Rust.Ai.SensationType.ThrownWeapon,
				Position = player.GetNetworkPosition(),
				Radius = 50f
			});
		}
	}

	// Token: 0x04000135 RID: 309
	[Header("Melee")]
	public global::DamageProperties damageProperties;

	// Token: 0x04000136 RID: 310
	public List<Rust.DamageTypeEntry> damageTypes;

	// Token: 0x04000137 RID: 311
	public float maxDistance = 1.5f;

	// Token: 0x04000138 RID: 312
	public float attackRadius = 0.3f;

	// Token: 0x04000139 RID: 313
	public bool isAutomatic = true;

	// Token: 0x0400013A RID: 314
	[Header("Effects")]
	public global::GameObjectRef strikeFX;

	// Token: 0x0400013B RID: 315
	[Header("NPCUsage")]
	public float aiStrikeDelay = 0.2f;

	// Token: 0x0400013C RID: 316
	public global::GameObjectRef swingEffect;

	// Token: 0x0400013D RID: 317
	public List<global::BaseMelee.MaterialFX> materialStrikeFX = new List<global::BaseMelee.MaterialFX>();

	// Token: 0x0400013E RID: 318
	[Header("Other")]
	[Range(0f, 1f)]
	public float heartStress = 0.5f;

	// Token: 0x0400013F RID: 319
	public global::ResourceDispenser.GatherProperties gathering;

	// Token: 0x04000140 RID: 320
	[Header("Throwing")]
	public bool canThrowAsProjectile;

	// Token: 0x02000032 RID: 50
	[Serializable]
	public class MaterialFX
	{
		// Token: 0x04000141 RID: 321
		public string materialName;

		// Token: 0x04000142 RID: 322
		public global::GameObjectRef fx;
	}
}
