﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200066A RID: 1642
public class ChatEntry : MonoBehaviour
{
	// Token: 0x04001BFF RID: 7167
	public Text text;

	// Token: 0x04001C00 RID: 7168
	public RawImage avatar;

	// Token: 0x04001C01 RID: 7169
	public CanvasGroup canvasGroup;

	// Token: 0x04001C02 RID: 7170
	public float lifeStarted;

	// Token: 0x04001C03 RID: 7171
	public ulong steamid;
}
