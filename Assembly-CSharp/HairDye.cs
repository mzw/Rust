﻿using System;
using UnityEngine;

// Token: 0x0200063B RID: 1595
[Serializable]
public class HairDye
{
	// Token: 0x06002051 RID: 8273 RVA: 0x000B8F40 File Offset: 0x000B7140
	public void Apply(MaterialPropertyBlock block)
	{
		block.SetColor("_BaseColor", this.baseColor);
		for (int i = 0; i < 8; i++)
		{
			if ((this.copyProperties & (global::HairDye.CopyPropertyMask)(1 << i)) != (global::HairDye.CopyPropertyMask)0)
			{
				global::MaterialPropertyDesc materialPropertyDesc = global::HairDye.transferableProps[i];
				if (this.sourceMaterial.HasProperty(materialPropertyDesc.name))
				{
					if (materialPropertyDesc.type == typeof(Color))
					{
						block.SetColor(materialPropertyDesc.name, this.sourceMaterial.GetColor(materialPropertyDesc.name));
					}
					else if (materialPropertyDesc.type == typeof(float))
					{
						block.SetFloat(materialPropertyDesc.name, this.sourceMaterial.GetFloat(materialPropertyDesc.name));
					}
				}
			}
		}
	}

	// Token: 0x04001B23 RID: 6947
	public Color baseColor;

	// Token: 0x04001B24 RID: 6948
	public Material sourceMaterial;

	// Token: 0x04001B25 RID: 6949
	[InspectorFlags]
	public global::HairDye.CopyPropertyMask copyProperties;

	// Token: 0x04001B26 RID: 6950
	private static global::MaterialPropertyDesc[] transferableProps = new global::MaterialPropertyDesc[]
	{
		new global::MaterialPropertyDesc("_DyeColor", typeof(Color)),
		new global::MaterialPropertyDesc("_RootColor", typeof(Color)),
		new global::MaterialPropertyDesc("_TipColor", typeof(Color)),
		new global::MaterialPropertyDesc("_Brightness", typeof(float)),
		new global::MaterialPropertyDesc("_DyeRoughness", typeof(float)),
		new global::MaterialPropertyDesc("_DyeScatter", typeof(float)),
		new global::MaterialPropertyDesc("_Specular", typeof(float)),
		new global::MaterialPropertyDesc("_Roughness", typeof(float))
	};

	// Token: 0x0200063C RID: 1596
	public enum CopyProperty
	{
		// Token: 0x04001B28 RID: 6952
		DyeColor,
		// Token: 0x04001B29 RID: 6953
		RootColor,
		// Token: 0x04001B2A RID: 6954
		TipColor,
		// Token: 0x04001B2B RID: 6955
		Brightness,
		// Token: 0x04001B2C RID: 6956
		DyeRoughness,
		// Token: 0x04001B2D RID: 6957
		DyeScatter,
		// Token: 0x04001B2E RID: 6958
		Specular,
		// Token: 0x04001B2F RID: 6959
		Roughness,
		// Token: 0x04001B30 RID: 6960
		Count
	}

	// Token: 0x0200063D RID: 1597
	[Flags]
	public enum CopyPropertyMask
	{
		// Token: 0x04001B32 RID: 6962
		DyeColor = 1,
		// Token: 0x04001B33 RID: 6963
		RootColor = 2,
		// Token: 0x04001B34 RID: 6964
		TipColor = 4,
		// Token: 0x04001B35 RID: 6965
		Brightness = 8,
		// Token: 0x04001B36 RID: 6966
		DyeRoughness = 16,
		// Token: 0x04001B37 RID: 6967
		DyeScatter = 32,
		// Token: 0x04001B38 RID: 6968
		Specular = 64,
		// Token: 0x04001B39 RID: 6969
		Roughness = 128
	}
}
