﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200008B RID: 139
public class RepairBench : global::StorageContainer
{
	// Token: 0x06000926 RID: 2342 RVA: 0x0003DC94 File Offset: 0x0003BE94
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("RepairBench.OnRpcMessage", 0.1f))
		{
			if (rpc == 2163974445u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ChangeSkin ");
				}
				using (TimeWarning.New("ChangeSkin", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ChangeSkin(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in ChangeSkin");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 2132347936u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RepairItem ");
				}
				using (TimeWarning.New("RepairItem", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RepairItem(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RepairItem");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000927 RID: 2343 RVA: 0x0003DF50 File Offset: 0x0003C150
	public float GetRepairFraction(global::Item itemToRepair)
	{
		return 1f - itemToRepair.condition / itemToRepair.maxCondition;
	}

	// Token: 0x06000928 RID: 2344 RVA: 0x0003DF68 File Offset: 0x0003C168
	public float RepairCostFraction(global::Item itemToRepair)
	{
		return this.GetRepairFraction(itemToRepair) * 0.2f;
	}

	// Token: 0x06000929 RID: 2345 RVA: 0x0003DF78 File Offset: 0x0003C178
	public List<global::ItemAmount> GetRepairCostList(global::ItemBlueprint bp)
	{
		List<global::ItemAmount> list = Facepunch.Pool.GetList<global::ItemAmount>();
		list.Clear();
		foreach (global::ItemAmount itemAmount in bp.ingredients)
		{
			list.Add(new global::ItemAmount(itemAmount.itemDef, itemAmount.amount));
		}
		foreach (global::ItemAmount itemAmount2 in bp.ingredients)
		{
			if (itemAmount2.itemDef.category == global::ItemCategory.Component && itemAmount2.itemDef.Blueprint != null)
			{
				bool flag = false;
				global::ItemAmount itemAmount3 = itemAmount2.itemDef.Blueprint.ingredients[0];
				foreach (global::ItemAmount itemAmount4 in list)
				{
					if (itemAmount4.itemDef == itemAmount3.itemDef)
					{
						itemAmount4.amount += itemAmount3.amount * itemAmount2.amount;
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					list.Add(new global::ItemAmount(itemAmount3.itemDef, itemAmount3.amount * itemAmount2.amount));
				}
			}
		}
		return list;
	}

	// Token: 0x0600092A RID: 2346 RVA: 0x0003E120 File Offset: 0x0003C320
	public void debugprint(string toPrint)
	{
		if (ConVar.Global.developer > 0)
		{
			Debug.LogWarning(toPrint);
		}
	}

	// Token: 0x0600092B RID: 2347 RVA: 0x0003E134 File Offset: 0x0003C334
	[global::BaseEntity.RPC_Server]
	public void ChangeSkin(global::BaseEntity.RPCMessage msg)
	{
		if (UnityEngine.Time.realtimeSinceStartup < this.nextSkinChangeTime)
		{
			return;
		}
		global::BasePlayer player = msg.player;
		int num = msg.read.Int32();
		global::Item slot = this.inventory.GetSlot(0);
		if (slot == null)
		{
			return;
		}
		if (num != 0 && !player.blueprints.steamInventory.HasItem(num))
		{
			this.debugprint("RepairBench.ChangeSkin player does not have item :" + num + ":");
			return;
		}
		ulong num2 = global::ItemDefinition.FindSkin(slot.info.itemid, num);
		if (num2 == slot.skin)
		{
			this.debugprint(string.Concat(new object[]
			{
				"RepairBench.ChangeSkin cannot apply same skin twice : ",
				num2,
				": ",
				slot.skin
			}));
			return;
		}
		this.nextSkinChangeTime = UnityEngine.Time.realtimeSinceStartup + 0.75f;
		slot.skin = num2;
		slot.MarkDirty();
		global::BaseEntity heldEntity = slot.GetHeldEntity();
		if (heldEntity != null)
		{
			heldEntity.skinID = num2;
			heldEntity.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
		if (this.skinchangeEffect.isValid)
		{
			global::Effect.server.Run(this.skinchangeEffect.resourcePath, this, 0u, new Vector3(0f, 1.5f, 0f), Vector3.zero, null, false);
		}
	}

	// Token: 0x0600092C RID: 2348 RVA: 0x0003E28C File Offset: 0x0003C48C
	[global::BaseEntity.RPC_Server]
	public void RepairItem(global::BaseEntity.RPCMessage msg)
	{
		global::Item slot = this.inventory.GetSlot(0);
		if (slot == null)
		{
			return;
		}
		global::ItemDefinition info = slot.info;
		global::ItemBlueprint component = info.GetComponent<global::ItemBlueprint>();
		if (!component)
		{
			return;
		}
		if (!info.condition.repairable)
		{
			return;
		}
		if (slot.condition == slot.maxCondition)
		{
			return;
		}
		global::BasePlayer player = msg.player;
		if (Interface.CallHook("OnItemRepair", new object[]
		{
			player,
			slot
		}) != null)
		{
			return;
		}
		if (!player.blueprints.HasUnlocked(info) && (!(info.Blueprint != null) || info.Blueprint.isResearchable))
		{
			return;
		}
		float num = this.RepairCostFraction(slot);
		bool flag = false;
		List<global::ItemAmount> repairCostList = this.GetRepairCostList(component);
		foreach (global::ItemAmount itemAmount in repairCostList)
		{
			if (itemAmount.itemDef.category != global::ItemCategory.Component)
			{
				int amount = player.inventory.GetAmount(itemAmount.itemDef.itemid);
				int num2 = Mathf.CeilToInt(itemAmount.amount * num);
				if (num2 > amount)
				{
					flag = true;
					break;
				}
			}
		}
		if (flag)
		{
			Facepunch.Pool.Free<List<global::ItemAmount>>(ref repairCostList);
			return;
		}
		foreach (global::ItemAmount itemAmount2 in repairCostList)
		{
			if (itemAmount2.itemDef.category != global::ItemCategory.Component)
			{
				int amount2 = Mathf.CeilToInt(itemAmount2.amount * num);
				player.inventory.Take(null, itemAmount2.itemid, amount2);
			}
		}
		Facepunch.Pool.Free<List<global::ItemAmount>>(ref repairCostList);
		slot.DoRepair(this.maxConditionLostOnRepair);
		if (ConVar.Global.developer > 0)
		{
			Debug.Log(string.Concat(new object[]
			{
				"Item repaired! condition : ",
				slot.condition,
				"/",
				slot.maxCondition
			}));
		}
		global::Effect.server.Run("assets/bundled/prefabs/fx/repairbench/itemrepair.prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
	}

	// Token: 0x0600092D RID: 2349 RVA: 0x0003E504 File Offset: 0x0003C704
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04000439 RID: 1081
	public float maxConditionLostOnRepair = 0.2f;

	// Token: 0x0400043A RID: 1082
	public global::GameObjectRef skinchangeEffect;

	// Token: 0x0400043B RID: 1083
	private float nextSkinChangeTime;
}
