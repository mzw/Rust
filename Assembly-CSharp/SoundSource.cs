﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000201 RID: 513
public class SoundSource : MonoBehaviour, global::IClientComponentEx, global::ILOD
{
	// Token: 0x06000F68 RID: 3944 RVA: 0x0005E094 File Offset: 0x0005C294
	public virtual void PreClientComponentCull(global::IPrefabProcessor p)
	{
		p.RemoveComponent(this);
	}

	// Token: 0x04000A12 RID: 2578
	[Header("Occlusion")]
	public bool handleOcclusionChecks;

	// Token: 0x04000A13 RID: 2579
	public LayerMask occlusionLayerMask;

	// Token: 0x04000A14 RID: 2580
	public List<global::SoundSource.OcclusionPoint> occlusionPoints = new List<global::SoundSource.OcclusionPoint>();

	// Token: 0x04000A15 RID: 2581
	public bool isOccluded;

	// Token: 0x04000A16 RID: 2582
	public float occlusionAmount;

	// Token: 0x04000A17 RID: 2583
	public float lodDistance = 100f;

	// Token: 0x04000A18 RID: 2584
	public bool inRange;

	// Token: 0x02000202 RID: 514
	[Serializable]
	public class OcclusionPoint
	{
		// Token: 0x04000A19 RID: 2585
		public Vector3 offset = Vector3.zero;

		// Token: 0x04000A1A RID: 2586
		public bool isOccluded;
	}
}
