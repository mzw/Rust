﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000714 RID: 1812
public class TweakUISlider : MonoBehaviour
{
	// Token: 0x06002272 RID: 8818 RVA: 0x000C0D80 File Offset: 0x000BEF80
	protected void Awake()
	{
		this.conVar = ConsoleSystem.Index.Client.Find(this.convarName);
		if (this.conVar != null)
		{
			this.UpdateSliderValue();
			this.UpdateTextValue();
		}
		else
		{
			Debug.LogWarning("Tweak Slider Convar Missing: " + this.convarName);
		}
	}

	// Token: 0x06002273 RID: 8819 RVA: 0x000C0DD0 File Offset: 0x000BEFD0
	protected void OnEnable()
	{
		this.UpdateSliderValue();
		this.UpdateTextValue();
	}

	// Token: 0x06002274 RID: 8820 RVA: 0x000C0DE0 File Offset: 0x000BEFE0
	public void OnChanged()
	{
		this.UpdateConVar();
		this.UpdateTextValue();
		this.UpdateSliderValue();
	}

	// Token: 0x06002275 RID: 8821 RVA: 0x000C0DF4 File Offset: 0x000BEFF4
	private void UpdateConVar()
	{
		if (this.conVar == null)
		{
			return;
		}
		float value = this.sliderControl.value;
		if (this.conVar.AsFloat == value)
		{
			return;
		}
		this.conVar.Set(value);
	}

	// Token: 0x06002276 RID: 8822 RVA: 0x000C0E38 File Offset: 0x000BF038
	private void UpdateSliderValue()
	{
		if (this.conVar == null)
		{
			return;
		}
		float asFloat = this.conVar.AsFloat;
		if (this.sliderControl.value == asFloat)
		{
			return;
		}
		this.sliderControl.value = asFloat;
	}

	// Token: 0x06002277 RID: 8823 RVA: 0x000C0E7C File Offset: 0x000BF07C
	private void UpdateTextValue()
	{
		if (this.sliderControl.wholeNumbers)
		{
			this.textControl.text = this.sliderControl.value.ToString("N0");
		}
		else
		{
			this.textControl.text = this.sliderControl.value.ToString("0.0");
		}
	}

	// Token: 0x04001EE1 RID: 7905
	public Slider sliderControl;

	// Token: 0x04001EE2 RID: 7906
	public Text textControl;

	// Token: 0x04001EE3 RID: 7907
	public string convarName = "effects.motionblur";

	// Token: 0x04001EE4 RID: 7908
	internal ConsoleSystem.Command conVar;
}
