﻿using System;

// Token: 0x020006C5 RID: 1733
public class SeedInformationPanel : global::ItemInformationPanel
{
	// Token: 0x04001D5D RID: 7517
	public global::ItemTextValue durationDisplay;

	// Token: 0x04001D5E RID: 7518
	public global::ItemTextValue waterRequiredDisplay;

	// Token: 0x04001D5F RID: 7519
	public global::ItemTextValue yieldDisplay;

	// Token: 0x04001D60 RID: 7520
	public global::ItemTextValue maxHarvestsDisplay;
}
