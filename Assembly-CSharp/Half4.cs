﻿using System;
using UnityEngine;

// Token: 0x020007AD RID: 1965
public struct Half4
{
	// Token: 0x06002492 RID: 9362 RVA: 0x000C9724 File Offset: 0x000C7924
	public Half4(Vector4 vec)
	{
		this.x = Mathf.FloatToHalf(vec.x);
		this.y = Mathf.FloatToHalf(vec.y);
		this.z = Mathf.FloatToHalf(vec.z);
		this.w = Mathf.FloatToHalf(vec.w);
	}

	// Token: 0x06002493 RID: 9363 RVA: 0x000C977C File Offset: 0x000C797C
	public static explicit operator Vector4(global::Half4 vec)
	{
		return new Vector4(Mathf.HalfToFloat(vec.x), Mathf.HalfToFloat(vec.y), Mathf.HalfToFloat(vec.z), Mathf.HalfToFloat(vec.w));
	}

	// Token: 0x04002008 RID: 8200
	public ushort x;

	// Token: 0x04002009 RID: 8201
	public ushort y;

	// Token: 0x0400200A RID: 8202
	public ushort z;

	// Token: 0x0400200B RID: 8203
	public ushort w;
}
