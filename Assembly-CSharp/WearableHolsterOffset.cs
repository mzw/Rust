﻿using System;
using UnityEngine;

// Token: 0x020004A9 RID: 1193
public class WearableHolsterOffset : MonoBehaviour
{
	// Token: 0x04001486 RID: 5254
	public global::WearableHolsterOffset.offsetInfo[] Offsets;

	// Token: 0x020004AA RID: 1194
	[Serializable]
	public class offsetInfo
	{
		// Token: 0x04001487 RID: 5255
		public global::HeldEntity.HolsterInfo.HolsterSlot type;

		// Token: 0x04001488 RID: 5256
		public Vector3 offset;

		// Token: 0x04001489 RID: 5257
		public Vector3 rotationOffset;

		// Token: 0x0400148A RID: 5258
		public int priority;
	}
}
