﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200007F RID: 127
public class MedicalTool : global::AttackEntity
{
	// Token: 0x06000880 RID: 2176 RVA: 0x000374F8 File Offset: 0x000356F8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("MedicalTool.OnRpcMessage", 0.1f))
		{
			if (rpc == 4026668521u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - UseOther ");
				}
				using (TimeWarning.New("UseOther", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("UseOther", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.UseOther(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in UseOther");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 4147870035u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - UseSelf ");
				}
				using (TimeWarning.New("UseSelf", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("UseSelf", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.UseSelf(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in UseSelf");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000881 RID: 2177 RVA: 0x0003785C File Offset: 0x00035A5C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void UseOther(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.VerifyClientAttack(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		if (!player.CanInteract())
		{
			return;
		}
		if (!base.HasItemAmount() || !this.canUseOnOther)
		{
			return;
		}
		global::BasePlayer basePlayer = global::BaseNetworkable.serverEntities.Find(msg.read.UInt32()) as global::BasePlayer;
		if (basePlayer != null && Vector3.Distance(basePlayer.transform.position, player.transform.position) < 4f)
		{
			base.ClientRPCPlayer(null, player, "Reset");
			this.GiveEffectsTo(basePlayer);
			base.UseItemAmount(1);
			base.StartAttackCooldown(this.repeatDelay);
		}
	}

	// Token: 0x06000882 RID: 2178 RVA: 0x00037920 File Offset: 0x00035B20
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void UseSelf(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.VerifyClientAttack(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		if (!player.CanInteract())
		{
			return;
		}
		if (!base.HasItemAmount())
		{
			return;
		}
		base.ClientRPCPlayer(null, player, "Reset");
		this.GiveEffectsTo(player);
		base.UseItemAmount(1);
		base.StartAttackCooldown(this.repeatDelay);
	}

	// Token: 0x06000883 RID: 2179 RVA: 0x0003798C File Offset: 0x00035B8C
	private void GiveEffectsTo(global::BasePlayer player)
	{
		if (!player)
		{
			return;
		}
		global::ItemModConsumable component = base.GetOwnerItemDefinition().GetComponent<global::ItemModConsumable>();
		if (!component)
		{
			Debug.LogWarning("No consumable for medicaltool :" + base.name);
			return;
		}
		if (Interface.CallHook("OnHealingItemUse", new object[]
		{
			this,
			player
		}) != null)
		{
			return;
		}
		if (player != base.GetOwnerPlayer() && player.IsWounded() && this.canRevive)
		{
			if (Interface.CallHook("OnPlayerRevive", new object[]
			{
				this.GetOwnerPlayer(),
				player
			}) != null)
			{
				return;
			}
			player.StopWounded();
		}
		foreach (global::ItemModConsumable.ConsumableEffect consumableEffect in component.effects)
		{
			if (consumableEffect.type == global::MetabolismAttribute.Type.Health)
			{
				player.health += consumableEffect.amount;
			}
			else
			{
				player.metabolism.ApplyChange(consumableEffect.type, consumableEffect.amount, consumableEffect.time);
			}
		}
	}

	// Token: 0x040003F5 RID: 1013
	public float healDurationSelf = 4f;

	// Token: 0x040003F6 RID: 1014
	public float healDurationOther = 4f;

	// Token: 0x040003F7 RID: 1015
	public float maxDistanceOther = 2f;

	// Token: 0x040003F8 RID: 1016
	public bool canUseOnOther = true;

	// Token: 0x040003F9 RID: 1017
	public bool canRevive = true;
}
