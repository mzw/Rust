﻿using System;
using UnityEngine;

// Token: 0x02000218 RID: 536
public class ModelConditionTest_RoofBottom : global::ModelConditionTest
{
	// Token: 0x06000FAF RID: 4015 RVA: 0x0005FD30 File Offset: 0x0005DF30
	protected void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.gray;
		Gizmos.DrawWireCube(new Vector3(0f, -1.5f, 3f), new Vector3(3f, 3f, 3f));
	}

	// Token: 0x06000FB0 RID: 4016 RVA: 0x0005FD84 File Offset: 0x0005DF84
	public override bool DoTest(global::BaseEntity ent)
	{
		global::EntityLink entityLink = ent.FindLink("roof/sockets/wall-male");
		if (entityLink == null)
		{
			return false;
		}
		for (int i = 0; i < entityLink.connections.Count; i++)
		{
			global::EntityLink entityLink2 = entityLink.connections[i];
			if (entityLink2.name == "roof/sockets/wall-female")
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x04000A7D RID: 2685
	private const string socket = "roof/sockets/wall-male";

	// Token: 0x04000A7E RID: 2686
	private const string socket_female = "roof/sockets/wall-female";
}
