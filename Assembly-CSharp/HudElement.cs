﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000689 RID: 1673
public class HudElement : MonoBehaviour
{
	// Token: 0x060020F9 RID: 8441 RVA: 0x000BB234 File Offset: 0x000B9434
	public void SetValue(float value, float max = 1f)
	{
		using (TimeWarning.New("HudElement.SetValue", 0.1f))
		{
			float num = value / max;
			if (num != this.LastValue)
			{
				this.LastValue = num;
				this.SetText(value.ToString("0"));
				this.SetImage(num);
			}
		}
	}

	// Token: 0x060020FA RID: 8442 RVA: 0x000BB2A8 File Offset: 0x000B94A8
	private void SetText(string v)
	{
		for (int i = 0; i < this.ValueText.Length; i++)
		{
			this.ValueText[i].text = v;
		}
	}

	// Token: 0x060020FB RID: 8443 RVA: 0x000BB2DC File Offset: 0x000B94DC
	private void SetImage(float f)
	{
		for (int i = 0; i < this.FilledImage.Length; i++)
		{
			this.FilledImage[i].fillAmount = f;
		}
	}

	// Token: 0x04001C58 RID: 7256
	public Text[] ValueText;

	// Token: 0x04001C59 RID: 7257
	public Image[] FilledImage;

	// Token: 0x04001C5A RID: 7258
	private float LastValue;
}
