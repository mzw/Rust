﻿using System;
using System.Linq;
using UnityEngine;

// Token: 0x0200064A RID: 1610
[CreateAssetMenu(menuName = "Rust/Loot Spawn")]
public class LootSpawn : ScriptableObject
{
	// Token: 0x0600206E RID: 8302 RVA: 0x000B9630 File Offset: 0x000B7830
	public global::ItemDefinition GetBlueprintBaseDef()
	{
		return global::ItemManager.FindItemDefinition("blueprintbase");
	}

	// Token: 0x0600206F RID: 8303 RVA: 0x000B963C File Offset: 0x000B783C
	public void SpawnIntoContainer(global::ItemContainer container)
	{
		if (this.subSpawn != null && this.subSpawn.Length > 0)
		{
			this.SubCategoryIntoContainer(container);
			return;
		}
		if (this.items != null)
		{
			foreach (global::ItemAmountRanged itemAmountRanged in this.items)
			{
				if (itemAmountRanged != null)
				{
					global::Item item2;
					if (itemAmountRanged.itemDef.spawnAsBlueprint)
					{
						global::ItemDefinition blueprintBaseDef = this.GetBlueprintBaseDef();
						if (blueprintBaseDef == null)
						{
							goto IL_111;
						}
						global::Item item = global::ItemManager.Create(blueprintBaseDef, 1, 0UL);
						item.blueprintTarget = itemAmountRanged.itemDef.itemid;
						item2 = item;
					}
					else
					{
						item2 = global::ItemManager.CreateByItemID(itemAmountRanged.itemid, (int)itemAmountRanged.GetAmount(), 0UL);
					}
					if (item2 != null)
					{
						item2.OnVirginSpawn();
						if (!item2.MoveToContainer(container, -1, true))
						{
							if (container.playerOwner)
							{
								item2.Drop(container.playerOwner.GetDropPosition(), container.playerOwner.GetDropVelocity(), default(Quaternion));
							}
							else
							{
								item2.Remove(0f);
							}
						}
					}
				}
				IL_111:;
			}
		}
	}

	// Token: 0x06002070 RID: 8304 RVA: 0x000B9768 File Offset: 0x000B7968
	private void SubCategoryIntoContainer(global::ItemContainer container)
	{
		int num = this.subSpawn.Sum((global::LootSpawn.Entry x) => x.weight);
		int num2 = Random.Range(0, num);
		for (int i = 0; i < this.subSpawn.Length; i++)
		{
			if (!(this.subSpawn[i].category == null))
			{
				num -= this.subSpawn[i].weight;
				if (num2 >= num)
				{
					this.subSpawn[i].category.SpawnIntoContainer(container);
					return;
				}
			}
		}
		Debug.LogWarning("SubCategoryIntoContainer: This should never happen!", this);
	}

	// Token: 0x04001B5D RID: 7005
	public global::ItemAmountRanged[] items;

	// Token: 0x04001B5E RID: 7006
	public global::LootSpawn.Entry[] subSpawn;

	// Token: 0x0200064B RID: 1611
	[Serializable]
	public struct Entry
	{
		// Token: 0x04001B60 RID: 7008
		[Tooltip("If a subcategory exists we'll choose from there instead of any items specified")]
		public global::LootSpawn category;

		// Token: 0x04001B61 RID: 7009
		[Tooltip("The higher this number, the more likely this will be chosen")]
		public int weight;
	}
}
