﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x02000740 RID: 1856
public class ConvarComponent : MonoBehaviour
{
	// Token: 0x060022DC RID: 8924 RVA: 0x000C1E84 File Offset: 0x000C0084
	protected void OnEnable()
	{
		if (!this.ShouldRun())
		{
			return;
		}
		foreach (global::ConvarComponent.ConvarEvent convarEvent in this.List)
		{
			convarEvent.OnEnable();
		}
	}

	// Token: 0x060022DD RID: 8925 RVA: 0x000C1EEC File Offset: 0x000C00EC
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (!this.ShouldRun())
		{
			return;
		}
		foreach (global::ConvarComponent.ConvarEvent convarEvent in this.List)
		{
			convarEvent.OnDisable();
		}
	}

	// Token: 0x060022DE RID: 8926 RVA: 0x000C1F60 File Offset: 0x000C0160
	protected void OnValidate()
	{
		if (!this.ShouldRun())
		{
			return;
		}
		foreach (global::ConvarComponent.ConvarEvent convarEvent in this.List)
		{
			if (convarEvent.component == null)
			{
				Debug.LogWarning("[ConvarComponent] Component is NULL on " + convarEvent.convar, base.gameObject);
			}
		}
	}

	// Token: 0x060022DF RID: 8927 RVA: 0x000C1FF0 File Offset: 0x000C01F0
	private bool ShouldRun()
	{
		return this.runOnServer;
	}

	// Token: 0x04001F4A RID: 8010
	public bool runOnServer = true;

	// Token: 0x04001F4B RID: 8011
	public bool runOnClient = true;

	// Token: 0x04001F4C RID: 8012
	public List<global::ConvarComponent.ConvarEvent> List = new List<global::ConvarComponent.ConvarEvent>();

	// Token: 0x02000741 RID: 1857
	[Serializable]
	public class ConvarEvent
	{
		// Token: 0x060022E1 RID: 8929 RVA: 0x000C2008 File Offset: 0x000C0208
		public void OnEnable()
		{
			this.cmd = ConsoleSystem.Index.Client.Find(this.convar);
			if (this.cmd == null)
			{
				this.cmd = ConsoleSystem.Index.Server.Find(this.convar);
			}
			if (this.cmd == null)
			{
				return;
			}
			this.cmd.OnValueChanged += this.cmd_OnValueChanged;
			this.cmd_OnValueChanged(this.cmd);
		}

		// Token: 0x060022E2 RID: 8930 RVA: 0x000C2074 File Offset: 0x000C0274
		private void cmd_OnValueChanged(ConsoleSystem.Command obj)
		{
			if (this.component == null)
			{
				return;
			}
			bool flag = obj.String == this.on;
			if (this.component.enabled == flag)
			{
				return;
			}
			this.component.enabled = flag;
		}

		// Token: 0x060022E3 RID: 8931 RVA: 0x000C20C4 File Offset: 0x000C02C4
		public void OnDisable()
		{
			if (Application.isQuitting)
			{
				return;
			}
			if (this.cmd == null)
			{
				return;
			}
			this.cmd.OnValueChanged -= this.cmd_OnValueChanged;
		}

		// Token: 0x04001F4D RID: 8013
		public string convar;

		// Token: 0x04001F4E RID: 8014
		public string on;

		// Token: 0x04001F4F RID: 8015
		public MonoBehaviour component;

		// Token: 0x04001F50 RID: 8016
		internal ConsoleSystem.Command cmd;
	}
}
