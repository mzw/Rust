﻿using System;

// Token: 0x020005FC RID: 1532
[Serializable]
public struct ExtendGBufferParams
{
	// Token: 0x04001A40 RID: 6720
	public bool enabled;

	// Token: 0x04001A41 RID: 6721
	public static global::ExtendGBufferParams Default = new global::ExtendGBufferParams
	{
		enabled = false
	};
}
