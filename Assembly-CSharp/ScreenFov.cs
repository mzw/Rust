﻿using System;
using UnityEngine;

// Token: 0x02000335 RID: 821
public class ScreenFov : global::BaseScreenShake
{
	// Token: 0x060013C8 RID: 5064 RVA: 0x00073F98 File Offset: 0x00072198
	public override void Setup()
	{
	}

	// Token: 0x060013C9 RID: 5065 RVA: 0x00073F9C File Offset: 0x0007219C
	public override void Run(float delta, ref global::CachedTransform<Camera> cam, ref global::CachedTransform<global::BaseViewModel> vm)
	{
		if (cam)
		{
			cam.component.fieldOfView += this.FovAdjustment.Evaluate(delta);
		}
	}

	// Token: 0x04000EB5 RID: 3765
	public AnimationCurve FovAdjustment;
}
