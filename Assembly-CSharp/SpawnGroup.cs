﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using UnityEngine;

// Token: 0x0200048D RID: 1165
public class SpawnGroup : global::BaseMonoBehaviour, IServerComponent
{
	// Token: 0x170001BC RID: 444
	// (get) Token: 0x0600194F RID: 6479 RVA: 0x0008E914 File Offset: 0x0008CB14
	public int currentPopulation
	{
		get
		{
			return this.spawnInstances.Count;
		}
	}

	// Token: 0x06001950 RID: 6480 RVA: 0x0008E924 File Offset: 0x0008CB24
	public virtual bool WantsInitialSpawn()
	{
		return this.wantsInitialSpawn;
	}

	// Token: 0x06001951 RID: 6481 RVA: 0x0008E92C File Offset: 0x0008CB2C
	public virtual bool WantsTimedSpawn()
	{
		return this.respawnDelayMax != float.PositiveInfinity;
	}

	// Token: 0x06001952 RID: 6482 RVA: 0x0008E940 File Offset: 0x0008CB40
	public float GetSpawnDelta()
	{
		return (this.respawnDelayMax + this.respawnDelayMin) * 0.5f / global::SpawnHandler.PlayerScale(ConVar.Spawn.player_scale);
	}

	// Token: 0x06001953 RID: 6483 RVA: 0x0008E960 File Offset: 0x0008CB60
	public float GetSpawnVariance()
	{
		return (this.respawnDelayMax - this.respawnDelayMin) * 0.5f / global::SpawnHandler.PlayerScale(ConVar.Spawn.player_scale);
	}

	// Token: 0x06001954 RID: 6484 RVA: 0x0008E980 File Offset: 0x0008CB80
	protected void Awake()
	{
		this.spawnPoints = base.GetComponentsInChildren<global::BaseSpawnPoint>();
		if (this.WantsTimedSpawn())
		{
			this.spawnClock.Add(this.GetSpawnDelta(), this.GetSpawnVariance(), new Action(this.Spawn));
		}
		if (!this.temporary && SingletonComponent<global::SpawnHandler>.Instance)
		{
			SingletonComponent<global::SpawnHandler>.Instance.SpawnGroups.Add(this);
		}
	}

	// Token: 0x06001955 RID: 6485 RVA: 0x0008E9F4 File Offset: 0x0008CBF4
	public void Fill()
	{
		this.Spawn(this.maxPopulation);
	}

	// Token: 0x06001956 RID: 6486 RVA: 0x0008EA04 File Offset: 0x0008CC04
	public void Clear()
	{
		foreach (global::SpawnPointInstance spawnPointInstance in this.spawnInstances)
		{
			global::BaseEntity baseEntity = spawnPointInstance.gameObject.ToBaseEntity();
			if (baseEntity)
			{
				baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
			}
		}
		this.spawnInstances.Clear();
	}

	// Token: 0x06001957 RID: 6487 RVA: 0x0008EA84 File Offset: 0x0008CC84
	public virtual void SpawnInitial()
	{
		if (!this.wantsInitialSpawn)
		{
			return;
		}
		if (this.fillOnSpawn)
		{
			this.Spawn(this.maxPopulation);
		}
		else
		{
			this.Spawn();
		}
	}

	// Token: 0x06001958 RID: 6488 RVA: 0x0008EAB4 File Offset: 0x0008CCB4
	public void SpawnRepeating()
	{
		for (int i = 0; i < this.spawnClock.events.Count; i++)
		{
			global::LocalClock.TimedEvent value = this.spawnClock.events[i];
			if (UnityEngine.Time.time > value.time)
			{
				value.delta = this.GetSpawnDelta();
				value.variance = this.GetSpawnVariance();
				this.spawnClock.events[i] = value;
			}
		}
		this.spawnClock.Tick();
	}

	// Token: 0x06001959 RID: 6489 RVA: 0x0008EB3C File Offset: 0x0008CD3C
	public void ObjectSpawned(global::SpawnPointInstance instance)
	{
		this.spawnInstances.Add(instance);
	}

	// Token: 0x0600195A RID: 6490 RVA: 0x0008EB4C File Offset: 0x0008CD4C
	public void ObjectRetired(global::SpawnPointInstance instance)
	{
		this.spawnInstances.Remove(instance);
	}

	// Token: 0x0600195B RID: 6491 RVA: 0x0008EB5C File Offset: 0x0008CD5C
	private void Spawn()
	{
		this.Spawn(Random.Range(this.numToSpawnPerTickMin, this.numToSpawnPerTickMax + 1));
	}

	// Token: 0x0600195C RID: 6492 RVA: 0x0008EB78 File Offset: 0x0008CD78
	protected virtual void Spawn(int numToSpawn)
	{
		numToSpawn = Mathf.Min(numToSpawn, this.maxPopulation - this.currentPopulation);
		for (int i = 0; i < numToSpawn; i++)
		{
			Vector3 pos;
			Quaternion rot;
			global::BaseSpawnPoint spawnPoint = this.GetSpawnPoint(out pos, out rot);
			if (spawnPoint)
			{
				global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.GetPrefab(), pos, rot, true);
				if (baseEntity)
				{
					baseEntity.Spawn();
					global::SpawnPointInstance spawnPointInstance = baseEntity.gameObject.AddComponent<global::SpawnPointInstance>();
					spawnPointInstance.parentSpawnGroup = this;
					spawnPointInstance.parentSpawnPoint = spawnPoint;
					spawnPointInstance.Notify();
				}
			}
		}
	}

	// Token: 0x0600195D RID: 6493 RVA: 0x0008EC1C File Offset: 0x0008CE1C
	protected string GetPrefab()
	{
		float num = (float)this.prefabs.Sum((global::SpawnGroup.SpawnEntry x) => x.weight);
		if (num == 0f)
		{
			return null;
		}
		float num2 = Random.Range(0f, num);
		foreach (global::SpawnGroup.SpawnEntry spawnEntry in this.prefabs)
		{
			if ((num2 -= (float)spawnEntry.weight) <= 0f)
			{
				return spawnEntry.prefab.resourcePath;
			}
		}
		return this.prefabs[this.prefabs.Count - 1].prefab.resourcePath;
	}

	// Token: 0x0600195E RID: 6494 RVA: 0x0008ED00 File Offset: 0x0008CF00
	protected virtual global::BaseSpawnPoint GetSpawnPoint(out Vector3 pos, out Quaternion rot)
	{
		global::BaseSpawnPoint baseSpawnPoint = null;
		pos = Vector3.zero;
		rot = Quaternion.identity;
		int num = Random.Range(0, this.spawnPoints.Length);
		for (int i = 0; i < this.spawnPoints.Length; i++)
		{
			baseSpawnPoint = this.spawnPoints[(num + i) % this.spawnPoints.Length];
			if (baseSpawnPoint && baseSpawnPoint.gameObject.activeSelf)
			{
				break;
			}
		}
		if (baseSpawnPoint)
		{
			baseSpawnPoint.GetLocation(out pos, out rot);
		}
		return baseSpawnPoint;
	}

	// Token: 0x0600195F RID: 6495 RVA: 0x0008ED98 File Offset: 0x0008CF98
	protected virtual void OnDrawGizmos()
	{
		Gizmos.color = new Color(1f, 1f, 0f, 1f);
		Gizmos.DrawSphere(base.transform.position, 0.25f);
	}

	// Token: 0x040013FE RID: 5118
	public List<global::SpawnGroup.SpawnEntry> prefabs;

	// Token: 0x040013FF RID: 5119
	public int maxPopulation = 5;

	// Token: 0x04001400 RID: 5120
	public int numToSpawnPerTickMin = 1;

	// Token: 0x04001401 RID: 5121
	public int numToSpawnPerTickMax = 2;

	// Token: 0x04001402 RID: 5122
	public float respawnDelayMin = 10f;

	// Token: 0x04001403 RID: 5123
	public float respawnDelayMax = 20f;

	// Token: 0x04001404 RID: 5124
	public bool wantsInitialSpawn = true;

	// Token: 0x04001405 RID: 5125
	public bool temporary;

	// Token: 0x04001406 RID: 5126
	private bool fillOnSpawn;

	// Token: 0x04001407 RID: 5127
	protected global::BaseSpawnPoint[] spawnPoints;

	// Token: 0x04001408 RID: 5128
	private List<global::SpawnPointInstance> spawnInstances = new List<global::SpawnPointInstance>();

	// Token: 0x04001409 RID: 5129
	private global::LocalClock spawnClock = new global::LocalClock();

	// Token: 0x0200048E RID: 1166
	[Serializable]
	public class SpawnEntry
	{
		// Token: 0x0400140B RID: 5131
		public global::GameObjectRef prefab;

		// Token: 0x0400140C RID: 5132
		public int weight = 1;

		// Token: 0x0400140D RID: 5133
		public bool mobile;
	}
}
