﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000513 RID: 1299
public static class ImageProcessing
{
	// Token: 0x06001B81 RID: 7041 RVA: 0x00099354 File Offset: 0x00097554
	public static void GaussianBlur2D(float[,] data)
	{
		global::ImageProcessing.GaussianBlur2D(data, 1);
	}

	// Token: 0x06001B82 RID: 7042 RVA: 0x00099360 File Offset: 0x00097560
	public static void GaussianBlur2D(float[,] data, int iterations)
	{
		int length = data.GetLength(0);
		int length2 = data.GetLength(1);
		float[,] array = data;
		float[,] array2 = new float[length, length2];
		for (int i = 0; i < iterations; i++)
		{
			for (int j = 0; j < length; j++)
			{
				int num = Mathf.Max(0, j - 1);
				int num2 = Mathf.Min(length - 1, j + 1);
				for (int k = 0; k < length2; k++)
				{
					int num3 = Mathf.Max(0, k - 1);
					int num4 = Mathf.Min(length2 - 1, k + 1);
					float num5 = array[j, k] * 4f + array[j, num3] + array[j, num4] + array[num, k] + array[num2, k];
					array2[j, k] = num5 * 0.125f;
				}
			}
			global::GenericsUtil.Swap<float[,]>(ref array, ref array2);
		}
		if (array != data)
		{
			Buffer.BlockCopy(array, 0, data, 0, data.Length * 4);
		}
	}

	// Token: 0x06001B83 RID: 7043 RVA: 0x00099470 File Offset: 0x00097670
	public static void GaussianBlur2D(float[,,] data)
	{
		global::ImageProcessing.GaussianBlur2D(data, 1);
	}

	// Token: 0x06001B84 RID: 7044 RVA: 0x0009947C File Offset: 0x0009767C
	public static void GaussianBlur2D(float[,,] data, int iterations)
	{
		int len1 = data.GetLength(0);
		int len2 = data.GetLength(1);
		int len3 = data.GetLength(2);
		float[,,] src = data;
		float[,,] dst = new float[len1, len2, len3];
		for (int i = 0; i < iterations; i++)
		{
			Parallel.For(0, len1, delegate(int x)
			{
				int num = Mathf.Max(0, x - 1);
				int num2 = Mathf.Min(len1 - 1, x + 1);
				for (int j = 0; j < len2; j++)
				{
					int num3 = Mathf.Max(0, j - 1);
					int num4 = Mathf.Min(len2 - 1, j + 1);
					for (int k = 0; k < len3; k++)
					{
						float num5 = src[x, j, k] * 4f + src[x, num3, k] + src[x, num4, k] + src[num, j, k] + src[num2, j, k];
						dst[x, j, k] = num5 * 0.125f;
					}
				}
			});
			global::GenericsUtil.Swap<float[,,]>(ref src, ref dst);
		}
		if (src != data)
		{
			Buffer.BlockCopy(src, 0, data, 0, data.Length * 4);
		}
	}

	// Token: 0x06001B85 RID: 7045 RVA: 0x00099538 File Offset: 0x00097738
	public static void Average2D(float[,] data)
	{
		global::ImageProcessing.Average2D(data, 1);
	}

	// Token: 0x06001B86 RID: 7046 RVA: 0x00099544 File Offset: 0x00097744
	public static void Average2D(float[,] data, int iterations)
	{
		int len1 = data.GetLength(0);
		int len2 = data.GetLength(1);
		float[,] src = data;
		float[,] dst = new float[len1, len2];
		for (int i = 0; i < iterations; i++)
		{
			Parallel.For(0, len1, delegate(int x)
			{
				int num = Mathf.Max(0, x - 1);
				int num2 = Mathf.Min(len1 - 1, x + 1);
				for (int j = 0; j < len2; j++)
				{
					int num3 = Mathf.Max(0, j - 1);
					int num4 = Mathf.Min(len2 - 1, j + 1);
					float num5 = src[x, j] + src[x, num3] + src[x, num4] + src[num, j] + src[num2, j];
					dst[x, j] = num5 * 0.2f;
				}
			});
			global::GenericsUtil.Swap<float[,]>(ref src, ref dst);
		}
		if (src != data)
		{
			Buffer.BlockCopy(src, 0, data, 0, data.Length * 4);
		}
	}

	// Token: 0x06001B87 RID: 7047 RVA: 0x000995EC File Offset: 0x000977EC
	public static void Average2D(float[,,] data)
	{
		global::ImageProcessing.Average2D(data, 1);
	}

	// Token: 0x06001B88 RID: 7048 RVA: 0x000995F8 File Offset: 0x000977F8
	public static void Average2D(float[,,] data, int iterations)
	{
		int len1 = data.GetLength(0);
		int len2 = data.GetLength(1);
		int len3 = data.GetLength(2);
		float[,,] src = data;
		float[,,] dst = new float[len1, len2, len3];
		for (int i = 0; i < iterations; i++)
		{
			Parallel.For(0, len1, delegate(int x)
			{
				int num = Mathf.Max(0, x - 1);
				int num2 = Mathf.Min(len1 - 1, x + 1);
				for (int j = 0; j < len2; j++)
				{
					int num3 = Mathf.Max(0, j - 1);
					int num4 = Mathf.Min(len2 - 1, j + 1);
					for (int k = 0; k < len3; k++)
					{
						float num5 = src[x, j, k] + src[x, num3, k] + src[x, num4, k] + src[num, j, k] + src[num2, j, k];
						dst[x, j, k] = num5 * 0.2f;
					}
				}
			});
			global::GenericsUtil.Swap<float[,,]>(ref src, ref dst);
		}
		if (src != data)
		{
			Buffer.BlockCopy(src, 0, data, 0, data.Length * 4);
		}
	}

	// Token: 0x06001B89 RID: 7049 RVA: 0x000996B4 File Offset: 0x000978B4
	public static void Upsample2D(float[,] src, float[,] dst)
	{
		int srclen1 = src.GetLength(0);
		int srclen2 = src.GetLength(1);
		int length = dst.GetLength(0);
		int length2 = dst.GetLength(1);
		if (2 * srclen1 != length || 2 * srclen2 != length2)
		{
			return;
		}
		Parallel.For(0, srclen1, delegate(int x)
		{
			int num = Mathf.Max(0, x - 1);
			int num2 = Mathf.Min(srclen1 - 1, x + 1);
			for (int i = 0; i < srclen2; i++)
			{
				int num3 = Mathf.Max(0, i - 1);
				int num4 = Mathf.Min(srclen2 - 1, i + 1);
				float num5 = src[x, i] * 6f;
				float num6 = num5 + src[num, i] + src[x, num3];
				dst[2 * x, 2 * i] = num6 * 0.125f;
				float num7 = num5 + src[num2, i] + src[x, num3];
				dst[2 * x + 1, 2 * i] = num7 * 0.125f;
				float num8 = num5 + src[num, i] + src[x, num4];
				dst[2 * x, 2 * i + 1] = num8 * 0.125f;
				float num9 = num5 + src[num2, i] + src[x, num4];
				dst[2 * x + 1, 2 * i + 1] = num9 * 0.125f;
			}
		});
	}

	// Token: 0x06001B8A RID: 7050 RVA: 0x00099748 File Offset: 0x00097948
	public static void Upsample2D(float[,,] src, float[,,] dst)
	{
		int srclen1 = src.GetLength(0);
		int srclen2 = src.GetLength(1);
		int srclen3 = src.GetLength(2);
		int length = dst.GetLength(0);
		int length2 = dst.GetLength(1);
		int length3 = src.GetLength(2);
		if (2 * srclen1 != length || 2 * srclen2 != length2 || srclen3 != length3)
		{
			return;
		}
		Parallel.For(0, srclen1, delegate(int x)
		{
			int num = Mathf.Max(0, x - 1);
			int num2 = Mathf.Min(srclen1 - 1, x + 1);
			for (int i = 0; i < srclen2; i++)
			{
				int num3 = Mathf.Max(0, i - 1);
				int num4 = Mathf.Min(srclen2 - 1, i + 1);
				for (int j = 0; j < srclen3; j++)
				{
					float num5 = src[x, i, j] * 6f;
					float num6 = num5 + src[num, i, j] + src[x, num3, j];
					dst[2 * x, 2 * i, j] = num6 * 0.125f;
					float num7 = num5 + src[num2, i, j] + src[x, num3, j];
					dst[2 * x + 1, 2 * i, j] = num7 * 0.125f;
					float num8 = num5 + src[num, i, j] + src[x, num4, j];
					dst[2 * x, 2 * i + 1, j] = num8 * 0.125f;
					float num9 = num5 + src[num2, i, j] + src[x, num4, j];
					dst[2 * x + 1, 2 * i + 1, j] = num9 * 0.125f;
				}
			}
		});
	}

	// Token: 0x06001B8B RID: 7051 RVA: 0x00099808 File Offset: 0x00097A08
	public static void Dilate2D(int[,] src, int srcmask, int radius, Action<int, int> action)
	{
		int dx = src.GetLength(0);
		int dy = src.GetLength(1);
		Parallel.For(0, dx, delegate(int x)
		{
			MaxQueue maxQueue = new MaxQueue(radius * 2 + 1);
			for (int i = 0; i < radius; i++)
			{
				maxQueue.Push(src[x, i] & srcmask);
			}
			for (int j = 0; j < dy; j++)
			{
				if (j > radius)
				{
					maxQueue.Pop();
				}
				if (j < dy - radius)
				{
					maxQueue.Push(src[x, j + radius] & srcmask);
				}
				int max = maxQueue.Max;
				if (max != 0)
				{
					action(x, j);
				}
			}
		});
		Parallel.For(0, dy, delegate(int y)
		{
			MaxQueue maxQueue = new MaxQueue(radius * 2 + 1);
			for (int i = 0; i < radius; i++)
			{
				maxQueue.Push(src[i, y] & srcmask);
			}
			for (int j = 0; j < dx; j++)
			{
				if (j > radius)
				{
					maxQueue.Pop();
				}
				if (j < dx - radius)
				{
					maxQueue.Push(src[j + radius, y] & srcmask);
				}
				int max = maxQueue.Max;
				if (max != 0)
				{
					action(j, y);
				}
			}
		});
	}

	// Token: 0x06001B8C RID: 7052 RVA: 0x0009988C File Offset: 0x00097A8C
	public static void FloodFill2D(int x, int y, int[,] data, int mask_any, int mask_not, Func<int, int> action)
	{
		int length = data.GetLength(0);
		int length2 = data.GetLength(1);
		Stack<KeyValuePair<int, int>> stack = new Stack<KeyValuePair<int, int>>();
		stack.Push(new KeyValuePair<int, int>(x, y));
		while (stack.Count > 0)
		{
			KeyValuePair<int, int> keyValuePair = stack.Pop();
			x = keyValuePair.Key;
			y = keyValuePair.Value;
			int i;
			for (i = y; i >= 0; i--)
			{
				int num = data[x, i];
				if ((num & mask_any) == 0 || (num & mask_not) != 0)
				{
					break;
				}
			}
			i++;
			bool flag2;
			bool flag = flag2 = false;
			while (i < length2)
			{
				int num2 = data[x, i];
				if ((num2 & mask_any) == 0 || (num2 & mask_not) != 0)
				{
					break;
				}
				data[x, i] = action(num2);
				if (x > 0)
				{
					int num3 = data[x - 1, i];
					bool flag3 = (num3 & mask_any) != 0 && (num3 & mask_not) == 0;
					if (!flag2 && flag3)
					{
						stack.Push(new KeyValuePair<int, int>(x - 1, i));
						flag2 = true;
					}
					else if (flag2 && !flag3)
					{
						flag2 = false;
					}
				}
				if (x < length - 1)
				{
					int num4 = data[x + 1, i];
					bool flag4 = (num4 & mask_any) != 0 && (num4 & mask_not) == 0;
					if (!flag && flag4)
					{
						stack.Push(new KeyValuePair<int, int>(x + 1, i));
						flag = true;
					}
					else if (flag && !flag4)
					{
						flag = false;
					}
				}
				i++;
			}
		}
	}
}
