﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

// Token: 0x02000579 RID: 1401
public abstract class TerrainMap2D<T> : global::TerrainMap where T : struct
{
	// Token: 0x06001D60 RID: 7520 RVA: 0x000A475C File Offset: 0x000A295C
	public void Push()
	{
		if (this.src != this.dst)
		{
			return;
		}
		this.dst = (T[,])this.src.Clone();
	}

	// Token: 0x06001D61 RID: 7521 RVA: 0x000A4788 File Offset: 0x000A2988
	public void Pop()
	{
		if (this.src == this.dst)
		{
			return;
		}
		Array.Copy(this.dst, this.src, this.src.Length);
		this.dst = this.src;
	}

	// Token: 0x06001D62 RID: 7522 RVA: 0x000A47C4 File Offset: 0x000A29C4
	public IEnumerable<T> ToEnumerable()
	{
		return this.src.Cast<T>();
	}

	// Token: 0x06001D63 RID: 7523 RVA: 0x000A47D4 File Offset: 0x000A29D4
	public int BytesPerElement()
	{
		return Marshal.SizeOf(typeof(T));
	}

	// Token: 0x06001D64 RID: 7524 RVA: 0x000A47E8 File Offset: 0x000A29E8
	public long GetMemoryUsage()
	{
		return (long)this.BytesPerElement() * (long)this.src.Length;
	}

	// Token: 0x06001D65 RID: 7525 RVA: 0x000A4800 File Offset: 0x000A2A00
	public byte[] ToByteArray()
	{
		byte[] array = new byte[this.BytesPerElement() * this.src.Length];
		Buffer.BlockCopy(this.src, 0, array, 0, array.Length);
		return array;
	}

	// Token: 0x06001D66 RID: 7526 RVA: 0x000A4838 File Offset: 0x000A2A38
	public void FromByteArray(byte[] dat)
	{
		Buffer.BlockCopy(dat, 0, this.dst, 0, dat.Length);
	}

	// Token: 0x04001835 RID: 6197
	internal T[,] src;

	// Token: 0x04001836 RID: 6198
	internal T[,] dst;
}
