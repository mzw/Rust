﻿using System;
using UnityEngine;

// Token: 0x020004F1 RID: 1265
public class ItemModReveal : global::ItemMod
{
	// Token: 0x06001B00 RID: 6912 RVA: 0x00097180 File Offset: 0x00095380
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (command == "reveal")
		{
			if (item.amount < this.numForReveal)
			{
				return;
			}
			int position = item.position;
			item.UseItem(this.numForReveal);
			global::Item item2 = null;
			if (this.revealedItemOverride)
			{
				item2 = global::ItemManager.Create(this.revealedItemOverride, this.revealedItemAmount, 0UL);
			}
			if (item2 != null && !item2.MoveToContainer(player.inventory.containerMain, (item.amount != 0) ? -1 : position, true))
			{
				item2.Drop(player.eyes.position, player.eyes.BodyForward() * 2f, default(Quaternion));
			}
			if (this.successEffect.isValid)
			{
				global::Effect.server.Run(this.successEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
			}
		}
	}

	// Token: 0x040015CB RID: 5579
	public int numForReveal = 10;

	// Token: 0x040015CC RID: 5580
	public global::ItemDefinition revealedItemOverride;

	// Token: 0x040015CD RID: 5581
	public int revealedItemAmount = 1;

	// Token: 0x040015CE RID: 5582
	public global::LootSpawn revealList;

	// Token: 0x040015CF RID: 5583
	public global::GameObjectRef successEffect;
}
