﻿using System;
using UnityEngine;

namespace Windows
{
	// Token: 0x02000350 RID: 848
	public class ConsoleInput
	{
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x0600143B RID: 5179 RVA: 0x00076570 File Offset: 0x00074770
		// (remove) Token: 0x0600143C RID: 5180 RVA: 0x000765A8 File Offset: 0x000747A8
		public event Action<string> OnInputText;

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x0600143D RID: 5181 RVA: 0x000765E0 File Offset: 0x000747E0
		public bool valid
		{
			get
			{
				return Console.BufferWidth > 0;
			}
		}

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x0600143E RID: 5182 RVA: 0x000765EC File Offset: 0x000747EC
		public int lineWidth
		{
			get
			{
				return Console.BufferWidth;
			}
		}

		// Token: 0x0600143F RID: 5183 RVA: 0x000765F4 File Offset: 0x000747F4
		public void ClearLine(int numLines)
		{
			Console.CursorLeft = 0;
			Console.Write(new string(' ', this.lineWidth * numLines));
			Console.CursorTop -= numLines;
			Console.CursorLeft = 0;
		}

		// Token: 0x06001440 RID: 5184 RVA: 0x00076624 File Offset: 0x00074824
		public void RedrawInputLine()
		{
			try
			{
				Console.ForegroundColor = ConsoleColor.White;
				Console.CursorTop++;
				for (int i = 0; i < this.statusText.Length; i++)
				{
					Console.CursorLeft = 0;
					Console.Write(this.statusText[i].PadRight(this.lineWidth));
				}
				Console.CursorTop -= this.statusText.Length + 1;
				Console.CursorLeft = 0;
				Console.BackgroundColor = ConsoleColor.Black;
				Console.ForegroundColor = ConsoleColor.Green;
				this.ClearLine(1);
				if (this.inputString.Length != 0)
				{
					if (this.inputString.Length < this.lineWidth - 2)
					{
						Console.Write(this.inputString);
					}
					else
					{
						Console.Write(this.inputString.Substring(this.inputString.Length - (this.lineWidth - 2)));
					}
				}
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06001441 RID: 5185 RVA: 0x00076728 File Offset: 0x00074928
		internal void OnBackspace()
		{
			if (this.inputString.Length < 1)
			{
				return;
			}
			this.inputString = this.inputString.Substring(0, this.inputString.Length - 1);
			this.RedrawInputLine();
		}

		// Token: 0x06001442 RID: 5186 RVA: 0x00076764 File Offset: 0x00074964
		internal void OnEscape()
		{
			this.inputString = string.Empty;
			this.RedrawInputLine();
		}

		// Token: 0x06001443 RID: 5187 RVA: 0x00076778 File Offset: 0x00074978
		internal void OnEnter()
		{
			this.ClearLine(this.statusText.Length);
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("> " + this.inputString);
			string obj = this.inputString;
			this.inputString = string.Empty;
			if (this.OnInputText != null)
			{
				this.OnInputText(obj);
			}
			this.RedrawInputLine();
		}

		// Token: 0x06001444 RID: 5188 RVA: 0x000767E0 File Offset: 0x000749E0
		public void Update()
		{
			if (!this.valid)
			{
				return;
			}
			if (this.nextUpdate < Time.realtimeSinceStartup)
			{
				this.RedrawInputLine();
				this.nextUpdate = Time.realtimeSinceStartup + 0.5f;
			}
			try
			{
				if (!Console.KeyAvailable)
				{
					return;
				}
			}
			catch (Exception)
			{
				return;
			}
			ConsoleKeyInfo consoleKeyInfo = Console.ReadKey();
			if (consoleKeyInfo.Key == ConsoleKey.Enter)
			{
				this.OnEnter();
				return;
			}
			if (consoleKeyInfo.Key == ConsoleKey.Backspace)
			{
				this.OnBackspace();
				return;
			}
			if (consoleKeyInfo.Key == ConsoleKey.Escape)
			{
				this.OnEscape();
				return;
			}
			if (consoleKeyInfo.KeyChar != '\0')
			{
				this.inputString += consoleKeyInfo.KeyChar;
				this.RedrawInputLine();
				return;
			}
		}

		// Token: 0x04000F1A RID: 3866
		public string inputString = string.Empty;

		// Token: 0x04000F1B RID: 3867
		public string[] statusText = new string[]
		{
			string.Empty,
			string.Empty,
			string.Empty
		};

		// Token: 0x04000F1C RID: 3868
		internal float nextUpdate;
	}
}
