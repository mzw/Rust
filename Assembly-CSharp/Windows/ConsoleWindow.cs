﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using Microsoft.Win32.SafeHandles;
using UnityEngine;

namespace Windows
{
	// Token: 0x02000351 RID: 849
	[SuppressUnmanagedCodeSecurity]
	public class ConsoleWindow
	{
		// Token: 0x06001446 RID: 5190 RVA: 0x000768C8 File Offset: 0x00074AC8
		public void Initialize()
		{
			if (!ConsoleWindow.AttachConsole(4294967295u))
			{
				ConsoleWindow.AllocConsole();
			}
			this.oldOutput = Console.Out;
			try
			{
				Console.OutputEncoding = Encoding.UTF8;
				IntPtr stdHandle = ConsoleWindow.GetStdHandle(-11);
				SafeFileHandle handle = new SafeFileHandle(stdHandle, true);
				FileStream stream = new FileStream(handle, FileAccess.Write);
				Console.SetOut(new StreamWriter(stream, Encoding.UTF8)
				{
					AutoFlush = true
				});
			}
			catch (Exception ex)
			{
				Debug.Log("Couldn't redirect output: " + ex.Message);
			}
		}

		// Token: 0x06001447 RID: 5191 RVA: 0x00076960 File Offset: 0x00074B60
		public void Shutdown()
		{
			Console.SetOut(this.oldOutput);
			ConsoleWindow.FreeConsole();
		}

		// Token: 0x06001448 RID: 5192 RVA: 0x00076974 File Offset: 0x00074B74
		public void SetTitle(string strName)
		{
			ConsoleWindow.SetConsoleTitleA(strName);
		}

		// Token: 0x06001449 RID: 5193
		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool AttachConsole(uint dwProcessId);

		// Token: 0x0600144A RID: 5194
		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool AllocConsole();

		// Token: 0x0600144B RID: 5195
		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool FreeConsole();

		// Token: 0x0600144C RID: 5196
		[DllImport("kernel32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetStdHandle(int nStdHandle);

		// Token: 0x0600144D RID: 5197
		[DllImport("kernel32.dll")]
		private static extern bool SetConsoleTitleA(string lpConsoleTitle);

		// Token: 0x04000F1D RID: 3869
		private TextWriter oldOutput;

		// Token: 0x04000F1E RID: 3870
		private const int STD_INPUT_HANDLE = -10;

		// Token: 0x04000F1F RID: 3871
		private const int STD_OUTPUT_HANDLE = -11;
	}
}
