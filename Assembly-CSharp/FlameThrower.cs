﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

// Token: 0x02000069 RID: 105
public class FlameThrower : global::AttackEntity
{
	// Token: 0x060007B5 RID: 1973 RVA: 0x00032280 File Offset: 0x00030480
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("FlameThrower.OnRpcMessage", 0.1f))
		{
			if (rpc == 1276361188u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoReload ");
				}
				using (TimeWarning.New("DoReload", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoReload", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoReload(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoReload");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 1123207253u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SetFiring ");
				}
				using (TimeWarning.New("SetFiring", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage firing = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SetFiring(firing);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in SetFiring");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 1064739762u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - TogglePilotLight ");
				}
				using (TimeWarning.New("TogglePilotLight", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.TogglePilotLight(msg3);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in TogglePilotLight");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060007B6 RID: 1974 RVA: 0x000326C4 File Offset: 0x000308C4
	private bool IsWeaponBusy()
	{
		return UnityEngine.Time.realtimeSinceStartup < this.nextReadyTime;
	}

	// Token: 0x060007B7 RID: 1975 RVA: 0x000326D4 File Offset: 0x000308D4
	private void SetBusyFor(float dur)
	{
		this.nextReadyTime = UnityEngine.Time.realtimeSinceStartup + dur;
	}

	// Token: 0x060007B8 RID: 1976 RVA: 0x000326E4 File Offset: 0x000308E4
	private void ClearBusy()
	{
		this.nextReadyTime = UnityEngine.Time.realtimeSinceStartup - 1f;
	}

	// Token: 0x060007B9 RID: 1977 RVA: 0x000326F8 File Offset: 0x000308F8
	public void ReduceAmmo(float firingTime)
	{
		this.ammoRemainder += this.fuelPerSec * firingTime;
		if (this.ammoRemainder >= 1f)
		{
			int num = Mathf.FloorToInt(this.ammoRemainder);
			this.ammoRemainder -= (float)num;
			if (this.ammoRemainder >= 1f)
			{
				num++;
				this.ammoRemainder -= 1f;
			}
			this.ammo -= num;
			if (this.ammo <= 0)
			{
				this.ammo = 0;
			}
		}
	}

	// Token: 0x060007BA RID: 1978 RVA: 0x0003278C File Offset: 0x0003098C
	public void PilotLightToggle_Shared()
	{
		base.SetFlag(global::BaseEntity.Flags.On, !base.HasFlag(global::BaseEntity.Flags.On), false);
		if (base.isServer)
		{
			base.SendNetworkUpdateImmediate(false);
		}
	}

	// Token: 0x060007BB RID: 1979 RVA: 0x000327B4 File Offset: 0x000309B4
	public bool IsPilotOn()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x060007BC RID: 1980 RVA: 0x000327C0 File Offset: 0x000309C0
	public bool IsFlameOn()
	{
		return base.HasFlag(global::BaseEntity.Flags.OnFire);
	}

	// Token: 0x060007BD RID: 1981 RVA: 0x000327CC File Offset: 0x000309CC
	public bool HasAmmo()
	{
		return this.GetAmmo() != null;
	}

	// Token: 0x060007BE RID: 1982 RVA: 0x000327DC File Offset: 0x000309DC
	public global::Item GetAmmo()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return null;
		}
		global::Item item = ownerPlayer.inventory.containerMain.FindItemsByItemName(this.fuelType.shortname);
		if (item == null)
		{
			item = ownerPlayer.inventory.containerBelt.FindItemsByItemName(this.fuelType.shortname);
		}
		return item;
	}

	// Token: 0x060007BF RID: 1983 RVA: 0x0003283C File Offset: 0x00030A3C
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.baseProjectile != null && info.msg.baseProjectile.primaryMagazine != null)
		{
			this.ammo = info.msg.baseProjectile.primaryMagazine.contents;
		}
	}

	// Token: 0x060007C0 RID: 1984 RVA: 0x00032894 File Offset: 0x00030A94
	public override void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
		this.ServerCommand(item, "unload_ammo", crafter);
	}

	// Token: 0x060007C1 RID: 1985 RVA: 0x000328A4 File Offset: 0x00030AA4
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.baseProjectile = new ProtoBuf.BaseProjectile();
		info.msg.baseProjectile.primaryMagazine = Facepunch.Pool.Get<Magazine>();
		info.msg.baseProjectile.primaryMagazine.contents = this.ammo;
	}

	// Token: 0x060007C2 RID: 1986 RVA: 0x000328FC File Offset: 0x00030AFC
	[global::BaseEntity.RPC_Server]
	public void SetFiring(global::BaseEntity.RPCMessage msg)
	{
		bool flameState = msg.read.Bit();
		this.SetFlameState(flameState);
	}

	// Token: 0x060007C3 RID: 1987 RVA: 0x00032920 File Offset: 0x00030B20
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	public void DoReload(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		global::Item item;
		while (this.ammo < this.maxAmmo && (item = this.GetAmmo()) != null && item.amount > 0)
		{
			int num = Mathf.Min(this.maxAmmo - this.ammo, item.amount);
			this.ammo += num;
			item.UseItem(num);
		}
		base.SendNetworkUpdateImmediate(false);
		global::ItemManager.DoRemoves();
		ownerPlayer.inventory.ServerUpdate(0f);
	}

	// Token: 0x060007C4 RID: 1988 RVA: 0x000329BC File Offset: 0x00030BBC
	public void SetFlameState(bool wantsOn)
	{
		if (wantsOn)
		{
			this.ammo--;
			if (this.ammo < 0)
			{
				this.ammo = 0;
			}
		}
		if (wantsOn && this.ammo <= 0)
		{
			wantsOn = false;
		}
		if (wantsOn)
		{
		}
		base.SetFlag(global::BaseEntity.Flags.OnFire, wantsOn, false);
		if (this.IsFlameOn())
		{
			this.nextFlameTime = UnityEngine.Time.realtimeSinceStartup + 1f;
			this.lastFlameTick = UnityEngine.Time.realtimeSinceStartup;
			base.InvokeRepeating(new Action(this.FlameTick), this.tickRate, this.tickRate);
		}
		else
		{
			base.CancelInvoke(new Action(this.FlameTick));
		}
	}

	// Token: 0x060007C5 RID: 1989 RVA: 0x00032A70 File Offset: 0x00030C70
	[global::BaseEntity.RPC_Server]
	public void TogglePilotLight(global::BaseEntity.RPCMessage msg)
	{
		this.PilotLightToggle_Shared();
	}

	// Token: 0x060007C6 RID: 1990 RVA: 0x00032A78 File Offset: 0x00030C78
	public override void OnHeldChanged()
	{
		this.SetFlameState(false);
		base.OnHeldChanged();
	}

	// Token: 0x060007C7 RID: 1991 RVA: 0x00032A88 File Offset: 0x00030C88
	public void FlameTick()
	{
		float num = UnityEngine.Time.realtimeSinceStartup - this.lastFlameTick;
		this.lastFlameTick = UnityEngine.Time.realtimeSinceStartup;
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		this.ReduceAmmo(num);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		Ray ray = ownerPlayer.eyes.BodyRay();
		Vector3 origin = ray.origin;
		RaycastHit raycastHit;
		bool flag = UnityEngine.Physics.SphereCast(ray, 0.3f, ref raycastHit, this.flameRange, 1084434689);
		if (!flag)
		{
			raycastHit.point = origin + ray.direction * this.flameRange;
		}
		float amount = this.damagePerSec[0].amount;
		this.damagePerSec[0].amount = amount * num;
		global::DamageUtil.RadiusDamage(ownerPlayer, base.LookupPrefab(), raycastHit.point - ray.direction * 0.1f, this.flameRadius * 0.5f, this.flameRadius, this.damagePerSec, 2246913, true);
		this.damagePerSec[0].amount = amount;
		if (flag && UnityEngine.Time.realtimeSinceStartup >= this.nextFlameTime && raycastHit.distance > 1.1f)
		{
			this.nextFlameTime = UnityEngine.Time.realtimeSinceStartup + 0.45f;
			Vector3 point = raycastHit.point;
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.fireballPrefab.resourcePath, point - ray.direction * 0.25f, default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.creatorEntity = ownerPlayer;
				baseEntity.Spawn();
			}
		}
		if (this.ammo == 0)
		{
			this.SetFlameState(false);
		}
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem != null)
		{
			ownerItem.LoseCondition(num);
		}
	}

	// Token: 0x060007C8 RID: 1992 RVA: 0x00032C68 File Offset: 0x00030E68
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (item == null)
		{
			return;
		}
		if (command == "unload_ammo")
		{
			int num = this.ammo;
			if (num > 0)
			{
				this.ammo = 0;
				base.SendNetworkUpdateImmediate(false);
				global::Item item2 = global::ItemManager.Create(this.fuelType, num, 0UL);
				if (!item2.MoveToContainer(player.inventory.containerMain, -1, true))
				{
					item2.Drop(player.eyes.position, player.eyes.BodyForward() * 2f, default(Quaternion));
				}
			}
		}
	}

	// Token: 0x04000383 RID: 899
	[Header("Flame Thrower")]
	public int maxAmmo = 100;

	// Token: 0x04000384 RID: 900
	public int ammo = 100;

	// Token: 0x04000385 RID: 901
	public global::ItemDefinition fuelType;

	// Token: 0x04000386 RID: 902
	public float timeSinceLastAttack;

	// Token: 0x04000387 RID: 903
	[FormerlySerializedAs("nextAttackTime")]
	public float nextReadyTime;

	// Token: 0x04000388 RID: 904
	public float flameRange = 10f;

	// Token: 0x04000389 RID: 905
	public float flameRadius = 2.5f;

	// Token: 0x0400038A RID: 906
	public ParticleSystem[] flameEffects;

	// Token: 0x0400038B RID: 907
	public global::FlameJet jet;

	// Token: 0x0400038C RID: 908
	public global::GameObjectRef fireballPrefab;

	// Token: 0x0400038D RID: 909
	public List<Rust.DamageTypeEntry> damagePerSec;

	// Token: 0x0400038E RID: 910
	public global::SoundDefinition flameStart3P;

	// Token: 0x0400038F RID: 911
	public global::SoundDefinition flameLoop3P;

	// Token: 0x04000390 RID: 912
	public global::SoundDefinition flameStop3P;

	// Token: 0x04000391 RID: 913
	public global::SoundDefinition pilotLoopSoundDef;

	// Token: 0x04000392 RID: 914
	private float tickRate = 0.25f;

	// Token: 0x04000393 RID: 915
	private float lastFlameTick;

	// Token: 0x04000394 RID: 916
	public float fuelPerSec;

	// Token: 0x04000395 RID: 917
	private float ammoRemainder;

	// Token: 0x04000396 RID: 918
	private float nextFlameTime;
}
