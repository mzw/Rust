﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000061 RID: 97
public class CollectibleEntity : global::BaseEntity, global::IPrefabPreProcess
{
	// Token: 0x06000769 RID: 1897 RVA: 0x0002F928 File Offset: 0x0002DB28
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("CollectibleEntity.OnRpcMessage", 0.1f))
		{
			if (rpc == 3306490492u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Pickup ");
				}
				using (TimeWarning.New("Pickup", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("Pickup", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Pickup(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in Pickup");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600076A RID: 1898 RVA: 0x0002FB08 File Offset: 0x0002DD08
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void Pickup(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (this.itemList == null)
		{
			return;
		}
		foreach (global::ItemAmount itemAmount in this.itemList)
		{
			global::Item item = global::ItemManager.Create(itemAmount.itemDef, (int)itemAmount.amount, 0UL);
			Interface.CallHook("OnCollectiblePickup", new object[]
			{
				item,
				msg.player,
				this
			});
			msg.player.GiveItem(item, global::BaseEntity.GiveItemReason.ResourceHarvested);
		}
		this.itemList = null;
		if (this.pickupEffect.isValid)
		{
			global::Effect.server.Run(this.pickupEffect.resourcePath, base.transform.position, base.transform.up, null, false);
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x0600076B RID: 1899 RVA: 0x0002FBE4 File Offset: 0x0002DDE4
	public virtual void PreProcess(global::IPrefabProcessor preProcess, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		if (serverside)
		{
			preProcess.RemoveComponent(base.GetComponent<Collider>());
		}
	}

	// Token: 0x0600076C RID: 1900 RVA: 0x0002FBFC File Offset: 0x0002DDFC
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04000364 RID: 868
	public global::Translate.Phrase itemName;

	// Token: 0x04000365 RID: 869
	public global::ItemAmount[] itemList;

	// Token: 0x04000366 RID: 870
	public global::GameObjectRef pickupEffect;

	// Token: 0x04000367 RID: 871
	public float xpScale = 1f;
}
