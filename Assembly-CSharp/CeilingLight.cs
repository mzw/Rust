﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using Rust;
using UnityEngine;

// Token: 0x0200005E RID: 94
public class CeilingLight : global::BaseFuelLightSource
{
	// Token: 0x0600073D RID: 1853 RVA: 0x0002DBB0 File Offset: 0x0002BDB0
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("CeilingLight.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600073E RID: 1854 RVA: 0x0002DBF8 File Offset: 0x0002BDF8
	public override void Hurt(global::HitInfo info)
	{
		if (base.isServer)
		{
			if (info.damageTypes.Has(Rust.DamageType.Explosion))
			{
				base.ClientRPC<int, Vector3, Vector3>(null, "ClientPhysPush", 0, info.attackNormal * 3f * (info.damageTypes.Total() / 50f), info.HitPositionWorld);
			}
			base.Hurt(info);
		}
	}

	// Token: 0x0600073F RID: 1855 RVA: 0x0002DC64 File Offset: 0x0002BE64
	public void RefreshPlants()
	{
		List<global::PlantEntity> list = Pool.GetList<global::PlantEntity>();
		global::Vis.Entities<global::PlantEntity>(base.transform.position + new Vector3(0f, -2f, 0f), 5f, list, 512, 2);
		foreach (global::PlantEntity plantEntity in list)
		{
			plantEntity.RefreshLightExposure();
		}
		Pool.FreeList<global::PlantEntity>(ref list);
	}

	// Token: 0x06000740 RID: 1856 RVA: 0x0002DCFC File Offset: 0x0002BEFC
	public override void StartCooking()
	{
		base.StartCooking();
		this.RefreshPlants();
	}

	// Token: 0x06000741 RID: 1857 RVA: 0x0002DD0C File Offset: 0x0002BF0C
	public override void StopCooking()
	{
		base.StopCooking();
		this.RefreshPlants();
	}

	// Token: 0x06000742 RID: 1858 RVA: 0x0002DD1C File Offset: 0x0002BF1C
	public override void OnKilled(global::HitInfo info)
	{
		base.OnKilled(info);
		this.RefreshPlants();
	}

	// Token: 0x06000743 RID: 1859 RVA: 0x0002DD2C File Offset: 0x0002BF2C
	public override void OnAttacked(global::HitInfo info)
	{
		float num = 3f * (info.damageTypes.Total() / 50f);
		base.ClientRPC<uint, Vector3, Vector3>(null, "ClientPhysPush", (!(info.Initiator != null) || !(info.Initiator is global::BasePlayer) || info.IsPredicting) ? 0u : info.Initiator.net.ID, info.attackNormal * num, info.HitPositionWorld);
		base.OnAttacked(info);
	}

	// Token: 0x06000744 RID: 1860 RVA: 0x0002DDB8 File Offset: 0x0002BFB8
	public override bool SupportsPooling()
	{
		return false;
	}

	// Token: 0x04000346 RID: 838
	public float pushScale = 2f;
}
