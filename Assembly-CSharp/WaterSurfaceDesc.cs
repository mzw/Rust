﻿using System;
using UnityEngine;

// Token: 0x020005E7 RID: 1511
[Serializable]
public class WaterSurfaceDesc
{
	// Token: 0x040019F1 RID: 6641
	public Material material;

	// Token: 0x040019F2 RID: 6642
	public Renderer[] renderers = new Renderer[0];

	// Token: 0x040019F3 RID: 6643
	public Collider[] triggers = new Collider[0];
}
