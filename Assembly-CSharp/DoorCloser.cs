﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000065 RID: 101
public class DoorCloser : global::BaseEntity
{
	// Token: 0x0600078F RID: 1935 RVA: 0x00030F74 File Offset: 0x0002F174
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("DoorCloser.OnRpcMessage", 0.1f))
		{
			if (rpc == 358652577u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Take ");
				}
				using (TimeWarning.New("RPC_Take", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Take", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Take(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Take");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000790 RID: 1936 RVA: 0x00031154 File Offset: 0x0002F354
	public override float BoundsPadding()
	{
		return 1f;
	}

	// Token: 0x06000791 RID: 1937 RVA: 0x0003115C File Offset: 0x0002F35C
	public void Think()
	{
		base.Invoke(new Action(this.SendClose), this.delay);
	}

	// Token: 0x06000792 RID: 1938 RVA: 0x00031178 File Offset: 0x0002F378
	public void SendClose()
	{
		global::BaseEntity parentEntity = base.GetParentEntity();
		if (this.children != null)
		{
			foreach (global::BaseEntity baseEntity in this.children)
			{
				if (baseEntity != null)
				{
					base.Invoke(new Action(this.SendClose), this.delay);
					return;
				}
			}
		}
		if (parentEntity)
		{
			parentEntity.SendMessage("CloseRequest");
		}
	}

	// Token: 0x06000793 RID: 1939 RVA: 0x00031220 File Offset: 0x0002F420
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_Take(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!rpc.player.CanBuild())
		{
			return;
		}
		global::Door door = this.GetDoor();
		if (door == null)
		{
			return;
		}
		if (!door.GetPlayerLockPermission(rpc.player))
		{
			return;
		}
		global::Item item = global::ItemManager.Create(this.itemType, 1, this.skinID);
		if (item != null)
		{
			rpc.player.GiveItem(item, global::BaseEntity.GiveItemReason.Generic);
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06000794 RID: 1940 RVA: 0x000312A8 File Offset: 0x0002F4A8
	public global::Door GetDoor()
	{
		return base.GetParentEntity() as global::Door;
	}

	// Token: 0x04000377 RID: 887
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition itemType;

	// Token: 0x04000378 RID: 888
	public float delay = 3f;
}
