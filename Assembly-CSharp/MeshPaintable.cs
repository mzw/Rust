﻿using System;
using UnityEngine;

// Token: 0x0200026D RID: 621
public class MeshPaintable : MonoBehaviour, IClientComponent
{
	// Token: 0x04000B72 RID: 2930
	public string replacementTextureName = "_MainTex";

	// Token: 0x04000B73 RID: 2931
	public int textureWidth = 256;

	// Token: 0x04000B74 RID: 2932
	public int textureHeight = 256;

	// Token: 0x04000B75 RID: 2933
	public Color clearColor = Color.clear;

	// Token: 0x04000B76 RID: 2934
	public Texture2D targetTexture;

	// Token: 0x04000B77 RID: 2935
	public bool hasChanges;
}
