﻿using System;
using UnityEngine;

// Token: 0x020007B3 RID: 1971
[Serializable]
public class ResourceRef<T> where T : Object
{
	// Token: 0x17000298 RID: 664
	// (get) Token: 0x060024AD RID: 9389 RVA: 0x000C9EFC File Offset: 0x000C80FC
	public bool isValid
	{
		get
		{
			return !string.IsNullOrEmpty(this.guid);
		}
	}

	// Token: 0x060024AE RID: 9390 RVA: 0x000C9F0C File Offset: 0x000C810C
	public T Get()
	{
		if (this._cachedObject == null)
		{
			this._cachedObject = global::GameManifest.GUIDToObject(this.guid);
		}
		return this._cachedObject as T;
	}

	// Token: 0x17000299 RID: 665
	// (get) Token: 0x060024AF RID: 9391 RVA: 0x000C9F40 File Offset: 0x000C8140
	public string resourcePath
	{
		get
		{
			return global::GameManifest.GUIDToPath(this.guid);
		}
	}

	// Token: 0x1700029A RID: 666
	// (get) Token: 0x060024B0 RID: 9392 RVA: 0x000C9F50 File Offset: 0x000C8150
	public uint resourceID
	{
		get
		{
			return global::StringPool.Get(this.resourcePath);
		}
	}

	// Token: 0x0400201B RID: 8219
	public string guid;

	// Token: 0x0400201C RID: 8220
	private Object _cachedObject;
}
