﻿using System;
using UnityEngine;

// Token: 0x0200049A RID: 1178
public class TriggerLadder : global::TriggerBase
{
	// Token: 0x060019AF RID: 6575 RVA: 0x00090890 File Offset: 0x0008EA90
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity as global::BasePlayer == null)
		{
			return null;
		}
		return baseEntity.gameObject;
	}
}
