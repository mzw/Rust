﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Facepunch.Steamworks;
using Network;
using Rust;

// Token: 0x0200061A RID: 1562
public static class Auth_Steam
{
	// Token: 0x06001F91 RID: 8081 RVA: 0x000B343C File Offset: 0x000B163C
	public static IEnumerator Run(Connection connection)
	{
		connection.authStatus = string.Empty;
		if (!Global.SteamServer.Auth.StartSession(connection.token, connection.userid))
		{
			global::ConnectionAuth.Reject(connection, "Steam Auth Failed");
			yield break;
		}
		global::Auth_Steam.waitingList.Add(connection);
		Stopwatch timeout = Stopwatch.StartNew();
		while (timeout.Elapsed.TotalSeconds < 30.0 && connection.active)
		{
			if (connection.authStatus != string.Empty)
			{
				break;
			}
			yield return null;
		}
		global::Auth_Steam.waitingList.Remove(connection);
		if (!connection.active)
		{
			yield break;
		}
		if (connection.authStatus.Length == 0)
		{
			global::ConnectionAuth.Reject(connection, "Steam Auth Timeout");
			Global.SteamServer.Auth.EndSession(connection.userid);
			yield break;
		}
		if (connection.authStatus == "banned")
		{
			global::ConnectionAuth.Reject(connection, "Auth: " + connection.authStatus);
			Global.SteamServer.Auth.EndSession(connection.userid);
			yield break;
		}
		if (connection.authStatus == "gamebanned")
		{
			global::ServerUsers.Set(connection.userid, global::ServerUsers.UserGroup.Banned, connection.username, "GAMEBAN");
			global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
			{
				0,
				"<color=#fff>SERVER</color> Kicking " + connection.username + " (banned by Facepunch)"
			});
			global::ConnectionAuth.Reject(connection, "Steam Auth: " + connection.authStatus);
			Global.SteamServer.Auth.EndSession(connection.userid);
			yield break;
		}
		if (connection.authStatus == "vacbanned")
		{
			global::ServerUsers.Set(connection.userid, global::ServerUsers.UserGroup.Banned, connection.username, "VAC");
			global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
			{
				0,
				"<color=#fff>SERVER</color> Kicking " + connection.username + " (banned by Valve Anti Cheat)"
			});
			global::ConnectionAuth.Reject(connection, "Steam Auth: " + connection.authStatus);
			Global.SteamServer.Auth.EndSession(connection.userid);
			yield break;
		}
		if (connection.authStatus != "ok")
		{
			global::ConnectionAuth.Reject(connection, "Steam Auth Error: " + connection.authStatus);
			Global.SteamServer.Auth.EndSession(connection.userid);
			yield break;
		}
		Global.SteamServer.UpdatePlayer(connection.userid, connection.username, 0);
		yield break;
	}

	// Token: 0x06001F92 RID: 8082 RVA: 0x000B3458 File Offset: 0x000B1658
	public static bool ValidateConnecting(ulong steamid, ulong ownerSteamID, ServerAuth.Status response)
	{
		Connection connection = global::Auth_Steam.waitingList.Find((Connection x) => x.userid == steamid);
		if (connection == null)
		{
			return false;
		}
		connection.ownerid = ownerSteamID;
		if (global::ServerUsers.Is(ownerSteamID, global::ServerUsers.UserGroup.Banned) || global::ServerUsers.Is(steamid, global::ServerUsers.UserGroup.Banned))
		{
			connection.authStatus = "banned";
			return true;
		}
		if (response == null)
		{
			connection.authStatus = "ok";
			return true;
		}
		if (response == 3)
		{
			connection.authStatus = "vacbanned";
			return true;
		}
		if (response == 9)
		{
			connection.authStatus = "gamebanned";
			return true;
		}
		if (response == 5)
		{
			connection.authStatus = "ok";
			return true;
		}
		connection.authStatus = response.ToString();
		return true;
	}

	// Token: 0x04001A93 RID: 6803
	internal static List<Connection> waitingList = new List<Connection>();
}
