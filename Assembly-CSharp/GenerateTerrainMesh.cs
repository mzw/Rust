﻿using System;

// Token: 0x020005BC RID: 1468
public class GenerateTerrainMesh : global::ProceduralComponent
{
	// Token: 0x06001E79 RID: 7801 RVA: 0x000AABE8 File Offset: 0x000A8DE8
	public override void Process(uint seed)
	{
		if (!global::World.Serialization.Cached)
		{
			global::World.Serialization.AddMap("terrain", global::TerrainMeta.HeightMap.ToByteArray());
		}
		global::TerrainMeta.HeightMap.ApplyToTerrain();
		if (global::World.Serialization.Cached)
		{
			global::TerrainMeta.HeightMap.FromByteArray(global::World.Serialization.GetMap("height"));
		}
	}

	// Token: 0x1700021A RID: 538
	// (get) Token: 0x06001E7A RID: 7802 RVA: 0x000AAC50 File Offset: 0x000A8E50
	public override bool RunOnCache
	{
		get
		{
			return true;
		}
	}
}
