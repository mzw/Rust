﻿using System;
using System.Collections.Generic;
using ConVar;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000798 RID: 1944
public class MeshRendererBatch : global::MeshBatch
{
	// Token: 0x17000286 RID: 646
	// (get) Token: 0x0600241B RID: 9243 RVA: 0x000C7070 File Offset: 0x000C5270
	public override int VertexCapacity
	{
		get
		{
			return ConVar.Batching.renderer_capacity;
		}
	}

	// Token: 0x17000287 RID: 647
	// (get) Token: 0x0600241C RID: 9244 RVA: 0x000C7078 File Offset: 0x000C5278
	public override int VertexCutoff
	{
		get
		{
			return ConVar.Batching.renderer_vertices;
		}
	}

	// Token: 0x0600241D RID: 9245 RVA: 0x000C7080 File Offset: 0x000C5280
	public static GameObject CreateInstance()
	{
		GameObject gameObject = new GameObject("MeshRendererBatch");
		gameObject.AddComponent<MeshFilter>();
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<global::MeshRendererBatch>();
		return gameObject;
	}

	// Token: 0x0600241E RID: 9246 RVA: 0x000C70B0 File Offset: 0x000C52B0
	protected void Awake()
	{
		this.meshFilter = base.GetComponent<MeshFilter>();
		this.meshRenderer = base.GetComponent<MeshRenderer>();
		this.meshData = new global::MeshRendererData();
		this.meshGroup = new global::MeshRendererGroup();
		this.meshLookup = new global::MeshRendererLookup();
	}

	// Token: 0x0600241F RID: 9247 RVA: 0x000C70EC File Offset: 0x000C52EC
	public void Setup(Vector3 position, Material material, ShadowCastingMode shadows, int layer)
	{
		base.transform.position = position;
		this.position = position;
		base.gameObject.layer = layer;
		this.meshRenderer.sharedMaterial = material;
		this.meshRenderer.shadowCastingMode = shadows;
		if (shadows == 3)
		{
			this.meshRenderer.receiveShadows = false;
			this.meshRenderer.motionVectors = false;
			this.meshRenderer.lightProbeUsage = 0;
			this.meshRenderer.reflectionProbeUsage = 0;
		}
		else
		{
			this.meshRenderer.receiveShadows = true;
			this.meshRenderer.motionVectors = true;
			this.meshRenderer.lightProbeUsage = 1;
			this.meshRenderer.reflectionProbeUsage = 1;
		}
	}

	// Token: 0x06002420 RID: 9248 RVA: 0x000C71A0 File Offset: 0x000C53A0
	public void Add(global::MeshRendererInstance instance)
	{
		instance.position -= this.position;
		this.meshGroup.data.Add(instance);
		base.AddVertices(instance.mesh.vertexCount);
	}

	// Token: 0x06002421 RID: 9249 RVA: 0x000C71E0 File Offset: 0x000C53E0
	protected override void AllocMemory()
	{
		this.meshGroup.Alloc();
		this.meshData.Alloc();
	}

	// Token: 0x06002422 RID: 9250 RVA: 0x000C71F8 File Offset: 0x000C53F8
	protected override void FreeMemory()
	{
		this.meshGroup.Free();
		this.meshData.Free();
	}

	// Token: 0x06002423 RID: 9251 RVA: 0x000C7210 File Offset: 0x000C5410
	protected override void RefreshMesh()
	{
		this.meshLookup.dst.Clear();
		this.meshData.Clear();
		this.meshData.Combine(this.meshGroup, this.meshLookup);
	}

	// Token: 0x06002424 RID: 9252 RVA: 0x000C7244 File Offset: 0x000C5444
	protected override void ApplyMesh()
	{
		if (!this.meshBatch)
		{
			this.meshBatch = AssetPool.Get<UnityEngine.Mesh>();
		}
		this.meshLookup.Apply();
		this.meshData.Apply(this.meshBatch);
		this.meshBatch.UploadMeshData(false);
	}

	// Token: 0x06002425 RID: 9253 RVA: 0x000C7294 File Offset: 0x000C5494
	protected override void ToggleMesh(bool state)
	{
		List<global::MeshRendererLookup.LookupEntry> data = this.meshLookup.src.data;
		for (int i = 0; i < data.Count; i++)
		{
			Renderer renderer = data[i].renderer;
			if (renderer)
			{
				renderer.enabled = !state;
			}
		}
		if (state)
		{
			if (this.meshFilter)
			{
				this.meshFilter.sharedMesh = this.meshBatch;
			}
			if (this.meshRenderer)
			{
				this.meshRenderer.enabled = true;
			}
		}
		else
		{
			if (this.meshFilter)
			{
				this.meshFilter.sharedMesh = null;
			}
			if (this.meshRenderer)
			{
				this.meshRenderer.enabled = false;
			}
		}
	}

	// Token: 0x06002426 RID: 9254 RVA: 0x000C7370 File Offset: 0x000C5570
	protected override void OnPooled()
	{
		if (this.meshFilter)
		{
			this.meshFilter.sharedMesh = null;
		}
		if (this.meshBatch)
		{
			AssetPool.Free(ref this.meshBatch);
		}
		this.meshData.Free();
		this.meshGroup.Free();
		this.meshLookup.src.Clear();
		this.meshLookup.dst.Clear();
	}

	// Token: 0x04001FC3 RID: 8131
	private Vector3 position;

	// Token: 0x04001FC4 RID: 8132
	private UnityEngine.Mesh meshBatch;

	// Token: 0x04001FC5 RID: 8133
	private MeshFilter meshFilter;

	// Token: 0x04001FC6 RID: 8134
	private MeshRenderer meshRenderer;

	// Token: 0x04001FC7 RID: 8135
	private global::MeshRendererData meshData;

	// Token: 0x04001FC8 RID: 8136
	private global::MeshRendererGroup meshGroup;

	// Token: 0x04001FC9 RID: 8137
	private global::MeshRendererLookup meshLookup;
}
