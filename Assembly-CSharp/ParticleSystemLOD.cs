﻿using System;
using UnityEngine;

// Token: 0x02000459 RID: 1113
public class ParticleSystemLOD : global::LODComponentParticleSystem
{
	// Token: 0x0400134D RID: 4941
	[Horizontal(1, 0)]
	public global::ParticleSystemLOD.State[] States;

	// Token: 0x0200045A RID: 1114
	[Serializable]
	public class State
	{
		// Token: 0x0400134E RID: 4942
		public float distance;

		// Token: 0x0400134F RID: 4943
		[Range(0f, 1f)]
		public float emission;
	}
}
