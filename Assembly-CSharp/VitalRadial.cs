﻿using System;
using UnityEngine;

// Token: 0x02000733 RID: 1843
public class VitalRadial : MonoBehaviour
{
	// Token: 0x060022B1 RID: 8881 RVA: 0x000C1484 File Offset: 0x000BF684
	private void Awake()
	{
		Debug.LogWarning("VitalRadial is obsolete " + base.transform.GetRecursiveName(string.Empty), base.gameObject);
	}
}
