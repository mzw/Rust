﻿using System;
using UnityEngine;

// Token: 0x020001DB RID: 475
public class FootstepSound : MonoBehaviour
{
	// Token: 0x0400092A RID: 2346
	public global::SoundDefinition lightSound;

	// Token: 0x0400092B RID: 2347
	public global::SoundDefinition medSound;

	// Token: 0x0400092C RID: 2348
	public global::SoundDefinition hardSound;

	// Token: 0x0400092D RID: 2349
	private const float panAmount = 0.05f;

	// Token: 0x020001DC RID: 476
	public enum Hardness
	{
		// Token: 0x0400092F RID: 2351
		Light = 1,
		// Token: 0x04000930 RID: 2352
		Medium,
		// Token: 0x04000931 RID: 2353
		Hard
	}
}
