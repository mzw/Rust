﻿using System;
using UnityEngine;

// Token: 0x02000674 RID: 1652
public class NeedsCursor : MonoBehaviour, IClientComponent
{
	// Token: 0x060020CE RID: 8398 RVA: 0x000BAAA0 File Offset: 0x000B8CA0
	private void Update()
	{
		global::CursorManager.HoldOpen(false);
	}
}
