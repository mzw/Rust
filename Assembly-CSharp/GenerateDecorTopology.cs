﻿using System;
using UnityEngine;

// Token: 0x0200059F RID: 1439
public class GenerateDecorTopology : global::ProceduralComponent
{
	// Token: 0x06001E3A RID: 7738 RVA: 0x000A7DDC File Offset: 0x000A5FDC
	public override void Process(uint seed)
	{
		global::TerrainTopologyMap topomap = global::TerrainMeta.TopologyMap;
		int topores = topomap.res;
		Parallel.For(0, topores, delegate(int z)
		{
			for (int i = 0; i < topores; i++)
			{
				if (topomap.GetTopology(i, z, 4194306))
				{
					topomap.AddTopology(i, z, 512);
				}
				else if (!this.KeepExisting)
				{
					topomap.RemoveTopology(i, z, 512);
				}
			}
		});
	}

	// Token: 0x040018C9 RID: 6345
	public bool KeepExisting = true;
}
