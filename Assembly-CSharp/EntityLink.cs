﻿using System;
using System.Collections.Generic;
using Facepunch;

// Token: 0x02000384 RID: 900
public class EntityLink : Pool.IPooled
{
	// Token: 0x17000185 RID: 389
	// (get) Token: 0x0600155B RID: 5467 RVA: 0x0007B248 File Offset: 0x00079448
	public string name
	{
		get
		{
			return this.socket.socketName;
		}
	}

	// Token: 0x0600155C RID: 5468 RVA: 0x0007B258 File Offset: 0x00079458
	public void Setup(global::BaseEntity owner, global::Socket_Base socket)
	{
		this.owner = owner;
		this.socket = socket;
		if (socket.monogamous)
		{
			this.capacity = 1;
		}
	}

	// Token: 0x0600155D RID: 5469 RVA: 0x0007B27C File Offset: 0x0007947C
	public void EnterPool()
	{
		this.owner = null;
		this.socket = null;
		this.capacity = int.MaxValue;
	}

	// Token: 0x0600155E RID: 5470 RVA: 0x0007B298 File Offset: 0x00079498
	public void LeavePool()
	{
	}

	// Token: 0x0600155F RID: 5471 RVA: 0x0007B29C File Offset: 0x0007949C
	public bool Contains(global::EntityLink entity)
	{
		return this.connections.Contains(entity);
	}

	// Token: 0x06001560 RID: 5472 RVA: 0x0007B2AC File Offset: 0x000794AC
	public void Add(global::EntityLink entity)
	{
		this.connections.Add(entity);
	}

	// Token: 0x06001561 RID: 5473 RVA: 0x0007B2BC File Offset: 0x000794BC
	public void Remove(global::EntityLink entity)
	{
		this.connections.Remove(entity);
	}

	// Token: 0x06001562 RID: 5474 RVA: 0x0007B2CC File Offset: 0x000794CC
	public void Clear()
	{
		for (int i = 0; i < this.connections.Count; i++)
		{
			this.connections[i].Remove(this);
		}
		this.connections.Clear();
	}

	// Token: 0x06001563 RID: 5475 RVA: 0x0007B314 File Offset: 0x00079514
	public bool IsEmpty()
	{
		return this.connections.Count == 0;
	}

	// Token: 0x06001564 RID: 5476 RVA: 0x0007B324 File Offset: 0x00079524
	public bool IsOccupied()
	{
		return this.connections.Count >= this.capacity;
	}

	// Token: 0x06001565 RID: 5477 RVA: 0x0007B33C File Offset: 0x0007953C
	public bool IsMale()
	{
		return this.socket.male;
	}

	// Token: 0x06001566 RID: 5478 RVA: 0x0007B34C File Offset: 0x0007954C
	public bool IsFemale()
	{
		return this.socket.female;
	}

	// Token: 0x06001567 RID: 5479 RVA: 0x0007B35C File Offset: 0x0007955C
	public bool CanConnect(global::EntityLink link)
	{
		return !this.IsOccupied() && link != null && !link.IsOccupied() && this.socket.CanConnect(this.owner.transform.position, this.owner.transform.rotation, link.socket, link.owner.transform.position, link.owner.transform.rotation);
	}

	// Token: 0x04000FB2 RID: 4018
	public global::BaseEntity owner;

	// Token: 0x04000FB3 RID: 4019
	public global::Socket_Base socket;

	// Token: 0x04000FB4 RID: 4020
	public List<global::EntityLink> connections = new List<global::EntityLink>(8);

	// Token: 0x04000FB5 RID: 4021
	public int capacity = int.MaxValue;
}
