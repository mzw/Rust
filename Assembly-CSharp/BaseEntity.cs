﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ConVar;
using Facepunch;
using Facepunch.Extend;
using Network;
using ProtoBuf;
using Rust;
using Rust.Ai;
using Spatial;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000014 RID: 20
public class BaseEntity : global::BaseNetworkable
{
	// Token: 0x0600035C RID: 860 RVA: 0x000135D0 File Offset: 0x000117D0
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseEntity.OnRpcMessage", 0.1f))
		{
			if (rpc == 555001694u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - BroadcastSignalFromClient ");
				}
				using (TimeWarning.New("BroadcastSignalFromClient", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("BroadcastSignalFromClient", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.BroadcastSignalFromClient(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in BroadcastSignalFromClient");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 2257057967u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SV_RequestFile ");
				}
				using (TimeWarning.New("SV_RequestFile", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SV_RequestFile(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in SV_RequestFile");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600035D RID: 861 RVA: 0x000138E0 File Offset: 0x00011AE0
	public virtual void OnCollision(Collision collision, global::BaseEntity hitEntity)
	{
		throw new NotImplementedException();
	}

	// Token: 0x0600035E RID: 862 RVA: 0x000138E8 File Offset: 0x00011AE8
	protected void ReceiveCollisionMessages(bool b)
	{
		if (b)
		{
			Facepunch.Extend.TransformEx.GetOrAddComponent<global::EntityCollisionMessage>(base.gameObject.transform);
		}
		else
		{
			base.gameObject.transform.RemoveComponent<global::EntityCollisionMessage>();
		}
	}

	// Token: 0x17000009 RID: 9
	// (get) Token: 0x0600035F RID: 863 RVA: 0x00013918 File Offset: 0x00011B18
	public global::EntityComponentBase[] Components
	{
		get
		{
			global::EntityComponentBase[] result;
			if ((result = this._components) == null)
			{
				result = (this._components = base.GetComponentsInChildren<global::EntityComponentBase>(true));
			}
			return result;
		}
	}

	// Token: 0x06000360 RID: 864 RVA: 0x00013944 File Offset: 0x00011B44
	public virtual global::BasePlayer ToPlayer()
	{
		return null;
	}

	// Token: 0x06000361 RID: 865 RVA: 0x00013948 File Offset: 0x00011B48
	public override void InitShared()
	{
		base.InitShared();
		this.InitEntityLinks();
	}

	// Token: 0x06000362 RID: 866 RVA: 0x00013958 File Offset: 0x00011B58
	public override void DestroyShared()
	{
		base.DestroyShared();
		this.FreeEntityLinks();
	}

	// Token: 0x06000363 RID: 867 RVA: 0x00013968 File Offset: 0x00011B68
	public override void ResetState()
	{
		base.ResetState();
		this.parentBone = 0u;
		this.OwnerID = 0UL;
		this.flags = (global::BaseEntity.Flags)0;
		this.parentEntity = default(global::EntityRef);
		this._spawnable = null;
	}

	// Token: 0x06000364 RID: 868 RVA: 0x000139A8 File Offset: 0x00011BA8
	public OBB WorldSpaceBounds()
	{
		return new OBB(this.GetEstimatedWorldPosition(), base.transform.lossyScale, this.GetEstimatedWorldRotation(), this.bounds);
	}

	// Token: 0x06000365 RID: 869 RVA: 0x000139CC File Offset: 0x00011BCC
	public Vector3 PivotPoint()
	{
		return this.GetEstimatedWorldPosition();
	}

	// Token: 0x06000366 RID: 870 RVA: 0x000139D4 File Offset: 0x00011BD4
	public Vector3 CenterPoint()
	{
		return this.WorldSpaceBounds().position;
	}

	// Token: 0x06000367 RID: 871 RVA: 0x000139F0 File Offset: 0x00011BF0
	public Vector3 ClosestPoint(Vector3 position)
	{
		return this.WorldSpaceBounds().ClosestPoint(position);
	}

	// Token: 0x06000368 RID: 872 RVA: 0x00013A0C File Offset: 0x00011C0C
	public float Distance(Vector3 position)
	{
		return (this.ClosestPoint(position) - position).magnitude;
	}

	// Token: 0x06000369 RID: 873 RVA: 0x00013A30 File Offset: 0x00011C30
	public float SqrDistance(Vector3 position)
	{
		return (this.ClosestPoint(position) - position).sqrMagnitude;
	}

	// Token: 0x0600036A RID: 874 RVA: 0x00013A54 File Offset: 0x00011C54
	public float Distance(global::BaseEntity other)
	{
		return this.Distance(other.GetEstimatedWorldPosition());
	}

	// Token: 0x0600036B RID: 875 RVA: 0x00013A64 File Offset: 0x00011C64
	public float SqrDistance(global::BaseEntity other)
	{
		return this.SqrDistance(other.GetEstimatedWorldPosition());
	}

	// Token: 0x0600036C RID: 876 RVA: 0x00013A74 File Offset: 0x00011C74
	public float Distance2D(Vector3 position)
	{
		return Vector3Ex.Magnitude2D(this.ClosestPoint(position) - position);
	}

	// Token: 0x0600036D RID: 877 RVA: 0x00013A88 File Offset: 0x00011C88
	public float SqrDistance2D(Vector3 position)
	{
		return Vector3Ex.SqrMagnitude2D(this.ClosestPoint(position) - position);
	}

	// Token: 0x0600036E RID: 878 RVA: 0x00013A9C File Offset: 0x00011C9C
	public float Distance2D(global::BaseEntity other)
	{
		return this.Distance(other.GetEstimatedWorldPosition());
	}

	// Token: 0x0600036F RID: 879 RVA: 0x00013AAC File Offset: 0x00011CAC
	public float SqrDistance2D(global::BaseEntity other)
	{
		return this.SqrDistance(other.GetEstimatedWorldPosition());
	}

	// Token: 0x06000370 RID: 880 RVA: 0x00013ABC File Offset: 0x00011CBC
	public bool IsVisible(Ray ray, float maxDistance)
	{
		if (Vector3Ex.IsNaNOrInfinity(ray.origin))
		{
			return false;
		}
		if (Vector3Ex.IsNaNOrInfinity(ray.direction))
		{
			return false;
		}
		if (ray.direction == Vector3.zero)
		{
			return false;
		}
		RaycastHit raycastHit;
		if (!this.WorldSpaceBounds().Trace(ray, ref raycastHit, maxDistance))
		{
			return false;
		}
		RaycastHit hit;
		if (UnityEngine.Physics.Raycast(ray, ref hit, maxDistance, 1075904513))
		{
			if (hit.GetEntity() == this)
			{
				return true;
			}
			if (hit.distance <= raycastHit.distance)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000371 RID: 881 RVA: 0x00013B5C File Offset: 0x00011D5C
	public bool IsVisible(Vector3 position, Vector3 target)
	{
		Vector3 vector = target - position;
		float magnitude = vector.magnitude;
		if (magnitude < Mathf.Epsilon)
		{
			return true;
		}
		Vector3 vector2 = vector / magnitude;
		Vector3 vector3 = vector2 * Mathf.Min(magnitude, 0.01f);
		return this.IsVisible(new Ray(position + vector3, vector2), magnitude);
	}

	// Token: 0x06000372 RID: 882 RVA: 0x00013BB4 File Offset: 0x00011DB4
	public bool IsVisible(Vector3 position)
	{
		return this.IsVisible(position, this.CenterPoint()) || this.IsVisible(position, this.ClosestPoint(position));
	}

	// Token: 0x06000373 RID: 883 RVA: 0x00013BDC File Offset: 0x00011DDC
	public bool IsOlderThan(global::BaseEntity other)
	{
		if (other == null)
		{
			return true;
		}
		uint num = (this.net != null) ? this.net.ID : 0u;
		uint num2 = (other.net != null) ? other.net.ID : 0u;
		return num < num2;
	}

	// Token: 0x06000374 RID: 884 RVA: 0x00013C38 File Offset: 0x00011E38
	public virtual bool IsOutside()
	{
		OBB obb = this.WorldSpaceBounds();
		Vector3 position = obb.position + obb.up * obb.extents.y;
		return this.IsOutside(position);
	}

	// Token: 0x06000375 RID: 885 RVA: 0x00013C78 File Offset: 0x00011E78
	public bool IsOutside(Vector3 position)
	{
		return !UnityEngine.Physics.Raycast(position, Vector3.up, 100f, 1101070337);
	}

	// Token: 0x06000376 RID: 886 RVA: 0x00013CA0 File Offset: 0x00011EA0
	public float WaterFactor()
	{
		return global::WaterLevel.Factor(this.WorldSpaceBounds().ToBounds());
	}

	// Token: 0x06000377 RID: 887 RVA: 0x00013CC0 File Offset: 0x00011EC0
	public virtual float Health()
	{
		return 0f;
	}

	// Token: 0x06000378 RID: 888 RVA: 0x00013CC8 File Offset: 0x00011EC8
	public virtual float MaxHealth()
	{
		return 0f;
	}

	// Token: 0x06000379 RID: 889 RVA: 0x00013CD0 File Offset: 0x00011ED0
	public virtual float MaxVelocity()
	{
		return 0f;
	}

	// Token: 0x0600037A RID: 890 RVA: 0x00013CD8 File Offset: 0x00011ED8
	public virtual float BoundsPadding()
	{
		return 0.1f;
	}

	// Token: 0x0600037B RID: 891 RVA: 0x00013CE0 File Offset: 0x00011EE0
	public virtual float PenetrationResistance(global::HitInfo info)
	{
		return 1f;
	}

	// Token: 0x0600037C RID: 892 RVA: 0x00013CE8 File Offset: 0x00011EE8
	public virtual global::GameObjectRef GetImpactEffect(global::HitInfo info)
	{
		return this.impactEffect;
	}

	// Token: 0x0600037D RID: 893 RVA: 0x00013CF0 File Offset: 0x00011EF0
	public virtual void OnAttacked(global::HitInfo info)
	{
	}

	// Token: 0x0600037E RID: 894 RVA: 0x00013CF4 File Offset: 0x00011EF4
	public virtual global::Item GetItem()
	{
		return null;
	}

	// Token: 0x0600037F RID: 895 RVA: 0x00013CF8 File Offset: 0x00011EF8
	public virtual global::Item GetItem(uint itemId)
	{
		return null;
	}

	// Token: 0x06000380 RID: 896 RVA: 0x00013CFC File Offset: 0x00011EFC
	public virtual void GiveItem(global::Item item, global::BaseEntity.GiveItemReason reason = global::BaseEntity.GiveItemReason.Generic)
	{
		item.Remove(0f);
	}

	// Token: 0x06000381 RID: 897 RVA: 0x00013D0C File Offset: 0x00011F0C
	public virtual bool CanBeLooted(global::BasePlayer player)
	{
		return true;
	}

	// Token: 0x06000382 RID: 898 RVA: 0x00013D10 File Offset: 0x00011F10
	public virtual global::BaseEntity GetEntity()
	{
		return this;
	}

	// Token: 0x06000383 RID: 899 RVA: 0x00013D14 File Offset: 0x00011F14
	public override string ToString()
	{
		if (this._name == null)
		{
			if (base.isServer)
			{
				this._name = string.Format("{1}[{0}]", (this.net == null) ? 0u : this.net.ID, base.ShortPrefabName);
			}
			else
			{
				this._name = base.ShortPrefabName;
			}
		}
		return this._name;
	}

	// Token: 0x06000384 RID: 900 RVA: 0x00013D88 File Offset: 0x00011F88
	public virtual string Categorize()
	{
		return "entity";
	}

	// Token: 0x06000385 RID: 901 RVA: 0x00013D90 File Offset: 0x00011F90
	public void Log(string str)
	{
		if (base.isClient)
		{
			Debug.Log(string.Concat(new string[]
			{
				"<color=#ffa>[",
				this.ToString(),
				"] ",
				str,
				"</color>"
			}), base.gameObject);
		}
		else
		{
			Debug.Log(string.Concat(new string[]
			{
				"<color=#aff>[",
				this.ToString(),
				"] ",
				str,
				"</color>"
			}), base.gameObject);
		}
	}

	// Token: 0x06000386 RID: 902 RVA: 0x00013E24 File Offset: 0x00012024
	public void SetModel(global::Model mdl)
	{
		if (this.model == mdl)
		{
			return;
		}
		this.model = mdl;
	}

	// Token: 0x06000387 RID: 903 RVA: 0x00013E40 File Offset: 0x00012040
	public void SwitchToFastPhysics()
	{
		Rigidbody component = base.gameObject.GetComponent<Rigidbody>();
		if (component)
		{
			component.collisionDetectionMode = 0;
		}
	}

	// Token: 0x1700000A RID: 10
	// (get) Token: 0x06000388 RID: 904 RVA: 0x00013E6C File Offset: 0x0001206C
	// (set) Token: 0x06000389 RID: 905 RVA: 0x00013E74 File Offset: 0x00012074
	public ulong OwnerID { get; set; }

	// Token: 0x0600038A RID: 906 RVA: 0x00013E80 File Offset: 0x00012080
	public virtual bool ShouldBlockProjectiles()
	{
		return true;
	}

	// Token: 0x0600038B RID: 907 RVA: 0x00013E84 File Offset: 0x00012084
	public virtual void DebugServer(int rep, float time)
	{
		this.DebugText(base.transform.position + Vector3.up * 1f, string.Format("{0}: {1}\n{2}", (this.net == null) ? 0u : this.net.ID, base.name, this.DebugText()), Color.white, time);
	}

	// Token: 0x0600038C RID: 908 RVA: 0x00013EF4 File Offset: 0x000120F4
	public virtual string DebugText()
	{
		return string.Empty;
	}

	// Token: 0x0600038D RID: 909 RVA: 0x00013EFC File Offset: 0x000120FC
	public void OnDebugStart()
	{
		global::EntityDebug entityDebug = base.gameObject.GetComponent<global::EntityDebug>();
		if (entityDebug == null)
		{
			entityDebug = base.gameObject.AddComponent<global::EntityDebug>();
		}
		entityDebug.enabled = true;
	}

	// Token: 0x0600038E RID: 910 RVA: 0x00013F34 File Offset: 0x00012134
	protected void DebugText(Vector3 pos, string str, Color color, float time)
	{
		if (base.isServer)
		{
			global::ConsoleNetwork.BroadcastToAllClients("ddraw.text", new object[]
			{
				time,
				color,
				pos,
				str
			});
		}
	}

	// Token: 0x0600038F RID: 911 RVA: 0x00013F74 File Offset: 0x00012174
	public bool HasFlag(global::BaseEntity.Flags f)
	{
		return (this.flags & f) == f;
	}

	// Token: 0x06000390 RID: 912 RVA: 0x00013F84 File Offset: 0x00012184
	public bool ParentHasFlag(global::BaseEntity.Flags f)
	{
		global::BaseEntity baseEntity = this.GetParentEntity();
		return !(baseEntity == null) && baseEntity.HasFlag(f);
	}

	// Token: 0x06000391 RID: 913 RVA: 0x00013FB0 File Offset: 0x000121B0
	public void SetFlag(global::BaseEntity.Flags f, bool b, bool recursive = false)
	{
		global::BaseEntity.Flags old = this.flags;
		if (b)
		{
			if (this.HasFlag(f))
			{
				return;
			}
			this.flags |= f;
		}
		else
		{
			if (!this.HasFlag(f))
			{
				return;
			}
			this.flags &= ~f;
		}
		this.OnFlagsChanged(old, this.flags);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		if (recursive && this.children != null)
		{
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].SetFlag(f, b, true);
			}
		}
	}

	// Token: 0x06000392 RID: 914 RVA: 0x0001405C File Offset: 0x0001225C
	public bool IsOn()
	{
		return this.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000393 RID: 915 RVA: 0x00014068 File Offset: 0x00012268
	public bool IsOpen()
	{
		return this.HasFlag(global::BaseEntity.Flags.Open);
	}

	// Token: 0x06000394 RID: 916 RVA: 0x00014074 File Offset: 0x00012274
	public bool IsOnFire()
	{
		return this.HasFlag(global::BaseEntity.Flags.OnFire);
	}

	// Token: 0x06000395 RID: 917 RVA: 0x00014080 File Offset: 0x00012280
	public bool IsLocked()
	{
		return this.HasFlag(global::BaseEntity.Flags.Locked);
	}

	// Token: 0x06000396 RID: 918 RVA: 0x0001408C File Offset: 0x0001228C
	public override bool IsDebugging()
	{
		return this.HasFlag(global::BaseEntity.Flags.Debugging);
	}

	// Token: 0x06000397 RID: 919 RVA: 0x00014098 File Offset: 0x00012298
	public bool IsDisabled()
	{
		return this.HasFlag(global::BaseEntity.Flags.Disabled) || this.ParentHasFlag(global::BaseEntity.Flags.Disabled);
	}

	// Token: 0x06000398 RID: 920 RVA: 0x000140B4 File Offset: 0x000122B4
	public bool IsBroken()
	{
		return this.HasFlag(global::BaseEntity.Flags.Broken);
	}

	// Token: 0x06000399 RID: 921 RVA: 0x000140C4 File Offset: 0x000122C4
	public bool IsBusy()
	{
		return this.HasFlag(global::BaseEntity.Flags.Busy);
	}

	// Token: 0x0600039A RID: 922 RVA: 0x000140D4 File Offset: 0x000122D4
	public override string GetLogColor()
	{
		if (base.isServer)
		{
			return "cyan";
		}
		return "yellow";
	}

	// Token: 0x0600039B RID: 923 RVA: 0x000140EC File Offset: 0x000122EC
	public virtual void OnFlagsChanged(global::BaseEntity.Flags old, global::BaseEntity.Flags next)
	{
		if (this.IsDebugging() && (old & global::BaseEntity.Flags.Debugging) != (next & global::BaseEntity.Flags.Debugging))
		{
			this.OnDebugStart();
		}
	}

	// Token: 0x0600039C RID: 924 RVA: 0x0001410C File Offset: 0x0001230C
	public bool IsOccupied(global::Socket_Base socket)
	{
		global::EntityLink entityLink = this.FindLink(socket);
		return entityLink != null && entityLink.IsOccupied();
	}

	// Token: 0x0600039D RID: 925 RVA: 0x00014130 File Offset: 0x00012330
	public bool IsOccupied(string socketName)
	{
		global::EntityLink entityLink = this.FindLink(socketName);
		return entityLink != null && entityLink.IsOccupied();
	}

	// Token: 0x0600039E RID: 926 RVA: 0x00014154 File Offset: 0x00012354
	public global::EntityLink FindLink(global::Socket_Base socket)
	{
		List<global::EntityLink> entityLinks = this.GetEntityLinks(true);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			if (entityLinks[i].socket == socket)
			{
				return entityLinks[i];
			}
		}
		return null;
	}

	// Token: 0x0600039F RID: 927 RVA: 0x000141A0 File Offset: 0x000123A0
	public global::EntityLink FindLink(string socketName)
	{
		List<global::EntityLink> entityLinks = this.GetEntityLinks(true);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			if (entityLinks[i].socket.socketName == socketName)
			{
				return entityLinks[i];
			}
		}
		return null;
	}

	// Token: 0x060003A0 RID: 928 RVA: 0x000141F4 File Offset: 0x000123F4
	public T FindLinkedEntity<T>() where T : global::BaseEntity
	{
		List<global::EntityLink> entityLinks = this.GetEntityLinks(true);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			global::EntityLink entityLink = entityLinks[i];
			for (int j = 0; j < entityLink.connections.Count; j++)
			{
				global::EntityLink entityLink2 = entityLink.connections[j];
				if (entityLink2.owner is T)
				{
					return entityLink2.owner as T;
				}
			}
		}
		return (T)((object)null);
	}

	// Token: 0x060003A1 RID: 929 RVA: 0x0001427C File Offset: 0x0001247C
	public void EntityLinkMessage<T>(Action<T> action) where T : global::BaseEntity
	{
		List<global::EntityLink> entityLinks = this.GetEntityLinks(true);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			global::EntityLink entityLink = entityLinks[i];
			for (int j = 0; j < entityLink.connections.Count; j++)
			{
				global::EntityLink entityLink2 = entityLink.connections[j];
				if (entityLink2.owner is T)
				{
					action(entityLink2.owner as T);
				}
			}
		}
	}

	// Token: 0x060003A2 RID: 930 RVA: 0x00014304 File Offset: 0x00012504
	public void EntityLinkBroadcast<T>(Action<T> action) where T : global::BaseEntity
	{
		global::BaseEntity.globalBroadcastProtocol += 1u;
		global::BaseEntity.globalBroadcastQueue.Clear();
		this.broadcastProtocol = global::BaseEntity.globalBroadcastProtocol;
		global::BaseEntity.globalBroadcastQueue.Enqueue(this);
		if (this is T)
		{
			action(this as T);
		}
		while (global::BaseEntity.globalBroadcastQueue.Count > 0)
		{
			global::BaseEntity baseEntity = global::BaseEntity.globalBroadcastQueue.Dequeue();
			List<global::EntityLink> entityLinks = baseEntity.GetEntityLinks(true);
			for (int i = 0; i < entityLinks.Count; i++)
			{
				global::EntityLink entityLink = entityLinks[i];
				for (int j = 0; j < entityLink.connections.Count; j++)
				{
					global::BaseEntity owner = entityLink.connections[j].owner;
					if (owner.broadcastProtocol != global::BaseEntity.globalBroadcastProtocol)
					{
						owner.broadcastProtocol = global::BaseEntity.globalBroadcastProtocol;
						global::BaseEntity.globalBroadcastQueue.Enqueue(owner);
						if (owner is T)
						{
							action(owner as T);
						}
					}
				}
			}
		}
	}

	// Token: 0x060003A3 RID: 931 RVA: 0x00014420 File Offset: 0x00012620
	public void EntityLinkBroadcast()
	{
		global::BaseEntity.globalBroadcastProtocol += 1u;
		global::BaseEntity.globalBroadcastQueue.Clear();
		this.broadcastProtocol = global::BaseEntity.globalBroadcastProtocol;
		global::BaseEntity.globalBroadcastQueue.Enqueue(this);
		while (global::BaseEntity.globalBroadcastQueue.Count > 0)
		{
			global::BaseEntity baseEntity = global::BaseEntity.globalBroadcastQueue.Dequeue();
			List<global::EntityLink> entityLinks = baseEntity.GetEntityLinks(true);
			for (int i = 0; i < entityLinks.Count; i++)
			{
				global::EntityLink entityLink = entityLinks[i];
				for (int j = 0; j < entityLink.connections.Count; j++)
				{
					global::BaseEntity owner = entityLink.connections[j].owner;
					if (owner.broadcastProtocol != global::BaseEntity.globalBroadcastProtocol)
					{
						owner.broadcastProtocol = global::BaseEntity.globalBroadcastProtocol;
						global::BaseEntity.globalBroadcastQueue.Enqueue(owner);
					}
				}
			}
		}
	}

	// Token: 0x060003A4 RID: 932 RVA: 0x00014504 File Offset: 0x00012704
	public bool ReceivedEntityLinkBroadcast()
	{
		return this.broadcastProtocol == global::BaseEntity.globalBroadcastProtocol;
	}

	// Token: 0x060003A5 RID: 933 RVA: 0x00014514 File Offset: 0x00012714
	public List<global::EntityLink> GetEntityLinks(bool linkToNeighbours = true)
	{
		if (Application.isLoadingSave)
		{
			return this.links;
		}
		if (!this.linkedToNeighbours && linkToNeighbours)
		{
			this.LinkToNeighbours();
		}
		return this.links;
	}

	// Token: 0x060003A6 RID: 934 RVA: 0x00014544 File Offset: 0x00012744
	private void LinkToEntity(global::BaseEntity other)
	{
		if (this == other)
		{
			return;
		}
		if (this.links.Count == 0 || other.links.Count == 0)
		{
			return;
		}
		using (TimeWarning.New("LinkToEntity", 0.1f))
		{
			for (int i = 0; i < this.links.Count; i++)
			{
				global::EntityLink entityLink = this.links[i];
				for (int j = 0; j < other.links.Count; j++)
				{
					global::EntityLink entityLink2 = other.links[j];
					if (entityLink.CanConnect(entityLink2))
					{
						if (!entityLink.Contains(entityLink2))
						{
							entityLink.Add(entityLink2);
						}
						if (!entityLink2.Contains(entityLink))
						{
							entityLink2.Add(entityLink);
						}
					}
				}
			}
		}
	}

	// Token: 0x060003A7 RID: 935 RVA: 0x0001463C File Offset: 0x0001283C
	private void LinkToNeighbours()
	{
		if (this.links.Count == 0)
		{
			return;
		}
		this.linkedToNeighbours = true;
		using (TimeWarning.New("LinkToNeighbours", 0.1f))
		{
			List<global::BaseEntity> list = Facepunch.Pool.GetList<global::BaseEntity>();
			OBB obb = this.WorldSpaceBounds();
			global::Vis.Entities<global::BaseEntity>(obb.position, obb.extents.magnitude + 1f, list, -1, 2);
			for (int i = 0; i < list.Count; i++)
			{
				global::BaseEntity baseEntity = list[i];
				if (baseEntity.isServer == base.isServer)
				{
					this.LinkToEntity(baseEntity);
				}
			}
			Facepunch.Pool.FreeList<global::BaseEntity>(ref list);
		}
	}

	// Token: 0x060003A8 RID: 936 RVA: 0x00014704 File Offset: 0x00012904
	private void InitEntityLinks()
	{
		using (TimeWarning.New("InitEntityLinks", 0.1f))
		{
			if (base.isServer)
			{
				this.links.AddLinks(this, global::PrefabAttribute.server.FindAll<global::Socket_Base>(this.prefabID));
			}
		}
	}

	// Token: 0x060003A9 RID: 937 RVA: 0x0001476C File Offset: 0x0001296C
	private void FreeEntityLinks()
	{
		this.links.FreeLinks();
		this.linkedToNeighbours = false;
	}

	// Token: 0x060003AA RID: 938 RVA: 0x00014780 File Offset: 0x00012980
	public void RefreshEntityLinks()
	{
		this.links.ClearLinks();
		this.LinkToNeighbours();
	}

	// Token: 0x060003AB RID: 939 RVA: 0x00014794 File Offset: 0x00012994
	[global::BaseEntity.RPC_Server]
	public void SV_RequestFile(global::BaseEntity.RPCMessage msg)
	{
		uint num = msg.read.UInt32();
		global::FileStorage.Type type = (global::FileStorage.Type)msg.read.UInt8();
		string funcName = global::StringPool.Get(msg.read.UInt32());
		byte[] array = global::FileStorage.server.Get(num, type, this.net.ID);
		if (array == null)
		{
			return;
		}
		SendInfo sendInfo = new SendInfo(msg.connection);
		sendInfo.channel = 2;
		sendInfo.method = 0;
		this.ClientRPCEx<uint, uint, byte[]>(sendInfo, null, funcName, num, (uint)array.Length, array);
	}

	// Token: 0x060003AC RID: 940 RVA: 0x0001481C File Offset: 0x00012A1C
	public global::BaseEntity GetParentEntity()
	{
		return this.parentEntity.Get(base.isServer);
	}

	// Token: 0x060003AD RID: 941 RVA: 0x00014830 File Offset: 0x00012A30
	public void SetParent(global::BaseEntity entity, string strBone)
	{
		this.SetParent(entity, (!string.IsNullOrEmpty(strBone)) ? global::StringPool.Get(strBone) : 0u);
	}

	// Token: 0x060003AE RID: 942 RVA: 0x00014850 File Offset: 0x00012A50
	public bool HasChild(global::BaseEntity c)
	{
		if (c == this)
		{
			return true;
		}
		global::BaseEntity baseEntity = c.GetParentEntity();
		return baseEntity != null && this.HasChild(baseEntity);
	}

	// Token: 0x060003AF RID: 943 RVA: 0x00014888 File Offset: 0x00012A88
	public bool HasParent()
	{
		return this.parentEntity.IsValid(base.isServer);
	}

	// Token: 0x060003B0 RID: 944 RVA: 0x0001489C File Offset: 0x00012A9C
	public void SetParent(global::BaseEntity entity, uint boneID = 0u)
	{
		if (entity != null)
		{
			if (entity == this)
			{
				Debug.LogError("Trying to parent to self " + this, base.gameObject);
				return;
			}
			if (this.HasChild(entity))
			{
				Debug.LogError("Trying to parent to child " + this, base.gameObject);
				return;
			}
		}
		base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Hierarchy, 2, "SetParent {0} {1}", entity, boneID);
		global::BaseEntity baseEntity = this.GetParentEntity();
		if (baseEntity)
		{
			baseEntity.RemoveChild(this);
		}
		if (base.limitNetworking && baseEntity != null && baseEntity != entity)
		{
			global::BasePlayer basePlayer = baseEntity as global::BasePlayer;
			if (basePlayer.IsValid())
			{
				this.DestroyOnClient(basePlayer.net.connection);
			}
		}
		if (entity == null)
		{
			if (this.parentEntity.IsValid(base.isServer))
			{
				this.parentEntity.Set(null);
				base.transform.SetParent(null, false);
				this.parentBone = 0u;
				this.UpdateNetworkGroup();
				base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
				this.SendChildrenNetworkUpdate();
			}
			return;
		}
		Debug.Assert(entity.isServer, "SetParent - child should be a SERVER entity");
		Debug.Assert(entity.net != null, "Setting parent to entity that hasn't spawned yet! (net is null)");
		Debug.Assert(entity.net.ID != 0u, "Setting parent to entity that hasn't spawned yet! (id = 0)");
		entity.AddChild(this);
		this.parentEntity.Set(entity);
		base.transform.SetParent(entity.transform, false);
		this.parentBone = boneID;
		this.UpdateNetworkGroup();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		this.SendChildrenNetworkUpdate();
	}

	// Token: 0x060003B1 RID: 945 RVA: 0x00014A44 File Offset: 0x00012C44
	private void DestroyOnClient(Connection connection)
	{
		if (this.children != null)
		{
			foreach (global::BaseEntity baseEntity in this.children)
			{
				baseEntity.DestroyOnClient(connection);
			}
		}
		if (Network.Net.sv.IsConnected() && Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(6);
			Network.Net.sv.write.EntityID(this.net.ID);
			Network.Net.sv.write.UInt8(0);
			Network.Net.sv.write.Send(new SendInfo(connection));
			base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "EntityDestroy");
		}
	}

	// Token: 0x060003B2 RID: 946 RVA: 0x00014B2C File Offset: 0x00012D2C
	private void SendChildrenNetworkUpdate()
	{
		if (this.children == null)
		{
			return;
		}
		foreach (global::BaseEntity baseEntity in this.children)
		{
			baseEntity.UpdateNetworkGroup();
			baseEntity.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x060003B3 RID: 947 RVA: 0x00014B9C File Offset: 0x00012D9C
	public virtual void ParentBecoming(global::BaseEntity ent)
	{
		this.Log("ParentBecoming Missed " + ent);
	}

	// Token: 0x060003B4 RID: 948 RVA: 0x00014BB0 File Offset: 0x00012DB0
	public Vector3 GetEstimatedWorldPosition()
	{
		return base.transform.position;
	}

	// Token: 0x060003B5 RID: 949 RVA: 0x00014BC0 File Offset: 0x00012DC0
	public Quaternion GetEstimatedWorldRotation()
	{
		return base.transform.rotation;
	}

	// Token: 0x060003B6 RID: 950 RVA: 0x00014BD0 File Offset: 0x00012DD0
	public virtual global::BuildingPrivlidge GetBuildingPrivilege()
	{
		return this.GetBuildingPrivilege(this.WorldSpaceBounds());
	}

	// Token: 0x060003B7 RID: 951 RVA: 0x00014BE0 File Offset: 0x00012DE0
	public global::BuildingPrivlidge GetBuildingPrivilege(OBB obb)
	{
		global::BuildingBlock other = null;
		global::BuildingPrivlidge result = null;
		List<global::BuildingBlock> list = Facepunch.Pool.GetList<global::BuildingBlock>();
		global::Vis.Entities<global::BuildingBlock>(obb.position, 16f + obb.extents.magnitude, list, 2097152, 2);
		for (int i = 0; i < list.Count; i++)
		{
			global::BuildingBlock buildingBlock = list[i];
			if (buildingBlock.isServer == base.isServer)
			{
				if (buildingBlock.IsOlderThan(other))
				{
					if (obb.Distance(buildingBlock.WorldSpaceBounds()) <= 16f)
					{
						global::BuildingManager.Building building = buildingBlock.GetBuilding();
						if (building != null)
						{
							global::BuildingPrivlidge dominatingBuildingPrivilege = building.GetDominatingBuildingPrivilege();
							if (!(dominatingBuildingPrivilege == null))
							{
								other = buildingBlock;
								result = dominatingBuildingPrivilege;
							}
						}
					}
				}
			}
		}
		Facepunch.Pool.FreeList<global::BuildingBlock>(ref list);
		return result;
	}

	// Token: 0x060003B8 RID: 952 RVA: 0x00014CC0 File Offset: 0x00012EC0
	public void SV_RPCMessage(uint nameID, Message message)
	{
		Assert.IsTrue(base.isServer, "Should be server!");
		global::BasePlayer basePlayer = message.Player();
		if (!basePlayer.IsValid())
		{
			if (ConVar.Global.developer > 0)
			{
				Debug.Log("SV_RPCMessage: From invalid player " + basePlayer);
			}
			return;
		}
		if (basePlayer.isStalled)
		{
			if (ConVar.Global.developer > 0)
			{
				Debug.Log("SV_RPCMessage: player is stalled " + basePlayer);
			}
			return;
		}
		if (this.OnRpcMessage(basePlayer, nameID, message))
		{
			return;
		}
		for (int i = 0; i < this.Components.Length; i++)
		{
			if (this.Components[i].OnRpcMessage(basePlayer, nameID, message))
			{
				return;
			}
		}
	}

	// Token: 0x060003B9 RID: 953 RVA: 0x00014D70 File Offset: 0x00012F70
	public void ClientRPCPlayer<T1, T2, T3, T4, T5>(Connection sourceConnection, global::BasePlayer player, string funcName, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (player.net.connection == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2, T3, T4, T5>(new SendInfo(player.net.connection), sourceConnection, funcName, arg1, arg2, arg3, arg4, arg5);
	}

	// Token: 0x060003BA RID: 954 RVA: 0x00014DCC File Offset: 0x00012FCC
	public void ClientRPCPlayer<T1, T2, T3, T4>(Connection sourceConnection, global::BasePlayer player, string funcName, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (player.net.connection == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2, T3, T4>(new SendInfo(player.net.connection), sourceConnection, funcName, arg1, arg2, arg3, arg4);
	}

	// Token: 0x060003BB RID: 955 RVA: 0x00014E28 File Offset: 0x00013028
	public void ClientRPCPlayer<T1, T2, T3>(Connection sourceConnection, global::BasePlayer player, string funcName, T1 arg1, T2 arg2, T3 arg3)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (player.net.connection == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2, T3>(new SendInfo(player.net.connection), sourceConnection, funcName, arg1, arg2, arg3);
	}

	// Token: 0x060003BC RID: 956 RVA: 0x00014E80 File Offset: 0x00013080
	public void ClientRPCPlayer<T1, T2>(Connection sourceConnection, global::BasePlayer player, string funcName, T1 arg1, T2 arg2)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (player.net.connection == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2>(new SendInfo(player.net.connection), sourceConnection, funcName, arg1, arg2);
	}

	// Token: 0x060003BD RID: 957 RVA: 0x00014ED8 File Offset: 0x000130D8
	public void ClientRPCPlayer<T1>(Connection sourceConnection, global::BasePlayer player, string funcName, T1 arg1)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (player.net.connection == null)
		{
			return;
		}
		this.ClientRPCEx<T1>(new SendInfo(player.net.connection), sourceConnection, funcName, arg1);
	}

	// Token: 0x060003BE RID: 958 RVA: 0x00014F2C File Offset: 0x0001312C
	public void ClientRPCPlayer(Connection sourceConnection, global::BasePlayer player, string funcName)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (player.net.connection == null)
		{
			return;
		}
		this.ClientRPCEx(new SendInfo(player.net.connection), sourceConnection, funcName);
	}

	// Token: 0x060003BF RID: 959 RVA: 0x00014F80 File Offset: 0x00013180
	public void ClientRPC<T1, T2, T3, T4, T5>(Connection sourceConnection, string funcName, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2, T3, T4, T5>(new SendInfo(this.net.group.subscribers), sourceConnection, funcName, arg1, arg2, arg3, arg4, arg5);
	}

	// Token: 0x060003C0 RID: 960 RVA: 0x00014FE0 File Offset: 0x000131E0
	public void ClientRPC<T1, T2, T3, T4>(Connection sourceConnection, string funcName, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2, T3, T4>(new SendInfo(this.net.group.subscribers), sourceConnection, funcName, arg1, arg2, arg3, arg4);
	}

	// Token: 0x060003C1 RID: 961 RVA: 0x00015040 File Offset: 0x00013240
	public void ClientRPC<T1, T2, T3>(Connection sourceConnection, string funcName, T1 arg1, T2 arg2, T3 arg3)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2, T3>(new SendInfo(this.net.group.subscribers), sourceConnection, funcName, arg1, arg2, arg3);
	}

	// Token: 0x060003C2 RID: 962 RVA: 0x0001509C File Offset: 0x0001329C
	public void ClientRPC<T1, T2>(Connection sourceConnection, string funcName, T1 arg1, T2 arg2)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		this.ClientRPCEx<T1, T2>(new SendInfo(this.net.group.subscribers), sourceConnection, funcName, arg1, arg2);
	}

	// Token: 0x060003C3 RID: 963 RVA: 0x000150F8 File Offset: 0x000132F8
	public void ClientRPC<T1>(Connection sourceConnection, string funcName, T1 arg1)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		this.ClientRPCEx<T1>(new SendInfo(this.net.group.subscribers), sourceConnection, funcName, arg1);
	}

	// Token: 0x060003C4 RID: 964 RVA: 0x00015150 File Offset: 0x00013350
	public void ClientRPC(Connection sourceConnection, string funcName)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		this.ClientRPCEx(new SendInfo(this.net.group.subscribers), sourceConnection, funcName);
	}

	// Token: 0x060003C5 RID: 965 RVA: 0x000151A8 File Offset: 0x000133A8
	public void ClientRPCEx<T1, T2, T3, T4, T5>(SendInfo sendInfo, Connection sourceConnection, string funcName, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.ClientRPCStart(sourceConnection, funcName))
		{
			this.ClientRPCWrite<T1>(arg1);
			this.ClientRPCWrite<T2>(arg2);
			this.ClientRPCWrite<T3>(arg3);
			this.ClientRPCWrite<T4>(arg4);
			this.ClientRPCWrite<T5>(arg5);
			this.ClientRPCSend(sendInfo);
		}
	}

	// Token: 0x060003C6 RID: 966 RVA: 0x00015210 File Offset: 0x00013410
	public void ClientRPCEx<T1, T2, T3, T4>(SendInfo sendInfo, Connection sourceConnection, string funcName, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.ClientRPCStart(sourceConnection, funcName))
		{
			this.ClientRPCWrite<T1>(arg1);
			this.ClientRPCWrite<T2>(arg2);
			this.ClientRPCWrite<T3>(arg3);
			this.ClientRPCWrite<T4>(arg4);
			this.ClientRPCSend(sendInfo);
		}
	}

	// Token: 0x060003C7 RID: 967 RVA: 0x00015270 File Offset: 0x00013470
	public void ClientRPCEx<T1, T2, T3>(SendInfo sendInfo, Connection sourceConnection, string funcName, T1 arg1, T2 arg2, T3 arg3)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.ClientRPCStart(sourceConnection, funcName))
		{
			this.ClientRPCWrite<T1>(arg1);
			this.ClientRPCWrite<T2>(arg2);
			this.ClientRPCWrite<T3>(arg3);
			this.ClientRPCSend(sendInfo);
		}
	}

	// Token: 0x060003C8 RID: 968 RVA: 0x000152C8 File Offset: 0x000134C8
	public void ClientRPCEx<T1, T2>(SendInfo sendInfo, Connection sourceConnection, string funcName, T1 arg1, T2 arg2)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.ClientRPCStart(sourceConnection, funcName))
		{
			this.ClientRPCWrite<T1>(arg1);
			this.ClientRPCWrite<T2>(arg2);
			this.ClientRPCSend(sendInfo);
		}
	}

	// Token: 0x060003C9 RID: 969 RVA: 0x00015318 File Offset: 0x00013518
	public void ClientRPCEx<T1>(SendInfo sendInfo, Connection sourceConnection, string funcName, T1 arg1)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.ClientRPCStart(sourceConnection, funcName))
		{
			this.ClientRPCWrite<T1>(arg1);
			this.ClientRPCSend(sendInfo);
		}
	}

	// Token: 0x060003CA RID: 970 RVA: 0x00015354 File Offset: 0x00013554
	public void ClientRPCEx(SendInfo sendInfo, Connection sourceConnection, string funcName)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (this.ClientRPCStart(sourceConnection, funcName))
		{
			this.ClientRPCSend(sendInfo);
		}
	}

	// Token: 0x060003CB RID: 971 RVA: 0x00015388 File Offset: 0x00013588
	private bool ClientRPCStart(Connection sourceConnection, string funcName)
	{
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(9);
			Network.Net.sv.write.UInt32(this.net.ID);
			Network.Net.sv.write.UInt32(global::StringPool.Get(funcName));
			Network.Net.sv.write.UInt64((sourceConnection != null) ? sourceConnection.userid : 0UL);
			return true;
		}
		return false;
	}

	// Token: 0x060003CC RID: 972 RVA: 0x00015410 File Offset: 0x00013610
	private void ClientRPCWrite<T>(T arg)
	{
		Network.Net.sv.write.WriteObject(arg);
	}

	// Token: 0x060003CD RID: 973 RVA: 0x00015424 File Offset: 0x00013624
	private void ClientRPCSend(SendInfo sendInfo)
	{
		Network.Net.sv.write.Send(sendInfo);
	}

	// Token: 0x060003CE RID: 974 RVA: 0x00015438 File Offset: 0x00013638
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.baseEntity = Facepunch.Pool.Get<ProtoBuf.BaseEntity>();
		if (info.forDisk)
		{
			info.msg.baseEntity.pos = base.transform.localPosition;
			info.msg.baseEntity.rot = base.transform.localRotation.eulerAngles;
		}
		else
		{
			info.msg.baseEntity.pos = this.GetNetworkPosition();
			info.msg.baseEntity.rot = this.GetNetworkRotation();
		}
		info.msg.baseEntity.flags = (int)this.flags;
		info.msg.baseEntity.skinid = this.skinID;
		if (this.parentEntity.IsValid(base.isServer))
		{
			info.msg.parent = Facepunch.Pool.Get<ParentInfo>();
			info.msg.parent.uid = this.parentEntity.uid;
			info.msg.parent.bone = this.parentBone;
		}
		if (this.HasAnySlot())
		{
			info.msg.entitySlots = Facepunch.Pool.Get<EntitySlots>();
			info.msg.entitySlots.slotLock = this.entitySlots[0].uid;
			info.msg.entitySlots.slotFireMod = this.entitySlots[1].uid;
			info.msg.entitySlots.slotUpperModification = this.entitySlots[2].uid;
			info.msg.entitySlots.centerDecoration = this.entitySlots[5].uid;
		}
		if (info.forDisk && this._spawnable)
		{
			this._spawnable.Save(info);
		}
		if (this.OwnerID != 0UL && (info.forDisk || this.ShouldNetworkOwnerInfo()))
		{
			info.msg.ownerInfo = Facepunch.Pool.Get<OwnerInfo>();
			info.msg.ownerInfo.steamid = this.OwnerID;
		}
	}

	// Token: 0x060003CF RID: 975 RVA: 0x0001567C File Offset: 0x0001387C
	public virtual bool ShouldNetworkOwnerInfo()
	{
		return false;
	}

	// Token: 0x060003D0 RID: 976 RVA: 0x00015680 File Offset: 0x00013880
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.baseEntity != null)
		{
			ProtoBuf.BaseEntity baseEntity = info.msg.baseEntity;
			global::BaseEntity.Flags old = this.flags;
			this.flags = (global::BaseEntity.Flags)baseEntity.flags;
			this.OnFlagsChanged(old, this.flags);
			this.OnSkinChanged(this.skinID, info.msg.baseEntity.skinid);
			if (info.fromDisk)
			{
				if (Vector3Ex.IsNaNOrInfinity(baseEntity.pos))
				{
					Debug.LogWarning(this.ToString() + " has broken position - " + baseEntity.pos);
					baseEntity.pos = Vector3.zero;
				}
				base.transform.localPosition = baseEntity.pos;
				base.transform.localRotation = Quaternion.Euler(baseEntity.rot);
			}
		}
		if (info.msg.entitySlots != null)
		{
			this.entitySlots[0].uid = info.msg.entitySlots.slotLock;
			this.entitySlots[1].uid = info.msg.entitySlots.slotFireMod;
			this.entitySlots[2].uid = info.msg.entitySlots.slotUpperModification;
			this.entitySlots[5].uid = info.msg.entitySlots.centerDecoration;
		}
		if (info.msg.parent != null)
		{
			if (base.isServer)
			{
				global::BaseEntity entity = global::BaseNetworkable.serverEntities.Find(info.msg.parent.uid) as global::BaseEntity;
				this.SetParent(entity, 0u);
			}
			this.parentEntity.uid = info.msg.parent.uid;
			this.parentBone = info.msg.parent.bone;
		}
		else
		{
			this.parentEntity.uid = 0u;
			this.parentBone = 0u;
		}
		if (info.msg.ownerInfo != null)
		{
			this.OwnerID = info.msg.ownerInfo.steamid;
		}
		if (this._spawnable)
		{
			this._spawnable.Load(info);
		}
	}

	// Token: 0x060003D1 RID: 977 RVA: 0x000158D0 File Offset: 0x00013AD0
	public override void ServerInit()
	{
		this._spawnable = base.GetComponent<global::Spawnable>();
		base.ServerInit();
		if (this.enableSaving)
		{
			Assert.IsTrue(!global::BaseEntity.saveList.Contains(this), "Already in save list - server Init being called twice?");
			global::BaseEntity.saveList.Add(this);
		}
		if (this.flags != (global::BaseEntity.Flags)0)
		{
			this.OnFlagsChanged((global::BaseEntity.Flags)0, this.flags);
		}
		if (this.syncPosition && this.PositionTickRate >= 0f)
		{
			base.InvokeRandomized(new Action(this.NetworkPositionTick), this.PositionTickRate, this.PositionTickRate - this.PositionTickRate * 0.05f, this.PositionTickRate * 0.05f);
		}
		global::BaseEntity.Query.Server.Add(this);
	}

	// Token: 0x060003D2 RID: 978 RVA: 0x00015994 File Offset: 0x00013B94
	public virtual void OnSensation(Rust.Ai.Sensation sensation)
	{
	}

	// Token: 0x1700000B RID: 11
	// (get) Token: 0x060003D3 RID: 979 RVA: 0x00015998 File Offset: 0x00013B98
	protected virtual float PositionTickRate
	{
		get
		{
			return 0.1f;
		}
	}

	// Token: 0x060003D4 RID: 980 RVA: 0x000159A0 File Offset: 0x00013BA0
	protected void NetworkPositionTick()
	{
		if (!base.transform.hasChanged)
		{
			return;
		}
		this.TransformChanged();
		base.transform.hasChanged = false;
	}

	// Token: 0x060003D5 RID: 981 RVA: 0x000159C8 File Offset: 0x00013BC8
	private void TransformChanged()
	{
		if (global::BaseEntity.Query.Server != null)
		{
			global::BaseEntity.Query.Server.Move(this);
		}
		if (this.net == null)
		{
			return;
		}
		base.InvalidateNetworkCache();
		if (!this.globalBroadcast && !global::ValidBounds.Test(base.transform.position))
		{
			this.OnInvalidPosition();
			return;
		}
		if (this.syncPosition)
		{
			if (!this.isCallingUpdateNetworkGroup)
			{
				base.Invoke(new Action(this.UpdateNetworkGroup), 5f);
				this.isCallingUpdateNetworkGroup = true;
			}
			base.SendNetworkUpdate_Position();
			this.OnPositionalNetworkUpdate();
		}
	}

	// Token: 0x060003D6 RID: 982 RVA: 0x00015A64 File Offset: 0x00013C64
	public virtual void OnPositionalNetworkUpdate()
	{
	}

	// Token: 0x060003D7 RID: 983 RVA: 0x00015A68 File Offset: 0x00013C68
	public void DoMovingWithoutARigidBodyCheck()
	{
		if (this.doneMovingWithoutARigidBodyCheck > 10)
		{
			return;
		}
		this.doneMovingWithoutARigidBodyCheck++;
		if (this.doneMovingWithoutARigidBodyCheck < 10)
		{
			return;
		}
		if (base.GetComponent<Collider>() == null)
		{
			return;
		}
		if (base.GetComponent<Rigidbody>() == null)
		{
			Debug.LogWarning("Entity moving without a rigid body! (" + base.gameObject + ")", this);
		}
	}

	// Token: 0x060003D8 RID: 984 RVA: 0x00015AE0 File Offset: 0x00013CE0
	public void SpawnAsMapEntity()
	{
		if (this.net != null)
		{
			return;
		}
		if (base.IsDestroyed)
		{
			return;
		}
		this.Spawn();
	}

	// Token: 0x060003D9 RID: 985 RVA: 0x00015B00 File Offset: 0x00013D00
	internal override void DoServerDestroy()
	{
		base.CancelInvoke(new Action(this.NetworkPositionTick));
		global::BaseEntity.saveList.Remove(this);
		this.RemoveFromTriggers();
		if (this.children != null)
		{
			foreach (global::BaseEntity baseEntity in this.children.ToArray())
			{
				baseEntity.OnParentRemoved();
			}
		}
		this.SetParent(null, 0u);
		global::BaseEntity.Query.Server.Remove(this, false);
		base.DoServerDestroy();
	}

	// Token: 0x060003DA RID: 986 RVA: 0x00015B80 File Offset: 0x00013D80
	internal virtual void OnParentRemoved()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x060003DB RID: 987 RVA: 0x00015B8C File Offset: 0x00013D8C
	public virtual void OnInvalidPosition()
	{
		Debug.Log(string.Concat(new object[]
		{
			"Invalid Position: ",
			this,
			" ",
			base.transform.position,
			" (destroying)"
		}));
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x060003DC RID: 988 RVA: 0x00015BE0 File Offset: 0x00013DE0
	public global::BaseCorpse DropCorpse(string strCorpsePrefab)
	{
		Assert.IsTrue(base.isServer, "DropCorpse called on client!");
		if (!ConVar.Server.corpses)
		{
			return null;
		}
		if (string.IsNullOrEmpty(strCorpsePrefab))
		{
			return null;
		}
		global::BaseCorpse baseCorpse = global::GameManager.server.CreateEntity(strCorpsePrefab, default(Vector3), default(Quaternion), true) as global::BaseCorpse;
		if (baseCorpse == null)
		{
			Debug.LogWarning(string.Concat(new object[]
			{
				"Error creating corpse: ",
				base.gameObject,
				" - ",
				strCorpsePrefab
			}));
			return null;
		}
		baseCorpse.InitCorpse(this);
		return baseCorpse;
	}

	// Token: 0x060003DD RID: 989 RVA: 0x00015C80 File Offset: 0x00013E80
	public override void UpdateNetworkGroup()
	{
		this.isCallingUpdateNetworkGroup = false;
		if (this.net == null)
		{
			return;
		}
		if (Network.Net.sv == null)
		{
			return;
		}
		if (Network.Net.sv.visibility == null)
		{
			return;
		}
		using (TimeWarning.New("UpdateNetworkGroup", 0.1f))
		{
			if (this.globalBroadcast)
			{
				if (this.net.SwitchGroup(global::BaseNetworkable.GlobalNetworkGroup))
				{
					base.SendNetworkGroupChange();
				}
			}
			else if (this.parentEntity.IsSet())
			{
				global::BaseEntity baseEntity = this.GetParentEntity();
				if (!baseEntity.IsValid())
				{
					Debug.LogWarning("UpdateNetworkGroup: Missing parent entity " + this.parentEntity.uid);
					base.Invoke(new Action(this.UpdateNetworkGroup), 2f);
					this.isCallingUpdateNetworkGroup = true;
				}
				else if (baseEntity != null)
				{
					if (this.net.SwitchGroup(baseEntity.net.group))
					{
						base.SendNetworkGroupChange();
					}
				}
				else
				{
					Debug.LogWarning(base.gameObject + ": has parent id - but couldn't find parent! " + this.parentEntity);
				}
			}
			else
			{
				base.UpdateNetworkGroup();
			}
		}
	}

	// Token: 0x060003DE RID: 990 RVA: 0x00015DDC File Offset: 0x00013FDC
	public virtual void Eat(global::BaseNpc baseNpc, float timeSpent)
	{
		baseNpc.AddCalories(100f);
	}

	// Token: 0x060003DF RID: 991 RVA: 0x00015DEC File Offset: 0x00013FEC
	public virtual void OnDeployed(global::BaseEntity parent)
	{
	}

	// Token: 0x060003E0 RID: 992 RVA: 0x00015DF0 File Offset: 0x00013FF0
	public override bool ShouldNetworkTo(global::BasePlayer player)
	{
		if (player == this)
		{
			return true;
		}
		global::BaseEntity baseEntity = this.GetParentEntity();
		if (base.limitNetworking)
		{
			if (baseEntity == null)
			{
				return false;
			}
			if (baseEntity != player)
			{
				return false;
			}
		}
		if (baseEntity != null)
		{
			return baseEntity.ShouldNetworkTo(player);
		}
		return base.ShouldNetworkTo(player);
	}

	// Token: 0x060003E1 RID: 993 RVA: 0x00015E54 File Offset: 0x00014054
	public virtual void AttackerInfo(PlayerLifeStory.DeathInfo info)
	{
		info.attackerName = base.ShortPrefabName;
		info.attackerSteamID = 0UL;
		info.inflictorName = string.Empty;
	}

	// Token: 0x060003E2 RID: 994 RVA: 0x00015E78 File Offset: 0x00014078
	public virtual void Push(Vector3 velocity)
	{
		this.SetVelocity(velocity);
	}

	// Token: 0x060003E3 RID: 995 RVA: 0x00015E84 File Offset: 0x00014084
	public virtual void SetVelocity(Vector3 velocity)
	{
		Rigidbody component = base.GetComponent<Rigidbody>();
		if (component)
		{
			component.velocity = velocity;
		}
	}

	// Token: 0x060003E4 RID: 996 RVA: 0x00015EAC File Offset: 0x000140AC
	public virtual void SetAngularVelocity(Vector3 velocity)
	{
		Rigidbody component = base.GetComponent<Rigidbody>();
		if (component)
		{
			component.angularVelocity = velocity;
		}
	}

	// Token: 0x060003E5 RID: 997 RVA: 0x00015ED4 File Offset: 0x000140D4
	public virtual Vector3 GetDropPosition()
	{
		return base.transform.position;
	}

	// Token: 0x060003E6 RID: 998 RVA: 0x00015EE4 File Offset: 0x000140E4
	public virtual Vector3 GetDropVelocity()
	{
		return Vector3.up;
	}

	// Token: 0x060003E7 RID: 999 RVA: 0x00015EEC File Offset: 0x000140EC
	public virtual bool OnStartBeingLooted(global::BasePlayer baseEntity)
	{
		return true;
	}

	// Token: 0x1700000C RID: 12
	// (get) Token: 0x060003E8 RID: 1000 RVA: 0x00015EF0 File Offset: 0x000140F0
	// (set) Token: 0x060003E9 RID: 1001 RVA: 0x00015F00 File Offset: 0x00014100
	public virtual Vector3 ServerPosition
	{
		get
		{
			return base.transform.localPosition;
		}
		set
		{
			if (base.transform.localPosition == value)
			{
				return;
			}
			base.transform.localPosition = value;
			base.transform.hasChanged = true;
		}
	}

	// Token: 0x1700000D RID: 13
	// (get) Token: 0x060003EA RID: 1002 RVA: 0x00015F34 File Offset: 0x00014134
	// (set) Token: 0x060003EB RID: 1003 RVA: 0x00015F44 File Offset: 0x00014144
	public virtual Quaternion ServerRotation
	{
		get
		{
			return base.transform.localRotation;
		}
		set
		{
			if (base.transform.localRotation == value)
			{
				return;
			}
			base.transform.localRotation = value;
			base.transform.hasChanged = true;
		}
	}

	// Token: 0x1700000E RID: 14
	// (get) Token: 0x060003EC RID: 1004 RVA: 0x00015F78 File Offset: 0x00014178
	public float radiationLevel
	{
		get
		{
			if (this.triggers == null)
			{
				return 0f;
			}
			float num = 0f;
			for (int i = 0; i < this.triggers.Count; i++)
			{
				global::TriggerRadiation triggerRadiation = this.triggers[i] as global::TriggerRadiation;
				if (!(triggerRadiation == null))
				{
					num = Mathf.Max(num, triggerRadiation.GetRadiation(this.GetNetworkPosition(), this.RadiationProtection()));
				}
			}
			return num;
		}
	}

	// Token: 0x060003ED RID: 1005 RVA: 0x00015FF8 File Offset: 0x000141F8
	public virtual float RadiationProtection()
	{
		return 0f;
	}

	// Token: 0x060003EE RID: 1006 RVA: 0x00016000 File Offset: 0x00014200
	public virtual float RadiationExposureFraction()
	{
		return 1f;
	}

	// Token: 0x1700000F RID: 15
	// (get) Token: 0x060003EF RID: 1007 RVA: 0x00016008 File Offset: 0x00014208
	public float currentTemperature
	{
		get
		{
			float num = global::Climate.GetTemperature(base.transform.position);
			if (this.triggers == null)
			{
				return num;
			}
			for (int i = 0; i < this.triggers.Count; i++)
			{
				global::TriggerTemperature triggerTemperature = this.triggers[i] as global::TriggerTemperature;
				if (!(triggerTemperature == null))
				{
					num = triggerTemperature.WorkoutTemperature(this.GetNetworkPosition(), num);
				}
			}
			return num;
		}
	}

	// Token: 0x060003F0 RID: 1008 RVA: 0x00016084 File Offset: 0x00014284
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	private void BroadcastSignalFromClient(global::BaseEntity.RPCMessage msg)
	{
		global::BaseEntity.Signal signal = (global::BaseEntity.Signal)msg.read.Int32();
		string arg = msg.read.String();
		this.SignalBroadcast(signal, arg, msg.connection);
	}

	// Token: 0x060003F1 RID: 1009 RVA: 0x000160BC File Offset: 0x000142BC
	public void SignalBroadcast(global::BaseEntity.Signal signal, string arg, Connection sourceConnection = null)
	{
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		SendInfo sendInfo = new SendInfo(this.net.group.subscribers);
		sendInfo.method = 3;
		sendInfo.priority = 0;
		this.ClientRPCEx<int, string>(sendInfo, sourceConnection, "SignalFromServerEx", (int)signal, arg);
	}

	// Token: 0x060003F2 RID: 1010 RVA: 0x0001611C File Offset: 0x0001431C
	public void SignalBroadcast(global::BaseEntity.Signal signal, Connection sourceConnection = null)
	{
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		SendInfo sendInfo = new SendInfo(this.net.group.subscribers);
		sendInfo.method = 3;
		sendInfo.priority = 0;
		this.ClientRPCEx<int>(sendInfo, sourceConnection, "SignalFromServer", (int)signal);
	}

	// Token: 0x060003F3 RID: 1011 RVA: 0x0001617C File Offset: 0x0001437C
	private void OnSkinChanged(ulong oldSkinID, ulong newSkinID)
	{
		if (oldSkinID == newSkinID)
		{
			return;
		}
		this.skinID = newSkinID;
	}

	// Token: 0x060003F4 RID: 1012 RVA: 0x00016190 File Offset: 0x00014390
	public bool HasAnySlot()
	{
		for (int i = 0; i < this.entitySlots.Length; i++)
		{
			if (this.entitySlots[i].IsValid(base.isServer))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060003F5 RID: 1013 RVA: 0x000161D8 File Offset: 0x000143D8
	public global::BaseEntity GetSlot(global::BaseEntity.Slot slot)
	{
		return this.entitySlots[(int)slot].Get(base.isServer);
	}

	// Token: 0x060003F6 RID: 1014 RVA: 0x000161F4 File Offset: 0x000143F4
	public string GetSlotAnchorName(global::BaseEntity.Slot slot)
	{
		return slot.ToString().ToLower();
	}

	// Token: 0x060003F7 RID: 1015 RVA: 0x00016218 File Offset: 0x00014418
	public void SetSlot(global::BaseEntity.Slot slot, global::BaseEntity ent)
	{
		this.entitySlots[(int)slot].Set(ent);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060003F8 RID: 1016 RVA: 0x00016234 File Offset: 0x00014434
	public virtual bool HasSlot(global::BaseEntity.Slot slot)
	{
		return false;
	}

	// Token: 0x17000010 RID: 16
	// (get) Token: 0x060003F9 RID: 1017 RVA: 0x00016238 File Offset: 0x00014438
	public virtual global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.None;
		}
	}

	// Token: 0x060003FA RID: 1018 RVA: 0x0001623C File Offset: 0x0001443C
	public bool HasTrait(global::BaseEntity.TraitFlag f)
	{
		return (this.Traits & f) == f;
	}

	// Token: 0x060003FB RID: 1019 RVA: 0x0001624C File Offset: 0x0001444C
	public bool HasAnyTrait(global::BaseEntity.TraitFlag f)
	{
		return (this.Traits & f) != global::BaseEntity.TraitFlag.None;
	}

	// Token: 0x060003FC RID: 1020 RVA: 0x0001625C File Offset: 0x0001445C
	public virtual bool EnterTrigger(global::TriggerBase trigger)
	{
		if (this.triggers == null)
		{
			this.triggers = Facepunch.Pool.Get<List<global::TriggerBase>>();
		}
		this.triggers.Add(trigger);
		return true;
	}

	// Token: 0x060003FD RID: 1021 RVA: 0x00016284 File Offset: 0x00014484
	public virtual void LeaveTrigger(global::TriggerBase trigger)
	{
		if (this.triggers == null)
		{
			return;
		}
		this.triggers.Remove(trigger);
		if (this.triggers.Count == 0)
		{
			Facepunch.Pool.FreeList<global::TriggerBase>(ref this.triggers);
		}
	}

	// Token: 0x060003FE RID: 1022 RVA: 0x000162BC File Offset: 0x000144BC
	public void RemoveFromTriggers()
	{
		if (this.triggers == null)
		{
			return;
		}
		using (TimeWarning.New("RemoveFromTriggers", 0.1f))
		{
			foreach (global::TriggerBase triggerBase in this.triggers.ToArray())
			{
				if (triggerBase)
				{
					triggerBase.RemoveEntity(this);
				}
			}
			if (this.triggers != null && this.triggers.Count == 0)
			{
				Facepunch.Pool.FreeList<global::TriggerBase>(ref this.triggers);
			}
		}
	}

	// Token: 0x060003FF RID: 1023 RVA: 0x00016364 File Offset: 0x00014564
	public T FindTrigger<T>() where T : global::TriggerBase
	{
		if (this.triggers == null)
		{
			return (T)((object)null);
		}
		foreach (global::TriggerBase triggerBase in this.triggers)
		{
			if (!(triggerBase as T == null))
			{
				return triggerBase as T;
			}
		}
		return (T)((object)null);
	}

	// Token: 0x0400007E RID: 126
	[Header("BaseEntity")]
	public Bounds bounds;

	// Token: 0x0400007F RID: 127
	public global::GameObjectRef impactEffect;

	// Token: 0x04000080 RID: 128
	public bool enableSaving = true;

	// Token: 0x04000081 RID: 129
	public bool syncPosition;

	// Token: 0x04000082 RID: 130
	public global::Model model;

	// Token: 0x04000083 RID: 131
	[InspectorFlags]
	public global::BaseEntity.Flags flags;

	// Token: 0x04000084 RID: 132
	[NonSerialized]
	public uint parentBone;

	// Token: 0x04000085 RID: 133
	[NonSerialized]
	public ulong skinID;

	// Token: 0x04000086 RID: 134
	private global::EntityComponentBase[] _components;

	// Token: 0x04000087 RID: 135
	[NonSerialized]
	public string _name;

	// Token: 0x04000089 RID: 137
	private static Queue<global::BaseEntity> globalBroadcastQueue = new Queue<global::BaseEntity>();

	// Token: 0x0400008A RID: 138
	private static uint globalBroadcastProtocol = 0u;

	// Token: 0x0400008B RID: 139
	private uint broadcastProtocol;

	// Token: 0x0400008C RID: 140
	private List<global::EntityLink> links = new List<global::EntityLink>();

	// Token: 0x0400008D RID: 141
	private bool linkedToNeighbours;

	// Token: 0x0400008E RID: 142
	[NonSerialized]
	public global::EntityRef parentEntity;

	// Token: 0x0400008F RID: 143
	private global::Spawnable _spawnable;

	// Token: 0x04000090 RID: 144
	public static HashSet<global::BaseEntity> saveList = new HashSet<global::BaseEntity>();

	// Token: 0x04000091 RID: 145
	[NonSerialized]
	public global::BaseEntity creatorEntity;

	// Token: 0x04000092 RID: 146
	private int doneMovingWithoutARigidBodyCheck = 1;

	// Token: 0x04000093 RID: 147
	private bool isCallingUpdateNetworkGroup;

	// Token: 0x04000094 RID: 148
	private global::EntityRef[] entitySlots = new global::EntityRef[6];

	// Token: 0x04000095 RID: 149
	protected List<global::TriggerBase> triggers;

	// Token: 0x04000096 RID: 150
	protected bool isVisible = true;

	// Token: 0x04000097 RID: 151
	protected bool isAnimatorVisible = true;

	// Token: 0x04000098 RID: 152
	protected bool isShadowVisible = true;

	// Token: 0x04000099 RID: 153
	protected global::OccludeeSphere localOccludee = new global::OccludeeSphere(-1);

	// Token: 0x02000015 RID: 21
	public class Menu : Attribute
	{
		// Token: 0x06000401 RID: 1025 RVA: 0x00016420 File Offset: 0x00014620
		public Menu()
		{
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x00016428 File Offset: 0x00014628
		public Menu(string menuTitleToken, string menuTitleEnglish)
		{
			this.TitleToken = menuTitleToken;
			this.TitleEnglish = menuTitleEnglish;
		}

		// Token: 0x0400009A RID: 154
		public string TitleToken;

		// Token: 0x0400009B RID: 155
		public string TitleEnglish;

		// Token: 0x0400009C RID: 156
		public string UseVariable;

		// Token: 0x0400009D RID: 157
		public int Order;

		// Token: 0x0400009E RID: 158
		public string ProxyFunction;

		// Token: 0x0400009F RID: 159
		public float Time;

		// Token: 0x040000A0 RID: 160
		public string OnStart;

		// Token: 0x040000A1 RID: 161
		public string OnProgress;

		// Token: 0x02000016 RID: 22
		[Serializable]
		public struct Option
		{
			// Token: 0x040000A2 RID: 162
			public global::Translate.Phrase name;

			// Token: 0x040000A3 RID: 163
			public global::Translate.Phrase description;

			// Token: 0x040000A4 RID: 164
			public Sprite icon;

			// Token: 0x040000A5 RID: 165
			public int order;
		}

		// Token: 0x02000017 RID: 23
		public class Description : Attribute
		{
			// Token: 0x06000403 RID: 1027 RVA: 0x00016440 File Offset: 0x00014640
			public Description(string t, string e)
			{
				this.token = t;
				this.english = e;
			}

			// Token: 0x040000A6 RID: 166
			public string token;

			// Token: 0x040000A7 RID: 167
			public string english;
		}

		// Token: 0x02000018 RID: 24
		public class Icon : Attribute
		{
			// Token: 0x06000404 RID: 1028 RVA: 0x00016458 File Offset: 0x00014658
			public Icon(string i)
			{
				this.icon = i;
			}

			// Token: 0x040000A8 RID: 168
			public string icon;
		}

		// Token: 0x02000019 RID: 25
		public class ShowIf : Attribute
		{
			// Token: 0x06000405 RID: 1029 RVA: 0x00016468 File Offset: 0x00014668
			public ShowIf(string testFunc)
			{
				this.functionName = testFunc;
			}

			// Token: 0x040000A9 RID: 169
			public string functionName;
		}
	}

	// Token: 0x0200001A RID: 26
	[Serializable]
	public struct MovementModify
	{
		// Token: 0x040000AA RID: 170
		public float drag;
	}

	// Token: 0x0200001B RID: 27
	public enum GiveItemReason
	{
		// Token: 0x040000AC RID: 172
		Generic,
		// Token: 0x040000AD RID: 173
		ResourceHarvested,
		// Token: 0x040000AE RID: 174
		PickedUp,
		// Token: 0x040000AF RID: 175
		Crafted
	}

	// Token: 0x0200001C RID: 28
	[Flags]
	public enum Flags
	{
		// Token: 0x040000B1 RID: 177
		Placeholder = 1,
		// Token: 0x040000B2 RID: 178
		On = 2,
		// Token: 0x040000B3 RID: 179
		OnFire = 4,
		// Token: 0x040000B4 RID: 180
		Open = 8,
		// Token: 0x040000B5 RID: 181
		Locked = 16,
		// Token: 0x040000B6 RID: 182
		Debugging = 32,
		// Token: 0x040000B7 RID: 183
		Disabled = 64,
		// Token: 0x040000B8 RID: 184
		Reserved1 = 128,
		// Token: 0x040000B9 RID: 185
		Reserved2 = 256,
		// Token: 0x040000BA RID: 186
		Reserved3 = 512,
		// Token: 0x040000BB RID: 187
		Reserved4 = 1024,
		// Token: 0x040000BC RID: 188
		Reserved5 = 2048,
		// Token: 0x040000BD RID: 189
		Broken = 4096,
		// Token: 0x040000BE RID: 190
		Busy = 8192,
		// Token: 0x040000BF RID: 191
		Reserved6 = 16384,
		// Token: 0x040000C0 RID: 192
		Reserved7 = 32768,
		// Token: 0x040000C1 RID: 193
		Reserved8 = 65536
	}

	// Token: 0x0200001D RID: 29
	public static class Query
	{
		// Token: 0x040000C2 RID: 194
		public static global::BaseEntity.Query.EntityTree Server;

		// Token: 0x0200001E RID: 30
		public class EntityTree
		{
			// Token: 0x06000406 RID: 1030 RVA: 0x00016478 File Offset: 0x00014678
			public EntityTree(float worldSize)
			{
				this.Grid = new Grid<global::BaseEntity>(32, worldSize);
				this.PlayerGrid = new Grid<global::BasePlayer>(32, worldSize);
			}

			// Token: 0x06000407 RID: 1031 RVA: 0x0001649C File Offset: 0x0001469C
			public void Add(global::BaseEntity ent)
			{
				Vector3 localPosition = ent.transform.localPosition;
				this.Grid.Add(ent, localPosition.x, localPosition.z);
			}

			// Token: 0x06000408 RID: 1032 RVA: 0x000164D0 File Offset: 0x000146D0
			public void AddPlayer(global::BasePlayer player)
			{
				Vector3 localPosition = player.transform.localPosition;
				this.PlayerGrid.Add(player, localPosition.x, localPosition.z);
			}

			// Token: 0x06000409 RID: 1033 RVA: 0x00016504 File Offset: 0x00014704
			public void Remove(global::BaseEntity ent, bool isPlayer = false)
			{
				this.Grid.Remove(ent);
				if (isPlayer)
				{
					global::BasePlayer basePlayer = ent as global::BasePlayer;
					if (basePlayer != null)
					{
						this.PlayerGrid.Remove(basePlayer);
					}
				}
			}

			// Token: 0x0600040A RID: 1034 RVA: 0x00016544 File Offset: 0x00014744
			public void RemovePlayer(global::BasePlayer player)
			{
				this.PlayerGrid.Remove(player);
			}

			// Token: 0x0600040B RID: 1035 RVA: 0x00016554 File Offset: 0x00014754
			public void Move(global::BaseEntity ent)
			{
				Vector3 localPosition = ent.transform.localPosition;
				this.Grid.Move(ent, localPosition.x, localPosition.z);
				global::BasePlayer basePlayer = ent as global::BasePlayer;
				if (basePlayer != null)
				{
					this.MovePlayer(basePlayer);
				}
			}

			// Token: 0x0600040C RID: 1036 RVA: 0x000165A4 File Offset: 0x000147A4
			public void MovePlayer(global::BasePlayer player)
			{
				Vector3 localPosition = player.transform.localPosition;
				this.PlayerGrid.Move(player, localPosition.x, localPosition.z);
			}

			// Token: 0x0600040D RID: 1037 RVA: 0x000165D8 File Offset: 0x000147D8
			public int GetInSphere(Vector3 position, float distance, global::BaseEntity[] results, Func<global::BaseEntity, bool> filter = null)
			{
				return this.Grid.Query(position.x, position.z, distance, results, filter);
			}

			// Token: 0x0600040E RID: 1038 RVA: 0x00016604 File Offset: 0x00014804
			public int GetPlayersInSphere(Vector3 position, float distance, global::BasePlayer[] results, Func<global::BasePlayer, bool> filter = null)
			{
				return this.PlayerGrid.Query(position.x, position.z, distance, results, filter);
			}

			// Token: 0x040000C3 RID: 195
			private Grid<global::BaseEntity> Grid;

			// Token: 0x040000C4 RID: 196
			private Grid<global::BasePlayer> PlayerGrid;
		}
	}

	// Token: 0x0200001F RID: 31
	public class RPC_Shared : Attribute
	{
	}

	// Token: 0x02000020 RID: 32
	public struct RPCMessage
	{
		// Token: 0x040000C5 RID: 197
		public Connection connection;

		// Token: 0x040000C6 RID: 198
		public global::BasePlayer player;

		// Token: 0x040000C7 RID: 199
		public Read read;
	}

	// Token: 0x02000021 RID: 33
	public class RPC_Server : global::BaseEntity.RPC_Shared
	{
		// Token: 0x02000022 RID: 34
		public abstract class Conditional : Attribute
		{
			// Token: 0x06000412 RID: 1042 RVA: 0x00016648 File Offset: 0x00014848
			public virtual string GetArgs()
			{
				return null;
			}
		}

		// Token: 0x02000023 RID: 35
		public class MaxDistance : global::BaseEntity.RPC_Server.Conditional
		{
			// Token: 0x06000413 RID: 1043 RVA: 0x0001664C File Offset: 0x0001484C
			public MaxDistance(float maxDist)
			{
				this.maximumDistance = maxDist;
			}

			// Token: 0x06000414 RID: 1044 RVA: 0x0001665C File Offset: 0x0001485C
			public override string GetArgs()
			{
				return this.maximumDistance.ToString("0.00f");
			}

			// Token: 0x06000415 RID: 1045 RVA: 0x00016670 File Offset: 0x00014870
			public static bool Test(string debugName, global::BaseEntity ent, global::BasePlayer player, float maximumDistance)
			{
				return !(ent == null) && !(player == null) && ent.Distance(player.eyes.position) <= maximumDistance;
			}

			// Token: 0x040000C8 RID: 200
			private float maximumDistance;
		}

		// Token: 0x02000024 RID: 36
		public class IsVisible : global::BaseEntity.RPC_Server.Conditional
		{
			// Token: 0x06000416 RID: 1046 RVA: 0x000166B0 File Offset: 0x000148B0
			public IsVisible(float maxDist)
			{
				this.maximumDistance = maxDist;
			}

			// Token: 0x06000417 RID: 1047 RVA: 0x000166C0 File Offset: 0x000148C0
			public override string GetArgs()
			{
				return this.maximumDistance.ToString("0.00f");
			}

			// Token: 0x06000418 RID: 1048 RVA: 0x000166D4 File Offset: 0x000148D4
			public static bool Test(string debugName, global::BaseEntity ent, global::BasePlayer player, float maximumDistance)
			{
				return !(ent == null) && !(player == null) && (ent.IsVisible(player.eyes.HeadRay(), maximumDistance) || ent.IsVisible(player.eyes.position));
			}

			// Token: 0x040000C9 RID: 201
			private float maximumDistance;
		}

		// Token: 0x02000025 RID: 37
		public class FromOwner : global::BaseEntity.RPC_Server.Conditional
		{
			// Token: 0x0600041A RID: 1050 RVA: 0x00016730 File Offset: 0x00014930
			public static bool Test(string debugName, global::BaseEntity ent, global::BasePlayer player)
			{
				return !(ent == null) && !(player == null) && ent.net != null && player.net != null && (ent.net.ID == player.net.ID || ent.parentEntity.uid == player.net.ID);
			}
		}

		// Token: 0x02000026 RID: 38
		public class IsActiveItem : global::BaseEntity.RPC_Server.Conditional
		{
			// Token: 0x0600041C RID: 1052 RVA: 0x000167B4 File Offset: 0x000149B4
			public static bool Test(string debugName, global::BaseEntity ent, global::BasePlayer player)
			{
				if (ent == null || player == null)
				{
					return false;
				}
				if (ent.net == null || player.net == null)
				{
					return false;
				}
				if (ent.net.ID == player.net.ID)
				{
					return true;
				}
				if (ent.parentEntity.uid != player.net.ID)
				{
					return false;
				}
				global::Item activeItem = player.GetActiveItem();
				return activeItem != null && !(activeItem.GetHeldEntity() != ent);
			}
		}
	}

	// Token: 0x02000027 RID: 39
	public enum Signal
	{
		// Token: 0x040000CB RID: 203
		Attack,
		// Token: 0x040000CC RID: 204
		Alt_Attack,
		// Token: 0x040000CD RID: 205
		DryFire,
		// Token: 0x040000CE RID: 206
		Reload,
		// Token: 0x040000CF RID: 207
		Deploy,
		// Token: 0x040000D0 RID: 208
		Flinch_Head,
		// Token: 0x040000D1 RID: 209
		Flinch_Chest,
		// Token: 0x040000D2 RID: 210
		Flinch_Stomach,
		// Token: 0x040000D3 RID: 211
		Flinch_RearHead,
		// Token: 0x040000D4 RID: 212
		Flinch_RearTorso,
		// Token: 0x040000D5 RID: 213
		Throw,
		// Token: 0x040000D6 RID: 214
		Relax,
		// Token: 0x040000D7 RID: 215
		Gesture,
		// Token: 0x040000D8 RID: 216
		PhysImpact,
		// Token: 0x040000D9 RID: 217
		Eat,
		// Token: 0x040000DA RID: 218
		Startled
	}

	// Token: 0x02000028 RID: 40
	public enum Slot
	{
		// Token: 0x040000DC RID: 220
		Lock,
		// Token: 0x040000DD RID: 221
		FireMod,
		// Token: 0x040000DE RID: 222
		UpperModifier,
		// Token: 0x040000DF RID: 223
		MiddleModifier,
		// Token: 0x040000E0 RID: 224
		LowerModifier,
		// Token: 0x040000E1 RID: 225
		CenterDecoration,
		// Token: 0x040000E2 RID: 226
		Count
	}

	// Token: 0x02000029 RID: 41
	[Flags]
	public enum TraitFlag
	{
		// Token: 0x040000E4 RID: 228
		None = 0,
		// Token: 0x040000E5 RID: 229
		Alive = 1,
		// Token: 0x040000E6 RID: 230
		Animal = 2,
		// Token: 0x040000E7 RID: 231
		Human = 4,
		// Token: 0x040000E8 RID: 232
		Interesting = 8,
		// Token: 0x040000E9 RID: 233
		Food = 16,
		// Token: 0x040000EA RID: 234
		Meat = 32,
		// Token: 0x040000EB RID: 235
		Water = 32
	}

	// Token: 0x0200002A RID: 42
	public static class Util
	{
		// Token: 0x0600041D RID: 1053 RVA: 0x00016850 File Offset: 0x00014A50
		public static global::BaseEntity[] FindTargets(string strFilter, bool onlyPlayers)
		{
			return (from x in global::BaseNetworkable.serverEntities.Where(delegate(global::BaseNetworkable x)
			{
				if (x is global::BasePlayer)
				{
					global::BasePlayer basePlayer = x as global::BasePlayer;
					return string.IsNullOrEmpty(strFilter) || (strFilter == "!alive" && basePlayer.IsAlive()) || (strFilter == "!sleeping" && basePlayer.IsSleeping()) || strFilter[0] == '!' || StringEx.Contains(basePlayer.displayName, strFilter, CompareOptions.IgnoreCase) || basePlayer.UserIDString.Contains(strFilter);
				}
				return !onlyPlayers && !string.IsNullOrEmpty(strFilter) && x.ShortPrefabName.Contains(strFilter);
			})
			select x as global::BaseEntity).ToArray<global::BaseEntity>();
		}
	}
}
