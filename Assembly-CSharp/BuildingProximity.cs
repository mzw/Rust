﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000208 RID: 520
public class BuildingProximity : global::PrefabAttribute
{
	// Token: 0x06000F71 RID: 3953 RVA: 0x0005E118 File Offset: 0x0005C318
	protected bool Check(global::BasePlayer player, global::Construction construction, Vector3 position, Quaternion rotation)
	{
		OBB obb;
		obb..ctor(position, rotation, this.bounds);
		float radius = Vector3Ex.Max(this.bounds.size) + 5f;
		List<global::BuildingBlock> list = Pool.GetList<global::BuildingBlock>();
		global::Vis.Entities<global::BuildingBlock>(obb.position, radius, list, 2097152, 2);
		uint num = 0u;
		for (int i = 0; i < list.Count; i++)
		{
			global::BuildingBlock buildingBlock = list[i];
			global::Construction blockDefinition = buildingBlock.blockDefinition;
			Vector3 position2 = buildingBlock.transform.position;
			Quaternion rotation2 = buildingBlock.transform.rotation;
			global::BuildingProximity.ProximityInfo proximity = this.GetProximity(construction, position, rotation, blockDefinition, position2, rotation2);
			if (proximity.connection)
			{
				global::BuildingManager.Building building = buildingBlock.GetBuilding();
				if (building != null)
				{
					global::BuildingPrivlidge dominatingBuildingPrivilege = building.GetDominatingBuildingPrivilege();
					if (dominatingBuildingPrivilege != null)
					{
						if (!construction.canBypassBuildingPermission && !dominatingBuildingPrivilege.IsAuthed(player))
						{
							global::Construction.lastPlacementError = "Cannot attach to unauthorized building";
							Pool.FreeList<global::BuildingBlock>(ref list);
							return true;
						}
						if (num == 0u)
						{
							num = building.ID;
						}
						else if (num != building.ID)
						{
							global::Construction.lastPlacementError = "Cannot connect two buildings with cupboards";
							Pool.FreeList<global::BuildingBlock>(ref list);
							return true;
						}
					}
				}
			}
			if (proximity.hit)
			{
				Vector3 vector = proximity.line.point1 - proximity.line.point0;
				if (Mathf.Abs(vector.y) <= 2.99f)
				{
					if (Vector3Ex.Magnitude2D(vector) <= 1.49f)
					{
						global::Construction.lastPlacementError = "Not enough space";
						Pool.FreeList<global::BuildingBlock>(ref list);
						return true;
					}
				}
			}
		}
		Pool.FreeList<global::BuildingBlock>(ref list);
		return false;
	}

	// Token: 0x06000F72 RID: 3954 RVA: 0x0005E2D8 File Offset: 0x0005C4D8
	private global::BuildingProximity.ProximityInfo GetProximity(global::Construction construction1, Vector3 position1, Quaternion rotation1, global::Construction construction2, Vector3 position2, Quaternion rotation2)
	{
		List<global::ConstructionSocket> list = Pool.GetList<global::ConstructionSocket>();
		List<global::ConstructionSocket> list2 = Pool.GetList<global::ConstructionSocket>();
		global::BuildingProximity.ProximityInfo result = default(global::BuildingProximity.ProximityInfo);
		result.hit = false;
		result.connection = false;
		result.line = default(Line);
		result.sqrDist = float.MaxValue;
		for (int i = 0; i < construction1.allSockets.Length; i++)
		{
			global::ConstructionSocket constructionSocket = construction1.allSockets[i] as global::ConstructionSocket;
			if (!(constructionSocket == null))
			{
				bool flag = false;
				for (int j = 0; j < construction2.allSockets.Length; j++)
				{
					global::Socket_Base socket = construction2.allSockets[j];
					if (constructionSocket.CanConnect(position1, rotation1, socket, position2, rotation2))
					{
						flag = (result.connection = true);
						break;
					}
				}
				if (!flag)
				{
					list.Add(constructionSocket);
				}
			}
		}
		for (int k = 0; k < construction2.allSockets.Length; k++)
		{
			global::ConstructionSocket constructionSocket2 = construction2.allSockets[k] as global::ConstructionSocket;
			if (!(constructionSocket2 == null))
			{
				bool flag2 = false;
				for (int l = 0; l < construction1.allSockets.Length; l++)
				{
					global::Socket_Base socket2 = construction1.allSockets[l];
					if (constructionSocket2.CanConnect(position2, rotation2, socket2, position1, rotation1))
					{
						flag2 = (result.connection = true);
						break;
					}
				}
				if (!flag2)
				{
					list2.Add(constructionSocket2);
				}
			}
		}
		for (int m = 0; m < list.Count; m++)
		{
			global::ConstructionSocket constructionSocket3 = list[m];
			if (constructionSocket3.socketType == global::ConstructionSocket.Type.Foundation)
			{
				Vector3 selectPivot = constructionSocket3.GetSelectPivot(position1, rotation1);
				Vector3 vector = rotation1 * constructionSocket3.worldRotation * Vector3.right * 1.5f;
				for (int n = 0; n < list2.Count; n++)
				{
					global::ConstructionSocket constructionSocket4 = list2[n];
					if (constructionSocket4.socketType == global::ConstructionSocket.Type.Foundation)
					{
						Vector3 selectPivot2 = constructionSocket4.GetSelectPivot(position2, rotation2);
						Vector3 vector2 = rotation2 * constructionSocket4.worldRotation * Vector3.right * 1.5f;
						if (constructionSocket3.IsCompatible(constructionSocket4))
						{
							Line line;
							line..ctor(selectPivot, selectPivot2);
							Line line2;
							line2..ctor(selectPivot, selectPivot2 + vector2);
							Line line3;
							line3..ctor(selectPivot, selectPivot2 - vector2);
							Line line4;
							line4..ctor(selectPivot + vector, selectPivot2);
							Line line5;
							line5..ctor(selectPivot - vector, selectPivot2);
							float sqrMagnitude = (line.point1 - line.point0).sqrMagnitude;
							float sqrMagnitude2 = (line2.point1 - line2.point0).sqrMagnitude;
							float sqrMagnitude3 = (line3.point1 - line3.point0).sqrMagnitude;
							float sqrMagnitude4 = (line4.point1 - line4.point0).sqrMagnitude;
							float sqrMagnitude5 = (line5.point1 - line5.point0).sqrMagnitude;
							if (sqrMagnitude < result.sqrDist)
							{
								result.hit = true;
								result.line = line;
								result.sqrDist = sqrMagnitude;
							}
							if (sqrMagnitude2 < result.sqrDist)
							{
								result.hit = true;
								result.line = line2;
								result.sqrDist = sqrMagnitude2;
							}
							if (sqrMagnitude3 < result.sqrDist)
							{
								result.hit = true;
								result.line = line3;
								result.sqrDist = sqrMagnitude3;
							}
							if (sqrMagnitude4 < result.sqrDist)
							{
								result.hit = true;
								result.line = line4;
								result.sqrDist = sqrMagnitude4;
							}
							if (sqrMagnitude5 < result.sqrDist)
							{
								result.hit = true;
								result.line = line5;
								result.sqrDist = sqrMagnitude5;
							}
						}
					}
				}
			}
		}
		Pool.FreeList<global::ConstructionSocket>(ref list);
		Pool.FreeList<global::ConstructionSocket>(ref list2);
		return result;
	}

	// Token: 0x06000F73 RID: 3955 RVA: 0x0005E700 File Offset: 0x0005C900
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		this.bounds = rootObj.GetComponent<global::BaseEntity>().bounds;
	}

	// Token: 0x06000F74 RID: 3956 RVA: 0x0005E714 File Offset: 0x0005C914
	protected override Type GetIndexedType()
	{
		return typeof(global::BuildingProximity);
	}

	// Token: 0x06000F75 RID: 3957 RVA: 0x0005E720 File Offset: 0x0005C920
	public static bool Check(global::BasePlayer player, global::Construction construction, Vector3 position, Quaternion rotation, global::BuildingProximity[] volumes)
	{
		for (int i = 0; i < volumes.Length; i++)
		{
			if (volumes[i].Check(player, construction, position, rotation))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x04000A21 RID: 2593
	private const float distance = 5f;

	// Token: 0x04000A22 RID: 2594
	private const float forgiveness = 0.01f;

	// Token: 0x04000A23 RID: 2595
	private const float foundation_width = 3f;

	// Token: 0x04000A24 RID: 2596
	private const float foundation_extents = 1.5f;

	// Token: 0x04000A25 RID: 2597
	private Bounds bounds = new Bounds(Vector3.zero, Vector3.one);

	// Token: 0x02000209 RID: 521
	private struct ProximityInfo
	{
		// Token: 0x04000A26 RID: 2598
		public bool hit;

		// Token: 0x04000A27 RID: 2599
		public bool connection;

		// Token: 0x04000A28 RID: 2600
		public Line line;

		// Token: 0x04000A29 RID: 2601
		public float sqrDist;
	}
}
