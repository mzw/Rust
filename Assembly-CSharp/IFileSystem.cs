﻿using System;
using UnityEngine;

// Token: 0x02000338 RID: 824
public interface IFileSystem
{
	// Token: 0x060013D3 RID: 5075
	T Load<T>(string filePath, bool bComplain = true) where T : Object;

	// Token: 0x060013D4 RID: 5076
	string[] FindAll(string folder, string search);

	// Token: 0x060013D5 RID: 5077
	AsyncOperation LoadAsync(string filePath);
}
