﻿using System;
using UnityEngine;

// Token: 0x02000699 RID: 1689
public class UIFadeOut : MonoBehaviour
{
	// Token: 0x06002126 RID: 8486 RVA: 0x000BBCA4 File Offset: 0x000B9EA4
	private void Start()
	{
		this.timeStarted = Time.realtimeSinceStartup;
	}

	// Token: 0x06002127 RID: 8487 RVA: 0x000BBCB4 File Offset: 0x000B9EB4
	private void Update()
	{
		this.targetGroup.alpha = Mathf.InverseLerp(this.timeStarted + this.secondsToFadeOut, this.timeStarted, Time.realtimeSinceStartup);
		if (this.destroyOnFaded && Time.realtimeSinceStartup > this.timeStarted + this.secondsToFadeOut)
		{
			global::GameManager.Destroy(base.gameObject, 0f);
		}
	}

	// Token: 0x04001C8D RID: 7309
	public float secondsToFadeOut = 3f;

	// Token: 0x04001C8E RID: 7310
	public bool destroyOnFaded = true;

	// Token: 0x04001C8F RID: 7311
	public CanvasGroup targetGroup;

	// Token: 0x04001C90 RID: 7312
	private float timeStarted;
}
