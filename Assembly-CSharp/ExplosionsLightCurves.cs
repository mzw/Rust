﻿using System;
using UnityEngine;

// Token: 0x020007F8 RID: 2040
public class ExplosionsLightCurves : MonoBehaviour
{
	// Token: 0x060025B7 RID: 9655 RVA: 0x000D08CC File Offset: 0x000CEACC
	private void Awake()
	{
		this.lightSource = base.GetComponent<Light>();
		this.lightSource.intensity = this.LightCurve.Evaluate(0f);
	}

	// Token: 0x060025B8 RID: 9656 RVA: 0x000D08F8 File Offset: 0x000CEAF8
	private void OnEnable()
	{
		this.startTime = Time.time;
		this.canUpdate = true;
	}

	// Token: 0x060025B9 RID: 9657 RVA: 0x000D090C File Offset: 0x000CEB0C
	private void Update()
	{
		float num = Time.time - this.startTime;
		if (this.canUpdate)
		{
			float intensity = this.LightCurve.Evaluate(num / this.GraphTimeMultiplier) * this.GraphIntensityMultiplier;
			this.lightSource.intensity = intensity;
		}
		if (num >= this.GraphTimeMultiplier)
		{
			this.canUpdate = false;
		}
	}

	// Token: 0x040021D5 RID: 8661
	public AnimationCurve LightCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

	// Token: 0x040021D6 RID: 8662
	public float GraphTimeMultiplier = 1f;

	// Token: 0x040021D7 RID: 8663
	public float GraphIntensityMultiplier = 1f;

	// Token: 0x040021D8 RID: 8664
	private bool canUpdate;

	// Token: 0x040021D9 RID: 8665
	private float startTime;

	// Token: 0x040021DA RID: 8666
	private Light lightSource;
}
