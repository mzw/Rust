﻿using System;
using UnityStandardAssets.ImageEffects;

// Token: 0x0200023D RID: 573
public class HotOverlay : ImageEffectLayer
{
	// Token: 0x04000AE3 RID: 2787
	public LensDirtiness lensDirtyness;

	// Token: 0x04000AE4 RID: 2788
	public VignetteAndChromaticAberration vingette;
}
