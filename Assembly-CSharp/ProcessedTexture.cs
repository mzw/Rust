﻿using System;
using UnityEngine;

// Token: 0x0200077C RID: 1916
public class ProcessedTexture
{
	// Token: 0x06002399 RID: 9113 RVA: 0x000C535C File Offset: 0x000C355C
	public void Dispose()
	{
		this.DestroyRenderTexture(ref this.result);
		this.DestroyMaterial(ref this.material);
	}

	// Token: 0x0600239A RID: 9114 RVA: 0x000C5378 File Offset: 0x000C3578
	protected RenderTexture CreateRenderTexture(string name, int width, int height, bool linear)
	{
		RenderTexture renderTexture = new RenderTexture(width, height, 0, 0, (!linear) ? 2 : 1);
		renderTexture.hideFlags = 52;
		renderTexture.name = name;
		renderTexture.filterMode = 1;
		renderTexture.anisoLevel = 0;
		renderTexture.Create();
		return renderTexture;
	}

	// Token: 0x0600239B RID: 9115 RVA: 0x000C53C4 File Offset: 0x000C35C4
	protected void DestroyRenderTexture(ref RenderTexture rt)
	{
		if (rt == null)
		{
			return;
		}
		Object.Destroy(rt);
		rt = null;
	}

	// Token: 0x0600239C RID: 9116 RVA: 0x000C53E0 File Offset: 0x000C35E0
	protected RenderTexture CreateTemporary()
	{
		return RenderTexture.GetTemporary(this.result.width, this.result.height, this.result.depth, this.result.format, (!this.result.sRGB) ? 1 : 2);
	}

	// Token: 0x0600239D RID: 9117 RVA: 0x000C5438 File Offset: 0x000C3638
	protected void ReleaseTemporary(RenderTexture rt)
	{
		RenderTexture.ReleaseTemporary(rt);
	}

	// Token: 0x0600239E RID: 9118 RVA: 0x000C5440 File Offset: 0x000C3640
	protected Material CreateMaterial(string shader)
	{
		return this.CreateMaterial(Shader.Find(shader));
	}

	// Token: 0x0600239F RID: 9119 RVA: 0x000C5450 File Offset: 0x000C3650
	protected Material CreateMaterial(Shader shader)
	{
		return new Material(shader)
		{
			hideFlags = 52
		};
	}

	// Token: 0x060023A0 RID: 9120 RVA: 0x000C5470 File Offset: 0x000C3670
	protected void DestroyMaterial(ref Material mat)
	{
		if (mat == null)
		{
			return;
		}
		Object.Destroy(mat);
		mat = null;
	}

	// Token: 0x060023A1 RID: 9121 RVA: 0x000C548C File Offset: 0x000C368C
	public static implicit operator Texture(global::ProcessedTexture t)
	{
		return t.result;
	}

	// Token: 0x04001F9B RID: 8091
	protected RenderTexture result;

	// Token: 0x04001F9C RID: 8092
	protected Material material;
}
