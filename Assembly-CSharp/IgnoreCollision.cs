﻿using System;
using UnityEngine;

// Token: 0x02000744 RID: 1860
public class IgnoreCollision : MonoBehaviour
{
	// Token: 0x060022E7 RID: 8935 RVA: 0x000C210C File Offset: 0x000C030C
	protected void OnTriggerEnter(Collider other)
	{
		Debug.Log("IgnoreCollision: " + this.collider.gameObject.name + " + " + other.gameObject.name);
		Physics.IgnoreCollision(other, this.collider, true);
	}

	// Token: 0x04001F52 RID: 8018
	public Collider collider;
}
