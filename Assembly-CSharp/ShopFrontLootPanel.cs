﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020000AC RID: 172
public class ShopFrontLootPanel : global::LootPanel
{
	// Token: 0x040004EC RID: 1260
	public Text playerLabelA;

	// Token: 0x040004ED RID: 1261
	public Text playerLabelB;

	// Token: 0x040004EE RID: 1262
	public GameObject confirmButton;

	// Token: 0x040004EF RID: 1263
	public GameObject confirmHelp;

	// Token: 0x040004F0 RID: 1264
	public GameObject denyButton;

	// Token: 0x040004F1 RID: 1265
	public GameObject denyHelp;

	// Token: 0x040004F2 RID: 1266
	public GameObject waitingText;

	// Token: 0x040004F3 RID: 1267
	public GameObject exchangeInProgressImage;

	// Token: 0x040004F4 RID: 1268
	public global::Translate.Phrase acceptedPhrase;

	// Token: 0x040004F5 RID: 1269
	public global::Translate.Phrase noOnePhrase;
}
