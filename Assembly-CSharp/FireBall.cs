﻿using System;
using System.Collections.Generic;
using Facepunch;
using Rust;
using UnityEngine;

// Token: 0x02000387 RID: 903
public class FireBall : global::BaseEntity, global::ISplashable
{
	// Token: 0x06001572 RID: 5490 RVA: 0x0007B654 File Offset: 0x00079854
	public override void ServerInit()
	{
		base.ServerInit();
		base.InvokeRepeating(new Action(this.Think), Random.Range(0f, 1f), this.tickRate);
		float num = Random.Range(this.lifeTimeMin, this.lifeTimeMax);
		float num2 = num * Random.Range(0.9f, 1.1f);
		base.Invoke(new Action(this.Extinguish), num2);
		base.Invoke(new Action(this.TryToSpread), num * Random.Range(0.3f, 0.5f));
		this.deathTime = Time.realtimeSinceStartup + num2;
	}

	// Token: 0x06001573 RID: 5491 RVA: 0x0007B6F8 File Offset: 0x000798F8
	public float GetDeathTime()
	{
		return this.deathTime;
	}

	// Token: 0x06001574 RID: 5492 RVA: 0x0007B700 File Offset: 0x00079900
	public void AddLife(float amountToAdd)
	{
		float num = Mathf.Clamp(this.GetDeathTime() + amountToAdd, 0f, this.MaxLifeTime());
		base.Invoke(new Action(this.Extinguish), num);
		this.deathTime = num;
	}

	// Token: 0x06001575 RID: 5493 RVA: 0x0007B740 File Offset: 0x00079940
	public float MaxLifeTime()
	{
		return this.lifeTimeMax * 2.5f;
	}

	// Token: 0x06001576 RID: 5494 RVA: 0x0007B750 File Offset: 0x00079950
	public float TimeLeft()
	{
		float num = this.deathTime - Time.realtimeSinceStartup;
		if (num < 0f)
		{
			num = 0f;
		}
		return num;
	}

	// Token: 0x06001577 RID: 5495 RVA: 0x0007B77C File Offset: 0x0007997C
	public void TryToSpread()
	{
		float num = 0.9f - this.generation * 0.1f;
		if (Random.Range(0f, 1f) >= num)
		{
			return;
		}
		if (this.spreadSubEntity.isValid)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.spreadSubEntity.resourcePath, default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.transform.position = base.transform.position + Vector3.up * 0.25f;
				baseEntity.Spawn();
				float aimCone = 45f;
				Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(aimCone, Vector3.up, true);
				baseEntity.creatorEntity = ((!(this.creatorEntity == null)) ? this.creatorEntity : baseEntity);
				baseEntity.SetVelocity(modifiedAimConeDirection * Random.Range(5f, 8f));
				baseEntity.SendMessage("SetGeneration", this.generation + 1f);
			}
		}
	}

	// Token: 0x06001578 RID: 5496 RVA: 0x0007B89C File Offset: 0x00079A9C
	public void SetGeneration(int gen)
	{
		this.generation = (float)gen;
	}

	// Token: 0x06001579 RID: 5497 RVA: 0x0007B8A8 File Offset: 0x00079AA8
	public void Think()
	{
		if (!base.isServer)
		{
			return;
		}
		this.SetResting(Vector3.Distance(this.lastPos, base.transform.position) < 0.25f);
		this.lastPos = base.transform.position;
		if (this.IsResting())
		{
			this.DoRadialDamage();
		}
		if (base.WaterFactor() > 0.5f)
		{
			this.Extinguish();
		}
		if (this.wetness > this.waterToExtinguish)
		{
			this.Extinguish();
		}
	}

	// Token: 0x0600157A RID: 5498 RVA: 0x0007B934 File Offset: 0x00079B34
	public void DoRadialDamage()
	{
		List<Collider> list = Pool.GetList<Collider>();
		Vector3 position = base.transform.position + new Vector3(0f, this.radius * 0.75f, 0f);
		global::Vis.Colliders<Collider>(position, this.radius, list, this.AttackLayers, 2);
		global::HitInfo hitInfo = new global::HitInfo();
		hitInfo.DoHitEffects = true;
		hitInfo.DidHit = true;
		hitInfo.HitBone = 0u;
		hitInfo.Initiator = ((!(this.creatorEntity == null)) ? this.creatorEntity : base.gameObject.ToBaseEntity());
		hitInfo.PointStart = base.transform.position;
		foreach (Collider collider in list)
		{
			if (collider.isTrigger)
			{
				if (collider.gameObject.layer == 29)
				{
					continue;
				}
				if (collider.gameObject.layer == 18)
				{
					continue;
				}
			}
			global::BaseCombatEntity baseCombatEntity = collider.gameObject.ToBaseEntity() as global::BaseCombatEntity;
			if (!(baseCombatEntity == null))
			{
				if (baseCombatEntity.isServer)
				{
					if (baseCombatEntity.IsAlive())
					{
						if (baseCombatEntity.IsVisible(position))
						{
							if (baseCombatEntity is global::BasePlayer)
							{
								global::Effect.server.Run("assets/bundled/prefabs/fx/impacts/additive/fire.prefab", baseCombatEntity, 0u, new Vector3(0f, 1f, 0f), Vector3.up, null, false);
							}
							hitInfo.PointEnd = baseCombatEntity.transform.position;
							hitInfo.HitPositionWorld = baseCombatEntity.transform.position;
							hitInfo.damageTypes.Set(Rust.DamageType.Heat, this.damagePerSecond * this.tickRate);
							baseCombatEntity.OnAttacked(hitInfo);
						}
					}
				}
			}
		}
		Pool.FreeList<Collider>(ref list);
	}

	// Token: 0x0600157B RID: 5499 RVA: 0x0007BB48 File Offset: 0x00079D48
	public bool CanMerge()
	{
		return this.canMerge && this.TimeLeft() < this.MaxLifeTime() * 0.8f;
	}

	// Token: 0x0600157C RID: 5500 RVA: 0x0007BB6C File Offset: 0x00079D6C
	public void SetResting(bool isResting)
	{
		if (isResting != this.IsResting() && isResting && this.CanMerge())
		{
			List<Collider> list = Pool.GetList<Collider>();
			global::Vis.Colliders<Collider>(base.transform.position, 0.5f, list, 512, 2);
			foreach (Collider collider in list)
			{
				global::BaseEntity baseEntity = collider.gameObject.ToBaseEntity();
				if (baseEntity)
				{
					global::FireBall fireBall = baseEntity.ToServer<global::FireBall>();
					if (fireBall && fireBall.CanMerge() && fireBall != this)
					{
						fireBall.Invoke(new Action(this.Extinguish), 1f);
						fireBall.canMerge = false;
						this.AddLife(fireBall.TimeLeft() * 0.25f);
					}
				}
			}
			Pool.FreeList<Collider>(ref list);
		}
		base.SetFlag(global::BaseEntity.Flags.OnFire, isResting, false);
	}

	// Token: 0x0600157D RID: 5501 RVA: 0x0007BC88 File Offset: 0x00079E88
	public void Extinguish()
	{
		base.CancelInvoke(new Action(this.Extinguish));
		if (!base.IsDestroyed)
		{
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
	}

	// Token: 0x0600157E RID: 5502 RVA: 0x0007BCB0 File Offset: 0x00079EB0
	public bool wantsSplash(global::ItemDefinition splashType, int amount)
	{
		return !base.IsDestroyed;
	}

	// Token: 0x0600157F RID: 5503 RVA: 0x0007BCBC File Offset: 0x00079EBC
	public int DoSplash(global::ItemDefinition splashType, int amount)
	{
		this.wetness += amount;
		return amount;
	}

	// Token: 0x06001580 RID: 5504 RVA: 0x0007BCD0 File Offset: 0x00079ED0
	public bool IsResting()
	{
		return base.HasFlag(global::BaseEntity.Flags.OnFire);
	}

	// Token: 0x06001581 RID: 5505 RVA: 0x0007BCDC File Offset: 0x00079EDC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
	}

	// Token: 0x04000FB8 RID: 4024
	public float lifeTimeMin = 20f;

	// Token: 0x04000FB9 RID: 4025
	public float lifeTimeMax = 40f;

	// Token: 0x04000FBA RID: 4026
	public ParticleSystem[] movementSystems;

	// Token: 0x04000FBB RID: 4027
	public ParticleSystem[] restingSystems;

	// Token: 0x04000FBC RID: 4028
	[NonSerialized]
	public float generation;

	// Token: 0x04000FBD RID: 4029
	public global::GameObjectRef spreadSubEntity;

	// Token: 0x04000FBE RID: 4030
	public float tickRate = 0.5f;

	// Token: 0x04000FBF RID: 4031
	public float damagePerSecond = 2f;

	// Token: 0x04000FC0 RID: 4032
	public float radius = 0.5f;

	// Token: 0x04000FC1 RID: 4033
	public int waterToExtinguish = 200;

	// Token: 0x04000FC2 RID: 4034
	public bool canMerge;

	// Token: 0x04000FC3 RID: 4035
	public LayerMask AttackLayers = 1084435201;

	// Token: 0x04000FC4 RID: 4036
	private Vector3 lastPos = Vector3.zero;

	// Token: 0x04000FC5 RID: 4037
	private float deathTime;

	// Token: 0x04000FC6 RID: 4038
	private int wetness;
}
