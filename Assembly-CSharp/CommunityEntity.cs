﻿using System;
using Network;

// Token: 0x02000062 RID: 98
public class CommunityEntity : global::PointEntity
{
	// Token: 0x0600076E RID: 1902 RVA: 0x0002FC08 File Offset: 0x0002DE08
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("CommunityEntity.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600076F RID: 1903 RVA: 0x0002FC50 File Offset: 0x0002DE50
	public override void InitShared()
	{
		if (base.isServer)
		{
			global::CommunityEntity.ServerInstance = this;
		}
		else
		{
			global::CommunityEntity.ClientInstance = this;
		}
		base.InitShared();
	}

	// Token: 0x06000770 RID: 1904 RVA: 0x0002FC74 File Offset: 0x0002DE74
	public override void DestroyShared()
	{
		base.DestroyShared();
		if (base.isServer)
		{
			global::CommunityEntity.ServerInstance = null;
		}
		else
		{
			global::CommunityEntity.ClientInstance = null;
		}
	}

	// Token: 0x04000368 RID: 872
	public static global::CommunityEntity ServerInstance;

	// Token: 0x04000369 RID: 873
	public static global::CommunityEntity ClientInstance;
}
