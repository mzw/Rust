﻿using System;
using UnityEngine;

// Token: 0x020004D4 RID: 1236
public class ItemMod : MonoBehaviour
{
	// Token: 0x06001A9E RID: 6814 RVA: 0x00095880 File Offset: 0x00093A80
	public virtual void ModInit()
	{
		this.siblingMods = base.GetComponents<global::ItemMod>();
	}

	// Token: 0x06001A9F RID: 6815 RVA: 0x00095890 File Offset: 0x00093A90
	public virtual void OnItemCreated(global::Item item)
	{
	}

	// Token: 0x06001AA0 RID: 6816 RVA: 0x00095894 File Offset: 0x00093A94
	public virtual void OnVirginItem(global::Item item)
	{
	}

	// Token: 0x06001AA1 RID: 6817 RVA: 0x00095898 File Offset: 0x00093A98
	public virtual void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
	}

	// Token: 0x06001AA2 RID: 6818 RVA: 0x0009589C File Offset: 0x00093A9C
	public virtual void DoAction(global::Item item, global::BasePlayer player)
	{
	}

	// Token: 0x06001AA3 RID: 6819 RVA: 0x000958A0 File Offset: 0x00093AA0
	public virtual void OnRemove(global::Item item)
	{
	}

	// Token: 0x06001AA4 RID: 6820 RVA: 0x000958A4 File Offset: 0x00093AA4
	public virtual void OnParentChanged(global::Item item)
	{
	}

	// Token: 0x06001AA5 RID: 6821 RVA: 0x000958A8 File Offset: 0x00093AA8
	public virtual void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
	}

	// Token: 0x06001AA6 RID: 6822 RVA: 0x000958AC File Offset: 0x00093AAC
	public virtual void ReturnedFromCancelledCraft(global::Item item, global::BasePlayer crafter)
	{
	}

	// Token: 0x06001AA7 RID: 6823 RVA: 0x000958B0 File Offset: 0x00093AB0
	public virtual void OnAttacked(global::Item item, global::HitInfo info)
	{
	}

	// Token: 0x06001AA8 RID: 6824 RVA: 0x000958B4 File Offset: 0x00093AB4
	public virtual void OnChanged(global::Item item)
	{
	}

	// Token: 0x06001AA9 RID: 6825 RVA: 0x000958B8 File Offset: 0x00093AB8
	public virtual bool CanDoAction(global::Item item, global::BasePlayer player)
	{
		foreach (global::ItemMod itemMod in this.siblingMods)
		{
			if (!itemMod.Passes(item))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06001AAA RID: 6826 RVA: 0x000958F4 File Offset: 0x00093AF4
	public virtual bool Passes(global::Item item)
	{
		return true;
	}

	// Token: 0x06001AAB RID: 6827 RVA: 0x000958F8 File Offset: 0x00093AF8
	public virtual void OnRemovedFromWorld(global::Item item)
	{
	}

	// Token: 0x06001AAC RID: 6828 RVA: 0x000958FC File Offset: 0x00093AFC
	public virtual void OnMovedToWorld(global::Item item)
	{
	}

	// Token: 0x04001572 RID: 5490
	private global::ItemMod[] siblingMods;
}
