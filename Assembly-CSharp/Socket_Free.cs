﻿using System;
using UnityEngine;

// Token: 0x0200021E RID: 542
public class Socket_Free : global::Socket_Base
{
	// Token: 0x06000FC9 RID: 4041 RVA: 0x000602C8 File Offset: 0x0005E4C8
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.green;
		Gizmos.DrawLine(Vector3.zero, Vector3.forward * 1f);
		global::GizmosUtil.DrawWireCircleZ(Vector3.forward * 0f, 0.2f);
		Gizmos.DrawIcon(base.transform.position, "light_circle_green.png", false);
	}

	// Token: 0x06000FCA RID: 4042 RVA: 0x00060338 File Offset: 0x0005E538
	public override bool TestTarget(global::Construction.Target target)
	{
		return target.onTerrain;
	}

	// Token: 0x06000FCB RID: 4043 RVA: 0x00060344 File Offset: 0x0005E544
	public override global::Construction.Placement DoPlacement(global::Construction.Target target)
	{
		Quaternion quaternion = Quaternion.identity;
		if (this.useTargetNormal)
		{
			Vector3 vector = (target.position - target.ray.origin).normalized;
			float num = Mathf.Abs(Vector3.Dot(vector, target.normal));
			vector = Vector3.Lerp(vector, this.idealPlacementNormal, num);
			quaternion = Quaternion.Euler(target.rotation) * Quaternion.LookRotation(target.normal, vector) * Quaternion.Inverse(this.rotation);
		}
		else
		{
			Vector3 normalized = (target.position - target.ray.origin).normalized;
			normalized.y = 0f;
			quaternion = Quaternion.Euler(target.rotation) * Quaternion.LookRotation(normalized, this.idealPlacementNormal);
		}
		Vector3 vector2 = target.position;
		vector2 -= quaternion * this.position;
		return new global::Construction.Placement
		{
			rotation = quaternion,
			position = vector2
		};
	}

	// Token: 0x04000A8E RID: 2702
	public Vector3 idealPlacementNormal = Vector3.up;

	// Token: 0x04000A8F RID: 2703
	public bool useTargetNormal = true;
}
