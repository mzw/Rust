﻿using System;

// Token: 0x020007A1 RID: 1953
public class RadixSorter
{
	// Token: 0x06002468 RID: 9320 RVA: 0x000C8878 File Offset: 0x000C6A78
	public RadixSorter()
	{
		this.histogram = new uint[768];
		this.offset = new uint[768];
	}

	// Token: 0x06002469 RID: 9321 RVA: 0x000C88A0 File Offset: 0x000C6AA0
	public void SortU8(uint[] values, uint[] remap, uint num)
	{
		for (int i = 0; i < 256; i++)
		{
			this.histogram[i] = 0u;
		}
		for (uint num2 = 0u; num2 < num; num2 += 1u)
		{
			this.histogram[(int)((UIntPtr)(values[(int)((UIntPtr)num2)] & 255u))] += 1u;
		}
		this.offset[0] = 0u;
		for (uint num3 = 0u; num3 < 255u; num3 += 1u)
		{
			this.offset[(int)((UIntPtr)(num3 + 1u))] = this.offset[(int)((UIntPtr)num3)] + this.histogram[(int)((UIntPtr)num3)];
		}
		for (uint num4 = 0u; num4 < num; num4 += 1u)
		{
			remap[(int)((UIntPtr)(this.offset[(int)((UIntPtr)(values[(int)((UIntPtr)num4)] & 255u))]++))] = num4;
		}
	}

	// Token: 0x0600246A RID: 9322 RVA: 0x000C896C File Offset: 0x000C6B6C
	public void SortU24(uint[] values, uint[] remap, uint[] remapTemp, uint num)
	{
		for (int i = 0; i < 768; i++)
		{
			this.histogram[i] = 0u;
		}
		for (uint num2 = 0u; num2 < num; num2 += 1u)
		{
			uint num3 = values[(int)((UIntPtr)num2)];
			this.histogram[(int)((UIntPtr)(num3 & 255u))] += 1u;
			this.histogram[(int)((UIntPtr)(256u + (num3 >> 8 & 255u)))] += 1u;
			this.histogram[(int)((UIntPtr)(512u + (num3 >> 16 & 255u)))] += 1u;
		}
		this.offset[0] = (this.offset[256] = (this.offset[512] = 0u));
		uint num4 = 0u;
		uint num5 = 256u;
		uint num6 = 512u;
		while (num4 < 255u)
		{
			this.offset[(int)((UIntPtr)(num4 + 1u))] = this.offset[(int)((UIntPtr)num4)] + this.histogram[(int)((UIntPtr)num4)];
			this.offset[(int)((UIntPtr)(num5 + 1u))] = this.offset[(int)((UIntPtr)num5)] + this.histogram[(int)((UIntPtr)num5)];
			this.offset[(int)((UIntPtr)(num6 + 1u))] = this.offset[(int)((UIntPtr)num6)] + this.histogram[(int)((UIntPtr)num6)];
			num4 += 1u;
			num5 += 1u;
			num6 += 1u;
		}
		for (uint num7 = 0u; num7 < num; num7 += 1u)
		{
			remapTemp[(int)((UIntPtr)(this.offset[(int)((UIntPtr)(values[(int)((UIntPtr)num7)] & 255u))]++))] = num7;
		}
		for (uint num8 = 0u; num8 < num; num8 += 1u)
		{
			uint num9 = remapTemp[(int)((UIntPtr)num8)];
			remap[(int)((UIntPtr)(this.offset[(int)((UIntPtr)(256u + (values[(int)((UIntPtr)num9)] >> 8 & 255u)))]++))] = num9;
		}
		for (uint num10 = 0u; num10 < num; num10 += 1u)
		{
			uint num9 = remap[(int)((UIntPtr)num10)];
			remapTemp[(int)((UIntPtr)(this.offset[(int)((UIntPtr)(512u + (values[(int)((UIntPtr)num9)] >> 16 & 255u)))]++))] = num9;
		}
		for (uint num11 = 0u; num11 < num; num11 += 1u)
		{
			remap[(int)((UIntPtr)num11)] = remapTemp[(int)((UIntPtr)num11)];
		}
	}

	// Token: 0x04001FDF RID: 8159
	private uint[] histogram;

	// Token: 0x04001FE0 RID: 8160
	private uint[] offset;
}
