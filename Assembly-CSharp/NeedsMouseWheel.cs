﻿using System;

// Token: 0x02000675 RID: 1653
public class NeedsMouseWheel : ListComponent<global::NeedsMouseWheel>
{
	// Token: 0x060020D0 RID: 8400 RVA: 0x000BAAB0 File Offset: 0x000B8CB0
	public static bool AnyActive()
	{
		return ListComponent<global::NeedsMouseWheel>.InstanceList.Count > 0;
	}
}
