﻿using System;
using UnityEngine;

// Token: 0x0200026B RID: 619
[ExecuteInEditMode]
public class LookAt : MonoBehaviour, IClientComponent
{
	// Token: 0x06001087 RID: 4231 RVA: 0x00063F50 File Offset: 0x00062150
	private void Update()
	{
		if (this.target == null)
		{
			return;
		}
		base.transform.LookAt(this.target);
	}

	// Token: 0x04000B64 RID: 2916
	public Transform target;
}
