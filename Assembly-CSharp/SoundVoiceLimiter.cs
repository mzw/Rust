﻿using System;
using UnityEngine;

// Token: 0x02000204 RID: 516
public class SoundVoiceLimiter : MonoBehaviour, IClientComponent
{
	// Token: 0x04000A1B RID: 2587
	public int maxSimultaneousSounds = 5;
}
