﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006D2 RID: 1746
public class LanguageSelection : MonoBehaviour, global::ILanguageChanged
{
	// Token: 0x04001DA0 RID: 7584
	public GameObject languagePopup;

	// Token: 0x04001DA1 RID: 7585
	public GameObject buttonContainer;

	// Token: 0x04001DA2 RID: 7586
	public Image flagImage;
}
