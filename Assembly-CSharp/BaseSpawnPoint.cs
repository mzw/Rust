﻿using System;
using UnityEngine;

// Token: 0x02000489 RID: 1161
public abstract class BaseSpawnPoint : MonoBehaviour, IServerComponent
{
	// Token: 0x0600193F RID: 6463
	public abstract void GetLocation(out Vector3 pos, out Quaternion rot);

	// Token: 0x06001940 RID: 6464
	public abstract void ObjectSpawned(global::SpawnPointInstance instance);

	// Token: 0x06001941 RID: 6465
	public abstract void ObjectRetired(global::SpawnPointInstance instance);

	// Token: 0x06001942 RID: 6466 RVA: 0x0008E630 File Offset: 0x0008C830
	protected void DropToGround(ref Vector3 pos, ref Quaternion rot)
	{
		if (global::TerrainMeta.HeightMap && global::TerrainMeta.Collision && !global::TerrainMeta.Collision.GetIgnore(pos, 0.01f))
		{
			float height = global::TerrainMeta.HeightMap.GetHeight(pos);
			pos.y = Mathf.Max(pos.y, height);
		}
		RaycastHit raycastHit;
		if (global::TransformUtil.GetGroundInfo(pos, out raycastHit, 20f, 1101070337, null))
		{
			pos = raycastHit.point;
			rot = Quaternion.LookRotation(rot * Vector3.forward, raycastHit.normal);
		}
	}
}
