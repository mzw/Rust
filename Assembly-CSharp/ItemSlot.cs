﻿using System;

// Token: 0x020004D1 RID: 1233
[Flags]
public enum ItemSlot
{
	// Token: 0x04001567 RID: 5479
	None = 1,
	// Token: 0x04001568 RID: 5480
	Barrel = 2,
	// Token: 0x04001569 RID: 5481
	Silencer = 4,
	// Token: 0x0400156A RID: 5482
	Scope = 8,
	// Token: 0x0400156B RID: 5483
	UnderBarrel = 16
}
