﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020000B5 RID: 181
public class VendingMachineScreen : MonoBehaviour
{
	// Token: 0x04000521 RID: 1313
	public RawImage largeIcon;

	// Token: 0x04000522 RID: 1314
	public RawImage blueprintIcon;

	// Token: 0x04000523 RID: 1315
	public Text mainText;

	// Token: 0x04000524 RID: 1316
	public Text lowerText;

	// Token: 0x04000525 RID: 1317
	public Text centerText;

	// Token: 0x04000526 RID: 1318
	public RawImage smallIcon;

	// Token: 0x04000527 RID: 1319
	public global::VendingMachine vendingMachine;

	// Token: 0x04000528 RID: 1320
	public Sprite outOfStockSprite;

	// Token: 0x04000529 RID: 1321
	public Renderer fadeoutMesh;

	// Token: 0x0400052A RID: 1322
	public CanvasGroup screenCanvas;

	// Token: 0x0400052B RID: 1323
	public Renderer light1;

	// Token: 0x0400052C RID: 1324
	public Renderer light2;

	// Token: 0x0400052D RID: 1325
	public float nextImageTime;

	// Token: 0x0400052E RID: 1326
	public int currentImageIndex;

	// Token: 0x020000B6 RID: 182
	public enum vmScreenState
	{
		// Token: 0x04000530 RID: 1328
		ItemScroll,
		// Token: 0x04000531 RID: 1329
		Vending,
		// Token: 0x04000532 RID: 1330
		Message,
		// Token: 0x04000533 RID: 1331
		ShopName,
		// Token: 0x04000534 RID: 1332
		OutOfStock
	}
}
