﻿using System;

// Token: 0x020000D1 RID: 209
public class NPCPlayerCorpse : global::PlayerCorpse
{
	// Token: 0x06000AE3 RID: 2787 RVA: 0x00049A40 File Offset: 0x00047C40
	public override float GetRemovalTime()
	{
		return 60f;
	}

	// Token: 0x06000AE4 RID: 2788 RVA: 0x00049A48 File Offset: 0x00047C48
	public override bool CanLoot()
	{
		return this.lootEnabled;
	}

	// Token: 0x06000AE5 RID: 2789 RVA: 0x00049A50 File Offset: 0x00047C50
	public void SetLootableIn(float when)
	{
		base.Invoke(new Action(this.EnableLooting), when);
	}

	// Token: 0x06000AE6 RID: 2790 RVA: 0x00049A68 File Offset: 0x00047C68
	public void EnableLooting()
	{
		this.lootEnabled = true;
	}

	// Token: 0x040005AA RID: 1450
	private bool lootEnabled;
}
