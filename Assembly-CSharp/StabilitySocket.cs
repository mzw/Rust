﻿using System;
using UnityEngine;

// Token: 0x0200022F RID: 559
public class StabilitySocket : global::Socket_Base
{
	// Token: 0x06001003 RID: 4099 RVA: 0x000616F4 File Offset: 0x0005F8F4
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.DrawWireCube(this.selectCenter, this.selectSize);
	}

	// Token: 0x06001004 RID: 4100 RVA: 0x00061718 File Offset: 0x0005F918
	public override bool TestTarget(global::Construction.Target target)
	{
		return false;
	}

	// Token: 0x06001005 RID: 4101 RVA: 0x0006171C File Offset: 0x0005F91C
	public override bool CanConnect(Vector3 position, Quaternion rotation, global::Socket_Base socket, Vector3 socketPosition, Quaternion socketRotation)
	{
		if (!base.CanConnect(position, rotation, socket, socketPosition, socketRotation))
		{
			return false;
		}
		OBB selectBounds = base.GetSelectBounds(position, rotation);
		OBB selectBounds2 = socket.GetSelectBounds(socketPosition, socketRotation);
		return selectBounds.Intersects(selectBounds2);
	}

	// Token: 0x04000AB6 RID: 2742
	[Range(0f, 1f)]
	public float support = 1f;
}
