﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000461 RID: 1121
public class MonumentInfo : MonoBehaviour
{
	// Token: 0x0600189E RID: 6302 RVA: 0x0008ABA4 File Offset: 0x00088DA4
	protected void Awake()
	{
		if (global::TerrainMeta.Path)
		{
			global::TerrainMeta.Path.Monuments.Add(this);
		}
		foreach (global::TerrainPathConnect target in base.GetComponentsInChildren<global::TerrainPathConnect>())
		{
			this.AddTarget(target);
		}
	}

	// Token: 0x0600189F RID: 6303 RVA: 0x0008ABF8 File Offset: 0x00088DF8
	public bool CheckPlacement(Vector3 pos, Quaternion rot, Vector3 scale)
	{
		OBB obb;
		obb..ctor(pos, scale, rot, this.Bounds);
		Vector3 point = obb.GetPoint(-1f, 0f, -1f);
		Vector3 point2 = obb.GetPoint(-1f, 0f, 1f);
		Vector3 point3 = obb.GetPoint(1f, 0f, -1f);
		Vector3 point4 = obb.GetPoint(1f, 0f, 1f);
		int topology = global::TerrainMeta.TopologyMap.GetTopology(point);
		int topology2 = global::TerrainMeta.TopologyMap.GetTopology(point2);
		int topology3 = global::TerrainMeta.TopologyMap.GetTopology(point3);
		int topology4 = global::TerrainMeta.TopologyMap.GetTopology(point4);
		int num = 0;
		if ((this.Tier & global::MonumentTier.Tier0) != (global::MonumentTier)0)
		{
			num |= 67108864;
		}
		if ((this.Tier & global::MonumentTier.Tier1) != (global::MonumentTier)0)
		{
			num |= 134217728;
		}
		if ((this.Tier & global::MonumentTier.Tier2) != (global::MonumentTier)0)
		{
			num |= 268435456;
		}
		return (num & topology) != 0 && (num & topology2) != 0 && (num & topology3) != 0 && (num & topology4) != 0;
	}

	// Token: 0x060018A0 RID: 6304 RVA: 0x0008AD20 File Offset: 0x00088F20
	protected void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = new Color(0f, 0.7f, 1f, 0.1f);
		Gizmos.DrawCube(this.Bounds.center, this.Bounds.size);
		Gizmos.color = new Color(0f, 0.7f, 1f, 1f);
		Gizmos.DrawWireCube(this.Bounds.center, this.Bounds.size);
	}

	// Token: 0x060018A1 RID: 6305 RVA: 0x0008ADB0 File Offset: 0x00088FB0
	public void AddTarget(global::TerrainPathConnect target)
	{
		global::InfrastructureType type = target.Type;
		if (!this.targets.ContainsKey(type))
		{
			this.targets.Add(type, new List<global::TerrainPathConnect>());
		}
		this.targets[type].Add(target);
	}

	// Token: 0x060018A2 RID: 6306 RVA: 0x0008ADF8 File Offset: 0x00088FF8
	public List<global::TerrainPathConnect> GetTargets(global::InfrastructureType type)
	{
		if (!this.targets.ContainsKey(type))
		{
			this.targets.Add(type, new List<global::TerrainPathConnect>());
		}
		return this.targets[type];
	}

	// Token: 0x060018A3 RID: 6307 RVA: 0x0008AE28 File Offset: 0x00089028
	public global::MonumentNavMesh GetMonumentNavMesh()
	{
		return base.GetComponent<global::MonumentNavMesh>();
	}

	// Token: 0x0400135C RID: 4956
	public global::MonumentType Type = global::MonumentType.Building;

	// Token: 0x0400135D RID: 4957
	[InspectorFlags]
	public global::MonumentTier Tier = (global::MonumentTier)-1;

	// Token: 0x0400135E RID: 4958
	[ReadOnly]
	public Bounds Bounds = new Bounds(Vector3.zero, Vector3.zero);

	// Token: 0x0400135F RID: 4959
	public bool HasNavmesh;

	// Token: 0x04001360 RID: 4960
	public bool shouldDisplayOnMap;

	// Token: 0x04001361 RID: 4961
	public global::Translate.Phrase displayPhrase;

	// Token: 0x04001362 RID: 4962
	private Dictionary<global::InfrastructureType, List<global::TerrainPathConnect>> targets = new Dictionary<global::InfrastructureType, List<global::TerrainPathConnect>>();
}
