﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200026F RID: 623
public class MeshPaintController : MonoBehaviour, IClientComponent
{
	// Token: 0x04000B83 RID: 2947
	public Camera pickerCamera;

	// Token: 0x04000B84 RID: 2948
	public Texture2D brushTexture;

	// Token: 0x04000B85 RID: 2949
	public Vector2 brushScale = new Vector2(8f, 8f);

	// Token: 0x04000B86 RID: 2950
	public Color brushColor = Color.white;

	// Token: 0x04000B87 RID: 2951
	public float brushSpacing = 2f;

	// Token: 0x04000B88 RID: 2952
	public RawImage brushImage;

	// Token: 0x04000B89 RID: 2953
	private Vector3 lastPosition;
}
