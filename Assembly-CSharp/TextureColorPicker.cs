﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

// Token: 0x02000283 RID: 643
public class TextureColorPicker : MonoBehaviour, IPointerDownHandler, IDragHandler, IEventSystemHandler
{
	// Token: 0x060010C5 RID: 4293 RVA: 0x00064C28 File Offset: 0x00062E28
	public virtual void OnPointerDown(PointerEventData eventData)
	{
		this.OnDrag(eventData);
	}

	// Token: 0x060010C6 RID: 4294 RVA: 0x00064C34 File Offset: 0x00062E34
	public virtual void OnDrag(PointerEventData eventData)
	{
		RectTransform rectTransform = base.transform as RectTransform;
		Vector2 vector = default(Vector2);
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, ref vector))
		{
			vector.x += rectTransform.rect.width * 0.5f;
			vector.y += rectTransform.rect.height * 0.5f;
			vector.x /= rectTransform.rect.width;
			vector.y /= rectTransform.rect.height;
			Color pixel = this.texture.GetPixel((int)(vector.x * (float)this.texture.width), (int)(vector.y * (float)this.texture.height));
			this.onColorSelected.Invoke(pixel);
		}
	}

	// Token: 0x04000BED RID: 3053
	public Texture2D texture;

	// Token: 0x04000BEE RID: 3054
	public global::TextureColorPicker.onColorSelectedEvent onColorSelected = new global::TextureColorPicker.onColorSelectedEvent();

	// Token: 0x02000284 RID: 644
	[Serializable]
	public class onColorSelectedEvent : UnityEvent<Color>
	{
	}
}
