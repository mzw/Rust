﻿using System;
using UnityEngine;

// Token: 0x020005F3 RID: 1523
[Serializable]
public class WaterSimulation
{
	// Token: 0x04001A27 RID: 6695
	public int solverResolution = 64;

	// Token: 0x04001A28 RID: 6696
	public float solverSizeInWorld = 20f;

	// Token: 0x04001A29 RID: 6697
	public float gravity = 9.81f;

	// Token: 0x04001A2A RID: 6698
	public float amplitude = 0.0002f;

	// Token: 0x04001A2B RID: 6699
	public TextAsset perlinNoiseData;
}
