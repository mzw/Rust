﻿using System;
using UnityEngine;

// Token: 0x02000323 RID: 803
public class FireBomb : MonoBehaviour
{
	// Token: 0x04000E64 RID: 3684
	public GameObject fireParticle;

	// Token: 0x04000E65 RID: 3685
	public float bombRadius;

	// Token: 0x04000E66 RID: 3686
	public float particleDuration;

	// Token: 0x04000E67 RID: 3687
	public float emitDuration;
}
