﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000602 RID: 1538
public class SubsurfaceProfileTexture
{
	// Token: 0x06001F4E RID: 8014 RVA: 0x000B1C84 File Offset: 0x000AFE84
	public SubsurfaceProfileTexture()
	{
		this.AddProfile(global::SubsurfaceProfileData.Default, null);
	}

	// Token: 0x1700022D RID: 557
	// (get) Token: 0x06001F4F RID: 8015 RVA: 0x000B1CA8 File Offset: 0x000AFEA8
	public Texture2D Texture
	{
		get
		{
			return (!(this.texture == null)) ? this.texture : this.CreateTexture();
		}
	}

	// Token: 0x06001F50 RID: 8016 RVA: 0x000B1CCC File Offset: 0x000AFECC
	public int FindEntryIndex(global::SubsurfaceProfile profile)
	{
		for (int i = 0; i < this.entries.Count; i++)
		{
			if (this.entries[i].profile == profile)
			{
				return i;
			}
		}
		return -1;
	}

	// Token: 0x06001F51 RID: 8017 RVA: 0x000B1D18 File Offset: 0x000AFF18
	public int AddProfile(global::SubsurfaceProfileData data, global::SubsurfaceProfile profile)
	{
		int num = -1;
		for (int i = 0; i < this.entries.Count; i++)
		{
			if (this.entries[i].profile == profile)
			{
				num = i;
				this.entries[num] = new global::SubsurfaceProfileTexture.SubsurfaceProfileEntry(data, profile);
				break;
			}
		}
		if (num < 0)
		{
			num = this.entries.Count;
			this.entries.Add(new global::SubsurfaceProfileTexture.SubsurfaceProfileEntry(data, profile));
		}
		this.ReleaseTexture();
		return num;
	}

	// Token: 0x06001F52 RID: 8018 RVA: 0x000B1DA8 File Offset: 0x000AFFA8
	public void UpdateProfile(int id, global::SubsurfaceProfileData data)
	{
		if (id >= 0)
		{
			this.entries[id] = new global::SubsurfaceProfileTexture.SubsurfaceProfileEntry(data, this.entries[id].profile);
			this.ReleaseTexture();
		}
	}

	// Token: 0x06001F53 RID: 8019 RVA: 0x000B1DE8 File Offset: 0x000AFFE8
	public void RemoveProfile(int id)
	{
		if (id >= 0)
		{
			this.entries[id] = new global::SubsurfaceProfileTexture.SubsurfaceProfileEntry(global::SubsurfaceProfileData.Invalid, null);
			this.CheckReleaseTexture();
		}
	}

	// Token: 0x06001F54 RID: 8020 RVA: 0x000B1E10 File Offset: 0x000B0010
	public static Color ColorClamp(Color color, float min = 0f, float max = 1f)
	{
		Color result;
		result.r = Mathf.Clamp(color.r, min, max);
		result.g = Mathf.Clamp(color.g, min, max);
		result.b = Mathf.Clamp(color.b, min, max);
		result.a = Mathf.Clamp(color.a, min, max);
		return result;
	}

	// Token: 0x06001F55 RID: 8021 RVA: 0x000B1E74 File Offset: 0x000B0074
	private Texture2D CreateTexture()
	{
		if (this.entries.Count > 0)
		{
			int num = 32;
			int num2 = Mathf.Max(this.entries.Count, 64);
			this.ReleaseTexture();
			this.texture = new Texture2D(num, num2, 17, false, true);
			this.texture.name = "SubsurfaceProfiles";
			this.texture.wrapMode = 1;
			this.texture.filterMode = 1;
			Color[] pixels = this.texture.GetPixels(0);
			for (int i = 0; i < pixels.Length; i++)
			{
				pixels[i] = Color.clear;
			}
			Color[] array = new Color[num];
			for (int j = 0; j < this.entries.Count; j++)
			{
				global::SubsurfaceProfileData data = this.entries[j].data;
				data.SubsurfaceColor = global::SubsurfaceProfileTexture.ColorClamp(data.SubsurfaceColor, 0f, 1f);
				data.FalloffColor = global::SubsurfaceProfileTexture.ColorClamp(data.FalloffColor, 0.009f, 1f);
				array[0] = data.SubsurfaceColor;
				array[0].a = 0f;
				global::SeparableSSS.CalculateKernel(array, 1, 13, data.SubsurfaceColor, data.FalloffColor);
				global::SeparableSSS.CalculateKernel(array, 14, 9, data.SubsurfaceColor, data.FalloffColor);
				global::SeparableSSS.CalculateKernel(array, 23, 6, data.SubsurfaceColor, data.FalloffColor);
				int num3 = num * (num2 - j - 1);
				for (int k = 0; k < 29; k++)
				{
					Color color = array[k] * new Color(1f, 1f, 1f, 0.333333343f);
					color.a *= data.ScatterRadius / 1024f;
					pixels[num3 + k] = color;
				}
			}
			this.texture.SetPixels(pixels, 0);
			this.texture.Apply(false, false);
			return this.texture;
		}
		return null;
	}

	// Token: 0x06001F56 RID: 8022 RVA: 0x000B20A0 File Offset: 0x000B02A0
	private void CheckReleaseTexture()
	{
		int num = 0;
		for (int i = 0; i < this.entries.Count; i++)
		{
			num += ((!(this.entries[i].profile == null)) ? 0 : 1);
		}
		if (this.entries.Count == num)
		{
			this.ReleaseTexture();
		}
	}

	// Token: 0x06001F57 RID: 8023 RVA: 0x000B210C File Offset: 0x000B030C
	private void ReleaseTexture()
	{
		if (this.texture != null)
		{
			Object.DestroyImmediate(this.texture);
			this.texture = null;
		}
	}

	// Token: 0x04001A4E RID: 6734
	public const int SUBSURFACE_RADIUS_SCALE = 1024;

	// Token: 0x04001A4F RID: 6735
	public const int SUBSURFACE_KERNEL_SIZE = 3;

	// Token: 0x04001A50 RID: 6736
	private List<global::SubsurfaceProfileTexture.SubsurfaceProfileEntry> entries = new List<global::SubsurfaceProfileTexture.SubsurfaceProfileEntry>(16);

	// Token: 0x04001A51 RID: 6737
	private Texture2D texture;

	// Token: 0x02000603 RID: 1539
	private struct SubsurfaceProfileEntry
	{
		// Token: 0x06001F58 RID: 8024 RVA: 0x000B2134 File Offset: 0x000B0334
		public SubsurfaceProfileEntry(global::SubsurfaceProfileData data, global::SubsurfaceProfile profile)
		{
			this.data = data;
			this.profile = profile;
		}

		// Token: 0x04001A52 RID: 6738
		public global::SubsurfaceProfileData data;

		// Token: 0x04001A53 RID: 6739
		public global::SubsurfaceProfile profile;
	}
}
