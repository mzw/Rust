﻿using System;
using UnityEngine;

// Token: 0x020001EE RID: 494
public class OnePoleLowpassFilter : MonoBehaviour
{
	// Token: 0x040009AA RID: 2474
	[Range(10f, 20000f)]
	public float frequency = 20000f;
}
