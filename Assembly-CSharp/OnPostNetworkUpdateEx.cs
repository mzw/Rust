﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000438 RID: 1080
public static class OnPostNetworkUpdateEx
{
	// Token: 0x06001869 RID: 6249 RVA: 0x0008A5C0 File Offset: 0x000887C0
	public static void BroadcastOnPostNetworkUpdate(this GameObject go, global::BaseEntity entity)
	{
		List<global::IOnPostNetworkUpdate> list = Pool.GetList<global::IOnPostNetworkUpdate>();
		go.GetComponentsInChildren<global::IOnPostNetworkUpdate>(list);
		for (int i = 0; i < list.Count; i++)
		{
			global::IOnPostNetworkUpdate onPostNetworkUpdate = list[i];
			GameObject gameObject = (onPostNetworkUpdate as MonoBehaviour).gameObject;
			if (gameObject.ToBaseEntity() == entity)
			{
				onPostNetworkUpdate.OnPostNetworkUpdate(entity);
			}
		}
		Pool.FreeList<global::IOnPostNetworkUpdate>(ref list);
	}

	// Token: 0x0600186A RID: 6250 RVA: 0x0008A624 File Offset: 0x00088824
	public static void SendOnPostNetworkUpdate(this GameObject go, global::BaseEntity entity)
	{
		List<global::IOnPostNetworkUpdate> list = Pool.GetList<global::IOnPostNetworkUpdate>();
		go.GetComponents<global::IOnPostNetworkUpdate>(list);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].OnPostNetworkUpdate(entity);
		}
		Pool.FreeList<global::IOnPostNetworkUpdate>(ref list);
	}
}
