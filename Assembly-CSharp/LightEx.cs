﻿using System;
using UnityEngine;

// Token: 0x02000269 RID: 617
public class LightEx : global::UpdateBehaviour, IClientComponent
{
	// Token: 0x06001083 RID: 4227 RVA: 0x00063E0C File Offset: 0x0006200C
	protected void OnValidate()
	{
		global::LightEx.CheckConflict(base.gameObject);
	}

	// Token: 0x06001084 RID: 4228 RVA: 0x00063E1C File Offset: 0x0006201C
	public static bool CheckConflict(GameObject go)
	{
		global::LightEx component = go.GetComponent<global::LightEx>();
		if (!component || !component.alterIntensity)
		{
			return false;
		}
		global::LightLOD component2 = go.GetComponent<global::LightLOD>();
		if (component2 && component2.ToggleLight)
		{
			Debug.LogError("Using LightEx.AlterIntensity and LightLOD.ToggleLight on the same light source is not permitted: " + go.transform.root.name, go.transform.root);
			return true;
		}
		global::AmbientLightLOD component3 = go.GetComponent<global::AmbientLightLOD>();
		if (component3)
		{
			Debug.LogError("Using LightEx.AlterIntensity and AmbientLightLOD on the same light source is not permitted: " + go.transform.root.name, go.transform.root);
			return true;
		}
		return false;
	}

	// Token: 0x04000B4D RID: 2893
	public bool alterColor;

	// Token: 0x04000B4E RID: 2894
	public float colorTimeScale = 1f;

	// Token: 0x04000B4F RID: 2895
	public Color colorA = Color.red;

	// Token: 0x04000B50 RID: 2896
	public Color colorB = Color.yellow;

	// Token: 0x04000B51 RID: 2897
	public AnimationCurve blendCurve = new AnimationCurve();

	// Token: 0x04000B52 RID: 2898
	public bool loopColor = true;

	// Token: 0x04000B53 RID: 2899
	public bool alterIntensity;

	// Token: 0x04000B54 RID: 2900
	public float intensityTimeScale = 1f;

	// Token: 0x04000B55 RID: 2901
	public AnimationCurve intenseCurve = new AnimationCurve();

	// Token: 0x04000B56 RID: 2902
	public float intensityCurveScale = 3f;

	// Token: 0x04000B57 RID: 2903
	public bool loopIntensity = true;

	// Token: 0x04000B58 RID: 2904
	public bool randomOffset;
}
