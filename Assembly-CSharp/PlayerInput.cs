﻿using System;
using Rust;

// Token: 0x020003C2 RID: 962
public class PlayerInput : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x060016A8 RID: 5800 RVA: 0x00082DA8 File Offset: 0x00080FA8
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.state.Clear();
	}

	// Token: 0x0400112E RID: 4398
	public global::InputState state = new global::InputState();

	// Token: 0x0400112F RID: 4399
	[NonSerialized]
	public bool hadInputBuffer = true;
}
