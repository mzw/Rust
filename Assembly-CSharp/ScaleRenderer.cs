﻿using System;
using UnityEngine;

// Token: 0x0200032F RID: 815
public class ScaleRenderer : MonoBehaviour
{
	// Token: 0x060013AD RID: 5037 RVA: 0x00073964 File Offset: 0x00071B64
	private bool ScaleDifferent(float newScale)
	{
		return newScale != this.lastScale;
	}

	// Token: 0x060013AE RID: 5038 RVA: 0x00073974 File Offset: 0x00071B74
	public void Start()
	{
		if (this.useRandomScale)
		{
			this.SetScale(Random.Range(this.scaleMin, this.scaleMax));
		}
	}

	// Token: 0x060013AF RID: 5039 RVA: 0x00073998 File Offset: 0x00071B98
	public void SetScale(float scale)
	{
		if (!this.hasInitialValues)
		{
			this.GatherInitialValues();
		}
		if (this.ScaleDifferent(scale))
		{
			this.SetRendererEnabled(scale != 0f);
			this.SetScale_Internal(scale);
		}
	}

	// Token: 0x060013B0 RID: 5040 RVA: 0x000739D0 File Offset: 0x00071BD0
	public virtual void SetScale_Internal(float scale)
	{
		this.lastScale = scale;
	}

	// Token: 0x060013B1 RID: 5041 RVA: 0x000739DC File Offset: 0x00071BDC
	public virtual void SetRendererEnabled(bool isEnabled)
	{
		if (this.myRenderer && this.myRenderer.enabled != isEnabled)
		{
			this.myRenderer.enabled = isEnabled;
		}
	}

	// Token: 0x060013B2 RID: 5042 RVA: 0x00073A0C File Offset: 0x00071C0C
	public virtual void GatherInitialValues()
	{
		this.hasInitialValues = true;
	}

	// Token: 0x04000E98 RID: 3736
	public bool useRandomScale;

	// Token: 0x04000E99 RID: 3737
	public float scaleMin = 1f;

	// Token: 0x04000E9A RID: 3738
	public float scaleMax = 1f;

	// Token: 0x04000E9B RID: 3739
	private float lastScale = -1f;

	// Token: 0x04000E9C RID: 3740
	protected bool hasInitialValues;

	// Token: 0x04000E9D RID: 3741
	public Renderer myRenderer;
}
