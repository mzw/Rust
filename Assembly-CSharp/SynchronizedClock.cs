﻿using System;
using System.Collections.Generic;

// Token: 0x020007A3 RID: 1955
public class SynchronizedClock
{
	// Token: 0x17000293 RID: 659
	// (get) Token: 0x06002470 RID: 9328 RVA: 0x000C8E4C File Offset: 0x000C704C
	private static long Ticks
	{
		get
		{
			return (!TOD_Sky.Instance) ? DateTime.Now.Ticks : TOD_Sky.Instance.Cycle.Ticks;
		}
	}

	// Token: 0x17000294 RID: 660
	// (get) Token: 0x06002471 RID: 9329 RVA: 0x000C8E8C File Offset: 0x000C708C
	private static float DayLengthInMinutes
	{
		get
		{
			return (!TOD_Sky.Instance) ? 30f : TOD_Sky.Instance.Components.Time.DayLengthInMinutes;
		}
	}

	// Token: 0x06002472 RID: 9330 RVA: 0x000C8EBC File Offset: 0x000C70BC
	public void Add(float delta, float variance, Action<uint> action)
	{
		global::SynchronizedClock.TimedEvent item = default(global::SynchronizedClock.TimedEvent);
		item.ticks = global::SynchronizedClock.Ticks;
		item.delta = delta;
		item.variance = variance;
		item.action = action;
		this.events.Add(item);
	}

	// Token: 0x06002473 RID: 9331 RVA: 0x000C8F04 File Offset: 0x000C7104
	public void Tick()
	{
		long num = 10000000L;
		double num2 = 1440.0 / (double)global::SynchronizedClock.DayLengthInMinutes;
		double num3 = (double)num * num2;
		for (int i = 0; i < this.events.Count; i++)
		{
			global::SynchronizedClock.TimedEvent value = this.events[i];
			long ticks = value.ticks;
			long ticks2 = global::SynchronizedClock.Ticks;
			long num4 = (long)((double)value.delta * num3);
			long num5 = ticks / num4 * num4;
			uint obj = (uint)(num5 % (long)((ulong)-1));
			SeedRandom.Wanghash(ref obj);
			double num6 = (double)SeedRandom.Range(ref obj, -value.variance, value.variance);
			long num7 = (long)(num6 * num3);
			long num8 = num5 + num4 + num7;
			if (ticks < num8 && ticks2 >= num8)
			{
				value.action(obj);
				value.ticks = ticks2;
			}
			else if (ticks2 > ticks || ticks2 < num5)
			{
				value.ticks = ticks2;
			}
			this.events[i] = value;
		}
	}

	// Token: 0x04001FE1 RID: 8161
	public List<global::SynchronizedClock.TimedEvent> events = new List<global::SynchronizedClock.TimedEvent>();

	// Token: 0x020007A4 RID: 1956
	public struct TimedEvent
	{
		// Token: 0x04001FE2 RID: 8162
		public long ticks;

		// Token: 0x04001FE3 RID: 8163
		public float delta;

		// Token: 0x04001FE4 RID: 8164
		public float variance;

		// Token: 0x04001FE5 RID: 8165
		public Action<uint> action;
	}
}
