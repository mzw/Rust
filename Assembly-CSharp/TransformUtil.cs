﻿using System;
using System.Linq;
using UnityEngine;

// Token: 0x020007A8 RID: 1960
public static class TransformUtil
{
	// Token: 0x0600247E RID: 9342 RVA: 0x000C91F0 File Offset: 0x000C73F0
	public static bool GetGroundInfo(Vector3 startPos, out RaycastHit hit, Transform ignoreTransform = null)
	{
		return global::TransformUtil.GetGroundInfo(startPos, out hit, 100f, -1, ignoreTransform);
	}

	// Token: 0x0600247F RID: 9343 RVA: 0x000C9208 File Offset: 0x000C7408
	public static bool GetGroundInfo(Vector3 startPos, out RaycastHit hit, float range, Transform ignoreTransform = null)
	{
		return global::TransformUtil.GetGroundInfo(startPos, out hit, range, -1, ignoreTransform);
	}

	// Token: 0x06002480 RID: 9344 RVA: 0x000C921C File Offset: 0x000C741C
	public static bool GetGroundInfo(Vector3 startPos, out RaycastHit hitOut, float range, LayerMask mask, Transform ignoreTransform = null)
	{
		startPos.y += 0.25f;
		range += 0.25f;
		hitOut = default(RaycastHit);
		Ray ray;
		ray..ctor(startPos, Vector3.down);
		RaycastHit raycastHit;
		if (!Physics.Raycast(ray, ref raycastHit, range, mask))
		{
			return false;
		}
		if (ignoreTransform != null && (raycastHit.collider.transform == ignoreTransform || raycastHit.collider.transform.IsChildOf(ignoreTransform)))
		{
			return global::TransformUtil.GetGroundInfo(startPos - new Vector3(0f, 0.01f, 0f), out hitOut, range, mask, ignoreTransform);
		}
		hitOut = raycastHit;
		return true;
	}

	// Token: 0x06002481 RID: 9345 RVA: 0x000C92DC File Offset: 0x000C74DC
	public static bool GetGroundInfo(Vector3 startPos, out Vector3 pos, out Vector3 normal, Transform ignoreTransform = null)
	{
		return global::TransformUtil.GetGroundInfo(startPos, out pos, out normal, 100f, -1, ignoreTransform);
	}

	// Token: 0x06002482 RID: 9346 RVA: 0x000C92F4 File Offset: 0x000C74F4
	public static bool GetGroundInfo(Vector3 startPos, out Vector3 pos, out Vector3 normal, float range, Transform ignoreTransform = null)
	{
		return global::TransformUtil.GetGroundInfo(startPos, out pos, out normal, range, -1, ignoreTransform);
	}

	// Token: 0x06002483 RID: 9347 RVA: 0x000C9308 File Offset: 0x000C7508
	public static bool GetGroundInfo(Vector3 startPos, out Vector3 pos, out Vector3 normal, float range, LayerMask mask, Transform ignoreTransform = null)
	{
		startPos.y += 0.25f;
		range += 0.25f;
		Ray ray;
		ray..ctor(startPos, Vector3.down);
		IOrderedEnumerable<RaycastHit> orderedEnumerable = from h in Physics.RaycastAll(ray, range, mask)
		orderby h.distance
		select h;
		foreach (RaycastHit raycastHit in orderedEnumerable)
		{
			if (!(ignoreTransform != null) || (!(raycastHit.collider.transform == ignoreTransform) && !raycastHit.collider.transform.IsChildOf(ignoreTransform)))
			{
				pos = raycastHit.point;
				normal = raycastHit.normal;
				return true;
			}
		}
		pos = startPos;
		normal = Vector3.up;
		return false;
	}

	// Token: 0x06002484 RID: 9348 RVA: 0x000C942C File Offset: 0x000C762C
	public static bool GetGroundInfoTerrainOnly(Vector3 startPos, out Vector3 pos, out Vector3 normal)
	{
		return global::TransformUtil.GetGroundInfoTerrainOnly(startPos, out pos, out normal, 100f, -1);
	}

	// Token: 0x06002485 RID: 9349 RVA: 0x000C9444 File Offset: 0x000C7644
	public static bool GetGroundInfoTerrainOnly(Vector3 startPos, out Vector3 pos, out Vector3 normal, float range)
	{
		return global::TransformUtil.GetGroundInfoTerrainOnly(startPos, out pos, out normal, range, -1);
	}

	// Token: 0x06002486 RID: 9350 RVA: 0x000C9458 File Offset: 0x000C7658
	public static bool GetGroundInfoTerrainOnly(Vector3 startPos, out Vector3 pos, out Vector3 normal, float range, LayerMask mask)
	{
		startPos.y += 0.25f;
		range += 0.25f;
		Ray ray;
		ray..ctor(startPos, Vector3.down);
		RaycastHit raycastHit;
		if (Physics.Raycast(ray, ref raycastHit, range, mask) && raycastHit.collider is TerrainCollider)
		{
			pos = raycastHit.point;
			normal = raycastHit.normal;
			return true;
		}
		pos = startPos;
		normal = Vector3.up;
		return false;
	}

	// Token: 0x06002487 RID: 9351 RVA: 0x000C94E4 File Offset: 0x000C76E4
	public static Transform[] GetRootObjects()
	{
		return (from x in Object.FindObjectsOfType<Transform>()
		where x.transform == x.transform.root
		select x).ToArray<Transform>();
	}
}
