﻿using System;
using UnityEngine;

// Token: 0x020005C2 RID: 1474
public class PlaceDecorValueNoise : global::ProceduralComponent
{
	// Token: 0x06001E92 RID: 7826 RVA: 0x000AB78C File Offset: 0x000A998C
	public override void Process(uint seed)
	{
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + this.ResourceFolder, null, null, true);
		if (array == null || array.Length == 0)
		{
			return;
		}
		Vector3 position = global::TerrainMeta.Position;
		Vector3 size = global::TerrainMeta.Size;
		int num = Mathf.RoundToInt(this.ObjectDensity * size.x * size.z * 1E-06f);
		float x = position.x;
		float z = position.z;
		float num2 = position.x + size.x;
		float num3 = position.z + size.z;
		float num4 = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float num5 = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		int octaves = this.Cluster.Octaves;
		float offset = this.Cluster.Offset;
		float num6 = this.Cluster.Frequency * 0.01f;
		float amplitude = this.Cluster.Amplitude;
		for (int i = 0; i < num; i++)
		{
			float num7 = SeedRandom.Range(ref seed, x, num2);
			float num8 = SeedRandom.Range(ref seed, z, num3);
			float normX = global::TerrainMeta.NormalizeX(num7);
			float normZ = global::TerrainMeta.NormalizeZ(num8);
			float num9 = SeedRandom.Value(ref seed);
			float factor = this.Filter.GetFactor(normX, normZ);
			global::Prefab random = array.GetRandom(ref seed);
			if (factor > 0f)
			{
				float num10 = offset + global::Noise.Turbulence((double)(num4 + num7), (double)(num5 + num8), octaves, (double)num6, (double)amplitude, 2.0, 0.5);
				if (num10 * factor * factor >= num9)
				{
					float height = heightMap.GetHeight(normX, normZ);
					Vector3 vector;
					vector..ctor(num7, height, num8);
					Quaternion localRotation = random.Object.transform.localRotation;
					Vector3 localScale = random.Object.transform.localScale;
					random.ApplyDecorComponents(ref vector, ref localRotation, ref localScale);
					if (random.ApplyTerrainAnchors(ref vector, localRotation, localScale, this.Filter))
					{
						if (random.ApplyTerrainChecks(vector, localRotation, localScale, this.Filter))
						{
							if (random.ApplyTerrainFilters(vector, localRotation, localScale, null))
							{
								random.ApplyTerrainModifiers(vector, localRotation, localScale);
								global::World.Serialization.AddPrefab("Decor", random.ID, vector, localRotation, localScale);
							}
						}
					}
				}
			}
		}
	}

	// Token: 0x0400195C RID: 6492
	public global::SpawnFilter Filter;

	// Token: 0x0400195D RID: 6493
	public string ResourceFolder = string.Empty;

	// Token: 0x0400195E RID: 6494
	public global::NoiseParameters Cluster = new global::NoiseParameters(2, 0.5f, 1f, 0f);

	// Token: 0x0400195F RID: 6495
	public float ObjectDensity = 100f;
}
