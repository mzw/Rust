﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x02000535 RID: 1333
public class AsyncTerrainNavMeshBake : CustomYieldInstruction
{
	// Token: 0x06001C09 RID: 7177 RVA: 0x0009E048 File Offset: 0x0009C248
	public AsyncTerrainNavMeshBake(Vector3 pivot, int width, int height, bool normal, bool alpha)
	{
		this.pivot = pivot;
		this.width = width;
		this.height = height;
		this.normal = normal;
		this.alpha = alpha;
		this.indices = Pool.GetList<int>();
		this.vertices = Pool.GetList<Vector3>();
		this.normals = ((!normal) ? null : Pool.GetList<Vector3>());
		this.triangles = Pool.GetList<int>();
		this.Invoke();
	}

	// Token: 0x170001EA RID: 490
	// (get) Token: 0x06001C0A RID: 7178 RVA: 0x0009E0C0 File Offset: 0x0009C2C0
	public override bool keepWaiting
	{
		get
		{
			return this.worker != null;
		}
	}

	// Token: 0x170001EB RID: 491
	// (get) Token: 0x06001C0B RID: 7179 RVA: 0x0009E0D0 File Offset: 0x0009C2D0
	public bool isDone
	{
		get
		{
			return this.worker == null;
		}
	}

	// Token: 0x06001C0C RID: 7180 RVA: 0x0009E0DC File Offset: 0x0009C2DC
	public NavMeshBuildSource CreateNavMeshBuildSource(bool addSourceObject)
	{
		NavMeshBuildSource result = default(NavMeshBuildSource);
		result.transform = Matrix4x4.TRS(this.pivot, Quaternion.identity, Vector3.one);
		result.shape = 0;
		if (addSourceObject)
		{
			result.sourceObject = this.mesh;
		}
		return result;
	}

	// Token: 0x170001EC RID: 492
	// (get) Token: 0x06001C0D RID: 7181 RVA: 0x0009E12C File Offset: 0x0009C32C
	public Mesh mesh
	{
		get
		{
			Mesh mesh = new Mesh();
			if (this.vertices != null)
			{
				mesh.SetVertices(this.vertices);
				Pool.FreeList<Vector3>(ref this.vertices);
			}
			if (this.normals != null)
			{
				mesh.SetNormals(this.normals);
				Pool.FreeList<Vector3>(ref this.normals);
			}
			if (this.triangles != null)
			{
				mesh.SetTriangles(this.triangles, 0);
				Pool.FreeList<int>(ref this.triangles);
			}
			if (this.indices != null)
			{
				Pool.FreeList<int>(ref this.indices);
			}
			return mesh;
		}
	}

	// Token: 0x06001C0E RID: 7182 RVA: 0x0009E1C0 File Offset: 0x0009C3C0
	private void DoWork()
	{
		Vector3 vector;
		vector..ctor((float)(this.width / 2), 0f, (float)(this.height / 2));
		Vector3 vector2;
		vector2..ctor(this.pivot.x - vector.x, 0f, this.pivot.z - vector.z);
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::TerrainAlphaMap alphaMap = global::TerrainMeta.AlphaMap;
		int num = 0;
		for (int i = 0; i <= this.height; i++)
		{
			int j = 0;
			while (j <= this.width)
			{
				Vector3 worldPos = new Vector3((float)j, 0f, (float)i) + vector2;
				Vector3 item = new Vector3((float)j, 0f, (float)i) - vector;
				float num2 = heightMap.GetHeight(worldPos);
				if (num2 < -1f)
				{
					this.indices.Add(-1);
				}
				else
				{
					if (this.alpha)
					{
						float num3 = alphaMap.GetAlpha(worldPos);
						if (num3 < 0.1f)
						{
							this.indices.Add(-1);
							goto IL_15E;
						}
					}
					if (this.normal)
					{
						Vector3 item2 = heightMap.GetNormal(worldPos);
						this.normals.Add(item2);
					}
					worldPos.y = (item.y = num2 - this.pivot.y);
					this.indices.Add(this.vertices.Count);
					this.vertices.Add(item);
				}
				IL_15E:
				j++;
				num++;
			}
		}
		int num4 = 0;
		int k = 0;
		while (k < this.height)
		{
			int l = 0;
			while (l < this.width)
			{
				int num5 = this.indices[num4];
				int num6 = this.indices[num4 + this.width + 1];
				int num7 = this.indices[num4 + 1];
				int num8 = this.indices[num4 + 1];
				int num9 = this.indices[num4 + this.width + 1];
				int num10 = this.indices[num4 + this.width + 2];
				if (num5 != -1 && num6 != -1 && num7 != -1)
				{
					this.triangles.Add(num5);
					this.triangles.Add(num6);
					this.triangles.Add(num7);
				}
				if (num8 != -1 && num9 != -1 && num10 != -1)
				{
					this.triangles.Add(num8);
					this.triangles.Add(num9);
					this.triangles.Add(num10);
				}
				l++;
				num4++;
			}
			k++;
			num4++;
		}
	}

	// Token: 0x06001C0F RID: 7183 RVA: 0x0009E494 File Offset: 0x0009C694
	private void Invoke()
	{
		this.worker = new Action(this.DoWork);
		this.worker.BeginInvoke(new AsyncCallback(this.Callback), null);
	}

	// Token: 0x06001C10 RID: 7184 RVA: 0x0009E4C4 File Offset: 0x0009C6C4
	private void Callback(IAsyncResult result)
	{
		this.worker.EndInvoke(result);
		this.worker = null;
	}

	// Token: 0x04001747 RID: 5959
	private List<int> indices;

	// Token: 0x04001748 RID: 5960
	private List<Vector3> vertices;

	// Token: 0x04001749 RID: 5961
	private List<Vector3> normals;

	// Token: 0x0400174A RID: 5962
	private List<int> triangles;

	// Token: 0x0400174B RID: 5963
	private Vector3 pivot;

	// Token: 0x0400174C RID: 5964
	private int width;

	// Token: 0x0400174D RID: 5965
	private int height;

	// Token: 0x0400174E RID: 5966
	private bool normal;

	// Token: 0x0400174F RID: 5967
	private bool alpha;

	// Token: 0x04001750 RID: 5968
	private Action worker;
}
