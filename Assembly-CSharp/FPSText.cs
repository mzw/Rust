﻿using System;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000684 RID: 1668
public class FPSText : MonoBehaviour
{
	// Token: 0x060020EB RID: 8427 RVA: 0x000BAE20 File Offset: 0x000B9020
	protected void Update()
	{
		if (this.fpsTimer.Elapsed.TotalSeconds < 0.5)
		{
			return;
		}
		this.text.enabled = true;
		this.fpsTimer.Reset();
		this.fpsTimer.Start();
		string text = global::Performance.current.frameRate + " FPS";
		this.text.text = text;
	}

	// Token: 0x04001C3E RID: 7230
	public Text text;

	// Token: 0x04001C3F RID: 7231
	private Stopwatch fpsTimer = Stopwatch.StartNew();
}
