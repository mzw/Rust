﻿using System;

// Token: 0x020001DF RID: 479
public interface ISoundBudgetedUpdate
{
	// Token: 0x06000F17 RID: 3863
	void DoUpdate();
}
