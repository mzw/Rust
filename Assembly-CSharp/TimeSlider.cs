﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000708 RID: 1800
public class TimeSlider : MonoBehaviour
{
	// Token: 0x06002242 RID: 8770 RVA: 0x000C0504 File Offset: 0x000BE704
	private void Start()
	{
		this.slider = base.GetComponent<Slider>();
	}

	// Token: 0x06002243 RID: 8771 RVA: 0x000C0514 File Offset: 0x000BE714
	private void Update()
	{
		if (TOD_Sky.Instance == null)
		{
			return;
		}
		this.slider.value = TOD_Sky.Instance.Cycle.Hour;
	}

	// Token: 0x06002244 RID: 8772 RVA: 0x000C0544 File Offset: 0x000BE744
	public void OnValue(float f)
	{
		if (TOD_Sky.Instance == null)
		{
			return;
		}
		TOD_Sky.Instance.Cycle.Hour = f;
		TOD_Sky.Instance.UpdateAmbient();
		TOD_Sky.Instance.UpdateReflection();
		TOD_Sky.Instance.UpdateFog();
	}

	// Token: 0x04001EBE RID: 7870
	private Slider slider;
}
