﻿using System;
using UnityEngine;

// Token: 0x020005D5 RID: 1493
public static class TerrainModifierEx
{
	// Token: 0x06001EC3 RID: 7875 RVA: 0x000AD850 File Offset: 0x000ABA50
	public static void ApplyTerrainModifiers(this Transform transform, global::TerrainModifier[] modifiers, Vector3 pos, Quaternion rot, Vector3 scale)
	{
		foreach (global::TerrainModifier terrainModifier in modifiers)
		{
			Vector3 vector = Vector3.Scale(terrainModifier.worldPosition, scale);
			Vector3 pos2 = pos + rot * vector;
			float y = scale.y;
			terrainModifier.Apply(pos2, y);
		}
	}

	// Token: 0x06001EC4 RID: 7876 RVA: 0x000AD8A4 File Offset: 0x000ABAA4
	public static void ApplyTerrainModifiers(this Transform transform, global::TerrainModifier[] modifiers)
	{
		transform.ApplyTerrainModifiers(modifiers, transform.position, transform.rotation, transform.lossyScale);
	}
}
