﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000653 RID: 1619
[CreateAssetMenu(menuName = "Rust/Skin Set")]
public class SkinSet : ScriptableObject
{
	// Token: 0x06002085 RID: 8325 RVA: 0x000B9DB0 File Offset: 0x000B7FB0
	public void Process(GameObject obj, float Seed)
	{
		List<SkinnedMeshRenderer> list = Pool.GetList<SkinnedMeshRenderer>();
		obj.GetComponentsInChildren<SkinnedMeshRenderer>(true, list);
		foreach (SkinnedMeshRenderer skinnedMeshRenderer in list)
		{
			if (!(skinnedMeshRenderer.sharedMesh == null))
			{
				if (!(skinnedMeshRenderer.sharedMaterial == null))
				{
					string name = skinnedMeshRenderer.sharedMesh.name;
					string name2 = skinnedMeshRenderer.sharedMaterial.name;
					for (int i = 0; i < this.MeshReplacements.Length; i++)
					{
						if (this.MeshReplacements[i].Test(name))
						{
							SkinnedMeshRenderer skinnedMeshRenderer2 = this.MeshReplacements[i].Get(Seed);
							skinnedMeshRenderer.sharedMesh = skinnedMeshRenderer2.sharedMesh;
							skinnedMeshRenderer.rootBone = skinnedMeshRenderer2.rootBone;
							skinnedMeshRenderer.bones = skinnedMeshRenderer2.bones;
						}
					}
					for (int j = 0; j < this.MaterialReplacements.Length; j++)
					{
						if (this.MaterialReplacements[j].Test(name2))
						{
							skinnedMeshRenderer.sharedMaterial = this.MaterialReplacements[j].Get(Seed);
						}
					}
				}
			}
		}
		Pool.FreeList<SkinnedMeshRenderer>(ref list);
	}

	// Token: 0x06002086 RID: 8326 RVA: 0x000B9F18 File Offset: 0x000B8118
	internal Color GetSkinColor(float skinNumber)
	{
		return this.SkinColour.Evaluate(skinNumber);
	}

	// Token: 0x04001B7D RID: 7037
	public string Label;

	// Token: 0x04001B7E RID: 7038
	public global::SkinSet.MeshReplace[] MeshReplacements;

	// Token: 0x04001B7F RID: 7039
	public global::SkinSet.MaterialReplace[] MaterialReplacements;

	// Token: 0x04001B80 RID: 7040
	public Gradient SkinColour;

	// Token: 0x04001B81 RID: 7041
	public global::HairSetCollection HairCollection;

	// Token: 0x02000654 RID: 1620
	[Serializable]
	public class MeshReplace
	{
		// Token: 0x06002088 RID: 8328 RVA: 0x000B9F30 File Offset: 0x000B8130
		public SkinnedMeshRenderer Get(float MeshNumber)
		{
			return this.Replace[Mathf.Clamp(Mathf.FloorToInt(MeshNumber * (float)this.Replace.Length), 0, this.Replace.Length - 1)];
		}

		// Token: 0x06002089 RID: 8329 RVA: 0x000B9F5C File Offset: 0x000B815C
		public bool Test(string materialName)
		{
			return this.FindName == materialName;
		}

		// Token: 0x04001B82 RID: 7042
		[HideInInspector]
		public string FindName;

		// Token: 0x04001B83 RID: 7043
		public Mesh Find;

		// Token: 0x04001B84 RID: 7044
		public SkinnedMeshRenderer[] Replace;
	}

	// Token: 0x02000655 RID: 1621
	[Serializable]
	public class MaterialReplace
	{
		// Token: 0x0600208B RID: 8331 RVA: 0x000B9F74 File Offset: 0x000B8174
		public Material Get(float MeshNumber)
		{
			return this.Replace[Mathf.Clamp(Mathf.FloorToInt(MeshNumber * (float)this.Replace.Length), 0, this.Replace.Length - 1)];
		}

		// Token: 0x0600208C RID: 8332 RVA: 0x000B9FA0 File Offset: 0x000B81A0
		public bool Test(string materialName)
		{
			return this.FindName == materialName;
		}

		// Token: 0x04001B85 RID: 7045
		[HideInInspector]
		public string FindName;

		// Token: 0x04001B86 RID: 7046
		public Material Find;

		// Token: 0x04001B87 RID: 7047
		public Material[] Replace;
	}
}
