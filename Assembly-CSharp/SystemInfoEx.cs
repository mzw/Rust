﻿using System;
using System.Runtime.InteropServices;

// Token: 0x020002F9 RID: 761
public static class SystemInfoEx
{
	// Token: 0x0600133B RID: 4923
	[DllImport("RustNative")]
	private static extern ulong System_GetMemoryUsage();

	// Token: 0x17000169 RID: 361
	// (get) Token: 0x0600133C RID: 4924 RVA: 0x000712B4 File Offset: 0x0006F4B4
	public static int systemMemoryUsed
	{
		get
		{
			return (int)(global::SystemInfoEx.System_GetMemoryUsage() / 1024UL / 1024UL);
		}
	}
}
