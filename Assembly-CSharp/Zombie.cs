﻿using System;

// Token: 0x020000F6 RID: 246
public class Zombie : global::BaseAnimalNPC
{
	// Token: 0x170000AF RID: 175
	// (get) Token: 0x06000C2F RID: 3119 RVA: 0x0004FFB4 File Offset: 0x0004E1B4
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000C30 RID: 3120 RVA: 0x0004FFB8 File Offset: 0x0004E1B8
	public override bool WantsToEat(global::BaseEntity best)
	{
		return !best.HasTrait(global::BaseEntity.TraitFlag.Alive) && base.WantsToEat(best);
	}

	// Token: 0x06000C31 RID: 3121 RVA: 0x0004FFD0 File Offset: 0x0004E1D0
	protected override void TickSleep()
	{
		this.Sleep = 100f;
	}

	// Token: 0x06000C32 RID: 3122 RVA: 0x0004FFE0 File Offset: 0x0004E1E0
	public override string Categorize()
	{
		return "Zombie";
	}

	// Token: 0x040006B8 RID: 1720
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population;
}
