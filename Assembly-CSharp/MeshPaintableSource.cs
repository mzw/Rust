﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200026E RID: 622
public class MeshPaintableSource : MonoBehaviour, IClientComponent
{
	// Token: 0x06001097 RID: 4247 RVA: 0x00064134 File Offset: 0x00062334
	public void Init()
	{
		if (this.texture)
		{
			return;
		}
		this.texture = new Texture2D(this.texWidth, this.texHeight, 5, false);
		this.texture.name = "MeshPaintableSource_" + base.gameObject.name;
		this.texture.Clear(Color.clear);
		if (global::MeshPaintableSource.block == null)
		{
			global::MeshPaintableSource.block = new MaterialPropertyBlock();
		}
		else
		{
			global::MeshPaintableSource.block.Clear();
		}
		global::MeshPaintableSource.block.SetTexture(this.replacementTextureName, this.texture);
		List<Renderer> list = Pool.GetList<Renderer>();
		base.transform.root.GetComponentsInChildren<Renderer>(true, list);
		foreach (Renderer renderer in list)
		{
			renderer.SetPropertyBlock(global::MeshPaintableSource.block);
		}
		Pool.FreeList<Renderer>(ref list);
	}

	// Token: 0x06001098 RID: 4248 RVA: 0x00064248 File Offset: 0x00062448
	public void Free()
	{
		if (this.texture)
		{
			Object.Destroy(this.texture);
			this.texture = null;
		}
	}

	// Token: 0x06001099 RID: 4249 RVA: 0x0006426C File Offset: 0x0006246C
	public void UpdateFrom(Texture2D input)
	{
		this.Init();
		this.texture.SetPixels32(input.GetPixels32());
		this.texture.Apply(true, false);
	}

	// Token: 0x0600109A RID: 4250 RVA: 0x00064294 File Offset: 0x00062494
	public void Load(byte[] data)
	{
		this.Init();
		if (data != null)
		{
			ImageConversion.LoadImage(this.texture, data);
			if (this.texture.width != this.texWidth || this.texture.height != this.texHeight)
			{
				this.texture.Resize(this.texWidth, this.texHeight);
			}
			this.texture.Apply(true, false);
		}
	}

	// Token: 0x04000B78 RID: 2936
	public int texWidth = 256;

	// Token: 0x04000B79 RID: 2937
	public int texHeight = 128;

	// Token: 0x04000B7A RID: 2938
	public string replacementTextureName = "_DecalTexture";

	// Token: 0x04000B7B RID: 2939
	public float cameraFOV = 60f;

	// Token: 0x04000B7C RID: 2940
	public float cameraDistance = 2f;

	// Token: 0x04000B7D RID: 2941
	[NonSerialized]
	public Texture2D texture;

	// Token: 0x04000B7E RID: 2942
	public GameObject sourceObject;

	// Token: 0x04000B7F RID: 2943
	public Mesh collisionMesh;

	// Token: 0x04000B80 RID: 2944
	public Vector3 localPosition;

	// Token: 0x04000B81 RID: 2945
	public Vector3 localRotation;

	// Token: 0x04000B82 RID: 2946
	private static MaterialPropertyBlock block;
}
