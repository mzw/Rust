﻿using System;
using System.Linq;
using Rust;
using UnityEngine;

// Token: 0x02000499 RID: 1177
public class TriggerHurt : global::TriggerBase
{
	// Token: 0x060019AA RID: 6570 RVA: 0x00090768 File Offset: 0x0008E968
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x060019AB RID: 6571 RVA: 0x000907B4 File Offset: 0x0008E9B4
	internal override void OnObjects()
	{
		base.InvokeRepeating(new Action(this.OnTick), 0f, 1f / this.DamageTickRate);
	}

	// Token: 0x060019AC RID: 6572 RVA: 0x000907DC File Offset: 0x0008E9DC
	internal override void OnEmpty()
	{
		base.CancelInvoke(new Action(this.OnTick));
	}

	// Token: 0x060019AD RID: 6573 RVA: 0x000907F0 File Offset: 0x0008E9F0
	private void OnTick()
	{
		global::BaseEntity attacker = base.gameObject.ToBaseEntity();
		if (this.entityContents == null)
		{
			return;
		}
		foreach (global::BaseEntity baseEntity in this.entityContents.ToArray<global::BaseEntity>())
		{
			if (baseEntity.IsValid())
			{
				global::BaseCombatEntity baseCombatEntity = baseEntity as global::BaseCombatEntity;
				if (!(baseCombatEntity == null))
				{
					baseCombatEntity.Hurt(this.DamagePerSecond * (1f / this.DamageTickRate), this.damageType, attacker, true);
				}
			}
		}
	}

	// Token: 0x04001441 RID: 5185
	public float DamagePerSecond = 1f;

	// Token: 0x04001442 RID: 5186
	public float DamageTickRate = 4f;

	// Token: 0x04001443 RID: 5187
	public Rust.DamageType damageType;
}
