﻿using System;
using UnityEngine;

// Token: 0x020004EA RID: 1258
public class ItemModEntity : global::ItemMod
{
	// Token: 0x06001AE4 RID: 6884 RVA: 0x000968F8 File Offset: 0x00094AF8
	public override void OnItemCreated(global::Item item)
	{
		if (item.GetHeldEntity() == null)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.entityPrefab.resourcePath, default(Vector3), default(Quaternion), true);
			if (baseEntity == null)
			{
				Debug.LogWarning(string.Concat(new string[]
				{
					"Couldn't create item entity ",
					item.info.displayName.english,
					" (",
					this.entityPrefab.resourcePath,
					")"
				}));
				return;
			}
			baseEntity.skinID = item.skin;
			baseEntity.Spawn();
			item.SetHeldEntity(baseEntity);
		}
	}

	// Token: 0x06001AE5 RID: 6885 RVA: 0x000969B0 File Offset: 0x00094BB0
	public override void OnRemove(global::Item item)
	{
		global::BaseEntity heldEntity = item.GetHeldEntity();
		if (heldEntity == null)
		{
			return;
		}
		heldEntity.Kill(global::BaseNetworkable.DestroyMode.None);
		item.SetHeldEntity(null);
	}

	// Token: 0x06001AE6 RID: 6886 RVA: 0x000969E0 File Offset: 0x00094BE0
	private bool ParentToParent(global::Item item, global::BaseEntity ourEntity)
	{
		if (item.parentItem == null)
		{
			return false;
		}
		global::BaseEntity baseEntity = item.parentItem.GetWorldEntity();
		if (baseEntity == null)
		{
			baseEntity = item.parentItem.GetHeldEntity();
		}
		ourEntity.SetFlag(global::BaseEntity.Flags.Disabled, false, false);
		ourEntity.limitNetworking = false;
		ourEntity.SetParent(baseEntity, this.defaultBone);
		return true;
	}

	// Token: 0x06001AE7 RID: 6887 RVA: 0x00096A40 File Offset: 0x00094C40
	private bool ParentToPlayer(global::Item item, global::BaseEntity ourEntity)
	{
		global::HeldEntity heldEntity = ourEntity as global::HeldEntity;
		if (heldEntity == null)
		{
			return false;
		}
		global::BasePlayer ownerPlayer = item.GetOwnerPlayer();
		if (ownerPlayer)
		{
			heldEntity.SetOwnerPlayer(ownerPlayer);
			return true;
		}
		heldEntity.ClearOwnerPlayer();
		return true;
	}

	// Token: 0x06001AE8 RID: 6888 RVA: 0x00096A84 File Offset: 0x00094C84
	public override void OnParentChanged(global::Item item)
	{
		global::BaseEntity heldEntity = item.GetHeldEntity();
		if (heldEntity == null)
		{
			return;
		}
		if (this.ParentToParent(item, heldEntity))
		{
			return;
		}
		if (this.ParentToPlayer(item, heldEntity))
		{
			return;
		}
		heldEntity.SetParent(null, 0u);
		heldEntity.limitNetworking = true;
		heldEntity.SetFlag(global::BaseEntity.Flags.Disabled, true, false);
	}

	// Token: 0x06001AE9 RID: 6889 RVA: 0x00096ADC File Offset: 0x00094CDC
	public override void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
		global::BaseEntity heldEntity = item.GetHeldEntity();
		if (heldEntity == null)
		{
			return;
		}
		global::HeldEntity heldEntity2 = heldEntity as global::HeldEntity;
		if (heldEntity2 == null)
		{
			return;
		}
		heldEntity2.CollectedForCrafting(item, crafter);
	}

	// Token: 0x06001AEA RID: 6890 RVA: 0x00096B1C File Offset: 0x00094D1C
	public override void ReturnedFromCancelledCraft(global::Item item, global::BasePlayer crafter)
	{
		global::BaseEntity heldEntity = item.GetHeldEntity();
		if (heldEntity == null)
		{
			return;
		}
		global::HeldEntity heldEntity2 = heldEntity as global::HeldEntity;
		if (heldEntity2 == null)
		{
			return;
		}
		heldEntity2.ReturnedFromCancelledCraft(item, crafter);
	}

	// Token: 0x040015AD RID: 5549
	public global::GameObjectRef entityPrefab = new global::GameObjectRef();

	// Token: 0x040015AE RID: 5550
	public string defaultBone;
}
