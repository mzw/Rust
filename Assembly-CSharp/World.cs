﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x02000537 RID: 1335
public static class World
{
	// Token: 0x170001ED RID: 493
	// (get) Token: 0x06001C2E RID: 7214 RVA: 0x0009ED08 File Offset: 0x0009CF08
	// (set) Token: 0x06001C2F RID: 7215 RVA: 0x0009ED10 File Offset: 0x0009CF10
	public static uint Seed { get; set; }

	// Token: 0x170001EE RID: 494
	// (get) Token: 0x06001C30 RID: 7216 RVA: 0x0009ED18 File Offset: 0x0009CF18
	// (set) Token: 0x06001C31 RID: 7217 RVA: 0x0009ED20 File Offset: 0x0009CF20
	public static uint Salt { get; set; }

	// Token: 0x170001EF RID: 495
	// (get) Token: 0x06001C32 RID: 7218 RVA: 0x0009ED28 File Offset: 0x0009CF28
	// (set) Token: 0x06001C33 RID: 7219 RVA: 0x0009ED30 File Offset: 0x0009CF30
	public static uint Size { get; set; }

	// Token: 0x170001F0 RID: 496
	// (get) Token: 0x06001C34 RID: 7220 RVA: 0x0009ED38 File Offset: 0x0009CF38
	// (set) Token: 0x06001C35 RID: 7221 RVA: 0x0009ED40 File Offset: 0x0009CF40
	public static string Checksum { get; set; }

	// Token: 0x170001F1 RID: 497
	// (get) Token: 0x06001C36 RID: 7222 RVA: 0x0009ED48 File Offset: 0x0009CF48
	// (set) Token: 0x06001C37 RID: 7223 RVA: 0x0009ED50 File Offset: 0x0009CF50
	public static bool Procedural { get; set; }

	// Token: 0x170001F2 RID: 498
	// (get) Token: 0x06001C38 RID: 7224 RVA: 0x0009ED58 File Offset: 0x0009CF58
	// (set) Token: 0x06001C39 RID: 7225 RVA: 0x0009ED60 File Offset: 0x0009CF60
	public static global::WorldSerialization Serialization { get; set; }

	// Token: 0x170001F3 RID: 499
	// (get) Token: 0x06001C3A RID: 7226 RVA: 0x0009ED68 File Offset: 0x0009CF68
	public static string Name
	{
		get
		{
			return Application.loadedLevelName.Replace(" ", string.Empty).ToLower();
		}
	}

	// Token: 0x170001F4 RID: 500
	// (get) Token: 0x06001C3B RID: 7227 RVA: 0x0009ED84 File Offset: 0x0009CF84
	public static string FileName
	{
		get
		{
			return string.Concat(new object[]
			{
				global::World.Name,
				".",
				global::World.Size,
				".",
				global::World.Seed,
				".",
				158
			});
		}
	}

	// Token: 0x170001F5 RID: 501
	// (get) Token: 0x06001C3C RID: 7228 RVA: 0x0009EDE4 File Offset: 0x0009CFE4
	public static string FileRegex
	{
		get
		{
			return "\\.[0-9]+\\.[0-9]+\\." + 158;
		}
	}

	// Token: 0x06001C3D RID: 7229 RVA: 0x0009EDFC File Offset: 0x0009CFFC
	public static void InitSeed(int seed)
	{
		global::World.InitSeed((uint)seed);
	}

	// Token: 0x06001C3E RID: 7230 RVA: 0x0009EE04 File Offset: 0x0009D004
	public static void InitSeed(uint seed)
	{
		if (seed == 0u)
		{
			seed = global::World.SeedIdentifier().MurmurHashUnsigned() % 2147483647u;
		}
		if (seed == 0u)
		{
			seed = 123456u;
		}
		global::World.Seed = seed;
		ConVar.Server.seed = (int)seed;
	}

	// Token: 0x06001C3F RID: 7231 RVA: 0x0009EE38 File Offset: 0x0009D038
	private static string SeedIdentifier()
	{
		return SystemInfo.deviceUniqueIdentifier + "_" + 158;
	}

	// Token: 0x06001C40 RID: 7232 RVA: 0x0009EE54 File Offset: 0x0009D054
	public static void InitSalt(int salt)
	{
		global::World.InitSalt((uint)salt);
	}

	// Token: 0x06001C41 RID: 7233 RVA: 0x0009EE5C File Offset: 0x0009D05C
	public static void InitSalt(uint salt)
	{
		if (salt == 0u)
		{
			salt = global::World.SaltIdentifier().MurmurHashUnsigned() % 2147483647u;
		}
		if (salt == 0u)
		{
			salt = 654321u;
		}
		global::World.Salt = salt;
		ConVar.Server.salt = (int)salt;
	}

	// Token: 0x06001C42 RID: 7234 RVA: 0x0009EE90 File Offset: 0x0009D090
	private static string SaltIdentifier()
	{
		return SystemInfo.deviceUniqueIdentifier + "_salt";
	}

	// Token: 0x06001C43 RID: 7235 RVA: 0x0009EEA4 File Offset: 0x0009D0A4
	public static void InitSize(int size)
	{
		global::World.InitSize((uint)size);
	}

	// Token: 0x06001C44 RID: 7236 RVA: 0x0009EEAC File Offset: 0x0009D0AC
	public static void InitSize(uint size)
	{
		if (size == 0u)
		{
			size = 4000u;
		}
		if (size < 1000u)
		{
			size = 1000u;
		}
		if (size > 6000u)
		{
			size = 6000u;
		}
		global::World.Size = size;
		ConVar.Server.worldsize = (int)size;
	}

	// Token: 0x04001758 RID: 5976
	public const uint Version = 6u;
}
