﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200068F RID: 1679
public class MapGrid : SingletonComponent<global::MapGrid>
{
	// Token: 0x04001C64 RID: 7268
	public Text coordinatePrefab;

	// Token: 0x04001C65 RID: 7269
	public RawImage gridLinePrefab;

	// Token: 0x04001C66 RID: 7270
	public int gridCellSize = 200;

	// Token: 0x04001C67 RID: 7271
	public float lineThickness = 0.3f;

	// Token: 0x04001C68 RID: 7272
	public CanvasGroup group;

	// Token: 0x04001C69 RID: 7273
	public float visibleAlphaLevel = 0.6f;

	// Token: 0x04001C6A RID: 7274
	public bool show;
}
