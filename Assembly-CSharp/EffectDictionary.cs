﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x02000416 RID: 1046
public class EffectDictionary
{
	// Token: 0x060017FF RID: 6143 RVA: 0x00088DC0 File Offset: 0x00086FC0
	public static string GetParticle(string impactType, string materialName)
	{
		return global::EffectDictionary.LookupEffect("impacts", impactType, materialName);
	}

	// Token: 0x06001800 RID: 6144 RVA: 0x00088DD0 File Offset: 0x00086FD0
	public static string GetParticle(Rust.DamageType damageType, string materialName)
	{
		switch (damageType)
		{
		case Rust.DamageType.Bullet:
			return global::EffectDictionary.GetParticle("bullet", materialName);
		case Rust.DamageType.Slash:
			return global::EffectDictionary.GetParticle("slash", materialName);
		case Rust.DamageType.Blunt:
			return global::EffectDictionary.GetParticle("blunt", materialName);
		default:
			if (damageType != Rust.DamageType.Arrow)
			{
				return global::EffectDictionary.GetParticle("blunt", materialName);
			}
			return global::EffectDictionary.GetParticle("bullet", materialName);
		case Rust.DamageType.Stab:
			return global::EffectDictionary.GetParticle("stab", materialName);
		}
	}

	// Token: 0x06001801 RID: 6145 RVA: 0x00088E58 File Offset: 0x00087058
	public static string GetDecal(string impactType, string materialName)
	{
		return global::EffectDictionary.LookupEffect("decals", impactType, materialName);
	}

	// Token: 0x06001802 RID: 6146 RVA: 0x00088E68 File Offset: 0x00087068
	public static string GetDecal(Rust.DamageType damageType, string materialName)
	{
		switch (damageType)
		{
		case Rust.DamageType.Bullet:
			return global::EffectDictionary.GetDecal("bullet", materialName);
		case Rust.DamageType.Slash:
			return global::EffectDictionary.GetDecal("slash", materialName);
		case Rust.DamageType.Blunt:
			return global::EffectDictionary.GetDecal("blunt", materialName);
		default:
			if (damageType != Rust.DamageType.Arrow)
			{
				return global::EffectDictionary.GetDecal("blunt", materialName);
			}
			return global::EffectDictionary.GetDecal("bullet", materialName);
		case Rust.DamageType.Stab:
			return global::EffectDictionary.GetDecal("stab", materialName);
		}
	}

	// Token: 0x06001803 RID: 6147 RVA: 0x00088EF0 File Offset: 0x000870F0
	public static string GetDisplacement(string impactType, string materialName)
	{
		return global::EffectDictionary.LookupEffect("displacement", impactType, materialName);
	}

	// Token: 0x06001804 RID: 6148 RVA: 0x00088F00 File Offset: 0x00087100
	private static string LookupEffect(string category, string effect, string material)
	{
		if (global::EffectDictionary.effectDictionary == null)
		{
			global::EffectDictionary.effectDictionary = global::GameManifest.LoadEffectDictionary();
		}
		string key = global::StringFormatCache.Get("assets/bundled/prefabs/fx/{0}/{1}/{2}", category, effect, material);
		string[] array;
		if (!global::EffectDictionary.effectDictionary.TryGetValue(key, out array))
		{
			return string.Empty;
		}
		return array[Random.Range(0, array.Length)];
	}

	// Token: 0x04001288 RID: 4744
	private static Dictionary<string, string[]> effectDictionary;
}
