﻿using System;

// Token: 0x0200041D RID: 1053
public enum EnvironmentType
{
	// Token: 0x04001297 RID: 4759
	Underground = 1,
	// Token: 0x04001298 RID: 4760
	Building,
	// Token: 0x04001299 RID: 4761
	Outdoor = 4,
	// Token: 0x0400129A RID: 4762
	Elevator = 8,
	// Token: 0x0400129B RID: 4763
	PlayerConstruction = 16
}
