﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rust;
using UnityEngine;

// Token: 0x020003E2 RID: 994
public class WildlifeTrap : global::StorageContainer
{
	// Token: 0x0600171F RID: 5919 RVA: 0x00084DC8 File Offset: 0x00082FC8
	public void SetTrapActive(bool trapOn)
	{
		if (trapOn == this.IsTrapActive())
		{
			return;
		}
		base.CancelInvoke(new Action(this.TrapThink));
		base.SetFlag(global::BaseEntity.Flags.On, trapOn, false);
		if (trapOn)
		{
			base.InvokeRepeating(new Action(this.TrapThink), this.tickRate * 0.8f + this.tickRate * Random.Range(0f, 0.4f), this.tickRate);
		}
	}

	// Token: 0x06001720 RID: 5920 RVA: 0x00084E40 File Offset: 0x00083040
	public int GetBaitCalories()
	{
		int num = 0;
		foreach (global::Item item in this.inventory.itemList)
		{
			global::ItemModConsumable component = item.info.GetComponent<global::ItemModConsumable>();
			if (!(component == null))
			{
				if (!this.ignoreBait.Contains(item.info))
				{
					foreach (global::ItemModConsumable.ConsumableEffect consumableEffect in component.effects)
					{
						if (consumableEffect.type == global::MetabolismAttribute.Type.Calories && consumableEffect.amount > 0f)
						{
							num += Mathf.CeilToInt(consumableEffect.amount * (float)item.amount);
						}
					}
				}
			}
		}
		return num;
	}

	// Token: 0x06001721 RID: 5921 RVA: 0x00084F50 File Offset: 0x00083150
	public void DestroyRandomFoodItem()
	{
		int count = this.inventory.itemList.Count;
		int num = Random.Range(0, count);
		for (int i = 0; i < count; i++)
		{
			int num2 = num + i;
			if (num2 >= count)
			{
				num2 -= count;
			}
			global::Item item = this.inventory.itemList[num2];
			if (item != null)
			{
				global::ItemModConsumable component = item.info.GetComponent<global::ItemModConsumable>();
				if (!(component == null))
				{
					item.UseItem(1);
					break;
				}
			}
		}
	}

	// Token: 0x06001722 RID: 5922 RVA: 0x00084FE4 File Offset: 0x000831E4
	public void UseBaitCalories(int numToUse)
	{
		foreach (global::Item item in this.inventory.itemList)
		{
			int itemCalories = this.GetItemCalories(item);
			if (itemCalories > 0)
			{
				numToUse -= itemCalories;
				item.UseItem(1);
				if (numToUse <= 0)
				{
					break;
				}
			}
		}
	}

	// Token: 0x06001723 RID: 5923 RVA: 0x00085068 File Offset: 0x00083268
	public int GetItemCalories(global::Item item)
	{
		global::ItemModConsumable component = item.info.GetComponent<global::ItemModConsumable>();
		if (component == null)
		{
			return 0;
		}
		foreach (global::ItemModConsumable.ConsumableEffect consumableEffect in component.effects)
		{
			if (consumableEffect.type == global::MetabolismAttribute.Type.Calories && consumableEffect.amount > 0f)
			{
				return Mathf.CeilToInt(consumableEffect.amount);
			}
		}
		return 0;
	}

	// Token: 0x06001724 RID: 5924 RVA: 0x00085108 File Offset: 0x00083308
	public virtual void TrapThink()
	{
		int baitCalories = this.GetBaitCalories();
		if (baitCalories <= 0)
		{
			return;
		}
		global::TrappableWildlife randomWildlife = this.GetRandomWildlife();
		if (baitCalories >= randomWildlife.caloriesForInterest && Random.Range(0f, 1f) <= randomWildlife.successRate)
		{
			this.UseBaitCalories(randomWildlife.caloriesForInterest);
			if (Random.Range(0f, 1f) <= this.trapSuccessRate)
			{
				this.TrapWildlife(randomWildlife);
			}
		}
	}

	// Token: 0x06001725 RID: 5925 RVA: 0x00085180 File Offset: 0x00083380
	public void TrapWildlife(global::TrappableWildlife trapped)
	{
		global::Item item = global::ItemManager.Create(trapped.inventoryObject, Random.Range(trapped.minToCatch, trapped.maxToCatch + 1), 0UL);
		if (!item.MoveToContainer(this.inventory, -1, true))
		{
			item.Remove(0f);
		}
		else
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved1, true, false);
		}
		this.SetTrapActive(false);
		this.Hurt(this.StartMaxHealth() * 0.1f, Rust.DamageType.Decay, null, false);
	}

	// Token: 0x06001726 RID: 5926 RVA: 0x000851FC File Offset: 0x000833FC
	public void ClearTrap()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
	}

	// Token: 0x06001727 RID: 5927 RVA: 0x0008520C File Offset: 0x0008340C
	public bool HasBait()
	{
		return this.GetBaitCalories() > 0;
	}

	// Token: 0x06001728 RID: 5928 RVA: 0x00085218 File Offset: 0x00083418
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		this.SetTrapActive(this.HasBait());
		this.ClearTrap();
		base.PlayerStoppedLooting(player);
	}

	// Token: 0x06001729 RID: 5929 RVA: 0x00085234 File Offset: 0x00083434
	public override bool OnStartBeingLooted(global::BasePlayer baseEntity)
	{
		this.ClearTrap();
		return base.OnStartBeingLooted(baseEntity);
	}

	// Token: 0x0600172A RID: 5930 RVA: 0x00085244 File Offset: 0x00083444
	public global::TrappableWildlife GetRandomWildlife()
	{
		int num = this.targetWildlife.Sum((global::WildlifeTrap.WildlifeWeight x) => x.weight);
		int num2 = Random.Range(0, num);
		for (int i = 0; i < this.targetWildlife.Count; i++)
		{
			num -= this.targetWildlife[i].weight;
			if (num2 >= num)
			{
				return this.targetWildlife[i].wildlife;
			}
		}
		return null;
	}

	// Token: 0x0600172B RID: 5931 RVA: 0x000852CC File Offset: 0x000834CC
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x0600172C RID: 5932 RVA: 0x000852D0 File Offset: 0x000834D0
	public bool HasCatch()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x0600172D RID: 5933 RVA: 0x000852E0 File Offset: 0x000834E0
	public bool IsTrapActive()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x040011C8 RID: 4552
	public float tickRate = 60f;

	// Token: 0x040011C9 RID: 4553
	public global::GameObjectRef trappedEffect;

	// Token: 0x040011CA RID: 4554
	public float trappedEffectRepeatRate = 30f;

	// Token: 0x040011CB RID: 4555
	public float trapSuccessRate = 0.5f;

	// Token: 0x040011CC RID: 4556
	public List<global::ItemDefinition> ignoreBait;

	// Token: 0x040011CD RID: 4557
	public List<global::WildlifeTrap.WildlifeWeight> targetWildlife;

	// Token: 0x020003E3 RID: 995
	[Serializable]
	public class WildlifeWeight
	{
		// Token: 0x040011CF RID: 4559
		public global::TrappableWildlife wildlife;

		// Token: 0x040011D0 RID: 4560
		public int weight;
	}

	// Token: 0x020003E4 RID: 996
	public static class WildlifeTrapFlags
	{
		// Token: 0x040011D1 RID: 4561
		public const global::BaseEntity.Flags Occupied = global::BaseEntity.Flags.Reserved1;
	}
}
