﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000726 RID: 1830
public class UIVoiceIcon : MonoBehaviour
{
	// Token: 0x04001F06 RID: 7942
	public Text nameText;

	// Token: 0x04001F07 RID: 7943
	public RawImage avatar;
}
