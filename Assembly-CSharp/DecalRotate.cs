﻿using System;

// Token: 0x02000260 RID: 608
public class DecalRotate : global::DecalComponent
{
	// Token: 0x04000B43 RID: 2883
	[global::MinMax(0f, 360f)]
	public global::MinMax range = new global::MinMax(0f, 360f);
}
