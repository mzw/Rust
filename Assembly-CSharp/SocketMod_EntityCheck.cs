﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch;
using UnityEngine;

// Token: 0x02000227 RID: 551
public class SocketMod_EntityCheck : global::SocketMod
{
	// Token: 0x06000FEB RID: 4075 RVA: 0x00060E9C File Offset: 0x0005F09C
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = ((!this.wantsCollide) ? new Color(1f, 0f, 0f, 0.7f) : new Color(0f, 1f, 0f, 0.7f));
		Gizmos.DrawSphere(Vector3.zero, this.sphereRadius);
	}

	// Token: 0x06000FEC RID: 4076 RVA: 0x00060F10 File Offset: 0x0005F110
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 position = place.position + place.rotation * this.worldPosition;
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(position, this.sphereRadius, list, this.layerMask.value, this.queryTriggers);
		using (List<global::BaseEntity>.Enumerator enumerator = list.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				global::BaseEntity ent = enumerator.Current;
				bool flag = this.entityTypes.Any((global::BaseEntity x) => x.prefabID == ent.prefabID);
				if (flag && this.wantsCollide)
				{
					Pool.FreeList<global::BaseEntity>(ref list);
					return true;
				}
				if (flag && !this.wantsCollide)
				{
					Pool.FreeList<global::BaseEntity>(ref list);
					return false;
				}
			}
		}
		Pool.FreeList<global::BaseEntity>(ref list);
		return !this.wantsCollide;
	}

	// Token: 0x04000AA1 RID: 2721
	public float sphereRadius = 1f;

	// Token: 0x04000AA2 RID: 2722
	public LayerMask layerMask;

	// Token: 0x04000AA3 RID: 2723
	public QueryTriggerInteraction queryTriggers;

	// Token: 0x04000AA4 RID: 2724
	public global::BaseEntity[] entityTypes;

	// Token: 0x04000AA5 RID: 2725
	public bool wantsCollide;
}
