﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x02000354 RID: 852
public class AttackEntity : global::HeldEntity
{
	// Token: 0x06001461 RID: 5217 RVA: 0x00076E68 File Offset: 0x00075068
	public virtual void ServerUse()
	{
	}

	// Token: 0x06001462 RID: 5218 RVA: 0x00076E6C File Offset: 0x0007506C
	public virtual Vector3 ModifyAIAim(Vector3 eulerInput, float swayModifier = 1f)
	{
		return eulerInput;
	}

	// Token: 0x17000174 RID: 372
	// (get) Token: 0x06001463 RID: 5219 RVA: 0x00076E70 File Offset: 0x00075070
	public float NextAttackTime
	{
		get
		{
			return this.nextAttackTime;
		}
	}

	// Token: 0x06001464 RID: 5220 RVA: 0x00076E78 File Offset: 0x00075078
	public virtual void GetAttackStats(global::HitInfo info)
	{
	}

	// Token: 0x06001465 RID: 5221 RVA: 0x00076E7C File Offset: 0x0007507C
	protected void StartAttackCooldown(float cooldown)
	{
		this.nextAttackTime = this.CalculateCooldownTime(this.nextAttackTime, cooldown, true);
	}

	// Token: 0x06001466 RID: 5222 RVA: 0x00076E94 File Offset: 0x00075094
	protected void ResetAttackCooldown()
	{
		this.nextAttackTime = float.NegativeInfinity;
	}

	// Token: 0x06001467 RID: 5223 RVA: 0x00076EA4 File Offset: 0x000750A4
	protected bool HasAttackCooldown()
	{
		return UnityEngine.Time.time < this.nextAttackTime;
	}

	// Token: 0x06001468 RID: 5224 RVA: 0x00076EB4 File Offset: 0x000750B4
	protected float GetAttackCooldown()
	{
		return Mathf.Max(this.nextAttackTime - UnityEngine.Time.time, 0f);
	}

	// Token: 0x06001469 RID: 5225 RVA: 0x00076ECC File Offset: 0x000750CC
	protected float GetAttackIdle()
	{
		return Mathf.Max(UnityEngine.Time.time - this.nextAttackTime, 0f);
	}

	// Token: 0x0600146A RID: 5226 RVA: 0x00076EE4 File Offset: 0x000750E4
	protected float CalculateCooldownTime(float nextTime, float cooldown, bool catchup)
	{
		float time = UnityEngine.Time.time;
		float num = 0f;
		float num2 = 0f;
		if (base.isServer)
		{
			global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
			num2 = 0.1f + ((!ownerPlayer) ? 0.1f : ownerPlayer.desyncTime);
			num = Mathf.Max(UnityEngine.Time.deltaTime, UnityEngine.Time.smoothDeltaTime);
			cooldown *= 0.9f;
		}
		if (nextTime < 0f)
		{
			nextTime = Mathf.Max(0f, time + cooldown - num - num2);
		}
		else if (time - nextTime <= num + num2)
		{
			nextTime = Mathf.Min(nextTime + cooldown, time + cooldown);
		}
		else
		{
			nextTime = Mathf.Max(nextTime + cooldown, time + cooldown - num - num2);
		}
		return nextTime;
	}

	// Token: 0x0600146B RID: 5227 RVA: 0x00076FA4 File Offset: 0x000751A4
	protected bool VerifyClientRPC(global::BasePlayer player)
	{
		if (player == null)
		{
			Debug.LogWarning("Received RPC from null player");
			return false;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Owner not found (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "owner_missing");
			return false;
		}
		if (ownerPlayer != player)
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Player mismatch (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "player_mismatch");
			return false;
		}
		if (player.IsDead())
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Player dead (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "player_dead");
			return false;
		}
		if (player.IsWounded())
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Player down (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "player_down");
			return false;
		}
		if (player.IsSleeping())
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Player sleeping (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "player_sleeping");
			return false;
		}
		if (player.desyncTime > ConVar.AntiHack.maxdesync)
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, string.Concat(new object[]
			{
				"Player stalled (",
				base.ShortPrefabName,
				" with ",
				player.desyncTime,
				"s)"
			}));
			player.stats.combat.Log(this, "player_stalled");
			return false;
		}
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem == null)
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Item not found (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "item_missing");
			return false;
		}
		if (ownerItem.isBroken)
		{
			global::AntiHack.Log(player, global::AntiHackType.AttackHack, "Item broken (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "item_broken");
			return false;
		}
		return true;
	}

	// Token: 0x0600146C RID: 5228 RVA: 0x000771F8 File Offset: 0x000753F8
	protected virtual bool VerifyClientAttack(global::BasePlayer player)
	{
		if (!this.VerifyClientRPC(player))
		{
			return false;
		}
		if (this.HasAttackCooldown())
		{
			global::AntiHack.Log(player, global::AntiHackType.CooldownHack, string.Concat(new object[]
			{
				"T-",
				this.GetAttackCooldown(),
				"s (",
				base.ShortPrefabName,
				")"
			}));
			player.stats.combat.Log(this, "attack_cooldown");
			return false;
		}
		return true;
	}

	// Token: 0x0600146D RID: 5229 RVA: 0x0007727C File Offset: 0x0007547C
	protected bool ValidateEyePos(global::BasePlayer player, Vector3 eyePos)
	{
		bool flag = true;
		if (Vector3Ex.IsNaNOrInfinity(eyePos))
		{
			string shortPrefabName = base.ShortPrefabName;
			global::AntiHack.Log(player, global::AntiHackType.EyeHack, "Contains NaN (" + shortPrefabName + ")");
			player.stats.combat.Log(this, "eye_nan");
			flag = false;
		}
		if (ConVar.AntiHack.eye_protection > 0)
		{
			float num = 1f + ConVar.AntiHack.eye_forgiveness;
			float eye_clientframes = ConVar.AntiHack.eye_clientframes;
			float eye_serverframes = ConVar.AntiHack.eye_serverframes;
			float num2 = eye_clientframes / 60f;
			float num3 = eye_serverframes * Mathx.Max(UnityEngine.Time.deltaTime, UnityEngine.Time.smoothDeltaTime, UnityEngine.Time.fixedDeltaTime);
			float num4 = (player.desyncTime + num2 + num3) * num;
			if (ConVar.AntiHack.eye_protection >= 1)
			{
				float num5 = player.BoundsPadding() + num4 * player.MaxVelocity();
				float num6 = Vector3.Distance(player.eyes.position, eyePos);
				if (num6 > num5)
				{
					string shortPrefabName2 = base.ShortPrefabName;
					global::AntiHack.Log(player, global::AntiHackType.EyeHack, string.Concat(new object[]
					{
						"Distance (",
						shortPrefabName2,
						" on attack with ",
						num6,
						"m > ",
						num5,
						"m)"
					}));
					player.stats.combat.Log(this, "eye_distance");
					flag = false;
				}
			}
			if (ConVar.AntiHack.eye_protection >= 2)
			{
				Vector3 position = player.eyes.position;
				if (!global::GamePhysics.LineOfSight(position, eyePos, 2162688, 0f))
				{
					string shortPrefabName3 = base.ShortPrefabName;
					global::AntiHack.Log(player, global::AntiHackType.EyeHack, string.Concat(new object[]
					{
						"Line of sight (",
						shortPrefabName3,
						" on attack) ",
						position,
						" ",
						eyePos
					}));
					player.stats.combat.Log(this, "eye_los");
					flag = false;
				}
			}
			if (!flag)
			{
				global::AntiHack.AddViolation(player, global::AntiHackType.EyeHack, ConVar.AntiHack.eye_penalty);
			}
		}
		return flag;
	}

	// Token: 0x0600146E RID: 5230 RVA: 0x00077474 File Offset: 0x00075674
	public override void OnHeldChanged()
	{
		base.OnHeldChanged();
		this.StartAttackCooldown(this.deployDelay);
	}

	// Token: 0x04000F23 RID: 3875
	[Header("Attack Entity")]
	public float deployDelay = 1f;

	// Token: 0x04000F24 RID: 3876
	public float repeatDelay = 0.5f;

	// Token: 0x04000F25 RID: 3877
	[Header("NPCUsage")]
	public float effectiveRange = 1f;

	// Token: 0x04000F26 RID: 3878
	public float npcDamageScale = 1f;

	// Token: 0x04000F27 RID: 3879
	public float attackLengthMin = -1f;

	// Token: 0x04000F28 RID: 3880
	public float attackLengthMax = -1f;

	// Token: 0x04000F29 RID: 3881
	public float attackSpacing;

	// Token: 0x04000F2A RID: 3882
	public float aiAimSwayOffset;

	// Token: 0x04000F2B RID: 3883
	public float aiAimCone;

	// Token: 0x04000F2C RID: 3884
	public global::NPCPlayerApex.WeaponTypeEnum effectiveRangeType = global::NPCPlayerApex.WeaponTypeEnum.MediumRange;

	// Token: 0x04000F2D RID: 3885
	public global::SoundDefinition[] reloadSounds;

	// Token: 0x04000F2E RID: 3886
	public global::SoundDefinition thirdPersonMeleeSound;

	// Token: 0x04000F2F RID: 3887
	private float nextAttackTime = float.NegativeInfinity;
}
