﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200009B RID: 155
public class StashContainer : global::StorageContainer
{
	// Token: 0x060009B2 RID: 2482 RVA: 0x00042050 File Offset: 0x00040250
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("StashContainer.OnRpcMessage", 0.1f))
		{
			if (rpc == 62864025u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_HideStash ");
				}
				using (TimeWarning.New("RPC_HideStash", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_HideStash(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_HideStash");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 188414500u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_WantsUnhide ");
				}
				using (TimeWarning.New("RPC_WantsUnhide", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_WantsUnhide(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_WantsUnhide");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009B3 RID: 2483 RVA: 0x0004230C File Offset: 0x0004050C
	public bool PlayerInRange(global::BasePlayer ply)
	{
		if (Vector3.Distance(base.transform.position, ply.transform.position) <= this.uncoverRange)
		{
			Vector3 normalized = (base.transform.position - ply.eyes.position).normalized;
			float num = Vector3.Dot(ply.eyes.BodyForward(), normalized);
			if (num > 0.95f)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060009B4 RID: 2484 RVA: 0x00042384 File Offset: 0x00040584
	public void SetHidden(bool isHidden)
	{
		if (UnityEngine.Time.realtimeSinceStartup - this.lastToggleTime < 3f)
		{
			return;
		}
		this.lastToggleTime = UnityEngine.Time.realtimeSinceStartup;
		base.Invoke(new Action(this.Decay), 259200f);
		if (base.isServer)
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved5, isHidden, false);
		}
	}

	// Token: 0x060009B5 RID: 2485 RVA: 0x000423E4 File Offset: 0x000405E4
	public void Decay()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x060009B6 RID: 2486 RVA: 0x000423F0 File Offset: 0x000405F0
	public override void ServerInit()
	{
		base.ServerInit();
		this.SetHidden(false);
	}

	// Token: 0x060009B7 RID: 2487 RVA: 0x00042400 File Offset: 0x00040600
	public void ToggleHidden()
	{
		this.SetHidden(!this.IsHidden());
	}

	// Token: 0x060009B8 RID: 2488 RVA: 0x00042414 File Offset: 0x00040614
	[global::BaseEntity.RPC_Server]
	public void RPC_HideStash(global::BaseEntity.RPCMessage rpc)
	{
		if (Interface.CallHook("CanHideStash", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		this.SetHidden(true);
	}

	// Token: 0x060009B9 RID: 2489 RVA: 0x0004244C File Offset: 0x0004064C
	[global::BaseEntity.RPC_Server]
	public void RPC_WantsUnhide(global::BaseEntity.RPCMessage rpc)
	{
		if (!this.IsHidden())
		{
			return;
		}
		if (Interface.CallHook("CanSeeStash", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		global::BasePlayer player = rpc.player;
		if (this.PlayerInRange(player))
		{
			this.SetHidden(false);
		}
	}

	// Token: 0x060009BA RID: 2490 RVA: 0x000424A4 File Offset: 0x000406A4
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x060009BB RID: 2491 RVA: 0x000424A8 File Offset: 0x000406A8
	public bool IsHidden()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved5);
	}

	// Token: 0x0400047E RID: 1150
	public Transform visuals;

	// Token: 0x0400047F RID: 1151
	public float burriedOffset;

	// Token: 0x04000480 RID: 1152
	public float raisedOffset;

	// Token: 0x04000481 RID: 1153
	public global::GameObjectRef buryEffect;

	// Token: 0x04000482 RID: 1154
	public float uncoverRange = 3f;

	// Token: 0x04000483 RID: 1155
	private float lastToggleTime;

	// Token: 0x0200009C RID: 156
	public static class StashContainerFlags
	{
		// Token: 0x04000484 RID: 1156
		public const global::BaseEntity.Flags Hidden = global::BaseEntity.Flags.Reserved5;
	}
}
