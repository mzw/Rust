﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x02000087 RID: 135
public class PlayerMetabolism : global::BaseMetabolism<global::BasePlayer>
{
	// Token: 0x060008F5 RID: 2293 RVA: 0x0003BB8C File Offset: 0x00039D8C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("PlayerMetabolism.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060008F6 RID: 2294 RVA: 0x0003BBD4 File Offset: 0x00039DD4
	public override void Reset()
	{
		base.Reset();
		this.poison.Reset();
		this.radiation_level.Reset();
		this.radiation_poison.Reset();
		this.temperature.Reset();
		this.oxygen.Reset();
		this.bleeding.Reset();
		this.wetness.Reset();
		this.dirtyness.Reset();
		this.comfort.Reset();
		this.pending_health.Reset();
		this.lastConsumeTime = float.NegativeInfinity;
		this.isDirty = true;
	}

	// Token: 0x060008F7 RID: 2295 RVA: 0x0003BC68 File Offset: 0x00039E68
	public override void ServerUpdate(global::BaseCombatEntity ownerEntity, float delta)
	{
		base.ServerUpdate(ownerEntity, delta);
		this.SendChangesToClient();
	}

	// Token: 0x060008F8 RID: 2296 RVA: 0x0003BC78 File Offset: 0x00039E78
	internal bool HasChanged()
	{
		bool flag = this.isDirty;
		flag = (this.calories.HasChanged() || flag);
		flag = (this.hydration.HasChanged() || flag);
		flag = (this.heartrate.HasChanged() || flag);
		flag = (this.poison.HasChanged() || flag);
		flag = (this.radiation_level.HasChanged() || flag);
		flag = (this.radiation_poison.HasChanged() || flag);
		flag = (this.temperature.HasChanged() || flag);
		flag = (this.wetness.HasChanged() || flag);
		flag = (this.dirtyness.HasChanged() || flag);
		flag = (this.comfort.HasChanged() || flag);
		return this.pending_health.HasChanged() || flag;
	}

	// Token: 0x060008F9 RID: 2297 RVA: 0x0003BD74 File Offset: 0x00039F74
	protected override void DoMetabolismDamage(global::BaseCombatEntity ownerEntity, float delta)
	{
		base.DoMetabolismDamage(ownerEntity, delta);
		if (this.temperature.value < -20f)
		{
			this.owner.Hurt(Mathf.InverseLerp(1f, -50f, this.temperature.value) * delta * 1f, Rust.DamageType.Cold, null, true);
		}
		else if (this.temperature.value < -10f)
		{
			this.owner.Hurt(Mathf.InverseLerp(1f, -50f, this.temperature.value) * delta * 0.3f, Rust.DamageType.Cold, null, true);
		}
		else if (this.temperature.value < 1f)
		{
			this.owner.Hurt(Mathf.InverseLerp(1f, -50f, this.temperature.value) * delta * 0.1f, Rust.DamageType.Cold, null, true);
		}
		if (this.temperature.value > 60f)
		{
			this.owner.Hurt(Mathf.InverseLerp(60f, 200f, this.temperature.value) * delta * 5f, Rust.DamageType.Heat, null, true);
		}
		if (this.oxygen.value < 0.5f)
		{
			this.owner.Hurt(Mathf.InverseLerp(0.5f, 0f, this.oxygen.value) * delta * 2f, Rust.DamageType.Drowned, null, true);
		}
		if (this.bleeding.value > 0f)
		{
			float num = delta * 0.333333343f;
			this.owner.Hurt(num, Rust.DamageType.Bleeding, null, true);
			this.bleeding.Subtract(num);
		}
		if (this.poison.value > 0f)
		{
			this.owner.Hurt(this.poison.value * delta * 0.1f, Rust.DamageType.Poison, null, true);
		}
		if (ConVar.Server.radiation && this.radiation_poison.value > 0f)
		{
			float num2 = (1f + Mathf.Clamp01(this.radiation_poison.value / 25f) * 5f) * (delta / 5f);
			this.owner.Hurt(num2, Rust.DamageType.Radiation, null, true);
			this.radiation_poison.Subtract(num2);
		}
	}

	// Token: 0x060008FA RID: 2298 RVA: 0x0003BFC0 File Offset: 0x0003A1C0
	public bool SignificantBleeding()
	{
		return this.bleeding.value > 0f;
	}

	// Token: 0x060008FB RID: 2299 RVA: 0x0003BFD4 File Offset: 0x0003A1D4
	protected override void RunMetabolism(global::BaseCombatEntity ownerEntity, float delta)
	{
		if (Interface.CallHook("OnRunPlayerMetabolism", new object[]
		{
			this,
			ownerEntity,
			delta
		}) != null)
		{
			return;
		}
		float currentTemperature = this.owner.currentTemperature;
		float fTarget = this.owner.currentComfort;
		float currentCraftLevel = this.owner.currentCraftLevel;
		this.owner.SetPlayerFlag(global::BasePlayer.PlayerFlags.Workbench1, currentCraftLevel == 1f);
		this.owner.SetPlayerFlag(global::BasePlayer.PlayerFlags.Workbench2, currentCraftLevel == 2f);
		this.owner.SetPlayerFlag(global::BasePlayer.PlayerFlags.Workbench3, currentCraftLevel == 3f);
		float num = currentTemperature;
		num -= this.DeltaWet() * 34f;
		float num2 = Mathf.Clamp(this.owner.baseProtection.amounts[18] * 1.5f, -1f, 1f);
		float num3 = num2;
		float num4 = Mathf.InverseLerp(20f, -50f, currentTemperature);
		float num5 = Mathf.InverseLerp(20f, 30f, currentTemperature);
		num += num4 * 70f * num3;
		num += num5 * 10f * Mathf.Abs(num3);
		num += this.heartrate.value * 5f;
		this.temperature.MoveTowards(num, delta * 5f);
		if (this.temperature.value >= 40f)
		{
			fTarget = 0f;
		}
		this.comfort.MoveTowards(fTarget, delta / 5f);
		float num6 = 0.6f + 0.4f * this.comfort.value;
		bool flag = this.calories.value > 100f && this.owner.healthFraction < num6 && this.radiation_poison.Fraction() < 0.25f && this.owner.SecondsSinceAttacked > 10f && !this.SignificantBleeding() && this.temperature.value >= 10f && this.hydration.value > 40f;
		if (flag)
		{
			float num7 = Mathf.InverseLerp(this.calories.min, this.calories.max, this.calories.value);
			float num8 = 5f;
			float num9 = num8 * this.owner.MaxHealth() * 0.8f / 600f;
			num9 += num9 * num7 * 0.5f;
			float num10 = num9 / num8;
			num10 += num10 * this.comfort.value * 6f;
			ownerEntity.Heal(num10 * delta);
			this.calories.Subtract(num9 * delta);
			this.hydration.Subtract(num9 * delta * 0.2f);
		}
		float num11 = this.owner.estimatedSpeed2D / this.owner.GetMaxSpeed() * 0.75f;
		float fTarget2 = Mathf.Clamp(0.05f + num11, 0f, 1f);
		this.heartrate.MoveTowards(fTarget2, delta * 0.1f);
		float num12 = this.heartrate.Fraction() * 0.375f;
		this.calories.MoveTowards(0f, delta * num12);
		float num13 = 0.008333334f;
		num13 += Mathf.InverseLerp(40f, 60f, this.temperature.value) * 0.0833333358f;
		num13 += this.heartrate.value * 0.06666667f;
		this.hydration.MoveTowards(0f, delta * num13);
		bool b = this.hydration.Fraction() <= 0f || this.radiation_poison.value >= 100f;
		this.owner.SetPlayerFlag(global::BasePlayer.PlayerFlags.NoSprint, b);
		if (this.temperature.value > 40f)
		{
			this.hydration.Add(Mathf.InverseLerp(40f, 200f, this.temperature.value) * delta * -1f);
		}
		if (this.temperature.value < 10f)
		{
			float num14 = Mathf.InverseLerp(20f, -100f, this.temperature.value);
			this.heartrate.MoveTowards(Mathf.Lerp(0.2f, 1f, num14), delta * 2f * num14);
		}
		float num15 = this.owner.WaterFactor();
		if (num15 > 0.85f)
		{
			this.oxygen.MoveTowards(0f, delta * 0.1f);
		}
		else
		{
			this.oxygen.MoveTowards(1f, delta * 1f);
		}
		float num16 = 0f;
		float num17 = 0f;
		if (this.owner.IsOutside(this.owner.eyes.position))
		{
			num16 = global::Climate.GetRain(this.owner.eyes.position) * 0.6f;
			num17 = global::Climate.GetSnow(this.owner.eyes.position) * 0.2f;
		}
		this.wetness.value = Mathf.Max(this.wetness.value, num15);
		this.wetness.MoveTowards(Mathx.Max(this.wetness.value, num16, num17), delta * 0.05f);
		if (num15 < this.wetness.value)
		{
			this.wetness.MoveTowards(0f, delta * 0.2f * Mathf.InverseLerp(0f, 100f, currentTemperature));
		}
		this.poison.MoveTowards(0f, delta * 0.5555556f);
		if (this.wetness.Fraction() > 0.4f && this.owner.estimatedSpeed > 0.25f && this.radiation_level.Fraction() == 0f)
		{
			this.radiation_poison.Subtract(this.radiation_poison.value * 0.2f * this.wetness.Fraction() * delta * 0.2f);
		}
		if (ConVar.Server.radiation)
		{
			this.radiation_level.value = this.owner.radiationLevel;
			if (this.radiation_level.value > 0f)
			{
				this.radiation_poison.Add(this.radiation_level.value * delta);
			}
		}
		if (this.pending_health.value > 0f)
		{
			float num18 = Mathf.Min(2f * delta, this.pending_health.value);
			ownerEntity.Heal(num18);
			if (ownerEntity.healthFraction == 1f)
			{
				this.pending_health.value = 0f;
			}
			else
			{
				this.pending_health.Subtract(num18);
			}
		}
	}

	// Token: 0x060008FC RID: 2300 RVA: 0x0003C6B4 File Offset: 0x0003A8B4
	private float DeltaHot()
	{
		return Mathf.InverseLerp(20f, 100f, this.temperature.value);
	}

	// Token: 0x060008FD RID: 2301 RVA: 0x0003C6D0 File Offset: 0x0003A8D0
	private float DeltaCold()
	{
		return Mathf.InverseLerp(20f, -50f, this.temperature.value);
	}

	// Token: 0x060008FE RID: 2302 RVA: 0x0003C6EC File Offset: 0x0003A8EC
	private float DeltaWet()
	{
		return this.wetness.value;
	}

	// Token: 0x060008FF RID: 2303 RVA: 0x0003C6FC File Offset: 0x0003A8FC
	public void UseHeart(float frate)
	{
		if (this.heartrate.value > frate)
		{
			this.heartrate.Add(frate);
			return;
		}
		this.heartrate.value = frate;
	}

	// Token: 0x06000900 RID: 2304 RVA: 0x0003C728 File Offset: 0x0003A928
	public void SendChangesToClient()
	{
		if (!this.HasChanged())
		{
			return;
		}
		this.isDirty = false;
		using (ProtoBuf.PlayerMetabolism playerMetabolism = this.Save())
		{
			base.baseEntity.ClientRPCPlayer<ProtoBuf.PlayerMetabolism>(null, base.baseEntity, "UpdateMetabolism", playerMetabolism);
		}
	}

	// Token: 0x06000901 RID: 2305 RVA: 0x0003C78C File Offset: 0x0003A98C
	public bool CanConsume()
	{
		return (!this.owner || !this.owner.IsHeadUnderwater()) && UnityEngine.Time.time - this.lastConsumeTime > 1f;
	}

	// Token: 0x06000902 RID: 2306 RVA: 0x0003C7C4 File Offset: 0x0003A9C4
	public void MarkConsumption()
	{
		this.lastConsumeTime = UnityEngine.Time.time;
	}

	// Token: 0x06000903 RID: 2307 RVA: 0x0003C7D4 File Offset: 0x0003A9D4
	public ProtoBuf.PlayerMetabolism Save()
	{
		ProtoBuf.PlayerMetabolism playerMetabolism = Facepunch.Pool.Get<ProtoBuf.PlayerMetabolism>();
		playerMetabolism.calories = this.calories.value;
		playerMetabolism.hydration = this.hydration.value;
		playerMetabolism.heartrate = this.heartrate.value;
		playerMetabolism.temperature = this.temperature.value;
		playerMetabolism.radiation_level = this.radiation_level.value;
		playerMetabolism.radiation_poisoning = this.radiation_poison.value;
		playerMetabolism.wetness = this.wetness.value;
		playerMetabolism.dirtyness = this.dirtyness.value;
		playerMetabolism.oxygen = this.oxygen.value;
		playerMetabolism.bleeding = this.bleeding.value;
		playerMetabolism.comfort = this.comfort.value;
		playerMetabolism.pending_health = this.pending_health.value;
		if (this.owner)
		{
			playerMetabolism.health = this.owner.Health();
		}
		return playerMetabolism;
	}

	// Token: 0x06000904 RID: 2308 RVA: 0x0003C8D8 File Offset: 0x0003AAD8
	public void Load(ProtoBuf.PlayerMetabolism s)
	{
		this.calories.SetValue(s.calories);
		this.hydration.SetValue(s.hydration);
		this.comfort.SetValue(s.comfort);
		this.heartrate.value = s.heartrate;
		this.temperature.value = s.temperature;
		this.radiation_level.value = s.radiation_level;
		this.radiation_poison.value = s.radiation_poisoning;
		this.wetness.value = s.wetness;
		this.dirtyness.value = s.dirtyness;
		this.oxygen.value = s.oxygen;
		this.bleeding.value = s.bleeding;
		this.pending_health.value = s.pending_health;
		if (this.owner)
		{
			this.owner.health = s.health;
		}
	}

	// Token: 0x06000905 RID: 2309 RVA: 0x0003C9D4 File Offset: 0x0003ABD4
	public override global::MetabolismAttribute FindAttribute(global::MetabolismAttribute.Type type)
	{
		switch (type)
		{
		case global::MetabolismAttribute.Type.Poison:
			return this.poison;
		case global::MetabolismAttribute.Type.Radiation:
			return this.radiation_poison;
		case global::MetabolismAttribute.Type.Bleeding:
			return this.bleeding;
		case global::MetabolismAttribute.Type.HealthOverTime:
			return this.pending_health;
		}
		return base.FindAttribute(type);
	}

	// Token: 0x0400041D RID: 1053
	public const float HotThreshold = 40f;

	// Token: 0x0400041E RID: 1054
	public const float ColdThreshold = 5f;

	// Token: 0x0400041F RID: 1055
	public global::MetabolismAttribute temperature = new global::MetabolismAttribute();

	// Token: 0x04000420 RID: 1056
	public global::MetabolismAttribute poison = new global::MetabolismAttribute();

	// Token: 0x04000421 RID: 1057
	public global::MetabolismAttribute radiation_level = new global::MetabolismAttribute();

	// Token: 0x04000422 RID: 1058
	public global::MetabolismAttribute radiation_poison = new global::MetabolismAttribute();

	// Token: 0x04000423 RID: 1059
	public global::MetabolismAttribute wetness = new global::MetabolismAttribute();

	// Token: 0x04000424 RID: 1060
	public global::MetabolismAttribute dirtyness = new global::MetabolismAttribute();

	// Token: 0x04000425 RID: 1061
	public global::MetabolismAttribute oxygen = new global::MetabolismAttribute();

	// Token: 0x04000426 RID: 1062
	public global::MetabolismAttribute bleeding = new global::MetabolismAttribute();

	// Token: 0x04000427 RID: 1063
	public global::MetabolismAttribute comfort = new global::MetabolismAttribute();

	// Token: 0x04000428 RID: 1064
	public global::MetabolismAttribute pending_health = new global::MetabolismAttribute();

	// Token: 0x04000429 RID: 1065
	public bool isDirty;

	// Token: 0x0400042A RID: 1066
	private float lastConsumeTime;
}
