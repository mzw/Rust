﻿using System;
using UnityEngine;

// Token: 0x02000273 RID: 627
public class PlayAudioEx : MonoBehaviour
{
	// Token: 0x060010A5 RID: 4261 RVA: 0x00064408 File Offset: 0x00062608
	private void Start()
	{
	}

	// Token: 0x060010A6 RID: 4262 RVA: 0x0006440C File Offset: 0x0006260C
	private void OnEnable()
	{
		AudioSource component = base.GetComponent<AudioSource>();
		if (component)
		{
			component.PlayDelayed(this.delay);
		}
	}

	// Token: 0x04000B96 RID: 2966
	public float delay;
}
