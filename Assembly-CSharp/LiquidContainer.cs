﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000077 RID: 119
public class LiquidContainer : global::StorageContainer, global::ISplashable
{
	// Token: 0x0600083B RID: 2107 RVA: 0x00035748 File Offset: 0x00033948
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("LiquidContainer.OnRpcMessage", 0.1f))
		{
			if (rpc == 2457800725u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SVDrink ");
				}
				using (TimeWarning.New("SVDrink", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SVDrink", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SVDrink(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in SVDrink");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600083C RID: 2108 RVA: 0x00035928 File Offset: 0x00033B28
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.startingAmount > 0)
		{
			this.inventory.AddItem(this.defaultLiquid, this.startingAmount);
		}
	}

	// Token: 0x0600083D RID: 2109 RVA: 0x00035954 File Offset: 0x00033B54
	protected void UpdateOnFlag()
	{
		base.SetFlag(global::BaseEntity.Flags.On, this.inventory.itemList.Count > 0 && this.inventory.itemList[0].amount > 0, false);
	}

	// Token: 0x0600083E RID: 2110 RVA: 0x00035990 File Offset: 0x00033B90
	protected override void OnInventoryDirty()
	{
		this.UpdateOnFlag();
	}

	// Token: 0x0600083F RID: 2111 RVA: 0x00035998 File Offset: 0x00033B98
	public virtual void OpenTap(float duration)
	{
		if (base.HasFlag(global::BaseEntity.Flags.Reserved5))
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved5, true, false);
		base.Invoke(new Action(this.ShutTap), duration);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000840 RID: 2112 RVA: 0x000359D4 File Offset: 0x00033BD4
	public virtual void ShutTap()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved5, false, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000841 RID: 2113 RVA: 0x000359EC File Offset: 0x00033BEC
	public bool HasLiquidItem()
	{
		return this.GetLiquidItem() != null;
	}

	// Token: 0x06000842 RID: 2114 RVA: 0x000359FC File Offset: 0x00033BFC
	public global::Item GetLiquidItem()
	{
		return (this.inventory.itemList.Count != 0) ? this.inventory.itemList[0] : null;
	}

	// Token: 0x06000843 RID: 2115 RVA: 0x00035A2C File Offset: 0x00033C2C
	public bool wantsSplash(global::ItemDefinition splashType, int amount)
	{
		return !this.HasLiquidItem() || (this.GetLiquidItem().info == splashType && this.GetLiquidItem().amount < this.maxStackSize);
	}

	// Token: 0x06000844 RID: 2116 RVA: 0x00035A68 File Offset: 0x00033C68
	public int DoSplash(global::ItemDefinition splashType, int amount)
	{
		int num;
		if (this.HasLiquidItem())
		{
			global::Item item = this.GetLiquidItem();
			int amount2 = item.amount;
			global::ItemDefinition itemDefinition = global::WaterResource.Merge(splashType, item.info);
			if (item.info != itemDefinition)
			{
				item.Remove(0f);
				item = global::ItemManager.Create(itemDefinition, amount2, 0UL);
				if (!item.MoveToContainer(this.inventory, -1, true))
				{
					item.Remove(0f);
					return 0;
				}
			}
			num = Mathf.Min(this.maxStackSize - amount2, amount);
			item.amount += num;
		}
		else
		{
			num = Mathf.Min(amount, this.maxStackSize);
			global::Item item2 = global::ItemManager.Create(splashType, num, 0UL);
			if (item2 != null && !item2.MoveToContainer(this.inventory, -1, true))
			{
				item2.Remove(0f);
			}
		}
		return num;
	}

	// Token: 0x06000845 RID: 2117 RVA: 0x00035B48 File Offset: 0x00033D48
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void SVDrink(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.metabolism.CanConsume())
		{
			return;
		}
		foreach (global::Item item in this.inventory.itemList)
		{
			global::ItemModConsume component = item.info.GetComponent<global::ItemModConsume>();
			if (!(component == null))
			{
				if (component.CanDoAction(item, rpc.player))
				{
					component.DoAction(item, rpc.player);
					break;
				}
			}
		}
	}

	// Token: 0x06000846 RID: 2118 RVA: 0x00035C04 File Offset: 0x00033E04
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x040003DF RID: 991
	public global::ItemDefinition defaultLiquid;

	// Token: 0x040003E0 RID: 992
	public int startingAmount;
}
