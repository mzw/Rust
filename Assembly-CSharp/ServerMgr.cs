﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using ConVar;
using Facepunch;
using Facepunch.Math;
using Facepunch.Steamworks;
using Ionic.Crc;
using Network;
using Network.Visibility;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000622 RID: 1570
public class ServerMgr : SingletonComponent<global::ServerMgr>
{
	// Token: 0x06001FCD RID: 8141 RVA: 0x000B47D0 File Offset: 0x000B29D0
	public void Initialize(bool loadSave = true, string saveFile = "", bool allowOutOfDateSaves = false, bool skipInitialSpawn = false)
	{
		if (!ConVar.Server.official)
		{
			ExceptionReporter.Disabled = true;
		}
		this.persistance = new global::UserPersistance(ConVar.Server.rootFolder);
		this.SpawnMapEntities();
		if (SingletonComponent<global::SpawnHandler>.Instance)
		{
			using (TimeWarning.New("SpawnHandler.UpdateDistributions", 0.1f))
			{
				SingletonComponent<global::SpawnHandler>.Instance.UpdateDistributions();
			}
		}
		if (loadSave)
		{
			skipInitialSpawn = global::SaveRestore.Load(saveFile, allowOutOfDateSaves);
		}
		if (SingletonComponent<global::SpawnHandler>.Instance)
		{
			if (!skipInitialSpawn)
			{
				using (TimeWarning.New("SpawnHandler.InitialSpawn", 200L))
				{
					SingletonComponent<global::SpawnHandler>.Instance.InitialSpawn();
				}
			}
			using (TimeWarning.New("SpawnHandler.StartSpawnTick", 200L))
			{
				SingletonComponent<global::SpawnHandler>.Instance.StartSpawnTick();
			}
		}
		this.CreateImportantEntities();
		this.auth = base.GetComponent<global::ConnectionAuth>();
	}

	// Token: 0x06001FCE RID: 8142 RVA: 0x000B48F8 File Offset: 0x000B2AF8
	public void OpenConnection()
	{
		this.useQueryPort = (ConVar.Server.queryport > 0 && ConVar.Server.queryport != ConVar.Server.port);
		Network.Net.sv.ip = ConVar.Server.ip;
		Network.Net.sv.port = ConVar.Server.port;
		if (!Network.Net.sv.Start())
		{
			Debug.LogWarning("Couldn't Start Server.");
			return;
		}
		this.StartSteamServer();
		Network.Net.sv.onMessage = new Action<Message>(this.OnNetworkMessage);
		Network.Net.sv.onDisconnected = new Action<string, Connection>(this.OnDisconnected);
		if (!this.useQueryPort)
		{
			Network.Net.sv.onUnconnectedMessage = new Func<int, Read, uint, int, bool>(this.OnUnconnectedMessage);
		}
		Network.Net.sv.cryptography = new global::NetworkCryptographyServer();
		global::EACServer.DoStartup();
		base.InvokeRepeating("EACUpdate", 1f, 1f);
		base.InvokeRepeating("DoTick", 1f, 1f / (float)ConVar.Server.tickrate);
		base.InvokeRepeating("DoHeartbeat", 1f, 1f);
		this.runFrameUpdate = true;
		Interface.CallHook("OnServerInitialized", null);
	}

	// Token: 0x06001FCF RID: 8143 RVA: 0x000B4A20 File Offset: 0x000B2C20
	private void CloseConnection()
	{
		if (this.persistance != null)
		{
			this.persistance.Dispose();
			this.persistance = null;
		}
		global::EACServer.DoShutdown();
		Network.Server sv = Network.Net.sv;
		sv.onDisconnected = (Action<string, Connection>)Delegate.Remove(sv.onDisconnected, new Action<string, Connection>(this.OnDisconnected));
		using (TimeWarning.New("sv.Stop", 0.1f))
		{
			Network.Net.sv.Stop("Shutting Down");
		}
		using (TimeWarning.New("RCon.Shutdown", 0.1f))
		{
			Facepunch.RCon.Shutdown();
		}
		using (TimeWarning.New("Steamworks.GameServer.Shutdown", 0.1f))
		{
			if (Rust.Global.SteamServer != null)
			{
				Debug.Log("Steamworks Shutting Down");
				Rust.Global.SteamServer.Dispose();
				Rust.Global.SteamServer = null;
				Debug.Log("Okay");
			}
		}
	}

	// Token: 0x06001FD0 RID: 8144 RVA: 0x000B4B48 File Offset: 0x000B2D48
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.CloseConnection();
	}

	// Token: 0x06001FD1 RID: 8145 RVA: 0x000B4B5C File Offset: 0x000B2D5C
	private void OnApplicationQuit()
	{
		Application.isQuitting = true;
		this.CloseConnection();
	}

	// Token: 0x06001FD2 RID: 8146 RVA: 0x000B4B6C File Offset: 0x000B2D6C
	private void CreateImportantEntities()
	{
		this.CreateImportantEntity<global::EnvSync>("assets/bundled/prefabs/system/net_env.prefab");
		this.CreateImportantEntity<global::CommunityEntity>("assets/bundled/prefabs/system/server/community.prefab");
		this.CreateImportantEntity<global::ResourceDepositManager>("assets/bundled/prefabs/system/server/resourcedepositmanager.prefab");
	}

	// Token: 0x06001FD3 RID: 8147 RVA: 0x000B4B90 File Offset: 0x000B2D90
	private void CreateImportantEntity<T>(string prefabName) where T : global::BaseEntity
	{
		bool flag = global::BaseNetworkable.serverEntities.Any((global::BaseNetworkable x) => x is T);
		if (flag)
		{
			return;
		}
		Debug.LogWarning("Missing " + typeof(T).Name + " - creating");
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(prefabName, default(Vector3), default(Quaternion), true);
		if (baseEntity == null)
		{
			Debug.LogWarning("Couldn't create");
			return;
		}
		baseEntity.Spawn();
	}

	// Token: 0x06001FD4 RID: 8148 RVA: 0x000B4C1C File Offset: 0x000B2E1C
	private void StartSteamServer()
	{
		if (Rust.Global.SteamServer != null)
		{
			return;
		}
		IPAddress ipaddress = null;
		if (!string.IsNullOrEmpty(ConVar.Server.ip))
		{
			ipaddress = IPAddress.Parse(ConVar.Server.ip);
		}
		Config.ForUnity(Application.platform.ToString());
		ServerInit serverInit = new ServerInit("rust", "Rust");
		serverInit.IpAddress = ipaddress;
		serverInit.GamePort = (ushort)Network.Net.sv.port;
		serverInit.Secure = ConVar.Server.secure;
		serverInit.VersionString = 2065.ToString();
		if (this.useQueryPort)
		{
			serverInit.QueryPort = (ushort)ConVar.Server.queryport;
		}
		else
		{
			serverInit.QueryShareGamePort();
		}
		Rust.Global.SteamServer = new Facepunch.Steamworks.Server(Rust.Defines.appID, serverInit);
		if (!Rust.Global.SteamServer.IsValid)
		{
			Debug.LogWarning("Couldn't initialize Steam Server (" + ipaddress + ")");
			Rust.Global.SteamServer.Dispose();
			Rust.Global.SteamServer = null;
			Application.Quit();
			return;
		}
		Rust.Global.SteamServer.Auth.OnAuthChange = new Action<ulong, ulong, ServerAuth.Status>(this.OnValidateAuthTicketResponse);
		Rust.Global.SteamServer.LogOnAnonymous();
		base.InvokeRepeating("UpdateServerInformation", 1f, 10f);
		DebugEx.Log("Connected to Steam", 0);
	}

	// Token: 0x06001FD5 RID: 8149 RVA: 0x000B4D68 File Offset: 0x000B2F68
	private void OnValidateAuthTicketResponse(ulong SteamId, ulong OwnerId, ServerAuth.Status Status)
	{
		if (global::Auth_Steam.ValidateConnecting(SteamId, OwnerId, Status))
		{
			return;
		}
		Connection connection = Network.Net.sv.connections.FirstOrDefault((Connection x) => x.userid == SteamId);
		if (connection == null)
		{
			Debug.LogWarning(string.Concat(new object[]
			{
				"Steam gave us a ",
				Status,
				" ticket response for unconnected id ",
				SteamId
			}));
			return;
		}
		if (Status == null)
		{
			Debug.LogWarning("Steam gave us a 'ok' ticket response for already connected id " + SteamId);
			return;
		}
		if (Status == 5)
		{
			return;
		}
		connection.authStatus = Status.ToString();
		Network.Net.sv.Kick(connection, "Steam: " + Status.ToString());
	}

	// Token: 0x06001FD6 RID: 8150 RVA: 0x000B4E50 File Offset: 0x000B3050
	private void EACUpdate()
	{
		global::EACServer.DoUpdate();
	}

	// Token: 0x17000237 RID: 567
	// (get) Token: 0x06001FD7 RID: 8151 RVA: 0x000B4E58 File Offset: 0x000B3058
	public static int AvailableSlots
	{
		get
		{
			return ConVar.Server.maxplayers - global::BasePlayer.activePlayerList.Count<global::BasePlayer>();
		}
	}

	// Token: 0x06001FD8 RID: 8152 RVA: 0x000B4E6C File Offset: 0x000B306C
	private void Update()
	{
		if (!this.runFrameUpdate)
		{
			return;
		}
		using (TimeWarning.New("ServerMgr.Update", 500L))
		{
			try
			{
				using (TimeWarning.New("Net.sv.Cycle", 100L))
				{
					Network.Net.sv.Cycle();
				}
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Server Network Exception");
				Debug.LogException(ex, this);
			}
			using (TimeWarning.New("ServerBuildingManager.Cycle", 0.1f))
			{
				global::BuildingManager.server.Cycle();
			}
			using (TimeWarning.New("BasePlayer.ServerCycle", 0.1f))
			{
				global::BasePlayer.ServerCycle(UnityEngine.Time.deltaTime);
			}
			using (TimeWarning.New("SteamQueryResponse", 0.1f))
			{
				this.SteamQueryResponse();
			}
			using (TimeWarning.New("connectionQueue.Cycle", 0.1f))
			{
				this.connectionQueue.Cycle(global::ServerMgr.AvailableSlots);
			}
		}
	}

	// Token: 0x06001FD9 RID: 8153 RVA: 0x000B5058 File Offset: 0x000B3258
	private void SteamQueryResponse()
	{
		if (Rust.Global.SteamServer == null)
		{
			return;
		}
		using (TimeWarning.New("SteamGameServer.GetNextOutgoingPacket", 0.1f))
		{
			ServerQuery.Packet packet;
			while (Rust.Global.SteamServer.Query.GetOutgoingPacket(ref packet))
			{
				Network.Net.sv.SendUnconnected(packet.Address, packet.Port, packet.Data, packet.Size);
			}
		}
	}

	// Token: 0x06001FDA RID: 8154 RVA: 0x000B50E4 File Offset: 0x000B32E4
	private void DoTick()
	{
		if (Rust.Global.SteamServer != null)
		{
			Interface.CallHook("OnTick", null);
			Rust.Global.SteamServer.Update();
		}
		Facepunch.RCon.Update();
		for (int i = 0; i < Network.Net.sv.connections.Count; i++)
		{
			Connection connection = Network.Net.sv.connections[i];
			if (!connection.isAuthenticated)
			{
				if (connection.GetSecondsConnected() >= (float)ConVar.Server.authtimeout)
				{
					Network.Net.sv.Kick(connection, "Authentication Timed Out");
				}
			}
		}
	}

	// Token: 0x06001FDB RID: 8155 RVA: 0x000B5180 File Offset: 0x000B3380
	private void DoHeartbeat()
	{
		global::ItemManager.Heartbeat();
	}

	// Token: 0x17000238 RID: 568
	// (get) Token: 0x06001FDC RID: 8156 RVA: 0x000B5188 File Offset: 0x000B3388
	private string AssemblyHash
	{
		get
		{
			if (this._AssemblyHash == null)
			{
				Assembly assembly = typeof(global::ServerMgr).Assembly;
				byte[] array = File.ReadAllBytes(assembly.Location);
				CRC32 crc = new CRC32();
				crc.SlurpBlock(array, 0, array.Length);
				this._AssemblyHash = crc.Crc32Result.ToString("x");
			}
			return this._AssemblyHash;
		}
	}

	// Token: 0x06001FDD RID: 8157 RVA: 0x000B51EC File Offset: 0x000B33EC
	private void UpdateServerInformation()
	{
		if (Rust.Global.SteamServer == null)
		{
			return;
		}
		using (TimeWarning.New("UpdateServerInformation", 0.1f))
		{
			Rust.Global.SteamServer.ServerName = ConVar.Server.hostname;
			Rust.Global.SteamServer.MaxPlayers = ConVar.Server.maxplayers;
			Rust.Global.SteamServer.Passworded = false;
			Rust.Global.SteamServer.MapName = Application.loadedLevelName;
			string text = "stok";
			if (this.Restarting)
			{
				text = "strst";
			}
			string text2 = string.Format("born{0}", Epoch.FromDateTime(global::SaveRestore.SaveCreatedTime));
			string text3 = string.Format("mp{0},cp{1},qp{5},v{2}{3},h{4},{6},{7}", new object[]
			{
				ConVar.Server.maxplayers,
				global::BasePlayer.activePlayerList.Count,
				2065,
				(!ConVar.Server.pve) ? string.Empty : ",pve",
				this.AssemblyHash,
				SingletonComponent<global::ServerMgr>.Instance.connectionQueue.Queued,
				text,
				text2
			});
			if (Interface.Oxide.Config.Options.Modded)
			{
				text3 += ",modded,oxide";
			}
			Rust.Global.SteamServer.GameTags = text3;
			if (ConVar.Server.description != null && ConVar.Server.description.Length > 100)
			{
				string[] array = StringEx.SplitToChunks(ConVar.Server.description, 100).ToArray<string>();
				for (int i = 0; i < 16; i++)
				{
					if (i < array.Length)
					{
						Rust.Global.SteamServer.SetKey(string.Format("description_{0:00}", i), array[i]);
					}
					else
					{
						Rust.Global.SteamServer.SetKey(string.Format("description_{0:00}", i), string.Empty);
					}
				}
			}
			else
			{
				Rust.Global.SteamServer.SetKey("description_0", ConVar.Server.description);
				for (int j = 1; j < 16; j++)
				{
					Rust.Global.SteamServer.SetKey(string.Format("description_{0:00}", j), string.Empty);
				}
			}
			Rust.Global.SteamServer.SetKey("hash", this.AssemblyHash);
			Rust.Global.SteamServer.SetKey("world.seed", global::World.Seed.ToString());
			Rust.Global.SteamServer.SetKey("world.size", global::World.Size.ToString());
			Rust.Global.SteamServer.SetKey("pve", ConVar.Server.pve.ToString());
			Rust.Global.SteamServer.SetKey("headerimage", ConVar.Server.headerimage);
			Rust.Global.SteamServer.SetKey("url", ConVar.Server.url);
			Rust.Global.SteamServer.SetKey("uptime", ((int)UnityEngine.Time.realtimeSinceStartup).ToString());
			Rust.Global.SteamServer.SetKey("gc_mb", global::Performance.report.memoryAllocations.ToString());
			Rust.Global.SteamServer.SetKey("gc_cl", global::Performance.report.memoryCollections.ToString());
			Rust.Global.SteamServer.SetKey("fps", global::Performance.report.frameRate.ToString());
			Rust.Global.SteamServer.SetKey("fps_avg", global::Performance.report.frameRateAverage.ToString("0.00"));
			Rust.Global.SteamServer.SetKey("ent_cnt", global::BaseNetworkable.serverEntities.Count.ToString());
			Rust.Global.SteamServer.SetKey("build", BuildInfo.Current.Scm.ChangeId);
		}
	}

	// Token: 0x06001FDE RID: 8158 RVA: 0x000B55DC File Offset: 0x000B37DC
	private void OnDisconnected(string strReason, Connection connection)
	{
		this.connectionQueue.RemoveConnection(connection);
		global::ConnectionAuth.OnDisconnect(connection);
		Rust.Global.SteamServer.Auth.EndSession(connection.userid);
		global::EACServer.OnLeaveGame(connection);
		global::BasePlayer basePlayer = connection.player as global::BasePlayer;
		if (basePlayer)
		{
			Interface.CallHook("OnPlayerDisconnected", new object[]
			{
				basePlayer,
				strReason
			});
			basePlayer.OnDisconnected();
		}
	}

	// Token: 0x06001FDF RID: 8159 RVA: 0x000B5650 File Offset: 0x000B3850
	public static void OnEnterVisibility(Connection connection, Group group)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(19);
			Network.Net.sv.write.GroupID(group.ID);
			Network.Net.sv.write.Send(new SendInfo(connection));
		}
	}

	// Token: 0x06001FE0 RID: 8160 RVA: 0x000B56BC File Offset: 0x000B38BC
	public static void OnLeaveVisibility(Connection connection, Group group)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(20);
			Network.Net.sv.write.GroupID(group.ID);
			Network.Net.sv.write.Send(new SendInfo(connection));
		}
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(8);
			Network.Net.sv.write.GroupID(group.ID);
			Network.Net.sv.write.Send(new SendInfo(connection));
		}
	}

	// Token: 0x06001FE1 RID: 8161 RVA: 0x000B5778 File Offset: 0x000B3978
	internal void SpawnMapEntities()
	{
		global::PrefabPreProcess prefabPreProcess = new global::PrefabPreProcess(false, true);
		global::BaseEntity[] array = Object.FindObjectsOfType<global::BaseEntity>();
		foreach (global::BaseEntity baseEntity in array)
		{
			if (prefabPreProcess.NeedsProcessing(baseEntity.gameObject))
			{
				prefabPreProcess.ProcessObject(null, baseEntity.gameObject, false, false);
			}
			baseEntity.transform.parent = null;
			SceneManager.MoveGameObjectToScene(baseEntity.gameObject, Rust.Server.EntityScene);
			baseEntity.gameObject.SetActive(true);
			baseEntity.SpawnAsMapEntity();
		}
		DebugEx.Log("Map Spawned " + array.Length + " entities", 0);
	}

	// Token: 0x06001FE2 RID: 8162 RVA: 0x000B5820 File Offset: 0x000B3A20
	public static global::BasePlayer.SpawnPoint FindSpawnPoint()
	{
		if (SingletonComponent<global::SpawnHandler>.Instance != null)
		{
			global::BasePlayer.SpawnPoint spawnPoint = global::SpawnHandler.GetSpawnPoint();
			if (spawnPoint != null)
			{
				return spawnPoint;
			}
		}
		global::BasePlayer.SpawnPoint spawnPoint2 = new global::BasePlayer.SpawnPoint();
		GameObject[] array = GameObject.FindGameObjectsWithTag("spawnpoint");
		if (array.Length > 0)
		{
			GameObject gameObject = array[Random.Range(0, array.Length)];
			spawnPoint2.pos = gameObject.transform.position;
			spawnPoint2.rot = gameObject.transform.rotation;
		}
		else
		{
			Debug.Log("Couldn't find an appropriate spawnpoint for the player - so spawning at camera");
			if (global::MainCamera.mainCamera != null)
			{
				spawnPoint2.pos = global::MainCamera.mainCamera.transform.position;
				spawnPoint2.rot = global::MainCamera.mainCamera.transform.rotation;
			}
		}
		RaycastHit raycastHit;
		if (UnityEngine.Physics.Raycast(new Ray(spawnPoint2.pos, Vector3.down), ref raycastHit, 32f, 1403068673))
		{
			spawnPoint2.pos = raycastHit.point;
		}
		return spawnPoint2;
	}

	// Token: 0x06001FE3 RID: 8163 RVA: 0x000B5910 File Offset: 0x000B3B10
	public void JoinGame(Connection connection)
	{
		using (Approval approval = Facepunch.Pool.Get<Approval>())
		{
			uint num = (uint)ConVar.Server.encryption;
			if (num > 1u && connection.os == "editor" && global::DeveloperList.Contains(connection.ownerid))
			{
				num = 1u;
			}
			approval.level = Application.loadedLevelName;
			approval.levelSeed = global::World.Seed;
			approval.levelSize = global::World.Size;
			approval.checksum = global::World.Checksum;
			approval.hostname = ConVar.Server.hostname;
			approval.official = ConVar.Server.official;
			approval.encryption = num;
			if (Network.Net.sv.write.Start())
			{
				Network.Net.sv.write.PacketID(3);
				approval.WriteToStream(Network.Net.sv.write);
				Network.Net.sv.write.Send(new SendInfo(connection));
			}
			connection.encryptionLevel = num;
			connection.encryptOutgoing = true;
		}
		connection.connected = true;
	}

	// Token: 0x17000239 RID: 569
	// (get) Token: 0x06001FE4 RID: 8164 RVA: 0x000B5A20 File Offset: 0x000B3C20
	public bool Restarting
	{
		get
		{
			return this.restartCoroutine != null;
		}
	}

	// Token: 0x06001FE5 RID: 8165 RVA: 0x000B5A30 File Offset: 0x000B3C30
	internal void Shutdown()
	{
		Interface.CallHook("OnServerShutdown", null);
		foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList.ToArray())
		{
			basePlayer.Kick("Server Shutting Down");
		}
		ConsoleSystem.Run(ConsoleSystem.Option.Server, "server.save", new object[0]);
		ConsoleSystem.Run(ConsoleSystem.Option.Server, "server.writecfg", new object[0]);
	}

	// Token: 0x06001FE6 RID: 8166 RVA: 0x000B5AA4 File Offset: 0x000B3CA4
	private IEnumerator ServerRestartWarning(string info, int iSeconds)
	{
		if (iSeconds < 0)
		{
			yield break;
		}
		if (!string.IsNullOrEmpty(info))
		{
			global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
			{
				0,
				"<color=#fff>SERVER</color> Restarting: " + info
			});
		}
		for (int i = iSeconds; i > 0; i--)
		{
			if (i == iSeconds || i % 60 == 0 || (i < 300 && i % 30 == 0) || (i < 60 && i % 10 == 0) || i < 10)
			{
				global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
				{
					0,
					"<color=#fff>SERVER</color> Restarting in " + i + " seconds!"
				});
				Debug.Log("Restarting in " + i + " seconds");
			}
			yield return UnityEngine.CoroutineEx.waitForSeconds(1f);
		}
		global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
		{
			0,
			"<color=#fff>SERVER</color> Restarting"
		});
		yield return UnityEngine.CoroutineEx.waitForSeconds(2f);
		foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList.ToArray())
		{
			basePlayer.Kick("Server Restarting");
		}
		yield return UnityEngine.CoroutineEx.waitForSeconds(1f);
		ConsoleSystem.Run(ConsoleSystem.Option.Server, "quit", new object[0]);
		yield break;
	}

	// Token: 0x06001FE7 RID: 8167 RVA: 0x000B5AC8 File Offset: 0x000B3CC8
	public static void RestartServer(string strNotice, int iSeconds)
	{
		if (SingletonComponent<global::ServerMgr>.Instance == null)
		{
			return;
		}
		if (SingletonComponent<global::ServerMgr>.Instance.restartCoroutine != null)
		{
			global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
			{
				0,
				"<color=#fff>SERVER</color> Restart interrupted!"
			});
			SingletonComponent<global::ServerMgr>.Instance.StopCoroutine(SingletonComponent<global::ServerMgr>.Instance.restartCoroutine);
			SingletonComponent<global::ServerMgr>.Instance.restartCoroutine = null;
		}
		SingletonComponent<global::ServerMgr>.Instance.restartCoroutine = SingletonComponent<global::ServerMgr>.Instance.ServerRestartWarning(strNotice, iSeconds);
		SingletonComponent<global::ServerMgr>.Instance.StartCoroutine(SingletonComponent<global::ServerMgr>.Instance.restartCoroutine);
		SingletonComponent<global::ServerMgr>.Instance.UpdateServerInformation();
	}

	// Token: 0x06001FE8 RID: 8168 RVA: 0x000B5B6C File Offset: 0x000B3D6C
	private void Log(Exception e)
	{
		if (ConVar.Global.developer > 0)
		{
			Debug.LogException(e);
		}
	}

	// Token: 0x06001FE9 RID: 8169 RVA: 0x000B5B80 File Offset: 0x000B3D80
	private void OnNetworkMessage(Message packet)
	{
		Message.Type type = packet.type;
		switch (type)
		{
		case 9:
			if (!packet.connection.isAuthenticated)
			{
				return;
			}
			if (packet.connection.GetPacketsPerSecond(packet.type) > (ulong)((long)ConVar.Server.maxrpcspersecond))
			{
				Network.Net.sv.Kick(packet.connection, "Paket Flooding: RPC Message");
				return;
			}
			using (TimeWarning.New("OnRPCMessage", 20L))
			{
				try
				{
					this.OnRPCMessage(packet);
				}
				catch (Exception e)
				{
					this.Log(e);
					Network.Net.sv.Kick(packet.connection, "Invalid Packet: RPC Message");
				}
			}
			packet.connection.AddPacketsPerSecond(packet.type);
			return;
		default:
			if (type != 21)
			{
				if (type == 22)
				{
					using (TimeWarning.New("OnEACMessage", 20L))
					{
						try
						{
							global::EACServer.OnMessageReceived(packet);
						}
						catch (Exception e2)
						{
							this.Log(e2);
							Network.Net.sv.Kick(packet.connection, "Invalid Packet: EAC");
						}
					}
					return;
				}
				if (type != 4)
				{
					this.ProcessUnhandledPacket(packet);
					return;
				}
				if (!packet.connection.isAuthenticated)
				{
					return;
				}
				if (packet.connection.GetPacketsPerSecond(packet.type) > 1UL)
				{
					Network.Net.sv.Kick(packet.connection, "Packet Flooding: Client Ready");
					return;
				}
				using (TimeWarning.New("ClientReady", 20L))
				{
					try
					{
						this.ClientReady(packet);
					}
					catch (Exception e3)
					{
						this.Log(e3);
						Network.Net.sv.Kick(packet.connection, "Invalid Packet: Client Ready");
					}
				}
				packet.connection.AddPacketsPerSecond(packet.type);
				return;
			}
			else
			{
				if (!packet.connection.isAuthenticated)
				{
					return;
				}
				if (packet.connection.GetPacketsPerSecond(packet.type) > 100UL)
				{
					Network.Net.sv.Kick(packet.connection, "Packet Flooding: Disconnect Reason");
					return;
				}
				using (TimeWarning.New("OnPlayerVoice", 20L))
				{
					try
					{
						this.OnPlayerVoice(packet);
					}
					catch (Exception e4)
					{
						this.Log(e4);
						Network.Net.sv.Kick(packet.connection, "Invalid Packet: Player Voice");
					}
				}
				packet.connection.AddPacketsPerSecond(packet.type);
				return;
			}
			break;
		case 12:
			if (!packet.connection.isAuthenticated)
			{
				return;
			}
			if (packet.connection.GetPacketsPerSecond(packet.type) > (ulong)((long)ConVar.Server.maxcommandspersecond))
			{
				Network.Net.sv.Kick(packet.connection, "Packet Flooding: Client Command");
				return;
			}
			using (TimeWarning.New("OnClientCommand", 20L))
			{
				try
				{
					global::ConsoleNetwork.OnClientCommand(packet);
				}
				catch (Exception e5)
				{
					this.Log(e5);
					Network.Net.sv.Kick(packet.connection, "Invalid Packet: Client Command");
				}
			}
			packet.connection.AddPacketsPerSecond(packet.type);
			return;
		case 14:
			if (!packet.connection.isAuthenticated)
			{
				return;
			}
			if (packet.connection.GetPacketsPerSecond(packet.type) > 1UL)
			{
				Network.Net.sv.Kick(packet.connection, "Packet Flooding: Disconnect Reason");
				return;
			}
			using (TimeWarning.New("ReadDisconnectReason", 20L))
			{
				try
				{
					this.ReadDisconnectReason(packet);
				}
				catch (Exception e6)
				{
					this.Log(e6);
					Network.Net.sv.Kick(packet.connection, "Invalid Packet: Disconnect Reason");
				}
			}
			packet.connection.AddPacketsPerSecond(packet.type);
			return;
		case 15:
			if (!packet.connection.isAuthenticated)
			{
				return;
			}
			if (packet.connection.GetPacketsPerSecond(packet.type) > (ulong)((long)ConVar.Server.maxtickspersecond))
			{
				Network.Net.sv.Kick(packet.connection, "Packet Flooding: Player Tick");
				return;
			}
			using (TimeWarning.New("OnPlayerTick", 20L))
			{
				try
				{
					this.OnPlayerTick(packet);
				}
				catch (Exception e7)
				{
					this.Log(e7);
					Network.Net.sv.Kick(packet.connection, "Invalid Packet: Player Tick");
				}
			}
			packet.connection.AddPacketsPerSecond(packet.type);
			return;
		case 18:
			if (packet.connection.GetPacketsPerSecond(packet.type) > 1UL)
			{
				Network.Net.sv.Kick(packet.connection, "Packet Flooding: User Information");
				return;
			}
			using (TimeWarning.New("GiveUserInformation", 20L))
			{
				try
				{
					this.OnGiveUserInformation(packet);
				}
				catch (Exception e8)
				{
					this.Log(e8);
					Network.Net.sv.Kick(packet.connection, "Invalid Packet: User Information");
				}
			}
			packet.connection.AddPacketsPerSecond(packet.type);
			return;
		}
	}

	// Token: 0x06001FEA RID: 8170 RVA: 0x000B6170 File Offset: 0x000B4370
	public void ProcessUnhandledPacket(Message packet)
	{
		Debug.LogWarning("[SERVER][UNHANDLED] " + packet.type);
		Network.Net.sv.Kick(packet.connection, "Sent Unhandled Message");
	}

	// Token: 0x06001FEB RID: 8171 RVA: 0x000B61A4 File Offset: 0x000B43A4
	public void ReadDisconnectReason(Message packet)
	{
		string text = packet.read.String();
		string text2 = packet.connection.ToString();
		if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(text2))
		{
			DebugEx.Log(text2 + " disconnecting: " + text, 0);
		}
	}

	// Token: 0x06001FEC RID: 8172 RVA: 0x000B61F4 File Offset: 0x000B43F4
	private bool SpawnPlayerSleeping(Connection connection)
	{
		global::BasePlayer basePlayer = global::BasePlayer.FindSleeping(connection.userid);
		if (basePlayer == null)
		{
			return false;
		}
		if (!basePlayer.IsSleeping())
		{
			Debug.LogWarning("Player spawning into sleeper that isn't sleeping!");
			basePlayer.Kill(global::BaseNetworkable.DestroyMode.None);
			return false;
		}
		basePlayer.PlayerInit(connection);
		basePlayer.inventory.SendSnapshot();
		DebugEx.Log(string.Concat(new object[]
		{
			basePlayer.net.connection.ToString(),
			" joined [",
			basePlayer.net.connection.os,
			"/",
			basePlayer.net.connection.ownerid,
			"]"
		}), 0);
		return true;
	}

	// Token: 0x06001FED RID: 8173 RVA: 0x000B62B4 File Offset: 0x000B44B4
	private void SpawnNewPlayer(Connection connection)
	{
		global::BasePlayer.SpawnPoint spawnPoint = global::ServerMgr.FindSpawnPoint();
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/player/player.prefab", spawnPoint.pos, spawnPoint.rot, true);
		global::BasePlayer basePlayer = baseEntity.ToPlayer();
		if (Interface.CallHook("OnPlayerSpawn", new object[]
		{
			basePlayer
		}) != null)
		{
			return;
		}
		basePlayer.health = 0f;
		basePlayer.lifestate = global::BaseCombatEntity.LifeState.Dead;
		basePlayer.ResetLifeStateOnSpawn = false;
		basePlayer.limitNetworking = true;
		basePlayer.Spawn();
		basePlayer.limitNetworking = false;
		basePlayer.PlayerInit(connection);
		if (global::SleepingBag.FindForPlayer(basePlayer.userID, true).Length == 0 && !basePlayer.hasPreviousLife)
		{
			basePlayer.Respawn();
		}
		else
		{
			basePlayer.SendRespawnOptions();
		}
		DebugEx.Log(string.Concat(new object[]
		{
			basePlayer.net.connection.ToString(),
			" joined [",
			basePlayer.net.connection.os,
			"/",
			basePlayer.net.connection.ownerid,
			"]"
		}), 0);
	}

	// Token: 0x06001FEE RID: 8174 RVA: 0x000B63D0 File Offset: 0x000B45D0
	private void ClientReady(Message packet)
	{
		packet.connection.decryptIncoming = true;
		using (ClientReady clientReady = ProtoBuf.ClientReady.Deserialize(packet.read))
		{
			foreach (ClientReady.ClientInfo clientInfo in clientReady.clientInfo)
			{
				packet.connection.info.Set(clientInfo.name, clientInfo.value);
			}
			this.connectionQueue.JoinedGame(packet.connection);
			Interface.CallHook("OnPlayerConnected", new object[]
			{
				packet
			});
			using (TimeWarning.New("ClientReady", 0.1f))
			{
				using (TimeWarning.New("SpawnPlayerSleeping", 0.1f))
				{
					bool flag = this.SpawnPlayerSleeping(packet.connection);
					if (flag)
					{
						return;
					}
				}
				using (TimeWarning.New("SpawnNewPlayer", 0.1f))
				{
					this.SpawnNewPlayer(packet.connection);
				}
			}
		}
	}

	// Token: 0x06001FEF RID: 8175 RVA: 0x000B6594 File Offset: 0x000B4794
	private void OnRPCMessage(Message packet)
	{
		uint uid = packet.read.UInt32();
		uint nameID = packet.read.UInt32();
		global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find(uid) as global::BaseEntity;
		if (baseEntity == null)
		{
			return;
		}
		baseEntity.SV_RPCMessage(nameID, packet);
	}

	// Token: 0x06001FF0 RID: 8176 RVA: 0x000B65E0 File Offset: 0x000B47E0
	private void OnPlayerTick(Message packet)
	{
		global::BasePlayer basePlayer = packet.Player();
		if (basePlayer == null)
		{
			return;
		}
		basePlayer.OnReceivedTick(packet.read);
	}

	// Token: 0x06001FF1 RID: 8177 RVA: 0x000B6610 File Offset: 0x000B4810
	private void OnPlayerVoice(Message packet)
	{
		global::BasePlayer basePlayer = packet.Player();
		if (basePlayer == null)
		{
			return;
		}
		basePlayer.OnReceivedVoice(packet.read.BytesWithSize());
	}

	// Token: 0x06001FF2 RID: 8178 RVA: 0x000B6644 File Offset: 0x000B4844
	private void OnGiveUserInformation(Message packet)
	{
		if (packet.connection.state != null)
		{
			Network.Net.sv.Kick(packet.connection, "Invalid connection state");
			return;
		}
		packet.connection.state = 1;
		byte b = packet.read.UInt8();
		if (b != 228)
		{
			Network.Net.sv.Kick(packet.connection, "Invalid Connection Protocol");
			return;
		}
		packet.connection.userid = packet.read.UInt64();
		packet.connection.protocol = packet.read.UInt32();
		packet.connection.os = packet.read.String();
		packet.connection.username = packet.read.String();
		Interface.CallHook("OnClientAuth", new object[]
		{
			packet.connection
		});
		if (string.IsNullOrEmpty(packet.connection.os))
		{
			throw new Exception("Invalid OS");
		}
		if (string.IsNullOrEmpty(packet.connection.username))
		{
			Network.Net.sv.Kick(packet.connection, "Invalid Username");
			return;
		}
		packet.connection.username = packet.connection.username.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ').Trim();
		if (string.IsNullOrEmpty(packet.connection.username))
		{
			Network.Net.sv.Kick(packet.connection, "Invalid Username");
			return;
		}
		string text = string.Empty;
		string branch = ConVar.Server.branch;
		if (packet.read.unread >= 4)
		{
			text = packet.read.String();
		}
		if (branch != string.Empty && branch != text)
		{
			DebugEx.Log(string.Concat(new object[]
			{
				"Kicking ",
				packet.connection,
				" - their branch is '",
				text,
				"' not '",
				branch,
				"'"
			}), 0);
			Network.Net.sv.Kick(packet.connection, "Wrong Steam Beta: Requires '" + branch + "' branch!");
			return;
		}
		if (packet.connection.protocol > 2065u)
		{
			DebugEx.Log(string.Concat(new object[]
			{
				"Kicking ",
				packet.connection,
				" - their protocol is ",
				packet.connection.protocol,
				" not ",
				2065
			}), 0);
			Network.Net.sv.Kick(packet.connection, "Wrong Connection Protocol: Server update required!");
			return;
		}
		if (packet.connection.protocol < 2065u)
		{
			DebugEx.Log(string.Concat(new object[]
			{
				"Kicking ",
				packet.connection,
				" - their protocol is ",
				packet.connection.protocol,
				" not ",
				2065
			}), 0);
			Network.Net.sv.Kick(packet.connection, "Wrong Connection Protocol: Client update required!");
			return;
		}
		packet.connection.token = packet.read.BytesWithSize();
		if (packet.connection.token == null || packet.connection.token.Length < 1)
		{
			Network.Net.sv.Kick(packet.connection, "Invalid Token");
			return;
		}
		this.auth.OnNewConnection(packet.connection);
	}

	// Token: 0x06001FF3 RID: 8179 RVA: 0x000B69D4 File Offset: 0x000B4BD4
	private bool OnUnconnectedMessage(int type, Read read, uint ip, int port)
	{
		if (this.queryTimer.Elapsed.TotalSeconds > 60.0)
		{
			this.queryTimer.Reset();
			this.queryTimer.Start();
			this.unconnectedQueries.Clear();
		}
		if (this.queriesPerSeconTimer.Elapsed.TotalSeconds > 1.0)
		{
			this.queriesPerSeconTimer.Reset();
			this.queriesPerSeconTimer.Start();
			this.NumQueriesLastSecond = 0;
		}
		if (type != 255)
		{
			return false;
		}
		if (this.NumQueriesLastSecond > ConVar.Server.queriesPerSecond)
		{
			return false;
		}
		if (read.UInt8() != 255)
		{
			return false;
		}
		if (read.UInt8() != 255)
		{
			return false;
		}
		if (read.UInt8() != 255)
		{
			return false;
		}
		if (!this.unconnectedQueries.ContainsKey(ip))
		{
			this.unconnectedQueries.Add(ip, 0);
		}
		int num = this.unconnectedQueries[ip] + 1;
		this.unconnectedQueries[ip] = num;
		if (num > ConVar.Server.ipQueriesPerMin)
		{
			return true;
		}
		this.NumQueriesLastSecond++;
		read.Position = 0L;
		int unread = read.unread;
		if (unread > 4096)
		{
			return true;
		}
		if (this.queryBuffer.Capacity < unread)
		{
			this.queryBuffer.Capacity = unread;
		}
		int num2 = read.Read(this.queryBuffer.GetBuffer(), 0, unread);
		Rust.Global.SteamServer.Query.Handle(this.queryBuffer.GetBuffer(), num2, ip, (ushort)port);
		return true;
	}

	// Token: 0x04001AAC RID: 6828
	private global::ConnectionAuth auth;

	// Token: 0x04001AAD RID: 6829
	private bool runFrameUpdate;

	// Token: 0x04001AAE RID: 6830
	private bool useQueryPort;

	// Token: 0x04001AAF RID: 6831
	public global::UserPersistance persistance;

	// Token: 0x04001AB0 RID: 6832
	private string _AssemblyHash;

	// Token: 0x04001AB1 RID: 6833
	private IEnumerator restartCoroutine;

	// Token: 0x04001AB2 RID: 6834
	public global::ConnectionQueue connectionQueue = new global::ConnectionQueue();

	// Token: 0x04001AB3 RID: 6835
	private Stopwatch queryTimer = Stopwatch.StartNew();

	// Token: 0x04001AB4 RID: 6836
	private Dictionary<uint, int> unconnectedQueries = new Dictionary<uint, int>();

	// Token: 0x04001AB5 RID: 6837
	private Stopwatch queriesPerSeconTimer = Stopwatch.StartNew();

	// Token: 0x04001AB6 RID: 6838
	private int NumQueriesLastSecond;

	// Token: 0x04001AB7 RID: 6839
	private MemoryStream queryBuffer = new MemoryStream();
}
