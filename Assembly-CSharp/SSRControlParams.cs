﻿using System;

// Token: 0x020005EF RID: 1519
[Serializable]
public class SSRControlParams
{
	// Token: 0x04001A12 RID: 6674
	public float fresnelCutoff = 0.05f;

	// Token: 0x04001A13 RID: 6675
	public float thicknessMin = 1f;

	// Token: 0x04001A14 RID: 6676
	public float thicknessMax = 20f;

	// Token: 0x04001A15 RID: 6677
	public float thicknessStartDist = 40f;

	// Token: 0x04001A16 RID: 6678
	public float thicknessEndDist = 100f;
}
