﻿using System;
using UnityEngine;

// Token: 0x02000504 RID: 1284
[CreateAssetMenu(menuName = "Rust/MaterialEffect")]
public class MaterialEffect : ScriptableObject
{
	// Token: 0x06001B47 RID: 6983 RVA: 0x000984E8 File Offset: 0x000966E8
	public void SpawnOnRay(Ray ray, int mask, float length = 0.5f, Vector3 forward = default(Vector3))
	{
		RaycastHit raycastHit;
		if (!global::GamePhysics.Trace(ray, 0f, out raycastHit, length, mask, 0))
		{
			global::Effect.client.Run(this.DefaultEffect.resourcePath, ray.origin, ray.direction * -1f, forward);
			return;
		}
		global::Effect.client.Run(this.DefaultEffect.resourcePath, raycastHit.point, raycastHit.normal, forward);
	}

	// Token: 0x04001607 RID: 5639
	public global::GameObjectRef DefaultEffect;

	// Token: 0x04001608 RID: 5640
	public global::MaterialEffect.Entry[] Entries;

	// Token: 0x02000505 RID: 1285
	[Serializable]
	public class Entry
	{
		// Token: 0x04001609 RID: 5641
		public PhysicMaterial Material;

		// Token: 0x0400160A RID: 5642
		public global::GameObjectRef Effect;
	}
}
