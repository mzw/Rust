﻿using System;
using UnityEngine;

// Token: 0x02000464 RID: 1124
public class Prefab<T> : global::Prefab, IComparable<global::Prefab<T>> where T : Component
{
	// Token: 0x060018A4 RID: 6308 RVA: 0x0008AE30 File Offset: 0x00089030
	public Prefab(string name, GameObject prefab, T component, global::GameManager manager, global::PrefabAttribute.Library attribute) : base(name, prefab, manager, attribute)
	{
		this.Component = component;
	}

	// Token: 0x060018A5 RID: 6309 RVA: 0x0008AE48 File Offset: 0x00089048
	public int CompareTo(global::Prefab<T> that)
	{
		return base.CompareTo(that);
	}

	// Token: 0x0400136E RID: 4974
	public T Component;
}
