﻿using System;
using Network;
using UnityEngine;

// Token: 0x020000A3 RID: 163
public class TreeEntity : global::ResourceEntity
{
	// Token: 0x060009FB RID: 2555 RVA: 0x000440B8 File Offset: 0x000422B8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("TreeEntity.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009FC RID: 2556 RVA: 0x00044100 File Offset: 0x00042300
	public override void DestroyShared()
	{
		base.DestroyShared();
		if (base.isServer)
		{
			this.CleanupMarker();
		}
	}

	// Token: 0x060009FD RID: 2557 RVA: 0x0004411C File Offset: 0x0004231C
	public override float BoundsPadding()
	{
		return 1f;
	}

	// Token: 0x060009FE RID: 2558 RVA: 0x00044124 File Offset: 0x00042324
	public override bool SupportsPooling()
	{
		return false;
	}

	// Token: 0x060009FF RID: 2559 RVA: 0x00044128 File Offset: 0x00042328
	public override void ServerInit()
	{
		base.ServerInit();
		this.lastDirection = (float)((Random.Range(0, 2) != 0) ? 1 : -1);
	}

	// Token: 0x06000A00 RID: 2560 RVA: 0x0004414C File Offset: 0x0004234C
	public bool DidHitMarker(global::HitInfo info)
	{
		if (this.xMarker == null)
		{
			return false;
		}
		Vector3 vector = Vector3Ex.Direction2D(base.transform.position, this.xMarker.GetEstimatedWorldPosition());
		Vector3 attackNormal = info.attackNormal;
		return (double)Vector3.Dot(vector, attackNormal) >= 0.3 && Vector3.Distance(this.xMarker.GetEstimatedWorldPosition(), info.HitPositionWorld) <= 0.2f;
	}

	// Token: 0x06000A01 RID: 2561 RVA: 0x000441C8 File Offset: 0x000423C8
	public void StartBonusGame()
	{
		if (base.IsInvoking(new Action(this.StopBonusGame)))
		{
			base.CancelInvoke(new Action(this.StopBonusGame));
		}
		base.Invoke(new Action(this.StopBonusGame), 60f);
	}

	// Token: 0x06000A02 RID: 2562 RVA: 0x00044218 File Offset: 0x00042418
	public void StopBonusGame()
	{
		this.CleanupMarker();
		this.lastHitTime = 0f;
		this.currentBonusLevel = 0;
	}

	// Token: 0x06000A03 RID: 2563 RVA: 0x00044234 File Offset: 0x00042434
	public bool BonusActive()
	{
		return this.xMarker != null;
	}

	// Token: 0x06000A04 RID: 2564 RVA: 0x00044244 File Offset: 0x00042444
	public override void OnAttacked(global::HitInfo info)
	{
		bool canGather = info.CanGather;
		float num = Time.time - this.lastHitTime;
		this.lastHitTime = Time.time;
		if (!this.hasBonusGame || !canGather || info.Initiator == null || (this.BonusActive() && !this.DidHitMarker(info)))
		{
			base.OnAttacked(info);
			return;
		}
		if (this.xMarker != null && !info.DidGather && info.gatherScale > 0f)
		{
			this.xMarker.ClientRPC<int>(null, "MarkerHit", this.currentBonusLevel);
			this.currentBonusLevel++;
			info.gatherScale = 1f + Mathf.Clamp((float)this.currentBonusLevel * 0.125f, 0f, 1f);
		}
		Vector3 vector = (!(this.xMarker != null)) ? info.HitPositionWorld : this.xMarker.GetEstimatedWorldPosition();
		this.CleanupMarker();
		Vector3 vector2 = Vector3Ex.Direction2D(base.transform.position, vector);
		Vector3 vector3 = vector2;
		Vector3 vector4 = Vector3.Cross(vector3, Vector3.up);
		float num2 = this.lastDirection;
		float num3 = Random.Range(0.5f, 0.5f);
		Vector3 vector5 = Vector3.Lerp(-vector3, vector4 * num2, num3);
		Vector3 vector6 = base.transform.InverseTransformDirection(vector5.normalized) * 2.5f;
		vector6 = base.transform.InverseTransformPoint(this.GetCollider().ClosestPoint(base.transform.TransformPoint(vector6)));
		Vector3 vector7 = base.transform.TransformPoint(vector6);
		vector6.y = base.transform.InverseTransformPoint(info.HitPositionWorld).y;
		Vector3 vector8 = base.transform.InverseTransformPoint(info.Initiator.CenterPoint());
		float num4 = Mathf.Max(0.75f, vector8.y);
		float num5 = vector8.y + 0.5f;
		vector6.y = Mathf.Clamp(vector6.y + Random.Range(0.1f, 0.2f) * ((Random.Range(0, 2) != 0) ? 1f : -1f), num4, num5);
		Vector3 vector9 = Vector3Ex.Direction2D(base.transform.position, vector7);
		Vector3 vector10 = vector9;
		vector9 = base.transform.InverseTransformDirection(vector9);
		Quaternion rot = UnityEngine.QuaternionEx.LookRotationNormal(-vector9, Vector3.zero);
		vector6 = base.transform.TransformPoint(vector6);
		rot = UnityEngine.QuaternionEx.LookRotationNormal(-vector10, Vector3.zero);
		this.xMarker = global::GameManager.server.CreateEntity("assets/content/nature/treesprefabs/trees/effects/tree_marking.prefab", vector6, rot, true);
		this.xMarker.Spawn();
		if (num > 5f)
		{
			this.StartBonusGame();
		}
		base.OnAttacked(info);
		if (this.health > 0f)
		{
			this.lastAttackDamage = info.damageTypes.Total();
			int num6 = Mathf.CeilToInt(this.health / this.lastAttackDamage);
			if (num6 < 2)
			{
				base.ClientRPC<int>(null, "CrackSound", 1);
			}
			else if (num6 < 5)
			{
				base.ClientRPC<int>(null, "CrackSound", 0);
			}
		}
	}

	// Token: 0x06000A05 RID: 2565 RVA: 0x0004459C File Offset: 0x0004279C
	public void CleanupMarker()
	{
		if (this.xMarker)
		{
			this.xMarker.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		this.xMarker = null;
	}

	// Token: 0x06000A06 RID: 2566 RVA: 0x000445C4 File Offset: 0x000427C4
	public Collider GetCollider()
	{
		if (base.isServer)
		{
			return (!(this.serverCollider == null)) ? this.serverCollider : base.GetComponentInChildren<CapsuleCollider>();
		}
		return (!(this.clientCollider == null)) ? this.clientCollider : base.GetComponent<Collider>();
	}

	// Token: 0x06000A07 RID: 2567 RVA: 0x00044624 File Offset: 0x00042824
	public override void OnKilled(global::HitInfo info)
	{
		if (this.isKilled)
		{
			return;
		}
		this.isKilled = true;
		this.CleanupMarker();
		if (this.fallOnKilled)
		{
			Collider collider = this.GetCollider();
			if (collider)
			{
				collider.enabled = false;
			}
			base.ClientRPC<Vector3>(null, "TreeFall", info.attackNormal);
			base.Invoke(new Action(this.DelayedKill), this.fallDuration + 1f);
		}
		else
		{
			this.DelayedKill();
		}
	}

	// Token: 0x06000A08 RID: 2568 RVA: 0x000446AC File Offset: 0x000428AC
	public void DelayedKill()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x040004A2 RID: 1186
	public global::GameObjectRef prefab;

	// Token: 0x040004A3 RID: 1187
	public bool hasBonusGame = true;

	// Token: 0x040004A4 RID: 1188
	public global::GameObjectRef bonusHitEffect;

	// Token: 0x040004A5 RID: 1189
	public global::GameObjectRef bonusHitSound;

	// Token: 0x040004A6 RID: 1190
	public Collider serverCollider;

	// Token: 0x040004A7 RID: 1191
	public Collider clientCollider;

	// Token: 0x040004A8 RID: 1192
	public global::SoundDefinition smallCrackSoundDef;

	// Token: 0x040004A9 RID: 1193
	public global::SoundDefinition medCrackSoundDef;

	// Token: 0x040004AA RID: 1194
	private float lastAttackDamage;

	// Token: 0x040004AB RID: 1195
	[NonSerialized]
	protected global::BaseEntity xMarker;

	// Token: 0x040004AC RID: 1196
	private int currentBonusLevel;

	// Token: 0x040004AD RID: 1197
	private float lastDirection = -1f;

	// Token: 0x040004AE RID: 1198
	private float lastHitTime;

	// Token: 0x040004AF RID: 1199
	[Header("Falling")]
	public bool fallOnKilled = true;

	// Token: 0x040004B0 RID: 1200
	public float fallDuration = 1.5f;

	// Token: 0x040004B1 RID: 1201
	public global::GameObjectRef fallStartSound;

	// Token: 0x040004B2 RID: 1202
	public global::GameObjectRef fallImpactSound;

	// Token: 0x040004B3 RID: 1203
	public global::GameObjectRef fallImpactParticles;

	// Token: 0x040004B4 RID: 1204
	public global::SoundDefinition fallLeavesLoopDef;

	// Token: 0x040004B5 RID: 1205
	[NonSerialized]
	public bool[] usedHeights = new bool[20];

	// Token: 0x040004B6 RID: 1206
	public bool impactSoundPlayed;

	// Token: 0x040004B7 RID: 1207
	[NonSerialized]
	public float treeDistanceUponFalling;
}
