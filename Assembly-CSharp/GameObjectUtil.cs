﻿using System;
using UnityEngine;

// Token: 0x02000785 RID: 1925
public static class GameObjectUtil
{
	// Token: 0x060023BF RID: 9151 RVA: 0x000C5B5C File Offset: 0x000C3D5C
	public static void GlobalBroadcast(string messageName, object param = null)
	{
		Transform[] rootObjects = global::TransformUtil.GetRootObjects();
		foreach (Transform transform in rootObjects)
		{
			transform.BroadcastMessage(messageName, param, 1);
		}
	}
}
