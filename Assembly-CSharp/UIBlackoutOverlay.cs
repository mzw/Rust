﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000718 RID: 1816
public class UIBlackoutOverlay : MonoBehaviour
{
	// Token: 0x04001EEC RID: 7916
	public CanvasGroup group;

	// Token: 0x04001EED RID: 7917
	public static Dictionary<global::UIBlackoutOverlay.blackoutType, global::UIBlackoutOverlay> instances;

	// Token: 0x04001EEE RID: 7918
	public global::UIBlackoutOverlay.blackoutType overlayType = global::UIBlackoutOverlay.blackoutType.NONE;

	// Token: 0x02000719 RID: 1817
	public enum blackoutType
	{
		// Token: 0x04001EF0 RID: 7920
		FULLBLACK,
		// Token: 0x04001EF1 RID: 7921
		BINOCULAR,
		// Token: 0x04001EF2 RID: 7922
		SCOPE,
		// Token: 0x04001EF3 RID: 7923
		HELMETSLIT,
		// Token: 0x04001EF4 RID: 7924
		NONE = 64
	}
}
