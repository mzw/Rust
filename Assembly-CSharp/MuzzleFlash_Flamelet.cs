﻿using System;
using UnityEngine;

// Token: 0x02000328 RID: 808
public class MuzzleFlash_Flamelet : MonoBehaviour
{
	// Token: 0x0600139F RID: 5023 RVA: 0x00073594 File Offset: 0x00071794
	private void OnEnable()
	{
		this.flameletParticle.shape.angle = (float)Random.Range(6, 13);
		float num = Random.Range(7f, 9f);
		this.flameletParticle.startSpeed = Random.Range(2.5f, num);
		this.flameletParticle.startSize = Random.Range(0.05f, num * 0.015f);
	}

	// Token: 0x04000E75 RID: 3701
	public ParticleSystem flameletParticle;
}
