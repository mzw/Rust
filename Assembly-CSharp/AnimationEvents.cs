﻿using System;
using UnityEngine;

// Token: 0x02000311 RID: 785
public class AnimationEvents : global::BaseMonoBehaviour
{
	// Token: 0x06001373 RID: 4979 RVA: 0x000726CC File Offset: 0x000708CC
	protected void OnEnable()
	{
		if (this.rootObject == null)
		{
			this.rootObject = base.transform;
		}
	}

	// Token: 0x04000E28 RID: 3624
	public Transform rootObject;

	// Token: 0x04000E29 RID: 3625
	public global::HeldEntity targetEntity;

	// Token: 0x04000E2A RID: 3626
	[Tooltip("Path to the effect folder for these animations. Relative to this object.")]
	public string effectFolder;

	// Token: 0x04000E2B RID: 3627
	public string localFolder;

	// Token: 0x04000E2C RID: 3628
	public bool IsBusy;
}
