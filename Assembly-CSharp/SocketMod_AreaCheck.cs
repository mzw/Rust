﻿using System;
using UnityEngine;

// Token: 0x02000225 RID: 549
public class SocketMod_AreaCheck : global::SocketMod
{
	// Token: 0x06000FE3 RID: 4067 RVA: 0x00060B20 File Offset: 0x0005ED20
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		bool flag = true;
		if (!this.wantsInside)
		{
			flag = !flag;
		}
		Gizmos.color = ((!flag) ? Color.red : Color.green);
		Gizmos.DrawSphere(Vector3.zero, 0.05f);
	}

	// Token: 0x06000FE4 RID: 4068 RVA: 0x00060B78 File Offset: 0x0005ED78
	public static bool IsInArea(Vector3 vPoint, LayerMask layerMask)
	{
		return global::GamePhysics.CheckSphere(vPoint, 0.05f, layerMask.value, 0);
	}

	// Token: 0x06000FE5 RID: 4069 RVA: 0x00060B90 File Offset: 0x0005ED90
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 vPoint = place.position + place.rotation * this.worldPosition;
		bool flag = global::SocketMod_AreaCheck.IsInArea(vPoint, this.layerMask) == this.wantsInside;
		if (flag)
		{
			return true;
		}
		global::Construction.lastPlacementError = "Failed Check: IsInArea (" + this.hierachyName + ")";
		return false;
	}

	// Token: 0x04000A9C RID: 2716
	public bool wantsInside = true;

	// Token: 0x04000A9D RID: 2717
	public LayerMask layerMask;
}
