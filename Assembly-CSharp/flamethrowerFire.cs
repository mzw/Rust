﻿using System;
using UnityEngine;

// Token: 0x020007B8 RID: 1976
public class flamethrowerFire : MonoBehaviour
{
	// Token: 0x060024B7 RID: 9399 RVA: 0x000CA014 File Offset: 0x000C8214
	public void PilotLightOn()
	{
		this.pilotLightFX.enableEmission = true;
		this.SetFlameStatus(false);
	}

	// Token: 0x060024B8 RID: 9400 RVA: 0x000CA02C File Offset: 0x000C822C
	public void SetFlameStatus(bool status)
	{
		foreach (ParticleSystem particleSystem in this.flameFX)
		{
			particleSystem.enableEmission = status;
		}
	}

	// Token: 0x060024B9 RID: 9401 RVA: 0x000CA060 File Offset: 0x000C8260
	public void ShutOff()
	{
		this.pilotLightFX.enableEmission = false;
		this.SetFlameStatus(false);
	}

	// Token: 0x060024BA RID: 9402 RVA: 0x000CA078 File Offset: 0x000C8278
	public void FlameOn()
	{
		this.pilotLightFX.enableEmission = false;
		this.SetFlameStatus(true);
	}

	// Token: 0x060024BB RID: 9403 RVA: 0x000CA090 File Offset: 0x000C8290
	private void Start()
	{
		this.previousflameState = (this.flameState = global::flamethrowerState.OFF);
	}

	// Token: 0x060024BC RID: 9404 RVA: 0x000CA0B0 File Offset: 0x000C82B0
	private void Update()
	{
		if (this.previousflameState != this.flameState)
		{
			global::flamethrowerState flamethrowerState = this.flameState;
			if (flamethrowerState != global::flamethrowerState.OFF)
			{
				if (flamethrowerState != global::flamethrowerState.PILOT_LIGHT)
				{
					if (flamethrowerState == global::flamethrowerState.FLAME_ON)
					{
						this.FlameOn();
					}
				}
				else
				{
					this.PilotLightOn();
				}
			}
			else
			{
				this.ShutOff();
			}
			this.previousflameState = this.flameState;
			this.jet.SetOn(this.flameState == global::flamethrowerState.FLAME_ON);
		}
	}

	// Token: 0x0400202B RID: 8235
	public ParticleSystem pilotLightFX;

	// Token: 0x0400202C RID: 8236
	public ParticleSystem[] flameFX;

	// Token: 0x0400202D RID: 8237
	public global::FlameJet jet;

	// Token: 0x0400202E RID: 8238
	public AudioSource oneShotSound;

	// Token: 0x0400202F RID: 8239
	public AudioSource loopSound;

	// Token: 0x04002030 RID: 8240
	public AudioClip pilotlightIdle;

	// Token: 0x04002031 RID: 8241
	public AudioClip flameLoop;

	// Token: 0x04002032 RID: 8242
	public AudioClip flameStart;

	// Token: 0x04002033 RID: 8243
	public global::flamethrowerState flameState;

	// Token: 0x04002034 RID: 8244
	private global::flamethrowerState previousflameState;
}
