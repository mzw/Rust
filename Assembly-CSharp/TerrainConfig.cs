﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

// Token: 0x0200052D RID: 1325
[CreateAssetMenu(menuName = "Rust/Terrain Config")]
public class TerrainConfig : ScriptableObject
{
	// Token: 0x06001BF9 RID: 7161 RVA: 0x0009DC30 File Offset: 0x0009BE30
	public PhysicMaterial[] GetPhysicMaterials()
	{
		PhysicMaterial[] array = new PhysicMaterial[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = this.Splats[i].Material;
		}
		return array;
	}

	// Token: 0x06001BFA RID: 7162 RVA: 0x0009DC78 File Offset: 0x0009BE78
	public Color[] GetAridColors()
	{
		Color[] array = new Color[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = this.Splats[i].AridColor;
		}
		return array;
	}

	// Token: 0x06001BFB RID: 7163 RVA: 0x0009DCC8 File Offset: 0x0009BEC8
	public Color[] GetTemperateColors()
	{
		Color[] array = new Color[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = this.Splats[i].TemperateColor;
		}
		return array;
	}

	// Token: 0x06001BFC RID: 7164 RVA: 0x0009DD18 File Offset: 0x0009BF18
	public Color[] GetTundraColors()
	{
		Color[] array = new Color[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = this.Splats[i].TundraColor;
		}
		return array;
	}

	// Token: 0x06001BFD RID: 7165 RVA: 0x0009DD68 File Offset: 0x0009BF68
	public Color[] GetArcticColors()
	{
		Color[] array = new Color[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = this.Splats[i].ArcticColor;
		}
		return array;
	}

	// Token: 0x06001BFE RID: 7166 RVA: 0x0009DDB8 File Offset: 0x0009BFB8
	public float[] GetSplatTiling()
	{
		float[] array = new float[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = this.Splats[i].SplatTiling;
		}
		return array;
	}

	// Token: 0x06001BFF RID: 7167 RVA: 0x0009DE00 File Offset: 0x0009C000
	public float GetMaxSplatTiling()
	{
		float num = float.MinValue;
		for (int i = 0; i < this.Splats.Length; i++)
		{
			if (this.Splats[i].SplatTiling > num)
			{
				num = this.Splats[i].SplatTiling;
			}
		}
		return num;
	}

	// Token: 0x06001C00 RID: 7168 RVA: 0x0009DE50 File Offset: 0x0009C050
	public float GetMinSplatTiling()
	{
		float num = float.MaxValue;
		for (int i = 0; i < this.Splats.Length; i++)
		{
			if (this.Splats[i].SplatTiling < num)
			{
				num = this.Splats[i].SplatTiling;
			}
		}
		return num;
	}

	// Token: 0x06001C01 RID: 7169 RVA: 0x0009DEA0 File Offset: 0x0009C0A0
	public Vector3[] GetPackedUVMIX()
	{
		Vector3[] array = new Vector3[this.Splats.Length];
		for (int i = 0; i < this.Splats.Length; i++)
		{
			array[i] = new Vector3(this.Splats[i].UVMIXMult, this.Splats[i].UVMIXStart, this.Splats[i].UVMIXDist);
		}
		return array;
	}

	// Token: 0x040016BC RID: 5820
	public bool CastShadows = true;

	// Token: 0x040016BD RID: 5821
	public LayerMask GroundMask = 0;

	// Token: 0x040016BE RID: 5822
	public LayerMask WaterMask = 0;

	// Token: 0x040016BF RID: 5823
	public PhysicMaterial GenericMaterial;

	// Token: 0x040016C0 RID: 5824
	public Material Material;

	// Token: 0x040016C1 RID: 5825
	public Texture2D SplatAtlas;

	// Token: 0x040016C2 RID: 5826
	public Texture2D NormalAtlas;

	// Token: 0x040016C3 RID: 5827
	public float HeightMapErrorMin = 5f;

	// Token: 0x040016C4 RID: 5828
	public float HeightMapErrorMax = 100f;

	// Token: 0x040016C5 RID: 5829
	public float BaseMapDistanceMin = 100f;

	// Token: 0x040016C6 RID: 5830
	public float BaseMapDistanceMax = 500f;

	// Token: 0x040016C7 RID: 5831
	public float ShaderLodMin = 100f;

	// Token: 0x040016C8 RID: 5832
	public float ShaderLodMax = 600f;

	// Token: 0x040016C9 RID: 5833
	public global::TerrainConfig.SplatType[] Splats = new global::TerrainConfig.SplatType[8];

	// Token: 0x0200052E RID: 1326
	[Serializable]
	public class SplatType
	{
		// Token: 0x040016CA RID: 5834
		public string Name = string.Empty;

		// Token: 0x040016CB RID: 5835
		[FormerlySerializedAs("WarmColor")]
		public Color AridColor = Color.white;

		// Token: 0x040016CC RID: 5836
		[FormerlySerializedAs("Color")]
		public Color TemperateColor = Color.white;

		// Token: 0x040016CD RID: 5837
		[FormerlySerializedAs("ColdColor")]
		public Color TundraColor = Color.white;

		// Token: 0x040016CE RID: 5838
		[FormerlySerializedAs("ColdColor")]
		public Color ArcticColor = Color.white;

		// Token: 0x040016CF RID: 5839
		public PhysicMaterial Material;

		// Token: 0x040016D0 RID: 5840
		public float SplatTiling = 5f;

		// Token: 0x040016D1 RID: 5841
		[Range(0f, 1f)]
		public float UVMIXMult = 0.15f;

		// Token: 0x040016D2 RID: 5842
		public float UVMIXStart;

		// Token: 0x040016D3 RID: 5843
		public float UVMIXDist = 100f;
	}
}
