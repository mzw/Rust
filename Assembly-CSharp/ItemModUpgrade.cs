﻿using System;
using Oxide.Core;
using UnityEngine;

// Token: 0x020004F7 RID: 1271
public class ItemModUpgrade : global::ItemMod
{
	// Token: 0x06001B0A RID: 6922 RVA: 0x000975EC File Offset: 0x000957EC
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (command == "upgrade_item")
		{
			if (item.amount < this.numForUpgrade)
			{
				return;
			}
			float num = Random.Range(0f, 1f);
			if (num <= this.upgradeSuccessChance)
			{
				item.UseItem(this.numForUpgrade);
				global::Item item2 = global::ItemManager.Create(this.upgradedItem, this.numUpgradedItem, 0UL);
				Interface.CallHook("OnItemUpgrade", new object[]
				{
					item,
					item2,
					player
				});
				if (!item2.MoveToContainer(player.inventory.containerMain, -1, true))
				{
					item2.Drop(player.eyes.position, player.eyes.BodyForward() * 2f, default(Quaternion));
				}
				if (this.successEffect.isValid)
				{
					global::Effect.server.Run(this.successEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
				}
			}
			else
			{
				item.UseItem(this.numToLoseOnFail);
				if (this.failEffect.isValid)
				{
					global::Effect.server.Run(this.failEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
				}
			}
		}
	}

	// Token: 0x040015DC RID: 5596
	public int numForUpgrade = 10;

	// Token: 0x040015DD RID: 5597
	public float upgradeSuccessChance = 1f;

	// Token: 0x040015DE RID: 5598
	public int numToLoseOnFail = 2;

	// Token: 0x040015DF RID: 5599
	public global::ItemDefinition upgradedItem;

	// Token: 0x040015E0 RID: 5600
	public int numUpgradedItem = 1;

	// Token: 0x040015E1 RID: 5601
	public global::GameObjectRef successEffect;

	// Token: 0x040015E2 RID: 5602
	public global::GameObjectRef failEffect;
}
