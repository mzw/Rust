﻿using System;
using UnityEngine;

// Token: 0x02000549 RID: 1353
public class DecorSwim : global::DecorComponent
{
	// Token: 0x06001C90 RID: 7312 RVA: 0x000A0184 File Offset: 0x0009E384
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		pos.y = global::TerrainMeta.WaterMap.GetHeight(pos);
	}
}
