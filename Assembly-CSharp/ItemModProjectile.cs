﻿using System;
using Rust;
using UnityEngine;

// Token: 0x020004EC RID: 1260
public class ItemModProjectile : MonoBehaviour
{
	// Token: 0x06001AEF RID: 6895 RVA: 0x00096C54 File Offset: 0x00094E54
	public float GetRandomVelocity()
	{
		return this.projectileVelocity + Random.Range(-this.projectileVelocitySpread, this.projectileVelocitySpread);
	}

	// Token: 0x06001AF0 RID: 6896 RVA: 0x00096C70 File Offset: 0x00094E70
	public float GetSpreadScalar()
	{
		if (this.useCurve)
		{
			return this.spreadScalar.Evaluate(Random.Range(0f, 1f));
		}
		return 1f;
	}

	// Token: 0x06001AF1 RID: 6897 RVA: 0x00096CA0 File Offset: 0x00094EA0
	public float GetIndexedSpreadScalar(int shotIndex, int maxShots)
	{
		float num2;
		if (shotIndex != -1)
		{
			float num = 1f / (float)maxShots;
			num2 = (float)shotIndex * num;
		}
		else
		{
			num2 = Random.Range(0f, 1f);
		}
		return this.spreadScalar.Evaluate(num2);
	}

	// Token: 0x06001AF2 RID: 6898 RVA: 0x00096CEC File Offset: 0x00094EEC
	public float GetAverageVelocity()
	{
		return this.projectileVelocity;
	}

	// Token: 0x06001AF3 RID: 6899 RVA: 0x00096CF4 File Offset: 0x00094EF4
	public float GetMinVelocity()
	{
		return this.projectileVelocity - this.projectileVelocitySpread;
	}

	// Token: 0x06001AF4 RID: 6900 RVA: 0x00096D04 File Offset: 0x00094F04
	public float GetMaxVelocity()
	{
		return this.projectileVelocity + this.projectileVelocitySpread;
	}

	// Token: 0x06001AF5 RID: 6901 RVA: 0x00096D14 File Offset: 0x00094F14
	public bool IsAmmo(AmmoTypes ammo)
	{
		return (this.ammoType & ammo) != 0;
	}

	// Token: 0x06001AF6 RID: 6902 RVA: 0x00096D24 File Offset: 0x00094F24
	public virtual void ServerProjectileHit(global::HitInfo info)
	{
		if (this.mods == null)
		{
			return;
		}
		foreach (global::ItemModProjectileMod itemModProjectileMod in this.mods)
		{
			if (!(itemModProjectileMod == null))
			{
				itemModProjectileMod.ServerProjectileHit(info);
			}
		}
	}

	// Token: 0x040015B3 RID: 5555
	public global::GameObjectRef projectileObject = new global::GameObjectRef();

	// Token: 0x040015B4 RID: 5556
	public global::ItemModProjectileMod[] mods;

	// Token: 0x040015B5 RID: 5557
	public AmmoTypes ammoType;

	// Token: 0x040015B6 RID: 5558
	public int numProjectiles = 1;

	// Token: 0x040015B7 RID: 5559
	public float projectileSpread;

	// Token: 0x040015B8 RID: 5560
	public float projectileVelocity = 100f;

	// Token: 0x040015B9 RID: 5561
	public float projectileVelocitySpread;

	// Token: 0x040015BA RID: 5562
	public bool useCurve;

	// Token: 0x040015BB RID: 5563
	public AnimationCurve spreadScalar;

	// Token: 0x040015BC RID: 5564
	public string category = "bullet";
}
