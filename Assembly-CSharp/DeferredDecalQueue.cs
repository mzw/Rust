﻿using System;

// Token: 0x020005F8 RID: 1528
[Serializable]
public enum DeferredDecalQueue
{
	// Token: 0x04001A34 RID: 6708
	Background,
	// Token: 0x04001A35 RID: 6709
	Foreground
}
