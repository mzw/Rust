﻿using System;
using UnityEngine;

// Token: 0x02000778 RID: 1912
public class BlendTexture : global::ProcessedTexture
{
	// Token: 0x0600238B RID: 9099 RVA: 0x000C4DF0 File Offset: 0x000C2FF0
	public BlendTexture(int width, int height, bool linear = true)
	{
		this.material = base.CreateMaterial("Hidden/BlitCopyAlpha");
		this.result = base.CreateRenderTexture("Blend Texture", width, height, linear);
	}

	// Token: 0x0600238C RID: 9100 RVA: 0x000C4E20 File Offset: 0x000C3020
	public void Blend(Texture source, Texture target, float alpha)
	{
		this.material.SetTexture("_BlendTex", target);
		this.material.SetFloat("_Alpha", Mathf.Clamp01(alpha));
		Graphics.Blit(source, this.result, this.material);
	}

	// Token: 0x0600238D RID: 9101 RVA: 0x000C4E5C File Offset: 0x000C305C
	public void CopyTo(global::BlendTexture target)
	{
		Graphics.Blit(this.result, target.result);
	}
}
