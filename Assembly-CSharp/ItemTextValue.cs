﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006B5 RID: 1717
public class ItemTextValue : MonoBehaviour
{
	// Token: 0x06002150 RID: 8528 RVA: 0x000BBF78 File Offset: 0x000BA178
	public void SetValue(float val, int numDecimals = 0, string overrideText = "")
	{
		this.text.text = ((!(overrideText == string.Empty)) ? overrideText : string.Format("{0}{1:n" + numDecimals + "}", (val <= 0f || !this.signed) ? string.Empty : "+", val));
		if (this.asPercentage)
		{
			Text text = this.text;
			text.text += " %";
		}
		if (this.suffix != string.Empty)
		{
			Text text2 = this.text;
			text2.text += this.suffix;
		}
		bool flag = val > 0f;
		if (this.negativestat)
		{
			flag = !flag;
		}
		if (this.useColors)
		{
			this.text.color = ((!flag) ? this.bad : this.good);
		}
	}

	// Token: 0x04001D15 RID: 7445
	public Text text;

	// Token: 0x04001D16 RID: 7446
	public Color bad;

	// Token: 0x04001D17 RID: 7447
	public Color good;

	// Token: 0x04001D18 RID: 7448
	public bool negativestat;

	// Token: 0x04001D19 RID: 7449
	public bool asPercentage;

	// Token: 0x04001D1A RID: 7450
	public bool useColors = true;

	// Token: 0x04001D1B RID: 7451
	public bool signed = true;

	// Token: 0x04001D1C RID: 7452
	public string suffix;
}
