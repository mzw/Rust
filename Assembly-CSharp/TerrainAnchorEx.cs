﻿using System;
using UnityEngine;

// Token: 0x02000558 RID: 1368
public static class TerrainAnchorEx
{
	// Token: 0x06001CAA RID: 7338 RVA: 0x000A0A08 File Offset: 0x0009EC08
	public static bool ApplyTerrainAnchors(this Transform transform, global::TerrainAnchor[] anchors, ref Vector3 pos, Quaternion rot, Vector3 scale, global::SpawnFilter filter = null)
	{
		return transform.ApplyTerrainAnchors(anchors, ref pos, rot, scale, global::TerrainAnchorMode.MinimizeError, filter);
	}

	// Token: 0x06001CAB RID: 7339 RVA: 0x000A0A18 File Offset: 0x0009EC18
	public static bool ApplyTerrainAnchors(this Transform transform, global::TerrainAnchor[] anchors, ref Vector3 pos, Quaternion rot, Vector3 scale, global::TerrainAnchorMode mode, global::SpawnFilter filter = null)
	{
		if (anchors.Length <= 0)
		{
			return true;
		}
		float num = 0f;
		float num2 = float.MinValue;
		float num3 = float.MaxValue;
		foreach (global::TerrainAnchor terrainAnchor in anchors)
		{
			Vector3 vector = rot * Vector3.Scale(terrainAnchor.worldPosition, scale);
			Vector3 vector2 = pos + vector;
			if (global::TerrainMeta.OutOfBounds(vector2))
			{
				return false;
			}
			if (filter != null && filter.GetFactor(vector2) == 0f)
			{
				return false;
			}
			float num4;
			float num5;
			float num6;
			terrainAnchor.Apply(out num4, out num5, out num6, vector2);
			num += num4 - vector.y;
			num2 = Mathf.Max(num2, num5 - vector.y);
			num3 = Mathf.Min(num3, num6 - vector.y);
			if (num3 < num2)
			{
				return false;
			}
		}
		if (mode == global::TerrainAnchorMode.MinimizeError)
		{
			pos.y = Mathf.Clamp(num / (float)anchors.Length, num2, num3);
		}
		else
		{
			pos.y = Mathf.Clamp(pos.y, num2, num3);
		}
		return true;
	}

	// Token: 0x06001CAC RID: 7340 RVA: 0x000A0B24 File Offset: 0x0009ED24
	public static void ApplyTerrainAnchors(this Transform transform, global::TerrainAnchor[] anchors)
	{
		Vector3 position = transform.position;
		transform.ApplyTerrainAnchors(anchors, ref position, transform.rotation, transform.lossyScale, null);
		transform.position = position;
	}
}
