﻿using System;
using UnityEngine.UI;

// Token: 0x0200067D RID: 1661
public class KeyCodeEntry : global::UIDialog
{
	// Token: 0x04001C2D RID: 7213
	public Text textDisplay;

	// Token: 0x04001C2E RID: 7214
	public Action<string> onCodeEntered;

	// Token: 0x04001C2F RID: 7215
	public Text typeDisplay;

	// Token: 0x04001C30 RID: 7216
	public global::Translate.Phrase masterCodePhrase;

	// Token: 0x04001C31 RID: 7217
	public global::Translate.Phrase guestCodePhrase;
}
