﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000AA RID: 170
public class WorldItem : global::BaseEntity
{
	// Token: 0x06000A59 RID: 2649 RVA: 0x000476B4 File Offset: 0x000458B4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("WorldItem.OnRpcMessage", 0.1f))
		{
			if (rpc == 3306490492u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Pickup ");
				}
				using (TimeWarning.New("Pickup", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("Pickup", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Pickup(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in Pickup");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000A5A RID: 2650 RVA: 0x00047894 File Offset: 0x00045A94
	public override global::Item GetItem()
	{
		return this.item;
	}

	// Token: 0x06000A5B RID: 2651 RVA: 0x0004789C File Offset: 0x00045A9C
	public void InitializeItem(global::Item in_item)
	{
		if (this.item != null)
		{
			this.RemoveItem();
		}
		this.item = in_item;
		if (this.item == null)
		{
			return;
		}
		this.item.OnDirty += this.OnItemDirty;
		base.name = this.item.info.shortname + " (world)";
		this.item.SetWorldEntity(this);
		this.OnItemDirty(this.item);
	}

	// Token: 0x06000A5C RID: 2652 RVA: 0x00047920 File Offset: 0x00045B20
	public void RemoveItem()
	{
		if (this.item == null)
		{
			return;
		}
		this.item.OnDirty -= this.OnItemDirty;
		this.item = null;
	}

	// Token: 0x06000A5D RID: 2653 RVA: 0x00047950 File Offset: 0x00045B50
	public void DestroyItem()
	{
		if (this.item == null)
		{
			return;
		}
		this.item.OnDirty -= this.OnItemDirty;
		this.item.Remove(0f);
		this.item = null;
	}

	// Token: 0x06000A5E RID: 2654 RVA: 0x00047990 File Offset: 0x00045B90
	protected virtual void OnItemDirty(global::Item in_item)
	{
		Assert.IsTrue(this.item == in_item, "WorldItem:OnItemDirty - dirty item isn't ours!");
		if (this.item != null)
		{
			base.BroadcastMessage("OnItemChanged", this.item, 1);
		}
		this.DoItemNetworking();
	}

	// Token: 0x06000A5F RID: 2655 RVA: 0x000479C8 File Offset: 0x00045BC8
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.worldItem == null)
		{
			return;
		}
		if (info.msg.worldItem.item == null)
		{
			return;
		}
		global::Item item = global::ItemManager.Load(info.msg.worldItem.item, this.item, base.isServer);
		if (item != null)
		{
			this.InitializeItem(item);
		}
	}

	// Token: 0x17000069 RID: 105
	// (get) Token: 0x06000A60 RID: 2656 RVA: 0x00047A38 File Offset: 0x00045C38
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			if (this.item != null)
			{
				return this.item.Traits;
			}
			return base.Traits;
		}
	}

	// Token: 0x06000A61 RID: 2657 RVA: 0x00047A58 File Offset: 0x00045C58
	public override void Eat(global::BaseNpc baseNpc, float timeSpent)
	{
		if (this.eatSeconds <= 0f)
		{
			return;
		}
		this.eatSeconds -= timeSpent;
		baseNpc.AddCalories(this.caloriesPerSecond * timeSpent);
		if (this.eatSeconds < 0f)
		{
			this.DestroyItem();
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
	}

	// Token: 0x06000A62 RID: 2658 RVA: 0x00047AB0 File Offset: 0x00045CB0
	public override string ToString()
	{
		if (this._name == null)
		{
			if (base.isServer)
			{
				this._name = string.Format("{1}[{0}] {2}", (this.net == null) ? 0u : this.net.ID, base.ShortPrefabName, base.name);
			}
			else
			{
				this._name = base.ShortPrefabName;
			}
		}
		return this._name;
	}

	// Token: 0x06000A63 RID: 2659 RVA: 0x00047B28 File Offset: 0x00045D28
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.item != null)
		{
			base.BroadcastMessage("OnItemChanged", this.item, 1);
		}
	}

	// Token: 0x06000A64 RID: 2660 RVA: 0x00047B50 File Offset: 0x00045D50
	private void DoItemNetworking()
	{
		if (this._isInvokingSendItemUpdate)
		{
			return;
		}
		this._isInvokingSendItemUpdate = true;
		base.Invoke(new Action(this.SendItemUpdate), 0.1f);
	}

	// Token: 0x06000A65 RID: 2661 RVA: 0x00047B7C File Offset: 0x00045D7C
	private void SendItemUpdate()
	{
		this._isInvokingSendItemUpdate = false;
		if (this.item == null)
		{
			return;
		}
		using (UpdateItem updateItem = Facepunch.Pool.Get<UpdateItem>())
		{
			updateItem.item = this.item.Save(false, false);
			base.ClientRPC<UpdateItem>(null, "UpdateItem", updateItem);
		}
	}

	// Token: 0x06000A66 RID: 2662 RVA: 0x00047BE4 File Offset: 0x00045DE4
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void Pickup(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (this.item == null)
		{
			return;
		}
		if (!this.allowPickup)
		{
			return;
		}
		if (Interface.CallHook("OnItemPickup", new object[]
		{
			this.item,
			msg.player
		}) != null)
		{
			return;
		}
		base.ClientRPC(null, "PickupSound");
		msg.player.GiveItem(this.item, global::BaseEntity.GiveItemReason.PickedUp);
		msg.player.SignalBroadcast(global::BaseEntity.Signal.Gesture, "pickup_item", null);
	}

	// Token: 0x06000A67 RID: 2663 RVA: 0x00047C78 File Offset: 0x00045E78
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (this.item == null)
		{
			return;
		}
		bool forDisk = info.forDisk;
		info.msg.worldItem = Facepunch.Pool.Get<ProtoBuf.WorldItem>();
		info.msg.worldItem.item = this.item.Save(forDisk, false);
	}

	// Token: 0x06000A68 RID: 2664 RVA: 0x00047CD0 File Offset: 0x00045ED0
	public override void OnInvalidPosition()
	{
		this.DestroyItem();
		base.OnInvalidPosition();
	}

	// Token: 0x06000A69 RID: 2665 RVA: 0x00047CE0 File Offset: 0x00045EE0
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		this.RemoveItem();
	}

	// Token: 0x06000A6A RID: 2666 RVA: 0x00047CF0 File Offset: 0x00045EF0
	public override void ParentBecoming(global::BaseEntity ent)
	{
		base.SetParent(ent, this.parentBone);
	}

	// Token: 0x040004E3 RID: 1251
	[Header("WorldItem")]
	public bool allowPickup = true;

	// Token: 0x040004E4 RID: 1252
	[NonSerialized]
	public global::Item item;

	// Token: 0x040004E5 RID: 1253
	protected float eatSeconds = 10f;

	// Token: 0x040004E6 RID: 1254
	protected float caloriesPerSecond = 1f;

	// Token: 0x040004E7 RID: 1255
	private bool _isInvokingSendItemUpdate;
}
