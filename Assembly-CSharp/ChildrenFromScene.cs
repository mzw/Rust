﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x0200066C RID: 1644
public class ChildrenFromScene : MonoBehaviour
{
	// Token: 0x060020B8 RID: 8376 RVA: 0x000BA684 File Offset: 0x000B8884
	private IEnumerator Start()
	{
		Scene scene = SceneManager.GetSceneByName(this.SceneName);
		if (!scene.isLoaded)
		{
			yield return SceneManager.LoadSceneAsync(this.SceneName, 1);
		}
		scene = SceneManager.GetSceneByName(this.SceneName);
		GameObject[] objects = scene.GetRootGameObjects();
		foreach (GameObject gameObject in objects)
		{
			gameObject.transform.SetParent(base.transform, false);
			gameObject.Identity();
			RectTransform rectTransform = gameObject.transform as RectTransform;
			if (rectTransform)
			{
				rectTransform.pivot = Vector2.zero;
				rectTransform.anchoredPosition = Vector2.zero;
				rectTransform.anchorMin = Vector2.zero;
				rectTransform.anchorMax = Vector2.one;
				rectTransform.sizeDelta = Vector2.one;
			}
			foreach (SingletonComponent singletonComponent in gameObject.GetComponentsInChildren<SingletonComponent>(true))
			{
				singletonComponent.Setup();
			}
			if (this.StartChildrenDisabled)
			{
				gameObject.SetActive(false);
			}
		}
		SceneManager.UnloadSceneAsync(scene);
		yield break;
	}

	// Token: 0x04001C0B RID: 7179
	public string SceneName;

	// Token: 0x04001C0C RID: 7180
	public bool StartChildrenDisabled;
}
