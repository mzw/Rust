﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000713 RID: 1811
public class TweakUIMultiSelect : MonoBehaviour
{
	// Token: 0x0600226B RID: 8811 RVA: 0x000C0C30 File Offset: 0x000BEE30
	protected void Awake()
	{
		this.conVar = ConsoleSystem.Index.Client.Find(this.convarName);
		if (this.conVar != null)
		{
			this.UpdateToggleGroup();
		}
		else
		{
			Debug.LogWarning("Tweak Slider Convar Missing: " + this.convarName);
		}
	}

	// Token: 0x0600226C RID: 8812 RVA: 0x000C0C70 File Offset: 0x000BEE70
	protected void OnEnable()
	{
		this.UpdateToggleGroup();
	}

	// Token: 0x0600226D RID: 8813 RVA: 0x000C0C78 File Offset: 0x000BEE78
	public void OnChanged()
	{
		this.UpdateConVar();
	}

	// Token: 0x0600226E RID: 8814 RVA: 0x000C0C80 File Offset: 0x000BEE80
	private void UpdateToggleGroup()
	{
		if (this.conVar == null)
		{
			return;
		}
		string @string = this.conVar.String;
		foreach (Toggle toggle in this.toggleGroup.GetComponentsInChildren<Toggle>())
		{
			toggle.isOn = (toggle.name == @string);
		}
	}

	// Token: 0x0600226F RID: 8815 RVA: 0x000C0CDC File Offset: 0x000BEEDC
	private void UpdateConVar()
	{
		if (this.conVar == null)
		{
			return;
		}
		Toggle toggle = (from x in this.toggleGroup.GetComponentsInChildren<Toggle>()
		where x.isOn
		select x).FirstOrDefault<Toggle>();
		if (toggle == null)
		{
			return;
		}
		if (this.conVar.String == toggle.name)
		{
			return;
		}
		this.conVar.Set(toggle.name);
	}

	// Token: 0x04001EDD RID: 7901
	public ToggleGroup toggleGroup;

	// Token: 0x04001EDE RID: 7902
	public string convarName = "effects.motionblur";

	// Token: 0x04001EDF RID: 7903
	internal ConsoleSystem.Command conVar;
}
