﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using UnityEngine;

// Token: 0x020003A9 RID: 937
public class ServerGib : global::BaseCombatEntity
{
	// Token: 0x06001644 RID: 5700 RVA: 0x000810E4 File Offset: 0x0007F2E4
	public override float BoundsPadding()
	{
		return 3f;
	}

	// Token: 0x06001645 RID: 5701 RVA: 0x000810EC File Offset: 0x0007F2EC
	public static List<global::ServerGib> CreateGibs(string entityToCreatePath, GameObject creator, GameObject gibSource, Vector3 inheritVelocity, float spreadVelocity)
	{
		List<global::ServerGib> list = new List<global::ServerGib>();
		foreach (MeshRenderer meshRenderer in gibSource.GetComponentsInChildren<MeshRenderer>(true))
		{
			MeshFilter component = meshRenderer.GetComponent<MeshFilter>();
			Vector3 normalized = meshRenderer.transform.localPosition.normalized;
			Vector3 vector = creator.transform.localToWorldMatrix.MultiplyPoint(meshRenderer.transform.localPosition) + normalized * 0.5f;
			Quaternion quaternion = creator.transform.rotation * meshRenderer.transform.localRotation;
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(entityToCreatePath, vector, quaternion, true);
			if (baseEntity)
			{
				global::ServerGib component2 = baseEntity.GetComponent<global::ServerGib>();
				component2.transform.position = vector;
				component2.transform.rotation = quaternion;
				component2._gibName = meshRenderer.name;
				component2.PhysicsInit(component.sharedMesh);
				Vector3 vector2 = meshRenderer.transform.localPosition.normalized * spreadVelocity;
				component2.rigidBody.velocity = inheritVelocity + vector2;
				component2.rigidBody.angularVelocity = Vector3Ex.Range(-1f, 1f).normalized * 1f;
				component2.rigidBody.WakeUp();
				component2.Spawn();
				list.Add(component2);
			}
		}
		foreach (global::ServerGib serverGib in list)
		{
			foreach (global::ServerGib serverGib2 in list)
			{
				if (!(serverGib == serverGib2))
				{
					Physics.IgnoreCollision(serverGib2.GetCollider(), serverGib.GetCollider(), true);
				}
			}
		}
		return list;
	}

	// Token: 0x06001646 RID: 5702 RVA: 0x00081314 File Offset: 0x0007F514
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (!info.forDisk && this._gibName != string.Empty)
		{
			info.msg.servergib = new ProtoBuf.ServerGib();
			info.msg.servergib.gibName = this._gibName;
		}
	}

	// Token: 0x06001647 RID: 5703 RVA: 0x00081374 File Offset: 0x0007F574
	public MeshCollider GetCollider()
	{
		return this.meshCollider;
	}

	// Token: 0x06001648 RID: 5704 RVA: 0x0008137C File Offset: 0x0007F57C
	public override void ServerInit()
	{
		base.ServerInit();
		base.Invoke(new Action(this.RemoveMe), 1800f);
	}

	// Token: 0x06001649 RID: 5705 RVA: 0x0008139C File Offset: 0x0007F59C
	public void RemoveMe()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x0600164A RID: 5706 RVA: 0x000813A8 File Offset: 0x0007F5A8
	public virtual void PhysicsInit(Mesh mesh)
	{
		this.meshCollider = base.gameObject.AddComponent<MeshCollider>();
		this.meshCollider.sharedMesh = mesh;
		this.meshCollider.convex = true;
		this.meshCollider.material = this.physicsMaterial;
		Rigidbody rigidbody = base.gameObject.AddComponent<Rigidbody>();
		rigidbody.useGravity = true;
		rigidbody.mass = Mathf.Clamp(this.meshCollider.bounds.size.magnitude * this.meshCollider.bounds.size.magnitude * 20f, 10f, 2000f);
		rigidbody.interpolation = 1;
		if (base.isServer)
		{
			rigidbody.drag = 0.1f;
			rigidbody.angularDrag = 0.1f;
		}
		this.rigidBody = rigidbody;
		base.gameObject.layer = LayerMask.NameToLayer("Default");
		if (base.isClient)
		{
			rigidbody.isKinematic = true;
		}
	}

	// Token: 0x040010C6 RID: 4294
	public GameObject _gibSource;

	// Token: 0x040010C7 RID: 4295
	public string _gibName;

	// Token: 0x040010C8 RID: 4296
	public PhysicMaterial physicsMaterial;

	// Token: 0x040010C9 RID: 4297
	private MeshCollider meshCollider;

	// Token: 0x040010CA RID: 4298
	private Rigidbody rigidBody;
}
