﻿using System;
using Rust;
using UnityEngine;

// Token: 0x0200048F RID: 1167
public class SpawnPointInstance : MonoBehaviour
{
	// Token: 0x06001963 RID: 6499 RVA: 0x0008EDF0 File Offset: 0x0008CFF0
	public void Notify()
	{
		if (this.parentSpawnGroup)
		{
			this.parentSpawnGroup.ObjectSpawned(this);
		}
		if (this.parentSpawnPoint)
		{
			this.parentSpawnPoint.ObjectSpawned(this);
		}
	}

	// Token: 0x06001964 RID: 6500 RVA: 0x0008EE2C File Offset: 0x0008D02C
	protected void OnDestroy()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (this.parentSpawnGroup)
		{
			this.parentSpawnGroup.ObjectRetired(this);
		}
		if (this.parentSpawnPoint)
		{
			this.parentSpawnPoint.ObjectRetired(this);
		}
	}

	// Token: 0x0400140E RID: 5134
	internal global::SpawnGroup parentSpawnGroup;

	// Token: 0x0400140F RID: 5135
	internal global::BaseSpawnPoint parentSpawnPoint;
}
