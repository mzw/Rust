﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

// Token: 0x020005C8 RID: 1480
public class PlaceRoadObjects : global::ProceduralComponent
{
	// Token: 0x06001E9D RID: 7837 RVA: 0x000AC364 File Offset: 0x000AA564
	public override void Process(uint seed)
	{
		List<global::PathList> roads = global::TerrainMeta.Path.Roads;
		foreach (global::PathList pathList in roads)
		{
			foreach (global::PathList.BasicObject obj in this.Start)
			{
				pathList.TrimStart(obj);
			}
			foreach (global::PathList.BasicObject obj2 in this.End)
			{
				pathList.TrimEnd(obj2);
			}
			foreach (global::PathList.BasicObject obj3 in this.Start)
			{
				pathList.SpawnStart(ref seed, obj3);
			}
			foreach (global::PathList.BasicObject obj4 in this.End)
			{
				pathList.SpawnEnd(ref seed, obj4);
			}
			foreach (global::PathList.PathObject obj5 in this.Path)
			{
				pathList.SpawnAlong(ref seed, obj5);
			}
			foreach (global::PathList.SideObject obj6 in this.Side)
			{
				pathList.SpawnSide(ref seed, obj6);
			}
			pathList.ResetTrims();
		}
	}

	// Token: 0x04001975 RID: 6517
	public global::PathList.BasicObject[] Start;

	// Token: 0x04001976 RID: 6518
	public global::PathList.BasicObject[] End;

	// Token: 0x04001977 RID: 6519
	[FormerlySerializedAs("RoadsideObjects")]
	public global::PathList.SideObject[] Side;

	// Token: 0x04001978 RID: 6520
	[FormerlySerializedAs("RoadObjects")]
	public global::PathList.PathObject[] Path;
}
