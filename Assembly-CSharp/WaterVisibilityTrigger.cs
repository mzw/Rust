﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x020005F4 RID: 1524
public class WaterVisibilityTrigger : global::EnvironmentVolumeTrigger
{
	// Token: 0x06001F29 RID: 7977 RVA: 0x000B10B4 File Offset: 0x000AF2B4
	public static void Reset()
	{
		global::WaterVisibilityTrigger.ticks = 1L;
		global::WaterVisibilityTrigger.tracker.Clear();
	}

	// Token: 0x06001F2A RID: 7978 RVA: 0x000B10C8 File Offset: 0x000AF2C8
	protected void OnDestroy()
	{
		if (Application.isQuitting)
		{
			return;
		}
		global::WaterVisibilityTrigger.tracker.Remove(this.enteredTick);
	}

	// Token: 0x06001F2B RID: 7979 RVA: 0x000B10E8 File Offset: 0x000AF2E8
	private int GetVisibilityMask()
	{
		return 0;
	}

	// Token: 0x06001F2C RID: 7980 RVA: 0x000B10EC File Offset: 0x000AF2EC
	private void ToggleVisibility()
	{
	}

	// Token: 0x06001F2D RID: 7981 RVA: 0x000B10F0 File Offset: 0x000AF2F0
	private void ResetVisibility()
	{
	}

	// Token: 0x06001F2E RID: 7982 RVA: 0x000B10F4 File Offset: 0x000AF2F4
	private void ToggleCollision(Collider other)
	{
		if (global::Water.Collision != null)
		{
			global::Water.Collision.SetIgnore(other, base.volume.trigger, true);
		}
	}

	// Token: 0x06001F2F RID: 7983 RVA: 0x000B1120 File Offset: 0x000AF320
	private void ResetCollision(Collider other)
	{
		if (global::Water.Collision != null)
		{
			global::Water.Collision.SetIgnore(other, base.volume.trigger, false);
		}
	}

	// Token: 0x06001F30 RID: 7984 RVA: 0x000B114C File Offset: 0x000AF34C
	protected void OnTriggerEnter(Collider other)
	{
		bool flag = other.gameObject.GetComponent<global::PlayerWalkMovement>() != null;
		bool flag2 = other.gameObject.CompareTag("MainCamera");
		if ((flag || flag2) && !global::WaterVisibilityTrigger.tracker.ContainsValue(this))
		{
			long num = global::WaterVisibilityTrigger.ticks;
			global::WaterVisibilityTrigger.ticks = num + 1L;
			this.enteredTick = num;
			global::WaterVisibilityTrigger.tracker.Add(this.enteredTick, this);
			this.ToggleVisibility();
		}
		if (!flag2 && !other.isTrigger)
		{
			this.ToggleCollision(other);
		}
	}

	// Token: 0x06001F31 RID: 7985 RVA: 0x000B11DC File Offset: 0x000AF3DC
	protected void OnTriggerExit(Collider other)
	{
		bool flag = other.gameObject.GetComponent<global::PlayerWalkMovement>() != null;
		bool flag2 = other.gameObject.CompareTag("MainCamera");
		if ((flag || flag2) && global::WaterVisibilityTrigger.tracker.ContainsValue(this))
		{
			global::WaterVisibilityTrigger.tracker.Remove(this.enteredTick);
			if (global::WaterVisibilityTrigger.tracker.Count > 0)
			{
				global::WaterVisibilityTrigger.tracker.Values[global::WaterVisibilityTrigger.tracker.Count - 1].ToggleVisibility();
			}
			else
			{
				this.ResetVisibility();
			}
		}
		if (!flag2 && !other.isTrigger)
		{
			this.ResetCollision(other);
		}
	}

	// Token: 0x04001A2C RID: 6700
	private long enteredTick;

	// Token: 0x04001A2D RID: 6701
	private static long ticks = 1L;

	// Token: 0x04001A2E RID: 6702
	private static SortedList<long, global::WaterVisibilityTrigger> tracker = new SortedList<long, global::WaterVisibilityTrigger>();
}
