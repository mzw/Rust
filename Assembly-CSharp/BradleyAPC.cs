﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using Rust.AI;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x02000053 RID: 83
public class BradleyAPC : global::BaseCombatEntity
{
	// Token: 0x06000693 RID: 1683 RVA: 0x000283A4 File Offset: 0x000265A4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BradleyAPC.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000694 RID: 1684 RVA: 0x000283EC File Offset: 0x000265EC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.bradley != null && !info.fromDisk)
		{
			this.throttle = info.msg.bradley.engineThrottle;
			this.rightThrottle = info.msg.bradley.throttleRight;
			this.leftThrottle = info.msg.bradley.throttleLeft;
			this.desiredAimVector = info.msg.bradley.mainGunVec;
			this.desiredTopTurretAimVector = info.msg.bradley.topTurretVec;
		}
	}

	// Token: 0x06000695 RID: 1685 RVA: 0x00028490 File Offset: 0x00026690
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (!info.forDisk)
		{
			info.msg.bradley = Facepunch.Pool.Get<ProtoBuf.BradleyAPC>();
			info.msg.bradley.engineThrottle = this.throttle;
			info.msg.bradley.throttleLeft = this.leftThrottle;
			info.msg.bradley.throttleRight = this.rightThrottle;
			info.msg.bradley.mainGunVec = this.turretAimVector;
			info.msg.bradley.topTurretVec = this.topTurretAimVector;
		}
	}

	// Token: 0x06000696 RID: 1686 RVA: 0x00028534 File Offset: 0x00026734
	public void SetDestination(Vector3 dest)
	{
		this.destination = dest;
	}

	// Token: 0x06000697 RID: 1687 RVA: 0x00028540 File Offset: 0x00026740
	public override void ServerInit()
	{
		base.ServerInit();
		this.Initialize();
		base.InvokeRepeating(new Action(this.UpdateTargetList), 0f, 2f);
		base.InvokeRepeating(new Action(this.UpdateTargetVisibilities), 0f, global::BradleyAPC.sightUpdateRate);
	}

	// Token: 0x06000698 RID: 1688 RVA: 0x00028594 File Offset: 0x00026794
	public override void OnCollision(Collision collision, global::BaseEntity hitEntity)
	{
	}

	// Token: 0x06000699 RID: 1689 RVA: 0x00028598 File Offset: 0x00026798
	public void Initialize()
	{
		if (Interface.CallHook("OnBradleyApcInitialize", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		this.myRigidBody.centerOfMass = this.centerOfMass.localPosition;
		this.destination = base.transform.position;
		this.finalDestination = base.transform.position;
	}

	// Token: 0x0600069A RID: 1690 RVA: 0x000285F8 File Offset: 0x000267F8
	public global::BasePlayer FollowPlayer()
	{
		foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
		{
			if (basePlayer.IsAdmin && basePlayer.IsAlive() && !basePlayer.IsSleeping() && basePlayer.GetActiveItem() != null && basePlayer.GetActiveItem().info.shortname == "tool.binoculars")
			{
				return basePlayer;
			}
		}
		return null;
	}

	// Token: 0x0600069B RID: 1691 RVA: 0x000286A4 File Offset: 0x000268A4
	public static Vector3 Direction2D(Vector3 aimAt, Vector3 aimFrom)
	{
		return (new Vector3(aimAt.x, 0f, aimAt.z) - new Vector3(aimFrom.x, 0f, aimFrom.z)).normalized;
	}

	// Token: 0x17000059 RID: 89
	// (get) Token: 0x0600069C RID: 1692 RVA: 0x000286F0 File Offset: 0x000268F0
	protected override float PositionTickRate
	{
		get
		{
			return 0.1f;
		}
	}

	// Token: 0x0600069D RID: 1693 RVA: 0x000286F8 File Offset: 0x000268F8
	public bool IsAtDestination()
	{
		return Vector3Ex.Distance2D(base.transform.position, this.destination) <= this.stoppingDist;
	}

	// Token: 0x0600069E RID: 1694 RVA: 0x0002871C File Offset: 0x0002691C
	public bool IsAtFinalDestination()
	{
		return Vector3Ex.Distance2D(base.transform.position, this.finalDestination) <= this.stoppingDist;
	}

	// Token: 0x0600069F RID: 1695 RVA: 0x00028740 File Offset: 0x00026940
	public Vector3 ClosestPointAlongPath(Vector3 start, Vector3 end, Vector3 fromPos)
	{
		Vector3 vector = end - start;
		Vector3 vector2 = fromPos - start;
		float num = Vector3.Dot(vector, vector2);
		float num2 = Vector3.SqrMagnitude(end - start);
		float num3 = Mathf.Clamp01(num / num2);
		return start + vector * num3;
	}

	// Token: 0x060006A0 RID: 1696 RVA: 0x00028798 File Offset: 0x00026998
	public void FireGunTest()
	{
		if (UnityEngine.Time.time < this.nextFireTime)
		{
			return;
		}
		this.nextFireTime = UnityEngine.Time.time + 0.25f;
		this.numBursted++;
		if (this.numBursted >= 4)
		{
			this.nextFireTime = UnityEngine.Time.time + 5f;
			this.numBursted = 0;
		}
		Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(2f, this.CannonMuzzle.rotation * Vector3.forward, true);
		Vector3 normalized = (this.CannonPitch.transform.rotation * Vector3.back + base.transform.up * -1f).normalized;
		this.myRigidBody.AddForceAtPosition(normalized * this.recoilScale, this.CannonPitch.transform.position, 1);
		global::Effect.server.Run(this.mainCannonMuzzleFlash.resourcePath, this, global::StringPool.Get(this.CannonMuzzle.gameObject.name), Vector3.zero, Vector3.zero, null, false);
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.mainCannonProjectile.resourcePath, this.CannonMuzzle.transform.position, Quaternion.LookRotation(modifiedAimConeDirection), true);
		if (baseEntity == null)
		{
			return;
		}
		baseEntity.SendMessage("InitializeVelocity", modifiedAimConeDirection);
		baseEntity.Spawn();
	}

	// Token: 0x060006A1 RID: 1697 RVA: 0x00028904 File Offset: 0x00026B04
	public void InstallPatrolPath(global::BasePath path)
	{
		this.patrolPath = path;
		this.currentPath = new List<Vector3>();
		this.currentPathIndex = -1;
	}

	// Token: 0x060006A2 RID: 1698 RVA: 0x00028920 File Offset: 0x00026B20
	public void UpdateMovement_Patrol()
	{
		if (this.patrolPath == null)
		{
			return;
		}
		if (UnityEngine.Time.time < this.nextPatrolTime)
		{
			return;
		}
		this.nextPatrolTime = UnityEngine.Time.time + 20f;
		if (this.HasPath() && !this.IsAtFinalDestination())
		{
			return;
		}
		if (Interface.CallHook("OnBradleyApcPatrol", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		global::PathInterestNode randomInterestNodeAwayFrom = this.patrolPath.GetRandomInterestNodeAwayFrom(base.transform.position, 10f);
		global::BasePathNode closestToPoint = this.patrolPath.GetClosestToPoint(randomInterestNodeAwayFrom.transform.position);
		bool flag = false;
		List<global::BasePathNode> list = Facepunch.Pool.GetList<global::BasePathNode>();
		global::BasePathNode basePathNode;
		if (this.GetEngagementPath(ref list))
		{
			flag = true;
			basePathNode = list[list.Count - 1];
		}
		else
		{
			basePathNode = this.patrolPath.GetClosestToPoint(base.transform.position);
		}
		if (Vector3.Distance(this.finalDestination, closestToPoint.transform.position) > 2f)
		{
			Stack<global::BasePathNode> stack;
			float num;
			if (closestToPoint == basePathNode)
			{
				this.currentPath.Clear();
				this.currentPath.Add(closestToPoint.transform.position);
				this.currentPathIndex = -1;
				this.pathLooping = false;
				this.finalDestination = closestToPoint.transform.position;
			}
			else if (Rust.AI.AStarPath.FindPath(basePathNode, closestToPoint, out stack, out num))
			{
				this.currentPath.Clear();
				if (flag)
				{
					for (int i = 0; i < list.Count - 1; i++)
					{
						this.currentPath.Add(list[i].transform.position);
					}
				}
				foreach (global::BasePathNode basePathNode2 in stack)
				{
					this.currentPath.Add(basePathNode2.transform.position);
				}
				this.currentPathIndex = -1;
				this.pathLooping = false;
				this.finalDestination = closestToPoint.transform.position;
			}
		}
	}

	// Token: 0x060006A3 RID: 1699 RVA: 0x00028B5C File Offset: 0x00026D5C
	public void UpdateMovement_Hunt()
	{
		if (Interface.CallHook("OnBradleyApcHunt", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		if (this.patrolPath == null)
		{
			return;
		}
		global::BradleyAPC.TargetInfo targetInfo = this.targetList[0];
		if (targetInfo.IsValid())
		{
			if (this.HasPath() && targetInfo.IsVisible())
			{
				if (this.currentPath.Count > 1)
				{
					Vector3 item = this.currentPath[this.currentPathIndex];
					this.ClearPath();
					this.currentPath.Add(item);
					this.finalDestination = item;
					this.currentPathIndex = 0;
				}
			}
			else if (UnityEngine.Time.time > this.nextEngagementPathTime && !this.HasPath() && !targetInfo.IsVisible())
			{
				bool flag = false;
				global::BasePathNode start = this.patrolPath.GetClosestToPoint(base.transform.position);
				List<global::BasePathNode> list = Facepunch.Pool.GetList<global::BasePathNode>();
				if (this.GetEngagementPath(ref list))
				{
					flag = true;
					start = list[list.Count - 1];
				}
				global::BasePathNode basePathNode = null;
				List<global::BasePathNode> list2 = Facepunch.Pool.GetList<global::BasePathNode>();
				this.patrolPath.GetNodesNear(targetInfo.lastSeenPosition, ref list2, 30f);
				Stack<global::BasePathNode> stack = null;
				float num = float.PositiveInfinity;
				float y = this.mainTurretEyePos.localPosition.y;
				foreach (global::BasePathNode basePathNode2 in list2)
				{
					Stack<global::BasePathNode> stack2 = new Stack<global::BasePathNode>();
					if (targetInfo.entity.IsVisible(basePathNode2.transform.position + new Vector3(0f, y, 0f)))
					{
						float num2;
						if (Rust.AI.AStarPath.FindPath(start, basePathNode2, out stack2, out num2) && num2 < num)
						{
							stack = stack2;
							num = num2;
							basePathNode = basePathNode2;
						}
					}
				}
				if (stack != null)
				{
					this.currentPath.Clear();
					if (flag)
					{
						for (int i = 0; i < list.Count - 1; i++)
						{
							this.currentPath.Add(list[i].transform.position);
						}
					}
					foreach (global::BasePathNode basePathNode3 in stack)
					{
						this.currentPath.Add(basePathNode3.transform.position);
					}
					this.currentPathIndex = -1;
					this.pathLooping = false;
					this.finalDestination = basePathNode.transform.position;
				}
				Facepunch.Pool.FreeList<global::BasePathNode>(ref list2);
				Facepunch.Pool.FreeList<global::BasePathNode>(ref list);
				this.nextEngagementPathTime = UnityEngine.Time.time + 5f;
			}
		}
	}

	// Token: 0x060006A4 RID: 1700 RVA: 0x00028E4C File Offset: 0x0002704C
	public void DoSimpleAI()
	{
		if (base.isClient)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved5, TOD_Sky.Instance.IsNight, false);
		if (!this.DoAI)
		{
			return;
		}
		if (this.targetList.Count > 0)
		{
			if (this.targetList[0].IsValid() && this.targetList[0].IsVisible())
			{
				this.mainGunTarget = (this.targetList[0].entity as global::BaseCombatEntity);
			}
			else
			{
				this.mainGunTarget = null;
			}
			this.UpdateMovement_Hunt();
		}
		else
		{
			this.mainGunTarget = null;
			this.UpdateMovement_Patrol();
		}
		this.AdvancePathMovement();
		float num = Vector3.Distance(base.transform.position, this.destination);
		float num2 = Vector3.Distance(base.transform.position, this.finalDestination);
		if (num > this.stoppingDist)
		{
			Vector3 vector = global::BradleyAPC.Direction2D(this.destination, base.transform.position);
			float num3 = Vector3.Dot(vector, base.transform.right);
			float num4 = Vector3.Dot(vector, base.transform.right);
			float num5 = Vector3.Dot(vector, -base.transform.right);
			float num6 = Vector3.Dot(vector, -base.transform.forward);
			if (num6 > num3)
			{
				if (num4 >= num5)
				{
					this.turning = 1f;
				}
				else
				{
					this.turning = -1f;
				}
			}
			else
			{
				this.turning = Mathf.Clamp(num3 * 3f, -1f, 1f);
			}
			float num7 = 1f - Mathf.InverseLerp(0f, 0.3f, Mathf.Abs(this.turning));
			float num8 = Mathf.InverseLerp(0.1f, 0.4f, Vector3.Dot(base.transform.forward, Vector3.up));
			this.throttle = (0.1f + Mathf.InverseLerp(0f, 20f, num2) * 1f) * num7 + num8;
		}
		this.DoWeaponAiming();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060006A5 RID: 1701 RVA: 0x0002907C File Offset: 0x0002727C
	public void FixedUpdate()
	{
		this.DoSimpleAI();
		this.DoPhysicsMove();
		this.DoWeapons();
		this.DoHealing();
	}

	// Token: 0x060006A6 RID: 1702 RVA: 0x00029098 File Offset: 0x00027298
	public void DoPhysicsMove()
	{
		if (base.isClient)
		{
			return;
		}
		Vector3 velocity = this.myRigidBody.velocity;
		this.throttle = Mathf.Clamp(this.throttle, -1f, 1f);
		this.leftThrottle = this.throttle;
		this.rightThrottle = this.throttle;
		if (this.turning > 0f)
		{
			this.rightThrottle = -this.turning;
			this.leftThrottle = this.turning;
		}
		else if (this.turning < 0f)
		{
			this.leftThrottle = this.turning;
			this.rightThrottle = this.turning * -1f;
		}
		float num = Vector3.Distance(base.transform.position, this.GetFinalDestination());
		float num2 = Vector3.Distance(base.transform.position, this.GetCurrentPathDestination());
		float num3 = 15f;
		if (num2 < 20f)
		{
			float num4 = Vector3.Dot(this.PathDirection(this.currentPathIndex), this.PathDirection(this.currentPathIndex + 1));
			float num5 = Mathf.InverseLerp(2f, 10f, num2);
			float num6 = Mathf.InverseLerp(0.5f, 0.8f, num4);
			num3 = 15f - 14f * ((1f - num6) * (1f - num5));
		}
		if (num < 20f)
		{
		}
		if (this.patrolPath != null)
		{
			float num7 = num3;
			foreach (global::PathSpeedZone pathSpeedZone in this.patrolPath.speedZones)
			{
				if (pathSpeedZone.WorldSpaceBounds().Contains(base.transform.position))
				{
					num7 = Mathf.Min(num7, pathSpeedZone.GetMaxSpeed());
				}
			}
			this.currentSpeedZoneLimit = Mathf.Lerp(this.currentSpeedZoneLimit, num7, UnityEngine.Time.deltaTime);
			num3 = Mathf.Min(num3, this.currentSpeedZoneLimit);
		}
		if (this.PathComplete())
		{
			num3 = 0f;
		}
		if (ConVar.Global.developer > 1)
		{
			Debug.Log(string.Concat(new object[]
			{
				"velocity:",
				velocity.magnitude,
				"max : ",
				num3
			}));
		}
		this.brake = (velocity.magnitude >= num3);
		this.ApplyBrakes((!this.brake) ? 0f : 1f);
		float num8 = this.throttle;
		this.leftThrottle = Mathf.Clamp(this.leftThrottle + num8, -1f, 1f);
		this.rightThrottle = Mathf.Clamp(this.rightThrottle + num8, -1f, 1f);
		float num9 = Mathf.InverseLerp(2f, 1f, velocity.magnitude * Mathf.Abs(Vector3.Dot(velocity.normalized, base.transform.forward)));
		float torqueAmount = Mathf.Lerp(this.moveForceMax, this.turnForce, num9);
		float num10 = Mathf.InverseLerp(5f, 1.5f, velocity.magnitude * Mathf.Abs(Vector3.Dot(velocity.normalized, base.transform.forward)));
		this.ScaleSidewaysFriction(1f - num10);
		this.SetMotorTorque(this.leftThrottle, false, torqueAmount);
		this.SetMotorTorque(this.rightThrottle, true, torqueAmount);
		this.impactDamager.damageEnabled = (this.myRigidBody.velocity.magnitude > 2f);
	}

	// Token: 0x060006A7 RID: 1703 RVA: 0x00029450 File Offset: 0x00027650
	public void ApplyBrakes(float amount)
	{
		this.ApplyBrakeTorque(amount, true);
		this.ApplyBrakeTorque(amount, false);
	}

	// Token: 0x060006A8 RID: 1704 RVA: 0x00029464 File Offset: 0x00027664
	public float GetMotorTorque(bool rightSide)
	{
		float num = 0f;
		foreach (WheelCollider wheelCollider in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			num += wheelCollider.motorTorque;
		}
		return num / (float)this.rightWheels.Length;
	}

	// Token: 0x060006A9 RID: 1705 RVA: 0x000294C0 File Offset: 0x000276C0
	public void ScaleSidewaysFriction(float scale)
	{
		float stiffness = 0.75f + 0.75f * scale;
		foreach (WheelCollider wheelCollider in this.rightWheels)
		{
			WheelFrictionCurve sidewaysFriction = wheelCollider.sidewaysFriction;
			sidewaysFriction.stiffness = stiffness;
			wheelCollider.sidewaysFriction = sidewaysFriction;
		}
		foreach (WheelCollider wheelCollider2 in this.leftWheels)
		{
			WheelFrictionCurve sidewaysFriction2 = wheelCollider2.sidewaysFriction;
			sidewaysFriction2.stiffness = stiffness;
			wheelCollider2.sidewaysFriction = sidewaysFriction2;
		}
	}

	// Token: 0x060006AA RID: 1706 RVA: 0x00029554 File Offset: 0x00027754
	public void SetMotorTorque(float newThrottle, bool rightSide, float torqueAmount)
	{
		newThrottle = Mathf.Clamp(newThrottle, -1f, 1f);
		float num = torqueAmount * newThrottle;
		int num2 = (!rightSide) ? this.leftWheels.Length : this.rightWheels.Length;
		int num3 = 0;
		foreach (WheelCollider wheelCollider in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			WheelHit wheelHit;
			if (wheelCollider.GetGroundHit(ref wheelHit))
			{
				num3++;
			}
		}
		float num4 = 1f;
		if (num3 > 0)
		{
			num4 = (float)(num2 / num3);
		}
		foreach (WheelCollider wheelCollider2 in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			WheelHit wheelHit2;
			if (wheelCollider2.GetGroundHit(ref wheelHit2))
			{
				wheelCollider2.motorTorque = num * num4;
			}
			else
			{
				wheelCollider2.motorTorque = num;
			}
		}
	}

	// Token: 0x060006AB RID: 1707 RVA: 0x00029654 File Offset: 0x00027854
	public void ApplyBrakeTorque(float amount, bool rightSide)
	{
		foreach (WheelCollider wheelCollider in (!rightSide) ? this.leftWheels : this.rightWheels)
		{
			wheelCollider.brakeTorque = this.brakeForce * amount;
		}
	}

	// Token: 0x060006AC RID: 1708 RVA: 0x000296A0 File Offset: 0x000278A0
	public void CreateExplosionMarker(float durationMinutes)
	{
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.debrisFieldMarker.resourcePath, base.transform.position, Quaternion.identity, true);
		baseEntity.Spawn();
		baseEntity.SendMessage("SetDuration", durationMinutes, 1);
	}

	// Token: 0x060006AD RID: 1709 RVA: 0x000296EC File Offset: 0x000278EC
	public override void OnKilled(global::HitInfo info)
	{
		if (base.isClient)
		{
			return;
		}
		this.CreateExplosionMarker(10f);
		global::Effect.server.Run(this.explosionEffect.resourcePath, this.mainTurretEyePos.transform.position, Vector3.up, null, true);
		Vector3 zero = Vector3.zero;
		GameObject gibSource = this.servergibs.Get().GetComponent<global::ServerGib>()._gibSource;
		List<global::ServerGib> list = global::ServerGib.CreateGibs(this.servergibs.resourcePath, base.gameObject, gibSource, zero, 3f);
		for (int i = 0; i < 12 - this.maxCratesToSpawn; i++)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.fireBall.resourcePath, base.transform.position, base.transform.rotation, true);
			if (baseEntity)
			{
				float num = 3f;
				float num2 = 10f;
				Vector3 onUnitSphere = Random.onUnitSphere;
				baseEntity.transform.position = base.transform.position + new Vector3(0f, 1.5f, 0f) + onUnitSphere * Random.Range(-4f, 4f);
				Collider component = baseEntity.GetComponent<Collider>();
				baseEntity.Spawn();
				baseEntity.SetVelocity(zero + onUnitSphere * Random.Range(num, num2));
				foreach (global::ServerGib serverGib in list)
				{
					UnityEngine.Physics.IgnoreCollision(component, serverGib.GetCollider(), true);
				}
			}
		}
		for (int j = 0; j < this.maxCratesToSpawn; j++)
		{
			Vector3 onUnitSphere2 = Random.onUnitSphere;
			Vector3 pos = base.transform.position + new Vector3(0f, 1.5f, 0f) + onUnitSphere2 * Random.Range(2f, 3f);
			global::BaseEntity baseEntity2 = global::GameManager.server.CreateEntity(this.crateToDrop.resourcePath, pos, Quaternion.LookRotation(onUnitSphere2), true);
			baseEntity2.Spawn();
			global::LootContainer lootContainer = baseEntity2 as global::LootContainer;
			if (lootContainer)
			{
				lootContainer.Invoke(new Action(lootContainer.RemoveMe), 1800f);
			}
			Collider component2 = baseEntity2.GetComponent<Collider>();
			Rigidbody rigidbody = baseEntity2.gameObject.AddComponent<Rigidbody>();
			rigidbody.useGravity = true;
			rigidbody.collisionDetectionMode = 2;
			rigidbody.mass = 2f;
			rigidbody.interpolation = 1;
			rigidbody.velocity = zero + onUnitSphere2 * Random.Range(1f, 3f);
			rigidbody.angularVelocity = Vector3Ex.Range(-1.75f, 1.75f);
			rigidbody.drag = 0.5f * (rigidbody.mass / 5f);
			rigidbody.angularDrag = 0.2f * (rigidbody.mass / 5f);
			global::FireBall fireBall = global::GameManager.server.CreateEntity(this.fireBall.resourcePath, default(Vector3), default(Quaternion), true) as global::FireBall;
			if (fireBall)
			{
				fireBall.SetParent(baseEntity2, 0u);
				fireBall.Spawn();
				fireBall.GetComponent<Rigidbody>().isKinematic = true;
				fireBall.GetComponent<Collider>().enabled = false;
			}
			baseEntity2.SendMessage("SetLockingEnt", fireBall.gameObject, 1);
			foreach (global::ServerGib serverGib2 in list)
			{
				UnityEngine.Physics.IgnoreCollision(component2, serverGib2.GetCollider(), true);
			}
		}
		base.OnKilled(info);
	}

	// Token: 0x060006AE RID: 1710 RVA: 0x00029ADC File Offset: 0x00027CDC
	public override void OnAttacked(global::HitInfo info)
	{
		base.OnAttacked(info);
		global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
		if (basePlayer != null)
		{
			this.AddOrUpdateTarget(basePlayer, info.PointStart, info.damageTypes.Total());
		}
	}

	// Token: 0x060006AF RID: 1711 RVA: 0x00029B20 File Offset: 0x00027D20
	public override void OnHealthChanged(float oldvalue, float newvalue)
	{
		base.OnHealthChanged(oldvalue, newvalue);
		base.SetFlag(global::BaseEntity.Flags.Reserved2, base.healthFraction <= 0.75f, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved3, base.healthFraction < 0.4f, false);
	}

	// Token: 0x060006B0 RID: 1712 RVA: 0x00029B60 File Offset: 0x00027D60
	public void DoHealing()
	{
		if (base.isClient)
		{
			return;
		}
		if (base.healthFraction < 1f && base.SecondsSinceAttacked > 600f)
		{
			float amount = this.MaxHealth() / 300f * UnityEngine.Time.fixedDeltaTime;
			this.Heal(amount);
		}
	}

	// Token: 0x060006B1 RID: 1713 RVA: 0x00029BB4 File Offset: 0x00027DB4
	public bool HasPath()
	{
		return this.currentPath != null && this.currentPath.Count > 0;
	}

	// Token: 0x060006B2 RID: 1714 RVA: 0x00029BD4 File Offset: 0x00027DD4
	public void ClearPath()
	{
		this.currentPath.Clear();
		this.currentPathIndex = -1;
	}

	// Token: 0x060006B3 RID: 1715 RVA: 0x00029BE8 File Offset: 0x00027DE8
	public bool IndexValid(int index)
	{
		return this.HasPath() && index >= 0 && index < this.currentPath.Count;
	}

	// Token: 0x060006B4 RID: 1716 RVA: 0x00029C10 File Offset: 0x00027E10
	public Vector3 GetFinalDestination()
	{
		if (!this.HasPath())
		{
			return base.transform.position;
		}
		return this.finalDestination;
	}

	// Token: 0x060006B5 RID: 1717 RVA: 0x00029C30 File Offset: 0x00027E30
	public Vector3 GetCurrentPathDestination()
	{
		if (!this.HasPath())
		{
			return base.transform.position;
		}
		return this.currentPath[this.currentPathIndex];
	}

	// Token: 0x060006B6 RID: 1718 RVA: 0x00029C5C File Offset: 0x00027E5C
	public bool PathComplete()
	{
		return !this.HasPath() || (this.currentPathIndex == this.currentPath.Count - 1 && this.AtCurrentPathNode());
	}

	// Token: 0x060006B7 RID: 1719 RVA: 0x00029C90 File Offset: 0x00027E90
	public bool AtCurrentPathNode()
	{
		return this.currentPathIndex >= 0 && this.currentPathIndex < this.currentPath.Count && Vector3.Distance(base.transform.position, this.currentPath[this.currentPathIndex]) <= this.stoppingDist;
	}

	// Token: 0x060006B8 RID: 1720 RVA: 0x00029CF0 File Offset: 0x00027EF0
	public int GetLoopedIndex(int index)
	{
		if (!this.HasPath())
		{
			Debug.LogWarning("Warning, GetLoopedIndex called without a path");
			return 0;
		}
		if (!this.pathLooping)
		{
			return Mathf.Clamp(index, 0, this.currentPath.Count - 1);
		}
		if (index >= this.currentPath.Count)
		{
			return index % this.currentPath.Count;
		}
		if (index < 0)
		{
			return this.currentPath.Count - Mathf.Abs(index % this.currentPath.Count);
		}
		return index;
	}

	// Token: 0x060006B9 RID: 1721 RVA: 0x00029D7C File Offset: 0x00027F7C
	public Vector3 PathDirection(int index)
	{
		if (!this.HasPath() || this.currentPath.Count <= 1)
		{
			return base.transform.forward;
		}
		index = this.GetLoopedIndex(index);
		Vector3 vector = Vector3.zero;
		Vector3 vector2 = Vector3.zero;
		if (this.pathLooping)
		{
			int loopedIndex = this.GetLoopedIndex(index - 1);
			vector = this.currentPath[loopedIndex];
			vector2 = this.currentPath[this.GetLoopedIndex(index)];
		}
		else
		{
			if (index - 1 < 0)
			{
				vector = base.transform.position;
			}
			else
			{
				vector = this.currentPath[index - 1];
			}
			vector2 = this.currentPath[index];
		}
		return (vector2 - vector).normalized;
	}

	// Token: 0x060006BA RID: 1722 RVA: 0x00029E48 File Offset: 0x00028048
	public Vector3 IdealPathPosition()
	{
		if (!this.HasPath())
		{
			return base.transform.position;
		}
		int loopedIndex = this.GetLoopedIndex(this.currentPathIndex - 1);
		if (loopedIndex == this.currentPathIndex)
		{
			return this.currentPath[this.currentPathIndex];
		}
		return this.ClosestPointAlongPath(this.currentPath[loopedIndex], this.currentPath[this.currentPathIndex], base.GetEstimatedWorldPosition());
	}

	// Token: 0x060006BB RID: 1723 RVA: 0x00029EC4 File Offset: 0x000280C4
	public void AdvancePathMovement()
	{
		if (!this.HasPath())
		{
			return;
		}
		if (this.AtCurrentPathNode() || this.currentPathIndex == -1)
		{
			this.currentPathIndex = this.GetLoopedIndex(this.currentPathIndex + 1);
		}
		if (this.PathComplete())
		{
			this.ClearPath();
			return;
		}
		Vector3 vector = this.IdealPathPosition();
		float num = Vector3.Distance(vector, this.currentPath[this.currentPathIndex]);
		float num2 = Vector3.Distance(base.transform.position, vector);
		float num3 = Mathf.InverseLerp(8f, 0f, num2);
		vector += global::BradleyAPC.Direction2D(this.currentPath[this.currentPathIndex], vector) * Mathf.Min(num, num3 * 20f);
		this.SetDestination(vector);
	}

	// Token: 0x060006BC RID: 1724 RVA: 0x00029F94 File Offset: 0x00028194
	public bool GetPathToClosestTurnableNode(global::BasePathNode start, Vector3 forward, ref List<global::BasePathNode> nodes)
	{
		float num = float.NegativeInfinity;
		global::BasePathNode basePathNode = null;
		foreach (global::BasePathNode basePathNode2 in start.linked)
		{
			float num2 = Vector3.Dot(forward, (basePathNode2.transform.position - start.transform.position).normalized);
			if (num2 > num)
			{
				num = num2;
				basePathNode = basePathNode2;
			}
		}
		if (basePathNode != null)
		{
			nodes.Add(basePathNode);
			return !basePathNode.straightaway || this.GetPathToClosestTurnableNode(basePathNode, (basePathNode.transform.position - start.transform.position).normalized, ref nodes);
		}
		return false;
	}

	// Token: 0x060006BD RID: 1725 RVA: 0x0002A07C File Offset: 0x0002827C
	public bool GetEngagementPath(ref List<global::BasePathNode> nodes)
	{
		global::BasePathNode closestToPoint = this.patrolPath.GetClosestToPoint(base.transform.position);
		Vector3 normalized = (closestToPoint.transform.position - base.transform.position).normalized;
		float num = Vector3.Dot(base.transform.forward, normalized);
		if (num > 0f)
		{
			nodes.Add(closestToPoint);
			if (!closestToPoint.straightaway)
			{
				return true;
			}
		}
		return this.GetPathToClosestTurnableNode(closestToPoint, base.transform.forward, ref nodes);
	}

	// Token: 0x060006BE RID: 1726 RVA: 0x0002A10C File Offset: 0x0002830C
	public void AddOrUpdateTarget(global::BaseEntity ent, Vector3 pos, float damageFrom = 0f)
	{
		if (!(ent is global::BasePlayer))
		{
			return;
		}
		global::BradleyAPC.TargetInfo targetInfo = null;
		foreach (global::BradleyAPC.TargetInfo targetInfo2 in this.targetList)
		{
			if (targetInfo2.entity == ent)
			{
				targetInfo = targetInfo2;
				break;
			}
		}
		if (targetInfo == null)
		{
			targetInfo = Facepunch.Pool.Get<global::BradleyAPC.TargetInfo>();
			targetInfo.Setup(ent, UnityEngine.Time.time - 1f);
			this.targetList.Add(targetInfo);
		}
		targetInfo.lastSeenPosition = pos;
		targetInfo.damageReceivedFrom += damageFrom;
	}

	// Token: 0x060006BF RID: 1727 RVA: 0x0002A1C8 File Offset: 0x000283C8
	public void UpdateTargetList()
	{
		List<global::BaseEntity> list = Facepunch.Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(base.transform.position, this.searchRange, list, 133120, 2);
		foreach (global::BaseEntity baseEntity in list)
		{
			if (baseEntity is global::BasePlayer)
			{
				global::BasePlayer basePlayer = baseEntity as global::BasePlayer;
				if (!basePlayer.IsDead() && !(basePlayer is global::Scientist))
				{
					if (this.VisibilityTest(baseEntity))
					{
						bool flag = false;
						foreach (global::BradleyAPC.TargetInfo targetInfo in this.targetList)
						{
							if (targetInfo.entity == baseEntity)
							{
								targetInfo.lastSeenTime = UnityEngine.Time.time;
								flag = true;
								break;
							}
						}
						if (!flag)
						{
							global::BradleyAPC.TargetInfo targetInfo2 = Facepunch.Pool.Get<global::BradleyAPC.TargetInfo>();
							targetInfo2.Setup(baseEntity, UnityEngine.Time.time);
							this.targetList.Add(targetInfo2);
						}
					}
				}
			}
		}
		for (int i = this.targetList.Count - 1; i >= 0; i--)
		{
			global::BradleyAPC.TargetInfo targetInfo3 = this.targetList[i];
			global::BasePlayer basePlayer2 = targetInfo3.entity as global::BasePlayer;
			if (targetInfo3.entity == null || UnityEngine.Time.time - targetInfo3.lastSeenTime > this.memoryDuration || basePlayer2.IsDead())
			{
				this.targetList.Remove(targetInfo3);
				Facepunch.Pool.Free<global::BradleyAPC.TargetInfo>(ref targetInfo3);
			}
		}
		Facepunch.Pool.FreeList<global::BaseEntity>(ref list);
		this.targetList.Sort(new Comparison<global::BradleyAPC.TargetInfo>(this.SortTargets));
	}

	// Token: 0x060006C0 RID: 1728 RVA: 0x0002A3C0 File Offset: 0x000285C0
	public int SortTargets(global::BradleyAPC.TargetInfo t1, global::BradleyAPC.TargetInfo t2)
	{
		return t2.GetPriorityScore(this).CompareTo(t1.GetPriorityScore(this));
	}

	// Token: 0x060006C1 RID: 1729 RVA: 0x0002A3E4 File Offset: 0x000285E4
	public Vector3 GetAimPoint(global::BaseEntity ent)
	{
		global::BasePlayer basePlayer = ent as global::BasePlayer;
		if (basePlayer != null)
		{
			return basePlayer.eyes.position;
		}
		return ent.CenterPoint();
	}

	// Token: 0x060006C2 RID: 1730 RVA: 0x0002A418 File Offset: 0x00028618
	public bool VisibilityTest(global::BaseEntity ent)
	{
		if (ent == null)
		{
			return false;
		}
		if (Vector3.Distance(ent.GetEstimatedWorldPosition(), base.transform.position) >= this.viewDistance)
		{
			return false;
		}
		object obj = Interface.CallHook("CanBradleyApcTarget", new object[]
		{
			this,
			ent
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		bool result;
		if (ent is global::BasePlayer)
		{
			global::BasePlayer basePlayer = ent as global::BasePlayer;
			Vector3 position = this.mainTurret.transform.position;
			result = (basePlayer.IsVisible(position, base.CenterPoint()) || basePlayer.IsVisible(position, basePlayer.eyes.position) || basePlayer.IsVisible(position, basePlayer.transform.position));
		}
		else
		{
			Debug.LogWarning("Standard vis test!");
			result = base.IsVisible(ent.CenterPoint());
		}
		return result;
	}

	// Token: 0x060006C3 RID: 1731 RVA: 0x0002A510 File Offset: 0x00028710
	public void UpdateTargetVisibilities()
	{
		foreach (global::BradleyAPC.TargetInfo targetInfo in this.targetList)
		{
			if (targetInfo.IsValid())
			{
				if (this.VisibilityTest(targetInfo.entity))
				{
					targetInfo.lastSeenTime = UnityEngine.Time.time;
					targetInfo.lastSeenPosition = targetInfo.entity.GetEstimatedWorldPosition();
				}
			}
		}
	}

	// Token: 0x060006C4 RID: 1732 RVA: 0x0002A5A4 File Offset: 0x000287A4
	public void DoWeaponAiming()
	{
		this.desiredAimVector = ((!(this.mainGunTarget != null)) ? this.desiredAimVector : (this.GetAimPoint(this.mainGunTarget) - this.mainTurretEyePos.transform.position).normalized);
		global::BaseEntity baseEntity = null;
		if (this.targetList.Count > 0)
		{
			if (this.targetList.Count > 1 && this.targetList[1].IsValid() && this.targetList[1].IsVisible())
			{
				baseEntity = this.targetList[1].entity;
			}
			else if (this.targetList[0].IsValid() && this.targetList[0].IsVisible())
			{
				baseEntity = this.targetList[0].entity;
			}
		}
		this.desiredTopTurretAimVector = ((!(baseEntity != null)) ? base.transform.forward : (this.GetAimPoint(baseEntity) - this.topTurretEyePos.transform.position).normalized);
	}

	// Token: 0x060006C5 RID: 1733 RVA: 0x0002A6E8 File Offset: 0x000288E8
	public void DoWeapons()
	{
		if (this.mainGunTarget != null && Vector3.Dot(this.turretAimVector, (this.GetAimPoint(this.mainGunTarget) - this.mainTurretEyePos.transform.position).normalized) >= 0.99f)
		{
			bool flag = this.VisibilityTest(this.mainGunTarget);
			float num = Vector3.Distance(this.mainGunTarget.transform.position, base.transform.position);
			if (UnityEngine.Time.time > this.nextCoaxTime && flag && num <= 40f)
			{
				this.numCoaxBursted++;
				this.FireGun(this.GetAimPoint(this.mainGunTarget), 3f, true);
				this.nextCoaxTime = UnityEngine.Time.time + this.coaxFireRate;
				if (this.numCoaxBursted >= 10)
				{
					this.nextCoaxTime = UnityEngine.Time.time + 1f;
					this.numCoaxBursted = 0;
				}
			}
			if (num >= 10f && flag)
			{
				this.FireGunTest();
			}
		}
	}

	// Token: 0x060006C6 RID: 1734 RVA: 0x0002A808 File Offset: 0x00028A08
	public void FireGun(Vector3 targetPos, float aimCone, bool isCoax)
	{
		Transform transform = (!isCoax) ? this.topTurretMuzzle : this.coaxMuzzle;
		Vector3 vector = transform.transform.position - transform.forward * 0.25f;
		Vector3 normalized = (targetPos - vector).normalized;
		Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(aimCone, normalized, true);
		targetPos = vector + modifiedAimConeDirection * 300f;
		List<RaycastHit> list = Facepunch.Pool.GetList<RaycastHit>();
		global::GamePhysics.TraceAll(new Ray(vector, modifiedAimConeDirection), 0f, list, 300f, 1084435201, 0);
		for (int i = 0; i < list.Count; i++)
		{
			RaycastHit hit = list[i];
			global::BaseEntity entity = hit.GetEntity();
			if (!(entity != null) || (!(entity == this) && !entity.EqualNetID(this)))
			{
				global::BaseCombatEntity baseCombatEntity = entity as global::BaseCombatEntity;
				if (baseCombatEntity != null)
				{
					this.ApplyDamage(baseCombatEntity, hit.point, modifiedAimConeDirection);
				}
				if (!(entity != null) || entity.ShouldBlockProjectiles())
				{
					targetPos = hit.point;
					break;
				}
			}
		}
		base.ClientRPC<bool, Vector3>(null, "CLIENT_FireGun", isCoax, targetPos);
		Facepunch.Pool.FreeList<RaycastHit>(ref list);
	}

	// Token: 0x060006C7 RID: 1735 RVA: 0x0002A968 File Offset: 0x00028B68
	private void ApplyDamage(global::BaseCombatEntity entity, Vector3 point, Vector3 normal)
	{
		float damageAmount = this.bulletDamage * Random.Range(0.9f, 1.1f);
		global::HitInfo info = new global::HitInfo(this, entity, Rust.DamageType.Bullet, damageAmount, point);
		entity.OnAttacked(info);
		if (entity is global::BasePlayer || entity is global::BaseNpc)
		{
			global::Effect.server.ImpactEffect(new global::HitInfo
			{
				HitPositionWorld = point,
				HitNormalWorld = -normal,
				HitMaterial = global::StringPool.Get("Flesh")
			});
		}
	}

	// Token: 0x060006C8 RID: 1736 RVA: 0x0002A9E4 File Offset: 0x00028BE4
	public void AimWeaponAt(Transform weaponYaw, Transform weaponPitch, Vector3 direction, float minPitch = -360f, float maxPitch = 360f, float maxYaw = 360f, Transform parentOverride = null)
	{
		Transform parent = weaponYaw.parent;
		Vector3 vector = parent.InverseTransformDirection(direction);
		Quaternion localRotation = Quaternion.LookRotation(vector);
		Vector3 eulerAngles = localRotation.eulerAngles;
		for (int i = 0; i < 3; i++)
		{
			eulerAngles[i] -= ((eulerAngles[i] <= 180f) ? 0f : 360f);
		}
		Quaternion localRotation2 = Quaternion.Euler(0f, Mathf.Clamp(eulerAngles.y, -maxYaw, maxYaw), 0f);
		Quaternion localRotation3 = Quaternion.Euler(Mathf.Clamp(eulerAngles.x, minPitch, maxPitch), 0f, 0f);
		if (weaponYaw == null && weaponPitch != null)
		{
			weaponPitch.transform.localRotation = localRotation3;
		}
		else if (weaponPitch == null && weaponYaw != null)
		{
			weaponYaw.transform.localRotation = localRotation;
		}
		else
		{
			weaponYaw.transform.localRotation = localRotation2;
			weaponPitch.transform.localRotation = localRotation3;
		}
	}

	// Token: 0x060006C9 RID: 1737 RVA: 0x0002AB0C File Offset: 0x00028D0C
	public void LateUpdate()
	{
		float num = UnityEngine.Time.time - this.lastLateUpdate;
		this.lastLateUpdate = UnityEngine.Time.time;
		if (base.isServer)
		{
			float num2 = 2.09439516f;
			this.turretAimVector = Vector3.RotateTowards(this.turretAimVector, this.desiredAimVector, num2 * num, 0f);
		}
		else
		{
			this.turretAimVector = Vector3.Lerp(this.turretAimVector, this.desiredAimVector, UnityEngine.Time.deltaTime * 10f);
		}
		this.AimWeaponAt(this.mainTurret, this.coaxPitch, this.turretAimVector, -90f, 90f, 360f, null);
		this.AimWeaponAt(this.mainTurret, this.CannonPitch, this.turretAimVector, -90f, 7f, 360f, null);
		this.topTurretAimVector = Vector3.Lerp(this.topTurretAimVector, this.desiredTopTurretAimVector, UnityEngine.Time.deltaTime * 5f);
		this.AimWeaponAt(this.topTurretYaw, this.topTurretPitch, this.topTurretAimVector, -360f, 360f, 360f, this.mainTurret);
	}

	// Token: 0x040002CA RID: 714
	[Header("Sound")]
	public global::EngineAudioClip engineAudioClip;

	// Token: 0x040002CB RID: 715
	public global::SlicedGranularAudioClip treadAudioClip;

	// Token: 0x040002CC RID: 716
	public float treadGrainFreqMin = 0.025f;

	// Token: 0x040002CD RID: 717
	public float treadGrainFreqMax = 0.5f;

	// Token: 0x040002CE RID: 718
	public AnimationCurve treadFreqCurve;

	// Token: 0x040002CF RID: 719
	public global::SoundDefinition chasisLurchSoundDef;

	// Token: 0x040002D0 RID: 720
	public float chasisLurchAngleDelta = 2f;

	// Token: 0x040002D1 RID: 721
	public float chasisLurchSpeedDelta = 2f;

	// Token: 0x040002D2 RID: 722
	private float lastAngle;

	// Token: 0x040002D3 RID: 723
	private float lastSpeed;

	// Token: 0x040002D4 RID: 724
	public global::SoundDefinition turretTurnLoopDef;

	// Token: 0x040002D5 RID: 725
	public float turretLoopGainSpeed = 3f;

	// Token: 0x040002D6 RID: 726
	public float turretLoopPitchSpeed = 3f;

	// Token: 0x040002D7 RID: 727
	public float turretLoopMinAngleDelta;

	// Token: 0x040002D8 RID: 728
	public float turretLoopMaxAngleDelta = 10f;

	// Token: 0x040002D9 RID: 729
	public float turretLoopPitchMin = 0.5f;

	// Token: 0x040002DA RID: 730
	public float turretLoopPitchMax = 1f;

	// Token: 0x040002DB RID: 731
	public float turretLoopGainThreshold = 0.0001f;

	// Token: 0x040002DC RID: 732
	private global::Sound turretTurnLoop;

	// Token: 0x040002DD RID: 733
	private global::SoundModulation.Modulator turretTurnLoopGain;

	// Token: 0x040002DE RID: 734
	private global::SoundModulation.Modulator turretTurnLoopPitch;

	// Token: 0x040002DF RID: 735
	public float enginePitch = 0.9f;

	// Token: 0x040002E0 RID: 736
	public float rpmMultiplier = 0.6f;

	// Token: 0x040002E1 RID: 737
	private global::TreadAnimator treadAnimator;

	// Token: 0x040002E2 RID: 738
	[Header("Wheels")]
	public WheelCollider[] leftWheels;

	// Token: 0x040002E3 RID: 739
	public WheelCollider[] rightWheels;

	// Token: 0x040002E4 RID: 740
	[Header("Movement Config")]
	public float moveForceMax = 2000f;

	// Token: 0x040002E5 RID: 741
	public float brakeForce = 100f;

	// Token: 0x040002E6 RID: 742
	public float turnForce = 2000f;

	// Token: 0x040002E7 RID: 743
	public float sideStiffnessMax = 1f;

	// Token: 0x040002E8 RID: 744
	public float sideStiffnessMin = 0.5f;

	// Token: 0x040002E9 RID: 745
	public Transform centerOfMass;

	// Token: 0x040002EA RID: 746
	public float stoppingDist = 5f;

	// Token: 0x040002EB RID: 747
	[Header("Control")]
	public float throttle = 1f;

	// Token: 0x040002EC RID: 748
	public float turning;

	// Token: 0x040002ED RID: 749
	public float rightThrottle;

	// Token: 0x040002EE RID: 750
	public float leftThrottle;

	// Token: 0x040002EF RID: 751
	public bool brake;

	// Token: 0x040002F0 RID: 752
	[Header("Other")]
	public Rigidbody myRigidBody;

	// Token: 0x040002F1 RID: 753
	public Collider myCollider;

	// Token: 0x040002F2 RID: 754
	public Vector3 destination;

	// Token: 0x040002F3 RID: 755
	private Vector3 finalDestination;

	// Token: 0x040002F4 RID: 756
	public Transform followTest;

	// Token: 0x040002F5 RID: 757
	public global::TriggerHurtEx impactDamager;

	// Token: 0x040002F6 RID: 758
	[Header("Weapons")]
	public Transform mainTurretEyePos;

	// Token: 0x040002F7 RID: 759
	public Transform mainTurret;

	// Token: 0x040002F8 RID: 760
	public Transform CannonPitch;

	// Token: 0x040002F9 RID: 761
	public Transform CannonMuzzle;

	// Token: 0x040002FA RID: 762
	public Transform coaxPitch;

	// Token: 0x040002FB RID: 763
	public Transform coaxMuzzle;

	// Token: 0x040002FC RID: 764
	public Transform topTurretEyePos;

	// Token: 0x040002FD RID: 765
	public Transform topTurretYaw;

	// Token: 0x040002FE RID: 766
	public Transform topTurretPitch;

	// Token: 0x040002FF RID: 767
	public Transform topTurretMuzzle;

	// Token: 0x04000300 RID: 768
	public Vector3 turretAimVector = Vector3.forward;

	// Token: 0x04000301 RID: 769
	private Vector3 desiredAimVector = Vector3.forward;

	// Token: 0x04000302 RID: 770
	public Vector3 topTurretAimVector = Vector3.forward;

	// Token: 0x04000303 RID: 771
	private Vector3 desiredTopTurretAimVector = Vector3.forward;

	// Token: 0x04000304 RID: 772
	[Header("Effects")]
	public global::GameObjectRef explosionEffect;

	// Token: 0x04000305 RID: 773
	public global::GameObjectRef servergibs;

	// Token: 0x04000306 RID: 774
	public global::GameObjectRef fireBall;

	// Token: 0x04000307 RID: 775
	public global::GameObjectRef crateToDrop;

	// Token: 0x04000308 RID: 776
	public global::GameObjectRef debrisFieldMarker;

	// Token: 0x04000309 RID: 777
	[Header("Loot")]
	public int maxCratesToSpawn;

	// Token: 0x0400030A RID: 778
	public int patrolPathIndex;

	// Token: 0x0400030B RID: 779
	public global::BasePath patrolPath;

	// Token: 0x0400030C RID: 780
	public bool DoAI = true;

	// Token: 0x0400030D RID: 781
	public global::GameObjectRef mainCannonMuzzleFlash;

	// Token: 0x0400030E RID: 782
	public global::GameObjectRef mainCannonProjectile;

	// Token: 0x0400030F RID: 783
	private float nextFireTime = 10f;

	// Token: 0x04000310 RID: 784
	private int numBursted;

	// Token: 0x04000311 RID: 785
	public float recoilScale = 200f;

	// Token: 0x04000312 RID: 786
	public NavMeshPath navMeshPath;

	// Token: 0x04000313 RID: 787
	public int navMeshPathIndex;

	// Token: 0x04000314 RID: 788
	private float nextPatrolTime;

	// Token: 0x04000315 RID: 789
	private float nextEngagementPathTime;

	// Token: 0x04000316 RID: 790
	private float currentSpeedZoneLimit;

	// Token: 0x04000317 RID: 791
	[Header("Pathing")]
	public List<Vector3> currentPath;

	// Token: 0x04000318 RID: 792
	public int currentPathIndex;

	// Token: 0x04000319 RID: 793
	public bool pathLooping;

	// Token: 0x0400031A RID: 794
	[Header("Targeting")]
	public float viewDistance = 100f;

	// Token: 0x0400031B RID: 795
	public float searchRange = 100f;

	// Token: 0x0400031C RID: 796
	public float searchFrequency = 2f;

	// Token: 0x0400031D RID: 797
	public float memoryDuration = 20f;

	// Token: 0x0400031E RID: 798
	public static float sightUpdateRate = 0.5f;

	// Token: 0x0400031F RID: 799
	private global::BaseCombatEntity mainGunTarget;

	// Token: 0x04000320 RID: 800
	public List<global::BradleyAPC.TargetInfo> targetList = new List<global::BradleyAPC.TargetInfo>();

	// Token: 0x04000321 RID: 801
	private float nextCoaxTime;

	// Token: 0x04000322 RID: 802
	private float coaxFireRate = 0.06667f;

	// Token: 0x04000323 RID: 803
	private int numCoaxBursted;

	// Token: 0x04000324 RID: 804
	private float bulletDamage = 7f;

	// Token: 0x04000325 RID: 805
	public global::GameObjectRef gun_fire_effect;

	// Token: 0x04000326 RID: 806
	public global::GameObjectRef bulletEffect;

	// Token: 0x04000327 RID: 807
	private float lastLateUpdate;

	// Token: 0x02000054 RID: 84
	[Serializable]
	public class TargetInfo : Facepunch.Pool.IPooled
	{
		// Token: 0x060006CC RID: 1740 RVA: 0x0002AC3C File Offset: 0x00028E3C
		public void EnterPool()
		{
			this.entity = null;
			this.lastSeenPosition = Vector3.zero;
			this.lastSeenTime = 0f;
		}

		// Token: 0x060006CD RID: 1741 RVA: 0x0002AC5C File Offset: 0x00028E5C
		public void Setup(global::BaseEntity ent, float time)
		{
			this.entity = ent;
			this.lastSeenTime = time;
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x0002AC6C File Offset: 0x00028E6C
		public void LeavePool()
		{
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x0002AC70 File Offset: 0x00028E70
		public float GetPriorityScore(global::BradleyAPC apc)
		{
			global::BasePlayer basePlayer = this.entity as global::BasePlayer;
			if (basePlayer)
			{
				float num = Vector3.Distance(this.entity.transform.position, apc.transform.position);
				float num2 = (1f - Mathf.InverseLerp(10f, 80f, num)) * 50f;
				float num3 = (!(basePlayer.GetHeldEntity() == null)) ? basePlayer.GetHeldEntity().hostileScore : 0f;
				float num4 = Mathf.InverseLerp(4f, 20f, num3) * 100f;
				float num5 = Mathf.InverseLerp(10f, 3f, UnityEngine.Time.time - this.lastSeenTime) * 100f;
				float num6 = Mathf.InverseLerp(0f, 100f, this.damageReceivedFrom) * 50f;
				return num2 + num4 + num6 + num5;
			}
			return 0f;
		}

		// Token: 0x060006D0 RID: 1744 RVA: 0x0002AD64 File Offset: 0x00028F64
		public bool IsVisible()
		{
			return this.lastSeenTime != -1f && UnityEngine.Time.time - this.lastSeenTime < global::BradleyAPC.sightUpdateRate * 2f;
		}

		// Token: 0x060006D1 RID: 1745 RVA: 0x0002AD94 File Offset: 0x00028F94
		public bool IsValid()
		{
			return this.entity != null;
		}

		// Token: 0x04000328 RID: 808
		public float damageReceivedFrom;

		// Token: 0x04000329 RID: 809
		public global::BaseEntity entity;

		// Token: 0x0400032A RID: 810
		public float lastSeenTime;

		// Token: 0x0400032B RID: 811
		public Vector3 lastSeenPosition;
	}
}
