﻿using System;
using UnityEngine;

// Token: 0x02000679 RID: 1657
public abstract class ItemContainerSource : MonoBehaviour
{
	// Token: 0x060020D8 RID: 8408
	public abstract global::ItemContainer GetItemContainer();
}
