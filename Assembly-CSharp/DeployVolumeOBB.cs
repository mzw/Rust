﻿using System;
using UnityEngine;

// Token: 0x02000413 RID: 1043
public class DeployVolumeOBB : global::DeployVolume
{
	// Token: 0x060017F7 RID: 6135 RVA: 0x00088B64 File Offset: 0x00086D64
	protected override bool Check(Vector3 position, Quaternion rotation, int mask = -1)
	{
		position += rotation * (this.worldRotation * this.bounds.center + this.worldPosition);
		OBB obb;
		obb..ctor(position, this.bounds.size, rotation * this.worldRotation);
		return global::DeployVolume.CheckOBB(obb, this.layers & mask, this.ignore);
	}

	// Token: 0x060017F8 RID: 6136 RVA: 0x00088BE0 File Offset: 0x00086DE0
	protected override bool Check(Vector3 position, Quaternion rotation, OBB test, int mask = -1)
	{
		position += rotation * (this.worldRotation * this.bounds.center + this.worldPosition);
		OBB obb;
		obb..ctor(position, this.bounds.size, rotation * this.worldRotation);
		return (this.layers & mask) != 0 && obb.Intersects(test);
	}

	// Token: 0x04001285 RID: 4741
	public Bounds bounds = new Bounds(Vector3.zero, Vector3.one);
}
