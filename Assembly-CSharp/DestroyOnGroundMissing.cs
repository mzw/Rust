﻿using System;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000415 RID: 1045
public class DestroyOnGroundMissing : MonoBehaviour, IServerComponent
{
	// Token: 0x060017FD RID: 6141 RVA: 0x00088D50 File Offset: 0x00086F50
	private void OnGroundMissing()
	{
		global::BaseEntity baseEntity = base.gameObject.ToBaseEntity();
		if (baseEntity != null)
		{
			if (Interface.CallHook("OnEntityGroundMissing", new object[]
			{
				baseEntity
			}) != null)
			{
				return;
			}
			global::BaseCombatEntity baseCombatEntity = baseEntity as global::BaseCombatEntity;
			if (baseCombatEntity != null)
			{
				baseCombatEntity.Die(null);
			}
			else
			{
				baseEntity.Kill(global::BaseNetworkable.DestroyMode.Gib);
			}
		}
	}
}
