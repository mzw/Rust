﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Token: 0x020005A3 RID: 1443
public class GeneratePowerlineLayout : global::ProceduralComponent
{
	// Token: 0x06001E42 RID: 7746 RVA: 0x000A8AB0 File Offset: 0x000A6CB0
	public override void Process(uint seed)
	{
		List<global::PathList> list = new List<global::PathList>();
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		List<global::MonumentInfo> monuments = global::TerrainMeta.Path.Monuments;
		if (monuments.Count == 0)
		{
			return;
		}
		int num = Mathf.NextPowerOfTwo((int)(global::World.Size / 10f));
		int[,] array = new int[num, num];
		float radius = 5f;
		for (int i = 0; i < num; i++)
		{
			float normZ = ((float)i + 0.5f) / (float)num;
			for (int j = 0; j < num; j++)
			{
				float normX = ((float)j + 0.5f) / (float)num;
				float slope = heightMap.GetSlope(normX, normZ);
				int topology = topologyMap.GetTopology(normX, normZ, radius);
				int num2 = 2295174;
				int num3 = 55296;
				int num4 = 512;
				if ((topology & num2) != 0)
				{
					array[i, j] = int.MaxValue;
				}
				else if ((topology & num3) != 0)
				{
					array[i, j] = 2500;
				}
				else if ((topology & num4) != 0)
				{
					array[i, j] = 1000;
				}
				else
				{
					array[i, j] = 1 + (int)(slope * slope * 10f);
				}
			}
		}
		global::PathFinder pathFinder = new global::PathFinder(array, true);
		List<global::GeneratePowerlineLayout.PathSegment> list2 = new List<global::GeneratePowerlineLayout.PathSegment>();
		List<global::GeneratePowerlineLayout.PathNode> list3 = new List<global::GeneratePowerlineLayout.PathNode>();
		List<global::GeneratePowerlineLayout.PathNode> list4 = new List<global::GeneratePowerlineLayout.PathNode>();
		List<global::PathFinder.Point> list5 = new List<global::PathFinder.Point>();
		List<global::PathFinder.Point> list6 = new List<global::PathFinder.Point>();
		List<global::PathFinder.Point> list7 = new List<global::PathFinder.Point>();
		foreach (global::MonumentInfo monumentInfo in monuments)
		{
			bool flag = list3.Count == 0;
			foreach (global::TerrainPathConnect terrainPathConnect in monumentInfo.GetTargets(global::InfrastructureType.Power))
			{
				global::PathFinder.Point point = terrainPathConnect.GetPoint(num);
				global::PathFinder.Node node = pathFinder.FindClosestWalkable(point, 100000);
				if (node != null)
				{
					global::GeneratePowerlineLayout.PathNode pathNode = new global::GeneratePowerlineLayout.PathNode();
					pathNode.monument = monumentInfo;
					pathNode.node = node;
					if (flag)
					{
						list3.Add(pathNode);
					}
					else
					{
						list4.Add(pathNode);
					}
				}
			}
		}
		while (list4.Count != 0)
		{
			list6.Clear();
			list7.Clear();
			list6.AddRange(from x in list3
			select x.node.point);
			list6.AddRange(list5);
			list7.AddRange(from x in list4
			select x.node.point);
			global::PathFinder.Node node2 = pathFinder.FindPathUndirected(list6, list7, 100000);
			if (node2 == null)
			{
				global::GeneratePowerlineLayout.PathNode copy = list4[0];
				list3.AddRange(from x in list4
				where x.monument == copy.monument
				select x);
				list4.RemoveAll((global::GeneratePowerlineLayout.PathNode x) => x.monument == copy.monument);
			}
			else
			{
				global::GeneratePowerlineLayout.PathSegment segment = new global::GeneratePowerlineLayout.PathSegment();
				for (global::PathFinder.Node node3 = node2; node3 != null; node3 = node3.next)
				{
					if (node3 == node2)
					{
						segment.start = node3;
					}
					if (node3.next == null)
					{
						segment.end = node3;
					}
				}
				list2.Add(segment);
				global::GeneratePowerlineLayout.PathNode copy = list4.Find((global::GeneratePowerlineLayout.PathNode x) => x.node.point == segment.start.point || x.node.point == segment.end.point);
				list3.AddRange(from x in list4
				where x.monument == copy.monument
				select x);
				list4.RemoveAll((global::GeneratePowerlineLayout.PathNode x) => x.monument == copy.monument);
				int num5 = 1;
				for (global::PathFinder.Node node4 = node2; node4 != null; node4 = node4.next)
				{
					if (num5 % 8 == 0)
					{
						list5.Add(node4.point);
					}
					num5++;
				}
			}
		}
		List<Vector3> list8 = new List<Vector3>();
		foreach (global::GeneratePowerlineLayout.PathSegment pathSegment in list2)
		{
			for (global::PathFinder.Node node5 = pathSegment.start; node5 != null; node5 = node5.next)
			{
				float num6 = ((float)node5.point.x + 0.5f) / (float)num;
				float num7 = ((float)node5.point.y + 0.5f) / (float)num;
				float height = heightMap.GetHeight01(num6, num7);
				list8.Add(global::TerrainMeta.Denormalize(new Vector3(num6, height, num7)));
			}
			if (list8.Count != 0)
			{
				if (list8.Count >= 8)
				{
					list.Add(new global::PathList("Powerline " + list.Count, list8.ToArray())
					{
						Start = true,
						End = true
					});
				}
				list8.Clear();
			}
		}
		global::TerrainMeta.Path.Powerlines.AddRange(list);
	}

	// Token: 0x0400190E RID: 6414
	public const float Width = 10f;

	// Token: 0x0400190F RID: 6415
	private const int MaxDepth = 100000;

	// Token: 0x020005A4 RID: 1444
	private class PathNode
	{
		// Token: 0x04001912 RID: 6418
		public global::MonumentInfo monument;

		// Token: 0x04001913 RID: 6419
		public global::PathFinder.Node node;
	}

	// Token: 0x020005A5 RID: 1445
	private class PathSegment
	{
		// Token: 0x04001914 RID: 6420
		public global::PathFinder.Node start;

		// Token: 0x04001915 RID: 6421
		public global::PathFinder.Node end;
	}
}
