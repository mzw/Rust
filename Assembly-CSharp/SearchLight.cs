﻿using System;
using ConVar;
using Network;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200008E RID: 142
public class SearchLight : global::StorageContainer
{
	// Token: 0x0600094A RID: 2378 RVA: 0x0003EF54 File Offset: 0x0003D154
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("SearchLight.OnRpcMessage", 0.1f))
		{
			if (rpc == 1059389134u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Switch ");
				}
				using (TimeWarning.New("RPC_Switch", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Switch", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Switch(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Switch");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 4274558217u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_UseLight ");
				}
				using (TimeWarning.New("RPC_UseLight", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_UseLight", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_UseLight(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_UseLight");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600094B RID: 2379 RVA: 0x0003F2C0 File Offset: 0x0003D4C0
	public override void ResetState()
	{
		this.aimDir = Vector3.zero;
	}

	// Token: 0x0600094C RID: 2380 RVA: 0x0003F2D0 File Offset: 0x0003D4D0
	public bool IsMounted()
	{
		return this.mountedPlayer != null;
	}

	// Token: 0x0600094D RID: 2381 RVA: 0x0003F2E0 File Offset: 0x0003D4E0
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.autoturret = new ProtoBuf.AutoTurret();
		info.msg.autoturret.aimDir = this.aimDir;
	}

	// Token: 0x0600094E RID: 2382 RVA: 0x0003F314 File Offset: 0x0003D514
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.autoturret != null)
		{
			this.aimDir = info.msg.autoturret.aimDir;
		}
	}

	// Token: 0x0600094F RID: 2383 RVA: 0x0003F348 File Offset: 0x0003D548
	public void PlayerEnter(global::BasePlayer player)
	{
		if (this.IsMounted() && player != this.mountedPlayer)
		{
			return;
		}
		this.PlayerExit();
		if (player != null)
		{
			this.mountedPlayer = player;
			base.SetFlag(global::BaseEntity.Flags.Reserved5, true, false);
		}
	}

	// Token: 0x06000950 RID: 2384 RVA: 0x0003F398 File Offset: 0x0003D598
	public void PlayerExit()
	{
		if (this.mountedPlayer)
		{
			this.mountedPlayer = null;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved5, false, false);
	}

	// Token: 0x06000951 RID: 2385 RVA: 0x0003F3C0 File Offset: 0x0003D5C0
	public void MountedUpdate()
	{
		if (this.mountedPlayer == null || this.mountedPlayer.IsSleeping() || !this.mountedPlayer.IsAlive() || this.mountedPlayer.IsWounded() || Vector3.Distance(this.mountedPlayer.transform.position, base.transform.position) > 2f)
		{
			this.PlayerExit();
			return;
		}
		Vector3 targetAimpoint = this.eyePoint.transform.position + this.mountedPlayer.eyes.BodyForward() * 100f;
		this.SetTargetAimpoint(targetAimpoint);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000952 RID: 2386 RVA: 0x0003F480 File Offset: 0x0003D680
	public void SetTargetAimpoint(Vector3 worldPos)
	{
		this.aimDir = (worldPos - this.eyePoint.transform.position).normalized;
	}

	// Token: 0x06000953 RID: 2387 RVA: 0x0003F4B4 File Offset: 0x0003D6B4
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_UseLight(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		bool flag = msg.read.Bit();
		if (flag && this.IsMounted())
		{
			return;
		}
		if (this.needsBuildingPrivilegeToUse && !msg.player.CanBuild())
		{
			return;
		}
		if (flag)
		{
			this.PlayerEnter(player);
		}
		else
		{
			this.PlayerExit();
		}
	}

	// Token: 0x06000954 RID: 2388 RVA: 0x0003F520 File Offset: 0x0003D720
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_Switch(global::BaseEntity.RPCMessage msg)
	{
		bool b = msg.read.Bit();
		if (this.needsBuildingPrivilegeToUse && !msg.player.CanBuild())
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.On, b, false);
		this.FuelUpdate();
	}

	// Token: 0x06000955 RID: 2389 RVA: 0x0003F568 File Offset: 0x0003D768
	public override void OnKilled(global::HitInfo info)
	{
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		base.OnKilled(info);
	}

	// Token: 0x06000956 RID: 2390 RVA: 0x0003F57C File Offset: 0x0003D77C
	public void FuelUpdate()
	{
		if (base.IsOn())
		{
			this.secondsRemaining -= UnityEngine.Time.deltaTime;
			if (this.secondsRemaining <= 0f)
			{
				global::Item slot = this.inventory.GetSlot(0);
				if (slot == null || slot.info != this.inventory.onlyAllowedItem)
				{
					base.SetFlag(global::BaseEntity.Flags.On, false, false);
					return;
				}
				slot.UseItem(1);
				this.secondsRemaining += 20f;
			}
		}
	}

	// Token: 0x06000957 RID: 2391 RVA: 0x0003F608 File Offset: 0x0003D808
	public void Update()
	{
		if (base.isServer)
		{
			if (this.IsMounted())
			{
				this.MountedUpdate();
			}
			this.FuelUpdate();
		}
	}

	// Token: 0x06000958 RID: 2392 RVA: 0x0003F62C File Offset: 0x0003D82C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04000449 RID: 1097
	public GameObject pitchObject;

	// Token: 0x0400044A RID: 1098
	public GameObject yawObject;

	// Token: 0x0400044B RID: 1099
	public GameObject eyePoint;

	// Token: 0x0400044C RID: 1100
	public GameObject lightEffect;

	// Token: 0x0400044D RID: 1101
	public global::SoundPlayer turnLoop;

	// Token: 0x0400044E RID: 1102
	public global::ItemDefinition fuelType;

	// Token: 0x0400044F RID: 1103
	private Vector3 aimDir = Vector3.zero;

	// Token: 0x04000450 RID: 1104
	private global::BasePlayer mountedPlayer;

	// Token: 0x04000451 RID: 1105
	private float secondsRemaining;

	// Token: 0x0200008F RID: 143
	public static class SearchLightFlags
	{
		// Token: 0x04000452 RID: 1106
		public const global::BaseEntity.Flags PlayerUsing = global::BaseEntity.Flags.Reserved5;
	}
}
