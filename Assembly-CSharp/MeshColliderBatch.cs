﻿using System;
using System.Collections.Generic;
using ConVar;
using Rust;
using UnityEngine;

// Token: 0x02000796 RID: 1942
public class MeshColliderBatch : global::MeshBatch
{
	// Token: 0x17000282 RID: 642
	// (get) Token: 0x060023FD RID: 9213 RVA: 0x000C69AC File Offset: 0x000C4BAC
	public override int VertexCapacity
	{
		get
		{
			return ConVar.Batching.collider_capacity;
		}
	}

	// Token: 0x17000283 RID: 643
	// (get) Token: 0x060023FE RID: 9214 RVA: 0x000C69B4 File Offset: 0x000C4BB4
	public override int VertexCutoff
	{
		get
		{
			return ConVar.Batching.collider_vertices;
		}
	}

	// Token: 0x060023FF RID: 9215 RVA: 0x000C69BC File Offset: 0x000C4BBC
	public static GameObject CreateInstance()
	{
		GameObject gameObject = new GameObject("MeshColliderBatch");
		gameObject.tag = "MeshColliderBatch";
		gameObject.AddComponent<MeshCollider>();
		gameObject.AddComponent<global::MeshColliderBatch>();
		return gameObject;
	}

	// Token: 0x06002400 RID: 9216 RVA: 0x000C69F0 File Offset: 0x000C4BF0
	public Transform LookupTransform(int triangleIndex)
	{
		return this.meshLookup.Get(triangleIndex).transform;
	}

	// Token: 0x06002401 RID: 9217 RVA: 0x000C6A14 File Offset: 0x000C4C14
	public Rigidbody LookupRigidbody(int triangleIndex)
	{
		return this.meshLookup.Get(triangleIndex).rigidbody;
	}

	// Token: 0x06002402 RID: 9218 RVA: 0x000C6A38 File Offset: 0x000C4C38
	public Collider LookupCollider(int triangleIndex)
	{
		return this.meshLookup.Get(triangleIndex).collider;
	}

	// Token: 0x06002403 RID: 9219 RVA: 0x000C6A5C File Offset: 0x000C4C5C
	public void LookupColliders<T>(Vector3 position, float distance, List<T> list) where T : Collider
	{
		List<global::MeshColliderLookup.LookupEntry> data = this.meshLookup.src.data;
		float num = distance * distance;
		for (int i = 0; i < data.Count; i++)
		{
			global::MeshColliderLookup.LookupEntry lookupEntry = data[i];
			if (lookupEntry.collider && (lookupEntry.bounds.ClosestPoint(position) - position).sqrMagnitude <= num)
			{
				list.Add(lookupEntry.collider as T);
			}
		}
	}

	// Token: 0x06002404 RID: 9220 RVA: 0x000C6AE8 File Offset: 0x000C4CE8
	protected void Awake()
	{
		this.meshCollider = base.GetComponent<MeshCollider>();
		this.meshData = new global::MeshColliderData();
		this.meshGroup = new global::MeshColliderGroup();
		this.meshLookup = new global::MeshColliderLookup();
	}

	// Token: 0x06002405 RID: 9221 RVA: 0x000C6B18 File Offset: 0x000C4D18
	public void Setup(Vector3 position, LayerMask layer, PhysicMaterial material)
	{
		base.transform.position = position;
		this.position = position;
		base.gameObject.layer = layer;
		this.meshCollider.sharedMaterial = material;
	}

	// Token: 0x06002406 RID: 9222 RVA: 0x000C6B58 File Offset: 0x000C4D58
	public void Add(global::MeshColliderInstance instance)
	{
		instance.position -= this.position;
		this.meshGroup.data.Add(instance);
		base.AddVertices(instance.mesh.vertexCount);
	}

	// Token: 0x06002407 RID: 9223 RVA: 0x000C6B98 File Offset: 0x000C4D98
	protected override void AllocMemory()
	{
		this.meshGroup.Alloc();
		this.meshData.Alloc();
	}

	// Token: 0x06002408 RID: 9224 RVA: 0x000C6BB0 File Offset: 0x000C4DB0
	protected override void FreeMemory()
	{
		this.meshGroup.Free();
		this.meshData.Free();
	}

	// Token: 0x06002409 RID: 9225 RVA: 0x000C6BC8 File Offset: 0x000C4DC8
	protected override void RefreshMesh()
	{
		this.meshLookup.dst.Clear();
		this.meshData.Clear();
		this.meshData.Combine(this.meshGroup, this.meshLookup);
	}

	// Token: 0x0600240A RID: 9226 RVA: 0x000C6BFC File Offset: 0x000C4DFC
	protected override void ApplyMesh()
	{
		if (!this.meshBatch)
		{
			this.meshBatch = AssetPool.Get<UnityEngine.Mesh>();
		}
		this.meshLookup.Apply();
		this.meshData.Apply(this.meshBatch);
	}

	// Token: 0x0600240B RID: 9227 RVA: 0x000C6C38 File Offset: 0x000C4E38
	protected override void ToggleMesh(bool state)
	{
		if (!Application.isLoading)
		{
			List<global::MeshColliderLookup.LookupEntry> data = this.meshLookup.src.data;
			for (int i = 0; i < data.Count; i++)
			{
				Collider collider = data[i].collider;
				if (collider)
				{
					collider.enabled = !state;
				}
			}
		}
		if (state)
		{
			if (this.meshCollider)
			{
				this.meshCollider.sharedMesh = this.meshBatch;
				this.meshCollider.enabled = false;
				this.meshCollider.enabled = true;
			}
		}
		else if (this.meshCollider)
		{
			this.meshCollider.sharedMesh = null;
			this.meshCollider.enabled = false;
		}
	}

	// Token: 0x0600240C RID: 9228 RVA: 0x000C6D08 File Offset: 0x000C4F08
	protected override void OnPooled()
	{
		if (this.meshCollider)
		{
			this.meshCollider.sharedMesh = null;
		}
		if (this.meshBatch)
		{
			AssetPool.Free(ref this.meshBatch);
		}
		this.meshData.Free();
		this.meshGroup.Free();
		this.meshLookup.src.Clear();
		this.meshLookup.dst.Clear();
	}

	// Token: 0x04001FB7 RID: 8119
	private Vector3 position;

	// Token: 0x04001FB8 RID: 8120
	private UnityEngine.Mesh meshBatch;

	// Token: 0x04001FB9 RID: 8121
	private MeshCollider meshCollider;

	// Token: 0x04001FBA RID: 8122
	private global::MeshColliderData meshData;

	// Token: 0x04001FBB RID: 8123
	private global::MeshColliderGroup meshGroup;

	// Token: 0x04001FBC RID: 8124
	private global::MeshColliderLookup meshLookup;
}
