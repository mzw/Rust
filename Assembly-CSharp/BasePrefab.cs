﻿using System;
using UnityEngine;

// Token: 0x0200073C RID: 1852
public class BasePrefab : global::BaseMonoBehaviour, global::IPrefabPreProcess
{
	// Token: 0x17000272 RID: 626
	// (get) Token: 0x060022CC RID: 8908 RVA: 0x000C1910 File Offset: 0x000BFB10
	public bool isServer
	{
		get
		{
			return !this.isClient;
		}
	}

	// Token: 0x060022CD RID: 8909 RVA: 0x000C191C File Offset: 0x000BFB1C
	public virtual void PreProcess(global::IPrefabProcessor preProcess, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		this.prefabID = global::StringPool.Get(name);
		this.isClient = clientside;
	}

	// Token: 0x04001F3E RID: 7998
	[HideInInspector]
	public uint prefabID;

	// Token: 0x04001F3F RID: 7999
	[HideInInspector]
	public bool isClient;
}
