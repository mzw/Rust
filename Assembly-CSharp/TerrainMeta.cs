﻿using System;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000586 RID: 1414
[ExecuteInEditMode]
public class TerrainMeta : MonoBehaviour
{
	// Token: 0x170001FE RID: 510
	// (get) Token: 0x06001DCD RID: 7629 RVA: 0x000A659C File Offset: 0x000A479C
	// (set) Token: 0x06001DCE RID: 7630 RVA: 0x000A65A4 File Offset: 0x000A47A4
	public static global::TerrainConfig Config { get; private set; }

	// Token: 0x170001FF RID: 511
	// (get) Token: 0x06001DCF RID: 7631 RVA: 0x000A65AC File Offset: 0x000A47AC
	// (set) Token: 0x06001DD0 RID: 7632 RVA: 0x000A65B4 File Offset: 0x000A47B4
	public static Terrain Terrain { get; private set; }

	// Token: 0x17000200 RID: 512
	// (get) Token: 0x06001DD1 RID: 7633 RVA: 0x000A65BC File Offset: 0x000A47BC
	// (set) Token: 0x06001DD2 RID: 7634 RVA: 0x000A65C4 File Offset: 0x000A47C4
	public static Transform Transform { get; private set; }

	// Token: 0x17000201 RID: 513
	// (get) Token: 0x06001DD3 RID: 7635 RVA: 0x000A65CC File Offset: 0x000A47CC
	// (set) Token: 0x06001DD4 RID: 7636 RVA: 0x000A65D4 File Offset: 0x000A47D4
	public static Vector3 Position { get; private set; }

	// Token: 0x17000202 RID: 514
	// (get) Token: 0x06001DD5 RID: 7637 RVA: 0x000A65DC File Offset: 0x000A47DC
	// (set) Token: 0x06001DD6 RID: 7638 RVA: 0x000A65E4 File Offset: 0x000A47E4
	public static Vector3 Size { get; private set; }

	// Token: 0x17000203 RID: 515
	// (get) Token: 0x06001DD7 RID: 7639 RVA: 0x000A65EC File Offset: 0x000A47EC
	// (set) Token: 0x06001DD8 RID: 7640 RVA: 0x000A65F4 File Offset: 0x000A47F4
	public static Vector3 OneOverSize { get; private set; }

	// Token: 0x17000204 RID: 516
	// (get) Token: 0x06001DD9 RID: 7641 RVA: 0x000A65FC File Offset: 0x000A47FC
	// (set) Token: 0x06001DDA RID: 7642 RVA: 0x000A6604 File Offset: 0x000A4804
	public static Vector3 HighestPoint { get; set; }

	// Token: 0x17000205 RID: 517
	// (get) Token: 0x06001DDB RID: 7643 RVA: 0x000A660C File Offset: 0x000A480C
	// (set) Token: 0x06001DDC RID: 7644 RVA: 0x000A6614 File Offset: 0x000A4814
	public static Vector3 LowestPoint { get; set; }

	// Token: 0x17000206 RID: 518
	// (get) Token: 0x06001DDD RID: 7645 RVA: 0x000A661C File Offset: 0x000A481C
	// (set) Token: 0x06001DDE RID: 7646 RVA: 0x000A6624 File Offset: 0x000A4824
	public static float LootAxisAngle { get; private set; }

	// Token: 0x17000207 RID: 519
	// (get) Token: 0x06001DDF RID: 7647 RVA: 0x000A662C File Offset: 0x000A482C
	// (set) Token: 0x06001DE0 RID: 7648 RVA: 0x000A6634 File Offset: 0x000A4834
	public static float BiomeAxisAngle { get; private set; }

	// Token: 0x17000208 RID: 520
	// (get) Token: 0x06001DE1 RID: 7649 RVA: 0x000A663C File Offset: 0x000A483C
	// (set) Token: 0x06001DE2 RID: 7650 RVA: 0x000A6644 File Offset: 0x000A4844
	public static TerrainData Data { get; private set; }

	// Token: 0x17000209 RID: 521
	// (get) Token: 0x06001DE3 RID: 7651 RVA: 0x000A664C File Offset: 0x000A484C
	// (set) Token: 0x06001DE4 RID: 7652 RVA: 0x000A6654 File Offset: 0x000A4854
	public static TerrainCollider Collider { get; private set; }

	// Token: 0x1700020A RID: 522
	// (get) Token: 0x06001DE5 RID: 7653 RVA: 0x000A665C File Offset: 0x000A485C
	// (set) Token: 0x06001DE6 RID: 7654 RVA: 0x000A6664 File Offset: 0x000A4864
	public static global::TerrainCollision Collision { get; private set; }

	// Token: 0x1700020B RID: 523
	// (get) Token: 0x06001DE7 RID: 7655 RVA: 0x000A666C File Offset: 0x000A486C
	// (set) Token: 0x06001DE8 RID: 7656 RVA: 0x000A6674 File Offset: 0x000A4874
	public static global::TerrainPhysics Physics { get; private set; }

	// Token: 0x1700020C RID: 524
	// (get) Token: 0x06001DE9 RID: 7657 RVA: 0x000A667C File Offset: 0x000A487C
	// (set) Token: 0x06001DEA RID: 7658 RVA: 0x000A6684 File Offset: 0x000A4884
	public static global::TerrainColors Colors { get; private set; }

	// Token: 0x1700020D RID: 525
	// (get) Token: 0x06001DEB RID: 7659 RVA: 0x000A668C File Offset: 0x000A488C
	// (set) Token: 0x06001DEC RID: 7660 RVA: 0x000A6694 File Offset: 0x000A4894
	public static global::TerrainQuality Quality { get; private set; }

	// Token: 0x1700020E RID: 526
	// (get) Token: 0x06001DED RID: 7661 RVA: 0x000A669C File Offset: 0x000A489C
	// (set) Token: 0x06001DEE RID: 7662 RVA: 0x000A66A4 File Offset: 0x000A48A4
	public static global::TerrainPath Path { get; private set; }

	// Token: 0x1700020F RID: 527
	// (get) Token: 0x06001DEF RID: 7663 RVA: 0x000A66AC File Offset: 0x000A48AC
	// (set) Token: 0x06001DF0 RID: 7664 RVA: 0x000A66B4 File Offset: 0x000A48B4
	public static global::TerrainBiomeMap BiomeMap { get; private set; }

	// Token: 0x17000210 RID: 528
	// (get) Token: 0x06001DF1 RID: 7665 RVA: 0x000A66BC File Offset: 0x000A48BC
	// (set) Token: 0x06001DF2 RID: 7666 RVA: 0x000A66C4 File Offset: 0x000A48C4
	public static global::TerrainAlphaMap AlphaMap { get; private set; }

	// Token: 0x17000211 RID: 529
	// (get) Token: 0x06001DF3 RID: 7667 RVA: 0x000A66CC File Offset: 0x000A48CC
	// (set) Token: 0x06001DF4 RID: 7668 RVA: 0x000A66D4 File Offset: 0x000A48D4
	public static global::TerrainBlendMap BlendMap { get; private set; }

	// Token: 0x17000212 RID: 530
	// (get) Token: 0x06001DF5 RID: 7669 RVA: 0x000A66DC File Offset: 0x000A48DC
	// (set) Token: 0x06001DF6 RID: 7670 RVA: 0x000A66E4 File Offset: 0x000A48E4
	public static global::TerrainHeightMap HeightMap { get; private set; }

	// Token: 0x17000213 RID: 531
	// (get) Token: 0x06001DF7 RID: 7671 RVA: 0x000A66EC File Offset: 0x000A48EC
	// (set) Token: 0x06001DF8 RID: 7672 RVA: 0x000A66F4 File Offset: 0x000A48F4
	public static global::TerrainSplatMap SplatMap { get; private set; }

	// Token: 0x17000214 RID: 532
	// (get) Token: 0x06001DF9 RID: 7673 RVA: 0x000A66FC File Offset: 0x000A48FC
	// (set) Token: 0x06001DFA RID: 7674 RVA: 0x000A6704 File Offset: 0x000A4904
	public static global::TerrainTopologyMap TopologyMap { get; private set; }

	// Token: 0x17000215 RID: 533
	// (get) Token: 0x06001DFB RID: 7675 RVA: 0x000A670C File Offset: 0x000A490C
	// (set) Token: 0x06001DFC RID: 7676 RVA: 0x000A6714 File Offset: 0x000A4914
	public static global::TerrainWaterMap WaterMap { get; private set; }

	// Token: 0x17000216 RID: 534
	// (get) Token: 0x06001DFD RID: 7677 RVA: 0x000A671C File Offset: 0x000A491C
	// (set) Token: 0x06001DFE RID: 7678 RVA: 0x000A6724 File Offset: 0x000A4924
	public static global::TerrainTexturing Texturing { get; private set; }

	// Token: 0x06001DFF RID: 7679 RVA: 0x000A672C File Offset: 0x000A492C
	public static bool OutOfBounds(Vector3 worldPos)
	{
		return worldPos.x < global::TerrainMeta.Position.x || worldPos.z < global::TerrainMeta.Position.z || worldPos.x > global::TerrainMeta.Position.x + global::TerrainMeta.Size.x || worldPos.z > global::TerrainMeta.Position.z + global::TerrainMeta.Size.z;
	}

	// Token: 0x06001E00 RID: 7680 RVA: 0x000A67BC File Offset: 0x000A49BC
	public static Vector3 Normalize(Vector3 worldPos)
	{
		float num = (worldPos.x - global::TerrainMeta.Position.x) * global::TerrainMeta.OneOverSize.x;
		float num2 = (worldPos.y - global::TerrainMeta.Position.y) * global::TerrainMeta.OneOverSize.y;
		float num3 = (worldPos.z - global::TerrainMeta.Position.z) * global::TerrainMeta.OneOverSize.z;
		return new Vector3(num, num2, num3);
	}

	// Token: 0x06001E01 RID: 7681 RVA: 0x000A6844 File Offset: 0x000A4A44
	public static float NormalizeX(float x)
	{
		return (x - global::TerrainMeta.Position.x) * global::TerrainMeta.OneOverSize.x;
	}

	// Token: 0x06001E02 RID: 7682 RVA: 0x000A6870 File Offset: 0x000A4A70
	public static float NormalizeY(float y)
	{
		return (y - global::TerrainMeta.Position.y) * global::TerrainMeta.OneOverSize.y;
	}

	// Token: 0x06001E03 RID: 7683 RVA: 0x000A689C File Offset: 0x000A4A9C
	public static float NormalizeZ(float z)
	{
		return (z - global::TerrainMeta.Position.z) * global::TerrainMeta.OneOverSize.z;
	}

	// Token: 0x06001E04 RID: 7684 RVA: 0x000A68C8 File Offset: 0x000A4AC8
	public static Vector3 Denormalize(Vector3 normPos)
	{
		float num = global::TerrainMeta.Position.x + normPos.x * global::TerrainMeta.Size.x;
		float num2 = global::TerrainMeta.Position.y + normPos.y * global::TerrainMeta.Size.y;
		float num3 = global::TerrainMeta.Position.z + normPos.z * global::TerrainMeta.Size.z;
		return new Vector3(num, num2, num3);
	}

	// Token: 0x06001E05 RID: 7685 RVA: 0x000A6950 File Offset: 0x000A4B50
	public static float DenormalizeX(float normX)
	{
		return global::TerrainMeta.Position.x + normX * global::TerrainMeta.Size.x;
	}

	// Token: 0x06001E06 RID: 7686 RVA: 0x000A697C File Offset: 0x000A4B7C
	public static float DenormalizeY(float normY)
	{
		return global::TerrainMeta.Position.y + normY * global::TerrainMeta.Size.y;
	}

	// Token: 0x06001E07 RID: 7687 RVA: 0x000A69A8 File Offset: 0x000A4BA8
	public static float DenormalizeZ(float normZ)
	{
		return global::TerrainMeta.Position.z + normZ * global::TerrainMeta.Size.z;
	}

	// Token: 0x06001E08 RID: 7688 RVA: 0x000A69D4 File Offset: 0x000A4BD4
	protected void Awake()
	{
		if (!Application.isPlaying)
		{
			return;
		}
		Shader.DisableKeyword("TERRAIN_PAINTING");
	}

	// Token: 0x06001E09 RID: 7689 RVA: 0x000A69EC File Offset: 0x000A4BEC
	public void Init(Terrain terrainOverride = null, global::TerrainConfig configOverride = null)
	{
		if (terrainOverride != null)
		{
			this.terrain = terrainOverride;
		}
		if (configOverride != null)
		{
			this.config = configOverride;
		}
		global::TerrainMeta.Terrain = this.terrain;
		global::TerrainMeta.Config = this.config;
		global::TerrainMeta.Transform = this.terrain.transform;
		global::TerrainMeta.Data = this.terrain.terrainData;
		global::TerrainMeta.Size = this.terrain.terrainData.size;
		global::TerrainMeta.OneOverSize = Vector3Ex.Inverse(global::TerrainMeta.Size);
		global::TerrainMeta.Position = this.terrain.GetPosition();
		global::TerrainMeta.Collider = this.terrain.GetComponent<TerrainCollider>();
		global::TerrainMeta.Collision = this.terrain.GetComponent<global::TerrainCollision>();
		global::TerrainMeta.Physics = this.terrain.GetComponent<global::TerrainPhysics>();
		global::TerrainMeta.Colors = this.terrain.GetComponent<global::TerrainColors>();
		global::TerrainMeta.Quality = this.terrain.GetComponent<global::TerrainQuality>();
		global::TerrainMeta.Path = this.terrain.GetComponent<global::TerrainPath>();
		global::TerrainMeta.BiomeMap = this.terrain.GetComponent<global::TerrainBiomeMap>();
		global::TerrainMeta.AlphaMap = this.terrain.GetComponent<global::TerrainAlphaMap>();
		global::TerrainMeta.BlendMap = this.terrain.GetComponent<global::TerrainBlendMap>();
		global::TerrainMeta.HeightMap = this.terrain.GetComponent<global::TerrainHeightMap>();
		global::TerrainMeta.SplatMap = this.terrain.GetComponent<global::TerrainSplatMap>();
		global::TerrainMeta.TopologyMap = this.terrain.GetComponent<global::TerrainTopologyMap>();
		global::TerrainMeta.WaterMap = this.terrain.GetComponent<global::TerrainWaterMap>();
		global::TerrainMeta.Texturing = this.terrain.GetComponent<global::TerrainTexturing>();
		global::TerrainMeta.HighestPoint = new Vector3(global::TerrainMeta.Position.x, global::TerrainMeta.Position.y + global::TerrainMeta.Size.y, global::TerrainMeta.Position.z);
		global::TerrainMeta.LowestPoint = new Vector3(global::TerrainMeta.Position.x, global::TerrainMeta.Position.y, global::TerrainMeta.Position.z);
		foreach (global::TerrainExtension terrainExtension in base.GetComponents<global::TerrainExtension>())
		{
			terrainExtension.Init(this.terrain, this.config);
		}
		uint seed = global::World.Seed;
		int num = SeedRandom.Range(ref seed, 0, 4) * 90;
		int num2 = SeedRandom.Range(ref seed, -45, 46);
		int num3 = SeedRandom.Sign(ref seed);
		global::TerrainMeta.LootAxisAngle = (float)num;
		global::TerrainMeta.BiomeAxisAngle = (float)(num + num2 + num3 * 90);
	}

	// Token: 0x06001E0A RID: 7690 RVA: 0x000A6C5C File Offset: 0x000A4E5C
	public static void InitNoTerrain()
	{
		global::TerrainMeta.Size = new Vector3(4096f, 4096f, 4096f);
	}

	// Token: 0x06001E0B RID: 7691 RVA: 0x000A6C78 File Offset: 0x000A4E78
	public void SetupComponents()
	{
		foreach (global::TerrainExtension terrainExtension in base.GetComponents<global::TerrainExtension>())
		{
			terrainExtension.Setup();
			terrainExtension.isInitialized = true;
		}
	}

	// Token: 0x06001E0C RID: 7692 RVA: 0x000A6CB4 File Offset: 0x000A4EB4
	public void PostSetupComponents()
	{
		foreach (global::TerrainExtension terrainExtension in base.GetComponents<global::TerrainExtension>())
		{
			terrainExtension.PostSetup();
		}
		Interface.CallHook("OnTerrainInitialized", null);
	}

	// Token: 0x06001E0D RID: 7693 RVA: 0x000A6CF4 File Offset: 0x000A4EF4
	public void BindShaderProperties()
	{
		if (this.config)
		{
			Shader.SetGlobalTexture("Terrain_SplatAtlas", this.config.SplatAtlas);
			Shader.SetGlobalTexture("Terrain_NormalAtlas", this.config.NormalAtlas);
			Shader.SetGlobalVector("Terrain_TexelSize", new Vector2(1f / this.config.GetMinSplatTiling(), 1f / this.config.GetMinSplatTiling()));
			Shader.SetGlobalVector("Terrain_TexelSize0", new Vector4(1f / this.config.Splats[0].SplatTiling, 1f / this.config.Splats[1].SplatTiling, 1f / this.config.Splats[2].SplatTiling, 1f / this.config.Splats[3].SplatTiling));
			Shader.SetGlobalVector("Terrain_TexelSize1", new Vector4(1f / this.config.Splats[4].SplatTiling, 1f / this.config.Splats[5].SplatTiling, 1f / this.config.Splats[6].SplatTiling, 1f / this.config.Splats[7].SplatTiling));
			Shader.SetGlobalVector("Splat0_UVMIX", new Vector3(this.config.Splats[0].UVMIXMult, this.config.Splats[0].UVMIXStart, 1f / this.config.Splats[0].UVMIXDist));
			Shader.SetGlobalVector("Splat1_UVMIX", new Vector3(this.config.Splats[1].UVMIXMult, this.config.Splats[1].UVMIXStart, 1f / this.config.Splats[1].UVMIXDist));
			Shader.SetGlobalVector("Splat2_UVMIX", new Vector3(this.config.Splats[2].UVMIXMult, this.config.Splats[2].UVMIXStart, 1f / this.config.Splats[2].UVMIXDist));
			Shader.SetGlobalVector("Splat3_UVMIX", new Vector3(this.config.Splats[3].UVMIXMult, this.config.Splats[3].UVMIXStart, 1f / this.config.Splats[3].UVMIXDist));
			Shader.SetGlobalVector("Splat4_UVMIX", new Vector3(this.config.Splats[4].UVMIXMult, this.config.Splats[4].UVMIXStart, 1f / this.config.Splats[4].UVMIXDist));
			Shader.SetGlobalVector("Splat5_UVMIX", new Vector3(this.config.Splats[5].UVMIXMult, this.config.Splats[5].UVMIXStart, 1f / this.config.Splats[5].UVMIXDist));
			Shader.SetGlobalVector("Splat6_UVMIX", new Vector3(this.config.Splats[6].UVMIXMult, this.config.Splats[6].UVMIXStart, 1f / this.config.Splats[6].UVMIXDist));
			Shader.SetGlobalVector("Splat7_UVMIX", new Vector3(this.config.Splats[7].UVMIXMult, this.config.Splats[7].UVMIXStart, 1f / this.config.Splats[7].UVMIXDist));
		}
		if (global::TerrainMeta.HeightMap)
		{
			Shader.SetGlobalTexture("Terrain_Normal", global::TerrainMeta.HeightMap.NormalTexture);
		}
		if (global::TerrainMeta.AlphaMap)
		{
			Shader.SetGlobalTexture("Terrain_Alpha", global::TerrainMeta.AlphaMap.AlphaTexture);
		}
		if (global::TerrainMeta.BiomeMap)
		{
			Shader.SetGlobalTexture("Terrain_Biome", global::TerrainMeta.BiomeMap.BiomeTexture);
		}
		if (global::TerrainMeta.SplatMap)
		{
			Shader.SetGlobalTexture("Terrain_Control0", global::TerrainMeta.SplatMap.SplatTexture0);
			Shader.SetGlobalTexture("Terrain_Control1", global::TerrainMeta.SplatMap.SplatTexture1);
		}
		if (global::TerrainMeta.WaterMap)
		{
		}
		if (this.terrain)
		{
			Shader.SetGlobalVector("Terrain_Position", global::TerrainMeta.Position);
			Shader.SetGlobalVector("Terrain_Size", global::TerrainMeta.Size);
			Shader.SetGlobalVector("Terrain_RcpSize", global::TerrainMeta.OneOverSize);
			if (this.terrain.materialTemplate)
			{
				if (this.terrain.materialTemplate.IsKeywordEnabled("_TERRAIN_BLEND_LINEAR"))
				{
					this.terrain.materialTemplate.DisableKeyword("_TERRAIN_BLEND_LINEAR");
				}
				if (this.terrain.materialTemplate.IsKeywordEnabled("_TERRAIN_VERTEX_NORMALS"))
				{
					this.terrain.materialTemplate.DisableKeyword("_TERRAIN_VERTEX_NORMALS");
				}
			}
		}
	}

	// Token: 0x04001853 RID: 6227
	public Terrain terrain;

	// Token: 0x04001854 RID: 6228
	public global::TerrainConfig config;

	// Token: 0x04001855 RID: 6229
	public global::TerrainMeta.PaintMode paint;

	// Token: 0x04001856 RID: 6230
	[HideInInspector]
	public global::TerrainMeta.PaintMode currentPaintMode;

	// Token: 0x02000587 RID: 1415
	public enum PaintMode
	{
		// Token: 0x04001871 RID: 6257
		None,
		// Token: 0x04001872 RID: 6258
		Splats,
		// Token: 0x04001873 RID: 6259
		Biomes,
		// Token: 0x04001874 RID: 6260
		Alpha,
		// Token: 0x04001875 RID: 6261
		Blend,
		// Token: 0x04001876 RID: 6262
		Field,
		// Token: 0x04001877 RID: 6263
		Cliff,
		// Token: 0x04001878 RID: 6264
		Summit,
		// Token: 0x04001879 RID: 6265
		Beachside,
		// Token: 0x0400187A RID: 6266
		Beach,
		// Token: 0x0400187B RID: 6267
		Forest,
		// Token: 0x0400187C RID: 6268
		Forestside,
		// Token: 0x0400187D RID: 6269
		Ocean,
		// Token: 0x0400187E RID: 6270
		Oceanside,
		// Token: 0x0400187F RID: 6271
		Decor,
		// Token: 0x04001880 RID: 6272
		Monument,
		// Token: 0x04001881 RID: 6273
		Road,
		// Token: 0x04001882 RID: 6274
		Roadside,
		// Token: 0x04001883 RID: 6275
		Bridge,
		// Token: 0x04001884 RID: 6276
		River,
		// Token: 0x04001885 RID: 6277
		Riverside,
		// Token: 0x04001886 RID: 6278
		Lake,
		// Token: 0x04001887 RID: 6279
		Lakeside,
		// Token: 0x04001888 RID: 6280
		Offshore,
		// Token: 0x04001889 RID: 6281
		Powerline,
		// Token: 0x0400188A RID: 6282
		Runway,
		// Token: 0x0400188B RID: 6283
		Building,
		// Token: 0x0400188C RID: 6284
		Cliffside,
		// Token: 0x0400188D RID: 6285
		Mountain,
		// Token: 0x0400188E RID: 6286
		Clutter,
		// Token: 0x0400188F RID: 6287
		Padding,
		// Token: 0x04001890 RID: 6288
		Tier0,
		// Token: 0x04001891 RID: 6289
		Tier1,
		// Token: 0x04001892 RID: 6290
		Tier2,
		// Token: 0x04001893 RID: 6291
		Mainland,
		// Token: 0x04001894 RID: 6292
		Hilltop
	}
}
