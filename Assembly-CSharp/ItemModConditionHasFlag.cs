﻿using System;

// Token: 0x020004DC RID: 1244
public class ItemModConditionHasFlag : global::ItemMod
{
	// Token: 0x06001ABF RID: 6847 RVA: 0x00095CFC File Offset: 0x00093EFC
	public override bool Passes(global::Item item)
	{
		return item.HasFlag(this.flag) == this.requiredState;
	}

	// Token: 0x0400157F RID: 5503
	public global::Item.Flag flag;

	// Token: 0x04001580 RID: 5504
	public bool requiredState;
}
