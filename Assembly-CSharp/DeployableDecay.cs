﻿using System;
using ConVar;

// Token: 0x02000382 RID: 898
public class DeployableDecay : global::Decay
{
	// Token: 0x06001555 RID: 5461 RVA: 0x0007B1CC File Offset: 0x000793CC
	public override float GetDecayDelay(global::BaseEntity entity)
	{
		return this.decayDelay * 60f * 60f;
	}

	// Token: 0x06001556 RID: 5462 RVA: 0x0007B1E0 File Offset: 0x000793E0
	public override float GetDecayDuration(global::BaseEntity entity)
	{
		return this.decayDuration * 60f * 60f;
	}

	// Token: 0x06001557 RID: 5463 RVA: 0x0007B1F4 File Offset: 0x000793F4
	public override bool ShouldDecay(global::BaseEntity entity)
	{
		return ConVar.Decay.upkeep || entity.IsOutside();
	}

	// Token: 0x04000FAF RID: 4015
	public float decayDelay = 8f;

	// Token: 0x04000FB0 RID: 4016
	public float decayDuration = 8f;
}
