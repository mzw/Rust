﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000052 RID: 82
public class BowWeapon : global::BaseProjectile
{
	// Token: 0x0600068F RID: 1679 RVA: 0x00028040 File Offset: 0x00026240
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BowWeapon.OnRpcMessage", 0.1f))
		{
			if (rpc == 740622307u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - BowReload ");
				}
				using (TimeWarning.New("BowReload", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("BowReload", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.BowReload(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in BowReload");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000690 RID: 1680 RVA: 0x0002821C File Offset: 0x0002641C
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void BowReload(global::BaseEntity.RPCMessage msg)
	{
		base.ReloadMagazine();
	}

	// Token: 0x06000691 RID: 1681 RVA: 0x00028224 File Offset: 0x00026424
	public override bool ForceSendMagazine()
	{
		return true;
	}
}
