﻿using System;
using System.Collections.Generic;

// Token: 0x020005A9 RID: 1449
public class GeneratePowerlineTopology : global::ProceduralComponent
{
	// Token: 0x06001E50 RID: 7760 RVA: 0x000A913C File Offset: 0x000A733C
	public override void Process(uint seed)
	{
		List<global::PathList> powerlines = global::TerrainMeta.Path.Powerlines;
		foreach (global::PathList pathList in powerlines)
		{
			pathList.Path.RecalculateTangents();
		}
	}
}
