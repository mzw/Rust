﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x02000381 RID: 897
public class DecayEntity : global::BaseCombatEntity
{
	// Token: 0x06001543 RID: 5443 RVA: 0x0007AA84 File Offset: 0x00078C84
	public override void ResetState()
	{
		base.ResetState();
		this.buildingID = 0u;
		this.decayTimer = 0f;
	}

	// Token: 0x06001544 RID: 5444 RVA: 0x0007AAA0 File Offset: 0x00078CA0
	public void AttachToBuilding(uint id)
	{
		if (base.isServer)
		{
			global::BuildingManager.server.Remove(this);
			this.buildingID = id;
			global::BuildingManager.server.Add(this);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x06001545 RID: 5445 RVA: 0x0007AAD4 File Offset: 0x00078CD4
	public global::BuildingManager.Building GetBuilding()
	{
		if (base.isServer)
		{
			return global::BuildingManager.server.GetBuilding(this.buildingID);
		}
		return null;
	}

	// Token: 0x06001546 RID: 5446 RVA: 0x0007AAF4 File Offset: 0x00078CF4
	public override global::BuildingPrivlidge GetBuildingPrivilege()
	{
		global::BuildingManager.Building building = this.GetBuilding();
		if (building != null)
		{
			return building.GetDominatingBuildingPrivilege();
		}
		return base.GetBuildingPrivilege();
	}

	// Token: 0x06001547 RID: 5447 RVA: 0x0007AB1C File Offset: 0x00078D1C
	public void CalculateUpkeepCostAmounts(List<global::ItemAmount> itemAmounts, float multiplier)
	{
		if (this.upkeep == null)
		{
			return;
		}
		float num = this.upkeep.upkeepMultiplier * multiplier;
		if (num == 0f)
		{
			return;
		}
		List<global::ItemAmount> list = this.BuildCost();
		if (list == null)
		{
			return;
		}
		foreach (global::ItemAmount itemAmount in list)
		{
			if (itemAmount.itemDef.category == global::ItemCategory.Resources)
			{
				float num2 = itemAmount.amount * num;
				bool flag = false;
				foreach (global::ItemAmount itemAmount2 in itemAmounts)
				{
					if (itemAmount2.itemDef == itemAmount.itemDef)
					{
						itemAmount2.amount += num2;
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					itemAmounts.Add(new global::ItemAmount(itemAmount.itemDef, num2));
				}
			}
		}
	}

	// Token: 0x06001548 RID: 5448 RVA: 0x0007AC54 File Offset: 0x00078E54
	public override void ServerInit()
	{
		base.ServerInit();
		this.decayVariance = Random.Range(0.95f, 1f);
		this.decay = global::PrefabAttribute.server.Find<global::Decay>(this.prefabID);
		this.decayPoints = global::PrefabAttribute.server.FindAll<global::DecayPoint>(this.prefabID);
		this.upkeep = global::PrefabAttribute.server.Find<global::Upkeep>(this.prefabID);
		global::BuildingManager.server.Add(this);
		if (!Application.isLoadingSave)
		{
			global::BuildingManager.server.CheckMerge(this);
		}
		this.lastDecayTick = UnityEngine.Time.time;
	}

	// Token: 0x06001549 RID: 5449 RVA: 0x0007ACEC File Offset: 0x00078EEC
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		global::BuildingManager.server.Remove(this);
		global::BuildingManager.server.CheckSplit(this);
	}

	// Token: 0x0600154A RID: 5450 RVA: 0x0007AD0C File Offset: 0x00078F0C
	public void AttachToBuilding(global::DecayEntity other)
	{
		if (other != null)
		{
			this.AttachToBuilding(other.buildingID);
			global::BuildingManager.server.CheckMerge(this);
		}
		else if (this is global::BuildingBlock)
		{
			this.AttachToBuilding(global::BuildingManager.server.NewBuildingID());
		}
		else
		{
			global::BuildingBlock nearbyBuildingBlock = this.GetNearbyBuildingBlock();
			if (nearbyBuildingBlock)
			{
				this.AttachToBuilding(nearbyBuildingBlock.buildingID);
			}
		}
	}

	// Token: 0x0600154B RID: 5451 RVA: 0x0007AD80 File Offset: 0x00078F80
	public global::BuildingBlock GetNearbyBuildingBlock()
	{
		float num = float.MaxValue;
		global::BuildingBlock result = null;
		Vector3 position = base.PivotPoint();
		List<global::BuildingBlock> list = Facepunch.Pool.GetList<global::BuildingBlock>();
		global::Vis.Entities<global::BuildingBlock>(position, 1.5f, list, 2097152, 2);
		for (int i = 0; i < list.Count; i++)
		{
			global::BuildingBlock buildingBlock = list[i];
			if (buildingBlock.isServer == base.isServer)
			{
				float num2 = buildingBlock.SqrDistance(position);
				if (num2 < num)
				{
					num = num2;
					result = buildingBlock;
				}
			}
		}
		Facepunch.Pool.FreeList<global::BuildingBlock>(ref list);
		return result;
	}

	// Token: 0x0600154C RID: 5452 RVA: 0x0007AE0C File Offset: 0x0007900C
	public void ResetUpkeepTime()
	{
		this.upkeepTimer = 0f;
	}

	// Token: 0x0600154D RID: 5453 RVA: 0x0007AE1C File Offset: 0x0007901C
	public void DecayTouch()
	{
		this.decayTimer = 0f;
	}

	// Token: 0x0600154E RID: 5454 RVA: 0x0007AE2C File Offset: 0x0007902C
	public void AddUpkeepTime(float time)
	{
		this.upkeepTimer -= time;
	}

	// Token: 0x0600154F RID: 5455 RVA: 0x0007AE3C File Offset: 0x0007903C
	public float GetProtectedSeconds()
	{
		return Mathf.Max(0f, -this.upkeepTimer);
	}

	// Token: 0x06001550 RID: 5456 RVA: 0x0007AE50 File Offset: 0x00079050
	public virtual void DecayTick()
	{
		if (this.decay == null)
		{
			return;
		}
		float num = UnityEngine.Time.time - this.lastDecayTick;
		if (num < ConVar.Decay.tick)
		{
			return;
		}
		this.lastDecayTick = UnityEngine.Time.time;
		if (!this.decay.ShouldDecay(this))
		{
			return;
		}
		float num2 = num * ConVar.Decay.scale;
		if (ConVar.Decay.upkeep)
		{
			this.upkeepTimer += num2;
			if (this.upkeepTimer > 0f)
			{
				global::BuildingPrivlidge buildingPrivilege = this.GetBuildingPrivilege();
				if (buildingPrivilege != null)
				{
					this.upkeepTimer -= buildingPrivilege.PurchaseUpkeepTime(this, Mathf.Max(this.upkeepTimer, 600f));
				}
			}
			if (this.upkeepTimer < 1f)
			{
				if (base.healthFraction < 1f && ConVar.Decay.upkeep_heal_scale > 0f && base.SecondsSinceAttacked > 600f)
				{
					float num3 = num / this.decay.GetDecayDuration(this) * ConVar.Decay.upkeep_heal_scale;
					this.Heal(this.MaxHealth() * num3);
				}
				return;
			}
			this.upkeepTimer = 1f;
		}
		this.decayTimer += num2;
		if (this.decayTimer < this.decay.GetDecayDelay(this))
		{
			return;
		}
		using (TimeWarning.New("DecayTick", 0.1f))
		{
			float num4 = 1f;
			if (ConVar.Decay.upkeep)
			{
				if (!this.IsOutside())
				{
					num4 *= ConVar.Decay.upkeep_inside_decay_scale;
				}
			}
			else
			{
				for (int i = 0; i < this.decayPoints.Length; i++)
				{
					global::DecayPoint decayPoint = this.decayPoints[i];
					if (decayPoint.IsOccupied(this))
					{
						num4 -= decayPoint.protection;
					}
				}
			}
			if (num4 > 0f)
			{
				float num5 = num2 / this.decay.GetDecayDuration(this);
				float num6 = num5 * this.MaxHealth();
				this.Hurt(num6 * num4 * this.decayVariance, Rust.DamageType.Decay, null, true);
			}
		}
	}

	// Token: 0x06001551 RID: 5457 RVA: 0x0007B080 File Offset: 0x00079280
	public override void OnRepairFinished()
	{
		base.OnRepairFinished();
		this.DecayTouch();
	}

	// Token: 0x06001552 RID: 5458 RVA: 0x0007B090 File Offset: 0x00079290
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.decayEntity = Facepunch.Pool.Get<ProtoBuf.DecayEntity>();
		info.msg.decayEntity.buildingID = this.buildingID;
		if (info.forDisk)
		{
			info.msg.decayEntity.decayTimer = this.decayTimer;
			info.msg.decayEntity.upkeepTimer = this.upkeepTimer;
		}
	}

	// Token: 0x06001553 RID: 5459 RVA: 0x0007B108 File Offset: 0x00079308
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.decayEntity != null)
		{
			this.decayTimer = info.msg.decayEntity.decayTimer;
			this.upkeepTimer = info.msg.decayEntity.upkeepTimer;
			if (this.buildingID != info.msg.decayEntity.buildingID)
			{
				this.AttachToBuilding(info.msg.decayEntity.buildingID);
				if (info.fromDisk)
				{
					global::BuildingManager.server.LoadBuildingID(this.buildingID);
				}
			}
		}
	}

	// Token: 0x04000FA7 RID: 4007
	[NonSerialized]
	public uint buildingID;

	// Token: 0x04000FA8 RID: 4008
	private float decayTimer;

	// Token: 0x04000FA9 RID: 4009
	private float upkeepTimer;

	// Token: 0x04000FAA RID: 4010
	private global::Upkeep upkeep;

	// Token: 0x04000FAB RID: 4011
	private global::Decay decay;

	// Token: 0x04000FAC RID: 4012
	private global::DecayPoint[] decayPoints;

	// Token: 0x04000FAD RID: 4013
	private float lastDecayTick;

	// Token: 0x04000FAE RID: 4014
	private float decayVariance = 1f;
}
