﻿using System;

// Token: 0x020000E2 RID: 226
public class Horse : global::BaseAnimalNPC
{
	// Token: 0x17000072 RID: 114
	// (get) Token: 0x06000B2C RID: 2860 RVA: 0x0004AB54 File Offset: 0x00048D54
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000B2D RID: 2861 RVA: 0x0004AB58 File Offset: 0x00048D58
	public override bool WantsToEat(global::BaseEntity best)
	{
		if (best.HasTrait(global::BaseEntity.TraitFlag.Alive))
		{
			return false;
		}
		if (best.HasTrait(global::BaseEntity.TraitFlag.Meat))
		{
			return false;
		}
		global::CollectibleEntity collectibleEntity = best as global::CollectibleEntity;
		if (collectibleEntity != null)
		{
			foreach (global::ItemAmount itemAmount in collectibleEntity.itemList)
			{
				if (itemAmount.itemDef.category == global::ItemCategory.Food)
				{
					return true;
				}
			}
		}
		return base.WantsToEat(best);
	}

	// Token: 0x06000B2E RID: 2862 RVA: 0x0004ABD0 File Offset: 0x00048DD0
	public override string Categorize()
	{
		return "Horse";
	}

	// Token: 0x040005EC RID: 1516
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population = 2f;
}
