﻿using System;
using UnityEngine;

// Token: 0x020007FA RID: 2042
public class ExplosionsScaleCurves : MonoBehaviour
{
	// Token: 0x060025BE RID: 9662 RVA: 0x000D0A10 File Offset: 0x000CEC10
	private void Awake()
	{
		this.t = base.transform;
	}

	// Token: 0x060025BF RID: 9663 RVA: 0x000D0A20 File Offset: 0x000CEC20
	private void OnEnable()
	{
		this.startTime = Time.time;
		this.evalX = 0f;
		this.evalY = 0f;
		this.evalZ = 0f;
	}

	// Token: 0x060025C0 RID: 9664 RVA: 0x000D0A50 File Offset: 0x000CEC50
	private void Update()
	{
		float num = Time.time - this.startTime;
		if (num <= this.GraphTimeMultiplier.x)
		{
			this.evalX = this.ScaleCurveX.Evaluate(num / this.GraphTimeMultiplier.x) * this.GraphScaleMultiplier.x;
		}
		if (num <= this.GraphTimeMultiplier.y)
		{
			this.evalY = this.ScaleCurveY.Evaluate(num / this.GraphTimeMultiplier.y) * this.GraphScaleMultiplier.y;
		}
		if (num <= this.GraphTimeMultiplier.z)
		{
			this.evalZ = this.ScaleCurveZ.Evaluate(num / this.GraphTimeMultiplier.z) * this.GraphScaleMultiplier.z;
		}
		this.t.localScale = new Vector3(this.evalX, this.evalY, this.evalZ);
	}

	// Token: 0x040021DC RID: 8668
	public AnimationCurve ScaleCurveX = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

	// Token: 0x040021DD RID: 8669
	public AnimationCurve ScaleCurveY = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

	// Token: 0x040021DE RID: 8670
	public AnimationCurve ScaleCurveZ = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

	// Token: 0x040021DF RID: 8671
	public Vector3 GraphTimeMultiplier = Vector3.one;

	// Token: 0x040021E0 RID: 8672
	public Vector3 GraphScaleMultiplier = Vector3.one;

	// Token: 0x040021E1 RID: 8673
	private float startTime;

	// Token: 0x040021E2 RID: 8674
	private Transform t;

	// Token: 0x040021E3 RID: 8675
	private float evalX;

	// Token: 0x040021E4 RID: 8676
	private float evalY;

	// Token: 0x040021E5 RID: 8677
	private float evalZ;
}
