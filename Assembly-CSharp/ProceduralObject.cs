﻿using System;
using UnityEngine;

// Token: 0x020005CA RID: 1482
public abstract class ProceduralObject : MonoBehaviour
{
	// Token: 0x06001EA2 RID: 7842 RVA: 0x000AC56C File Offset: 0x000AA76C
	protected void Awake()
	{
		if (SingletonComponent<global::WorldSetup>.Instance == null)
		{
			return;
		}
		if (SingletonComponent<global::WorldSetup>.Instance.ProceduralObjects == null)
		{
			Debug.LogError("WorldSetup.Instance.ProceduralObjects is null.", this);
		}
		else
		{
			SingletonComponent<global::WorldSetup>.Instance.ProceduralObjects.Add(this);
		}
	}

	// Token: 0x06001EA3 RID: 7843
	public abstract void Process();
}
