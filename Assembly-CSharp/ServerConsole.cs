﻿using System;
using ConVar;
using Facepunch;
using Facepunch.Extend;
using Network;
using Oxide.Core;
using UnityEngine;
using Windows;

// Token: 0x020002F8 RID: 760
public class ServerConsole : SingletonComponent<global::ServerConsole>
{
	// Token: 0x0600132F RID: 4911 RVA: 0x00070D50 File Offset: 0x0006EF50
	public void OnEnable()
	{
		if (Interface.CallHook("IOnEnableServerConsole", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		this.console.Initialize();
		this.input.OnInputText += this.OnInputText;
		Facepunch.Output.OnMessage += this.HandleLog;
		this.input.ClearLine(System.Console.WindowHeight);
		for (int i = 0; i < System.Console.WindowHeight; i++)
		{
			System.Console.WriteLine(string.Empty);
		}
	}

	// Token: 0x06001330 RID: 4912 RVA: 0x00070DDC File Offset: 0x0006EFDC
	private void OnDisable()
	{
		if (Interface.CallHook("IOnDisableServerConsole", null) != null)
		{
			return;
		}
		Facepunch.Output.OnMessage -= this.HandleLog;
		this.input.OnInputText -= this.OnInputText;
		this.console.Shutdown();
	}

	// Token: 0x06001331 RID: 4913 RVA: 0x00070E2C File Offset: 0x0006F02C
	private void OnInputText(string obj)
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Server, obj, new object[0]);
	}

	// Token: 0x06001332 RID: 4914 RVA: 0x00070E40 File Offset: 0x0006F040
	public static void PrintColoured(params object[] objects)
	{
		if (SingletonComponent<global::ServerConsole>.Instance == null)
		{
			return;
		}
		SingletonComponent<global::ServerConsole>.Instance.input.ClearLine(SingletonComponent<global::ServerConsole>.Instance.input.statusText.Length);
		for (int i = 0; i < objects.Length; i++)
		{
			if (i % 2 == 0)
			{
				System.Console.ForegroundColor = (ConsoleColor)objects[i];
			}
			else
			{
				System.Console.Write((string)objects[i]);
			}
		}
		if (System.Console.CursorLeft != 0)
		{
			System.Console.CursorTop++;
		}
		SingletonComponent<global::ServerConsole>.Instance.input.RedrawInputLine();
	}

	// Token: 0x06001333 RID: 4915 RVA: 0x00070EE0 File Offset: 0x0006F0E0
	private void HandleLog(string message, string stackTrace, LogType type)
	{
		if (message.StartsWith("[CHAT]"))
		{
			return;
		}
		if (type == 2)
		{
			if (message.StartsWith("HDR RenderTexture format is not") || message.StartsWith("The image effect") || message.StartsWith("Image Effects are not supported on this platform") || message.StartsWith("[AmplifyColor]") || message.StartsWith("Skipping profile frame."))
			{
				return;
			}
			System.Console.ForegroundColor = ConsoleColor.Yellow;
		}
		else if (type == null)
		{
			System.Console.ForegroundColor = ConsoleColor.Red;
		}
		else if (type == 4)
		{
			System.Console.ForegroundColor = ConsoleColor.Red;
		}
		else if (type == 1)
		{
			System.Console.ForegroundColor = ConsoleColor.Red;
		}
		else
		{
			System.Console.ForegroundColor = ConsoleColor.Gray;
		}
		this.input.ClearLine(this.input.statusText.Length);
		System.Console.WriteLine(message);
		this.input.RedrawInputLine();
	}

	// Token: 0x06001334 RID: 4916 RVA: 0x00070FCC File Offset: 0x0006F1CC
	private void Update()
	{
		this.UpdateStatus();
		this.input.Update();
	}

	// Token: 0x06001335 RID: 4917 RVA: 0x00070FE0 File Offset: 0x0006F1E0
	private void UpdateStatus()
	{
		if (this.nextUpdate > UnityEngine.Time.realtimeSinceStartup)
		{
			return;
		}
		if (Network.Net.sv == null || !Network.Net.sv.IsConnected())
		{
			return;
		}
		this.nextUpdate = UnityEngine.Time.realtimeSinceStartup + 0.33f;
		if (!this.input.valid)
		{
			return;
		}
		string text = NumberExtensions.FormatSeconds((long)UnityEngine.Time.realtimeSinceStartup);
		string text2 = this.currentGameTime.ToString("[H:mm]");
		string text3 = string.Concat(new object[]
		{
			" ",
			text2,
			" [",
			this.currentPlayerCount,
			"/",
			this.maxPlayerCount,
			"] ",
			ConVar.Server.hostname,
			" [",
			ConVar.Server.level,
			"]"
		});
		string text4 = string.Concat(new object[]
		{
			global::Performance.current.frameRate,
			"fps ",
			global::Performance.current.memoryCollections,
			"gc ",
			text,
			string.Empty
		});
		string text5 = NumberExtensions.FormatBytes<ulong>(Network.Net.sv.GetStat(null, 3), true) + "/s in, " + NumberExtensions.FormatBytes<ulong>(Network.Net.sv.GetStat(null, 1), true) + "/s out";
		string text6 = text4.PadLeft(this.input.lineWidth - 1);
		text6 = text3 + ((text3.Length >= text6.Length) ? string.Empty : text6.Substring(text3.Length));
		string text7 = string.Concat(new string[]
		{
			" ",
			this.currentEntityCount.ToString("n0"),
			" ents, ",
			this.currentSleeperCount.ToString("n0"),
			" slprs"
		});
		string text8 = text5.PadLeft(this.input.lineWidth - 1);
		text8 = text7 + ((text7.Length >= text8.Length) ? string.Empty : text8.Substring(text7.Length));
		this.input.statusText[0] = string.Empty;
		this.input.statusText[1] = text6;
		this.input.statusText[2] = text8;
	}

	// Token: 0x17000164 RID: 356
	// (get) Token: 0x06001336 RID: 4918 RVA: 0x00071260 File Offset: 0x0006F460
	private DateTime currentGameTime
	{
		get
		{
			if (!TOD_Sky.Instance)
			{
				return DateTime.Now;
			}
			return TOD_Sky.Instance.Cycle.DateTime;
		}
	}

	// Token: 0x17000165 RID: 357
	// (get) Token: 0x06001337 RID: 4919 RVA: 0x00071288 File Offset: 0x0006F488
	private int currentPlayerCount
	{
		get
		{
			return global::BasePlayer.activePlayerList.Count;
		}
	}

	// Token: 0x17000166 RID: 358
	// (get) Token: 0x06001338 RID: 4920 RVA: 0x00071294 File Offset: 0x0006F494
	private int maxPlayerCount
	{
		get
		{
			return ConVar.Server.maxplayers;
		}
	}

	// Token: 0x17000167 RID: 359
	// (get) Token: 0x06001339 RID: 4921 RVA: 0x0007129C File Offset: 0x0006F49C
	private int currentEntityCount
	{
		get
		{
			return global::BaseNetworkable.serverEntities.Count;
		}
	}

	// Token: 0x17000168 RID: 360
	// (get) Token: 0x0600133A RID: 4922 RVA: 0x000712A8 File Offset: 0x0006F4A8
	private int currentSleeperCount
	{
		get
		{
			return global::BasePlayer.sleepingPlayerList.Count;
		}
	}

	// Token: 0x04000DF0 RID: 3568
	private Windows.ConsoleWindow console = new Windows.ConsoleWindow();

	// Token: 0x04000DF1 RID: 3569
	private Windows.ConsoleInput input = new Windows.ConsoleInput();

	// Token: 0x04000DF2 RID: 3570
	private float nextUpdate;
}
