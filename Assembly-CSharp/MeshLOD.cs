﻿using System;
using UnityEngine;

// Token: 0x02000453 RID: 1107
public class MeshLOD : global::LODComponent, global::IBatchingHandler
{
	// Token: 0x04001341 RID: 4929
	[Horizontal(1, 0)]
	public global::MeshLOD.State[] States;

	// Token: 0x02000454 RID: 1108
	[Serializable]
	public class State
	{
		// Token: 0x04001342 RID: 4930
		public float distance;

		// Token: 0x04001343 RID: 4931
		public Mesh mesh;
	}
}
