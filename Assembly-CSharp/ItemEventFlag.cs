﻿using System;
using UnityEngine;
using UnityEngine.Events;

// Token: 0x020004BE RID: 1214
public class ItemEventFlag : MonoBehaviour, global::IItemUpdate
{
	// Token: 0x06001A05 RID: 6661 RVA: 0x000924C4 File Offset: 0x000906C4
	public void OnItemUpdate(global::Item item)
	{
		bool flag = item.HasFlag(this.flag);
		if (!this.firstRun && flag == this.lastState)
		{
			return;
		}
		if (flag)
		{
			this.onEnabled.Invoke();
		}
		else
		{
			this.onDisable.Invoke();
		}
		this.lastState = flag;
		this.firstRun = false;
	}

	// Token: 0x040014CE RID: 5326
	public global::Item.Flag flag;

	// Token: 0x040014CF RID: 5327
	public UnityEvent onEnabled = new UnityEvent();

	// Token: 0x040014D0 RID: 5328
	public UnityEvent onDisable = new UnityEvent();

	// Token: 0x040014D1 RID: 5329
	internal bool firstRun = true;

	// Token: 0x040014D2 RID: 5330
	internal bool lastState;
}
