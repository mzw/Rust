﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000705 RID: 1797
public class UIStyle_Menu_Input : MonoBehaviour, IClientComponent
{
	// Token: 0x0600223A RID: 8762 RVA: 0x000C0390 File Offset: 0x000BE590
	private void OnValidate()
	{
		base.GetComponent<Image>().color = Color.white;
		ColorBlock colors = base.GetComponent<InputField>().colors;
		colors.normalColor = new Color32(43, 41, 36, byte.MaxValue);
		colors.highlightedColor = new Color32(72, 86, 46, byte.MaxValue);
		colors.pressedColor = new Color32(37, 86, 122, byte.MaxValue);
		colors.disabledColor = new Color32(33, 31, 26, byte.MaxValue);
		base.GetComponent<InputField>().colors = colors;
	}

	// Token: 0x04001EBC RID: 7868
	public bool apply;
}
