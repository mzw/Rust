﻿using System;
using System.Diagnostics;
using Facepunch;
using Rust;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000682 RID: 1666
public class ErrorText : MonoBehaviour
{
	// Token: 0x060020E3 RID: 8419 RVA: 0x000BAB50 File Offset: 0x000B8D50
	public void OnEnable()
	{
		Facepunch.Output.OnMessage += this.CaptureLog;
	}

	// Token: 0x060020E4 RID: 8420 RVA: 0x000BAB64 File Offset: 0x000B8D64
	public void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		Facepunch.Output.OnMessage -= this.CaptureLog;
	}

	// Token: 0x060020E5 RID: 8421 RVA: 0x000BAB84 File Offset: 0x000B8D84
	internal void CaptureLog(string error, string stacktrace, LogType type)
	{
		if (type != null && type != 4 && type != 1)
		{
			return;
		}
		Text text = this.text;
		string text2 = text.text;
		text.text = string.Concat(new string[]
		{
			text2,
			error,
			"\n",
			stacktrace,
			"\n\n"
		});
		if (this.text.text.Length > this.maxLength)
		{
			this.text.text = this.text.text.Substring(this.text.text.Length - this.maxLength, this.maxLength);
		}
		this.stopwatch = Stopwatch.StartNew();
	}

	// Token: 0x060020E6 RID: 8422 RVA: 0x000BAC40 File Offset: 0x000B8E40
	protected void Update()
	{
		if (this.stopwatch != null && this.stopwatch.Elapsed.TotalSeconds > 30.0)
		{
			this.text.text = string.Empty;
			this.stopwatch = null;
		}
	}

	// Token: 0x04001C36 RID: 7222
	public Text text;

	// Token: 0x04001C37 RID: 7223
	public int maxLength = 1024;

	// Token: 0x04001C38 RID: 7224
	private Stopwatch stopwatch;
}
