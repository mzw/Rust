﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200071E RID: 1822
public abstract class UIRoot : MonoBehaviour
{
	// Token: 0x0600228A RID: 8842 RVA: 0x000C11A8 File Offset: 0x000BF3A8
	private void ToggleRaycasters(bool state)
	{
		for (int i = 0; i < this.graphicRaycasters.Length; i++)
		{
			GraphicRaycaster graphicRaycaster = this.graphicRaycasters[i];
			if (graphicRaycaster.enabled != state)
			{
				graphicRaycaster.enabled = state;
			}
		}
	}

	// Token: 0x0600228B RID: 8843 RVA: 0x000C11EC File Offset: 0x000BF3EC
	protected virtual void Awake()
	{
		Object.DontDestroyOnLoad(base.gameObject);
	}

	// Token: 0x0600228C RID: 8844 RVA: 0x000C11FC File Offset: 0x000BF3FC
	protected virtual void Start()
	{
		this.graphicRaycasters = base.GetComponentsInChildren<GraphicRaycaster>(true);
	}

	// Token: 0x0600228D RID: 8845 RVA: 0x000C120C File Offset: 0x000BF40C
	protected void Update()
	{
		this.Refresh();
	}

	// Token: 0x0600228E RID: 8846
	protected abstract void Refresh();

	// Token: 0x04001EFE RID: 7934
	private GraphicRaycaster[] graphicRaycasters;

	// Token: 0x04001EFF RID: 7935
	public Canvas overlayCanvas;
}
