﻿using System;
using UnityEngine;

// Token: 0x020007BD RID: 1981
public class RandomParameterNumber : StateMachineBehaviour
{
	// Token: 0x060024CB RID: 9419 RVA: 0x000CA62C File Offset: 0x000C882C
	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animator.SetInteger(this.parameterName, Random.Range(this.min, this.max));
	}

	// Token: 0x04002047 RID: 8263
	public string parameterName;

	// Token: 0x04002048 RID: 8264
	public int min;

	// Token: 0x04002049 RID: 8265
	public int max;
}
