﻿using System;
using UnityEngine;

// Token: 0x020000AD RID: 173
public class DropBox : global::Mailbox
{
	// Token: 0x06000A78 RID: 2680 RVA: 0x00048174 File Offset: 0x00046374
	public override bool PlayerIsOwner(global::BasePlayer player)
	{
		return this.PlayerBehind(player);
	}

	// Token: 0x06000A79 RID: 2681 RVA: 0x00048180 File Offset: 0x00046380
	public bool PlayerBehind(global::BasePlayer player)
	{
		float num = Vector3.Dot(base.transform.forward, (player.GetEstimatedWorldPosition() - base.GetEstimatedWorldPosition()).normalized);
		return num <= -0.3f;
	}

	// Token: 0x06000A7A RID: 2682 RVA: 0x000481C4 File Offset: 0x000463C4
	public bool PlayerInfront(global::BasePlayer player)
	{
		return Vector3.Dot(base.transform.forward, (player.GetEstimatedWorldPosition() - base.transform.position).normalized) >= 0.7f;
	}
}
