﻿using System;
using UnityEngine;

// Token: 0x02000724 RID: 1828
public class UISleepingScreen : SingletonComponent<global::UISleepingScreen>, global::IUIScreen
{
	// Token: 0x0600229A RID: 8858 RVA: 0x000C12F8 File Offset: 0x000BF4F8
	protected override void Awake()
	{
		base.Awake();
		this.canvasGroup = base.GetComponent<CanvasGroup>();
	}

	// Token: 0x0600229B RID: 8859 RVA: 0x000C130C File Offset: 0x000BF50C
	public void SetVisible(bool b)
	{
		this.canvasGroup.alpha = ((!b) ? 0f : 1f);
	}

	// Token: 0x04001F05 RID: 7941
	protected CanvasGroup canvasGroup;
}
