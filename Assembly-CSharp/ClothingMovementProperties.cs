﻿using System;
using UnityEngine;

// Token: 0x0200046D RID: 1133
[CreateAssetMenu(menuName = "Rust/Clothing Movement Properties")]
public class ClothingMovementProperties : ScriptableObject
{
	// Token: 0x0400138B RID: 5003
	public float speedReduction;

	// Token: 0x0400138C RID: 5004
	[Tooltip("If this piece of clothing is worn movement speed will be reduced by atleast this much")]
	public float minSpeedReduction;
}
