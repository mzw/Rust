﻿using System;
using UnityEngine;

// Token: 0x0200075F RID: 1887
public static class ArrayEx
{
	// Token: 0x0600233B RID: 9019 RVA: 0x000C2F78 File Offset: 0x000C1178
	public static T GetRandom<T>(this T[] array)
	{
		if (array == null || array.Length == 0)
		{
			return default(T);
		}
		return array[Random.Range(0, array.Length)];
	}

	// Token: 0x0600233C RID: 9020 RVA: 0x000C2FAC File Offset: 0x000C11AC
	public static T GetRandom<T>(this T[] array, uint seed)
	{
		if (array == null || array.Length == 0)
		{
			return default(T);
		}
		return array[SeedRandom.Range(ref seed, 0, array.Length)];
	}

	// Token: 0x0600233D RID: 9021 RVA: 0x000C2FE4 File Offset: 0x000C11E4
	public static T GetRandom<T>(this T[] array, ref uint seed)
	{
		if (array == null || array.Length == 0)
		{
			return default(T);
		}
		return array[SeedRandom.Range(ref seed, 0, array.Length)];
	}

	// Token: 0x0600233E RID: 9022 RVA: 0x000C301C File Offset: 0x000C121C
	public static void Shuffle<T>(this T[] array, uint seed)
	{
		array.Shuffle(ref seed);
	}

	// Token: 0x0600233F RID: 9023 RVA: 0x000C3028 File Offset: 0x000C1228
	public static void Shuffle<T>(this T[] array, ref uint seed)
	{
		for (int i = 0; i < array.Length; i++)
		{
			int num = SeedRandom.Range(ref seed, 0, array.Length);
			int num2 = SeedRandom.Range(ref seed, 0, array.Length);
			T t = array[num];
			array[num] = array[num2];
			array[num2] = t;
		}
	}

	// Token: 0x06002340 RID: 9024 RVA: 0x000C3080 File Offset: 0x000C1280
	public static void BubbleSort<T>(this T[] array) where T : IComparable<T>
	{
		for (int i = 1; i < array.Length; i++)
		{
			T t = array[i];
			for (int j = i - 1; j >= 0; j--)
			{
				T t2 = array[j];
				if (t.CompareTo(t2) >= 0)
				{
					break;
				}
				array[j + 1] = t2;
				array[j] = t;
			}
		}
	}
}
