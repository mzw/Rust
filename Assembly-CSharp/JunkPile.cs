﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using UnityEngine;

// Token: 0x02000073 RID: 115
public class JunkPile : global::BaseEntity
{
	// Token: 0x0600080E RID: 2062 RVA: 0x00034440 File Offset: 0x00032640
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("JunkPile.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600080F RID: 2063 RVA: 0x00034488 File Offset: 0x00032688
	public override void ServerInit()
	{
		base.ServerInit();
		base.Invoke(new Action(this.TimeOut), 1800f);
		base.InvokeRepeating(new Action(this.CheckEmpty), 10f, 30f);
		foreach (global::SpawnGroup spawnGroup in this.spawngroups)
		{
			spawnGroup.SpawnInitial();
		}
	}

	// Token: 0x06000810 RID: 2064 RVA: 0x000344F4 File Offset: 0x000326F4
	public bool SpawnGroupsEmpty()
	{
		foreach (global::SpawnGroup spawnGroup in this.spawngroups)
		{
			if (spawnGroup.currentPopulation > 0)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000811 RID: 2065 RVA: 0x00034530 File Offset: 0x00032730
	public void CheckEmpty()
	{
		if (this.SpawnGroupsEmpty())
		{
			base.CancelInvoke(new Action(this.CheckEmpty));
			this.SinkAndDestroy();
		}
	}

	// Token: 0x06000812 RID: 2066 RVA: 0x00034558 File Offset: 0x00032758
	public void TimeOut()
	{
		if (this.SpawnGroupsEmpty())
		{
			this.SinkAndDestroy();
			return;
		}
		List<global::BasePlayer> list = Pool.GetList<global::BasePlayer>();
		global::Vis.Entities<global::BasePlayer>(base.transform.position, 15f, list, 131072, 2);
		bool flag = false;
		foreach (global::BasePlayer basePlayer in list)
		{
			if (!basePlayer.IsSleeping() && basePlayer.IsAlive())
			{
				flag = true;
				break;
			}
		}
		if (flag)
		{
			base.Invoke(new Action(this.TimeOut), 300f);
		}
		else
		{
			this.SinkAndDestroy();
		}
		Pool.FreeList<global::BasePlayer>(ref list);
	}

	// Token: 0x06000813 RID: 2067 RVA: 0x0003462C File Offset: 0x0003282C
	public void SinkAndDestroy()
	{
		base.CancelInvoke(new Action(this.SinkAndDestroy));
		foreach (global::SpawnGroup spawnGroup in this.spawngroups)
		{
			spawnGroup.Clear();
		}
		base.ClientRPC(null, "CLIENT_StartSink");
		base.transform.position -= new Vector3(0f, 5f, 0f);
		base.Invoke(new Action(this.KillMe), 22f);
	}

	// Token: 0x06000814 RID: 2068 RVA: 0x000346C0 File Offset: 0x000328C0
	public void KillMe()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x040003D1 RID: 977
	public global::GameObjectRef sinkEffect;

	// Token: 0x040003D2 RID: 978
	public global::SpawnGroup[] spawngroups;

	// Token: 0x040003D3 RID: 979
	private const float lifetimeMinutes = 30f;
}
