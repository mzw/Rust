﻿using System;
using ConVar;
using Facepunch;
using Network;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200006D RID: 109
public class HeldEntity : global::BaseEntity
{
	// Token: 0x060007E8 RID: 2024 RVA: 0x00033C5C File Offset: 0x00031E5C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("HeldEntity.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x1700005C RID: 92
	// (get) Token: 0x060007E9 RID: 2025 RVA: 0x00033CA4 File Offset: 0x00031EA4
	public bool hostile
	{
		get
		{
			return this.hostileScore > 0f;
		}
	}

	// Token: 0x060007EA RID: 2026 RVA: 0x00033CB4 File Offset: 0x00031EB4
	public bool LightsOn()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved5);
	}

	// Token: 0x060007EB RID: 2027 RVA: 0x00033CC4 File Offset: 0x00031EC4
	public bool IsDeployed()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved4);
	}

	// Token: 0x060007EC RID: 2028 RVA: 0x00033CD4 File Offset: 0x00031ED4
	public global::BasePlayer GetOwnerPlayer()
	{
		global::BaseEntity parentEntity = base.GetParentEntity();
		if (!parentEntity.IsValid())
		{
			return null;
		}
		global::BasePlayer basePlayer = parentEntity.ToPlayer();
		if (basePlayer == null)
		{
			return null;
		}
		if (basePlayer.IsDead())
		{
			return null;
		}
		return basePlayer;
	}

	// Token: 0x060007ED RID: 2029 RVA: 0x00033D18 File Offset: 0x00031F18
	public Connection GetOwnerConnection()
	{
		global::BasePlayer ownerPlayer = this.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return null;
		}
		if (ownerPlayer.net == null)
		{
			return null;
		}
		return ownerPlayer.net.connection;
	}

	// Token: 0x060007EE RID: 2030 RVA: 0x00033D54 File Offset: 0x00031F54
	public virtual void SetOwnerPlayer(global::BasePlayer player)
	{
		Assert.IsTrue(base.isServer, "Should be server!");
		Assert.IsTrue(player.isServer, "Player should be serverside!");
		base.gameObject.Identity();
		base.SetParent(player, this.handBone);
		this.SetHeld(false);
	}

	// Token: 0x060007EF RID: 2031 RVA: 0x00033DA0 File Offset: 0x00031FA0
	public virtual void ClearOwnerPlayer()
	{
		Assert.IsTrue(base.isServer, "Should be server!");
		base.SetParent(null, 0u);
		this.SetHeld(false);
	}

	// Token: 0x060007F0 RID: 2032 RVA: 0x00033DC4 File Offset: 0x00031FC4
	public virtual void SetVisibleWhileHolstered(bool visible)
	{
		if (!this.holsterInfo.displayWhenHolstered)
		{
			return;
		}
		this.holsterVisible = visible;
		this.UpdateHeldItemVisibility();
	}

	// Token: 0x060007F1 RID: 2033 RVA: 0x00033DE4 File Offset: 0x00031FE4
	public uint GetBone(string bone)
	{
		return global::StringPool.Get(bone);
	}

	// Token: 0x060007F2 RID: 2034 RVA: 0x00033DEC File Offset: 0x00031FEC
	public virtual void SetLightsOn(bool isOn)
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved5, isOn, false);
	}

	// Token: 0x060007F3 RID: 2035 RVA: 0x00033DFC File Offset: 0x00031FFC
	public void UpdateHeldItemVisibility()
	{
		if (!this.GetOwnerPlayer())
		{
			return;
		}
		bool flag = this.GetOwnerPlayer().GetHeldEntity() == this;
		bool flag2;
		if (!ConVar.Server.showHolsteredItems && !flag)
		{
			flag2 = this.UpdateVisiblity_Invis();
		}
		else if (flag)
		{
			flag2 = this.UpdateVisibility_Hand();
		}
		else if (this.holsterVisible)
		{
			flag2 = this.UpdateVisiblity_Holster();
		}
		else
		{
			flag2 = this.UpdateVisiblity_Invis();
		}
		if (flag2)
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x060007F4 RID: 2036 RVA: 0x00033E88 File Offset: 0x00032088
	public bool UpdateVisibility_Hand()
	{
		if (this.currentVisState == global::HeldEntity.heldEntityVisState.Hand)
		{
			return false;
		}
		this.currentVisState = global::HeldEntity.heldEntityVisState.Hand;
		base.limitNetworking = false;
		base.SetFlag(global::BaseEntity.Flags.Disabled, false, false);
		base.SetParent(this.GetOwnerPlayer(), this.GetBone(this.handBone));
		return true;
	}

	// Token: 0x060007F5 RID: 2037 RVA: 0x00033ED4 File Offset: 0x000320D4
	public bool UpdateVisiblity_Holster()
	{
		if (this.currentVisState == global::HeldEntity.heldEntityVisState.Holster)
		{
			return false;
		}
		this.currentVisState = global::HeldEntity.heldEntityVisState.Holster;
		base.limitNetworking = false;
		base.SetFlag(global::BaseEntity.Flags.Disabled, false, false);
		base.SetParent(this.GetOwnerPlayer(), this.GetBone(this.holsterInfo.holsterBone));
		this.SetLightsOn(false);
		return true;
	}

	// Token: 0x060007F6 RID: 2038 RVA: 0x00033F2C File Offset: 0x0003212C
	public bool UpdateVisiblity_Invis()
	{
		if (this.currentVisState == global::HeldEntity.heldEntityVisState.Invis)
		{
			return false;
		}
		this.currentVisState = global::HeldEntity.heldEntityVisState.Invis;
		base.SetParent(this.GetOwnerPlayer(), this.GetBone(this.handBone));
		base.limitNetworking = true;
		base.SetFlag(global::BaseEntity.Flags.Disabled, true, false);
		return true;
	}

	// Token: 0x060007F7 RID: 2039 RVA: 0x00033F78 File Offset: 0x00032178
	public virtual void SetHeld(bool bHeld)
	{
		Assert.IsTrue(base.isServer, "Should be server!");
		base.SetFlag(global::BaseEntity.Flags.Reserved4, bHeld, false);
		if (!bHeld)
		{
			this.UpdateVisiblity_Invis();
		}
		base.limitNetworking = !bHeld;
		base.SetFlag(global::BaseEntity.Flags.Disabled, !bHeld, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		this.OnHeldChanged();
	}

	// Token: 0x060007F8 RID: 2040 RVA: 0x00033FD4 File Offset: 0x000321D4
	public virtual void OnHeldChanged()
	{
	}

	// Token: 0x060007F9 RID: 2041 RVA: 0x00033FD8 File Offset: 0x000321D8
	public virtual bool CanBeUsedInWater()
	{
		return false;
	}

	// Token: 0x060007FA RID: 2042 RVA: 0x00033FDC File Offset: 0x000321DC
	protected global::Item GetOwnerItem()
	{
		global::BasePlayer ownerPlayer = this.GetOwnerPlayer();
		if (ownerPlayer == null || ownerPlayer.inventory == null)
		{
			return null;
		}
		return ownerPlayer.inventory.FindItemUID(this.ownerItemUID);
	}

	// Token: 0x060007FB RID: 2043 RVA: 0x00034020 File Offset: 0x00032220
	public override global::Item GetItem()
	{
		return this.GetOwnerItem();
	}

	// Token: 0x060007FC RID: 2044 RVA: 0x00034028 File Offset: 0x00032228
	public global::ItemDefinition GetOwnerItemDefinition()
	{
		global::Item ownerItem = this.GetOwnerItem();
		if (ownerItem == null)
		{
			Debug.LogWarning("GetOwnerItem - null!", this);
			return null;
		}
		return ownerItem.info;
	}

	// Token: 0x060007FD RID: 2045 RVA: 0x00034058 File Offset: 0x00032258
	public virtual void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
	}

	// Token: 0x060007FE RID: 2046 RVA: 0x0003405C File Offset: 0x0003225C
	public virtual void ReturnedFromCancelledCraft(global::Item item, global::BasePlayer crafter)
	{
	}

	// Token: 0x060007FF RID: 2047 RVA: 0x00034060 File Offset: 0x00032260
	public virtual void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
	}

	// Token: 0x06000800 RID: 2048 RVA: 0x00034064 File Offset: 0x00032264
	public virtual void SetupHeldEntity(global::Item item)
	{
		this.ownerItemUID = item.uid;
		this.InitOwnerPlayer();
	}

	// Token: 0x06000801 RID: 2049 RVA: 0x00034078 File Offset: 0x00032278
	public override void PostServerLoad()
	{
		this.InitOwnerPlayer();
	}

	// Token: 0x06000802 RID: 2050 RVA: 0x00034080 File Offset: 0x00032280
	private void InitOwnerPlayer()
	{
		global::BasePlayer ownerPlayer = this.GetOwnerPlayer();
		if (ownerPlayer != null)
		{
			this.SetOwnerPlayer(ownerPlayer);
		}
		else
		{
			this.ClearOwnerPlayer();
		}
	}

	// Token: 0x06000803 RID: 2051 RVA: 0x000340B4 File Offset: 0x000322B4
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.heldEntity = Facepunch.Pool.Get<ProtoBuf.HeldEntity>();
		info.msg.heldEntity.itemUID = this.ownerItemUID;
	}

	// Token: 0x06000804 RID: 2052 RVA: 0x000340E8 File Offset: 0x000322E8
	public void DestroyThis()
	{
		global::Item ownerItem = this.GetOwnerItem();
		if (ownerItem != null)
		{
			ownerItem.Remove(0f);
		}
	}

	// Token: 0x06000805 RID: 2053 RVA: 0x00034110 File Offset: 0x00032310
	protected bool HasItemAmount()
	{
		global::Item ownerItem = this.GetOwnerItem();
		return ownerItem != null && ownerItem.amount > 0;
	}

	// Token: 0x06000806 RID: 2054 RVA: 0x00034138 File Offset: 0x00032338
	protected bool UseItemAmount(int iAmount)
	{
		if (iAmount <= 0)
		{
			return true;
		}
		global::Item ownerItem = this.GetOwnerItem();
		if (ownerItem == null)
		{
			this.DestroyThis();
			return true;
		}
		ownerItem.amount -= iAmount;
		ownerItem.MarkDirty();
		if (ownerItem.amount <= 0)
		{
			this.DestroyThis();
			return true;
		}
		return false;
	}

	// Token: 0x06000807 RID: 2055 RVA: 0x0003418C File Offset: 0x0003238C
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.heldEntity != null)
		{
			this.ownerItemUID = info.msg.heldEntity.itemUID;
		}
	}

	// Token: 0x06000808 RID: 2056 RVA: 0x000341C0 File Offset: 0x000323C0
	public void SendPunch(Vector3 amount, float duration)
	{
		base.ClientRPCPlayer<Vector3, float>(null, this.GetOwnerPlayer(), "CL_Punch", amount, duration);
	}

	// Token: 0x040003B4 RID: 948
	public Animator worldModelAnimator;

	// Token: 0x040003B5 RID: 949
	public global::SoundDefinition thirdPersonDeploySound;

	// Token: 0x040003B6 RID: 950
	public global::SoundDefinition thirdPersonAimSound;

	// Token: 0x040003B7 RID: 951
	public global::SoundDefinition thirdPersonAimEndSound;

	// Token: 0x040003B8 RID: 952
	[Header("Held Entity")]
	public string handBone = "r_prop";

	// Token: 0x040003B9 RID: 953
	public AnimatorOverrideController HoldAnimationOverride;

	// Token: 0x040003BA RID: 954
	public global::NPCPlayerApex.ToolTypeEnum toolType;

	// Token: 0x040003BB RID: 955
	[Header("Hostility")]
	public float hostileScore;

	// Token: 0x040003BC RID: 956
	public global::HeldEntity.HolsterInfo holsterInfo;

	// Token: 0x040003BD RID: 957
	private bool holsterVisible;

	// Token: 0x040003BE RID: 958
	private global::HeldEntity.heldEntityVisState currentVisState;

	// Token: 0x040003BF RID: 959
	internal uint ownerItemUID;

	// Token: 0x0200006E RID: 110
	[Serializable]
	public class HolsterInfo
	{
		// Token: 0x040003C0 RID: 960
		public global::HeldEntity.HolsterInfo.HolsterSlot slot;

		// Token: 0x040003C1 RID: 961
		public bool displayWhenHolstered;

		// Token: 0x040003C2 RID: 962
		public string holsterBone = "spine3";

		// Token: 0x040003C3 RID: 963
		public Vector3 holsterOffset;

		// Token: 0x040003C4 RID: 964
		public Vector3 holsterRotationOffset;

		// Token: 0x0200006F RID: 111
		public enum HolsterSlot
		{
			// Token: 0x040003C6 RID: 966
			BACK,
			// Token: 0x040003C7 RID: 967
			RIGHT_THIGH,
			// Token: 0x040003C8 RID: 968
			LEFT_THIGH
		}
	}

	// Token: 0x02000070 RID: 112
	public static class HeldEntityFlags
	{
		// Token: 0x040003C9 RID: 969
		public const global::BaseEntity.Flags Deployed = global::BaseEntity.Flags.Reserved4;

		// Token: 0x040003CA RID: 970
		public const global::BaseEntity.Flags LightsOn = global::BaseEntity.Flags.Reserved5;
	}

	// Token: 0x02000071 RID: 113
	public enum heldEntityVisState
	{
		// Token: 0x040003CC RID: 972
		UNSET,
		// Token: 0x040003CD RID: 973
		Invis,
		// Token: 0x040003CE RID: 974
		Hand,
		// Token: 0x040003CF RID: 975
		Holster
	}
}
