﻿using System;

// Token: 0x02000462 RID: 1122
public enum MonumentType
{
	// Token: 0x04001364 RID: 4964
	Cave,
	// Token: 0x04001365 RID: 4965
	Airport,
	// Token: 0x04001366 RID: 4966
	Building,
	// Token: 0x04001367 RID: 4967
	Town,
	// Token: 0x04001368 RID: 4968
	Radtown,
	// Token: 0x04001369 RID: 4969
	Lighthouse
}
