﻿using System;
using UnityEngine;

// Token: 0x02000215 RID: 533
public class DirectionProperties : global::PrefabAttribute
{
	// Token: 0x06000FA4 RID: 4004 RVA: 0x0005F9DC File Offset: 0x0005DBDC
	protected override Type GetIndexedType()
	{
		return typeof(global::DirectionProperties);
	}

	// Token: 0x06000FA5 RID: 4005 RVA: 0x0005F9E8 File Offset: 0x0005DBE8
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		Gizmos.matrix = base.transform.localToWorldMatrix;
		global::GizmosUtil.DrawSemiCircle(200f);
		Gizmos.DrawWireCube(this.bounds.center, this.bounds.size);
	}

	// Token: 0x06000FA6 RID: 4006 RVA: 0x0005FA34 File Offset: 0x0005DC34
	public bool IsWeakspot(Transform tx, global::HitInfo info)
	{
		if (this.bounds.size == Vector3.zero)
		{
			return false;
		}
		Vector3 vector = tx.worldToLocalMatrix.MultiplyPoint3x4(info.PointStart);
		float num = Vector3Ex.DotDegrees(this.worldForward, vector);
		OBB obb;
		obb..ctor(tx.position + tx.rotation * (this.worldRotation * this.bounds.center + this.worldPosition), this.bounds.size, tx.rotation * this.worldRotation);
		return num > 100f && obb.Contains(info.HitPositionWorld);
	}

	// Token: 0x04000A72 RID: 2674
	private const float radius = 200f;

	// Token: 0x04000A73 RID: 2675
	public Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

	// Token: 0x04000A74 RID: 2676
	public global::ProtectionProperties extraProtection;
}
