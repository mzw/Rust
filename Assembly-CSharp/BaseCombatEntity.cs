﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200000F RID: 15
public class BaseCombatEntity : global::BaseEntity
{
	// Token: 0x06000323 RID: 803 RVA: 0x000120F8 File Offset: 0x000102F8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseCombatEntity.OnRpcMessage", 0.1f))
		{
			if (rpc == 4143599628u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_PickupStart ");
				}
				using (TimeWarning.New("RPC_PickupStart", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_PickupStart", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_PickupStart(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_PickupStart");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000324 RID: 804 RVA: 0x000122D8 File Offset: 0x000104D8
	public virtual bool IsDead()
	{
		return this.lifestate == global::BaseCombatEntity.LifeState.Dead;
	}

	// Token: 0x06000325 RID: 805 RVA: 0x000122E4 File Offset: 0x000104E4
	public virtual bool IsAlive()
	{
		return this.lifestate == global::BaseCombatEntity.LifeState.Alive;
	}

	// Token: 0x17000004 RID: 4
	// (get) Token: 0x06000326 RID: 806 RVA: 0x000122F0 File Offset: 0x000104F0
	// (set) Token: 0x06000327 RID: 807 RVA: 0x000122F8 File Offset: 0x000104F8
	public Vector3 LastAttackedDir { get; protected set; }

	// Token: 0x17000005 RID: 5
	// (get) Token: 0x06000328 RID: 808 RVA: 0x00012304 File Offset: 0x00010504
	public float SecondsSinceAttacked
	{
		get
		{
			return UnityEngine.Time.time - this.lastAttackedTime;
		}
	}

	// Token: 0x17000006 RID: 6
	// (get) Token: 0x06000329 RID: 809 RVA: 0x00012314 File Offset: 0x00010514
	public float SecondsSinceDealtDamage
	{
		get
		{
			return UnityEngine.Time.time - this.lastDealtDamageTime;
		}
	}

	// Token: 0x17000007 RID: 7
	// (get) Token: 0x0600032A RID: 810 RVA: 0x00012324 File Offset: 0x00010524
	// (set) Token: 0x0600032B RID: 811 RVA: 0x00012334 File Offset: 0x00010534
	public float healthFraction
	{
		get
		{
			return this.Health() / this.MaxHealth();
		}
		set
		{
			this.health = this.MaxHealth() * value;
		}
	}

	// Token: 0x0600032C RID: 812 RVA: 0x00012344 File Offset: 0x00010544
	public override void ResetState()
	{
		base.ResetState();
		this._health = this._maxHealth;
		this.lastAttackedTime = float.NegativeInfinity;
		this.lastDealtDamageTime = float.NegativeInfinity;
	}

	// Token: 0x0600032D RID: 813 RVA: 0x00012370 File Offset: 0x00010570
	public override void DestroyShared()
	{
		base.DestroyShared();
		if (base.isServer)
		{
			this.UpdateSurroundings();
		}
	}

	// Token: 0x0600032E RID: 814 RVA: 0x0001238C File Offset: 0x0001058C
	public virtual float GetThreatLevel()
	{
		return 0f;
	}

	// Token: 0x0600032F RID: 815 RVA: 0x00012394 File Offset: 0x00010594
	public override float PenetrationResistance(global::HitInfo info)
	{
		return (!this.baseProtection) ? 1f : this.baseProtection.density;
	}

	// Token: 0x06000330 RID: 816 RVA: 0x000123BC File Offset: 0x000105BC
	public virtual void ScaleDamage(global::HitInfo info)
	{
		if (info.UseProtection && this.baseProtection != null)
		{
			this.baseProtection.Scale(info.damageTypes, 1f);
		}
	}

	// Token: 0x06000331 RID: 817 RVA: 0x000123F0 File Offset: 0x000105F0
	public global::HitArea SkeletonLookup(uint boneID)
	{
		if (this.skeletonProperties == null)
		{
			return (global::HitArea)-1;
		}
		global::SkeletonProperties.BoneProperty boneProperty = this.skeletonProperties.FindBone(boneID);
		if (boneProperty == null)
		{
			return (global::HitArea)-1;
		}
		return boneProperty.area;
	}

	// Token: 0x06000332 RID: 818 RVA: 0x0001242C File Offset: 0x0001062C
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.baseCombat = Facepunch.Pool.Get<BaseCombat>();
		info.msg.baseCombat.state = (int)this.lifestate;
		info.msg.baseCombat.health = this._health;
	}

	// Token: 0x06000333 RID: 819 RVA: 0x00012480 File Offset: 0x00010680
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		if (this.Health() > this.MaxHealth())
		{
			this.health = this.MaxHealth();
		}
		if (float.IsNaN(this.Health()))
		{
			this.health = this.MaxHealth();
		}
	}

	// Token: 0x06000334 RID: 820 RVA: 0x000124CC File Offset: 0x000106CC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		if (base.isServer)
		{
			this.lifestate = global::BaseCombatEntity.LifeState.Alive;
		}
		if (info.msg.baseCombat != null)
		{
			this.lifestate = (global::BaseCombatEntity.LifeState)info.msg.baseCombat.state;
			this._health = info.msg.baseCombat.health;
		}
		base.Load(info);
	}

	// Token: 0x17000008 RID: 8
	// (get) Token: 0x06000335 RID: 821 RVA: 0x00012534 File Offset: 0x00010734
	// (set) Token: 0x06000336 RID: 822 RVA: 0x0001253C File Offset: 0x0001073C
	public float health
	{
		get
		{
			return this._health;
		}
		set
		{
			float health = this._health;
			this._health = Mathf.Clamp(value, 0f, this.MaxHealth());
			if (base.isServer && this._health != health)
			{
				this.OnHealthChanged(health, this._health);
			}
		}
	}

	// Token: 0x06000337 RID: 823 RVA: 0x0001258C File Offset: 0x0001078C
	public override float Health()
	{
		return this._health;
	}

	// Token: 0x06000338 RID: 824 RVA: 0x00012594 File Offset: 0x00010794
	public override float MaxHealth()
	{
		return this._maxHealth;
	}

	// Token: 0x06000339 RID: 825 RVA: 0x0001259C File Offset: 0x0001079C
	public virtual float StartHealth()
	{
		return this.startHealth;
	}

	// Token: 0x0600033A RID: 826 RVA: 0x000125A4 File Offset: 0x000107A4
	public virtual float StartMaxHealth()
	{
		return this.StartHealth();
	}

	// Token: 0x0600033B RID: 827 RVA: 0x000125AC File Offset: 0x000107AC
	public void DoHitNotify(global::HitInfo info)
	{
		using (TimeWarning.New("DoHitNotify", 0.1f))
		{
			if (this.sendsHitNotification && !(info.Initiator == null) && info.Initiator is global::BasePlayer && !info.isHeadshot && !(this == info.Initiator))
			{
				if (UnityEngine.Time.frameCount != this.lastNotifyFrame)
				{
					this.lastNotifyFrame = UnityEngine.Time.frameCount;
					bool flag = info.Weapon is global::BaseMelee;
					if (base.isServer && (!flag || this.sendsMeleeHitNotification))
					{
						bool arg = info.Initiator.net.connection == info.Predicted;
						base.ClientRPCPlayer<bool>(null, info.Initiator as global::BasePlayer, "HitNotify", arg);
					}
				}
			}
		}
	}

	// Token: 0x0600033C RID: 828 RVA: 0x000126B4 File Offset: 0x000108B4
	public override void OnAttacked(global::HitInfo info)
	{
		using (TimeWarning.New("BaseCombatEntity.OnAttacked", 0.1f))
		{
			if (!this.IsDead())
			{
				this.DoHitNotify(info);
			}
			if (base.isServer)
			{
				this.Hurt(info);
			}
		}
		base.OnAttacked(info);
	}

	// Token: 0x0600033D RID: 829 RVA: 0x00012720 File Offset: 0x00010920
	public virtual bool CanPickup(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanPickupEntity", new object[]
		{
			this,
			player
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.pickup.enabled && (!this.pickup.requireBuildingPrivilege || (player.CanBuild() && (!this.pickup.requireHammer || player.IsHoldingEntity<global::Hammer>())));
	}

	// Token: 0x0600033E RID: 830 RVA: 0x000127A8 File Offset: 0x000109A8
	public virtual void OnPickedUp(global::Item createdItem, global::BasePlayer player)
	{
	}

	// Token: 0x0600033F RID: 831 RVA: 0x000127AC File Offset: 0x000109AC
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_PickupStart(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.CanPickup(rpc.player))
		{
			return;
		}
		global::Item item = global::ItemManager.Create(this.pickup.itemTarget, this.pickup.itemCount, this.skinID);
		if (this.pickup.setConditionFromHealth && item.hasCondition)
		{
			item.conditionNormalized = Mathf.Clamp01(this.healthFraction - this.pickup.subtractCondition);
		}
		rpc.player.GiveItem(item, global::BaseEntity.GiveItemReason.PickedUp);
		this.OnPickedUp(item, rpc.player);
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06000340 RID: 832 RVA: 0x0001285C File Offset: 0x00010A5C
	public virtual List<global::ItemAmount> BuildCost()
	{
		if (this.repair.itemTarget == null)
		{
			return null;
		}
		global::ItemBlueprint itemBlueprint = global::ItemManager.FindBlueprint(this.repair.itemTarget);
		if (itemBlueprint == null)
		{
			return null;
		}
		return itemBlueprint.ingredients;
	}

	// Token: 0x06000341 RID: 833 RVA: 0x000128A8 File Offset: 0x00010AA8
	public virtual float RepairCostFraction()
	{
		return 0.5f;
	}

	// Token: 0x06000342 RID: 834 RVA: 0x000128B0 File Offset: 0x00010AB0
	public virtual List<global::ItemAmount> RepairCost(float healthMissingFraction)
	{
		List<global::ItemAmount> list = this.BuildCost();
		if (list == null)
		{
			return null;
		}
		List<global::ItemAmount> list2 = new List<global::ItemAmount>();
		foreach (global::ItemAmount itemAmount in list)
		{
			int num = Mathf.RoundToInt(itemAmount.amount * this.RepairCostFraction() * healthMissingFraction);
			if (num > 0)
			{
				list2.Add(new global::ItemAmount(itemAmount.itemDef, (float)num));
			}
		}
		return list2;
	}

	// Token: 0x06000343 RID: 835 RVA: 0x00012948 File Offset: 0x00010B48
	public virtual void OnRepair()
	{
		global::Effect.server.Run("assets/bundled/prefabs/fx/build/repair.prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
	}

	// Token: 0x06000344 RID: 836 RVA: 0x00012964 File Offset: 0x00010B64
	public virtual void OnRepairFinished()
	{
		global::Effect.server.Run("assets/bundled/prefabs/fx/build/repair_full.prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
	}

	// Token: 0x06000345 RID: 837 RVA: 0x00012980 File Offset: 0x00010B80
	public virtual void OnRepairFailed()
	{
		global::Effect.server.Run("assets/bundled/prefabs/fx/build/repair_failed.prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
	}

	// Token: 0x06000346 RID: 838 RVA: 0x0001299C File Offset: 0x00010B9C
	public virtual void DoRepair(global::BasePlayer player)
	{
		global::BasePlayer player = player2;
		if (!this.repair.enabled)
		{
			return;
		}
		if (Interface.CallHook("OnStructureRepair", new object[]
		{
			this,
			player2
		}) != null)
		{
			return;
		}
		if (this.SecondsSinceAttacked <= 30f)
		{
			this.OnRepairFailed();
			return;
		}
		float num = this.MaxHealth() - this.health;
		float num2 = num / this.MaxHealth();
		if (num <= 0f || num2 <= 0f)
		{
			this.OnRepairFailed();
			return;
		}
		List<global::ItemAmount> list = this.RepairCost(num2);
		if (list == null)
		{
			return;
		}
		float num3 = list.Sum((global::ItemAmount x) => x.amount);
		if (num3 > 0f)
		{
			float num4 = list.Min((global::ItemAmount x) => Mathf.Clamp01((float)player.inventory.GetAmount(x.itemid) / x.amount));
			num4 = Mathf.Min(num4, 50f / num);
			if (num4 <= 0f)
			{
				this.OnRepairFailed();
				return;
			}
			int num5 = 0;
			foreach (global::ItemAmount itemAmount in list)
			{
				int amount = Mathf.CeilToInt(num4 * itemAmount.amount);
				int num6 = player.inventory.Take(null, itemAmount.itemid, amount);
				if (num6 > 0)
				{
					num5 += num6;
					player.Command("note.inv", new object[]
					{
						itemAmount.itemid,
						num6 * -1
					});
				}
			}
			float num7 = (float)num5 / num3;
			this.health += num * num7;
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
		else
		{
			this.health += num;
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
		if (this.health >= this.MaxHealth())
		{
			this.OnRepairFinished();
		}
		else
		{
			this.OnRepair();
		}
	}

	// Token: 0x06000347 RID: 839 RVA: 0x00012BC0 File Offset: 0x00010DC0
	public virtual void InitializeHealth(float newhealth, float newmax)
	{
		this._maxHealth = newmax;
		this._health = newhealth;
		this.lifestate = global::BaseCombatEntity.LifeState.Alive;
	}

	// Token: 0x06000348 RID: 840 RVA: 0x00012BD8 File Offset: 0x00010DD8
	public override void ServerInit()
	{
		this._collider = base.transform.GetComponentInChildrenIncludeDisabled<Collider>();
		this.propDirection = global::PrefabAttribute.server.FindAll<global::DirectionProperties>(this.prefabID);
		if (this.ResetLifeStateOnSpawn)
		{
			this.InitializeHealth(this.StartHealth(), this.StartMaxHealth());
			this.lifestate = global::BaseCombatEntity.LifeState.Alive;
		}
		base.ServerInit();
	}

	// Token: 0x06000349 RID: 841 RVA: 0x00012C38 File Offset: 0x00010E38
	public virtual void Hurt(float amount)
	{
		this.Hurt(Mathf.Abs(amount), Rust.DamageType.Generic, null, true);
	}

	// Token: 0x0600034A RID: 842 RVA: 0x00012C4C File Offset: 0x00010E4C
	public virtual void Hurt(float amount, Rust.DamageType type, global::BaseEntity attacker = null, bool useProtection = true)
	{
		using (TimeWarning.New("Hurt", 0.1f))
		{
			this.Hurt(new global::HitInfo(attacker, this, type, amount, base.transform.position)
			{
				UseProtection = useProtection
			});
		}
	}

	// Token: 0x0600034B RID: 843 RVA: 0x00012CB0 File Offset: 0x00010EB0
	public virtual void Hurt(global::HitInfo info)
	{
		Assert.IsTrue(base.isServer, "This should be called serverside only");
		if (this.IsDead())
		{
			return;
		}
		using (TimeWarning.New("Hurt( HitInfo )", 50L))
		{
			float health = this.health;
			this.ScaleDamage(info);
			if (info.PointStart != Vector3.zero)
			{
				for (int i = 0; i < this.propDirection.Length; i++)
				{
					if (!(this.propDirection[i].extraProtection == null))
					{
						if (!this.propDirection[i].IsWeakspot(base.transform, info))
						{
							this.propDirection[i].extraProtection.Scale(info.damageTypes, 1f);
						}
					}
				}
			}
			info.damageTypes.Scale(Rust.DamageType.Arrow, ConVar.Server.arrowdamage);
			info.damageTypes.Scale(Rust.DamageType.Bullet, ConVar.Server.bulletdamage);
			info.damageTypes.Scale(Rust.DamageType.Slash, ConVar.Server.meleedamage);
			info.damageTypes.Scale(Rust.DamageType.Blunt, ConVar.Server.meleedamage);
			info.damageTypes.Scale(Rust.DamageType.Stab, ConVar.Server.meleedamage);
			info.damageTypes.Scale(Rust.DamageType.Bleeding, ConVar.Server.bleedingdamage);
			if (Interface.CallHook("IOnBaseCombatEntityHurt", new object[]
			{
				this,
				info
			}) != null)
			{
				return;
			}
			this.DebugHurt(info);
			this.health = health - info.damageTypes.Total();
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			if (SingletonComponent<Rust.Ai.AiManager>.Instance != null && SingletonComponent<Rust.Ai.AiManager>.Instance.enabled && SingletonComponent<Rust.Ai.AiManager>.Instance.UseIntensity)
			{
				SingletonComponent<Rust.Ai.AiManager>.Instance.OnHit(info);
			}
			if (ConVar.Global.developer > 1)
			{
				Debug.Log(string.Concat(new object[]
				{
					"[Combat]".PadRight(10),
					base.gameObject.name,
					" hurt ",
					info.damageTypes.GetMajorityDamageType(),
					"/",
					info.damageTypes.Total(),
					" - ",
					this.health.ToString("0"),
					" health left"
				}));
			}
			this.lastDamage = info.damageTypes.GetMajorityDamageType();
			this.lastAttacker = info.Initiator;
			if (this.lastAttacker != null)
			{
				global::BaseCombatEntity baseCombatEntity = this.lastAttacker as global::BaseCombatEntity;
				if (baseCombatEntity != null)
				{
					baseCombatEntity.lastDealtDamageTime = UnityEngine.Time.time;
				}
			}
			global::BaseCombatEntity baseCombatEntity2 = this.lastAttacker as global::BaseCombatEntity;
			if (baseCombatEntity2 != null)
			{
				baseCombatEntity2.MarkHostileTime();
			}
			if (this.lastDamage != Rust.DamageType.Decay)
			{
				this.lastAttackedTime = UnityEngine.Time.time;
				if (this.lastAttacker != null)
				{
					this.LastAttackedDir = (this.lastAttacker.ServerPosition - this.ServerPosition).normalized;
				}
			}
			if (this.health <= 0f)
			{
				this.Die(info);
			}
			global::BasePlayer initiatorPlayer = info.InitiatorPlayer;
			if (initiatorPlayer)
			{
				if (this.IsDead())
				{
					initiatorPlayer.stats.combat.Log(info, health, this.health, "killed");
				}
				else
				{
					initiatorPlayer.stats.combat.Log(info, health, this.health, null);
				}
			}
		}
	}

	// Token: 0x0600034C RID: 844 RVA: 0x0001305C File Offset: 0x0001125C
	public virtual void MarkHostileTime()
	{
		this.lastHostileTime = UnityEngine.Time.realtimeSinceStartup;
		this.lastHostilePos = base.GetEstimatedWorldPosition();
	}

	// Token: 0x0600034D RID: 845 RVA: 0x00013078 File Offset: 0x00011278
	public virtual void ClearHostileTime()
	{
		this.lastHostileTime = float.NegativeInfinity;
		this.lastHostilePos = Vector3.zero;
	}

	// Token: 0x0600034E RID: 846 RVA: 0x00013090 File Offset: 0x00011290
	public float TimeSinceHostile()
	{
		return UnityEngine.Time.realtimeSinceStartup - this.lastHostileTime;
	}

	// Token: 0x0600034F RID: 847 RVA: 0x000130A0 File Offset: 0x000112A0
	public Vector3 GetHostilePos()
	{
		return this.lastHostilePos;
	}

	// Token: 0x06000350 RID: 848 RVA: 0x000130A8 File Offset: 0x000112A8
	private void DebugHurt(global::HitInfo info)
	{
		if (!ConVar.Vis.damage)
		{
			return;
		}
		if (info.PointStart != info.PointEnd)
		{
			global::ConsoleNetwork.BroadcastToAllClients("ddraw.arrow", new object[]
			{
				60,
				Color.cyan,
				info.PointStart,
				info.PointEnd,
				0.1f
			});
			global::ConsoleNetwork.BroadcastToAllClients("ddraw.sphere", new object[]
			{
				60,
				Color.cyan,
				info.HitPositionWorld,
				0.01f
			});
		}
		string text = string.Empty;
		for (int i = 0; i < info.damageTypes.types.Length; i++)
		{
			float num = info.damageTypes.types[i];
			if (num != 0f)
			{
				string text2 = text;
				string[] array = new string[5];
				array[0] = text2;
				array[1] = " ";
				int num2 = 2;
				Rust.DamageType damageType = (Rust.DamageType)i;
				array[num2] = damageType.ToString().PadRight(10);
				array[3] = num.ToString("0.00");
				array[4] = "\n";
				text = string.Concat(array);
			}
		}
		string text3 = string.Concat(new object[]
		{
			"<color=lightblue>Damage:</color>".PadRight(10),
			info.damageTypes.Total().ToString("0.00"),
			"\n<color=lightblue>Health:</color>".PadRight(10),
			this.health.ToString("0.00"),
			" / ",
			(this.health - info.damageTypes.Total() > 0f) ? "<color=green>" : "<color=red>",
			(this.health - info.damageTypes.Total()).ToString("0.00"),
			"</color>",
			"\n<color=lightblue>HitEnt:</color>".PadRight(10),
			this,
			"\n<color=lightblue>HitBone:</color>".PadRight(10),
			info.boneName,
			"\n<color=lightblue>Attacker:</color>".PadRight(10),
			info.Initiator,
			"\n<color=lightblue>WeaponPrefab:</color>".PadRight(10),
			info.WeaponPrefab,
			"\n<color=lightblue>Damages:</color>\n",
			text
		});
		global::ConsoleNetwork.BroadcastToAllClients("ddraw.text", new object[]
		{
			60,
			Color.white,
			info.HitPositionWorld,
			text3
		});
	}

	// Token: 0x06000351 RID: 849 RVA: 0x00013360 File Offset: 0x00011560
	public virtual void ChangeHealth(float amount)
	{
		if (amount == 0f)
		{
			return;
		}
		if (amount > 0f)
		{
			this.Heal(amount);
		}
		else
		{
			this.Hurt(Mathf.Abs(amount));
		}
	}

	// Token: 0x06000352 RID: 850 RVA: 0x00013394 File Offset: 0x00011594
	public virtual void OnHealthChanged(float oldvalue, float newvalue)
	{
	}

	// Token: 0x06000353 RID: 851 RVA: 0x00013398 File Offset: 0x00011598
	public virtual void Heal(float amount)
	{
		if (ConVar.Global.developer > 1)
		{
			Debug.Log("[Combat]".PadRight(10) + base.gameObject.name + " healed");
		}
		this.health = Mathf.Clamp(this.health + amount, 0f, this.MaxHealth());
	}

	// Token: 0x06000354 RID: 852 RVA: 0x000133F4 File Offset: 0x000115F4
	public virtual void OnKilled(global::HitInfo info)
	{
		base.Kill(global::BaseNetworkable.DestroyMode.Gib);
	}

	// Token: 0x06000355 RID: 853 RVA: 0x00013400 File Offset: 0x00011600
	public virtual void Die(global::HitInfo info = null)
	{
		if (this.IsDead())
		{
			return;
		}
		Interface.CallHook("OnEntityDeath", new object[]
		{
			this,
			info
		});
		if (ConVar.Global.developer > 1)
		{
			Debug.Log("[Combat]".PadRight(10) + base.gameObject.name + " died");
		}
		this.health = 0f;
		this.lifestate = global::BaseCombatEntity.LifeState.Dead;
		using (TimeWarning.New("OnKilled", 0.1f))
		{
			this.OnKilled(info);
		}
	}

	// Token: 0x06000356 RID: 854 RVA: 0x000134B0 File Offset: 0x000116B0
	public void DieInstantly()
	{
		if (this.IsDead())
		{
			return;
		}
		if (ConVar.Global.developer > 1)
		{
			Debug.Log("[Combat]".PadRight(10) + base.gameObject.name + " died");
		}
		this.health = 0f;
		this.lifestate = global::BaseCombatEntity.LifeState.Dead;
		this.OnKilled(null);
	}

	// Token: 0x06000357 RID: 855 RVA: 0x00013514 File Offset: 0x00011714
	public void UpdateSurroundings()
	{
		global::StabilityEntity.updateSurroundingsQueue.Add(base.WorldSpaceBounds().ToBounds());
	}

	// Token: 0x04000058 RID: 88
	[Header("BaseCombatEntity")]
	public global::SkeletonProperties skeletonProperties;

	// Token: 0x04000059 RID: 89
	public global::ProtectionProperties baseProtection;

	// Token: 0x0400005A RID: 90
	public float startHealth;

	// Token: 0x0400005B RID: 91
	public global::BaseCombatEntity.Pickup pickup;

	// Token: 0x0400005C RID: 92
	public global::BaseCombatEntity.Repair repair;

	// Token: 0x0400005D RID: 93
	public bool ShowHealthInfo = true;

	// Token: 0x0400005E RID: 94
	public global::BaseCombatEntity.LifeState lifestate;

	// Token: 0x0400005F RID: 95
	public bool sendsHitNotification;

	// Token: 0x04000060 RID: 96
	public bool sendsMeleeHitNotification = true;

	// Token: 0x04000061 RID: 97
	public float _health;

	// Token: 0x04000062 RID: 98
	public float _maxHealth = 100f;

	// Token: 0x04000063 RID: 99
	public float lastAttackedTime = float.NegativeInfinity;

	// Token: 0x04000065 RID: 101
	protected float lastDealtDamageTime = float.NegativeInfinity;

	// Token: 0x04000066 RID: 102
	private int lastNotifyFrame;

	// Token: 0x04000067 RID: 103
	private const float MAX_HEALTH_REPAIR = 50f;

	// Token: 0x04000068 RID: 104
	[NonSerialized]
	public Rust.DamageType lastDamage;

	// Token: 0x04000069 RID: 105
	[NonSerialized]
	public global::BaseEntity lastAttacker;

	// Token: 0x0400006A RID: 106
	[NonSerialized]
	public Collider _collider;

	// Token: 0x0400006B RID: 107
	[NonSerialized]
	public bool ResetLifeStateOnSpawn = true;

	// Token: 0x0400006C RID: 108
	protected global::DirectionProperties[] propDirection;

	// Token: 0x0400006D RID: 109
	public Vector3 lastHostilePos = Vector3.zero;

	// Token: 0x0400006E RID: 110
	public float lastHostileTime = float.NegativeInfinity;

	// Token: 0x02000010 RID: 16
	public enum LifeState
	{
		// Token: 0x04000071 RID: 113
		Alive,
		// Token: 0x04000072 RID: 114
		Dead
	}

	// Token: 0x02000011 RID: 17
	[Serializable]
	public struct Pickup
	{
		// Token: 0x04000073 RID: 115
		public bool enabled;

		// Token: 0x04000074 RID: 116
		[global::ItemSelector(global::ItemCategory.All)]
		public global::ItemDefinition itemTarget;

		// Token: 0x04000075 RID: 117
		public int itemCount;

		// Token: 0x04000076 RID: 118
		[Tooltip("Should we set the condition of the item based on the health of the picked up entity")]
		public bool setConditionFromHealth;

		// Token: 0x04000077 RID: 119
		[Tooltip("How much to reduce the item condition when picking up")]
		public float subtractCondition;

		// Token: 0x04000078 RID: 120
		[Tooltip("Must have building access to pick up")]
		public bool requireBuildingPrivilege;

		// Token: 0x04000079 RID: 121
		[Tooltip("Must have hammer equipped to pick up")]
		public bool requireHammer;

		// Token: 0x0400007A RID: 122
		[Tooltip("Inventory Must be empty (if applicable) to be picked up")]
		public bool requireEmptyInv;
	}

	// Token: 0x02000012 RID: 18
	[Serializable]
	public struct Repair
	{
		// Token: 0x0400007B RID: 123
		public bool enabled;

		// Token: 0x0400007C RID: 124
		[global::ItemSelector(global::ItemCategory.All)]
		public global::ItemDefinition itemTarget;
	}
}
