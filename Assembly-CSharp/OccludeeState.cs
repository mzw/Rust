﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000803 RID: 2051
public class OccludeeState : global::OcclusionCulling.SmartListValue
{
	// Token: 0x170002C1 RID: 705
	// (get) Token: 0x060025EC RID: 9708 RVA: 0x000D1890 File Offset: 0x000CFA90
	public bool isVisible
	{
		get
		{
			return this.states[this.slot].isVisible;
		}
	}

	// Token: 0x060025ED RID: 9709 RVA: 0x000D18B8 File Offset: 0x000CFAB8
	public global::OccludeeState Initialize(global::OcclusionCulling.SimpleList<global::OccludeeState.State> states, global::OcclusionCulling.BufferSet set, int slot, Vector4 sphereBounds, bool isVisible, float minTimeVisible, bool isStatic, global::OcclusionCulling.OnVisibilityChanged onVisibilityChanged)
	{
		states[slot] = new global::OccludeeState.State
		{
			sphereBounds = sphereBounds,
			minTimeVisible = minTimeVisible,
			waitTime = ((!isVisible) ? 0f : (Time.time + minTimeVisible)),
			waitFrame = (uint)(Time.frameCount + 1),
			isVisible = isVisible,
			active = true,
			callback = (onVisibilityChanged != null)
		};
		this.slot = slot;
		this.isStatic = isStatic;
		this.onVisibilityChanged = onVisibilityChanged;
		this.cell = null;
		this.states = states;
		return this;
	}

	// Token: 0x060025EE RID: 9710 RVA: 0x000D195C File Offset: 0x000CFB5C
	public void Invalidate()
	{
		this.states[this.slot] = global::OccludeeState.State.Unused;
		this.slot = -1;
		this.onVisibilityChanged = null;
		this.cell = null;
	}

	// Token: 0x060025EF RID: 9711 RVA: 0x000D198C File Offset: 0x000CFB8C
	public void MakeVisible()
	{
		this.states.array[this.slot].waitTime = Time.time + this.states[this.slot].minTimeVisible;
		this.states.array[this.slot].isVisible = true;
		if (this.onVisibilityChanged != null)
		{
			this.onVisibilityChanged(true);
		}
	}

	// Token: 0x04002227 RID: 8743
	public int slot;

	// Token: 0x04002228 RID: 8744
	public bool isStatic;

	// Token: 0x04002229 RID: 8745
	public global::OcclusionCulling.OnVisibilityChanged onVisibilityChanged;

	// Token: 0x0400222A RID: 8746
	public global::OcclusionCulling.Cell cell;

	// Token: 0x0400222B RID: 8747
	public global::OcclusionCulling.SimpleList<global::OccludeeState.State> states;

	// Token: 0x02000804 RID: 2052
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	public struct State
	{
		// Token: 0x0400222C RID: 8748
		[FieldOffset(0)]
		public Vector4 sphereBounds;

		// Token: 0x0400222D RID: 8749
		[FieldOffset(16)]
		public float minTimeVisible;

		// Token: 0x0400222E RID: 8750
		[FieldOffset(20)]
		public float waitTime;

		// Token: 0x0400222F RID: 8751
		[FieldOffset(24)]
		public uint waitFrame;

		// Token: 0x04002230 RID: 8752
		[FieldOffset(28)]
		public bool isVisible;

		// Token: 0x04002231 RID: 8753
		[FieldOffset(29)]
		public bool active;

		// Token: 0x04002232 RID: 8754
		[FieldOffset(30)]
		public bool callback;

		// Token: 0x04002233 RID: 8755
		[FieldOffset(31)]
		public byte pad1;

		// Token: 0x04002234 RID: 8756
		public static global::OccludeeState.State Unused = new global::OccludeeState.State
		{
			active = false
		};
	}
}
