﻿using System;
using UnityEngine;

// Token: 0x020005E8 RID: 1512
public struct MeshLink
{
	// Token: 0x06001EFE RID: 7934 RVA: 0x000AF728 File Offset: 0x000AD928
	public MeshLink(Renderer renderer, Mesh sharedMesh)
	{
		this.renderer = renderer;
		this.transform = renderer.transform;
		this.sharedMesh = sharedMesh;
	}

	// Token: 0x040019F4 RID: 6644
	public Renderer renderer;

	// Token: 0x040019F5 RID: 6645
	public Transform transform;

	// Token: 0x040019F6 RID: 6646
	public Mesh sharedMesh;
}
