﻿using System;
using Facepunch.Steamworks;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006DF RID: 1759
public class ServerBrowserInfo : SingletonComponent<global::ServerBrowserInfo>
{
	// Token: 0x04001DD8 RID: 7640
	public bool isMain;

	// Token: 0x04001DD9 RID: 7641
	public Text serverName;

	// Token: 0x04001DDA RID: 7642
	public Text serverMeta;

	// Token: 0x04001DDB RID: 7643
	public Text serverText;

	// Token: 0x04001DDC RID: 7644
	public RawImage headerImage;

	// Token: 0x04001DDD RID: 7645
	public Button viewWebpage;

	// Token: 0x04001DDE RID: 7646
	public Button refresh;

	// Token: 0x04001DDF RID: 7647
	public ServerList.Server currentServer;

	// Token: 0x04001DE0 RID: 7648
	public Texture defaultServerImage;
}
