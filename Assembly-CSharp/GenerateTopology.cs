﻿using System;
using UnityEngine;

// Token: 0x020005BE RID: 1470
public class GenerateTopology : global::ProceduralComponent
{
	// Token: 0x06001E7F RID: 7807 RVA: 0x000AAD1C File Offset: 0x000A8F1C
	public override void Process(uint seed)
	{
		int res = global::TerrainMeta.TopologyMap.res;
		int[,] map = global::TerrainMeta.TopologyMap.dst;
		global::TerrainTopologyMap topomap = global::TerrainMeta.TopologyMap;
		global::TerrainBiomeMap biomemap = global::TerrainMeta.BiomeMap;
		global::TerrainHeightMap heightmap = global::TerrainMeta.HeightMap;
		float noiseX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float noiseZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float lootAngle = global::TerrainMeta.LootAxisAngle;
		Parallel.For(0, res, delegate(int z)
		{
			for (int i = 0; i < res; i++)
			{
				float num = topomap.Coordinate(i);
				float num2 = topomap.Coordinate(z);
				float height = heightmap.GetHeight01(num, num2);
				float slope = heightmap.GetSlope(num, num2);
				Vector2 vector;
				vector..ctor(num * 2f - 1f, num2 * 2f - 1f);
				float num3 = (Vector2Ex.Rotate(vector, lootAngle).y + 1f) * 0.5f;
				int num4 = map[z, i];
				if (height < 0.475f)
				{
					num4 |= 65536;
					float num5 = num * 2f - 1f;
					float num6 = num2 * 2f - 1f;
					float num7 = (num5 * num5 + num6 * num6) * 0.75f;
					float num8 = Mathf.Max(Mathf.Abs(num5), Mathf.Abs(num6));
					float num9 = Mathf.Max(num7, num8);
					if (num9 > 0.8f)
					{
						num4 |= 262144;
					}
					if (num9 > 0.95f)
					{
						num4 |= 33554432;
					}
				}
				else if (height < 0.5f)
				{
					num4 |= 65536;
				}
				else if (height < 0.502f)
				{
					num4 |= 16;
				}
				else if (height < 0.6f)
				{
					int biomeMaxIndex = biomemap.GetBiomeMaxIndex(num, num2, -1);
					float num10 = Mathf.InverseLerp(0.506f, 0.51f, height);
					if (biomeMaxIndex == 0)
					{
						float num11 = global::Noise.Ridge((double)((float)i + noiseX), (double)((float)z + noiseZ), 6, 0.0060000000521540642, 1.0, 2.0, 0.5);
						num4 |= ((num11 <= 1.575f) ? 1 : 32);
					}
					else if (biomeMaxIndex == 1)
					{
						float num12 = global::Noise.Ridge((double)((float)i + noiseX), (double)((float)z + noiseZ), 6, 0.0020000000949949026, 1.0, 2.0, 0.5);
						num4 |= ((num10 * num12 <= 1.5f) ? 1 : 32);
					}
					else if (biomeMaxIndex == 2)
					{
						float num13 = global::Noise.Ridge((double)((float)i + noiseX), (double)((float)z + noiseZ), 6, 0.0020000000949949026, 1.0, 2.0, 0.5);
						num4 |= ((num10 * num13 <= 1.525f) ? 1 : 32);
					}
					else
					{
						float num14 = global::Noise.Ridge((double)((float)i + noiseX), (double)((float)z + noiseZ), 6, 0.0020000000949949026, 1.0, 2.0, 0.5);
						num4 |= ((num10 * num14 <= 1.5f) ? 1 : 32);
					}
				}
				if (height >= 0.6f)
				{
					num4 |= 4;
				}
				else if (height >= 0.56f)
				{
					num4 |= 1073741824;
				}
				if (num3 < 0.3f)
				{
					num4 |= 67108864;
				}
				else if (num3 < 0.6f)
				{
					num4 |= 134217728;
				}
				else
				{
					num4 |= 268435456;
				}
				if ((num4 & 197016) != 0)
				{
					if (slope > 35f)
					{
						num4 |= 2;
					}
				}
				else if (slope > 40f)
				{
					num4 |= 2;
				}
				map[z, i] = num4;
			}
		});
		global::ImageProcessing.FloodFill2D(0, 0, map, 65536, 128, (int v) => (v & -65537) | 128);
		global::ImageProcessing.FloodFill2D(res / 2, res / 2, map, 65589, 536870912, (int v) => v | 536870912);
		global::ImageProcessing.Dilate2D(map, 24, 2, delegate(int x, int y)
		{
			if ((map[x, y] & 33) != 0)
			{
				map[x, y] |= 8;
			}
		});
		global::ImageProcessing.Dilate2D(map, 41, 2, delegate(int x, int y)
		{
			if ((map[x, y] & 16) != 0)
			{
				map[x, y] |= 8;
			}
		});
		global::ImageProcessing.Dilate2D(map, 384, 4, delegate(int x, int y)
		{
			if ((map[x, y] & 16) != 0)
			{
				map[x, y] |= 256;
			}
		});
		global::ImageProcessing.Dilate2D(map, 272, 4, delegate(int x, int y)
		{
			if ((map[x, y] & 128) != 0)
			{
				map[x, y] |= 256;
			}
		});
		global::ImageProcessing.Dilate2D(map, 196608, 4, delegate(int x, int y)
		{
			if ((map[x, y] & 16) != 0)
			{
				map[x, y] |= 131072;
			}
		});
		global::ImageProcessing.Dilate2D(map, 131088, 4, delegate(int x, int y)
		{
			if ((map[x, y] & 65536) != 0)
			{
				map[x, y] |= 131072;
			}
		});
		global::ImageProcessing.Dilate2D(map, 96, 4, delegate(int x, int y)
		{
			if ((map[x, y] & 1) != 0)
			{
				map[x, y] |= 64;
			}
		});
		Parallel.For(0, res, delegate(int z)
		{
			for (int i = 0; i < res; i++)
			{
				float normX = topomap.Coordinate(i);
				float normZ = topomap.Coordinate(z);
				float height = heightmap.GetHeight01(normX, normZ);
				if (height > 0.495f && height < 0.5f)
				{
					int num = map[z, i];
					if ((num & 128) != 0)
					{
						num |= 256;
					}
					if ((num & 65536) != 0)
					{
						num |= 131072;
					}
					map[z, i] = num;
				}
			}
		});
	}
}
