﻿using System;
using UnityEngine;

// Token: 0x0200081D RID: 2077
[Serializable]
public class MeshHolder
{
	// Token: 0x06002696 RID: 9878 RVA: 0x000D64DC File Offset: 0x000D46DC
	public void setAnimationData(Mesh mesh)
	{
		this._colors = mesh.colors;
	}

	// Token: 0x04002300 RID: 8960
	[HideInInspector]
	public Vector3[] _vertices;

	// Token: 0x04002301 RID: 8961
	[HideInInspector]
	public Vector3[] _normals;

	// Token: 0x04002302 RID: 8962
	[HideInInspector]
	public int[] _triangles;

	// Token: 0x04002303 RID: 8963
	[HideInInspector]
	public global::trisPerSubmesh[] _TrianglesOfSubs;

	// Token: 0x04002304 RID: 8964
	[HideInInspector]
	public Matrix4x4[] _bindPoses;

	// Token: 0x04002305 RID: 8965
	[HideInInspector]
	public BoneWeight[] _boneWeights;

	// Token: 0x04002306 RID: 8966
	[HideInInspector]
	public Bounds _bounds;

	// Token: 0x04002307 RID: 8967
	[HideInInspector]
	public int _subMeshCount;

	// Token: 0x04002308 RID: 8968
	[HideInInspector]
	public Vector4[] _tangents;

	// Token: 0x04002309 RID: 8969
	[HideInInspector]
	public Vector2[] _uv;

	// Token: 0x0400230A RID: 8970
	[HideInInspector]
	public Vector2[] _uv2;

	// Token: 0x0400230B RID: 8971
	[HideInInspector]
	public Vector2[] _uv3;

	// Token: 0x0400230C RID: 8972
	[HideInInspector]
	public Color[] _colors;

	// Token: 0x0400230D RID: 8973
	[HideInInspector]
	public Vector2[] _uv4;
}
