﻿using System;
using UnityEngine;

// Token: 0x020007B2 RID: 1970
[Serializable]
public struct LayerSelect
{
	// Token: 0x060024A7 RID: 9383 RVA: 0x000C9EB4 File Offset: 0x000C80B4
	public LayerSelect(int layer)
	{
		this.layer = layer;
	}

	// Token: 0x060024A8 RID: 9384 RVA: 0x000C9EC0 File Offset: 0x000C80C0
	public static implicit operator int(global::LayerSelect layer)
	{
		return layer.layer;
	}

	// Token: 0x060024A9 RID: 9385 RVA: 0x000C9ECC File Offset: 0x000C80CC
	public static implicit operator global::LayerSelect(int layer)
	{
		return new global::LayerSelect(layer);
	}

	// Token: 0x17000296 RID: 662
	// (get) Token: 0x060024AA RID: 9386 RVA: 0x000C9ED4 File Offset: 0x000C80D4
	public int Mask
	{
		get
		{
			return 1 << this.layer;
		}
	}

	// Token: 0x17000297 RID: 663
	// (get) Token: 0x060024AB RID: 9387 RVA: 0x000C9EE4 File Offset: 0x000C80E4
	public string Name
	{
		get
		{
			return LayerMask.LayerToName(this.layer);
		}
	}

	// Token: 0x0400201A RID: 8218
	[SerializeField]
	private int layer;
}
