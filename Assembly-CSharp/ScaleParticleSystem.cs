﻿using System;
using UnityEngine;

// Token: 0x0200032E RID: 814
public class ScaleParticleSystem : global::ScaleRenderer
{
	// Token: 0x060013AA RID: 5034 RVA: 0x00073880 File Offset: 0x00071A80
	public override void GatherInitialValues()
	{
		base.GatherInitialValues();
		this.startGravity = this.pSystem.gravityModifier;
		this.startSpeed = this.pSystem.startSpeed;
		this.startSize = this.pSystem.startSize;
		this.startLifeTime = this.pSystem.startLifetime;
	}

	// Token: 0x060013AB RID: 5035 RVA: 0x000738D8 File Offset: 0x00071AD8
	public override void SetScale_Internal(float scale)
	{
		base.SetScale_Internal(scale);
		this.pSystem.startSize = this.startSize * scale;
		this.pSystem.startLifetime = this.startLifeTime * scale;
		this.pSystem.startSpeed = this.startSpeed * scale;
		this.pSystem.gravityModifier = this.startGravity * scale;
	}

	// Token: 0x04000E92 RID: 3730
	public ParticleSystem pSystem;

	// Token: 0x04000E93 RID: 3731
	public bool scaleGravity;

	// Token: 0x04000E94 RID: 3732
	[NonSerialized]
	private float startSize;

	// Token: 0x04000E95 RID: 3733
	[NonSerialized]
	private float startLifeTime;

	// Token: 0x04000E96 RID: 3734
	[NonSerialized]
	private float startSpeed;

	// Token: 0x04000E97 RID: 3735
	[NonSerialized]
	private float startGravity;
}
