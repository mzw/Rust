﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

// Token: 0x0200077F RID: 1919
public class Checksum
{
	// Token: 0x060023AC RID: 9132 RVA: 0x000C5664 File Offset: 0x000C3864
	public void Add(float f, int bytes)
	{
		Union32 union = default(Union32);
		union.f = f;
		if (bytes >= 4)
		{
			this.values.Add(union.b1);
		}
		if (bytes >= 3)
		{
			this.values.Add(union.b2);
		}
		if (bytes >= 2)
		{
			this.values.Add(union.b3);
		}
		if (bytes >= 1)
		{
			this.values.Add(union.b4);
		}
	}

	// Token: 0x060023AD RID: 9133 RVA: 0x000C56E8 File Offset: 0x000C38E8
	public void Add(float f)
	{
		Union32 union = default(Union32);
		union.f = f;
		this.values.Add(union.b1);
		this.values.Add(union.b2);
		this.values.Add(union.b3);
		this.values.Add(union.b4);
	}

	// Token: 0x060023AE RID: 9134 RVA: 0x000C5750 File Offset: 0x000C3950
	public void Add(int i)
	{
		Union32 union = default(Union32);
		union.i = i;
		this.values.Add(union.b1);
		this.values.Add(union.b2);
		this.values.Add(union.b3);
		this.values.Add(union.b4);
	}

	// Token: 0x060023AF RID: 9135 RVA: 0x000C57B8 File Offset: 0x000C39B8
	public void Add(uint u)
	{
		Union32 union = default(Union32);
		union.u = u;
		this.values.Add(union.b1);
		this.values.Add(union.b2);
		this.values.Add(union.b3);
		this.values.Add(union.b4);
	}

	// Token: 0x060023B0 RID: 9136 RVA: 0x000C5820 File Offset: 0x000C3A20
	public void Add(short i)
	{
		Union16 union = default(Union16);
		union.i = i;
		this.values.Add(union.b1);
		this.values.Add(union.b2);
	}

	// Token: 0x060023B1 RID: 9137 RVA: 0x000C5864 File Offset: 0x000C3A64
	public void Add(ushort u)
	{
		Union16 union = default(Union16);
		union.u = u;
		this.values.Add(union.b1);
		this.values.Add(union.b2);
	}

	// Token: 0x060023B2 RID: 9138 RVA: 0x000C58A8 File Offset: 0x000C3AA8
	public void Add(byte b)
	{
		this.values.Add(b);
	}

	// Token: 0x060023B3 RID: 9139 RVA: 0x000C58B8 File Offset: 0x000C3AB8
	public void Clear()
	{
		this.values.Clear();
	}

	// Token: 0x060023B4 RID: 9140 RVA: 0x000C58C8 File Offset: 0x000C3AC8
	public string MD5()
	{
		MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
		byte[] bytes = md5CryptoServiceProvider.ComputeHash(this.values.ToArray());
		return this.BytesToString(bytes);
	}

	// Token: 0x060023B5 RID: 9141 RVA: 0x000C58F4 File Offset: 0x000C3AF4
	public string SHA1()
	{
		SHA1CryptoServiceProvider sha1CryptoServiceProvider = new SHA1CryptoServiceProvider();
		byte[] bytes = sha1CryptoServiceProvider.ComputeHash(this.values.ToArray());
		return this.BytesToString(bytes);
	}

	// Token: 0x060023B6 RID: 9142 RVA: 0x000C5920 File Offset: 0x000C3B20
	public override string ToString()
	{
		return this.BytesToString(this.values.ToArray());
	}

	// Token: 0x060023B7 RID: 9143 RVA: 0x000C5934 File Offset: 0x000C3B34
	private string BytesToString(byte[] bytes)
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < bytes.Length; i++)
		{
			stringBuilder.Append(bytes[i].ToString("x2"));
		}
		return stringBuilder.ToString();
	}

	// Token: 0x04001F9F RID: 8095
	private List<byte> values = new List<byte>();
}
