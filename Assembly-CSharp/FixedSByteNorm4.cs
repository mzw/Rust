﻿using System;
using UnityEngine;

// Token: 0x020007AB RID: 1963
public struct FixedSByteNorm4
{
	// Token: 0x0600248E RID: 9358 RVA: 0x000C9620 File Offset: 0x000C7820
	public FixedSByteNorm4(Vector4 vec)
	{
		this.x = (sbyte)(vec.x * 128f);
		this.y = (sbyte)(vec.y * 128f);
		this.z = (sbyte)(vec.z * 128f);
		this.w = (sbyte)(vec.w * 128f);
	}

	// Token: 0x0600248F RID: 9359 RVA: 0x000C9680 File Offset: 0x000C7880
	public static explicit operator Vector4(global::FixedSByteNorm4 vec)
	{
		return new Vector4((float)vec.x * 0.0078125f, (float)vec.y * 0.0078125f, (float)vec.z * 0.0078125f, (float)vec.w * 0.0078125f);
	}

	// Token: 0x04001FFE RID: 8190
	private const int FracBits = 7;

	// Token: 0x04001FFF RID: 8191
	private const float MaxFrac = 128f;

	// Token: 0x04002000 RID: 8192
	private const float RcpMaxFrac = 0.0078125f;

	// Token: 0x04002001 RID: 8193
	public sbyte x;

	// Token: 0x04002002 RID: 8194
	public sbyte y;

	// Token: 0x04002003 RID: 8195
	public sbyte z;

	// Token: 0x04002004 RID: 8196
	public sbyte w;
}
