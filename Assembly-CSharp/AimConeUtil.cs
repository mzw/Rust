﻿using System;
using UnityEngine;

// Token: 0x020001CA RID: 458
public class AimConeUtil
{
	// Token: 0x06000EEC RID: 3820 RVA: 0x0005C3FC File Offset: 0x0005A5FC
	public static Vector3 GetModifiedAimConeDirection(float aimCone, Vector3 inputVec, bool anywhereInside = true)
	{
		Quaternion quaternion = Quaternion.LookRotation(inputVec);
		Vector2 vector = (!anywhereInside) ? Random.insideUnitCircle.normalized : Random.insideUnitCircle;
		Quaternion quaternion2 = quaternion * Quaternion.Euler(vector.x * aimCone * 0.5f, vector.y * aimCone * 0.5f, 0f);
		return quaternion2 * Vector3.forward;
	}

	// Token: 0x06000EED RID: 3821 RVA: 0x0005C468 File Offset: 0x0005A668
	public static Quaternion GetAimConeQuat(float aimCone)
	{
		Vector3 insideUnitSphere = Random.insideUnitSphere;
		return Quaternion.Euler(insideUnitSphere.x * aimCone * 0.5f, insideUnitSphere.y * aimCone * 0.5f, 0f);
	}
}
