﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200070F RID: 1807
public class ToggleTerrainTrees : MonoBehaviour
{
	// Token: 0x0600225A RID: 8794 RVA: 0x000C091C File Offset: 0x000BEB1C
	protected void OnEnable()
	{
		if (Terrain.activeTerrain)
		{
			this.toggleControl.isOn = Terrain.activeTerrain.drawTreesAndFoliage;
		}
	}

	// Token: 0x0600225B RID: 8795 RVA: 0x000C0944 File Offset: 0x000BEB44
	public void OnToggleChanged()
	{
		if (Terrain.activeTerrain)
		{
			Terrain.activeTerrain.drawTreesAndFoliage = this.toggleControl.isOn;
		}
	}

	// Token: 0x0600225C RID: 8796 RVA: 0x000C096C File Offset: 0x000BEB6C
	protected void OnValidate()
	{
		if (this.textControl)
		{
			this.textControl.text = "Terrain Trees";
		}
	}

	// Token: 0x04001ECE RID: 7886
	public Toggle toggleControl;

	// Token: 0x04001ECF RID: 7887
	public Text textControl;
}
