﻿using System;
using UnityEngine;

// Token: 0x020005CC RID: 1484
public class AddToWaterMap : global::ProceduralObject
{
	// Token: 0x06001EA7 RID: 7847 RVA: 0x000AC75C File Offset: 0x000AA95C
	public override void Process()
	{
		Collider component = base.GetComponent<Collider>();
		Bounds bounds = component.bounds;
		int num = global::TerrainMeta.WaterMap.Index(global::TerrainMeta.NormalizeX(bounds.min.x));
		int num2 = global::TerrainMeta.WaterMap.Index(global::TerrainMeta.NormalizeZ(bounds.max.x));
		int num3 = global::TerrainMeta.WaterMap.Index(global::TerrainMeta.NormalizeX(bounds.min.z));
		int num4 = global::TerrainMeta.WaterMap.Index(global::TerrainMeta.NormalizeZ(bounds.max.z));
		if (component is BoxCollider && base.transform.rotation == Quaternion.identity)
		{
			float num5 = global::TerrainMeta.NormalizeY(bounds.max.y);
			for (int i = num3; i <= num4; i++)
			{
				for (int j = num; j <= num2; j++)
				{
					float height = global::TerrainMeta.WaterMap.GetHeight01(j, i);
					if (num5 > height)
					{
						global::TerrainMeta.WaterMap.SetHeight(j, i, num5);
					}
				}
			}
		}
		else
		{
			for (int k = num3; k <= num4; k++)
			{
				float normZ = global::TerrainMeta.WaterMap.Coordinate(k);
				for (int l = num; l <= num2; l++)
				{
					float normX = global::TerrainMeta.WaterMap.Coordinate(l);
					Vector3 vector;
					vector..ctor(global::TerrainMeta.DenormalizeX(normX), bounds.max.y + 1f, global::TerrainMeta.DenormalizeZ(normZ));
					Ray ray;
					ray..ctor(vector, Vector3.down);
					RaycastHit raycastHit;
					if (component.Raycast(ray, ref raycastHit, bounds.size.y + 1f + 1f))
					{
						float y = raycastHit.point.y;
						float num6 = global::TerrainMeta.NormalizeY(y);
						float height2 = global::TerrainMeta.WaterMap.GetHeight01(l, k);
						if (num6 > height2)
						{
							global::TerrainMeta.WaterMap.SetHeight(l, k, num6);
						}
					}
				}
			}
		}
		global::GameManager.Destroy(this, 0f);
	}
}
