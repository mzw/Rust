﻿using System;
using UnityEngine.Serialization;

// Token: 0x02000417 RID: 1047
public class EffectRecycle : global::BaseMonoBehaviour, IRagdollInhert, global::IEffectRecycle
{
	// Token: 0x04001289 RID: 4745
	[FormerlySerializedAs("lifeTime")]
	[ReadOnly]
	public float detachTime;

	// Token: 0x0400128A RID: 4746
	[ReadOnly]
	[FormerlySerializedAs("lifeTime")]
	public float recycleTime;

	// Token: 0x0400128B RID: 4747
	public global::EffectRecycle.PlayMode playMode;

	// Token: 0x0400128C RID: 4748
	public global::EffectRecycle.ParentDestroyBehaviour onParentDestroyed;

	// Token: 0x02000418 RID: 1048
	public enum PlayMode
	{
		// Token: 0x0400128E RID: 4750
		Once,
		// Token: 0x0400128F RID: 4751
		Looped
	}

	// Token: 0x02000419 RID: 1049
	public enum ParentDestroyBehaviour
	{
		// Token: 0x04001291 RID: 4753
		Detach,
		// Token: 0x04001292 RID: 4754
		Destroy,
		// Token: 0x04001293 RID: 4755
		DetachWaitDestroy
	}
}
