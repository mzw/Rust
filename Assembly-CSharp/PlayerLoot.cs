﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine.Assertions;

// Token: 0x02000086 RID: 134
public class PlayerLoot : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x060008E7 RID: 2279 RVA: 0x0003B4CC File Offset: 0x000396CC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("PlayerLoot.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060008E8 RID: 2280 RVA: 0x0003B514 File Offset: 0x00039714
	public bool IsLooting()
	{
		return this.containers.Count > 0;
	}

	// Token: 0x060008E9 RID: 2281 RVA: 0x0003B524 File Offset: 0x00039724
	public void Clear()
	{
		if (!this.IsLooting())
		{
			return;
		}
		Interface.CallHook("OnPlayerLootEnd", new object[]
		{
			this
		});
		this.MarkDirty();
		if (this.entitySource)
		{
			this.entitySource.SendMessage("PlayerStoppedLooting", base.baseEntity, 1);
		}
		foreach (global::ItemContainer itemContainer in this.containers)
		{
			if (itemContainer != null)
			{
				itemContainer.onDirty -= this.MarkDirty;
			}
		}
		this.containers.Clear();
		this.entitySource = null;
		this.itemSource = null;
	}

	// Token: 0x060008EA RID: 2282 RVA: 0x0003B600 File Offset: 0x00039800
	public global::ItemContainer FindContainer(uint id)
	{
		this.Check();
		if (!this.IsLooting())
		{
			return null;
		}
		foreach (global::ItemContainer itemContainer in this.containers)
		{
			global::ItemContainer itemContainer2 = itemContainer.FindContainer(id);
			if (itemContainer2 != null)
			{
				return itemContainer2;
			}
		}
		return null;
	}

	// Token: 0x060008EB RID: 2283 RVA: 0x0003B680 File Offset: 0x00039880
	public global::Item FindItem(uint id)
	{
		this.Check();
		if (!this.IsLooting())
		{
			return null;
		}
		foreach (global::ItemContainer itemContainer in this.containers)
		{
			global::Item item = itemContainer.FindItemByUID(id);
			if (item != null && item.IsValid())
			{
				return item;
			}
		}
		return null;
	}

	// Token: 0x060008EC RID: 2284 RVA: 0x0003B70C File Offset: 0x0003990C
	public void Check()
	{
		if (!this.IsLooting())
		{
			return;
		}
		if (!base.baseEntity.isServer)
		{
			return;
		}
		if (this.entitySource == null)
		{
			base.baseEntity.ChatMessage("Stopping Looting because lootable doesn't exist!");
			this.Clear();
			return;
		}
		if (!this.entitySource.CanBeLooted(base.baseEntity))
		{
			this.Clear();
			return;
		}
		if (this.PositionChecks && this.entitySource.Distance(base.baseEntity.eyes.position) > 4f)
		{
			this.Clear();
			return;
		}
	}

	// Token: 0x060008ED RID: 2285 RVA: 0x0003B7B4 File Offset: 0x000399B4
	private void MarkDirty()
	{
		if (!this.isInvokingSendUpdate)
		{
			this.isInvokingSendUpdate = true;
			base.Invoke(new Action(this.SendUpdate), 0.1f);
		}
	}

	// Token: 0x060008EE RID: 2286 RVA: 0x0003B7E0 File Offset: 0x000399E0
	public void SendImmediate()
	{
		if (this.isInvokingSendUpdate)
		{
			this.isInvokingSendUpdate = false;
			base.CancelInvoke(new Action(this.SendUpdate));
		}
		this.SendUpdate();
	}

	// Token: 0x060008EF RID: 2287 RVA: 0x0003B80C File Offset: 0x00039A0C
	private void SendUpdate()
	{
		this.isInvokingSendUpdate = false;
		if (!base.baseEntity.IsValid())
		{
			return;
		}
		using (PlayerUpdateLoot playerUpdateLoot = Pool.Get<PlayerUpdateLoot>())
		{
			if (this.entitySource && this.entitySource.net != null)
			{
				playerUpdateLoot.entityID = this.entitySource.net.ID;
			}
			if (this.itemSource != null)
			{
				playerUpdateLoot.itemID = this.itemSource.uid;
			}
			if (this.containers.Count > 0)
			{
				playerUpdateLoot.containers = Pool.Get<List<ProtoBuf.ItemContainer>>();
				foreach (global::ItemContainer itemContainer in this.containers)
				{
					playerUpdateLoot.containers.Add(itemContainer.Save());
				}
			}
			base.baseEntity.ClientRPCPlayer<PlayerUpdateLoot>(null, base.baseEntity, "UpdateLoot", playerUpdateLoot);
		}
	}

	// Token: 0x060008F0 RID: 2288 RVA: 0x0003B934 File Offset: 0x00039B34
	public void StartLootingEntity(global::BaseEntity targetEntity, bool doPositionChecks = true)
	{
		this.Clear();
		if (!targetEntity)
		{
			return;
		}
		if (!targetEntity.OnStartBeingLooted(base.baseEntity))
		{
			return;
		}
		Assert.IsTrue(targetEntity.isServer, "Assure is server");
		this.PositionChecks = doPositionChecks;
		this.entitySource = targetEntity;
		this.itemSource = null;
		this.MarkDirty();
		Interface.CallHook("OnLootEntity", new object[]
		{
			this.GetComponent<global::BasePlayer>(),
			targetEntity
		});
	}

	// Token: 0x060008F1 RID: 2289 RVA: 0x0003B9B0 File Offset: 0x00039BB0
	public void AddContainer(global::ItemContainer container)
	{
		if (container == null)
		{
			return;
		}
		this.containers.Add(container);
		container.onDirty += this.MarkDirty;
	}

	// Token: 0x060008F2 RID: 2290 RVA: 0x0003B9D8 File Offset: 0x00039BD8
	public void StartLootingPlayer(global::BasePlayer player)
	{
		this.Clear();
		if (!player)
		{
			return;
		}
		if (!player.inventory)
		{
			return;
		}
		this.AddContainer(player.inventory.containerWear);
		this.AddContainer(player.inventory.containerMain);
		this.AddContainer(player.inventory.containerBelt);
		this.PositionChecks = true;
		this.entitySource = player;
		this.itemSource = null;
		this.MarkDirty();
		Interface.CallHook("OnLootPlayer", new object[]
		{
			this.GetComponent<global::BasePlayer>(),
			player
		});
	}

	// Token: 0x060008F3 RID: 2291 RVA: 0x0003BA78 File Offset: 0x00039C78
	public void StartLootingItem(global::Item item)
	{
		this.Clear();
		if (item == null)
		{
			return;
		}
		if (item.contents == null)
		{
			return;
		}
		this.PositionChecks = true;
		this.containers.Add(item.contents);
		item.contents.onDirty += this.MarkDirty;
		this.itemSource = item;
		this.entitySource = item.GetWorldEntity();
		this.MarkDirty();
		Interface.CallHook("OnLootItem", new object[]
		{
			this.GetComponent<global::BasePlayer>(),
			item
		});
	}

	// Token: 0x04000418 RID: 1048
	public global::BaseEntity entitySource;

	// Token: 0x04000419 RID: 1049
	public global::Item itemSource;

	// Token: 0x0400041A RID: 1050
	public List<global::ItemContainer> containers = new List<global::ItemContainer>();

	// Token: 0x0400041B RID: 1051
	internal bool PositionChecks = true;

	// Token: 0x0400041C RID: 1052
	private bool isInvokingSendUpdate;
}
