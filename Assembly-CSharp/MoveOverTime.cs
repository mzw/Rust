﻿using System;
using UnityEngine;

// Token: 0x0200050A RID: 1290
public class MoveOverTime : MonoBehaviour
{
	// Token: 0x06001B4E RID: 6990 RVA: 0x000985B8 File Offset: 0x000967B8
	private void Update()
	{
		base.transform.rotation = Quaternion.Euler(base.transform.rotation.eulerAngles + this.rotation * this.speed * Time.deltaTime);
	}

	// Token: 0x04001616 RID: 5654
	[Range(-10f, 10f)]
	public float speed = 1f;

	// Token: 0x04001617 RID: 5655
	public Vector3 position;

	// Token: 0x04001618 RID: 5656
	public Vector3 rotation;

	// Token: 0x04001619 RID: 5657
	public Vector3 scale;
}
