﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001CE RID: 462
[CreateAssetMenu(menuName = "Rust/Ambience Definition List")]
public class AmbienceDefinitionList : ScriptableObject
{
	// Token: 0x040008CE RID: 2254
	public List<global::AmbienceDefinition> defs;
}
