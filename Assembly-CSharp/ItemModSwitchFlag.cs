﻿using System;

// Token: 0x020004F6 RID: 1270
public class ItemModSwitchFlag : global::ItemMod
{
	// Token: 0x06001B08 RID: 6920 RVA: 0x00097580 File Offset: 0x00095780
	public override void DoAction(global::Item item, global::BasePlayer player)
	{
		if (item.amount < 1)
		{
			return;
		}
		if (item.HasFlag(this.flag) == this.state)
		{
			return;
		}
		item.SetFlag(this.flag, this.state);
		item.MarkDirty();
	}

	// Token: 0x040015DA RID: 5594
	public global::Item.Flag flag;

	// Token: 0x040015DB RID: 5595
	public bool state;
}
