﻿using System;
using UnityEngine;

// Token: 0x020004E5 RID: 1253
public class ItemModCookable : global::ItemMod
{
	// Token: 0x06001AD5 RID: 6869 RVA: 0x00096478 File Offset: 0x00094678
	public void OnValidate()
	{
		if (this.amountOfBecome < 1)
		{
			this.amountOfBecome = 1;
		}
		if (this.becomeOnCooked == null)
		{
			Debug.LogWarning("[ItemModCookable] becomeOnCooked is unset! [" + base.name + "]", base.gameObject);
		}
	}

	// Token: 0x06001AD6 RID: 6870 RVA: 0x000964CC File Offset: 0x000946CC
	public override void OnItemCreated(global::Item itemcreated)
	{
		float cooktimeLeft = this.cookTime;
		itemcreated.onCycle += delegate(global::Item item, float delta)
		{
			float temperature = item.temperature;
			if (temperature < (float)this.lowTemp || temperature > (float)this.highTemp || cooktimeLeft < 0f)
			{
				if (this.setCookingFlag && item.HasFlag(global::Item.Flag.Cooking))
				{
					item.SetFlag(global::Item.Flag.Cooking, false);
					item.MarkDirty();
				}
				return;
			}
			if (this.setCookingFlag && !item.HasFlag(global::Item.Flag.Cooking))
			{
				item.SetFlag(global::Item.Flag.Cooking, true);
				item.MarkDirty();
			}
			cooktimeLeft -= delta;
			if (cooktimeLeft > 0f)
			{
				return;
			}
			int position = item.position;
			if (item.amount > 1)
			{
				cooktimeLeft = this.cookTime;
				item.amount--;
				item.MarkDirty();
			}
			else
			{
				item.Remove(0f);
			}
			if (this.becomeOnCooked != null)
			{
				global::Item item2 = global::ItemManager.Create(this.becomeOnCooked, this.amountOfBecome, 0UL);
				if (item2 != null && !item2.MoveToContainer(item.parent, position, true) && !item2.MoveToContainer(item.parent, -1, true))
				{
					item2.Drop(item.parent.dropPosition, item.parent.dropVelocity, default(Quaternion));
					if (item.parent.entityOwner)
					{
						global::BaseOven component = item.parent.entityOwner.GetComponent<global::BaseOven>();
						if (component != null)
						{
							component.OvenFull();
						}
					}
				}
			}
		};
	}

	// Token: 0x0400159C RID: 5532
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition becomeOnCooked;

	// Token: 0x0400159D RID: 5533
	public float cookTime = 30f;

	// Token: 0x0400159E RID: 5534
	public int amountOfBecome = 1;

	// Token: 0x0400159F RID: 5535
	public int lowTemp;

	// Token: 0x040015A0 RID: 5536
	public int highTemp;

	// Token: 0x040015A1 RID: 5537
	public bool setCookingFlag;
}
