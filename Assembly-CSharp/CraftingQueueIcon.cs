﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006A5 RID: 1701
public class CraftingQueueIcon : MonoBehaviour
{
	// Token: 0x04001CC9 RID: 7369
	public CanvasGroup canvasGroup;

	// Token: 0x04001CCA RID: 7370
	public Image icon;

	// Token: 0x04001CCB RID: 7371
	public GameObject timeLeft;

	// Token: 0x04001CCC RID: 7372
	public GameObject craftingCount;
}
