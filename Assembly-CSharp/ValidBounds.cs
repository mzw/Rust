﻿using System;
using UnityEngine;

// Token: 0x0200049F RID: 1183
public class ValidBounds : SingletonComponent<global::ValidBounds>
{
	// Token: 0x060019BE RID: 6590 RVA: 0x00090C00 File Offset: 0x0008EE00
	public static bool Test(Vector3 vPos)
	{
		return !SingletonComponent<global::ValidBounds>.Instance || SingletonComponent<global::ValidBounds>.Instance.IsInside(vPos);
	}

	// Token: 0x060019BF RID: 6591 RVA: 0x00090C20 File Offset: 0x0008EE20
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube(this.worldBounds.center, this.worldBounds.size);
	}

	// Token: 0x060019C0 RID: 6592 RVA: 0x00090C48 File Offset: 0x0008EE48
	internal bool IsInside(Vector3 vPos)
	{
		return !Vector3Ex.IsNaNOrInfinity(vPos) && this.worldBounds.Contains(vPos);
	}

	// Token: 0x04001450 RID: 5200
	public Bounds worldBounds;
}
