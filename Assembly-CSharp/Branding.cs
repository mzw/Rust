﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000669 RID: 1641
public class Branding : global::BaseMonoBehaviour
{
	// Token: 0x04001BFC RID: 7164
	public Text versionText;

	// Token: 0x04001BFD RID: 7165
	public CanvasGroup canvasGroup;

	// Token: 0x04001BFE RID: 7166
	private string oldChangeId;
}
