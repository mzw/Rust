﻿using System;

// Token: 0x02000782 RID: 1922
[Serializable]
public class FloatConditions
{
	// Token: 0x060023BD RID: 9149 RVA: 0x000C5AB8 File Offset: 0x000C3CB8
	public bool AllTrue(float val)
	{
		foreach (global::FloatConditions.Condition condition in this.conditions)
		{
			if (!condition.Test(val))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x04001FA4 RID: 8100
	public global::FloatConditions.Condition[] conditions;

	// Token: 0x02000783 RID: 1923
	[Serializable]
	public struct Condition
	{
		// Token: 0x060023BE RID: 9150 RVA: 0x000C5B00 File Offset: 0x000C3D00
		public bool Test(float val)
		{
			switch (this.type)
			{
			case global::FloatConditions.Condition.Types.Equal:
				return val == this.value;
			case global::FloatConditions.Condition.Types.NotEqual:
				return val != this.value;
			case global::FloatConditions.Condition.Types.Higher:
				return val > this.value;
			case global::FloatConditions.Condition.Types.Lower:
				return val < this.value;
			default:
				return false;
			}
		}

		// Token: 0x04001FA5 RID: 8101
		public global::FloatConditions.Condition.Types type;

		// Token: 0x04001FA6 RID: 8102
		public float value;

		// Token: 0x02000784 RID: 1924
		public enum Types
		{
			// Token: 0x04001FA8 RID: 8104
			Equal,
			// Token: 0x04001FA9 RID: 8105
			NotEqual,
			// Token: 0x04001FAA RID: 8106
			Higher,
			// Token: 0x04001FAB RID: 8107
			Lower
		}
	}
}
