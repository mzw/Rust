﻿using System;

// Token: 0x02000246 RID: 582
public abstract class ComponentInfo<T> : global::ComponentInfo
{
	// Token: 0x0600102F RID: 4143 RVA: 0x00061E68 File Offset: 0x00060068
	public void Initialize(T source)
	{
		this.component = source;
		this.Setup();
	}

	// Token: 0x04000AF3 RID: 2803
	public T component;
}
