﻿using System;
using UnityEngine;

// Token: 0x0200021B RID: 539
public class ModelConditionTest_RoofTop : global::ModelConditionTest
{
	// Token: 0x06000FB8 RID: 4024 RVA: 0x0005FEF8 File Offset: 0x0005E0F8
	protected void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.gray;
		Gizmos.DrawWireCube(new Vector3(0f, 4.5f, -3f), new Vector3(3f, 3f, 3f));
	}

	// Token: 0x06000FB9 RID: 4025 RVA: 0x0005FF4C File Offset: 0x0005E14C
	public override bool DoTest(global::BaseEntity ent)
	{
		global::EntityLink entityLink = ent.FindLink("roof/sockets/wall-female");
		if (entityLink == null)
		{
			return false;
		}
		for (int i = 0; i < entityLink.connections.Count; i++)
		{
			global::EntityLink entityLink2 = entityLink.connections[i];
			if (entityLink2.name == "roof/sockets/wall-male")
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x04000A81 RID: 2689
	private const string socket = "roof/sockets/wall-female";

	// Token: 0x04000A82 RID: 2690
	private const string socket_male = "roof/sockets/wall-male";
}
