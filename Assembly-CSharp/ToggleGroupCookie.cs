﻿using System;
using System.Linq;
using Rust;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Token: 0x02000709 RID: 1801
public class ToggleGroupCookie : MonoBehaviour
{
	// Token: 0x1700026E RID: 622
	// (get) Token: 0x06002246 RID: 8774 RVA: 0x000C0598 File Offset: 0x000BE798
	public ToggleGroup group
	{
		get
		{
			return base.GetComponent<ToggleGroup>();
		}
	}

	// Token: 0x06002247 RID: 8775 RVA: 0x000C05A0 File Offset: 0x000BE7A0
	private void OnEnable()
	{
		string @string = PlayerPrefs.GetString("ToggleGroupCookie_" + base.name);
		if (!string.IsNullOrEmpty(@string))
		{
			Transform transform = base.transform.Find(@string);
			if (transform)
			{
				Toggle component = transform.GetComponent<Toggle>();
				if (component)
				{
					foreach (Toggle toggle in base.GetComponentsInChildren<Toggle>(true))
					{
						toggle.isOn = false;
					}
					component.isOn = false;
					component.isOn = true;
					this.SetupListeners();
					return;
				}
			}
		}
		Toggle toggle2 = this.group.ActiveToggles().FirstOrDefault((Toggle x) => x.isOn);
		if (toggle2)
		{
			toggle2.isOn = false;
			toggle2.isOn = true;
		}
		this.SetupListeners();
	}

	// Token: 0x06002248 RID: 8776 RVA: 0x000C0690 File Offset: 0x000BE890
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		foreach (Toggle toggle in base.GetComponentsInChildren<Toggle>(true))
		{
			toggle.onValueChanged.RemoveListener(new UnityAction<bool>(this.OnToggleChanged));
		}
	}

	// Token: 0x06002249 RID: 8777 RVA: 0x000C06E0 File Offset: 0x000BE8E0
	private void SetupListeners()
	{
		foreach (Toggle toggle in base.GetComponentsInChildren<Toggle>(true))
		{
			toggle.onValueChanged.AddListener(new UnityAction<bool>(this.OnToggleChanged));
		}
	}

	// Token: 0x0600224A RID: 8778 RVA: 0x000C0724 File Offset: 0x000BE924
	private void OnToggleChanged(bool b)
	{
		Toggle toggle = base.GetComponentsInChildren<Toggle>().FirstOrDefault((Toggle x) => x.isOn);
		if (toggle)
		{
			PlayerPrefs.SetString("ToggleGroupCookie_" + base.name, toggle.gameObject.name);
		}
	}
}
