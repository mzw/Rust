﻿using System;
using System.Collections.Generic;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000392 RID: 914
public class ResourceDepositManager : global::BaseEntity
{
	// Token: 0x060015AF RID: 5551 RVA: 0x0007CB50 File Offset: 0x0007AD50
	public ResourceDepositManager()
	{
		global::ResourceDepositManager._manager = this;
		this._deposits = new Dictionary<Vector2i, global::ResourceDepositManager.ResourceDeposit>();
	}

	// Token: 0x060015B0 RID: 5552 RVA: 0x0007CB6C File Offset: 0x0007AD6C
	public static Vector2i GetIndexFrom(Vector3 pos)
	{
		return new Vector2i((int)pos.x / 20, (int)pos.z / 20);
	}

	// Token: 0x060015B1 RID: 5553 RVA: 0x0007CB8C File Offset: 0x0007AD8C
	public static global::ResourceDepositManager Get()
	{
		return global::ResourceDepositManager._manager;
	}

	// Token: 0x060015B2 RID: 5554 RVA: 0x0007CB94 File Offset: 0x0007AD94
	public global::ResourceDepositManager.ResourceDeposit CreateFromPosition(Vector3 pos)
	{
		Vector2i indexFrom = global::ResourceDepositManager.GetIndexFrom(pos);
		Random.State state = Random.state;
		uint num = SeedEx.Seed(new Vector2((float)indexFrom.x, (float)indexFrom.y), global::World.Seed + global::World.Salt);
		Random.InitState((int)num);
		global::ResourceDepositManager.ResourceDeposit resourceDeposit = new global::ResourceDepositManager.ResourceDeposit();
		resourceDeposit.origin = new Vector3((float)(indexFrom.x * 20), 0f, (float)(indexFrom.y * 20));
		float num2 = Random.Range(0f, 1f);
		if (num2 < 0.5f)
		{
			resourceDeposit.Add(global::ItemManager.FindItemDefinition("stones"), 1f, 100, 1f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
		}
		else if (!false)
		{
			resourceDeposit.Add(global::ItemManager.FindItemDefinition("stones"), 1f, Random.Range(30000, 100000), Random.Range(0.3f, 0.5f), global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			float num4;
			if (global::World.Procedural)
			{
				float num3 = (global::TerrainMeta.BiomeMap.GetBiome(pos, 2) <= 0.5f) ? 0f : 1f;
				num4 = num3 * 0.25f;
			}
			else
			{
				num4 = 0.1f;
			}
			if (Random.Range(0f, 1f) >= 1f - num4)
			{
				resourceDeposit.Add(global::ItemManager.FindItemDefinition("metal.ore"), 1f, Random.Range(10000, 100000), Random.Range(2f, 4f), global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			}
			float num6;
			if (global::World.Procedural)
			{
				float num5 = (global::TerrainMeta.BiomeMap.GetBiome(pos, 1) <= 0.5f) ? 0f : 1f;
				num6 = num5 * (0.25f + 0.25f * ((!global::TerrainMeta.TopologyMap.GetTopology(pos, 8)) ? 0f : 1f) + 0.25f * ((!global::TerrainMeta.TopologyMap.GetTopology(pos, 1)) ? 0f : 1f));
			}
			else
			{
				num6 = 0.1f;
			}
			if (Random.Range(0f, 1f) >= 1f - num6)
			{
				resourceDeposit.Add(global::ItemManager.FindItemDefinition("sulfur.ore"), 1f, Random.Range(10000, 100000), Random.Range(4f, 4f), global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			}
			float num7 = 0f;
			if (global::World.Procedural)
			{
				if (global::TerrainMeta.BiomeMap.GetBiome(pos, 8) > 0.5f || global::TerrainMeta.BiomeMap.GetBiome(pos, 4) > 0.5f)
				{
					num7 += 0.25f;
				}
			}
			else
			{
				num7 += 0.15f;
			}
			if (Random.Range(0f, 1f) >= 1f - num7)
			{
				resourceDeposit.Add(global::ItemManager.FindItemDefinition("hq.metal.ore"), 1f, Random.Range(5000, 10000), Random.Range(30f, 50f), global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			}
		}
		this._deposits.Add(indexFrom, resourceDeposit);
		Interface.CallHook("OnResourceDepositCreated", new object[]
		{
			resourceDeposit
		});
		Random.state = state;
		return resourceDeposit;
	}

	// Token: 0x060015B3 RID: 5555 RVA: 0x0007CEE8 File Offset: 0x0007B0E8
	public global::ResourceDepositManager.ResourceDeposit GetFromPosition(Vector3 pos)
	{
		global::ResourceDepositManager.ResourceDeposit result = null;
		if (this._deposits.TryGetValue(global::ResourceDepositManager.GetIndexFrom(pos), out result))
		{
			return result;
		}
		return null;
	}

	// Token: 0x060015B4 RID: 5556 RVA: 0x0007CF14 File Offset: 0x0007B114
	public static global::ResourceDepositManager.ResourceDeposit GetOrCreate(Vector3 pos)
	{
		global::ResourceDepositManager.ResourceDeposit fromPosition = global::ResourceDepositManager.Get().GetFromPosition(pos);
		if (fromPosition != null)
		{
			return fromPosition;
		}
		return global::ResourceDepositManager.Get().CreateFromPosition(pos);
	}

	// Token: 0x04000FFC RID: 4092
	public static global::ResourceDepositManager _manager;

	// Token: 0x04000FFD RID: 4093
	private const int resolution = 20;

	// Token: 0x04000FFE RID: 4094
	public Dictionary<Vector2i, global::ResourceDepositManager.ResourceDeposit> _deposits;

	// Token: 0x02000393 RID: 915
	[Serializable]
	public class ResourceDeposit
	{
		// Token: 0x060015B5 RID: 5557 RVA: 0x0007CF44 File Offset: 0x0007B144
		public ResourceDeposit()
		{
			this._resources = new List<global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry>();
		}

		// Token: 0x060015B6 RID: 5558 RVA: 0x0007CF64 File Offset: 0x0007B164
		public void Add(global::ItemDefinition type, float efficiency, int amount, float workNeeded, global::ResourceDepositManager.ResourceDeposit.surveySpawnType spawnType, bool liquid = false)
		{
			global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry resourceDepositEntry = new global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry();
			resourceDepositEntry.type = type;
			resourceDepositEntry.efficiency = efficiency;
			global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry resourceDepositEntry2 = resourceDepositEntry;
			resourceDepositEntry.amount = amount;
			resourceDepositEntry2.startAmount = amount;
			resourceDepositEntry.spawnType = spawnType;
			resourceDepositEntry.workNeeded = workNeeded;
			resourceDepositEntry.isLiquid = liquid;
			this._resources.Add(resourceDepositEntry);
		}

		// Token: 0x04000FFF RID: 4095
		public float lastSurveyTime = float.NegativeInfinity;

		// Token: 0x04001000 RID: 4096
		public Vector3 origin;

		// Token: 0x04001001 RID: 4097
		public List<global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry> _resources;

		// Token: 0x02000394 RID: 916
		[Serializable]
		public enum surveySpawnType
		{
			// Token: 0x04001003 RID: 4099
			ITEM,
			// Token: 0x04001004 RID: 4100
			OIL,
			// Token: 0x04001005 RID: 4101
			WATER
		}

		// Token: 0x02000395 RID: 917
		[Serializable]
		public class ResourceDepositEntry
		{
			// Token: 0x060015B8 RID: 5560 RVA: 0x0007CFDC File Offset: 0x0007B1DC
			public void Subtract(int subamount)
			{
				if (subamount <= 0)
				{
					return;
				}
				this.amount -= subamount;
				if (this.amount < 0)
				{
					this.amount = 0;
				}
			}

			// Token: 0x04001006 RID: 4102
			public global::ItemDefinition type;

			// Token: 0x04001007 RID: 4103
			public float efficiency = 1f;

			// Token: 0x04001008 RID: 4104
			public int amount;

			// Token: 0x04001009 RID: 4105
			public int startAmount;

			// Token: 0x0400100A RID: 4106
			public float workNeeded = 1f;

			// Token: 0x0400100B RID: 4107
			public float workDone;

			// Token: 0x0400100C RID: 4108
			public global::ResourceDepositManager.ResourceDeposit.surveySpawnType spawnType;

			// Token: 0x0400100D RID: 4109
			public bool isLiquid;
		}
	}
}
