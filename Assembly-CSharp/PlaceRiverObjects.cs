﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

// Token: 0x020005C7 RID: 1479
public class PlaceRiverObjects : global::ProceduralComponent
{
	// Token: 0x06001E9B RID: 7835 RVA: 0x000AC1C8 File Offset: 0x000AA3C8
	public override void Process(uint seed)
	{
		List<global::PathList> rivers = global::TerrainMeta.Path.Rivers;
		foreach (global::PathList pathList in rivers)
		{
			foreach (global::PathList.BasicObject obj in this.Start)
			{
				pathList.TrimStart(obj);
			}
			foreach (global::PathList.BasicObject obj2 in this.End)
			{
				pathList.TrimEnd(obj2);
			}
			foreach (global::PathList.BasicObject obj3 in this.Start)
			{
				pathList.SpawnStart(ref seed, obj3);
			}
			foreach (global::PathList.PathObject obj4 in this.Path)
			{
				pathList.SpawnAlong(ref seed, obj4);
			}
			foreach (global::PathList.SideObject obj5 in this.Side)
			{
				pathList.SpawnSide(ref seed, obj5);
			}
			foreach (global::PathList.BasicObject obj6 in this.End)
			{
				pathList.SpawnEnd(ref seed, obj6);
			}
			pathList.ResetTrims();
		}
	}

	// Token: 0x04001971 RID: 6513
	public global::PathList.BasicObject[] Start;

	// Token: 0x04001972 RID: 6514
	public global::PathList.BasicObject[] End;

	// Token: 0x04001973 RID: 6515
	[FormerlySerializedAs("RiversideObjects")]
	public global::PathList.SideObject[] Side;

	// Token: 0x04001974 RID: 6516
	[FormerlySerializedAs("RiverObjects")]
	public global::PathList.PathObject[] Path;
}
