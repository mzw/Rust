﻿using System;
using UnityEngine;

// Token: 0x02000321 RID: 801
public class EyeBlink : MonoBehaviour
{
	// Token: 0x04000E51 RID: 3665
	public Transform LeftEye;

	// Token: 0x04000E52 RID: 3666
	public Vector3 LeftEyeOffset = new Vector3(0.01f, -0.002f, 0f);

	// Token: 0x04000E53 RID: 3667
	public Transform RightEye;

	// Token: 0x04000E54 RID: 3668
	public Vector3 RightEyeOffset = new Vector3(0.01f, -0.002f, 0f);

	// Token: 0x04000E55 RID: 3669
	public Vector2 TimeWithoutBlinking = new Vector2(1f, 10f);

	// Token: 0x04000E56 RID: 3670
	public float BlinkSpeed = 0.2f;
}
