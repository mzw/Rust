﻿using System;
using System.Collections.Generic;
using ConVar;
using Oxide.Core;
using Rust;
using UnityEngine;

// Token: 0x020003A6 RID: 934
public class PatrolHelicopterAI : global::BaseMonoBehaviour
{
	// Token: 0x06001601 RID: 5633 RVA: 0x0007F054 File Offset: 0x0007D254
	public void Awake()
	{
		if (ConVar.PatrolHelicopter.lifetimeMinutes == 0f)
		{
			base.Invoke(new Action(this.DestroyMe), 1f);
			return;
		}
		base.InvokeRepeating(new Action(this.UpdateWind), 0f, 1f / this.windFrequency);
		this._lastPos = base.transform.position;
		this.spawnTime = UnityEngine.Time.realtimeSinceStartup;
		this.InitializeAI();
	}

	// Token: 0x06001602 RID: 5634 RVA: 0x0007F0D0 File Offset: 0x0007D2D0
	public void SetInitialDestination(Vector3 dest, float mapScaleDistance = 0.25f)
	{
		this.hasInterestZone = true;
		this.interestZoneOrigin = dest;
		float x = global::TerrainMeta.Size.x;
		float y = dest.y + 25f;
		Vector3 vector = Vector3Ex.Range(-1f, 1f);
		vector.y = 0f;
		vector.Normalize();
		vector *= x * mapScaleDistance;
		vector.y = y;
		if (mapScaleDistance == 0f)
		{
			vector = this.interestZoneOrigin + new Vector3(0f, 10f, 0f);
		}
		base.transform.position = vector;
		this.ExitCurrentState();
		this.State_Move_Enter(dest);
	}

	// Token: 0x06001603 RID: 5635 RVA: 0x0007F180 File Offset: 0x0007D380
	public void Retire()
	{
		if (this.isRetiring)
		{
			return;
		}
		this.isRetiring = true;
		base.Invoke(new Action(this.DestroyMe), 240f);
		float x = global::TerrainMeta.Size.x;
		float y = 200f;
		Vector3 vector = Vector3Ex.Range(-1f, 1f);
		vector.y = 0f;
		vector.Normalize();
		vector *= x * 20f;
		vector.y = y;
		this.ExitCurrentState();
		this.State_Move_Enter(vector);
	}

	// Token: 0x06001604 RID: 5636 RVA: 0x0007F214 File Offset: 0x0007D414
	public void SetIdealRotation(Quaternion newTargetRot, float rotationSpeedOverride = -1f)
	{
		float num = (rotationSpeedOverride != -1f) ? rotationSpeedOverride : Mathf.Clamp01(this.moveSpeed / (this.maxSpeed * 0.5f));
		this.rotationSpeed = num * this.maxRotationSpeed;
		this.targetRotation = newTargetRot;
	}

	// Token: 0x06001605 RID: 5637 RVA: 0x0007F260 File Offset: 0x0007D460
	public Quaternion GetYawRotationTo(Vector3 targetDest)
	{
		Vector3 vector = targetDest;
		vector.y = 0f;
		Vector3 position = base.transform.position;
		position.y = 0f;
		Vector3 normalized = (vector - position).normalized;
		return Quaternion.LookRotation(normalized);
	}

	// Token: 0x06001606 RID: 5638 RVA: 0x0007F2AC File Offset: 0x0007D4AC
	public void SetTargetDestination(Vector3 targetDest, float minDist = 5f, float minDistForFacingRotation = 30f)
	{
		this.destination = targetDest;
		this.destination_min_dist = minDist;
		float num = Vector3.Distance(targetDest, base.transform.position);
		if (num > minDistForFacingRotation && !this.IsTargeting())
		{
			this.SetIdealRotation(this.GetYawRotationTo(this.destination), -1f);
		}
		this.targetThrottleSpeed = this.GetThrottleForDistance(num);
	}

	// Token: 0x06001607 RID: 5639 RVA: 0x0007F310 File Offset: 0x0007D510
	public bool AtDestination()
	{
		return Vector3.Distance(base.transform.position, this.destination) < this.destination_min_dist;
	}

	// Token: 0x06001608 RID: 5640 RVA: 0x0007F330 File Offset: 0x0007D530
	public void MoveToDestination()
	{
		Vector3 vector = Vector3.Lerp(this._lastMoveDir, (this.destination - base.transform.position).normalized, UnityEngine.Time.deltaTime / this.courseAdjustLerpTime);
		this._lastMoveDir = vector;
		this.throttleSpeed = Mathf.Lerp(this.throttleSpeed, this.targetThrottleSpeed, UnityEngine.Time.deltaTime / 3f);
		float num = this.throttleSpeed * this.maxSpeed;
		this.TerrainPushback();
		base.transform.position += vector * num * UnityEngine.Time.deltaTime;
		this.windVec = Vector3.Lerp(this.windVec, this.targetWindVec, UnityEngine.Time.deltaTime);
		base.transform.position += this.windVec * this.windForce * UnityEngine.Time.deltaTime;
		this.moveSpeed = Mathf.Lerp(this.moveSpeed, Vector3.Distance(this._lastPos, base.transform.position) / UnityEngine.Time.deltaTime, UnityEngine.Time.deltaTime * 2f);
		this._lastPos = base.transform.position;
	}

	// Token: 0x06001609 RID: 5641 RVA: 0x0007F470 File Offset: 0x0007D670
	public void TerrainPushback()
	{
		if (this._currentState == global::PatrolHelicopterAI.aiState.DEATH)
		{
			return;
		}
		Vector3 vector = base.transform.position + new Vector3(0f, 2f, 0f);
		Vector3 normalized = (this.destination - vector).normalized;
		float num = Vector3.Distance(this.destination, base.transform.position);
		Ray ray;
		ray..ctor(vector, normalized);
		float num2 = 5f;
		float num3 = Mathf.Min(100f, num);
		int mask = LayerMask.GetMask(new string[]
		{
			"Terrain",
			"World",
			"Construction"
		});
		Vector3 vector2 = Vector3.zero;
		RaycastHit raycastHit;
		if (UnityEngine.Physics.SphereCast(ray, num2, ref raycastHit, num3 - num2 * 0.5f, mask))
		{
			float num4 = 1f - raycastHit.distance / num3;
			float num5 = this.terrainPushForce * num4;
			vector2 = Vector3.up * num5;
		}
		Ray ray2;
		ray2..ctor(vector, this._lastMoveDir);
		float num6 = Mathf.Min(10f, num);
		RaycastHit raycastHit2;
		if (UnityEngine.Physics.SphereCast(ray2, num2, ref raycastHit2, num6 - num2 * 0.5f, mask))
		{
			float num7 = 1f - raycastHit2.distance / num6;
			float num8 = this.obstaclePushForce * num7;
			vector2 += this._lastMoveDir * num8 * -1f;
			vector2 += Vector3.up * num8;
		}
		this.pushVec = Vector3.Lerp(this.pushVec, vector2, UnityEngine.Time.deltaTime);
		base.transform.position += this.pushVec * UnityEngine.Time.deltaTime;
	}

	// Token: 0x0600160A RID: 5642 RVA: 0x0007F634 File Offset: 0x0007D834
	public void UpdateRotation()
	{
		if (this.hasAimTarget)
		{
			Vector3 position = base.transform.position;
			position.y = 0f;
			Vector3 aimTarget = this._aimTarget;
			aimTarget.y = 0f;
			Vector3 normalized = (aimTarget - position).normalized;
			Vector3 vector = Vector3.Cross(normalized, Vector3.up);
			float num = Vector3.Angle(normalized, base.transform.right);
			float num2 = Vector3.Angle(normalized, -base.transform.right);
			if (this.aimDoorSide)
			{
				if (num < num2)
				{
					this.targetRotation = Quaternion.LookRotation(vector);
				}
				else
				{
					this.targetRotation = Quaternion.LookRotation(-vector);
				}
			}
			else
			{
				this.targetRotation = Quaternion.LookRotation(normalized);
			}
		}
		this.rotationSpeed = Mathf.Lerp(this.rotationSpeed, this.maxRotationSpeed, UnityEngine.Time.deltaTime / 2f);
		base.transform.rotation = Quaternion.Lerp(base.transform.rotation, this.targetRotation, this.rotationSpeed * UnityEngine.Time.deltaTime);
	}

	// Token: 0x0600160B RID: 5643 RVA: 0x0007F758 File Offset: 0x0007D958
	public void UpdateSpotlight()
	{
		if (this.hasInterestZone)
		{
			this.helicopterBase.spotlightTarget = new Vector3(this.interestZoneOrigin.x, global::TerrainMeta.HeightMap.GetHeight(this.interestZoneOrigin), this.interestZoneOrigin.z);
		}
		else
		{
			this.helicopterBase.spotlightTarget = Vector3.zero;
		}
	}

	// Token: 0x0600160C RID: 5644 RVA: 0x0007F7BC File Offset: 0x0007D9BC
	public void Update()
	{
		if (this.helicopterBase.isClient)
		{
			return;
		}
		global::PatrolHelicopterAI.heliInstance = this;
		this.UpdateTargetList();
		this.MoveToDestination();
		this.UpdateRotation();
		this.UpdateSpotlight();
		this.AIThink();
		this.DoMachineGuns();
		if (!this.isRetiring)
		{
			float num = Mathf.Max(this.spawnTime + ConVar.PatrolHelicopter.lifetimeMinutes * 60f, this.lastDamageTime + 120f);
			if (UnityEngine.Time.realtimeSinceStartup > num)
			{
				this.Retire();
			}
		}
	}

	// Token: 0x0600160D RID: 5645 RVA: 0x0007F844 File Offset: 0x0007DA44
	public void WeakspotDamaged(global::BaseHelicopter.weakspot weak, global::HitInfo info)
	{
		float num = UnityEngine.Time.realtimeSinceStartup - this.lastDamageTime;
		this.lastDamageTime = UnityEngine.Time.realtimeSinceStartup;
		global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
		bool flag = this.ValidStrafeTarget(basePlayer);
		bool flag2 = flag && this.CanStrafe();
		bool flag3 = !flag && this.CanUseNapalm();
		if (num < 5f && basePlayer != null && (flag2 || flag3))
		{
			this.ExitCurrentState();
			this.State_Strafe_Enter(info.Initiator.transform.position, flag3);
		}
	}

	// Token: 0x0600160E RID: 5646 RVA: 0x0007F8E0 File Offset: 0x0007DAE0
	public void CriticalDamage()
	{
		this.isDead = true;
		this.ExitCurrentState();
		this.State_Death_Enter();
	}

	// Token: 0x0600160F RID: 5647 RVA: 0x0007F8F8 File Offset: 0x0007DAF8
	public void DoMachineGuns()
	{
		if (this._targetList.Count > 0)
		{
			if (this.leftGun.NeedsNewTarget())
			{
				this.leftGun.UpdateTargetFromList(this._targetList);
			}
			if (this.rightGun.NeedsNewTarget())
			{
				this.rightGun.UpdateTargetFromList(this._targetList);
			}
		}
		this.leftGun.TurretThink();
		this.rightGun.TurretThink();
	}

	// Token: 0x06001610 RID: 5648 RVA: 0x0007F970 File Offset: 0x0007DB70
	public void FireGun(Vector3 targetPos, float aimCone, bool left)
	{
		if (ConVar.PatrolHelicopter.guns == 0)
		{
			return;
		}
		Transform transform = (!left) ? this.helicopterBase.right_gun_muzzle.transform : this.helicopterBase.left_gun_muzzle.transform;
		Vector3 vector = transform.position;
		Vector3 normalized = (targetPos - vector).normalized;
		vector += normalized * 2f;
		Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(aimCone, normalized, true);
		RaycastHit hit;
		if (global::GamePhysics.Trace(new Ray(vector, modifiedAimConeDirection), 0f, out hit, 300f, 1084435201, 0))
		{
			targetPos = hit.point;
			if (hit.collider)
			{
				global::BaseEntity entity = hit.GetEntity();
				if (entity && entity != this.helicopterBase)
				{
					global::BaseCombatEntity baseCombatEntity = entity as global::BaseCombatEntity;
					global::HitInfo info = new global::HitInfo(this.helicopterBase, entity, Rust.DamageType.Bullet, this.helicopterBase.bulletDamage * ConVar.PatrolHelicopter.bulletDamageScale, hit.point);
					if (baseCombatEntity)
					{
						baseCombatEntity.OnAttacked(info);
						if (baseCombatEntity is global::BasePlayer)
						{
							global::Effect.server.ImpactEffect(new global::HitInfo
							{
								HitPositionWorld = hit.point - modifiedAimConeDirection * 0.25f,
								HitNormalWorld = -modifiedAimConeDirection,
								HitMaterial = global::StringPool.Get("Flesh")
							});
						}
					}
					else
					{
						entity.OnAttacked(info);
					}
				}
			}
		}
		else
		{
			targetPos = vector + modifiedAimConeDirection * 300f;
		}
		this.helicopterBase.ClientRPC<bool, Vector3>(null, "FireGun", left, targetPos);
	}

	// Token: 0x06001611 RID: 5649 RVA: 0x0007FB28 File Offset: 0x0007DD28
	public bool CanInterruptState()
	{
		return this._currentState != global::PatrolHelicopterAI.aiState.STRAFE && this._currentState != global::PatrolHelicopterAI.aiState.DEATH;
	}

	// Token: 0x06001612 RID: 5650 RVA: 0x0007FB48 File Offset: 0x0007DD48
	public bool IsAlive()
	{
		return !this.isDead;
	}

	// Token: 0x06001613 RID: 5651 RVA: 0x0007FB54 File Offset: 0x0007DD54
	public void DestroyMe()
	{
		this.helicopterBase.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06001614 RID: 5652 RVA: 0x0007FB64 File Offset: 0x0007DD64
	public Vector3 GetLastMoveDir()
	{
		return this._lastMoveDir;
	}

	// Token: 0x06001615 RID: 5653 RVA: 0x0007FB6C File Offset: 0x0007DD6C
	public Vector3 GetMoveDirection()
	{
		return (this.destination - base.transform.position).normalized;
	}

	// Token: 0x06001616 RID: 5654 RVA: 0x0007FB98 File Offset: 0x0007DD98
	public float GetMoveSpeed()
	{
		return this.moveSpeed;
	}

	// Token: 0x06001617 RID: 5655 RVA: 0x0007FBA0 File Offset: 0x0007DDA0
	public float GetMaxRotationSpeed()
	{
		return this.maxRotationSpeed;
	}

	// Token: 0x06001618 RID: 5656 RVA: 0x0007FBA8 File Offset: 0x0007DDA8
	public bool IsTargeting()
	{
		return this.hasAimTarget;
	}

	// Token: 0x06001619 RID: 5657 RVA: 0x0007FBB0 File Offset: 0x0007DDB0
	public void UpdateWind()
	{
		this.targetWindVec = Random.onUnitSphere;
	}

	// Token: 0x0600161A RID: 5658 RVA: 0x0007FBC0 File Offset: 0x0007DDC0
	public void SetAimTarget(Vector3 aimTarg, bool isDoorSide)
	{
		if (this.movementLockingAiming)
		{
			return;
		}
		this.hasAimTarget = true;
		this._aimTarget = aimTarg;
		this.aimDoorSide = isDoorSide;
	}

	// Token: 0x0600161B RID: 5659 RVA: 0x0007FBE4 File Offset: 0x0007DDE4
	public void ClearAimTarget()
	{
		this.hasAimTarget = false;
		this._aimTarget = Vector3.zero;
	}

	// Token: 0x0600161C RID: 5660 RVA: 0x0007FBF8 File Offset: 0x0007DDF8
	public void UpdateTargetList()
	{
		Vector3 strafePos = Vector3.zero;
		bool flag = false;
		bool shouldUseNapalm = false;
		for (int i = this._targetList.Count - 1; i >= 0; i--)
		{
			global::PatrolHelicopterAI.targetinfo targetinfo = this._targetList[i];
			if (targetinfo == null || targetinfo.ent == null)
			{
				this._targetList.Remove(targetinfo);
			}
			else
			{
				if (UnityEngine.Time.realtimeSinceStartup > targetinfo.nextLOSCheck)
				{
					targetinfo.nextLOSCheck = UnityEngine.Time.realtimeSinceStartup + 1f;
					if (this.PlayerVisible(targetinfo.ply))
					{
						targetinfo.lastSeenTime = UnityEngine.Time.realtimeSinceStartup;
						targetinfo.visibleFor += 1f;
					}
					else
					{
						targetinfo.visibleFor = 0f;
					}
				}
				bool flag2 = (!targetinfo.ply) ? (targetinfo.ent.Health() <= 0f) : targetinfo.ply.IsDead();
				if (targetinfo.TimeSinceSeen() >= 6f || flag2)
				{
					bool flag3 = Random.Range(0f, 1f) >= 0f;
					if ((this.CanStrafe() || this.CanUseNapalm()) && this.IsAlive() && !flag && !flag2 && (targetinfo.ply == this.leftGun._target || targetinfo.ply == this.rightGun._target) && flag3)
					{
						shouldUseNapalm = (!this.ValidStrafeTarget(targetinfo.ply) || Random.Range(0f, 1f) > 0.75f);
						flag = true;
						strafePos = targetinfo.ply.transform.position;
					}
					this._targetList.Remove(targetinfo);
				}
			}
		}
		foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
		{
			if (Vector3Ex.Distance2D(base.transform.position, basePlayer.transform.position) <= 150f)
			{
				bool flag4 = false;
				foreach (global::PatrolHelicopterAI.targetinfo targetinfo2 in this._targetList)
				{
					if (targetinfo2.ply == basePlayer)
					{
						flag4 = true;
						break;
					}
				}
				if (!flag4 && basePlayer.GetThreatLevel() > 0.5f && this.PlayerVisible(basePlayer))
				{
					this._targetList.Add(new global::PatrolHelicopterAI.targetinfo(basePlayer, basePlayer));
				}
			}
		}
		if (flag)
		{
			this.ExitCurrentState();
			this.State_Strafe_Enter(strafePos, shouldUseNapalm);
		}
	}

	// Token: 0x0600161D RID: 5661 RVA: 0x0007FF1C File Offset: 0x0007E11C
	public bool PlayerVisible(global::BasePlayer ply)
	{
		object obj = Interface.CallHook("CanHelicopterTarget", new object[]
		{
			this,
			ply
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		Vector3 position = ply.eyes.position;
		if (TOD_Sky.Instance.IsNight && Vector3.Distance(position, this.interestZoneOrigin) > 40f)
		{
			return false;
		}
		Vector3 vector = base.transform.position - Vector3.up * 6f;
		float num = Vector3.Distance(position, vector);
		Vector3 normalized = (position - vector).normalized;
		RaycastHit raycastHit;
		return global::GamePhysics.Trace(new Ray(vector + normalized * 5f, normalized), 0f, out raycastHit, num * 1.1f, 1084434689, 0) && raycastHit.collider.gameObject.ToBaseEntity() == ply;
	}

	// Token: 0x0600161E RID: 5662 RVA: 0x00080020 File Offset: 0x0007E220
	public void WasAttacked(global::HitInfo info)
	{
		global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
		if (basePlayer != null)
		{
			this._targetList.Add(new global::PatrolHelicopterAI.targetinfo(basePlayer, basePlayer));
		}
	}

	// Token: 0x0600161F RID: 5663 RVA: 0x00080058 File Offset: 0x0007E258
	public void State_Death_Think(float timePassed)
	{
		float num = UnityEngine.Time.realtimeSinceStartup * 0.25f;
		float num2 = Mathf.Sin(6.28318548f * num) * 10f;
		float num3 = Mathf.Cos(6.28318548f * num) * 10f;
		Vector3 vector;
		vector..ctor(num2, 0f, num3);
		this.SetAimTarget(base.transform.position + vector, true);
		Ray ray;
		ray..ctor(base.transform.position, this.GetLastMoveDir());
		int mask = LayerMask.GetMask(new string[]
		{
			"Terrain",
			"World",
			"Construction",
			"Water"
		});
		RaycastHit raycastHit;
		if (UnityEngine.Physics.SphereCast(ray, 3f, ref raycastHit, 5f, mask) || UnityEngine.Time.realtimeSinceStartup > this.deathTimeout)
		{
			this.helicopterBase.Hurt(this.helicopterBase.health * 2f, Rust.DamageType.Generic, null, false);
		}
	}

	// Token: 0x06001620 RID: 5664 RVA: 0x00080150 File Offset: 0x0007E350
	public void State_Death_Enter()
	{
		this.maxRotationSpeed *= 8f;
		this._currentState = global::PatrolHelicopterAI.aiState.DEATH;
		Vector3 randomOffset = this.GetRandomOffset(base.transform.position, 20f, 60f, 20f, 30f);
		int num = 1101212417;
		Vector3 targetDest;
		Vector3 vector;
		global::TransformUtil.GetGroundInfo(randomOffset - Vector3.up * 2f, out targetDest, out vector, 500f, num, null);
		this.SetTargetDestination(targetDest, 5f, 30f);
		this.targetThrottleSpeed = 0.5f;
		this.deathTimeout = UnityEngine.Time.realtimeSinceStartup + 10f;
	}

	// Token: 0x06001621 RID: 5665 RVA: 0x000801FC File Offset: 0x0007E3FC
	public void State_Death_Leave()
	{
	}

	// Token: 0x06001622 RID: 5666 RVA: 0x00080200 File Offset: 0x0007E400
	public void State_Idle_Think(float timePassed)
	{
		this.ExitCurrentState();
		this.State_Patrol_Enter();
	}

	// Token: 0x06001623 RID: 5667 RVA: 0x00080210 File Offset: 0x0007E410
	public void State_Idle_Enter()
	{
		this._currentState = global::PatrolHelicopterAI.aiState.IDLE;
	}

	// Token: 0x06001624 RID: 5668 RVA: 0x0008021C File Offset: 0x0007E41C
	public void State_Idle_Leave()
	{
	}

	// Token: 0x06001625 RID: 5669 RVA: 0x00080220 File Offset: 0x0007E420
	public void State_Move_Think(float timePassed)
	{
		float distToTarget = Vector3.Distance(base.transform.position, this.destination);
		this.targetThrottleSpeed = this.GetThrottleForDistance(distToTarget);
		if (this.AtDestination())
		{
			this.ExitCurrentState();
			this.State_Idle_Enter();
		}
	}

	// Token: 0x06001626 RID: 5670 RVA: 0x00080268 File Offset: 0x0007E468
	public void State_Move_Enter(Vector3 newPos)
	{
		this._currentState = global::PatrolHelicopterAI.aiState.MOVE;
		this.destination_min_dist = 5f;
		this.SetTargetDestination(newPos, 5f, 30f);
		float distToTarget = Vector3.Distance(base.transform.position, this.destination);
		this.targetThrottleSpeed = this.GetThrottleForDistance(distToTarget);
	}

	// Token: 0x06001627 RID: 5671 RVA: 0x000802BC File Offset: 0x0007E4BC
	public void State_Move_Leave()
	{
	}

	// Token: 0x06001628 RID: 5672 RVA: 0x000802C0 File Offset: 0x0007E4C0
	public void State_Orbit_Think(float timePassed)
	{
		if (this.breakingOrbit)
		{
			if (this.AtDestination())
			{
				this.ExitCurrentState();
				this.State_Idle_Enter();
			}
		}
		else
		{
			if (Vector3Ex.Distance2D(base.transform.position, this.destination) > 15f)
			{
				return;
			}
			if (!this.hasEnteredOrbit)
			{
				this.hasEnteredOrbit = true;
				this.orbitStartTime = UnityEngine.Time.realtimeSinceStartup;
			}
			float num = 6.28318548f * this.currentOrbitDistance;
			float num2 = 0.5f * this.maxSpeed;
			float num3 = num / num2;
			this.currentOrbitTime += timePassed / (num3 * 1.01f);
			float rate = this.currentOrbitTime;
			Vector3 orbitPosition = this.GetOrbitPosition(rate);
			this.ClearAimTarget();
			this.SetTargetDestination(orbitPosition, 0f, 1f);
			this.targetThrottleSpeed = 0.5f;
		}
		if (UnityEngine.Time.realtimeSinceStartup - this.orbitStartTime > this.maxOrbitDuration && !this.breakingOrbit)
		{
			this.breakingOrbit = true;
			Vector3 appropriatePosition = this.GetAppropriatePosition(base.transform.position + base.transform.forward * 75f, 40f, 50f);
			this.SetTargetDestination(appropriatePosition, 10f, 0f);
		}
	}

	// Token: 0x06001629 RID: 5673 RVA: 0x0008040C File Offset: 0x0007E60C
	public Vector3 GetOrbitPosition(float rate)
	{
		float num = Mathf.Sin(6.28318548f * rate) * this.currentOrbitDistance;
		float num2 = Mathf.Cos(6.28318548f * rate) * this.currentOrbitDistance;
		Vector3 vector;
		vector..ctor(num, 20f, num2);
		vector = this.interestZoneOrigin + vector;
		return vector;
	}

	// Token: 0x0600162A RID: 5674 RVA: 0x00080460 File Offset: 0x0007E660
	public void State_Orbit_Enter(float orbitDistance)
	{
		this._currentState = global::PatrolHelicopterAI.aiState.ORBIT;
		this.breakingOrbit = false;
		this.hasEnteredOrbit = false;
		this.orbitStartTime = UnityEngine.Time.realtimeSinceStartup;
		Vector3 vector = base.transform.position - this.interestZoneOrigin;
		this.currentOrbitTime = Mathf.Atan2(vector.x, vector.z);
		this.currentOrbitDistance = orbitDistance;
		this.ClearAimTarget();
		this.SetTargetDestination(this.GetOrbitPosition(this.currentOrbitTime), 20f, 0f);
	}

	// Token: 0x0600162B RID: 5675 RVA: 0x000804E8 File Offset: 0x0007E6E8
	public void State_Orbit_Leave()
	{
		this.breakingOrbit = false;
		this.hasEnteredOrbit = false;
		this.currentOrbitTime = 0f;
		this.ClearAimTarget();
	}

	// Token: 0x0600162C RID: 5676 RVA: 0x0008050C File Offset: 0x0007E70C
	public Vector3 GetRandomPatrolDestination()
	{
		Vector3 vector = Vector3.zero;
		if (global::TerrainMeta.Path != null && global::TerrainMeta.Path.Monuments != null && global::TerrainMeta.Path.Monuments.Count > 0)
		{
			global::MonumentInfo monumentInfo = null;
			if (this._visitedMonuments.Count > 0)
			{
				foreach (global::MonumentInfo monumentInfo2 in global::TerrainMeta.Path.Monuments)
				{
					bool flag = false;
					foreach (global::MonumentInfo monumentInfo3 in this._visitedMonuments)
					{
						if (monumentInfo2 == monumentInfo3)
						{
							flag = true;
						}
					}
					if (!flag)
					{
						monumentInfo = monumentInfo2;
						break;
					}
				}
			}
			if (monumentInfo == null)
			{
				this._visitedMonuments.Clear();
				monumentInfo = global::TerrainMeta.Path.Monuments[Random.Range(0, global::TerrainMeta.Path.Monuments.Count)];
			}
			if (monumentInfo)
			{
				vector = monumentInfo.transform.position;
				this._visitedMonuments.Add(monumentInfo);
				vector.y = global::TerrainMeta.HeightMap.GetHeight(vector) + 200f;
				RaycastHit raycastHit;
				if (global::TransformUtil.GetGroundInfo(vector, out raycastHit, 300f, 1101070337, null))
				{
					vector.y = raycastHit.point.y;
				}
				vector.y += 30f;
			}
		}
		else
		{
			float x = global::TerrainMeta.Size.x;
			float y = 30f;
			vector = Vector3Ex.Range(-1f, 1f);
			vector.y = 0f;
			vector.Normalize();
			vector *= x * Random.Range(0f, 0.75f);
			vector.y = y;
		}
		return vector;
	}

	// Token: 0x0600162D RID: 5677 RVA: 0x0008073C File Offset: 0x0007E93C
	public void State_Patrol_Think(float timePassed)
	{
		float num = Vector3.Distance(base.transform.position, this.destination);
		if (num <= 25f)
		{
			this.targetThrottleSpeed = this.GetThrottleForDistance(num);
		}
		else
		{
			this.targetThrottleSpeed = 0.5f;
		}
		if (this.AtDestination() && this.arrivalTime == 0f)
		{
			this.arrivalTime = UnityEngine.Time.realtimeSinceStartup;
			this.ExitCurrentState();
			this.maxOrbitDuration = 20f;
			this.State_Orbit_Enter(75f);
		}
		if (this._targetList.Count > 0)
		{
			this.interestZoneOrigin = this._targetList[0].ply.transform.position + new Vector3(0f, 20f, 0f);
			this.ExitCurrentState();
			this.maxOrbitDuration = 10f;
			this.State_Orbit_Enter(75f);
		}
	}

	// Token: 0x0600162E RID: 5678 RVA: 0x00080834 File Offset: 0x0007EA34
	public void State_Patrol_Enter()
	{
		this._currentState = global::PatrolHelicopterAI.aiState.PATROL;
		Vector3 randomPatrolDestination = this.GetRandomPatrolDestination();
		this.SetTargetDestination(randomPatrolDestination, 10f, 30f);
		this.interestZoneOrigin = randomPatrolDestination;
		this.arrivalTime = 0f;
	}

	// Token: 0x0600162F RID: 5679 RVA: 0x00080874 File Offset: 0x0007EA74
	public void State_Patrol_Leave()
	{
	}

	// Token: 0x06001630 RID: 5680 RVA: 0x00080878 File Offset: 0x0007EA78
	public int ClipRocketsLeft()
	{
		return this.numRocketsLeft;
	}

	// Token: 0x06001631 RID: 5681 RVA: 0x00080880 File Offset: 0x0007EA80
	public bool CanStrafe()
	{
		object obj = Interface.CallHook("CanHelicopterStrafe", new object[]
		{
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return UnityEngine.Time.realtimeSinceStartup - this.lastStrafeTime >= 20f && this.CanInterruptState();
	}

	// Token: 0x06001632 RID: 5682 RVA: 0x000808D4 File Offset: 0x0007EAD4
	public bool CanUseNapalm()
	{
		object obj = Interface.CallHook("CanHelicopterUseNapalm", new object[]
		{
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return UnityEngine.Time.realtimeSinceStartup - this.lastNapalmTime >= 30f;
	}

	// Token: 0x06001633 RID: 5683 RVA: 0x00080920 File Offset: 0x0007EB20
	public void State_Strafe_Enter(Vector3 strafePos, bool shouldUseNapalm = false)
	{
		if (this.CanUseNapalm() && shouldUseNapalm)
		{
			this.useNapalm = shouldUseNapalm;
			this.lastNapalmTime = UnityEngine.Time.realtimeSinceStartup;
		}
		this.lastStrafeTime = UnityEngine.Time.realtimeSinceStartup;
		this._currentState = global::PatrolHelicopterAI.aiState.STRAFE;
		int mask = LayerMask.GetMask(new string[]
		{
			"Terrain",
			"World",
			"Construction",
			"Water"
		});
		Vector3 vector;
		Vector3 vector2;
		if (global::TransformUtil.GetGroundInfo(strafePos, out vector, out vector2, 100f, mask, base.transform))
		{
			this.strafe_target_position = vector;
		}
		else
		{
			this.strafe_target_position = strafePos;
		}
		this.numRocketsLeft = 12;
		this.lastRocketTime = 0f;
		this.movementLockingAiming = true;
		Vector3 randomOffset = this.GetRandomOffset(strafePos, 175f, 192.5f, 20f, 30f);
		this.SetTargetDestination(randomOffset, 10f, 30f);
		this.SetIdealRotation(this.GetYawRotationTo(randomOffset), -1f);
		this.puttingDistance = true;
	}

	// Token: 0x06001634 RID: 5684 RVA: 0x00080A24 File Offset: 0x0007EC24
	public void State_Strafe_Think(float timePassed)
	{
		if (this.puttingDistance)
		{
			if (this.AtDestination())
			{
				this.puttingDistance = false;
				this.SetTargetDestination(this.strafe_target_position + new Vector3(0f, 40f, 0f), 10f, 30f);
				this.SetIdealRotation(this.GetYawRotationTo(this.strafe_target_position), -1f);
			}
		}
		else
		{
			this.SetIdealRotation(this.GetYawRotationTo(this.strafe_target_position), -1f);
			float num = Vector3Ex.Distance2D(this.strafe_target_position, base.transform.position);
			if (num <= 150f && this.ClipRocketsLeft() > 0 && UnityEngine.Time.realtimeSinceStartup - this.lastRocketTime > this.timeBetweenRockets)
			{
				float num2 = Vector3.Distance(this.strafe_target_position, base.transform.position) - 10f;
				if (num2 < 0f)
				{
					num2 = 0f;
				}
				bool flag = !UnityEngine.Physics.Raycast(base.transform.position, (this.strafe_target_position - base.transform.position).normalized, num2, LayerMask.GetMask(new string[]
				{
					"Terrain",
					"World"
				}));
				if (flag)
				{
					this.FireRocket();
				}
			}
			if (this.ClipRocketsLeft() <= 0 || num <= 15f)
			{
				this.ExitCurrentState();
				this.State_Move_Enter(this.GetAppropriatePosition(this.strafe_target_position + base.transform.forward * 120f, 20f, 30f));
			}
		}
	}

	// Token: 0x06001635 RID: 5685 RVA: 0x00080BD0 File Offset: 0x0007EDD0
	public bool ValidStrafeTarget(global::BasePlayer ply)
	{
		object obj = Interface.CallHook("CanHelicopterStrafeTarget", new object[]
		{
			this,
			ply
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return !ply.IsNearEnemyBase();
	}

	// Token: 0x06001636 RID: 5686 RVA: 0x00080C14 File Offset: 0x0007EE14
	public void State_Strafe_Leave()
	{
		this.lastStrafeTime = UnityEngine.Time.realtimeSinceStartup;
		if (this.useNapalm)
		{
			this.lastNapalmTime = UnityEngine.Time.realtimeSinceStartup;
		}
		this.useNapalm = false;
		this.movementLockingAiming = false;
	}

	// Token: 0x06001637 RID: 5687 RVA: 0x00080C48 File Offset: 0x0007EE48
	public void FireRocket()
	{
		this.numRocketsLeft--;
		this.lastRocketTime = UnityEngine.Time.realtimeSinceStartup;
		float num = 4f;
		bool flag = this.leftTubeFiredLast;
		this.leftTubeFiredLast = !this.leftTubeFiredLast;
		Transform transform = (!flag) ? this.helicopterBase.rocket_tube_right.transform : this.helicopterBase.rocket_tube_left.transform;
		Vector3 vector = transform.position + transform.forward * 1f;
		Vector3 vector2 = (this.strafe_target_position - vector).normalized;
		if (num > 0f)
		{
			vector2 = global::AimConeUtil.GetModifiedAimConeDirection(num, vector2, true);
		}
		float num2 = 1f;
		RaycastHit raycastHit;
		if (UnityEngine.Physics.Raycast(vector, vector2, ref raycastHit, num2, 1101212417))
		{
			num2 = raycastHit.distance - 0.1f;
		}
		global::Effect.server.Run(this.helicopterBase.rocket_fire_effect.resourcePath, this.helicopterBase, global::StringPool.Get((!flag) ? "rocket_tube_right" : "rocket_tube_left"), Vector3.zero, Vector3.forward, null, true);
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity((!this.useNapalm) ? this.rocketProjectile.resourcePath : this.rocketProjectile_Napalm.resourcePath, vector, default(Quaternion), true);
		if (baseEntity == null)
		{
			return;
		}
		baseEntity.SendMessage("InitializeVelocity", vector2 * 1f);
		baseEntity.Spawn();
	}

	// Token: 0x06001638 RID: 5688 RVA: 0x00080DDC File Offset: 0x0007EFDC
	public void InitializeAI()
	{
		this._lastThinkTime = UnityEngine.Time.realtimeSinceStartup;
	}

	// Token: 0x06001639 RID: 5689 RVA: 0x00080DEC File Offset: 0x0007EFEC
	public void OnCurrentStateExit()
	{
		switch (this._currentState)
		{
		default:
			this.State_Idle_Leave();
			break;
		case global::PatrolHelicopterAI.aiState.MOVE:
			this.State_Move_Leave();
			break;
		case global::PatrolHelicopterAI.aiState.ORBIT:
			this.State_Orbit_Leave();
			break;
		case global::PatrolHelicopterAI.aiState.STRAFE:
			this.State_Strafe_Leave();
			break;
		case global::PatrolHelicopterAI.aiState.PATROL:
			this.State_Patrol_Leave();
			break;
		}
	}

	// Token: 0x0600163A RID: 5690 RVA: 0x00080E58 File Offset: 0x0007F058
	public void ExitCurrentState()
	{
		this.OnCurrentStateExit();
		this._currentState = global::PatrolHelicopterAI.aiState.IDLE;
	}

	// Token: 0x0600163B RID: 5691 RVA: 0x00080E68 File Offset: 0x0007F068
	public float GetTime()
	{
		return UnityEngine.Time.realtimeSinceStartup;
	}

	// Token: 0x0600163C RID: 5692 RVA: 0x00080E70 File Offset: 0x0007F070
	public void AIThink()
	{
		float time = this.GetTime();
		float timePassed = time - this._lastThinkTime;
		this._lastThinkTime = time;
		switch (this._currentState)
		{
		default:
			this.State_Idle_Think(timePassed);
			break;
		case global::PatrolHelicopterAI.aiState.MOVE:
			this.State_Move_Think(timePassed);
			break;
		case global::PatrolHelicopterAI.aiState.ORBIT:
			this.State_Orbit_Think(timePassed);
			break;
		case global::PatrolHelicopterAI.aiState.STRAFE:
			this.State_Strafe_Think(timePassed);
			break;
		case global::PatrolHelicopterAI.aiState.PATROL:
			this.State_Patrol_Think(timePassed);
			break;
		case global::PatrolHelicopterAI.aiState.DEATH:
			this.State_Death_Think(timePassed);
			break;
		}
	}

	// Token: 0x0600163D RID: 5693 RVA: 0x00080F0C File Offset: 0x0007F10C
	public Vector3 GetRandomOffset(Vector3 origin, float minRange, float maxRange = 0f, float minHeight = 20f, float maxHeight = 30f)
	{
		Vector3 onUnitSphere = Random.onUnitSphere;
		onUnitSphere.y = 0f;
		onUnitSphere.Normalize();
		maxRange = Mathf.Max(minRange, maxRange);
		Vector3 origin2 = origin + onUnitSphere * Random.Range(minRange, maxRange);
		return this.GetAppropriatePosition(origin2, minHeight, maxHeight);
	}

	// Token: 0x0600163E RID: 5694 RVA: 0x00080F5C File Offset: 0x0007F15C
	public Vector3 GetAppropriatePosition(Vector3 origin, float minHeight = 20f, float maxHeight = 30f)
	{
		float num = 100f;
		Ray ray;
		ray..ctor(origin + new Vector3(0f, num, 0f), Vector3.down);
		float num2 = 5f;
		int mask = LayerMask.GetMask(new string[]
		{
			"Terrain",
			"World",
			"Construction",
			"Water"
		});
		RaycastHit raycastHit;
		if (UnityEngine.Physics.SphereCast(ray, num2, ref raycastHit, num * 2f - num2, mask))
		{
			origin = raycastHit.point;
		}
		origin.y += Random.Range(minHeight, maxHeight);
		return origin;
	}

	// Token: 0x0600163F RID: 5695 RVA: 0x00080FFC File Offset: 0x0007F1FC
	public float GetThrottleForDistance(float distToTarget)
	{
		float result;
		if (distToTarget >= 75f)
		{
			result = 1f;
		}
		else if (distToTarget >= 50f)
		{
			result = 0.75f;
		}
		else if (distToTarget >= 25f)
		{
			result = 0.33f;
		}
		else if (distToTarget >= 5f)
		{
			result = 0.05f;
		}
		else
		{
			result = 0.05f * (1f - distToTarget / 5f);
		}
		return result;
	}

	// Token: 0x0400107E RID: 4222
	public Vector3 interestZoneOrigin;

	// Token: 0x0400107F RID: 4223
	public Vector3 destination;

	// Token: 0x04001080 RID: 4224
	public bool hasInterestZone;

	// Token: 0x04001081 RID: 4225
	public float moveSpeed;

	// Token: 0x04001082 RID: 4226
	public float maxSpeed = 25f;

	// Token: 0x04001083 RID: 4227
	public float courseAdjustLerpTime = 2f;

	// Token: 0x04001084 RID: 4228
	public Quaternion targetRotation;

	// Token: 0x04001085 RID: 4229
	public Vector3 windVec;

	// Token: 0x04001086 RID: 4230
	public Vector3 targetWindVec;

	// Token: 0x04001087 RID: 4231
	public float windForce = 5f;

	// Token: 0x04001088 RID: 4232
	public float windFrequency = 1f;

	// Token: 0x04001089 RID: 4233
	public float targetThrottleSpeed;

	// Token: 0x0400108A RID: 4234
	public float throttleSpeed;

	// Token: 0x0400108B RID: 4235
	public float maxRotationSpeed = 90f;

	// Token: 0x0400108C RID: 4236
	public float rotationSpeed;

	// Token: 0x0400108D RID: 4237
	public float terrainPushForce = 100f;

	// Token: 0x0400108E RID: 4238
	public float obstaclePushForce = 100f;

	// Token: 0x0400108F RID: 4239
	public global::HelicopterTurret leftGun;

	// Token: 0x04001090 RID: 4240
	public global::HelicopterTurret rightGun;

	// Token: 0x04001091 RID: 4241
	public static global::PatrolHelicopterAI heliInstance;

	// Token: 0x04001092 RID: 4242
	public global::BaseHelicopter helicopterBase;

	// Token: 0x04001093 RID: 4243
	public global::PatrolHelicopterAI.aiState _currentState;

	// Token: 0x04001094 RID: 4244
	private Vector3 _aimTarget;

	// Token: 0x04001095 RID: 4245
	private bool movementLockingAiming;

	// Token: 0x04001096 RID: 4246
	private bool hasAimTarget;

	// Token: 0x04001097 RID: 4247
	private bool aimDoorSide;

	// Token: 0x04001098 RID: 4248
	private Vector3 pushVec = Vector3.zero;

	// Token: 0x04001099 RID: 4249
	private Vector3 _lastPos;

	// Token: 0x0400109A RID: 4250
	private Vector3 _lastMoveDir;

	// Token: 0x0400109B RID: 4251
	private bool isDead;

	// Token: 0x0400109C RID: 4252
	private bool isRetiring;

	// Token: 0x0400109D RID: 4253
	private float spawnTime;

	// Token: 0x0400109E RID: 4254
	private float lastDamageTime;

	// Token: 0x0400109F RID: 4255
	public List<global::PatrolHelicopterAI.targetinfo> _targetList = new List<global::PatrolHelicopterAI.targetinfo>();

	// Token: 0x040010A0 RID: 4256
	private float deathTimeout;

	// Token: 0x040010A1 RID: 4257
	private float destination_min_dist = 2f;

	// Token: 0x040010A2 RID: 4258
	private float currentOrbitDistance;

	// Token: 0x040010A3 RID: 4259
	private float currentOrbitTime;

	// Token: 0x040010A4 RID: 4260
	private bool hasEnteredOrbit;

	// Token: 0x040010A5 RID: 4261
	private float orbitStartTime;

	// Token: 0x040010A6 RID: 4262
	private float maxOrbitDuration = 30f;

	// Token: 0x040010A7 RID: 4263
	private bool breakingOrbit;

	// Token: 0x040010A8 RID: 4264
	public List<global::MonumentInfo> _visitedMonuments;

	// Token: 0x040010A9 RID: 4265
	public float arrivalTime;

	// Token: 0x040010AA RID: 4266
	public global::GameObjectRef rocketProjectile;

	// Token: 0x040010AB RID: 4267
	public global::GameObjectRef rocketProjectile_Napalm;

	// Token: 0x040010AC RID: 4268
	private bool leftTubeFiredLast;

	// Token: 0x040010AD RID: 4269
	private float lastRocketTime;

	// Token: 0x040010AE RID: 4270
	private float timeBetweenRockets = 0.2f;

	// Token: 0x040010AF RID: 4271
	private int numRocketsLeft = 12;

	// Token: 0x040010B0 RID: 4272
	private const int maxRockets = 12;

	// Token: 0x040010B1 RID: 4273
	private Vector3 strafe_target_position;

	// Token: 0x040010B2 RID: 4274
	private bool puttingDistance;

	// Token: 0x040010B3 RID: 4275
	private const float strafe_approach_range = 175f;

	// Token: 0x040010B4 RID: 4276
	private const float strafe_firing_range = 150f;

	// Token: 0x040010B5 RID: 4277
	private bool useNapalm;

	// Token: 0x040010B6 RID: 4278
	[NonSerialized]
	private float lastNapalmTime = float.NegativeInfinity;

	// Token: 0x040010B7 RID: 4279
	[NonSerialized]
	private float lastStrafeTime = float.NegativeInfinity;

	// Token: 0x040010B8 RID: 4280
	private float _lastThinkTime;

	// Token: 0x020003A7 RID: 935
	public class targetinfo
	{
		// Token: 0x06001640 RID: 5696 RVA: 0x0008107C File Offset: 0x0007F27C
		public targetinfo(global::BaseEntity initEnt, global::BasePlayer initPly = null)
		{
			this.ply = initPly;
			this.ent = initEnt;
			this.lastSeenTime = float.PositiveInfinity;
			this.nextLOSCheck = UnityEngine.Time.realtimeSinceStartup + 1.5f;
		}

		// Token: 0x06001641 RID: 5697 RVA: 0x000810BC File Offset: 0x0007F2BC
		public bool IsVisible()
		{
			return this.TimeSinceSeen() < 1.5f;
		}

		// Token: 0x06001642 RID: 5698 RVA: 0x000810CC File Offset: 0x0007F2CC
		public float TimeSinceSeen()
		{
			return UnityEngine.Time.realtimeSinceStartup - this.lastSeenTime;
		}

		// Token: 0x040010B9 RID: 4281
		public global::BasePlayer ply;

		// Token: 0x040010BA RID: 4282
		public global::BaseEntity ent;

		// Token: 0x040010BB RID: 4283
		public float lastSeenTime = float.PositiveInfinity;

		// Token: 0x040010BC RID: 4284
		public float visibleFor;

		// Token: 0x040010BD RID: 4285
		public float nextLOSCheck;
	}

	// Token: 0x020003A8 RID: 936
	public enum aiState
	{
		// Token: 0x040010BF RID: 4287
		IDLE,
		// Token: 0x040010C0 RID: 4288
		MOVE,
		// Token: 0x040010C1 RID: 4289
		ORBIT,
		// Token: 0x040010C2 RID: 4290
		STRAFE,
		// Token: 0x040010C3 RID: 4291
		PATROL,
		// Token: 0x040010C4 RID: 4292
		GUARD,
		// Token: 0x040010C5 RID: 4293
		DEATH
	}
}
