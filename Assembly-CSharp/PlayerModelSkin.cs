﻿using System;
using UnityEngine;

// Token: 0x02000276 RID: 630
public class PlayerModelSkin : MonoBehaviour
{
	// Token: 0x060010AE RID: 4270 RVA: 0x0006468C File Offset: 0x0006288C
	public void Setup(global::SkinSetCollection skin, float materialNum, float meshNum)
	{
		global::SkinSet skinSet = skin.Get(meshNum);
		if (skinSet == null)
		{
			Debug.LogError("Skin.Get returned a NULL skin");
		}
		skinSet.Process(base.gameObject, materialNum);
	}
}
