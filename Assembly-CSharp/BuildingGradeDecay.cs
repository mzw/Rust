﻿using System;
using ConVar;

// Token: 0x0200037F RID: 895
public class BuildingGradeDecay : global::Decay
{
	// Token: 0x06001534 RID: 5428 RVA: 0x0007A780 File Offset: 0x00078980
	public override float GetDecayDelay(global::BaseEntity entity)
	{
		return base.GetDecayDelay(this.decayGrade);
	}

	// Token: 0x06001535 RID: 5429 RVA: 0x0007A790 File Offset: 0x00078990
	public override float GetDecayDuration(global::BaseEntity entity)
	{
		return base.GetDecayDuration(this.decayGrade);
	}

	// Token: 0x06001536 RID: 5430 RVA: 0x0007A7A0 File Offset: 0x000789A0
	public override bool ShouldDecay(global::BaseEntity entity)
	{
		return ConVar.Decay.upkeep || entity.IsOutside();
	}

	// Token: 0x04000FA4 RID: 4004
	public global::BuildingGrade.Enum decayGrade;
}
