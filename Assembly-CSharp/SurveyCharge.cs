﻿using System;
using System.Collections.Generic;
using Facepunch;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000397 RID: 919
public class SurveyCharge : global::TimedExplosive
{
	// Token: 0x060015BC RID: 5564 RVA: 0x0007D01C File Offset: 0x0007B21C
	public override void Explode()
	{
		base.Explode();
		if (global::WaterLevel.Test(base.transform.position))
		{
			return;
		}
		global::ResourceDepositManager.ResourceDeposit orCreate = global::ResourceDepositManager.GetOrCreate(base.transform.position);
		if (orCreate == null)
		{
			return;
		}
		if (Time.realtimeSinceStartup - orCreate.lastSurveyTime < 10f)
		{
			return;
		}
		orCreate.lastSurveyTime = Time.realtimeSinceStartup;
		RaycastHit raycastHit;
		if (!global::TransformUtil.GetGroundInfo(base.transform.position, out raycastHit, 0.3f, 8388608, null))
		{
			return;
		}
		Vector3 point = raycastHit.point;
		Vector3 normal = raycastHit.normal;
		List<global::SurveyCrater> list = Pool.GetList<global::SurveyCrater>();
		global::Vis.Entities<global::SurveyCrater>(base.transform.position, 10f, list, 1, 2);
		bool flag = list.Count > 0;
		Pool.FreeList<global::SurveyCrater>(ref list);
		if (flag)
		{
			return;
		}
		bool flag2 = false;
		bool flag3 = false;
		foreach (global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry resourceDepositEntry in orCreate._resources)
		{
			if (resourceDepositEntry.spawnType == global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM)
			{
				if (!resourceDepositEntry.isLiquid)
				{
					if (resourceDepositEntry.amount >= 1000)
					{
						int num = Mathf.Clamp(Mathf.CeilToInt(2.5f / resourceDepositEntry.workNeeded * 10f), 0, 5);
						int iAmount = 1;
						flag2 = true;
						if (resourceDepositEntry.isLiquid)
						{
							flag3 = true;
						}
						for (int i = 0; i < num; i++)
						{
							global::Item item = global::ItemManager.Create(resourceDepositEntry.type, iAmount, 0UL);
							Interface.CallHook("OnSurveyGather", new object[]
							{
								this,
								item
							});
							float aimCone = 20f;
							Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(aimCone, Vector3.up, true);
							global::BaseEntity baseEntity = item.Drop(base.transform.position + Vector3.up * 1f, modifiedAimConeDirection * Random.Range(5f, 10f), Random.rotation);
							baseEntity.SetAngularVelocity(Random.rotation.eulerAngles * 5f);
						}
					}
				}
			}
		}
		if (flag2)
		{
			string strPrefab = (!flag3) ? this.craterPrefab.resourcePath : this.craterPrefab_Oil.resourcePath;
			global::BaseEntity baseEntity2 = global::GameManager.server.CreateEntity(strPrefab, point, Quaternion.identity, true);
			if (baseEntity2)
			{
				baseEntity2.Spawn();
			}
		}
	}

	// Token: 0x0400100E RID: 4110
	public global::GameObjectRef craterPrefab;

	// Token: 0x0400100F RID: 4111
	public global::GameObjectRef craterPrefab_Oil;
}
