﻿using System;
using ConVar;

// Token: 0x020003FB RID: 1019
public class ServerBuildingManager : global::BuildingManager
{
	// Token: 0x060017A8 RID: 6056 RVA: 0x0008750C File Offset: 0x0008570C
	public void CheckSplit(global::DecayEntity ent)
	{
		if (ent.buildingID == 0u)
		{
			return;
		}
		global::BuildingManager.Building building = ent.GetBuilding();
		if (building == null)
		{
			return;
		}
		if (this.ShouldSplit(building))
		{
			this.Split(building);
		}
	}

	// Token: 0x060017A9 RID: 6057 RVA: 0x00087548 File Offset: 0x00085748
	private bool ShouldSplit(global::BuildingManager.Building building)
	{
		if (building.HasBuildingBlocks())
		{
			building.buildingBlocks[0].EntityLinkBroadcast();
			foreach (global::BuildingBlock buildingBlock in building.buildingBlocks)
			{
				if (!buildingBlock.ReceivedEntityLinkBroadcast())
				{
					return true;
				}
			}
			return false;
		}
		return false;
	}

	// Token: 0x060017AA RID: 6058 RVA: 0x000875CC File Offset: 0x000857CC
	private void Split(global::BuildingManager.Building building)
	{
		while (building.HasBuildingBlocks())
		{
			global::BuildingBlock buildingBlock = building.buildingBlocks[0];
			uint newID = global::BuildingManager.server.NewBuildingID();
			buildingBlock.EntityLinkBroadcast<global::BuildingBlock>(delegate(global::BuildingBlock b)
			{
				b.AttachToBuilding(newID);
			});
		}
		while (building.HasBuildingPrivileges())
		{
			global::BuildingPrivlidge buildingPrivlidge = building.buildingPrivileges[0];
			global::BuildingBlock nearbyBuildingBlock = buildingPrivlidge.GetNearbyBuildingBlock();
			buildingPrivlidge.AttachToBuilding((!nearbyBuildingBlock) ? 0u : nearbyBuildingBlock.buildingID);
		}
		while (building.HasDecayEntities())
		{
			global::DecayEntity decayEntity = building.decayEntities[0];
			global::BuildingBlock nearbyBuildingBlock2 = decayEntity.GetNearbyBuildingBlock();
			decayEntity.AttachToBuilding((!nearbyBuildingBlock2) ? 0u : nearbyBuildingBlock2.buildingID);
		}
	}

	// Token: 0x060017AB RID: 6059 RVA: 0x000876A8 File Offset: 0x000858A8
	public void CheckMerge(global::DecayEntity ent)
	{
		if (ent.buildingID == 0u)
		{
			return;
		}
		global::BuildingManager.Building building = ent.GetBuilding();
		if (building == null)
		{
			return;
		}
		ent.EntityLinkMessage<global::BuildingBlock>(delegate(global::BuildingBlock b)
		{
			global::BuildingManager.Building building;
			if (b.buildingID != building.ID)
			{
				building = b.GetBuilding();
				if (building != null)
				{
					this.Merge(building, building);
				}
			}
		});
	}

	// Token: 0x060017AC RID: 6060 RVA: 0x000876F8 File Offset: 0x000858F8
	private void Merge(global::BuildingManager.Building building1, global::BuildingManager.Building building2)
	{
		while (building2.HasDecayEntities())
		{
			global::DecayEntity decayEntity = building2.decayEntities[0];
			decayEntity.AttachToBuilding(building1.ID);
		}
	}

	// Token: 0x060017AD RID: 6061 RVA: 0x00087730 File Offset: 0x00085930
	public void Cycle()
	{
		using (TimeWarning.New("StabilityCheckQueue", 0.1f))
		{
			global::StabilityEntity.stabilityCheckQueue.RunQueue((double)ConVar.Stability.stabilityqueue);
		}
		using (TimeWarning.New("UpdateSurroundingsQueue", 0.1f))
		{
			global::StabilityEntity.updateSurroundingsQueue.RunQueue((double)ConVar.Stability.surroundingsqueue);
		}
		using (TimeWarning.New("UpdateSkinQueue", 0.1f))
		{
			global::BuildingBlock.updateSkinQueueServer.RunQueue(1.0);
		}
		using (TimeWarning.New("BuildingDecayTick", 0.1f))
		{
			int num = 5;
			BufferList<global::BuildingManager.Building> values = this.buildingDictionary.Values;
			int num2 = this.decayTickBuildingIndex;
			while (num2 < values.Count && num > 0)
			{
				BufferList<global::DecayEntity> values2 = values[num2].decayEntities.Values;
				int num3 = this.decayTickEntityIndex;
				while (num3 < values2.Count && num > 0)
				{
					values2[num3].DecayTick();
					num--;
					if (num <= 0)
					{
						this.decayTickBuildingIndex = num2;
						this.decayTickEntityIndex = num3;
					}
					num3++;
				}
				if (num > 0)
				{
					this.decayTickEntityIndex = 0;
				}
				num2++;
			}
			if (num > 0)
			{
				this.decayTickBuildingIndex = 0;
			}
		}
		using (TimeWarning.New("WorldDecayTick", 0.1f))
		{
			int num4 = 5;
			BufferList<global::DecayEntity> values3 = this.decayEntities.Values;
			int num5 = this.decayTickWorldIndex;
			while (num5 < values3.Count && num4 > 0)
			{
				values3[num5].DecayTick();
				num4--;
				if (num4 <= 0)
				{
					this.decayTickWorldIndex = num5;
				}
				num5++;
			}
			if (num4 > 0)
			{
				this.decayTickWorldIndex = 0;
			}
		}
	}

	// Token: 0x060017AE RID: 6062 RVA: 0x0008798C File Offset: 0x00085B8C
	public uint NewBuildingID()
	{
		return this.maxBuildingID += 1u;
	}

	// Token: 0x060017AF RID: 6063 RVA: 0x000879AC File Offset: 0x00085BAC
	public void LoadBuildingID(uint id)
	{
		this.maxBuildingID = Mathx.Max(this.maxBuildingID, id);
	}

	// Token: 0x04001224 RID: 4644
	private int decayTickBuildingIndex;

	// Token: 0x04001225 RID: 4645
	private int decayTickEntityIndex;

	// Token: 0x04001226 RID: 4646
	private int decayTickWorldIndex;

	// Token: 0x04001227 RID: 4647
	private uint maxBuildingID;
}
