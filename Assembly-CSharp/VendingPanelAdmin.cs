﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020000B4 RID: 180
public class VendingPanelAdmin : global::UIDialog
{
	// Token: 0x0400051E RID: 1310
	public GameObject sellOrderAdminContainer;

	// Token: 0x0400051F RID: 1311
	public GameObject sellOrderAdminPrefab;

	// Token: 0x04000520 RID: 1312
	public InputField storeNameInputField;
}
