﻿using System;
using UnityEngine;

// Token: 0x02000802 RID: 2050
public struct OccludeeSphere
{
	// Token: 0x060025E7 RID: 9703 RVA: 0x000D17E8 File Offset: 0x000CF9E8
	public OccludeeSphere(int id)
	{
		this.id = id;
		this.state = ((id >= 0) ? global::OcclusionCulling.GetStateById(id) : null);
		this.sphere = new global::OcclusionCulling.Sphere(Vector3.zero, 0f);
	}

	// Token: 0x060025E8 RID: 9704 RVA: 0x000D1820 File Offset: 0x000CFA20
	public OccludeeSphere(int id, global::OcclusionCulling.Sphere sphere)
	{
		this.id = id;
		this.state = ((id >= 0) ? global::OcclusionCulling.GetStateById(id) : null);
		this.sphere = sphere;
	}

	// Token: 0x170002C0 RID: 704
	// (get) Token: 0x060025E9 RID: 9705 RVA: 0x000D184C File Offset: 0x000CFA4C
	public bool IsRegistered
	{
		get
		{
			return this.id >= 0;
		}
	}

	// Token: 0x060025EA RID: 9706 RVA: 0x000D185C File Offset: 0x000CFA5C
	public void Invalidate()
	{
		this.id = -1;
		this.state = null;
		this.sphere = default(global::OcclusionCulling.Sphere);
	}

	// Token: 0x04002224 RID: 8740
	public int id;

	// Token: 0x04002225 RID: 8741
	public global::OccludeeState state;

	// Token: 0x04002226 RID: 8742
	public global::OcclusionCulling.Sphere sphere;
}
