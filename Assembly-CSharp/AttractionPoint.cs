﻿using System;

// Token: 0x02000207 RID: 519
public class AttractionPoint : global::PrefabAttribute
{
	// Token: 0x06000F6F RID: 3951 RVA: 0x0005E0EC File Offset: 0x0005C2EC
	protected override Type GetIndexedType()
	{
		return typeof(global::AttractionPoint);
	}

	// Token: 0x04000A20 RID: 2592
	public string groupName;
}
