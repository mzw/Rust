﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using UnityEngine;

// Token: 0x02000380 RID: 896
public abstract class Decay : global::PrefabAttribute, IServerComponent
{
	// Token: 0x06001538 RID: 5432 RVA: 0x0007A7BC File Offset: 0x000789BC
	protected float GetDecayDelay(global::BuildingGrade.Enum grade)
	{
		if (ConVar.Decay.upkeep)
		{
			if (ConVar.Decay.delay_override > 0f)
			{
				return ConVar.Decay.delay_override;
			}
			switch (grade)
			{
			default:
				return ConVar.Decay.delay_twig * 3600f;
			case global::BuildingGrade.Enum.Wood:
				return ConVar.Decay.delay_wood * 3600f;
			case global::BuildingGrade.Enum.Stone:
				return ConVar.Decay.delay_stone * 3600f;
			case global::BuildingGrade.Enum.Metal:
				return ConVar.Decay.delay_metal * 3600f;
			case global::BuildingGrade.Enum.TopTier:
				return ConVar.Decay.delay_toptier * 3600f;
			}
		}
		else
		{
			switch (grade)
			{
			default:
				return 3600f;
			case global::BuildingGrade.Enum.Wood:
				return 64800f;
			case global::BuildingGrade.Enum.Stone:
				return 64800f;
			case global::BuildingGrade.Enum.Metal:
				return 64800f;
			case global::BuildingGrade.Enum.TopTier:
				return 86400f;
			}
		}
	}

	// Token: 0x06001539 RID: 5433 RVA: 0x0007A880 File Offset: 0x00078A80
	protected float GetDecayDuration(global::BuildingGrade.Enum grade)
	{
		if (ConVar.Decay.upkeep)
		{
			if (ConVar.Decay.duration_override > 0f)
			{
				return ConVar.Decay.duration_override;
			}
			switch (grade)
			{
			default:
				return ConVar.Decay.duration_twig * 3600f;
			case global::BuildingGrade.Enum.Wood:
				return ConVar.Decay.duration_wood * 3600f;
			case global::BuildingGrade.Enum.Stone:
				return ConVar.Decay.duration_stone * 3600f;
			case global::BuildingGrade.Enum.Metal:
				return ConVar.Decay.duration_metal * 3600f;
			case global::BuildingGrade.Enum.TopTier:
				return ConVar.Decay.duration_toptier * 3600f;
			}
		}
		else
		{
			switch (grade)
			{
			default:
				return 3600f;
			case global::BuildingGrade.Enum.Wood:
				return 86400f;
			case global::BuildingGrade.Enum.Stone:
				return 172800f;
			case global::BuildingGrade.Enum.Metal:
				return 259200f;
			case global::BuildingGrade.Enum.TopTier:
				return 432000f;
			}
		}
	}

	// Token: 0x0600153A RID: 5434 RVA: 0x0007A944 File Offset: 0x00078B44
	public static void BuildingDecayTouch(global::BuildingBlock buildingBlock)
	{
		if (ConVar.Decay.upkeep)
		{
			return;
		}
		List<global::DecayEntity> list = Facepunch.Pool.GetList<global::DecayEntity>();
		global::Vis.Entities<global::DecayEntity>(buildingBlock.transform.position, 40f, list, 2097408, 2);
		for (int i = 0; i < list.Count; i++)
		{
			global::DecayEntity decayEntity = list[i];
			global::BuildingBlock buildingBlock2 = decayEntity as global::BuildingBlock;
			if (!buildingBlock2 || buildingBlock2.buildingID == buildingBlock.buildingID)
			{
				decayEntity.DecayTouch();
			}
		}
		Facepunch.Pool.FreeList<global::DecayEntity>(ref list);
	}

	// Token: 0x0600153B RID: 5435 RVA: 0x0007A9D4 File Offset: 0x00078BD4
	public static void EntityLinkDecayTouch(global::BaseEntity ent)
	{
		if (ConVar.Decay.upkeep)
		{
			return;
		}
		ent.EntityLinkBroadcast<global::DecayEntity>(delegate(global::DecayEntity decayEnt)
		{
			decayEnt.DecayTouch();
		});
	}

	// Token: 0x0600153C RID: 5436 RVA: 0x0007AA04 File Offset: 0x00078C04
	public static void RadialDecayTouch(Vector3 pos, float radius, int mask)
	{
		if (ConVar.Decay.upkeep)
		{
			return;
		}
		List<global::DecayEntity> list = Facepunch.Pool.GetList<global::DecayEntity>();
		global::Vis.Entities<global::DecayEntity>(pos, radius, list, mask, 2);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].DecayTouch();
		}
		Facepunch.Pool.FreeList<global::DecayEntity>(ref list);
	}

	// Token: 0x0600153D RID: 5437 RVA: 0x0007AA58 File Offset: 0x00078C58
	public virtual bool ShouldDecay(global::BaseEntity entity)
	{
		return true;
	}

	// Token: 0x0600153E RID: 5438
	public abstract float GetDecayDelay(global::BaseEntity entity);

	// Token: 0x0600153F RID: 5439
	public abstract float GetDecayDuration(global::BaseEntity entity);

	// Token: 0x06001540 RID: 5440 RVA: 0x0007AA5C File Offset: 0x00078C5C
	protected override Type GetIndexedType()
	{
		return typeof(global::Decay);
	}

	// Token: 0x04000FA5 RID: 4005
	private const float hours = 3600f;
}
