﻿using System;

// Token: 0x0200044D RID: 1101
public class LODGrid : SingletonComponent<global::LODGrid>, IClientComponent
{
	// Token: 0x04001337 RID: 4919
	public static bool Paused;

	// Token: 0x04001338 RID: 4920
	public float CellSize = 50f;

	// Token: 0x04001339 RID: 4921
	public float MaxMilliseconds = 0.1f;

	// Token: 0x0400133A RID: 4922
	public const float MaxRefreshDistance = 500f;
}
