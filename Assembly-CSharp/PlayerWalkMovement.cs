﻿using System;
using UnityEngine;

// Token: 0x020003CA RID: 970
public class PlayerWalkMovement : global::BaseMovement
{
	// Token: 0x0400116A RID: 4458
	public const float WaterLevelHead = 0.75f;

	// Token: 0x0400116B RID: 4459
	public const float WaterLevelNeck = 0.65f;

	// Token: 0x0400116C RID: 4460
	public PhysicMaterial zeroFrictionMaterial;

	// Token: 0x0400116D RID: 4461
	public PhysicMaterial highFrictionMaterial;

	// Token: 0x0400116E RID: 4462
	public float capsuleHeight = 1f;

	// Token: 0x0400116F RID: 4463
	public float capsuleCenter = 1f;

	// Token: 0x04001170 RID: 4464
	public float capsuleHeightDucked = 1f;

	// Token: 0x04001171 RID: 4465
	public float capsuleCenterDucked = 1f;

	// Token: 0x04001172 RID: 4466
	public float gravityTestRadius = 0.2f;

	// Token: 0x04001173 RID: 4467
	public float gravityMultiplier = 2.5f;

	// Token: 0x04001174 RID: 4468
	public float gravityMultiplierSwimming = 0.1f;

	// Token: 0x04001175 RID: 4469
	public float maxAngleWalking = 50f;

	// Token: 0x04001176 RID: 4470
	public float maxAngleClimbing = 60f;

	// Token: 0x04001177 RID: 4471
	public float maxAngleSliding = 90f;
}
