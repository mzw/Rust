﻿using System;
using UnityStandardAssets.ImageEffects;

// Token: 0x0200023E RID: 574
public class HurtOverlay : ImageEffectLayer
{
	// Token: 0x04000AE5 RID: 2789
	public ScreenOverlayEx bloodOverlay;

	// Token: 0x04000AE6 RID: 2790
	public VignetteAndChromaticAberration vignetting;

	// Token: 0x04000AE7 RID: 2791
	public CC_Grayscale grayScale;
}
