﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x020003F8 RID: 1016
public static class SelfCheck
{
	// Token: 0x0600178E RID: 6030 RVA: 0x00086FD4 File Offset: 0x000851D4
	public static bool Run()
	{
		if (global::FileSystem_AssetBundles.isError)
		{
			return global::SelfCheck.Failed("Asset Bundle Error: " + global::FileSystem_AssetBundles.loadingError);
		}
		global::GameManifest gameManifest = global::FileSystem.Load<global::GameManifest>("Assets/manifest.asset", true);
		if (gameManifest == null)
		{
			return global::SelfCheck.Failed("Couldn't load game manifest - verify your game content!");
		}
		return global::SelfCheck.TestRustNative();
	}

	// Token: 0x0600178F RID: 6031 RVA: 0x00087030 File Offset: 0x00085230
	private static bool Failed(string Message)
	{
		if (SingletonComponent<global::Bootstrap>.Instance)
		{
			SingletonComponent<global::Bootstrap>.Instance.messageString = string.Empty;
			SingletonComponent<global::Bootstrap>.Instance.ThrowError(Message);
		}
		Debug.LogError("SelfCheck Failed: " + Message);
		return false;
	}

	// Token: 0x06001790 RID: 6032 RVA: 0x0008706C File Offset: 0x0008526C
	private static bool TestRustNative()
	{
		try
		{
			if (!global::SelfCheck.RustNative_VersionCheck(5))
			{
				return global::SelfCheck.Failed("RustNative is wrong version!");
			}
		}
		catch (DllNotFoundException ex)
		{
			return global::SelfCheck.Failed("RustNative library couldn't load! " + ex.Message);
		}
		return true;
	}

	// Token: 0x06001791 RID: 6033
	[DllImport("RustNative")]
	private static extern bool RustNative_VersionCheck(int version);
}
