﻿using System;

// Token: 0x0200023B RID: 571
public class ColdOverlay : ImageEffectLayer
{
	// Token: 0x04000ADE RID: 2782
	internal bool isPlaying;

	// Token: 0x04000ADF RID: 2783
	public ScreenOverlayEx screenOverlay;

	// Token: 0x04000AE0 RID: 2784
	public CC_Frost frost;

	// Token: 0x04000AE1 RID: 2785
	public LensDirtiness lensDirtyness;
}
