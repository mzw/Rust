﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000412 RID: 1042
public class DeployVolumeEntityBoundsReverse : global::DeployVolume
{
	// Token: 0x060017F3 RID: 6131 RVA: 0x00088A20 File Offset: 0x00086C20
	protected override bool Check(Vector3 position, Quaternion rotation, int mask = -1)
	{
		position += rotation * this.bounds.center;
		OBB test;
		test..ctor(position, this.bounds.size, rotation);
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(position, test.extents.magnitude, list, this.layers & mask, 2);
		foreach (global::BaseEntity baseEntity in list)
		{
			global::DeployVolume[] volumes = global::PrefabAttribute.server.FindAll<global::DeployVolume>(baseEntity.prefabID);
			if (global::DeployVolume.Check(baseEntity.transform.position, baseEntity.transform.rotation, volumes, test, 1 << this.layer))
			{
				Pool.FreeList<global::BaseEntity>(ref list);
				return true;
			}
		}
		Pool.FreeList<global::BaseEntity>(ref list);
		return false;
	}

	// Token: 0x060017F4 RID: 6132 RVA: 0x00088B20 File Offset: 0x00086D20
	protected override bool Check(Vector3 position, Quaternion rotation, OBB test, int mask = -1)
	{
		return false;
	}

	// Token: 0x060017F5 RID: 6133 RVA: 0x00088B24 File Offset: 0x00086D24
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		this.bounds = rootObj.GetComponent<global::BaseEntity>().bounds;
		this.layer = rootObj.layer;
	}

	// Token: 0x04001283 RID: 4739
	private Bounds bounds = new Bounds(Vector3.zero, Vector3.one);

	// Token: 0x04001284 RID: 4740
	private int layer;
}
