﻿using System;
using UnityEngine;

// Token: 0x02000503 RID: 1283
public class LeavesBlowing : MonoBehaviour
{
	// Token: 0x06001B44 RID: 6980 RVA: 0x00098428 File Offset: 0x00096628
	private void Start()
	{
	}

	// Token: 0x06001B45 RID: 6981 RVA: 0x0009842C File Offset: 0x0009662C
	private void Update()
	{
		base.transform.RotateAround(base.transform.position, Vector3.up, Time.deltaTime * this.m_flSwirl);
		if (this.m_psLeaves != null)
		{
			this.m_psLeaves.startSpeed = this.m_flSpeed;
			this.m_psLeaves.startSpeed += Mathf.Sin(Time.time * 0.4f) * (this.m_flSpeed * 0.75f);
			this.m_psLeaves.emissionRate = this.m_flEmissionRate + Mathf.Sin(Time.time * 1f) * (this.m_flEmissionRate * 0.3f);
		}
	}

	// Token: 0x04001603 RID: 5635
	public ParticleSystem m_psLeaves;

	// Token: 0x04001604 RID: 5636
	public float m_flSwirl;

	// Token: 0x04001605 RID: 5637
	public float m_flSpeed;

	// Token: 0x04001606 RID: 5638
	public float m_flEmissionRate;
}
