﻿using System;

// Token: 0x02000463 RID: 1123
public enum MonumentTier
{
	// Token: 0x0400136B RID: 4971
	Tier0 = 1,
	// Token: 0x0400136C RID: 4972
	Tier1,
	// Token: 0x0400136D RID: 4973
	Tier2 = 4
}
