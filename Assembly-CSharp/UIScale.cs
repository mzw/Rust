﻿using System;
using ConVar;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000721 RID: 1825
public class UIScale : MonoBehaviour
{
	// Token: 0x06002297 RID: 8855 RVA: 0x000C129C File Offset: 0x000BF49C
	private void Update()
	{
		Vector2 vector;
		vector..ctor(1280f / ConVar.Graphics.uiscale, 720f / ConVar.Graphics.uiscale);
		if (this.scaler.referenceResolution != vector)
		{
			this.scaler.referenceResolution = vector;
		}
	}

	// Token: 0x04001F02 RID: 7938
	public CanvasScaler scaler;
}
