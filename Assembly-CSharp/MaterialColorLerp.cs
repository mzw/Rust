﻿using System;
using UnityEngine;

// Token: 0x020003B9 RID: 953
public class MaterialColorLerp : MonoBehaviour, IClientComponent
{
	// Token: 0x04001101 RID: 4353
	public Color startColor;

	// Token: 0x04001102 RID: 4354
	public Color endColor;

	// Token: 0x04001103 RID: 4355
	public Color currentColor;

	// Token: 0x04001104 RID: 4356
	public float delta;
}
