﻿using System;
using ConVar;
using Facepunch;
using ProtoBuf;
using UnityEngine;

// Token: 0x02000352 RID: 850
public class BaseCorpse : global::BaseCombatEntity
{
	// Token: 0x0600144F RID: 5199 RVA: 0x00076988 File Offset: 0x00074B88
	public override void ServerInit()
	{
		this.SetupRigidBody();
		this.ResetRemovalTime();
		this.resourceDispenser = base.GetComponent<global::ResourceDispenser>();
		base.ServerInit();
	}

	// Token: 0x06001450 RID: 5200 RVA: 0x000769AC File Offset: 0x00074BAC
	public virtual void InitCorpse(global::BaseEntity pr)
	{
		this.parentEnt = pr;
		base.transform.position = this.parentEnt.transform.position;
		base.transform.rotation = this.parentEnt.transform.rotation;
	}

	// Token: 0x06001451 RID: 5201 RVA: 0x000769EC File Offset: 0x00074BEC
	public virtual bool CanRemove()
	{
		return true;
	}

	// Token: 0x06001452 RID: 5202 RVA: 0x000769F0 File Offset: 0x00074BF0
	public void RemoveCorpse()
	{
		if (!this.CanRemove())
		{
			this.ResetRemovalTime();
			return;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06001453 RID: 5203 RVA: 0x00076A0C File Offset: 0x00074C0C
	public void ResetRemovalTime(float dur)
	{
		using (TimeWarning.New("ResetRemovalTime", 0.1f))
		{
			if (base.IsInvoking(new Action(this.RemoveCorpse)))
			{
				base.CancelInvoke(new Action(this.RemoveCorpse));
			}
			base.Invoke(new Action(this.RemoveCorpse), dur);
		}
	}

	// Token: 0x06001454 RID: 5204 RVA: 0x00076A88 File Offset: 0x00074C88
	public virtual float GetRemovalTime()
	{
		return ConVar.Server.corpsedespawn;
	}

	// Token: 0x06001455 RID: 5205 RVA: 0x00076A90 File Offset: 0x00074C90
	public void ResetRemovalTime()
	{
		this.ResetRemovalTime(this.GetRemovalTime());
	}

	// Token: 0x06001456 RID: 5206 RVA: 0x00076AA0 File Offset: 0x00074CA0
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.corpse = Facepunch.Pool.Get<Corpse>();
		if (this.parentEnt.IsValid())
		{
			info.msg.corpse.parentID = this.parentEnt.net.ID;
		}
	}

	// Token: 0x06001457 RID: 5207 RVA: 0x00076AF8 File Offset: 0x00074CF8
	public void TakeChildren(global::BaseEntity takeChildrenFrom)
	{
		if (takeChildrenFrom.children == null)
		{
			return;
		}
		using (TimeWarning.New("Corpse.TakeChildren", 0.1f))
		{
			foreach (global::BaseEntity baseEntity in takeChildrenFrom.children.ToArray())
			{
				baseEntity.ParentBecoming(this);
			}
		}
	}

	// Token: 0x06001458 RID: 5208 RVA: 0x00076B70 File Offset: 0x00074D70
	private Rigidbody SetupRigidBody()
	{
		GameObject gameObject = base.gameManager.FindPrefab(this.prefabRagdoll.resourcePath);
		if (gameObject == null)
		{
			return null;
		}
		global::Ragdoll component = gameObject.GetComponent<global::Ragdoll>();
		if (component == null)
		{
			return null;
		}
		if (component.primaryBody == null)
		{
			Debug.LogError("[BaseCorpse] ragdoll.primaryBody isn't set!" + component.gameObject.name);
			return null;
		}
		BoxCollider component2 = component.primaryBody.GetComponent<BoxCollider>();
		if (!component2)
		{
			Debug.LogError("Ragdoll has unsupported primary collider (make it supported) ", component);
			return null;
		}
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		boxCollider.size = component2.size;
		boxCollider.center = component2.center;
		Rigidbody rigidbody = base.gameObject.AddComponent<Rigidbody>();
		if (rigidbody == null)
		{
			Debug.LogError("[BaseCorpse] already has a RigidBody defined - and it shouldn't!" + base.gameObject.name);
			return null;
		}
		rigidbody.mass = 10f;
		rigidbody.useGravity = true;
		rigidbody.drag = 0.5f;
		rigidbody.collisionDetectionMode = 0;
		ConVar.Physics.ApplyDropped(rigidbody, this);
		Vector3 velocity = Vector3Ex.Range(-1f, 1f);
		velocity.y += 1f;
		rigidbody.velocity = velocity;
		rigidbody.angularVelocity = Vector3Ex.Range(-10f, 10f);
		if (base.isClient)
		{
			rigidbody.useGravity = false;
			rigidbody.isKinematic = true;
		}
		return rigidbody;
	}

	// Token: 0x06001459 RID: 5209 RVA: 0x00076CF8 File Offset: 0x00074EF8
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.corpse != null)
		{
			this.Load(info.msg.corpse);
		}
	}

	// Token: 0x0600145A RID: 5210 RVA: 0x00076D24 File Offset: 0x00074F24
	private void Load(Corpse corpse)
	{
		if (base.isServer)
		{
			this.parentEnt = (global::BaseNetworkable.serverEntities.Find(corpse.parentID) as global::BaseEntity);
		}
		if (base.isClient)
		{
		}
	}

	// Token: 0x0600145B RID: 5211 RVA: 0x00076D58 File Offset: 0x00074F58
	public override void OnAttacked(global::HitInfo info)
	{
		if (base.isServer)
		{
			this.ResetRemovalTime();
			if (this.resourceDispenser)
			{
				this.resourceDispenser.OnAttacked(info);
			}
			if (!info.DidGather)
			{
				base.OnAttacked(info);
			}
		}
	}

	// Token: 0x0600145C RID: 5212 RVA: 0x00076DA4 File Offset: 0x00074FA4
	public override string Categorize()
	{
		return "corpse";
	}

	// Token: 0x17000173 RID: 371
	// (get) Token: 0x0600145D RID: 5213 RVA: 0x00076DAC File Offset: 0x00074FAC
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return base.Traits | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x0600145E RID: 5214 RVA: 0x00076DBC File Offset: 0x00074FBC
	public override void Eat(global::BaseNpc baseNpc, float timeSpent)
	{
		this.ResetRemovalTime();
		this.Hurt(timeSpent * 5f);
		baseNpc.AddCalories(timeSpent * 2f);
	}

	// Token: 0x04000F20 RID: 3872
	public global::GameObjectRef prefabRagdoll;

	// Token: 0x04000F21 RID: 3873
	public global::BaseEntity parentEnt;

	// Token: 0x04000F22 RID: 3874
	[NonSerialized]
	internal global::ResourceDispenser resourceDispenser;
}
