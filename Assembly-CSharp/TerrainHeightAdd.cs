﻿using System;
using UnityEngine;

// Token: 0x020005D2 RID: 1490
public class TerrainHeightAdd : global::TerrainModifier
{
	// Token: 0x06001EBB RID: 7867 RVA: 0x000AD748 File Offset: 0x000AB948
	protected override void Apply(Vector3 position, float opacity, float radius, float fade)
	{
		if (!global::TerrainMeta.HeightMap)
		{
			return;
		}
		global::TerrainMeta.HeightMap.AddHeight(position, opacity * this.Delta * global::TerrainMeta.OneOverSize.y, radius, fade);
	}

	// Token: 0x04001998 RID: 6552
	public float Delta = 1f;
}
