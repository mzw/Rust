﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000589 RID: 1417
public class TerrainPath : global::TerrainExtension
{
	// Token: 0x06001E10 RID: 7696 RVA: 0x000A72A8 File Offset: 0x000A54A8
	public void Clear()
	{
		this.Roads.Clear();
		this.Rivers.Clear();
		this.Powerlines.Clear();
	}

	// Token: 0x06001E11 RID: 7697 RVA: 0x000A72CC File Offset: 0x000A54CC
	public void AddWire(global::PowerlineNode node)
	{
		string name = node.transform.root.name;
		if (!this.wires.ContainsKey(name))
		{
			this.wires.Add(name, new List<global::PowerlineNode>());
		}
		this.wires[name].Add(node);
	}

	// Token: 0x06001E12 RID: 7698 RVA: 0x000A7320 File Offset: 0x000A5520
	public void CreateWires()
	{
		List<GameObject> list = new List<GameObject>();
		int num = 0;
		Material material = null;
		foreach (KeyValuePair<string, List<global::PowerlineNode>> keyValuePair in this.wires)
		{
			foreach (global::PowerlineNode powerlineNode in keyValuePair.Value)
			{
				MegaWireConnectionHelper component = powerlineNode.GetComponent<MegaWireConnectionHelper>();
				if (component)
				{
					if (list.Count == 0)
					{
						material = powerlineNode.WireMaterial;
						num = component.connections.Count;
					}
					else
					{
						GameObject gameObject = list[list.Count - 1];
						if (powerlineNode.WireMaterial != material || component.connections.Count != num || (gameObject.transform.position - powerlineNode.transform.position).sqrMagnitude > powerlineNode.MaxDistance * powerlineNode.MaxDistance)
						{
							this.CreateWire(keyValuePair.Key, list, material);
							list.Clear();
						}
					}
					list.Add(powerlineNode.gameObject);
				}
			}
			this.CreateWire(keyValuePair.Key, list, material);
			list.Clear();
		}
	}

	// Token: 0x06001E13 RID: 7699 RVA: 0x000A74C0 File Offset: 0x000A56C0
	private void CreateWire(string name, List<GameObject> objects, Material material)
	{
		if (objects.Count >= 3 && material != null)
		{
			MegaWire megaWire = MegaWire.Create(null, objects, material, "Powerline Wires", null, 1f, 0.1f);
			if (megaWire)
			{
				megaWire.enabled = false;
				megaWire.RunPhysics(megaWire.warmPhysicsTime);
				megaWire.gameObject.SetHierarchyGroup(name, true, false);
			}
		}
	}

	// Token: 0x04001895 RID: 6293
	internal List<global::PathList> Roads = new List<global::PathList>();

	// Token: 0x04001896 RID: 6294
	internal List<global::PathList> Rivers = new List<global::PathList>();

	// Token: 0x04001897 RID: 6295
	internal List<global::PathList> Powerlines = new List<global::PathList>();

	// Token: 0x04001898 RID: 6296
	internal List<global::MonumentInfo> Monuments = new List<global::MonumentInfo>();

	// Token: 0x04001899 RID: 6297
	internal List<global::RiverInfo> RiverObjs = new List<global::RiverInfo>();

	// Token: 0x0400189A RID: 6298
	internal List<global::LakeInfo> LakeObjs = new List<global::LakeInfo>();

	// Token: 0x0400189B RID: 6299
	private Dictionary<string, List<global::PowerlineNode>> wires = new Dictionary<string, List<global::PowerlineNode>>();
}
