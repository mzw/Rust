﻿using System;
using UnityEngine;

// Token: 0x02000510 RID: 1296
[Serializable]
public class ByteMap
{
	// Token: 0x06001B6A RID: 7018 RVA: 0x00098CB0 File Offset: 0x00096EB0
	public ByteMap(int size, int bytes = 1)
	{
		this.size = size;
		this.bytes = bytes;
		this.values = new byte[bytes * size * size];
	}

	// Token: 0x06001B6B RID: 7019 RVA: 0x00098CD8 File Offset: 0x00096ED8
	public ByteMap(int size, byte[] values, int bytes = 1)
	{
		this.size = size;
		this.bytes = bytes;
		this.values = values;
	}

	// Token: 0x170001DA RID: 474
	// (get) Token: 0x06001B6C RID: 7020 RVA: 0x00098CF8 File Offset: 0x00096EF8
	public int Size
	{
		get
		{
			return this.size;
		}
	}

	// Token: 0x170001DB RID: 475
	public uint this[int x, int y]
	{
		get
		{
			int num = y * this.bytes * this.size + x * this.bytes;
			switch (this.bytes)
			{
			case 1:
				return (uint)this.values[num];
			case 2:
			{
				uint num2 = (uint)this.values[num];
				uint num3 = (uint)this.values[num + 1];
				return num2 << 8 | num3;
			}
			case 3:
			{
				uint num2 = (uint)this.values[num];
				uint num3 = (uint)this.values[num + 1];
				uint num4 = (uint)this.values[num + 2];
				return num2 << 16 | num3 << 8 | num4;
			}
			default:
			{
				uint num2 = (uint)this.values[num];
				uint num3 = (uint)this.values[num + 1];
				uint num4 = (uint)this.values[num + 2];
				uint num5 = (uint)this.values[num + 3];
				return num2 << 24 | num3 << 16 | num4 << 8 | num5;
			}
			}
		}
		set
		{
			int num = y * this.bytes * this.size + x * this.bytes;
			switch (this.bytes)
			{
			case 1:
				this.values[num] = (byte)(value >> 0 & 255u);
				break;
			case 2:
				this.values[num] = (byte)(value >> 8 & 255u);
				this.values[num + 1] = (byte)(value >> 0 & 255u);
				break;
			case 3:
				this.values[num] = (byte)(value >> 16 & 255u);
				this.values[num + 1] = (byte)(value >> 8 & 255u);
				this.values[num + 2] = (byte)(value >> 0 & 255u);
				break;
			default:
				this.values[num] = (byte)(value >> 24 & 255u);
				this.values[num + 1] = (byte)(value >> 16 & 255u);
				this.values[num + 2] = (byte)(value >> 8 & 255u);
				this.values[num + 3] = (byte)(value >> 0 & 255u);
				break;
			}
		}
	}

	// Token: 0x04001624 RID: 5668
	[SerializeField]
	private int size;

	// Token: 0x04001625 RID: 5669
	[SerializeField]
	private int bytes;

	// Token: 0x04001626 RID: 5670
	[SerializeField]
	private byte[] values;
}
