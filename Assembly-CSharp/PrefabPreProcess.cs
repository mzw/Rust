﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

// Token: 0x0200046A RID: 1130
public class PrefabPreProcess : global::IPrefabProcessor
{
	// Token: 0x060018C1 RID: 6337 RVA: 0x0008B400 File Offset: 0x00089600
	public PrefabPreProcess(bool clientside, bool serverside)
	{
		this.isClientside = clientside;
		this.isServerside = serverside;
	}

	// Token: 0x060018C2 RID: 6338 RVA: 0x0008B43C File Offset: 0x0008963C
	public GameObject Find(string strPrefab)
	{
		GameObject gameObject;
		if (!this.prefabList.TryGetValue(strPrefab, out gameObject))
		{
			return null;
		}
		if (gameObject == null)
		{
			this.prefabList.Remove(strPrefab);
			return null;
		}
		return gameObject;
	}

	// Token: 0x060018C3 RID: 6339 RVA: 0x0008B47C File Offset: 0x0008967C
	public bool NeedsProcessing(GameObject go)
	{
		if (go.CompareTag("NoPreProcessing"))
		{
			return false;
		}
		if (global::PrefabPreProcess.HasComponents<global::IPrefabPreProcess>(go.transform) || global::PrefabPreProcess.HasComponents<IEditorComponent>(go.transform))
		{
			return true;
		}
		if (!this.isClientside)
		{
			if (global::PrefabPreProcess.clientsideOnlyTypes.Any((Type type) => global::PrefabPreProcess.HasComponents(go.transform, type)))
			{
				return true;
			}
			if (global::PrefabPreProcess.HasComponents<global::IClientComponentEx>(go.transform))
			{
				return true;
			}
		}
		if (!this.isServerside)
		{
			if (global::PrefabPreProcess.serversideOnlyTypes.Any((Type type) => global::PrefabPreProcess.HasComponents(go.transform, type)))
			{
				return true;
			}
			if (global::PrefabPreProcess.HasComponents<global::IServerComponentEx>(go.transform))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060018C4 RID: 6340 RVA: 0x0008B558 File Offset: 0x00089758
	public bool ProcessObject(string name, GameObject go, bool isBundling = false, bool resetLocalTransform = true)
	{
		if (!this.NeedsProcessing(go))
		{
			return false;
		}
		if (!this.isClientside)
		{
			foreach (Type t in global::PrefabPreProcess.clientsideOnlyTypes)
			{
				this.DestroyComponents(t, go, this.isClientside, this.isServerside);
			}
			foreach (global::IClientComponentEx clientComponentEx in global::PrefabPreProcess.FindComponents<global::IClientComponentEx>(go.transform))
			{
				clientComponentEx.PreClientComponentCull(this);
			}
		}
		if (!this.isServerside)
		{
			foreach (Type t2 in global::PrefabPreProcess.serversideOnlyTypes)
			{
				this.DestroyComponents(t2, go, this.isClientside, this.isServerside);
			}
			foreach (global::IServerComponentEx serverComponentEx in global::PrefabPreProcess.FindComponents<global::IServerComponentEx>(go.transform))
			{
				serverComponentEx.PreServerComponentCull(this);
			}
		}
		this.DestroyComponents(typeof(IEditorComponent), go, this.isClientside, this.isServerside);
		if (resetLocalTransform)
		{
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
		}
		List<Transform> list = global::PrefabPreProcess.FindComponents<Transform>(go.transform);
		list.Reverse();
		foreach (global::IPrefabPreProcess prefabPreProcess in global::PrefabPreProcess.FindComponents<global::IPrefabPreProcess>(go.transform))
		{
			prefabPreProcess.PreProcess(this, go, name, this.isServerside, this.isClientside, isBundling);
		}
		foreach (Transform transform in list)
		{
			if (transform && transform.gameObject)
			{
				if (this.isServerside && transform.gameObject.CompareTag("Server Cull"))
				{
					this.RemoveComponents(transform.gameObject);
					this.NominateForDeletion(transform.gameObject);
				}
				if (this.isClientside && transform.gameObject.CompareTag("Client Cull"))
				{
					this.RemoveComponents(transform.gameObject);
					this.NominateForDeletion(transform.gameObject);
				}
				if (transform.gameObject.layer == 27)
				{
					this.RemoveComponents(transform.gameObject);
					this.NominateForDeletion(transform.gameObject);
				}
			}
		}
		this.RunCleanupQueue();
		return true;
	}

	// Token: 0x060018C5 RID: 6341 RVA: 0x0008B868 File Offset: 0x00089A68
	public void Process(string name, GameObject go)
	{
		if (go.CompareTag("NoPreProcessing"))
		{
			return;
		}
		if (this.NeedsProcessing(go))
		{
			this.ProcessObject(name, go, false, true);
		}
		this.AddPrefab(name, go);
	}

	// Token: 0x060018C6 RID: 6342 RVA: 0x0008B89C File Offset: 0x00089A9C
	public void AddPrefab(string name, GameObject go)
	{
		go.SetActive(false);
		this.prefabList.Add(name, go);
	}

	// Token: 0x060018C7 RID: 6343 RVA: 0x0008B8B4 File Offset: 0x00089AB4
	private void DestroyComponents(Type t, GameObject go, bool client, bool server)
	{
		List<Component> list = new List<Component>();
		global::PrefabPreProcess.FindComponents(go.transform, list, t);
		list.Reverse();
		foreach (Component component in list)
		{
			foreach (global::DeferredMeshDecal deferredMeshDecal in component.GetComponents<global::DeferredMeshDecal>())
			{
				if (deferredMeshDecal != null)
				{
					Object.DestroyImmediate(deferredMeshDecal, true);
				}
			}
		}
		foreach (Component component2 in list)
		{
			global::RealmedRemove component3 = component2.GetComponent<global::RealmedRemove>();
			if (!(component3 != null) || component3.ShouldDelete(component2, client, server))
			{
				this.NominateForDeletion(component2.gameObject);
				Object.DestroyImmediate(component2, true);
			}
		}
	}

	// Token: 0x060018C8 RID: 6344 RVA: 0x0008B9DC File Offset: 0x00089BDC
	private static bool HasComponents<T>(Transform transform)
	{
		if (transform.GetComponent<T>() != null)
		{
			return true;
		}
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform2 = (Transform)obj;
				if (transform2.GetComponent<global::BaseEntity>() == null && global::PrefabPreProcess.HasComponents<T>(transform2))
				{
					return true;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return false;
	}

	// Token: 0x060018C9 RID: 6345 RVA: 0x0008BA70 File Offset: 0x00089C70
	private static bool HasComponents(Transform transform, Type t)
	{
		if (transform.GetComponent(t) != null)
		{
			return true;
		}
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform2 = (Transform)obj;
				if (transform2.GetComponent<global::BaseEntity>() == null && global::PrefabPreProcess.HasComponents(transform2, t))
				{
					return true;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return false;
	}

	// Token: 0x060018CA RID: 6346 RVA: 0x0008BB04 File Offset: 0x00089D04
	private static List<T> FindComponents<T>(Transform transform)
	{
		List<T> list = new List<T>();
		global::PrefabPreProcess.FindComponents<T>(transform, list);
		return list;
	}

	// Token: 0x060018CB RID: 6347 RVA: 0x0008BB20 File Offset: 0x00089D20
	private static void FindComponents<T>(Transform transform, List<T> list)
	{
		list.AddRange(transform.GetComponents<T>());
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform2 = (Transform)obj;
				if (transform2.GetComponent<global::BaseEntity>() == null)
				{
					global::PrefabPreProcess.FindComponents<T>(transform2, list);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	// Token: 0x060018CC RID: 6348 RVA: 0x0008BBA0 File Offset: 0x00089DA0
	private static List<Component> FindComponents(Transform transform, Type t)
	{
		List<Component> list = new List<Component>();
		global::PrefabPreProcess.FindComponents(transform, list, t);
		return list;
	}

	// Token: 0x060018CD RID: 6349 RVA: 0x0008BBBC File Offset: 0x00089DBC
	private static void FindComponents(Transform transform, List<Component> list, Type t)
	{
		list.AddRange(transform.GetComponents(t));
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform2 = (Transform)obj;
				if (transform2.GetComponent<global::BaseEntity>() == null)
				{
					global::PrefabPreProcess.FindComponents(transform2, list, t);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	// Token: 0x060018CE RID: 6350 RVA: 0x0008BC3C File Offset: 0x00089E3C
	public void RemoveComponent(Component c)
	{
		if (c == null)
		{
			return;
		}
		this.destroyList.Add(c);
	}

	// Token: 0x060018CF RID: 6351 RVA: 0x0008BC58 File Offset: 0x00089E58
	public void RemoveComponents(GameObject gameObj)
	{
		foreach (Component component in gameObj.GetComponents<Component>())
		{
			if (!(component is Transform))
			{
				this.destroyList.Add(component);
			}
		}
	}

	// Token: 0x060018D0 RID: 6352 RVA: 0x0008BCA0 File Offset: 0x00089EA0
	public void NominateForDeletion(GameObject gameObj)
	{
		this.cleanupList.Add(gameObj);
	}

	// Token: 0x060018D1 RID: 6353 RVA: 0x0008BCB0 File Offset: 0x00089EB0
	private void RunCleanupQueue()
	{
		foreach (Component component in this.destroyList)
		{
			Object.DestroyImmediate(component, true);
		}
		this.destroyList.Clear();
		foreach (GameObject go in this.cleanupList)
		{
			this.DoCleanup(go);
		}
		this.cleanupList.Clear();
	}

	// Token: 0x060018D2 RID: 6354 RVA: 0x0008BD70 File Offset: 0x00089F70
	private void DoCleanup(GameObject go)
	{
		if (go == null)
		{
			return;
		}
		if (go.GetComponentsInChildren<Component>(true).Length > 1)
		{
			return;
		}
		Transform parent = go.transform.parent;
		if (parent == null)
		{
			return;
		}
		Object.DestroyImmediate(go, true);
	}

	// Token: 0x04001382 RID: 4994
	public static Type[] clientsideOnlyTypes = new Type[]
	{
		typeof(IClientComponent),
		typeof(ImageEffectLayer),
		typeof(MeshFilter),
		typeof(Renderer),
		typeof(AudioSource),
		typeof(AudioListener),
		typeof(ParticleEmitter),
		typeof(ParticleSystemRenderer),
		typeof(ParticleSystem),
		typeof(global::ParticleEmitFromParentObject),
		typeof(Light),
		typeof(LODGroup),
		typeof(Animator),
		typeof(global::AnimationEvents),
		typeof(global::PlayerVoiceSpeaker),
		typeof(global::PlayerVoiceRecorder),
		typeof(ParticleScaler),
		typeof(PostEffectsBase),
		typeof(TOD_ImageEffect),
		typeof(Tree),
		typeof(Projector),
		typeof(StandaloneInputModule),
		typeof(UIBehaviour),
		typeof(Canvas),
		typeof(CanvasRenderer),
		typeof(GraphicRaycaster)
	};

	// Token: 0x04001383 RID: 4995
	public static Type[] serversideOnlyTypes = new Type[]
	{
		typeof(IServerComponent),
		typeof(NavMeshObstacle)
	};

	// Token: 0x04001384 RID: 4996
	public bool isClientside;

	// Token: 0x04001385 RID: 4997
	public bool isServerside;

	// Token: 0x04001386 RID: 4998
	internal Dictionary<string, GameObject> prefabList = new Dictionary<string, GameObject>(StringComparer.OrdinalIgnoreCase);

	// Token: 0x04001387 RID: 4999
	private List<Component> destroyList = new List<Component>();

	// Token: 0x04001388 RID: 5000
	private List<GameObject> cleanupList = new List<GameObject>();
}
