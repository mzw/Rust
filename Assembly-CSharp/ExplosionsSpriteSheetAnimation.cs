﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020007FE RID: 2046
internal class ExplosionsSpriteSheetAnimation : MonoBehaviour
{
	// Token: 0x060025CE RID: 9678 RVA: 0x000D0ED8 File Offset: 0x000CF0D8
	private void Start()
	{
		this.currentRenderer = base.GetComponent<Renderer>();
		this.InitDefaultVariables();
		this.isInizialised = true;
		this.isVisible = true;
		this.Play();
	}

	// Token: 0x060025CF RID: 9679 RVA: 0x000D0F00 File Offset: 0x000CF100
	private void InitDefaultVariables()
	{
		this.currentRenderer = base.GetComponent<Renderer>();
		if (this.currentRenderer == null)
		{
			throw new Exception("UvTextureAnimator can't get renderer");
		}
		if (!this.currentRenderer.enabled)
		{
			this.currentRenderer.enabled = true;
		}
		this.allCount = 0;
		this.animationStoped = false;
		this.animationLifeTime = (float)(this.TilesX * this.TilesY) / this.AnimationFPS;
		this.count = this.TilesY * this.TilesX;
		this.index = this.TilesX - 1;
		Vector3 zero = Vector3.zero;
		this.StartFrameOffset -= this.StartFrameOffset / this.count * this.count;
		Vector2 vector;
		vector..ctor(1f / (float)this.TilesX, 1f / (float)this.TilesY);
		if (this.currentRenderer != null)
		{
			this.instanceMaterial = this.currentRenderer.material;
			this.instanceMaterial.SetTextureScale("_MainTex", vector);
			this.instanceMaterial.SetTextureOffset("_MainTex", zero);
		}
	}

	// Token: 0x060025D0 RID: 9680 RVA: 0x000D102C File Offset: 0x000CF22C
	private void Play()
	{
		if (this.isCorutineStarted)
		{
			return;
		}
		if (this.StartDelay > 0.0001f)
		{
			base.Invoke("PlayDelay", this.StartDelay);
		}
		else
		{
			base.StartCoroutine(this.UpdateCorutine());
		}
		this.isCorutineStarted = true;
	}

	// Token: 0x060025D1 RID: 9681 RVA: 0x000D1080 File Offset: 0x000CF280
	private void PlayDelay()
	{
		base.StartCoroutine(this.UpdateCorutine());
	}

	// Token: 0x060025D2 RID: 9682 RVA: 0x000D1090 File Offset: 0x000CF290
	private void OnEnable()
	{
		if (!this.isInizialised)
		{
			return;
		}
		this.InitDefaultVariables();
		this.isVisible = true;
		this.Play();
	}

	// Token: 0x060025D3 RID: 9683 RVA: 0x000D10B4 File Offset: 0x000CF2B4
	private void OnDisable()
	{
		this.isCorutineStarted = false;
		this.isVisible = false;
		base.StopAllCoroutines();
		base.CancelInvoke("PlayDelay");
	}

	// Token: 0x060025D4 RID: 9684 RVA: 0x000D10D8 File Offset: 0x000CF2D8
	private IEnumerator UpdateCorutine()
	{
		this.animationStartTime = Time.time;
		while (this.isVisible && (this.IsLoop || !this.animationStoped))
		{
			this.UpdateFrame();
			if (!this.IsLoop && this.animationStoped)
			{
				break;
			}
			float frameTime = (Time.time - this.animationStartTime) / this.animationLifeTime;
			float currentSpeedFps = this.FrameOverTime.Evaluate(Mathf.Clamp01(frameTime));
			yield return new WaitForSeconds(1f / (this.AnimationFPS * currentSpeedFps));
		}
		this.isCorutineStarted = false;
		this.currentRenderer.enabled = false;
		yield break;
	}

	// Token: 0x060025D5 RID: 9685 RVA: 0x000D10F4 File Offset: 0x000CF2F4
	private void UpdateFrame()
	{
		this.allCount++;
		this.index++;
		if (this.index >= this.count)
		{
			this.index = 0;
		}
		if (this.count == this.allCount)
		{
			this.animationStartTime = Time.time;
			this.allCount = 0;
			this.animationStoped = true;
		}
		Vector2 vector;
		vector..ctor((float)this.index / (float)this.TilesX - (float)(this.index / this.TilesX), 1f - (float)(this.index / this.TilesX) / (float)this.TilesY);
		if (this.currentRenderer != null)
		{
			this.instanceMaterial.SetTextureOffset("_MainTex", vector);
		}
		if (this.IsInterpolateFrames)
		{
			this.currentInterpolatedTime = 0f;
		}
	}

	// Token: 0x060025D6 RID: 9686 RVA: 0x000D11D8 File Offset: 0x000CF3D8
	private void Update()
	{
		if (!this.IsInterpolateFrames)
		{
			return;
		}
		this.currentInterpolatedTime += Time.deltaTime;
		int num = this.index + 1;
		if (this.allCount == 0)
		{
			num = this.index;
		}
		Vector4 vector;
		vector..ctor(1f / (float)this.TilesX, 1f / (float)this.TilesY, (float)num / (float)this.TilesX - (float)(num / this.TilesX), 1f - (float)(num / this.TilesX) / (float)this.TilesY);
		if (this.currentRenderer != null)
		{
			this.instanceMaterial.SetVector("_MainTex_NextFrame", vector);
			float num2 = (Time.time - this.animationStartTime) / this.animationLifeTime;
			float num3 = this.FrameOverTime.Evaluate(Mathf.Clamp01(num2));
			this.instanceMaterial.SetFloat("InterpolationValue", Mathf.Clamp01(this.currentInterpolatedTime * this.AnimationFPS * num3));
		}
	}

	// Token: 0x060025D7 RID: 9687 RVA: 0x000D12D8 File Offset: 0x000CF4D8
	private void OnDestroy()
	{
		if (this.instanceMaterial != null)
		{
			Object.Destroy(this.instanceMaterial);
			this.instanceMaterial = null;
		}
	}

	// Token: 0x040021FA RID: 8698
	public int TilesX = 4;

	// Token: 0x040021FB RID: 8699
	public int TilesY = 4;

	// Token: 0x040021FC RID: 8700
	public float AnimationFPS = 30f;

	// Token: 0x040021FD RID: 8701
	public bool IsInterpolateFrames;

	// Token: 0x040021FE RID: 8702
	public int StartFrameOffset;

	// Token: 0x040021FF RID: 8703
	public bool IsLoop = true;

	// Token: 0x04002200 RID: 8704
	public float StartDelay;

	// Token: 0x04002201 RID: 8705
	public AnimationCurve FrameOverTime = AnimationCurve.Linear(0f, 1f, 1f, 1f);

	// Token: 0x04002202 RID: 8706
	private bool isInizialised;

	// Token: 0x04002203 RID: 8707
	private int index;

	// Token: 0x04002204 RID: 8708
	private int count;

	// Token: 0x04002205 RID: 8709
	private int allCount;

	// Token: 0x04002206 RID: 8710
	private float animationLifeTime;

	// Token: 0x04002207 RID: 8711
	private bool isVisible;

	// Token: 0x04002208 RID: 8712
	private bool isCorutineStarted;

	// Token: 0x04002209 RID: 8713
	private Renderer currentRenderer;

	// Token: 0x0400220A RID: 8714
	private Material instanceMaterial;

	// Token: 0x0400220B RID: 8715
	private float currentInterpolatedTime;

	// Token: 0x0400220C RID: 8716
	private float animationStartTime;

	// Token: 0x0400220D RID: 8717
	private bool animationStoped;
}
