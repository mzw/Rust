﻿using System;
using UnityEngine;

// Token: 0x02000475 RID: 1141
public class RealmedCollider : global::BasePrefab
{
	// Token: 0x060018E8 RID: 6376 RVA: 0x0008C2F8 File Offset: 0x0008A4F8
	public override void PreProcess(global::IPrefabProcessor process, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		base.PreProcess(process, rootObj, name, serverside, clientside, bundling);
		if (this.ServerCollider != this.ClientCollider)
		{
			if (clientside)
			{
				if (this.ServerCollider)
				{
					process.RemoveComponent(this.ServerCollider);
					this.ServerCollider = null;
				}
			}
			else if (this.ClientCollider)
			{
				process.RemoveComponent(this.ClientCollider);
				this.ClientCollider = null;
			}
		}
		process.RemoveComponent(this);
	}

	// Token: 0x0400139A RID: 5018
	public Collider ServerCollider;

	// Token: 0x0400139B RID: 5019
	public Collider ClientCollider;
}
