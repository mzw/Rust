﻿using System;
using UnityEngine;

namespace Smaa
{
	// Token: 0x02000817 RID: 2071
	[Serializable]
	public class Preset
	{
		// Token: 0x040022DB RID: 8923
		public bool DiagDetection = true;

		// Token: 0x040022DC RID: 8924
		public bool CornerDetection = true;

		// Token: 0x040022DD RID: 8925
		[Range(0f, 0.5f)]
		public float Threshold = 0.1f;

		// Token: 0x040022DE RID: 8926
		[Min(0.0001f)]
		public float DepthThreshold = 0.01f;

		// Token: 0x040022DF RID: 8927
		[Range(0f, 112f)]
		public int MaxSearchSteps = 16;

		// Token: 0x040022E0 RID: 8928
		[Range(0f, 20f)]
		public int MaxSearchStepsDiag = 8;

		// Token: 0x040022E1 RID: 8929
		[Range(0f, 100f)]
		public int CornerRounding = 25;

		// Token: 0x040022E2 RID: 8930
		[Min(0f)]
		public float LocalContrastAdaptationFactor = 2f;
	}
}
