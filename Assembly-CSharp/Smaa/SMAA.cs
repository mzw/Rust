﻿using System;
using UnityEngine;

namespace Smaa
{
	// Token: 0x0200081B RID: 2075
	[AddComponentMenu("Image Effects/Subpixel Morphological Antialiasing")]
	[RequireComponent(typeof(Camera))]
	[ExecuteInEditMode]
	public class SMAA : MonoBehaviour
	{
		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x0600268A RID: 9866 RVA: 0x000D5A54 File Offset: 0x000D3C54
		public Material Material
		{
			get
			{
				if (this.m_Material == null)
				{
					this.m_Material = new Material(this.Shader);
					this.m_Material.hideFlags = 61;
				}
				return this.m_Material;
			}
		}

		// Token: 0x040022F1 RID: 8945
		public DebugPass DebugPass;

		// Token: 0x040022F2 RID: 8946
		public QualityPreset Quality = QualityPreset.High;

		// Token: 0x040022F3 RID: 8947
		public EdgeDetectionMethod DetectionMethod = EdgeDetectionMethod.Luma;

		// Token: 0x040022F4 RID: 8948
		public bool UsePredication;

		// Token: 0x040022F5 RID: 8949
		public Preset CustomPreset;

		// Token: 0x040022F6 RID: 8950
		public PredicationPreset CustomPredicationPreset;

		// Token: 0x040022F7 RID: 8951
		public Shader Shader;

		// Token: 0x040022F8 RID: 8952
		public Texture2D AreaTex;

		// Token: 0x040022F9 RID: 8953
		public Texture2D SearchTex;

		// Token: 0x040022FA RID: 8954
		protected Camera m_Camera;

		// Token: 0x040022FB RID: 8955
		protected Preset m_LowPreset;

		// Token: 0x040022FC RID: 8956
		protected Preset m_MediumPreset;

		// Token: 0x040022FD RID: 8957
		protected Preset m_HighPreset;

		// Token: 0x040022FE RID: 8958
		protected Preset m_UltraPreset;

		// Token: 0x040022FF RID: 8959
		protected Material m_Material;
	}
}
