﻿using System;

namespace Smaa
{
	// Token: 0x02000819 RID: 2073
	public enum QualityPreset
	{
		// Token: 0x040022E8 RID: 8936
		Low,
		// Token: 0x040022E9 RID: 8937
		Medium,
		// Token: 0x040022EA RID: 8938
		High,
		// Token: 0x040022EB RID: 8939
		Ultra,
		// Token: 0x040022EC RID: 8940
		Custom
	}
}
