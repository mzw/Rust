﻿using System;

namespace Smaa
{
	// Token: 0x0200081A RID: 2074
	public enum DebugPass
	{
		// Token: 0x040022EE RID: 8942
		Off,
		// Token: 0x040022EF RID: 8943
		Edges,
		// Token: 0x040022F0 RID: 8944
		Weights
	}
}
