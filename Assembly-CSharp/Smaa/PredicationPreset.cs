﻿using System;
using UnityEngine;

namespace Smaa
{
	// Token: 0x02000816 RID: 2070
	[Serializable]
	public class PredicationPreset
	{
		// Token: 0x040022D8 RID: 8920
		[Min(0.0001f)]
		public float Threshold = 0.01f;

		// Token: 0x040022D9 RID: 8921
		[Range(1f, 5f)]
		public float Scale = 2f;

		// Token: 0x040022DA RID: 8922
		[Range(0f, 1f)]
		public float Strength = 0.4f;
	}
}
