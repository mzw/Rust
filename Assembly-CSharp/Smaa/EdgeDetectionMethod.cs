﻿using System;

namespace Smaa
{
	// Token: 0x02000818 RID: 2072
	public enum EdgeDetectionMethod
	{
		// Token: 0x040022E4 RID: 8932
		Luma = 1,
		// Token: 0x040022E5 RID: 8933
		Color,
		// Token: 0x040022E6 RID: 8934
		Depth
	}
}
