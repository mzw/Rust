﻿using System;
using UnityEngine;

namespace Smaa
{
	// Token: 0x02000815 RID: 2069
	public sealed class MinAttribute : PropertyAttribute
	{
		// Token: 0x06002686 RID: 9862 RVA: 0x000D59A4 File Offset: 0x000D3BA4
		public MinAttribute(float min)
		{
			this.min = min;
		}

		// Token: 0x040022D7 RID: 8919
		public readonly float min;
	}
}
