﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core;
using Rust;
using UnityEngine;

// Token: 0x02000373 RID: 883
public class ResourceDispenser : global::EntityComponent<global::BaseEntity>, IServerComponent
{
	// Token: 0x060014FC RID: 5372 RVA: 0x0007946C File Offset: 0x0007766C
	public void Start()
	{
		this.Initialize();
	}

	// Token: 0x060014FD RID: 5373 RVA: 0x00079474 File Offset: 0x00077674
	public void Initialize()
	{
		this.UpdateFraction();
		this.UpdateRemainingCategories();
		this.CountAllItems();
	}

	// Token: 0x060014FE RID: 5374 RVA: 0x00079488 File Offset: 0x00077688
	public void DoGather(global::HitInfo info)
	{
		if (!base.baseEntity.isServer)
		{
			return;
		}
		if (!info.CanGather || info.DidGather)
		{
			return;
		}
		if (this.gatherType == global::ResourceDispenser.GatherType.UNSET)
		{
			Debug.LogWarning("Object :" + base.gameObject.name + ": has unset gathertype!");
			return;
		}
		global::BaseMelee baseMelee = (!(info.Weapon == null)) ? info.Weapon.GetComponent<global::BaseMelee>() : null;
		float num;
		float num2;
		if (baseMelee != null)
		{
			global::ResourceDispenser.GatherPropertyEntry gatherInfoFromIndex = baseMelee.GetGatherInfoFromIndex(this.gatherType);
			num = gatherInfoFromIndex.gatherDamage * info.gatherScale;
			num2 = gatherInfoFromIndex.destroyFraction;
			if (num == 0f)
			{
				return;
			}
			baseMelee.SendPunch(new Vector3(Random.Range(0.5f, 1f), Random.Range(-0.25f, -0.5f), 0f) * -30f * (gatherInfoFromIndex.conditionLost / 6f), 0.05f);
			baseMelee.LoseCondition(gatherInfoFromIndex.conditionLost);
			if (!baseMelee.IsValid() || baseMelee.IsBroken())
			{
				return;
			}
			info.DidGather = true;
		}
		else
		{
			float num3 = info.damageTypes.Total();
			num = num3;
			num2 = 0.5f;
		}
		float num4 = this.fractionRemaining;
		this.GiveResources(info.Initiator, num, num2, info.Weapon);
		this.UpdateFraction();
		float damageAmount;
		if (this.fractionRemaining <= 0f)
		{
			damageAmount = base.baseEntity.MaxHealth();
			if (info.DidGather && num2 < this.maxDestroyFractionForFinishBonus)
			{
				this.AssignFinishBonus(info.InitiatorPlayer, 1f - num2);
			}
		}
		else
		{
			damageAmount = (num4 - this.fractionRemaining) * base.baseEntity.MaxHealth();
		}
		global::HitInfo hitInfo = new global::HitInfo(info.Initiator, base.baseEntity, Rust.DamageType.Generic, damageAmount, base.transform.position);
		hitInfo.gatherScale = 0f;
		hitInfo.PointStart = info.PointStart;
		hitInfo.PointEnd = info.PointEnd;
		base.baseEntity.OnAttacked(hitInfo);
	}

	// Token: 0x060014FF RID: 5375 RVA: 0x000796C8 File Offset: 0x000778C8
	public void AssignFinishBonus(global::BasePlayer player, float fraction)
	{
		base.SendMessage("FinishBonusAssigned", 1);
		if (fraction <= 0f)
		{
			return;
		}
		if (this.finishBonus != null)
		{
			foreach (global::ItemAmount itemAmount in this.finishBonus)
			{
				global::Item item = global::ItemManager.Create(itemAmount.itemDef, Mathf.CeilToInt((float)((int)itemAmount.amount) * Mathf.Clamp01(fraction)), 0UL);
				if (item != null)
				{
					object obj = Interface.CallHook("OnDispenserBonus", new object[]
					{
						this,
						player,
						item
					});
					if (obj is global::Item)
					{
						item = (global::Item)obj;
					}
					player.GiveItem(item, global::BaseEntity.GiveItemReason.ResourceHarvested);
				}
			}
		}
	}

	// Token: 0x06001500 RID: 5376 RVA: 0x000797A4 File Offset: 0x000779A4
	public void OnAttacked(global::HitInfo info)
	{
		this.DoGather(info);
	}

	// Token: 0x06001501 RID: 5377 RVA: 0x000797B0 File Offset: 0x000779B0
	private void GiveResources(global::BaseEntity entity, float gatherDamage, float destroyFraction, global::AttackEntity attackWeapon)
	{
		if (!entity.IsValid())
		{
			return;
		}
		if (gatherDamage <= 0f)
		{
			return;
		}
		global::ItemAmount itemAmount = null;
		int i = this.containedItems.Count;
		int num = Random.Range(0, this.containedItems.Count);
		while (i > 0)
		{
			if (num >= this.containedItems.Count)
			{
				num = 0;
			}
			if (this.containedItems[num].amount > 0f)
			{
				itemAmount = this.containedItems[num];
				break;
			}
			num++;
			i--;
		}
		if (itemAmount == null)
		{
			return;
		}
		this.GiveResourceFromItem(entity, itemAmount, gatherDamage, destroyFraction, attackWeapon);
		this.UpdateVars();
		global::BasePlayer basePlayer = entity.ToPlayer();
		if (basePlayer)
		{
			global::Item item = attackWeapon.GetItem();
			Debug.Assert(item != null, "Attack Weapon " + attackWeapon + " has no Item");
			Debug.Assert(attackWeapon.ownerItemUID != 0u, "Attack Weapon " + attackWeapon + " ownerItemUID is 0");
			Debug.Assert(attackWeapon.GetParentEntity() != null, "Attack Weapon " + attackWeapon + " GetParentEntity is null");
			Debug.Assert(attackWeapon.GetParentEntity().IsValid(), "Attack Weapon " + attackWeapon + " GetParentEntity is not valid");
			Debug.Assert(attackWeapon.GetParentEntity().ToPlayer() != null, "Attack Weapon " + attackWeapon + " GetParentEntity is not a player");
			Debug.Assert(!attackWeapon.GetParentEntity().ToPlayer().IsDead(), "Attack Weapon " + attackWeapon + " GetParentEntity is not valid");
			global::BasePlayer ownerPlayer = attackWeapon.GetOwnerPlayer();
			Debug.Assert(ownerPlayer != null, "Attack Weapon " + attackWeapon + " ownerPlayer is null");
			Debug.Assert(ownerPlayer == basePlayer, "Attack Weapon " + attackWeapon + " ownerPlayer is not player");
			if (ownerPlayer != null)
			{
				Debug.Assert(ownerPlayer.inventory != null, "Attack Weapon " + attackWeapon + " ownerPlayer inventory is null");
				Debug.Assert(ownerPlayer.inventory.FindItemUID(attackWeapon.ownerItemUID) != null, "Attack Weapon " + attackWeapon + " FindItemUID is null");
			}
		}
	}

	// Token: 0x06001502 RID: 5378 RVA: 0x000799FC File Offset: 0x00077BFC
	public void DestroyFraction(float fraction)
	{
		foreach (global::ItemAmount itemAmount in this.containedItems)
		{
			if (itemAmount.amount > 0f)
			{
				itemAmount.amount -= fraction / this.categoriesRemaining;
			}
		}
		this.UpdateVars();
	}

	// Token: 0x06001503 RID: 5379 RVA: 0x00079A7C File Offset: 0x00077C7C
	private void GiveResourceFromItem(global::BaseEntity entity, global::ItemAmount itemAmt, float gatherDamage, float destroyFraction, global::AttackEntity attackWeapon)
	{
		if (itemAmt.amount == 0f)
		{
			return;
		}
		float num = Mathf.Min(gatherDamage, base.baseEntity.Health()) / base.baseEntity.MaxHealth();
		float num2 = itemAmt.startAmount / this.startingItemCounts;
		float num3 = itemAmt.startAmount * num / num2;
		float num4 = Mathf.Clamp(num3, 0f, itemAmt.amount);
		float num5 = num4 * destroyFraction * 2f;
		if (itemAmt.amount <= num4 + num5)
		{
			float num6 = (num4 + num5) / itemAmt.amount;
			num4 /= num6;
			num5 /= num6;
		}
		itemAmt.amount -= Mathf.Floor(num4);
		itemAmt.amount -= Mathf.Floor(num5);
		if (num4 < 1f)
		{
			num4 = ((Random.Range(0f, 1f) > num4) ? 0f : 1f);
			itemAmt.amount = 0f;
		}
		if (itemAmt.amount < 0f)
		{
			itemAmt.amount = 0f;
		}
		if (num4 >= 1f)
		{
			global::Item item = global::ItemManager.CreateByItemID(itemAmt.itemid, Mathf.FloorToInt(num4), 0UL);
			if (item == null)
			{
				return;
			}
			Interface.CallHook("OnDispenserGather", new object[]
			{
				this,
				entity,
				item
			});
			this.OverrideOwnership(item, attackWeapon);
			entity.GiveItem(item, global::BaseEntity.GiveItemReason.ResourceHarvested);
		}
	}

	// Token: 0x06001504 RID: 5380 RVA: 0x00079BF8 File Offset: 0x00077DF8
	public virtual bool OverrideOwnership(global::Item item, global::AttackEntity weapon)
	{
		return false;
	}

	// Token: 0x06001505 RID: 5381 RVA: 0x00079BFC File Offset: 0x00077DFC
	private void UpdateVars()
	{
		this.UpdateFraction();
		this.UpdateRemainingCategories();
	}

	// Token: 0x06001506 RID: 5382 RVA: 0x00079C0C File Offset: 0x00077E0C
	public void UpdateRemainingCategories()
	{
		int num = 0;
		foreach (global::ItemAmount itemAmount in this.containedItems)
		{
			if (itemAmount.amount > 0f)
			{
				num++;
			}
		}
		this.categoriesRemaining = (float)num;
	}

	// Token: 0x06001507 RID: 5383 RVA: 0x00079C80 File Offset: 0x00077E80
	public void CountAllItems()
	{
		this.startingItemCounts = this.containedItems.Sum((global::ItemAmount x) => x.startAmount);
	}

	// Token: 0x06001508 RID: 5384 RVA: 0x00079CB0 File Offset: 0x00077EB0
	private void UpdateFraction()
	{
		float num = this.containedItems.Sum((global::ItemAmount x) => x.startAmount);
		float num2 = this.containedItems.Sum((global::ItemAmount x) => x.amount);
		if (num == 0f)
		{
			this.fractionRemaining = 0f;
			return;
		}
		this.fractionRemaining = num2 / num;
	}

	// Token: 0x04000F76 RID: 3958
	public global::ResourceDispenser.GatherType gatherType = global::ResourceDispenser.GatherType.UNSET;

	// Token: 0x04000F77 RID: 3959
	public List<global::ItemAmount> containedItems;

	// Token: 0x04000F78 RID: 3960
	public float maxDestroyFractionForFinishBonus = 0.2f;

	// Token: 0x04000F79 RID: 3961
	public List<global::ItemAmount> finishBonus;

	// Token: 0x04000F7A RID: 3962
	public float fractionRemaining = 1f;

	// Token: 0x04000F7B RID: 3963
	private float categoriesRemaining;

	// Token: 0x04000F7C RID: 3964
	private float startingItemCounts;

	// Token: 0x02000374 RID: 884
	public enum GatherType
	{
		// Token: 0x04000F81 RID: 3969
		Tree,
		// Token: 0x04000F82 RID: 3970
		Ore,
		// Token: 0x04000F83 RID: 3971
		Flesh,
		// Token: 0x04000F84 RID: 3972
		UNSET,
		// Token: 0x04000F85 RID: 3973
		LAST
	}

	// Token: 0x02000375 RID: 885
	[Serializable]
	public class GatherPropertyEntry
	{
		// Token: 0x04000F86 RID: 3974
		public float gatherDamage;

		// Token: 0x04000F87 RID: 3975
		public float destroyFraction;

		// Token: 0x04000F88 RID: 3976
		public float conditionLost;
	}

	// Token: 0x02000376 RID: 886
	[Serializable]
	public class GatherProperties
	{
		// Token: 0x0600150E RID: 5390 RVA: 0x00079D58 File Offset: 0x00077F58
		public float GetProficiency()
		{
			float num = 0f;
			for (int i = 0; i < 3; i++)
			{
				global::ResourceDispenser.GatherPropertyEntry fromIndex = this.GetFromIndex(i);
				float num2 = fromIndex.gatherDamage * fromIndex.destroyFraction;
				if (num2 > 0f)
				{
					num += fromIndex.gatherDamage / num2;
				}
			}
			return num;
		}

		// Token: 0x0600150F RID: 5391 RVA: 0x00079DAC File Offset: 0x00077FAC
		public bool Any()
		{
			for (int i = 0; i < 3; i++)
			{
				global::ResourceDispenser.GatherPropertyEntry fromIndex = this.GetFromIndex(i);
				if (fromIndex.gatherDamage > 0f || fromIndex.conditionLost > 0f)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001510 RID: 5392 RVA: 0x00079DF8 File Offset: 0x00077FF8
		public global::ResourceDispenser.GatherPropertyEntry GetFromIndex(int index)
		{
			return this.GetFromIndex((global::ResourceDispenser.GatherType)index);
		}

		// Token: 0x06001511 RID: 5393 RVA: 0x00079E04 File Offset: 0x00078004
		public global::ResourceDispenser.GatherPropertyEntry GetFromIndex(global::ResourceDispenser.GatherType index)
		{
			switch (index)
			{
			case global::ResourceDispenser.GatherType.Tree:
				return this.Tree;
			case global::ResourceDispenser.GatherType.Ore:
				return this.Ore;
			case global::ResourceDispenser.GatherType.Flesh:
				return this.Flesh;
			default:
				return null;
			}
		}

		// Token: 0x04000F89 RID: 3977
		public global::ResourceDispenser.GatherPropertyEntry Tree;

		// Token: 0x04000F8A RID: 3978
		public global::ResourceDispenser.GatherPropertyEntry Ore;

		// Token: 0x04000F8B RID: 3979
		public global::ResourceDispenser.GatherPropertyEntry Flesh;
	}
}
