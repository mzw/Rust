﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Token: 0x020003B5 RID: 949
public class ProjectileWeaponMod : global::BaseEntity
{
	// Token: 0x0600167A RID: 5754 RVA: 0x0008214C File Offset: 0x0008034C
	public override void ServerInit()
	{
		base.SetFlag(global::BaseEntity.Flags.Disabled, true, false);
		base.ServerInit();
	}

	// Token: 0x0600167B RID: 5755 RVA: 0x00082160 File Offset: 0x00080360
	public override void PostServerLoad()
	{
		base.limitNetworking = base.HasFlag(global::BaseEntity.Flags.Disabled);
	}

	// Token: 0x0600167C RID: 5756 RVA: 0x00082170 File Offset: 0x00080370
	public static float Sum(global::BaseEntity parentEnt, Func<global::ProjectileWeaponMod, global::ProjectileWeaponMod.Modifier> selector_modifier, Func<global::ProjectileWeaponMod.Modifier, float> selector_value, float def)
	{
		if (parentEnt.children == null)
		{
			return def;
		}
		IEnumerable<float> mods = global::ProjectileWeaponMod.GetMods(parentEnt, selector_modifier, selector_value);
		if (mods.Count<float>() != 0)
		{
			return mods.Sum();
		}
		return def;
	}

	// Token: 0x0600167D RID: 5757 RVA: 0x000821A8 File Offset: 0x000803A8
	public static float Average(global::BaseEntity parentEnt, Func<global::ProjectileWeaponMod, global::ProjectileWeaponMod.Modifier> selector_modifier, Func<global::ProjectileWeaponMod.Modifier, float> selector_value, float def)
	{
		if (parentEnt.children == null)
		{
			return def;
		}
		IEnumerable<float> mods = global::ProjectileWeaponMod.GetMods(parentEnt, selector_modifier, selector_value);
		if (mods.Count<float>() != 0)
		{
			return mods.Average();
		}
		return def;
	}

	// Token: 0x0600167E RID: 5758 RVA: 0x000821E0 File Offset: 0x000803E0
	public static float Max(global::BaseEntity parentEnt, Func<global::ProjectileWeaponMod, global::ProjectileWeaponMod.Modifier> selector_modifier, Func<global::ProjectileWeaponMod.Modifier, float> selector_value, float def)
	{
		if (parentEnt.children == null)
		{
			return def;
		}
		IEnumerable<float> mods = global::ProjectileWeaponMod.GetMods(parentEnt, selector_modifier, selector_value);
		if (mods.Count<float>() != 0)
		{
			return mods.Max();
		}
		return def;
	}

	// Token: 0x0600167F RID: 5759 RVA: 0x00082218 File Offset: 0x00080418
	public static float Min(global::BaseEntity parentEnt, Func<global::ProjectileWeaponMod, global::ProjectileWeaponMod.Modifier> selector_modifier, Func<global::ProjectileWeaponMod.Modifier, float> selector_value, float def)
	{
		if (parentEnt.children == null)
		{
			return def;
		}
		IEnumerable<float> mods = global::ProjectileWeaponMod.GetMods(parentEnt, selector_modifier, selector_value);
		if (mods.Count<float>() != 0)
		{
			return mods.Min();
		}
		return def;
	}

	// Token: 0x06001680 RID: 5760 RVA: 0x00082250 File Offset: 0x00080450
	public static IEnumerable<float> GetMods(global::BaseEntity parentEnt, Func<global::ProjectileWeaponMod, global::ProjectileWeaponMod.Modifier> selector_modifier, Func<global::ProjectileWeaponMod.Modifier, float> selector_value)
	{
		return (from x in (from global::ProjectileWeaponMod x in parentEnt.children
		where x != null && (!x.needsOnForEffects || x.HasFlag(global::BaseEntity.Flags.On))
		select x).Select(selector_modifier)
		where x.enabled
		select x).Select(selector_value);
	}

	// Token: 0x06001681 RID: 5761 RVA: 0x000822B8 File Offset: 0x000804B8
	public static bool HasBrokenWeaponMod(global::BaseEntity parentEnt)
	{
		if (parentEnt.children == null)
		{
			return false;
		}
		return parentEnt.children.Cast<global::ProjectileWeaponMod>().Any((global::ProjectileWeaponMod x) => x != null && x.IsBroken());
	}

	// Token: 0x040010E6 RID: 4326
	[Header("Silencer")]
	public global::GameObjectRef defaultSilencerEffect;

	// Token: 0x040010E7 RID: 4327
	public bool isSilencer;

	// Token: 0x040010E8 RID: 4328
	[Header("Weapon Basics")]
	public global::ProjectileWeaponMod.Modifier repeatDelay;

	// Token: 0x040010E9 RID: 4329
	public global::ProjectileWeaponMod.Modifier projectileVelocity;

	// Token: 0x040010EA RID: 4330
	public global::ProjectileWeaponMod.Modifier projectileDamage;

	// Token: 0x040010EB RID: 4331
	public global::ProjectileWeaponMod.Modifier projectileDistance;

	// Token: 0x040010EC RID: 4332
	[Header("Recoil")]
	public global::ProjectileWeaponMod.Modifier aimsway;

	// Token: 0x040010ED RID: 4333
	public global::ProjectileWeaponMod.Modifier aimswaySpeed;

	// Token: 0x040010EE RID: 4334
	public global::ProjectileWeaponMod.Modifier recoil;

	// Token: 0x040010EF RID: 4335
	[Header("Aim Cone")]
	public global::ProjectileWeaponMod.Modifier sightAimCone;

	// Token: 0x040010F0 RID: 4336
	public global::ProjectileWeaponMod.Modifier hipAimCone;

	// Token: 0x040010F1 RID: 4337
	[Header("Light Effects")]
	public bool isLight;

	// Token: 0x040010F2 RID: 4338
	[Header("MuzzleBrake")]
	public bool isMuzzleBrake;

	// Token: 0x040010F3 RID: 4339
	[Header("MuzzleBoost")]
	public bool isMuzzleBoost;

	// Token: 0x040010F4 RID: 4340
	[Header("Scope")]
	public bool isScope;

	// Token: 0x040010F5 RID: 4341
	public float zoomAmountDisplayOnly;

	// Token: 0x040010F6 RID: 4342
	public bool needsOnForEffects;

	// Token: 0x020003B6 RID: 950
	[Serializable]
	public struct Modifier
	{
		// Token: 0x040010FA RID: 4346
		public bool enabled;

		// Token: 0x040010FB RID: 4347
		[Tooltip("1 means no change. 0.5 is half.")]
		public float scalar;

		// Token: 0x040010FC RID: 4348
		[Tooltip("Added after the scalar is applied.")]
		public float offset;
	}
}
