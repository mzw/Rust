﻿using System;
using System.Collections.Generic;
using Facepunch;
using Network;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x0200002C RID: 44
public class BaseHelicopter : global::BaseCombatEntity
{
	// Token: 0x06000422 RID: 1058 RVA: 0x00016A28 File Offset: 0x00014C28
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseHelicopter.OnRpcMessage", 0.1f))
		{
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000423 RID: 1059 RVA: 0x00016A70 File Offset: 0x00014C70
	public override float MaxVelocity()
	{
		return 100f;
	}

	// Token: 0x06000424 RID: 1060 RVA: 0x00016A78 File Offset: 0x00014C78
	public override void InitShared()
	{
		base.InitShared();
		this.InitalizeWeakspots();
	}

	// Token: 0x06000425 RID: 1061 RVA: 0x00016A88 File Offset: 0x00014C88
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.helicopter != null)
		{
			this.spotlightTarget = info.msg.helicopter.spotlightVec;
		}
	}

	// Token: 0x06000426 RID: 1062 RVA: 0x00016ABC File Offset: 0x00014CBC
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.helicopter = Pool.Get<Helicopter>();
		info.msg.helicopter.tiltRot = this.rotorPivot.transform.localRotation.eulerAngles;
		info.msg.helicopter.spotlightVec = this.spotlightTarget;
		info.msg.helicopter.weakspothealths = Pool.Get<List<float>>();
		for (int i = 0; i < this.weakspots.Length; i++)
		{
			info.msg.helicopter.weakspothealths.Add(this.weakspots[i].health);
		}
	}

	// Token: 0x06000427 RID: 1063 RVA: 0x00016B74 File Offset: 0x00014D74
	public override void ServerInit()
	{
		base.ServerInit();
		this.myAI = base.GetComponent<global::PatrolHelicopterAI>();
		if (!this.myAI.hasInterestZone)
		{
			this.myAI.SetInitialDestination(Vector3.zero, 1.25f);
			this.myAI.targetThrottleSpeed = 1f;
			this.myAI.ExitCurrentState();
			this.myAI.State_Patrol_Enter();
		}
	}

	// Token: 0x06000428 RID: 1064 RVA: 0x00016BE0 File Offset: 0x00014DE0
	public override void OnPositionalNetworkUpdate()
	{
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		base.OnPositionalNetworkUpdate();
	}

	// Token: 0x06000429 RID: 1065 RVA: 0x00016BF0 File Offset: 0x00014DF0
	public void CreateExplosionMarker(float durationMinutes)
	{
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.debrisFieldMarker.resourcePath, base.transform.position, Quaternion.identity, true);
		baseEntity.Spawn();
		baseEntity.SendMessage("SetDuration", durationMinutes, 1);
	}

	// Token: 0x0600042A RID: 1066 RVA: 0x00016C3C File Offset: 0x00014E3C
	public override void OnKilled(global::HitInfo info)
	{
		if (base.isClient)
		{
			return;
		}
		this.CreateExplosionMarker(10f);
		global::Effect.server.Run(this.explosionEffect.resourcePath, base.transform.position, Vector3.up, null, true);
		Vector3 vector = this.myAI.GetLastMoveDir() * this.myAI.GetMoveSpeed() * 0.75f;
		GameObject gibSource = this.servergibs.Get().GetComponent<global::ServerGib>()._gibSource;
		List<global::ServerGib> list = global::ServerGib.CreateGibs(this.servergibs.resourcePath, base.gameObject, gibSource, vector, 3f);
		for (int i = 0; i < 12 - this.maxCratesToSpawn; i++)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.fireBall.resourcePath, base.transform.position, base.transform.rotation, true);
			if (baseEntity)
			{
				float num = 3f;
				float num2 = 10f;
				Vector3 onUnitSphere = Random.onUnitSphere;
				baseEntity.transform.position = base.transform.position + new Vector3(0f, 1.5f, 0f) + onUnitSphere * Random.Range(-4f, 4f);
				Collider component = baseEntity.GetComponent<Collider>();
				baseEntity.Spawn();
				baseEntity.SetVelocity(vector + onUnitSphere * Random.Range(num, num2));
				foreach (global::ServerGib serverGib in list)
				{
					Physics.IgnoreCollision(component, serverGib.GetCollider(), true);
				}
			}
		}
		for (int j = 0; j < this.maxCratesToSpawn; j++)
		{
			Vector3 onUnitSphere2 = Random.onUnitSphere;
			Vector3 pos = base.transform.position + new Vector3(0f, 1.5f, 0f) + onUnitSphere2 * Random.Range(2f, 3f);
			global::BaseEntity baseEntity2 = global::GameManager.server.CreateEntity(this.crateToDrop.resourcePath, pos, Quaternion.LookRotation(onUnitSphere2), true);
			baseEntity2.Spawn();
			global::LootContainer lootContainer = baseEntity2 as global::LootContainer;
			if (lootContainer)
			{
				lootContainer.Invoke(new Action(lootContainer.RemoveMe), 1800f);
			}
			Collider component2 = baseEntity2.GetComponent<Collider>();
			Rigidbody rigidbody = baseEntity2.gameObject.AddComponent<Rigidbody>();
			rigidbody.useGravity = true;
			rigidbody.collisionDetectionMode = 2;
			rigidbody.mass = 2f;
			rigidbody.interpolation = 1;
			rigidbody.velocity = vector + onUnitSphere2 * Random.Range(1f, 3f);
			rigidbody.angularVelocity = Vector3Ex.Range(-1.75f, 1.75f);
			rigidbody.drag = 0.5f * (rigidbody.mass / 5f);
			rigidbody.angularDrag = 0.2f * (rigidbody.mass / 5f);
			global::FireBall fireBall = global::GameManager.server.CreateEntity(this.fireBall.resourcePath, default(Vector3), default(Quaternion), true) as global::FireBall;
			if (fireBall)
			{
				fireBall.SetParent(baseEntity2, 0u);
				fireBall.Spawn();
				fireBall.GetComponent<Rigidbody>().isKinematic = true;
				fireBall.GetComponent<Collider>().enabled = false;
			}
			baseEntity2.SendMessage("SetLockingEnt", fireBall.gameObject, 1);
			foreach (global::ServerGib serverGib2 in list)
			{
				Physics.IgnoreCollision(component2, serverGib2.GetCollider(), true);
			}
		}
		base.OnKilled(info);
	}

	// Token: 0x0600042B RID: 1067 RVA: 0x00017048 File Offset: 0x00015248
	public void Update()
	{
		if (base.isServer && Time.realtimeSinceStartup - this.lastNetworkUpdate >= 0.25f)
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			this.lastNetworkUpdate = Time.realtimeSinceStartup;
		}
	}

	// Token: 0x0600042C RID: 1068 RVA: 0x00017080 File Offset: 0x00015280
	public void InitalizeWeakspots()
	{
		foreach (global::BaseHelicopter.weakspot weakspot in this.weakspots)
		{
			weakspot.body = this;
		}
	}

	// Token: 0x0600042D RID: 1069 RVA: 0x000170B4 File Offset: 0x000152B4
	public override void OnAttacked(global::HitInfo info)
	{
		base.OnAttacked(info);
		if (base.isServer)
		{
			this.myAI.WasAttacked(info);
		}
	}

	// Token: 0x0600042E RID: 1070 RVA: 0x000170D4 File Offset: 0x000152D4
	public override void Hurt(global::HitInfo info)
	{
		bool flag = false;
		if (info.damageTypes.Total() >= base.health)
		{
			base.health = 1000000f;
			this.myAI.CriticalDamage();
			flag = true;
		}
		base.Hurt(info);
		if (!flag)
		{
			foreach (global::BaseHelicopter.weakspot weakspot in this.weakspots)
			{
				foreach (string i2 in weakspot.bonenames)
				{
					if (info.HitBone == global::StringPool.Get(i2))
					{
						weakspot.Hurt(info.damageTypes.Total(), info);
						this.myAI.WeakspotDamaged(weakspot, info);
					}
				}
			}
		}
	}

	// Token: 0x040000EF RID: 239
	public GameObject rotorPivot;

	// Token: 0x040000F0 RID: 240
	public GameObject mainRotor;

	// Token: 0x040000F1 RID: 241
	public GameObject mainRotor_blades;

	// Token: 0x040000F2 RID: 242
	public GameObject mainRotor_blur;

	// Token: 0x040000F3 RID: 243
	public GameObject tailRotor;

	// Token: 0x040000F4 RID: 244
	public GameObject tailRotor_blades;

	// Token: 0x040000F5 RID: 245
	public GameObject tailRotor_blur;

	// Token: 0x040000F6 RID: 246
	public GameObject rocket_tube_left;

	// Token: 0x040000F7 RID: 247
	public GameObject rocket_tube_right;

	// Token: 0x040000F8 RID: 248
	public GameObject left_gun_yaw;

	// Token: 0x040000F9 RID: 249
	public GameObject left_gun_pitch;

	// Token: 0x040000FA RID: 250
	public GameObject left_gun_muzzle;

	// Token: 0x040000FB RID: 251
	public GameObject right_gun_yaw;

	// Token: 0x040000FC RID: 252
	public GameObject right_gun_pitch;

	// Token: 0x040000FD RID: 253
	public GameObject right_gun_muzzle;

	// Token: 0x040000FE RID: 254
	public GameObject spotlight_rotation;

	// Token: 0x040000FF RID: 255
	public global::GameObjectRef rocket_fire_effect;

	// Token: 0x04000100 RID: 256
	public global::GameObjectRef gun_fire_effect;

	// Token: 0x04000101 RID: 257
	public global::GameObjectRef bulletEffect;

	// Token: 0x04000102 RID: 258
	public global::GameObjectRef explosionEffect;

	// Token: 0x04000103 RID: 259
	public global::GameObjectRef fireBall;

	// Token: 0x04000104 RID: 260
	public global::GameObjectRef crateToDrop;

	// Token: 0x04000105 RID: 261
	public int maxCratesToSpawn = 4;

	// Token: 0x04000106 RID: 262
	public float bulletSpeed = 250f;

	// Token: 0x04000107 RID: 263
	public float bulletDamage = 20f;

	// Token: 0x04000108 RID: 264
	public global::GameObjectRef servergibs;

	// Token: 0x04000109 RID: 265
	public global::GameObjectRef debrisFieldMarker;

	// Token: 0x0400010A RID: 266
	public global::SoundDefinition rotorWashSoundDef;

	// Token: 0x0400010B RID: 267
	public global::SoundDefinition engineSoundDef;

	// Token: 0x0400010C RID: 268
	public global::SoundDefinition rotorSoundDef;

	// Token: 0x0400010D RID: 269
	private global::Sound _engineSound;

	// Token: 0x0400010E RID: 270
	private global::Sound _rotorSound;

	// Token: 0x0400010F RID: 271
	public float spotlightJitterAmount = 5f;

	// Token: 0x04000110 RID: 272
	public float spotlightJitterSpeed = 5f;

	// Token: 0x04000111 RID: 273
	public GameObject[] nightLights;

	// Token: 0x04000112 RID: 274
	public Vector3 spotlightTarget;

	// Token: 0x04000113 RID: 275
	public float engineSpeed = 1f;

	// Token: 0x04000114 RID: 276
	public float targetEngineSpeed = 1f;

	// Token: 0x04000115 RID: 277
	public float blur_rotationScale = 0.05f;

	// Token: 0x04000116 RID: 278
	public ParticleSystem[] _rotorWashParticles;

	// Token: 0x04000117 RID: 279
	private global::PatrolHelicopterAI myAI;

	// Token: 0x04000118 RID: 280
	private float lastNetworkUpdate = float.NegativeInfinity;

	// Token: 0x04000119 RID: 281
	private const float networkUpdateRate = 0.25f;

	// Token: 0x0400011A RID: 282
	public global::BaseHelicopter.weakspot[] weakspots;

	// Token: 0x0200002D RID: 45
	[Serializable]
	public class weakspot
	{
		// Token: 0x06000430 RID: 1072 RVA: 0x000171AC File Offset: 0x000153AC
		public float HealthFraction()
		{
			return this.health / this.maxHealth;
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x000171BC File Offset: 0x000153BC
		public void Hurt(float amount, global::HitInfo info)
		{
			if (this.isDestroyed)
			{
				return;
			}
			this.health -= amount;
			global::Effect.server.Run(this.damagedParticles.resourcePath, this.body, global::StringPool.Get(this.bonenames[Random.Range(0, this.bonenames.Length)]), Vector3.zero, Vector3.up, null, true);
			if (this.health <= 0f)
			{
				this.health = 0f;
				this.WeakspotDestroyed();
			}
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x00017240 File Offset: 0x00015440
		public void Heal(float amount)
		{
			this.health += amount;
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x00017250 File Offset: 0x00015450
		public void WeakspotDestroyed()
		{
			this.isDestroyed = true;
			global::Effect.server.Run(this.destroyedParticles.resourcePath, this.body, global::StringPool.Get(this.bonenames[Random.Range(0, this.bonenames.Length)]), Vector3.zero, Vector3.up, null, true);
			this.body.Hurt(this.body.MaxHealth() * this.healthFractionOnDestroyed, Rust.DamageType.Generic, null, false);
		}

		// Token: 0x0400011B RID: 283
		[NonSerialized]
		public global::BaseHelicopter body;

		// Token: 0x0400011C RID: 284
		public string[] bonenames;

		// Token: 0x0400011D RID: 285
		public float maxHealth;

		// Token: 0x0400011E RID: 286
		public float health;

		// Token: 0x0400011F RID: 287
		public float healthFractionOnDestroyed = 0.5f;

		// Token: 0x04000120 RID: 288
		public global::GameObjectRef destroyedParticles;

		// Token: 0x04000121 RID: 289
		public global::GameObjectRef damagedParticles;

		// Token: 0x04000122 RID: 290
		public GameObject damagedEffect;

		// Token: 0x04000123 RID: 291
		public GameObject destroyedEffect;

		// Token: 0x04000124 RID: 292
		public List<global::BasePlayer> attackers;

		// Token: 0x04000125 RID: 293
		private bool isDestroyed;
	}
}
