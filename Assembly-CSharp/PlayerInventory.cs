﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch;
using Facepunch.Steamworks;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000084 RID: 132
public class PlayerInventory : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x060008C2 RID: 2242 RVA: 0x00039D40 File Offset: 0x00037F40
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("PlayerInventory.OnRpcMessage", 0.1f))
		{
			if (rpc == 2116208967u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ItemCmd ");
				}
				using (TimeWarning.New("ItemCmd", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("ItemCmd", this.GetBaseEntity(), player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ItemCmd(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in ItemCmd");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 4191184484u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - MoveItem ");
				}
				using (TimeWarning.New("MoveItem", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("MoveItem", this.GetBaseEntity(), player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.MoveItem(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in MoveItem");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060008C3 RID: 2243 RVA: 0x0003A0AC File Offset: 0x000382AC
	protected void Initialize()
	{
		this.containerMain = new global::ItemContainer();
		this.containerMain.SetFlag(global::ItemContainer.Flag.IsPlayer, true);
		this.containerBelt = new global::ItemContainer();
		this.containerBelt.SetFlag(global::ItemContainer.Flag.IsPlayer, true);
		this.containerBelt.SetFlag(global::ItemContainer.Flag.Belt, true);
		this.containerWear = new global::ItemContainer();
		this.containerWear.SetFlag(global::ItemContainer.Flag.IsPlayer, true);
		this.containerWear.SetFlag(global::ItemContainer.Flag.Clothing, true);
		this.crafting = base.GetComponent<global::ItemCrafter>();
		this.crafting.AddContainer(this.containerMain);
		this.crafting.AddContainer(this.containerBelt);
		this.loot = base.GetComponent<global::PlayerLoot>();
		if (!this.loot)
		{
			this.loot = base.gameObject.AddComponent<global::PlayerLoot>();
		}
	}

	// Token: 0x060008C4 RID: 2244 RVA: 0x0003A178 File Offset: 0x00038378
	public void DoDestroy()
	{
		if (this.containerMain != null)
		{
			this.containerMain.Kill();
			this.containerMain = null;
		}
		if (this.containerBelt != null)
		{
			this.containerBelt.Kill();
			this.containerBelt = null;
		}
		if (this.containerWear != null)
		{
			this.containerWear.Kill();
			this.containerWear = null;
		}
	}

	// Token: 0x060008C5 RID: 2245 RVA: 0x0003A1DC File Offset: 0x000383DC
	public void ServerInit(global::BasePlayer owner)
	{
		this.Initialize();
		this.containerMain.ServerInitialize(null, 24);
		if (this.containerMain.uid == 0u)
		{
			this.containerMain.GiveUID();
		}
		this.containerBelt.ServerInitialize(null, 6);
		if (this.containerBelt.uid == 0u)
		{
			this.containerBelt.GiveUID();
		}
		this.containerWear.ServerInitialize(null, 6);
		if (this.containerWear.uid == 0u)
		{
			this.containerWear.GiveUID();
		}
		this.containerMain.playerOwner = owner;
		this.containerBelt.playerOwner = owner;
		this.containerWear.playerOwner = owner;
		this.containerWear.onItemAddedRemoved = new Action<global::Item, bool>(this.OnClothingChanged);
		this.containerWear.canAcceptItem = new Func<global::Item, int, bool>(this.CanWearItem);
		this.containerBelt.canAcceptItem = new Func<global::Item, int, bool>(this.CanEquipItem);
		this.containerMain.onPreItemRemove = new Action<global::Item>(this.OnItemRemoved);
		this.containerWear.onPreItemRemove = new Action<global::Item>(this.OnItemRemoved);
		this.containerBelt.onPreItemRemove = new Action<global::Item>(this.OnItemRemoved);
		this.containerMain.onDirty += this.OnContentsDirty;
		this.containerBelt.onDirty += this.OnContentsDirty;
		this.containerWear.onDirty += this.OnContentsDirty;
		this.containerBelt.onItemAddedRemoved = new Action<global::Item, bool>(this.OnItemAddedOrRemoved);
		this.containerMain.onItemAddedRemoved = new Action<global::Item, bool>(this.OnItemAddedOrRemoved);
	}

	// Token: 0x060008C6 RID: 2246 RVA: 0x0003A38C File Offset: 0x0003858C
	public void OnItemAddedOrRemoved(global::Item item, bool bAdded)
	{
		if (item.info.isHoldable)
		{
			base.Invoke(new Action(this.UpdatedVisibleHolsteredItems), 0.1f);
		}
		if (!bAdded)
		{
			return;
		}
		global::BasePlayer baseEntity = base.baseEntity;
		if (!baseEntity.HasPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash) && baseEntity.IsHostileItem(item))
		{
			base.baseEntity.SetPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash, true);
		}
	}

	// Token: 0x060008C7 RID: 2247 RVA: 0x0003A3FC File Offset: 0x000385FC
	public void UpdatedVisibleHolsteredItems()
	{
		List<global::HeldEntity> list = Facepunch.Pool.GetList<global::HeldEntity>();
		foreach (global::Item item in this.AllItems())
		{
			if (item.info.isHoldable)
			{
				global::BaseEntity heldEntity = item.GetHeldEntity();
				if (!(heldEntity == null))
				{
					global::HeldEntity component = item.GetHeldEntity().GetComponent<global::HeldEntity>();
					if (!(component == null))
					{
						list.Add(component);
					}
				}
			}
		}
		IOrderedEnumerable<global::HeldEntity> orderedEnumerable = from x in list
		orderby x.hostileScore descending
		select x;
		bool flag = true;
		bool flag2 = true;
		bool flag3 = true;
		foreach (global::HeldEntity heldEntity2 in orderedEnumerable)
		{
			if (!(heldEntity2 == null))
			{
				if (heldEntity2.holsterInfo.displayWhenHolstered)
				{
					if (flag3 && !heldEntity2.IsDeployed() && heldEntity2.holsterInfo.slot == global::HeldEntity.HolsterInfo.HolsterSlot.BACK)
					{
						heldEntity2.SetVisibleWhileHolstered(true);
						flag3 = false;
					}
					else if (flag2 && !heldEntity2.IsDeployed() && heldEntity2.holsterInfo.slot == global::HeldEntity.HolsterInfo.HolsterSlot.RIGHT_THIGH)
					{
						heldEntity2.SetVisibleWhileHolstered(true);
						flag2 = false;
					}
					else if (flag && !heldEntity2.IsDeployed() && heldEntity2.holsterInfo.slot == global::HeldEntity.HolsterInfo.HolsterSlot.LEFT_THIGH)
					{
						heldEntity2.SetVisibleWhileHolstered(true);
						flag = false;
					}
					else
					{
						heldEntity2.SetVisibleWhileHolstered(false);
					}
				}
			}
		}
		Facepunch.Pool.FreeList<global::HeldEntity>(ref list);
	}

	// Token: 0x060008C8 RID: 2248 RVA: 0x0003A5CC File Offset: 0x000387CC
	private void OnContentsDirty()
	{
		if (base.baseEntity != null)
		{
			base.baseEntity.InvalidateNetworkCache();
		}
	}

	// Token: 0x060008C9 RID: 2249 RVA: 0x0003A5EC File Offset: 0x000387EC
	private bool CanMoveItemsFrom(global::BaseEntity entity, global::Item item)
	{
		global::StorageContainer storageContainer = entity as global::StorageContainer;
		return !storageContainer || storageContainer.CanMoveFrom(base.baseEntity, item);
	}

	// Token: 0x060008CA RID: 2250 RVA: 0x0003A620 File Offset: 0x00038820
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.FromOwner]
	private void ItemCmd(global::BaseEntity.RPCMessage msg)
	{
		uint id = msg.read.UInt32();
		string text = msg.read.String();
		global::Item item = this.FindItemUID(id);
		if (item == null)
		{
			return;
		}
		if (Interface.CallHook("OnItemAction", new object[]
		{
			item,
			text,
			msg.player
		}) != null)
		{
			return;
		}
		if (item.IsLocked())
		{
			return;
		}
		if (!this.CanMoveItemsFrom(item.parent.entityOwner, item))
		{
			return;
		}
		if (text == "drop")
		{
			int num = item.amount;
			if (msg.read.unread >= 4)
			{
				num = msg.read.Int32();
			}
			base.baseEntity.stats.Add("item_drop", 1, global::Stats.Steam);
			if (num < item.amount)
			{
				global::Item item2 = item.SplitItem(num);
				if (item2 != null)
				{
					item2.Drop(base.baseEntity.eyes.position, base.baseEntity.eyes.BodyForward() * 4f + Vector3Ex.Range(-0.5f, 0.5f), default(Quaternion));
				}
			}
			else
			{
				item.Drop(base.baseEntity.eyes.position, base.baseEntity.eyes.BodyForward() * 4f + Vector3Ex.Range(-0.5f, 0.5f), default(Quaternion));
			}
			base.baseEntity.SignalBroadcast(global::BaseEntity.Signal.Gesture, "drop_item", null);
			return;
		}
		item.ServerCommand(text, base.baseEntity);
		global::ItemManager.DoRemoves();
		this.ServerUpdate(0f);
	}

	// Token: 0x060008CB RID: 2251 RVA: 0x0003A7E4 File Offset: 0x000389E4
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.FromOwner]
	private void MoveItem(global::BaseEntity.RPCMessage msg)
	{
		uint id = msg.read.UInt32();
		uint num = msg.read.UInt32();
		int num2 = (int)msg.read.Int8();
		int num3 = (int)msg.read.UInt16();
		global::Item item = this.FindItemUID(id);
		if (item == null)
		{
			return;
		}
		if (Interface.CallHook("CanMoveItem", new object[]
		{
			item,
			this,
			num,
			num2,
			num3
		}) != null)
		{
			return;
		}
		if (!this.CanMoveItemsFrom(item.parent.entityOwner, item))
		{
			return;
		}
		if (num3 <= 0)
		{
			num3 = item.amount;
		}
		num3 = Mathf.Clamp(num3, 1, item.info.stackable);
		if (num == 0u)
		{
			this.GiveItem(item, null);
			return;
		}
		global::ItemContainer itemContainer = this.FindContainer(num);
		if (itemContainer == null)
		{
			return;
		}
		global::ItemContainer parent = item.parent;
		if ((parent != null && parent.IsLocked()) || itemContainer.IsLocked())
		{
			return;
		}
		if (itemContainer.PlayerItemInputBlocked())
		{
			return;
		}
		using (TimeWarning.New("Split", 0.1f))
		{
			if (item.amount > num3)
			{
				global::Item item2 = item.SplitItem(num3);
				if (!item2.MoveToContainer(itemContainer, num2, true))
				{
					item.amount += item2.amount;
					item2.Remove(0f);
				}
				global::ItemManager.DoRemoves();
				this.ServerUpdate(0f);
				return;
			}
		}
		if (!item.MoveToContainer(itemContainer, num2, true))
		{
			return;
		}
		global::ItemManager.DoRemoves();
		this.ServerUpdate(0f);
	}

	// Token: 0x060008CC RID: 2252 RVA: 0x0003A9C0 File Offset: 0x00038BC0
	private void OnClothingChanged(global::Item item, bool bAdded)
	{
		base.baseEntity.SV_ClothingChanged();
		global::ItemManager.DoRemoves();
		this.ServerUpdate(0f);
	}

	// Token: 0x060008CD RID: 2253 RVA: 0x0003A9E0 File Offset: 0x00038BE0
	private void OnItemRemoved(global::Item item)
	{
		base.baseEntity.InvalidateNetworkCache();
	}

	// Token: 0x060008CE RID: 2254 RVA: 0x0003A9F0 File Offset: 0x00038BF0
	private bool CanEquipItem(global::Item item, int targetSlot)
	{
		object obj = Interface.CallHook("CanEquipItem", new object[]
		{
			this,
			item,
			targetSlot
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		global::ItemModContainerRestriction component = item.info.GetComponent<global::ItemModContainerRestriction>();
		if (component == null)
		{
			return true;
		}
		foreach (global::Item item2 in this.containerBelt.itemList.ToArray())
		{
			if (item2 != item)
			{
				global::ItemModContainerRestriction component2 = item2.info.GetComponent<global::ItemModContainerRestriction>();
				if (!(component2 == null))
				{
					if (!component.CanExistWith(component2) && !item2.MoveToContainer(this.containerMain, -1, true))
					{
						item2.Drop(base.baseEntity.eyes.position, base.baseEntity.eyes.BodyForward() * 2f, default(Quaternion));
					}
				}
			}
		}
		return true;
	}

	// Token: 0x060008CF RID: 2255 RVA: 0x0003AB04 File Offset: 0x00038D04
	private bool CanWearItem(global::Item item, int targetSlot)
	{
		global::ItemModWearable component = item.info.GetComponent<global::ItemModWearable>();
		if (component == null)
		{
			return false;
		}
		object obj = Interface.CallHook("CanWearItem", new object[]
		{
			this,
			item,
			targetSlot
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		foreach (global::Item item2 in this.containerWear.itemList.ToArray())
		{
			if (item2 != item)
			{
				global::ItemModWearable component2 = item2.info.GetComponent<global::ItemModWearable>();
				if (!(component2 == null))
				{
					if (!component.CanExistWith(component2) && !item2.MoveToContainer(this.containerMain, -1, true))
					{
						item2.Drop(base.baseEntity.eyes.position, base.baseEntity.eyes.BodyForward() * 2f, default(Quaternion));
					}
				}
			}
		}
		return true;
	}

	// Token: 0x060008D0 RID: 2256 RVA: 0x0003AC18 File Offset: 0x00038E18
	public void ServerUpdate(float delta)
	{
		this.loot.Check();
		if (delta > 0f)
		{
			this.crafting.ServerUpdate(delta);
		}
		float currentTemperature = base.baseEntity.currentTemperature;
		this.UpdateContainer(delta, global::PlayerInventory.Type.Main, this.containerMain, false, currentTemperature);
		this.UpdateContainer(delta, global::PlayerInventory.Type.Belt, this.containerBelt, true, currentTemperature);
		this.UpdateContainer(delta, global::PlayerInventory.Type.Wear, this.containerWear, true, currentTemperature);
	}

	// Token: 0x060008D1 RID: 2257 RVA: 0x0003AC84 File Offset: 0x00038E84
	public void UpdateContainer(float delta, global::PlayerInventory.Type type, global::ItemContainer container, bool bSendInventoryToEveryone, float temperature)
	{
		if (container == null)
		{
			return;
		}
		container.temperature = temperature;
		if (delta > 0f)
		{
			container.OnCycle(delta);
		}
		if (container.dirty)
		{
			this.SendUpdatedInventory(type, container, bSendInventoryToEveryone);
			base.baseEntity.InvalidateNetworkCache();
		}
	}

	// Token: 0x060008D2 RID: 2258 RVA: 0x0003ACD4 File Offset: 0x00038ED4
	public void SendSnapshot()
	{
		using (TimeWarning.New("PlayerInventory.SendSnapshot", 0.1f))
		{
			this.SendUpdatedInventory(global::PlayerInventory.Type.Main, this.containerMain, false);
			this.SendUpdatedInventory(global::PlayerInventory.Type.Belt, this.containerBelt, true);
			this.SendUpdatedInventory(global::PlayerInventory.Type.Wear, this.containerWear, true);
		}
	}

	// Token: 0x060008D3 RID: 2259 RVA: 0x0003AD40 File Offset: 0x00038F40
	public void SendUpdatedInventory(global::PlayerInventory.Type type, global::ItemContainer container, bool bSendInventoryToEveryone = false)
	{
		using (UpdateItemContainer updateItemContainer = Facepunch.Pool.Get<UpdateItemContainer>())
		{
			updateItemContainer.type = (int)type;
			if (container != null)
			{
				container.dirty = false;
				updateItemContainer.container = Facepunch.Pool.Get<List<ProtoBuf.ItemContainer>>();
				updateItemContainer.container.Add(container.Save());
			}
			if (bSendInventoryToEveryone)
			{
				base.baseEntity.ClientRPC<UpdateItemContainer>(null, "UpdatedItemContainer", updateItemContainer);
			}
			else
			{
				base.baseEntity.ClientRPCPlayer<UpdateItemContainer>(null, base.baseEntity, "UpdatedItemContainer", updateItemContainer);
			}
		}
	}

	// Token: 0x060008D4 RID: 2260 RVA: 0x0003ADDC File Offset: 0x00038FDC
	public global::Item FindItemUID(uint id)
	{
		if (id == 0u)
		{
			return null;
		}
		if (this.containerMain != null)
		{
			global::Item item = this.containerMain.FindItemByUID(id);
			if (item != null && item.IsValid())
			{
				return item;
			}
		}
		if (this.containerBelt != null)
		{
			global::Item item2 = this.containerBelt.FindItemByUID(id);
			if (item2 != null && item2.IsValid())
			{
				return item2;
			}
		}
		if (this.containerWear != null)
		{
			global::Item item3 = this.containerWear.FindItemByUID(id);
			if (item3 != null && item3.IsValid())
			{
				return item3;
			}
		}
		return this.loot.FindItem(id);
	}

	// Token: 0x060008D5 RID: 2261 RVA: 0x0003AE80 File Offset: 0x00039080
	public global::Item FindItemID(string itemName)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(itemName);
		if (itemDefinition == null)
		{
			return null;
		}
		return this.FindItemID(itemDefinition.itemid);
	}

	// Token: 0x060008D6 RID: 2262 RVA: 0x0003AEB0 File Offset: 0x000390B0
	public global::Item FindItemID(int id)
	{
		if (this.containerMain != null)
		{
			global::Item item = this.containerMain.FindItemByItemID(id);
			if (item != null && item.IsValid())
			{
				return item;
			}
		}
		if (this.containerBelt != null)
		{
			global::Item item2 = this.containerBelt.FindItemByItemID(id);
			if (item2 != null && item2.IsValid())
			{
				return item2;
			}
		}
		if (this.containerWear != null)
		{
			global::Item item3 = this.containerWear.FindItemByItemID(id);
			if (item3 != null && item3.IsValid())
			{
				return item3;
			}
		}
		return null;
	}

	// Token: 0x060008D7 RID: 2263 RVA: 0x0003AF40 File Offset: 0x00039140
	public List<global::Item> FindItemIDs(int id)
	{
		List<global::Item> list = new List<global::Item>();
		if (this.containerMain != null)
		{
			list.AddRange(this.containerMain.FindItemsByItemID(id));
		}
		if (this.containerBelt != null)
		{
			list.AddRange(this.containerBelt.FindItemsByItemID(id));
		}
		if (this.containerWear != null)
		{
			list.AddRange(this.containerWear.FindItemsByItemID(id));
		}
		return list;
	}

	// Token: 0x060008D8 RID: 2264 RVA: 0x0003AFAC File Offset: 0x000391AC
	public global::ItemContainer FindContainer(uint id)
	{
		global::ItemContainer result;
		using (TimeWarning.New("FindContainer", 0.1f))
		{
			global::ItemContainer itemContainer = this.containerMain.FindContainer(id);
			if (itemContainer != null)
			{
				result = itemContainer;
			}
			else
			{
				itemContainer = this.containerBelt.FindContainer(id);
				if (itemContainer != null)
				{
					result = itemContainer;
				}
				else
				{
					itemContainer = this.containerWear.FindContainer(id);
					if (itemContainer != null)
					{
						result = itemContainer;
					}
					else
					{
						result = this.loot.FindContainer(id);
					}
				}
			}
		}
		return result;
	}

	// Token: 0x060008D9 RID: 2265 RVA: 0x0003B048 File Offset: 0x00039248
	public global::ItemContainer GetContainer(global::PlayerInventory.Type id)
	{
		if (id == global::PlayerInventory.Type.Main)
		{
			return this.containerMain;
		}
		if (id == global::PlayerInventory.Type.Belt)
		{
			return this.containerBelt;
		}
		if (id == global::PlayerInventory.Type.Wear)
		{
			return this.containerWear;
		}
		return null;
	}

	// Token: 0x060008DA RID: 2266 RVA: 0x0003B074 File Offset: 0x00039274
	public bool GiveItem(global::Item item, global::ItemContainer container = null)
	{
		if (item == null)
		{
			return false;
		}
		int iTargetPos = -1;
		this.GetIdealPickupContainer(item, ref container, ref iTargetPos);
		return (container != null && item.MoveToContainer(container, iTargetPos, true)) || item.MoveToContainer(this.containerMain, -1, true) || item.MoveToContainer(this.containerBelt, -1, true);
	}

	// Token: 0x060008DB RID: 2267 RVA: 0x0003B0D8 File Offset: 0x000392D8
	protected void GetIdealPickupContainer(global::Item item, ref global::ItemContainer container, ref int position)
	{
		if (item.info.stackable > 1)
		{
			if (this.containerBelt != null && this.containerBelt.FindItemByItemID(item.info.itemid) != null)
			{
				container = this.containerBelt;
				return;
			}
			if (this.containerMain != null && this.containerMain.FindItemByItemID(item.info.itemid) != null)
			{
				container = this.containerMain;
				return;
			}
		}
		if (item.info.isUsable && !item.info.HasFlag(global::ItemDefinition.Flag.NotStraightToBelt))
		{
			container = this.containerBelt;
			return;
		}
	}

	// Token: 0x060008DC RID: 2268 RVA: 0x0003B180 File Offset: 0x00039380
	public void Strip()
	{
		this.containerMain.Clear();
		this.containerBelt.Clear();
		this.containerWear.Clear();
		global::ItemManager.DoRemoves();
	}

	// Token: 0x060008DD RID: 2269 RVA: 0x0003B1A8 File Offset: 0x000393A8
	public void GiveDefaultItems()
	{
		this.Strip();
		ulong skin = 0UL;
		int infoInt = base.baseEntity.GetInfoInt("client.rockskin", 0);
		if (infoInt > 0 && base.baseEntity.blueprints.steamInventory.HasItem(infoInt))
		{
			Facepunch.Steamworks.Inventory.Definition definition = Rust.Global.SteamServer.Inventory.FindDefinition(infoInt);
			if (definition != null)
			{
				skin = definition.GetProperty<ulong>("workshopdownload");
			}
		}
		this.GiveItem(global::ItemManager.CreateByName("rock", 1, skin), this.containerBelt);
		this.GiveItem(global::ItemManager.CreateByName("torch", 1, 0UL), this.containerBelt);
	}

	// Token: 0x060008DE RID: 2270 RVA: 0x0003B248 File Offset: 0x00039448
	public ProtoBuf.PlayerInventory Save(bool bForDisk)
	{
		ProtoBuf.PlayerInventory playerInventory = Facepunch.Pool.Get<ProtoBuf.PlayerInventory>();
		if (bForDisk)
		{
			playerInventory.invMain = this.containerMain.Save();
		}
		playerInventory.invBelt = this.containerBelt.Save();
		playerInventory.invWear = this.containerWear.Save();
		return playerInventory;
	}

	// Token: 0x060008DF RID: 2271 RVA: 0x0003B298 File Offset: 0x00039498
	public void Load(ProtoBuf.PlayerInventory msg)
	{
		if (msg.invMain != null)
		{
			this.containerMain.Load(msg.invMain);
		}
		if (msg.invBelt != null)
		{
			this.containerBelt.Load(msg.invBelt);
		}
		if (msg.invWear != null)
		{
			this.containerWear.Load(msg.invWear);
		}
	}

	// Token: 0x060008E0 RID: 2272 RVA: 0x0003B2FC File Offset: 0x000394FC
	public int Take(List<global::Item> collect, int itemid, int amount)
	{
		int num = 0;
		if (this.containerMain != null)
		{
			num += this.containerMain.Take(collect, itemid, amount);
		}
		if (amount == num)
		{
			return num;
		}
		if (this.containerBelt != null)
		{
			num += this.containerBelt.Take(collect, itemid, amount);
		}
		if (amount == num)
		{
			return num;
		}
		if (this.containerWear != null)
		{
			num += this.containerWear.Take(collect, itemid, amount);
		}
		return num;
	}

	// Token: 0x060008E1 RID: 2273 RVA: 0x0003B374 File Offset: 0x00039574
	public int GetAmount(int itemid)
	{
		if (itemid == 0)
		{
			return 0;
		}
		int num = 0;
		if (this.containerMain != null)
		{
			num += this.containerMain.GetAmount(itemid, true);
		}
		if (this.containerBelt != null)
		{
			num += this.containerBelt.GetAmount(itemid, true);
		}
		if (this.containerWear != null)
		{
			num += this.containerWear.GetAmount(itemid, true);
		}
		return num;
	}

	// Token: 0x060008E2 RID: 2274 RVA: 0x0003B3E0 File Offset: 0x000395E0
	public global::Item[] AllItems()
	{
		List<global::Item> list = new List<global::Item>();
		if (this.containerMain != null)
		{
			list.AddRange(this.containerMain.itemList);
		}
		if (this.containerBelt != null)
		{
			list.AddRange(this.containerBelt.itemList);
		}
		if (this.containerWear != null)
		{
			list.AddRange(this.containerWear.itemList);
		}
		return list.ToArray();
	}

	// Token: 0x060008E3 RID: 2275 RVA: 0x0003B450 File Offset: 0x00039650
	public void FindAmmo(List<global::Item> list, AmmoTypes ammoType)
	{
		if (this.containerMain != null)
		{
			this.containerMain.FindAmmo(list, ammoType);
		}
		if (this.containerBelt != null)
		{
			this.containerBelt.FindAmmo(list, ammoType);
		}
	}

	// Token: 0x060008E4 RID: 2276 RVA: 0x0003B484 File Offset: 0x00039684
	public bool HasAmmo(AmmoTypes ammoType)
	{
		return this.containerMain.HasAmmo(ammoType) || this.containerBelt.HasAmmo(ammoType);
	}

	// Token: 0x0400040E RID: 1038
	public global::ItemContainer containerMain;

	// Token: 0x0400040F RID: 1039
	public global::ItemContainer containerBelt;

	// Token: 0x04000410 RID: 1040
	public global::ItemContainer containerWear;

	// Token: 0x04000411 RID: 1041
	public global::ItemCrafter crafting;

	// Token: 0x04000412 RID: 1042
	public global::PlayerLoot loot;

	// Token: 0x02000085 RID: 133
	public enum Type
	{
		// Token: 0x04000415 RID: 1045
		Main,
		// Token: 0x04000416 RID: 1046
		Belt,
		// Token: 0x04000417 RID: 1047
		Wear
	}
}
