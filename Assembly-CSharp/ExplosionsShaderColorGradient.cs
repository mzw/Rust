﻿using System;
using UnityEngine;

// Token: 0x020007FB RID: 2043
public class ExplosionsShaderColorGradient : MonoBehaviour
{
	// Token: 0x060025C2 RID: 9666 RVA: 0x000D0B6C File Offset: 0x000CED6C
	private void Start()
	{
		Material[] materials = base.GetComponent<Renderer>().materials;
		if (this.MaterialID >= materials.Length)
		{
			Debug.Log("ShaderColorGradient: Material ID more than shader materials count.");
		}
		this.matInstance = materials[this.MaterialID];
		if (!this.matInstance.HasProperty(this.ShaderProperty))
		{
			Debug.Log("ShaderColorGradient: Shader not have \"" + this.ShaderProperty + "\" property");
		}
		this.propertyID = Shader.PropertyToID(this.ShaderProperty);
		this.oldColor = this.matInstance.GetColor(this.propertyID);
	}

	// Token: 0x060025C3 RID: 9667 RVA: 0x000D0C04 File Offset: 0x000CEE04
	private void OnEnable()
	{
		this.startTime = Time.time;
		this.canUpdate = true;
	}

	// Token: 0x060025C4 RID: 9668 RVA: 0x000D0C18 File Offset: 0x000CEE18
	private void Update()
	{
		float num = Time.time - this.startTime;
		if (this.canUpdate)
		{
			Color color = this.Color.Evaluate(num / this.TimeMultiplier);
			this.matInstance.SetColor(this.propertyID, color * this.oldColor);
		}
		if (num >= this.TimeMultiplier)
		{
			this.canUpdate = false;
		}
	}

	// Token: 0x040021E6 RID: 8678
	public string ShaderProperty = "_TintColor";

	// Token: 0x040021E7 RID: 8679
	public int MaterialID;

	// Token: 0x040021E8 RID: 8680
	public Gradient Color = new Gradient();

	// Token: 0x040021E9 RID: 8681
	public float TimeMultiplier = 1f;

	// Token: 0x040021EA RID: 8682
	private bool canUpdate;

	// Token: 0x040021EB RID: 8683
	private Material matInstance;

	// Token: 0x040021EC RID: 8684
	private int propertyID;

	// Token: 0x040021ED RID: 8685
	private float startTime;

	// Token: 0x040021EE RID: 8686
	private Color oldColor;
}
