﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

// Token: 0x02000064 RID: 100
public class Door : global::AnimatedBuildingBlock
{
	// Token: 0x0600077D RID: 1917 RVA: 0x0003034C File Offset: 0x0002E54C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Door.OnRpcMessage", 0.1f))
		{
			if (rpc == 3003686860u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_CloseDoor ");
				}
				using (TimeWarning.New("RPC_CloseDoor", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_CloseDoor", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_CloseDoor(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_CloseDoor");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 167081128u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_KnockDoor ");
				}
				using (TimeWarning.New("RPC_KnockDoor", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_KnockDoor", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_KnockDoor(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_KnockDoor");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 4041793042u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_OpenDoor ");
				}
				using (TimeWarning.New("RPC_OpenDoor", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_OpenDoor", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_OpenDoor(rpc4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in RPC_OpenDoor");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 3327264274u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_ToggleHatch ");
				}
				using (TimeWarning.New("RPC_ToggleHatch", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_ToggleHatch", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_ToggleHatch(rpc5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in RPC_ToggleHatch");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600077E RID: 1918 RVA: 0x000309D4 File Offset: 0x0002EBD4
	public override void ServerInit()
	{
		base.ServerInit();
		if (global::Door.nonWalkableArea < 0)
		{
			global::Door.nonWalkableArea = NavMesh.GetAreaFromName("Not Walkable");
		}
		if (global::Door.animalAgentTypeId < 0)
		{
			global::Door.animalAgentTypeId = NavMesh.GetSettingsByIndex(1).agentTypeID;
		}
		if (this.NavMeshVolumeAnimals == null)
		{
			this.NavMeshVolumeAnimals = base.gameObject.AddComponent<NavMeshModifierVolume>();
			this.NavMeshVolumeAnimals.area = global::Door.nonWalkableArea;
			this.NavMeshVolumeAnimals.AddAgentType(global::Door.animalAgentTypeId);
			this.NavMeshVolumeAnimals.center = Vector3.zero;
			this.NavMeshVolumeAnimals.size = Vector3.one;
		}
		if (this.HasSlot(global::BaseEntity.Slot.Lock))
		{
			this.canNpcOpen = false;
		}
		if (!this.canNpcOpen)
		{
			if (global::Door.humanoidAgentTypeId < 0)
			{
				global::Door.humanoidAgentTypeId = NavMesh.GetSettingsByIndex(0).agentTypeID;
			}
			if (this.NavMeshVolumeHumanoids == null)
			{
				this.NavMeshVolumeHumanoids = base.gameObject.AddComponent<NavMeshModifierVolume>();
				this.NavMeshVolumeHumanoids.area = global::Door.nonWalkableArea;
				this.NavMeshVolumeHumanoids.AddAgentType(global::Door.humanoidAgentTypeId);
				this.NavMeshVolumeHumanoids.center = Vector3.zero;
				this.NavMeshVolumeHumanoids.size = Vector3.one;
			}
		}
		else if (this.NpcTriggerBox == null)
		{
			this.NpcTriggerBox = new GameObject("NpcTriggerBox").AddComponent<global::NPCDoorTriggerBox>();
			this.NpcTriggerBox.Setup(this);
		}
	}

	// Token: 0x0600077F RID: 1919 RVA: 0x00030B58 File Offset: 0x0002ED58
	public override void ResetState()
	{
		base.ResetState();
		this.decayResetTimeLast = float.NegativeInfinity;
	}

	// Token: 0x06000780 RID: 1920 RVA: 0x00030B6C File Offset: 0x0002ED6C
	public override bool HasSlot(global::BaseEntity.Slot slot)
	{
		return (slot == global::BaseEntity.Slot.Lock && this.canTakeLock) || slot == global::BaseEntity.Slot.UpperModifier || (slot == global::BaseEntity.Slot.CenterDecoration && this.canTakeCloser) || base.HasSlot(slot);
	}

	// Token: 0x06000781 RID: 1921 RVA: 0x00030BA8 File Offset: 0x0002EDA8
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.IsOpen() && !base.GetSlot(global::BaseEntity.Slot.Lock) && !base.GetSlot(global::BaseEntity.Slot.UpperModifier) && !base.GetSlot(global::BaseEntity.Slot.CenterDecoration) && base.CanPickup(player);
	}

	// Token: 0x06000782 RID: 1922 RVA: 0x00030C04 File Offset: 0x0002EE04
	public void CloseRequest()
	{
		this.SetOpen(false);
	}

	// Token: 0x06000783 RID: 1923 RVA: 0x00030C10 File Offset: 0x0002EE10
	public override void OnFlagsChanged(global::BaseEntity.Flags old, global::BaseEntity.Flags next)
	{
		base.OnFlagsChanged(old, next);
		global::BaseEntity slot = base.GetSlot(global::BaseEntity.Slot.UpperModifier);
		if (slot)
		{
			slot.SendMessage("Think");
		}
	}

	// Token: 0x06000784 RID: 1924 RVA: 0x00030C44 File Offset: 0x0002EE44
	public void SetOpen(bool open)
	{
		base.SetFlag(global::BaseEntity.Flags.Open, open, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000785 RID: 1925 RVA: 0x00030C58 File Offset: 0x0002EE58
	public void SetLocked(bool locked)
	{
		base.SetFlag(global::BaseEntity.Flags.Locked, false, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000786 RID: 1926 RVA: 0x00030C6C File Offset: 0x0002EE6C
	public bool GetPlayerLockPermission(global::BasePlayer player)
	{
		global::BaseLock baseLock = base.GetSlot(global::BaseEntity.Slot.Lock) as global::BaseLock;
		return baseLock == null || baseLock.GetPlayerLockPermission(player);
	}

	// Token: 0x06000787 RID: 1927 RVA: 0x00030C9C File Offset: 0x0002EE9C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_OpenDoor(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (base.IsOpen())
		{
			return;
		}
		if (base.IsBusy())
		{
			return;
		}
		if (base.IsLocked())
		{
			return;
		}
		global::BaseLock baseLock = base.GetSlot(global::BaseEntity.Slot.Lock) as global::BaseLock;
		if (baseLock != null)
		{
			if (!baseLock.OnTryToOpen(rpc.player))
			{
				return;
			}
			if (baseLock.IsLocked() && UnityEngine.Time.realtimeSinceStartup - this.decayResetTimeLast > 60f)
			{
				global::BuildingBlock buildingBlock = base.FindLinkedEntity<global::BuildingBlock>();
				if (buildingBlock)
				{
					global::Decay.BuildingDecayTouch(buildingBlock);
				}
				else
				{
					global::Decay.RadialDecayTouch(base.transform.position, 40f, 2097408);
				}
				this.decayResetTimeLast = UnityEngine.Time.realtimeSinceStartup;
			}
		}
		base.SetFlag(global::BaseEntity.Flags.Open, true, false);
		base.SendNetworkUpdateImmediate(false);
		Interface.CallHook("OnDoorOpened", new object[]
		{
			this,
			rpc.player
		});
	}

	// Token: 0x06000788 RID: 1928 RVA: 0x00030DA0 File Offset: 0x0002EFA0
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_CloseDoor(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!base.IsOpen())
		{
			return;
		}
		if (base.IsBusy())
		{
			return;
		}
		if (base.IsLocked())
		{
			return;
		}
		global::BaseLock baseLock = base.GetSlot(global::BaseEntity.Slot.Lock) as global::BaseLock;
		if (baseLock != null && !baseLock.OnTryToClose(rpc.player))
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, false, false);
		base.SendNetworkUpdateImmediate(false);
		Interface.CallHook("OnDoorClosed", new object[]
		{
			this,
			rpc.player
		});
	}

	// Token: 0x06000789 RID: 1929 RVA: 0x00030E40 File Offset: 0x0002F040
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_KnockDoor(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.knockEffect.isValid)
		{
			return;
		}
		if (UnityEngine.Time.realtimeSinceStartup < this.nextKnockTime)
		{
			return;
		}
		this.nextKnockTime = UnityEngine.Time.realtimeSinceStartup + 0.5f;
		global::Effect.server.Run(this.knockEffect.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		Interface.CallHook("OnDoorKnocked", new object[]
		{
			this,
			rpc.player
		});
	}

	// Token: 0x0600078A RID: 1930 RVA: 0x00030ED0 File Offset: 0x0002F0D0
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_ToggleHatch(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.hasHatch)
		{
			return;
		}
		global::BaseLock baseLock = base.GetSlot(global::BaseEntity.Slot.Lock) as global::BaseLock;
		if (!baseLock || baseLock.OnTryToOpen(rpc.player))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved3, !base.HasFlag(global::BaseEntity.Flags.Reserved3), false);
		}
	}

	// Token: 0x0600078B RID: 1931 RVA: 0x00030F40 File Offset: 0x0002F140
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x0600078C RID: 1932 RVA: 0x00030F44 File Offset: 0x0002F144
	public override float BoundsPadding()
	{
		return 2f;
	}

	// Token: 0x0400036A RID: 874
	public global::GameObjectRef knockEffect;

	// Token: 0x0400036B RID: 875
	public bool canTakeLock = true;

	// Token: 0x0400036C RID: 876
	public bool hasHatch;

	// Token: 0x0400036D RID: 877
	public bool canTakeCloser;

	// Token: 0x0400036E RID: 878
	public bool canNpcOpen = true;

	// Token: 0x0400036F RID: 879
	private float decayResetTimeLast = float.NegativeInfinity;

	// Token: 0x04000370 RID: 880
	public NavMeshModifierVolume NavMeshVolumeAnimals;

	// Token: 0x04000371 RID: 881
	public NavMeshModifierVolume NavMeshVolumeHumanoids;

	// Token: 0x04000372 RID: 882
	public global::NPCDoorTriggerBox NpcTriggerBox;

	// Token: 0x04000373 RID: 883
	private static int nonWalkableArea = -1;

	// Token: 0x04000374 RID: 884
	private static int animalAgentTypeId = -1;

	// Token: 0x04000375 RID: 885
	private static int humanoidAgentTypeId = -1;

	// Token: 0x04000376 RID: 886
	private float nextKnockTime = float.NegativeInfinity;
}
