﻿using System;
using UnityEngine;

// Token: 0x0200048B RID: 1163
public class RadialSpawnPoint : global::BaseSpawnPoint
{
	// Token: 0x06001948 RID: 6472 RVA: 0x0008E814 File Offset: 0x0008CA14
	public override void GetLocation(out Vector3 pos, out Quaternion rot)
	{
		Vector2 vector = Random.insideUnitCircle * this.radius;
		pos = base.transform.position + new Vector3(vector.x, 0f, vector.y);
		rot = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
		base.DropToGround(ref pos, ref rot);
	}

	// Token: 0x06001949 RID: 6473 RVA: 0x0008E88C File Offset: 0x0008CA8C
	public override void ObjectSpawned(global::SpawnPointInstance instance)
	{
	}

	// Token: 0x0600194A RID: 6474 RVA: 0x0008E890 File Offset: 0x0008CA90
	public override void ObjectRetired(global::SpawnPointInstance instance)
	{
	}

	// Token: 0x040013FD RID: 5117
	public float radius = 10f;
}
