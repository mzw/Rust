﻿using System;
using UnityEngine;

// Token: 0x020000D0 RID: 208
public class NPCMurderer : global::NPCPlayerApex
{
	// Token: 0x06000ADB RID: 2779 RVA: 0x000497E0 File Offset: 0x000479E0
	public override string Categorize()
	{
		return "murderer";
	}

	// Token: 0x06000ADC RID: 2780 RVA: 0x000497E8 File Offset: 0x000479E8
	public override float StartHealth()
	{
		return Random.Range(100f, 100f);
	}

	// Token: 0x06000ADD RID: 2781 RVA: 0x000497FC File Offset: 0x000479FC
	public override float StartMaxHealth()
	{
		return 100f;
	}

	// Token: 0x06000ADE RID: 2782 RVA: 0x00049804 File Offset: 0x00047A04
	public override float MaxHealth()
	{
		return 100f;
	}

	// Token: 0x1700006C RID: 108
	// (get) Token: 0x06000ADF RID: 2783 RVA: 0x0004980C File Offset: 0x00047A0C
	public override global::BaseNpc.AiStatistics.FamilyEnum Family
	{
		get
		{
			return global::BaseNpc.AiStatistics.FamilyEnum.Murderer;
		}
	}

	// Token: 0x06000AE0 RID: 2784 RVA: 0x00049810 File Offset: 0x00047A10
	public override bool ShouldDropActiveItem()
	{
		return false;
	}

	// Token: 0x06000AE1 RID: 2785 RVA: 0x00049814 File Offset: 0x00047A14
	public override void CreateCorpse()
	{
		using (TimeWarning.New("Create corpse", 0.1f))
		{
			global::NPCPlayerCorpse npcplayerCorpse = base.DropCorpse("assets/prefabs/npc/murderer/murderer_corpse.prefab") as global::NPCPlayerCorpse;
			if (npcplayerCorpse)
			{
				npcplayerCorpse.SetLootableIn(2f);
				npcplayerCorpse.SetFlag(global::BaseEntity.Flags.Reserved5, base.HasPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash), false);
				npcplayerCorpse.SetFlag(global::BaseEntity.Flags.Reserved2, true, false);
				for (int i = 0; i < this.inventory.containerWear.itemList.Count; i++)
				{
					global::Item item = this.inventory.containerWear.itemList[i];
					if (item != null && item.info.shortname == "gloweyes")
					{
						this.inventory.containerWear.Remove(item);
						break;
					}
				}
				npcplayerCorpse.TakeFrom(new global::ItemContainer[]
				{
					this.inventory.containerMain,
					this.inventory.containerWear,
					this.inventory.containerBelt
				});
				npcplayerCorpse.playerName = base.displayName;
				npcplayerCorpse.playerSteamID = this.userID;
				npcplayerCorpse.Spawn();
				npcplayerCorpse.TakeChildren(this);
				foreach (global::ItemContainer itemContainer in npcplayerCorpse.containers)
				{
					itemContainer.Clear();
				}
				if (this.LootSpawnSlots.Length > 0)
				{
					foreach (global::LootContainer.LootSpawnSlot lootSpawnSlot in this.LootSpawnSlots)
					{
						for (int l = 0; l < lootSpawnSlot.numberToSpawn; l++)
						{
							float num = Random.Range(0f, 1f);
							if (num <= lootSpawnSlot.probability)
							{
								lootSpawnSlot.definition.SpawnIntoContainer(npcplayerCorpse.containers[0]);
							}
						}
					}
				}
			}
		}
	}

	// Token: 0x040005A9 RID: 1449
	public global::LootContainer.LootSpawnSlot[] LootSpawnSlots;
}
