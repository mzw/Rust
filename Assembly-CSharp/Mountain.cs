﻿using System;
using UnityEngine;

// Token: 0x020005DE RID: 1502
public class Mountain : global::TerrainPlacement
{
	// Token: 0x06001EDC RID: 7900 RVA: 0x000AE638 File Offset: 0x000AC838
	protected void OnDrawGizmosSelected()
	{
		Vector3 vector = Vector3.up * (0.5f * this.Fade);
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		Gizmos.DrawCube(base.transform.position + vector, new Vector3(this.size.x, this.Fade, this.size.z));
		Gizmos.DrawWireCube(base.transform.position + vector, new Vector3(this.size.x, this.Fade, this.size.z));
	}

	// Token: 0x06001EDD RID: 7901 RVA: 0x000AE6E8 File Offset: 0x000AC8E8
	protected override void ApplyHeight(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		Vector3 position = localToWorld.MultiplyPoint3x4(Vector3.zero);
		global::TextureData heightdata = new global::TextureData(this.heightmap);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, -this.extents.z));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, -this.extents.z));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, this.extents.z));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, this.extents.z));
		global::TerrainMeta.HeightMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			float normZ = global::TerrainMeta.HeightMap.Coordinate(z);
			float normX = global::TerrainMeta.HeightMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			float num = position.y + this.offset.y + heightdata.GetInterpolatedHalf((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z) * this.size.y;
			float num2 = Mathf.InverseLerp(position.y, position.y + this.Fade, num);
			if (num2 == 0f)
			{
				return;
			}
			float num3 = global::TerrainMeta.NormalizeY(num);
			float height = global::TerrainMeta.HeightMap.GetHeight01(x, z);
			num3 = Mathx.SmoothMax(height, num3, 0.1f);
			global::TerrainMeta.HeightMap.SetHeight(x, z, num3, num2);
		});
	}

	// Token: 0x06001EDE RID: 7902 RVA: 0x000AE818 File Offset: 0x000ACA18
	protected override void ApplySplat(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		Vector3 position = localToWorld.MultiplyPoint3x4(Vector3.zero);
		global::TextureData heightdata = new global::TextureData(this.heightmap);
		global::TextureData splat0data = new global::TextureData(this.splatmap0);
		global::TextureData splat1data = new global::TextureData(this.splatmap1);
		bool should0 = base.ShouldSplat(1);
		bool should1 = base.ShouldSplat(2);
		bool should2 = base.ShouldSplat(4);
		bool should3 = base.ShouldSplat(8);
		bool should4 = base.ShouldSplat(16);
		bool should5 = base.ShouldSplat(32);
		bool should6 = base.ShouldSplat(64);
		bool should7 = base.ShouldSplat(128);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, -this.extents.z));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, -this.extents.z));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, this.extents.z));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, this.extents.z));
		global::TerrainMeta.SplatMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			float normZ = global::TerrainMeta.SplatMap.Coordinate(z);
			float normX = global::TerrainMeta.SplatMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			float num = position.y + this.offset.y + heightdata.GetInterpolatedHalf((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z) * this.size.y;
			float num2 = Mathf.InverseLerp(position.y, position.y + this.Fade, num);
			if (num2 == 0f)
			{
				return;
			}
			Vector4 interpolatedVector = splat0data.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			Vector4 interpolatedVector2 = splat1data.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			if (!should0)
			{
				interpolatedVector.x = 0f;
			}
			if (!should1)
			{
				interpolatedVector.y = 0f;
			}
			if (!should2)
			{
				interpolatedVector.z = 0f;
			}
			if (!should3)
			{
				interpolatedVector.w = 0f;
			}
			if (!should4)
			{
				interpolatedVector2.x = 0f;
			}
			if (!should5)
			{
				interpolatedVector2.y = 0f;
			}
			if (!should6)
			{
				interpolatedVector2.z = 0f;
			}
			if (!should7)
			{
				interpolatedVector2.w = 0f;
			}
			global::TerrainMeta.SplatMap.SetSplatRaw(x, z, interpolatedVector, interpolatedVector2, num2);
		});
	}

	// Token: 0x06001EDF RID: 7903 RVA: 0x000AE9DC File Offset: 0x000ACBDC
	protected override void ApplyAlpha(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
	}

	// Token: 0x06001EE0 RID: 7904 RVA: 0x000AE9E0 File Offset: 0x000ACBE0
	protected override void ApplyBiome(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		Vector3 position = localToWorld.MultiplyPoint3x4(Vector3.zero);
		global::TextureData heightdata = new global::TextureData(this.heightmap);
		global::TextureData biomedata = new global::TextureData(this.biomemap);
		bool should0 = base.ShouldBiome(1);
		bool should1 = base.ShouldBiome(2);
		bool should2 = base.ShouldBiome(4);
		bool should3 = base.ShouldBiome(8);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, -this.extents.z));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, -this.extents.z));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, this.extents.z));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, this.extents.z));
		global::TerrainMeta.BiomeMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			float normZ = global::TerrainMeta.BiomeMap.Coordinate(z);
			float normX = global::TerrainMeta.BiomeMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			float num = position.y + this.offset.y + heightdata.GetInterpolatedHalf((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z) * this.size.y;
			float num2 = Mathf.InverseLerp(position.y, position.y + this.Fade, num);
			if (num2 == 0f)
			{
				return;
			}
			Vector4 interpolatedVector = biomedata.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			if (!should0)
			{
				interpolatedVector.x = 0f;
			}
			if (!should1)
			{
				interpolatedVector.y = 0f;
			}
			if (!should2)
			{
				interpolatedVector.z = 0f;
			}
			if (!should3)
			{
				interpolatedVector.w = 0f;
			}
			global::TerrainMeta.BiomeMap.SetBiomeRaw(x, z, interpolatedVector, num2);
		});
	}

	// Token: 0x06001EE1 RID: 7905 RVA: 0x000AEB58 File Offset: 0x000ACD58
	protected override void ApplyTopology(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		global::TextureData topologydata = new global::TextureData(this.topologymap);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, -this.extents.z));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, -this.extents.z));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.extents.x, 0f, this.extents.z));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.extents.x, 0f, this.extents.z));
		global::TerrainMeta.TopologyMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			global::GenerateCliffTopology.Process(x, z);
			float normZ = global::TerrainMeta.TopologyMap.Coordinate(z);
			float normX = global::TerrainMeta.TopologyMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			int interpolatedInt = topologydata.GetInterpolatedInt((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			if (this.ShouldTopology(interpolatedInt))
			{
				global::TerrainMeta.TopologyMap.AddTopology(x, z, interpolatedInt & (int)this.TopologyMask);
			}
		});
	}

	// Token: 0x06001EE2 RID: 7906 RVA: 0x000AEC78 File Offset: 0x000ACE78
	protected override void ApplyWater(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
	}

	// Token: 0x040019B9 RID: 6585
	public float Fade = 10f;
}
