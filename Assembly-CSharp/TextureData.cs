﻿using System;
using UnityEngine;

// Token: 0x02000536 RID: 1334
public struct TextureData
{
	// Token: 0x06001C11 RID: 7185 RVA: 0x0009E4DC File Offset: 0x0009C6DC
	public TextureData(Texture2D tex)
	{
		this.width = tex.width;
		this.height = tex.height;
		this.colors = tex.GetPixels32();
	}

	// Token: 0x06001C12 RID: 7186 RVA: 0x0009E504 File Offset: 0x0009C704
	public Color32 GetColor(int x, int y)
	{
		return this.colors[y * this.width + x];
	}

	// Token: 0x06001C13 RID: 7187 RVA: 0x0009E520 File Offset: 0x0009C720
	public int GetShort(int x, int y)
	{
		return (int)global::TextureData.DecodeShort(this.GetColor(x, y));
	}

	// Token: 0x06001C14 RID: 7188 RVA: 0x0009E530 File Offset: 0x0009C730
	public int GetInt(int x, int y)
	{
		return global::TextureData.DecodeInt(this.GetColor(x, y));
	}

	// Token: 0x06001C15 RID: 7189 RVA: 0x0009E540 File Offset: 0x0009C740
	public float GetFloat(int x, int y)
	{
		return global::TextureData.DecodeFloat(this.GetColor(x, y));
	}

	// Token: 0x06001C16 RID: 7190 RVA: 0x0009E550 File Offset: 0x0009C750
	public float GetHalf(int x, int y)
	{
		return global::TextureData.Short2Float(this.GetShort(x, y));
	}

	// Token: 0x06001C17 RID: 7191 RVA: 0x0009E560 File Offset: 0x0009C760
	public Vector4 GetVector(int x, int y)
	{
		return global::TextureData.DecodeVector(this.GetColor(x, y));
	}

	// Token: 0x06001C18 RID: 7192 RVA: 0x0009E570 File Offset: 0x0009C770
	public Vector3 GetNormal(int x, int y)
	{
		return global::TextureData.DecodeNormal(this.GetColor(x, y));
	}

	// Token: 0x06001C19 RID: 7193 RVA: 0x0009E584 File Offset: 0x0009C784
	public Color32 GetInterpolatedColor(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int num3 = Mathf.Clamp((int)num, 1, this.width - 2);
		int num4 = Mathf.Clamp((int)num2, 1, this.height - 2);
		int x2 = Mathf.Min(num3 + 1, this.width - 2);
		int y2 = Mathf.Min(num4 + 1, this.height - 2);
		Color color = this.GetColor(num3, num4);
		Color color2 = this.GetColor(x2, num4);
		Color color3 = this.GetColor(num3, y2);
		Color color4 = this.GetColor(x2, y2);
		float num5 = num - (float)num3;
		float num6 = num2 - (float)num4;
		Color color5 = Color.Lerp(color, color2, num5);
		Color color6 = Color.Lerp(color3, color4, num5);
		return Color.Lerp(color5, color6, num6);
	}

	// Token: 0x06001C1A RID: 7194 RVA: 0x0009E668 File Offset: 0x0009C868
	public int GetInterpolatedInt(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int x2 = Mathf.Clamp(Mathf.RoundToInt(num), 1, this.width - 2);
		int y2 = Mathf.Clamp(Mathf.RoundToInt(num2), 1, this.height - 2);
		return this.GetInt(x2, y2);
	}

	// Token: 0x06001C1B RID: 7195 RVA: 0x0009E6C0 File Offset: 0x0009C8C0
	public int GetInterpolatedShort(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int x2 = Mathf.Clamp(Mathf.RoundToInt(num), 1, this.width - 2);
		int y2 = Mathf.Clamp(Mathf.RoundToInt(num2), 1, this.height - 2);
		return this.GetShort(x2, y2);
	}

	// Token: 0x06001C1C RID: 7196 RVA: 0x0009E718 File Offset: 0x0009C918
	public float GetInterpolatedFloat(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int num3 = Mathf.Clamp((int)num, 1, this.width - 2);
		int num4 = Mathf.Clamp((int)num2, 1, this.height - 2);
		int x2 = Mathf.Min(num3 + 1, this.width - 2);
		int y2 = Mathf.Min(num4 + 1, this.height - 2);
		float @float = this.GetFloat(num3, num4);
		float float2 = this.GetFloat(x2, num4);
		float float3 = this.GetFloat(num3, y2);
		float float4 = this.GetFloat(x2, y2);
		float num5 = num - (float)num3;
		float num6 = num2 - (float)num4;
		float num7 = Mathf.Lerp(@float, float2, num5);
		float num8 = Mathf.Lerp(float3, float4, num5);
		return Mathf.Lerp(num7, num8, num6);
	}

	// Token: 0x06001C1D RID: 7197 RVA: 0x0009E7E0 File Offset: 0x0009C9E0
	public float GetInterpolatedHalf(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int num3 = Mathf.Clamp((int)num, 1, this.width - 2);
		int num4 = Mathf.Clamp((int)num2, 1, this.height - 2);
		int x2 = Mathf.Min(num3 + 1, this.width - 2);
		int y2 = Mathf.Min(num4 + 1, this.height - 2);
		float half = this.GetHalf(num3, num4);
		float half2 = this.GetHalf(x2, num4);
		float half3 = this.GetHalf(num3, y2);
		float half4 = this.GetHalf(x2, y2);
		float num5 = num - (float)num3;
		float num6 = num2 - (float)num4;
		float num7 = Mathf.Lerp(half, half2, num5);
		float num8 = Mathf.Lerp(half3, half4, num5);
		return Mathf.Lerp(num7, num8, num6);
	}

	// Token: 0x06001C1E RID: 7198 RVA: 0x0009E8A8 File Offset: 0x0009CAA8
	public Vector4 GetInterpolatedVector(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int num3 = Mathf.Clamp((int)num, 1, this.width - 2);
		int num4 = Mathf.Clamp((int)num2, 1, this.height - 2);
		int x2 = Mathf.Min(num3 + 1, this.width - 2);
		int y2 = Mathf.Min(num4 + 1, this.height - 2);
		Vector4 vector = this.GetVector(num3, num4);
		Vector4 vector2 = this.GetVector(x2, num4);
		Vector4 vector3 = this.GetVector(num3, y2);
		Vector4 vector4 = this.GetVector(x2, y2);
		float num5 = num - (float)num3;
		float num6 = num2 - (float)num4;
		Vector4 vector5 = Vector4.Lerp(vector, vector2, num5);
		Vector4 vector6 = Vector4.Lerp(vector3, vector4, num5);
		return Vector4.Lerp(vector5, vector6, num6);
	}

	// Token: 0x06001C1F RID: 7199 RVA: 0x0009E970 File Offset: 0x0009CB70
	public Vector3 GetInterpolatedNormal(float x, float y)
	{
		float num = x * (float)(this.width - 1);
		float num2 = y * (float)(this.height - 1);
		int num3 = Mathf.Clamp((int)num, 1, this.width - 2);
		int num4 = Mathf.Clamp((int)num2, 1, this.height - 2);
		int x2 = Mathf.Min(num3 + 1, this.width - 2);
		int y2 = Mathf.Min(num4 + 1, this.height - 2);
		Vector3 normal = this.GetNormal(num3, num4);
		Vector3 normal2 = this.GetNormal(x2, num4);
		Vector3 normal3 = this.GetNormal(num3, y2);
		Vector3 normal4 = this.GetNormal(x2, y2);
		float num5 = num - (float)num3;
		float num6 = num2 - (float)num4;
		Vector3 vector = Vector3.Lerp(normal, normal2, num5);
		Vector3 vector2 = Vector3.Lerp(normal3, normal4, num5);
		return Vector3.Lerp(vector, vector2, num6);
	}

	// Token: 0x06001C20 RID: 7200 RVA: 0x0009EA38 File Offset: 0x0009CC38
	public static byte Float2Byte(float f)
	{
		return (byte)(f * 255f + 0.5f);
	}

	// Token: 0x06001C21 RID: 7201 RVA: 0x0009EA48 File Offset: 0x0009CC48
	public static float Byte2Float(int b)
	{
		return (float)b * 0.003921569f;
	}

	// Token: 0x06001C22 RID: 7202 RVA: 0x0009EA54 File Offset: 0x0009CC54
	public static short Float2Short(float f)
	{
		return (short)(f * 32766f + 0.5f);
	}

	// Token: 0x06001C23 RID: 7203 RVA: 0x0009EA64 File Offset: 0x0009CC64
	public static float Short2Float(int b)
	{
		return (float)b * 3.051944E-05f;
	}

	// Token: 0x06001C24 RID: 7204 RVA: 0x0009EA70 File Offset: 0x0009CC70
	public static Color32 EncodeFloat(float f)
	{
		Union32 union = default(Union32);
		union.f = f;
		return new Color32(union.b1, union.b2, union.b3, union.b4);
	}

	// Token: 0x06001C25 RID: 7205 RVA: 0x0009EAB0 File Offset: 0x0009CCB0
	public static float DecodeFloat(Color32 c)
	{
		Union32 union = default(Union32);
		union.b1 = c.r;
		union.b2 = c.g;
		union.b3 = c.b;
		union.b4 = c.a;
		return union.f;
	}

	// Token: 0x06001C26 RID: 7206 RVA: 0x0009EB04 File Offset: 0x0009CD04
	public static Color32 EncodeInt(int i)
	{
		Union32 union = default(Union32);
		union.i = i;
		return new Color32(union.b1, union.b2, union.b3, union.b4);
	}

	// Token: 0x06001C27 RID: 7207 RVA: 0x0009EB44 File Offset: 0x0009CD44
	public static int DecodeInt(Color32 c)
	{
		Union32 union = default(Union32);
		union.b1 = c.r;
		union.b2 = c.g;
		union.b3 = c.b;
		union.b4 = c.a;
		return union.i;
	}

	// Token: 0x06001C28 RID: 7208 RVA: 0x0009EB98 File Offset: 0x0009CD98
	public static Color32 EncodeShort(short i)
	{
		Union16 union = default(Union16);
		union.i = i;
		return new Color32(union.b1, 0, union.b2, 1);
	}

	// Token: 0x06001C29 RID: 7209 RVA: 0x0009EBCC File Offset: 0x0009CDCC
	public static short DecodeShort(Color32 c)
	{
		Union16 union = default(Union16);
		union.b1 = c.r;
		union.b2 = c.b;
		return union.i;
	}

	// Token: 0x06001C2A RID: 7210 RVA: 0x0009EC04 File Offset: 0x0009CE04
	public static Color EncodeNormal(Vector3 n)
	{
		n = (n + Vector3.one) * 0.5f;
		return new Color(n.z, n.z, n.z, n.x);
	}

	// Token: 0x06001C2B RID: 7211 RVA: 0x0009EC40 File Offset: 0x0009CE40
	public static Vector3 DecodeNormal(Color c)
	{
		float num = c.a * 2f - 1f;
		float num2 = c.g * 2f - 1f;
		float num3 = Mathf.Sqrt(1f - Mathf.Clamp01(num * num + num2 * num2));
		return new Vector3(num, num3, num2);
	}

	// Token: 0x06001C2C RID: 7212 RVA: 0x0009EC98 File Offset: 0x0009CE98
	public static Color32 EncodeVector(Vector4 v)
	{
		return new Color32(global::TextureData.Float2Byte(v.x), global::TextureData.Float2Byte(v.y), global::TextureData.Float2Byte(v.z), global::TextureData.Float2Byte(v.w));
	}

	// Token: 0x06001C2D RID: 7213 RVA: 0x0009ECD0 File Offset: 0x0009CED0
	public static Vector4 DecodeVector(Color32 c)
	{
		return new Vector4(global::TextureData.Byte2Float((int)c.r), global::TextureData.Byte2Float((int)c.g), global::TextureData.Byte2Float((int)c.b), global::TextureData.Byte2Float((int)c.a));
	}

	// Token: 0x04001751 RID: 5969
	public int width;

	// Token: 0x04001752 RID: 5970
	public int height;

	// Token: 0x04001753 RID: 5971
	public Color32[] colors;

	// Token: 0x04001754 RID: 5972
	private const float float2byte = 255f;

	// Token: 0x04001755 RID: 5973
	private const float byte2float = 0.003921569f;

	// Token: 0x04001756 RID: 5974
	private const float float2short = 32766f;

	// Token: 0x04001757 RID: 5975
	private const float short2float = 3.051944E-05f;
}
