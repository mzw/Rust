﻿using System;
using UnityEngine;

// Token: 0x0200051C RID: 1308
public static class Noise
{
	// Token: 0x06001BC0 RID: 7104 RVA: 0x0009B470 File Offset: 0x00099670
	public static float Simplex1D(double x)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Simplex1D(x);
		}
		return (float)global::ManagedNoise.Simplex1D(x);
	}

	// Token: 0x06001BC1 RID: 7105 RVA: 0x0009B48C File Offset: 0x0009968C
	public static float Simplex2D(double x, double y)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Simplex2D(x, y);
		}
		return (float)global::ManagedNoise.Simplex2D(x, y);
	}

	// Token: 0x06001BC2 RID: 7106 RVA: 0x0009B4AC File Offset: 0x000996AC
	public static float Turbulence(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Turbulence(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.Turbulence(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC3 RID: 7107 RVA: 0x0009B4DC File Offset: 0x000996DC
	public static float Billow(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Billow(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.Billow(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC4 RID: 7108 RVA: 0x0009B50C File Offset: 0x0009970C
	public static float Ridge(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Ridge(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.Ridge(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC5 RID: 7109 RVA: 0x0009B53C File Offset: 0x0009973C
	public static float Sharp(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Sharp(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.Sharp(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC6 RID: 7110 RVA: 0x0009B56C File Offset: 0x0009976C
	public static float TurbulenceIQ(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.TurbulenceIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.TurbulenceIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC7 RID: 7111 RVA: 0x0009B59C File Offset: 0x0009979C
	public static float BillowIQ(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.BillowIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.BillowIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC8 RID: 7112 RVA: 0x0009B5CC File Offset: 0x000997CC
	public static float RidgeIQ(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.RidgeIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.RidgeIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BC9 RID: 7113 RVA: 0x0009B5FC File Offset: 0x000997FC
	public static float SharpIQ(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.SharpIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
		}
		return (float)global::ManagedNoise.SharpIQ(x, y, octaves, frequency, amplitude, lacunarity, gain);
	}

	// Token: 0x06001BCA RID: 7114 RVA: 0x0009B62C File Offset: 0x0009982C
	public static float TurbulenceWarp(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5, double warp = 0.25)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.TurbulenceWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
		}
		return (float)global::ManagedNoise.TurbulenceWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
	}

	// Token: 0x06001BCB RID: 7115 RVA: 0x0009B660 File Offset: 0x00099860
	public static float BillowWarp(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5, double warp = 0.25)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.BillowWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
		}
		return (float)global::ManagedNoise.BillowWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
	}

	// Token: 0x06001BCC RID: 7116 RVA: 0x0009B694 File Offset: 0x00099894
	public static float RidgeWarp(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5, double warp = 0.25)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.RidgeWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
		}
		return (float)global::ManagedNoise.RidgeWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
	}

	// Token: 0x06001BCD RID: 7117 RVA: 0x0009B6C8 File Offset: 0x000998C8
	public static float SharpWarp(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5, double warp = 0.25)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.SharpWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
		}
		return (float)global::ManagedNoise.SharpWarp(x, y, octaves, frequency, amplitude, lacunarity, gain, warp);
	}

	// Token: 0x06001BCE RID: 7118 RVA: 0x0009B6FC File Offset: 0x000998FC
	public static float Jordan(double x, double y, int octaves = 1, double frequency = 1.0, double amplitude = 1.0, double lacunarity = 2.0, double gain = 0.5, double warp = 1.0, double damp = 1.0, double damp_scale = 1.0)
	{
		if (global::Noise.native)
		{
			return (float)global::NativeNoise.Jordan(x, y, octaves, frequency, amplitude, lacunarity, gain, warp, damp, damp_scale);
		}
		return (float)global::ManagedNoise.Jordan(x, y, octaves, frequency, amplitude, lacunarity, gain, warp, damp, damp_scale);
	}

	// Token: 0x06001BCF RID: 7119 RVA: 0x0009B740 File Offset: 0x00099940
	public static void ConnectToNativeBackend()
	{
		try
		{
			global::Noise.native = true;
			double[] array = new double[12];
			double[] array2 = new double[12];
			for (int i = 0; i < 12; i++)
			{
				array[i] = (double)Random.Range(-1E+09f, 1E+09f);
				array2[i] = (double)Random.Range(-1E+09f, 1E+09f);
			}
			double[] array3 = new double[12];
			double[] array4 = new double[12];
			double[] array5 = new double[]
			{
				global::NativeNoise.Simplex1D(array[0]),
				global::NativeNoise.Simplex1D(array[1]),
				global::NativeNoise.Simplex1D(array[2]),
				global::NativeNoise.Simplex2D(array[3], array2[3]),
				global::NativeNoise.Simplex2D(array[4], array2[4]),
				global::NativeNoise.Simplex2D(array[5], array2[5]),
				global::NativeNoise.Simplex1D(array[6], out array3[6]),
				global::NativeNoise.Simplex1D(array[7], out array3[7]),
				global::NativeNoise.Simplex1D(array[8], out array3[8]),
				global::NativeNoise.Simplex2D(array[9], array2[9], out array3[9], out array4[9]),
				global::NativeNoise.Simplex2D(array[10], array2[10], out array3[10], out array4[10]),
				global::NativeNoise.Simplex2D(array[11], array2[11], out array3[11], out array4[11])
			};
			double[] array6 = new double[12];
			double[] array7 = new double[12];
			double[] array8 = new double[]
			{
				global::ManagedNoise.Simplex1D(array[0]),
				global::ManagedNoise.Simplex1D(array[1]),
				global::ManagedNoise.Simplex1D(array[2]),
				global::ManagedNoise.Simplex2D(array[3], array2[3]),
				global::ManagedNoise.Simplex2D(array[4], array2[4]),
				global::ManagedNoise.Simplex2D(array[5], array2[5]),
				global::ManagedNoise.Simplex1D(array[6], out array6[6]),
				global::ManagedNoise.Simplex1D(array[7], out array6[7]),
				global::ManagedNoise.Simplex1D(array[8], out array6[8]),
				global::ManagedNoise.Simplex2D(array[9], array2[9], out array6[9], out array7[9]),
				global::ManagedNoise.Simplex2D(array[10], array2[10], out array6[10], out array7[10]),
				global::ManagedNoise.Simplex2D(array[11], array2[11], out array6[11], out array7[11])
			};
			for (int j = 0; j < 12; j++)
			{
				if (array5[j] != array8[j] || array3[j] != array6[j] || array4[j] != array7[j])
				{
					string text = string.Concat(new object[]
					{
						"(",
						array5[j],
						" ",
						array3[j],
						" ",
						array4[j],
						")"
					});
					string text2 = string.Concat(new object[]
					{
						"(",
						array8[j],
						" ",
						array6[j],
						" ",
						array7[j],
						")"
					});
					Debug.LogWarning(string.Concat(new object[]
					{
						"Noise test #",
						j,
						" returned ",
						text,
						" != ",
						text2
					}));
					global::Noise.native = false;
				}
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarning(ex.Message);
			global::Noise.native = false;
		}
		finally
		{
			if (!global::Noise.native)
			{
				Debug.LogWarning("Cannot use native noise backend, using managed fallback instead.");
			}
		}
	}

	// Token: 0x06001BD0 RID: 7120 RVA: 0x0009BB50 File Offset: 0x00099D50
	public static bool IsUsingNativeBackend()
	{
		return global::Noise.native;
	}

	// Token: 0x04001659 RID: 5721
	public const float MIN = -1E+09f;

	// Token: 0x0400165A RID: 5722
	public const float MAX = 1E+09f;

	// Token: 0x0400165B RID: 5723
	private static bool native;
}
