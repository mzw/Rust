﻿using System;
using UnityEngine;

// Token: 0x0200057B RID: 1403
public class TerrainSplatMap : global::TerrainMap3D<byte>
{
	// Token: 0x06001D70 RID: 7536 RVA: 0x000A494C File Offset: 0x000A2B4C
	public override void Setup()
	{
		this.res = this.terrain.terrainData.alphamapResolution;
		this.num = this.config.Splats.Length;
		this.src = (this.dst = new byte[this.num, this.res, this.res]);
		if (this.SplatTexture0 != null)
		{
			if (this.SplatTexture0.width == this.SplatTexture0.height && this.SplatTexture0.width == this.res)
			{
				Color32[] pixels = this.SplatTexture0.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						Color32 color = pixels[num];
						if (this.num > 0)
						{
							this.dst[0, i, j] = color.r;
						}
						if (this.num > 1)
						{
							this.dst[1, i, j] = color.g;
						}
						if (this.num > 2)
						{
							this.dst[2, i, j] = color.b;
						}
						if (this.num > 3)
						{
							this.dst[3, i, j] = color.a;
						}
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid splat texture: " + this.SplatTexture0.name, this.SplatTexture0);
			}
		}
		if (this.SplatTexture1 != null)
		{
			if (this.SplatTexture1.width == this.SplatTexture1.height && this.SplatTexture1.width == this.res && this.num > 5)
			{
				Color32[] pixels2 = this.SplatTexture1.GetPixels32();
				int k = 0;
				int num2 = 0;
				while (k < this.res)
				{
					int l = 0;
					while (l < this.res)
					{
						Color32 color2 = pixels2[num2];
						if (this.num > 4)
						{
							this.dst[4, k, l] = color2.r;
						}
						if (this.num > 5)
						{
							this.dst[5, k, l] = color2.g;
						}
						if (this.num > 6)
						{
							this.dst[6, k, l] = color2.b;
						}
						if (this.num > 7)
						{
							this.dst[7, k, l] = color2.a;
						}
						l++;
						num2++;
					}
					k++;
				}
			}
			else
			{
				Debug.LogError("Invalid splat texture: " + this.SplatTexture1.name, this.SplatTexture1);
			}
		}
	}

	// Token: 0x06001D71 RID: 7537 RVA: 0x000A4C44 File Offset: 0x000A2E44
	public void GenerateTextures()
	{
		this.SplatTexture0 = new Texture2D(this.res, this.res, 4, true, true);
		this.SplatTexture0.name = "SplatTexture0";
		this.SplatTexture0.wrapMode = 1;
		Color32[] cols2 = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				byte b = (this.num <= 0) ? 0 : this.src[0, z, i];
				byte b2 = (this.num <= 1) ? 0 : this.src[1, z, i];
				byte b3 = (this.num <= 2) ? 0 : this.src[2, z, i];
				byte b4 = (this.num <= 3) ? 0 : this.src[3, z, i];
				cols2[z * this.res + i] = new Color32(b, b2, b3, b4);
			}
		});
		this.SplatTexture0.SetPixels32(cols2);
		this.SplatTexture1 = new Texture2D(this.res, this.res, 4, true, true);
		this.SplatTexture1.name = "SplatTexture1";
		this.SplatTexture1.wrapMode = 1;
		Color32[] cols = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				byte b = (this.num <= 4) ? 0 : this.src[4, z, i];
				byte b2 = (this.num <= 5) ? 0 : this.src[5, z, i];
				byte b3 = (this.num <= 6) ? 0 : this.src[6, z, i];
				byte b4 = (this.num <= 7) ? 0 : this.src[7, z, i];
				cols[z * this.res + i] = new Color32(b, b2, b3, b4);
			}
		});
		this.SplatTexture1.SetPixels32(cols);
	}

	// Token: 0x06001D72 RID: 7538 RVA: 0x000A4D5C File Offset: 0x000A2F5C
	public void ApplyTextures()
	{
		this.SplatTexture0.Apply(true, true);
		this.SplatTexture1.Apply(true, true);
	}

	// Token: 0x06001D73 RID: 7539 RVA: 0x000A4D78 File Offset: 0x000A2F78
	public float GetSplatMax(Vector3 worldPos, int mask = -1)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetSplatMax(normX, normZ, mask);
	}

	// Token: 0x06001D74 RID: 7540 RVA: 0x000A4DA8 File Offset: 0x000A2FA8
	public float GetSplatMax(float normX, float normZ, int mask = -1)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetSplatMax(x, z, mask);
	}

	// Token: 0x06001D75 RID: 7541 RVA: 0x000A4DD0 File Offset: 0x000A2FD0
	public float GetSplatMax(int x, int z, int mask = -1)
	{
		byte b = 0;
		for (int i = 0; i < this.num; i++)
		{
			if ((global::TerrainSplat.IndexToType(i) & mask) != 0)
			{
				byte b2 = this.src[i, z, x];
				if (b2 >= b)
				{
					b = b2;
				}
			}
		}
		return global::TextureData.Byte2Float((int)b);
	}

	// Token: 0x06001D76 RID: 7542 RVA: 0x000A4E2C File Offset: 0x000A302C
	public int GetSplatMaxIndex(Vector3 worldPos, int mask = -1)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetSplatMaxIndex(normX, normZ, mask);
	}

	// Token: 0x06001D77 RID: 7543 RVA: 0x000A4E5C File Offset: 0x000A305C
	public int GetSplatMaxIndex(float normX, float normZ, int mask = -1)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetSplatMaxIndex(x, z, mask);
	}

	// Token: 0x06001D78 RID: 7544 RVA: 0x000A4E84 File Offset: 0x000A3084
	public int GetSplatMaxIndex(int x, int z, int mask = -1)
	{
		byte b = 0;
		int result = 0;
		for (int i = 0; i < this.num; i++)
		{
			int num = global::TerrainSplat.IndexToType(i);
			if ((num & mask) != 0)
			{
				byte b2 = this.src[i, z, x];
				if (b2 >= b)
				{
					b = b2;
					result = i;
				}
			}
		}
		return result;
	}

	// Token: 0x06001D79 RID: 7545 RVA: 0x000A4EE4 File Offset: 0x000A30E4
	public int GetSplatMaxType(Vector3 worldPos, int mask = -1)
	{
		return global::TerrainSplat.IndexToType(this.GetSplatMaxIndex(worldPos, mask));
	}

	// Token: 0x06001D7A RID: 7546 RVA: 0x000A4EF4 File Offset: 0x000A30F4
	public int GetSplatMaxType(float normX, float normZ, int mask = -1)
	{
		return global::TerrainSplat.IndexToType(this.GetSplatMaxIndex(normX, normZ, mask));
	}

	// Token: 0x06001D7B RID: 7547 RVA: 0x000A4F04 File Offset: 0x000A3104
	public int GetSplatMaxType(int x, int z, int mask = -1)
	{
		return global::TerrainSplat.IndexToType(this.GetSplatMaxIndex(x, z, mask));
	}

	// Token: 0x06001D7C RID: 7548 RVA: 0x000A4F14 File Offset: 0x000A3114
	public float GetSplat(Vector3 worldPos, int mask)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetSplat(normX, normZ, mask);
	}

	// Token: 0x06001D7D RID: 7549 RVA: 0x000A4F44 File Offset: 0x000A3144
	public float GetSplat(float normX, float normZ, int mask)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		float num6 = Mathf.Lerp(this.GetSplat(num4, num5, mask), this.GetSplat(x, num5, mask), num2 - (float)num4);
		float num7 = Mathf.Lerp(this.GetSplat(num4, z, mask), this.GetSplat(x, z, mask), num2 - (float)num4);
		return Mathf.Lerp(num6, num7, num3 - (float)num5);
	}

	// Token: 0x06001D7E RID: 7550 RVA: 0x000A4FE0 File Offset: 0x000A31E0
	public float GetSplat(int x, int z, int mask)
	{
		if (Mathf.IsPowerOfTwo(mask))
		{
			return global::TextureData.Byte2Float((int)this.src[global::TerrainSplat.TypeToIndex(mask), z, x]);
		}
		int num = 0;
		for (int i = 0; i < this.num; i++)
		{
			if ((global::TerrainSplat.IndexToType(i) & mask) != 0)
			{
				num += (int)this.src[i, z, x];
			}
		}
		return Mathf.Clamp01(global::TextureData.Byte2Float(num));
	}

	// Token: 0x06001D7F RID: 7551 RVA: 0x000A5058 File Offset: 0x000A3258
	public void SetSplat(Vector3 worldPos, int id)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetSplat(normX, normZ, id);
	}

	// Token: 0x06001D80 RID: 7552 RVA: 0x000A5088 File Offset: 0x000A3288
	public void SetSplat(float normX, float normZ, int id)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetSplat(x, z, id);
	}

	// Token: 0x06001D81 RID: 7553 RVA: 0x000A50B0 File Offset: 0x000A32B0
	public void SetSplat(int x, int z, int id)
	{
		int num = global::TerrainSplat.TypeToIndex(id);
		for (int i = 0; i < this.num; i++)
		{
			if (i == num)
			{
				this.dst[i, z, x] = byte.MaxValue;
			}
			else
			{
				this.dst[i, z, x] = 0;
			}
		}
	}

	// Token: 0x06001D82 RID: 7554 RVA: 0x000A510C File Offset: 0x000A330C
	public void SetSplat(Vector3 worldPos, int id, float v)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetSplat(normX, normZ, id, v);
	}

	// Token: 0x06001D83 RID: 7555 RVA: 0x000A5140 File Offset: 0x000A3340
	public void SetSplat(float normX, float normZ, int id, float v)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetSplat(x, z, id, v);
	}

	// Token: 0x06001D84 RID: 7556 RVA: 0x000A5168 File Offset: 0x000A3368
	public void SetSplat(int x, int z, int id, float v)
	{
		this.SetSplat(x, z, id, this.GetSplat(x, z, id), v);
	}

	// Token: 0x06001D85 RID: 7557 RVA: 0x000A5180 File Offset: 0x000A3380
	public void SetSplatRaw(int x, int z, Vector4 v1, Vector4 v2, float opacity)
	{
		if (opacity == 0f)
		{
			return;
		}
		float num = Mathf.Clamp01(v1.x + v1.y + v1.z + v1.w + v2.x + v2.y + v2.z + v2.w);
		if (num == 0f)
		{
			return;
		}
		float num2 = 1f - opacity * num;
		if (num2 == 0f && opacity == 1f)
		{
			this.dst[0, z, x] = global::TextureData.Float2Byte(v1.x);
			this.dst[1, z, x] = global::TextureData.Float2Byte(v1.y);
			this.dst[2, z, x] = global::TextureData.Float2Byte(v1.z);
			this.dst[3, z, x] = global::TextureData.Float2Byte(v1.w);
			this.dst[4, z, x] = global::TextureData.Float2Byte(v2.x);
			this.dst[5, z, x] = global::TextureData.Float2Byte(v2.y);
			this.dst[6, z, x] = global::TextureData.Float2Byte(v2.z);
			this.dst[7, z, x] = global::TextureData.Float2Byte(v2.w);
		}
		else
		{
			this.dst[0, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[0, z, x]) * num2 + v1.x * opacity);
			this.dst[1, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[1, z, x]) * num2 + v1.y * opacity);
			this.dst[2, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[2, z, x]) * num2 + v1.z * opacity);
			this.dst[3, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[3, z, x]) * num2 + v1.w * opacity);
			this.dst[4, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[4, z, x]) * num2 + v2.x * opacity);
			this.dst[5, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[5, z, x]) * num2 + v2.y * opacity);
			this.dst[6, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[6, z, x]) * num2 + v2.z * opacity);
			this.dst[7, z, x] = global::TextureData.Float2Byte(global::TextureData.Byte2Float((int)this.src[7, z, x]) * num2 + v2.w * opacity);
		}
	}

	// Token: 0x06001D86 RID: 7558 RVA: 0x000A5474 File Offset: 0x000A3674
	public void SetSplat(Vector3 worldPos, int id, float opacity, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetSplat(normX, normZ, id, opacity, radius, fade);
	}

	// Token: 0x06001D87 RID: 7559 RVA: 0x000A54AC File Offset: 0x000A36AC
	public void SetSplat(float normX, float normZ, int id, float opacity, float radius, float fade = 0f)
	{
		int idx = global::TerrainSplat.TypeToIndex(id);
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if (lerp > 0f)
			{
				float num = (float)this.dst[idx, z, x];
				float new_val = Mathf.Lerp(num, 1f, lerp * opacity);
				this.SetSplat(x, z, id, num, new_val);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x06001D88 RID: 7560 RVA: 0x000A5500 File Offset: 0x000A3700
	public void AddSplat(Vector3 worldPos, int id, float delta, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.AddSplat(normX, normZ, id, delta, radius, fade);
	}

	// Token: 0x06001D89 RID: 7561 RVA: 0x000A5538 File Offset: 0x000A3738
	public void AddSplat(float normX, float normZ, int id, float delta, float radius, float fade = 0f)
	{
		int idx = global::TerrainSplat.TypeToIndex(id);
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if (lerp > 0f)
			{
				float num = (float)this.dst[idx, z, x];
				float new_val = Mathf.Clamp01(num + lerp * delta);
				this.SetSplat(x, z, id, num, new_val);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x06001D8A RID: 7562 RVA: 0x000A558C File Offset: 0x000A378C
	private void SetSplat(int x, int z, int id, float old_val, float new_val)
	{
		int num = global::TerrainSplat.TypeToIndex(id);
		if (old_val >= 1f)
		{
			return;
		}
		float num2 = (1f - new_val) / (1f - old_val);
		for (int i = 0; i < this.num; i++)
		{
			if (i == num)
			{
				this.dst[i, z, x] = global::TextureData.Float2Byte(new_val);
			}
			else
			{
				this.dst[i, z, x] = global::TextureData.Float2Byte(num2 * global::TextureData.Byte2Float((int)this.dst[i, z, x]));
			}
		}
	}

	// Token: 0x04001839 RID: 6201
	public Texture2D SplatTexture0;

	// Token: 0x0400183A RID: 6202
	public Texture2D SplatTexture1;

	// Token: 0x0400183B RID: 6203
	internal int num;
}
