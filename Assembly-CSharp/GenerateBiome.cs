﻿using System;
using UnityEngine;

// Token: 0x02000596 RID: 1430
public class GenerateBiome : global::ProceduralComponent
{
	// Token: 0x06001E25 RID: 7717 RVA: 0x000A7730 File Offset: 0x000A5930
	public override void Process(uint seed)
	{
		int res = global::TerrainMeta.BiomeMap.res;
		byte[,,] map = global::TerrainMeta.BiomeMap.dst;
		global::TerrainBiomeMap biomemap = global::TerrainMeta.BiomeMap;
		global::TerrainHeightMap heightmap = global::TerrainMeta.HeightMap;
		float noiseX = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float noiseZ = SeedRandom.Range(ref seed, -1E+09f, 1E+09f);
		float lootAngle = global::TerrainMeta.LootAxisAngle;
		float biomeAngle = global::TerrainMeta.BiomeAxisAngle;
		Parallel.For(0, res, delegate(int z)
		{
			for (int i = 0; i < res; i++)
			{
				float num = biomemap.Coordinate(i);
				float num2 = biomemap.Coordinate(z);
				float num3 = global::TerrainMeta.DenormalizeX(num);
				float num4 = global::TerrainMeta.DenormalizeZ(num2);
				float height = heightmap.GetHeight01(num, num2);
				float num5 = global::Noise.Turbulence((double)(num3 + noiseX), (double)(num4 + noiseZ), 6, 0.0099999997764825821, 0.0099999997764825821, 2.0, 0.5);
				Vector2 vector;
				vector..ctor(num * 2f - 1f, num2 * 2f - 1f);
				float num6 = (Vector2Ex.Rotate(vector, lootAngle).y + 1f) * 0.5f;
				float num7 = (Vector2Ex.Rotate(vector, biomeAngle).y + 1f) * 0.5f;
				float num8 = Mathf.InverseLerp(0.35f, 0.25f, num6);
				float num9 = Mathf.Lerp(num7, 0.475000024f, num8);
				float num10 = 4f * Mathf.Clamp01(height + num5 - 0.525f);
				float num11 = Mathf.Clamp01(num9 + num10);
				if (num11 <= 0.450000018f)
				{
					float num12 = Mathf.InverseLerp(0.450000018f, 0.35f, num11);
					map[0, z, i] = global::TextureData.Float2Byte(num12);
					map[1, z, i] = global::TextureData.Float2Byte(1f - num12);
				}
				else if (num11 <= 0.6f)
				{
					float num13 = Mathf.InverseLerp(0.6f, 0.5f, num11);
					map[1, z, i] = global::TextureData.Float2Byte(num13);
					map[2, z, i] = global::TextureData.Float2Byte(1f - num13);
				}
				else
				{
					float num14 = Mathf.InverseLerp(0.7f, 0.599999964f, num11);
					map[2, z, i] = global::TextureData.Float2Byte(num14);
					map[3, z, i] = global::TextureData.Float2Byte(1f - num14);
				}
			}
		});
	}
}
