﻿using System;
using UnityEngine.UI;

// Token: 0x020006BE RID: 1726
public class PaperDollSegment : global::BaseMonoBehaviour
{
	// Token: 0x04001D35 RID: 7477
	public static global::HitArea selectedAreas;

	// Token: 0x04001D36 RID: 7478
	[InspectorFlags]
	public global::HitArea area;

	// Token: 0x04001D37 RID: 7479
	public Image overlayImg;
}
