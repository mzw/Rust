﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200070E RID: 1806
public class ToggleTerrainRenderer : MonoBehaviour
{
	// Token: 0x06002256 RID: 8790 RVA: 0x000C08A0 File Offset: 0x000BEAA0
	protected void OnEnable()
	{
		if (Terrain.activeTerrain)
		{
			this.toggleControl.isOn = Terrain.activeTerrain.drawHeightmap;
		}
	}

	// Token: 0x06002257 RID: 8791 RVA: 0x000C08C8 File Offset: 0x000BEAC8
	public void OnToggleChanged()
	{
		if (Terrain.activeTerrain)
		{
			Terrain.activeTerrain.drawHeightmap = this.toggleControl.isOn;
		}
	}

	// Token: 0x06002258 RID: 8792 RVA: 0x000C08F0 File Offset: 0x000BEAF0
	protected void OnValidate()
	{
		if (this.textControl)
		{
			this.textControl.text = "Terrain Renderer";
		}
	}

	// Token: 0x04001ECC RID: 7884
	public Toggle toggleControl;

	// Token: 0x04001ECD RID: 7885
	public Text textControl;
}
