﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x02000332 RID: 818
public abstract class BaseScreenShake : MonoBehaviour
{
	// Token: 0x060013BA RID: 5050 RVA: 0x00073B54 File Offset: 0x00071D54
	public static void Apply(Camera cam, global::BaseViewModel vm)
	{
		global::CachedTransform<Camera> cachedTransform = new global::CachedTransform<Camera>(cam);
		global::CachedTransform<global::BaseViewModel> cachedTransform2 = new global::CachedTransform<global::BaseViewModel>(vm);
		for (int i = 0; i < global::BaseScreenShake.list.Count; i++)
		{
			global::BaseScreenShake.list[i].Run(ref cachedTransform, ref cachedTransform2);
		}
		cachedTransform.Apply();
		cachedTransform2.Apply();
	}

	// Token: 0x060013BB RID: 5051 RVA: 0x00073BB0 File Offset: 0x00071DB0
	protected void OnEnable()
	{
		global::BaseScreenShake.list.Add(this);
		this.timeTaken = 0f;
		this.Setup();
	}

	// Token: 0x060013BC RID: 5052 RVA: 0x00073BD0 File Offset: 0x00071DD0
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		global::BaseScreenShake.list.Remove(this);
	}

	// Token: 0x060013BD RID: 5053 RVA: 0x00073BEC File Offset: 0x00071DEC
	public void Run(ref global::CachedTransform<Camera> cam, ref global::CachedTransform<global::BaseViewModel> vm)
	{
		if (this.timeTaken > this.length)
		{
			return;
		}
		if (Time.frameCount != this.currentFrame)
		{
			this.timeTaken += Time.deltaTime;
			this.currentFrame = Time.frameCount;
		}
		float delta = Mathf.InverseLerp(0f, this.length, this.timeTaken);
		this.Run(delta, ref cam, ref vm);
	}

	// Token: 0x060013BE RID: 5054
	public abstract void Setup();

	// Token: 0x060013BF RID: 5055
	public abstract void Run(float delta, ref global::CachedTransform<Camera> cam, ref global::CachedTransform<global::BaseViewModel> vm);

	// Token: 0x04000EA3 RID: 3747
	public static List<global::BaseScreenShake> list = new List<global::BaseScreenShake>();

	// Token: 0x04000EA4 RID: 3748
	public float length = 2f;

	// Token: 0x04000EA5 RID: 3749
	internal float timeTaken;

	// Token: 0x04000EA6 RID: 3750
	private int currentFrame = -1;
}
