﻿using System;
using UnityEngine;

// Token: 0x020000D3 RID: 211
public class MapMarkerExplosion : global::MapMarker
{
	// Token: 0x06000AFD RID: 2813 RVA: 0x0004A0CC File Offset: 0x000482CC
	public void SetDuration(float newDuration)
	{
		this.duration = newDuration;
		if (base.IsInvoking(new Action(this.DelayedDestroy)))
		{
			base.CancelInvoke(new Action(this.DelayedDestroy));
		}
		base.Invoke(new Action(this.DelayedDestroy), this.duration * 60f);
	}

	// Token: 0x06000AFE RID: 2814 RVA: 0x0004A128 File Offset: 0x00048328
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.fromDisk)
		{
			Debug.LogWarning("Loaded explosion marker from disk, cleaning up");
			base.Invoke(new Action(this.DelayedDestroy), 3f);
		}
	}

	// Token: 0x06000AFF RID: 2815 RVA: 0x0004A160 File Offset: 0x00048360
	public void DelayedDestroy()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x040005AF RID: 1455
	private float duration = 10f;
}
