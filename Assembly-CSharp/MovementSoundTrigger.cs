﻿using System;
using UnityEngine;

// Token: 0x020001E1 RID: 481
public class MovementSoundTrigger : global::TriggerBase, global::IClientComponentEx, global::ILOD
{
	// Token: 0x06000F1A RID: 3866 RVA: 0x0005CFEC File Offset: 0x0005B1EC
	public virtual void PreClientComponentCull(global::IPrefabProcessor p)
	{
		p.RemoveComponent(this.collider);
		p.RemoveComponent(this);
		p.NominateForDeletion(base.gameObject);
	}

	// Token: 0x0400095A RID: 2394
	public global::SoundDefinition softSound;

	// Token: 0x0400095B RID: 2395
	public global::SoundDefinition medSound;

	// Token: 0x0400095C RID: 2396
	public global::SoundDefinition hardSound;

	// Token: 0x0400095D RID: 2397
	public Collider collider;
}
