﻿using System;
using ProtoBuf;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000694 RID: 1684
public class UIMapVendingMachineMarker : MonoBehaviour
{
	// Token: 0x06002115 RID: 8469 RVA: 0x000BB680 File Offset: 0x000B9880
	public void SetOutOfStock(bool stock)
	{
		this.colorBackground.color = ((!stock) ? this.outOfStock : this.inStock);
		this.isInStock = stock;
	}

	// Token: 0x06002116 RID: 8470 RVA: 0x000BB6AC File Offset: 0x000B98AC
	public void UpdateDisplayName(string newName, ProtoBuf.VendingMachine.SellOrderContainer sellOrderContainer)
	{
		this.displayName = newName;
		this.toolTip.Text = this.displayName;
		if (this.isInStock && sellOrderContainer != null && sellOrderContainer.sellOrders != null && sellOrderContainer.sellOrders.Count > 0)
		{
			global::Tooltip tooltip = this.toolTip;
			tooltip.Text += "\n";
			foreach (ProtoBuf.VendingMachine.SellOrder sellOrder in sellOrderContainer.sellOrders)
			{
				if (sellOrder.inStock > 0)
				{
					string text = global::ItemManager.FindItemDefinition(sellOrder.itemToSellID).displayName.translated + ((!sellOrder.itemToSellIsBP) ? string.Empty : " (BP)");
					string text2 = global::ItemManager.FindItemDefinition(sellOrder.currencyID).displayName.translated + ((!sellOrder.currencyIsBP) ? string.Empty : " (BP)");
					global::Tooltip tooltip2 = this.toolTip;
					string text3 = tooltip2.Text;
					tooltip2.Text = string.Concat(new object[]
					{
						text3,
						"\n",
						sellOrder.itemToSellAmount,
						" ",
						text,
						" | ",
						sellOrder.currencyAmountPerItem,
						" ",
						text2
					});
					global::Tooltip tooltip3 = this.toolTip;
					text3 = tooltip3.Text;
					tooltip3.Text = string.Concat(new object[]
					{
						text3,
						" (",
						sellOrder.inStock,
						" Left)"
					});
				}
			}
		}
		this.toolTip.enabled = (this.toolTip.Text != string.Empty);
	}

	// Token: 0x04001C7B RID: 7291
	public Color inStock;

	// Token: 0x04001C7C RID: 7292
	public Color outOfStock;

	// Token: 0x04001C7D RID: 7293
	public Image colorBackground;

	// Token: 0x04001C7E RID: 7294
	public string displayName;

	// Token: 0x04001C7F RID: 7295
	public global::Tooltip toolTip;

	// Token: 0x04001C80 RID: 7296
	private bool isInStock;
}
