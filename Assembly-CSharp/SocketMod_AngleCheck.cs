﻿using System;
using UnityEngine;

// Token: 0x02000224 RID: 548
public class SocketMod_AngleCheck : global::SocketMod
{
	// Token: 0x06000FE0 RID: 4064 RVA: 0x00060A80 File Offset: 0x0005EC80
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.yellow;
		Gizmos.DrawFrustum(Vector3.zero, this.withinDegrees, 1f, 0f, 1f);
	}

	// Token: 0x06000FE1 RID: 4065 RVA: 0x00060ABC File Offset: 0x0005ECBC
	public override bool DoCheck(global::Construction.Placement place)
	{
		float num = Vector3Ex.DotDegrees(this.worldNormal, place.rotation * Vector3.up);
		if (num < this.withinDegrees)
		{
			return true;
		}
		global::Construction.lastPlacementError = "Failed Check: AngleCheck (" + this.hierachyName + ")";
		return false;
	}

	// Token: 0x04000A99 RID: 2713
	public bool wantsAngle = true;

	// Token: 0x04000A9A RID: 2714
	public Vector3 worldNormal = Vector3.up;

	// Token: 0x04000A9B RID: 2715
	public float withinDegrees = 45f;
}
