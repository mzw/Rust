﻿using System;
using UnityEngine;
using UnityEngine.Audio;

// Token: 0x020001E0 RID: 480
public class MixerSnapshotManager : MonoBehaviour
{
	// Token: 0x04000952 RID: 2386
	public AudioMixerSnapshot defaultSnapshot;

	// Token: 0x04000953 RID: 2387
	public AudioMixerSnapshot underwaterSnapshot;

	// Token: 0x04000954 RID: 2388
	public AudioMixerSnapshot loadingSnapshot;

	// Token: 0x04000955 RID: 2389
	public AudioMixerSnapshot woundedSnapshot;

	// Token: 0x04000956 RID: 2390
	public global::SoundDefinition underwaterInSound;

	// Token: 0x04000957 RID: 2391
	public global::SoundDefinition underwaterOutSound;

	// Token: 0x04000958 RID: 2392
	public global::SoundDefinition woundedLoop;

	// Token: 0x04000959 RID: 2393
	private global::Sound woundedLoopSound;
}
