﻿using System;
using System.Linq;
using UnityEngine;

// Token: 0x02000646 RID: 1606
public class ItemSkinDirectory : ScriptableObject
{
	// Token: 0x1700023D RID: 573
	// (get) Token: 0x06002065 RID: 8293 RVA: 0x000B9510 File Offset: 0x000B7710
	public static global::ItemSkinDirectory Instance
	{
		get
		{
			if (global::ItemSkinDirectory._Instance == null)
			{
				global::ItemSkinDirectory._Instance = global::FileSystem.Load<global::ItemSkinDirectory>("assets/skins.asset", true);
			}
			return global::ItemSkinDirectory._Instance;
		}
	}

	// Token: 0x06002066 RID: 8294 RVA: 0x000B9538 File Offset: 0x000B7738
	public static global::ItemSkinDirectory.Skin[] ForItem(global::ItemDefinition item)
	{
		return (from x in global::ItemSkinDirectory.Instance.skins
		where x.isSkin && x.itemid == item.itemid
		select x).ToArray<global::ItemSkinDirectory.Skin>();
	}

	// Token: 0x06002067 RID: 8295 RVA: 0x000B9574 File Offset: 0x000B7774
	public static global::ItemSkinDirectory.Skin FindByInventoryDefinitionId(int id)
	{
		return (from x in global::ItemSkinDirectory.Instance.skins
		where x.id == id
		select x).FirstOrDefault<global::ItemSkinDirectory.Skin>();
	}

	// Token: 0x04001B54 RID: 6996
	private static global::ItemSkinDirectory _Instance;

	// Token: 0x04001B55 RID: 6997
	public global::ItemSkinDirectory.Skin[] skins;

	// Token: 0x02000647 RID: 1607
	[Serializable]
	public struct Skin
	{
		// Token: 0x1700023E RID: 574
		// (get) Token: 0x06002068 RID: 8296 RVA: 0x000B95B0 File Offset: 0x000B77B0
		public global::SteamInventoryItem invItem
		{
			get
			{
				if (this._invItem == null)
				{
					this._invItem = global::FileSystem.Load<global::SteamInventoryItem>(this.name, true);
				}
				return this._invItem;
			}
		}

		// Token: 0x04001B56 RID: 6998
		public int id;

		// Token: 0x04001B57 RID: 6999
		public int itemid;

		// Token: 0x04001B58 RID: 7000
		public string name;

		// Token: 0x04001B59 RID: 7001
		public bool isSkin;

		// Token: 0x04001B5A RID: 7002
		private global::SteamInventoryItem _invItem;
	}
}
