﻿using System;
using System.Collections.Generic;
using Facepunch;
using Rust;
using UnityEngine;

// Token: 0x020004EE RID: 1262
public class ItemModProjectileRadialDamage : global::ItemModProjectileMod
{
	// Token: 0x06001AFA RID: 6906 RVA: 0x00096D9C File Offset: 0x00094F9C
	public override void ServerProjectileHit(global::HitInfo info)
	{
		if (this.effect.isValid)
		{
			global::Effect.server.Run(this.effect.resourcePath, info.HitPositionWorld, info.HitNormalWorld, null, false);
		}
		List<global::BaseCombatEntity> list = Pool.GetList<global::BaseCombatEntity>();
		List<global::BaseCombatEntity> list2 = Pool.GetList<global::BaseCombatEntity>();
		global::Vis.Entities<global::BaseCombatEntity>(info.HitPositionWorld, this.radius, list2, 1101212417, 2);
		foreach (global::BaseCombatEntity baseCombatEntity in list2)
		{
			if (baseCombatEntity.isServer)
			{
				if (!list.Contains(baseCombatEntity))
				{
					if (!(baseCombatEntity == info.HitEntity) || !this.ignoreHitObject)
					{
						Vector3 vector = baseCombatEntity.ClosestPoint(info.HitPositionWorld);
						float num = Vector3.Distance(vector, info.HitPositionWorld);
						float num2 = num / this.radius;
						if (num2 <= 1f)
						{
							float num3 = 1f - num2;
							if (baseCombatEntity.IsVisible(info.HitPositionWorld + info.HitNormalWorld * 0.1f))
							{
								list.Add(baseCombatEntity);
								baseCombatEntity.OnAttacked(new global::HitInfo(info.Initiator, baseCombatEntity, this.damage.type, this.damage.amount * num3));
							}
						}
					}
				}
			}
		}
		Pool.FreeList<global::BaseCombatEntity>(ref list);
		Pool.FreeList<global::BaseCombatEntity>(ref list2);
	}

	// Token: 0x040015BD RID: 5565
	public float radius = 0.5f;

	// Token: 0x040015BE RID: 5566
	public Rust.DamageTypeEntry damage;

	// Token: 0x040015BF RID: 5567
	public global::GameObjectRef effect;

	// Token: 0x040015C0 RID: 5568
	public bool ignoreHitObject = true;
}
