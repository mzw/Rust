﻿using System;
using System.IO;
using System.Threading;

// Token: 0x02000766 RID: 1894
public static class DirectoryEx
{
	// Token: 0x0600234B RID: 9035 RVA: 0x000C3440 File Offset: 0x000C1640
	public static void Backup(DirectoryInfo parent, params string[] names)
	{
		for (int i = 0; i < names.Length; i++)
		{
			names[i] = Path.Combine(parent.FullName, names[i]);
		}
		global::DirectoryEx.Backup(names);
	}

	// Token: 0x0600234C RID: 9036 RVA: 0x000C3478 File Offset: 0x000C1678
	public static bool MoveToSafe(this DirectoryInfo parent, string target, int retries = 10)
	{
		for (int i = 0; i < retries; i++)
		{
			try
			{
				parent.MoveTo(target);
			}
			catch (Exception)
			{
				Thread.Sleep(5);
				goto IL_21;
			}
			return true;
			IL_21:;
		}
		return false;
	}

	// Token: 0x0600234D RID: 9037 RVA: 0x000C34C4 File Offset: 0x000C16C4
	public static void Backup(params string[] names)
	{
		for (int i = names.Length - 2; i >= 0; i--)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(names[i]);
			DirectoryInfo directoryInfo2 = new DirectoryInfo(names[i + 1]);
			if (directoryInfo.Exists)
			{
				if (directoryInfo2.Exists)
				{
					double totalHours = (DateTime.Now - directoryInfo2.LastWriteTime).TotalHours;
					int num = (i != 0) ? (1 << i - 1) : 0;
					if (totalHours >= (double)num)
					{
						directoryInfo2.Delete(true);
						directoryInfo.MoveToSafe(directoryInfo2.FullName, 10);
					}
				}
				else
				{
					if (!directoryInfo2.Parent.Exists)
					{
						directoryInfo2.Parent.Create();
					}
					directoryInfo.MoveToSafe(directoryInfo2.FullName, 10);
				}
			}
		}
	}

	// Token: 0x0600234E RID: 9038 RVA: 0x000C3594 File Offset: 0x000C1794
	public static void CopyAll(string sourceDirectory, string targetDirectory)
	{
		DirectoryInfo source = new DirectoryInfo(sourceDirectory);
		DirectoryInfo target = new DirectoryInfo(targetDirectory);
		global::DirectoryEx.CopyAll(source, target);
	}

	// Token: 0x0600234F RID: 9039 RVA: 0x000C35B8 File Offset: 0x000C17B8
	public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
	{
		if (source.FullName.ToLower() == target.FullName.ToLower())
		{
			return;
		}
		if (!source.Exists)
		{
			return;
		}
		if (!target.Exists)
		{
			target.Create();
		}
		foreach (FileInfo fileInfo in source.GetFiles())
		{
			FileInfo fileInfo2 = new FileInfo(Path.Combine(target.FullName, fileInfo.Name));
			fileInfo.CopyTo(fileInfo2.FullName, true);
			fileInfo2.CreationTime = fileInfo.CreationTime;
			fileInfo2.LastAccessTime = fileInfo.LastAccessTime;
			fileInfo2.LastWriteTime = fileInfo.LastWriteTime;
		}
		foreach (DirectoryInfo directoryInfo in source.GetDirectories())
		{
			DirectoryInfo directoryInfo2 = target.CreateSubdirectory(directoryInfo.Name);
			global::DirectoryEx.CopyAll(directoryInfo, directoryInfo2);
			directoryInfo2.CreationTime = directoryInfo.CreationTime;
			directoryInfo2.LastAccessTime = directoryInfo.LastAccessTime;
			directoryInfo2.LastWriteTime = directoryInfo.LastWriteTime;
		}
	}
}
