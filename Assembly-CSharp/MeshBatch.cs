﻿using System;
using Rust;
using UnityEngine;

// Token: 0x02000795 RID: 1941
public abstract class MeshBatch : MonoBehaviour
{
	// Token: 0x1700027B RID: 635
	// (get) Token: 0x060023E2 RID: 9186 RVA: 0x000C686C File Offset: 0x000C4A6C
	// (set) Token: 0x060023E3 RID: 9187 RVA: 0x000C6874 File Offset: 0x000C4A74
	public bool NeedsRefresh { get; private set; }

	// Token: 0x1700027C RID: 636
	// (get) Token: 0x060023E4 RID: 9188 RVA: 0x000C6880 File Offset: 0x000C4A80
	// (set) Token: 0x060023E5 RID: 9189 RVA: 0x000C6888 File Offset: 0x000C4A88
	public int Count { get; private set; }

	// Token: 0x1700027D RID: 637
	// (get) Token: 0x060023E6 RID: 9190 RVA: 0x000C6894 File Offset: 0x000C4A94
	// (set) Token: 0x060023E7 RID: 9191 RVA: 0x000C689C File Offset: 0x000C4A9C
	public int BatchedCount { get; private set; }

	// Token: 0x1700027E RID: 638
	// (get) Token: 0x060023E8 RID: 9192 RVA: 0x000C68A8 File Offset: 0x000C4AA8
	// (set) Token: 0x060023E9 RID: 9193 RVA: 0x000C68B0 File Offset: 0x000C4AB0
	public int VertexCount { get; private set; }

	// Token: 0x060023EA RID: 9194
	protected abstract void AllocMemory();

	// Token: 0x060023EB RID: 9195
	protected abstract void FreeMemory();

	// Token: 0x060023EC RID: 9196
	protected abstract void RefreshMesh();

	// Token: 0x060023ED RID: 9197
	protected abstract void ApplyMesh();

	// Token: 0x060023EE RID: 9198
	protected abstract void ToggleMesh(bool state);

	// Token: 0x060023EF RID: 9199
	protected abstract void OnPooled();

	// Token: 0x1700027F RID: 639
	// (get) Token: 0x060023F0 RID: 9200
	public abstract int VertexCapacity { get; }

	// Token: 0x17000280 RID: 640
	// (get) Token: 0x060023F1 RID: 9201
	public abstract int VertexCutoff { get; }

	// Token: 0x17000281 RID: 641
	// (get) Token: 0x060023F2 RID: 9202 RVA: 0x000C68BC File Offset: 0x000C4ABC
	public int AvailableVertices
	{
		get
		{
			return Mathf.Clamp(this.VertexCapacity, this.VertexCutoff, 65534) - this.VertexCount;
		}
	}

	// Token: 0x060023F3 RID: 9203 RVA: 0x000C68DC File Offset: 0x000C4ADC
	public void Alloc()
	{
		this.AllocMemory();
	}

	// Token: 0x060023F4 RID: 9204 RVA: 0x000C68E4 File Offset: 0x000C4AE4
	public void Free()
	{
		this.FreeMemory();
	}

	// Token: 0x060023F5 RID: 9205 RVA: 0x000C68EC File Offset: 0x000C4AEC
	public void Refresh()
	{
		this.RefreshMesh();
	}

	// Token: 0x060023F6 RID: 9206 RVA: 0x000C68F4 File Offset: 0x000C4AF4
	public void Apply()
	{
		this.NeedsRefresh = false;
		this.ApplyMesh();
	}

	// Token: 0x060023F7 RID: 9207 RVA: 0x000C6904 File Offset: 0x000C4B04
	public void Display()
	{
		this.ToggleMesh(true);
		this.BatchedCount = this.Count;
	}

	// Token: 0x060023F8 RID: 9208 RVA: 0x000C691C File Offset: 0x000C4B1C
	public void Invalidate()
	{
		this.ToggleMesh(false);
		this.BatchedCount = 0;
	}

	// Token: 0x060023F9 RID: 9209 RVA: 0x000C692C File Offset: 0x000C4B2C
	protected void AddVertices(int vertices)
	{
		this.NeedsRefresh = true;
		this.Count++;
		this.VertexCount += vertices;
	}

	// Token: 0x060023FA RID: 9210 RVA: 0x000C6954 File Offset: 0x000C4B54
	protected void OnEnable()
	{
		this.NeedsRefresh = false;
		this.Count = 0;
		this.BatchedCount = 0;
		this.VertexCount = 0;
	}

	// Token: 0x060023FB RID: 9211 RVA: 0x000C6974 File Offset: 0x000C4B74
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		this.NeedsRefresh = false;
		this.Count = 0;
		this.BatchedCount = 0;
		this.VertexCount = 0;
		this.OnPooled();
	}
}
