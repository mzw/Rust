﻿using System;
using UnityEngine;

// Token: 0x0200073A RID: 1850
public class MinMaxAttribute : PropertyAttribute
{
	// Token: 0x060022C8 RID: 8904 RVA: 0x000C18CC File Offset: 0x000BFACC
	public MinMaxAttribute(float min, float max)
	{
		this.min = min;
		this.max = max;
	}

	// Token: 0x04001F3B RID: 7995
	public float min;

	// Token: 0x04001F3C RID: 7996
	public float max;
}
