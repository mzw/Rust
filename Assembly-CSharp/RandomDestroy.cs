﻿using System;
using UnityEngine;

// Token: 0x0200054D RID: 1357
public class RandomDestroy : MonoBehaviour
{
	// Token: 0x06001C96 RID: 7318 RVA: 0x000A02F8 File Offset: 0x0009E4F8
	protected void Start()
	{
		uint num = SeedEx.Seed(base.transform.position, global::World.Seed + this.Seed);
		if (SeedRandom.Value(ref num) > this.Probability)
		{
			global::GameManager.Destroy(this, 0f);
		}
		else
		{
			global::GameManager.Destroy(base.gameObject, 0f);
		}
	}

	// Token: 0x040017AA RID: 6058
	public uint Seed;

	// Token: 0x040017AB RID: 6059
	public float Probability = 0.5f;
}
