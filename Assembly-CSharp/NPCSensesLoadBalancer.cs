﻿using System;
using Apex.LoadBalancing;

// Token: 0x02000179 RID: 377
public sealed class NPCSensesLoadBalancer : Apex.LoadBalancing.LoadBalancer
{
	// Token: 0x06000D4D RID: 3405 RVA: 0x000542B0 File Offset: 0x000524B0
	private NPCSensesLoadBalancer()
	{
	}

	// Token: 0x04000751 RID: 1873
	public static readonly ILoadBalancer NpcSensesLoadBalancer = new LoadBalancedQueue(50, 0.1f, 50, 4);
}
