﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rust;
using UnityEngine;

// Token: 0x020002ED RID: 749
public class DirectionalDamageTrigger : global::TriggerBase
{
	// Token: 0x0600130C RID: 4876 RVA: 0x00070124 File Offset: 0x0006E324
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (!(baseEntity is global::BaseCombatEntity))
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x0600130D RID: 4877 RVA: 0x00070180 File Offset: 0x0006E380
	internal override void OnObjects()
	{
		base.InvokeRepeating(new Action(this.OnTick), this.repeatRate, this.repeatRate);
	}

	// Token: 0x0600130E RID: 4878 RVA: 0x000701A0 File Offset: 0x0006E3A0
	internal override void OnEmpty()
	{
		base.CancelInvoke(new Action(this.OnTick));
	}

	// Token: 0x0600130F RID: 4879 RVA: 0x000701B4 File Offset: 0x0006E3B4
	private void OnTick()
	{
		if (this.attackEffect.isValid)
		{
			global::Effect.server.Run(this.attackEffect.resourcePath, base.transform.position, Vector3.up, null, false);
		}
		if (this.entityContents == null)
		{
			return;
		}
		foreach (global::BaseEntity baseEntity in this.entityContents.ToArray<global::BaseEntity>())
		{
			if (baseEntity.IsValid())
			{
				global::BaseCombatEntity baseCombatEntity = baseEntity as global::BaseCombatEntity;
				if (!(baseCombatEntity == null))
				{
					global::HitInfo hitInfo = new global::HitInfo();
					hitInfo.damageTypes.Add(this.damageType);
					hitInfo.DoHitEffects = true;
					hitInfo.DidHit = true;
					hitInfo.PointStart = base.transform.position;
					hitInfo.PointEnd = baseCombatEntity.transform.position;
					baseCombatEntity.Hurt(hitInfo);
				}
			}
		}
	}

	// Token: 0x04000DC5 RID: 3525
	public float repeatRate = 1f;

	// Token: 0x04000DC6 RID: 3526
	public List<Rust.DamageTypeEntry> damageType;

	// Token: 0x04000DC7 RID: 3527
	public global::GameObjectRef attackEffect;
}
