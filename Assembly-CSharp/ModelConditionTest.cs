﻿using System;

// Token: 0x02000216 RID: 534
public abstract class ModelConditionTest : global::PrefabAttribute
{
	// Token: 0x06000FA8 RID: 4008
	public abstract bool DoTest(global::BaseEntity ent);

	// Token: 0x06000FA9 RID: 4009 RVA: 0x0005FB00 File Offset: 0x0005DD00
	protected override Type GetIndexedType()
	{
		return typeof(global::ModelConditionTest);
	}
}
