﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200043A RID: 1082
public static class OnSendNetworkUpdateEx
{
	// Token: 0x0600186C RID: 6252 RVA: 0x0008A66C File Offset: 0x0008886C
	public static void BroadcastOnSendNetworkUpdate(this GameObject go, global::BaseEntity entity)
	{
		List<global::IOnSendNetworkUpdate> list = Pool.GetList<global::IOnSendNetworkUpdate>();
		go.GetComponentsInChildren<global::IOnSendNetworkUpdate>(list);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].OnSendNetworkUpdate(entity);
		}
		Pool.FreeList<global::IOnSendNetworkUpdate>(ref list);
	}

	// Token: 0x0600186D RID: 6253 RVA: 0x0008A6B4 File Offset: 0x000888B4
	public static void SendOnSendNetworkUpdate(this GameObject go, global::BaseEntity entity)
	{
		List<global::IOnSendNetworkUpdate> list = Pool.GetList<global::IOnSendNetworkUpdate>();
		go.GetComponents<global::IOnSendNetworkUpdate>(list);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].OnSendNetworkUpdate(entity);
		}
		Pool.FreeList<global::IOnSendNetworkUpdate>(ref list);
	}
}
