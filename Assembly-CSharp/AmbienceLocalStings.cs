﻿using System;
using UnityEngine;

// Token: 0x020001D0 RID: 464
public class AmbienceLocalStings : MonoBehaviour
{
	// Token: 0x040008DA RID: 2266
	public float maxDistance = 100f;

	// Token: 0x040008DB RID: 2267
	public float stingRadius = 10f;

	// Token: 0x040008DC RID: 2268
	public float stingFrequency = 30f;

	// Token: 0x040008DD RID: 2269
	public float stingFrequencyVariance = 15f;

	// Token: 0x040008DE RID: 2270
	public global::SoundDefinition[] stingSounds;
}
