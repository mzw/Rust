﻿using System;

// Token: 0x020005BD RID: 1469
public class GenerateTextures : global::ProceduralComponent
{
	// Token: 0x06001E7C RID: 7804 RVA: 0x000AAC5C File Offset: 0x000A8E5C
	public override void Process(uint seed)
	{
		if (!global::World.Serialization.Cached)
		{
			global::World.Serialization.AddMap("height", global::TerrainMeta.HeightMap.ToByteArray());
			global::World.Serialization.AddMap("splat", global::TerrainMeta.SplatMap.ToByteArray());
			global::World.Serialization.AddMap("biome", global::TerrainMeta.BiomeMap.ToByteArray());
			global::World.Serialization.AddMap("topology", global::TerrainMeta.TopologyMap.ToByteArray());
			global::World.Serialization.AddMap("alpha", global::TerrainMeta.AlphaMap.ToByteArray());
			global::World.Serialization.AddMap("water", global::TerrainMeta.WaterMap.ToByteArray());
		}
	}

	// Token: 0x1700021B RID: 539
	// (get) Token: 0x06001E7D RID: 7805 RVA: 0x000AAD10 File Offset: 0x000A8F10
	public override bool RunOnCache
	{
		get
		{
			return true;
		}
	}
}
