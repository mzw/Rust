﻿using System;
using UnityEngine;

// Token: 0x02000511 RID: 1297
[Serializable]
public sealed class ByteQuadtree
{
	// Token: 0x06001B70 RID: 7024 RVA: 0x00098EFC File Offset: 0x000970FC
	public void UpdateValues(byte[] baseValues)
	{
		this.size = Mathf.RoundToInt(Mathf.Sqrt((float)baseValues.Length));
		this.levels = Mathf.RoundToInt(Mathf.Log((float)this.size, 2f)) + 1;
		this.values = new global::ByteMap[this.levels];
		this.values[0] = new global::ByteMap(this.size, baseValues, 1);
		for (int i = 1; i < this.levels; i++)
		{
			global::ByteMap byteMap = this.values[i - 1];
			global::ByteMap byteMap2 = this.values[i] = this.CreateLevel(i);
			for (int j = 0; j < byteMap2.Size; j++)
			{
				for (int k = 0; k < byteMap2.Size; k++)
				{
					byteMap2[k, j] = byteMap[2 * k, 2 * j] + byteMap[2 * k + 1, 2 * j] + byteMap[2 * k, 2 * j + 1] + byteMap[2 * k + 1, 2 * j + 1];
				}
			}
		}
	}

	// Token: 0x170001DC RID: 476
	// (get) Token: 0x06001B71 RID: 7025 RVA: 0x0009901C File Offset: 0x0009721C
	public int Size
	{
		get
		{
			return this.size;
		}
	}

	// Token: 0x170001DD RID: 477
	// (get) Token: 0x06001B72 RID: 7026 RVA: 0x00099024 File Offset: 0x00097224
	public global::ByteQuadtree.Element Root
	{
		get
		{
			return new global::ByteQuadtree.Element(this, 0, 0, this.levels - 1);
		}
	}

	// Token: 0x06001B73 RID: 7027 RVA: 0x00099038 File Offset: 0x00097238
	private global::ByteMap CreateLevel(int level)
	{
		int num = 1 << this.levels - level - 1;
		int bytes = 1 + (level + 3) / 4;
		return new global::ByteMap(num, bytes);
	}

	// Token: 0x04001627 RID: 5671
	[SerializeField]
	private int size;

	// Token: 0x04001628 RID: 5672
	[SerializeField]
	private int levels;

	// Token: 0x04001629 RID: 5673
	[SerializeField]
	private global::ByteMap[] values;

	// Token: 0x02000512 RID: 1298
	public struct Element
	{
		// Token: 0x06001B74 RID: 7028 RVA: 0x00099064 File Offset: 0x00097264
		public Element(global::ByteQuadtree source, int x, int y, int level)
		{
			this.source = source;
			this.x = x;
			this.y = y;
			this.level = level;
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x06001B75 RID: 7029 RVA: 0x00099084 File Offset: 0x00097284
		public bool IsLeaf
		{
			get
			{
				return this.level == 0;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06001B76 RID: 7030 RVA: 0x00099090 File Offset: 0x00097290
		public bool IsRoot
		{
			get
			{
				return this.level == this.source.levels - 1;
			}
		}

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06001B77 RID: 7031 RVA: 0x000990A8 File Offset: 0x000972A8
		public int ByteMap
		{
			get
			{
				return this.level;
			}
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06001B78 RID: 7032 RVA: 0x000990B0 File Offset: 0x000972B0
		public uint Value
		{
			get
			{
				return this.source.values[this.level][this.x, this.y];
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06001B79 RID: 7033 RVA: 0x000990D8 File Offset: 0x000972D8
		public Vector2 Coords
		{
			get
			{
				return new Vector2((float)this.x, (float)this.y);
			}
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06001B7A RID: 7034 RVA: 0x000990F0 File Offset: 0x000972F0
		public global::ByteQuadtree.Element Parent
		{
			get
			{
				if (this.IsRoot)
				{
					throw new Exception("Element is the root and therefore has no parent.");
				}
				return new global::ByteQuadtree.Element(this.source, this.x / 2, this.y / 2, this.level + 1);
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06001B7B RID: 7035 RVA: 0x0009912C File Offset: 0x0009732C
		public global::ByteQuadtree.Element Child1
		{
			get
			{
				if (this.IsLeaf)
				{
					throw new Exception("Element is a leaf and therefore has no children.");
				}
				return new global::ByteQuadtree.Element(this.source, this.x * 2, this.y * 2, this.level - 1);
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06001B7C RID: 7036 RVA: 0x00099168 File Offset: 0x00097368
		public global::ByteQuadtree.Element Child2
		{
			get
			{
				if (this.IsLeaf)
				{
					throw new Exception("Element is a leaf and therefore has no children.");
				}
				return new global::ByteQuadtree.Element(this.source, this.x * 2 + 1, this.y * 2, this.level - 1);
			}
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06001B7D RID: 7037 RVA: 0x000991A8 File Offset: 0x000973A8
		public global::ByteQuadtree.Element Child3
		{
			get
			{
				if (this.IsLeaf)
				{
					throw new Exception("Element is a leaf and therefore has no children.");
				}
				return new global::ByteQuadtree.Element(this.source, this.x * 2, this.y * 2 + 1, this.level - 1);
			}
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06001B7E RID: 7038 RVA: 0x000991E8 File Offset: 0x000973E8
		public global::ByteQuadtree.Element Child4
		{
			get
			{
				if (this.IsLeaf)
				{
					throw new Exception("Element is a leaf and therefore has no children.");
				}
				return new global::ByteQuadtree.Element(this.source, this.x * 2 + 1, this.y * 2 + 1, this.level - 1);
			}
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06001B7F RID: 7039 RVA: 0x00099228 File Offset: 0x00097428
		public global::ByteQuadtree.Element MaxChild
		{
			get
			{
				global::ByteQuadtree.Element child = this.Child1;
				global::ByteQuadtree.Element child2 = this.Child2;
				global::ByteQuadtree.Element child3 = this.Child3;
				global::ByteQuadtree.Element child4 = this.Child4;
				uint value = child.Value;
				uint value2 = child2.Value;
				uint value3 = child3.Value;
				uint value4 = child4.Value;
				if (value >= value2 && value >= value3 && value >= value4)
				{
					return child;
				}
				if (value2 >= value3 && value2 >= value4)
				{
					return child2;
				}
				if (value3 >= value4)
				{
					return child3;
				}
				return child4;
			}
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06001B80 RID: 7040 RVA: 0x000992B4 File Offset: 0x000974B4
		public global::ByteQuadtree.Element RandChild
		{
			get
			{
				global::ByteQuadtree.Element child = this.Child1;
				global::ByteQuadtree.Element child2 = this.Child2;
				global::ByteQuadtree.Element child3 = this.Child3;
				global::ByteQuadtree.Element child4 = this.Child4;
				uint value = child.Value;
				uint value2 = child2.Value;
				uint value3 = child3.Value;
				uint value4 = child4.Value;
				float num = value + value2 + value3 + value4;
				float value5 = Random.value;
				if (value / num >= value5)
				{
					return child;
				}
				if ((value + value2) / num >= value5)
				{
					return child2;
				}
				if ((value + value2 + value3) / num >= value5)
				{
					return child3;
				}
				return child4;
			}
		}

		// Token: 0x0400162A RID: 5674
		private global::ByteQuadtree source;

		// Token: 0x0400162B RID: 5675
		private int x;

		// Token: 0x0400162C RID: 5676
		private int y;

		// Token: 0x0400162D RID: 5677
		private int level;
	}
}
