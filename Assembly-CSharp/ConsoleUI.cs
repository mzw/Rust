﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200066F RID: 1647
public class ConsoleUI : SingletonComponent<global::ConsoleUI>
{
	// Token: 0x04001C1A RID: 7194
	public Text text;

	// Token: 0x04001C1B RID: 7195
	public InputField outputField;

	// Token: 0x04001C1C RID: 7196
	public InputField inputField;

	// Token: 0x04001C1D RID: 7197
	public GameObject AutocompleteDropDown;

	// Token: 0x04001C1E RID: 7198
	public GameObject ItemTemplate;

	// Token: 0x04001C1F RID: 7199
	public Color errorColor;

	// Token: 0x04001C20 RID: 7200
	public Color warningColor;

	// Token: 0x04001C21 RID: 7201
	public Color inputColor;
}
