﻿using System;
using UnityEngine;

// Token: 0x02000441 RID: 1089
public class LevelInfo : SingletonComponent<global::LevelInfo>
{
	// Token: 0x04001323 RID: 4899
	public string shortName;

	// Token: 0x04001324 RID: 4900
	public string displayName;

	// Token: 0x04001325 RID: 4901
	[TextArea]
	public string description;

	// Token: 0x04001326 RID: 4902
	[Tooltip("A background image to be shown when loading the map")]
	public Texture2D image;

	// Token: 0x04001327 RID: 4903
	[Space(10f)]
	[Tooltip("You should incrememnt this version when you make changes to the map that will invalidate old saves")]
	public int version = 1;
}
