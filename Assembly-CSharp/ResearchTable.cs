﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200008C RID: 140
public class ResearchTable : global::StorageContainer
{
	// Token: 0x0600092F RID: 2351 RVA: 0x0003E530 File Offset: 0x0003C730
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("ResearchTable.OnRpcMessage", 0.1f))
		{
			if (rpc == 2708607782u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoResearch ");
				}
				using (TimeWarning.New("DoResearch", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoResearch(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoResearch");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000930 RID: 2352 RVA: 0x0003E684 File Offset: 0x0003C884
	public override void ResetState()
	{
		base.ResetState();
		this.researchFinishedTime = 0f;
	}

	// Token: 0x06000931 RID: 2353 RVA: 0x0003E698 File Offset: 0x0003C898
	public bool IsResearching()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000932 RID: 2354 RVA: 0x0003E6A4 File Offset: 0x0003C8A4
	public int RarityMultiplier(Rarity rarity)
	{
		if (rarity == 1)
		{
			return 20;
		}
		if (rarity == 2)
		{
			return 15;
		}
		if (rarity == 3)
		{
			return 10;
		}
		return 5;
	}

	// Token: 0x06000933 RID: 2355 RVA: 0x0003E6C8 File Offset: 0x0003C8C8
	public int GetBlueprintStacksize(global::Item sourceItem)
	{
		int result = this.RarityMultiplier(sourceItem.info.rarity);
		if (sourceItem.info.category == global::ItemCategory.Ammunition)
		{
			result = Mathf.FloorToInt((float)sourceItem.info.stackable / (float)sourceItem.info.Blueprint.amountToCreate) * 2;
		}
		return result;
	}

	// Token: 0x06000934 RID: 2356 RVA: 0x0003E720 File Offset: 0x0003C920
	public int ScrapForResearch(global::Item item)
	{
		object obj = Interface.CallHook("OnItemScrap", new object[]
		{
			this,
			item
		});
		if (obj is int)
		{
			return (int)obj;
		}
		int result = 0;
		if (item.info.rarity == 1)
		{
			result = 20;
		}
		if (item.info.rarity == 2)
		{
			result = 75;
		}
		if (item.info.rarity == 3)
		{
			result = 250;
		}
		if (item.info.rarity == 4 || item.info.rarity == null)
		{
			result = 750;
		}
		return result;
	}

	// Token: 0x06000935 RID: 2357 RVA: 0x0003E7C4 File Offset: 0x0003C9C4
	public bool IsItemResearchable(global::Item item)
	{
		global::ItemBlueprint itemBlueprint = global::ItemManager.FindBlueprint(item.info);
		return !(itemBlueprint == null) && itemBlueprint.isResearchable && !itemBlueprint.defaultBlueprint;
	}

	// Token: 0x06000936 RID: 2358 RVA: 0x0003E804 File Offset: 0x0003CA04
	public override void ServerInit()
	{
		base.ServerInit();
		this.inventory.canAcceptItem = new Func<global::Item, int, bool>(this.ItemFilter);
	}

	// Token: 0x06000937 RID: 2359 RVA: 0x0003E824 File Offset: 0x0003CA24
	public override bool ItemFilter(global::Item item, int targetSlot)
	{
		return (targetSlot != 1 || !(item.info != this.researchResource)) && base.ItemFilter(item, targetSlot);
	}

	// Token: 0x06000938 RID: 2360 RVA: 0x0003E850 File Offset: 0x0003CA50
	public global::Item GetTargetItem()
	{
		return this.inventory.GetSlot(0);
	}

	// Token: 0x06000939 RID: 2361 RVA: 0x0003E860 File Offset: 0x0003CA60
	public global::Item GetScrapItem()
	{
		global::Item slot = this.inventory.GetSlot(1);
		if (slot.info != this.researchResource)
		{
			return null;
		}
		return slot;
	}

	// Token: 0x0600093A RID: 2362 RVA: 0x0003E894 File Offset: 0x0003CA94
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		if (base.HasFlag(global::BaseEntity.Flags.On) && this.researchFinishedTime != 0f)
		{
			base.Invoke(new Action(this.ResearchAttemptFinished), this.researchFinishedTime - UnityEngine.Time.realtimeSinceStartup);
		}
		this.inventory.SetLocked(false);
	}

	// Token: 0x0600093B RID: 2363 RVA: 0x0003E8F0 File Offset: 0x0003CAF0
	public override bool PlayerOpenLoot(global::BasePlayer player)
	{
		this.user = player;
		return base.PlayerOpenLoot(player);
	}

	// Token: 0x0600093C RID: 2364 RVA: 0x0003E900 File Offset: 0x0003CB00
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		this.user = null;
		base.PlayerStoppedLooting(player);
	}

	// Token: 0x0600093D RID: 2365 RVA: 0x0003E910 File Offset: 0x0003CB10
	[global::BaseEntity.RPC_Server]
	public void DoResearch(global::BaseEntity.RPCMessage msg)
	{
		if (this.IsResearching())
		{
			return;
		}
		global::BasePlayer player = msg.player;
		global::Item targetItem = this.GetTargetItem();
		if (targetItem == null)
		{
			return;
		}
		if (Interface.CallHook("CanResearchItem", new object[]
		{
			targetItem,
			player
		}) != null)
		{
			return;
		}
		if (targetItem.amount > 1)
		{
			return;
		}
		if (!this.IsItemResearchable(targetItem))
		{
			return;
		}
		Interface.CallHook("OnItemResearch", new object[]
		{
			this,
			targetItem,
			player
		});
		targetItem.CollectedForCrafting(player);
		this.researchFinishedTime = UnityEngine.Time.realtimeSinceStartup + this.researchDuration;
		base.Invoke(new Action(this.ResearchAttemptFinished), this.researchDuration);
		this.inventory.SetLocked(true);
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		player.inventory.loot.SendImmediate();
		if (this.researchStartEffect.isValid)
		{
			global::Effect.server.Run(this.researchStartEffect.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		}
		msg.player.GiveAchievement("RESEARCH_ITEM");
	}

	// Token: 0x0600093E RID: 2366 RVA: 0x0003EA34 File Offset: 0x0003CC34
	public static global::ItemDefinition GetBlueprintTemplate()
	{
		if (global::ResearchTable.blueprintBaseDef == null)
		{
			global::ResearchTable.blueprintBaseDef = global::ItemManager.FindItemDefinition("blueprintbase");
		}
		return global::ResearchTable.blueprintBaseDef;
	}

	// Token: 0x0600093F RID: 2367 RVA: 0x0003EA5C File Offset: 0x0003CC5C
	public void ResearchAttemptFinished()
	{
		global::Item targetItem = this.GetTargetItem();
		global::Item scrapItem = this.GetScrapItem();
		if (targetItem != null && scrapItem != null)
		{
			int num = this.ScrapForResearch(targetItem);
			object obj = Interface.CallHook("OnItemResearched", new object[]
			{
				this,
				num
			});
			if (obj is int)
			{
				num = (int)obj;
			}
			if (scrapItem.amount >= num)
			{
				if (scrapItem.amount <= num)
				{
					this.inventory.Remove(scrapItem);
					scrapItem.RemoveFromContainer();
					scrapItem.Remove(0f);
				}
				else
				{
					scrapItem.UseItem(num);
				}
				this.inventory.Remove(targetItem);
				targetItem.Remove(0f);
				global::Item item = global::ItemManager.Create(global::ResearchTable.GetBlueprintTemplate(), 1, 0UL);
				item.blueprintTarget = targetItem.info.itemid;
				if (!item.MoveToContainer(this.inventory, 0, true))
				{
					item.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
				}
				if (this.researchSuccessEffect.isValid)
				{
					global::Effect.server.Run(this.researchSuccessEffect.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
				}
			}
		}
		base.SendNetworkUpdateImmediate(false);
		if (this.user != null)
		{
			this.user.inventory.loot.SendImmediate();
		}
		this.EndResearch();
	}

	// Token: 0x06000940 RID: 2368 RVA: 0x0003EBCC File Offset: 0x0003CDCC
	public void CancelResearch()
	{
	}

	// Token: 0x06000941 RID: 2369 RVA: 0x0003EBD0 File Offset: 0x0003CDD0
	public void EndResearch()
	{
		this.inventory.SetLocked(false);
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		this.researchFinishedTime = 0f;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		if (this.user != null)
		{
			this.user.inventory.loot.SendImmediate();
		}
	}

	// Token: 0x06000942 RID: 2370 RVA: 0x0003EC2C File Offset: 0x0003CE2C
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.researchTable = Facepunch.Pool.Get<ProtoBuf.ResearchTable>();
		info.msg.researchTable.researchTimeLeft = this.researchFinishedTime - UnityEngine.Time.realtimeSinceStartup;
	}

	// Token: 0x06000943 RID: 2371 RVA: 0x0003EC64 File Offset: 0x0003CE64
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.researchTable != null)
		{
			this.researchFinishedTime = UnityEngine.Time.realtimeSinceStartup + info.msg.researchTable.researchTimeLeft;
		}
	}

	// Token: 0x06000944 RID: 2372 RVA: 0x0003EC9C File Offset: 0x0003CE9C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x0400043C RID: 1084
	[NonSerialized]
	public float researchFinishedTime;

	// Token: 0x0400043D RID: 1085
	public float researchCostFraction = 1f;

	// Token: 0x0400043E RID: 1086
	public float researchDuration = 10f;

	// Token: 0x0400043F RID: 1087
	public int requiredPaper = 10;

	// Token: 0x04000440 RID: 1088
	public global::GameObjectRef researchStartEffect;

	// Token: 0x04000441 RID: 1089
	public global::GameObjectRef researchFailEffect;

	// Token: 0x04000442 RID: 1090
	public global::GameObjectRef researchSuccessEffect;

	// Token: 0x04000443 RID: 1091
	public global::ItemDefinition researchResource;

	// Token: 0x04000444 RID: 1092
	public global::BasePlayer user;

	// Token: 0x04000445 RID: 1093
	public static global::ItemDefinition blueprintBaseDef;
}
