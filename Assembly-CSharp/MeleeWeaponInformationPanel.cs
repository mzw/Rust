﻿using System;

// Token: 0x020006BC RID: 1724
public class MeleeWeaponInformationPanel : global::ItemInformationPanel
{
	// Token: 0x04001D2C RID: 7468
	public global::ItemStatValue damageDisplay;

	// Token: 0x04001D2D RID: 7469
	public global::ItemStatValue attackRateDisplay;

	// Token: 0x04001D2E RID: 7470
	public global::ItemStatValue attackSizeDisplay;

	// Token: 0x04001D2F RID: 7471
	public global::ItemStatValue attackRangeDisplay;

	// Token: 0x04001D30 RID: 7472
	public global::ItemStatValue oreGatherDisplay;

	// Token: 0x04001D31 RID: 7473
	public global::ItemStatValue treeGatherDisplay;

	// Token: 0x04001D32 RID: 7474
	public global::ItemStatValue fleshGatherDisplay;
}
