﻿using System;
using System.Collections;
using System.IO;
using ConVar;
using Facepunch.Steamworks;
using Network;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200009D RID: 157
public class SteamInventory : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x060009BD RID: 2493 RVA: 0x000424C0 File Offset: 0x000406C0
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("SteamInventory.OnRpcMessage", 0.1f))
		{
			if (rpc == 1662543637u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - UpdateSteamInventory ");
				}
				using (TimeWarning.New("UpdateSteamInventory", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("UpdateSteamInventory", this.GetBaseEntity(), player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.UpdateSteamInventory(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in UpdateSteamInventory");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009BE RID: 2494 RVA: 0x000426A0 File Offset: 0x000408A0
	public bool HasItem(int itemid)
	{
		if (this.Items == null)
		{
			return false;
		}
		foreach (Facepunch.Steamworks.Inventory.Item item in this.Items)
		{
			if (item.DefinitionId == itemid)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060009BF RID: 2495 RVA: 0x000426E8 File Offset: 0x000408E8
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	private void UpdateSteamInventory(global::BaseEntity.RPCMessage msg)
	{
		MemoryStream memoryStream = msg.read.MemoryStreamWithSize();
		if (memoryStream == null)
		{
			Debug.LogWarning("UpdateSteamInventory: Data is null");
			return;
		}
		Facepunch.Steamworks.Inventory.Result result = Rust.Global.SteamServer.Inventory.Deserialize(memoryStream.GetBuffer(), (int)memoryStream.Length);
		if (result == null)
		{
			Debug.LogWarning("UpdateSteamInventory: result is null");
			return;
		}
		base.StopAllCoroutines();
		base.StartCoroutine(this.ProcessInventoryResult(result));
	}

	// Token: 0x060009C0 RID: 2496 RVA: 0x00042758 File Offset: 0x00040958
	private IEnumerator ProcessInventoryResult(Facepunch.Steamworks.Inventory.Result result)
	{
		float count = 0f;
		while (result.IsPending)
		{
			count += 1f;
			yield return UnityEngine.CoroutineEx.waitForSeconds(1f);
			if (count > 30f)
			{
				Debug.LogFormat("Steam Inventory result timed out for {0}", new object[]
				{
					base.baseEntity.displayName
				});
			}
		}
		if (result.Items != null)
		{
			this.Items = result.Items;
		}
		result.Dispose();
		yield break;
	}

	// Token: 0x04000485 RID: 1157
	private Facepunch.Steamworks.Inventory.Item[] Items;
}
