﻿using System;
using UnityEngine;

// Token: 0x02000270 RID: 624
public class Model : MonoBehaviour
{
	// Token: 0x0600109E RID: 4254 RVA: 0x0006434C File Offset: 0x0006254C
	protected void OnEnable()
	{
		this.skin = -1;
	}

	// Token: 0x0600109F RID: 4255 RVA: 0x00064358 File Offset: 0x00062558
	public int GetSkin()
	{
		return this.skin;
	}

	// Token: 0x04000B8A RID: 2954
	public SphereCollider collision;

	// Token: 0x04000B8B RID: 2955
	public Transform rootBone;

	// Token: 0x04000B8C RID: 2956
	public Transform headBone;

	// Token: 0x04000B8D RID: 2957
	public Transform eyeBone;

	// Token: 0x04000B8E RID: 2958
	public Animator animator;

	// Token: 0x04000B8F RID: 2959
	[HideInInspector]
	public Transform[] boneTransforms;

	// Token: 0x04000B90 RID: 2960
	[HideInInspector]
	public string[] boneNames;

	// Token: 0x04000B91 RID: 2961
	internal int skin;
}
