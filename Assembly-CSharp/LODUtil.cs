﻿using System;
using UnityEngine;

// Token: 0x0200044F RID: 1103
public static class LODUtil
{
	// Token: 0x0600188A RID: 6282 RVA: 0x0008A9F4 File Offset: 0x00088BF4
	public static float GetDistance(Transform transform, global::LODDistanceMode mode = global::LODDistanceMode.XYZ)
	{
		return global::LODUtil.GetDistance(transform.position, mode);
	}

	// Token: 0x0600188B RID: 6283 RVA: 0x0008AA04 File Offset: 0x00088C04
	public static float GetDistance(Vector3 worldPos, global::LODDistanceMode mode = global::LODDistanceMode.XYZ)
	{
		return (!global::MainCamera.isValid) ? 1000f : ((mode != global::LODDistanceMode.XYZ) ? Vector3Ex.Distance2D(global::MainCamera.position, worldPos) : Vector3.Distance(global::MainCamera.position, worldPos));
	}

	// Token: 0x0600188C RID: 6284 RVA: 0x0008AA3C File Offset: 0x00088C3C
	public static float VerifyDistance(float distance)
	{
		return Mathf.Min(500f, distance);
	}
}
