﻿using System;

// Token: 0x0200058D RID: 1421
internal enum TextureCacheState
{
	// Token: 0x040018A2 RID: 6306
	Skipped,
	// Token: 0x040018A3 RID: 6307
	Initializing,
	// Token: 0x040018A4 RID: 6308
	Uncached,
	// Token: 0x040018A5 RID: 6309
	CachedRaw,
	// Token: 0x040018A6 RID: 6310
	CachedRawCompressFailed,
	// Token: 0x040018A7 RID: 6311
	CachedRawCompressWaiting,
	// Token: 0x040018A8 RID: 6312
	CachedCompressed
}
