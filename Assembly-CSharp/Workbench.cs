﻿using System;
using ConVar;
using Network;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000A9 RID: 169
public class Workbench : global::StorageContainer
{
	// Token: 0x06000A4A RID: 2634 RVA: 0x00046DB4 File Offset: 0x00044FB4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Workbench.OnRpcMessage", 0.1f))
		{
			if (rpc == 315030284u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_BeginExperiment ");
				}
				using (TimeWarning.New("RPC_BeginExperiment", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_BeginExperiment", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_BeginExperiment(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_BeginExperiment");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 1023681781u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Rotate ");
				}
				using (TimeWarning.New("RPC_Rotate", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_Rotate", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Rotate(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_Rotate");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000A4B RID: 2635 RVA: 0x00047120 File Offset: 0x00045320
	public int GetScrapForExperiment()
	{
		if (this.Workbenchlevel == 1)
		{
			return 75;
		}
		if (this.Workbenchlevel == 2)
		{
			return 300;
		}
		if (this.Workbenchlevel == 3)
		{
			return 1000;
		}
		Debug.LogWarning("GetScrapForExperiment fucked up big time.");
		return 0;
	}

	// Token: 0x06000A4C RID: 2636 RVA: 0x00047160 File Offset: 0x00045360
	public bool IsWorking()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000A4D RID: 2637 RVA: 0x0004716C File Offset: 0x0004536C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_Rotate(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (player.CanBuild() && player.GetHeldEntity() && player.GetHeldEntity().GetComponent<global::Hammer>() != null)
		{
			base.transform.rotation = Quaternion.LookRotation(-base.transform.forward, base.transform.up);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			global::Deployable component = base.GetComponent<global::Deployable>();
			if (component != null)
			{
				global::Effect.server.Run(component.placeEffect.resourcePath, base.transform.position, Vector3.up, null, false);
			}
		}
	}

	// Token: 0x06000A4E RID: 2638 RVA: 0x0004721C File Offset: 0x0004541C
	public static global::ItemDefinition GetBlueprintTemplate()
	{
		if (global::Workbench.blueprintBaseDef == null)
		{
			global::Workbench.blueprintBaseDef = global::ItemManager.FindItemDefinition("blueprintbase");
		}
		return global::Workbench.blueprintBaseDef;
	}

	// Token: 0x06000A4F RID: 2639 RVA: 0x00047244 File Offset: 0x00045444
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_BeginExperiment(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (player == null)
		{
			return;
		}
		if (this.IsWorking())
		{
			return;
		}
		PersistantPlayer playerInfo = SingletonComponent<global::ServerMgr>.Instance.persistance.GetPlayerInfo(player.userID);
		int num = Random.Range(0, this.experimentalItems.subSpawn.Length);
		for (int i = 0; i < this.experimentalItems.subSpawn.Length; i++)
		{
			int num2 = i + num;
			if (num2 >= this.experimentalItems.subSpawn.Length)
			{
				num2 -= this.experimentalItems.subSpawn.Length;
			}
			global::ItemDefinition itemDef = this.experimentalItems.subSpawn[num2].category.items[0].itemDef;
			if (itemDef.Blueprint)
			{
				if (!itemDef.Blueprint.defaultBlueprint)
				{
					if (itemDef.Blueprint.userCraftable)
					{
						if (itemDef.Blueprint.isResearchable)
						{
							if (!itemDef.Blueprint.NeedsSteamItem)
							{
								if (!playerInfo.unlockedItems.Contains(itemDef.itemid))
								{
									this.pendingBlueprint = itemDef;
									break;
								}
							}
						}
					}
				}
			}
		}
		if (this.pendingBlueprint == null)
		{
			player.ChatMessage("You have already unlocked everything for this workbench tier.");
			return;
		}
		global::Item slot = this.inventory.GetSlot(0);
		if (slot != null)
		{
			if (!slot.MoveToContainer(player.inventory.containerMain, -1, true))
			{
				slot.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
			}
			player.inventory.loot.SendImmediate();
		}
		if (this.experimentStartEffect.isValid)
		{
			global::Effect.server.Run(this.experimentStartEffect.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
		}
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		this.inventory.SetLocked(true);
		base.CancelInvoke(new Action(this.ExperimentComplete));
		base.Invoke(new Action(this.ExperimentComplete), 5f);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000A50 RID: 2640 RVA: 0x00047490 File Offset: 0x00045690
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
	}

	// Token: 0x06000A51 RID: 2641 RVA: 0x0004749C File Offset: 0x0004569C
	public override void OnKilled(global::HitInfo info)
	{
		base.OnKilled(info);
		base.CancelInvoke(new Action(this.ExperimentComplete));
	}

	// Token: 0x06000A52 RID: 2642 RVA: 0x000474B8 File Offset: 0x000456B8
	public int GetAvailableExperimentResources()
	{
		global::Item experimentResourceItem = this.GetExperimentResourceItem();
		if (experimentResourceItem == null || experimentResourceItem.info != this.experimentResource)
		{
			return 0;
		}
		return experimentResourceItem.amount;
	}

	// Token: 0x06000A53 RID: 2643 RVA: 0x000474F0 File Offset: 0x000456F0
	public global::Item GetExperimentResourceItem()
	{
		return this.inventory.GetSlot(1);
	}

	// Token: 0x06000A54 RID: 2644 RVA: 0x00047500 File Offset: 0x00045700
	public void ExperimentComplete()
	{
		global::Item experimentResourceItem = this.GetExperimentResourceItem();
		int scrapForExperiment = this.GetScrapForExperiment();
		if (this.pendingBlueprint == null)
		{
			Debug.LogWarning("Pending blueprint was null!");
		}
		if (experimentResourceItem != null && experimentResourceItem.amount >= scrapForExperiment && this.pendingBlueprint != null)
		{
			experimentResourceItem.UseItem(scrapForExperiment);
			global::Item item = global::ItemManager.Create(global::Workbench.GetBlueprintTemplate(), 1, 0UL);
			item.blueprintTarget = this.pendingBlueprint.itemid;
			this.creatingBlueprint = true;
			if (!item.MoveToContainer(this.inventory, 0, true))
			{
				item.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
			}
			this.creatingBlueprint = false;
			if (this.experimentSuccessEffect.isValid)
			{
				global::Effect.server.Run(this.experimentSuccessEffect.resourcePath, this, 0u, Vector3.zero, Vector3.zero, null, false);
			}
		}
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		this.pendingBlueprint = null;
		this.inventory.SetLocked(false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000A55 RID: 2645 RVA: 0x00047610 File Offset: 0x00045810
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		if (this.inventory != null)
		{
			this.inventory.SetLocked(false);
		}
	}

	// Token: 0x06000A56 RID: 2646 RVA: 0x00047638 File Offset: 0x00045838
	public override void ServerInit()
	{
		base.ServerInit();
		this.inventory.canAcceptItem = new Func<global::Item, int, bool>(this.ItemFilter);
	}

	// Token: 0x06000A57 RID: 2647 RVA: 0x00047658 File Offset: 0x00045858
	public override bool ItemFilter(global::Item item, int targetSlot)
	{
		return (targetSlot == 1 && item.info == this.experimentResource) || (targetSlot == 0 && this.creatingBlueprint);
	}

	// Token: 0x040004D9 RID: 1241
	public const int blueprintSlot = 0;

	// Token: 0x040004DA RID: 1242
	public const int experimentSlot = 1;

	// Token: 0x040004DB RID: 1243
	public int Workbenchlevel;

	// Token: 0x040004DC RID: 1244
	public global::LootSpawn experimentalItems;

	// Token: 0x040004DD RID: 1245
	public global::GameObjectRef experimentStartEffect;

	// Token: 0x040004DE RID: 1246
	public global::GameObjectRef experimentSuccessEffect;

	// Token: 0x040004DF RID: 1247
	public global::ItemDefinition experimentResource;

	// Token: 0x040004E0 RID: 1248
	public static global::ItemDefinition blueprintBaseDef;

	// Token: 0x040004E1 RID: 1249
	private global::ItemDefinition pendingBlueprint;

	// Token: 0x040004E2 RID: 1250
	private bool creatingBlueprint;
}
