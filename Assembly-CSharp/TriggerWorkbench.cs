﻿using System;
using UnityEngine;

// Token: 0x0200049E RID: 1182
public class TriggerWorkbench : global::TriggerBase
{
	// Token: 0x060019BB RID: 6587 RVA: 0x00090B9C File Offset: 0x0008ED9C
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x060019BC RID: 6588 RVA: 0x00090BE8 File Offset: 0x0008EDE8
	public float WorkbenchLevel()
	{
		return (float)this.parentBench.Workbenchlevel;
	}

	// Token: 0x0400144F RID: 5199
	public global::Workbench parentBench;
}
