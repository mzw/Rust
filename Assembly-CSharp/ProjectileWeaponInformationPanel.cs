﻿using System;

// Token: 0x020006BF RID: 1727
public class ProjectileWeaponInformationPanel : global::ItemInformationPanel
{
	// Token: 0x04001D38 RID: 7480
	public global::ItemStatValue damageDisplay;

	// Token: 0x04001D39 RID: 7481
	public global::ItemStatValue recoilDisplay;

	// Token: 0x04001D3A RID: 7482
	public global::ItemStatValue rofDisplay;

	// Token: 0x04001D3B RID: 7483
	public global::ItemStatValue accuracyDisplay;

	// Token: 0x04001D3C RID: 7484
	public global::ItemStatValue rangeDisplay;
}
