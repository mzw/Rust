﻿using System;
using UnityEngine;

// Token: 0x0200040E RID: 1038
public class Deployable : global::PrefabAttribute
{
	// Token: 0x060017DF RID: 6111 RVA: 0x0008868C File Offset: 0x0008688C
	protected override Type GetIndexedType()
	{
		return typeof(global::Deployable);
	}

	// Token: 0x04001274 RID: 4724
	public Mesh guideMesh;

	// Token: 0x04001275 RID: 4725
	public Vector3 guideMeshScale = Vector3.one;

	// Token: 0x04001276 RID: 4726
	public bool guideLights = true;

	// Token: 0x04001277 RID: 4727
	public bool wantsInstanceData;

	// Token: 0x04001278 RID: 4728
	public bool copyInventoryFromItem;

	// Token: 0x04001279 RID: 4729
	public bool setSocketParent;

	// Token: 0x0400127A RID: 4730
	public bool toSlot;

	// Token: 0x0400127B RID: 4731
	public global::BaseEntity.Slot slot;

	// Token: 0x0400127C RID: 4732
	public global::GameObjectRef placeEffect;
}
