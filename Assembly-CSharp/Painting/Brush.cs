﻿using System;
using UnityEngine;

namespace Painting
{
	// Token: 0x02000697 RID: 1687
	[Serializable]
	public class Brush
	{
		// Token: 0x04001C83 RID: 7299
		public float spacing;

		// Token: 0x04001C84 RID: 7300
		public Vector2 brushSize;

		// Token: 0x04001C85 RID: 7301
		public Texture2D texture;

		// Token: 0x04001C86 RID: 7302
		public Color color;

		// Token: 0x04001C87 RID: 7303
		public bool erase;
	}
}
