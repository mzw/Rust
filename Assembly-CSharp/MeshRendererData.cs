﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using UnityEngine;

// Token: 0x02000253 RID: 595
public class MeshRendererData
{
	// Token: 0x06001053 RID: 4179 RVA: 0x00062C28 File Offset: 0x00060E28
	public void Alloc()
	{
		if (this.triangles == null)
		{
			this.triangles = Facepunch.Pool.GetList<int>();
		}
		if (this.vertices == null)
		{
			this.vertices = Facepunch.Pool.GetList<Vector3>();
		}
		if (this.normals == null)
		{
			this.normals = Facepunch.Pool.GetList<Vector3>();
		}
		if (this.tangents == null)
		{
			this.tangents = Facepunch.Pool.GetList<Vector4>();
		}
		if (this.colors32 == null)
		{
			this.colors32 = Facepunch.Pool.GetList<Color32>();
		}
		if (this.uv == null)
		{
			this.uv = Facepunch.Pool.GetList<Vector2>();
		}
		if (this.uv2 == null)
		{
			this.uv2 = Facepunch.Pool.GetList<Vector2>();
		}
		if (this.positions == null)
		{
			this.positions = Facepunch.Pool.GetList<Vector4>();
		}
	}

	// Token: 0x06001054 RID: 4180 RVA: 0x00062CE8 File Offset: 0x00060EE8
	public void Free()
	{
		if (this.triangles != null)
		{
			Facepunch.Pool.FreeList<int>(ref this.triangles);
		}
		if (this.vertices != null)
		{
			Facepunch.Pool.FreeList<Vector3>(ref this.vertices);
		}
		if (this.normals != null)
		{
			Facepunch.Pool.FreeList<Vector3>(ref this.normals);
		}
		if (this.tangents != null)
		{
			Facepunch.Pool.FreeList<Vector4>(ref this.tangents);
		}
		if (this.colors32 != null)
		{
			Facepunch.Pool.FreeList<Color32>(ref this.colors32);
		}
		if (this.uv != null)
		{
			Facepunch.Pool.FreeList<Vector2>(ref this.uv);
		}
		if (this.uv2 != null)
		{
			Facepunch.Pool.FreeList<Vector2>(ref this.uv2);
		}
		if (this.positions != null)
		{
			Facepunch.Pool.FreeList<Vector4>(ref this.positions);
		}
	}

	// Token: 0x06001055 RID: 4181 RVA: 0x00062DA8 File Offset: 0x00060FA8
	public void Clear()
	{
		if (this.triangles != null)
		{
			this.triangles.Clear();
		}
		if (this.vertices != null)
		{
			this.vertices.Clear();
		}
		if (this.normals != null)
		{
			this.normals.Clear();
		}
		if (this.tangents != null)
		{
			this.tangents.Clear();
		}
		if (this.colors32 != null)
		{
			this.colors32.Clear();
		}
		if (this.uv != null)
		{
			this.uv.Clear();
		}
		if (this.uv2 != null)
		{
			this.uv2.Clear();
		}
		if (this.positions != null)
		{
			this.positions.Clear();
		}
	}

	// Token: 0x06001056 RID: 4182 RVA: 0x00062E68 File Offset: 0x00061068
	public void Apply(UnityEngine.Mesh mesh)
	{
		mesh.Clear();
		if (this.vertices != null)
		{
			mesh.SetVertices(this.vertices);
		}
		if (this.triangles != null)
		{
			mesh.SetTriangles(this.triangles, 0);
		}
		if (this.normals != null)
		{
			if (this.normals.Count == this.vertices.Count)
			{
				mesh.SetNormals(this.normals);
			}
			else if (this.normals.Count > 0 && ConVar.Batching.verbose > 0)
			{
				Debug.LogWarning("Skipping renderer normals because some meshes were missing them.");
			}
		}
		if (this.tangents != null)
		{
			if (this.tangents.Count == this.vertices.Count)
			{
				mesh.SetTangents(this.tangents);
			}
			else if (this.tangents.Count > 0 && ConVar.Batching.verbose > 0)
			{
				Debug.LogWarning("Skipping renderer tangents because some meshes were missing them.");
			}
		}
		if (this.colors32 != null)
		{
			if (this.colors32.Count == this.vertices.Count)
			{
				mesh.SetColors(this.colors32);
			}
			else if (this.colors32.Count > 0 && ConVar.Batching.verbose > 0)
			{
				Debug.LogWarning("Skipping renderer colors because some meshes were missing them.");
			}
		}
		if (this.uv != null)
		{
			if (this.uv.Count == this.vertices.Count)
			{
				mesh.SetUVs(0, this.uv);
			}
			else if (this.uv.Count > 0 && ConVar.Batching.verbose > 0)
			{
				Debug.LogWarning("Skipping renderer uvs because some meshes were missing them.");
			}
		}
		if (this.uv2 != null)
		{
			if (this.uv2.Count == this.vertices.Count)
			{
				mesh.SetUVs(1, this.uv2);
			}
			else if (this.uv2.Count > 0 && ConVar.Batching.verbose > 0)
			{
				Debug.LogWarning("Skipping renderer uv2s because some meshes were missing them.");
			}
		}
		if (this.positions != null)
		{
			mesh.SetUVs(2, this.positions);
		}
	}

	// Token: 0x06001057 RID: 4183 RVA: 0x00063098 File Offset: 0x00061298
	public void Combine(global::MeshRendererGroup meshGroup)
	{
		for (int i = 0; i < meshGroup.data.Count; i++)
		{
			global::MeshRendererInstance meshRendererInstance = meshGroup.data[i];
			Matrix4x4 matrix4x = Matrix4x4.TRS(meshRendererInstance.position, meshRendererInstance.rotation, meshRendererInstance.scale);
			int count = this.vertices.Count;
			for (int j = 0; j < meshRendererInstance.data.triangles.Length; j++)
			{
				this.triangles.Add(count + meshRendererInstance.data.triangles[j]);
			}
			for (int k = 0; k < meshRendererInstance.data.vertices.Length; k++)
			{
				this.vertices.Add(matrix4x.MultiplyPoint3x4(meshRendererInstance.data.vertices[k]));
				this.positions.Add(meshRendererInstance.position);
			}
			for (int l = 0; l < meshRendererInstance.data.normals.Length; l++)
			{
				this.normals.Add(matrix4x.MultiplyVector(meshRendererInstance.data.normals[l]));
			}
			for (int m = 0; m < meshRendererInstance.data.tangents.Length; m++)
			{
				Vector4 vector = meshRendererInstance.data.tangents[m];
				Vector3 vector2;
				vector2..ctor(vector.x, vector.y, vector.z);
				Vector3 vector3 = matrix4x.MultiplyVector(vector2);
				this.tangents.Add(new Vector4(vector3.x, vector3.y, vector3.z, vector.w));
			}
			for (int n = 0; n < meshRendererInstance.data.colors32.Length; n++)
			{
				this.colors32.Add(meshRendererInstance.data.colors32[n]);
			}
			for (int num = 0; num < meshRendererInstance.data.uv.Length; num++)
			{
				this.uv.Add(meshRendererInstance.data.uv[num]);
			}
			for (int num2 = 0; num2 < meshRendererInstance.data.uv2.Length; num2++)
			{
				this.uv2.Add(meshRendererInstance.data.uv2[num2]);
			}
		}
	}

	// Token: 0x06001058 RID: 4184 RVA: 0x00063348 File Offset: 0x00061548
	public void Combine(global::MeshRendererGroup meshGroup, global::MeshRendererLookup rendererLookup)
	{
		for (int i = 0; i < meshGroup.data.Count; i++)
		{
			global::MeshRendererInstance instance = meshGroup.data[i];
			Matrix4x4 matrix4x = Matrix4x4.TRS(instance.position, instance.rotation, instance.scale);
			int count = this.vertices.Count;
			for (int j = 0; j < instance.data.triangles.Length; j++)
			{
				this.triangles.Add(count + instance.data.triangles[j]);
			}
			for (int k = 0; k < instance.data.vertices.Length; k++)
			{
				this.vertices.Add(matrix4x.MultiplyPoint3x4(instance.data.vertices[k]));
				this.positions.Add(instance.position);
			}
			for (int l = 0; l < instance.data.normals.Length; l++)
			{
				this.normals.Add(matrix4x.MultiplyVector(instance.data.normals[l]));
			}
			for (int m = 0; m < instance.data.tangents.Length; m++)
			{
				Vector4 vector = instance.data.tangents[m];
				Vector3 vector2;
				vector2..ctor(vector.x, vector.y, vector.z);
				Vector3 vector3 = matrix4x.MultiplyVector(vector2);
				this.tangents.Add(new Vector4(vector3.x, vector3.y, vector3.z, vector.w));
			}
			for (int n = 0; n < instance.data.colors32.Length; n++)
			{
				this.colors32.Add(instance.data.colors32[n]);
			}
			for (int num = 0; num < instance.data.uv.Length; num++)
			{
				this.uv.Add(instance.data.uv[num]);
			}
			for (int num2 = 0; num2 < instance.data.uv2.Length; num2++)
			{
				this.uv2.Add(instance.data.uv2[num2]);
			}
			rendererLookup.Add(instance);
		}
	}

	// Token: 0x04000B1E RID: 2846
	public List<int> triangles;

	// Token: 0x04000B1F RID: 2847
	public List<Vector3> vertices;

	// Token: 0x04000B20 RID: 2848
	public List<Vector3> normals;

	// Token: 0x04000B21 RID: 2849
	public List<Vector4> tangents;

	// Token: 0x04000B22 RID: 2850
	public List<Color32> colors32;

	// Token: 0x04000B23 RID: 2851
	public List<Vector2> uv;

	// Token: 0x04000B24 RID: 2852
	public List<Vector2> uv2;

	// Token: 0x04000B25 RID: 2853
	public List<Vector4> positions;
}
