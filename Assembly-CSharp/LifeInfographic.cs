﻿using System;
using ProtoBuf;
using UnityEngine;

// Token: 0x020006CC RID: 1740
public class LifeInfographic : MonoBehaviour
{
	// Token: 0x04001D86 RID: 7558
	[NonSerialized]
	public PlayerLifeStory life;

	// Token: 0x04001D87 RID: 7559
	public GameObject container;
}
