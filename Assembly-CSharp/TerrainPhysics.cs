﻿using System;
using UnityEngine;

// Token: 0x0200058B RID: 1419
public class TerrainPhysics : global::TerrainExtension
{
	// Token: 0x06001E15 RID: 7701 RVA: 0x000A7534 File Offset: 0x000A5734
	public override void Setup()
	{
		this.splat = this.terrain.GetComponent<global::TerrainSplatMap>();
		this.materials = this.config.GetPhysicMaterials();
	}

	// Token: 0x06001E16 RID: 7702 RVA: 0x000A7558 File Offset: 0x000A5758
	public PhysicMaterial GetMaterial(Vector3 worldPos)
	{
		return this.materials[this.splat.GetSplatMaxIndex(worldPos, -1)];
	}

	// Token: 0x0400189F RID: 6303
	private global::TerrainSplatMap splat;

	// Token: 0x040018A0 RID: 6304
	private PhysicMaterial[] materials;
}
