﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000256 RID: 598
public class MeshRendererLookup
{
	// Token: 0x0600105D RID: 4189 RVA: 0x00063648 File Offset: 0x00061848
	public void Apply()
	{
		global::MeshRendererLookup.LookupGroup lookupGroup = this.src;
		this.src = this.dst;
		this.dst = lookupGroup;
		this.dst.Clear();
	}

	// Token: 0x0600105E RID: 4190 RVA: 0x0006367C File Offset: 0x0006187C
	public void Clear()
	{
		this.dst.Clear();
	}

	// Token: 0x0600105F RID: 4191 RVA: 0x0006368C File Offset: 0x0006188C
	public void Add(global::MeshRendererInstance instance)
	{
		this.dst.Add(instance);
	}

	// Token: 0x06001060 RID: 4192 RVA: 0x0006369C File Offset: 0x0006189C
	public global::MeshRendererLookup.LookupEntry Get(int index)
	{
		return this.src.Get(index);
	}

	// Token: 0x04000B2C RID: 2860
	public global::MeshRendererLookup.LookupGroup src = new global::MeshRendererLookup.LookupGroup();

	// Token: 0x04000B2D RID: 2861
	public global::MeshRendererLookup.LookupGroup dst = new global::MeshRendererLookup.LookupGroup();

	// Token: 0x02000257 RID: 599
	public class LookupGroup
	{
		// Token: 0x06001062 RID: 4194 RVA: 0x000636C0 File Offset: 0x000618C0
		public void Clear()
		{
			this.data.Clear();
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x000636D0 File Offset: 0x000618D0
		public void Add(global::MeshRendererInstance instance)
		{
			this.data.Add(new global::MeshRendererLookup.LookupEntry(instance));
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x000636E4 File Offset: 0x000618E4
		public global::MeshRendererLookup.LookupEntry Get(int index)
		{
			return this.data[index];
		}

		// Token: 0x04000B2E RID: 2862
		public List<global::MeshRendererLookup.LookupEntry> data = new List<global::MeshRendererLookup.LookupEntry>();
	}

	// Token: 0x02000258 RID: 600
	public struct LookupEntry
	{
		// Token: 0x06001065 RID: 4197 RVA: 0x000636F4 File Offset: 0x000618F4
		public LookupEntry(global::MeshRendererInstance instance)
		{
			this.renderer = instance.renderer;
		}

		// Token: 0x04000B2F RID: 2863
		public Renderer renderer;
	}
}
