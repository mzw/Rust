﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Network;
using UnityEngine;

// Token: 0x020002EC RID: 748
[ConsoleSystem.Factory("global")]
public class DiagnosticsConSys : ConsoleSystem
{
	// Token: 0x060012E7 RID: 4839 RVA: 0x0006EC24 File Offset: 0x0006CE24
	private static void DumpAnimators(string targetFolder)
	{
		Animator[] array = Object.FindObjectsOfType<Animator>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("All animators");
		stringBuilder.AppendLine();
		foreach (Animator animator in array)
		{
			stringBuilder.AppendFormat("{1}\t{0}", animator.transform.GetRecursiveName(string.Empty), animator.enabled);
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Animators.List.txt", stringBuilder.ToString());
		StringBuilder stringBuilder2 = new StringBuilder();
		stringBuilder2.AppendLine("All animators - grouped by object name");
		stringBuilder2.AppendLine();
		IEnumerable<IGrouping<string, Animator>> source = from x in array
		group x by x.transform.GetRecursiveName(string.Empty);
		if (global::DiagnosticsConSys.<>f__mg$cache0 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache0 = new Func<IGrouping<string, Animator>, int>(Enumerable.Count<Animator>);
		}
		foreach (IGrouping<string, Animator> source2 in source.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache0))
		{
			stringBuilder2.AppendFormat("{1:N0}\t{0}", source2.First<Animator>().transform.GetRecursiveName(string.Empty), source2.Count<Animator>());
			stringBuilder2.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Animators.Counts.txt", stringBuilder2.ToString());
		StringBuilder stringBuilder3 = new StringBuilder();
		stringBuilder3.AppendLine("All animators - grouped by enabled/disabled");
		stringBuilder3.AppendLine();
		IEnumerable<IGrouping<string, Animator>> source3 = from x in array
		group x by x.transform.GetRecursiveName((!x.enabled) ? " (DISABLED)" : string.Empty);
		if (global::DiagnosticsConSys.<>f__mg$cache1 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache1 = new Func<IGrouping<string, Animator>, int>(Enumerable.Count<Animator>);
		}
		foreach (IGrouping<string, Animator> source4 in source3.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache1))
		{
			stringBuilder3.AppendFormat("{1:N0}\t{0}", source4.First<Animator>().transform.GetRecursiveName((!source4.First<Animator>().enabled) ? " (DISABLED)" : string.Empty), source4.Count<Animator>());
			stringBuilder3.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Animators.Counts.Enabled.txt", stringBuilder3.ToString());
	}

	// Token: 0x060012E8 RID: 4840 RVA: 0x0006EEB4 File Offset: 0x0006D0B4
	[ClientVar]
	[ServerVar]
	public static void dump(ConsoleSystem.Arg args)
	{
		if (Directory.Exists("diagnostics"))
		{
			Directory.CreateDirectory("diagnostics");
		}
		int num = 1;
		while (Directory.Exists("diagnostics/" + num))
		{
			num++;
		}
		Directory.CreateDirectory("diagnostics/" + num);
		string targetFolder = "diagnostics/" + num + "/";
		global::DiagnosticsConSys.DumpLODGroups(targetFolder);
		global::DiagnosticsConSys.DumpSystemInformation(targetFolder);
		global::DiagnosticsConSys.DumpGameObjects(targetFolder);
		global::DiagnosticsConSys.DumpObjects(targetFolder);
		global::DiagnosticsConSys.DumpEntities(targetFolder);
		global::DiagnosticsConSys.DumpNetwork(targetFolder);
		global::DiagnosticsConSys.DumpPhysics(targetFolder);
		global::DiagnosticsConSys.DumpAnimators(targetFolder);
	}

	// Token: 0x060012E9 RID: 4841 RVA: 0x0006EF5C File Offset: 0x0006D15C
	private static void DumpSystemInformation(string targetFolder)
	{
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "System.Info.txt", global::SystemInfoGeneralText.currentInfo);
	}

	// Token: 0x060012EA RID: 4842 RVA: 0x0006EF74 File Offset: 0x0006D174
	private static void WriteTextToFile(string file, string text)
	{
		File.WriteAllText(file, text);
	}

	// Token: 0x060012EB RID: 4843 RVA: 0x0006EF80 File Offset: 0x0006D180
	private static void DumpEntities(string targetFolder)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("All entities");
		stringBuilder.AppendLine();
		foreach (global::BaseNetworkable baseNetworkable in global::BaseNetworkable.serverEntities)
		{
			stringBuilder.AppendFormat("{1}\t{0}", baseNetworkable.PrefabName, (baseNetworkable.net == null) ? 0u : baseNetworkable.net.ID);
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Entity.SV.List.txt", stringBuilder.ToString());
		StringBuilder stringBuilder2 = new StringBuilder();
		stringBuilder2.AppendLine("All entities");
		stringBuilder2.AppendLine();
		IEnumerable<IGrouping<uint, global::BaseNetworkable>> source = from x in global::BaseNetworkable.serverEntities
		group x by x.prefabID;
		if (global::DiagnosticsConSys.<>f__mg$cache2 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache2 = new Func<IGrouping<uint, global::BaseNetworkable>, int>(Enumerable.Count<global::BaseNetworkable>);
		}
		foreach (IGrouping<uint, global::BaseNetworkable> source2 in source.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache2))
		{
			stringBuilder2.AppendFormat("{1:N0}\t{0}", source2.First<global::BaseNetworkable>().PrefabName, source2.Count<global::BaseNetworkable>());
			stringBuilder2.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Entity.SV.Counts.txt", stringBuilder2.ToString());
		StringBuilder stringBuilder3 = new StringBuilder();
		stringBuilder3.AppendLine("Saved entities");
		stringBuilder3.AppendLine();
		IEnumerable<IGrouping<uint, global::BaseEntity>> source3 = from x in global::BaseEntity.saveList
		group x by x.prefabID;
		if (global::DiagnosticsConSys.<>f__mg$cache3 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache3 = new Func<IGrouping<uint, global::BaseEntity>, int>(Enumerable.Count<global::BaseEntity>);
		}
		foreach (IGrouping<uint, global::BaseEntity> source4 in source3.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache3))
		{
			stringBuilder3.AppendFormat("{1:N0}\t{0}", source4.First<global::BaseEntity>().PrefabName, source4.Count<global::BaseEntity>());
			stringBuilder3.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Entity.SV.Savelist.Counts.txt", stringBuilder3.ToString());
	}

	// Token: 0x060012EC RID: 4844 RVA: 0x0006F208 File Offset: 0x0006D408
	private static void DumpLODGroups(string targetFolder)
	{
		global::DiagnosticsConSys.DumpLODGroupTotals(targetFolder);
	}

	// Token: 0x060012ED RID: 4845 RVA: 0x0006F210 File Offset: 0x0006D410
	private static void DumpLODGroupTotals(string targetFolder)
	{
		LODGroup[] source = Object.FindObjectsOfType<LODGroup>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("LODGroups");
		stringBuilder.AppendLine();
		IEnumerable<IGrouping<string, LODGroup>> source2 = from x in source
		group x by x.transform.GetRecursiveName(string.Empty);
		if (global::DiagnosticsConSys.<>f__mg$cache4 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache4 = new Func<IGrouping<string, LODGroup>, int>(Enumerable.Count<LODGroup>);
		}
		foreach (IGrouping<string, LODGroup> grouping in source2.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache4))
		{
			stringBuilder.AppendFormat("{1:N0}\t{0}", grouping.Key, grouping.Count<LODGroup>());
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "LODGroups.Objects.txt", stringBuilder.ToString());
	}

	// Token: 0x060012EE RID: 4846 RVA: 0x0006F2FC File Offset: 0x0006D4FC
	private static void DumpNetwork(string targetFolder)
	{
		if (Net.sv.IsConnected())
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendLine("Server Network Statistics");
			stringBuilder.AppendLine();
			stringBuilder.Append(Net.sv.GetDebug(null).Replace("\n", "\r\n"));
			stringBuilder.AppendLine();
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				stringBuilder.AppendLine("Name: " + basePlayer.displayName);
				stringBuilder.AppendLine("SteamID: " + basePlayer.userID);
				stringBuilder.Append((basePlayer.net != null) ? Net.sv.GetDebug(basePlayer.net.connection).Replace("\n", "\r\n") : "INVALID - NET IS NULL");
				stringBuilder.AppendLine();
				stringBuilder.AppendLine();
				stringBuilder.AppendLine();
				stringBuilder.AppendLine();
			}
			global::DiagnosticsConSys.WriteTextToFile(targetFolder + "Network.Server.txt", stringBuilder.ToString());
		}
	}

	// Token: 0x060012EF RID: 4847 RVA: 0x0006F448 File Offset: 0x0006D648
	private static void DumpObjects(string targetFolder)
	{
		Object[] source = Object.FindObjectsOfType<Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("All active UnityEngine.Object, ordered by count");
		stringBuilder.AppendLine();
		IEnumerable<IGrouping<Type, Object>> source2 = from x in source
		group x by x.GetType();
		if (global::DiagnosticsConSys.<>f__mg$cache5 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache5 = new Func<IGrouping<Type, Object>, int>(Enumerable.Count<Object>);
		}
		foreach (IGrouping<Type, Object> source3 in source2.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache5))
		{
			stringBuilder.AppendFormat("{1:N0}\t{0}", source3.First<Object>().GetType().Name, source3.Count<Object>());
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.Object.Count.txt", stringBuilder.ToString());
		StringBuilder stringBuilder2 = new StringBuilder();
		stringBuilder2.AppendLine("All active UnityEngine.ScriptableObject, ordered by count");
		stringBuilder2.AppendLine();
		IEnumerable<IGrouping<Type, Object>> source4 = from x in source
		where x is ScriptableObject
		group x by x.GetType();
		if (global::DiagnosticsConSys.<>f__mg$cache6 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache6 = new Func<IGrouping<Type, Object>, int>(Enumerable.Count<Object>);
		}
		foreach (IGrouping<Type, Object> source5 in source4.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache6))
		{
			stringBuilder2.AppendFormat("{1:N0}\t{0}", source5.First<Object>().GetType().Name, source5.Count<Object>());
			stringBuilder2.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "UnityEngine.ScriptableObject.Count.txt", stringBuilder2.ToString());
	}

	// Token: 0x060012F0 RID: 4848 RVA: 0x0006F64C File Offset: 0x0006D84C
	private static void DumpPhysics(string targetFolder)
	{
		global::DiagnosticsConSys.DumpTotals(targetFolder);
		global::DiagnosticsConSys.DumpColliders(targetFolder);
		global::DiagnosticsConSys.DumpRigidBodies(targetFolder);
	}

	// Token: 0x060012F1 RID: 4849 RVA: 0x0006F660 File Offset: 0x0006D860
	private static void DumpTotals(string targetFolder)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("Physics Information");
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Total Colliders:\t{0:N0}", Object.FindObjectsOfType<Collider>().Count<Collider>());
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Active Colliders:\t{0:N0}", (from x in Object.FindObjectsOfType<Collider>()
		where x.enabled
		select x).Count<Collider>());
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Batched Colliders:\t{0:N0}", (!SingletonComponent<global::ColliderGrid>.Instance) ? 0 : SingletonComponent<global::ColliderGrid>.Instance.BatchedMeshCount());
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Total RigidBodys:\t{0:N0}", Object.FindObjectsOfType<Rigidbody>().Count<Rigidbody>());
		stringBuilder.AppendLine();
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Mesh Colliders:\t{0:N0}", Object.FindObjectsOfType<MeshCollider>().Count<MeshCollider>());
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Box Colliders:\t{0:N0}", Object.FindObjectsOfType<BoxCollider>().Count<BoxCollider>());
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Sphere Colliders:\t{0:N0}", Object.FindObjectsOfType<SphereCollider>().Count<SphereCollider>());
		stringBuilder.AppendLine();
		stringBuilder.AppendFormat("Capsule Colliders:\t{0:N0}", Object.FindObjectsOfType<CapsuleCollider>().Count<CapsuleCollider>());
		stringBuilder.AppendLine();
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "Physics.txt", stringBuilder.ToString());
	}

	// Token: 0x060012F2 RID: 4850 RVA: 0x0006F7EC File Offset: 0x0006D9EC
	private static void DumpColliders(string targetFolder)
	{
		Collider[] source = Object.FindObjectsOfType<Collider>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("Physics Colliders");
		stringBuilder.AppendLine();
		IEnumerable<IGrouping<string, Collider>> source2 = from x in source
		group x by x.transform.GetRecursiveName(string.Empty);
		if (global::DiagnosticsConSys.<>f__mg$cache7 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache7 = new Func<IGrouping<string, Collider>, int>(Enumerable.Count<Collider>);
		}
		foreach (IGrouping<string, Collider> grouping in source2.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache7))
		{
			StringBuilder stringBuilder2 = stringBuilder;
			string format = "{1:N0}\t{0} ({2:N0} triggers) ({3:N0} enabled)";
			object[] array = new object[4];
			array[0] = grouping.Key;
			array[1] = grouping.Count<Collider>();
			array[2] = grouping.Count((Collider x) => x.isTrigger);
			array[3] = grouping.Count((Collider x) => x.enabled);
			stringBuilder2.AppendFormat(format, array);
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "Physics.Colliders.Objects.txt", stringBuilder.ToString());
	}

	// Token: 0x060012F3 RID: 4851 RVA: 0x0006F938 File Offset: 0x0006DB38
	private static void DumpRigidBodies(string targetFolder)
	{
		Rigidbody[] source = Object.FindObjectsOfType<Rigidbody>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("RigidBody");
		stringBuilder.AppendLine();
		StringBuilder stringBuilder2 = new StringBuilder();
		stringBuilder2.AppendLine("RigidBody");
		stringBuilder2.AppendLine();
		IEnumerable<IGrouping<string, Rigidbody>> source2 = from x in source
		group x by x.transform.GetRecursiveName(string.Empty);
		if (global::DiagnosticsConSys.<>f__mg$cache8 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache8 = new Func<IGrouping<string, Rigidbody>, int>(Enumerable.Count<Rigidbody>);
		}
		foreach (IGrouping<string, Rigidbody> grouping in source2.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache8))
		{
			StringBuilder stringBuilder3 = stringBuilder;
			string format = "{1:N0}\t{0} ({2:N0} awake) ({3:N0} kinematic) ({4:N0} non-discrete)";
			object[] array = new object[5];
			array[0] = grouping.Key;
			array[1] = grouping.Count<Rigidbody>();
			array[2] = grouping.Count((Rigidbody x) => !x.IsSleeping());
			array[3] = grouping.Count((Rigidbody x) => x.isKinematic);
			array[4] = grouping.Count((Rigidbody x) => x.collisionDetectionMode != 0);
			stringBuilder3.AppendFormat(format, array);
			stringBuilder.AppendLine();
			foreach (Rigidbody rigidbody in grouping)
			{
				stringBuilder2.AppendFormat("{0} -{1}{2}{3}", new object[]
				{
					grouping.Key,
					(!rigidbody.isKinematic) ? string.Empty : " KIN",
					(!rigidbody.IsSleeping()) ? string.Empty : " SLEEP",
					(!rigidbody.useGravity) ? string.Empty : " GRAVITY"
				});
				stringBuilder2.AppendLine();
				stringBuilder2.AppendFormat("Mass: {0}\tVelocity: {1}\tsleepThreshold: {2}", rigidbody.mass, rigidbody.velocity, rigidbody.sleepThreshold);
				stringBuilder2.AppendLine();
				stringBuilder2.AppendLine();
			}
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "Physics.RigidBody.Objects.txt", stringBuilder.ToString());
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "Physics.RigidBody.All.txt", stringBuilder2.ToString());
	}

	// Token: 0x060012F4 RID: 4852 RVA: 0x0006FBF8 File Offset: 0x0006DDF8
	private static void DumpGameObjects(string targetFolder)
	{
		Transform[] rootObjects = global::TransformUtil.GetRootObjects();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("All active game objects");
		stringBuilder.AppendLine();
		foreach (Transform tx in rootObjects)
		{
			global::DiagnosticsConSys.DumpGameObjectRecursive(stringBuilder, tx, 0, false);
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "GameObject.Hierarchy.txt", stringBuilder.ToString());
		stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("All active game objects including components");
		stringBuilder.AppendLine();
		foreach (Transform tx2 in rootObjects)
		{
			global::DiagnosticsConSys.DumpGameObjectRecursive(stringBuilder, tx2, 0, true);
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "GameObject.Hierarchy.Components.txt", stringBuilder.ToString());
		stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("Root gameobjects, grouped by name, ordered by the total number of objects excluding children");
		stringBuilder.AppendLine();
		IEnumerable<IGrouping<string, Transform>> source = from x in rootObjects
		group x by x.name;
		if (global::DiagnosticsConSys.<>f__mg$cache9 == null)
		{
			global::DiagnosticsConSys.<>f__mg$cache9 = new Func<IGrouping<string, Transform>, int>(Enumerable.Count<Transform>);
		}
		foreach (IGrouping<string, Transform> source2 in source.OrderByDescending(global::DiagnosticsConSys.<>f__mg$cache9))
		{
			Transform transform = source2.First<Transform>();
			stringBuilder.AppendFormat("{1:N0}\t{0}", transform.name, source2.Count<Transform>());
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "GameObject.Count.txt", stringBuilder.ToString());
		stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("Root gameobjects, grouped by name, ordered by the total number of objects including children");
		stringBuilder.AppendLine();
		foreach (KeyValuePair<Transform, int> keyValuePair in from x in rootObjects
		group x by x.name into x
		select new KeyValuePair<Transform, int>(x.First<Transform>(), x.Sum((Transform y) => y.GetAllChildren().Count)) into x
		orderby x.Value descending
		select x)
		{
			stringBuilder.AppendFormat("{1:N0}\t{0}", keyValuePair.Key.name, keyValuePair.Value);
			stringBuilder.AppendLine();
		}
		global::DiagnosticsConSys.WriteTextToFile(targetFolder + "GameObject.Count.Children.txt", stringBuilder.ToString());
	}

	// Token: 0x060012F5 RID: 4853 RVA: 0x0006FEB8 File Offset: 0x0006E0B8
	private static void DumpGameObjectRecursive(StringBuilder str, Transform tx, int indent, bool includeComponents = false)
	{
		if (tx == null)
		{
			return;
		}
		for (int i = 0; i < indent; i++)
		{
			str.Append(" ");
		}
		str.AppendFormat("{0} {1:N0}", tx.name, tx.GetComponents<Component>().Length - 1);
		str.AppendLine();
		if (includeComponents)
		{
			foreach (Component component in tx.GetComponents<Component>())
			{
				if (!(component is Transform))
				{
					for (int k = 0; k < indent + 1; k++)
					{
						str.Append(" ");
					}
					str.AppendFormat("[c] {0}", (!(component == null)) ? component.GetType().ToString() : "NULL");
					str.AppendLine();
				}
			}
		}
		for (int l = 0; l < tx.childCount; l++)
		{
			global::DiagnosticsConSys.DumpGameObjectRecursive(str, tx.GetChild(l), indent + 2, includeComponents);
		}
	}

	// Token: 0x04000DA6 RID: 3494
	[CompilerGenerated]
	private static Func<IGrouping<string, Animator>, int> <>f__mg$cache0;

	// Token: 0x04000DA7 RID: 3495
	[CompilerGenerated]
	private static Func<IGrouping<string, Animator>, int> <>f__mg$cache1;

	// Token: 0x04000DAA RID: 3498
	[CompilerGenerated]
	private static Func<IGrouping<uint, global::BaseNetworkable>, int> <>f__mg$cache2;

	// Token: 0x04000DAB RID: 3499
	[CompilerGenerated]
	private static Func<IGrouping<uint, global::BaseEntity>, int> <>f__mg$cache3;

	// Token: 0x04000DAE RID: 3502
	[CompilerGenerated]
	private static Func<IGrouping<string, LODGroup>, int> <>f__mg$cache4;

	// Token: 0x04000DB0 RID: 3504
	[CompilerGenerated]
	private static Func<IGrouping<Type, Object>, int> <>f__mg$cache5;

	// Token: 0x04000DB1 RID: 3505
	[CompilerGenerated]
	private static Func<IGrouping<Type, Object>, int> <>f__mg$cache6;

	// Token: 0x04000DB6 RID: 3510
	[CompilerGenerated]
	private static Func<IGrouping<string, Collider>, int> <>f__mg$cache7;

	// Token: 0x04000DBA RID: 3514
	[CompilerGenerated]
	private static Func<IGrouping<string, Rigidbody>, int> <>f__mg$cache8;

	// Token: 0x04000DBF RID: 3519
	[CompilerGenerated]
	private static Func<IGrouping<string, Transform>, int> <>f__mg$cache9;
}
