﻿using System;
using UnityEngine;

// Token: 0x02000370 RID: 880
public class HideIfOwnerFirstPerson : global::EntityComponent<global::BaseEntity>, IClientComponent, global::IViewModeChanged
{
	// Token: 0x04000F72 RID: 3954
	public GameObject[] disableGameObjects;

	// Token: 0x04000F73 RID: 3955
	public bool worldModelEffect;
}
