﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020004E2 RID: 1250
public class ItemModContainer : global::ItemMod
{
	// Token: 0x06001ACF RID: 6863 RVA: 0x00096248 File Offset: 0x00094448
	public override void OnItemCreated(global::Item item)
	{
		if (!item.isServer)
		{
			return;
		}
		if (this.capacity <= 0)
		{
			return;
		}
		if (item.contents != null)
		{
			return;
		}
		item.contents = new global::ItemContainer();
		item.contents.flags = this.containerFlags;
		item.contents.allowedContents = ((this.onlyAllowedContents != (global::ItemContainer.ContentsType)0) ? this.onlyAllowedContents : global::ItemContainer.ContentsType.Generic);
		item.contents.onlyAllowedItem = this.onlyAllowedItemType;
		item.contents.availableSlots = this.availableSlots;
		item.contents.ServerInitialize(item, this.capacity);
		item.contents.maxStackSize = this.maxStackSize;
		item.contents.GiveUID();
	}

	// Token: 0x06001AD0 RID: 6864 RVA: 0x00096308 File Offset: 0x00094508
	public override void OnVirginItem(global::Item item)
	{
		base.OnVirginItem(item);
		foreach (global::ItemAmount itemAmount in this.defaultContents)
		{
			global::Item item2 = global::ItemManager.Create(itemAmount.itemDef, (int)itemAmount.amount, 0UL);
			if (item2 != null)
			{
				item2.MoveToContainer(item.contents, -1, true);
			}
		}
	}

	// Token: 0x06001AD1 RID: 6865 RVA: 0x00096394 File Offset: 0x00094594
	public override void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
		if (item.contents == null)
		{
			return;
		}
		for (int i = item.contents.itemList.Count - 1; i >= 0; i--)
		{
			global::Item item2 = item.contents.itemList[i];
			if (!item2.MoveToContainer(crafter.inventory.containerMain, -1, true))
			{
				item2.Drop(crafter.eyes.position, crafter.eyes.BodyForward() * 2f, default(Quaternion));
			}
		}
	}

	// Token: 0x04001590 RID: 5520
	public int capacity = 6;

	// Token: 0x04001591 RID: 5521
	public int maxStackSize;

	// Token: 0x04001592 RID: 5522
	[InspectorFlags]
	public global::ItemContainer.Flag containerFlags;

	// Token: 0x04001593 RID: 5523
	public global::ItemContainer.ContentsType onlyAllowedContents = global::ItemContainer.ContentsType.Generic;

	// Token: 0x04001594 RID: 5524
	public global::ItemDefinition onlyAllowedItemType;

	// Token: 0x04001595 RID: 5525
	public List<global::ItemSlot> availableSlots = new List<global::ItemSlot>();

	// Token: 0x04001596 RID: 5526
	public bool openInDeployed = true;

	// Token: 0x04001597 RID: 5527
	public bool openInInventory = true;

	// Token: 0x04001598 RID: 5528
	public List<global::ItemAmount> defaultContents = new List<global::ItemAmount>();
}
