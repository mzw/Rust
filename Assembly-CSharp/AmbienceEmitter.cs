﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001CF RID: 463
public class AmbienceEmitter : MonoBehaviour, IClientComponent, IComparable<global::AmbienceEmitter>
{
	// Token: 0x17000101 RID: 257
	// (get) Token: 0x06000EF4 RID: 3828 RVA: 0x0005C5BC File Offset: 0x0005A7BC
	// (set) Token: 0x06000EF5 RID: 3829 RVA: 0x0005C5C4 File Offset: 0x0005A7C4
	public global::TerrainTopology.Enum currentTopology { get; private set; }

	// Token: 0x17000102 RID: 258
	// (get) Token: 0x06000EF6 RID: 3830 RVA: 0x0005C5D0 File Offset: 0x0005A7D0
	// (set) Token: 0x06000EF7 RID: 3831 RVA: 0x0005C5D8 File Offset: 0x0005A7D8
	public global::TerrainBiome.Enum currentBiome { get; private set; }

	// Token: 0x06000EF8 RID: 3832 RVA: 0x0005C5E4 File Offset: 0x0005A7E4
	public int CompareTo(global::AmbienceEmitter other)
	{
		return this.cameraDistance.CompareTo(other.cameraDistance);
	}

	// Token: 0x040008CF RID: 2255
	public global::AmbienceDefinitionList baseAmbience;

	// Token: 0x040008D0 RID: 2256
	public global::AmbienceDefinitionList stings;

	// Token: 0x040008D1 RID: 2257
	public bool isStatic = true;

	// Token: 0x040008D2 RID: 2258
	public bool active;

	// Token: 0x040008D3 RID: 2259
	public float cameraDistance = float.PositiveInfinity;

	// Token: 0x040008D4 RID: 2260
	public BoundingSphere boundingSphere;

	// Token: 0x040008D5 RID: 2261
	public float crossfadeTime = 2f;

	// Token: 0x040008D8 RID: 2264
	public Dictionary<global::AmbienceDefinition, float> nextStingTime = new Dictionary<global::AmbienceDefinition, float>();

	// Token: 0x040008D9 RID: 2265
	public float deactivateTime = float.PositiveInfinity;
}
