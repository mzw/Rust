﻿using System;
using System.Collections.Generic;

// Token: 0x020005AC RID: 1452
public class GenerateRiverTopology : global::ProceduralComponent
{
	// Token: 0x06001E58 RID: 7768 RVA: 0x000A9800 File Offset: 0x000A7A00
	public override void Process(uint seed)
	{
		global::GenerateRiverTopology.<Process>c__AnonStorey1 <Process>c__AnonStorey = new global::GenerateRiverTopology.<Process>c__AnonStorey1();
		List<global::PathList> rivers = global::TerrainMeta.Path.Rivers;
		<Process>c__AnonStorey.heightmap = global::TerrainMeta.HeightMap;
		<Process>c__AnonStorey.topomap = global::TerrainMeta.TopologyMap;
		foreach (global::PathList pathList in rivers)
		{
			pathList.Path.RecalculateTangents();
		}
		<Process>c__AnonStorey.heightmap.Push();
		foreach (global::PathList pathList2 in rivers)
		{
			pathList2.AdjustTerrainHeight();
			pathList2.AdjustTerrainTexture();
			pathList2.AdjustTerrainTopology();
		}
		<Process>c__AnonStorey.heightmap.Pop();
		int[,] map = <Process>c__AnonStorey.topomap.dst;
		global::ImageProcessing.Dilate2D(map, 49152, 6, delegate(int x, int y)
		{
			if ((map[x, y] & 49) != 0)
			{
				map[x, y] |= 32768;
			}
			float normX = <Process>c__AnonStorey.topomap.Coordinate(x);
			float normZ = <Process>c__AnonStorey.topomap.Coordinate(y);
			if (<Process>c__AnonStorey.heightmap.GetSlope(normX, normZ) > 40f)
			{
				map[x, y] |= 2;
			}
		});
	}
}
