﻿using System;
using UnityEngine;

// Token: 0x02000368 RID: 872
public class EntityCollisionMessage : global::EntityComponent<global::BaseEntity>
{
	// Token: 0x060014E2 RID: 5346 RVA: 0x00078FF4 File Offset: 0x000771F4
	private void OnCollisionEnter(Collision collision)
	{
		if (base.baseEntity == null)
		{
			return;
		}
		if (base.baseEntity.IsDestroyed)
		{
			return;
		}
		global::BaseEntity baseEntity = collision.GetEntity();
		if (baseEntity == base.baseEntity)
		{
			return;
		}
		if (baseEntity != null)
		{
			if (baseEntity.IsDestroyed)
			{
				return;
			}
			if (base.baseEntity.isServer)
			{
				baseEntity = baseEntity.ToServer<global::BaseEntity>();
			}
		}
		base.baseEntity.OnCollision(collision, baseEntity);
	}
}
