﻿using System;
using UnityEngine;

// Token: 0x020007F4 RID: 2036
public class ExplosionPlatformActivator : MonoBehaviour
{
	// Token: 0x060025A6 RID: 9638 RVA: 0x000D050C File Offset: 0x000CE70C
	private void Start()
	{
		this.currentRepeatTime = this.DefaultRepeatTime;
		base.Invoke("Init", this.TimeDelay);
	}

	// Token: 0x060025A7 RID: 9639 RVA: 0x000D052C File Offset: 0x000CE72C
	private void Init()
	{
		this.canUpdate = true;
		this.Effect.SetActive(true);
	}

	// Token: 0x060025A8 RID: 9640 RVA: 0x000D0544 File Offset: 0x000CE744
	private void Update()
	{
		if (!this.canUpdate || this.Effect == null)
		{
			return;
		}
		this.currentTime += Time.deltaTime;
		if (this.currentTime > this.currentRepeatTime)
		{
			this.currentTime = 0f;
			this.Effect.SetActive(false);
			this.Effect.SetActive(true);
		}
	}

	// Token: 0x060025A9 RID: 9641 RVA: 0x000D05B4 File Offset: 0x000CE7B4
	private void OnTriggerEnter(Collider coll)
	{
		this.currentRepeatTime = this.NearRepeatTime;
	}

	// Token: 0x060025AA RID: 9642 RVA: 0x000D05C4 File Offset: 0x000CE7C4
	private void OnTriggerExit(Collider other)
	{
		this.currentRepeatTime = this.DefaultRepeatTime;
	}

	// Token: 0x040021C1 RID: 8641
	public GameObject Effect;

	// Token: 0x040021C2 RID: 8642
	public float TimeDelay;

	// Token: 0x040021C3 RID: 8643
	public float DefaultRepeatTime = 5f;

	// Token: 0x040021C4 RID: 8644
	public float NearRepeatTime = 3f;

	// Token: 0x040021C5 RID: 8645
	private float currentTime;

	// Token: 0x040021C6 RID: 8646
	private float currentRepeatTime;

	// Token: 0x040021C7 RID: 8647
	private bool canUpdate;
}
