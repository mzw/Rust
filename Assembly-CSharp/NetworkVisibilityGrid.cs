﻿using System;
using System.Collections.Generic;
using Network;
using Network.Visibility;
using Rust;
using UnityEngine;

// Token: 0x0200050F RID: 1295
public class NetworkVisibilityGrid : MonoBehaviour, Provider
{
	// Token: 0x06001B5C RID: 7004 RVA: 0x000987BC File Offset: 0x000969BC
	private void Awake()
	{
		Debug.Assert(Net.sv != null, "Network.Net.sv is NULL when creating Visibility Grid");
		Debug.Assert(Net.sv.visibility == null, "Network.Net.sv.visibility is being set multiple times");
		Net.sv.visibility = new Manager(this);
	}

	// Token: 0x06001B5D RID: 7005 RVA: 0x000987FC File Offset: 0x000969FC
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (Net.sv != null && Net.sv.visibility != null)
		{
			Net.sv.visibility.Dispose();
			Net.sv.visibility = null;
		}
	}

	// Token: 0x06001B5E RID: 7006 RVA: 0x0009883C File Offset: 0x00096A3C
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
		float num = this.CellSize();
		float num2 = (float)this.gridSize / 2f;
		Vector3 position = base.transform.position;
		for (int i = 0; i <= this.cellCount; i++)
		{
			float num3 = -num2 + (float)i * num - num / 2f;
			Gizmos.DrawLine(new Vector3(num2, position.y, num3), new Vector3(-num2, position.y, num3));
			Gizmos.DrawLine(new Vector3(num3, position.y, num2), new Vector3(num3, position.y, -num2));
		}
	}

	// Token: 0x06001B5F RID: 7007 RVA: 0x000988E4 File Offset: 0x00096AE4
	private int PositionToGrid(float f)
	{
		f += (float)this.gridSize / 2f;
		return Mathf.RoundToInt(f / this.CellSize());
	}

	// Token: 0x06001B60 RID: 7008 RVA: 0x00098904 File Offset: 0x00096B04
	private float GridToPosition(int i)
	{
		return (float)i * this.CellSize() - (float)this.gridSize / 2f;
	}

	// Token: 0x06001B61 RID: 7009 RVA: 0x00098920 File Offset: 0x00096B20
	public uint CoordToID(int x, int y)
	{
		return (uint)(x * this.cellCount + y + this.startID);
	}

	// Token: 0x06001B62 RID: 7010 RVA: 0x00098934 File Offset: 0x00096B34
	public uint GetID(Vector3 vPos)
	{
		int num = this.PositionToGrid(vPos.x);
		int num2 = this.PositionToGrid(vPos.z);
		if (num < 0)
		{
			return 0u;
		}
		if (num >= this.cellCount)
		{
			return 0u;
		}
		if (num2 < 0)
		{
			return 0u;
		}
		if (num2 >= this.cellCount)
		{
			return 0u;
		}
		uint num3 = this.CoordToID(num, num2);
		if ((ulong)num3 < (ulong)((long)this.startID))
		{
			Debug.LogError(string.Concat(new object[]
			{
				"NetworkVisibilityGrid.GetID - group is below range ",
				num,
				" ",
				num2,
				" ",
				num3,
				" ",
				this.cellCount
			}));
		}
		if ((ulong)num3 > (ulong)((long)(this.startID + this.cellCount * this.cellCount)))
		{
			Debug.LogError(string.Concat(new object[]
			{
				"NetworkVisibilityGrid.GetID - group is higher than range ",
				num,
				" ",
				num2,
				" ",
				num3,
				" ",
				this.cellCount
			}));
		}
		return num3;
	}

	// Token: 0x06001B63 RID: 7011 RVA: 0x00098A74 File Offset: 0x00096C74
	public Vector3 GetPosition(uint uid)
	{
		uid -= (uint)this.startID;
		int i = (int)((ulong)uid / (ulong)((long)this.cellCount));
		int i2 = (int)((ulong)uid % (ulong)((long)this.cellCount));
		return new Vector3(this.GridToPosition(i), 0f, this.GridToPosition(i2));
	}

	// Token: 0x06001B64 RID: 7012 RVA: 0x00098ABC File Offset: 0x00096CBC
	public Bounds GetBounds(uint uid)
	{
		float num = this.CellSize();
		return new Bounds(this.GetPosition(uid), new Vector3(num, 1048576f, num));
	}

	// Token: 0x06001B65 RID: 7013 RVA: 0x00098AE8 File Offset: 0x00096CE8
	public float CellSize()
	{
		return (float)this.gridSize / (float)this.cellCount;
	}

	// Token: 0x06001B66 RID: 7014 RVA: 0x00098AFC File Offset: 0x00096CFC
	public void OnGroupAdded(Group group)
	{
		group.bounds = this.GetBounds(group.ID);
	}

	// Token: 0x06001B67 RID: 7015 RVA: 0x00098B10 File Offset: 0x00096D10
	public bool IsInside(Group group, Vector3 vPos)
	{
		return false || group.ID == 0u || group.bounds.Contains(vPos) || group.bounds.SqrDistance(vPos) < this.switchTolerance;
	}

	// Token: 0x06001B68 RID: 7016 RVA: 0x00098B68 File Offset: 0x00096D68
	public Group GetGroup(Vector3 vPos)
	{
		uint id = this.GetID(vPos);
		if (id == 0u)
		{
			return null;
		}
		Group group = Net.sv.visibility.Get(id);
		if (!this.IsInside(group, vPos))
		{
			float num = group.bounds.SqrDistance(vPos);
			Debug.Log(string.Concat(new object[]
			{
				"Group is inside is all fucked ",
				id,
				"/",
				num,
				"/",
				vPos
			}));
		}
		return group;
	}

	// Token: 0x06001B69 RID: 7017 RVA: 0x00098BF4 File Offset: 0x00096DF4
	public void GetVisibleFrom(Group group, List<Group> groups)
	{
		groups.Add(Net.sv.visibility.Get(0u));
		uint num = group.ID;
		if ((ulong)num < (ulong)((long)this.startID))
		{
			return;
		}
		num -= (uint)this.startID;
		int num2 = (int)((ulong)num / (ulong)((long)this.cellCount));
		int num3 = (int)((ulong)num % (ulong)((long)this.cellCount));
		for (int i = num2 - this.visibilityRadius; i <= num2 + this.visibilityRadius; i++)
		{
			for (int j = num3 - this.visibilityRadius; j <= num3 + this.visibilityRadius; j++)
			{
				groups.Add(Net.sv.visibility.Get(this.CoordToID(i, j)));
			}
		}
	}

	// Token: 0x0400161F RID: 5663
	public int startID = 1024;

	// Token: 0x04001620 RID: 5664
	public int gridSize = 100;

	// Token: 0x04001621 RID: 5665
	public int cellCount = 32;

	// Token: 0x04001622 RID: 5666
	public int visibilityRadius = 2;

	// Token: 0x04001623 RID: 5667
	public float switchTolerance = 20f;
}
