﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005AB RID: 1451
public class GenerateRiverMeshes : global::ProceduralComponent
{
	// Token: 0x06001E55 RID: 7765 RVA: 0x000A96F4 File Offset: 0x000A78F4
	public override void Process(uint seed)
	{
		List<global::PathList> rivers = global::TerrainMeta.Path.Rivers;
		foreach (global::PathList pathList in rivers)
		{
			foreach (Mesh sharedMesh in pathList.CreateMesh())
			{
				GameObject gameObject = new GameObject("River Mesh");
				MeshCollider meshCollider = gameObject.AddComponent<MeshCollider>();
				meshCollider.sharedMaterial = this.RiverPhysicMaterial;
				meshCollider.sharedMesh = sharedMesh;
				gameObject.AddComponent<global::RiverInfo>();
				gameObject.AddComponent<global::AddToWaterMap>();
				gameObject.tag = "River";
				gameObject.layer = 4;
				gameObject.SetHierarchyGroup(pathList.Name, true, false);
			}
		}
	}

	// Token: 0x17000218 RID: 536
	// (get) Token: 0x06001E56 RID: 7766 RVA: 0x000A97F4 File Offset: 0x000A79F4
	public override bool RunOnCache
	{
		get
		{
			return true;
		}
	}

	// Token: 0x04001923 RID: 6435
	public Material RiverMaterial;

	// Token: 0x04001924 RID: 6436
	public PhysicMaterial RiverPhysicMaterial;
}
