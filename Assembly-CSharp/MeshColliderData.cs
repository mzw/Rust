﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using UnityEngine;

// Token: 0x0200024A RID: 586
public class MeshColliderData
{
	// Token: 0x06001037 RID: 4151 RVA: 0x00061F44 File Offset: 0x00060144
	public void Alloc()
	{
		if (this.triangles == null)
		{
			this.triangles = Facepunch.Pool.GetList<int>();
		}
		if (this.vertices == null)
		{
			this.vertices = Facepunch.Pool.GetList<Vector3>();
		}
		if (this.normals == null)
		{
			this.normals = Facepunch.Pool.GetList<Vector3>();
		}
	}

	// Token: 0x06001038 RID: 4152 RVA: 0x00061F94 File Offset: 0x00060194
	public void Free()
	{
		if (this.triangles != null)
		{
			Facepunch.Pool.FreeList<int>(ref this.triangles);
		}
		if (this.vertices != null)
		{
			Facepunch.Pool.FreeList<Vector3>(ref this.vertices);
		}
		if (this.normals != null)
		{
			Facepunch.Pool.FreeList<Vector3>(ref this.normals);
		}
	}

	// Token: 0x06001039 RID: 4153 RVA: 0x00061FE4 File Offset: 0x000601E4
	public void Clear()
	{
		if (this.triangles != null)
		{
			this.triangles.Clear();
		}
		if (this.vertices != null)
		{
			this.vertices.Clear();
		}
		if (this.normals != null)
		{
			this.normals.Clear();
		}
	}

	// Token: 0x0600103A RID: 4154 RVA: 0x00062034 File Offset: 0x00060234
	public void Apply(UnityEngine.Mesh mesh)
	{
		mesh.Clear();
		if (this.vertices != null)
		{
			mesh.SetVertices(this.vertices);
		}
		if (this.triangles != null)
		{
			mesh.SetTriangles(this.triangles, 0);
		}
		if (this.normals != null)
		{
			if (this.normals.Count == this.vertices.Count)
			{
				mesh.SetNormals(this.normals);
			}
			else if (this.normals.Count > 0 && ConVar.Batching.verbose > 0)
			{
				Debug.LogWarning("Skipping collider normals because some meshes were missing them.");
			}
		}
	}

	// Token: 0x0600103B RID: 4155 RVA: 0x000620D4 File Offset: 0x000602D4
	public void Combine(global::MeshColliderGroup meshGroup)
	{
		for (int i = 0; i < meshGroup.data.Count; i++)
		{
			global::MeshColliderInstance meshColliderInstance = meshGroup.data[i];
			Matrix4x4 matrix4x = Matrix4x4.TRS(meshColliderInstance.position, meshColliderInstance.rotation, meshColliderInstance.scale);
			int count = this.vertices.Count;
			for (int j = 0; j < meshColliderInstance.data.triangles.Length; j++)
			{
				this.triangles.Add(count + meshColliderInstance.data.triangles[j]);
			}
			for (int k = 0; k < meshColliderInstance.data.vertices.Length; k++)
			{
				this.vertices.Add(matrix4x.MultiplyPoint3x4(meshColliderInstance.data.vertices[k]));
			}
			for (int l = 0; l < meshColliderInstance.data.normals.Length; l++)
			{
				this.normals.Add(matrix4x.MultiplyVector(meshColliderInstance.data.normals[l]));
			}
		}
	}

	// Token: 0x0600103C RID: 4156 RVA: 0x0006220C File Offset: 0x0006040C
	public void Combine(global::MeshColliderGroup meshGroup, global::MeshColliderLookup colliderLookup)
	{
		for (int i = 0; i < meshGroup.data.Count; i++)
		{
			global::MeshColliderInstance instance = meshGroup.data[i];
			Matrix4x4 matrix4x = Matrix4x4.TRS(instance.position, instance.rotation, instance.scale);
			int count = this.vertices.Count;
			for (int j = 0; j < instance.data.triangles.Length; j++)
			{
				this.triangles.Add(count + instance.data.triangles[j]);
			}
			for (int k = 0; k < instance.data.vertices.Length; k++)
			{
				this.vertices.Add(matrix4x.MultiplyPoint3x4(instance.data.vertices[k]));
			}
			for (int l = 0; l < instance.data.normals.Length; l++)
			{
				this.normals.Add(matrix4x.MultiplyVector(instance.data.normals[l]));
			}
			colliderLookup.Add(instance);
		}
	}

	// Token: 0x04000AFF RID: 2815
	public List<int> triangles;

	// Token: 0x04000B00 RID: 2816
	public List<Vector3> vertices;

	// Token: 0x04000B01 RID: 2817
	public List<Vector3> normals;
}
