﻿using System;
using UnityEngine;

// Token: 0x02000615 RID: 1557
public class SendMessageToEntityOnAnimationFinish : StateMachineBehaviour
{
	// Token: 0x06001F72 RID: 8050 RVA: 0x000B23EC File Offset: 0x000B05EC
	public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (this.repeatRate > Time.time)
		{
			return;
		}
		if (animator.IsInTransition(layerIndex))
		{
			return;
		}
		if (stateInfo.normalizedTime < 1f)
		{
			return;
		}
		for (int i = 0; i < animator.layerCount; i++)
		{
			if (i != layerIndex)
			{
				if (animator.IsInTransition(i))
				{
					return;
				}
				AnimatorStateInfo currentAnimatorStateInfo = animator.GetCurrentAnimatorStateInfo(i);
				if (currentAnimatorStateInfo.speed > 0f && currentAnimatorStateInfo.normalizedTime < 1f)
				{
					return;
				}
			}
		}
		global::BaseEntity baseEntity = animator.gameObject.ToBaseEntity();
		if (baseEntity)
		{
			baseEntity.SendMessage(this.messageToSendToEntity);
		}
	}

	// Token: 0x04001A7B RID: 6779
	public string messageToSendToEntity;

	// Token: 0x04001A7C RID: 6780
	public float repeatRate = 0.1f;

	// Token: 0x04001A7D RID: 6781
	private const float lastMessageSent = 0f;
}
