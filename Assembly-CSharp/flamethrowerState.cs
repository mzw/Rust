﻿using System;

// Token: 0x020007B7 RID: 1975
public enum flamethrowerState
{
	// Token: 0x04002028 RID: 8232
	OFF,
	// Token: 0x04002029 RID: 8233
	PILOT_LIGHT,
	// Token: 0x0400202A RID: 8234
	FLAME_ON
}
