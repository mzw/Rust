﻿using System;
using UnityEngine;

// Token: 0x02000688 RID: 1672
public abstract class Graph : MonoBehaviour
{
	// Token: 0x060020F3 RID: 8435
	protected abstract float GetValue();

	// Token: 0x060020F4 RID: 8436
	protected abstract Color GetColor(float value);

	// Token: 0x060020F5 RID: 8437 RVA: 0x000BAFBC File Offset: 0x000B91BC
	protected Vector3 GetVertex(float x, float y)
	{
		return new Vector3(x, y, 0f);
	}

	// Token: 0x060020F6 RID: 8438 RVA: 0x000BAFCC File Offset: 0x000B91CC
	protected void Update()
	{
		if (this.values == null || this.values.Length != this.Resolution)
		{
			this.values = new float[this.Resolution];
		}
		this.max = 0f;
		for (int i = 0; i < this.values.Length - 1; i++)
		{
			this.max = Mathf.Max(this.max, this.values[i] = this.values[i + 1]);
		}
		this.max = Mathf.Max(this.max, this.CurrentValue = (this.values[this.values.Length - 1] = this.GetValue()));
	}

	// Token: 0x060020F7 RID: 8439 RVA: 0x000BB088 File Offset: 0x000B9288
	protected void OnGUI()
	{
		if (Event.current.type != 7)
		{
			return;
		}
		if (this.values == null || this.values.Length == 0)
		{
			return;
		}
		float num = Mathf.Max(this.Area.width, this.ScreenFill.x * (float)Screen.width);
		float num2 = Mathf.Max(this.Area.height, this.ScreenFill.y * (float)Screen.height);
		float num3 = this.Area.x - this.Pivot.x * num + this.ScreenOrigin.x * (float)Screen.width;
		float num4 = this.Area.y - this.Pivot.y * num2 + this.ScreenOrigin.y * (float)Screen.height;
		GL.PushMatrix();
		this.Material.SetPass(0);
		GL.LoadPixelMatrix();
		GL.Begin(7);
		for (int i = 0; i < this.values.Length; i++)
		{
			float num5 = this.values[i];
			float num6 = num / (float)this.values.Length;
			float num7 = num2 * num5 / this.max;
			float num8 = num3 + (float)i * num6;
			float num9 = num4;
			GL.Color(this.GetColor(num5));
			GL.Vertex(this.GetVertex(num8, num9 + num7));
			GL.Vertex(this.GetVertex(num8 + num6, num9 + num7));
			GL.Vertex(this.GetVertex(num8 + num6, num9));
			GL.Vertex(this.GetVertex(num8, num9));
		}
		GL.End();
		GL.PopMatrix();
	}

	// Token: 0x04001C4E RID: 7246
	public Material Material;

	// Token: 0x04001C4F RID: 7247
	public int Resolution = 128;

	// Token: 0x04001C50 RID: 7248
	public Vector2 ScreenFill = new Vector2(0f, 0f);

	// Token: 0x04001C51 RID: 7249
	public Vector2 ScreenOrigin = new Vector2(0f, 0f);

	// Token: 0x04001C52 RID: 7250
	public Vector2 Pivot = new Vector2(0f, 0f);

	// Token: 0x04001C53 RID: 7251
	public Rect Area = new Rect(0f, 0f, 128f, 32f);

	// Token: 0x04001C54 RID: 7252
	internal float CurrentValue;

	// Token: 0x04001C55 RID: 7253
	private int index;

	// Token: 0x04001C56 RID: 7254
	private float[] values;

	// Token: 0x04001C57 RID: 7255
	private float max;
}
