﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006A6 RID: 1702
public class SelectedBlueprint : SingletonComponent<global::SelectedBlueprint>, global::IInventoryChanged
{
	// Token: 0x17000246 RID: 582
	// (get) Token: 0x06002137 RID: 8503 RVA: 0x000BBDE0 File Offset: 0x000B9FE0
	public static bool isOpen
	{
		get
		{
			return !(SingletonComponent<global::SelectedBlueprint>.Instance == null) && SingletonComponent<global::SelectedBlueprint>.Instance.blueprint != null;
		}
	}

	// Token: 0x04001CCD RID: 7373
	public global::ItemBlueprint blueprint;

	// Token: 0x04001CCE RID: 7374
	public InputField craftAmountText;

	// Token: 0x04001CCF RID: 7375
	public GameObject ingredientGrid;

	// Token: 0x04001CD0 RID: 7376
	public global::IconSkinPicker skinPicker;

	// Token: 0x04001CD1 RID: 7377
	public Image iconImage;

	// Token: 0x04001CD2 RID: 7378
	public Text titleText;

	// Token: 0x04001CD3 RID: 7379
	public Text descriptionText;

	// Token: 0x04001CD4 RID: 7380
	public CanvasGroup CraftArea;

	// Token: 0x04001CD5 RID: 7381
	public Button CraftButton;

	// Token: 0x04001CD6 RID: 7382
	public Text CraftTime;

	// Token: 0x04001CD7 RID: 7383
	public Text CraftAmount;

	// Token: 0x04001CD8 RID: 7384
	public GameObject[] workbenchReqs;

	// Token: 0x04001CD9 RID: 7385
	private global::ItemInformationPanel[] informationPanels;
}
