﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006EF RID: 1775
public class UIParticle : global::BaseMonoBehaviour
{
	// Token: 0x060021B6 RID: 8630 RVA: 0x000BCEBC File Offset: 0x000BB0BC
	public static void Add(global::UIParticle particleSource, RectTransform spawnPosition, RectTransform particleCanvas)
	{
		GameObject gameObject = Object.Instantiate<GameObject>(particleSource.gameObject);
		gameObject.transform.SetParent(spawnPosition, false);
		gameObject.transform.localPosition = new Vector3(Random.Range(0f, spawnPosition.rect.width) - spawnPosition.rect.width * spawnPosition.pivot.x, Random.Range(0f, spawnPosition.rect.height) - spawnPosition.rect.height * spawnPosition.pivot.y, 0f);
		gameObject.transform.SetParent(particleCanvas, true);
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localRotation = Quaternion.identity;
	}

	// Token: 0x060021B7 RID: 8631 RVA: 0x000BCF94 File Offset: 0x000BB194
	private void Start()
	{
		base.transform.localScale *= Random.Range(this.InitialScale.x, this.InitialScale.y);
		this.velocity.x = Random.Range(this.InitialX.x, this.InitialX.y);
		this.velocity.y = Random.Range(this.InitialY.x, this.InitialY.y);
		this.gravity = Random.Range(this.Gravity.x, this.Gravity.y);
		this.scaleVelocity = Random.Range(this.ScaleVelocity.x, this.ScaleVelocity.y);
		Image component = base.GetComponent<Image>();
		if (component)
		{
			component.color = this.InitialColor.Evaluate(Random.Range(0f, 1f));
		}
		this.lifetime = Random.Range(this.InitialDelay.x, this.InitialDelay.y) * -1f;
		if (this.lifetime < 0f)
		{
			base.GetComponent<CanvasGroup>().alpha = 0f;
		}
		base.Invoke(new Action(this.Die), Random.Range(this.LifeTime.x, this.LifeTime.y) + this.lifetime * -1f);
	}

	// Token: 0x060021B8 RID: 8632 RVA: 0x000BD114 File Offset: 0x000BB314
	private void Update()
	{
		if (this.lifetime < 0f)
		{
			this.lifetime += Time.deltaTime;
			if (this.lifetime < 0f)
			{
				return;
			}
			base.GetComponent<CanvasGroup>().alpha = 1f;
		}
		else
		{
			this.lifetime += Time.deltaTime;
		}
		Vector3 position = base.transform.position;
		Vector3 vector = base.transform.localScale;
		this.velocity.y = this.velocity.y - this.gravity * Time.deltaTime;
		position.x += this.velocity.x * Time.deltaTime;
		position.y += this.velocity.y * Time.deltaTime;
		vector += Vector3.one * this.scaleVelocity * Time.deltaTime;
		if (vector.x <= 0f || vector.y <= 0f)
		{
			this.Die();
			return;
		}
		base.transform.position = position;
		base.transform.localScale = vector;
	}

	// Token: 0x060021B9 RID: 8633 RVA: 0x000BD254 File Offset: 0x000BB454
	private void Die()
	{
		Object.Destroy(base.gameObject);
	}

	// Token: 0x04001E16 RID: 7702
	public Vector2 LifeTime;

	// Token: 0x04001E17 RID: 7703
	public Vector2 Gravity = new Vector2(1000f, 1000f);

	// Token: 0x04001E18 RID: 7704
	public Vector2 InitialX;

	// Token: 0x04001E19 RID: 7705
	public Vector2 InitialY;

	// Token: 0x04001E1A RID: 7706
	public Vector2 InitialScale = Vector2.one;

	// Token: 0x04001E1B RID: 7707
	public Vector2 InitialDelay;

	// Token: 0x04001E1C RID: 7708
	public Vector2 ScaleVelocity;

	// Token: 0x04001E1D RID: 7709
	public Gradient InitialColor;

	// Token: 0x04001E1E RID: 7710
	private float lifetime;

	// Token: 0x04001E1F RID: 7711
	private float gravity;

	// Token: 0x04001E20 RID: 7712
	private Vector2 velocity;

	// Token: 0x04001E21 RID: 7713
	private float scaleVelocity;
}
