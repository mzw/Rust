﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x02000735 RID: 1845
public abstract class BaseMonoBehaviour : FacepunchBehaviour
{
	// Token: 0x060022B7 RID: 8887 RVA: 0x000C163C File Offset: 0x000BF83C
	public virtual bool IsDebugging()
	{
		return false;
	}

	// Token: 0x060022B8 RID: 8888 RVA: 0x000C1640 File Offset: 0x000BF840
	public virtual string GetLogColor()
	{
		return "yellow";
	}

	// Token: 0x060022B9 RID: 8889 RVA: 0x000C1648 File Offset: 0x000BF848
	public void LogEntry(global::BaseMonoBehaviour.LogEntryType log, int level, string str, object arg1)
	{
		if (!this.IsDebugging() && ConVar.Global.developer < level)
		{
			return;
		}
		string text = string.Format(str, arg1);
		string text2 = string.Format("<color=white>{0}</color>[<color={3}>{1}</color>] {2}", new object[]
		{
			log.ToString().PadRight(10),
			this.ToString(),
			text,
			this.GetLogColor()
		});
		Debug.Log(text2, base.gameObject);
	}

	// Token: 0x060022BA RID: 8890 RVA: 0x000C16C0 File Offset: 0x000BF8C0
	public void LogEntry(global::BaseMonoBehaviour.LogEntryType log, int level, string str, object arg1, object arg2)
	{
		if (!this.IsDebugging() && ConVar.Global.developer < level)
		{
			return;
		}
		string text = string.Format(str, arg1, arg2);
		string text2 = string.Format("<color=white>{0}</color>[<color={3}>{1}</color>] {2}", new object[]
		{
			log.ToString().PadRight(10),
			this.ToString(),
			text,
			this.GetLogColor()
		});
		Debug.Log(text2, base.gameObject);
	}

	// Token: 0x060022BB RID: 8891 RVA: 0x000C173C File Offset: 0x000BF93C
	public void LogEntry(global::BaseMonoBehaviour.LogEntryType log, int level, string str)
	{
		if (!this.IsDebugging() && ConVar.Global.developer < level)
		{
			return;
		}
		string text = string.Format("<color=white>{0}</color>[<color={3}>{1}</color>] {2}", new object[]
		{
			log.ToString().PadRight(10),
			this.ToString(),
			str,
			this.GetLogColor()
		});
		Debug.Log(text, base.gameObject);
	}

	// Token: 0x02000736 RID: 1846
	public enum LogEntryType
	{
		// Token: 0x04001F34 RID: 7988
		General,
		// Token: 0x04001F35 RID: 7989
		Network,
		// Token: 0x04001F36 RID: 7990
		Hierarchy,
		// Token: 0x04001F37 RID: 7991
		Serialization
	}
}
