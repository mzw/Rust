﻿using System;
using UnityEngine;

// Token: 0x02000466 RID: 1126
public class PrefabInformation : global::PrefabAttribute
{
	// Token: 0x060018BE RID: 6334 RVA: 0x0008B3D4 File Offset: 0x000895D4
	protected override Type GetIndexedType()
	{
		return typeof(global::PrefabInformation);
	}

	// Token: 0x04001375 RID: 4981
	public global::ItemDefinition associatedItemDefinition;

	// Token: 0x04001376 RID: 4982
	public global::Translate.Phrase title;

	// Token: 0x04001377 RID: 4983
	public global::Translate.Phrase description;

	// Token: 0x04001378 RID: 4984
	public Sprite sprite;
}
