﻿using System;
using UnityEngine;

// Token: 0x020005D8 RID: 1496
public class TerrainTopologySet : global::TerrainModifier
{
	// Token: 0x06001ECA RID: 7882 RVA: 0x000AD940 File Offset: 0x000ABB40
	protected override void Apply(Vector3 position, float opacity, float radius, float fade)
	{
		if (!global::TerrainMeta.TopologyMap)
		{
			return;
		}
		global::TerrainMeta.TopologyMap.SetTopology(position, (int)this.TopologyType, radius, fade);
	}

	// Token: 0x0400199E RID: 6558
	[InspectorFlags]
	public global::TerrainTopology.Enum TopologyType = global::TerrainTopology.Enum.Decor;
}
