﻿using System;

// Token: 0x02000610 RID: 1552
public struct MaterialPropertyDesc
{
	// Token: 0x06001F6B RID: 8043 RVA: 0x000B2398 File Offset: 0x000B0598
	public MaterialPropertyDesc(string name, Type type)
	{
		this.name = name;
		this.type = type;
	}

	// Token: 0x04001A74 RID: 6772
	public string name;

	// Token: 0x04001A75 RID: 6773
	public Type type;
}
