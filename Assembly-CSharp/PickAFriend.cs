﻿using System;
using UnityEngine.UI;

// Token: 0x0200067E RID: 1662
public class PickAFriend : global::UIDialog
{
	// Token: 0x04001C32 RID: 7218
	public InputField input;

	// Token: 0x04001C33 RID: 7219
	public Action<ulong> onSelected;
}
