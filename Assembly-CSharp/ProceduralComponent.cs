﻿using System;
using UnityEngine;

// Token: 0x02000594 RID: 1428
public abstract class ProceduralComponent : MonoBehaviour
{
	// Token: 0x17000217 RID: 535
	// (get) Token: 0x06001E21 RID: 7713 RVA: 0x000A76F4 File Offset: 0x000A58F4
	public virtual bool RunOnCache
	{
		get
		{
			return false;
		}
	}

	// Token: 0x06001E22 RID: 7714 RVA: 0x000A76F8 File Offset: 0x000A58F8
	public bool ShouldRun()
	{
		return (!global::World.Serialization.Cached || this.RunOnCache) && (this.Mode & global::ProceduralComponent.Realm.Server) != (global::ProceduralComponent.Realm)0;
	}

	// Token: 0x06001E23 RID: 7715
	public abstract void Process(uint seed);

	// Token: 0x040018B3 RID: 6323
	[InspectorFlags]
	public global::ProceduralComponent.Realm Mode = (global::ProceduralComponent.Realm)3;

	// Token: 0x040018B4 RID: 6324
	public string Description = "Procedural Component";

	// Token: 0x02000595 RID: 1429
	public enum Realm
	{
		// Token: 0x040018B6 RID: 6326
		Client = 1,
		// Token: 0x040018B7 RID: 6327
		Server
	}
}
