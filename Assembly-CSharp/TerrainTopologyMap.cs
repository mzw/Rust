﻿using System;
using UnityEngine;

// Token: 0x02000580 RID: 1408
public class TerrainTopologyMap : global::TerrainMap2D<int>
{
	// Token: 0x06001D94 RID: 7572 RVA: 0x000A58FC File Offset: 0x000A3AFC
	public override void Setup()
	{
		if (this.TopologyTexture != null)
		{
			if (this.TopologyTexture.width == this.TopologyTexture.height)
			{
				this.res = this.TopologyTexture.width;
				this.src = (this.dst = new int[this.res, this.res]);
				Color32[] pixels = this.TopologyTexture.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						this.dst[i, j] = global::TextureData.DecodeInt(pixels[num]);
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid topology texture: " + this.TopologyTexture.name);
			}
		}
		else
		{
			this.res = this.terrain.terrainData.alphamapResolution;
			this.src = (this.dst = new int[this.res, this.res]);
		}
	}

	// Token: 0x06001D95 RID: 7573 RVA: 0x000A5A24 File Offset: 0x000A3C24
	public void GenerateTextures()
	{
		this.TopologyTexture = new Texture2D(this.res, this.res, 4, false, true);
		this.TopologyTexture.name = "TopologyTexture";
		this.TopologyTexture.wrapMode = 1;
		Color32[] col = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				col[z * this.res + i] = global::TextureData.EncodeInt(this.src[z, i]);
			}
		});
		this.TopologyTexture.SetPixels32(col);
	}

	// Token: 0x06001D96 RID: 7574 RVA: 0x000A5AB8 File Offset: 0x000A3CB8
	public void ApplyTextures()
	{
		this.TopologyTexture.Apply(false, true);
	}

	// Token: 0x06001D97 RID: 7575 RVA: 0x000A5AC8 File Offset: 0x000A3CC8
	public bool GetTopology(Vector3 worldPos, int mask)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetTopology(normX, normZ, mask);
	}

	// Token: 0x06001D98 RID: 7576 RVA: 0x000A5AF8 File Offset: 0x000A3CF8
	public bool GetTopology(float normX, float normZ, int mask)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetTopology(x, z, mask);
	}

	// Token: 0x06001D99 RID: 7577 RVA: 0x000A5B20 File Offset: 0x000A3D20
	public bool GetTopology(int x, int z, int mask)
	{
		return (this.src[z, x] & mask) != 0;
	}

	// Token: 0x06001D9A RID: 7578 RVA: 0x000A5B38 File Offset: 0x000A3D38
	public int GetTopology(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetTopology(normX, normZ);
	}

	// Token: 0x06001D9B RID: 7579 RVA: 0x000A5B68 File Offset: 0x000A3D68
	public int GetTopology(float normX, float normZ)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		return this.GetTopology(x, z);
	}

	// Token: 0x06001D9C RID: 7580 RVA: 0x000A5B90 File Offset: 0x000A3D90
	public int GetTopology(int x, int z)
	{
		return this.src[z, x];
	}

	// Token: 0x06001D9D RID: 7581 RVA: 0x000A5BA0 File Offset: 0x000A3DA0
	public void SetTopology(Vector3 worldPos, int mask)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetTopology(normX, normZ, mask);
	}

	// Token: 0x06001D9E RID: 7582 RVA: 0x000A5BD0 File Offset: 0x000A3DD0
	public void SetTopology(float normX, float normZ, int mask)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetTopology(x, z, mask);
	}

	// Token: 0x06001D9F RID: 7583 RVA: 0x000A5BF8 File Offset: 0x000A3DF8
	public void SetTopology(int x, int z, int mask)
	{
		this.dst[z, x] = mask;
	}

	// Token: 0x06001DA0 RID: 7584 RVA: 0x000A5C08 File Offset: 0x000A3E08
	public void AddTopology(Vector3 worldPos, int mask)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.AddTopology(normX, normZ, mask);
	}

	// Token: 0x06001DA1 RID: 7585 RVA: 0x000A5C38 File Offset: 0x000A3E38
	public void AddTopology(float normX, float normZ, int mask)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.AddTopology(x, z, mask);
	}

	// Token: 0x06001DA2 RID: 7586 RVA: 0x000A5C60 File Offset: 0x000A3E60
	public void AddTopology(int x, int z, int mask)
	{
		this.dst[z, x] |= mask;
	}

	// Token: 0x06001DA3 RID: 7587 RVA: 0x000A5C74 File Offset: 0x000A3E74
	public void RemoveTopology(Vector3 worldPos, int mask)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.RemoveTopology(normX, normZ, mask);
	}

	// Token: 0x06001DA4 RID: 7588 RVA: 0x000A5CA4 File Offset: 0x000A3EA4
	public void RemoveTopology(float normX, float normZ, int mask)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.RemoveTopology(x, z, mask);
	}

	// Token: 0x06001DA5 RID: 7589 RVA: 0x000A5CCC File Offset: 0x000A3ECC
	public void RemoveTopology(int x, int z, int mask)
	{
		this.dst[z, x] &= ~mask;
	}

	// Token: 0x06001DA6 RID: 7590 RVA: 0x000A5CE4 File Offset: 0x000A3EE4
	public int GetTopology(Vector3 worldPos, float radius)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetTopology(normX, normZ, radius);
	}

	// Token: 0x06001DA7 RID: 7591 RVA: 0x000A5D14 File Offset: 0x000A3F14
	public int GetTopology(float normX, float normZ, float radius)
	{
		int num = 0;
		float num2 = global::TerrainMeta.OneOverSize.x * radius;
		int num3 = base.Index(normX - num2);
		int num4 = base.Index(normX + num2);
		int num5 = base.Index(normZ - num2);
		int num6 = base.Index(normZ + num2);
		for (int i = num5; i <= num6; i++)
		{
			for (int j = num3; j <= num4; j++)
			{
				num |= this.src[i, j];
			}
		}
		return num;
	}

	// Token: 0x06001DA8 RID: 7592 RVA: 0x000A5DA0 File Offset: 0x000A3FA0
	public void SetTopology(Vector3 worldPos, int mask, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetTopology(normX, normZ, mask, radius, fade);
	}

	// Token: 0x06001DA9 RID: 7593 RVA: 0x000A5DD4 File Offset: 0x000A3FD4
	public void SetTopology(float normX, float normZ, int mask, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if ((double)lerp > 0.5)
			{
				this.dst[z, x] = mask;
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x06001DAA RID: 7594 RVA: 0x000A5E10 File Offset: 0x000A4010
	public void AddTopology(Vector3 worldPos, int mask, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.AddTopology(normX, normZ, mask, radius, fade);
	}

	// Token: 0x06001DAB RID: 7595 RVA: 0x000A5E44 File Offset: 0x000A4044
	public void AddTopology(float normX, float normZ, int mask, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			if ((double)lerp > 0.5)
			{
				this.dst[z, x] |= mask;
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x04001848 RID: 6216
	public Texture2D TopologyTexture;
}
