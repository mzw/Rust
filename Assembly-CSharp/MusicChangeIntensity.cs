﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001E2 RID: 482
public class MusicChangeIntensity : MonoBehaviour
{
	// Token: 0x0400095E RID: 2398
	public float raiseTo;

	// Token: 0x0400095F RID: 2399
	public List<global::MusicChangeIntensity.DistanceIntensity> distanceIntensities = new List<global::MusicChangeIntensity.DistanceIntensity>();

	// Token: 0x04000960 RID: 2400
	public float tickInterval = 0.2f;

	// Token: 0x020001E3 RID: 483
	[Serializable]
	public class DistanceIntensity
	{
		// Token: 0x04000961 RID: 2401
		public float distance = 60f;

		// Token: 0x04000962 RID: 2402
		public float raiseTo;
	}
}
