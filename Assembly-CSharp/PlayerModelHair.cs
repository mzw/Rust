﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000274 RID: 628
public class PlayerModelHair : MonoBehaviour
{
	// Token: 0x1700011F RID: 287
	// (get) Token: 0x060010A8 RID: 4264 RVA: 0x00064440 File Offset: 0x00062640
	public Dictionary<Renderer, global::PlayerModelHair.RendererMaterials> Materials
	{
		get
		{
			return this.materials;
		}
	}

	// Token: 0x060010A9 RID: 4265 RVA: 0x00064448 File Offset: 0x00062648
	private void CacheOriginalMaterials()
	{
		if (this.materials == null)
		{
			this.materials = new Dictionary<Renderer, global::PlayerModelHair.RendererMaterials>();
			List<SkinnedMeshRenderer> list = Pool.GetList<SkinnedMeshRenderer>();
			base.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true, list);
			this.materials.Clear();
			foreach (SkinnedMeshRenderer skinnedMeshRenderer in list)
			{
				this.materials.Add(skinnedMeshRenderer, new global::PlayerModelHair.RendererMaterials(skinnedMeshRenderer));
			}
			Pool.FreeList<SkinnedMeshRenderer>(ref list);
		}
	}

	// Token: 0x060010AA RID: 4266 RVA: 0x000644E8 File Offset: 0x000626E8
	private void Setup(global::HairType type, global::HairSetCollection hair, int baseSeed, float typeNum, int meshIndex, MaterialPropertyBlock block)
	{
		this.CacheOriginalMaterials();
		global::HairSetCollection.HairSetEntry hairSetEntry = hair.Get(type, typeNum);
		if (hairSetEntry.HairSet == null)
		{
			Debug.LogWarning("Hair.Get returned a NULL hair");
			return;
		}
		int blendShapeIndex = -1;
		if (type == global::HairType.Facial || type == global::HairType.Eyebrow)
		{
			blendShapeIndex = meshIndex;
		}
		global::HairDye dye = null;
		if (hairSetEntry.HairDyeCollection != null)
		{
			Random.InitState(baseSeed + meshIndex);
			float seed = Random.Range(0f, 1f);
			dye = hairSetEntry.HairDyeCollection.Get(seed);
		}
		hairSetEntry.HairSet.Process(this, dye, block);
		hairSetEntry.HairSet.ProcessMorphs(base.gameObject, blendShapeIndex);
	}

	// Token: 0x060010AB RID: 4267 RVA: 0x00064594 File Offset: 0x00062794
	public void Setup(global::SkinSetCollection skin, float hairNum, float meshNum, MaterialPropertyBlock block)
	{
		int index = skin.GetIndex(meshNum);
		global::SkinSet skinSet = skin.Skins[index];
		if (skinSet == null)
		{
			Debug.LogError("Skin.Get returned a NULL skin");
		}
		else
		{
			int num = (int)this.type;
			int num2 = Mathf.FloorToInt(hairNum * 100000f);
			Random.InitState(num2 + num);
			float typeNum = Random.Range(0f, 1f);
			this.Setup(this.type, skinSet.HairCollection, num2, typeNum, index, block);
		}
	}

	// Token: 0x04000B97 RID: 2967
	public global::HairType type;

	// Token: 0x04000B98 RID: 2968
	private Dictionary<Renderer, global::PlayerModelHair.RendererMaterials> materials;

	// Token: 0x02000275 RID: 629
	public struct RendererMaterials
	{
		// Token: 0x060010AC RID: 4268 RVA: 0x00064614 File Offset: 0x00062814
		public RendererMaterials(Renderer r)
		{
			this.original = r.sharedMaterials;
			this.replacement = (this.original.Clone() as Material[]);
			this.names = new string[this.original.Length];
			for (int i = 0; i < this.original.Length; i++)
			{
				this.names[i] = this.original[i].name;
			}
		}

		// Token: 0x04000B99 RID: 2969
		public string[] names;

		// Token: 0x04000B9A RID: 2970
		public Material[] original;

		// Token: 0x04000B9B RID: 2971
		public Material[] replacement;
	}
}
