﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x020007C8 RID: 1992
[AddComponentMenu("")]
public class AmplifyOcclusionBase : MonoBehaviour
{
	// Token: 0x060024E0 RID: 9440 RVA: 0x000CAA28 File Offset: 0x000C8C28
	private bool CheckParamsChanged()
	{
		return this.prevScreenWidth != this.m_camera.pixelWidth || this.prevScreenHeight != this.m_camera.pixelHeight || this.prevHDR != this.m_camera.allowHDR || this.prevApplyMethod != this.ApplyMethod || this.prevSampleCount != this.SampleCount || this.prevPerPixelNormals != this.PerPixelNormals || this.prevCacheAware != this.CacheAware || this.prevDownscale != this.Downsample || this.prevFadeEnabled != this.FadeEnabled || this.prevFadeToIntensity != this.FadeToIntensity || this.prevFadeToRadius != this.FadeToRadius || this.prevFadeToPowerExponent != this.FadeToPowerExponent || this.prevFadeStart != this.FadeStart || this.prevFadeLength != this.FadeLength || this.prevBlurEnabled != this.BlurEnabled || this.prevBlurRadius != this.BlurRadius || this.prevBlurPasses != this.BlurPasses;
	}

	// Token: 0x060024E1 RID: 9441 RVA: 0x000CAB6C File Offset: 0x000C8D6C
	private void UpdateParams()
	{
		this.prevScreenWidth = this.m_camera.pixelWidth;
		this.prevScreenHeight = this.m_camera.pixelHeight;
		this.prevHDR = this.m_camera.allowHDR;
		this.prevApplyMethod = this.ApplyMethod;
		this.prevSampleCount = this.SampleCount;
		this.prevPerPixelNormals = this.PerPixelNormals;
		this.prevCacheAware = this.CacheAware;
		this.prevDownscale = this.Downsample;
		this.prevFadeEnabled = this.FadeEnabled;
		this.prevFadeToIntensity = this.FadeToIntensity;
		this.prevFadeToRadius = this.FadeToRadius;
		this.prevFadeToPowerExponent = this.FadeToPowerExponent;
		this.prevFadeStart = this.FadeStart;
		this.prevFadeLength = this.FadeLength;
		this.prevBlurEnabled = this.BlurEnabled;
		this.prevBlurRadius = this.BlurRadius;
		this.prevBlurPasses = this.BlurPasses;
	}

	// Token: 0x060024E2 RID: 9442 RVA: 0x000CAC54 File Offset: 0x000C8E54
	private void Warmup()
	{
		this.CheckMaterial();
		this.CheckRandomData();
		this.m_depthLayerRT = new int[16];
		this.m_normalLayerRT = new int[16];
		this.m_occlusionLayerRT = new int[16];
		this.m_mrtCount = Mathf.Min(SystemInfo.supportedRenderTargetCount, 4);
		this.m_layerOffsetNames = new string[this.m_mrtCount];
		this.m_layerRandomNames = new string[this.m_mrtCount];
		for (int i = 0; i < this.m_mrtCount; i++)
		{
			this.m_layerOffsetNames[i] = "_AO_LayerOffset" + i;
			this.m_layerRandomNames[i] = "_AO_LayerRandom" + i;
		}
		this.m_layerDepthNames = new string[16];
		this.m_layerNormalNames = new string[16];
		this.m_layerOcclusionNames = new string[16];
		for (int j = 0; j < 16; j++)
		{
			this.m_layerDepthNames[j] = "_AO_DepthLayer" + j;
			this.m_layerNormalNames[j] = "_AO_NormalLayer" + j;
			this.m_layerOcclusionNames[j] = "_AO_OcclusionLayer" + j;
		}
		this.m_depthTargets = new RenderTargetIdentifier[this.m_mrtCount];
		this.m_normalTargets = new RenderTargetIdentifier[this.m_mrtCount];
		int mrtCount = this.m_mrtCount;
		if (mrtCount != 4)
		{
			this.m_deinterleaveDepthPass = 5;
			this.m_deinterleaveNormalPass = 6;
		}
		else
		{
			this.m_deinterleaveDepthPass = 10;
			this.m_deinterleaveNormalPass = 11;
		}
		this.m_applyDeferredTargets = new RenderTargetIdentifier[2];
		if (this.m_blitMesh != null)
		{
			Object.DestroyImmediate(this.m_blitMesh);
		}
		this.m_blitMesh = new Mesh();
		this.m_blitMesh.vertices = new Vector3[]
		{
			new Vector3(0f, 0f, 0f),
			new Vector3(0f, 1f, 0f),
			new Vector3(1f, 1f, 0f),
			new Vector3(1f, 0f, 0f)
		};
		this.m_blitMesh.uv = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(0f, 1f),
			new Vector2(1f, 1f),
			new Vector2(1f, 0f)
		};
		this.m_blitMesh.triangles = new int[]
		{
			0,
			1,
			2,
			0,
			2,
			3
		};
	}

	// Token: 0x060024E3 RID: 9443 RVA: 0x000CAF4C File Offset: 0x000C914C
	private void Shutdown()
	{
		this.CommandBuffer_UnregisterAll();
		this.SafeReleaseRT(ref this.m_occlusionRT);
		if (this.m_occlusionMat != null)
		{
			Object.DestroyImmediate(this.m_occlusionMat);
		}
		if (this.m_blurMat != null)
		{
			Object.DestroyImmediate(this.m_blurMat);
		}
		if (this.m_copyMat != null)
		{
			Object.DestroyImmediate(this.m_copyMat);
		}
		if (this.m_randomTex != null)
		{
			Object.DestroyImmediate(this.m_randomTex);
		}
		if (this.m_blitMesh != null)
		{
			Object.DestroyImmediate(this.m_blitMesh);
		}
	}

	// Token: 0x060024E4 RID: 9444 RVA: 0x000CAFF8 File Offset: 0x000C91F8
	private bool CheckRenderTextureFormats()
	{
		if (SystemInfo.SupportsRenderTextureFormat(0) && SystemInfo.SupportsRenderTextureFormat(2))
		{
			this.m_depthRTFormat = 14;
			if (!SystemInfo.SupportsRenderTextureFormat(this.m_depthRTFormat))
			{
				this.m_depthRTFormat = 15;
				if (!SystemInfo.SupportsRenderTextureFormat(this.m_depthRTFormat))
				{
					this.m_depthRTFormat = 2;
				}
			}
			this.m_normalRTFormat = 8;
			if (!SystemInfo.SupportsRenderTextureFormat(this.m_normalRTFormat))
			{
				this.m_normalRTFormat = 0;
			}
			this.m_occlusionRTFormat = 13;
			if (!SystemInfo.SupportsRenderTextureFormat(this.m_occlusionRTFormat))
			{
				this.m_occlusionRTFormat = 12;
				if (!SystemInfo.SupportsRenderTextureFormat(this.m_occlusionRTFormat))
				{
					this.m_occlusionRTFormat = 2;
				}
			}
			return true;
		}
		return false;
	}

	// Token: 0x060024E5 RID: 9445 RVA: 0x000CB0AC File Offset: 0x000C92AC
	private void OnEnable()
	{
		if (SystemInfo.graphicsDeviceType == 4)
		{
			Debug.Log("[AmplifyOcclusion] Null graphics device detected. Skipping effect silently.");
			base.enabled = false;
			return;
		}
		if (!this.CheckRenderTextureFormats())
		{
			Debug.LogWarning("[AmplifyOcclusion] Target platform does not meet the minimum requirements for this effect to work properly.");
			base.enabled = false;
			return;
		}
		this.m_camera = base.GetComponent<Camera>();
		this.Warmup();
		this.CommandBuffer_UnregisterAll();
	}

	// Token: 0x060024E6 RID: 9446 RVA: 0x000CB10C File Offset: 0x000C930C
	private void OnDisable()
	{
		this.Shutdown();
	}

	// Token: 0x060024E7 RID: 9447 RVA: 0x000CB114 File Offset: 0x000C9314
	private void OnDestroy()
	{
		this.Shutdown();
	}

	// Token: 0x060024E8 RID: 9448 RVA: 0x000CB11C File Offset: 0x000C931C
	private void Update()
	{
		if (this.m_camera.actualRenderingPath != 3)
		{
			if (this.PerPixelNormals != global::AmplifyOcclusionBase.PerPixelNormalSource.None && this.PerPixelNormals != global::AmplifyOcclusionBase.PerPixelNormalSource.Camera)
			{
				this.PerPixelNormals = global::AmplifyOcclusionBase.PerPixelNormalSource.Camera;
				Debug.LogWarning("[AmplifyOcclusion] GBuffer Normals only available in Camera Deferred Shading mode. Switched to Camera source.");
			}
			if (this.ApplyMethod == global::AmplifyOcclusionBase.ApplicationMethod.Deferred)
			{
				this.ApplyMethod = global::AmplifyOcclusionBase.ApplicationMethod.PostEffect;
				Debug.LogWarning("[AmplifyOcclusion] Deferred Method requires a Deferred Shading path. Switching to Post Effect Method.");
			}
		}
		if (this.ApplyMethod == global::AmplifyOcclusionBase.ApplicationMethod.Deferred && this.PerPixelNormals == global::AmplifyOcclusionBase.PerPixelNormalSource.Camera)
		{
			this.PerPixelNormals = global::AmplifyOcclusionBase.PerPixelNormalSource.GBuffer;
			Debug.LogWarning("[AmplifyOcclusion] Camera Normals not supported for Deferred Method. Switching to GBuffer Normals.");
		}
		if ((this.m_camera.depthTextureMode & 1) == null)
		{
			this.m_camera.depthTextureMode |= 1;
		}
		if (this.PerPixelNormals == global::AmplifyOcclusionBase.PerPixelNormalSource.Camera && (this.m_camera.depthTextureMode & 2) == null)
		{
			this.m_camera.depthTextureMode |= 2;
		}
		this.CheckMaterial();
		this.CheckRandomData();
	}

	// Token: 0x060024E9 RID: 9449 RVA: 0x000CB20C File Offset: 0x000C940C
	private void CheckMaterial()
	{
		if (this.m_occlusionMat == null)
		{
			this.m_occlusionMat = new Material(Shader.Find("Hidden/Amplify Occlusion/Occlusion"))
			{
				hideFlags = 52
			};
		}
		if (this.m_blurMat == null)
		{
			this.m_blurMat = new Material(Shader.Find("Hidden/Amplify Occlusion/Blur"))
			{
				hideFlags = 52
			};
		}
		if (this.m_copyMat == null)
		{
			this.m_copyMat = new Material(Shader.Find("Hidden/Amplify Occlusion/Copy"))
			{
				hideFlags = 52
			};
		}
	}

	// Token: 0x060024EA RID: 9450 RVA: 0x000CB2AC File Offset: 0x000C94AC
	private void CheckRandomData()
	{
		if (this.m_randomData == null || this.m_randomData.Length == 0)
		{
			this.m_randomData = global::AmplifyOcclusionBase.GenerateRandomizationData();
		}
		if (this.m_randomTex == null)
		{
			this.m_randomTex = global::AmplifyOcclusionBase.GenerateRandomizationTexture(this.m_randomData);
		}
	}

	// Token: 0x060024EB RID: 9451 RVA: 0x000CB300 File Offset: 0x000C9500
	public static Color[] GenerateRandomizationData()
	{
		Color[] array = new Color[16];
		int i = 0;
		int num = 0;
		while (i < 16)
		{
			float num2 = global::RandomTable.Values[num++];
			float b = global::RandomTable.Values[num++];
			float num3 = 6.28318548f * num2 / 8f;
			array[i].r = Mathf.Cos(num3);
			array[i].g = Mathf.Sin(num3);
			array[i].b = b;
			array[i].a = 0f;
			i++;
		}
		return array;
	}

	// Token: 0x060024EC RID: 9452 RVA: 0x000CB398 File Offset: 0x000C9598
	public static Texture2D GenerateRandomizationTexture(Color[] randomPixels)
	{
		Texture2D texture2D = new Texture2D(4, 4, 5, false, true)
		{
			hideFlags = 52
		};
		texture2D.name = "RandomTexture";
		texture2D.filterMode = 0;
		texture2D.wrapMode = 0;
		texture2D.SetPixels(randomPixels);
		texture2D.Apply();
		return texture2D;
	}

	// Token: 0x060024ED RID: 9453 RVA: 0x000CB3E4 File Offset: 0x000C95E4
	private RenderTexture SafeAllocateRT(string name, int width, int height, RenderTextureFormat format, RenderTextureReadWrite readWrite)
	{
		width = Mathf.Max(width, 1);
		height = Mathf.Max(height, 1);
		RenderTexture renderTexture = new RenderTexture(width, height, 0, format, readWrite)
		{
			hideFlags = 52
		};
		renderTexture.name = name;
		renderTexture.filterMode = 0;
		renderTexture.wrapMode = 1;
		renderTexture.Create();
		return renderTexture;
	}

	// Token: 0x060024EE RID: 9454 RVA: 0x000CB438 File Offset: 0x000C9638
	private void SafeReleaseRT(ref RenderTexture rt)
	{
		if (rt != null)
		{
			RenderTexture.active = null;
			rt.Release();
			Object.DestroyImmediate(rt);
			rt = null;
		}
	}

	// Token: 0x060024EF RID: 9455 RVA: 0x000CB460 File Offset: 0x000C9660
	private int SafeAllocateTemporaryRT(CommandBuffer cb, string propertyName, int width, int height, RenderTextureFormat format = 7, RenderTextureReadWrite readWrite = 0, FilterMode filterMode = 0)
	{
		int num = Shader.PropertyToID(propertyName);
		cb.GetTemporaryRT(num, width, height, 0, filterMode, format, readWrite);
		return num;
	}

	// Token: 0x060024F0 RID: 9456 RVA: 0x000CB488 File Offset: 0x000C9688
	private void SafeReleaseTemporaryRT(CommandBuffer cb, int id)
	{
		cb.ReleaseTemporaryRT(id);
	}

	// Token: 0x060024F1 RID: 9457 RVA: 0x000CB494 File Offset: 0x000C9694
	private void SetBlitTarget(CommandBuffer cb, RenderTargetIdentifier[] targets, int targetWidth, int targetHeight)
	{
		cb.SetGlobalVector("_AO_Target_TexelSize", new Vector4(1f / (float)targetWidth, 1f / (float)targetHeight, (float)targetWidth, (float)targetHeight));
		cb.SetGlobalVector("_AO_Target_Position", Vector2.zero);
		cb.SetRenderTarget(targets, targets[0]);
	}

	// Token: 0x060024F2 RID: 9458 RVA: 0x000CB4F0 File Offset: 0x000C96F0
	private void SetBlitTarget(CommandBuffer cb, RenderTargetIdentifier target, int targetWidth, int targetHeight)
	{
		cb.SetGlobalVector("_AO_Target_TexelSize", new Vector4(1f / (float)targetWidth, 1f / (float)targetHeight, (float)targetWidth, (float)targetHeight));
		cb.SetRenderTarget(target);
	}

	// Token: 0x060024F3 RID: 9459 RVA: 0x000CB520 File Offset: 0x000C9720
	private void PerformBlit(CommandBuffer cb, Material mat, int pass)
	{
		cb.DrawMesh(this.m_blitMesh, Matrix4x4.identity, mat, 0, pass);
	}

	// Token: 0x060024F4 RID: 9460 RVA: 0x000CB538 File Offset: 0x000C9738
	private void PerformBlit(CommandBuffer cb, Material mat, int pass, int x, int y)
	{
		cb.SetGlobalVector("_AO_Target_Position", new Vector2((float)x, (float)y));
		this.PerformBlit(cb, mat, pass);
	}

	// Token: 0x060024F5 RID: 9461 RVA: 0x000CB560 File Offset: 0x000C9760
	private void PerformBlit(CommandBuffer cb, RenderTargetIdentifier source, int sourceWidth, int sourceHeight, Material mat, int pass)
	{
		cb.SetGlobalTexture("_AO_Source", source);
		cb.SetGlobalVector("_AO_Source_TexelSize", new Vector4(1f / (float)sourceWidth, 1f / (float)sourceHeight, (float)sourceWidth, (float)sourceHeight));
		this.PerformBlit(cb, mat, pass);
	}

	// Token: 0x060024F6 RID: 9462 RVA: 0x000CB5A0 File Offset: 0x000C97A0
	private void PerformBlit(CommandBuffer cb, RenderTargetIdentifier source, int sourceWidth, int sourceHeight, Material mat, int pass, int x, int y)
	{
		cb.SetGlobalVector("_AO_Target_Position", new Vector2((float)x, (float)y));
		this.PerformBlit(cb, source, sourceWidth, sourceHeight, mat, pass);
	}

	// Token: 0x060024F7 RID: 9463 RVA: 0x000CB5CC File Offset: 0x000C97CC
	private CommandBuffer CommandBuffer_Allocate(string name)
	{
		return new CommandBuffer
		{
			name = name
		};
	}

	// Token: 0x060024F8 RID: 9464 RVA: 0x000CB5E8 File Offset: 0x000C97E8
	private void CommandBuffer_Register(CameraEvent cameraEvent, CommandBuffer commandBuffer)
	{
		this.m_camera.AddCommandBuffer(cameraEvent, commandBuffer);
		this.m_registeredCommandBuffers.Add(cameraEvent, commandBuffer);
	}

	// Token: 0x060024F9 RID: 9465 RVA: 0x000CB604 File Offset: 0x000C9804
	private void CommandBuffer_Unregister(CameraEvent cameraEvent, CommandBuffer commandBuffer)
	{
		if (this.m_camera != null)
		{
			CommandBuffer[] commandBuffers = this.m_camera.GetCommandBuffers(cameraEvent);
			foreach (CommandBuffer commandBuffer2 in commandBuffers)
			{
				if (commandBuffer2.name == commandBuffer.name)
				{
					this.m_camera.RemoveCommandBuffer(cameraEvent, commandBuffer2);
				}
			}
		}
	}

	// Token: 0x060024FA RID: 9466 RVA: 0x000CB66C File Offset: 0x000C986C
	private CommandBuffer CommandBuffer_AllocateRegister(CameraEvent cameraEvent)
	{
		string name = string.Empty;
		if (cameraEvent == 6)
		{
			name = "AO-BeforeLighting";
		}
		else if (cameraEvent == 7)
		{
			name = "AO-AfterLighting";
		}
		else if (cameraEvent == 12)
		{
			name = "AO-BeforePostOpaque";
		}
		else
		{
			Debug.LogError("[AmplifyOcclusion] Unsupported CameraEvent. Please contact support.");
		}
		CommandBuffer commandBuffer = this.CommandBuffer_Allocate(name);
		this.CommandBuffer_Register(cameraEvent, commandBuffer);
		return commandBuffer;
	}

	// Token: 0x060024FB RID: 9467 RVA: 0x000CB6D4 File Offset: 0x000C98D4
	private void CommandBuffer_UnregisterAll()
	{
		foreach (KeyValuePair<CameraEvent, CommandBuffer> keyValuePair in this.m_registeredCommandBuffers)
		{
			this.CommandBuffer_Unregister(keyValuePair.Key, keyValuePair.Value);
		}
		this.m_registeredCommandBuffers.Clear();
	}

	// Token: 0x060024FC RID: 9468 RVA: 0x000CB748 File Offset: 0x000C9948
	private void UpdateGlobalShaderConstants(global::AmplifyOcclusionBase.TargetDesc target)
	{
		float num = this.m_camera.fieldOfView * 0.0174532924f;
		Vector2 vector;
		vector..ctor(1f / Mathf.Tan(num * 0.5f) * ((float)target.height / (float)target.width), 1f / Mathf.Tan(num * 0.5f));
		Vector2 vector2;
		vector2..ctor(1f / vector.x, 1f / vector.y);
		float num2;
		if (this.m_camera.orthographic)
		{
			num2 = (float)target.height / this.m_camera.orthographicSize;
		}
		else
		{
			num2 = (float)target.height / (Mathf.Tan(num * 0.5f) * 2f);
		}
		float num3 = Mathf.Clamp(this.Bias, 0f, 1f);
		this.FadeStart = Mathf.Max(0f, this.FadeStart);
		this.FadeLength = Mathf.Max(0.01f, this.FadeLength);
		float num4 = (!this.FadeEnabled) ? 0f : (1f / this.FadeLength);
		Shader.SetGlobalMatrix("_AO_CameraProj", GL.GetGPUProjectionMatrix(Matrix4x4.Ortho(0f, 1f, 0f, 1f, -1f, 100f), false));
		Shader.SetGlobalMatrix("_AO_CameraView", this.m_camera.worldToCameraMatrix);
		Shader.SetGlobalVector("_AO_UVToView", new Vector4(2f * vector2.x, -2f * vector2.y, -1f * vector2.x, 1f * vector2.y));
		Shader.SetGlobalFloat("_AO_HalfProjScale", 0.5f * num2);
		Shader.SetGlobalFloat("_AO_Radius", this.Radius);
		Shader.SetGlobalFloat("_AO_PowExponent", this.PowerExponent);
		Shader.SetGlobalFloat("_AO_Bias", num3);
		Shader.SetGlobalFloat("_AO_Multiplier", 1f / (1f - num3));
		Shader.SetGlobalFloat("_AO_BlurSharpness", this.BlurSharpness);
		Shader.SetGlobalColor("_AO_Levels", new Color(this.Tint.r, this.Tint.g, this.Tint.b, this.Intensity));
		Shader.SetGlobalVector("_AO_FadeParams", new Vector2(this.FadeStart, num4));
		Shader.SetGlobalVector("_AO_FadeValues", new Vector3(this.FadeToIntensity, this.FadeToRadius, this.FadeToPowerExponent));
	}

	// Token: 0x060024FD RID: 9469 RVA: 0x000CB9DC File Offset: 0x000C9BDC
	private void CommandBuffer_FillComputeOcclusion(CommandBuffer cb, global::AmplifyOcclusionBase.TargetDesc target)
	{
		this.CheckMaterial();
		this.CheckRandomData();
		cb.SetGlobalVector("_AO_Buffer_PadScale", new Vector4(target.padRatioWidth, target.padRatioHeight, 1f / target.padRatioWidth, 1f / target.padRatioHeight));
		cb.SetGlobalVector("_AO_Buffer_TexelSize", new Vector4(1f / (float)target.width, 1f / (float)target.height, (float)target.width, (float)target.height));
		cb.SetGlobalVector("_AO_QuarterBuffer_TexelSize", new Vector4(1f / (float)target.quarterWidth, 1f / (float)target.quarterHeight, (float)target.quarterWidth, (float)target.quarterHeight));
		cb.SetGlobalFloat("_AO_MaxRadiusPixels", (float)Mathf.Min(target.width, target.height));
		if (this.m_occlusionRT == null || this.m_occlusionRT.width != target.width || this.m_occlusionRT.height != target.height || !this.m_occlusionRT.IsCreated())
		{
			this.SafeReleaseRT(ref this.m_occlusionRT);
			this.m_occlusionRT = this.SafeAllocateRT("_AO_OcclusionTexture", target.width, target.height, this.m_occlusionRTFormat, 1);
		}
		int num = -1;
		if (this.Downsample)
		{
			num = this.SafeAllocateTemporaryRT(cb, "_AO_SmallOcclusionTexture", target.width / 2, target.height / 2, this.m_occlusionRTFormat, 1, 1);
		}
		if (this.CacheAware && !this.Downsample)
		{
			int num2 = this.SafeAllocateTemporaryRT(cb, "_AO_OcclusionAtlas", target.width, target.height, this.m_occlusionRTFormat, 1, 0);
			for (int i = 0; i < 16; i++)
			{
				this.m_depthLayerRT[i] = this.SafeAllocateTemporaryRT(cb, this.m_layerDepthNames[i], target.quarterWidth, target.quarterHeight, this.m_depthRTFormat, 1, 0);
				this.m_normalLayerRT[i] = this.SafeAllocateTemporaryRT(cb, this.m_layerNormalNames[i], target.quarterWidth, target.quarterHeight, this.m_normalRTFormat, 1, 0);
				this.m_occlusionLayerRT[i] = this.SafeAllocateTemporaryRT(cb, this.m_layerOcclusionNames[i], target.quarterWidth, target.quarterHeight, this.m_occlusionRTFormat, 1, 0);
			}
			for (int j = 0; j < 16; j += this.m_mrtCount)
			{
				for (int k = 0; k < this.m_mrtCount; k++)
				{
					int num3 = k + j;
					int num4 = num3 & 3;
					int num5 = num3 >> 2;
					cb.SetGlobalVector(this.m_layerOffsetNames[k], new Vector2((float)num4 + 0.5f, (float)num5 + 0.5f));
					this.m_depthTargets[k] = this.m_depthLayerRT[num3];
					this.m_normalTargets[k] = this.m_normalLayerRT[num3];
				}
				this.SetBlitTarget(cb, this.m_depthTargets, target.quarterWidth, target.quarterHeight);
				this.PerformBlit(cb, this.m_occlusionMat, this.m_deinterleaveDepthPass);
				this.SetBlitTarget(cb, this.m_normalTargets, target.quarterWidth, target.quarterHeight);
				this.PerformBlit(cb, this.m_occlusionMat, (int)(this.m_deinterleaveNormalPass + this.PerPixelNormals));
			}
			for (int l = 0; l < 16; l++)
			{
				cb.SetGlobalVector("_AO_LayerOffset", new Vector2((float)(l & 3) + 0.5f, (float)(l >> 2) + 0.5f));
				cb.SetGlobalVector("_AO_LayerRandom", this.m_randomData[l]);
				cb.SetGlobalTexture("_AO_NormalTexture", this.m_normalLayerRT[l]);
				cb.SetGlobalTexture("_AO_DepthTexture", this.m_depthLayerRT[l]);
				this.SetBlitTarget(cb, this.m_occlusionLayerRT[l], target.quarterWidth, target.quarterHeight);
				this.PerformBlit(cb, this.m_occlusionMat, (int)(15 + this.SampleCount));
			}
			this.SetBlitTarget(cb, num2, target.width, target.height);
			for (int m = 0; m < 16; m++)
			{
				int x = (m & 3) * target.quarterWidth;
				int y = (m >> 2) * target.quarterHeight;
				this.PerformBlit(cb, this.m_occlusionLayerRT[m], target.quarterWidth, target.quarterHeight, this.m_copyMat, 0, x, y);
			}
			cb.SetGlobalTexture("_AO_OcclusionAtlas", num2);
			this.SetBlitTarget(cb, this.m_occlusionRT, target.width, target.height);
			this.PerformBlit(cb, this.m_occlusionMat, 19);
			for (int n = 0; n < 16; n++)
			{
				this.SafeReleaseTemporaryRT(cb, this.m_occlusionLayerRT[n]);
				this.SafeReleaseTemporaryRT(cb, this.m_normalLayerRT[n]);
				this.SafeReleaseTemporaryRT(cb, this.m_depthLayerRT[n]);
			}
			this.SafeReleaseTemporaryRT(cb, num2);
		}
		else
		{
			this.m_occlusionMat.SetTexture("_AO_RandomTexture", this.m_randomTex);
			int num6 = (int)(20 + this.SampleCount * (global::AmplifyOcclusionBase.SampleCountLevel)4 + (int)this.PerPixelNormals);
			if (this.Downsample)
			{
				cb.Blit(null, new RenderTargetIdentifier(num), this.m_occlusionMat, num6);
				this.SetBlitTarget(cb, this.m_occlusionRT, target.width, target.height);
				this.PerformBlit(cb, num, target.width / 2, target.height / 2, this.m_occlusionMat, 41);
			}
			else
			{
				cb.Blit(null, this.m_occlusionRT, this.m_occlusionMat, num6);
			}
		}
		if (this.BlurEnabled)
		{
			int num7 = this.SafeAllocateTemporaryRT(cb, "_AO_TEMP", target.width, target.height, this.m_occlusionRTFormat, 1, 0);
			for (int num8 = 0; num8 < this.BlurPasses; num8++)
			{
				this.SetBlitTarget(cb, num7, target.width, target.height);
				this.PerformBlit(cb, this.m_occlusionRT, target.width, target.height, this.m_blurMat, (this.BlurRadius - 1) * 2);
				this.SetBlitTarget(cb, this.m_occlusionRT, target.width, target.height);
				this.PerformBlit(cb, num7, target.width, target.height, this.m_blurMat, 1 + (this.BlurRadius - 1) * 2);
			}
			this.SafeReleaseTemporaryRT(cb, num7);
		}
		if (this.Downsample && num >= 0)
		{
			this.SafeReleaseTemporaryRT(cb, num);
		}
		cb.SetRenderTarget(null);
	}

	// Token: 0x060024FE RID: 9470 RVA: 0x000CC104 File Offset: 0x000CA304
	private void CommandBuffer_FillApplyDeferred(CommandBuffer cb, global::AmplifyOcclusionBase.TargetDesc target, bool logTarget)
	{
		cb.SetGlobalTexture("_AO_OcclusionTexture", this.m_occlusionRT);
		this.m_applyDeferredTargets[0] = 10;
		this.m_applyDeferredTargets[1] = ((!logTarget) ? 2 : 13);
		if (!logTarget)
		{
			this.SetBlitTarget(cb, this.m_applyDeferredTargets, target.fullWidth, target.fullHeight);
			this.PerformBlit(cb, this.m_occlusionMat, 37);
		}
		else
		{
			int num = this.SafeAllocateTemporaryRT(cb, "_AO_GBufferAlbedo", target.fullWidth, target.fullHeight, 0, 0, 0);
			int num2 = this.SafeAllocateTemporaryRT(cb, "_AO_GBufferEmission", target.fullWidth, target.fullHeight, 0, 0, 0);
			cb.Blit(this.m_applyDeferredTargets[0], num);
			cb.Blit(this.m_applyDeferredTargets[1], num2);
			cb.SetGlobalTexture("_AO_GBufferAlbedo", num);
			cb.SetGlobalTexture("_AO_GBufferEmission", num2);
			this.SetBlitTarget(cb, this.m_applyDeferredTargets, target.fullWidth, target.fullHeight);
			this.PerformBlit(cb, this.m_occlusionMat, 38);
			this.SafeReleaseTemporaryRT(cb, num);
			this.SafeReleaseTemporaryRT(cb, num2);
		}
		cb.SetRenderTarget(null);
	}

	// Token: 0x060024FF RID: 9471 RVA: 0x000CC278 File Offset: 0x000CA478
	private void CommandBuffer_FillApplyPostEffect(CommandBuffer cb, global::AmplifyOcclusionBase.TargetDesc target, bool logTarget)
	{
		cb.SetGlobalTexture("_AO_OcclusionTexture", this.m_occlusionRT);
		if (!logTarget)
		{
			this.SetBlitTarget(cb, 2, target.fullWidth, target.fullHeight);
			this.PerformBlit(cb, this.m_occlusionMat, 39);
		}
		else
		{
			int num = this.SafeAllocateTemporaryRT(cb, "_AO_GBufferEmission", target.fullWidth, target.fullHeight, 0, 0, 0);
			cb.Blit(13, num);
			cb.SetGlobalTexture("_AO_GBufferEmission", num);
			this.SetBlitTarget(cb, 13, target.fullWidth, target.fullHeight);
			this.PerformBlit(cb, this.m_occlusionMat, 40);
			this.SafeReleaseTemporaryRT(cb, num);
		}
		cb.SetRenderTarget(null);
	}

	// Token: 0x06002500 RID: 9472 RVA: 0x000CC354 File Offset: 0x000CA554
	private void CommandBuffer_FillApplyDebug(CommandBuffer cb, global::AmplifyOcclusionBase.TargetDesc target)
	{
		cb.SetGlobalTexture("_AO_OcclusionTexture", this.m_occlusionRT);
		this.SetBlitTarget(cb, 2, target.fullWidth, target.fullHeight);
		this.PerformBlit(cb, this.m_occlusionMat, 36);
		cb.SetRenderTarget(null);
	}

	// Token: 0x06002501 RID: 9473 RVA: 0x000CC3B0 File Offset: 0x000CA5B0
	private void CommandBuffer_Rebuild(global::AmplifyOcclusionBase.TargetDesc target)
	{
		bool flag = this.PerPixelNormals == global::AmplifyOcclusionBase.PerPixelNormalSource.GBuffer || this.PerPixelNormals == global::AmplifyOcclusionBase.PerPixelNormalSource.GBufferOctaEncoded;
		CameraEvent cameraEvent = (!flag) ? 12 : 7;
		if (this.ApplyMethod == global::AmplifyOcclusionBase.ApplicationMethod.Debug)
		{
			CommandBuffer cb = this.CommandBuffer_AllocateRegister(cameraEvent);
			this.CommandBuffer_FillComputeOcclusion(cb, target);
			this.CommandBuffer_FillApplyDebug(cb, target);
		}
		else
		{
			bool logTarget = !this.m_camera.allowHDR && flag;
			cameraEvent = ((this.ApplyMethod != global::AmplifyOcclusionBase.ApplicationMethod.Deferred) ? cameraEvent : 6);
			CommandBuffer cb = this.CommandBuffer_AllocateRegister(cameraEvent);
			this.CommandBuffer_FillComputeOcclusion(cb, target);
			if (this.ApplyMethod == global::AmplifyOcclusionBase.ApplicationMethod.PostEffect)
			{
				this.CommandBuffer_FillApplyPostEffect(cb, target, logTarget);
			}
			else if (this.ApplyMethod == global::AmplifyOcclusionBase.ApplicationMethod.Deferred)
			{
				this.CommandBuffer_FillApplyDeferred(cb, target, logTarget);
			}
		}
	}

	// Token: 0x06002502 RID: 9474 RVA: 0x000CC47C File Offset: 0x000CA67C
	private void OnPreRender()
	{
		bool allowHDR = this.m_camera.allowHDR;
		this.m_target.fullWidth = this.m_camera.pixelWidth;
		this.m_target.fullHeight = this.m_camera.pixelHeight;
		this.m_target.width = ((!this.CacheAware) ? this.m_target.fullWidth : (this.m_target.fullWidth + 3 & -4));
		this.m_target.height = ((!this.CacheAware) ? this.m_target.fullHeight : (this.m_target.fullHeight + 3 & -4));
		this.m_target.quarterWidth = this.m_target.width / 4;
		this.m_target.quarterHeight = this.m_target.height / 4;
		this.m_target.padRatioWidth = (float)this.m_target.width / (float)this.m_target.fullWidth;
		this.m_target.padRatioHeight = (float)this.m_target.height / (float)this.m_target.fullHeight;
		this.UpdateGlobalShaderConstants(this.m_target);
		if (this.CheckParamsChanged() || this.m_registeredCommandBuffers.Count == 0)
		{
			this.CommandBuffer_UnregisterAll();
			this.CommandBuffer_Rebuild(this.m_target);
			this.UpdateParams();
		}
	}

	// Token: 0x06002503 RID: 9475 RVA: 0x000CC5E4 File Offset: 0x000CA7E4
	private void OnPostRender()
	{
		if (this.m_occlusionRT != null)
		{
			this.m_occlusionRT.MarkRestoreExpected();
		}
	}

	// Token: 0x04002062 RID: 8290
	[Header("Ambient Occlusion")]
	public global::AmplifyOcclusionBase.ApplicationMethod ApplyMethod;

	// Token: 0x04002063 RID: 8291
	public global::AmplifyOcclusionBase.SampleCountLevel SampleCount = global::AmplifyOcclusionBase.SampleCountLevel.Medium;

	// Token: 0x04002064 RID: 8292
	public global::AmplifyOcclusionBase.PerPixelNormalSource PerPixelNormals;

	// Token: 0x04002065 RID: 8293
	[Range(0f, 1f)]
	public float Intensity = 1f;

	// Token: 0x04002066 RID: 8294
	public Color Tint = Color.black;

	// Token: 0x04002067 RID: 8295
	[Range(0f, 16f)]
	public float Radius = 1f;

	// Token: 0x04002068 RID: 8296
	[Range(0f, 16f)]
	public float PowerExponent = 1.8f;

	// Token: 0x04002069 RID: 8297
	[Range(0f, 0.99f)]
	public float Bias = 0.05f;

	// Token: 0x0400206A RID: 8298
	public bool CacheAware;

	// Token: 0x0400206B RID: 8299
	public bool Downsample;

	// Token: 0x0400206C RID: 8300
	[Header("Distance Fade")]
	public bool FadeEnabled;

	// Token: 0x0400206D RID: 8301
	public float FadeStart = 100f;

	// Token: 0x0400206E RID: 8302
	public float FadeLength = 50f;

	// Token: 0x0400206F RID: 8303
	[Range(0f, 1f)]
	public float FadeToIntensity = 1f;

	// Token: 0x04002070 RID: 8304
	[Range(0f, 16f)]
	public float FadeToRadius = 1f;

	// Token: 0x04002071 RID: 8305
	[Range(0f, 16f)]
	public float FadeToPowerExponent = 1.8f;

	// Token: 0x04002072 RID: 8306
	[Header("Bilateral Blur")]
	public bool BlurEnabled = true;

	// Token: 0x04002073 RID: 8307
	[Range(1f, 4f)]
	public int BlurRadius = 2;

	// Token: 0x04002074 RID: 8308
	[Range(1f, 4f)]
	public int BlurPasses = 1;

	// Token: 0x04002075 RID: 8309
	[Range(0f, 20f)]
	public float BlurSharpness = 10f;

	// Token: 0x04002076 RID: 8310
	private const int PerPixelNormalSourceCount = 4;

	// Token: 0x04002077 RID: 8311
	private int prevScreenWidth;

	// Token: 0x04002078 RID: 8312
	private int prevScreenHeight;

	// Token: 0x04002079 RID: 8313
	private bool prevHDR;

	// Token: 0x0400207A RID: 8314
	private global::AmplifyOcclusionBase.ApplicationMethod prevApplyMethod;

	// Token: 0x0400207B RID: 8315
	private global::AmplifyOcclusionBase.SampleCountLevel prevSampleCount;

	// Token: 0x0400207C RID: 8316
	private global::AmplifyOcclusionBase.PerPixelNormalSource prevPerPixelNormals;

	// Token: 0x0400207D RID: 8317
	private bool prevCacheAware;

	// Token: 0x0400207E RID: 8318
	private bool prevDownscale;

	// Token: 0x0400207F RID: 8319
	private bool prevFadeEnabled;

	// Token: 0x04002080 RID: 8320
	private float prevFadeToIntensity;

	// Token: 0x04002081 RID: 8321
	private float prevFadeToRadius;

	// Token: 0x04002082 RID: 8322
	private float prevFadeToPowerExponent;

	// Token: 0x04002083 RID: 8323
	private float prevFadeStart;

	// Token: 0x04002084 RID: 8324
	private float prevFadeLength;

	// Token: 0x04002085 RID: 8325
	private bool prevBlurEnabled;

	// Token: 0x04002086 RID: 8326
	private int prevBlurRadius;

	// Token: 0x04002087 RID: 8327
	private int prevBlurPasses;

	// Token: 0x04002088 RID: 8328
	private Camera m_camera;

	// Token: 0x04002089 RID: 8329
	private Material m_occlusionMat;

	// Token: 0x0400208A RID: 8330
	private Material m_blurMat;

	// Token: 0x0400208B RID: 8331
	private Material m_copyMat;

	// Token: 0x0400208C RID: 8332
	private Texture2D m_randomTex;

	// Token: 0x0400208D RID: 8333
	private const int RandomSize = 4;

	// Token: 0x0400208E RID: 8334
	private const int DirectionCount = 8;

	// Token: 0x0400208F RID: 8335
	private Color[] m_randomData;

	// Token: 0x04002090 RID: 8336
	private string[] m_layerOffsetNames;

	// Token: 0x04002091 RID: 8337
	private string[] m_layerRandomNames;

	// Token: 0x04002092 RID: 8338
	private string[] m_layerDepthNames;

	// Token: 0x04002093 RID: 8339
	private string[] m_layerNormalNames;

	// Token: 0x04002094 RID: 8340
	private string[] m_layerOcclusionNames;

	// Token: 0x04002095 RID: 8341
	private RenderTextureFormat m_depthRTFormat = 14;

	// Token: 0x04002096 RID: 8342
	private RenderTextureFormat m_normalRTFormat = 8;

	// Token: 0x04002097 RID: 8343
	private RenderTextureFormat m_occlusionRTFormat = 13;

	// Token: 0x04002098 RID: 8344
	private RenderTexture m_occlusionRT;

	// Token: 0x04002099 RID: 8345
	private int[] m_depthLayerRT;

	// Token: 0x0400209A RID: 8346
	private int[] m_normalLayerRT;

	// Token: 0x0400209B RID: 8347
	private int[] m_occlusionLayerRT;

	// Token: 0x0400209C RID: 8348
	private int m_mrtCount;

	// Token: 0x0400209D RID: 8349
	private RenderTargetIdentifier[] m_depthTargets;

	// Token: 0x0400209E RID: 8350
	private RenderTargetIdentifier[] m_normalTargets;

	// Token: 0x0400209F RID: 8351
	private int m_deinterleaveDepthPass;

	// Token: 0x040020A0 RID: 8352
	private int m_deinterleaveNormalPass;

	// Token: 0x040020A1 RID: 8353
	private RenderTargetIdentifier[] m_applyDeferredTargets;

	// Token: 0x040020A2 RID: 8354
	private Mesh m_blitMesh;

	// Token: 0x040020A3 RID: 8355
	private global::AmplifyOcclusionBase.TargetDesc m_target = default(global::AmplifyOcclusionBase.TargetDesc);

	// Token: 0x040020A4 RID: 8356
	private Dictionary<CameraEvent, CommandBuffer> m_registeredCommandBuffers = new Dictionary<CameraEvent, CommandBuffer>();

	// Token: 0x020007C9 RID: 1993
	public enum ApplicationMethod
	{
		// Token: 0x040020A6 RID: 8358
		PostEffect,
		// Token: 0x040020A7 RID: 8359
		Deferred,
		// Token: 0x040020A8 RID: 8360
		Debug
	}

	// Token: 0x020007CA RID: 1994
	public enum PerPixelNormalSource
	{
		// Token: 0x040020AA RID: 8362
		None,
		// Token: 0x040020AB RID: 8363
		Camera,
		// Token: 0x040020AC RID: 8364
		GBuffer,
		// Token: 0x040020AD RID: 8365
		GBufferOctaEncoded
	}

	// Token: 0x020007CB RID: 1995
	public enum SampleCountLevel
	{
		// Token: 0x040020AF RID: 8367
		Low,
		// Token: 0x040020B0 RID: 8368
		Medium,
		// Token: 0x040020B1 RID: 8369
		High,
		// Token: 0x040020B2 RID: 8370
		VeryHigh
	}

	// Token: 0x020007CC RID: 1996
	private static class ShaderPass
	{
		// Token: 0x040020B3 RID: 8371
		public const int FullDepth = 0;

		// Token: 0x040020B4 RID: 8372
		public const int FullNormal_None = 1;

		// Token: 0x040020B5 RID: 8373
		public const int FullNormal_Camera = 2;

		// Token: 0x040020B6 RID: 8374
		public const int FullNormal_GBuffer = 3;

		// Token: 0x040020B7 RID: 8375
		public const int FullNormal_GBufferOctaEncoded = 4;

		// Token: 0x040020B8 RID: 8376
		public const int DeinterleaveDepth1 = 5;

		// Token: 0x040020B9 RID: 8377
		public const int DeinterleaveNormal1_None = 6;

		// Token: 0x040020BA RID: 8378
		public const int DeinterleaveNormal1_Camera = 7;

		// Token: 0x040020BB RID: 8379
		public const int DeinterleaveNormal1_GBuffer = 8;

		// Token: 0x040020BC RID: 8380
		public const int DeinterleaveNormal1_GBufferOctaEncoded = 9;

		// Token: 0x040020BD RID: 8381
		public const int DeinterleaveDepth4 = 10;

		// Token: 0x040020BE RID: 8382
		public const int DeinterleaveNormal4_None = 11;

		// Token: 0x040020BF RID: 8383
		public const int DeinterleaveNormal4_Camera = 12;

		// Token: 0x040020C0 RID: 8384
		public const int DeinterleaveNormal4_GBuffer = 13;

		// Token: 0x040020C1 RID: 8385
		public const int DeinterleaveNormal4_GBufferOctaEncoded = 14;

		// Token: 0x040020C2 RID: 8386
		public const int OcclusionCache_Low = 15;

		// Token: 0x040020C3 RID: 8387
		public const int OcclusionCache_Medium = 16;

		// Token: 0x040020C4 RID: 8388
		public const int OcclusionCache_High = 17;

		// Token: 0x040020C5 RID: 8389
		public const int OcclusionCache_VeryHigh = 18;

		// Token: 0x040020C6 RID: 8390
		public const int Reinterleave = 19;

		// Token: 0x040020C7 RID: 8391
		public const int OcclusionLow_None = 20;

		// Token: 0x040020C8 RID: 8392
		public const int OcclusionLow_Camera = 21;

		// Token: 0x040020C9 RID: 8393
		public const int OcclusionLow_GBuffer = 22;

		// Token: 0x040020CA RID: 8394
		public const int OcclusionLow_GBufferOctaEncoded = 23;

		// Token: 0x040020CB RID: 8395
		public const int OcclusionMedium_None = 24;

		// Token: 0x040020CC RID: 8396
		public const int OcclusionMedium_Camera = 25;

		// Token: 0x040020CD RID: 8397
		public const int OcclusionMedium_GBuffer = 26;

		// Token: 0x040020CE RID: 8398
		public const int OcclusionMedium_GBufferOctaEncoded = 27;

		// Token: 0x040020CF RID: 8399
		public const int OcclusionHigh_None = 28;

		// Token: 0x040020D0 RID: 8400
		public const int OcclusionHigh_Camera = 29;

		// Token: 0x040020D1 RID: 8401
		public const int OcclusionHigh_GBuffer = 30;

		// Token: 0x040020D2 RID: 8402
		public const int OcclusionHigh_GBufferOctaEncoded = 31;

		// Token: 0x040020D3 RID: 8403
		public const int OcclusionVeryHigh_None = 32;

		// Token: 0x040020D4 RID: 8404
		public const int OcclusionVeryHigh_Camera = 33;

		// Token: 0x040020D5 RID: 8405
		public const int OcclusionVeryHigh_GBuffer = 34;

		// Token: 0x040020D6 RID: 8406
		public const int OcclusionVeryHigh_GBufferNormalEncoded = 35;

		// Token: 0x040020D7 RID: 8407
		public const int ApplyDebug = 36;

		// Token: 0x040020D8 RID: 8408
		public const int ApplyDeferred = 37;

		// Token: 0x040020D9 RID: 8409
		public const int ApplyDeferredLog = 38;

		// Token: 0x040020DA RID: 8410
		public const int ApplyPostEffect = 39;

		// Token: 0x040020DB RID: 8411
		public const int ApplyPostEffectLog = 40;

		// Token: 0x040020DC RID: 8412
		public const int CombineDownsampledOcclusionDepth = 41;

		// Token: 0x040020DD RID: 8413
		public const int BlurHorizontal1 = 0;

		// Token: 0x040020DE RID: 8414
		public const int BlurVertical1 = 1;

		// Token: 0x040020DF RID: 8415
		public const int BlurHorizontal2 = 2;

		// Token: 0x040020E0 RID: 8416
		public const int BlurVertical2 = 3;

		// Token: 0x040020E1 RID: 8417
		public const int BlurHorizontal3 = 4;

		// Token: 0x040020E2 RID: 8418
		public const int BlurVertical3 = 5;

		// Token: 0x040020E3 RID: 8419
		public const int BlurHorizontal4 = 6;

		// Token: 0x040020E4 RID: 8420
		public const int BlurVertical4 = 7;

		// Token: 0x040020E5 RID: 8421
		public const int Copy = 0;
	}

	// Token: 0x020007CD RID: 1997
	private struct TargetDesc
	{
		// Token: 0x040020E6 RID: 8422
		public int fullWidth;

		// Token: 0x040020E7 RID: 8423
		public int fullHeight;

		// Token: 0x040020E8 RID: 8424
		public int width;

		// Token: 0x040020E9 RID: 8425
		public int height;

		// Token: 0x040020EA RID: 8426
		public int quarterWidth;

		// Token: 0x040020EB RID: 8427
		public int quarterHeight;

		// Token: 0x040020EC RID: 8428
		public float padRatioWidth;

		// Token: 0x040020ED RID: 8429
		public float padRatioHeight;
	}
}
