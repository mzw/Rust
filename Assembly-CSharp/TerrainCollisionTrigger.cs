﻿using System;
using UnityEngine;

// Token: 0x02000563 RID: 1379
public class TerrainCollisionTrigger : global::EnvironmentVolumeTrigger
{
	// Token: 0x06001CC6 RID: 7366 RVA: 0x000A1350 File Offset: 0x0009F550
	protected void OnTriggerEnter(Collider other)
	{
		if (!global::TerrainMeta.Collision || other.isTrigger)
		{
			return;
		}
		this.UpdateCollider(other, true);
	}

	// Token: 0x06001CC7 RID: 7367 RVA: 0x000A1378 File Offset: 0x0009F578
	protected void OnTriggerExit(Collider other)
	{
		if (!global::TerrainMeta.Collision || other.isTrigger)
		{
			return;
		}
		this.UpdateCollider(other, false);
	}

	// Token: 0x06001CC8 RID: 7368 RVA: 0x000A13A0 File Offset: 0x0009F5A0
	private void UpdateCollider(Collider other, bool state)
	{
		global::TerrainMeta.Collision.SetIgnore(other, base.volume.trigger, state);
		global::TerrainCollisionProxy component = other.GetComponent<global::TerrainCollisionProxy>();
		if (component)
		{
			for (int i = 0; i < component.colliders.Length; i++)
			{
				global::TerrainMeta.Collision.SetIgnore(component.colliders[i], base.volume.trigger, state);
			}
		}
	}
}
