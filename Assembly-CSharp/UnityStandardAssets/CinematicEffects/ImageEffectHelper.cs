﻿using System;
using UnityEngine;

namespace UnityStandardAssets.CinematicEffects
{
	// Token: 0x020007D3 RID: 2003
	public static class ImageEffectHelper
	{
		// Token: 0x06002518 RID: 9496 RVA: 0x000CCBA4 File Offset: 0x000CADA4
		public static bool IsSupported(Shader s, bool needDepth, bool needHdr, MonoBehaviour effect)
		{
			if (s == null || !s.isSupported)
			{
				Debug.LogWarningFormat("Missing shader for image effect {0}", new object[]
				{
					effect
				});
				return false;
			}
			if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
			{
				Debug.LogWarningFormat("Image effects aren't supported on this device ({0})", new object[]
				{
					effect
				});
				return false;
			}
			if (needDepth && !SystemInfo.SupportsRenderTextureFormat(1))
			{
				Debug.LogWarningFormat("Depth textures aren't supported on this device ({0})", new object[]
				{
					effect
				});
				return false;
			}
			if (needHdr && !SystemInfo.SupportsRenderTextureFormat(2))
			{
				Debug.LogWarningFormat("Floating point textures aren't supported on this device ({0})", new object[]
				{
					effect
				});
				return false;
			}
			return true;
		}

		// Token: 0x06002519 RID: 9497 RVA: 0x000CCC58 File Offset: 0x000CAE58
		public static Material CheckShaderAndCreateMaterial(Shader s)
		{
			if (s == null || !s.isSupported)
			{
				return null;
			}
			return new Material(s)
			{
				hideFlags = 52
			};
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x0600251A RID: 9498 RVA: 0x000CCC90 File Offset: 0x000CAE90
		public static bool supportsDX11
		{
			get
			{
				return SystemInfo.graphicsShaderLevel >= 50 && SystemInfo.supportsComputeShaders;
			}
		}
	}
}
