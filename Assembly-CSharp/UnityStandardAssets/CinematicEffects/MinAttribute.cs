﻿using System;
using UnityEngine;

namespace UnityStandardAssets.CinematicEffects
{
	// Token: 0x020007D4 RID: 2004
	public sealed class MinAttribute : PropertyAttribute
	{
		// Token: 0x0600251B RID: 9499 RVA: 0x000CCCA8 File Offset: 0x000CAEA8
		public MinAttribute(float min)
		{
			this.min = min;
		}

		// Token: 0x04002117 RID: 8471
		public readonly float min;
	}
}
