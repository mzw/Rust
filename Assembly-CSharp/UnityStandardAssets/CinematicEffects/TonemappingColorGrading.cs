﻿using System;
using UnityEngine;

namespace UnityStandardAssets.CinematicEffects
{
	// Token: 0x020007D6 RID: 2006
	[ExecuteInEditMode]
	[AddComponentMenu("Image Effects/Cinematic/Tonemapping and Color Grading")]
	[ImageEffectAllowedInSceneView]
	public class TonemappingColorGrading : MonoBehaviour
	{
		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06002521 RID: 9505 RVA: 0x000CCDF8 File Offset: 0x000CAFF8
		// (set) Token: 0x06002522 RID: 9506 RVA: 0x000CCE00 File Offset: 0x000CB000
		public TonemappingColorGrading.EyeAdaptationSettings eyeAdaptation
		{
			get
			{
				return this.m_EyeAdaptation;
			}
			set
			{
				this.m_EyeAdaptation = value;
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06002523 RID: 9507 RVA: 0x000CCE0C File Offset: 0x000CB00C
		// (set) Token: 0x06002524 RID: 9508 RVA: 0x000CCE14 File Offset: 0x000CB014
		public TonemappingColorGrading.TonemappingSettings tonemapping
		{
			get
			{
				return this.m_Tonemapping;
			}
			set
			{
				this.m_Tonemapping = value;
				this.SetTonemapperDirty();
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06002525 RID: 9509 RVA: 0x000CCE24 File Offset: 0x000CB024
		// (set) Token: 0x06002526 RID: 9510 RVA: 0x000CCE2C File Offset: 0x000CB02C
		public TonemappingColorGrading.ColorGradingSettings colorGrading
		{
			get
			{
				return this.m_ColorGrading;
			}
			set
			{
				this.m_ColorGrading = value;
				this.SetDirty();
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06002527 RID: 9511 RVA: 0x000CCE3C File Offset: 0x000CB03C
		// (set) Token: 0x06002528 RID: 9512 RVA: 0x000CCE44 File Offset: 0x000CB044
		public TonemappingColorGrading.LUTSettings lut
		{
			get
			{
				return this.m_Lut;
			}
			set
			{
				this.m_Lut = value;
			}
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06002529 RID: 9513 RVA: 0x000CCE50 File Offset: 0x000CB050
		private Texture2D identityLut
		{
			get
			{
				if (this.m_IdentityLut == null || this.m_IdentityLut.height != this.lutSize)
				{
					Object.DestroyImmediate(this.m_IdentityLut);
					this.m_IdentityLut = TonemappingColorGrading.GenerateIdentityLut(this.lutSize);
				}
				return this.m_IdentityLut;
			}
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x0600252A RID: 9514 RVA: 0x000CCEA8 File Offset: 0x000CB0A8
		private RenderTexture internalLutRt
		{
			get
			{
				if (this.m_InternalLut == null || !this.m_InternalLut.IsCreated() || this.m_InternalLut.height != this.lutSize)
				{
					Object.DestroyImmediate(this.m_InternalLut);
					this.m_InternalLut = new RenderTexture(this.lutSize * this.lutSize, this.lutSize, 0, 0)
					{
						name = "Internal LUT",
						filterMode = 1,
						anisoLevel = 0,
						hideFlags = 52
					};
				}
				return this.m_InternalLut;
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x0600252B RID: 9515 RVA: 0x000CCF40 File Offset: 0x000CB140
		private Texture2D curveTexture
		{
			get
			{
				if (this.m_CurveTexture == null)
				{
					this.m_CurveTexture = new Texture2D(256, 1, 5, false, true)
					{
						name = "Curve texture",
						wrapMode = 1,
						filterMode = 1,
						anisoLevel = 0,
						hideFlags = 52
					};
				}
				return this.m_CurveTexture;
			}
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x0600252C RID: 9516 RVA: 0x000CCFA4 File Offset: 0x000CB1A4
		private Texture2D tonemapperCurve
		{
			get
			{
				if (this.m_TonemapperCurve == null)
				{
					TextureFormat textureFormat = 3;
					if (SystemInfo.SupportsTextureFormat(18))
					{
						textureFormat = 18;
					}
					else if (SystemInfo.SupportsTextureFormat(15))
					{
						textureFormat = 15;
					}
					this.m_TonemapperCurve = new Texture2D(256, 1, textureFormat, false, true)
					{
						name = "Tonemapper curve texture",
						wrapMode = 1,
						filterMode = 1,
						anisoLevel = 0,
						hideFlags = 52
					};
				}
				return this.m_TonemapperCurve;
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x0600252D RID: 9517 RVA: 0x000CD02C File Offset: 0x000CB22C
		public Shader shader
		{
			get
			{
				if (this.m_Shader == null)
				{
					this.m_Shader = Shader.Find("Hidden/TonemappingColorGrading");
				}
				return this.m_Shader;
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x0600252E RID: 9518 RVA: 0x000CD058 File Offset: 0x000CB258
		public Material material
		{
			get
			{
				if (this.m_Material == null)
				{
					this.m_Material = ImageEffectHelper.CheckShaderAndCreateMaterial(this.shader);
				}
				return this.m_Material;
			}
		}

		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x0600252F RID: 9519 RVA: 0x000CD084 File Offset: 0x000CB284
		public bool isGammaColorSpace
		{
			get
			{
				return QualitySettings.activeColorSpace == 0;
			}
		}

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06002530 RID: 9520 RVA: 0x000CD090 File Offset: 0x000CB290
		public int lutSize
		{
			get
			{
				return (int)this.colorGrading.precision;
			}
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06002531 RID: 9521 RVA: 0x000CD0AC File Offset: 0x000CB2AC
		// (set) Token: 0x06002532 RID: 9522 RVA: 0x000CD0B4 File Offset: 0x000CB2B4
		public bool validRenderTextureFormat { get; private set; }

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06002533 RID: 9523 RVA: 0x000CD0C0 File Offset: 0x000CB2C0
		// (set) Token: 0x06002534 RID: 9524 RVA: 0x000CD0C8 File Offset: 0x000CB2C8
		public bool validUserLutSize { get; private set; }

		// Token: 0x06002535 RID: 9525 RVA: 0x000CD0D4 File Offset: 0x000CB2D4
		public void SetDirty()
		{
			this.m_Dirty = true;
		}

		// Token: 0x06002536 RID: 9526 RVA: 0x000CD0E0 File Offset: 0x000CB2E0
		public void SetTonemapperDirty()
		{
			this.m_TonemapperDirty = true;
		}

		// Token: 0x06002537 RID: 9527 RVA: 0x000CD0EC File Offset: 0x000CB2EC
		private void OnEnable()
		{
			if (!ImageEffectHelper.IsSupported(this.shader, false, true, this))
			{
				base.enabled = false;
				return;
			}
			this.SetDirty();
			this.SetTonemapperDirty();
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x000CD118 File Offset: 0x000CB318
		private void OnDisable()
		{
			if (this.m_Material != null)
			{
				Object.DestroyImmediate(this.m_Material);
			}
			if (this.m_IdentityLut != null)
			{
				Object.DestroyImmediate(this.m_IdentityLut);
			}
			if (this.m_InternalLut != null)
			{
				Object.DestroyImmediate(this.internalLutRt);
			}
			if (this.m_SmallAdaptiveRt != null)
			{
				Object.DestroyImmediate(this.m_SmallAdaptiveRt);
			}
			if (this.m_CurveTexture != null)
			{
				Object.DestroyImmediate(this.m_CurveTexture);
			}
			if (this.m_TonemapperCurve != null)
			{
				Object.DestroyImmediate(this.m_TonemapperCurve);
			}
			this.m_Material = null;
			this.m_IdentityLut = null;
			this.m_InternalLut = null;
			this.m_SmallAdaptiveRt = null;
			this.m_CurveTexture = null;
			this.m_TonemapperCurve = null;
		}

		// Token: 0x06002539 RID: 9529 RVA: 0x000CD1F8 File Offset: 0x000CB3F8
		private void OnValidate()
		{
			this.SetDirty();
			this.SetTonemapperDirty();
		}

		// Token: 0x0600253A RID: 9530 RVA: 0x000CD208 File Offset: 0x000CB408
		private static Texture2D GenerateIdentityLut(int dim)
		{
			Color[] array = new Color[dim * dim * dim];
			float num = 1f / ((float)dim - 1f);
			for (int i = 0; i < dim; i++)
			{
				for (int j = 0; j < dim; j++)
				{
					for (int k = 0; k < dim; k++)
					{
						array[i + j * dim + k * dim * dim] = new Color((float)i * num, Mathf.Abs((float)k * num), (float)j * num, 1f);
					}
				}
			}
			Texture2D texture2D = new Texture2D(dim * dim, dim, 3, false, true)
			{
				name = "Identity LUT",
				filterMode = 1,
				anisoLevel = 0,
				hideFlags = 52
			};
			texture2D.SetPixels(array);
			texture2D.Apply();
			return texture2D;
		}

		// Token: 0x0600253B RID: 9531 RVA: 0x000CD2E4 File Offset: 0x000CB4E4
		private float StandardIlluminantY(float x)
		{
			return 2.87f * x - 3f * x * x - 0.275095075f;
		}

		// Token: 0x0600253C RID: 9532 RVA: 0x000CD300 File Offset: 0x000CB500
		private Vector3 CIExyToLMS(float x, float y)
		{
			float num = 1f;
			float num2 = num * x / y;
			float num3 = num * (1f - x - y) / y;
			float num4 = 0.7328f * num2 + 0.4296f * num - 0.1624f * num3;
			float num5 = -0.7036f * num2 + 1.6975f * num + 0.0061f * num3;
			float num6 = 0.003f * num2 + 0.0136f * num + 0.9834f * num3;
			return new Vector3(num4, num5, num6);
		}

		// Token: 0x0600253D RID: 9533 RVA: 0x000CD37C File Offset: 0x000CB57C
		private Vector3 GetWhiteBalance()
		{
			float temperatureShift = this.colorGrading.basics.temperatureShift;
			float tint = this.colorGrading.basics.tint;
			float x = 0.31271f - temperatureShift * ((temperatureShift >= 0f) ? 0.05f : 0.1f);
			float y = this.StandardIlluminantY(x) + tint * 0.05f;
			Vector3 vector;
			vector..ctor(0.949237f, 1.03542f, 1.08728f);
			Vector3 vector2 = this.CIExyToLMS(x, y);
			return new Vector3(vector.x / vector2.x, vector.y / vector2.y, vector.z / vector2.z);
		}

		// Token: 0x0600253E RID: 9534 RVA: 0x000CD43C File Offset: 0x000CB63C
		private static Color NormalizeColor(Color c)
		{
			float num = (c.r + c.g + c.b) / 3f;
			if (Mathf.Approximately(num, 0f))
			{
				return new Color(1f, 1f, 1f, 1f);
			}
			Color result = default(Color);
			result.r = c.r / num;
			result.g = c.g / num;
			result.b = c.b / num;
			result.a = 1f;
			return result;
		}

		// Token: 0x0600253F RID: 9535 RVA: 0x000CD4D8 File Offset: 0x000CB6D8
		private void GenerateLiftGammaGain(out Color lift, out Color gamma, out Color gain)
		{
			Color color = TonemappingColorGrading.NormalizeColor(this.colorGrading.colorWheels.shadows);
			Color color2 = TonemappingColorGrading.NormalizeColor(this.colorGrading.colorWheels.midtones);
			Color color3 = TonemappingColorGrading.NormalizeColor(this.colorGrading.colorWheels.highlights);
			float num = (color.r + color.g + color.b) / 3f;
			float num2 = (color2.r + color2.g + color2.b) / 3f;
			float num3 = (color3.r + color3.g + color3.b) / 3f;
			float num4 = (color.r - num) * 0.1f;
			float num5 = (color.g - num) * 0.1f;
			float num6 = (color.b - num) * 0.1f;
			float num7 = Mathf.Pow(2f, (color2.r - num2) * 0.5f);
			float num8 = Mathf.Pow(2f, (color2.g - num2) * 0.5f);
			float num9 = Mathf.Pow(2f, (color2.b - num2) * 0.5f);
			float num10 = Mathf.Pow(2f, (color3.r - num3) * 0.5f);
			float num11 = Mathf.Pow(2f, (color3.g - num3) * 0.5f);
			float num12 = Mathf.Pow(2f, (color3.b - num3) * 0.5f);
			float num13 = 1f / Mathf.Max(0.01f, num7);
			float num14 = 1f / Mathf.Max(0.01f, num8);
			float num15 = 1f / Mathf.Max(0.01f, num9);
			lift..ctor(num4, num5, num6);
			gamma..ctor(num13, num14, num15);
			gain..ctor(num10, num11, num12);
		}

		// Token: 0x06002540 RID: 9536 RVA: 0x000CD6D0 File Offset: 0x000CB8D0
		private void GenCurveTexture()
		{
			AnimationCurve master = this.colorGrading.curves.master;
			AnimationCurve red = this.colorGrading.curves.red;
			AnimationCurve green = this.colorGrading.curves.green;
			AnimationCurve blue = this.colorGrading.curves.blue;
			Color[] array = new Color[256];
			for (float num = 0f; num <= 1f; num += 0.003921569f)
			{
				float num2 = Mathf.Clamp(master.Evaluate(num), 0f, 1f);
				float num3 = Mathf.Clamp(red.Evaluate(num), 0f, 1f);
				float num4 = Mathf.Clamp(green.Evaluate(num), 0f, 1f);
				float num5 = Mathf.Clamp(blue.Evaluate(num), 0f, 1f);
				array[(int)Mathf.Floor(num * 255f)] = new Color(num3, num4, num5, num2);
			}
			this.curveTexture.SetPixels(array);
			this.curveTexture.Apply();
		}

		// Token: 0x06002541 RID: 9537 RVA: 0x000CD804 File Offset: 0x000CBA04
		private bool CheckUserLut()
		{
			this.validUserLutSize = (this.lut.texture.height == (int)Mathf.Sqrt((float)this.lut.texture.width));
			return this.validUserLutSize;
		}

		// Token: 0x06002542 RID: 9538 RVA: 0x000CD84C File Offset: 0x000CBA4C
		private bool CheckSmallAdaptiveRt()
		{
			if (this.m_SmallAdaptiveRt != null)
			{
				return false;
			}
			this.m_AdaptiveRtFormat = 2;
			if (SystemInfo.SupportsRenderTextureFormat(13))
			{
				this.m_AdaptiveRtFormat = 13;
			}
			this.m_SmallAdaptiveRt = new RenderTexture(1, 1, 0, this.m_AdaptiveRtFormat);
			this.m_SmallAdaptiveRt.hideFlags = 52;
			return true;
		}

		// Token: 0x06002543 RID: 9539 RVA: 0x000CD8AC File Offset: 0x000CBAAC
		[ImageEffectTransformsToLDR]
		public void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			this.material.shaderKeywords = null;
			RenderTexture renderTexture = null;
			if (this.eyeAdaptation.enabled)
			{
				bool flag = this.CheckSmallAdaptiveRt();
				int num = (source.width >= source.height) ? source.height : source.width;
				int num2 = num;
				num2 |= num2 >> 1;
				num2 |= num2 >> 2;
				num2 |= num2 >> 4;
				num2 |= num2 >> 8;
				num2 |= num2 >> 16;
				num2 -= num2 >> 1;
				renderTexture = RenderTexture.GetTemporary(num2, num2, 0, this.m_AdaptiveRtFormat);
				Graphics.Blit(source, renderTexture);
				int num3 = (int)Mathf.Log((float)renderTexture.width, 2f);
				int num4 = 2;
				if (this.rts == null || this.rts.Length != num3)
				{
					this.rts = new RenderTexture[num3];
				}
				for (int i = 0; i < num3; i++)
				{
					this.rts[i] = RenderTexture.GetTemporary(renderTexture.width / num4, renderTexture.width / num4, 0, this.m_AdaptiveRtFormat);
					num4 <<= 1;
				}
				RenderTexture renderTexture2 = this.rts[num3 - 1];
				Graphics.Blit(renderTexture, this.rts[0], this.material, 1);
				for (int j = 0; j < num3 - 1; j++)
				{
					Graphics.Blit(this.rts[j], this.rts[j + 1]);
					renderTexture2 = this.rts[j + 1];
				}
				this.m_SmallAdaptiveRt.MarkRestoreExpected();
				this.material.SetFloat("_AdaptationSpeed", Mathf.Max(this.eyeAdaptation.speed, 0.001f));
				Graphics.Blit(renderTexture2, this.m_SmallAdaptiveRt, this.material, (!flag) ? 2 : 3);
				this.material.SetFloat("_MiddleGrey", this.eyeAdaptation.middleGrey);
				this.material.SetFloat("_AdaptationMin", Mathf.Pow(2f, this.eyeAdaptation.min));
				this.material.SetFloat("_AdaptationMax", Mathf.Pow(2f, this.eyeAdaptation.max));
				this.material.SetTexture("_LumTex", this.m_SmallAdaptiveRt);
			}
			bool flag2 = this.lut.enabled && this.lut.texture != null && this.CheckUserLut();
			int num5 = 4;
			if (this.tonemapping.enabled)
			{
				if (this.tonemapping.tonemapper == TonemappingColorGrading.Tonemapper.Curve)
				{
					if (this.m_TonemapperDirty)
					{
						float num6 = 1f;
						if (this.tonemapping.curve.length > 0)
						{
							num6 = this.tonemapping.curve[this.tonemapping.curve.length - 1].time;
							for (float num7 = 0f; num7 <= 1f; num7 += 0.003921569f)
							{
								float num8 = this.tonemapping.curve.Evaluate(num7 * num6);
								this.tonemapperCurve.SetPixel(Mathf.FloorToInt(num7 * 255f), 0, new Color(num8, num8, num8));
							}
							this.tonemapperCurve.Apply();
						}
						this.m_TonemapperCurveRange = 1f / num6;
						this.m_TonemapperDirty = false;
					}
					this.material.SetFloat("_ToneCurveRange", this.m_TonemapperCurveRange);
					this.material.SetTexture("_ToneCurve", this.tonemapperCurve);
				}
				else if (this.tonemapping.tonemapper == TonemappingColorGrading.Tonemapper.Neutral)
				{
					float num9 = this.tonemapping.neutralBlackIn * 20f + 1f;
					float num10 = this.tonemapping.neutralBlackOut * 10f + 1f;
					float num11 = this.tonemapping.neutralWhiteIn / 20f;
					float num12 = 1f - this.tonemapping.neutralWhiteOut / 20f;
					float num13 = num9 / num10;
					float num14 = num11 / num12;
					float num15 = Mathf.Max(0f, Mathf.LerpUnclamped(0.57f, 0.37f, num13));
					float num16 = Mathf.LerpUnclamped(0.01f, 0.24f, num14);
					float num17 = Mathf.Max(0f, Mathf.LerpUnclamped(0.02f, 0.2f, num13));
					this.material.SetVector("_NeutralTonemapperParams1", new Vector4(0.2f, num15, num16, num17));
					this.material.SetVector("_NeutralTonemapperParams2", new Vector4(0.02f, 0.3f, this.tonemapping.neutralWhiteLevel, this.tonemapping.neutralWhiteClip / 10f));
				}
				this.material.SetFloat("_Exposure", this.tonemapping.exposure);
				num5 = (int)(num5 + (this.tonemapping.tonemapper + 1) * TonemappingColorGrading.Tonemapper.Hable);
			}
			num5 += ((!flag2) ? 0 : 1);
			if (this.colorGrading.enabled)
			{
				if (this.m_Dirty || !this.m_InternalLut.IsCreated())
				{
					Color color;
					Color color2;
					Color color3;
					this.GenerateLiftGammaGain(out color, out color2, out color3);
					this.GenCurveTexture();
					this.material.SetVector("_WhiteBalance", this.GetWhiteBalance());
					this.material.SetVector("_Lift", color);
					this.material.SetVector("_Gamma", color2);
					this.material.SetVector("_Gain", color3);
					this.material.SetVector("_ContrastGainGamma", new Vector3(this.colorGrading.basics.contrast, this.colorGrading.basics.gain, 1f / this.colorGrading.basics.gamma));
					this.material.SetFloat("_Vibrance", this.colorGrading.basics.vibrance);
					this.material.SetVector("_HSV", new Vector4(this.colorGrading.basics.hue, this.colorGrading.basics.saturation, this.colorGrading.basics.value));
					this.material.SetVector("_ChannelMixerRed", this.colorGrading.channelMixer.channels[0]);
					this.material.SetVector("_ChannelMixerGreen", this.colorGrading.channelMixer.channels[1]);
					this.material.SetVector("_ChannelMixerBlue", this.colorGrading.channelMixer.channels[2]);
					this.material.SetTexture("_CurveTex", this.curveTexture);
					this.internalLutRt.MarkRestoreExpected();
					Graphics.Blit(this.identityLut, this.internalLutRt, this.material, 0);
					this.m_Dirty = false;
				}
				this.material.SetTexture("_InternalLutTex", this.internalLutRt);
				this.material.SetVector("_InternalLutParams", new Vector3(1f / (float)this.internalLutRt.width, 1f / (float)this.internalLutRt.height, (float)this.internalLutRt.height - 1f));
			}
			if (flag2)
			{
				this.material.SetTexture("_UserLutTex", this.lut.texture);
				this.material.SetVector("_UserLutParams", new Vector4(1f / (float)this.lut.texture.width, 1f / (float)this.lut.texture.height, (float)this.lut.texture.height - 1f, this.lut.contribution));
			}
			GL.sRGBWrite = true;
			Graphics.Blit(source, destination, this.material, num5);
			if (this.eyeAdaptation.enabled)
			{
				for (int k = 0; k < this.rts.Length; k++)
				{
					RenderTexture.ReleaseTemporary(this.rts[k]);
				}
				RenderTexture.ReleaseTemporary(renderTexture);
			}
		}

		// Token: 0x06002544 RID: 9540 RVA: 0x000CE1C0 File Offset: 0x000CC3C0
		public Texture2D BakeLUT()
		{
			Texture2D texture2D = new Texture2D(this.internalLutRt.width, this.internalLutRt.height, 3, false, true);
			RenderTexture.active = this.internalLutRt;
			texture2D.ReadPixels(new Rect(0f, 0f, (float)texture2D.width, (float)texture2D.height), 0, 0);
			RenderTexture.active = null;
			return texture2D;
		}

		// Token: 0x04002119 RID: 8473
		[SerializeField]
		[TonemappingColorGrading.SettingsGroup]
		private TonemappingColorGrading.EyeAdaptationSettings m_EyeAdaptation = TonemappingColorGrading.EyeAdaptationSettings.defaultSettings;

		// Token: 0x0400211A RID: 8474
		[SerializeField]
		[TonemappingColorGrading.SettingsGroup]
		private TonemappingColorGrading.TonemappingSettings m_Tonemapping = TonemappingColorGrading.TonemappingSettings.defaultSettings;

		// Token: 0x0400211B RID: 8475
		[SerializeField]
		[TonemappingColorGrading.SettingsGroup]
		private TonemappingColorGrading.ColorGradingSettings m_ColorGrading = TonemappingColorGrading.ColorGradingSettings.defaultSettings;

		// Token: 0x0400211C RID: 8476
		[SerializeField]
		[TonemappingColorGrading.SettingsGroup]
		private TonemappingColorGrading.LUTSettings m_Lut = TonemappingColorGrading.LUTSettings.defaultSettings;

		// Token: 0x0400211D RID: 8477
		private Texture2D m_IdentityLut;

		// Token: 0x0400211E RID: 8478
		private RenderTexture m_InternalLut;

		// Token: 0x0400211F RID: 8479
		private Texture2D m_CurveTexture;

		// Token: 0x04002120 RID: 8480
		private Texture2D m_TonemapperCurve;

		// Token: 0x04002121 RID: 8481
		private float m_TonemapperCurveRange;

		// Token: 0x04002122 RID: 8482
		[SerializeField]
		private Shader m_Shader;

		// Token: 0x04002123 RID: 8483
		private Material m_Material;

		// Token: 0x04002126 RID: 8486
		private bool m_Dirty = true;

		// Token: 0x04002127 RID: 8487
		private bool m_TonemapperDirty = true;

		// Token: 0x04002128 RID: 8488
		private RenderTexture m_SmallAdaptiveRt;

		// Token: 0x04002129 RID: 8489
		private RenderTextureFormat m_AdaptiveRtFormat;

		// Token: 0x0400212A RID: 8490
		private RenderTexture[] rts;

		// Token: 0x020007D7 RID: 2007
		[AttributeUsage(AttributeTargets.Field)]
		public class SettingsGroup : Attribute
		{
		}

		// Token: 0x020007D8 RID: 2008
		public class IndentedGroup : PropertyAttribute
		{
		}

		// Token: 0x020007D9 RID: 2009
		public class ChannelMixer : PropertyAttribute
		{
		}

		// Token: 0x020007DA RID: 2010
		public class ColorWheelGroup : PropertyAttribute
		{
			// Token: 0x06002548 RID: 9544 RVA: 0x000CE23C File Offset: 0x000CC43C
			public ColorWheelGroup()
			{
			}

			// Token: 0x06002549 RID: 9545 RVA: 0x000CE258 File Offset: 0x000CC458
			public ColorWheelGroup(int minSizePerWheel, int maxSizePerWheel)
			{
				this.minSizePerWheel = minSizePerWheel;
				this.maxSizePerWheel = maxSizePerWheel;
			}

			// Token: 0x0400212B RID: 8491
			public int minSizePerWheel = 60;

			// Token: 0x0400212C RID: 8492
			public int maxSizePerWheel = 150;
		}

		// Token: 0x020007DB RID: 2011
		public class Curve : PropertyAttribute
		{
			// Token: 0x0600254A RID: 9546 RVA: 0x000CE284 File Offset: 0x000CC484
			public Curve()
			{
			}

			// Token: 0x0600254B RID: 9547 RVA: 0x000CE298 File Offset: 0x000CC498
			public Curve(float r, float g, float b, float a)
			{
				this.color = new Color(r, g, b, a);
			}

			// Token: 0x0400212D RID: 8493
			public Color color = Color.white;
		}

		// Token: 0x020007DC RID: 2012
		[Serializable]
		public struct EyeAdaptationSettings
		{
			// Token: 0x170002AC RID: 684
			// (get) Token: 0x0600254C RID: 9548 RVA: 0x000CE2BC File Offset: 0x000CC4BC
			public static TonemappingColorGrading.EyeAdaptationSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.EyeAdaptationSettings
					{
						enabled = false,
						showDebug = false,
						middleGrey = 0.5f,
						min = -3f,
						max = 3f,
						speed = 1.5f
					};
				}
			}

			// Token: 0x0400212E RID: 8494
			public bool enabled;

			// Token: 0x0400212F RID: 8495
			[Min(0f)]
			[Tooltip("Midpoint Adjustment.")]
			public float middleGrey;

			// Token: 0x04002130 RID: 8496
			[Tooltip("The lowest possible exposure value; adjust this value to modify the brightest areas of your level.")]
			public float min;

			// Token: 0x04002131 RID: 8497
			[Tooltip("The highest possible exposure value; adjust this value to modify the darkest areas of your level.")]
			public float max;

			// Token: 0x04002132 RID: 8498
			[Tooltip("Speed of linear adaptation. Higher is faster.")]
			[Min(0f)]
			public float speed;

			// Token: 0x04002133 RID: 8499
			[Tooltip("Displays a luminosity helper in the GameView.")]
			public bool showDebug;
		}

		// Token: 0x020007DD RID: 2013
		public enum Tonemapper
		{
			// Token: 0x04002135 RID: 8501
			ACES,
			// Token: 0x04002136 RID: 8502
			Curve,
			// Token: 0x04002137 RID: 8503
			Hable,
			// Token: 0x04002138 RID: 8504
			HejlDawson,
			// Token: 0x04002139 RID: 8505
			Photographic,
			// Token: 0x0400213A RID: 8506
			Reinhard,
			// Token: 0x0400213B RID: 8507
			Neutral
		}

		// Token: 0x020007DE RID: 2014
		[Serializable]
		public struct TonemappingSettings
		{
			// Token: 0x170002AD RID: 685
			// (get) Token: 0x0600254D RID: 9549 RVA: 0x000CE314 File Offset: 0x000CC514
			public static TonemappingColorGrading.TonemappingSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.TonemappingSettings
					{
						enabled = false,
						tonemapper = TonemappingColorGrading.Tonemapper.Neutral,
						exposure = 1f,
						curve = TonemappingColorGrading.CurvesSettings.defaultCurve,
						neutralBlackIn = 0.02f,
						neutralWhiteIn = 10f,
						neutralBlackOut = 0f,
						neutralWhiteOut = 10f,
						neutralWhiteLevel = 5.3f,
						neutralWhiteClip = 10f
					};
				}
			}

			// Token: 0x0400213C RID: 8508
			public bool enabled;

			// Token: 0x0400213D RID: 8509
			[Tooltip("Tonemapping technique to use. ACES is the recommended one.")]
			public TonemappingColorGrading.Tonemapper tonemapper;

			// Token: 0x0400213E RID: 8510
			[Tooltip("Adjusts the overall exposure of the scene.")]
			[Min(0f)]
			public float exposure;

			// Token: 0x0400213F RID: 8511
			[Tooltip("Custom tonemapping curve.")]
			public AnimationCurve curve;

			// Token: 0x04002140 RID: 8512
			[Range(-0.1f, 0.1f)]
			public float neutralBlackIn;

			// Token: 0x04002141 RID: 8513
			[Range(1f, 20f)]
			public float neutralWhiteIn;

			// Token: 0x04002142 RID: 8514
			[Range(-0.09f, 0.1f)]
			public float neutralBlackOut;

			// Token: 0x04002143 RID: 8515
			[Range(1f, 19f)]
			public float neutralWhiteOut;

			// Token: 0x04002144 RID: 8516
			[Range(0.1f, 20f)]
			public float neutralWhiteLevel;

			// Token: 0x04002145 RID: 8517
			[Range(1f, 10f)]
			public float neutralWhiteClip;
		}

		// Token: 0x020007DF RID: 2015
		[Serializable]
		public struct LUTSettings
		{
			// Token: 0x170002AE RID: 686
			// (get) Token: 0x0600254E RID: 9550 RVA: 0x000CE39C File Offset: 0x000CC59C
			public static TonemappingColorGrading.LUTSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.LUTSettings
					{
						enabled = false,
						texture = null,
						contribution = 1f
					};
				}
			}

			// Token: 0x04002146 RID: 8518
			public bool enabled;

			// Token: 0x04002147 RID: 8519
			[Tooltip("Custom lookup texture (strip format, e.g. 256x16).")]
			public Texture texture;

			// Token: 0x04002148 RID: 8520
			[Range(0f, 1f)]
			[Tooltip("Blending factor.")]
			public float contribution;
		}

		// Token: 0x020007E0 RID: 2016
		[Serializable]
		public struct ColorWheelsSettings
		{
			// Token: 0x170002AF RID: 687
			// (get) Token: 0x0600254F RID: 9551 RVA: 0x000CE3D0 File Offset: 0x000CC5D0
			public static TonemappingColorGrading.ColorWheelsSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.ColorWheelsSettings
					{
						shadows = Color.white,
						midtones = Color.white,
						highlights = Color.white
					};
				}
			}

			// Token: 0x04002149 RID: 8521
			[ColorUsage(false)]
			public Color shadows;

			// Token: 0x0400214A RID: 8522
			[ColorUsage(false)]
			public Color midtones;

			// Token: 0x0400214B RID: 8523
			[ColorUsage(false)]
			public Color highlights;
		}

		// Token: 0x020007E1 RID: 2017
		[Serializable]
		public struct BasicsSettings
		{
			// Token: 0x170002B0 RID: 688
			// (get) Token: 0x06002550 RID: 9552 RVA: 0x000CE40C File Offset: 0x000CC60C
			public static TonemappingColorGrading.BasicsSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.BasicsSettings
					{
						temperatureShift = 0f,
						tint = 0f,
						contrast = 1f,
						hue = 0f,
						saturation = 1f,
						value = 1f,
						vibrance = 0f,
						gain = 1f,
						gamma = 1f
					};
				}
			}

			// Token: 0x0400214C RID: 8524
			[Range(-2f, 2f)]
			[Tooltip("Sets the white balance to a custom color temperature.")]
			public float temperatureShift;

			// Token: 0x0400214D RID: 8525
			[Range(-2f, 2f)]
			[Tooltip("Sets the white balance to compensate for a green or magenta tint.")]
			public float tint;

			// Token: 0x0400214E RID: 8526
			[Space]
			[Range(-0.5f, 0.5f)]
			[Tooltip("Shift the hue of all colors.")]
			public float hue;

			// Token: 0x0400214F RID: 8527
			[Range(0f, 2f)]
			[Tooltip("Pushes the intensity of all colors.")]
			public float saturation;

			// Token: 0x04002150 RID: 8528
			[Range(-1f, 1f)]
			[Tooltip("Adjusts the saturation so that clipping is minimized as colors approach full saturation.")]
			public float vibrance;

			// Token: 0x04002151 RID: 8529
			[Tooltip("Brightens or darkens all colors.")]
			[Range(0f, 10f)]
			public float value;

			// Token: 0x04002152 RID: 8530
			[Tooltip("Expands or shrinks the overall range of tonal values.")]
			[Range(0f, 2f)]
			[Space]
			public float contrast;

			// Token: 0x04002153 RID: 8531
			[Tooltip("Contrast gain curve. Controls the steepness of the curve.")]
			[Range(0.01f, 5f)]
			public float gain;

			// Token: 0x04002154 RID: 8532
			[Tooltip("Applies a pow function to the source.")]
			[Range(0.01f, 5f)]
			public float gamma;
		}

		// Token: 0x020007E2 RID: 2018
		[Serializable]
		public struct ChannelMixerSettings
		{
			// Token: 0x170002B1 RID: 689
			// (get) Token: 0x06002551 RID: 9553 RVA: 0x000CE490 File Offset: 0x000CC690
			public static TonemappingColorGrading.ChannelMixerSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.ChannelMixerSettings
					{
						currentChannel = 0,
						channels = new Vector3[]
						{
							new Vector3(1f, 0f, 0f),
							new Vector3(0f, 1f, 0f),
							new Vector3(0f, 0f, 1f)
						}
					};
				}
			}

			// Token: 0x04002155 RID: 8533
			public int currentChannel;

			// Token: 0x04002156 RID: 8534
			public Vector3[] channels;
		}

		// Token: 0x020007E3 RID: 2019
		[Serializable]
		public struct CurvesSettings
		{
			// Token: 0x170002B2 RID: 690
			// (get) Token: 0x06002552 RID: 9554 RVA: 0x000CE51C File Offset: 0x000CC71C
			public static TonemappingColorGrading.CurvesSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.CurvesSettings
					{
						master = TonemappingColorGrading.CurvesSettings.defaultCurve,
						red = TonemappingColorGrading.CurvesSettings.defaultCurve,
						green = TonemappingColorGrading.CurvesSettings.defaultCurve,
						blue = TonemappingColorGrading.CurvesSettings.defaultCurve
					};
				}
			}

			// Token: 0x170002B3 RID: 691
			// (get) Token: 0x06002553 RID: 9555 RVA: 0x000CE564 File Offset: 0x000CC764
			public static AnimationCurve defaultCurve
			{
				get
				{
					return new AnimationCurve(new Keyframe[]
					{
						new Keyframe(0f, 0f, 1f, 1f),
						new Keyframe(1f, 1f, 1f, 1f)
					});
				}
			}

			// Token: 0x04002157 RID: 8535
			[TonemappingColorGrading.Curve]
			public AnimationCurve master;

			// Token: 0x04002158 RID: 8536
			[TonemappingColorGrading.Curve(1f, 0f, 0f, 1f)]
			public AnimationCurve red;

			// Token: 0x04002159 RID: 8537
			[TonemappingColorGrading.Curve(0f, 1f, 0f, 1f)]
			public AnimationCurve green;

			// Token: 0x0400215A RID: 8538
			[TonemappingColorGrading.Curve(0f, 1f, 1f, 1f)]
			public AnimationCurve blue;
		}

		// Token: 0x020007E4 RID: 2020
		public enum ColorGradingPrecision
		{
			// Token: 0x0400215C RID: 8540
			Normal = 16,
			// Token: 0x0400215D RID: 8541
			High = 32
		}

		// Token: 0x020007E5 RID: 2021
		[Serializable]
		public struct ColorGradingSettings
		{
			// Token: 0x170002B4 RID: 692
			// (get) Token: 0x06002554 RID: 9556 RVA: 0x000CE5C8 File Offset: 0x000CC7C8
			public static TonemappingColorGrading.ColorGradingSettings defaultSettings
			{
				get
				{
					return new TonemappingColorGrading.ColorGradingSettings
					{
						enabled = false,
						useDithering = false,
						showDebug = false,
						precision = TonemappingColorGrading.ColorGradingPrecision.Normal,
						colorWheels = TonemappingColorGrading.ColorWheelsSettings.defaultSettings,
						basics = TonemappingColorGrading.BasicsSettings.defaultSettings,
						channelMixer = TonemappingColorGrading.ChannelMixerSettings.defaultSettings,
						curves = TonemappingColorGrading.CurvesSettings.defaultSettings
					};
				}
			}

			// Token: 0x06002555 RID: 9557 RVA: 0x000CE630 File Offset: 0x000CC830
			internal void Reset()
			{
				this.curves = TonemappingColorGrading.CurvesSettings.defaultSettings;
			}

			// Token: 0x0400215E RID: 8542
			public bool enabled;

			// Token: 0x0400215F RID: 8543
			[Tooltip("Internal LUT precision. \"Normal\" is 256x16, \"High\" is 1024x32. Prefer \"Normal\" on mobile devices.")]
			public TonemappingColorGrading.ColorGradingPrecision precision;

			// Token: 0x04002160 RID: 8544
			[TonemappingColorGrading.ColorWheelGroup]
			[Space]
			public TonemappingColorGrading.ColorWheelsSettings colorWheels;

			// Token: 0x04002161 RID: 8545
			[TonemappingColorGrading.IndentedGroup]
			[Space]
			public TonemappingColorGrading.BasicsSettings basics;

			// Token: 0x04002162 RID: 8546
			[Space]
			[TonemappingColorGrading.ChannelMixer]
			public TonemappingColorGrading.ChannelMixerSettings channelMixer;

			// Token: 0x04002163 RID: 8547
			[Space]
			[TonemappingColorGrading.IndentedGroup]
			public TonemappingColorGrading.CurvesSettings curves;

			// Token: 0x04002164 RID: 8548
			[Space]
			[Tooltip("Use dithering to try and minimize color banding in dark areas.")]
			public bool useDithering;

			// Token: 0x04002165 RID: 8549
			[Tooltip("Displays the generated LUT in the top left corner of the GameView.")]
			public bool showDebug;
		}

		// Token: 0x020007E6 RID: 2022
		private enum Pass
		{
			// Token: 0x04002167 RID: 8551
			LutGen,
			// Token: 0x04002168 RID: 8552
			AdaptationLog,
			// Token: 0x04002169 RID: 8553
			AdaptationExpBlend,
			// Token: 0x0400216A RID: 8554
			AdaptationExp,
			// Token: 0x0400216B RID: 8555
			TonemappingOff,
			// Token: 0x0400216C RID: 8556
			TonemappingOff_LUT,
			// Token: 0x0400216D RID: 8557
			TonemappingACES,
			// Token: 0x0400216E RID: 8558
			TonemappingACES_LUT,
			// Token: 0x0400216F RID: 8559
			TonemappingCurve,
			// Token: 0x04002170 RID: 8560
			TonemappingCurve_LUT,
			// Token: 0x04002171 RID: 8561
			TonemappingHable,
			// Token: 0x04002172 RID: 8562
			TonemappingHable_LUT,
			// Token: 0x04002173 RID: 8563
			TonemappingHejlDawson,
			// Token: 0x04002174 RID: 8564
			TonemappingHejlDawson_LUT,
			// Token: 0x04002175 RID: 8565
			TonemappingPhotographic,
			// Token: 0x04002176 RID: 8566
			TonemappingPhotographic_LUT,
			// Token: 0x04002177 RID: 8567
			TonemappingReinhard,
			// Token: 0x04002178 RID: 8568
			TonemappingReinhard_LUT,
			// Token: 0x04002179 RID: 8569
			TonemappingNeutral,
			// Token: 0x0400217A RID: 8570
			TonemappingNeutral_LUT,
			// Token: 0x0400217B RID: 8571
			AdaptationDebug
		}
	}
}
