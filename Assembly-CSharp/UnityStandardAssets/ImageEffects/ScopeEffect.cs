﻿using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
	// Token: 0x020004B7 RID: 1207
	[ExecuteInEditMode]
	[AddComponentMenu("Image Effects/Other/Scope Overlay")]
	public class ScopeEffect : PostEffectsBase, IImageEffect
	{
		// Token: 0x060019E5 RID: 6629 RVA: 0x0009164C File Offset: 0x0008F84C
		public override bool CheckResources()
		{
			return true;
		}

		// Token: 0x060019E6 RID: 6630 RVA: 0x00091650 File Offset: 0x0008F850
		public bool IsActive()
		{
			return base.enabled && this.CheckResources();
		}

		// Token: 0x060019E7 RID: 6631 RVA: 0x00091668 File Offset: 0x0008F868
		public void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			this.overlayMaterial.SetVector("_Screen", new Vector2((float)Screen.width, (float)Screen.height));
			Graphics.Blit(source, destination, this.overlayMaterial);
		}

		// Token: 0x040014AA RID: 5290
		public Material overlayMaterial;
	}
}
