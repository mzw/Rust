﻿using System;
using UnityEngine;

// Token: 0x0200054F RID: 1359
public class RandomDynamicPrefab : MonoBehaviour, IClientComponent, global::ILOD
{
	// Token: 0x040017B0 RID: 6064
	public uint Seed;

	// Token: 0x040017B1 RID: 6065
	public float Distance = 100f;

	// Token: 0x040017B2 RID: 6066
	public float Probability = 0.5f;

	// Token: 0x040017B3 RID: 6067
	public string ResourceFolder = string.Empty;
}
