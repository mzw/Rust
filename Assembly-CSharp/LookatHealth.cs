﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006CF RID: 1743
public class LookatHealth : MonoBehaviour
{
	// Token: 0x04001D90 RID: 7568
	public static bool Enabled = true;

	// Token: 0x04001D91 RID: 7569
	public GameObject container;

	// Token: 0x04001D92 RID: 7570
	public Text textHealth;

	// Token: 0x04001D93 RID: 7571
	public Text textStability;

	// Token: 0x04001D94 RID: 7572
	public Image healthBar;

	// Token: 0x04001D95 RID: 7573
	public Image healthBarBG;

	// Token: 0x04001D96 RID: 7574
	public Color barBGColorNormal;

	// Token: 0x04001D97 RID: 7575
	public Color barBGColorUnstable;
}
