﻿using System;

// Token: 0x020000E1 RID: 225
public class Chicken : global::BaseAnimalNPC
{
	// Token: 0x17000071 RID: 113
	// (get) Token: 0x06000B27 RID: 2855 RVA: 0x0004AABC File Offset: 0x00048CBC
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000B28 RID: 2856 RVA: 0x0004AAC0 File Offset: 0x00048CC0
	public override bool WantsToEat(global::BaseEntity best)
	{
		if (best.HasTrait(global::BaseEntity.TraitFlag.Alive))
		{
			return false;
		}
		if (best.HasTrait(global::BaseEntity.TraitFlag.Meat))
		{
			return false;
		}
		global::CollectibleEntity collectibleEntity = best as global::CollectibleEntity;
		if (collectibleEntity != null)
		{
			foreach (global::ItemAmount itemAmount in collectibleEntity.itemList)
			{
				if (itemAmount.itemDef.category == global::ItemCategory.Food)
				{
					return true;
				}
			}
		}
		return base.WantsToEat(best);
	}

	// Token: 0x06000B29 RID: 2857 RVA: 0x0004AB38 File Offset: 0x00048D38
	public override string Categorize()
	{
		return "Chicken";
	}

	// Token: 0x040005EB RID: 1515
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population = 3f;
}
