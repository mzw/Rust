﻿using System;
using System.Collections.Generic;
using ConVar;
using Oxide.Core;
using UnityEngine;

// Token: 0x020003A4 RID: 932
public class HelicopterTurret : MonoBehaviour
{
	// Token: 0x060015EF RID: 5615 RVA: 0x0007EAB8 File Offset: 0x0007CCB8
	public void SetTarget(global::BaseCombatEntity newTarget)
	{
		if (Interface.CallHook("OnHelicopterTarget", new object[]
		{
			this,
			newTarget
		}) != null)
		{
			return;
		}
		this._target = newTarget;
		this.UpdateTargetVisibility();
	}

	// Token: 0x060015F0 RID: 5616 RVA: 0x0007EAF0 File Offset: 0x0007CCF0
	public bool NeedsNewTarget()
	{
		return !this.HasTarget() || (!this.targetVisible && this.TimeSinceTargetLastSeen() > this.loseTargetAfter);
	}

	// Token: 0x060015F1 RID: 5617 RVA: 0x0007EB1C File Offset: 0x0007CD1C
	public bool UpdateTargetFromList(List<global::PatrolHelicopterAI.targetinfo> newTargetList)
	{
		int num = Random.Range(0, newTargetList.Count);
		int i = newTargetList.Count;
		while (i >= 0)
		{
			i--;
			global::PatrolHelicopterAI.targetinfo targetinfo = newTargetList[num];
			if (targetinfo != null && targetinfo.ent != null && targetinfo.IsVisible() && this.InFiringArc(targetinfo.ply))
			{
				this.SetTarget(targetinfo.ply);
				return true;
			}
			num++;
			if (num >= newTargetList.Count)
			{
				num = 0;
			}
		}
		return false;
	}

	// Token: 0x060015F2 RID: 5618 RVA: 0x0007EBAC File Offset: 0x0007CDAC
	public bool TargetVisible()
	{
		this.UpdateTargetVisibility();
		return this.targetVisible;
	}

	// Token: 0x060015F3 RID: 5619 RVA: 0x0007EBBC File Offset: 0x0007CDBC
	public float TimeSinceTargetLastSeen()
	{
		return UnityEngine.Time.realtimeSinceStartup - this.lastSeenTargetTime;
	}

	// Token: 0x060015F4 RID: 5620 RVA: 0x0007EBCC File Offset: 0x0007CDCC
	public bool HasTarget()
	{
		return this._target != null;
	}

	// Token: 0x060015F5 RID: 5621 RVA: 0x0007EBDC File Offset: 0x0007CDDC
	public void ClearTarget()
	{
		this._target = null;
		this.targetVisible = false;
	}

	// Token: 0x060015F6 RID: 5622 RVA: 0x0007EBEC File Offset: 0x0007CDEC
	public void TurretThink()
	{
		if (this.HasTarget() && this.TimeSinceTargetLastSeen() > this.loseTargetAfter * 2f)
		{
			this.ClearTarget();
		}
		if (!this.HasTarget())
		{
			return;
		}
		if (UnityEngine.Time.time - this.lastBurstTime > this.burstLength + this.timeBetweenBursts && this.TargetVisible())
		{
			this.lastBurstTime = UnityEngine.Time.time;
		}
		if (UnityEngine.Time.time < this.lastBurstTime + this.burstLength && UnityEngine.Time.time - this.lastFireTime >= this.fireRate && this.InFiringArc(this._target))
		{
			this.lastFireTime = UnityEngine.Time.time;
			this.FireGun();
		}
	}

	// Token: 0x060015F7 RID: 5623 RVA: 0x0007ECB8 File Offset: 0x0007CEB8
	public void FireGun()
	{
		this._heliAI.FireGun(this._target.transform.position + new Vector3(0f, 0.25f, 0f), ConVar.PatrolHelicopter.bulletAccuracy, this.left);
	}

	// Token: 0x060015F8 RID: 5624 RVA: 0x0007ED04 File Offset: 0x0007CF04
	public Vector3 GetPositionForEntity(global::BaseCombatEntity potentialtarget)
	{
		return potentialtarget.transform.position;
	}

	// Token: 0x060015F9 RID: 5625 RVA: 0x0007ED14 File Offset: 0x0007CF14
	public float AngleToTarget(global::BaseCombatEntity potentialtarget)
	{
		Vector3 positionForEntity = this.GetPositionForEntity(potentialtarget);
		Vector3 position = this.muzzleTransform.position;
		Vector3 normalized = (positionForEntity - position).normalized;
		return Vector3.Angle((!this.left) ? this._heliAI.transform.right : (-this._heliAI.transform.right), normalized);
	}

	// Token: 0x060015FA RID: 5626 RVA: 0x0007ED84 File Offset: 0x0007CF84
	public bool InFiringArc(global::BaseCombatEntity potentialtarget)
	{
		object obj = Interface.CallHook("CanBeTargeted", new object[]
		{
			potentialtarget,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.AngleToTarget(potentialtarget) < 80f;
	}

	// Token: 0x060015FB RID: 5627 RVA: 0x0007EDCC File Offset: 0x0007CFCC
	public void UpdateTargetVisibility()
	{
		if (!this.HasTarget())
		{
			return;
		}
		Vector3 position = this._target.transform.position;
		global::BasePlayer basePlayer = this._target as global::BasePlayer;
		if (basePlayer)
		{
			position = basePlayer.eyes.position;
		}
		bool flag = false;
		float num = Vector3.Distance(position, this.muzzleTransform.position);
		Vector3 normalized = (position - this.muzzleTransform.position).normalized;
		RaycastHit raycastHit;
		if (num < this.maxTargetRange && this.InFiringArc(this._target) && global::GamePhysics.Trace(new Ray(this.muzzleTransform.position + normalized * 6f, normalized), 0f, out raycastHit, num * 1.1f, 1084434689, 0) && raycastHit.collider.gameObject.ToBaseEntity() == this._target)
		{
			flag = true;
		}
		if (flag)
		{
			this.lastSeenTargetTime = UnityEngine.Time.realtimeSinceStartup;
		}
		this.targetVisible = flag;
	}

	// Token: 0x0400106E RID: 4206
	public global::PatrolHelicopterAI _heliAI;

	// Token: 0x0400106F RID: 4207
	public float fireRate = 0.125f;

	// Token: 0x04001070 RID: 4208
	public float burstLength = 3f;

	// Token: 0x04001071 RID: 4209
	public float timeBetweenBursts = 3f;

	// Token: 0x04001072 RID: 4210
	public float maxTargetRange = 300f;

	// Token: 0x04001073 RID: 4211
	public float loseTargetAfter = 5f;

	// Token: 0x04001074 RID: 4212
	public Transform gun_yaw;

	// Token: 0x04001075 RID: 4213
	public Transform gun_pitch;

	// Token: 0x04001076 RID: 4214
	public Transform muzzleTransform;

	// Token: 0x04001077 RID: 4215
	public bool left;

	// Token: 0x04001078 RID: 4216
	public global::BaseCombatEntity _target;

	// Token: 0x04001079 RID: 4217
	private float lastBurstTime = float.NegativeInfinity;

	// Token: 0x0400107A RID: 4218
	private float lastFireTime = float.NegativeInfinity;

	// Token: 0x0400107B RID: 4219
	private float lastSeenTargetTime = float.NegativeInfinity;

	// Token: 0x0400107C RID: 4220
	private bool targetVisible;
}
