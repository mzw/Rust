﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006CB RID: 1739
public class WorkbenchPanel : global::LootPanel, global::IInventoryChanged
{
	// Token: 0x04001D7E RID: 7550
	public Button experimentButton;

	// Token: 0x04001D7F RID: 7551
	public Text timerText;

	// Token: 0x04001D80 RID: 7552
	public Text costText;

	// Token: 0x04001D81 RID: 7553
	public GameObject expermentCostParent;

	// Token: 0x04001D82 RID: 7554
	public GameObject controlsParent;

	// Token: 0x04001D83 RID: 7555
	public GameObject allUnlockedNotification;

	// Token: 0x04001D84 RID: 7556
	public GameObject informationParent;

	// Token: 0x04001D85 RID: 7557
	public GameObject cycleIcon;
}
