﻿using System;
using UnityEngine;

// Token: 0x020001F9 RID: 505
public class SoundModulation : MonoBehaviour, IClientComponent
{
	// Token: 0x04000A00 RID: 2560
	private const int parameterCount = 4;

	// Token: 0x020001FA RID: 506
	public enum Parameter
	{
		// Token: 0x04000A02 RID: 2562
		Gain,
		// Token: 0x04000A03 RID: 2563
		Pitch,
		// Token: 0x04000A04 RID: 2564
		Spread,
		// Token: 0x04000A05 RID: 2565
		MaxDistance
	}

	// Token: 0x020001FB RID: 507
	[Serializable]
	public class Modulator
	{
		// Token: 0x04000A06 RID: 2566
		public global::SoundModulation.Parameter param;

		// Token: 0x04000A07 RID: 2567
		public float value = 1f;
	}
}
