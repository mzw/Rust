﻿using System;
using UnityEngine;

// Token: 0x02000313 RID: 787
public class BucketVMFluidSim : MonoBehaviour
{
	// Token: 0x04000E2E RID: 3630
	public Animator waterbucketAnim;

	// Token: 0x04000E2F RID: 3631
	public ParticleSystem waterPour;

	// Token: 0x04000E30 RID: 3632
	public ParticleSystem waterTurbulence;

	// Token: 0x04000E31 RID: 3633
	public ParticleSystem waterFill;

	// Token: 0x04000E32 RID: 3634
	public float waterLevel;

	// Token: 0x04000E33 RID: 3635
	public float targetWaterLevel;

	// Token: 0x04000E34 RID: 3636
	public AudioSource waterSpill;

	// Token: 0x04000E35 RID: 3637
	private float PlayerEyePitch;

	// Token: 0x04000E36 RID: 3638
	private float turb_forward;

	// Token: 0x04000E37 RID: 3639
	private float turb_side;

	// Token: 0x04000E38 RID: 3640
	private Vector3 lastPosition;

	// Token: 0x04000E39 RID: 3641
	protected Vector3 groundSpeedLast;

	// Token: 0x04000E3A RID: 3642
	private Vector3 lastAngle;

	// Token: 0x04000E3B RID: 3643
	protected Vector3 vecAngleSpeedLast;

	// Token: 0x04000E3C RID: 3644
	private Vector3 initialPosition;
}
