﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using ConVar;
using UnityEngine;

// Token: 0x0200033B RID: 827
public class FileSystem_AssetBundles : global::IFileSystem
{
	// Token: 0x060013E2 RID: 5090 RVA: 0x000745B4 File Offset: 0x000727B4
	public FileSystem_AssetBundles(string assetRoot)
	{
		global::FileSystem_AssetBundles.isError = false;
		this.assetPath = Path.GetDirectoryName(assetRoot) + Path.DirectorySeparatorChar;
		this.rootBundle = AssetBundle.LoadFromFile(assetRoot);
		if (this.rootBundle == null)
		{
			this.LoadError("Couldn't load root AssetBundle - " + assetRoot);
			return;
		}
		AssetBundleManifest[] array = this.rootBundle.LoadAllAssets<AssetBundleManifest>();
		if (array.Length != 1)
		{
			this.LoadError("Couldn't find AssetBundleManifest - " + array.Length);
			return;
		}
		this.manifest = array[0];
		foreach (string bundleName in this.manifest.GetAllAssetBundles())
		{
			this.LoadBundle(bundleName);
			if (global::FileSystem_AssetBundles.isError)
			{
				return;
			}
		}
		this.BuildFileIndex();
	}

	// Token: 0x060013E3 RID: 5091 RVA: 0x000746AC File Offset: 0x000728AC
	public void UnloadBundles()
	{
		this.manifest = null;
		foreach (KeyValuePair<string, AssetBundle> keyValuePair in this.bundles)
		{
			keyValuePair.Value.Unload(false);
			Object.DestroyImmediate(keyValuePair.Value);
		}
		this.bundles.Clear();
		if (this.rootBundle)
		{
			this.rootBundle.Unload(false);
			Object.DestroyImmediate(this.rootBundle);
			this.rootBundle = null;
		}
	}

	// Token: 0x060013E4 RID: 5092 RVA: 0x0007475C File Offset: 0x0007295C
	private void LoadError(string err)
	{
		Debug.LogError(err);
		global::FileSystem_AssetBundles.loadingError = err;
		global::FileSystem_AssetBundles.isError = true;
	}

	// Token: 0x060013E5 RID: 5093 RVA: 0x00074770 File Offset: 0x00072970
	private void LoadBundle(string bundleName)
	{
		if (this.bundles.ContainsKey(bundleName))
		{
			return;
		}
		string text = this.assetPath + bundleName;
		AssetBundle assetBundle = AssetBundle.LoadFromFile(text);
		if (assetBundle == null)
		{
			this.LoadError("Couldn't load AssetBundle - " + text);
			return;
		}
		this.bundles.Add(bundleName, assetBundle);
	}

	// Token: 0x060013E6 RID: 5094 RVA: 0x000747D0 File Offset: 0x000729D0
	private void BuildFileIndex()
	{
		this.files.Clear();
		foreach (KeyValuePair<string, AssetBundle> keyValuePair in this.bundles)
		{
			if (!keyValuePair.Key.StartsWith("content", StringComparison.InvariantCultureIgnoreCase))
			{
				foreach (string key in keyValuePair.Value.GetAllAssetNames())
				{
					this.files.Add(key, keyValuePair.Value);
				}
			}
		}
	}

	// Token: 0x060013E7 RID: 5095 RVA: 0x0007488C File Offset: 0x00072A8C
	public T[] LoadAll<T>(string folder, string search) where T : Object
	{
		List<T> list = new List<T>();
		foreach (KeyValuePair<string, AssetBundle> keyValuePair in from x in this.files
		where x.Key.StartsWith(folder, StringComparison.InvariantCultureIgnoreCase)
		select x)
		{
			if (string.IsNullOrEmpty(search) || StringEx.Contains(keyValuePair.Key, search, CompareOptions.IgnoreCase))
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				if (ConVar.FileConVar.debug)
				{
					File.AppendAllText("filesystem_debug.csv", string.Format("LoadAll,{0}\n", keyValuePair.Key));
				}
				T t = keyValuePair.Value.LoadAsset<T>(keyValuePair.Key);
				if (ConVar.FileConVar.time)
				{
					File.AppendAllText("filesystem.csv", string.Format("LoadAll,{0},{1}\n", keyValuePair.Key, stopwatch.Elapsed.TotalMilliseconds));
				}
				if (!(t == null))
				{
					list.Add(t);
				}
			}
		}
		return list.ToArray();
	}

	// Token: 0x060013E8 RID: 5096 RVA: 0x000749C8 File Offset: 0x00072BC8
	public T Load<T>(string filePath, bool bComplain = true) where T : Object
	{
		AssetBundle assetBundle = null;
		if (!this.files.TryGetValue(filePath, out assetBundle))
		{
			Debug.LogWarning("[BUNDLE] Not found: " + filePath);
			return (T)((object)null);
		}
		Stopwatch stopwatch = Stopwatch.StartNew();
		if (ConVar.FileConVar.debug)
		{
			File.AppendAllText("filesystem_debug.csv", string.Format("Load,{0}\n", filePath));
		}
		T t = assetBundle.LoadAsset<T>(filePath);
		if (ConVar.FileConVar.time)
		{
			File.AppendAllText("filesystem.csv", string.Format("Load,{0},{1}\n", filePath, stopwatch.Elapsed.TotalMilliseconds));
		}
		if (t == null && bComplain)
		{
			Debug.LogWarning("[BUNDLE] Not found in bundle: " + filePath);
		}
		return t;
	}

	// Token: 0x060013E9 RID: 5097 RVA: 0x00074A88 File Offset: 0x00072C88
	public AsyncOperation LoadAsync(string filePath)
	{
		AssetBundle assetBundle = null;
		if (!this.files.TryGetValue(filePath, out assetBundle))
		{
			Debug.LogWarning("[BUNDLE] Not found: " + filePath);
			return null;
		}
		AssetBundleRequest assetBundleRequest = assetBundle.LoadAssetAsync<Object>(filePath);
		if (assetBundleRequest == null)
		{
			Debug.LogWarning("[BUNDLE] Not found in bundle: " + filePath);
		}
		return assetBundleRequest;
	}

	// Token: 0x060013EA RID: 5098 RVA: 0x00074ADC File Offset: 0x00072CDC
	public string[] FindAll(string folder, string search)
	{
		List<string> list = new List<string>();
		foreach (KeyValuePair<string, AssetBundle> keyValuePair in from x in this.files
		where x.Key.StartsWith(folder, StringComparison.InvariantCultureIgnoreCase)
		select x)
		{
			if (string.IsNullOrEmpty(search) || StringEx.Contains(keyValuePair.Key, search, CompareOptions.IgnoreCase))
			{
				list.Add(keyValuePair.Key);
			}
		}
		return list.ToArray();
	}

	// Token: 0x04000EBF RID: 3775
	public static bool isError;

	// Token: 0x04000EC0 RID: 3776
	public static string loadingError = string.Empty;

	// Token: 0x04000EC1 RID: 3777
	private AssetBundle rootBundle;

	// Token: 0x04000EC2 RID: 3778
	private AssetBundleManifest manifest;

	// Token: 0x04000EC3 RID: 3779
	private Dictionary<string, AssetBundle> bundles = new Dictionary<string, AssetBundle>(StringComparer.OrdinalIgnoreCase);

	// Token: 0x04000EC4 RID: 3780
	private Dictionary<string, AssetBundle> files = new Dictionary<string, AssetBundle>(StringComparer.OrdinalIgnoreCase);

	// Token: 0x04000EC5 RID: 3781
	private string assetPath;
}
