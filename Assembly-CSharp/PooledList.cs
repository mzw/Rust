﻿using System;
using System.Collections.Generic;
using Facepunch;

// Token: 0x020007A0 RID: 1952
public class PooledList<T>
{
	// Token: 0x06002465 RID: 9317 RVA: 0x000C8830 File Offset: 0x000C6A30
	public void Alloc()
	{
		if (this.data == null)
		{
			this.data = Pool.GetList<T>();
		}
	}

	// Token: 0x06002466 RID: 9318 RVA: 0x000C8848 File Offset: 0x000C6A48
	public void Free()
	{
		if (this.data != null)
		{
			Pool.FreeList<T>(ref this.data);
		}
	}

	// Token: 0x06002467 RID: 9319 RVA: 0x000C8860 File Offset: 0x000C6A60
	public void Clear()
	{
		if (this.data != null)
		{
			this.data.Clear();
		}
	}

	// Token: 0x04001FDE RID: 8158
	public List<T> data;
}
