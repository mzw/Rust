﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000068 RID: 104
public class EngineSwitch : global::BaseEntity
{
	// Token: 0x060007B1 RID: 1969 RVA: 0x00031E80 File Offset: 0x00030080
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("EngineSwitch.OnRpcMessage", 0.1f))
		{
			if (rpc == 2938268964u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - StartEngine ");
				}
				using (TimeWarning.New("StartEngine", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("StartEngine", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.StartEngine(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in StartEngine");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3455350980u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - StopEngine ");
				}
				using (TimeWarning.New("StopEngine", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("StopEngine", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.StopEngine(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in StopEngine");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060007B2 RID: 1970 RVA: 0x000321EC File Offset: 0x000303EC
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void StopEngine(global::BaseEntity.RPCMessage msg)
	{
		global::MiningQuarry miningQuarry = base.GetParentEntity() as global::MiningQuarry;
		if (miningQuarry)
		{
			miningQuarry.EngineSwitch(false);
		}
	}

	// Token: 0x060007B3 RID: 1971 RVA: 0x00032218 File Offset: 0x00030418
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void StartEngine(global::BaseEntity.RPCMessage msg)
	{
		global::MiningQuarry miningQuarry = base.GetParentEntity() as global::MiningQuarry;
		if (miningQuarry)
		{
			miningQuarry.EngineSwitch(true);
		}
	}
}
