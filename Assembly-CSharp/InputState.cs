﻿using System;
using UnityEngine;

// Token: 0x020004B9 RID: 1209
public class InputState
{
	// Token: 0x060019E9 RID: 6633 RVA: 0x000916DC File Offset: 0x0008F8DC
	public bool IsDown(global::BUTTON btn)
	{
		return this.current != null && (this.SwallowedButtons & (int)btn) != (int)btn && (this.current.buttons & (int)btn) == (int)btn;
	}

	// Token: 0x060019EA RID: 6634 RVA: 0x0009170C File Offset: 0x0008F90C
	public bool WasDown(global::BUTTON btn)
	{
		return this.previous != null && (this.previous.buttons & (int)btn) == (int)btn;
	}

	// Token: 0x060019EB RID: 6635 RVA: 0x0009172C File Offset: 0x0008F92C
	public bool WasJustPressed(global::BUTTON btn)
	{
		return this.IsDown(btn) && !this.WasDown(btn);
	}

	// Token: 0x060019EC RID: 6636 RVA: 0x00091748 File Offset: 0x0008F948
	public bool WasJustReleased(global::BUTTON btn)
	{
		return !this.IsDown(btn) && this.WasDown(btn);
	}

	// Token: 0x060019ED RID: 6637 RVA: 0x00091760 File Offset: 0x0008F960
	public void SwallowButton(global::BUTTON btn)
	{
		if (this.current == null)
		{
			return;
		}
		this.SwallowedButtons |= (int)btn;
	}

	// Token: 0x060019EE RID: 6638 RVA: 0x0009177C File Offset: 0x0008F97C
	private Quaternion AimAngle()
	{
		if (this.current == null)
		{
			return Quaternion.identity;
		}
		return Quaternion.Euler(this.current.aimAngles);
	}

	// Token: 0x060019EF RID: 6639 RVA: 0x000917A0 File Offset: 0x0008F9A0
	public void Flip(InputMessage newcurrent)
	{
		this.SwallowedButtons = 0;
		this.previous.aimAngles = this.current.aimAngles;
		this.previous.buttons = this.current.buttons;
		this.current.aimAngles = newcurrent.aimAngles;
		this.current.buttons = newcurrent.buttons;
	}

	// Token: 0x060019F0 RID: 6640 RVA: 0x00091804 File Offset: 0x0008FA04
	public void Clear()
	{
		this.current.buttons = 0;
		this.previous.buttons = 0;
		this.SwallowedButtons = 0;
	}

	// Token: 0x040014B8 RID: 5304
	public InputMessage current = new InputMessage
	{
		ShouldPool = false
	};

	// Token: 0x040014B9 RID: 5305
	public InputMessage previous = new InputMessage
	{
		ShouldPool = false
	};

	// Token: 0x040014BA RID: 5306
	private int SwallowedButtons;
}
