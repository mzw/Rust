﻿using System;
using UnityEngine;

// Token: 0x0200064E RID: 1614
[CreateAssetMenu(menuName = "Rust/Object List")]
public class ObjectList : ScriptableObject
{
	// Token: 0x04001B65 RID: 7013
	public Object[] objects;
}
