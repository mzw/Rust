﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConVar;
using UnityEngine;

// Token: 0x02000490 RID: 1168
public class SpawnHandler : SingletonComponent<global::SpawnHandler>
{
	// Token: 0x06001966 RID: 6502 RVA: 0x0008EF08 File Offset: 0x0008D108
	protected void OnEnable()
	{
		this.AllSpawnPopulations = this.SpawnPopulations.Concat(this.ConvarSpawnPopulations).ToArray<global::SpawnPopulation>();
		base.StartCoroutine(this.SpawnTick());
		base.StartCoroutine(this.SpawnGroupTick());
	}

	// Token: 0x06001967 RID: 6503 RVA: 0x0008EF40 File Offset: 0x0008D140
	public static global::BasePlayer.SpawnPoint GetSpawnPoint()
	{
		if (SingletonComponent<global::SpawnHandler>.Instance == null || SingletonComponent<global::SpawnHandler>.Instance.CharDistribution == null)
		{
			return null;
		}
		global::BasePlayer.SpawnPoint spawnPoint = new global::BasePlayer.SpawnPoint();
		for (int i = 0; i < 60; i++)
		{
			if (SingletonComponent<global::SpawnHandler>.Instance.CharDistribution.Sample(out spawnPoint.pos, out spawnPoint.rot, false, 0f))
			{
				return spawnPoint;
			}
		}
		return null;
	}

	// Token: 0x06001968 RID: 6504 RVA: 0x0008EFB0 File Offset: 0x0008D1B0
	public void UpdateDistributions()
	{
		global::SpawnHandler.<UpdateDistributions>c__AnonStorey2 <UpdateDistributions>c__AnonStorey = new global::SpawnHandler.<UpdateDistributions>c__AnonStorey2();
		if (global::World.Size == 0u)
		{
			return;
		}
		this.SpawnDistributions = new global::SpawnDistribution[this.AllSpawnPopulations.Length];
		this.population2distribution = new Dictionary<global::SpawnPopulation, global::SpawnDistribution>();
		Vector3 size = global::TerrainMeta.Size;
		Vector3 position = global::TerrainMeta.Position;
		<UpdateDistributions>c__AnonStorey.pop_res = Mathf.NextPowerOfTwo((int)(global::World.Size * 0.25f));
		global::SpawnFilter filter;
		byte[] map;
		for (int i = 0; i < this.AllSpawnPopulations.Length; i++)
		{
			global::SpawnPopulation spawnPopulation = this.AllSpawnPopulations[i];
			if (spawnPopulation == null)
			{
				Debug.LogError("Spawn handler contains null spawn population.");
			}
			else
			{
				byte[] map = new byte[<UpdateDistributions>c__AnonStorey.pop_res * <UpdateDistributions>c__AnonStorey.pop_res];
				global::SpawnFilter filter = spawnPopulation.Filter;
				Parallel.For(0, <UpdateDistributions>c__AnonStorey.pop_res, delegate(int z)
				{
					for (int j = 0; j < <UpdateDistributions>c__AnonStorey.pop_res; j++)
					{
						float normX = ((float)j + 0.5f) / (float)<UpdateDistributions>c__AnonStorey.pop_res;
						float normZ = ((float)z + 0.5f) / (float)<UpdateDistributions>c__AnonStorey.pop_res;
						map[z * <UpdateDistributions>c__AnonStorey.pop_res + j] = (byte)(255f * filter.GetFactor(normX, normZ));
					}
				});
				global::SpawnDistribution value = this.SpawnDistributions[i] = new global::SpawnDistribution(this, map, position, size);
				this.population2distribution.Add(spawnPopulation, value);
			}
		}
		<UpdateDistributions>c__AnonStorey.char_res = Mathf.NextPowerOfTwo((int)(global::World.Size * 0.5f));
		map = new byte[<UpdateDistributions>c__AnonStorey.char_res * <UpdateDistributions>c__AnonStorey.char_res];
		filter = this.CharacterSpawn;
		Parallel.For(0, <UpdateDistributions>c__AnonStorey.char_res, delegate(int z)
		{
			for (int j = 0; j < <UpdateDistributions>c__AnonStorey.char_res; j++)
			{
				float normX = ((float)j + 0.5f) / (float)<UpdateDistributions>c__AnonStorey.char_res;
				float normZ = ((float)z + 0.5f) / (float)<UpdateDistributions>c__AnonStorey.char_res;
				map[z * <UpdateDistributions>c__AnonStorey.char_res + j] = (byte)(255f * filter.GetFactor(normX, normZ));
			}
		});
		this.CharDistribution = new global::SpawnDistribution(this, map, position, size);
	}

	// Token: 0x06001969 RID: 6505 RVA: 0x0008F148 File Offset: 0x0008D348
	public void FillPopulations()
	{
		if (this.SpawnDistributions == null)
		{
			return;
		}
		for (int i = 0; i < this.AllSpawnPopulations.Length; i++)
		{
			if (!(this.AllSpawnPopulations[i] == null))
			{
				this.SpawnInitial(this.AllSpawnPopulations[i], this.SpawnDistributions[i]);
			}
		}
	}

	// Token: 0x0600196A RID: 6506 RVA: 0x0008F1A8 File Offset: 0x0008D3A8
	public void FillGroups()
	{
		for (int i = 0; i < this.SpawnGroups.Count; i++)
		{
			this.SpawnGroups[i].Fill();
		}
	}

	// Token: 0x0600196B RID: 6507 RVA: 0x0008F1E4 File Offset: 0x0008D3E4
	public void InitialSpawn()
	{
		if (ConVar.Spawn.respawn_populations && this.SpawnDistributions != null)
		{
			for (int i = 0; i < this.AllSpawnPopulations.Length; i++)
			{
				if (!(this.AllSpawnPopulations[i] == null))
				{
					this.SpawnInitial(this.AllSpawnPopulations[i], this.SpawnDistributions[i]);
				}
			}
		}
		if (ConVar.Spawn.respawn_groups)
		{
			for (int j = 0; j < this.SpawnGroups.Count; j++)
			{
				this.SpawnGroups[j].SpawnInitial();
			}
		}
	}

	// Token: 0x0600196C RID: 6508 RVA: 0x0008F284 File Offset: 0x0008D484
	public void StartSpawnTick()
	{
		this.spawnTick = true;
	}

	// Token: 0x0600196D RID: 6509 RVA: 0x0008F290 File Offset: 0x0008D490
	private IEnumerator SpawnTick()
	{
		for (;;)
		{
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			if (this.spawnTick)
			{
				if (ConVar.Spawn.respawn_populations)
				{
					yield return UnityEngine.CoroutineEx.waitForSeconds(this.TickInterval);
					for (int i = 0; i < this.AllSpawnPopulations.Length; i++)
					{
						global::SpawnPopulation spawnPopulation = this.AllSpawnPopulations[i];
						if (!(spawnPopulation == null))
						{
							global::SpawnDistribution spawnDistribution = this.SpawnDistributions[i];
							if (spawnDistribution != null)
							{
								try
								{
									if (this.SpawnDistributions != null)
									{
										this.SpawnRepeating(spawnPopulation, spawnDistribution);
									}
								}
								catch (Exception ex)
								{
									Debug.LogError(ex);
								}
								yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
							}
						}
					}
				}
			}
		}
		yield break;
	}

	// Token: 0x0600196E RID: 6510 RVA: 0x0008F2AC File Offset: 0x0008D4AC
	private IEnumerator SpawnGroupTick()
	{
		for (;;)
		{
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			if (this.spawnTick)
			{
				if (ConVar.Spawn.respawn_groups)
				{
					yield return UnityEngine.CoroutineEx.waitForSeconds(1f);
					for (int i = 0; i < this.SpawnGroups.Count; i++)
					{
						global::SpawnGroup spawnGroup = this.SpawnGroups[i];
						if (!(spawnGroup == null))
						{
							try
							{
								spawnGroup.SpawnRepeating();
							}
							catch (Exception ex)
							{
								Debug.LogError(ex);
							}
							yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						}
					}
				}
			}
		}
		yield break;
	}

	// Token: 0x0600196F RID: 6511 RVA: 0x0008F2C8 File Offset: 0x0008D4C8
	public void SpawnInitial(global::SpawnPopulation population, global::SpawnDistribution distribution)
	{
		int targetCount = this.GetTargetCount(population, distribution);
		int currentCount = this.GetCurrentCount(population, distribution);
		int num = targetCount - currentCount;
		this.Spawn(population, distribution, targetCount, num, num * population.SpawnAttemptsInitial);
	}

	// Token: 0x06001970 RID: 6512 RVA: 0x0008F300 File Offset: 0x0008D500
	public void SpawnRepeating(global::SpawnPopulation population, global::SpawnDistribution distribution)
	{
		int targetCount = this.GetTargetCount(population, distribution);
		int currentCount = this.GetCurrentCount(population, distribution);
		int num = targetCount - currentCount;
		num = Mathf.RoundToInt((float)num * population.GetCurrentSpawnRate());
		num = Random.Range(Mathf.Min(num, this.MinSpawnsPerTick), Mathf.Min(num, this.MaxSpawnsPerTick));
		this.Spawn(population, distribution, targetCount, num, num * population.SpawnAttemptsRepeating);
	}

	// Token: 0x06001971 RID: 6513 RVA: 0x0008F364 File Offset: 0x0008D564
	private void Spawn(global::SpawnPopulation population, global::SpawnDistribution distribution, int targetCount, int numToFill, int numToTry)
	{
		if (targetCount == 0)
		{
			return;
		}
		if (!population.Initialize())
		{
			Debug.LogError("[Spawn] No prefabs to spawn in " + population.ResourceFolder, population);
			return;
		}
		if (ConVar.Global.developer > 1)
		{
			Debug.Log(string.Concat(new object[]
			{
				"[Spawn] Population ",
				population.ResourceFolder,
				" needs to spawn ",
				numToFill
			}));
		}
		float num = Mathf.Max((float)population.ClusterSizeMax, distribution.GetGridCellArea() * population.GetMaximumSpawnDensity());
		population.UpdateWeights(distribution, targetCount);
		while (numToFill >= population.ClusterSizeMin && numToTry > 0)
		{
			global::ByteQuadtree.Element node = distribution.SampleNode();
			int num2 = Random.Range(population.ClusterSizeMin, population.ClusterSizeMax + 1);
			num2 = Mathx.Min(numToTry, numToFill, num2);
			for (int i = 0; i < num2; i++)
			{
				Vector3 vector;
				Quaternion rot;
				if (distribution.Sample(out vector, out rot, node, population.AlignToNormal, (float)population.ClusterDithering) && population.Filter.GetFactor(vector) > 0f && (float)distribution.GetCount(vector) < num)
				{
					this.Spawn(population, distribution, vector, rot);
					numToFill--;
				}
				numToTry--;
			}
		}
	}

	// Token: 0x06001972 RID: 6514 RVA: 0x0008F4AC File Offset: 0x0008D6AC
	private GameObject Spawn(global::SpawnPopulation population, global::SpawnDistribution distribution, Vector3 pos, Quaternion rot)
	{
		global::Prefab<global::Spawnable> randomPrefab = population.GetRandomPrefab();
		if (randomPrefab == null)
		{
			return null;
		}
		if (randomPrefab.Component == null)
		{
			Debug.LogError("[Spawn] Missing component 'Spawnable' on " + randomPrefab.Name);
			return null;
		}
		Vector3 one = Vector3.one;
		global::DecorComponent[] components = global::PrefabAttribute.server.FindAll<global::DecorComponent>(randomPrefab.ID);
		randomPrefab.Object.transform.ApplyDecorComponents(components, ref pos, ref rot, ref one);
		if (!randomPrefab.ApplyTerrainAnchors(ref pos, rot, one, global::TerrainAnchorMode.MinimizeMovement, population.Filter))
		{
			return null;
		}
		if (!randomPrefab.ApplyTerrainChecks(pos, rot, one, population.Filter))
		{
			return null;
		}
		if (!randomPrefab.ApplyTerrainFilters(pos, rot, one, null))
		{
			return null;
		}
		LayerMask layerMask = (!SingletonComponent<global::SpawnHandler>.Instance) ? default(LayerMask) : SingletonComponent<global::SpawnHandler>.Instance.BoundsCheckMask;
		if (layerMask != 0)
		{
			global::BaseEntity component = randomPrefab.Object.GetComponent<global::BaseEntity>();
			if (component != null && UnityEngine.Physics.CheckBox(pos + rot * Vector3.Scale(component.bounds.center, one), Vector3.Scale(component.bounds.extents, one), rot, layerMask))
			{
				return null;
			}
		}
		if (randomPrefab.Component.Population != population)
		{
			randomPrefab.Component.Population = population;
		}
		if (ConVar.Global.developer > 1)
		{
			Debug.Log("[Spawn] Spawning " + randomPrefab.Name);
		}
		global::BaseEntity baseEntity = randomPrefab.SpawnEntity(pos, rot);
		if (baseEntity == null)
		{
			Debug.LogWarning("[Spawn] Couldn't create prefab as entity - " + randomPrefab.Name);
			return null;
		}
		baseEntity.Spawn();
		return baseEntity.gameObject;
	}

	// Token: 0x06001973 RID: 6515 RVA: 0x0008F674 File Offset: 0x0008D874
	public void EnforceLimits(bool forceAll = false)
	{
		if (this.SpawnDistributions == null)
		{
			return;
		}
		for (int i = 0; i < this.AllSpawnPopulations.Length; i++)
		{
			if (!(this.AllSpawnPopulations[i] == null))
			{
				global::SpawnPopulation spawnPopulation = this.AllSpawnPopulations[i];
				global::SpawnDistribution distribution = this.SpawnDistributions[i];
				if (forceAll || spawnPopulation.EnforcePopulationLimits)
				{
					this.EnforceLimits(spawnPopulation, distribution);
				}
			}
		}
	}

	// Token: 0x06001974 RID: 6516 RVA: 0x0008F6EC File Offset: 0x0008D8EC
	private void EnforceLimits(global::SpawnPopulation population, global::SpawnDistribution distribution)
	{
		int targetCount = this.GetTargetCount(population, distribution);
		global::Spawnable[] array = this.FindAll(population);
		if (array.Length <= targetCount)
		{
			return;
		}
		Debug.Log(string.Concat(new object[]
		{
			population,
			" has ",
			array.Length,
			" objects, but max allowed is ",
			targetCount
		}));
		int num = array.Length - targetCount;
		Debug.Log(" - deleting " + num + " objects");
		foreach (global::Spawnable spawnable in array.Take(num))
		{
			global::BaseEntity baseEntity = spawnable.gameObject.ToBaseEntity();
			if (baseEntity.IsValid())
			{
				baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
			}
			else
			{
				global::GameManager.Destroy(spawnable.gameObject, 0f);
			}
		}
	}

	// Token: 0x06001975 RID: 6517 RVA: 0x0008F7F0 File Offset: 0x0008D9F0
	public global::Spawnable[] FindAll(global::SpawnPopulation population)
	{
		return (from x in Object.FindObjectsOfType<global::Spawnable>()
		where x.gameObject.activeInHierarchy && x.Population == population
		select x).ToArray<global::Spawnable>();
	}

	// Token: 0x06001976 RID: 6518 RVA: 0x0008F828 File Offset: 0x0008DA28
	public int GetTargetCount(global::SpawnPopulation population, global::SpawnDistribution distribution)
	{
		float num = global::TerrainMeta.Size.x * global::TerrainMeta.Size.z;
		float num2 = population.GetCurrentSpawnDensity();
		if (population.ScaleWithSpawnFilter)
		{
			num2 *= distribution.Density;
		}
		return Mathf.RoundToInt(num * num2);
	}

	// Token: 0x06001977 RID: 6519 RVA: 0x0008F874 File Offset: 0x0008DA74
	public int GetCurrentCount(global::SpawnPopulation population, global::SpawnDistribution distribution)
	{
		return distribution.Count;
	}

	// Token: 0x06001978 RID: 6520 RVA: 0x0008F87C File Offset: 0x0008DA7C
	public void AddInstance(global::Spawnable spawnable)
	{
		global::SpawnDistribution spawnDistribution;
		if (!this.population2distribution.TryGetValue(spawnable.Population, out spawnDistribution))
		{
			Debug.LogWarning("[SpawnHandler] trying to add instance to invalid population: " + spawnable.Population);
			return;
		}
		spawnDistribution.AddInstance(spawnable);
	}

	// Token: 0x06001979 RID: 6521 RVA: 0x0008F8C0 File Offset: 0x0008DAC0
	public void RemoveInstance(global::Spawnable spawnable)
	{
		global::SpawnDistribution spawnDistribution;
		if (!this.population2distribution.TryGetValue(spawnable.Population, out spawnDistribution))
		{
			Debug.LogWarning("[SpawnHandler] trying to remove instance from invalid population: " + spawnable.Population);
			return;
		}
		spawnDistribution.RemoveInstance(spawnable);
	}

	// Token: 0x0600197A RID: 6522 RVA: 0x0008F904 File Offset: 0x0008DB04
	public static float PlayerFraction()
	{
		float num = (float)Mathf.Max(ConVar.Server.maxplayers, 1);
		float num2 = (float)global::BasePlayer.activePlayerList.Count;
		return Mathf.Clamp01(num2 / num);
	}

	// Token: 0x0600197B RID: 6523 RVA: 0x0008F934 File Offset: 0x0008DB34
	public static float PlayerLerp(float min, float max)
	{
		return Mathf.Lerp(min, max, global::SpawnHandler.PlayerFraction());
	}

	// Token: 0x0600197C RID: 6524 RVA: 0x0008F944 File Offset: 0x0008DB44
	public static float PlayerExcess()
	{
		float num = Mathf.Max(ConVar.Spawn.player_base, 1f);
		float num2 = (float)global::BasePlayer.activePlayerList.Count;
		if (num2 <= num)
		{
			return 0f;
		}
		return (num2 - num) / num;
	}

	// Token: 0x0600197D RID: 6525 RVA: 0x0008F980 File Offset: 0x0008DB80
	public static float PlayerScale(float scalar)
	{
		return Mathf.Max(1f, global::SpawnHandler.PlayerExcess() * scalar);
	}

	// Token: 0x0600197E RID: 6526 RVA: 0x0008F994 File Offset: 0x0008DB94
	public void DumpReport(string filename)
	{
		File.AppendAllText(filename, "\r\n\r\nSpawnHandler Report:\r\n\r\n" + this.GetReport(true));
	}

	// Token: 0x0600197F RID: 6527 RVA: 0x0008F9B0 File Offset: 0x0008DBB0
	public string GetReport(bool detailed = true)
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (this.AllSpawnPopulations == null)
		{
			stringBuilder.AppendLine("Spawn population array is null.");
		}
		if (this.SpawnDistributions == null)
		{
			stringBuilder.AppendLine("Spawn distribution array is null.");
		}
		if (this.AllSpawnPopulations != null && this.SpawnDistributions != null)
		{
			for (int i = 0; i < this.AllSpawnPopulations.Length; i++)
			{
				if (!(this.AllSpawnPopulations[i] == null))
				{
					global::SpawnPopulation spawnPopulation = this.AllSpawnPopulations[i];
					global::SpawnDistribution spawnDistribution = this.SpawnDistributions[i];
					if (spawnPopulation != null)
					{
						if (!string.IsNullOrEmpty(spawnPopulation.ResourceFolder))
						{
							stringBuilder.AppendLine(spawnPopulation.name + " (autospawn/" + spawnPopulation.ResourceFolder + ")");
						}
						else
						{
							stringBuilder.AppendLine(spawnPopulation.name);
						}
						if (detailed)
						{
							stringBuilder.AppendLine("\tPrefabs:");
							if (spawnPopulation.Prefabs != null)
							{
								foreach (global::Prefab<global::Spawnable> prefab in spawnPopulation.Prefabs)
								{
									stringBuilder.AppendLine(string.Concat(new object[]
									{
										"\t\t",
										prefab.Name,
										" - ",
										prefab.Object
									}));
								}
							}
							else
							{
								stringBuilder.AppendLine("\t\tN/A");
							}
						}
						if (spawnDistribution != null)
						{
							int currentCount = this.GetCurrentCount(spawnPopulation, spawnDistribution);
							int targetCount = this.GetTargetCount(spawnPopulation, spawnDistribution);
							stringBuilder.AppendLine(string.Concat(new object[]
							{
								"\tPopulation: ",
								currentCount,
								"/",
								targetCount
							}));
						}
						else
						{
							stringBuilder.AppendLine("\tDistribution #" + i + " is not set.");
						}
					}
					else
					{
						stringBuilder.AppendLine("Population #" + i + " is not set.");
					}
					stringBuilder.AppendLine();
				}
			}
		}
		return stringBuilder.ToString();
	}

	// Token: 0x04001410 RID: 5136
	public float TickInterval = 60f;

	// Token: 0x04001411 RID: 5137
	public int MinSpawnsPerTick = 100;

	// Token: 0x04001412 RID: 5138
	public int MaxSpawnsPerTick = 100;

	// Token: 0x04001413 RID: 5139
	public LayerMask PlacementMask = default(LayerMask);

	// Token: 0x04001414 RID: 5140
	public LayerMask PlacementCheckMask = default(LayerMask);

	// Token: 0x04001415 RID: 5141
	public float PlacementCheckHeight = 25f;

	// Token: 0x04001416 RID: 5142
	public LayerMask RadiusCheckMask = default(LayerMask);

	// Token: 0x04001417 RID: 5143
	public float RadiusCheckDistance = 5f;

	// Token: 0x04001418 RID: 5144
	public LayerMask BoundsCheckMask = default(LayerMask);

	// Token: 0x04001419 RID: 5145
	public global::SpawnFilter CharacterSpawn;

	// Token: 0x0400141A RID: 5146
	public global::SpawnPopulation[] SpawnPopulations;

	// Token: 0x0400141B RID: 5147
	internal global::SpawnDistribution[] SpawnDistributions;

	// Token: 0x0400141C RID: 5148
	internal global::SpawnDistribution CharDistribution;

	// Token: 0x0400141D RID: 5149
	internal List<global::SpawnGroup> SpawnGroups = new List<global::SpawnGroup>();

	// Token: 0x0400141E RID: 5150
	[ReadOnly]
	public global::SpawnPopulation[] ConvarSpawnPopulations;

	// Token: 0x0400141F RID: 5151
	private Dictionary<global::SpawnPopulation, global::SpawnDistribution> population2distribution;

	// Token: 0x04001420 RID: 5152
	private bool spawnTick;

	// Token: 0x04001421 RID: 5153
	private global::SpawnPopulation[] AllSpawnPopulations;
}
