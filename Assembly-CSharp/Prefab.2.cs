﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000465 RID: 1125
public class Prefab : IComparable<global::Prefab>
{
	// Token: 0x060018A6 RID: 6310 RVA: 0x0008AE54 File Offset: 0x00089054
	public Prefab(string name, GameObject prefab, global::GameManager manager, global::PrefabAttribute.Library attribute)
	{
		this.ID = global::StringPool.Get(name);
		this.Name = name;
		this.Object = prefab;
		this.Manager = manager;
		this.Attribute = attribute;
		this.Parameters = prefab.GetComponent<global::PrefabParameters>();
	}

	// Token: 0x060018A7 RID: 6311 RVA: 0x0008AE94 File Offset: 0x00089094
	public static implicit operator GameObject(global::Prefab prefab)
	{
		return prefab.Object;
	}

	// Token: 0x060018A8 RID: 6312 RVA: 0x0008AE9C File Offset: 0x0008909C
	public int CompareTo(global::Prefab that)
	{
		if (that == null)
		{
			return 1;
		}
		global::PrefabPriority prefabPriority = (!(this.Parameters != null)) ? global::PrefabPriority.Default : this.Parameters.Priority;
		return ((!(that.Parameters != null)) ? global::PrefabPriority.Default : that.Parameters.Priority).CompareTo(prefabPriority);
	}

	// Token: 0x060018A9 RID: 6313 RVA: 0x0008AF0C File Offset: 0x0008910C
	public bool ApplyTerrainAnchors(ref Vector3 pos, Quaternion rot, Vector3 scale, global::TerrainAnchorMode mode, global::SpawnFilter filter = null)
	{
		global::TerrainAnchor[] anchors = this.Attribute.FindAll<global::TerrainAnchor>(this.ID);
		return this.Object.transform.ApplyTerrainAnchors(anchors, ref pos, rot, scale, mode, filter);
	}

	// Token: 0x060018AA RID: 6314 RVA: 0x0008AF44 File Offset: 0x00089144
	public bool ApplyTerrainAnchors(ref Vector3 pos, Quaternion rot, Vector3 scale, global::SpawnFilter filter = null)
	{
		global::TerrainAnchor[] anchors = this.Attribute.FindAll<global::TerrainAnchor>(this.ID);
		return this.Object.transform.ApplyTerrainAnchors(anchors, ref pos, rot, scale, filter);
	}

	// Token: 0x060018AB RID: 6315 RVA: 0x0008AF7C File Offset: 0x0008917C
	public bool ApplyTerrainChecks(Vector3 pos, Quaternion rot, Vector3 scale, global::SpawnFilter filter = null)
	{
		global::TerrainCheck[] anchors = this.Attribute.FindAll<global::TerrainCheck>(this.ID);
		return this.Object.transform.ApplyTerrainChecks(anchors, pos, rot, scale, filter);
	}

	// Token: 0x060018AC RID: 6316 RVA: 0x0008AFB4 File Offset: 0x000891B4
	public bool ApplyTerrainFilters(Vector3 pos, Quaternion rot, Vector3 scale, global::SpawnFilter filter = null)
	{
		global::TerrainFilter[] filters = this.Attribute.FindAll<global::TerrainFilter>(this.ID);
		return this.Object.transform.ApplyTerrainFilters(filters, pos, rot, scale, filter);
	}

	// Token: 0x060018AD RID: 6317 RVA: 0x0008AFEC File Offset: 0x000891EC
	public void ApplyTerrainModifiers(Vector3 pos, Quaternion rot, Vector3 scale)
	{
		global::TerrainModifier[] modifiers = this.Attribute.FindAll<global::TerrainModifier>(this.ID);
		this.Object.transform.ApplyTerrainModifiers(modifiers, pos, rot, scale);
	}

	// Token: 0x060018AE RID: 6318 RVA: 0x0008B020 File Offset: 0x00089220
	public void ApplyTerrainPlacements(Vector3 pos, Quaternion rot, Vector3 scale)
	{
		global::TerrainPlacement[] placements = this.Attribute.FindAll<global::TerrainPlacement>(this.ID);
		this.Object.transform.ApplyTerrainPlacements(placements, pos, rot, scale);
	}

	// Token: 0x060018AF RID: 6319 RVA: 0x0008B054 File Offset: 0x00089254
	public void ApplyDecorComponents(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		global::DecorComponent[] components = this.Attribute.FindAll<global::DecorComponent>(this.ID);
		this.Object.transform.ApplyDecorComponents(components, ref pos, ref rot, ref scale);
	}

	// Token: 0x060018B0 RID: 6320 RVA: 0x0008B088 File Offset: 0x00089288
	public bool CheckEnvironmentVolumes(Vector3 pos, Quaternion rot, Vector3 scale, global::EnvironmentType type)
	{
		return this.Object.transform.CheckEnvironmentVolumes(pos, rot, scale, type);
	}

	// Token: 0x060018B1 RID: 6321 RVA: 0x0008B0A0 File Offset: 0x000892A0
	public GameObject Spawn(Transform transform)
	{
		return this.Manager.CreatePrefab(this.Name, transform, true);
	}

	// Token: 0x060018B2 RID: 6322 RVA: 0x0008B0B8 File Offset: 0x000892B8
	public GameObject Spawn(Vector3 pos, Quaternion rot)
	{
		return this.Manager.CreatePrefab(this.Name, pos, rot, true);
	}

	// Token: 0x060018B3 RID: 6323 RVA: 0x0008B0D0 File Offset: 0x000892D0
	public GameObject Spawn(Vector3 pos, Quaternion rot, Vector3 scale)
	{
		return this.Manager.CreatePrefab(this.Name, pos, rot, scale, true);
	}

	// Token: 0x060018B4 RID: 6324 RVA: 0x0008B0E8 File Offset: 0x000892E8
	public global::BaseEntity SpawnEntity(Vector3 pos, Quaternion rot)
	{
		return this.Manager.CreateEntity(this.Name, pos, rot, true);
	}

	// Token: 0x060018B5 RID: 6325 RVA: 0x0008B100 File Offset: 0x00089300
	public static global::Prefab[] Load(string folder, global::GameManager manager = null, global::PrefabAttribute.Library attribute = null, bool useProbabilities = true)
	{
		if (string.IsNullOrEmpty(folder))
		{
			return null;
		}
		if (manager == null)
		{
			manager = global::Prefab.DefaultManager;
		}
		if (attribute == null)
		{
			attribute = global::Prefab.DefaultAttribute;
		}
		string[] array = global::Prefab.FindPrefabNames(folder, useProbabilities);
		global::Prefab[] array2 = new global::Prefab[array.Length];
		for (int i = 0; i < array2.Length; i++)
		{
			string text = array[i];
			GameObject prefab = manager.FindPrefab(text);
			array2[i] = new global::Prefab(text, prefab, manager, attribute);
		}
		return array2;
	}

	// Token: 0x060018B6 RID: 6326 RVA: 0x0008B174 File Offset: 0x00089374
	public static global::Prefab<T>[] Load<T>(string folder, global::GameManager manager = null, global::PrefabAttribute.Library attribute = null, bool useProbabilities = true) where T : Component
	{
		if (string.IsNullOrEmpty(folder))
		{
			return null;
		}
		string[] names = global::Prefab.FindPrefabNames(folder, useProbabilities);
		return global::Prefab.Load<T>(names, manager, attribute);
	}

	// Token: 0x060018B7 RID: 6327 RVA: 0x0008B1A0 File Offset: 0x000893A0
	public static global::Prefab<T>[] Load<T>(string[] names, global::GameManager manager = null, global::PrefabAttribute.Library attribute = null) where T : Component
	{
		if (manager == null)
		{
			manager = global::Prefab.DefaultManager;
		}
		if (attribute == null)
		{
			attribute = global::Prefab.DefaultAttribute;
		}
		global::Prefab<T>[] array = new global::Prefab<T>[names.Length];
		for (int i = 0; i < array.Length; i++)
		{
			string text = names[i];
			GameObject gameObject = manager.FindPrefab(text);
			T component = gameObject.GetComponent<T>();
			array[i] = new global::Prefab<T>(text, gameObject, component, manager, attribute);
		}
		return array;
	}

	// Token: 0x060018B8 RID: 6328 RVA: 0x0008B208 File Offset: 0x00089408
	public static global::Prefab LoadRandom(string folder, ref uint seed, global::GameManager manager = null, global::PrefabAttribute.Library attribute = null, bool useProbabilities = true)
	{
		if (string.IsNullOrEmpty(folder))
		{
			return null;
		}
		if (manager == null)
		{
			manager = global::Prefab.DefaultManager;
		}
		if (attribute == null)
		{
			attribute = global::Prefab.DefaultAttribute;
		}
		string[] array = global::Prefab.FindPrefabNames(folder, useProbabilities);
		if (array.Length == 0)
		{
			return null;
		}
		string text = array[SeedRandom.Range(ref seed, 0, array.Length)];
		GameObject prefab = manager.FindPrefab(text);
		return new global::Prefab(text, prefab, manager, attribute);
	}

	// Token: 0x060018B9 RID: 6329 RVA: 0x0008B270 File Offset: 0x00089470
	public static global::Prefab<T> LoadRandom<T>(string folder, ref uint seed, global::GameManager manager = null, global::PrefabAttribute.Library attribute = null, bool useProbabilities = true) where T : Component
	{
		if (string.IsNullOrEmpty(folder))
		{
			return null;
		}
		if (manager == null)
		{
			manager = global::Prefab.DefaultManager;
		}
		if (attribute == null)
		{
			attribute = global::Prefab.DefaultAttribute;
		}
		string[] array = global::Prefab.FindPrefabNames(folder, useProbabilities);
		if (array.Length == 0)
		{
			return null;
		}
		string text = array[SeedRandom.Range(ref seed, 0, array.Length)];
		GameObject gameObject = manager.FindPrefab(text);
		T component = gameObject.GetComponent<T>();
		return new global::Prefab<T>(text, gameObject, component, manager, attribute);
	}

	// Token: 0x170001AD RID: 429
	// (get) Token: 0x060018BA RID: 6330 RVA: 0x0008B2E0 File Offset: 0x000894E0
	public static global::PrefabAttribute.Library DefaultAttribute
	{
		get
		{
			return global::PrefabAttribute.server;
		}
	}

	// Token: 0x170001AE RID: 430
	// (get) Token: 0x060018BB RID: 6331 RVA: 0x0008B2E8 File Offset: 0x000894E8
	public static global::GameManager DefaultManager
	{
		get
		{
			return global::GameManager.server;
		}
	}

	// Token: 0x060018BC RID: 6332 RVA: 0x0008B2F0 File Offset: 0x000894F0
	private static string[] FindPrefabNames(string strPrefab, bool useProbabilities = false)
	{
		strPrefab = strPrefab.TrimEnd(new char[]
		{
			'/'
		}).ToLower();
		GameObject[] array = global::FileSystem.LoadPrefabs(strPrefab + "/");
		List<string> list = new List<string>(array.Length);
		foreach (GameObject gameObject in array)
		{
			string item = strPrefab + "/" + gameObject.name.ToLower() + ".prefab";
			if (!useProbabilities)
			{
				list.Add(item);
			}
			else
			{
				global::PrefabParameters component = gameObject.GetComponent<global::PrefabParameters>();
				int num = (!component) ? 1 : component.Count;
				for (int j = 0; j < num; j++)
				{
					list.Add(item);
				}
			}
		}
		list.Sort();
		return list.ToArray();
	}

	// Token: 0x0400136F RID: 4975
	public uint ID;

	// Token: 0x04001370 RID: 4976
	public string Name;

	// Token: 0x04001371 RID: 4977
	public GameObject Object;

	// Token: 0x04001372 RID: 4978
	public global::GameManager Manager;

	// Token: 0x04001373 RID: 4979
	public global::PrefabAttribute.Library Attribute;

	// Token: 0x04001374 RID: 4980
	public global::PrefabParameters Parameters;
}
