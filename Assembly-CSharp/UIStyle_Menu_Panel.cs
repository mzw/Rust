﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000706 RID: 1798
public class UIStyle_Menu_Panel : MonoBehaviour, IClientComponent
{
	// Token: 0x0600223C RID: 8764 RVA: 0x000C0440 File Offset: 0x000BE640
	private void OnValidate()
	{
		base.GetComponent<Image>().color = new Color32(29, 32, 31, byte.MaxValue);
	}

	// Token: 0x04001EBD RID: 7869
	public bool toggle;
}
