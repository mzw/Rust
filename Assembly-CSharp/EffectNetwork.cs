﻿using System;
using Network;
using Network.Visibility;
using UnityEngine;

// Token: 0x0200031E RID: 798
public static class EffectNetwork
{
	// Token: 0x0600138D RID: 5005 RVA: 0x00072E70 File Offset: 0x00071070
	public static void Send(global::Effect effect)
	{
		if (Net.sv == null)
		{
			return;
		}
		if (!Net.sv.IsConnected())
		{
			return;
		}
		using (TimeWarning.New("EffectNetwork.Send", 0.1f))
		{
			if (!string.IsNullOrEmpty(effect.pooledString))
			{
				effect.pooledstringid = global::StringPool.Get(effect.pooledString);
			}
			if (effect.pooledstringid == 0u)
			{
				Debug.Log("String ID is 0 - unknown effect " + effect.pooledString);
			}
			else if (effect.broadcast)
			{
				if (Net.sv.write.Start())
				{
					Net.sv.write.PacketID(13);
					effect.WriteToStream(Net.sv.write);
					Net.sv.write.Send(new SendInfo(Net.sv.connections));
				}
			}
			else
			{
				Group group;
				if (effect.entity > 0u)
				{
					global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find(effect.entity) as global::BaseEntity;
					if (!baseEntity.IsValid())
					{
						return;
					}
					group = baseEntity.net.group;
				}
				else
				{
					group = Net.sv.visibility.GetGroup(effect.worldPos);
				}
				if (group != null)
				{
					Net.sv.write.Start();
					Net.sv.write.PacketID(13);
					effect.WriteToStream(Net.sv.write);
					Net.sv.write.Send(new SendInfo(group.subscribers));
				}
			}
		}
	}

	// Token: 0x0600138E RID: 5006 RVA: 0x00073030 File Offset: 0x00071230
	public static void Send(global::Effect effect, Connection target)
	{
		effect.pooledstringid = global::StringPool.Get(effect.pooledString);
		if (effect.pooledstringid == 0u)
		{
			Debug.LogWarning("EffectNetwork.Send - unpooled effect name: " + effect.pooledString);
			return;
		}
		Net.sv.write.Start();
		Net.sv.write.PacketID(13);
		effect.WriteToStream(Net.sv.write);
		Net.sv.write.Send(new SendInfo(target));
	}
}
