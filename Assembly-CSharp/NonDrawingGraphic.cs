﻿using System;
using UnityEngine.UI;

// Token: 0x0200072E RID: 1838
public class NonDrawingGraphic : Graphic
{
	// Token: 0x060022AB RID: 8875 RVA: 0x000C145C File Offset: 0x000BF65C
	public override void SetMaterialDirty()
	{
	}

	// Token: 0x060022AC RID: 8876 RVA: 0x000C1460 File Offset: 0x000BF660
	public override void SetVerticesDirty()
	{
	}

	// Token: 0x060022AD RID: 8877 RVA: 0x000C1464 File Offset: 0x000BF664
	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
	}
}
