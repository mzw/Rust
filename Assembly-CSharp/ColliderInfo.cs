﻿using System;
using UnityEngine;

// Token: 0x02000406 RID: 1030
public class ColliderInfo : MonoBehaviour
{
	// Token: 0x060017CA RID: 6090 RVA: 0x0008833C File Offset: 0x0008653C
	public bool HasFlag(global::ColliderInfo.Flags f)
	{
		return (this.flags & f) == f;
	}

	// Token: 0x060017CB RID: 6091 RVA: 0x0008834C File Offset: 0x0008654C
	public void SetFlag(global::ColliderInfo.Flags f, bool b)
	{
		if (b)
		{
			this.flags |= f;
		}
		else
		{
			this.flags &= ~f;
		}
	}

	// Token: 0x060017CC RID: 6092 RVA: 0x00088378 File Offset: 0x00086578
	public bool Filter(global::HitTest info)
	{
		switch (info.type)
		{
		case global::HitTest.Type.ProjectileEffect:
		case global::HitTest.Type.Projectile:
			if ((this.flags & global::ColliderInfo.Flags.Shootable) == (global::ColliderInfo.Flags)0)
			{
				return false;
			}
			break;
		case global::HitTest.Type.MeleeAttack:
			if ((this.flags & global::ColliderInfo.Flags.Melee) == (global::ColliderInfo.Flags)0)
			{
				return false;
			}
			break;
		case global::HitTest.Type.Use:
			if ((this.flags & global::ColliderInfo.Flags.Usable) == (global::ColliderInfo.Flags)0)
			{
				return false;
			}
			break;
		}
		return true;
	}

	// Token: 0x0400124C RID: 4684
	public const global::ColliderInfo.Flags FlagsNone = (global::ColliderInfo.Flags)0;

	// Token: 0x0400124D RID: 4685
	public const global::ColliderInfo.Flags FlagsEverything = (global::ColliderInfo.Flags)(-1);

	// Token: 0x0400124E RID: 4686
	public const global::ColliderInfo.Flags FlagsDefault = global::ColliderInfo.Flags.Usable | global::ColliderInfo.Flags.Shootable | global::ColliderInfo.Flags.Melee | global::ColliderInfo.Flags.Opaque;

	// Token: 0x0400124F RID: 4687
	[InspectorFlags]
	public global::ColliderInfo.Flags flags = global::ColliderInfo.Flags.Usable | global::ColliderInfo.Flags.Shootable | global::ColliderInfo.Flags.Melee | global::ColliderInfo.Flags.Opaque;

	// Token: 0x02000407 RID: 1031
	[Flags]
	public enum Flags
	{
		// Token: 0x04001251 RID: 4689
		Usable = 1,
		// Token: 0x04001252 RID: 4690
		Shootable = 2,
		// Token: 0x04001253 RID: 4691
		Melee = 4,
		// Token: 0x04001254 RID: 4692
		Opaque = 8,
		// Token: 0x04001255 RID: 4693
		Airflow = 16
	}
}
