﻿using System;
using UnityEngine;

// Token: 0x020004D6 RID: 1238
public class ItemModActionContainerChange : global::ItemMod
{
	// Token: 0x06001AB1 RID: 6833 RVA: 0x0009598C File Offset: 0x00093B8C
	public override void OnParentChanged(global::Item item)
	{
		if (!item.isServer)
		{
			return;
		}
		global::BasePlayer ownerPlayer = item.GetOwnerPlayer();
		foreach (global::ItemMod itemMod in this.actions)
		{
			if (itemMod.CanDoAction(item, ownerPlayer))
			{
				itemMod.DoAction(item, ownerPlayer);
			}
		}
	}

	// Token: 0x06001AB2 RID: 6834 RVA: 0x000959E8 File Offset: 0x00093BE8
	private void OnValidate()
	{
		if (this.actions == null)
		{
			Debug.LogWarning("ItemModMenuOption: actions is null!", base.gameObject);
		}
	}

	// Token: 0x04001574 RID: 5492
	public global::ItemMod[] actions;
}
