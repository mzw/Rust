﻿using System;

// Token: 0x02000455 RID: 1109
public class ParticleCollisionLOD : global::LODComponentParticleSystem
{
	// Token: 0x04001344 RID: 4932
	[Horizontal(1, 0)]
	public global::ParticleCollisionLOD.State[] States;

	// Token: 0x02000456 RID: 1110
	public enum QualityLevel
	{
		// Token: 0x04001346 RID: 4934
		Disabled = -1,
		// Token: 0x04001347 RID: 4935
		HighQuality,
		// Token: 0x04001348 RID: 4936
		MediumQuality,
		// Token: 0x04001349 RID: 4937
		LowQuality
	}

	// Token: 0x02000457 RID: 1111
	[Serializable]
	public class State
	{
		// Token: 0x0400134A RID: 4938
		public float distance;

		// Token: 0x0400134B RID: 4939
		public global::ParticleCollisionLOD.QualityLevel quality = global::ParticleCollisionLOD.QualityLevel.Disabled;
	}
}
