﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C9 RID: 1737
public class UIInventory : SingletonComponent<global::UIInventory>
{
	// Token: 0x04001D6E RID: 7534
	public Text PlayerName;

	// Token: 0x04001D6F RID: 7535
	public static bool isOpen;

	// Token: 0x04001D70 RID: 7536
	public static float LastOpened;

	// Token: 0x04001D71 RID: 7537
	public VerticalLayoutGroup rightContents;

	// Token: 0x04001D72 RID: 7538
	public GameObject QuickCraft;
}
