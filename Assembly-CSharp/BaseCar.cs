﻿using System;
using UnityEngine;

// Token: 0x020003E5 RID: 997
public class BaseCar : global::BaseWheeledVehicle
{
	// Token: 0x06001731 RID: 5937 RVA: 0x00085338 File Offset: 0x00083538
	public override float MaxVelocity()
	{
		return 50f;
	}

	// Token: 0x06001732 RID: 5938 RVA: 0x00085340 File Offset: 0x00083540
	public override Vector3 EyePositionForPlayer(global::BasePlayer player)
	{
		if (player.GetMounted() == this)
		{
			return this.driverEye.transform.position;
		}
		return Vector3.zero;
	}

	// Token: 0x06001733 RID: 5939 RVA: 0x0008536C File Offset: 0x0008356C
	public override float GetComfort()
	{
		return 0f;
	}

	// Token: 0x06001734 RID: 5940 RVA: 0x00085374 File Offset: 0x00083574
	public override void ServerInit()
	{
		if (base.isClient)
		{
			return;
		}
		base.ServerInit();
		this.myRigidBody = base.GetComponent<Rigidbody>();
		this.myRigidBody.centerOfMass = this.centerOfMass.localPosition;
		this.myRigidBody.isKinematic = false;
		if (global::BaseCar.chairtest)
		{
			this.SpawnChairTest();
		}
	}

	// Token: 0x06001735 RID: 5941 RVA: 0x000853D4 File Offset: 0x000835D4
	public void SpawnChairTest()
	{
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.chairRef.resourcePath, this.chairAnchorTest.transform.localPosition, default(Quaternion), true);
		baseEntity.Spawn();
		global::DestroyOnGroundMissing component = baseEntity.GetComponent<global::DestroyOnGroundMissing>();
		if (component != null)
		{
			component.enabled = false;
		}
		MeshCollider component2 = baseEntity.GetComponent<MeshCollider>();
		if (component2)
		{
			component2.convex = true;
		}
		baseEntity.SetParent(this, 0u);
	}

	// Token: 0x06001736 RID: 5942 RVA: 0x00085454 File Offset: 0x00083654
	public new void FixedUpdate()
	{
		base.FixedUpdate();
		if (base.isClient)
		{
			return;
		}
		if (!base.IsMounted())
		{
			this.NoDriverInput();
		}
		this.ConvertInputToThrottle();
		this.DoSteering();
		this.ApplyForceAtWheels();
		base.SetFlag(global::BaseEntity.Flags.Reserved1, base.IsMounted(), false);
		base.SetFlag(global::BaseEntity.Flags.Reserved2, base.IsMounted() && this.lightsOn, false);
	}

	// Token: 0x06001737 RID: 5943 RVA: 0x000854C8 File Offset: 0x000836C8
	private void DoSteering()
	{
		foreach (global::BaseWheeledVehicle.VehicleWheel vehicleWheel in this.wheels)
		{
			if (vehicleWheel.steerWheel)
			{
				vehicleWheel.wheelCollider.steerAngle = this.steering;
			}
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved4, this.steering < -2f, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved5, this.steering > 2f, false);
	}

	// Token: 0x06001738 RID: 5944 RVA: 0x00085544 File Offset: 0x00083744
	public void ConvertInputToThrottle()
	{
	}

	// Token: 0x06001739 RID: 5945 RVA: 0x00085548 File Offset: 0x00083748
	private void ApplyForceAtWheels()
	{
		if (this.myRigidBody == null)
		{
			return;
		}
		Vector3 velocity = this.myRigidBody.velocity;
		float num = velocity.magnitude * Vector3.Dot(velocity.normalized, base.transform.forward);
		float num2 = this.brakePedal;
		float num3 = this.gasPedal;
		if (num > 0f && num3 < 0f)
		{
			num2 = 100f;
		}
		else if (num < 0f && num3 > 0f)
		{
			num2 = 100f;
		}
		foreach (global::BaseWheeledVehicle.VehicleWheel vehicleWheel in this.wheels)
		{
			if (vehicleWheel.wheelCollider.isGrounded)
			{
				if (vehicleWheel.powerWheel)
				{
					vehicleWheel.wheelCollider.motorTorque = num3 * this.motorForceConstant;
				}
				if (vehicleWheel.brakeWheel)
				{
					vehicleWheel.wheelCollider.brakeTorque = num2 * this.brakeForceConstant;
				}
			}
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved3, num2 >= 100f && base.IsMounted(), false);
	}

	// Token: 0x0600173A RID: 5946 RVA: 0x00085678 File Offset: 0x00083878
	public void NoDriverInput()
	{
		if (global::BaseCar.chairtest)
		{
			this.gasPedal = Mathf.Sin(Time.time) * 50f;
		}
		else
		{
			this.gasPedal = 0f;
			this.brakePedal = Mathf.Lerp(this.brakePedal, 100f, Time.deltaTime * this.GasLerpTime / 5f);
		}
	}

	// Token: 0x0600173B RID: 5947 RVA: 0x000856E0 File Offset: 0x000838E0
	public override void PlayerServerInput(global::InputState inputState, global::BasePlayer player)
	{
		if (player != this._mounted)
		{
			return;
		}
		if (inputState.IsDown(global::BUTTON.FORWARD))
		{
			this.gasPedal = 100f;
			this.brakePedal = 0f;
		}
		else if (inputState.IsDown(global::BUTTON.BACKWARD))
		{
			this.gasPedal = -30f;
			this.brakePedal = 0f;
		}
		else
		{
			this.gasPedal = 0f;
			this.brakePedal = 30f;
		}
		if (inputState.IsDown(global::BUTTON.LEFT))
		{
			this.steering = -60f;
		}
		else if (inputState.IsDown(global::BUTTON.RIGHT))
		{
			this.steering = 60f;
		}
		else
		{
			this.steering = 0f;
		}
	}

	// Token: 0x0600173C RID: 5948 RVA: 0x000857A8 File Offset: 0x000839A8
	public void LightToggle()
	{
		this.lightsOn = !this.lightsOn;
	}

	// Token: 0x040011D2 RID: 4562
	public float brakePedal;

	// Token: 0x040011D3 RID: 4563
	public float gasPedal;

	// Token: 0x040011D4 RID: 4564
	public float steering;

	// Token: 0x040011D5 RID: 4565
	public Transform centerOfMass;

	// Token: 0x040011D6 RID: 4566
	public Transform steeringWheel;

	// Token: 0x040011D7 RID: 4567
	public float motorForceConstant = 150f;

	// Token: 0x040011D8 RID: 4568
	public float brakeForceConstant = 500f;

	// Token: 0x040011D9 RID: 4569
	public float GasLerpTime = 20f;

	// Token: 0x040011DA RID: 4570
	public float SteeringLerpTime = 20f;

	// Token: 0x040011DB RID: 4571
	public Transform driverEye;

	// Token: 0x040011DC RID: 4572
	public Rigidbody myRigidBody;

	// Token: 0x040011DD RID: 4573
	private static bool chairtest;

	// Token: 0x040011DE RID: 4574
	public global::GameObjectRef chairRef;

	// Token: 0x040011DF RID: 4575
	public Transform chairAnchorTest;

	// Token: 0x040011E0 RID: 4576
	private float throttle;

	// Token: 0x040011E1 RID: 4577
	private float brake;

	// Token: 0x040011E2 RID: 4578
	private bool lightsOn = true;
}
