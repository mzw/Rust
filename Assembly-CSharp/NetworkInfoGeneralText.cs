﻿using System;
using Network;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020002F3 RID: 755
public class NetworkInfoGeneralText : MonoBehaviour
{
	// Token: 0x06001324 RID: 4900 RVA: 0x000708C4 File Offset: 0x0006EAC4
	private void Update()
	{
		this.UpdateText();
	}

	// Token: 0x06001325 RID: 4901 RVA: 0x000708CC File Offset: 0x0006EACC
	private void UpdateText()
	{
		string str = string.Empty;
		if (Net.sv != null)
		{
			str += "Server\n";
			str += Net.sv.GetDebug(null);
			str += "\n";
		}
		this.text.text = str;
	}

	// Token: 0x06001326 RID: 4902 RVA: 0x00070920 File Offset: 0x0006EB20
	private static string ChannelStat(int window, int left)
	{
		return string.Format("{0}/{1}", left, window);
	}

	// Token: 0x04000DD6 RID: 3542
	public Text text;
}
