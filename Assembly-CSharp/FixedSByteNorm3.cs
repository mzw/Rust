﻿using System;
using UnityEngine;

// Token: 0x020007AA RID: 1962
public struct FixedSByteNorm3
{
	// Token: 0x0600248C RID: 9356 RVA: 0x000C95AC File Offset: 0x000C77AC
	public FixedSByteNorm3(Vector3 vec)
	{
		this.x = (sbyte)(vec.x * 128f);
		this.y = (sbyte)(vec.y * 128f);
		this.z = (sbyte)(vec.z * 128f);
	}

	// Token: 0x0600248D RID: 9357 RVA: 0x000C95EC File Offset: 0x000C77EC
	public static explicit operator Vector3(global::FixedSByteNorm3 vec)
	{
		return new Vector3((float)vec.x * 0.0078125f, (float)vec.y * 0.0078125f, (float)vec.z * 0.0078125f);
	}

	// Token: 0x04001FF8 RID: 8184
	private const int FracBits = 7;

	// Token: 0x04001FF9 RID: 8185
	private const float MaxFrac = 128f;

	// Token: 0x04001FFA RID: 8186
	private const float RcpMaxFrac = 0.0078125f;

	// Token: 0x04001FFB RID: 8187
	public sbyte x;

	// Token: 0x04001FFC RID: 8188
	public sbyte y;

	// Token: 0x04001FFD RID: 8189
	public sbyte z;
}
