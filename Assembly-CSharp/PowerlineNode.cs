﻿using System;
using UnityEngine;

// Token: 0x02000552 RID: 1362
public class PowerlineNode : MonoBehaviour
{
	// Token: 0x06001C9E RID: 7326 RVA: 0x000A0540 File Offset: 0x0009E740
	protected void Awake()
	{
		if (global::TerrainMeta.Path)
		{
			global::TerrainMeta.Path.AddWire(this);
		}
	}

	// Token: 0x040017BA RID: 6074
	public Material WireMaterial;

	// Token: 0x040017BB RID: 6075
	public float MaxDistance = 50f;
}
