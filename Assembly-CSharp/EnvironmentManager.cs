﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200041C RID: 1052
public class EnvironmentManager : SingletonComponent<global::EnvironmentManager>
{
	// Token: 0x0600180A RID: 6154 RVA: 0x00088F84 File Offset: 0x00087184
	public static global::EnvironmentType Get(OBB obb)
	{
		global::EnvironmentType environmentType = (global::EnvironmentType)0;
		List<global::EnvironmentVolume> list = Pool.GetList<global::EnvironmentVolume>();
		global::GamePhysics.OverlapOBB<global::EnvironmentVolume>(obb, list, 262144, 2);
		for (int i = 0; i < list.Count; i++)
		{
			environmentType |= list[i].Type;
		}
		Pool.FreeList<global::EnvironmentVolume>(ref list);
		return environmentType;
	}

	// Token: 0x0600180B RID: 6155 RVA: 0x00088FD4 File Offset: 0x000871D4
	public static global::EnvironmentType Get(Vector3 pos)
	{
		global::EnvironmentType environmentType = (global::EnvironmentType)0;
		List<global::EnvironmentVolume> list = Pool.GetList<global::EnvironmentVolume>();
		global::GamePhysics.OverlapSphere<global::EnvironmentVolume>(pos, 0.01f, list, 262144, 2);
		for (int i = 0; i < list.Count; i++)
		{
			environmentType |= list[i].Type;
		}
		Pool.FreeList<global::EnvironmentVolume>(ref list);
		return environmentType;
	}

	// Token: 0x0600180C RID: 6156 RVA: 0x0008902C File Offset: 0x0008722C
	public static bool Check(OBB obb, global::EnvironmentType type)
	{
		return (global::EnvironmentManager.Get(obb) & type) != (global::EnvironmentType)0;
	}

	// Token: 0x0600180D RID: 6157 RVA: 0x0008903C File Offset: 0x0008723C
	public static bool Check(Vector3 pos, global::EnvironmentType type)
	{
		return (global::EnvironmentManager.Get(pos) & type) != (global::EnvironmentType)0;
	}
}
