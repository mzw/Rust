﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x020006F6 RID: 1782
[ExecuteInEditMode]
public class CameraEx : MonoBehaviour
{
	// Token: 0x04001E57 RID: 7767
	public bool overrideAmbientLight;

	// Token: 0x04001E58 RID: 7768
	public AmbientMode ambientMode;

	// Token: 0x04001E59 RID: 7769
	public Color ambientGroundColor;

	// Token: 0x04001E5A RID: 7770
	public Color ambientEquatorColor;

	// Token: 0x04001E5B RID: 7771
	public Color ambientLight;

	// Token: 0x04001E5C RID: 7772
	public float ambientIntensity;

	// Token: 0x04001E5D RID: 7773
	internal Color old_ambientLight;

	// Token: 0x04001E5E RID: 7774
	internal Color old_ambientGroundColor;

	// Token: 0x04001E5F RID: 7775
	internal Color old_ambientEquatorColor;

	// Token: 0x04001E60 RID: 7776
	internal float old_ambientIntensity;

	// Token: 0x04001E61 RID: 7777
	internal AmbientMode old_ambientMode;

	// Token: 0x04001E62 RID: 7778
	public float aspect;
}
