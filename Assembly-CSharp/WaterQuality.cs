﻿using System;

// Token: 0x020005E6 RID: 1510
public enum WaterQuality
{
	// Token: 0x040019EE RID: 6638
	Low,
	// Token: 0x040019EF RID: 6639
	Medium,
	// Token: 0x040019F0 RID: 6640
	High
}
