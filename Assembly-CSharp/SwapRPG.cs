﻿using System;
using UnityEngine;

// Token: 0x020007C1 RID: 1985
public class SwapRPG : MonoBehaviour
{
	// Token: 0x060024D6 RID: 9430 RVA: 0x000CA808 File Offset: 0x000C8A08
	public void SelectRPGType(int iType)
	{
		foreach (GameObject gameObject in this.rpgModels)
		{
			gameObject.SetActive(false);
		}
		this.rpgModels[iType].SetActive(true);
	}

	// Token: 0x060024D7 RID: 9431 RVA: 0x000CA84C File Offset: 0x000C8A4C
	public void UpdateAmmoType(global::ItemDefinition ammoType)
	{
		if (this.curAmmoType == ammoType.shortname)
		{
			return;
		}
		this.curAmmoType = ammoType.shortname;
		string text = this.curAmmoType;
		if (text != null && !(text == "ammo.rocket.basic"))
		{
			if (text == "ammo.rocket.fire")
			{
				this.SelectRPGType(1);
				return;
			}
			if (text == "ammo.rocket.hv")
			{
				this.SelectRPGType(2);
				return;
			}
			if (text == "ammo.rocket.smoke")
			{
				this.SelectRPGType(3);
				return;
			}
		}
		this.SelectRPGType(0);
	}

	// Token: 0x060024D8 RID: 9432 RVA: 0x000CA8F0 File Offset: 0x000C8AF0
	private void Start()
	{
	}

	// Token: 0x04002054 RID: 8276
	public GameObject[] rpgModels;

	// Token: 0x04002055 RID: 8277
	[NonSerialized]
	private string curAmmoType = string.Empty;

	// Token: 0x020007C2 RID: 1986
	public enum RPGType
	{
		// Token: 0x04002057 RID: 8279
		One,
		// Token: 0x04002058 RID: 8280
		Two,
		// Token: 0x04002059 RID: 8281
		Three,
		// Token: 0x0400205A RID: 8282
		Four
	}
}
