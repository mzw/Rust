﻿using System;
using UnityEngine;

// Token: 0x02000302 RID: 770
public class DevEnableDisable : global::DevControlsTab
{
	// Token: 0x04000E02 RID: 3586
	public GameObject[] Objects;

	// Token: 0x04000E03 RID: 3587
	public string CookieName = "Cookie";

	// Token: 0x04000E04 RID: 3588
	public string TabName = "Scene";
}
