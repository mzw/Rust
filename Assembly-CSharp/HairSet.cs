﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200063F RID: 1599
[CreateAssetMenu(menuName = "Rust/Hair Set")]
public class HairSet : ScriptableObject
{
	// Token: 0x06002056 RID: 8278 RVA: 0x000B9180 File Offset: 0x000B7380
	public void Process(global::PlayerModelHair playerModelHair, global::HairDye dye, MaterialPropertyBlock block)
	{
		List<SkinnedMeshRenderer> list = Pool.GetList<SkinnedMeshRenderer>();
		playerModelHair.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true, list);
		foreach (SkinnedMeshRenderer skinnedMeshRenderer in list)
		{
			if (!(skinnedMeshRenderer.sharedMesh == null))
			{
				if (!(skinnedMeshRenderer.sharedMaterial == null))
				{
					string name = skinnedMeshRenderer.sharedMesh.name;
					string name2 = skinnedMeshRenderer.sharedMaterial.name;
					if (!skinnedMeshRenderer.gameObject.activeSelf)
					{
						skinnedMeshRenderer.gameObject.SetActive(true);
					}
					for (int i = 0; i < this.MeshReplacements.Length; i++)
					{
						if (this.MeshReplacements[i].Test(name))
						{
							SkinnedMeshRenderer replace = this.MeshReplacements[i].Replace;
							if (replace == null)
							{
								skinnedMeshRenderer.gameObject.SetActive(false);
							}
							else
							{
								skinnedMeshRenderer.sharedMesh = replace.sharedMesh;
								skinnedMeshRenderer.rootBone = replace.rootBone;
								skinnedMeshRenderer.bones = this.MeshReplacements[i].GetBones();
							}
						}
					}
					global::PlayerModelHair.RendererMaterials rendererMaterials;
					if (playerModelHair.Materials.TryGetValue(skinnedMeshRenderer, out rendererMaterials))
					{
						Array.Copy(rendererMaterials.original, rendererMaterials.replacement, rendererMaterials.original.Length);
						for (int j = 0; j < rendererMaterials.original.Length; j++)
						{
							for (int k = 0; k < this.MaterialReplacements.Length; k++)
							{
								if (this.MaterialReplacements[k].Test(rendererMaterials.names[j]))
								{
									rendererMaterials.replacement[j] = this.MaterialReplacements[k].Replace;
								}
							}
						}
						skinnedMeshRenderer.sharedMaterials = rendererMaterials.replacement;
					}
					else
					{
						Debug.LogWarning("[HairSet.Process] Missing cached renderer materials in " + playerModelHair.name);
					}
					if (dye != null && skinnedMeshRenderer.gameObject.activeSelf)
					{
						dye.Apply(block);
					}
				}
			}
		}
		Pool.FreeList<SkinnedMeshRenderer>(ref list);
	}

	// Token: 0x06002057 RID: 8279 RVA: 0x000B93C8 File Offset: 0x000B75C8
	public void ProcessMorphs(GameObject obj, int blendShapeIndex = -1)
	{
	}

	// Token: 0x04001B3B RID: 6971
	public global::HairSet.MeshReplace[] MeshReplacements;

	// Token: 0x04001B3C RID: 6972
	public global::HairSet.MaterialReplace[] MaterialReplacements;

	// Token: 0x02000640 RID: 1600
	[Serializable]
	public class MeshReplace
	{
		// Token: 0x06002059 RID: 8281 RVA: 0x000B93D4 File Offset: 0x000B75D4
		public bool Test(string materialName)
		{
			return this.FindName == materialName;
		}

		// Token: 0x0600205A RID: 8282 RVA: 0x000B93E4 File Offset: 0x000B75E4
		public Transform[] GetBones()
		{
			if (this.bones == null)
			{
				this.bones = this.Replace.bones;
			}
			return this.bones;
		}

		// Token: 0x04001B3D RID: 6973
		[HideInInspector]
		public string FindName;

		// Token: 0x04001B3E RID: 6974
		public Mesh Find;

		// Token: 0x04001B3F RID: 6975
		public SkinnedMeshRenderer Replace;

		// Token: 0x04001B40 RID: 6976
		private Transform[] bones;
	}

	// Token: 0x02000641 RID: 1601
	[Serializable]
	public class MaterialReplace
	{
		// Token: 0x0600205C RID: 8284 RVA: 0x000B9410 File Offset: 0x000B7610
		public bool Test(string materialName)
		{
			return this.FindName == materialName;
		}

		// Token: 0x04001B41 RID: 6977
		[HideInInspector]
		public string FindName;

		// Token: 0x04001B42 RID: 6978
		public Material Find;

		// Token: 0x04001B43 RID: 6979
		public Material Replace;
	}
}
