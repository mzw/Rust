﻿using System;

// Token: 0x020003CE RID: 974
public class PlayerStatistics
{
	// Token: 0x060016C1 RID: 5825 RVA: 0x000836B8 File Offset: 0x000818B8
	public PlayerStatistics(global::BasePlayer player)
	{
		this.steam = new global::SteamStatistics(player);
		this.server = new global::ServerStatistics(player);
		this.combat = new global::CombatLog(player);
	}

	// Token: 0x060016C2 RID: 5826 RVA: 0x000836E4 File Offset: 0x000818E4
	public void Init()
	{
		this.steam.Init();
		this.server.Init();
		this.combat.Init();
	}

	// Token: 0x060016C3 RID: 5827 RVA: 0x00083708 File Offset: 0x00081908
	public void Save()
	{
		this.steam.Save();
		this.server.Save();
		this.combat.Save();
	}

	// Token: 0x060016C4 RID: 5828 RVA: 0x0008372C File Offset: 0x0008192C
	public void Add(string name, int val, global::Stats stats = global::Stats.Steam)
	{
		if ((stats & global::Stats.Steam) != (global::Stats)0)
		{
			this.steam.Add(name, val);
		}
		if ((stats & global::Stats.Server) != (global::Stats)0)
		{
			this.server.Add(name, val);
		}
	}

	// Token: 0x0400118E RID: 4494
	public global::SteamStatistics steam;

	// Token: 0x0400118F RID: 4495
	public global::ServerStatistics server;

	// Token: 0x04001190 RID: 4496
	public global::CombatLog combat;
}
