﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006A3 RID: 1699
public class CraftingNotice : MonoBehaviour
{
	// Token: 0x04001CC3 RID: 7363
	public RectTransform rotatingIcon;

	// Token: 0x04001CC4 RID: 7364
	public CanvasGroup canvasGroup;

	// Token: 0x04001CC5 RID: 7365
	public Text itemName;

	// Token: 0x04001CC6 RID: 7366
	public Text craftSeconds;
}
