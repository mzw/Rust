﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020000C8 RID: 200
public class BasePath : MonoBehaviour
{
	// Token: 0x06000AB9 RID: 2745 RVA: 0x00048F3C File Offset: 0x0004713C
	public void Start()
	{
	}

	// Token: 0x06000ABA RID: 2746 RVA: 0x00048F40 File Offset: 0x00047140
	public void GetNodesNear(Vector3 point, ref List<global::BasePathNode> nearNodes, float dist = 10f)
	{
		foreach (global::BasePathNode basePathNode in this.nodes)
		{
			float num = Vector3Ex.Distance2D(point, basePathNode.transform.position);
			if (num <= dist)
			{
				nearNodes.Add(basePathNode);
			}
		}
	}

	// Token: 0x06000ABB RID: 2747 RVA: 0x00048FB8 File Offset: 0x000471B8
	public global::BasePathNode GetClosestToPoint(Vector3 point)
	{
		global::BasePathNode result = this.nodes[0];
		float num = float.PositiveInfinity;
		foreach (global::BasePathNode basePathNode in this.nodes)
		{
			float num2 = Vector3.Distance(point, basePathNode.transform.position);
			if (num2 < num)
			{
				num = num2;
				result = basePathNode;
			}
		}
		return result;
	}

	// Token: 0x06000ABC RID: 2748 RVA: 0x00049044 File Offset: 0x00047244
	public global::PathInterestNode GetRandomInterestNodeAwayFrom(Vector3 from, float dist = 10f)
	{
		global::PathInterestNode pathInterestNode = null;
		int num = 0;
		while (pathInterestNode == null && num < 20)
		{
			pathInterestNode = this.interestZones[Random.Range(0, this.interestZones.Count)];
			if (Vector3.Distance(pathInterestNode.transform.position, from) >= 10f)
			{
				break;
			}
			pathInterestNode = null;
			num++;
		}
		if (pathInterestNode == null)
		{
			pathInterestNode = this.interestZones[0];
		}
		return pathInterestNode;
	}

	// Token: 0x0400057C RID: 1404
	public List<global::BasePathNode> nodes;

	// Token: 0x0400057D RID: 1405
	public List<global::PathInterestNode> interestZones;

	// Token: 0x0400057E RID: 1406
	public List<global::PathSpeedZone> speedZones;
}
