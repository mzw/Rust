﻿using System;
using Facepunch;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;

// Token: 0x0200038F RID: 911
public class MiningQuarry : global::BaseResourceExtractor
{
	// Token: 0x060015A1 RID: 5537 RVA: 0x0007C3E0 File Offset: 0x0007A5E0
	public bool IsEngineOn()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x060015A2 RID: 5538 RVA: 0x0007C3EC File Offset: 0x0007A5EC
	private void SetOn(bool isOn)
	{
		base.SetFlag(global::BaseEntity.Flags.On, isOn, false);
		this.engineSwitchPrefab.instance.SetFlag(global::BaseEntity.Flags.On, isOn, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		this.engineSwitchPrefab.instance.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		if (isOn)
		{
			base.InvokeRepeating(new Action(this.ProcessResources), this.processRate, this.processRate);
			Interface.CallHook("OnQuarryEnabled", new object[]
			{
				this
			});
		}
		else
		{
			base.CancelInvoke(new Action(this.ProcessResources));
		}
	}

	// Token: 0x060015A3 RID: 5539 RVA: 0x0007C480 File Offset: 0x0007A680
	public void EngineSwitch(bool isOn)
	{
		if (isOn && this.FuelCheck())
		{
			this.SetOn(true);
		}
		else
		{
			this.SetOn(false);
		}
	}

	// Token: 0x060015A4 RID: 5540 RVA: 0x0007C4A8 File Offset: 0x0007A6A8
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.isStatic)
		{
			this.UpdateStaticDeposit();
		}
		else
		{
			global::ResourceDepositManager.ResourceDeposit orCreate = global::ResourceDepositManager.GetOrCreate(base.transform.position);
			this._linkedDeposit = orCreate;
		}
		this.SpawnChildEntities();
		this.engineSwitchPrefab.instance.SetFlag(global::BaseEntity.Flags.On, base.HasFlag(global::BaseEntity.Flags.On), false);
	}

	// Token: 0x060015A5 RID: 5541 RVA: 0x0007C508 File Offset: 0x0007A708
	public void UpdateStaticDeposit()
	{
		if (!this.isStatic)
		{
			return;
		}
		if (this._linkedDeposit == null)
		{
			this._linkedDeposit = new global::ResourceDepositManager.ResourceDeposit();
		}
		else
		{
			this._linkedDeposit._resources.Clear();
		}
		if (this.staticType == global::MiningQuarry.QuarryType.None)
		{
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("stones"), 1f, 1000, 0.3f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("metal.ore"), 1f, 1000, 5f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("sulfur.ore"), 1f, 1000, 7.5f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("hq.metal.ore"), 1f, 1000, 75f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
		}
		else if (this.staticType == global::MiningQuarry.QuarryType.Basic)
		{
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("metal.ore"), 1f, 1000, 2f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("stones"), 1f, 1000, 0.3f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
		}
		else if (this.staticType == global::MiningQuarry.QuarryType.Sulfur)
		{
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("sulfur.ore"), 1f, 1000, 2f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
		}
		else if (this.staticType == global::MiningQuarry.QuarryType.HQM)
		{
			this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("hq.metal.ore"), 1f, 1000, 30f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, false);
		}
		this._linkedDeposit.Add(global::ItemManager.FindItemDefinition("crude.oil"), 1f, 1000, 10f, global::ResourceDepositManager.ResourceDeposit.surveySpawnType.ITEM, true);
	}

	// Token: 0x060015A6 RID: 5542 RVA: 0x0007C6E0 File Offset: 0x0007A8E0
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		this.EngineSwitch(base.HasFlag(global::BaseEntity.Flags.On));
		this.UpdateStaticDeposit();
	}

	// Token: 0x060015A7 RID: 5543 RVA: 0x0007C6FC File Offset: 0x0007A8FC
	public void SpawnChildEntities()
	{
		this.engineSwitchPrefab.DoSpawn(this);
		this.hopperPrefab.DoSpawn(this);
		this.fuelStoragePrefab.DoSpawn(this);
	}

	// Token: 0x060015A8 RID: 5544 RVA: 0x0007C724 File Offset: 0x0007A924
	public void ProcessResources()
	{
		if (this._linkedDeposit == null)
		{
			return;
		}
		if (this.hopperPrefab.instance == null)
		{
			return;
		}
		foreach (global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry resourceDepositEntry in this._linkedDeposit._resources)
		{
			if (this.canExtractLiquid || !resourceDepositEntry.isLiquid)
			{
				if (this.canExtractSolid || resourceDepositEntry.isLiquid)
				{
					resourceDepositEntry.workDone += this.workToAdd;
					if (resourceDepositEntry.workDone >= resourceDepositEntry.workNeeded)
					{
						int num = Mathf.FloorToInt(resourceDepositEntry.workDone / resourceDepositEntry.workNeeded);
						resourceDepositEntry.workDone -= (float)num * resourceDepositEntry.workNeeded;
						global::Item item = global::ItemManager.Create(resourceDepositEntry.type, num, 0UL);
						Interface.CallHook("OnQuarryGather", new object[]
						{
							this,
							item
						});
						if (!item.MoveToContainer(this.hopperPrefab.instance.GetComponent<global::StorageContainer>().inventory, -1, true))
						{
							item.Remove(0f);
							this.SetOn(false);
						}
					}
				}
			}
		}
		if (!this.FuelCheck())
		{
			this.SetOn(false);
		}
	}

	// Token: 0x060015A9 RID: 5545 RVA: 0x0007C8AC File Offset: 0x0007AAAC
	public bool FuelCheck()
	{
		global::ItemContainer inventory = this.fuelStoragePrefab.instance.GetComponent<global::StorageContainer>().inventory;
		global::Item item = inventory.FindItemsByItemName("lowgradefuel");
		if (item != null && item.amount >= 1)
		{
			item.UseItem(1);
			return true;
		}
		return false;
	}

	// Token: 0x060015AA RID: 5546 RVA: 0x0007C8F8 File Offset: 0x0007AAF8
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (info.forDisk)
		{
			if (this.fuelStoragePrefab.instance == null || this.hopperPrefab.instance == null)
			{
				Debug.Log("Cannot save mining quary because children were null");
				return;
			}
			info.msg.miningQuarry = Pool.Get<ProtoBuf.MiningQuarry>();
			info.msg.miningQuarry.extractor = Pool.Get<ResourceExtractor>();
			info.msg.miningQuarry.extractor.fuelContents = this.fuelStoragePrefab.instance.GetComponent<global::StorageContainer>().inventory.Save();
			info.msg.miningQuarry.extractor.outputContents = this.hopperPrefab.instance.GetComponent<global::StorageContainer>().inventory.Save();
			info.msg.miningQuarry.staticType = (int)this.staticType;
		}
	}

	// Token: 0x060015AB RID: 5547 RVA: 0x0007C9F0 File Offset: 0x0007ABF0
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.fromDisk && info.msg.miningQuarry != null)
		{
			if (this.fuelStoragePrefab.instance == null || this.hopperPrefab.instance == null)
			{
				Debug.Log("Cannot load mining quary because children were null");
				return;
			}
			this.fuelStoragePrefab.instance.GetComponent<global::StorageContainer>().inventory.Load(info.msg.miningQuarry.extractor.fuelContents);
			this.hopperPrefab.instance.GetComponent<global::StorageContainer>().inventory.Load(info.msg.miningQuarry.extractor.outputContents);
			this.staticType = (global::MiningQuarry.QuarryType)info.msg.miningQuarry.staticType;
		}
	}

	// Token: 0x060015AC RID: 5548 RVA: 0x0007CAD0 File Offset: 0x0007ACD0
	public void Update()
	{
	}

	// Token: 0x04000FE6 RID: 4070
	public Animator beltAnimator;

	// Token: 0x04000FE7 RID: 4071
	public Renderer beltScrollRenderer;

	// Token: 0x04000FE8 RID: 4072
	public int scrollMatIndex = 3;

	// Token: 0x04000FE9 RID: 4073
	public global::SoundPlayer[] onSounds;

	// Token: 0x04000FEA RID: 4074
	public float processRate = 5f;

	// Token: 0x04000FEB RID: 4075
	public float workToAdd = 15f;

	// Token: 0x04000FEC RID: 4076
	public global::GameObjectRef bucketDropEffect;

	// Token: 0x04000FED RID: 4077
	public GameObject bucketDropTransform;

	// Token: 0x04000FEE RID: 4078
	public global::MiningQuarry.ChildPrefab engineSwitchPrefab;

	// Token: 0x04000FEF RID: 4079
	public global::MiningQuarry.ChildPrefab hopperPrefab;

	// Token: 0x04000FF0 RID: 4080
	public global::MiningQuarry.ChildPrefab fuelStoragePrefab;

	// Token: 0x04000FF1 RID: 4081
	public bool isStatic;

	// Token: 0x04000FF2 RID: 4082
	private global::ResourceDepositManager.ResourceDeposit _linkedDeposit;

	// Token: 0x04000FF3 RID: 4083
	public global::MiningQuarry.QuarryType staticType;

	// Token: 0x02000390 RID: 912
	[Serializable]
	public class ChildPrefab
	{
		// Token: 0x060015AE RID: 5550 RVA: 0x0007CADC File Offset: 0x0007ACDC
		public void DoSpawn(global::MiningQuarry owner)
		{
			if (!this.prefabToSpawn.isValid)
			{
				return;
			}
			this.instance = global::GameManager.server.CreateEntity(this.prefabToSpawn.resourcePath, this.origin.transform.localPosition, this.origin.transform.localRotation, true);
			this.instance.SetParent(owner, 0u);
			this.instance.Spawn();
		}

		// Token: 0x04000FF4 RID: 4084
		public global::GameObjectRef prefabToSpawn;

		// Token: 0x04000FF5 RID: 4085
		public GameObject origin;

		// Token: 0x04000FF6 RID: 4086
		public global::BaseEntity instance;
	}

	// Token: 0x02000391 RID: 913
	[Serializable]
	public enum QuarryType
	{
		// Token: 0x04000FF8 RID: 4088
		None,
		// Token: 0x04000FF9 RID: 4089
		Basic,
		// Token: 0x04000FFA RID: 4090
		Sulfur,
		// Token: 0x04000FFB RID: 4091
		HQM
	}
}
