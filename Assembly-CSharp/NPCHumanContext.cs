﻿using System;
using System.Collections.Generic;
using Rust.Ai;

// Token: 0x020000E3 RID: 227
public class NPCHumanContext : global::BaseNPCContext
{
	// Token: 0x06000B30 RID: 2864 RVA: 0x0004ABE4 File Offset: 0x00048DE4
	public NPCHumanContext(global::NPCPlayerApex human) : base(human)
	{
		this.sampledCoverPoints = new List<Rust.Ai.CoverPoint>();
		this.sampledCoverPointTypes = new List<Rust.Ai.CoverPoint.CoverType>();
		this.CoverSet.Setup(human);
	}

	// Token: 0x17000073 RID: 115
	// (get) Token: 0x06000B31 RID: 2865 RVA: 0x0004AC1C File Offset: 0x00048E1C
	// (set) Token: 0x06000B32 RID: 2866 RVA: 0x0004AC24 File Offset: 0x00048E24
	public global::BaseEntity LastAttacker { get; set; }

	// Token: 0x17000074 RID: 116
	// (get) Token: 0x06000B33 RID: 2867 RVA: 0x0004AC30 File Offset: 0x00048E30
	// (set) Token: 0x06000B34 RID: 2868 RVA: 0x0004AC38 File Offset: 0x00048E38
	public Rust.Ai.CoverPointVolume CurrentCoverVolume { get; set; }

	// Token: 0x17000075 RID: 117
	// (get) Token: 0x06000B35 RID: 2869 RVA: 0x0004AC44 File Offset: 0x00048E44
	// (set) Token: 0x06000B36 RID: 2870 RVA: 0x0004AC4C File Offset: 0x00048E4C
	public List<Rust.Ai.CoverPoint> sampledCoverPoints { get; private set; }

	// Token: 0x17000076 RID: 118
	// (get) Token: 0x06000B37 RID: 2871 RVA: 0x0004AC58 File Offset: 0x00048E58
	// (set) Token: 0x06000B38 RID: 2872 RVA: 0x0004AC60 File Offset: 0x00048E60
	public List<Rust.Ai.CoverPoint.CoverType> sampledCoverPointTypes { get; private set; }

	// Token: 0x17000077 RID: 119
	// (get) Token: 0x06000B39 RID: 2873 RVA: 0x0004AC6C File Offset: 0x00048E6C
	// (set) Token: 0x06000B3A RID: 2874 RVA: 0x0004AC74 File Offset: 0x00048E74
	public global::PathInterestNode CurrentPatrolPoint { get; set; }

	// Token: 0x06000B3B RID: 2875 RVA: 0x0004AC80 File Offset: 0x00048E80
	~NPCHumanContext()
	{
		this.CoverSet.Shutdown();
	}

	// Token: 0x040005F2 RID: 1522
	public global::NPCHumanContext.TacticalCoverPointSet CoverSet = new global::NPCHumanContext.TacticalCoverPointSet();

	// Token: 0x020000E4 RID: 228
	public struct TacticalCoverPoint
	{
		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000B3C RID: 2876 RVA: 0x0004ACB4 File Offset: 0x00048EB4
		// (set) Token: 0x06000B3D RID: 2877 RVA: 0x0004ACBC File Offset: 0x00048EBC
		public Rust.Ai.CoverPoint ReservedCoverPoint
		{
			get
			{
				return this.reservedCoverPoint;
			}
			set
			{
				if (this.reservedCoverPoint != null)
				{
					this.reservedCoverPoint.ReservedFor = null;
				}
				if (value == this.reservedCoverPoint && value == null)
				{
					this.Human.modelState.ducked = false;
				}
				this.reservedCoverPoint = value;
				if (this.reservedCoverPoint != null)
				{
					this.reservedCoverPoint.ReservedFor = this.Human;
				}
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000B3E RID: 2878 RVA: 0x0004AD28 File Offset: 0x00048F28
		// (set) Token: 0x06000B3F RID: 2879 RVA: 0x0004AD30 File Offset: 0x00048F30
		public Rust.Ai.CoverPoint.CoverType ActiveCoverType
		{
			get
			{
				return this.activeCoverType;
			}
			set
			{
				if (value == Rust.Ai.CoverPoint.CoverType.Full)
				{
					this.Human.modelState.ducked = false;
				}
				else if (value == Rust.Ai.CoverPoint.CoverType.Partial)
				{
					this.Human.modelState.ducked = true;
				}
				this.activeCoverType = value;
			}
		}

		// Token: 0x040005F3 RID: 1523
		public global::NPCPlayerApex Human;

		// Token: 0x040005F4 RID: 1524
		public Rust.Ai.CoverPoint reservedCoverPoint;

		// Token: 0x040005F5 RID: 1525
		public Rust.Ai.CoverPoint.CoverType activeCoverType;
	}

	// Token: 0x020000E5 RID: 229
	public class TacticalCoverPointSet
	{
		// Token: 0x06000B41 RID: 2881 RVA: 0x0004AD78 File Offset: 0x00048F78
		public void Setup(global::NPCPlayerApex human)
		{
			this.Retreat.Human = human;
			this.Flank.Human = human;
			this.Advance.Human = human;
			this.Closest.Human = human;
		}

		// Token: 0x06000B42 RID: 2882 RVA: 0x0004ADAC File Offset: 0x00048FAC
		public void Shutdown()
		{
			this.Reset();
		}

		// Token: 0x06000B43 RID: 2883 RVA: 0x0004ADB4 File Offset: 0x00048FB4
		public void Reset()
		{
			if (this.Retreat.ReservedCoverPoint != null)
			{
				this.Retreat.ReservedCoverPoint = null;
			}
			if (this.Flank.ReservedCoverPoint != null)
			{
				this.Flank.ReservedCoverPoint = null;
			}
			if (this.Advance.ReservedCoverPoint != null)
			{
				this.Advance.ReservedCoverPoint = null;
			}
			if (this.Closest.ReservedCoverPoint != null)
			{
				this.Closest.ReservedCoverPoint = null;
			}
		}

		// Token: 0x06000B44 RID: 2884 RVA: 0x0004AE34 File Offset: 0x00049034
		public void Update(Rust.Ai.CoverPoint retreat, Rust.Ai.CoverPoint flank, Rust.Ai.CoverPoint advance)
		{
			this.Retreat.ReservedCoverPoint = retreat;
			this.Flank.ReservedCoverPoint = flank;
			this.Advance.ReservedCoverPoint = advance;
			float num = float.MaxValue;
			if (retreat != null)
			{
				float sqrMagnitude = (retreat.Position - this.Retreat.Human.ServerPosition).sqrMagnitude;
				if (sqrMagnitude < num)
				{
					this.Closest.ReservedCoverPoint = retreat;
					num = sqrMagnitude;
				}
			}
			if (flank != null)
			{
				float sqrMagnitude2 = (flank.Position - this.Flank.Human.ServerPosition).sqrMagnitude;
				if (sqrMagnitude2 < num)
				{
					this.Closest.ReservedCoverPoint = flank;
					num = sqrMagnitude2;
				}
			}
			if (advance != null)
			{
				float sqrMagnitude3 = (advance.Position - this.Advance.Human.ServerPosition).sqrMagnitude;
				if (sqrMagnitude3 < num)
				{
					this.Closest.ReservedCoverPoint = advance;
				}
			}
		}

		// Token: 0x040005F6 RID: 1526
		public global::NPCHumanContext.TacticalCoverPoint Retreat;

		// Token: 0x040005F7 RID: 1527
		public global::NPCHumanContext.TacticalCoverPoint Flank;

		// Token: 0x040005F8 RID: 1528
		public global::NPCHumanContext.TacticalCoverPoint Advance;

		// Token: 0x040005F9 RID: 1529
		public global::NPCHumanContext.TacticalCoverPoint Closest;
	}
}
