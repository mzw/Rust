﻿using System;
using UnityEngine;

// Token: 0x02000613 RID: 1555
[RequireComponent(typeof(global::CommandBufferManager))]
[ExecuteInEditMode]
public class PostOpaqueDepth : SingletonComponent<global::PostOpaqueDepth>
{
	// Token: 0x1700022E RID: 558
	// (get) Token: 0x06001F6F RID: 8047 RVA: 0x000B23C8 File Offset: 0x000B05C8
	public RenderTexture PostOpaque
	{
		get
		{
			return this.postOpaqueDepth;
		}
	}

	// Token: 0x04001A79 RID: 6777
	public RenderTexture cameraDepth;

	// Token: 0x04001A7A RID: 6778
	public RenderTexture postOpaqueDepth;
}
