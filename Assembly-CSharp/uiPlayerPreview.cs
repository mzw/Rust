﻿using System;
using UnityEngine;

// Token: 0x020006F8 RID: 1784
public class uiPlayerPreview : SingletonComponent<global::uiPlayerPreview>
{
	// Token: 0x04001E68 RID: 7784
	public Camera previewCamera;

	// Token: 0x04001E69 RID: 7785
	public global::PlayerModel playermodel;

	// Token: 0x04001E6A RID: 7786
	public ReflectionProbe reflectionProbe;

	// Token: 0x04001E6B RID: 7787
	public global::SegmentMaskPositioning segmentMask;
}
