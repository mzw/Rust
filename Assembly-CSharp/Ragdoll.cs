﻿using System;
using UnityEngine;

// Token: 0x0200027B RID: 635
public class Ragdoll : global::BaseMonoBehaviour
{
	// Token: 0x04000BCA RID: 3018
	public Transform eyeTransform;

	// Token: 0x04000BCB RID: 3019
	public Transform centerBone;

	// Token: 0x04000BCC RID: 3020
	public Rigidbody primaryBody;

	// Token: 0x04000BCD RID: 3021
	public PhysicMaterial physicMaterial;

	// Token: 0x04000BCE RID: 3022
	public SpringJoint corpseJoint;

	// Token: 0x04000BCF RID: 3023
	public GameObject GibEffect;
}
