﻿using System;

// Token: 0x020000BA RID: 186
public class OreHotSpot : global::BaseCombatEntity, global::ILOD
{
	// Token: 0x06000A88 RID: 2696 RVA: 0x000482D8 File Offset: 0x000464D8
	public void OreOwner(global::OreResourceEntity newOwner)
	{
		this.owner = newOwner;
	}

	// Token: 0x06000A89 RID: 2697 RVA: 0x000482E4 File Offset: 0x000464E4
	public override void ServerInit()
	{
		base.ServerInit();
	}

	// Token: 0x06000A8A RID: 2698 RVA: 0x000482EC File Offset: 0x000464EC
	public override void OnAttacked(global::HitInfo info)
	{
		base.OnAttacked(info);
		if (base.isClient)
		{
			return;
		}
		if (this.owner)
		{
			this.owner.OnAttacked(info);
		}
	}

	// Token: 0x06000A8B RID: 2699 RVA: 0x00048320 File Offset: 0x00046520
	public override void OnKilled(global::HitInfo info)
	{
		this.FireFinishEffect();
		base.OnKilled(info);
	}

	// Token: 0x06000A8C RID: 2700 RVA: 0x00048330 File Offset: 0x00046530
	public void FireFinishEffect()
	{
		if (this.finishEffect.isValid)
		{
			global::Effect.server.Run(this.finishEffect.resourcePath, base.GetEstimatedWorldPosition(), base.transform.forward, null, false);
		}
	}

	// Token: 0x06000A8D RID: 2701 RVA: 0x00048368 File Offset: 0x00046568
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04000539 RID: 1337
	public float visualDistance = 20f;

	// Token: 0x0400053A RID: 1338
	public global::GameObjectRef visualEffect;

	// Token: 0x0400053B RID: 1339
	public global::GameObjectRef finishEffect;

	// Token: 0x0400053C RID: 1340
	public global::GameObjectRef damageEffect;

	// Token: 0x0400053D RID: 1341
	public global::OreResourceEntity owner;
}
