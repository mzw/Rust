﻿using System;
using UnityEngine;

// Token: 0x020005C3 RID: 1475
public class PlaceDecorWhiteNoise : global::ProceduralComponent
{
	// Token: 0x06001E94 RID: 7828 RVA: 0x000ABA30 File Offset: 0x000A9C30
	public override void Process(uint seed)
	{
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + this.ResourceFolder, null, null, true);
		if (array == null || array.Length == 0)
		{
			return;
		}
		Vector3 position = global::TerrainMeta.Position;
		Vector3 size = global::TerrainMeta.Size;
		int num = Mathf.RoundToInt(this.ObjectDensity * size.x * size.z * 1E-06f);
		float x = position.x;
		float z = position.z;
		float num2 = position.x + size.x;
		float num3 = position.z + size.z;
		for (int i = 0; i < num; i++)
		{
			float num4 = SeedRandom.Range(ref seed, x, num2);
			float num5 = SeedRandom.Range(ref seed, z, num3);
			float normX = global::TerrainMeta.NormalizeX(num4);
			float normZ = global::TerrainMeta.NormalizeZ(num5);
			float num6 = SeedRandom.Value(ref seed);
			float factor = this.Filter.GetFactor(normX, normZ);
			global::Prefab random = array.GetRandom(ref seed);
			if (factor * factor >= num6)
			{
				float height = heightMap.GetHeight(normX, normZ);
				Vector3 vector;
				vector..ctor(num4, height, num5);
				Quaternion localRotation = random.Object.transform.localRotation;
				Vector3 localScale = random.Object.transform.localScale;
				random.ApplyDecorComponents(ref vector, ref localRotation, ref localScale);
				if (random.ApplyTerrainAnchors(ref vector, localRotation, localScale, this.Filter))
				{
					if (random.ApplyTerrainChecks(vector, localRotation, localScale, this.Filter))
					{
						if (random.ApplyTerrainFilters(vector, localRotation, localScale, null))
						{
							random.ApplyTerrainModifiers(vector, localRotation, localScale);
							global::World.Serialization.AddPrefab("Decor", random.ID, vector, localRotation, localScale);
						}
					}
				}
			}
		}
	}

	// Token: 0x04001960 RID: 6496
	public global::SpawnFilter Filter;

	// Token: 0x04001961 RID: 6497
	public string ResourceFolder = string.Empty;

	// Token: 0x04001962 RID: 6498
	public float ObjectDensity = 100f;
}
