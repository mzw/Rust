﻿using System;
using UnityEngine;

// Token: 0x0200060F RID: 1551
public class LightCloneShadow : MonoBehaviour
{
	// Token: 0x04001A6C RID: 6764
	public bool cloneShadowMap;

	// Token: 0x04001A6D RID: 6765
	public string shaderPropNameMap = "_MainLightShadowMap";

	// Token: 0x04001A6E RID: 6766
	[Range(0f, 2f)]
	public int cloneShadowMapDownscale = 1;

	// Token: 0x04001A6F RID: 6767
	public RenderTexture map;

	// Token: 0x04001A70 RID: 6768
	public bool cloneShadowMask = true;

	// Token: 0x04001A71 RID: 6769
	public string shaderPropNameMask = "_MainLightShadowMask";

	// Token: 0x04001A72 RID: 6770
	[Range(0f, 2f)]
	public int cloneShadowMaskDownscale = 1;

	// Token: 0x04001A73 RID: 6771
	public RenderTexture mask;
}
