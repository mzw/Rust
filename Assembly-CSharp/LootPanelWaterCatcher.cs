﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006BB RID: 1723
public class LootPanelWaterCatcher : global::LootPanel
{
	// Token: 0x04001D25 RID: 7461
	public global::ItemIcon sourceItem;

	// Token: 0x04001D26 RID: 7462
	public Image capacityImage;

	// Token: 0x04001D27 RID: 7463
	public CanvasGroup helpCanvas;

	// Token: 0x04001D28 RID: 7464
	public CanvasGroup buttonsCanvas;

	// Token: 0x04001D29 RID: 7465
	public Button fromButton;

	// Token: 0x04001D2A RID: 7466
	public Button toButton;

	// Token: 0x04001D2B RID: 7467
	public Button drinkButton;
}
