﻿using System;
using System.Collections.Generic;

// Token: 0x020003CF RID: 975
public class ServerStatistics
{
	// Token: 0x060016C5 RID: 5829 RVA: 0x00083758 File Offset: 0x00081958
	public ServerStatistics(global::BasePlayer player)
	{
		this.player = player;
	}

	// Token: 0x060016C6 RID: 5830 RVA: 0x00083768 File Offset: 0x00081968
	public void Init()
	{
		this.storage = global::ServerStatistics.Get(this.player.userID);
	}

	// Token: 0x060016C7 RID: 5831 RVA: 0x00083780 File Offset: 0x00081980
	public void Save()
	{
	}

	// Token: 0x060016C8 RID: 5832 RVA: 0x00083784 File Offset: 0x00081984
	public void Add(string name, int val)
	{
		if (this.storage != null)
		{
			this.storage.Add(name, val);
		}
	}

	// Token: 0x060016C9 RID: 5833 RVA: 0x000837A0 File Offset: 0x000819A0
	public static global::ServerStatistics.Storage Get(ulong id)
	{
		global::ServerStatistics.Storage storage;
		if (global::ServerStatistics.players.TryGetValue(id, out storage))
		{
			return storage;
		}
		storage = new global::ServerStatistics.Storage();
		global::ServerStatistics.players.Add(id, storage);
		return storage;
	}

	// Token: 0x04001191 RID: 4497
	private global::BasePlayer player;

	// Token: 0x04001192 RID: 4498
	private global::ServerStatistics.Storage storage;

	// Token: 0x04001193 RID: 4499
	private static Dictionary<ulong, global::ServerStatistics.Storage> players = new Dictionary<ulong, global::ServerStatistics.Storage>();

	// Token: 0x020003D0 RID: 976
	public class Storage
	{
		// Token: 0x060016CC RID: 5836 RVA: 0x000837F4 File Offset: 0x000819F4
		public int Get(string name)
		{
			int result;
			this.dict.TryGetValue(name, out result);
			return result;
		}

		// Token: 0x060016CD RID: 5837 RVA: 0x00083814 File Offset: 0x00081A14
		public void Add(string name, int val)
		{
			if (this.dict.ContainsKey(name))
			{
				Dictionary<string, int> dictionary;
				(dictionary = this.dict)[name] = dictionary[name] + val;
			}
			else
			{
				this.dict.Add(name, val);
			}
		}

		// Token: 0x04001194 RID: 4500
		private Dictionary<string, int> dict = new Dictionary<string, int>();
	}
}
