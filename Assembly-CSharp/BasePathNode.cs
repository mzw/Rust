﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020000C9 RID: 201
public class BasePathNode : MonoBehaviour
{
	// Token: 0x06000ABE RID: 2750 RVA: 0x000490E4 File Offset: 0x000472E4
	public void OnDrawGizmosSelected()
	{
	}

	// Token: 0x0400057F RID: 1407
	public List<global::BasePathNode> linked;

	// Token: 0x04000580 RID: 1408
	public float maxVelocityOnApproach = -1f;

	// Token: 0x04000581 RID: 1409
	public bool straightaway;
}
