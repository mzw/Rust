﻿using System;
using System.Collections.Generic;
using ConVar;
using UnityEngine;

// Token: 0x020003CB RID: 971
public class CombatLog
{
	// Token: 0x060016B6 RID: 5814 RVA: 0x0008304C File Offset: 0x0008124C
	public CombatLog(global::BasePlayer player)
	{
		this.player = player;
	}

	// Token: 0x060016B7 RID: 5815 RVA: 0x0008305C File Offset: 0x0008125C
	public void Init()
	{
		this.storage = global::CombatLog.Get(this.player.userID);
	}

	// Token: 0x060016B8 RID: 5816 RVA: 0x00083074 File Offset: 0x00081274
	public void Save()
	{
	}

	// Token: 0x060016B9 RID: 5817 RVA: 0x00083078 File Offset: 0x00081278
	public void Log(global::AttackEntity weapon, string description = null)
	{
		this.Log(weapon, null, description);
	}

	// Token: 0x060016BA RID: 5818 RVA: 0x00083084 File Offset: 0x00081284
	public void Log(global::AttackEntity weapon, global::Projectile projectile, string description = null)
	{
		this.Log(new global::CombatLog.Event
		{
			time = UnityEngine.Time.realtimeSinceStartup,
			attacker_id = ((!this.player || this.player.net == null) ? 0u : this.player.net.ID),
			target_id = 0u,
			attacker = "you",
			target = "N/A",
			weapon = ((!weapon) ? "N/A" : weapon.name),
			ammo = ((!projectile) ? "N/A" : projectile.name),
			bone = "N/A",
			area = (global::HitArea)0,
			distance = 0f,
			health_old = 0f,
			health_new = 0f,
			info = ((description == null) ? string.Empty : description)
		});
	}

	// Token: 0x060016BB RID: 5819 RVA: 0x000831A0 File Offset: 0x000813A0
	public void Log(global::HitInfo info, string description = null)
	{
		float num = (!info.HitEntity) ? 0f : info.HitEntity.Health();
		this.Log(info, num, num, description);
	}

	// Token: 0x060016BC RID: 5820 RVA: 0x000831E0 File Offset: 0x000813E0
	public void Log(global::HitInfo info, float health_old, float health_new, string description = null)
	{
		this.Log(new global::CombatLog.Event
		{
			time = UnityEngine.Time.realtimeSinceStartup,
			attacker_id = ((!info.Initiator || info.Initiator.net == null) ? 0u : info.Initiator.net.ID),
			target_id = ((!info.HitEntity || info.HitEntity.net == null) ? 0u : info.HitEntity.net.ID),
			attacker = ((!(this.player == info.Initiator)) ? ((!info.Initiator) ? "N/A" : info.Initiator.ShortPrefabName) : "you"),
			target = ((!(this.player == info.HitEntity)) ? ((!info.HitEntity) ? "N/A" : info.HitEntity.ShortPrefabName) : "you"),
			weapon = ((!info.WeaponPrefab) ? "N/A" : info.WeaponPrefab.name),
			ammo = ((!info.ProjectilePrefab) ? "N/A" : info.ProjectilePrefab.name),
			bone = info.boneName,
			area = info.boneArea,
			distance = ((!info.IsProjectile()) ? Vector3.Distance(info.PointStart, info.HitPositionWorld) : info.ProjectileDistance),
			health_old = health_old,
			health_new = health_new,
			info = ((description == null) ? string.Empty : description)
		});
	}

	// Token: 0x060016BD RID: 5821 RVA: 0x000833E4 File Offset: 0x000815E4
	public void Log(global::CombatLog.Event val)
	{
		if (this.storage == null)
		{
			return;
		}
		this.storage.Enqueue(val);
		int num = Mathf.Max(0, ConVar.Server.combatlogsize);
		while (this.storage.Count > num)
		{
			this.storage.Dequeue();
		}
	}

	// Token: 0x060016BE RID: 5822 RVA: 0x00083438 File Offset: 0x00081638
	public string Get(int count)
	{
		if (this.storage == null)
		{
			return string.Empty;
		}
		if (this.storage.Count == 0)
		{
			return "Combat log empty.";
		}
		TextTable textTable = new TextTable();
		textTable.AddColumn("time");
		textTable.AddColumn("attacker");
		textTable.AddColumn("id");
		textTable.AddColumn("target");
		textTable.AddColumn("id");
		textTable.AddColumn("weapon");
		textTable.AddColumn("ammo");
		textTable.AddColumn("area");
		textTable.AddColumn("distance");
		textTable.AddColumn("old_hp");
		textTable.AddColumn("new_hp");
		textTable.AddColumn("info");
		int num = this.storage.Count - count;
		foreach (global::CombatLog.Event @event in this.storage)
		{
			if (num > 0)
			{
				num--;
			}
			else
			{
				string text = (UnityEngine.Time.realtimeSinceStartup - @event.time).ToString("0.0s");
				string attacker = @event.attacker;
				string text2 = @event.attacker_id.ToString();
				string target = @event.target;
				string text3 = @event.target_id.ToString();
				string weapon = @event.weapon;
				string ammo = @event.ammo;
				string text4 = global::HitAreaUtil.Format(@event.area).ToLower();
				string text5 = @event.distance.ToString("0.0m");
				string text6 = @event.health_old.ToString("0.0");
				string text7 = @event.health_new.ToString("0.0");
				string info = @event.info;
				textTable.AddRow(new string[]
				{
					text,
					attacker,
					text2,
					target,
					text3,
					weapon,
					ammo,
					text4,
					text5,
					text6,
					text7,
					info
				});
			}
		}
		return textTable.ToString();
	}

	// Token: 0x060016BF RID: 5823 RVA: 0x00083678 File Offset: 0x00081878
	public static Queue<global::CombatLog.Event> Get(ulong id)
	{
		Queue<global::CombatLog.Event> queue;
		if (global::CombatLog.players.TryGetValue(id, out queue))
		{
			return queue;
		}
		queue = new Queue<global::CombatLog.Event>();
		global::CombatLog.players.Add(id, queue);
		return queue;
	}

	// Token: 0x04001178 RID: 4472
	private const string selfname = "you";

	// Token: 0x04001179 RID: 4473
	private const string noname = "N/A";

	// Token: 0x0400117A RID: 4474
	private global::BasePlayer player;

	// Token: 0x0400117B RID: 4475
	private Queue<global::CombatLog.Event> storage;

	// Token: 0x0400117C RID: 4476
	private static Dictionary<ulong, Queue<global::CombatLog.Event>> players = new Dictionary<ulong, Queue<global::CombatLog.Event>>();

	// Token: 0x020003CC RID: 972
	public struct Event
	{
		// Token: 0x0400117D RID: 4477
		public float time;

		// Token: 0x0400117E RID: 4478
		public uint attacker_id;

		// Token: 0x0400117F RID: 4479
		public uint target_id;

		// Token: 0x04001180 RID: 4480
		public string attacker;

		// Token: 0x04001181 RID: 4481
		public string target;

		// Token: 0x04001182 RID: 4482
		public string weapon;

		// Token: 0x04001183 RID: 4483
		public string ammo;

		// Token: 0x04001184 RID: 4484
		public string bone;

		// Token: 0x04001185 RID: 4485
		public global::HitArea area;

		// Token: 0x04001186 RID: 4486
		public float distance;

		// Token: 0x04001187 RID: 4487
		public float health_old;

		// Token: 0x04001188 RID: 4488
		public float health_new;

		// Token: 0x04001189 RID: 4489
		public string info;
	}
}
