﻿using System;

// Token: 0x020001BF RID: 447
public class BaseAnimalNPC : global::BaseNpc
{
	// Token: 0x06000E8E RID: 3726 RVA: 0x0005BCAC File Offset: 0x00059EAC
	public override void OnKilled(global::HitInfo hitInfo = null)
	{
		if (hitInfo != null)
		{
			global::BasePlayer initiatorPlayer = hitInfo.InitiatorPlayer;
			if (initiatorPlayer != null)
			{
				initiatorPlayer.GiveAchievement("KILL_ANIMAL");
			}
		}
		base.OnKilled(null);
	}
}
