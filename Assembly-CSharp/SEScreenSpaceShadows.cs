﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000814 RID: 2068
[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Sonic Ether/SE Screen-Space Shadows")]
[RequireComponent(typeof(Camera))]
public class SEScreenSpaceShadows : MonoBehaviour
{
	// Token: 0x0600267B RID: 9851 RVA: 0x000D51EC File Offset: 0x000D33EC
	private void AddCommandBufferClean(Light light, CommandBuffer commandBuffer, LightEvent lightEvent)
	{
		bool flag = false;
		CommandBuffer[] commandBuffers = light.GetCommandBuffers(lightEvent);
		foreach (CommandBuffer commandBuffer2 in commandBuffers)
		{
			if (commandBuffer2.name == commandBuffer.name)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			light.AddCommandBuffer(lightEvent, commandBuffer);
		}
	}

	// Token: 0x0600267C RID: 9852 RVA: 0x000D5248 File Offset: 0x000D3448
	private void AddCommandBufferClean(Camera camera, CommandBuffer commandBuffer, CameraEvent cameraEvent)
	{
		bool flag = false;
		CommandBuffer[] commandBuffers = camera.GetCommandBuffers(cameraEvent);
		foreach (CommandBuffer commandBuffer2 in commandBuffers)
		{
			if (commandBuffer2.name == commandBuffer.name)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			camera.AddCommandBuffer(cameraEvent, commandBuffer);
		}
	}

	// Token: 0x0600267D RID: 9853 RVA: 0x000D52A4 File Offset: 0x000D34A4
	private void RemoveCommandBuffer(Light light, CommandBuffer commandBuffer, LightEvent lightEvent)
	{
		CommandBuffer[] commandBuffers = light.GetCommandBuffers(lightEvent);
		List<CommandBuffer> list = new List<CommandBuffer>();
		foreach (CommandBuffer commandBuffer2 in commandBuffers)
		{
			if (commandBuffer2.name != commandBuffer.name)
			{
				list.Add(commandBuffer2);
			}
		}
		light.RemoveCommandBuffers(lightEvent);
		foreach (CommandBuffer commandBuffer3 in list)
		{
			light.AddCommandBuffer(lightEvent, commandBuffer3);
		}
	}

	// Token: 0x0600267E RID: 9854 RVA: 0x000D5350 File Offset: 0x000D3550
	private void RemoveCommandBuffer(Camera camera, CommandBuffer commandBuffer, CameraEvent cameraEvent)
	{
		CommandBuffer[] commandBuffers = camera.GetCommandBuffers(cameraEvent);
		List<CommandBuffer> list = new List<CommandBuffer>();
		foreach (CommandBuffer commandBuffer2 in commandBuffers)
		{
			if (commandBuffer2.name != commandBuffer.name)
			{
				list.Add(commandBuffer2);
			}
		}
		camera.RemoveCommandBuffers(cameraEvent);
		foreach (CommandBuffer commandBuffer3 in list)
		{
			camera.AddCommandBuffer(cameraEvent, commandBuffer3);
		}
	}

	// Token: 0x0600267F RID: 9855 RVA: 0x000D53FC File Offset: 0x000D35FC
	private void RemoveCommandBuffers()
	{
		this.RemoveCommandBuffer(this.attachedCamera, this.renderShadowsCommandBuffer, 6);
		if (this.sun != null)
		{
			this.RemoveCommandBuffer(this.sun, this.blendShadowsCommandBuffer, 3);
		}
	}

	// Token: 0x06002680 RID: 9856 RVA: 0x000D5438 File Offset: 0x000D3638
	public bool GetCompatibleRenderPath()
	{
		if (this.attachedCamera)
		{
			return this.attachedCamera.actualRenderingPath == 3;
		}
		Camera component = base.GetComponent<Camera>();
		return component.actualRenderingPath == 3;
	}

	// Token: 0x06002681 RID: 9857 RVA: 0x000D5474 File Offset: 0x000D3674
	private void Init()
	{
		if (this.initChecker != null)
		{
			return;
		}
		if (!this.attachedCamera)
		{
			this.attachedCamera = base.GetComponent<Camera>();
			this.attachedCamera.depthTextureMode |= 1;
		}
		if (!this.GetCompatibleRenderPath())
		{
			this.initChecker = null;
			return;
		}
		this.material = new Material(Shader.Find("Hidden/SEScreenSpaceShadows"));
		this.sunInitialized = false;
		this.blendShadowsCommandBuffer = new CommandBuffer
		{
			name = "SE Screen Space Shadows: Blend"
		};
		this.blendShadowsCommandBuffer.Blit(0, 1, this.material, 0);
		this.renderShadowsCommandBuffer = new CommandBuffer
		{
			name = "SE Screen Space Shadows: Render"
		};
		int num = Shader.PropertyToID("SEScreenShadowBuffer0");
		int num2 = Shader.PropertyToID("SEScreenShadowBuffer1");
		int num3 = Shader.PropertyToID("DepthSource");
		this.renderShadowsCommandBuffer.GetTemporaryRT(num, -1, -1, 0, 1, 16);
		if (this.bilateralBlur)
		{
			this.renderShadowsCommandBuffer.GetTemporaryRT(num2, -1, -1, 0, 1, 16);
		}
		this.renderShadowsCommandBuffer.GetTemporaryRT(num3, -1, -1, 0, 0, 14);
		this.renderShadowsCommandBuffer.Blit(num, num3, this.material, 2);
		this.renderShadowsCommandBuffer.Blit(num3, num, this.material, 1);
		if (this.bilateralBlur)
		{
			for (int i = 0; i < this.blurPasses; i++)
			{
				this.renderShadowsCommandBuffer.SetGlobalVector("SESSSBlurKernel", new Vector2(0f, 1f));
				this.renderShadowsCommandBuffer.Blit(num, num2, this.material, 3);
				this.renderShadowsCommandBuffer.SetGlobalVector("SESSSBlurKernel", new Vector2(1f, 0f));
				this.renderShadowsCommandBuffer.Blit(num2, num, this.material, 3);
			}
		}
		this.renderShadowsCommandBuffer.SetGlobalTexture("SEScreenSpaceShadowsTexture", num);
		this.AddCommandBufferClean(this.attachedCamera, this.renderShadowsCommandBuffer, 6);
		this.noBlendTex = new Texture2D(1, 1, 5, false);
		this.noBlendTex.SetPixel(0, 0, Color.white);
		this.noBlendTex.Apply();
		this.initChecker = new object();
	}

	// Token: 0x06002682 RID: 9858 RVA: 0x000D56E4 File Offset: 0x000D38E4
	private void OnEnable()
	{
		this.Init();
	}

	// Token: 0x06002683 RID: 9859 RVA: 0x000D56EC File Offset: 0x000D38EC
	private void OnDisable()
	{
		this.RemoveCommandBuffers();
		this.initChecker = null;
	}

	// Token: 0x06002684 RID: 9860 RVA: 0x000D56FC File Offset: 0x000D38FC
	private void OnPreRender()
	{
		this.Init();
		if (this.initChecker == null)
		{
			return;
		}
		if (this.sun != null && !this.sunInitialized)
		{
			this.AddCommandBufferClean(this.sun, this.blendShadowsCommandBuffer, 3);
			this.sunInitialized = true;
		}
		if (this.leverageTemporalAA)
		{
			this.temporalJitterCounter = (this.temporalJitterCounter + 1) % 8;
		}
		if (this.previousBilateralBlurSetting != this.bilateralBlur || this.previousBlurPassesSetting != this.blurPasses)
		{
			this.RemoveCommandBuffers();
			this.initChecker = null;
			this.Init();
		}
		this.previousBilateralBlurSetting = this.bilateralBlur;
		this.previousBlurPassesSetting = this.blurPasses;
		if (!this.sunInitialized)
		{
			return;
		}
		this.material.SetMatrix("ProjectionMatrix", this.attachedCamera.projectionMatrix);
		this.material.SetMatrix("ProjectionMatrixInverse", this.attachedCamera.projectionMatrix.inverse);
		this.material.SetVector("SunlightVector", base.transform.InverseTransformDirection(this.sun.transform.forward));
		this.material.SetVector("ScreenRes", new Vector4((float)this.attachedCamera.pixelWidth, (float)this.attachedCamera.pixelHeight, 1f / (float)this.attachedCamera.pixelWidth, 1f / (float)this.attachedCamera.pixelHeight));
		this.material.SetFloat("BlendStrength", this.blendStrength);
		this.material.SetFloat("Accumulation", this.accumulation);
		this.material.SetFloat("Range", this.range);
		this.material.SetFloat("ZThickness", this.zThickness);
		this.material.SetInt("Samples", this.samples);
		this.material.SetFloat("NearQualityCutoff", 1f / this.nearSampleQuality);
		this.material.SetFloat("TraceBias", this.traceBias);
		this.material.SetFloat("StochasticSampling", (!this.stochasticSampling) ? 0f : 1f);
		this.material.SetInt("TJitter", this.temporalJitterCounter);
		this.material.SetFloat("LengthFade", this.lengthFade);
		this.material.SetFloat("BlurDepthTolerance", this.blurDepthTolerance);
	}

	// Token: 0x06002685 RID: 9861 RVA: 0x000D5990 File Offset: 0x000D3B90
	private void OnPostRender()
	{
		Shader.SetGlobalTexture("SEScreenSpaceShadowsTexture", this.noBlendTex);
	}

	// Token: 0x040022BF RID: 8895
	private CommandBuffer blendShadowsCommandBuffer;

	// Token: 0x040022C0 RID: 8896
	private CommandBuffer renderShadowsCommandBuffer;

	// Token: 0x040022C1 RID: 8897
	private Camera attachedCamera;

	// Token: 0x040022C2 RID: 8898
	public Light sun;

	// Token: 0x040022C3 RID: 8899
	private Material material;

	// Token: 0x040022C4 RID: 8900
	private object initChecker;

	// Token: 0x040022C5 RID: 8901
	private bool sunInitialized;

	// Token: 0x040022C6 RID: 8902
	private int temporalJitterCounter;

	// Token: 0x040022C7 RID: 8903
	[Range(0f, 1f)]
	public float blendStrength = 1f;

	// Token: 0x040022C8 RID: 8904
	[Range(0f, 1f)]
	public float accumulation = 0.9f;

	// Token: 0x040022C9 RID: 8905
	[Range(0.1f, 5f)]
	public float lengthFade = 0.7f;

	// Token: 0x040022CA RID: 8906
	[Range(0.01f, 5f)]
	public float range = 0.7f;

	// Token: 0x040022CB RID: 8907
	[Range(0f, 1f)]
	public float zThickness = 0.1f;

	// Token: 0x040022CC RID: 8908
	[Range(2f, 92f)]
	public int samples = 32;

	// Token: 0x040022CD RID: 8909
	[Range(0.5f, 4f)]
	public float nearSampleQuality = 1.5f;

	// Token: 0x040022CE RID: 8910
	[Range(0f, 1f)]
	public float traceBias = 0.03f;

	// Token: 0x040022CF RID: 8911
	public bool stochasticSampling = true;

	// Token: 0x040022D0 RID: 8912
	public bool leverageTemporalAA;

	// Token: 0x040022D1 RID: 8913
	public bool bilateralBlur = true;

	// Token: 0x040022D2 RID: 8914
	[Range(1f, 2f)]
	public int blurPasses = 1;

	// Token: 0x040022D3 RID: 8915
	[Range(0.01f, 0.5f)]
	public float blurDepthTolerance = 0.1f;

	// Token: 0x040022D4 RID: 8916
	private bool previousBilateralBlurSetting;

	// Token: 0x040022D5 RID: 8917
	private int previousBlurPassesSetting = 1;

	// Token: 0x040022D6 RID: 8918
	private Texture2D noBlendTex;
}
