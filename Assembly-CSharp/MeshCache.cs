﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000248 RID: 584
public static class MeshCache
{
	// Token: 0x06001033 RID: 4147 RVA: 0x00061E80 File Offset: 0x00060080
	public static global::MeshCache.Data Get(Mesh mesh)
	{
		global::MeshCache.Data data;
		if (!global::MeshCache.dictionary.TryGetValue(mesh, out data))
		{
			data = new global::MeshCache.Data();
			data.mesh = mesh;
			data.vertices = mesh.vertices;
			data.normals = mesh.normals;
			data.tangents = mesh.tangents;
			data.colors32 = mesh.colors32;
			data.triangles = mesh.triangles;
			data.uv = mesh.uv;
			data.uv2 = mesh.uv2;
			data.uv3 = mesh.uv3;
			data.uv4 = mesh.uv4;
			global::MeshCache.dictionary.Add(mesh, data);
		}
		return data;
	}

	// Token: 0x04000AF4 RID: 2804
	public static Dictionary<Mesh, global::MeshCache.Data> dictionary = new Dictionary<Mesh, global::MeshCache.Data>();

	// Token: 0x02000249 RID: 585
	[Serializable]
	public class Data
	{
		// Token: 0x04000AF5 RID: 2805
		public Mesh mesh;

		// Token: 0x04000AF6 RID: 2806
		public Vector3[] vertices;

		// Token: 0x04000AF7 RID: 2807
		public Vector3[] normals;

		// Token: 0x04000AF8 RID: 2808
		public Vector4[] tangents;

		// Token: 0x04000AF9 RID: 2809
		public Color32[] colors32;

		// Token: 0x04000AFA RID: 2810
		public int[] triangles;

		// Token: 0x04000AFB RID: 2811
		public Vector2[] uv;

		// Token: 0x04000AFC RID: 2812
		public Vector2[] uv2;

		// Token: 0x04000AFD RID: 2813
		public Vector2[] uv3;

		// Token: 0x04000AFE RID: 2814
		public Vector2[] uv4;
	}
}
