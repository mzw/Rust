﻿using System;
using System.Collections.Generic;
using ConVar;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000617 RID: 1559
public static class AntiHack
{
	// Token: 0x06001F73 RID: 8051 RVA: 0x000B24A8 File Offset: 0x000B06A8
	public static void ResetTimer(global::BasePlayer ply)
	{
		ply.lastViolationTime = UnityEngine.Time.realtimeSinceStartup;
	}

	// Token: 0x06001F74 RID: 8052 RVA: 0x000B24B8 File Offset: 0x000B06B8
	public static bool ShouldIgnore(OBB obb)
	{
		bool result;
		using (TimeWarning.New("AntiHack.ShouldIgnore", 0.1f))
		{
			result = ((global::EnvironmentManager.Get(obb) & global::EnvironmentType.Elevator) != (global::EnvironmentType)0);
		}
		return result;
	}

	// Token: 0x06001F75 RID: 8053 RVA: 0x000B2508 File Offset: 0x000B0708
	public static bool ShouldIgnore(Vector3 pos)
	{
		bool result;
		using (TimeWarning.New("AntiHack.ShouldIgnore", 0.1f))
		{
			result = ((global::EnvironmentManager.Get(pos) & global::EnvironmentType.Elevator) != (global::EnvironmentType)0);
		}
		return result;
	}

	// Token: 0x06001F76 RID: 8054 RVA: 0x000B2558 File Offset: 0x000B0758
	public static bool ValidateMove(global::BasePlayer ply, global::TickInterpolator ticks, float deltaTime)
	{
		bool result;
		using (TimeWarning.New("AntiHack.ValidateMove", 0.1f))
		{
			if (ply.IsFlying)
			{
				ply.lastAdminCheatTime = UnityEngine.Time.realtimeSinceStartup;
			}
			if (global::AntiHack.ShouldIgnore(ply.WorldSpaceBounds()))
			{
				result = true;
			}
			else
			{
				if (ply.IsAdmin)
				{
					if (ConVar.AntiHack.userlevel < 1)
					{
						return true;
					}
					if (ConVar.AntiHack.admincheat && ply.UsedAdminCheat(1f))
					{
						return true;
					}
				}
				if (ply.IsDeveloper)
				{
					if (ConVar.AntiHack.userlevel < 2)
					{
						return true;
					}
					if (ConVar.AntiHack.admincheat && ply.UsedAdminCheat(1f))
					{
						return true;
					}
				}
				if (ply.IsSleeping())
				{
					result = true;
				}
				else if (ply.IsWounded())
				{
					result = true;
				}
				else if (ply.IsSpectating())
				{
					result = true;
				}
				else if (ply.IsDead())
				{
					result = true;
				}
				else
				{
					bool flag = deltaTime > ConVar.AntiHack.maxdeltatime;
					using (TimeWarning.New("IsNoClipping", 0.1f))
					{
						if (global::AntiHack.IsNoClipping(ply, ticks, deltaTime))
						{
							if (flag)
							{
								return false;
							}
							global::AntiHack.AddViolation(ply, global::AntiHackType.NoClip, ConVar.AntiHack.noclip_penalty * ticks.Length);
							if (ConVar.AntiHack.noclip_reject)
							{
								return false;
							}
						}
					}
					using (TimeWarning.New("IsSpeeding", 0.1f))
					{
						if (global::AntiHack.IsSpeeding(ply, ticks, deltaTime))
						{
							if (flag)
							{
								return false;
							}
							global::AntiHack.AddViolation(ply, global::AntiHackType.SpeedHack, ConVar.AntiHack.speedhack_penalty * ticks.Length);
							if (ConVar.AntiHack.speedhack_reject)
							{
								return false;
							}
						}
					}
					using (TimeWarning.New("IsFlying", 0.1f))
					{
						if (global::AntiHack.IsFlying(ply, ticks, deltaTime))
						{
							if (flag)
							{
								return false;
							}
							global::AntiHack.AddViolation(ply, global::AntiHackType.FlyHack, ConVar.AntiHack.flyhack_penalty * ticks.Length);
							if (ConVar.AntiHack.flyhack_reject)
							{
								return false;
							}
						}
					}
					result = true;
				}
			}
		}
		return result;
	}

	// Token: 0x06001F77 RID: 8055 RVA: 0x000B2810 File Offset: 0x000B0A10
	public static bool IsNoClipping(global::BasePlayer ply, global::TickInterpolator ticks, float deltaTime)
	{
		if (ConVar.AntiHack.noclip_protection <= 0)
		{
			return false;
		}
		ticks.Reset();
		if (!ticks.HasNext())
		{
			return false;
		}
		Vector3 oldPos = ticks.StartPoint;
		Vector3 vector = ticks.EndPoint;
		if (ConVar.AntiHack.noclip_protection >= 3)
		{
			float num = Mathf.Max(ConVar.AntiHack.noclip_stepsize, 0.1f);
			int num2 = Mathf.Max(ConVar.AntiHack.noclip_maxsteps, 1);
			num = Mathf.Max(ticks.Length / (float)num2, num);
			while (ticks.MoveNext(num))
			{
				vector = ticks.CurrentPoint;
				if (global::AntiHack.TestNoClipping(ply, oldPos, vector, true))
				{
					return true;
				}
				oldPos = vector;
			}
		}
		else if (ConVar.AntiHack.noclip_protection >= 2)
		{
			if (global::AntiHack.TestNoClipping(ply, oldPos, vector, true))
			{
				return true;
			}
		}
		else if (global::AntiHack.TestNoClipping(ply, oldPos, vector, false))
		{
			return true;
		}
		return false;
	}

	// Token: 0x06001F78 RID: 8056 RVA: 0x000B28E4 File Offset: 0x000B0AE4
	private static bool TestNoClipping(global::BasePlayer ply, Vector3 oldPos, Vector3 newPos, bool sphereCast)
	{
		float noclip_backtracking = ConVar.AntiHack.noclip_backtracking;
		float noclip_margin = ConVar.AntiHack.noclip_margin;
		float radius = ply.GetRadius();
		float height = ply.GetHeight(true);
		Vector3 normalized = (newPos - oldPos).normalized;
		float num = radius - noclip_margin;
		Vector3 vector = oldPos + new Vector3(0f, height - radius, 0f) - normalized * noclip_backtracking;
		Vector3 vector2 = newPos + new Vector3(0f, height - radius, 0f);
		float magnitude = (vector2 - vector).magnitude;
		RaycastHit hitInfo = default(RaycastHit);
		bool flag = UnityEngine.Physics.Raycast(new Ray(vector, normalized), ref hitInfo, magnitude + num, 295772417, 1);
		if (!flag && sphereCast)
		{
			flag = UnityEngine.Physics.SphereCast(new Ray(vector, normalized), num, ref hitInfo, magnitude, 295772417, 1);
		}
		return flag && global::GamePhysics.Verify(hitInfo);
	}

	// Token: 0x06001F79 RID: 8057 RVA: 0x000B29D8 File Offset: 0x000B0BD8
	public static bool IsSpeeding(global::BasePlayer ply, global::TickInterpolator ticks, float deltaTime)
	{
		if (ConVar.AntiHack.speedhack_protection <= 0)
		{
			return false;
		}
		Vector3 startPoint = ticks.StartPoint;
		Vector3 endPoint = ticks.EndPoint;
		Vector3 vector = endPoint - startPoint;
		float num = Vector3Ex.Magnitude2D(vector);
		if (num > 0f)
		{
			Vector3 vector2 = (!global::TerrainMeta.HeightMap) ? Vector3.up : global::TerrainMeta.HeightMap.GetNormal(startPoint);
			float num2 = Mathf.Max(0f, Vector3.Dot(Vector3Ex.XZ3D(vector2), Vector3Ex.XZ3D(vector)));
			float num3 = num2 * ConVar.AntiHack.speedhack_slopespeed * deltaTime;
			num = Mathf.Max(0f, num - num3);
		}
		ply.speedhackDistance += num;
		ply.speedhackDeltaTime += deltaTime;
		if (ply.speedhackDeltaTime > ConVar.AntiHack.speedhack_deltatime)
		{
			float item = ply.speedhackDistance / ply.speedhackDeltaTime;
			float num4 = ply.GetMaxSpeed() + ConVar.AntiHack.speedhack_forgiveness;
			ply.speedhackDistance = 0f;
			ply.speedhackDeltaTime = 0f;
			ply.speedhackHistory.Enqueue(item);
			while (ply.speedhackHistory.Count > Mathf.Max(2, ConVar.AntiHack.speedhack_history))
			{
				ply.speedhackHistory.Dequeue();
			}
			ply.speedhackTickets = 0;
			foreach (float num5 in ply.speedhackHistory)
			{
				if (num5 > num4)
				{
					ply.speedhackTickets++;
				}
			}
		}
		return ply.speedhackTickets >= Mathf.Max(2, ConVar.AntiHack.speedhack_tickets);
	}

	// Token: 0x06001F7A RID: 8058 RVA: 0x000B2B94 File Offset: 0x000B0D94
	public static bool IsFlying(global::BasePlayer ply, global::TickInterpolator ticks, float deltaTime)
	{
		if (ConVar.AntiHack.flyhack_protection <= 0)
		{
			return false;
		}
		ticks.Reset();
		if (!ticks.HasNext())
		{
			return false;
		}
		Vector3 oldPos = ticks.StartPoint;
		Vector3 vector = ticks.EndPoint;
		if (ConVar.AntiHack.flyhack_protection >= 3)
		{
			float num = Mathf.Max(ConVar.AntiHack.flyhack_stepsize, 0.1f);
			int num2 = Mathf.Max(ConVar.AntiHack.flyhack_maxsteps, 1);
			num = Mathf.Max(ticks.Length / (float)num2, num);
			while (ticks.MoveNext(num))
			{
				vector = ticks.CurrentPoint;
				if (global::AntiHack.TestFlying(ply, oldPos, vector, true))
				{
					return true;
				}
				oldPos = vector;
			}
		}
		else if (ConVar.AntiHack.flyhack_protection >= 2)
		{
			if (global::AntiHack.TestFlying(ply, oldPos, vector, true))
			{
				return true;
			}
		}
		else if (global::AntiHack.TestFlying(ply, oldPos, vector, false))
		{
			return true;
		}
		return false;
	}

	// Token: 0x06001F7B RID: 8059 RVA: 0x000B2C68 File Offset: 0x000B0E68
	private static bool TestFlying(global::BasePlayer ply, Vector3 oldPos, Vector3 newPos, bool verifyGrounded)
	{
		ply.isInAir = false;
		ply.isOnPlayer = false;
		if (verifyGrounded)
		{
			float flyhack_extrusion = ConVar.AntiHack.flyhack_extrusion;
			Vector3 vector = (oldPos + newPos) * 0.5f;
			if (!ply.OnLadder() || !ply.FindTrigger<global::TriggerLadder>())
			{
				if (!global::WaterLevel.Test(vector - new Vector3(0f, flyhack_extrusion, 0f)))
				{
					float flyhack_margin = ConVar.AntiHack.flyhack_margin;
					float radius = ply.GetRadius();
					float height = ply.GetHeight(false);
					Vector3 vector2 = vector + new Vector3(0f, radius - flyhack_extrusion, 0f);
					Vector3 vector3 = vector + new Vector3(0f, height - radius, 0f);
					float num = radius - flyhack_margin;
					ply.isInAir = !UnityEngine.Physics.CheckCapsule(vector2, vector3, num, 1369514241, 1);
					if (ply.isInAir)
					{
						int num2 = UnityEngine.Physics.OverlapCapsuleNonAlloc(vector2, vector3, num, global::AntiHack.buffer, 131072, 1);
						for (int i = 0; i < num2; i++)
						{
							global::BasePlayer basePlayer = global::AntiHack.buffer[i].gameObject.ToBaseEntity() as global::BasePlayer;
							if (!(basePlayer == null))
							{
								if (!(basePlayer == ply))
								{
									if (!basePlayer.isInAir)
									{
										if (!basePlayer.isOnPlayer)
										{
											if (!basePlayer.TriggeredAntiHack(1f, float.PositiveInfinity))
											{
												if (!basePlayer.IsSleeping())
												{
													ply.isOnPlayer = true;
													ply.isInAir = false;
													break;
												}
											}
										}
									}
								}
							}
						}
						for (int j = 0; j < global::AntiHack.buffer.Length; j++)
						{
							global::AntiHack.buffer[j] = null;
						}
					}
				}
			}
		}
		else
		{
			ply.isInAir = (!ply.OnLadder() && !ply.IsSwimming() && !ply.IsOnGround());
		}
		if (ply.isInAir)
		{
			bool flag = false;
			Vector3 vector4 = newPos - oldPos;
			float num3 = Mathf.Abs(vector4.y);
			float num4 = Vector3Ex.Magnitude2D(vector4);
			if (vector4.y >= 0f)
			{
				ply.flyhackDistanceVertical += vector4.y;
				flag = true;
			}
			if (num3 < num4)
			{
				ply.flyhackDistanceHorizontal += num4;
				flag = true;
			}
			if (flag)
			{
				float num5 = ply.GetJumpHeight() + ConVar.AntiHack.flyhack_forgiveness_vertical;
				if (ply.flyhackDistanceVertical > num5)
				{
					return true;
				}
				float num6 = 5f + ConVar.AntiHack.flyhack_forgiveness_horizontal;
				if (ply.flyhackDistanceHorizontal > num6)
				{
					return true;
				}
			}
		}
		else
		{
			ply.flyhackDistanceVertical = 0f;
			ply.flyhackDistanceHorizontal = 0f;
		}
		return false;
	}

	// Token: 0x06001F7C RID: 8060 RVA: 0x000B2F48 File Offset: 0x000B1148
	public static void NoteAdminHack(global::BasePlayer ply)
	{
		global::AntiHack.Ban(ply, "Cheat Detected!");
	}

	// Token: 0x06001F7D RID: 8061 RVA: 0x000B2F58 File Offset: 0x000B1158
	public static void FadeViolations(global::BasePlayer ply, float deltaTime)
	{
		if (UnityEngine.Time.realtimeSinceStartup - ply.lastViolationTime > ConVar.AntiHack.relaxationpause)
		{
			ply.violationLevel = Mathf.Max(0f, ply.violationLevel - ConVar.AntiHack.relaxationrate * deltaTime);
		}
	}

	// Token: 0x06001F7E RID: 8062 RVA: 0x000B2F90 File Offset: 0x000B1190
	public static void EnforceViolations(global::BasePlayer ply)
	{
		if (ConVar.AntiHack.enforcementlevel <= 0)
		{
			return;
		}
		if (ply.violationLevel > ConVar.AntiHack.maxviolation)
		{
			if (ConVar.AntiHack.debuglevel >= 1)
			{
				global::AntiHack.LogToConsole(ply, ply.lastViolationType, "Enforcing (violation of " + ply.violationLevel + ")");
			}
			string reason = ply.lastViolationType + " Violation Level " + ply.violationLevel;
			if (ConVar.AntiHack.enforcementlevel > 1)
			{
				global::AntiHack.Kick(ply, reason);
			}
			else
			{
				global::AntiHack.Kick(ply, reason);
			}
		}
	}

	// Token: 0x06001F7F RID: 8063 RVA: 0x000B302C File Offset: 0x000B122C
	public static void Log(global::BasePlayer ply, global::AntiHackType type, string message)
	{
		if (ConVar.AntiHack.debuglevel > 1)
		{
			global::AntiHack.LogToConsole(ply, type, message);
		}
		global::AntiHack.LogToEAC(ply, type, message);
	}

	// Token: 0x06001F80 RID: 8064 RVA: 0x000B304C File Offset: 0x000B124C
	private static void LogToConsole(global::BasePlayer ply, global::AntiHackType type, string message)
	{
		Debug.LogWarning(string.Concat(new object[]
		{
			ply,
			" ",
			type,
			": ",
			message
		}));
	}

	// Token: 0x06001F81 RID: 8065 RVA: 0x000B3080 File Offset: 0x000B1280
	private static void LogToEAC(global::BasePlayer ply, global::AntiHackType type, string message)
	{
		if (ConVar.AntiHack.reporting && global::EACServer.eacScout != null)
		{
			global::EACServer.eacScout.SendInvalidPlayerStateReport(ply.UserIDString, 2, type + ": " + message);
		}
	}

	// Token: 0x06001F82 RID: 8066 RVA: 0x000B30B8 File Offset: 0x000B12B8
	public static void AddViolation(global::BasePlayer ply, global::AntiHackType type, float amount)
	{
		if (Interface.CallHook("OnPlayerViolation", new object[]
		{
			ply,
			type,
			amount
		}) != null)
		{
			return;
		}
		ply.lastViolationType = type;
		ply.lastViolationTime = UnityEngine.Time.realtimeSinceStartup;
		ply.violationLevel += amount;
		if ((ConVar.AntiHack.debuglevel >= 2 && amount > 0f) || ConVar.AntiHack.debuglevel >= 3)
		{
			global::AntiHack.LogToConsole(ply, type, string.Concat(new object[]
			{
				"Added violation of ",
				amount,
				" in frame ",
				UnityEngine.Time.frameCount,
				" (now has ",
				ply.violationLevel,
				")"
			}));
		}
		global::AntiHack.EnforceViolations(ply);
	}

	// Token: 0x06001F83 RID: 8067 RVA: 0x000B3190 File Offset: 0x000B1390
	public static void Kick(global::BasePlayer ply, string reason)
	{
		if (global::EACServer.eacScout != null)
		{
			global::EACServer.eacScout.SendKickReport(ply.userID.ToString(), reason, 4);
		}
		global::AntiHack.AddRecord(ply, global::AntiHack.kicks);
		ConsoleSystem.Run(ConsoleSystem.Option.Server, "kick", new object[]
		{
			ply.userID,
			reason
		});
	}

	// Token: 0x06001F84 RID: 8068 RVA: 0x000B31F8 File Offset: 0x000B13F8
	public static void Ban(global::BasePlayer ply, string reason)
	{
		if (global::EACServer.eacScout != null)
		{
			global::EACServer.eacScout.SendKickReport(ply.userID.ToString(), reason, 1);
		}
		global::AntiHack.AddRecord(ply, global::AntiHack.bans);
		ConsoleSystem.Run(ConsoleSystem.Option.Server, "ban", new object[]
		{
			ply.userID,
			reason
		});
	}

	// Token: 0x06001F85 RID: 8069 RVA: 0x000B3260 File Offset: 0x000B1460
	private static void AddRecord(global::BasePlayer ply, Dictionary<ulong, int> records)
	{
		if (records.ContainsKey(ply.userID))
		{
			ulong userID;
			records[userID = ply.userID] = records[userID] + 1;
		}
		else
		{
			records.Add(ply.userID, 1);
		}
	}

	// Token: 0x06001F86 RID: 8070 RVA: 0x000B32AC File Offset: 0x000B14AC
	public static int GetKickRecord(global::BasePlayer ply)
	{
		return global::AntiHack.GetRecord(ply, global::AntiHack.kicks);
	}

	// Token: 0x06001F87 RID: 8071 RVA: 0x000B32BC File Offset: 0x000B14BC
	public static int GetBanRecord(global::BasePlayer ply)
	{
		return global::AntiHack.GetRecord(ply, global::AntiHack.bans);
	}

	// Token: 0x06001F88 RID: 8072 RVA: 0x000B32CC File Offset: 0x000B14CC
	private static int GetRecord(global::BasePlayer ply, Dictionary<ulong, int> records)
	{
		return (!records.ContainsKey(ply.userID)) ? 0 : records[ply.userID];
	}

	// Token: 0x04001A89 RID: 6793
	private const int movement_mask = 295772417;

	// Token: 0x04001A8A RID: 6794
	private const int grounded_mask = 1369514241;

	// Token: 0x04001A8B RID: 6795
	private const int player_mask = 131072;

	// Token: 0x04001A8C RID: 6796
	private static Collider[] buffer = new Collider[4];

	// Token: 0x04001A8D RID: 6797
	private static Dictionary<ulong, int> kicks = new Dictionary<ulong, int>();

	// Token: 0x04001A8E RID: 6798
	private static Dictionary<ulong, int> bans = new Dictionary<ulong, int>();
}
