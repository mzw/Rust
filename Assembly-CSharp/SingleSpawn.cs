﻿using System;

// Token: 0x0200048C RID: 1164
public class SingleSpawn : global::SpawnGroup
{
	// Token: 0x0600194C RID: 6476 RVA: 0x0008E89C File Offset: 0x0008CA9C
	public override bool WantsInitialSpawn()
	{
		return false;
	}

	// Token: 0x0600194D RID: 6477 RVA: 0x0008E8A0 File Offset: 0x0008CAA0
	public void FillDelay(float delay)
	{
		base.Invoke(new Action(base.Fill), delay);
	}
}
