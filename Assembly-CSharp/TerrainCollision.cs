﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000561 RID: 1377
public class TerrainCollision : global::TerrainExtension
{
	// Token: 0x06001CBC RID: 7356 RVA: 0x000A10AC File Offset: 0x0009F2AC
	public override void Setup()
	{
		this.ignoredColliders = new ListDictionary<Collider, List<Collider>>(8);
		this.terrainCollider = this.terrain.GetComponent<TerrainCollider>();
	}

	// Token: 0x06001CBD RID: 7357 RVA: 0x000A10CC File Offset: 0x0009F2CC
	public void Clear()
	{
		if (!this.terrainCollider)
		{
			return;
		}
		foreach (Collider collider in this.ignoredColliders.Keys)
		{
			Physics.IgnoreCollision(collider, this.terrainCollider, false);
		}
		this.ignoredColliders.Clear();
	}

	// Token: 0x06001CBE RID: 7358 RVA: 0x000A114C File Offset: 0x0009F34C
	public void Reset(Collider collider)
	{
		if (!this.terrainCollider || !collider)
		{
			return;
		}
		Physics.IgnoreCollision(collider, this.terrainCollider, false);
		this.ignoredColliders.Remove(collider);
	}

	// Token: 0x06001CBF RID: 7359 RVA: 0x000A1184 File Offset: 0x0009F384
	public bool GetIgnore(Vector3 pos, float radius = 0.01f)
	{
		return global::GamePhysics.CheckSphere<global::TerrainCollisionTrigger>(pos, radius, 262144, 2);
	}

	// Token: 0x06001CC0 RID: 7360 RVA: 0x000A11A0 File Offset: 0x0009F3A0
	public bool GetIgnore(RaycastHit hit)
	{
		return hit.collider is TerrainCollider && this.GetIgnore(hit.point, 0.01f);
	}

	// Token: 0x06001CC1 RID: 7361 RVA: 0x000A11C8 File Offset: 0x0009F3C8
	public bool GetIgnore(Collider collider)
	{
		return this.terrainCollider && collider && this.ignoredColliders.Contains(collider);
	}

	// Token: 0x06001CC2 RID: 7362 RVA: 0x000A11F4 File Offset: 0x0009F3F4
	public void SetIgnore(Collider collider, Collider trigger, bool ignore = true)
	{
		if (!this.terrainCollider || !collider)
		{
			return;
		}
		if (!this.GetIgnore(collider))
		{
			if (ignore)
			{
				List<Collider> list = new List<Collider>
				{
					trigger
				};
				Physics.IgnoreCollision(collider, this.terrainCollider, true);
				this.ignoredColliders.Add(collider, list);
			}
		}
		else
		{
			List<Collider> list2 = this.ignoredColliders[collider];
			if (ignore)
			{
				if (!list2.Contains(trigger))
				{
					list2.Add(trigger);
				}
			}
			else if (list2.Contains(trigger))
			{
				list2.Remove(trigger);
			}
		}
	}

	// Token: 0x06001CC3 RID: 7363 RVA: 0x000A12AC File Offset: 0x0009F4AC
	protected void LateUpdate()
	{
		for (int i = 0; i < this.ignoredColliders.Count; i++)
		{
			KeyValuePair<Collider, List<Collider>> byIndex = this.ignoredColliders.GetByIndex(i);
			Collider key = byIndex.Key;
			List<Collider> value = byIndex.Value;
			if (key == null)
			{
				this.ignoredColliders.RemoveAt(i--);
			}
			else if (value.Count == 0)
			{
				Physics.IgnoreCollision(key, this.terrainCollider, false);
				this.ignoredColliders.RemoveAt(i--);
			}
		}
	}

	// Token: 0x040017F7 RID: 6135
	private ListDictionary<Collider, List<Collider>> ignoredColliders;

	// Token: 0x040017F8 RID: 6136
	private TerrainCollider terrainCollider;
}
