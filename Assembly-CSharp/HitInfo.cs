﻿using System;
using Network;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x0200042F RID: 1071
public class HitInfo
{
	// Token: 0x06001853 RID: 6227 RVA: 0x0008A080 File Offset: 0x00088280
	public HitInfo()
	{
	}

	// Token: 0x06001854 RID: 6228 RVA: 0x0008A0B4 File Offset: 0x000882B4
	public HitInfo(global::BaseEntity attacker, global::BaseEntity target, Rust.DamageType type, float damageAmount, Vector3 vhitPosition)
	{
		this.Initiator = attacker;
		this.HitEntity = target;
		this.HitPositionWorld = vhitPosition;
		if (attacker != null)
		{
			this.PointStart = attacker.transform.position;
		}
		this.damageTypes.Add(type, damageAmount);
	}

	// Token: 0x06001855 RID: 6229 RVA: 0x0008A134 File Offset: 0x00088334
	public HitInfo(global::BaseEntity attacker, global::BaseEntity target, Rust.DamageType type, float damageAmount) : this(attacker, target, type, damageAmount, target.transform.position)
	{
	}

	// Token: 0x06001856 RID: 6230 RVA: 0x0008A14C File Offset: 0x0008834C
	public bool IsProjectile()
	{
		return this.ProjectileID != 0;
	}

	// Token: 0x170001A3 RID: 419
	// (get) Token: 0x06001857 RID: 6231 RVA: 0x0008A15C File Offset: 0x0008835C
	public global::BasePlayer InitiatorPlayer
	{
		get
		{
			return (!this.Initiator) ? null : this.Initiator.ToPlayer();
		}
	}

	// Token: 0x170001A4 RID: 420
	// (get) Token: 0x06001858 RID: 6232 RVA: 0x0008A180 File Offset: 0x00088380
	public Vector3 attackNormal
	{
		get
		{
			return (this.PointEnd - this.PointStart).normalized;
		}
	}

	// Token: 0x170001A5 RID: 421
	// (get) Token: 0x06001859 RID: 6233 RVA: 0x0008A1A8 File Offset: 0x000883A8
	public bool hasDamage
	{
		get
		{
			return this.damageTypes.Total() > 0f;
		}
	}

	// Token: 0x0600185A RID: 6234 RVA: 0x0008A1BC File Offset: 0x000883BC
	public void LoadFromAttack(Attack attack, bool serverSide)
	{
		this.HitEntity = null;
		this.PointStart = attack.pointStart;
		this.PointEnd = attack.pointEnd;
		if (attack.hitID > 0u)
		{
			this.DidHit = true;
			if (serverSide)
			{
				this.HitEntity = (global::BaseNetworkable.serverEntities.Find(attack.hitID) as global::BaseEntity);
			}
			if (this.HitEntity)
			{
				this.HitBone = attack.hitBone;
				this.HitPart = attack.hitPartID;
			}
		}
		this.DidHit = true;
		this.HitPositionLocal = attack.hitPositionLocal;
		this.HitPositionWorld = attack.hitPositionWorld;
		this.HitNormalLocal = attack.hitNormalLocal;
		this.HitNormalWorld = attack.hitNormalWorld;
		this.HitMaterial = attack.hitMaterialID;
	}

	// Token: 0x170001A6 RID: 422
	// (get) Token: 0x0600185B RID: 6235 RVA: 0x0008A288 File Offset: 0x00088488
	public bool isHeadshot
	{
		get
		{
			if (this.HitEntity == null)
			{
				return false;
			}
			global::BaseCombatEntity baseCombatEntity = this.HitEntity as global::BaseCombatEntity;
			if (baseCombatEntity == null)
			{
				return false;
			}
			if (baseCombatEntity.skeletonProperties == null)
			{
				return false;
			}
			global::SkeletonProperties.BoneProperty boneProperty = baseCombatEntity.skeletonProperties.FindBone(this.HitBone);
			return boneProperty != null && boneProperty.area == global::HitArea.Head;
		}
	}

	// Token: 0x170001A7 RID: 423
	// (get) Token: 0x0600185C RID: 6236 RVA: 0x0008A2F8 File Offset: 0x000884F8
	public global::Translate.Phrase bonePhrase
	{
		get
		{
			if (this.HitEntity == null)
			{
				return null;
			}
			global::BaseCombatEntity baseCombatEntity = this.HitEntity as global::BaseCombatEntity;
			if (baseCombatEntity == null)
			{
				return null;
			}
			if (baseCombatEntity.skeletonProperties == null)
			{
				return null;
			}
			global::SkeletonProperties.BoneProperty boneProperty = baseCombatEntity.skeletonProperties.FindBone(this.HitBone);
			if (boneProperty == null)
			{
				return null;
			}
			return boneProperty.name;
		}
	}

	// Token: 0x170001A8 RID: 424
	// (get) Token: 0x0600185D RID: 6237 RVA: 0x0008A368 File Offset: 0x00088568
	public string boneName
	{
		get
		{
			global::Translate.Phrase bonePhrase = this.bonePhrase;
			return (bonePhrase != null) ? bonePhrase.english : "N/A";
		}
	}

	// Token: 0x170001A9 RID: 425
	// (get) Token: 0x0600185E RID: 6238 RVA: 0x0008A394 File Offset: 0x00088594
	public global::HitArea boneArea
	{
		get
		{
			if (this.HitEntity == null)
			{
				return (global::HitArea)-1;
			}
			global::BaseCombatEntity baseCombatEntity = this.HitEntity as global::BaseCombatEntity;
			if (baseCombatEntity == null)
			{
				return (global::HitArea)-1;
			}
			return baseCombatEntity.SkeletonLookup(this.HitBone);
		}
	}

	// Token: 0x0600185F RID: 6239 RVA: 0x0008A3DC File Offset: 0x000885DC
	public Vector3 PositionOnRay(Vector3 position)
	{
		Ray ray;
		ray..ctor(this.PointStart, this.attackNormal);
		if (this.ProjectilePrefab == null)
		{
			return ray.ClosestPoint(position);
		}
		Sphere sphere;
		sphere..ctor(position, this.ProjectilePrefab.thickness);
		RaycastHit raycastHit;
		if (sphere.Trace(ray, ref raycastHit, float.PositiveInfinity))
		{
			return raycastHit.point;
		}
		return position;
	}

	// Token: 0x06001860 RID: 6240 RVA: 0x0008A448 File Offset: 0x00088648
	public Vector3 HitPositionOnRay()
	{
		return this.PositionOnRay(this.HitPositionWorld);
	}

	// Token: 0x06001861 RID: 6241 RVA: 0x0008A458 File Offset: 0x00088658
	public bool IsNaNOrInfinity()
	{
		return Vector3Ex.IsNaNOrInfinity(this.PointStart) || Vector3Ex.IsNaNOrInfinity(this.PointEnd) || Vector3Ex.IsNaNOrInfinity(this.HitPositionWorld) || Vector3Ex.IsNaNOrInfinity(this.HitPositionLocal) || Vector3Ex.IsNaNOrInfinity(this.HitNormalWorld) || Vector3Ex.IsNaNOrInfinity(this.HitNormalLocal) || Vector3Ex.IsNaNOrInfinity(this.ProjectileVelocity) || float.IsNaN(this.ProjectileDistance) || float.IsInfinity(this.ProjectileDistance);
	}

	// Token: 0x040012FB RID: 4859
	public global::BaseEntity Initiator;

	// Token: 0x040012FC RID: 4860
	public global::BaseEntity WeaponPrefab;

	// Token: 0x040012FD RID: 4861
	public global::AttackEntity Weapon;

	// Token: 0x040012FE RID: 4862
	public bool DoHitEffects = true;

	// Token: 0x040012FF RID: 4863
	public bool DoDecals = true;

	// Token: 0x04001300 RID: 4864
	public bool IsPredicting;

	// Token: 0x04001301 RID: 4865
	public bool UseProtection = true;

	// Token: 0x04001302 RID: 4866
	public Connection Predicted;

	// Token: 0x04001303 RID: 4867
	public bool DidHit;

	// Token: 0x04001304 RID: 4868
	public global::BaseEntity HitEntity;

	// Token: 0x04001305 RID: 4869
	public uint HitBone;

	// Token: 0x04001306 RID: 4870
	public uint HitPart;

	// Token: 0x04001307 RID: 4871
	public uint HitMaterial;

	// Token: 0x04001308 RID: 4872
	public Vector3 HitPositionWorld;

	// Token: 0x04001309 RID: 4873
	public Vector3 HitPositionLocal;

	// Token: 0x0400130A RID: 4874
	public Vector3 HitNormalWorld;

	// Token: 0x0400130B RID: 4875
	public Vector3 HitNormalLocal;

	// Token: 0x0400130C RID: 4876
	public Vector3 PointStart;

	// Token: 0x0400130D RID: 4877
	public Vector3 PointEnd;

	// Token: 0x0400130E RID: 4878
	public int ProjectileID;

	// Token: 0x0400130F RID: 4879
	public float ProjectileDistance;

	// Token: 0x04001310 RID: 4880
	public Vector3 ProjectileVelocity;

	// Token: 0x04001311 RID: 4881
	public global::Projectile ProjectilePrefab;

	// Token: 0x04001312 RID: 4882
	public PhysicMaterial material;

	// Token: 0x04001313 RID: 4883
	public global::DamageProperties damageProperties;

	// Token: 0x04001314 RID: 4884
	public Rust.DamageTypeList damageTypes = new Rust.DamageTypeList();

	// Token: 0x04001315 RID: 4885
	public bool CanGather;

	// Token: 0x04001316 RID: 4886
	public bool DidGather;

	// Token: 0x04001317 RID: 4887
	public float gatherScale = 1f;
}
