﻿using System;
using UnityEngine;

// Token: 0x02000609 RID: 1545
public class FoliageDisplacement : MonoBehaviour, IClientComponent, global::ILOD
{
	// Token: 0x04001A5C RID: 6748
	public bool moving;

	// Token: 0x04001A5D RID: 6749
	public bool billboard;

	// Token: 0x04001A5E RID: 6750
	public Mesh mesh;

	// Token: 0x04001A5F RID: 6751
	public Material material;
}
