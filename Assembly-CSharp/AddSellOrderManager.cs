﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020000AF RID: 175
public class AddSellOrderManager : MonoBehaviour
{
	// Token: 0x040004FE RID: 1278
	public global::VirtualItemIcon sellItemIcon;

	// Token: 0x040004FF RID: 1279
	public global::VirtualItemIcon currencyItemIcon;

	// Token: 0x04000500 RID: 1280
	public GameObject itemSearchParent;

	// Token: 0x04000501 RID: 1281
	public global::ItemSearchEntry itemSearchEntryPrefab;

	// Token: 0x04000502 RID: 1282
	public InputField sellItemInput;

	// Token: 0x04000503 RID: 1283
	public InputField sellItemAmount;

	// Token: 0x04000504 RID: 1284
	public InputField currencyItemInput;

	// Token: 0x04000505 RID: 1285
	public InputField currencyItemAmount;

	// Token: 0x04000506 RID: 1286
	public global::VendingPanelAdmin adminPanel;
}
