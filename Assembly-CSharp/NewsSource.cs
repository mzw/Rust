﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Facepunch.Extend;
using Facepunch.Math;
using JSON;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006D8 RID: 1752
public class NewsSource : MonoBehaviour
{
	// Token: 0x0600218E RID: 8590 RVA: 0x000BC66C File Offset: 0x000BA86C
	private void OnEnable()
	{
		base.StartCoroutine(this.UpdateNews());
	}

	// Token: 0x0600218F RID: 8591 RVA: 0x000BC67C File Offset: 0x000BA87C
	public void SetStory(int i)
	{
		if (this.story == null)
		{
			return;
		}
		if (this.story.Length <= i)
		{
			return;
		}
		base.StopAllCoroutines();
		this.title.text = this.story[i].name;
		this.date.text = NumberExtensions.FormatSecondsLong((long)(Epoch.Current - this.story[i].date));
		string text = Regex.Replace(this.story[i].text, "\\[img\\].*\\[\\/img\\]", string.Empty, RegexOptions.IgnoreCase);
		text = text.Replace("\\n", "\n").Replace("\\r", string.Empty).Replace("\\\"", "\"");
		text = text.Replace("[list]", "<color=#F7EBE1aa>");
		text = text.Replace("[/list]", "</color>");
		text = text.Replace("[*]", "\t\t» ");
		text = Regex.Replace(text, "\\[(.*?)\\]", string.Empty, RegexOptions.IgnoreCase);
		text = text.Trim();
		Match match = Regex.Match(this.story[i].text, "url=(http|https):\\/\\/([\\w\\-_]+(?:(?:\\.[\\w\\-_]+)+))([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])");
		Match match2 = Regex.Match(this.story[i].text, "(http|https):\\/\\/([\\w\\-_]+(?:(?:\\.[\\w\\-_]+)+))([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])(.png|.jpg)");
		if (match != null)
		{
			string url = match.Value.Replace("url=", string.Empty);
			if (url == null || url.Trim().Length <= 0)
			{
				url = this.story[i].url;
			}
			this.button.gameObject.SetActive(true);
			this.button.onClick.RemoveAllListeners();
			this.button.onClick.AddListener(delegate
			{
				Debug.Log("Opening URL: " + url);
				Application.OpenURL(url);
			});
		}
		else
		{
			this.button.gameObject.SetActive(false);
		}
		this.text.text = text;
		this.authorName.text = string.Format("posted by {0}", this.story[i].author);
		if (this.image != null)
		{
			if (this.story[i].texture)
			{
				this.SetHeadlineTexture(this.story[i].texture);
			}
			else if (match2 != null)
			{
				base.StartCoroutine(this.LoadHeaderImage(match2.Value, i));
			}
		}
	}

	// Token: 0x06002190 RID: 8592 RVA: 0x000BC90C File Offset: 0x000BAB0C
	private void SetHeadlineTexture(Texture tex)
	{
		float num = (float)tex.height / (float)tex.width;
		this.image.texture = tex;
		this.image.rectTransform.sizeDelta = new Vector2(0f, this.image.rectTransform.rect.width * num);
		this.image.enabled = true;
		RectOffset padding = this.layoutGroup.padding;
		padding.top = (int)(this.image.rectTransform.rect.width * num) / 2;
		this.layoutGroup.padding = padding;
	}

	// Token: 0x06002191 RID: 8593 RVA: 0x000BC9B0 File Offset: 0x000BABB0
	private IEnumerator LoadHeaderImage(string url, int i)
	{
		this.image.enabled = false;
		WWW www = new WWW(url);
		yield return www;
		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogWarning("Couldn't load header image: " + www.error);
			www.Dispose();
			yield break;
		}
		Texture2D tex = www.textureNonReadable;
		tex.name = url;
		this.story[i].texture = tex;
		this.SetHeadlineTexture(this.story[i].texture);
		www.Dispose();
		yield break;
	}

	// Token: 0x06002192 RID: 8594 RVA: 0x000BC9DC File Offset: 0x000BABDC
	private IEnumerator UpdateNews()
	{
		WWW www = new WWW("http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=252490&count=8&format=json&feeds=steam_community_announcements");
		yield return www;
		Object json = Object.Parse(www.text);
		www.Dispose();
		if (json == null)
		{
			yield break;
		}
		Array items = json.GetObject("appnews").GetArray("newsitems");
		List<global::NewsSource.Story> storyList = new List<global::NewsSource.Story>();
		foreach (Value value in items)
		{
			string @string = value.Obj.GetString("contents", "Missing URL");
			storyList.Add(new global::NewsSource.Story
			{
				name = value.Obj.GetString("title", "Missing Title"),
				url = value.Obj.GetString("url", "Missing URL"),
				date = value.Obj.GetInt("date", 0),
				text = @string,
				author = value.Obj.GetString("author", "Missing Author")
			});
		}
		this.story = storyList.ToArray();
		this.SetStory(0);
		yield break;
	}

	// Token: 0x04001DB6 RID: 7606
	public global::NewsSource.Story[] story;

	// Token: 0x04001DB7 RID: 7607
	public Text title;

	// Token: 0x04001DB8 RID: 7608
	public Text date;

	// Token: 0x04001DB9 RID: 7609
	public Text text;

	// Token: 0x04001DBA RID: 7610
	public Text authorName;

	// Token: 0x04001DBB RID: 7611
	public RawImage image;

	// Token: 0x04001DBC RID: 7612
	public VerticalLayoutGroup layoutGroup;

	// Token: 0x04001DBD RID: 7613
	public Button button;

	// Token: 0x020006D9 RID: 1753
	public struct Story
	{
		// Token: 0x04001DBE RID: 7614
		public string name;

		// Token: 0x04001DBF RID: 7615
		public string url;

		// Token: 0x04001DC0 RID: 7616
		public int date;

		// Token: 0x04001DC1 RID: 7617
		public string text;

		// Token: 0x04001DC2 RID: 7618
		public string author;

		// Token: 0x04001DC3 RID: 7619
		public Texture texture;
	}
}
