﻿using System;
using UnityEngine;

// Token: 0x020007BB RID: 1979
public class IronSights : MonoBehaviour
{
	// Token: 0x04002039 RID: 8249
	public bool Enabled;

	// Token: 0x0400203A RID: 8250
	[Header("View Setup")]
	public global::IronsightAimPoint aimPoint;

	// Token: 0x0400203B RID: 8251
	public float fieldOfViewOffset = -20f;

	// Token: 0x0400203C RID: 8252
	[Header("Animation")]
	public float introSpeed = 1f;

	// Token: 0x0400203D RID: 8253
	public AnimationCurve introCurve = new AnimationCurve();

	// Token: 0x0400203E RID: 8254
	public float outroSpeed = 1f;

	// Token: 0x0400203F RID: 8255
	public AnimationCurve outroCurve = new AnimationCurve();

	// Token: 0x04002040 RID: 8256
	[Header("Sounds")]
	public global::SoundDefinition upSound;

	// Token: 0x04002041 RID: 8257
	public global::SoundDefinition downSound;

	// Token: 0x04002042 RID: 8258
	[Header("Info")]
	public global::IronSightOverride ironsightsOverride;
}
