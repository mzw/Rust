﻿using System;
using UnityEngine;

// Token: 0x0200062F RID: 1583
public class BoneInfoComponent : MonoBehaviour, IClientComponent
{
	// Token: 0x04001AF1 RID: 6897
	[Header("Size Variation")]
	public Vector3 sizeVariation = Vector3.zero;

	// Token: 0x04001AF2 RID: 6898
	public int sizeVariationSeed;
}
