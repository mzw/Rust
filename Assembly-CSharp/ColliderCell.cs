﻿using System;
using System.Collections;
using ConVar;
using Facepunch;
using UnityEngine;

// Token: 0x0200028F RID: 655
public class ColliderCell
{
	// Token: 0x060010E4 RID: 4324 RVA: 0x000652AC File Offset: 0x000634AC
	public ColliderCell(global::ColliderGrid grid, Vector3 position)
	{
		this.grid = grid;
		this.position = position;
	}

	// Token: 0x060010E5 RID: 4325 RVA: 0x000652D0 File Offset: 0x000634D0
	public bool NeedsRefresh()
	{
		BufferList<global::ColliderGroup> values = this.batches.Values;
		for (int i = 0; i < values.Count; i++)
		{
			if (values[i].NeedsRefresh)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060010E6 RID: 4326 RVA: 0x00065314 File Offset: 0x00063514
	public int MeshCount()
	{
		int num = 0;
		BufferList<global::ColliderGroup> values = this.batches.Values;
		for (int i = 0; i < values.Count; i++)
		{
			num += values[i].MeshCount();
		}
		return num;
	}

	// Token: 0x060010E7 RID: 4327 RVA: 0x00065358 File Offset: 0x00063558
	public int BatchedMeshCount()
	{
		int num = 0;
		BufferList<global::ColliderGroup> values = this.batches.Values;
		for (int i = 0; i < values.Count; i++)
		{
			num += values[i].BatchedMeshCount();
		}
		return num;
	}

	// Token: 0x060010E8 RID: 4328 RVA: 0x0006539C File Offset: 0x0006359C
	public void Refresh()
	{
		this.interrupt = false;
		BufferList<global::ColliderGroup> values = this.batches.Values;
		for (int i = 0; i < values.Count; i++)
		{
			global::ColliderGroup colliderGroup = values[i];
			if (!colliderGroup.Processing)
			{
				if (colliderGroup.Count > 0)
				{
					colliderGroup.Start();
					if (colliderGroup.Processing)
					{
						colliderGroup.UpdateData();
						colliderGroup.CreateBatches();
						colliderGroup.RefreshBatches();
						colliderGroup.ApplyBatches();
						colliderGroup.DisplayBatches();
					}
					colliderGroup.End();
				}
				else
				{
					colliderGroup.Clear();
					this.DestroyColliderGroup(ref colliderGroup);
					this.batches.RemoveAt(i--);
				}
			}
		}
	}

	// Token: 0x060010E9 RID: 4329 RVA: 0x0006544C File Offset: 0x0006364C
	public IEnumerator RefreshAsync()
	{
		this.interrupt = false;
		BufferList<global::ColliderGroup> batchGroups = this.batches.Values;
		for (int n = 0; n < batchGroups.Count; n++)
		{
			global::ColliderGroup colliderGroup = batchGroups[n];
			if (colliderGroup.Count > 0)
			{
				colliderGroup.Start();
			}
		}
		int i = 0;
		while (i < batchGroups.Count && !this.interrupt)
		{
			global::ColliderGroup batchGroup = batchGroups[i];
			if (batchGroup.Processing)
			{
				if (ConVar.Batching.collider_threading)
				{
					IEnumerator enumerator = batchGroup.UpdateDataAsync();
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						yield return obj;
					}
					this.grid.ResetTimeout();
				}
				else
				{
					batchGroup.UpdateData();
					if (this.grid.NeedsTimeout)
					{
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						this.grid.ResetTimeout();
					}
				}
			}
			i++;
		}
		int j = 0;
		while (j < batchGroups.Count && !this.interrupt)
		{
			global::ColliderGroup batchGroup2 = batchGroups[j];
			if (batchGroup2.Processing)
			{
				batchGroup2.CreateBatches();
				if (this.grid.NeedsTimeout)
				{
					yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
					this.grid.ResetTimeout();
				}
			}
			j++;
		}
		int k = 0;
		while (k < batchGroups.Count && !this.interrupt)
		{
			global::ColliderGroup batchGroup3 = batchGroups[k];
			if (batchGroup3.Processing)
			{
				if (ConVar.Batching.collider_threading)
				{
					IEnumerator enumerator2 = batchGroup3.RefreshBatchesAsync();
					while (enumerator2.MoveNext())
					{
						object obj2 = enumerator2.Current;
						yield return obj2;
					}
					this.grid.ResetTimeout();
				}
				else
				{
					batchGroup3.RefreshBatches();
					if (this.grid.NeedsTimeout)
					{
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						this.grid.ResetTimeout();
					}
				}
			}
			k++;
		}
		int l = 0;
		while (l < batchGroups.Count && !this.interrupt)
		{
			global::ColliderGroup batchGroup4 = batchGroups[l];
			if (batchGroup4.Processing)
			{
				int m = 0;
				while (m < batchGroup4.TempBatches.Count && !this.interrupt)
				{
					batchGroup4.TempBatches[m].Apply();
					if (this.grid.NeedsTimeout)
					{
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						this.grid.ResetTimeout();
					}
					m++;
				}
			}
			l++;
		}
		int num = 0;
		while (num < batchGroups.Count && !this.interrupt)
		{
			global::ColliderGroup colliderGroup2 = batchGroups[num];
			if (colliderGroup2.Processing)
			{
				colliderGroup2.DisplayBatches();
			}
			num++;
		}
		for (int num2 = 0; num2 < batchGroups.Count; num2++)
		{
			global::ColliderGroup colliderGroup3 = batchGroups[num2];
			if (colliderGroup3.Processing || colliderGroup3.Preserving)
			{
				colliderGroup3.End();
			}
			else if (colliderGroup3.Count == 0 && !this.interrupt)
			{
				colliderGroup3.Clear();
				this.DestroyColliderGroup(ref colliderGroup3);
				this.batches.RemoveAt(num2--);
			}
		}
		yield break;
	}

	// Token: 0x060010EA RID: 4330 RVA: 0x00065468 File Offset: 0x00063668
	public global::ColliderGroup FindBatchGroup(global::ColliderBatch collider)
	{
		global::ColliderKey colliderKey = new global::ColliderKey(collider);
		global::ColliderGroup colliderGroup;
		if (!this.batches.TryGetValue(colliderKey, ref colliderGroup))
		{
			colliderGroup = this.CreateColliderGroup(this.grid, this, colliderKey);
			this.batches.Add(colliderKey, colliderGroup);
		}
		return colliderGroup;
	}

	// Token: 0x060010EB RID: 4331 RVA: 0x000654B0 File Offset: 0x000636B0
	private global::ColliderGroup CreateColliderGroup(global::ColliderGrid grid, global::ColliderCell cell, global::ColliderKey key)
	{
		global::ColliderGroup colliderGroup = Facepunch.Pool.Get<global::ColliderGroup>();
		colliderGroup.Initialize(grid, cell, key);
		return colliderGroup;
	}

	// Token: 0x060010EC RID: 4332 RVA: 0x000654D0 File Offset: 0x000636D0
	private void DestroyColliderGroup(ref global::ColliderGroup grp)
	{
		Facepunch.Pool.Free<global::ColliderGroup>(ref grp);
	}

	// Token: 0x04000BFD RID: 3069
	public Vector3 position;

	// Token: 0x04000BFE RID: 3070
	public global::ColliderGrid grid;

	// Token: 0x04000BFF RID: 3071
	public bool interrupt;

	// Token: 0x04000C00 RID: 3072
	private ListDictionary<global::ColliderKey, global::ColliderGroup> batches = new ListDictionary<global::ColliderKey, global::ColliderGroup>(8);
}
