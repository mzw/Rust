﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200024D RID: 589
public class MeshColliderLookup
{
	// Token: 0x06001041 RID: 4161 RVA: 0x00062394 File Offset: 0x00060594
	public void Apply()
	{
		global::MeshColliderLookup.LookupGroup lookupGroup = this.src;
		this.src = this.dst;
		this.dst = lookupGroup;
		this.dst.Clear();
	}

	// Token: 0x06001042 RID: 4162 RVA: 0x000623C8 File Offset: 0x000605C8
	public void Add(global::MeshColliderInstance instance)
	{
		this.dst.Add(instance);
	}

	// Token: 0x06001043 RID: 4163 RVA: 0x000623D8 File Offset: 0x000605D8
	public global::MeshColliderLookup.LookupEntry Get(int index)
	{
		return this.src.Get(index);
	}

	// Token: 0x04000B0A RID: 2826
	public global::MeshColliderLookup.LookupGroup src = new global::MeshColliderLookup.LookupGroup();

	// Token: 0x04000B0B RID: 2827
	public global::MeshColliderLookup.LookupGroup dst = new global::MeshColliderLookup.LookupGroup();

	// Token: 0x0200024E RID: 590
	public class LookupGroup
	{
		// Token: 0x06001045 RID: 4165 RVA: 0x00062408 File Offset: 0x00060608
		public void Clear()
		{
			this.data.Clear();
			this.indices.Clear();
		}

		// Token: 0x06001046 RID: 4166 RVA: 0x00062420 File Offset: 0x00060620
		public void Add(global::MeshColliderInstance instance)
		{
			this.data.Add(new global::MeshColliderLookup.LookupEntry(instance));
			int item = this.data.Count - 1;
			int num = instance.data.triangles.Length / 3;
			for (int i = 0; i < num; i++)
			{
				this.indices.Add(item);
			}
		}

		// Token: 0x06001047 RID: 4167 RVA: 0x0006247C File Offset: 0x0006067C
		public global::MeshColliderLookup.LookupEntry Get(int index)
		{
			return this.data[this.indices[index]];
		}

		// Token: 0x04000B0C RID: 2828
		public List<global::MeshColliderLookup.LookupEntry> data = new List<global::MeshColliderLookup.LookupEntry>();

		// Token: 0x04000B0D RID: 2829
		public List<int> indices = new List<int>();
	}

	// Token: 0x0200024F RID: 591
	public struct LookupEntry
	{
		// Token: 0x06001048 RID: 4168 RVA: 0x00062498 File Offset: 0x00060698
		public LookupEntry(global::MeshColliderInstance instance)
		{
			this.transform = instance.transform;
			this.rigidbody = instance.rigidbody;
			this.collider = instance.collider;
			this.bounds = instance.bounds;
		}

		// Token: 0x04000B0E RID: 2830
		public Transform transform;

		// Token: 0x04000B0F RID: 2831
		public Rigidbody rigidbody;

		// Token: 0x04000B10 RID: 2832
		public Collider collider;

		// Token: 0x04000B11 RID: 2833
		public OBB bounds;
	}
}
