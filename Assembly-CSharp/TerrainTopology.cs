﻿using System;

// Token: 0x02000533 RID: 1331
public static class TerrainTopology
{
	// Token: 0x04001702 RID: 5890
	public const int COUNT = 31;

	// Token: 0x04001703 RID: 5891
	public const int EVERYTHING = -1;

	// Token: 0x04001704 RID: 5892
	public const int NOTHING = 0;

	// Token: 0x04001705 RID: 5893
	public const int FIELD = 1;

	// Token: 0x04001706 RID: 5894
	public const int CLIFF = 2;

	// Token: 0x04001707 RID: 5895
	public const int SUMMIT = 4;

	// Token: 0x04001708 RID: 5896
	public const int BEACHSIDE = 8;

	// Token: 0x04001709 RID: 5897
	public const int BEACH = 16;

	// Token: 0x0400170A RID: 5898
	public const int FOREST = 32;

	// Token: 0x0400170B RID: 5899
	public const int FORESTSIDE = 64;

	// Token: 0x0400170C RID: 5900
	public const int OCEAN = 128;

	// Token: 0x0400170D RID: 5901
	public const int OCEANSIDE = 256;

	// Token: 0x0400170E RID: 5902
	public const int DECOR = 512;

	// Token: 0x0400170F RID: 5903
	public const int MONUMENT = 1024;

	// Token: 0x04001710 RID: 5904
	public const int ROAD = 2048;

	// Token: 0x04001711 RID: 5905
	public const int ROADSIDE = 4096;

	// Token: 0x04001712 RID: 5906
	public const int BRIDGE = 8192;

	// Token: 0x04001713 RID: 5907
	public const int RIVER = 16384;

	// Token: 0x04001714 RID: 5908
	public const int RIVERSIDE = 32768;

	// Token: 0x04001715 RID: 5909
	public const int LAKE = 65536;

	// Token: 0x04001716 RID: 5910
	public const int LAKESIDE = 131072;

	// Token: 0x04001717 RID: 5911
	public const int OFFSHORE = 262144;

	// Token: 0x04001718 RID: 5912
	public const int POWERLINE = 524288;

	// Token: 0x04001719 RID: 5913
	public const int RUNWAY = 1048576;

	// Token: 0x0400171A RID: 5914
	public const int BUILDING = 2097152;

	// Token: 0x0400171B RID: 5915
	public const int CLIFFSIDE = 4194304;

	// Token: 0x0400171C RID: 5916
	public const int MOUNTAIN = 8388608;

	// Token: 0x0400171D RID: 5917
	public const int CLUTTER = 16777216;

	// Token: 0x0400171E RID: 5918
	public const int PADDING = 33554432;

	// Token: 0x0400171F RID: 5919
	public const int TIER0 = 67108864;

	// Token: 0x04001720 RID: 5920
	public const int TIER1 = 134217728;

	// Token: 0x04001721 RID: 5921
	public const int TIER2 = 268435456;

	// Token: 0x04001722 RID: 5922
	public const int MAINLAND = 536870912;

	// Token: 0x04001723 RID: 5923
	public const int HILLTOP = 1073741824;

	// Token: 0x04001724 RID: 5924
	public const int WATER = 82048;

	// Token: 0x04001725 RID: 5925
	public const int WATERSIDE = 164096;

	// Token: 0x04001726 RID: 5926
	public const int SAND = 197016;

	// Token: 0x02000534 RID: 1332
	public enum Enum
	{
		// Token: 0x04001728 RID: 5928
		Field = 1,
		// Token: 0x04001729 RID: 5929
		Cliff,
		// Token: 0x0400172A RID: 5930
		Summit = 4,
		// Token: 0x0400172B RID: 5931
		Beachside = 8,
		// Token: 0x0400172C RID: 5932
		Beach = 16,
		// Token: 0x0400172D RID: 5933
		Forest = 32,
		// Token: 0x0400172E RID: 5934
		Forestside = 64,
		// Token: 0x0400172F RID: 5935
		Ocean = 128,
		// Token: 0x04001730 RID: 5936
		Oceanside = 256,
		// Token: 0x04001731 RID: 5937
		Decor = 512,
		// Token: 0x04001732 RID: 5938
		Monument = 1024,
		// Token: 0x04001733 RID: 5939
		Road = 2048,
		// Token: 0x04001734 RID: 5940
		Roadside = 4096,
		// Token: 0x04001735 RID: 5941
		Bridge = 8192,
		// Token: 0x04001736 RID: 5942
		River = 16384,
		// Token: 0x04001737 RID: 5943
		Riverside = 32768,
		// Token: 0x04001738 RID: 5944
		Lake = 65536,
		// Token: 0x04001739 RID: 5945
		Lakeside = 131072,
		// Token: 0x0400173A RID: 5946
		Offshore = 262144,
		// Token: 0x0400173B RID: 5947
		Powerline = 524288,
		// Token: 0x0400173C RID: 5948
		Runway = 1048576,
		// Token: 0x0400173D RID: 5949
		Building = 2097152,
		// Token: 0x0400173E RID: 5950
		Cliffside = 4194304,
		// Token: 0x0400173F RID: 5951
		Mountain = 8388608,
		// Token: 0x04001740 RID: 5952
		Clutter = 16777216,
		// Token: 0x04001741 RID: 5953
		Padding = 33554432,
		// Token: 0x04001742 RID: 5954
		Tier0 = 67108864,
		// Token: 0x04001743 RID: 5955
		Tier1 = 134217728,
		// Token: 0x04001744 RID: 5956
		Tier2 = 268435456,
		// Token: 0x04001745 RID: 5957
		Mainland = 536870912,
		// Token: 0x04001746 RID: 5958
		Hilltop = 1073741824
	}
}
