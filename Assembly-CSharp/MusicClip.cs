﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001E4 RID: 484
public class MusicClip : ScriptableObject
{
	// Token: 0x06000F1E RID: 3870 RVA: 0x0005D060 File Offset: 0x0005B260
	public float GetNextFadeInPoint(float currentClipTimeBars)
	{
		if (this.fadeInPoints.Count == 0)
		{
			return currentClipTimeBars;
		}
		float result = -1f;
		float num = float.PositiveInfinity;
		for (int i = 0; i < this.fadeInPoints.Count; i++)
		{
			float num2 = this.fadeInPoints[i];
			float num3 = num2 - currentClipTimeBars;
			if (num2 > 0.01f)
			{
				if (num3 > 0f && num3 < num)
				{
					num = num3;
					result = num2;
				}
			}
		}
		return result;
	}

	// Token: 0x04000963 RID: 2403
	public AudioClip audioClip;

	// Token: 0x04000964 RID: 2404
	public int lengthInBars = 1;

	// Token: 0x04000965 RID: 2405
	public int lengthInBarsWithTail;

	// Token: 0x04000966 RID: 2406
	public List<float> fadeInPoints = new List<float>();
}
