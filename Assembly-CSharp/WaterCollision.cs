﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005EA RID: 1514
public class WaterCollision : MonoBehaviour
{
	// Token: 0x06001F06 RID: 7942 RVA: 0x000AF7EC File Offset: 0x000AD9EC
	private void Awake()
	{
		this.ignoredColliders = new ListDictionary<Collider, List<Collider>>(8);
		this.waterColliders = new HashSet<Collider>();
	}

	// Token: 0x06001F07 RID: 7943 RVA: 0x000AF808 File Offset: 0x000ADA08
	public void Clear()
	{
		if (this.waterColliders.Count == 0)
		{
			return;
		}
		HashSet<Collider>.Enumerator enumerator = this.waterColliders.GetEnumerator();
		while (enumerator.MoveNext())
		{
			foreach (Collider collider in this.ignoredColliders.Keys)
			{
				Physics.IgnoreCollision(collider, enumerator.Current, false);
			}
		}
		this.ignoredColliders.Clear();
	}

	// Token: 0x06001F08 RID: 7944 RVA: 0x000AF8A8 File Offset: 0x000ADAA8
	public void Reset(Collider collider)
	{
		if (this.waterColliders.Count == 0 || !collider)
		{
			return;
		}
		foreach (Collider collider2 in this.waterColliders)
		{
			Physics.IgnoreCollision(collider, collider2, false);
		}
		this.ignoredColliders.Remove(collider);
	}

	// Token: 0x06001F09 RID: 7945 RVA: 0x000AF90C File Offset: 0x000ADB0C
	public bool GetIgnore(Vector3 pos, float radius = 0.01f)
	{
		return global::GamePhysics.CheckSphere<global::WaterVisibilityTrigger>(pos, radius, 262144, 2);
	}

	// Token: 0x06001F0A RID: 7946 RVA: 0x000AF928 File Offset: 0x000ADB28
	public bool GetIgnore(Bounds bounds)
	{
		return global::GamePhysics.CheckBounds<global::WaterVisibilityTrigger>(bounds, 262144, 2);
	}

	// Token: 0x06001F0B RID: 7947 RVA: 0x000AF944 File Offset: 0x000ADB44
	public bool GetIgnore(RaycastHit hit)
	{
		return this.waterColliders.Contains(hit.collider) && this.GetIgnore(hit.point, 0.01f);
	}

	// Token: 0x06001F0C RID: 7948 RVA: 0x000AF974 File Offset: 0x000ADB74
	public bool GetIgnore(Collider collider)
	{
		return this.waterColliders.Count != 0 && collider && this.ignoredColliders.Contains(collider);
	}

	// Token: 0x06001F0D RID: 7949 RVA: 0x000AF9A0 File Offset: 0x000ADBA0
	public void SetIgnore(Collider collider, Collider trigger, bool ignore = true)
	{
		if (this.waterColliders.Count == 0 || !collider)
		{
			return;
		}
		if (!this.GetIgnore(collider))
		{
			if (ignore)
			{
				List<Collider> list = new List<Collider>
				{
					trigger
				};
				foreach (Collider collider2 in this.waterColliders)
				{
					Physics.IgnoreCollision(collider, collider2, true);
				}
				this.ignoredColliders.Add(collider, list);
			}
		}
		else
		{
			List<Collider> list2 = this.ignoredColliders[collider];
			if (ignore)
			{
				if (!list2.Contains(trigger))
				{
					list2.Add(trigger);
				}
			}
			else if (list2.Contains(trigger))
			{
				list2.Remove(trigger);
			}
		}
	}

	// Token: 0x06001F0E RID: 7950 RVA: 0x000AFA78 File Offset: 0x000ADC78
	protected void LateUpdate()
	{
		for (int i = 0; i < this.ignoredColliders.Count; i++)
		{
			KeyValuePair<Collider, List<Collider>> byIndex = this.ignoredColliders.GetByIndex(i);
			Collider key = byIndex.Key;
			List<Collider> value = byIndex.Value;
			if (key == null)
			{
				this.ignoredColliders.RemoveAt(i--);
			}
			else if (value.Count == 0)
			{
				foreach (Collider collider in this.waterColliders)
				{
					Physics.IgnoreCollision(key, collider, false);
				}
				this.ignoredColliders.RemoveAt(i--);
			}
		}
	}

	// Token: 0x04001A03 RID: 6659
	private ListDictionary<Collider, List<Collider>> ignoredColliders;

	// Token: 0x04001A04 RID: 6660
	private HashSet<Collider> waterColliders;
}
