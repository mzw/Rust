﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Token: 0x020002FF RID: 767
public class DevDressPlayer : MonoBehaviour
{
	// Token: 0x0600134B RID: 4939 RVA: 0x00071A10 File Offset: 0x0006FC10
	private void ServerInitComponent()
	{
		global::BasePlayer component = base.GetComponent<global::BasePlayer>();
		if (this.DressRandomly)
		{
			this.DoRandomClothes(component);
		}
		foreach (global::ItemAmount itemAmount in this.clothesToWear)
		{
			if (!(itemAmount.itemDef == null))
			{
				global::Item item = global::ItemManager.Create(itemAmount.itemDef, 1, 0UL);
				item.MoveToContainer(component.inventory.containerWear, -1, true);
			}
		}
	}

	// Token: 0x0600134C RID: 4940 RVA: 0x00071AB8 File Offset: 0x0006FCB8
	private void DoRandomClothes(global::BasePlayer player)
	{
		string text = string.Empty;
		IEnumerable<global::ItemDefinition> enumerable = (from x in global::ItemManager.GetItemDefinitions()
		where x.GetComponent<global::ItemModWearable>()
		orderby Guid.NewGuid()
		select x).Take(Random.Range(0, 4));
		foreach (global::ItemDefinition itemDefinition in enumerable)
		{
			global::Item item = global::ItemManager.Create(itemDefinition, 1, 0UL);
			item.MoveToContainer(player.inventory.containerWear, -1, true);
			text = text + itemDefinition.shortname + " ";
		}
		text = text.Trim();
		if (text == string.Empty)
		{
			text = "naked";
		}
		player.displayName = text;
	}

	// Token: 0x04000DFD RID: 3581
	public bool DressRandomly;

	// Token: 0x04000DFE RID: 3582
	public List<global::ItemAmount> clothesToWear;
}
