﻿using System;
using System.Diagnostics;

// Token: 0x0200036B RID: 875
public class EntityDebug : global::EntityComponent<global::BaseEntity>
{
	// Token: 0x060014EB RID: 5355 RVA: 0x00079118 File Offset: 0x00077318
	private void Update()
	{
		if (!base.baseEntity.IsValid() || !base.baseEntity.IsDebugging())
		{
			base.enabled = false;
			return;
		}
		if (this.stopwatch.Elapsed.TotalSeconds < 0.5)
		{
			return;
		}
		if (base.baseEntity.isClient)
		{
		}
		if (base.baseEntity.isServer)
		{
			base.baseEntity.DebugServer(1, (float)this.stopwatch.Elapsed.TotalSeconds);
		}
		this.stopwatch.Reset();
		this.stopwatch.Start();
	}

	// Token: 0x04000F5A RID: 3930
	internal Stopwatch stopwatch = Stopwatch.StartNew();
}
