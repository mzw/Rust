﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using ConVar;
using LZ4;
using UnityEngine;

// Token: 0x02000538 RID: 1336
public class WorldSerialization
{
	// Token: 0x06001C45 RID: 7237 RVA: 0x0009EEEC File Offset: 0x0009D0EC
	public WorldSerialization()
	{
		this.Version = 6u;
		this.Checksum = null;
		this.Cached = false;
	}

	// Token: 0x170001F6 RID: 502
	// (get) Token: 0x06001C46 RID: 7238 RVA: 0x0009EF14 File Offset: 0x0009D114
	public static string SaveFolder
	{
		get
		{
			return ConVar.Server.rootFolder;
		}
	}

	// Token: 0x170001F7 RID: 503
	// (get) Token: 0x06001C47 RID: 7239 RVA: 0x0009EF1C File Offset: 0x0009D11C
	public static string SaveFileName
	{
		get
		{
			return global::WorldSerialization.SaveFolder + "/" + global::World.FileName + ".map";
		}
	}

	// Token: 0x170001F8 RID: 504
	// (get) Token: 0x06001C48 RID: 7240 RVA: 0x0009EF38 File Offset: 0x0009D138
	public static string SaveFileRegex
	{
		get
		{
			return global::World.FileRegex + "\\.map";
		}
	}

	// Token: 0x170001F9 RID: 505
	// (get) Token: 0x06001C49 RID: 7241 RVA: 0x0009EF4C File Offset: 0x0009D14C
	// (set) Token: 0x06001C4A RID: 7242 RVA: 0x0009EF54 File Offset: 0x0009D154
	public uint Version { get; private set; }

	// Token: 0x170001FA RID: 506
	// (get) Token: 0x06001C4B RID: 7243 RVA: 0x0009EF60 File Offset: 0x0009D160
	// (set) Token: 0x06001C4C RID: 7244 RVA: 0x0009EF68 File Offset: 0x0009D168
	public bool Cached { get; private set; }

	// Token: 0x170001FB RID: 507
	// (get) Token: 0x06001C4D RID: 7245 RVA: 0x0009EF74 File Offset: 0x0009D174
	// (set) Token: 0x06001C4E RID: 7246 RVA: 0x0009EF7C File Offset: 0x0009D17C
	public string Checksum { get; private set; }

	// Token: 0x06001C4F RID: 7247 RVA: 0x0009EF88 File Offset: 0x0009D188
	public byte[] GetMap(string name)
	{
		for (int i = 0; i < this.world.maps.Count; i++)
		{
			if (this.world.maps[i].name == name)
			{
				return this.world.maps[i].data;
			}
		}
		return null;
	}

	// Token: 0x06001C50 RID: 7248 RVA: 0x0009EFF0 File Offset: 0x0009D1F0
	public void AddMap(string name, byte[] data)
	{
		global::WorldSerialization.MapData mapData = new global::WorldSerialization.MapData();
		mapData.name = name;
		mapData.data = data;
		this.world.maps.Add(mapData);
	}

	// Token: 0x06001C51 RID: 7249 RVA: 0x0009F024 File Offset: 0x0009D224
	public void AddPrefab(string category, uint id, Vector3 position, Quaternion rotation, Vector3 scale)
	{
		global::WorldSerialization.PrefabData prefabData = new global::WorldSerialization.PrefabData();
		prefabData.category = category;
		prefabData.id = id;
		prefabData.position = position;
		prefabData.rotation = rotation;
		prefabData.scale = scale;
		this.world.prefabs.Add(prefabData);
		if (!this.Cached)
		{
			this.Spawn(category, id, position, rotation, scale);
		}
	}

	// Token: 0x06001C52 RID: 7250 RVA: 0x0009F094 File Offset: 0x0009D294
	public IEnumerable<global::PathList> GetPaths(string name)
	{
		return from p in this.world.paths
		where p.name.Contains(name)
		select p.ToPathList();
	}

	// Token: 0x06001C53 RID: 7251 RVA: 0x0009F0EC File Offset: 0x0009D2EC
	public void AddPaths(IEnumerable<global::PathList> paths)
	{
		foreach (global::PathList source in paths)
		{
			this.AddPath(source);
		}
	}

	// Token: 0x06001C54 RID: 7252 RVA: 0x0009F140 File Offset: 0x0009D340
	public void AddPath(global::PathList source)
	{
		global::WorldSerialization.PathData pathData = new global::WorldSerialization.PathData();
		pathData.FromPathList(source);
		this.world.paths.Add(pathData);
	}

	// Token: 0x06001C55 RID: 7253 RVA: 0x0009F16C File Offset: 0x0009D36C
	public IEnumerator Spawn(float deltaTime, Action<string> statusFunction = null)
	{
		Stopwatch sw = Stopwatch.StartNew();
		for (int i = 0; i < this.world.prefabs.Count; i++)
		{
			if (sw.Elapsed.TotalSeconds > (double)deltaTime || i == 0 || i == this.world.prefabs.Count - 1)
			{
				global::WorldSerialization.Status(statusFunction, "Spawning World ({0}/{1})", i + 1, this.world.prefabs.Count);
				yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
				sw.Reset();
				sw.Start();
			}
			this.Spawn(this.world.prefabs[i]);
		}
		yield break;
	}

	// Token: 0x06001C56 RID: 7254 RVA: 0x0009F198 File Offset: 0x0009D398
	public void Spawn()
	{
		for (int i = 0; i < this.world.prefabs.Count; i++)
		{
			this.Spawn(this.world.prefabs[i]);
		}
	}

	// Token: 0x06001C57 RID: 7255 RVA: 0x0009F1E0 File Offset: 0x0009D3E0
	private void Spawn(global::WorldSerialization.PrefabData prefab)
	{
		this.Spawn(prefab.category, prefab.id, prefab.position, prefab.rotation, prefab.scale);
	}

	// Token: 0x06001C58 RID: 7256 RVA: 0x0009F218 File Offset: 0x0009D418
	private void Spawn(string category, uint id, Vector3 position, Quaternion rotation, Vector3 scale)
	{
		GameObject gameObject = global::Prefab.DefaultManager.CreatePrefab(global::StringPool.Get(id), position, rotation, scale, true);
		if (gameObject)
		{
			gameObject.SetHierarchyGroup(category, true, false);
		}
	}

	// Token: 0x06001C59 RID: 7257 RVA: 0x0009F250 File Offset: 0x0009D450
	public void Clear()
	{
		this.world.maps.Clear();
		this.world.prefabs.Clear();
		this.world.paths.Clear();
		this.Version = 6u;
		this.Checksum = null;
		this.Cached = false;
	}

	// Token: 0x06001C5A RID: 7258 RVA: 0x0009F2A4 File Offset: 0x0009D4A4
	public void CleanupOldFiles()
	{
		Regex regex = new Regex(global::WorldSerialization.SaveFileRegex);
		IEnumerable<string> enumerable = from path in Directory.GetFiles(global::WorldSerialization.SaveFolder, "*.map")
		where !regex.IsMatch(path)
		select path;
		foreach (string path2 in enumerable)
		{
			try
			{
				File.Delete(path2);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}
		}
	}

	// Token: 0x06001C5B RID: 7259 RVA: 0x0009F354 File Offset: 0x0009D554
	public bool CanLoadFromDisk()
	{
		return this.CanLoadFromDisk(global::WorldSerialization.SaveFileName);
	}

	// Token: 0x06001C5C RID: 7260 RVA: 0x0009F364 File Offset: 0x0009D564
	public bool CanLoadFromDisk(string fileName)
	{
		return File.Exists(fileName);
	}

	// Token: 0x06001C5D RID: 7261 RVA: 0x0009F36C File Offset: 0x0009D56C
	public void Save()
	{
		this.Save(global::WorldSerialization.SaveFileName);
	}

	// Token: 0x06001C5E RID: 7262 RVA: 0x0009F37C File Offset: 0x0009D57C
	public void Save(string fileName)
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		try
		{
			using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
				{
					binaryWriter.Write(this.Version);
					using (LZ4Stream lz4Stream = new LZ4Stream(fileStream, 0, 0, 1048576))
					{
						binaryFormatter.Serialize(lz4Stream, this.world);
					}
				}
			}
			this.Checksum = this.Hash();
			this.Cached = true;
		}
		catch (Exception ex)
		{
			Debug.LogError(ex.Message);
		}
	}

	// Token: 0x06001C5F RID: 7263 RVA: 0x0009F45C File Offset: 0x0009D65C
	public void Load()
	{
		this.Load(global::WorldSerialization.SaveFileName);
	}

	// Token: 0x06001C60 RID: 7264 RVA: 0x0009F46C File Offset: 0x0009D66C
	public void Load(string fileName)
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		try
		{
			using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				using (BinaryReader binaryReader = new BinaryReader(fileStream))
				{
					this.Version = binaryReader.ReadUInt32();
					if (this.Version == 6u)
					{
						using (LZ4Stream lz4Stream = new LZ4Stream(fileStream, 1, 0, 1048576))
						{
							this.world = (global::WorldSerialization.WorldData)binaryFormatter.Deserialize(lz4Stream);
						}
					}
				}
			}
			this.Checksum = this.Hash();
			this.Cached = true;
		}
		catch (Exception ex)
		{
			Debug.LogError(ex.Message);
		}
	}

	// Token: 0x06001C61 RID: 7265 RVA: 0x0009F560 File Offset: 0x0009D760
	public void CalculateChecksum()
	{
		this.Checksum = this.Hash();
	}

	// Token: 0x06001C62 RID: 7266 RVA: 0x0009F570 File Offset: 0x0009D770
	private string Hash()
	{
		global::Checksum checksum = new global::Checksum();
		byte[] map = this.GetMap("terrain");
		if (map != null)
		{
			for (int i = 0; i < map.Length; i += 2)
			{
				checksum.Add(map[i]);
			}
		}
		List<global::WorldSerialization.PrefabData> prefabs = this.world.prefabs;
		if (prefabs != null)
		{
			for (int j = 0; j < prefabs.Count; j++)
			{
				global::WorldSerialization.PrefabData prefabData = prefabs[j];
				checksum.Add(prefabData.id);
				checksum.Add(prefabData.position.x, 3);
				checksum.Add(prefabData.position.y, 3);
				checksum.Add(prefabData.position.z, 3);
				checksum.Add(prefabData.scale.x, 3);
				checksum.Add(prefabData.scale.y, 3);
				checksum.Add(prefabData.scale.z, 3);
			}
		}
		return checksum.MD5();
	}

	// Token: 0x06001C63 RID: 7267 RVA: 0x0009F670 File Offset: 0x0009D870
	private static void Status(Action<string> statusFunction, string status, object obj1)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, obj1));
		}
	}

	// Token: 0x06001C64 RID: 7268 RVA: 0x0009F688 File Offset: 0x0009D888
	private static void Status(Action<string> statusFunction, string status, object obj1, object obj2)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, obj1, obj2));
		}
	}

	// Token: 0x06001C65 RID: 7269 RVA: 0x0009F6A0 File Offset: 0x0009D8A0
	private static void Status(Action<string> statusFunction, string status, object obj1, object obj2, object obj3)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, obj1, obj2, obj3));
		}
	}

	// Token: 0x06001C66 RID: 7270 RVA: 0x0009F6B8 File Offset: 0x0009D8B8
	private static void Status(Action<string> statusFunction, string status, params object[] objs)
	{
		if (statusFunction != null)
		{
			statusFunction(string.Format(status, objs));
		}
	}

	// Token: 0x04001762 RID: 5986
	private global::WorldSerialization.WorldData world = new global::WorldSerialization.WorldData();

	// Token: 0x02000539 RID: 1337
	[Serializable]
	private class WorldData
	{
		// Token: 0x04001764 RID: 5988
		public List<global::WorldSerialization.MapData> maps = new List<global::WorldSerialization.MapData>();

		// Token: 0x04001765 RID: 5989
		public List<global::WorldSerialization.PrefabData> prefabs = new List<global::WorldSerialization.PrefabData>();

		// Token: 0x04001766 RID: 5990
		public List<global::WorldSerialization.PathData> paths = new List<global::WorldSerialization.PathData>();
	}

	// Token: 0x0200053A RID: 1338
	[Serializable]
	private class MapData
	{
		// Token: 0x04001767 RID: 5991
		public string name;

		// Token: 0x04001768 RID: 5992
		public byte[] data;
	}

	// Token: 0x0200053B RID: 1339
	[Serializable]
	private class PrefabData
	{
		// Token: 0x04001769 RID: 5993
		public string category;

		// Token: 0x0400176A RID: 5994
		public uint id;

		// Token: 0x0400176B RID: 5995
		public global::WorldSerialization.VectorData position;

		// Token: 0x0400176C RID: 5996
		public global::WorldSerialization.VectorData rotation;

		// Token: 0x0400176D RID: 5997
		public global::WorldSerialization.VectorData scale;
	}

	// Token: 0x0200053C RID: 1340
	[Serializable]
	private class PathData
	{
		// Token: 0x06001C6C RID: 7276 RVA: 0x0009F71C File Offset: 0x0009D91C
		public void FromPathList(global::PathList src)
		{
			this.name = src.Name;
			this.spline = src.Spline;
			this.start = src.Start;
			this.end = src.End;
			this.width = src.Width;
			this.innerPadding = src.InnerPadding;
			this.outerPadding = src.OuterPadding;
			this.innerFade = src.InnerFade;
			this.outerFade = src.OuterFade;
			this.randomScale = src.RandomScale;
			this.meshOffset = src.MeshOffset;
			this.terrainOffset = src.TerrainOffset;
			this.splat = src.Splat;
			this.topology = src.Topology;
			this.nodes = Array.ConvertAll<Vector3, global::WorldSerialization.VectorData>(src.Path.Points, (Vector3 item) => item);
		}

		// Token: 0x06001C6D RID: 7277 RVA: 0x0009F804 File Offset: 0x0009DA04
		public global::PathList ToPathList()
		{
			global::PathList pathList = new global::PathList(this.name, Array.ConvertAll<global::WorldSerialization.VectorData, Vector3>(this.nodes, (global::WorldSerialization.VectorData item) => item));
			pathList.Spline = this.spline;
			pathList.Start = this.start;
			pathList.End = this.end;
			pathList.Width = this.width;
			pathList.InnerPadding = this.innerPadding;
			pathList.OuterPadding = this.outerPadding;
			pathList.InnerFade = this.innerFade;
			pathList.OuterFade = this.outerFade;
			pathList.RandomScale = this.randomScale;
			pathList.MeshOffset = this.meshOffset;
			pathList.TerrainOffset = this.terrainOffset;
			pathList.Splat = this.splat;
			pathList.Topology = this.topology;
			pathList.Path.RecalculateTangents();
			return pathList;
		}

		// Token: 0x0400176E RID: 5998
		public string name;

		// Token: 0x0400176F RID: 5999
		public bool spline;

		// Token: 0x04001770 RID: 6000
		public bool start;

		// Token: 0x04001771 RID: 6001
		public bool end;

		// Token: 0x04001772 RID: 6002
		public float width;

		// Token: 0x04001773 RID: 6003
		public float innerPadding;

		// Token: 0x04001774 RID: 6004
		public float outerPadding;

		// Token: 0x04001775 RID: 6005
		public float innerFade;

		// Token: 0x04001776 RID: 6006
		public float outerFade;

		// Token: 0x04001777 RID: 6007
		public float randomScale;

		// Token: 0x04001778 RID: 6008
		public float meshOffset;

		// Token: 0x04001779 RID: 6009
		public float terrainOffset;

		// Token: 0x0400177A RID: 6010
		public int splat;

		// Token: 0x0400177B RID: 6011
		public int topology;

		// Token: 0x0400177C RID: 6012
		public global::WorldSerialization.VectorData[] nodes;
	}

	// Token: 0x0200053D RID: 1341
	[Serializable]
	private class VectorData
	{
		// Token: 0x06001C70 RID: 7280 RVA: 0x0009F900 File Offset: 0x0009DB00
		public VectorData(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		// Token: 0x06001C71 RID: 7281 RVA: 0x0009F920 File Offset: 0x0009DB20
		public static implicit operator global::WorldSerialization.VectorData(Vector3 v)
		{
			return new global::WorldSerialization.VectorData(v.x, v.y, v.z);
		}

		// Token: 0x06001C72 RID: 7282 RVA: 0x0009F93C File Offset: 0x0009DB3C
		public static implicit operator global::WorldSerialization.VectorData(Quaternion q)
		{
			return q.eulerAngles;
		}

		// Token: 0x06001C73 RID: 7283 RVA: 0x0009F94C File Offset: 0x0009DB4C
		public static implicit operator Vector3(global::WorldSerialization.VectorData v)
		{
			return new Vector3(v.x, v.y, v.z);
		}

		// Token: 0x06001C74 RID: 7284 RVA: 0x0009F968 File Offset: 0x0009DB68
		public static implicit operator Quaternion(global::WorldSerialization.VectorData v)
		{
			return Quaternion.Euler(v);
		}

		// Token: 0x0400177F RID: 6015
		public float x;

		// Token: 0x04001780 RID: 6016
		public float y;

		// Token: 0x04001781 RID: 6017
		public float z;
	}
}
