﻿using System;

// Token: 0x02000261 RID: 609
public class DecalScale : global::DecalComponent
{
	// Token: 0x04000B44 RID: 2884
	[global::MinMax(0f, 2f)]
	public global::MinMax range = new global::MinMax(0.9f, 1.1f);
}
