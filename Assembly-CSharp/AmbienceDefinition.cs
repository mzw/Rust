﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001CC RID: 460
[CreateAssetMenu(menuName = "Rust/Ambience Definition")]
public class AmbienceDefinition : ScriptableObject
{
	// Token: 0x040008C2 RID: 2242
	[Header("Sound")]
	public List<global::SoundDefinition> sounds;

	// Token: 0x040008C3 RID: 2243
	[Horizontal(2, -1)]
	public global::AmbienceDefinition.ValueRange stingFrequency = new global::AmbienceDefinition.ValueRange(15f, 30f);

	// Token: 0x040008C4 RID: 2244
	[Header("Environment")]
	[InspectorFlags]
	public global::TerrainBiome.Enum biomes = (global::TerrainBiome.Enum)(-1);

	// Token: 0x040008C5 RID: 2245
	[InspectorFlags]
	public global::TerrainTopology.Enum topologies = (global::TerrainTopology.Enum)(-1);

	// Token: 0x040008C6 RID: 2246
	public global::EnvironmentType environmentType = global::EnvironmentType.Underground;

	// Token: 0x040008C7 RID: 2247
	public bool useEnvironmentType;

	// Token: 0x040008C8 RID: 2248
	public AnimationCurve time = AnimationCurve.Linear(0f, 0f, 24f, 0f);

	// Token: 0x040008C9 RID: 2249
	[Horizontal(2, -1)]
	public global::AmbienceDefinition.ValueRange rain = new global::AmbienceDefinition.ValueRange(0f, 1f);

	// Token: 0x040008CA RID: 2250
	[Horizontal(2, -1)]
	public global::AmbienceDefinition.ValueRange wind = new global::AmbienceDefinition.ValueRange(0f, 1f);

	// Token: 0x040008CB RID: 2251
	[Horizontal(2, -1)]
	public global::AmbienceDefinition.ValueRange snow = new global::AmbienceDefinition.ValueRange(0f, 1f);

	// Token: 0x020001CD RID: 461
	[Serializable]
	public class ValueRange
	{
		// Token: 0x06000EF1 RID: 3825 RVA: 0x0005C560 File Offset: 0x0005A760
		public ValueRange(float min, float max)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x040008CC RID: 2252
		public float min;

		// Token: 0x040008CD RID: 2253
		public float max;
	}
}
