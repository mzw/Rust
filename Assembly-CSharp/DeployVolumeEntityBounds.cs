﻿using System;
using UnityEngine;

// Token: 0x02000411 RID: 1041
public class DeployVolumeEntityBounds : global::DeployVolume
{
	// Token: 0x060017EF RID: 6127 RVA: 0x0008898C File Offset: 0x00086B8C
	protected override bool Check(Vector3 position, Quaternion rotation, int mask = -1)
	{
		position += rotation * this.bounds.center;
		OBB obb;
		obb..ctor(position, this.bounds.size, rotation);
		return global::DeployVolume.CheckOBB(obb, this.layers & mask, this.ignore);
	}

	// Token: 0x060017F0 RID: 6128 RVA: 0x000889E8 File Offset: 0x00086BE8
	protected override bool Check(Vector3 position, Quaternion rotation, OBB obb, int mask = -1)
	{
		return false;
	}

	// Token: 0x060017F1 RID: 6129 RVA: 0x000889EC File Offset: 0x00086BEC
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		this.bounds = rootObj.GetComponent<global::BaseEntity>().bounds;
	}

	// Token: 0x04001282 RID: 4738
	private Bounds bounds = new Bounds(Vector3.zero, Vector3.one);
}
