﻿using System;
using UnityEngine;

// Token: 0x02000717 RID: 1815
public class UIBackgroundBlur : ListComponent<global::UIBackgroundBlur>, IClientComponent
{
	// Token: 0x17000270 RID: 624
	// (get) Token: 0x06002280 RID: 8832 RVA: 0x000C0FFC File Offset: 0x000BF1FC
	public static float currentMax
	{
		get
		{
			if (ListComponent<global::UIBackgroundBlur>.InstanceList.Count == 0)
			{
				return 0f;
			}
			float num = 0f;
			for (int i = 0; i < ListComponent<global::UIBackgroundBlur>.InstanceList.Count; i++)
			{
				num = Mathf.Max(ListComponent<global::UIBackgroundBlur>.InstanceList[i].amount, num);
			}
			return num;
		}
	}

	// Token: 0x04001EEB RID: 7915
	public float amount = 1f;
}
