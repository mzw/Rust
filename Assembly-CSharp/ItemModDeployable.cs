﻿using System;
using UnityEngine;

// Token: 0x020004E9 RID: 1257
public class ItemModDeployable : MonoBehaviour
{
	// Token: 0x06001AE1 RID: 6881 RVA: 0x00096870 File Offset: 0x00094A70
	public global::Deployable GetDeployable(global::BaseEntity entity)
	{
		GameObject gameObject = entity.gameManager.FindPrefab(this.entityPrefab.resourcePath);
		if (gameObject == null)
		{
			return null;
		}
		return entity.prefabAttribute.Find<global::Deployable>(this.entityPrefab.resourceID);
	}

	// Token: 0x06001AE2 RID: 6882 RVA: 0x000968B8 File Offset: 0x00094AB8
	internal void OnDeployed(global::BaseEntity ent, global::BasePlayer player)
	{
		if (player.IsValid() && !string.IsNullOrEmpty(this.UnlockAchievement))
		{
			player.GiveAchievement(this.UnlockAchievement);
		}
	}

	// Token: 0x040015AA RID: 5546
	public global::GameObjectRef entityPrefab = new global::GameObjectRef();

	// Token: 0x040015AB RID: 5547
	[Header("Tooltips")]
	public bool showCrosshair;

	// Token: 0x040015AC RID: 5548
	public string UnlockAchievement;
}
