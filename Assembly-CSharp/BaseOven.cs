﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000040 RID: 64
public class BaseOven : global::StorageContainer, global::ISplashable
{
	// Token: 0x06000529 RID: 1321 RVA: 0x0001D7F8 File Offset: 0x0001B9F8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseOven.OnRpcMessage", 0.1f))
		{
			if (rpc == 3611438935u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SVSwitch ");
				}
				using (TimeWarning.New("SVSwitch", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("SVSwitch", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SVSwitch(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in SVSwitch");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600052A RID: 1322 RVA: 0x0001D9D8 File Offset: 0x0001BBD8
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		if (base.IsOn())
		{
			this.StartCooking();
		}
	}

	// Token: 0x0600052B RID: 1323 RVA: 0x0001D9F4 File Offset: 0x0001BBF4
	public override void OnInventoryFirstCreated(global::ItemContainer container)
	{
		base.OnInventoryFirstCreated(container);
		if (this.startupContents == null)
		{
			return;
		}
		foreach (global::ItemAmount itemAmount in this.startupContents)
		{
			global::Item item = global::ItemManager.Create(itemAmount.itemDef, (int)itemAmount.amount, 0UL);
			item.MoveToContainer(container, -1, true);
		}
	}

	// Token: 0x0600052C RID: 1324 RVA: 0x0001DA54 File Offset: 0x0001BC54
	public override void OnItemAddedOrRemoved(global::Item item, bool bAdded)
	{
		base.OnItemAddedOrRemoved(item, bAdded);
		if (item != null && item.HasFlag(global::Item.Flag.OnFire))
		{
			item.SetFlag(global::Item.Flag.OnFire, false);
			item.MarkDirty();
		}
	}

	// Token: 0x0600052D RID: 1325 RVA: 0x0001DA80 File Offset: 0x0001BC80
	public void OvenFull()
	{
		this.StopCooking();
	}

	// Token: 0x0600052E RID: 1326 RVA: 0x0001DA88 File Offset: 0x0001BC88
	private global::Item FindBurnable()
	{
		object obj = Interface.CallHook("OnFindBurnable", new object[]
		{
			this
		});
		if (obj is global::Item)
		{
			return (global::Item)obj;
		}
		if (this.inventory == null)
		{
			return null;
		}
		foreach (global::Item item in this.inventory.itemList)
		{
			global::ItemModBurnable component = item.info.GetComponent<global::ItemModBurnable>();
			if (component && (this.fuelType == null || item.info == this.fuelType))
			{
				return item;
			}
		}
		return null;
	}

	// Token: 0x0600052F RID: 1327 RVA: 0x0001DB64 File Offset: 0x0001BD64
	public void Cook()
	{
		global::Item item = this.FindBurnable();
		if (item == null)
		{
			this.StopCooking();
			return;
		}
		this.inventory.OnCycle(0.5f);
		global::BaseEntity slot = base.GetSlot(global::BaseEntity.Slot.FireMod);
		if (slot)
		{
			slot.SendMessage("Cook", 0.5f, 1);
		}
		global::ItemModBurnable component = item.info.GetComponent<global::ItemModBurnable>();
		item.fuel -= 0.5f * (this.cookingTemperature / 200f);
		if (!item.HasFlag(global::Item.Flag.OnFire))
		{
			item.SetFlag(global::Item.Flag.OnFire, true);
			item.MarkDirty();
		}
		if (item.fuel <= 0f)
		{
			this.ConsumeFuel(item, component);
		}
	}

	// Token: 0x06000530 RID: 1328 RVA: 0x0001DC1C File Offset: 0x0001BE1C
	private void ConsumeFuel(global::Item fuel, global::ItemModBurnable burnable)
	{
		Interface.CallHook("OnConsumeFuel", new object[]
		{
			this,
			fuel,
			burnable
		});
		if (this.allowByproductCreation && burnable.byproductItem != null && Random.Range(0f, 1f) > burnable.byproductChance)
		{
			global::Item item = global::ItemManager.Create(burnable.byproductItem, burnable.byproductAmount, 0UL);
			if (!item.MoveToContainer(this.inventory, -1, true))
			{
				this.OvenFull();
				item.Drop(this.inventory.dropPosition, this.inventory.dropVelocity, default(Quaternion));
			}
		}
		if (fuel.amount <= 1)
		{
			fuel.Remove(0f);
			return;
		}
		fuel.amount--;
		fuel.fuel = burnable.fuelAmount;
		fuel.MarkDirty();
	}

	// Token: 0x06000531 RID: 1329 RVA: 0x0001DD0C File Offset: 0x0001BF0C
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void SVSwitch(global::BaseEntity.RPCMessage msg)
	{
		bool flag = msg.read.Bit();
		if (flag == base.IsOn())
		{
			return;
		}
		if (Interface.CallHook("OnOvenToggle", new object[]
		{
			this,
			msg.player
		}) != null)
		{
			return;
		}
		if (this.needsBuildingPrivilegeToUse && !msg.player.CanBuild())
		{
			return;
		}
		if (flag)
		{
			this.StartCooking();
		}
		else
		{
			this.StopCooking();
		}
	}

	// Token: 0x1700003D RID: 61
	// (get) Token: 0x06000532 RID: 1330 RVA: 0x0001DD8C File Offset: 0x0001BF8C
	private float cookingTemperature
	{
		get
		{
			switch (this.temperature)
			{
			case global::BaseOven.TemperatureType.Warming:
				return 50f;
			case global::BaseOven.TemperatureType.Cooking:
				return 200f;
			case global::BaseOven.TemperatureType.Smelting:
				return 1000f;
			case global::BaseOven.TemperatureType.Fractioning:
				return 1500f;
			default:
				return 15f;
			}
		}
	}

	// Token: 0x06000533 RID: 1331 RVA: 0x0001DDDC File Offset: 0x0001BFDC
	public void UpdateAttachmentTemperature()
	{
		global::BaseEntity slot = base.GetSlot(global::BaseEntity.Slot.FireMod);
		if (slot)
		{
			slot.SendMessage("ParentTemperatureUpdate", this.inventory.temperature, 1);
		}
	}

	// Token: 0x06000534 RID: 1332 RVA: 0x0001DE18 File Offset: 0x0001C018
	public virtual void StartCooking()
	{
		if (this.FindBurnable() == null)
		{
			return;
		}
		this.inventory.temperature = this.cookingTemperature;
		this.UpdateAttachmentTemperature();
		base.InvokeRepeating(new Action(this.Cook), 0.5f, 0.5f);
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
	}

	// Token: 0x06000535 RID: 1333 RVA: 0x0001DE70 File Offset: 0x0001C070
	public virtual void StopCooking()
	{
		this.UpdateAttachmentTemperature();
		if (this.inventory != null)
		{
			this.inventory.temperature = 15f;
			foreach (global::Item item in this.inventory.itemList)
			{
				if (item.HasFlag(global::Item.Flag.OnFire))
				{
					item.SetFlag(global::Item.Flag.OnFire, false);
					item.MarkDirty();
				}
			}
		}
		base.CancelInvoke(new Action(this.Cook));
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
	}

	// Token: 0x06000536 RID: 1334 RVA: 0x0001DF20 File Offset: 0x0001C120
	public bool wantsSplash(global::ItemDefinition splashType, int amount)
	{
		return base.IsOn() && this.disabledBySplash;
	}

	// Token: 0x06000537 RID: 1335 RVA: 0x0001DF38 File Offset: 0x0001C138
	public int DoSplash(global::ItemDefinition splashType, int amount)
	{
		this.StopCooking();
		return Mathf.Min(200, amount);
	}

	// Token: 0x06000538 RID: 1336 RVA: 0x0001DF4C File Offset: 0x0001C14C
	public override bool HasSlot(global::BaseEntity.Slot slot)
	{
		return (this.canModFire && slot == global::BaseEntity.Slot.FireMod) || base.HasSlot(slot);
	}

	// Token: 0x06000539 RID: 1337 RVA: 0x0001DF6C File Offset: 0x0001C16C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x040001F5 RID: 501
	public global::BaseOven.TemperatureType temperature;

	// Token: 0x040001F6 RID: 502
	public global::BaseEntity.Menu.Option switchOnMenu;

	// Token: 0x040001F7 RID: 503
	public global::BaseEntity.Menu.Option switchOffMenu;

	// Token: 0x040001F8 RID: 504
	public global::ItemAmount[] startupContents;

	// Token: 0x040001F9 RID: 505
	public bool allowByproductCreation = true;

	// Token: 0x040001FA RID: 506
	public global::ItemDefinition fuelType;

	// Token: 0x040001FB RID: 507
	public bool canModFire;

	// Token: 0x040001FC RID: 508
	public bool disabledBySplash = true;

	// Token: 0x040001FD RID: 509
	private const float UpdateRate = 0.5f;

	// Token: 0x02000041 RID: 65
	public enum TemperatureType
	{
		// Token: 0x040001FF RID: 511
		Normal,
		// Token: 0x04000200 RID: 512
		Warming,
		// Token: 0x04000201 RID: 513
		Cooking,
		// Token: 0x04000202 RID: 514
		Smelting,
		// Token: 0x04000203 RID: 515
		Fractioning
	}
}
