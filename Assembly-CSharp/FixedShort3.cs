﻿using System;
using UnityEngine;

// Token: 0x020007A9 RID: 1961
public struct FixedShort3
{
	// Token: 0x0600248A RID: 9354 RVA: 0x000C9538 File Offset: 0x000C7738
	public FixedShort3(Vector3 vec)
	{
		this.x = (short)(vec.x * 1024f);
		this.y = (short)(vec.y * 1024f);
		this.z = (short)(vec.z * 1024f);
	}

	// Token: 0x0600248B RID: 9355 RVA: 0x000C9578 File Offset: 0x000C7778
	public static explicit operator Vector3(global::FixedShort3 vec)
	{
		return new Vector3((float)vec.x * 0.0009765625f, (float)vec.y * 0.0009765625f, (float)vec.z * 0.0009765625f);
	}

	// Token: 0x04001FF2 RID: 8178
	private const int FracBits = 10;

	// Token: 0x04001FF3 RID: 8179
	private const float MaxFrac = 1024f;

	// Token: 0x04001FF4 RID: 8180
	private const float RcpMaxFrac = 0.0009765625f;

	// Token: 0x04001FF5 RID: 8181
	public short x;

	// Token: 0x04001FF6 RID: 8182
	public short y;

	// Token: 0x04001FF7 RID: 8183
	public short z;
}
