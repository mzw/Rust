﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020007A5 RID: 1957
public class TickInterpolator
{
	// Token: 0x06002475 RID: 9333 RVA: 0x000C9024 File Offset: 0x000C7224
	public void Reset()
	{
		this.index = 0;
		this.CurrentPoint = this.StartPoint;
	}

	// Token: 0x06002476 RID: 9334 RVA: 0x000C903C File Offset: 0x000C723C
	public void Reset(Vector3 point)
	{
		this.points.Clear();
		this.index = 0;
		this.Length = 0f;
		this.EndPoint = point;
		this.StartPoint = point;
		this.CurrentPoint = point;
	}

	// Token: 0x06002477 RID: 9335 RVA: 0x000C9080 File Offset: 0x000C7280
	public void AddPoint(Vector3 point)
	{
		global::TickInterpolator.Segment item = new global::TickInterpolator.Segment(this.EndPoint, point);
		this.points.Add(item);
		this.Length += item.length;
		this.EndPoint = item.point;
	}

	// Token: 0x06002478 RID: 9336 RVA: 0x000C90C8 File Offset: 0x000C72C8
	public bool MoveNext(float distance)
	{
		float num = 0f;
		while (num < distance && this.index < this.points.Count)
		{
			global::TickInterpolator.Segment segment = this.points[this.index];
			this.CurrentPoint = segment.point;
			num += segment.length;
			this.index++;
		}
		return num > 0f;
	}

	// Token: 0x06002479 RID: 9337 RVA: 0x000C913C File Offset: 0x000C733C
	public bool HasNext()
	{
		return this.index < this.points.Count;
	}

	// Token: 0x04001FE6 RID: 8166
	private List<global::TickInterpolator.Segment> points = new List<global::TickInterpolator.Segment>();

	// Token: 0x04001FE7 RID: 8167
	private int index;

	// Token: 0x04001FE8 RID: 8168
	public float Length;

	// Token: 0x04001FE9 RID: 8169
	public Vector3 CurrentPoint;

	// Token: 0x04001FEA RID: 8170
	public Vector3 StartPoint;

	// Token: 0x04001FEB RID: 8171
	public Vector3 EndPoint;

	// Token: 0x020007A6 RID: 1958
	private struct Segment
	{
		// Token: 0x0600247A RID: 9338 RVA: 0x000C9154 File Offset: 0x000C7354
		public Segment(Vector3 a, Vector3 b)
		{
			this.point = b;
			this.length = Vector3.Distance(a, b);
		}

		// Token: 0x04001FEC RID: 8172
		public Vector3 point;

		// Token: 0x04001FED RID: 8173
		public float length;
	}
}
