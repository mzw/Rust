﻿using System;
using UnityEngine;

// Token: 0x020007F2 RID: 2034
public class ExplosionDemoGUI : MonoBehaviour
{
	// Token: 0x0600259F RID: 9631 RVA: 0x000D012C File Offset: 0x000CE32C
	private void Start()
	{
		if (Screen.dpi < 1f)
		{
			this.dpiScale = 1f;
		}
		if (Screen.dpi < 200f)
		{
			this.dpiScale = 1f;
		}
		else
		{
			this.dpiScale = Screen.dpi / 200f;
		}
		this.guiStyleHeader.fontSize = (int)(15f * this.dpiScale);
		this.guiStyleHeader.normal.textColor = new Color(0.15f, 0.15f, 0.15f);
		this.currentInstance = Object.Instantiate<GameObject>(this.Prefabs[this.currentNomber], base.transform.position, default(Quaternion));
		global::ExplosionDemoReactivator explosionDemoReactivator = this.currentInstance.AddComponent<global::ExplosionDemoReactivator>();
		explosionDemoReactivator.TimeDelayToReactivate = this.reactivateTime;
		this.sunIntensity = this.Sun.intensity;
	}

	// Token: 0x060025A0 RID: 9632 RVA: 0x000D0214 File Offset: 0x000CE414
	private void OnGUI()
	{
		if (GUI.Button(new Rect(10f * this.dpiScale, 15f * this.dpiScale, 135f * this.dpiScale, 37f * this.dpiScale), "PREVIOUS EFFECT"))
		{
			this.ChangeCurrent(-1);
		}
		if (GUI.Button(new Rect(160f * this.dpiScale, 15f * this.dpiScale, 135f * this.dpiScale, 37f * this.dpiScale), "NEXT EFFECT"))
		{
			this.ChangeCurrent(1);
		}
		this.sunIntensity = GUI.HorizontalSlider(new Rect(10f * this.dpiScale, 70f * this.dpiScale, 285f * this.dpiScale, 15f * this.dpiScale), this.sunIntensity, 0f, 0.6f);
		this.Sun.intensity = this.sunIntensity;
		GUI.Label(new Rect(300f * this.dpiScale, 70f * this.dpiScale, 30f * this.dpiScale, 30f * this.dpiScale), "SUN INTENSITY", this.guiStyleHeader);
		GUI.Label(new Rect(400f * this.dpiScale, 15f * this.dpiScale, 100f * this.dpiScale, 20f * this.dpiScale), "Prefab name is \"" + this.Prefabs[this.currentNomber].name + "\"  \r\nHold any mouse button that would move the camera", this.guiStyleHeader);
	}

	// Token: 0x060025A1 RID: 9633 RVA: 0x000D03C0 File Offset: 0x000CE5C0
	private void ChangeCurrent(int delta)
	{
		this.currentNomber += delta;
		if (this.currentNomber > this.Prefabs.Length - 1)
		{
			this.currentNomber = 0;
		}
		else if (this.currentNomber < 0)
		{
			this.currentNomber = this.Prefabs.Length - 1;
		}
		if (this.currentInstance != null)
		{
			Object.Destroy(this.currentInstance);
		}
		this.currentInstance = Object.Instantiate<GameObject>(this.Prefabs[this.currentNomber], base.transform.position, default(Quaternion));
		global::ExplosionDemoReactivator explosionDemoReactivator = this.currentInstance.AddComponent<global::ExplosionDemoReactivator>();
		explosionDemoReactivator.TimeDelayToReactivate = this.reactivateTime;
	}

	// Token: 0x040021B8 RID: 8632
	public GameObject[] Prefabs;

	// Token: 0x040021B9 RID: 8633
	public float reactivateTime = 4f;

	// Token: 0x040021BA RID: 8634
	public Light Sun;

	// Token: 0x040021BB RID: 8635
	private int currentNomber;

	// Token: 0x040021BC RID: 8636
	private GameObject currentInstance;

	// Token: 0x040021BD RID: 8637
	private GUIStyle guiStyleHeader = new GUIStyle();

	// Token: 0x040021BE RID: 8638
	private float sunIntensity;

	// Token: 0x040021BF RID: 8639
	private float dpiScale;
}
