﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x020000CB RID: 203
public class BradleySpawner : MonoBehaviour, IServerComponent
{
	// Token: 0x06000ACA RID: 2762 RVA: 0x000494F4 File Offset: 0x000476F4
	public void Start()
	{
		global::BradleySpawner.singleton = this;
		base.Invoke("DelayedStart", 3f);
	}

	// Token: 0x06000ACB RID: 2763 RVA: 0x0004950C File Offset: 0x0004770C
	public void DelayedStart()
	{
		if (this.initialSpawn)
		{
			this.DoRespawn();
		}
		base.InvokeRepeating("CheckIfRespawnNeeded", 0f, 5f);
	}

	// Token: 0x06000ACC RID: 2764 RVA: 0x00049534 File Offset: 0x00047734
	public void CheckIfRespawnNeeded()
	{
		if (!this.pendingRespawn && (this.spawned == null || !this.spawned.IsAlive()))
		{
			this.ScheduleRespawn();
		}
	}

	// Token: 0x06000ACD RID: 2765 RVA: 0x00049568 File Offset: 0x00047768
	public void ScheduleRespawn()
	{
		base.CancelInvoke("DoRespawn");
		base.Invoke("DoRespawn", Random.Range(ConVar.Bradley.respawnDelayMinutes - ConVar.Bradley.respawnDelayVariance, ConVar.Bradley.respawnDelayMinutes + ConVar.Bradley.respawnDelayVariance) * 60f);
		this.pendingRespawn = true;
	}

	// Token: 0x06000ACE RID: 2766 RVA: 0x000495A8 File Offset: 0x000477A8
	public void DoRespawn()
	{
		this.SpawnBradley();
		this.pendingRespawn = false;
	}

	// Token: 0x06000ACF RID: 2767 RVA: 0x000495B8 File Offset: 0x000477B8
	public void SpawnBradley()
	{
		if (this.spawned != null)
		{
			Debug.LogWarning("Bradley attempting to spawn but one already exists!");
			return;
		}
		if (!ConVar.Bradley.enabled)
		{
			return;
		}
		Vector3 position = this.path.interestZones[Random.Range(0, this.path.interestZones.Count)].transform.position;
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.bradleyPrefab.resourcePath, position, default(Quaternion), true);
		global::BradleyAPC component = baseEntity.GetComponent<global::BradleyAPC>();
		if (component)
		{
			baseEntity.Spawn();
			component.InstallPatrolPath(this.path);
		}
		else
		{
			baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		Debug.Log("BradleyAPC Spawned at :" + position);
		this.spawned = component;
	}

	// Token: 0x04000591 RID: 1425
	public global::BasePath path;

	// Token: 0x04000592 RID: 1426
	public global::GameObjectRef bradleyPrefab;

	// Token: 0x04000593 RID: 1427
	[NonSerialized]
	public global::BradleyAPC spawned;

	// Token: 0x04000594 RID: 1428
	public bool initialSpawn;

	// Token: 0x04000595 RID: 1429
	public float minRespawnTimeMinutes = 5f;

	// Token: 0x04000596 RID: 1430
	public float maxRespawnTimeMinutes = 5f;

	// Token: 0x04000597 RID: 1431
	public static global::BradleySpawner singleton;

	// Token: 0x04000598 RID: 1432
	private bool pendingRespawn;
}
