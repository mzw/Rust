﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000A0 RID: 160
public class SurveyCrater : global::BaseCombatEntity
{
	// Token: 0x060009E8 RID: 2536 RVA: 0x00043260 File Offset: 0x00041460
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("SurveyCrater.OnRpcMessage", 0.1f))
		{
			if (rpc == 2008205333u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - AnalysisComplete ");
				}
				using (TimeWarning.New("AnalysisComplete", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.AnalysisComplete(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in AnalysisComplete");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009E9 RID: 2537 RVA: 0x000433B4 File Offset: 0x000415B4
	public override void ServerInit()
	{
		base.ServerInit();
		base.Invoke(new Action(this.RemoveMe), 1800f);
	}

	// Token: 0x060009EA RID: 2538 RVA: 0x000433D4 File Offset: 0x000415D4
	public override void OnAttacked(global::HitInfo info)
	{
		if (base.isServer)
		{
		}
		base.OnAttacked(info);
	}

	// Token: 0x060009EB RID: 2539 RVA: 0x000433E8 File Offset: 0x000415E8
	public void RemoveMe()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x060009EC RID: 2540 RVA: 0x000433F4 File Offset: 0x000415F4
	[global::BaseEntity.RPC_Server]
	public void AnalysisComplete(global::BaseEntity.RPCMessage msg)
	{
		global::ResourceDepositManager.ResourceDeposit orCreate = global::ResourceDepositManager.GetOrCreate(base.transform.position);
		if (orCreate == null)
		{
			return;
		}
		global::Item item = global::ItemManager.CreateByName("note", 1, 0UL);
		item.text = "-Mineral Analysis-\n\n";
		float num = 10f;
		float num2 = 7.5f;
		foreach (global::ResourceDepositManager.ResourceDeposit.ResourceDepositEntry resourceDepositEntry in orCreate._resources)
		{
			float num3 = 60f / num * (num2 / resourceDepositEntry.workNeeded);
			global::Item item2 = item;
			string text = item2.text;
			item2.text = string.Concat(new string[]
			{
				text,
				resourceDepositEntry.type.displayName.english,
				" : ",
				num3.ToString("0.0"),
				" pM\n"
			});
		}
		item.MarkDirty();
		msg.player.GiveItem(item, global::BaseEntity.GiveItemReason.PickedUp);
	}

	// Token: 0x060009ED RID: 2541 RVA: 0x00043500 File Offset: 0x00041700
	public override float BoundsPadding()
	{
		return 2f;
	}

	// Token: 0x0400049B RID: 1179
	private global::ResourceDispenser resourceDispenser;
}
