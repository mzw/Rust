﻿using System;
using System.Linq;
using UnityEngine;

// Token: 0x020004DB RID: 1243
public class ItemModConditionHasContents : global::ItemMod
{
	// Token: 0x06001ABC RID: 6844 RVA: 0x00095C5C File Offset: 0x00093E5C
	public override bool Passes(global::Item item)
	{
		if (item.contents == null)
		{
			return !this.requiredState;
		}
		if (item.contents.itemList.Count == 0)
		{
			return !this.requiredState;
		}
		if (this.itemDef && !item.contents.itemList.Any((global::Item x) => x.info == this.itemDef))
		{
			return !this.requiredState;
		}
		return this.requiredState;
	}

	// Token: 0x0400157D RID: 5501
	[Tooltip("Can be null to mean any item")]
	public global::ItemDefinition itemDef;

	// Token: 0x0400157E RID: 5502
	public bool requiredState;
}
