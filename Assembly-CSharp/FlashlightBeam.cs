﻿using System;
using UnityEngine;

// Token: 0x020000D5 RID: 213
public class FlashlightBeam : MonoBehaviour, IClientComponent
{
	// Token: 0x040005B3 RID: 1459
	public Vector2 scrollDir;

	// Token: 0x040005B4 RID: 1460
	public Vector3 localEndPoint = new Vector3(0f, 0f, 2f);

	// Token: 0x040005B5 RID: 1461
	public LineRenderer beamRenderer;
}
