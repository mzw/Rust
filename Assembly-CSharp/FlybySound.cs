﻿using System;
using UnityEngine;

// Token: 0x020001DA RID: 474
public class FlybySound : MonoBehaviour, IClientComponent
{
	// Token: 0x04000926 RID: 2342
	public global::SoundDefinition flybySound;

	// Token: 0x04000927 RID: 2343
	public float flybySoundDistance = 7f;

	// Token: 0x04000928 RID: 2344
	public global::SoundDefinition closeFlybySound;

	// Token: 0x04000929 RID: 2345
	public float closeFlybyDistance = 3f;
}
