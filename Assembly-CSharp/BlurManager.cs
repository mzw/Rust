﻿using System;
using UnityStandardAssets.ImageEffects;

// Token: 0x02000668 RID: 1640
public class BlurManager : ImageEffectLayer
{
	// Token: 0x04001BF7 RID: 7159
	public BlurOptimized blur;

	// Token: 0x04001BF8 RID: 7160
	public ColorCorrectionCurves color;

	// Token: 0x04001BF9 RID: 7161
	public float maxBlurScale;

	// Token: 0x04001BFA RID: 7162
	internal float blurAmount = 1f;

	// Token: 0x04001BFB RID: 7163
	internal float desaturationAmount = 0.6f;
}
