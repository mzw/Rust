﻿using System;
using UnityEngine;

// Token: 0x02000217 RID: 535
public class ModelConditionTest_FoundationSide : global::ModelConditionTest
{
	// Token: 0x06000FAB RID: 4011 RVA: 0x0005FB20 File Offset: 0x0005DD20
	protected void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.gray;
		Gizmos.DrawWireCube(new Vector3(1.5f, 1.5f, 0f), new Vector3(3f, 3f, 3f));
	}

	// Token: 0x06000FAC RID: 4012 RVA: 0x0005FB74 File Offset: 0x0005DD74
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		Vector3 vector = this.worldRotation * Vector3.right;
		if (name.Contains("foundation.triangle"))
		{
			if (vector.z < -0.9f)
			{
				this.socket = "foundation.triangle/sockets/foundation-top/1";
			}
			if (vector.x < -0.1f)
			{
				this.socket = "foundation.triangle/sockets/foundation-top/2";
			}
			if (vector.x > 0.1f)
			{
				this.socket = "foundation.triangle/sockets/foundation-top/3";
			}
		}
		else
		{
			if (vector.z < -0.9f)
			{
				this.socket = "foundation/sockets/foundation-top/1";
			}
			if (vector.z > 0.9f)
			{
				this.socket = "foundation/sockets/foundation-top/3";
			}
			if (vector.x < -0.9f)
			{
				this.socket = "foundation/sockets/foundation-top/2";
			}
			if (vector.x > 0.9f)
			{
				this.socket = "foundation/sockets/foundation-top/4";
			}
		}
	}

	// Token: 0x06000FAD RID: 4013 RVA: 0x0005FC6C File Offset: 0x0005DE6C
	public override bool DoTest(global::BaseEntity ent)
	{
		global::EntityLink entityLink = ent.FindLink(this.socket);
		if (entityLink == null)
		{
			return false;
		}
		for (int i = 0; i < entityLink.connections.Count; i++)
		{
			global::BuildingBlock buildingBlock = entityLink.connections[i].owner as global::BuildingBlock;
			if (!(buildingBlock == null))
			{
				if (!(buildingBlock.blockDefinition.info.name.token == "foundation_steps"))
				{
					if (buildingBlock.grade == global::BuildingGrade.Enum.TopTier)
					{
						return false;
					}
					if (buildingBlock.grade == global::BuildingGrade.Enum.Metal)
					{
						return false;
					}
					if (buildingBlock.grade == global::BuildingGrade.Enum.Stone)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	// Token: 0x04000A75 RID: 2677
	private const string square_south = "foundation/sockets/foundation-top/1";

	// Token: 0x04000A76 RID: 2678
	private const string square_north = "foundation/sockets/foundation-top/3";

	// Token: 0x04000A77 RID: 2679
	private const string square_west = "foundation/sockets/foundation-top/2";

	// Token: 0x04000A78 RID: 2680
	private const string square_east = "foundation/sockets/foundation-top/4";

	// Token: 0x04000A79 RID: 2681
	private const string triangle_south = "foundation.triangle/sockets/foundation-top/1";

	// Token: 0x04000A7A RID: 2682
	private const string triangle_northwest = "foundation.triangle/sockets/foundation-top/2";

	// Token: 0x04000A7B RID: 2683
	private const string triangle_northeast = "foundation.triangle/sockets/foundation-top/3";

	// Token: 0x04000A7C RID: 2684
	private string socket = string.Empty;
}
