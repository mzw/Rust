﻿using System;
using UnityEngine;

// Token: 0x02000548 RID: 1352
public class DecorScale : global::DecorComponent
{
	// Token: 0x06001C8E RID: 7310 RVA: 0x000A00D8 File Offset: 0x0009E2D8
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		uint num = SeedEx.Seed(pos, global::World.Seed) + 3u;
		float num2 = SeedRandom.Value(ref num);
		scale.x *= Mathf.Lerp(this.MinScale.x, this.MaxScale.x, num2);
		scale.y *= Mathf.Lerp(this.MinScale.y, this.MaxScale.y, num2);
		scale.z *= Mathf.Lerp(this.MinScale.z, this.MaxScale.z, num2);
	}

	// Token: 0x04001799 RID: 6041
	public Vector3 MinScale = new Vector3(1f, 1f, 1f);

	// Token: 0x0400179A RID: 6042
	public Vector3 MaxScale = new Vector3(2f, 2f, 2f);
}
