﻿using System;
using UnityEngine;

// Token: 0x02000608 RID: 1544
[CreateAssetMenu(menuName = "Rust/Environment Volume Properties Collection")]
public class EnvironmentVolumePropertiesCollection : ScriptableObject
{
	// Token: 0x06001F60 RID: 8032 RVA: 0x000B2208 File Offset: 0x000B0408
	public global::EnvironmentVolumeProperties FindQuality(int quality)
	{
		foreach (global::EnvironmentVolumeProperties environmentVolumeProperties in this.Properties)
		{
			if (environmentVolumeProperties.ReflectionQuality == quality)
			{
				return environmentVolumeProperties;
			}
		}
		return null;
	}

	// Token: 0x04001A5A RID: 6746
	public float TransitionSpeed = 1f;

	// Token: 0x04001A5B RID: 6747
	public global::EnvironmentVolumeProperties[] Properties;
}
