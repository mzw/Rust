﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000734 RID: 1844
public class BaseCommandBuffer : MonoBehaviour
{
	// Token: 0x060022B3 RID: 8883 RVA: 0x000C14C0 File Offset: 0x000BF6C0
	protected CommandBuffer GetCommandBuffer(string name, Camera camera, CameraEvent cameraEvent)
	{
		Dictionary<int, CommandBuffer> dictionary;
		if (!this.cameras.TryGetValue(camera, out dictionary))
		{
			dictionary = new Dictionary<int, CommandBuffer>();
			this.cameras.Add(camera, dictionary);
		}
		CommandBuffer commandBuffer;
		if (dictionary.TryGetValue(cameraEvent, out commandBuffer))
		{
			commandBuffer.Clear();
		}
		else
		{
			commandBuffer = new CommandBuffer();
			commandBuffer.name = name;
			dictionary.Add(cameraEvent, commandBuffer);
			camera.AddCommandBuffer(cameraEvent, commandBuffer);
		}
		return commandBuffer;
	}

	// Token: 0x060022B4 RID: 8884 RVA: 0x000C152C File Offset: 0x000BF72C
	protected void CleanupCommandBuffer(Camera camera, CameraEvent cameraEvent)
	{
		Dictionary<int, CommandBuffer> dictionary;
		if (!this.cameras.TryGetValue(camera, out dictionary))
		{
			return;
		}
		CommandBuffer commandBuffer;
		if (!dictionary.TryGetValue(cameraEvent, out commandBuffer))
		{
			return;
		}
		camera.RemoveCommandBuffer(cameraEvent, commandBuffer);
	}

	// Token: 0x060022B5 RID: 8885 RVA: 0x000C1564 File Offset: 0x000BF764
	protected void Cleanup()
	{
		foreach (KeyValuePair<Camera, Dictionary<int, CommandBuffer>> keyValuePair in this.cameras)
		{
			Camera key = keyValuePair.Key;
			Dictionary<int, CommandBuffer> value = keyValuePair.Value;
			if (key)
			{
				foreach (KeyValuePair<int, CommandBuffer> keyValuePair2 in value)
				{
					int key2 = keyValuePair2.Key;
					CommandBuffer value2 = keyValuePair2.Value;
					key.RemoveCommandBuffer(key2, value2);
				}
			}
		}
	}

	// Token: 0x04001F32 RID: 7986
	private Dictionary<Camera, Dictionary<int, CommandBuffer>> cameras = new Dictionary<Camera, Dictionary<int, CommandBuffer>>();
}
