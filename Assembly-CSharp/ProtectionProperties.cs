﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x02000651 RID: 1617
[CreateAssetMenu(menuName = "Rust/Protection Properties")]
public class ProtectionProperties : ScriptableObject
{
	// Token: 0x06002079 RID: 8313 RVA: 0x000B9A88 File Offset: 0x000B7C88
	public void OnValidate()
	{
		if (this.amounts.Length < 22)
		{
			float[] array = new float[22];
			for (int i = 0; i < array.Length; i++)
			{
				if (i >= this.amounts.Length)
				{
					if (i == 21)
					{
						array[i] = this.amounts[9];
					}
				}
				else
				{
					array[i] = this.amounts[i];
				}
			}
			this.amounts = array;
		}
	}

	// Token: 0x0600207A RID: 8314 RVA: 0x000B9AFC File Offset: 0x000B7CFC
	public void Clear()
	{
		for (int i = 0; i < this.amounts.Length; i++)
		{
			this.amounts[i] = 0f;
		}
	}

	// Token: 0x0600207B RID: 8315 RVA: 0x000B9B30 File Offset: 0x000B7D30
	public void Add(float amount)
	{
		for (int i = 0; i < this.amounts.Length; i++)
		{
			this.amounts[i] += amount;
		}
	}

	// Token: 0x0600207C RID: 8316 RVA: 0x000B9B68 File Offset: 0x000B7D68
	public void Add(Rust.DamageType index, float amount)
	{
		this.amounts[(int)index] += amount;
	}

	// Token: 0x0600207D RID: 8317 RVA: 0x000B9B7C File Offset: 0x000B7D7C
	public void Add(global::ProtectionProperties other, float scale)
	{
		for (int i = 0; i < Mathf.Min(other.amounts.Length, this.amounts.Length); i++)
		{
			this.amounts[i] += other.amounts[i] * scale;
		}
	}

	// Token: 0x0600207E RID: 8318 RVA: 0x000B9BCC File Offset: 0x000B7DCC
	public void Add(List<global::Item> items, global::HitArea area = (global::HitArea)-1)
	{
		for (int i = 0; i < items.Count; i++)
		{
			global::Item item = items[i];
			global::ItemModWearable component = item.info.GetComponent<global::ItemModWearable>();
			if (!(component == null))
			{
				if (component.ProtectsArea(area))
				{
					component.CollectProtection(item, this);
				}
			}
		}
	}

	// Token: 0x0600207F RID: 8319 RVA: 0x000B9C30 File Offset: 0x000B7E30
	public void Multiply(float multiplier)
	{
		for (int i = 0; i < this.amounts.Length; i++)
		{
			this.amounts[i] *= multiplier;
		}
	}

	// Token: 0x06002080 RID: 8320 RVA: 0x000B9C68 File Offset: 0x000B7E68
	public void Multiply(Rust.DamageType index, float multiplier)
	{
		this.amounts[(int)index] *= multiplier;
	}

	// Token: 0x06002081 RID: 8321 RVA: 0x000B9C7C File Offset: 0x000B7E7C
	public void Scale(Rust.DamageTypeList damageList, float ProtectionAmount = 1f)
	{
		for (int i = 0; i < this.amounts.Length; i++)
		{
			if (this.amounts[i] != 0f)
			{
				damageList.Scale((Rust.DamageType)i, 1f - this.amounts[i] * ProtectionAmount);
			}
		}
	}

	// Token: 0x06002082 RID: 8322 RVA: 0x000B9CD0 File Offset: 0x000B7ED0
	public float Get(Rust.DamageType damageType)
	{
		return this.amounts[(int)damageType];
	}

	// Token: 0x04001B6D RID: 7021
	[TextArea]
	public string comments;

	// Token: 0x04001B6E RID: 7022
	[Range(0f, 100f)]
	public float density = 1f;

	// Token: 0x04001B6F RID: 7023
	[ArrayIndexIsEnumRanged(enumType = typeof(Rust.DamageType), min = -4f, max = 1f)]
	public float[] amounts = new float[22];
}
