﻿using System;
using UnityEngine;

// Token: 0x02000307 RID: 775
public class DevWeatherAdjust : MonoBehaviour
{
	// Token: 0x0600135F RID: 4959 RVA: 0x000720D4 File Offset: 0x000702D4
	protected void Awake()
	{
		SingletonComponent<global::Climate>.Instance.Overrides.Clouds = 0f;
		SingletonComponent<global::Climate>.Instance.Overrides.Fog = 0f;
		SingletonComponent<global::Climate>.Instance.Overrides.Wind = 0f;
		SingletonComponent<global::Climate>.Instance.Overrides.Rain = 0f;
	}

	// Token: 0x06001360 RID: 4960 RVA: 0x00072134 File Offset: 0x00070334
	protected void OnGUI()
	{
		float num = (float)Screen.width * 0.2f;
		GUILayout.BeginArea(new Rect((float)Screen.width - num - 20f, 20f, num, 400f), string.Empty, "box");
		GUILayout.Box("Weather", new GUILayoutOption[0]);
		GUILayout.FlexibleSpace();
		GUILayout.Label("Clouds", new GUILayoutOption[0]);
		SingletonComponent<global::Climate>.Instance.Overrides.Clouds = GUILayout.HorizontalSlider(SingletonComponent<global::Climate>.Instance.Overrides.Clouds, 0f, 1f, new GUILayoutOption[0]);
		GUILayout.Label("Fog", new GUILayoutOption[0]);
		SingletonComponent<global::Climate>.Instance.Overrides.Fog = GUILayout.HorizontalSlider(SingletonComponent<global::Climate>.Instance.Overrides.Fog, 0f, 1f, new GUILayoutOption[0]);
		GUILayout.Label("Wind", new GUILayoutOption[0]);
		SingletonComponent<global::Climate>.Instance.Overrides.Wind = GUILayout.HorizontalSlider(SingletonComponent<global::Climate>.Instance.Overrides.Wind, 0f, 1f, new GUILayoutOption[0]);
		GUILayout.Label("Rain", new GUILayoutOption[0]);
		SingletonComponent<global::Climate>.Instance.Overrides.Rain = GUILayout.HorizontalSlider(SingletonComponent<global::Climate>.Instance.Overrides.Rain, 0f, 1f, new GUILayoutOption[0]);
		GUILayout.FlexibleSpace();
		GUILayout.EndArea();
	}
}
