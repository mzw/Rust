﻿using System;
using UnityEngine;

// Token: 0x0200074A RID: 1866
[CreateAssetMenu(menuName = "Rust/Material Config")]
public class MaterialConfig : ScriptableObject
{
	// Token: 0x060022FC RID: 8956 RVA: 0x000C24D4 File Offset: 0x000C06D4
	public MaterialPropertyBlock GetMaterialPropertyBlock(Material mat, Vector3 pos, Vector3 scale)
	{
		if (this.properties == null)
		{
			this.properties = new MaterialPropertyBlock();
		}
		this.properties.Clear();
		for (int i = 0; i < this.Floats.Length; i++)
		{
			global::MaterialConfig.ShaderParametersFloat shaderParametersFloat = this.Floats[i];
			float num2;
			float num3;
			float num = shaderParametersFloat.FindBlendParameters(pos, out num2, out num3);
			this.properties.SetFloat(shaderParametersFloat.Name, Mathf.Lerp(num2, num3, num));
		}
		for (int j = 0; j < this.Colors.Length; j++)
		{
			global::MaterialConfig.ShaderParametersColor shaderParametersColor = this.Colors[j];
			Color color;
			Color color2;
			float num4 = shaderParametersColor.FindBlendParameters(pos, out color, out color2);
			this.properties.SetColor(shaderParametersColor.Name, Color.Lerp(color, color2, num4));
		}
		for (int k = 0; k < this.Textures.Length; k++)
		{
			global::MaterialConfig.ShaderParametersTexture shaderParametersTexture = this.Textures[k];
			Texture texture = shaderParametersTexture.FindBlendParameters(pos);
			if (texture)
			{
				this.properties.SetTexture(shaderParametersTexture.Name, texture);
			}
		}
		for (int l = 0; l < this.ScaleUV.Length; l++)
		{
			Vector4 vector = mat.GetVector(this.ScaleUV[l]);
			vector..ctor(vector.x * scale.y, vector.y * scale.y, vector.z, vector.w);
			this.properties.SetVector(this.ScaleUV[l], vector);
		}
		return this.properties;
	}

	// Token: 0x04001F69 RID: 8041
	[Horizontal(4, 0)]
	public global::MaterialConfig.ShaderParametersFloat[] Floats;

	// Token: 0x04001F6A RID: 8042
	[Horizontal(4, 0)]
	public global::MaterialConfig.ShaderParametersColor[] Colors;

	// Token: 0x04001F6B RID: 8043
	[Horizontal(4, 0)]
	public global::MaterialConfig.ShaderParametersTexture[] Textures;

	// Token: 0x04001F6C RID: 8044
	public string[] ScaleUV;

	// Token: 0x04001F6D RID: 8045
	private MaterialPropertyBlock properties;

	// Token: 0x0200074B RID: 1867
	public class ShaderParameters<T>
	{
		// Token: 0x060022FE RID: 8958 RVA: 0x000C2678 File Offset: 0x000C0878
		public float FindBlendParameters(Vector3 pos, out T src, out T dst)
		{
			if (global::TerrainMeta.BiomeMap == null)
			{
				src = this.Temperate;
				dst = this.Tundra;
				return 0f;
			}
			if (this.climates == null || this.climates.Length == 0)
			{
				this.climates = new T[]
				{
					this.Arid,
					this.Temperate,
					this.Tundra,
					this.Arctic
				};
			}
			int biomeMaxType = global::TerrainMeta.BiomeMap.GetBiomeMaxType(pos, -1);
			int biomeMaxType2 = global::TerrainMeta.BiomeMap.GetBiomeMaxType(pos, ~biomeMaxType);
			src = this.climates[global::TerrainBiome.TypeToIndex(biomeMaxType)];
			dst = this.climates[global::TerrainBiome.TypeToIndex(biomeMaxType2)];
			return global::TerrainMeta.BiomeMap.GetBiome(pos, biomeMaxType2);
		}

		// Token: 0x060022FF RID: 8959 RVA: 0x000C2760 File Offset: 0x000C0960
		public T FindBlendParameters(Vector3 pos)
		{
			if (global::TerrainMeta.BiomeMap == null)
			{
				return this.Temperate;
			}
			if (this.climates == null || this.climates.Length == 0)
			{
				this.climates = new T[]
				{
					this.Arid,
					this.Temperate,
					this.Tundra,
					this.Arctic
				};
			}
			int biomeMaxType = global::TerrainMeta.BiomeMap.GetBiomeMaxType(pos, -1);
			return this.climates[global::TerrainBiome.TypeToIndex(biomeMaxType)];
		}

		// Token: 0x04001F6E RID: 8046
		public string Name;

		// Token: 0x04001F6F RID: 8047
		public T Arid;

		// Token: 0x04001F70 RID: 8048
		public T Temperate;

		// Token: 0x04001F71 RID: 8049
		public T Tundra;

		// Token: 0x04001F72 RID: 8050
		public T Arctic;

		// Token: 0x04001F73 RID: 8051
		private T[] climates;
	}

	// Token: 0x0200074C RID: 1868
	[Serializable]
	public class ShaderParametersFloat : global::MaterialConfig.ShaderParameters<float>
	{
	}

	// Token: 0x0200074D RID: 1869
	[Serializable]
	public class ShaderParametersColor : global::MaterialConfig.ShaderParameters<Color>
	{
	}

	// Token: 0x0200074E RID: 1870
	[Serializable]
	public class ShaderParametersTexture : global::MaterialConfig.ShaderParameters<Texture>
	{
	}
}
