﻿using System;
using UnityEngine;

// Token: 0x02000414 RID: 1044
public class DeployVolumeSphere : global::DeployVolume
{
	// Token: 0x060017FA RID: 6138 RVA: 0x00088C80 File Offset: 0x00086E80
	protected override bool Check(Vector3 position, Quaternion rotation, int mask = -1)
	{
		position += rotation * (this.worldRotation * this.center + this.worldPosition);
		return global::DeployVolume.CheckSphere(position, this.radius, this.layers & mask, this.ignore);
	}

	// Token: 0x060017FB RID: 6139 RVA: 0x00088CE0 File Offset: 0x00086EE0
	protected override bool Check(Vector3 position, Quaternion rotation, OBB obb, int mask = -1)
	{
		position += rotation * (this.worldRotation * this.center + this.worldPosition);
		return (this.layers & mask) != 0 && Vector3.Distance(position, obb.ClosestPoint(position)) <= this.radius;
	}

	// Token: 0x04001286 RID: 4742
	public Vector3 center = Vector3.zero;

	// Token: 0x04001287 RID: 4743
	public float radius = 0.5f;
}
