﻿using System;

// Token: 0x0200051A RID: 1306
public static class ManagedNoise
{
	// Token: 0x06001B9A RID: 7066 RVA: 0x0009A17C File Offset: 0x0009837C
	public static double Simplex1D(double x)
	{
		double num = 0.0;
		int num2 = global::ManagedNoise.Floor(x);
		int num3 = num2;
		double num4 = x - (double)num3;
		double num5 = 1.0 - num4 * num4;
		if (num5 > 0.0)
		{
			double num6 = num5 * num5;
			double num7 = num5 * num6;
			int num8 = global::ManagedNoise.hash[num3 & 255] & 1;
			double num9 = global::ManagedNoise.gradients1D[num8];
			double num10 = num9 * num4;
			num += num10 * num7;
		}
		int num11 = num2 + 1;
		double num12 = x - (double)num11;
		double num13 = 1.0 - num12 * num12;
		if (num13 > 0.0)
		{
			double num14 = num13 * num13;
			double num15 = num13 * num14;
			int num16 = global::ManagedNoise.hash[num11 & 255] & 1;
			double num17 = global::ManagedNoise.gradients1D[num16];
			double num18 = num17 * num12;
			num += num18 * num15;
		}
		return num * 2.4074074074074074;
	}

	// Token: 0x06001B9B RID: 7067 RVA: 0x0009A268 File Offset: 0x00098468
	public static double Simplex1D(double x, out double dx)
	{
		double num = 0.0;
		dx = 0.0;
		int num2 = global::ManagedNoise.Floor(x);
		int num3 = num2;
		double num4 = x - (double)num3;
		double num5 = 1.0 - num4 * num4;
		if (num5 > 0.0)
		{
			double num6 = num5 * num5;
			double num7 = num5 * num6;
			int num8 = global::ManagedNoise.hash[num3 & 255] & 1;
			double num9 = global::ManagedNoise.gradients1D[num8];
			double num10 = num9 * num4;
			double num11 = num10 * 6.0 * num6;
			dx += num9 * num7 - num11 * num4;
			num += num10 * num7;
		}
		int num12 = num2 + 1;
		double num13 = x - (double)num12;
		double num14 = 1.0 - num13 * num13;
		if (num14 > 0.0)
		{
			double num15 = num14 * num14;
			double num16 = num14 * num15;
			int num17 = global::ManagedNoise.hash[num12 & 255] & 1;
			double num18 = global::ManagedNoise.gradients1D[num17];
			double num19 = num18 * num13;
			double num20 = num19 * 6.0 * num15;
			dx += num18 * num16 - num20 * num13;
			num += num19 * num16;
		}
		return num * 2.4074074074074074;
	}

	// Token: 0x06001B9C RID: 7068 RVA: 0x0009A3A0 File Offset: 0x000985A0
	public static double Simplex2D(double x, double y)
	{
		double num = 0.0;
		double num2 = (x + y) * 0.36602540378443865;
		double num3 = x + num2;
		double num4 = y + num2;
		int num5 = global::ManagedNoise.Floor(num3);
		int num6 = global::ManagedNoise.Floor(num4);
		int num7 = num5;
		int num8 = num6;
		double num9 = (double)(num7 + num8) * 0.21132486540518711;
		double num10 = x - (double)num7 + num9;
		double num11 = y - (double)num8 + num9;
		double num12 = 0.5 - num10 * num10 - num11 * num11;
		if (num12 > 0.0)
		{
			double num13 = num12 * num12;
			double num14 = num12 * num13;
			int num15 = global::ManagedNoise.hash[global::ManagedNoise.hash[num7 & 255] + num8 & 255] & 7;
			double num16 = global::ManagedNoise.gradients2Dx[num15];
			double num17 = global::ManagedNoise.gradients2Dy[num15];
			double num18 = num16 * num10 + num17 * num11;
			num += num18 * num14;
		}
		int num19 = num5 + 1;
		int num20 = num6 + 1;
		double num21 = (double)(num19 + num20) * 0.21132486540518711;
		double num22 = x - (double)num19 + num21;
		double num23 = y - (double)num20 + num21;
		double num24 = 0.5 - num22 * num22 - num23 * num23;
		if (num24 > 0.0)
		{
			double num25 = num24 * num24;
			double num26 = num24 * num25;
			int num27 = global::ManagedNoise.hash[global::ManagedNoise.hash[num19 & 255] + num20 & 255] & 7;
			double num28 = global::ManagedNoise.gradients2Dx[num27];
			double num29 = global::ManagedNoise.gradients2Dy[num27];
			double num30 = num28 * num22 + num29 * num23;
			num += num30 * num26;
		}
		if (num3 - (double)num5 >= num4 - (double)num6)
		{
			int num31 = num5 + 1;
			int num32 = num6;
			double num33 = (double)(num31 + num32) * 0.21132486540518711;
			double num34 = x - (double)num31 + num33;
			double num35 = y - (double)num32 + num33;
			double num36 = 0.5 - num34 * num34 - num35 * num35;
			if (num36 > 0.0)
			{
				double num37 = num36 * num36;
				double num38 = num36 * num37;
				int num39 = global::ManagedNoise.hash[global::ManagedNoise.hash[num31 & 255] + num32 & 255] & 7;
				double num40 = global::ManagedNoise.gradients2Dx[num39];
				double num41 = global::ManagedNoise.gradients2Dy[num39];
				double num42 = num40 * num34 + num41 * num35;
				num += num42 * num38;
			}
		}
		else
		{
			int num43 = num5;
			int num44 = num6 + 1;
			double num45 = (double)(num43 + num44) * 0.21132486540518711;
			double num46 = x - (double)num43 + num45;
			double num47 = y - (double)num44 + num45;
			double num48 = 0.5 - num46 * num46 - num47 * num47;
			if (num48 > 0.0)
			{
				double num49 = num48 * num48;
				double num50 = num48 * num49;
				int num51 = global::ManagedNoise.hash[global::ManagedNoise.hash[num43 & 255] + num44 & 255] & 7;
				double num52 = global::ManagedNoise.gradients2Dx[num51];
				double num53 = global::ManagedNoise.gradients2Dy[num51];
				double num54 = num52 * num46 + num53 * num47;
				num += num54 * num50;
			}
		}
		return num * 32.99077398303956;
	}

	// Token: 0x06001B9D RID: 7069 RVA: 0x0009A6B8 File Offset: 0x000988B8
	public static double Simplex2D(double x, double y, out double dx, out double dy)
	{
		double num = 0.0;
		dx = 0.0;
		dy = 0.0;
		double num2 = (x + y) * 0.36602540378443865;
		double num3 = x + num2;
		double num4 = y + num2;
		int num5 = global::ManagedNoise.Floor(num3);
		int num6 = global::ManagedNoise.Floor(num4);
		int num7 = num5;
		int num8 = num6;
		double num9 = (double)(num7 + num8) * 0.21132486540518711;
		double num10 = x - (double)num7 + num9;
		double num11 = y - (double)num8 + num9;
		double num12 = 0.5 - num10 * num10 - num11 * num11;
		if (num12 > 0.0)
		{
			double num13 = num12 * num12;
			double num14 = num12 * num13;
			int num15 = global::ManagedNoise.hash[global::ManagedNoise.hash[num7 & 255] + num8 & 255] & 7;
			double num16 = global::ManagedNoise.gradients2Dx[num15];
			double num17 = global::ManagedNoise.gradients2Dy[num15];
			double num18 = num16 * num10 + num17 * num11;
			double num19 = num18 * 6.0 * num13;
			dx += num16 * num14 - num19 * num10;
			dy += num17 * num14 - num19 * num11;
			num += num18 * num14;
		}
		int num20 = num5 + 1;
		int num21 = num6 + 1;
		double num22 = (double)(num20 + num21) * 0.21132486540518711;
		double num23 = x - (double)num20 + num22;
		double num24 = y - (double)num21 + num22;
		double num25 = 0.5 - num23 * num23 - num24 * num24;
		if (num25 > 0.0)
		{
			double num26 = num25 * num25;
			double num27 = num25 * num26;
			int num28 = global::ManagedNoise.hash[global::ManagedNoise.hash[num20 & 255] + num21 & 255] & 7;
			double num29 = global::ManagedNoise.gradients2Dx[num28];
			double num30 = global::ManagedNoise.gradients2Dy[num28];
			double num31 = num29 * num23 + num30 * num24;
			double num32 = num31 * 6.0 * num26;
			dx += num29 * num27 - num32 * num23;
			dy += num30 * num27 - num32 * num24;
			num += num31 * num27;
		}
		if (num3 - (double)num5 >= num4 - (double)num6)
		{
			int num33 = num5 + 1;
			int num34 = num6;
			double num35 = (double)(num33 + num34) * 0.21132486540518711;
			double num36 = x - (double)num33 + num35;
			double num37 = y - (double)num34 + num35;
			double num38 = 0.5 - num36 * num36 - num37 * num37;
			if (num38 > 0.0)
			{
				double num39 = num38 * num38;
				double num40 = num38 * num39;
				int num41 = global::ManagedNoise.hash[global::ManagedNoise.hash[num33 & 255] + num34 & 255] & 7;
				double num42 = global::ManagedNoise.gradients2Dx[num41];
				double num43 = global::ManagedNoise.gradients2Dy[num41];
				double num44 = num42 * num36 + num43 * num37;
				double num45 = num44 * 6.0 * num39;
				dx += num42 * num40 - num45 * num36;
				dy += num43 * num40 - num45 * num37;
				num += num44 * num40;
			}
		}
		else
		{
			int num46 = num5;
			int num47 = num6 + 1;
			double num48 = (double)(num46 + num47) * 0.21132486540518711;
			double num49 = x - (double)num46 + num48;
			double num50 = y - (double)num47 + num48;
			double num51 = 0.5 - num49 * num49 - num50 * num50;
			if (num51 > 0.0)
			{
				double num52 = num51 * num51;
				double num53 = num51 * num52;
				int num54 = global::ManagedNoise.hash[global::ManagedNoise.hash[num46 & 255] + num47 & 255] & 7;
				double num55 = global::ManagedNoise.gradients2Dx[num54];
				double num56 = global::ManagedNoise.gradients2Dy[num54];
				double num57 = num55 * num49 + num56 * num50;
				double num58 = num57 * 6.0 * num52;
				dx += num55 * num53 - num58 * num49;
				dy += num56 * num53 - num58 * num50;
				num += num57 * num53;
			}
		}
		dx *= 4.0;
		dy *= 4.0;
		return num * 32.99077398303956;
	}

	// Token: 0x06001B9E RID: 7070 RVA: 0x0009AAC8 File Offset: 0x00098CC8
	public static double Turbulence(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		for (int i = 0; i < octaves; i++)
		{
			double num4 = global::ManagedNoise.Simplex2D(x * num2, y * num2);
			num += num3 * num4;
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001B9F RID: 7071 RVA: 0x0009AB34 File Offset: 0x00098D34
	public static double Billow(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		for (int i = 0; i < octaves; i++)
		{
			double x2 = global::ManagedNoise.Simplex2D(x * num2, y * num2);
			num += num3 * global::ManagedNoise.Abs(x2);
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA0 RID: 7072 RVA: 0x0009ABA4 File Offset: 0x00098DA4
	public static double Ridge(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		for (int i = 0; i < octaves; i++)
		{
			double x2 = global::ManagedNoise.Simplex2D(x * num2, y * num2);
			num += num3 * (1.0 - global::ManagedNoise.Abs(x2));
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA1 RID: 7073 RVA: 0x0009AC20 File Offset: 0x00098E20
	public static double Sharp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		for (int i = 0; i < octaves; i++)
		{
			double num4 = global::ManagedNoise.Simplex2D(x * num2, y * num2);
			num += num3 * (num4 * num4);
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA2 RID: 7074 RVA: 0x0009AC90 File Offset: 0x00098E90
	public static double TurbulenceIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num7;
			double num8;
			double num6 = global::ManagedNoise.Simplex2D(x * num2, y * num2, out num7, out num8);
			num4 += num7;
			num5 += num8;
			num += num3 * num6 / (1.0 + (num4 * num4 + num5 * num5));
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA3 RID: 7075 RVA: 0x0009AD40 File Offset: 0x00098F40
	public static double BillowIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num6;
			double num7;
			double x2 = global::ManagedNoise.Simplex2D(x * num2, y * num2, out num6, out num7);
			num4 += num6;
			num5 += num7;
			num += num3 * global::ManagedNoise.Abs(x2) / (1.0 + (num4 * num4 + num5 * num5));
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA4 RID: 7076 RVA: 0x0009ADF8 File Offset: 0x00098FF8
	public static double RidgeIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num6;
			double num7;
			double x2 = global::ManagedNoise.Simplex2D(x * num2, y * num2, out num6, out num7);
			num4 += num6;
			num5 += num7;
			num += num3 * (1.0 - global::ManagedNoise.Abs(x2)) / (1.0 + (num4 * num4 + num5 * num5));
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA5 RID: 7077 RVA: 0x0009AEB8 File Offset: 0x000990B8
	public static double SharpIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num7;
			double num8;
			double num6 = global::ManagedNoise.Simplex2D(x * num2, y * num2, out num7, out num8);
			num4 += num7;
			num5 += num8;
			num += num3 * (num6 * num6) / (1.0 + (num4 * num4 + num5 * num5));
			num2 *= lacunarity;
			num3 *= gain;
		}
		return num * amplitude;
	}

	// Token: 0x06001BA6 RID: 7078 RVA: 0x0009AF6C File Offset: 0x0009916C
	public static double TurbulenceWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num7;
			double num8;
			double num6 = global::ManagedNoise.Simplex2D((x + warp * num4) * num2, (y + warp * num5) * num2, out num7, out num8);
			num += num3 * num6;
			num4 += num3 * num7 * -num6;
			num5 += num3 * num8 * -num6;
			num2 *= lacunarity;
			num3 *= gain * global::ManagedNoise.Saturate(num);
		}
		return num * amplitude;
	}

	// Token: 0x06001BA7 RID: 7079 RVA: 0x0009B028 File Offset: 0x00099228
	public static double BillowWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num7;
			double num8;
			double num6 = global::ManagedNoise.Simplex2D((x + warp * num4) * num2, (y + warp * num5) * num2, out num7, out num8);
			num += num3 * global::ManagedNoise.Abs(num6);
			num4 += num3 * num7 * -num6;
			num5 += num3 * num8 * -num6;
			num2 *= lacunarity;
			num3 *= gain * global::ManagedNoise.Saturate(num);
		}
		return num * amplitude;
	}

	// Token: 0x06001BA8 RID: 7080 RVA: 0x0009B0E8 File Offset: 0x000992E8
	public static double RidgeWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num7;
			double num8;
			double num6 = global::ManagedNoise.Simplex2D((x + warp * num4) * num2, (y + warp * num5) * num2, out num7, out num8);
			num += num3 * (1.0 - global::ManagedNoise.Abs(num6));
			num4 += num3 * num7 * -num6;
			num5 += num3 * num8 * -num6;
			num2 *= lacunarity;
			num3 *= gain * global::ManagedNoise.Saturate(num);
		}
		return num * amplitude;
	}

	// Token: 0x06001BA9 RID: 7081 RVA: 0x0009B1B0 File Offset: 0x000993B0
	public static double SharpWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		for (int i = 0; i < octaves; i++)
		{
			double num7;
			double num8;
			double num6 = global::ManagedNoise.Simplex2D((x + warp * num4) * num2, (y + warp * num5) * num2, out num7, out num8);
			num += num3 * (num6 * num6);
			num4 += num3 * num7 * -num6;
			num5 += num3 * num8 * -num6;
			num2 *= lacunarity;
			num3 *= gain * global::ManagedNoise.Saturate(num);
		}
		return num * amplitude;
	}

	// Token: 0x06001BAA RID: 7082 RVA: 0x0009B26C File Offset: 0x0009946C
	public static double Jordan(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp, double damp, double damp_scale)
	{
		x *= frequency;
		y *= frequency;
		double num = 0.0;
		double num2 = 1.0;
		double num3 = 1.0;
		double num4 = 0.0;
		double num5 = 0.0;
		double num6 = 0.0;
		double num7 = 0.0;
		double num8 = num2 * gain;
		for (int i = 0; i < octaves; i++)
		{
			double num10;
			double num11;
			double num9 = global::ManagedNoise.Simplex2D(x * num3 + num4, y * num3 + num5, out num10, out num11);
			double num12 = num9 * num9;
			double num13 = num10 * num9;
			double num14 = num11 * num9;
			num += num8 * num12;
			num4 += warp * num13;
			num5 += warp * num14;
			num6 += damp * num13;
			num7 += damp * num14;
			num3 *= lacunarity;
			num2 *= gain;
			num8 = num2 * (1.0 - damp_scale / (1.0 + (num6 * num6 + num7 * num7)));
		}
		return num * amplitude;
	}

	// Token: 0x06001BAB RID: 7083 RVA: 0x0009B380 File Offset: 0x00099580
	private static int Floor(double x)
	{
		return (x < 0.0) ? ((int)x - 1) : ((int)x);
	}

	// Token: 0x06001BAC RID: 7084 RVA: 0x0009B39C File Offset: 0x0009959C
	private static double Abs(double x)
	{
		return (x < 0.0) ? (-x) : x;
	}

	// Token: 0x06001BAD RID: 7085 RVA: 0x0009B3B8 File Offset: 0x000995B8
	private static double Saturate(double x)
	{
		return (x <= 1.0) ? ((x >= 0.0) ? x : 0.0) : 1.0;
	}

	// Token: 0x0400164B RID: 5707
	private static readonly int[] hash = new int[]
	{
		151,
		160,
		137,
		91,
		90,
		15,
		131,
		13,
		201,
		95,
		96,
		53,
		194,
		233,
		7,
		225,
		140,
		36,
		103,
		30,
		69,
		142,
		8,
		99,
		37,
		240,
		21,
		10,
		23,
		190,
		6,
		148,
		247,
		120,
		234,
		75,
		0,
		26,
		197,
		62,
		94,
		252,
		219,
		203,
		117,
		35,
		11,
		32,
		57,
		177,
		33,
		88,
		237,
		149,
		56,
		87,
		174,
		20,
		125,
		136,
		171,
		168,
		68,
		175,
		74,
		165,
		71,
		134,
		139,
		48,
		27,
		166,
		77,
		146,
		158,
		231,
		83,
		111,
		229,
		122,
		60,
		211,
		133,
		230,
		220,
		105,
		92,
		41,
		55,
		46,
		245,
		40,
		244,
		102,
		143,
		54,
		65,
		25,
		63,
		161,
		1,
		216,
		80,
		73,
		209,
		76,
		132,
		187,
		208,
		89,
		18,
		169,
		200,
		196,
		135,
		130,
		116,
		188,
		159,
		86,
		164,
		100,
		109,
		198,
		173,
		186,
		3,
		64,
		52,
		217,
		226,
		250,
		124,
		123,
		5,
		202,
		38,
		147,
		118,
		126,
		255,
		82,
		85,
		212,
		207,
		206,
		59,
		227,
		47,
		16,
		58,
		17,
		182,
		189,
		28,
		42,
		223,
		183,
		170,
		213,
		119,
		248,
		152,
		2,
		44,
		154,
		163,
		70,
		221,
		153,
		101,
		155,
		167,
		43,
		172,
		9,
		129,
		22,
		39,
		253,
		19,
		98,
		108,
		110,
		79,
		113,
		224,
		232,
		178,
		185,
		112,
		104,
		218,
		246,
		97,
		228,
		251,
		34,
		242,
		193,
		238,
		210,
		144,
		12,
		191,
		179,
		162,
		241,
		81,
		51,
		145,
		235,
		249,
		14,
		239,
		107,
		49,
		192,
		214,
		31,
		181,
		199,
		106,
		157,
		184,
		84,
		204,
		176,
		115,
		121,
		50,
		45,
		127,
		4,
		150,
		254,
		138,
		236,
		205,
		93,
		222,
		114,
		67,
		29,
		24,
		72,
		243,
		141,
		128,
		195,
		78,
		66,
		215,
		61,
		156,
		180,
		151,
		160,
		137,
		91,
		90,
		15,
		131,
		13,
		201,
		95,
		96,
		53,
		194,
		233,
		7,
		225,
		140,
		36,
		103,
		30,
		69,
		142,
		8,
		99,
		37,
		240,
		21,
		10,
		23,
		190,
		6,
		148,
		247,
		120,
		234,
		75,
		0,
		26,
		197,
		62,
		94,
		252,
		219,
		203,
		117,
		35,
		11,
		32,
		57,
		177,
		33,
		88,
		237,
		149,
		56,
		87,
		174,
		20,
		125,
		136,
		171,
		168,
		68,
		175,
		74,
		165,
		71,
		134,
		139,
		48,
		27,
		166,
		77,
		146,
		158,
		231,
		83,
		111,
		229,
		122,
		60,
		211,
		133,
		230,
		220,
		105,
		92,
		41,
		55,
		46,
		245,
		40,
		244,
		102,
		143,
		54,
		65,
		25,
		63,
		161,
		1,
		216,
		80,
		73,
		209,
		76,
		132,
		187,
		208,
		89,
		18,
		169,
		200,
		196,
		135,
		130,
		116,
		188,
		159,
		86,
		164,
		100,
		109,
		198,
		173,
		186,
		3,
		64,
		52,
		217,
		226,
		250,
		124,
		123,
		5,
		202,
		38,
		147,
		118,
		126,
		255,
		82,
		85,
		212,
		207,
		206,
		59,
		227,
		47,
		16,
		58,
		17,
		182,
		189,
		28,
		42,
		223,
		183,
		170,
		213,
		119,
		248,
		152,
		2,
		44,
		154,
		163,
		70,
		221,
		153,
		101,
		155,
		167,
		43,
		172,
		9,
		129,
		22,
		39,
		253,
		19,
		98,
		108,
		110,
		79,
		113,
		224,
		232,
		178,
		185,
		112,
		104,
		218,
		246,
		97,
		228,
		251,
		34,
		242,
		193,
		238,
		210,
		144,
		12,
		191,
		179,
		162,
		241,
		81,
		51,
		145,
		235,
		249,
		14,
		239,
		107,
		49,
		192,
		214,
		31,
		181,
		199,
		106,
		157,
		184,
		84,
		204,
		176,
		115,
		121,
		50,
		45,
		127,
		4,
		150,
		254,
		138,
		236,
		205,
		93,
		222,
		114,
		67,
		29,
		24,
		72,
		243,
		141,
		128,
		195,
		78,
		66,
		215,
		61,
		156,
		180
	};

	// Token: 0x0400164C RID: 5708
	private const int hashMask = 255;

	// Token: 0x0400164D RID: 5709
	private const double sqrt2 = 1.4142135623730951;

	// Token: 0x0400164E RID: 5710
	private const double rsqrt2 = 0.70710678118654757;

	// Token: 0x0400164F RID: 5711
	private const double squaresToTriangles = 0.21132486540518711;

	// Token: 0x04001650 RID: 5712
	private const double trianglesToSquares = 0.36602540378443865;

	// Token: 0x04001651 RID: 5713
	private const double simplexScale1D = 2.4074074074074074;

	// Token: 0x04001652 RID: 5714
	private const double simplexScale2D = 32.99077398303956;

	// Token: 0x04001653 RID: 5715
	private const double gradientScale2D = 4.0;

	// Token: 0x04001654 RID: 5716
	private static double[] gradients1D = new double[]
	{
		1.0,
		-1.0
	};

	// Token: 0x04001655 RID: 5717
	private const int gradientsMask1D = 1;

	// Token: 0x04001656 RID: 5718
	private static double[] gradients2Dx = new double[]
	{
		1.0,
		-1.0,
		0.0,
		0.0,
		0.70710678118654757,
		-0.70710678118654757,
		0.70710678118654757,
		-0.70710678118654757
	};

	// Token: 0x04001657 RID: 5719
	private static double[] gradients2Dy = new double[]
	{
		0.0,
		0.0,
		1.0,
		-1.0,
		0.70710678118654757,
		0.70710678118654757,
		-0.70710678118654757,
		-0.70710678118654757
	};

	// Token: 0x04001658 RID: 5720
	private const int gradientsMask2D = 7;
}
