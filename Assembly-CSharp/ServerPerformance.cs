﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Facepunch;
using UnityEngine;
using UnityEngine.Profiling;

// Token: 0x02000625 RID: 1573
public class ServerPerformance : global::BaseMonoBehaviour
{
	// Token: 0x06001FFE RID: 8190 RVA: 0x000B6E44 File Offset: 0x000B5044
	private void Start()
	{
		if (!Profiler.supported)
		{
			return;
		}
		if (!CommandLine.HasSwitch("-perf"))
		{
			return;
		}
		this.fileName = "perfdata." + DateTime.Now.ToString() + ".txt";
		this.fileName = this.fileName.Replace('\\', '-');
		this.fileName = this.fileName.Replace('/', '-');
		this.fileName = this.fileName.Replace(' ', '_');
		this.fileName = this.fileName.Replace(':', '.');
		this.lastFrame = Time.frameCount;
		File.WriteAllText(this.fileName, "MemMono,MemUnity,Frame,PlayerCount,Sleepers,CollidersDisabled,BehavioursDisabled,GameObjects,Colliders,RigidBodies,BuildingBlocks,nwSend,nwRcv,cnInit,cnApp,cnRej,deaths,spawns,poschange\r\n");
		base.InvokeRepeating(new Action(this.WriteLine), 1f, 60f);
	}

	// Token: 0x06001FFF RID: 8191 RVA: 0x000B6F20 File Offset: 0x000B5120
	private void WriteLine()
	{
		GC.Collect();
		uint monoUsedSize = Profiler.GetMonoUsedSize();
		uint usedHeapSize = Profiler.usedHeapSize;
		int count = global::BasePlayer.activePlayerList.Count;
		int count2 = global::BasePlayer.sleepingPlayerList.Count;
		int num = Object.FindObjectsOfType<GameObject>().Length;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		int num7 = Time.frameCount - this.lastFrame;
		File.AppendAllText(this.fileName, string.Concat(new object[]
		{
			monoUsedSize,
			",",
			usedHeapSize,
			",",
			num7,
			",",
			count,
			",",
			count2,
			",",
			global::NetworkSleep.totalCollidersDisabled,
			",",
			global::NetworkSleep.totalBehavioursDisabled,
			",",
			num,
			",",
			Object.FindObjectsOfType<Collider>().Length,
			",",
			Object.FindObjectsOfType<Rigidbody>().Length,
			",",
			Object.FindObjectsOfType<global::BuildingBlock>().Length,
			",",
			num2,
			",",
			num3,
			",",
			num4,
			",",
			num5,
			",",
			num6,
			",",
			global::ServerPerformance.deaths,
			",",
			global::ServerPerformance.spawns,
			",",
			global::ServerPerformance.position_changes,
			"\r\n"
		}));
		this.lastFrame = Time.frameCount;
		global::ServerPerformance.deaths = 0UL;
		global::ServerPerformance.spawns = 0UL;
		global::ServerPerformance.position_changes = 0UL;
	}

	// Token: 0x06002000 RID: 8192 RVA: 0x000B7140 File Offset: 0x000B5340
	public static void DoReport()
	{
		string text = "report." + DateTime.Now.ToString() + ".txt";
		text = text.Replace('\\', '-');
		text = text.Replace('/', '-');
		text = text.Replace(' ', '_');
		text = text.Replace(':', '.');
		File.WriteAllText(text, "Report Generated " + DateTime.Now.ToString() + "\r\n");
		global::ServerPerformance.ComponentReport(text, "All Objects", Object.FindObjectsOfType<Transform>());
		global::ServerPerformance.ComponentReport(text, "Entities", Object.FindObjectsOfType<global::BaseEntity>());
		global::ServerPerformance.ComponentReport(text, "Rigidbodies", Object.FindObjectsOfType<Rigidbody>());
		global::ServerPerformance.ComponentReport(text, "Disabled Colliders", (from x in Object.FindObjectsOfType<Collider>()
		where !x.enabled
		select x).ToArray<Collider>());
		global::ServerPerformance.ComponentReport(text, "Enabled Colliders", (from x in Object.FindObjectsOfType<Collider>()
		where x.enabled
		select x).ToArray<Collider>());
		if (SingletonComponent<global::SpawnHandler>.Instance)
		{
			SingletonComponent<global::SpawnHandler>.Instance.DumpReport(text);
		}
	}

	// Token: 0x06002001 RID: 8193 RVA: 0x000B727C File Offset: 0x000B547C
	public static string WorkoutPrefabName(GameObject obj)
	{
		if (obj == null)
		{
			return "null";
		}
		string str = (!obj.activeSelf) ? " (inactive)" : string.Empty;
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity)
		{
			return baseEntity.PrefabName + str;
		}
		return obj.name + str;
	}

	// Token: 0x06002002 RID: 8194 RVA: 0x000B72E4 File Offset: 0x000B54E4
	public static void ComponentReport(string filename, string Title, Object[] objects)
	{
		File.AppendAllText(filename, "\r\n\r\n" + Title + ":\r\n\r\n");
		IEnumerable<IGrouping<string, Object>> enumerable = from x in objects
		group x by global::ServerPerformance.WorkoutPrefabName((x as Component).gameObject);
		IEnumerable<IGrouping<string, Object>> source = enumerable;
		if (global::ServerPerformance.<>f__mg$cache0 == null)
		{
			global::ServerPerformance.<>f__mg$cache0 = new Func<IGrouping<string, Object>, int>(Enumerable.Count<Object>);
		}
		enumerable = source.OrderByDescending(global::ServerPerformance.<>f__mg$cache0);
		foreach (IGrouping<string, Object> source2 in enumerable)
		{
			File.AppendAllText(filename, string.Concat(new object[]
			{
				"\t",
				global::ServerPerformance.WorkoutPrefabName((source2.ElementAt(0) as Component).gameObject),
				" - ",
				source2.Count<Object>(),
				"\r\n"
			}));
		}
		File.AppendAllText(filename, "\r\nTotal: " + objects.Count<Object>() + "\r\n\r\n\r\n");
	}

	// Token: 0x04001AC1 RID: 6849
	public static ulong deaths;

	// Token: 0x04001AC2 RID: 6850
	public static ulong spawns;

	// Token: 0x04001AC3 RID: 6851
	public static ulong position_changes;

	// Token: 0x04001AC4 RID: 6852
	private string fileName;

	// Token: 0x04001AC5 RID: 6853
	private int lastFrame;

	// Token: 0x04001AC8 RID: 6856
	[CompilerGenerated]
	private static Func<IGrouping<string, Object>, int> <>f__mg$cache0;
}
