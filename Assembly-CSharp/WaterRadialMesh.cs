﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005EE RID: 1518
[Serializable]
public class WaterRadialMesh
{
	// Token: 0x17000226 RID: 550
	// (get) Token: 0x06001F1B RID: 7963 RVA: 0x000B0804 File Offset: 0x000AEA04
	public Mesh[] Meshes
	{
		get
		{
			return this.meshes;
		}
	}

	// Token: 0x17000227 RID: 551
	// (get) Token: 0x06001F1C RID: 7964 RVA: 0x000B080C File Offset: 0x000AEA0C
	public bool IsInitialized
	{
		get
		{
			return this.initialized;
		}
	}

	// Token: 0x06001F1D RID: 7965 RVA: 0x000B0814 File Offset: 0x000AEA14
	public void Initialize(int vertexCount)
	{
		this.meshes = this.GenerateMeshes(vertexCount, false);
		this.initialized = true;
	}

	// Token: 0x06001F1E RID: 7966 RVA: 0x000B082C File Offset: 0x000AEA2C
	public void Destroy()
	{
		if (this.initialized)
		{
			foreach (Mesh mesh in this.meshes)
			{
				Object.DestroyImmediate(mesh);
			}
			this.meshes = null;
			this.initialized = false;
		}
	}

	// Token: 0x06001F1F RID: 7967 RVA: 0x000B0878 File Offset: 0x000AEA78
	private Mesh CreateMesh(string name, Vector3[] vertices, int[] indices)
	{
		Mesh mesh = new Mesh();
		mesh.hideFlags = 52;
		mesh.name = name;
		mesh.vertices = vertices;
		mesh.SetIndices(indices, 2, 0);
		mesh.RecalculateBounds();
		mesh.UploadMeshData(true);
		return mesh;
	}

	// Token: 0x06001F20 RID: 7968 RVA: 0x000B08B8 File Offset: 0x000AEAB8
	private Mesh[] GenerateMeshes(int vertexCount, bool volume = false)
	{
		int num = Mathf.RoundToInt(Mathf.Sqrt((float)vertexCount));
		int num2 = Mathf.RoundToInt((float)num * 0.4f);
		int num3 = Mathf.RoundToInt((float)vertexCount / (float)num2);
		int num4 = (!volume) ? num3 : (num3 / 2);
		List<Mesh> list = new List<Mesh>();
		List<Vector3> list2 = new List<Vector3>();
		List<int> list3 = new List<int>();
		Vector2[] array = new Vector2[num2];
		int num5 = 0;
		int num6 = 0;
		for (int i = 0; i < num2; i++)
		{
			float num7 = ((float)i / (float)(num2 - 1) * 2f - 1f) * 3.14159274f * 0.25f;
			Vector2[] array2 = array;
			int num8 = i;
			Vector2 vector;
			vector..ctor(Mathf.Sin(num7), Mathf.Cos(num7));
			array2[num8] = vector.normalized;
		}
		for (int j = 0; j < num4; j++)
		{
			float num9 = (float)j / (float)(num3 - 1);
			num9 = 1f - Mathf.Cos(num9 * 3.14159274f * 0.5f);
			for (int k = 0; k < num2; k++)
			{
				Vector2 vector2 = array[k] * num9;
				if (j < num4 - 2 || !volume)
				{
					list2.Add(new Vector3(vector2.x, 0f, vector2.y));
				}
				else if (j == num4 - 2)
				{
					list2.Add(new Vector3(vector2.x * 10f, -0.9f, vector2.y) * 0.5f);
				}
				else
				{
					list2.Add(new Vector3(vector2.x * 10f, -0.9f, vector2.y * -10f) * 0.5f);
				}
				if (k != 0 && j != 0 && num5 > num2)
				{
					list3.Add(num5);
					list3.Add(num5 - num2);
					list3.Add(num5 - num2 - 1);
					list3.Add(num5 - 1);
				}
				num5++;
				if (num5 >= 65000)
				{
					list.Add(this.CreateMesh(string.Concat(new object[]
					{
						"WaterMesh_",
						num2,
						"x",
						num3,
						"_",
						num6
					}), list2.ToArray(), list3.ToArray()));
					k--;
					j--;
					num9 = 1f - Mathf.Cos((float)j / (float)(num3 - 1) * 3.14159274f * 0.5f);
					num5 = 0;
					list2.Clear();
					list3.Clear();
					num6++;
				}
			}
		}
		if (num5 != 0)
		{
			list.Add(this.CreateMesh(string.Concat(new object[]
			{
				"WaterMesh_",
				num2,
				"x",
				num3,
				"_",
				num6
			}), list2.ToArray(), list3.ToArray()));
		}
		return list.ToArray();
	}

	// Token: 0x06001F21 RID: 7969 RVA: 0x000B0BEC File Offset: 0x000AEDEC
	private Vector3 RaycastPlane(Camera camera, float planeHeight, Vector3 pos)
	{
		Ray ray = camera.ViewportPointToRay(pos);
		if (camera.transform.position.y > planeHeight)
		{
			if (ray.direction.y > -0.01f)
			{
				ray.direction = new Vector3(ray.direction.x, -ray.direction.y - 0.02f, ray.direction.z);
			}
		}
		else if (ray.direction.y < 0.01f)
		{
			ray.direction = new Vector3(ray.direction.x, -ray.direction.y + 0.02f, ray.direction.z);
		}
		float num = -(ray.origin.y - planeHeight) / ray.direction.y;
		return Quaternion.AngleAxis(-camera.transform.eulerAngles.y, Vector3.up) * (ray.direction * num);
	}

	// Token: 0x06001F22 RID: 7970 RVA: 0x000B0D30 File Offset: 0x000AEF30
	public Matrix4x4 ComputeLocalToWorldMatrix(Camera camera, float oceanWaterLevel)
	{
		if (camera == null)
		{
			return Matrix4x4.identity;
		}
		Vector3 vector = camera.worldToCameraMatrix.MultiplyVector(new Vector3(0f, -1f, 0f));
		Vector3 vector2 = camera.worldToCameraMatrix.MultiplyVector(Vector3.Cross(camera.transform.forward, new Vector3(0f, -1f, 0f)));
		Vector3 vector3;
		vector3..ctor(vector.x, vector.y, 0f);
		vector = vector3.normalized * 0.5f + new Vector3(0.5f, 0f, 0.5f);
		Vector3 vector4;
		vector4..ctor(vector2.x, vector2.y, 0f);
		vector2 = vector4.normalized * 0.5f;
		Vector3 vector5 = this.RaycastPlane(camera, oceanWaterLevel, vector - vector2);
		Vector3 vector6 = this.RaycastPlane(camera, oceanWaterLevel, vector + vector2);
		Vector3 position = camera.transform.position;
		Vector3 vector7;
		vector7..ctor(camera.farClipPlane * Mathf.Tan(camera.fieldOfView * 0.5f * 0.0174532924f) * camera.aspect + 2f, camera.farClipPlane, camera.farClipPlane);
		float num = Mathf.Abs(vector6.x - vector5.x);
		float num2 = Mathf.Min(vector5.z, vector6.z) - (num + 2f) * vector7.z / vector7.x;
		Vector3 forward = camera.transform.forward;
		forward.y = 0f;
		forward.Normalize();
		float num3 = Vector3.Dot(Vector3.down, camera.transform.forward);
		if (num3 < -0.98f || num3 > 0.98f)
		{
			forward..ctor(-camera.transform.up.x, 0f, -camera.transform.up.z);
			forward.Normalize();
			num2 = -camera.transform.position.y * 4f * Mathf.Tan(camera.fieldOfView * 0.5f * 0.0174532924f);
		}
		vector7.z -= num2;
		return Matrix4x4.TRS(new Vector3(position.x, oceanWaterLevel, position.z) + forward * num2, Quaternion.AngleAxis(Mathf.Atan2(forward.x, forward.z) * 57.29578f, Vector3.up), vector7);
	}

	// Token: 0x04001A0E RID: 6670
	private const float AlignmentGranularity = 1f;

	// Token: 0x04001A0F RID: 6671
	private const float MaxHorizontalDisplacement = 1f;

	// Token: 0x04001A10 RID: 6672
	private Mesh[] meshes;

	// Token: 0x04001A11 RID: 6673
	private bool initialized;
}
