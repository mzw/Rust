﻿using System;
using System.Collections.Generic;

// Token: 0x02000769 RID: 1897
public static class LinqEx
{
	// Token: 0x06002357 RID: 9047 RVA: 0x000C391C File Offset: 0x000C1B1C
	public static int MaxIndex<T>(this IEnumerable<T> sequence) where T : IComparable<T>
	{
		int num = -1;
		T other = default(T);
		int num2 = 0;
		foreach (T t in sequence)
		{
			if (t.CompareTo(other) > 0 || num == -1)
			{
				num = num2;
				other = t;
			}
			num2++;
		}
		return num;
	}
}
