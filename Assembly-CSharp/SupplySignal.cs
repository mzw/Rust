﻿using System;
using UnityEngine;

// Token: 0x020003AF RID: 943
public class SupplySignal : global::TimedExplosive
{
	// Token: 0x06001663 RID: 5731 RVA: 0x00081884 File Offset: 0x0007FA84
	public override void Explode()
	{
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.EntityToCreate.resourcePath, default(Vector3), default(Quaternion), true);
		if (baseEntity)
		{
			Vector3 vector;
			vector..ctor(Random.Range(-20f, 20f), 0f, Random.Range(-20f, 20f));
			baseEntity.SendMessage("InitDropPosition", base.GetEstimatedWorldPosition() + vector, 1);
			baseEntity.Spawn();
		}
		base.Invoke(new Action(this.FinishUp), 210f);
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06001664 RID: 5732 RVA: 0x0008193C File Offset: 0x0007FB3C
	public void FinishUp()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x040010D5 RID: 4309
	public global::GameObjectRef smokeEffectPrefab;

	// Token: 0x040010D6 RID: 4310
	public global::GameObjectRef EntityToCreate;

	// Token: 0x040010D7 RID: 4311
	[NonSerialized]
	public GameObject smokeEffect;
}
