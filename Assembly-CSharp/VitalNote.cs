﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000731 RID: 1841
public class VitalNote : MonoBehaviour, IClientComponent
{
	// Token: 0x04001F22 RID: 7970
	public global::VitalNote.Vital VitalType;

	// Token: 0x04001F23 RID: 7971
	public global::FloatConditions showIf;

	// Token: 0x04001F24 RID: 7972
	public Text valueText;

	// Token: 0x04001F25 RID: 7973
	public Animator animator;

	// Token: 0x02000732 RID: 1842
	public enum Vital
	{
		// Token: 0x04001F27 RID: 7975
		Comfort,
		// Token: 0x04001F28 RID: 7976
		Radiation,
		// Token: 0x04001F29 RID: 7977
		Poison,
		// Token: 0x04001F2A RID: 7978
		Cold,
		// Token: 0x04001F2B RID: 7979
		Bleeding,
		// Token: 0x04001F2C RID: 7980
		Hot,
		// Token: 0x04001F2D RID: 7981
		Drowning,
		// Token: 0x04001F2E RID: 7982
		Wet,
		// Token: 0x04001F2F RID: 7983
		Hygiene,
		// Token: 0x04001F30 RID: 7984
		Starving,
		// Token: 0x04001F31 RID: 7985
		Dehydration
	}
}
