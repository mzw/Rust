﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConVar;
using UnityEngine;

// Token: 0x020005CE RID: 1486
public class WorldSetup : SingletonComponent<global::WorldSetup>
{
	// Token: 0x06001EAB RID: 7851 RVA: 0x000ACBD4 File Offset: 0x000AADD4
	private void OnValidate()
	{
		if (this.terrain == null)
		{
			UnityEngine.Terrain terrain = Object.FindObjectOfType<UnityEngine.Terrain>();
			if (terrain != null)
			{
				this.terrain = terrain.gameObject;
			}
		}
	}

	// Token: 0x06001EAC RID: 7852 RVA: 0x000ACC10 File Offset: 0x000AAE10
	protected override void Awake()
	{
		base.Awake();
		foreach (global::Prefab prefab in global::Prefab.Load("assets/bundled/prefabs/world", null, null, true))
		{
			if (prefab.Object.GetComponent<global::BaseEntity>() != null)
			{
				global::BaseEntity baseEntity = prefab.SpawnEntity(Vector3.zero, Quaternion.identity);
				baseEntity.Spawn();
			}
			else
			{
				prefab.Spawn(Vector3.zero, Quaternion.identity);
			}
		}
		foreach (SingletonComponent singletonComponent in Object.FindObjectsOfType<SingletonComponent>())
		{
			singletonComponent.Setup();
		}
		if (this.terrain)
		{
			global::TerrainGenerator component = this.terrain.GetComponent<global::TerrainGenerator>();
			if (component)
			{
				global::World.Procedural = true;
				this.terrain = component.CreateTerrain();
			}
			else
			{
				global::World.Procedural = false;
			}
			this.terrainMeta = this.terrain.GetComponent<global::TerrainMeta>();
			this.terrainMeta.Init(null, null);
			this.terrainMeta.SetupComponents();
			global::World.InitSize(Mathf.RoundToInt(global::TerrainMeta.Size.x));
		}
		global::World.Serialization = new global::WorldSerialization();
		global::World.Serialization.CleanupOldFiles();
		this.CreateObject(this.decorPrefab);
		this.CreateObject(this.grassPrefab);
		this.CreateObject(this.spawnPrefab);
		if (this.AutomaticallySetup)
		{
			base.StartCoroutine(this.InitCoroutine());
		}
	}

	// Token: 0x06001EAD RID: 7853 RVA: 0x000ACD94 File Offset: 0x000AAF94
	protected void CreateObject(GameObject prefab)
	{
		if (prefab == null)
		{
			return;
		}
		GameObject gameObject = Object.Instantiate<GameObject>(prefab);
		if (gameObject != null)
		{
			gameObject.SetActive(true);
		}
	}

	// Token: 0x06001EAE RID: 7854 RVA: 0x000ACDC8 File Offset: 0x000AAFC8
	public IEnumerator InitCoroutine()
	{
		global::ProceduralComponent[] components = base.GetComponentsInChildren<global::ProceduralComponent>(true);
		global::Timing loadTimer = global::Timing.Start("Loading World");
		if (ConVar.World.cache && global::World.Procedural && global::World.Serialization.CanLoadFromDisk())
		{
			global::LoadingScreen.Update("LOADING WORLD");
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			global::World.Serialization.Load();
		}
		loadTimer.End();
		if (global::World.Serialization.Cached && global::World.Serialization.Version != 6u)
		{
			global::World.Serialization.Clear();
		}
		if (global::World.Serialization.Cached && string.IsNullOrEmpty(global::World.Checksum))
		{
			global::World.Checksum = global::World.Serialization.Checksum;
		}
		global::Timing applyTimer = global::Timing.Start("Applying World");
		if (global::World.Serialization.Cached)
		{
			global::LoadingScreen.Update("APPLYING WORLD");
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			global::TerrainMeta.HeightMap.FromByteArray(global::World.Serialization.GetMap("terrain"));
			global::TerrainMeta.SplatMap.FromByteArray(global::World.Serialization.GetMap("splat"));
			global::TerrainMeta.BiomeMap.FromByteArray(global::World.Serialization.GetMap("biome"));
			global::TerrainMeta.TopologyMap.FromByteArray(global::World.Serialization.GetMap("topology"));
			global::TerrainMeta.AlphaMap.FromByteArray(global::World.Serialization.GetMap("alpha"));
			global::TerrainMeta.WaterMap.FromByteArray(global::World.Serialization.GetMap("water"));
		}
		applyTimer.End();
		global::Timing spawnTimer = global::Timing.Start("Spawning World");
		if (global::World.Serialization.Cached)
		{
			global::LoadingScreen.Update("SPAWNING WORLD");
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			IEnumerator worldSpawn = global::World.Serialization.Spawn(0.2f, delegate(string str)
			{
				global::LoadingScreen.Update(str);
			});
			while (worldSpawn.MoveNext())
			{
				object obj = worldSpawn.Current;
				yield return obj;
			}
			global::TerrainMeta.Path.Clear();
			global::TerrainMeta.Path.Roads.AddRange(global::World.Serialization.GetPaths("Road"));
			global::TerrainMeta.Path.Rivers.AddRange(global::World.Serialization.GetPaths("River"));
			global::TerrainMeta.Path.Powerlines.AddRange(global::World.Serialization.GetPaths("Powerline"));
		}
		spawnTimer.End();
		global::Timing procgenTimer = global::Timing.Start("Procedural Generation");
		if (components.Length > 0)
		{
			for (int i = 0; i < components.Length; i++)
			{
				global::ProceduralComponent component = components[i];
				if (component)
				{
					if (component.ShouldRun())
					{
						uint seed = (uint)((ulong)global::World.Seed + (ulong)((long)i));
						global::LoadingScreen.Update(component.Description.ToUpper());
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
						global::Timing timing = global::Timing.Start(component.Description);
						if (component)
						{
							component.Process(seed);
						}
						timing.End();
					}
				}
			}
		}
		procgenTimer.End();
		global::Timing saveTimer = global::Timing.Start("Saving World");
		if (ConVar.World.cache && global::World.Procedural && !global::World.Serialization.Cached)
		{
			global::LoadingScreen.Update("SAVING WORLD");
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			global::World.Serialization.AddPaths(global::TerrainMeta.Path.Roads);
			global::World.Serialization.AddPaths(global::TerrainMeta.Path.Rivers);
			global::World.Serialization.AddPaths(global::TerrainMeta.Path.Powerlines);
			global::World.Serialization.Save();
		}
		saveTimer.End();
		global::Timing checksumTimer = global::Timing.Start("Calculating Checksum");
		if (string.IsNullOrEmpty(global::World.Serialization.Checksum))
		{
			global::LoadingScreen.Update("CALCULATING CHECKSUM");
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			global::World.Serialization.CalculateChecksum();
		}
		checksumTimer.End();
		if (string.IsNullOrEmpty(global::World.Checksum))
		{
			global::World.Checksum = global::World.Serialization.Checksum;
		}
		global::Timing finalizeTimer = global::Timing.Start("Finalizing World");
		global::LoadingScreen.Update("FINALIZING WORLD");
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		if (this.terrainMeta)
		{
			this.terrainMeta.BindShaderProperties();
			this.terrainMeta.PostSetupComponents();
		}
		global::World.Serialization.Clear();
		finalizeTimer.End();
		global::LoadingScreen.Update("DONE");
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		if (this)
		{
			global::GameManager.Destroy(base.gameObject, 0f);
		}
		yield break;
	}

	// Token: 0x0400197E RID: 6526
	public bool AutomaticallySetup;

	// Token: 0x0400197F RID: 6527
	public GameObject terrain;

	// Token: 0x04001980 RID: 6528
	public GameObject decorPrefab;

	// Token: 0x04001981 RID: 6529
	public GameObject grassPrefab;

	// Token: 0x04001982 RID: 6530
	public GameObject spawnPrefab;

	// Token: 0x04001983 RID: 6531
	private global::TerrainMeta terrainMeta;

	// Token: 0x04001984 RID: 6532
	public uint EditorSeed;

	// Token: 0x04001985 RID: 6533
	public uint EditorSalt;

	// Token: 0x04001986 RID: 6534
	internal List<global::ProceduralObject> ProceduralObjects = new List<global::ProceduralObject>();
}
