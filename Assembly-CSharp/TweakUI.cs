﻿using System;
using UnityEngine;

// Token: 0x02000710 RID: 1808
public class TweakUI : SingletonComponent<global::TweakUI>
{
	// Token: 0x0600225E RID: 8798 RVA: 0x000C0998 File Offset: 0x000BEB98
	private void Update()
	{
		if (Input.GetKeyDown(283) && this.CanToggle())
		{
			this.SetVisible(!global::TweakUI.isOpen);
		}
	}

	// Token: 0x0600225F RID: 8799 RVA: 0x000C09C4 File Offset: 0x000BEBC4
	protected bool CanToggle()
	{
		return global::LevelManager.isLoaded;
	}

	// Token: 0x06002260 RID: 8800 RVA: 0x000C09D4 File Offset: 0x000BEBD4
	public void SetVisible(bool b)
	{
		if (b)
		{
			global::TweakUI.isOpen = true;
		}
		else
		{
			global::TweakUI.isOpen = false;
			ConsoleSystem.Run(ConsoleSystem.Option.Client, "writecfg", new object[0]);
			ConsoleSystem.Run(ConsoleSystem.Option.Client, "trackir.refresh", new object[0]);
		}
	}

	// Token: 0x04001ED0 RID: 7888
	public static bool isOpen;
}
