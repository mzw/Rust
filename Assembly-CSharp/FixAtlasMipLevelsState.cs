﻿using System;

// Token: 0x0200058E RID: 1422
internal enum FixAtlasMipLevelsState
{
	// Token: 0x040018AA RID: 6314
	Skipped,
	// Token: 0x040018AB RID: 6315
	Initializing,
	// Token: 0x040018AC RID: 6316
	Succeeded,
	// Token: 0x040018AD RID: 6317
	Failed
}
