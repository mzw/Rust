﻿using System;

// Token: 0x02000774 RID: 1908
public static class TimeSpanEx
{
	// Token: 0x0600236E RID: 9070 RVA: 0x000C4304 File Offset: 0x000C2504
	public static string ToShortString(this TimeSpan timeSpan)
	{
		return string.Format("{0:00}:{1:00}:{2:00}", (int)timeSpan.TotalHours, timeSpan.Minutes, timeSpan.Seconds);
	}
}
