﻿using System;
using UnityEngine;

// Token: 0x020003A0 RID: 928
public class M2BradleyPhysics : MonoBehaviour
{
	// Token: 0x04001057 RID: 4183
	private global::m2bradleyAnimator m2Animator;

	// Token: 0x04001058 RID: 4184
	public WheelCollider[] Wheels;

	// Token: 0x04001059 RID: 4185
	public WheelCollider[] TurningWheels;

	// Token: 0x0400105A RID: 4186
	public Rigidbody mainRigidbody;

	// Token: 0x0400105B RID: 4187
	public Transform[] waypoints;

	// Token: 0x0400105C RID: 4188
	private Vector3 currentWaypoint;

	// Token: 0x0400105D RID: 4189
	private Vector3 nextWaypoint;
}
