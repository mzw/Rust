﻿using System;

// Token: 0x02000277 RID: 631
[ConsoleSystem.Factory("lerp")]
public class lerp : ConsoleSystem
{
	// Token: 0x04000B9C RID: 2972
	[ClientVar(Help = "Enables interpolation and extrapolation of network positions")]
	public static bool enabled = true;

	// Token: 0x04000B9D RID: 2973
	[ClientVar(Help = "How many seconds to smoothen velocity")]
	public static float smoothing = 0.2f;

	// Token: 0x04000B9E RID: 2974
	[ClientVar(Help = "How many seconds behind to lerp")]
	public static float interpolation = 0.1f;

	// Token: 0x04000B9F RID: 2975
	[ClientVar(Help = "How many seconds ahead to lerp")]
	public static float extrapolation = 0.1f;
}
