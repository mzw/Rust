﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using EasyAntiCheat.Server.Hydra.Cerberus;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200004E RID: 78
public class BaseProjectile : global::AttackEntity
{
	// Token: 0x0600063F RID: 1599 RVA: 0x00025CD4 File Offset: 0x00023ED4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseProjectile.OnRpcMessage", 0.1f))
		{
			if (rpc == 386279056u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - CLProject ");
				}
				using (TimeWarning.New("CLProject", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("CLProject", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.CLProject(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in CLProject");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3360326041u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Reload ");
				}
				using (TimeWarning.New("Reload", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("Reload", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Reload(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in Reload");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 3302290555u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - StartReload ");
				}
				using (TimeWarning.New("StartReload", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("StartReload", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.StartReload(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in StartReload");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 1637077501u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SwitchAmmoTo ");
				}
				using (TimeWarning.New("SwitchAmmoTo", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("SwitchAmmoTo", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SwitchAmmoTo(msg5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in SwitchAmmoTo");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000640 RID: 1600 RVA: 0x00026348 File Offset: 0x00024548
	protected void StartReloadCooldown(float cooldown)
	{
		this.nextReloadTime = base.CalculateCooldownTime(this.nextReloadTime, cooldown, false);
	}

	// Token: 0x06000641 RID: 1601 RVA: 0x00026360 File Offset: 0x00024560
	protected void ResetReloadCooldown()
	{
		this.nextReloadTime = float.NegativeInfinity;
	}

	// Token: 0x06000642 RID: 1602 RVA: 0x00026370 File Offset: 0x00024570
	protected bool HasReloadCooldown()
	{
		return UnityEngine.Time.time < this.nextReloadTime;
	}

	// Token: 0x06000643 RID: 1603 RVA: 0x00026380 File Offset: 0x00024580
	protected float GetReloadCooldown()
	{
		return Mathf.Max(this.nextReloadTime - UnityEngine.Time.time, 0f);
	}

	// Token: 0x06000644 RID: 1604 RVA: 0x00026398 File Offset: 0x00024598
	protected float GetReloadIdle()
	{
		return Mathf.Max(UnityEngine.Time.time - this.nextReloadTime, 0f);
	}

	// Token: 0x06000645 RID: 1605 RVA: 0x000263B0 File Offset: 0x000245B0
	private void OnDrawGizmos()
	{
		if (!base.isClient)
		{
			return;
		}
		if (this.MuzzlePoint != null)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(this.MuzzlePoint.position, this.MuzzlePoint.position + this.MuzzlePoint.forward * 10f);
			global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
			if (ownerPlayer)
			{
				Gizmos.color = Color.cyan;
				Gizmos.DrawLine(this.MuzzlePoint.position, this.MuzzlePoint.position + ownerPlayer.eyes.rotation * Vector3.forward * 10f);
			}
		}
	}

	// Token: 0x06000646 RID: 1606 RVA: 0x00026474 File Offset: 0x00024674
	public virtual global::RecoilProperties GetRecoil()
	{
		return this.recoil;
	}

	// Token: 0x06000647 RID: 1607 RVA: 0x0002647C File Offset: 0x0002467C
	public virtual void ServerReload()
	{
		base.StartAttackCooldown(this.reloadTime);
		base.GetOwnerPlayer().SignalBroadcast(global::BaseEntity.Signal.Reload, null);
		if (this.worldModelAnimator != null)
		{
			this.worldModelAnimator.SetTrigger("reload");
		}
		this.primaryMagazine.contents = this.primaryMagazine.capacity;
	}

	// Token: 0x06000648 RID: 1608 RVA: 0x000264DC File Offset: 0x000246DC
	public override Vector3 ModifyAIAim(Vector3 eulerInput, float swayModifier = 1f)
	{
		bool flag = false;
		float num = UnityEngine.Time.time * (this.aimSwaySpeed * 1f + this.aiAimSwayOffset);
		float num2 = Mathf.Sin(UnityEngine.Time.time * 2f);
		float num3 = (num2 >= 0f) ? 1f : (1f - Mathf.Clamp(Mathf.Abs(num2) / 1f, 0f, 1f));
		float num4 = (!flag) ? 1f : 0.6f;
		float num5 = (this.aimSway * 1f + this.aiAimSwayOffset) * num4 * num3 * swayModifier;
		eulerInput.y += (Mathf.PerlinNoise(num, num) - 0.5f) * num5 * UnityEngine.Time.deltaTime;
		eulerInput.x += (Mathf.PerlinNoise(num + 0.1f, num + 0.2f) - 0.5f) * num5 * UnityEngine.Time.deltaTime;
		return eulerInput;
	}

	// Token: 0x06000649 RID: 1609 RVA: 0x000265D8 File Offset: 0x000247D8
	public float GetAIAimcone()
	{
		return this.aiAimCone;
	}

	// Token: 0x0600064A RID: 1610 RVA: 0x000265E0 File Offset: 0x000247E0
	public override void ServerUse()
	{
		if (base.isClient)
		{
			return;
		}
		if (base.HasAttackCooldown())
		{
			return;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (ownerPlayer == null)
		{
			return;
		}
		if (this.primaryMagazine.contents <= 0)
		{
			base.SignalBroadcast(global::BaseEntity.Signal.DryFire, null);
			base.StartAttackCooldown(1f);
		}
		this.primaryMagazine.contents--;
		if (this.primaryMagazine.contents < 0)
		{
			this.primaryMagazine.contents = 0;
		}
		base.StartAttackCooldown(this.repeatDelay);
		Vector3 position = ownerPlayer.eyes.position;
		global::ItemModProjectile component = this.primaryMagazine.ammoType.GetComponent<global::ItemModProjectile>();
		base.SignalBroadcast(global::BaseEntity.Signal.Attack, string.Empty, null);
		global::Projectile component2 = component.projectileObject.Get().GetComponent<global::Projectile>();
		for (int i = 0; i < component.numProjectiles; i++)
		{
			Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(component.projectileSpread + this.aimCone + this.GetAIAimcone(), ownerPlayer.eyes.BodyForward(), true);
			List<RaycastHit> list = Facepunch.Pool.GetList<RaycastHit>();
			global::GamePhysics.TraceAll(new Ray(position, modifiedAimConeDirection), 0f, list, 300f, 1084435201, 0);
			for (int j = 0; j < list.Count; j++)
			{
				RaycastHit hit = list[j];
				global::BaseEntity entity = hit.GetEntity();
				if (!(entity != null) || (!(entity == this) && !entity.EqualNetID(this)))
				{
					if (!(entity != null) || !entity.isClient)
					{
						global::BaseCombatEntity baseCombatEntity = entity as global::BaseCombatEntity;
						if (baseCombatEntity != null)
						{
							float num = 0f;
							foreach (Rust.DamageTypeEntry damageTypeEntry in component2.damageTypes)
							{
								num += damageTypeEntry.amount;
							}
							baseCombatEntity.Hurt(num * this.npcDamageScale, Rust.DamageType.Bullet, ownerPlayer, true);
						}
						if (!(entity != null) || entity.ShouldBlockProjectiles())
						{
							break;
						}
					}
				}
			}
			this.CreateProjectileEffectClientside(component.projectileObject.resourcePath, ownerPlayer.eyes.position, modifiedAimConeDirection * component.projectileVelocity, 0, null, this.IsSilenced(), true);
		}
	}

	// Token: 0x0600064B RID: 1611 RVA: 0x00026874 File Offset: 0x00024A74
	public override void ServerInit()
	{
		base.ServerInit();
		this.primaryMagazine.ServerInit();
	}

	// Token: 0x0600064C RID: 1612 RVA: 0x00026888 File Offset: 0x00024A88
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (item == null)
		{
			return;
		}
		if (command == "unload_ammo")
		{
			this.UnloadAmmo(item, player);
		}
	}

	// Token: 0x0600064D RID: 1613 RVA: 0x000268AC File Offset: 0x00024AAC
	public void UnloadAmmo(global::Item item, global::BasePlayer player)
	{
		global::BaseEntity heldEntity = item.GetHeldEntity();
		global::BaseProjectile component = heldEntity.GetComponent<global::BaseProjectile>();
		if (!component.canUnloadAmmo)
		{
			return;
		}
		if (component)
		{
			int contents = component.primaryMagazine.contents;
			if (contents > 0)
			{
				component.primaryMagazine.contents = 0;
				base.SendNetworkUpdateImmediate(false);
				global::Item item2 = global::ItemManager.Create(component.primaryMagazine.ammoType, contents, 0UL);
				if (!item2.MoveToContainer(player.inventory.containerMain, -1, true))
				{
					item2.Drop(player.eyes.position, player.eyes.BodyForward() * 2f, default(Quaternion));
				}
			}
		}
	}

	// Token: 0x0600064E RID: 1614 RVA: 0x00026964 File Offset: 0x00024B64
	public override void CollectedForCrafting(global::Item item, global::BasePlayer crafter)
	{
		if (crafter == null || item == null)
		{
			return;
		}
		this.UnloadAmmo(item, crafter);
	}

	// Token: 0x0600064F RID: 1615 RVA: 0x00026984 File Offset: 0x00024B84
	public override void ReturnedFromCancelledCraft(global::Item item, global::BasePlayer crafter)
	{
		if (crafter == null || item == null)
		{
			return;
		}
		global::BaseEntity heldEntity = item.GetHeldEntity();
		global::BaseProjectile component = heldEntity.GetComponent<global::BaseProjectile>();
		if (component)
		{
			component.primaryMagazine.contents = 0;
		}
	}

	// Token: 0x06000650 RID: 1616 RVA: 0x000269CC File Offset: 0x00024BCC
	public override void SetLightsOn(bool isOn)
	{
		base.SetLightsOn(isOn);
		if (this.children != null)
		{
			foreach (global::ProjectileWeaponMod projectileWeaponMod in from global::ProjectileWeaponMod x in this.children
			where x != null && x.isLight
			select x)
			{
				projectileWeaponMod.SetFlag(global::BaseEntity.Flags.On, isOn, false);
			}
		}
	}

	// Token: 0x06000651 RID: 1617 RVA: 0x00026A60 File Offset: 0x00024C60
	public bool CanAiAttack()
	{
		return true;
	}

	// Token: 0x06000652 RID: 1618 RVA: 0x00026A64 File Offset: 0x00024C64
	public virtual float GetAimCone()
	{
		float num = global::ProjectileWeaponMod.Average(this, (global::ProjectileWeaponMod x) => x.sightAimCone, (global::ProjectileWeaponMod.Modifier y) => y.scalar, 1f);
		float num2 = global::ProjectileWeaponMod.Sum(this, (global::ProjectileWeaponMod x) => x.sightAimCone, (global::ProjectileWeaponMod.Modifier y) => y.offset, 0f);
		float num3 = global::ProjectileWeaponMod.Average(this, (global::ProjectileWeaponMod x) => x.hipAimCone, (global::ProjectileWeaponMod.Modifier y) => y.scalar, 1f);
		float num4 = global::ProjectileWeaponMod.Sum(this, (global::ProjectileWeaponMod x) => x.hipAimCone, (global::ProjectileWeaponMod.Modifier y) => y.offset, 0f);
		if (this.aiming || base.isServer)
		{
			return (this.aimCone + this.aimconePenalty + this.stancePenalty * this.stancePenaltyScale) * num + num2;
		}
		return (this.aimCone + this.aimconePenalty + this.stancePenalty * this.stancePenaltyScale) * num + num2 + this.hipAimCone * num3 + num4;
	}

	// Token: 0x06000653 RID: 1619 RVA: 0x00026BEC File Offset: 0x00024DEC
	public float ScaleRepeatDelay(float delay)
	{
		float num = global::ProjectileWeaponMod.Average(this, (global::ProjectileWeaponMod x) => x.repeatDelay, (global::ProjectileWeaponMod.Modifier y) => y.scalar, 1f);
		float num2 = global::ProjectileWeaponMod.Sum(this, (global::ProjectileWeaponMod x) => x.repeatDelay, (global::ProjectileWeaponMod.Modifier y) => y.offset, 0f);
		return delay * num + num2;
	}

	// Token: 0x06000654 RID: 1620 RVA: 0x00026C8C File Offset: 0x00024E8C
	public global::Projectile.Modifier GetProjectileModifier()
	{
		global::Projectile.Modifier result = default(global::Projectile.Modifier);
		result.damageOffset = global::ProjectileWeaponMod.Sum(this, (global::ProjectileWeaponMod x) => x.projectileDamage, (global::ProjectileWeaponMod.Modifier y) => y.offset, 0f);
		result.damageScale = global::ProjectileWeaponMod.Average(this, (global::ProjectileWeaponMod x) => x.projectileDamage, (global::ProjectileWeaponMod.Modifier y) => y.scalar, 1f) * this.damageScale;
		result.distanceOffset = global::ProjectileWeaponMod.Sum(this, (global::ProjectileWeaponMod x) => x.projectileDistance, (global::ProjectileWeaponMod.Modifier y) => y.offset, 0f);
		result.distanceScale = global::ProjectileWeaponMod.Average(this, (global::ProjectileWeaponMod x) => x.projectileDistance, (global::ProjectileWeaponMod.Modifier y) => y.scalar, 1f) * this.distanceScale;
		return result;
	}

	// Token: 0x06000655 RID: 1621 RVA: 0x00026DE0 File Offset: 0x00024FE0
	protected void ReloadMagazine()
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		if (Interface.CallHook("OnReloadMagazine", new object[]
		{
			ownerPlayer,
			this
		}) != null)
		{
			return;
		}
		this.primaryMagazine.Reload(ownerPlayer);
		base.SendNetworkUpdateImmediate(false);
		global::ItemManager.DoRemoves();
		ownerPlayer.inventory.ServerUpdate(0f);
	}

	// Token: 0x06000656 RID: 1622 RVA: 0x00026E48 File Offset: 0x00025048
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void SwitchAmmoTo(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		int num = msg.read.Int32();
		if (num == this.primaryMagazine.ammoType.itemid)
		{
			return;
		}
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(num);
		if (itemDefinition == null)
		{
			return;
		}
		global::ItemModProjectile component = itemDefinition.GetComponent<global::ItemModProjectile>();
		if (!component || !component.IsAmmo(this.primaryMagazine.definition.ammoTypes))
		{
			return;
		}
		if (Interface.CallHook("OnSwitchAmmo", new object[]
		{
			ownerPlayer,
			this
		}) != null)
		{
			return;
		}
		if (this.primaryMagazine.contents > 0)
		{
			ownerPlayer.GiveItem(global::ItemManager.CreateByItemID(this.primaryMagazine.ammoType.itemid, this.primaryMagazine.contents, 0UL), global::BaseEntity.GiveItemReason.Generic);
			this.primaryMagazine.contents = 0;
		}
		this.primaryMagazine.ammoType = itemDefinition;
		base.SendNetworkUpdateImmediate(false);
		global::ItemManager.DoRemoves();
		ownerPlayer.inventory.ServerUpdate(0f);
	}

	// Token: 0x06000657 RID: 1623 RVA: 0x00026F60 File Offset: 0x00025160
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void StartReload(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!base.VerifyClientRPC(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			this.reloadStarted = false;
			this.reloadFinished = false;
			return;
		}
		if (Interface.CallHook("OnReloadWeapon", new object[]
		{
			player,
			this
		}) != null)
		{
			return;
		}
		this.reloadFinished = false;
		this.reloadStarted = true;
		this.StartReloadCooldown(this.reloadTime);
	}

	// Token: 0x06000658 RID: 1624 RVA: 0x00026FD0 File Offset: 0x000251D0
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void Reload(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!base.VerifyClientRPC(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			this.reloadStarted = false;
			this.reloadFinished = false;
			return;
		}
		if (!this.reloadStarted)
		{
			global::AntiHack.Log(player, global::AntiHackType.ReloadHack, "Request skipped (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "reload_skip");
			this.reloadStarted = false;
			this.reloadFinished = false;
			return;
		}
		if (this.GetReloadCooldown() > 1f)
		{
			global::AntiHack.Log(player, global::AntiHackType.ReloadHack, string.Concat(new object[]
			{
				"T-",
				this.GetReloadCooldown(),
				"s (",
				base.ShortPrefabName,
				")"
			}));
			player.stats.combat.Log(this, "reload_time");
			this.reloadStarted = false;
			this.reloadFinished = false;
			return;
		}
		if (this.GetReloadIdle() > 1f)
		{
			global::AntiHack.Log(player, global::AntiHackType.ReloadHack, string.Concat(new object[]
			{
				"T+",
				this.GetReloadIdle(),
				"s (",
				base.ShortPrefabName,
				")"
			}));
			player.stats.combat.Log(this, "reload_time");
			this.reloadStarted = false;
			this.reloadFinished = false;
			return;
		}
		this.reloadStarted = false;
		this.reloadFinished = true;
		this.ReloadMagazine();
	}

	// Token: 0x06000659 RID: 1625 RVA: 0x00027154 File Offset: 0x00025354
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	private void CLProject(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.VerifyClientAttack(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		if (this.reloadFinished && this.HasReloadCooldown())
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Reloading (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "reload_cooldown");
			return;
		}
		this.reloadStarted = false;
		this.reloadFinished = false;
		if (this.primaryMagazine.contents <= 0 && !this.UsingInfiniteAmmoCheat)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Magazine empty (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "ammo_missing");
			return;
		}
		global::ItemDefinition ammoType = this.primaryMagazine.ammoType;
		ProjectileShoot projectileShoot = ProjectileShoot.Deserialize(msg.read);
		if (ammoType.itemid != projectileShoot.ammoType)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Ammo mismatch (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "ammo_mismatch");
			return;
		}
		if (!this.UsingInfiniteAmmoCheat)
		{
			this.primaryMagazine.contents--;
		}
		global::ItemModProjectile component = ammoType.GetComponent<global::ItemModProjectile>();
		if (component == null)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Item mod not found (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "mod_missing");
			return;
		}
		if (projectileShoot.projectiles.Count > component.numProjectiles)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Count mismatch (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "count_mismatch");
			return;
		}
		Interface.CallHook("OnWeaponFired", new object[]
		{
			this,
			msg.player,
			component,
			projectileShoot
		});
		base.SignalBroadcast(global::BaseEntity.Signal.Attack, string.Empty, msg.connection);
		player.CleanupExpiredProjectiles();
		foreach (ProjectileShoot.Projectile projectile in projectileShoot.projectiles)
		{
			if (player.HasFiredProjectile(projectile.projectileID))
			{
				global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Duplicate ID (" + projectile.projectileID + ")");
				player.stats.combat.Log(this, "duplicate_id");
			}
			else
			{
				if (!base.ValidateEyePos(player, projectile.startPos))
				{
					projectile.startPos = player.eyes.position;
				}
				player.NoteFiredProjectile(projectile.projectileID, projectile.startPos, projectile.startVel, this, ammoType, null);
				this.CreateProjectileEffectClientside(component.projectileObject.resourcePath, projectile.startPos, projectile.startVel, projectile.seed, msg.connection, this.IsSilenced(), false);
			}
		}
		player.stats.Add(component.category + "_fired", projectileShoot.projectiles.Count<ProjectileShoot.Projectile>(), global::Stats.Steam);
		base.StartAttackCooldown(this.ScaleRepeatDelay(this.repeatDelay));
		this.UpdateItemCondition();
		Rust.Ai.Sense.Stimulate(new Rust.Ai.Sensation
		{
			Type = Rust.Ai.SensationType.Gunshot,
			Position = player.GetNetworkPosition(),
			Radius = 100f
		});
		if (global::EACServer.playerTracker != null)
		{
			using (TimeWarning.New("LogPlayerShooting", 0.1f))
			{
				Vector3 networkPosition = player.GetNetworkPosition();
				Vector3 networkRotation = player.GetNetworkRotation();
				global::Item item = this.GetItem();
				int weaponID = (item == null) ? 0 : item.info.itemid;
				PlayerUseWeapon playerUseWeapon = default(PlayerUseWeapon);
				playerUseWeapon.Client = global::EACServer.GetClient(player.net.connection);
				playerUseWeapon.PlayerPosition = new Vector3(networkPosition.x, networkPosition.y, networkPosition.z);
				playerUseWeapon.PlayerViewAngles = new Vector3(networkRotation.x, networkRotation.y, networkRotation.z);
				playerUseWeapon.WeaponID = weaponID;
				global::EACServer.playerTracker.LogPlayerUseWeapon(playerUseWeapon);
			}
		}
	}

	// Token: 0x0600065A RID: 1626 RVA: 0x000275D8 File Offset: 0x000257D8
	private void CreateProjectileEffectClientside(string prefabName, Vector3 pos, Vector3 velocity, int seed, Connection sourceConnection, bool silenced = false, bool forceClientsideEffects = false)
	{
		global::Effect effect = global::BaseProjectile.reusableInstance;
		effect.Clear();
		effect.Init(global::Effect.Type.Projectile, pos, velocity, sourceConnection);
		effect.scale = ((!silenced) ? 1f : 0f);
		if (forceClientsideEffects)
		{
			effect.scale = 2f;
		}
		effect.pooledString = prefabName;
		effect.number = seed;
		global::EffectNetwork.Send(effect);
	}

	// Token: 0x0600065B RID: 1627 RVA: 0x00027640 File Offset: 0x00025840
	public void UpdateItemCondition()
	{
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem == null)
		{
			return;
		}
		if (Random.Range(0, 2) == 0)
		{
			ownerItem.LoseCondition(0.5f);
			if (ownerItem.contents != null && ownerItem.contents.itemList != null)
			{
				for (int i = ownerItem.contents.itemList.Count - 1; i >= 0; i--)
				{
					global::Item item = ownerItem.contents.itemList[i];
					if (item != null)
					{
						item.LoseCondition(0.5f);
					}
				}
			}
		}
	}

	// Token: 0x0600065C RID: 1628 RVA: 0x000276D4 File Offset: 0x000258D4
	public bool IsSilenced()
	{
		if (this.children != null)
		{
			if ((from global::ProjectileWeaponMod x in this.children
			where x != null && x.isSilencer && !x.IsBroken()
			select x).Any<global::ProjectileWeaponMod>())
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x17000058 RID: 88
	// (get) Token: 0x0600065D RID: 1629 RVA: 0x00027728 File Offset: 0x00025928
	private bool UsingInfiniteAmmoCheat
	{
		get
		{
			return false;
		}
	}

	// Token: 0x0600065E RID: 1630 RVA: 0x0002772C File Offset: 0x0002592C
	public override bool CanUseNetworkCache(Connection sendingTo)
	{
		Connection ownerConnection = base.GetOwnerConnection();
		return sendingTo == null || ownerConnection == null || sendingTo != ownerConnection;
	}

	// Token: 0x0600065F RID: 1631 RVA: 0x00027758 File Offset: 0x00025958
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.baseProjectile = Facepunch.Pool.Get<ProtoBuf.BaseProjectile>();
		if (info.forDisk || info.SendingTo(base.GetOwnerConnection()) || this.ForceSendMagazine())
		{
			info.msg.baseProjectile.primaryMagazine = this.primaryMagazine.Save();
		}
	}

	// Token: 0x06000660 RID: 1632 RVA: 0x000277C4 File Offset: 0x000259C4
	public virtual bool ForceSendMagazine()
	{
		return false;
	}

	// Token: 0x06000661 RID: 1633 RVA: 0x000277C8 File Offset: 0x000259C8
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.baseProjectile != null && info.msg.baseProjectile.primaryMagazine != null)
		{
			this.primaryMagazine.Load(info.msg.baseProjectile.primaryMagazine);
		}
	}

	// Token: 0x0400028A RID: 650
	[Header("Projectile")]
	public float damageScale = 1f;

	// Token: 0x0400028B RID: 651
	public float distanceScale = 1f;

	// Token: 0x0400028C RID: 652
	public float projectileVelocityScale = 1f;

	// Token: 0x0400028D RID: 653
	public bool automatic;

	// Token: 0x0400028E RID: 654
	[Header("Effects")]
	public global::GameObjectRef attackFX;

	// Token: 0x0400028F RID: 655
	public global::GameObjectRef silencedAttack;

	// Token: 0x04000290 RID: 656
	public global::GameObjectRef muzzleBrakeAttack;

	// Token: 0x04000291 RID: 657
	public Transform MuzzlePoint;

	// Token: 0x04000292 RID: 658
	[Header("Reloading")]
	public float reloadTime = 1f;

	// Token: 0x04000293 RID: 659
	public bool canUnloadAmmo = true;

	// Token: 0x04000294 RID: 660
	public global::BaseProjectile.Magazine primaryMagazine;

	// Token: 0x04000295 RID: 661
	[Header("Recoil")]
	public float aimSway = 3f;

	// Token: 0x04000296 RID: 662
	public float aimSwaySpeed = 1f;

	// Token: 0x04000297 RID: 663
	public global::RecoilProperties recoil;

	// Token: 0x04000298 RID: 664
	[Header("Aim Cone")]
	public AnimationCurve aimconeCurve = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 1f),
		new Keyframe(1f, 1f)
	});

	// Token: 0x04000299 RID: 665
	public float aimCone;

	// Token: 0x0400029A RID: 666
	public float hipAimCone = 1.8f;

	// Token: 0x0400029B RID: 667
	public float aimconePenaltyPerShot;

	// Token: 0x0400029C RID: 668
	public float aimConePenaltyMax;

	// Token: 0x0400029D RID: 669
	public float aimconePenaltyRecoverTime = 0.1f;

	// Token: 0x0400029E RID: 670
	public float aimconePenaltyRecoverDelay = 0.1f;

	// Token: 0x0400029F RID: 671
	public float stancePenaltyScale = 1f;

	// Token: 0x040002A0 RID: 672
	[Header("Iconsights")]
	public bool hasADS = true;

	// Token: 0x040002A1 RID: 673
	public bool noAimingWhileCycling;

	// Token: 0x040002A2 RID: 674
	public bool manualCycle;

	// Token: 0x040002A3 RID: 675
	[NonSerialized]
	protected bool needsCycle;

	// Token: 0x040002A4 RID: 676
	[NonSerialized]
	protected bool isCycling;

	// Token: 0x040002A5 RID: 677
	[NonSerialized]
	public bool aiming;

	// Token: 0x040002A6 RID: 678
	private float nextReloadTime = float.NegativeInfinity;

	// Token: 0x040002A7 RID: 679
	private float stancePenalty;

	// Token: 0x040002A8 RID: 680
	private float aimconePenalty;

	// Token: 0x040002A9 RID: 681
	protected bool reloadStarted;

	// Token: 0x040002AA RID: 682
	protected bool reloadFinished;

	// Token: 0x040002AB RID: 683
	private static readonly global::Effect reusableInstance = new global::Effect();

	// Token: 0x0200004F RID: 79
	[Serializable]
	public class Magazine
	{
		// Token: 0x0600067A RID: 1658 RVA: 0x0002793C File Offset: 0x00025B3C
		public void ServerInit()
		{
			if (this.definition.builtInSize > 0)
			{
				this.capacity = this.definition.builtInSize;
			}
		}

		// Token: 0x0600067B RID: 1659 RVA: 0x00027960 File Offset: 0x00025B60
		public ProtoBuf.Magazine Save()
		{
			ProtoBuf.Magazine magazine = Facepunch.Pool.Get<ProtoBuf.Magazine>();
			if (this.ammoType == null)
			{
				magazine.capacity = this.capacity;
				magazine.contents = 0;
				magazine.ammoType = 0;
			}
			else
			{
				magazine.capacity = this.capacity;
				magazine.contents = this.contents;
				magazine.ammoType = this.ammoType.itemid;
			}
			return magazine;
		}

		// Token: 0x0600067C RID: 1660 RVA: 0x000279D0 File Offset: 0x00025BD0
		public void Load(ProtoBuf.Magazine mag)
		{
			this.contents = mag.contents;
			this.capacity = mag.capacity;
			this.ammoType = global::ItemManager.FindItemDefinition(mag.ammoType);
		}

		// Token: 0x0600067D RID: 1661 RVA: 0x000279FC File Offset: 0x00025BFC
		public bool CanReload(global::BasePlayer owner)
		{
			return this.contents < this.capacity && owner.inventory.HasAmmo(this.definition.ammoTypes);
		}

		// Token: 0x0600067E RID: 1662 RVA: 0x00027A28 File Offset: 0x00025C28
		public bool CanAiReload(global::BasePlayer owner)
		{
			return this.contents < this.capacity;
		}

		// Token: 0x0600067F RID: 1663 RVA: 0x00027A40 File Offset: 0x00025C40
		public bool Reload(global::BasePlayer owner)
		{
			List<global::Item> list = owner.inventory.FindItemIDs(this.ammoType.itemid).ToList<global::Item>();
			if (list.Count == 0)
			{
				List<global::Item> list2 = new List<global::Item>();
				owner.inventory.FindAmmo(list2, this.definition.ammoTypes);
				if (list2.Count == 0)
				{
					return false;
				}
				list = owner.inventory.FindItemIDs(list2[0].info.itemid).ToList<global::Item>();
				if (list == null || list.Count == 0)
				{
					return false;
				}
				if (this.contents > 0)
				{
					owner.GiveItem(global::ItemManager.CreateByItemID(this.ammoType.itemid, this.contents, 0UL), global::BaseEntity.GiveItemReason.Generic);
					this.contents = 0;
				}
				this.ammoType = list[0].info;
			}
			foreach (global::Item item in list)
			{
				int num = this.capacity - this.contents;
				int num2 = 0;
				while (num2 < num && item.amount > 0)
				{
					this.contents++;
					item.UseItem(1);
					num2++;
				}
			}
			return false;
		}

		// Token: 0x040002C2 RID: 706
		public global::BaseProjectile.Magazine.Definition definition;

		// Token: 0x040002C3 RID: 707
		public int capacity;

		// Token: 0x040002C4 RID: 708
		public int contents;

		// Token: 0x040002C5 RID: 709
		[global::ItemSelector(global::ItemCategory.All)]
		public global::ItemDefinition ammoType;

		// Token: 0x02000050 RID: 80
		[Serializable]
		public struct Definition
		{
			// Token: 0x040002C6 RID: 710
			[Tooltip("Set to 0 to not use inbuilt mag")]
			public int builtInSize;

			// Token: 0x040002C7 RID: 711
			[InspectorFlags]
			[Tooltip("If using inbuilt mag, will accept these types of ammo")]
			public AmmoTypes ammoTypes;
		}
	}
}
