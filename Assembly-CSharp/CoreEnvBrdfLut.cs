﻿using System;
using UnityEngine;

// Token: 0x020005F9 RID: 1529
public class CoreEnvBrdfLut
{
	// Token: 0x06001F37 RID: 7991 RVA: 0x000B12C0 File Offset: 0x000AF4C0
	[RuntimeInitializeOnLoadMethod(1)]
	private static void OnRuntimeLoad()
	{
		global::CoreEnvBrdfLut.PrepareTextureForRuntime();
		global::CoreEnvBrdfLut.UpdateReflProbe();
	}

	// Token: 0x06001F38 RID: 7992 RVA: 0x000B12CC File Offset: 0x000AF4CC
	private static void PrepareTextureForRuntime()
	{
		if (global::CoreEnvBrdfLut.runtimeEnvBrdfLut == null)
		{
			global::CoreEnvBrdfLut.runtimeEnvBrdfLut = global::CoreEnvBrdfLut.Generate(false);
		}
		Shader.SetGlobalTexture("_EnvBrdfLut", global::CoreEnvBrdfLut.runtimeEnvBrdfLut);
	}

	// Token: 0x06001F39 RID: 7993 RVA: 0x000B12F8 File Offset: 0x000AF4F8
	private static void UpdateReflProbe()
	{
		int num = (int)Mathf.Log((float)RenderSettings.defaultReflectionResolution, 2f) - 1;
		if (Shader.GetGlobalFloat("_ReflProbeMaxMip") != (float)num)
		{
			Shader.SetGlobalFloat("_ReflProbeMaxMip", (float)num);
		}
	}

	// Token: 0x06001F3A RID: 7994 RVA: 0x000B1338 File Offset: 0x000AF538
	public static Texture2D Generate(bool asset = false)
	{
		TextureFormat textureFormat = (!asset) ? 16 : 17;
		textureFormat = ((!SystemInfo.SupportsTextureFormat(textureFormat)) ? 5 : textureFormat);
		int num = 128;
		int num2 = 32;
		float num3 = 1f / (float)num;
		float num4 = 1f / (float)num2;
		Texture2D texture2D = new Texture2D(num, num2, textureFormat, false, true);
		texture2D.name = "_EnvBrdfLut";
		texture2D.wrapMode = 1;
		texture2D.filterMode = 1;
		Color[] array = new Color[num * num2];
		float num5 = 0.0078125f;
		for (int i = 0; i < num2; i++)
		{
			float num6 = ((float)i + 0.5f) * num4;
			float num7 = num6 * num6;
			float num8 = num7 * num7;
			int j = 0;
			int num9 = i * num;
			while (j < num)
			{
				float num10 = ((float)j + 0.5f) * num3;
				Vector3 vector;
				vector..ctor(Mathf.Sqrt(1f - num10 * num10), 0f, num10);
				float num11 = 0f;
				float num12 = 0f;
				for (uint num13 = 0u; num13 < 128u; num13 += 1u)
				{
					float num14 = num13 * num5;
					float num15 = (float)(global::CoreEnvBrdfLut.ReverseBits(num13) / 4294967296.0);
					float num16 = 6.28318548f * num14;
					float num17 = Mathf.Sqrt((1f - num15) / (1f + (num8 - 1f) * num15));
					float num18 = Mathf.Sqrt(1f - num17 * num17);
					Vector3 vector2;
					vector2..ctor(num18 * Mathf.Cos(num16), num18 * Mathf.Sin(num16), num17);
					float num19 = Mathf.Max((2f * Vector3.Dot(vector, vector2) * vector2 - vector).z, 0f);
					float num20 = Mathf.Max(vector2.z, 0f);
					float num21 = Mathf.Max(Vector3.Dot(vector, vector2), 0f);
					if (num19 > 0f)
					{
						float num22 = num19 * (num10 * (1f - num7) + num7);
						float num23 = num10 * (num19 * (1f - num7) + num7);
						float num24 = 0.5f / (num22 + num23);
						float num25 = num19 * num24 * (4f * num21 / num20);
						float num26 = 1f - num21;
						num26 *= num26 * num26 * (num26 * num26);
						num11 += num25 * (1f - num26);
						num12 += num25 * num26;
					}
				}
				num11 = Mathf.Clamp(num11 * num5, 0f, 1f);
				num12 = Mathf.Clamp(num12 * num5, 0f, 1f);
				array[num9++] = new Color(num11, num12, 0f, 0f);
				j++;
			}
		}
		texture2D.SetPixels(array);
		texture2D.Apply(false, !asset);
		return texture2D;
	}

	// Token: 0x06001F3B RID: 7995 RVA: 0x000B1624 File Offset: 0x000AF824
	private static uint ReverseBits(uint Bits)
	{
		Bits = (Bits << 16 | Bits >> 16);
		Bits = ((Bits & 16711935u) << 8 | (Bits & 4278255360u) >> 8);
		Bits = ((Bits & 252645135u) << 4 | (Bits & 4042322160u) >> 4);
		Bits = ((Bits & 858993459u) << 2 | (Bits & 3435973836u) >> 2);
		Bits = ((Bits & 1431655765u) << 1 | (Bits & 2863311530u) >> 1);
		return Bits;
	}

	// Token: 0x04001A36 RID: 6710
	private static Texture2D runtimeEnvBrdfLut;
}
