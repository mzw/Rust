﻿using System;
using UnityEngine;

// Token: 0x0200044A RID: 1098
public abstract class LODComponent : global::BaseMonoBehaviour, IClientComponent, global::ILOD
{
	// Token: 0x04001331 RID: 4913
	public global::LODDistanceMode DistanceMode;

	// Token: 0x04001332 RID: 4914
	public global::LODComponent.OccludeeParameters OccludeeParams = new global::LODComponent.OccludeeParameters
	{
		isDynamic = false,
		dynamicUpdateInterval = 0.2f,
		shadowRangeScale = 3f,
		showBounds = false
	};

	// Token: 0x0200044B RID: 1099
	[Serializable]
	public struct OccludeeParameters
	{
		// Token: 0x04001333 RID: 4915
		[Tooltip("Is Occludee dynamic or static?")]
		public bool isDynamic;

		// Token: 0x04001334 RID: 4916
		[Tooltip("Dynamic occludee update interval in seconds; 0 = every frame")]
		public float dynamicUpdateInterval;

		// Token: 0x04001335 RID: 4917
		[Tooltip("Distance scale combined with occludee max bounds size at which culled occludee shadows are still visible")]
		public float shadowRangeScale;

		// Token: 0x04001336 RID: 4918
		[Tooltip("Show culling bounds via gizmos; editor only")]
		public bool showBounds;
	}
}
