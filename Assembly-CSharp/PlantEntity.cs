﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Facepunch.Extend;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000083 RID: 131
public class PlantEntity : global::BaseCombatEntity, global::IInstanceDataReceiver
{
	// Token: 0x060008A3 RID: 2211 RVA: 0x00038D38 File Offset: 0x00036F38
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("PlantEntity.OnRpcMessage", 0.1f))
		{
			if (rpc == 2055322297u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_PickFruit ");
				}
				using (TimeWarning.New("RPC_PickFruit", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_PickFruit", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_PickFruit(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_PickFruit");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 693223260u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_TakeClone ");
				}
				using (TimeWarning.New("RPC_TakeClone", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_TakeClone", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_TakeClone(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_TakeClone");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x17000060 RID: 96
	// (get) Token: 0x060008A4 RID: 2212 RVA: 0x000390A4 File Offset: 0x000372A4
	private global::PlantProperties.Stage currentStage
	{
		get
		{
			return this.plantProperty.stages[(int)this.state];
		}
	}

	// Token: 0x17000061 RID: 97
	// (get) Token: 0x060008A5 RID: 2213 RVA: 0x000390C4 File Offset: 0x000372C4
	private float ageFraction
	{
		get
		{
			return this.age / (this.currentStage.lifeLength * 60f);
		}
	}

	// Token: 0x060008A6 RID: 2214 RVA: 0x000390EC File Offset: 0x000372EC
	public override void ResetState()
	{
		base.ResetState();
		this.state = global::PlantProperties.State.Seed;
	}

	// Token: 0x060008A7 RID: 2215 RVA: 0x000390FC File Offset: 0x000372FC
	public bool CanPick()
	{
		return this.currentStage.resources > 0f;
	}

	// Token: 0x060008A8 RID: 2216 RVA: 0x00039120 File Offset: 0x00037320
	public bool CanClone()
	{
		return this.currentStage.resources > 0f && this.plantProperty.cloneItem != null;
	}

	// Token: 0x060008A9 RID: 2217 RVA: 0x0003915C File Offset: 0x0003735C
	public void ReceiveInstanceData(ProtoBuf.Item.InstanceData data)
	{
		this.genetics = data.dataInt;
	}

	// Token: 0x060008AA RID: 2218 RVA: 0x0003916C File Offset: 0x0003736C
	public float YieldBonusScale()
	{
		return (float)this.consumedWater / (float)this.plantProperty.lifetimeWaterConsumption;
	}

	// Token: 0x060008AB RID: 2219 RVA: 0x00039184 File Offset: 0x00037384
	public override void ServerInit()
	{
		if (this.genetics == -1)
		{
			this.genetics = Random.Range(0, 10000);
		}
		this.groundConditions = global::PlantEntity.WorkoutGroundConditions(base.transform.position);
		base.ServerInit();
		base.InvokeRandomized(new Action(this.RunUpdate), this.thinkDeltaTime, this.thinkDeltaTime, this.thinkDeltaTime * 0.1f);
		base.health = 10f;
		this.ResetSeason();
	}

	// Token: 0x060008AC RID: 2220 RVA: 0x00039208 File Offset: 0x00037408
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.plantEntity = Facepunch.Pool.Get<ProtoBuf.PlantEntity>();
		info.msg.plantEntity.state = (int)this.state;
		info.msg.plantEntity.age = this.age;
		info.msg.plantEntity.genetics = this.genetics;
		info.msg.plantEntity.water = this.water;
		if (!info.forDisk)
		{
			info.msg.plantEntity.healthy = (float)this.consumedWater / (float)this.plantProperty.lifetimeWaterConsumption;
		}
	}

	// Token: 0x060008AD RID: 2221 RVA: 0x000392BC File Offset: 0x000374BC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_TakeClone(global::BaseEntity.RPCMessage msg)
	{
		if (!this.CanClone())
		{
			return;
		}
		int num = 1 + Mathf.Clamp(Mathf.CeilToInt(this.currentStage.resources * (1f + this.YieldBonusScale()) / 0.25f), 1, 4);
		for (int i = 0; i < num; i++)
		{
			global::Item item = global::ItemManager.Create(this.plantProperty.cloneItem, 1, 0UL);
			item.instanceData = new ProtoBuf.Item.InstanceData();
			item.instanceData.dataInt = Mathf.CeilToInt((float)this.genetics * 0.9f);
			item.instanceData.ShouldPool = false;
			msg.player.GiveItem(item, global::BaseEntity.GiveItemReason.PickedUp);
		}
		if (this.plantProperty.pickEffect.isValid)
		{
			global::Effect.server.Run(this.plantProperty.pickEffect.resourcePath, base.GetEstimatedWorldPosition(), Vector3.up, null, false);
		}
		this.Die(null);
	}

	// Token: 0x060008AE RID: 2222 RVA: 0x000393AC File Offset: 0x000375AC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_PickFruit(global::BaseEntity.RPCMessage msg)
	{
		if (!this.CanPick())
		{
			return;
		}
		this.harvests++;
		float num = this.YieldBonusScale() * (float)this.plantProperty.waterYieldBonus;
		int num2 = Mathf.RoundToInt((this.currentStage.resources + num) * (float)this.plantProperty.pickupAmount);
		this.ResetSeason();
		if (this.plantProperty.pickupItem.condition.enabled)
		{
			for (int i = 0; i < num2; i++)
			{
				global::Item item = global::ItemManager.Create(this.plantProperty.pickupItem, 1, 0UL);
				item.conditionNormalized = this.plantProperty.fruitCurve.Evaluate(this.ageFraction);
				Interface.CallHook("OnCropGather", new object[]
				{
					this,
					item,
					msg.player
				});
				msg.player.GiveItem(item, global::BaseEntity.GiveItemReason.PickedUp);
			}
		}
		else
		{
			global::Item item2 = global::ItemManager.Create(this.plantProperty.pickupItem, num2, 0UL);
			Interface.CallHook("OnCropGather", new object[]
			{
				this,
				item2,
				msg.player
			});
			msg.player.GiveItem(item2, global::BaseEntity.GiveItemReason.PickedUp);
		}
		if (this.plantProperty.pickEffect.isValid)
		{
			global::Effect.server.Run(this.plantProperty.pickEffect.resourcePath, base.GetEstimatedWorldPosition(), Vector3.up, null, false);
		}
		if (this.harvests >= this.plantProperty.maxHarvests)
		{
			if (this.plantProperty.disappearAfterHarvest)
			{
				this.Die(null);
			}
			else
			{
				this.BecomeState(global::PlantProperties.State.Dying, true);
			}
		}
		else
		{
			this.BecomeState(global::PlantProperties.State.Mature, true);
		}
	}

	// Token: 0x060008AF RID: 2223 RVA: 0x00039578 File Offset: 0x00037778
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.plantEntity != null)
		{
			this.genetics = info.msg.plantEntity.genetics;
			this.age = info.msg.plantEntity.age;
			this.water = info.msg.plantEntity.water;
			this.BecomeState((global::PlantProperties.State)info.msg.plantEntity.state, false);
		}
	}

	// Token: 0x060008B0 RID: 2224 RVA: 0x000395FC File Offset: 0x000377FC
	private void BecomeState(global::PlantProperties.State state, bool resetAge = true)
	{
		if (base.isServer && this.state == state)
		{
			return;
		}
		this.state = state;
		if (base.isServer)
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			if (resetAge)
			{
				this.age = 0f;
			}
		}
	}

	// Token: 0x060008B1 RID: 2225 RVA: 0x0003964C File Offset: 0x0003784C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x17000062 RID: 98
	// (get) Token: 0x060008B2 RID: 2226 RVA: 0x00039650 File Offset: 0x00037850
	protected float thinkDeltaTime
	{
		get
		{
			return ConVar.Server.planttick;
		}
	}

	// Token: 0x17000063 RID: 99
	// (get) Token: 0x060008B3 RID: 2227 RVA: 0x00039658 File Offset: 0x00037858
	protected float growDeltaTime
	{
		get
		{
			return ConVar.Server.planttick * ConVar.Server.planttickscale;
		}
	}

	// Token: 0x060008B4 RID: 2228 RVA: 0x00039668 File Offset: 0x00037868
	public void ResetSeason()
	{
		this.consumedWater = 0;
		if (this.water == -1)
		{
			this.water = Mathf.CeilToInt((float)this.plantProperty.maxHeldWater * 0.5f);
		}
	}

	// Token: 0x060008B5 RID: 2229 RVA: 0x0003969C File Offset: 0x0003789C
	public override string DebugText()
	{
		return string.Format("State: {0}\nGenetics: {1:0.00}\nHealth: {2:0.00}\nGroundCondition: {3:0.00}\nHappiness: {4:0.00}\nWater: {5:0.00}\nAge: {6}", new object[]
		{
			this.state,
			this.genetics,
			base.health,
			this.groundConditions,
			this.Happiness(),
			this.water,
			NumberExtensions.FormatSeconds((long)this.realAge)
		});
	}

	// Token: 0x060008B6 RID: 2230 RVA: 0x0003971C File Offset: 0x0003791C
	public void RefreshLightExposure()
	{
		if (!ConVar.Server.plantlightdetection)
		{
			this.lightExposure = this.plantProperty.timeOfDayHappiness.Evaluate(TOD_Sky.Instance.Cycle.Hour);
			return;
		}
		this.lightExposure = this.CalculateSunExposure() * this.plantProperty.timeOfDayHappiness.Evaluate(TOD_Sky.Instance.Cycle.Hour);
		if (this.lightExposure <= 0f)
		{
			this.lightExposure = this.CalculateArtificialLightExposure() * 2f;
		}
	}

	// Token: 0x060008B7 RID: 2231 RVA: 0x000397A8 File Offset: 0x000379A8
	public float CalculateArtificialLightExposure()
	{
		float num = 0f;
		List<global::CeilingLight> list = Facepunch.Pool.GetList<global::CeilingLight>();
		global::Vis.Entities<global::CeilingLight>(base.GetEstimatedWorldPosition() + new Vector3(0f, 2f, 0f), 2f, list, 256, 2);
		foreach (global::CeilingLight ceilingLight in list)
		{
			if (ceilingLight.IsOn())
			{
				num += 1f;
				break;
			}
		}
		Facepunch.Pool.FreeList<global::CeilingLight>(ref list);
		return num;
	}

	// Token: 0x060008B8 RID: 2232 RVA: 0x00039854 File Offset: 0x00037A54
	public float CalculateSunExposure()
	{
		if (TOD_Sky.Instance.IsNight)
		{
			return 0f;
		}
		Vector3 vector = base.GetEstimatedWorldPosition() + new Vector3(0f, 1f, 0f);
		Vector3 position = TOD_Sky.Instance.Components.Sun.transform.position;
		Vector3 vector2 = position - vector.normalized;
		RaycastHit raycastHit;
		if (UnityEngine.Physics.Raycast(vector, vector2, ref raycastHit, 100f, 10551297))
		{
			return 0f;
		}
		return 1f;
	}

	// Token: 0x060008B9 RID: 2233 RVA: 0x000398E4 File Offset: 0x00037AE4
	private void RunUpdate()
	{
		if (this.IsDead())
		{
			return;
		}
		this.RefreshLightExposure();
		float num = this.Happiness();
		this.realAge += this.thinkDeltaTime;
		this.age += this.growDeltaTime * Mathf.Max(num, 0f);
		base.health += num * this.currentStage.health * this.growDeltaTime;
		if (base.health <= 0f)
		{
			this.Die(null);
			return;
		}
		if (this.age > this.currentStage.lifeLength * 60f)
		{
			if (this.state == global::PlantProperties.State.Dying)
			{
				this.Die(null);
				return;
			}
			if (this.currentStage.nextState <= this.state)
			{
				this.seasons++;
			}
			if (this.seasons >= this.plantProperty.maxSeasons)
			{
				this.BecomeState(global::PlantProperties.State.Dying, true);
			}
			else
			{
				this.BecomeState(this.currentStage.nextState, true);
			}
		}
		if (this.PlacedInPlanter() && this.consumedWater < this.plantProperty.lifetimeWaterConsumption && this.state < global::PlantProperties.State.Fruiting)
		{
			float num2 = this.thinkDeltaTime / (this.plantProperty.waterConsumptionLifetime * 60f) * (float)this.plantProperty.lifetimeWaterConsumption;
			int num3 = Mathf.CeilToInt(Mathf.Min((float)this.water, num2));
			this.water -= num3;
			this.consumedWater += num3;
			global::PlanterBox planterBox = base.GetParentEntity() as global::PlanterBox;
			if (planterBox && planterBox.soilSaturationFraction > 0f)
			{
				int num4 = this.plantProperty.maxHeldWater - this.water;
				int num5 = planterBox.UseWater(Mathf.Min(Mathf.CeilToInt(num2 * 10f), num4));
				this.water += num5;
			}
		}
		else
		{
			this.water = this.plantProperty.maxHeldWater;
		}
		this.water = Mathf.Clamp(this.water, 0, this.plantProperty.maxHeldWater);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060008BA RID: 2234 RVA: 0x00039B38 File Offset: 0x00037D38
	private bool PlacedInPlanter()
	{
		return base.GetParentEntity() != null && base.GetParentEntity() is global::PlanterBox;
	}

	// Token: 0x060008BB RID: 2235 RVA: 0x00039B60 File Offset: 0x00037D60
	private float Happiness()
	{
		bool flag = this.PlacedInPlanter();
		float num = 0f;
		num += this.Energy_Light();
		num += this.Energy_Temperature();
		num += this.Energy_Water();
		num += ((!flag) ? this.groundConditions : 2f);
		num /= 4f;
		float num2 = (float)this.genetics / 10000f;
		num = Mathf.Clamp(num, -1f, 0.25f + num2 * 0.75f);
		if (num > -0.1f && num < 0.1f)
		{
			num = Mathf.Sign(num) * 0.1f;
		}
		return num;
	}

	// Token: 0x060008BC RID: 2236 RVA: 0x00039C04 File Offset: 0x00037E04
	private float Energy_Light()
	{
		return this.lightExposure;
	}

	// Token: 0x060008BD RID: 2237 RVA: 0x00039C0C File Offset: 0x00037E0C
	private float Energy_Temperature()
	{
		float num = this.plantProperty.temperatureHappiness.Evaluate(this.GetTemperature());
		if (num > 0f)
		{
			return num * 0.2f;
		}
		return num;
	}

	// Token: 0x060008BE RID: 2238 RVA: 0x00039C44 File Offset: 0x00037E44
	private float Energy_Water()
	{
		return (float)this.water;
	}

	// Token: 0x060008BF RID: 2239 RVA: 0x00039C50 File Offset: 0x00037E50
	private float GetTemperature()
	{
		float num = global::Climate.GetTemperature(base.GetEstimatedWorldPosition());
		if (this.PlacedInPlanter() && num < 10f)
		{
			num = 10f;
		}
		return num;
	}

	// Token: 0x060008C0 RID: 2240 RVA: 0x00039C88 File Offset: 0x00037E88
	public static float WorkoutGroundConditions(Vector3 pos)
	{
		if (global::WaterLevel.Test(pos))
		{
			return -1f;
		}
		global::TerrainSplat.Enum splatMaxType = (global::TerrainSplat.Enum)global::TerrainMeta.SplatMap.GetSplatMaxType(pos, -1);
		switch (splatMaxType)
		{
		case global::TerrainSplat.Enum.Dirt:
			return 0.5f;
		case global::TerrainSplat.Enum.Snow:
			return -1f;
		default:
			if (splatMaxType == global::TerrainSplat.Enum.Grass)
			{
				return 0.5f;
			}
			if (splatMaxType == global::TerrainSplat.Enum.Forest)
			{
				return 0.4f;
			}
			if (splatMaxType == global::TerrainSplat.Enum.Stones)
			{
				return -0.6f;
			}
			if (splatMaxType != global::TerrainSplat.Enum.Gravel)
			{
				return 0.5f;
			}
			return -0.6f;
		case global::TerrainSplat.Enum.Sand:
			return -0.3f;
		case global::TerrainSplat.Enum.Rock:
			return -0.7f;
		}
	}

	// Token: 0x04000403 RID: 1027
	public global::PlantProperties plantProperty;

	// Token: 0x04000404 RID: 1028
	public int water = -1;

	// Token: 0x04000405 RID: 1029
	public int consumedWater = -1;

	// Token: 0x04000406 RID: 1030
	private global::PlantProperties.State state;

	// Token: 0x04000407 RID: 1031
	private float age;

	// Token: 0x04000408 RID: 1032
	private float groundConditions = 1f;

	// Token: 0x04000409 RID: 1033
	private float realAge;

	// Token: 0x0400040A RID: 1034
	private float lightExposure;

	// Token: 0x0400040B RID: 1035
	private int genetics = -1;

	// Token: 0x0400040C RID: 1036
	private int seasons;

	// Token: 0x0400040D RID: 1037
	private int harvests;
}
