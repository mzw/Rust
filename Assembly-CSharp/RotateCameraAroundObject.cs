﻿using System;
using UnityEngine;

// Token: 0x02000759 RID: 1881
public class RotateCameraAroundObject : MonoBehaviour
{
	// Token: 0x06002331 RID: 9009 RVA: 0x000C2E10 File Offset: 0x000C1010
	private void FixedUpdate()
	{
		if (this.m_goObjectToRotateAround != null)
		{
			base.transform.LookAt(this.m_goObjectToRotateAround.transform.position + Vector3.up * 0.75f);
			base.transform.Translate(Vector3.right * this.m_flRotateSpeed * Time.deltaTime);
		}
	}

	// Token: 0x04001F91 RID: 8081
	public GameObject m_goObjectToRotateAround;

	// Token: 0x04001F92 RID: 8082
	public float m_flRotateSpeed = 10f;
}
