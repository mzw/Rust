﻿using System;
using UnityEngine;

// Token: 0x02000753 RID: 1875
[ExecuteInEditMode]
public class OneActiveSibling : MonoBehaviour
{
	// Token: 0x06002310 RID: 8976 RVA: 0x000C2980 File Offset: 0x000C0B80
	[ComponentHelp("This component will disable all of its siblings when it becomes enabled. This can be useful in situations where you only ever want one of the children active - but don't want to manage turning each one off.")]
	private void OnEnable()
	{
		foreach (Transform transform in base.transform.GetSiblings(false))
		{
			transform.gameObject.SetActive(false);
		}
	}
}
