﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000236 RID: 566
public class OptimizeAnimator : global::ArticulatedOccludee
{
	// Token: 0x06001013 RID: 4115 RVA: 0x00061A60 File Offset: 0x0005FC60
	protected override void OnEnable()
	{
		base.OnEnable();
		if (this.optimizeOnStart)
		{
			List<SkinnedMeshRenderer> list = Pool.GetList<SkinnedMeshRenderer>();
			base.GetComponentsInChildren<SkinnedMeshRenderer>(list);
			this.ProcessRootOptimization(list, base.transform);
			this.UpdateSkinnedRenderers();
			Pool.FreeList<SkinnedMeshRenderer>(ref list);
		}
	}

	// Token: 0x06001014 RID: 4116 RVA: 0x00061AA8 File Offset: 0x0005FCA8
	public void ClearSkinnedRenderers()
	{
		if (this.rendererSet != null)
		{
			this.rendererSet.Clear();
		}
	}

	// Token: 0x06001015 RID: 4117 RVA: 0x00061AC0 File Offset: 0x0005FCC0
	public void ProcessRootOptimization(List<SkinnedMeshRenderer> skinnedRenderers, Transform reference)
	{
		if (skinnedRenderers != null && skinnedRenderers.Count > 0)
		{
			foreach (SkinnedMeshRenderer skinnedMeshRenderer in skinnedRenderers)
			{
				if (skinnedMeshRenderer != null)
				{
					skinnedMeshRenderer.rootBone = null;
					skinnedMeshRenderer.transform.position = reference.position;
					skinnedMeshRenderer.transform.rotation = reference.rotation;
					skinnedMeshRenderer.localBounds = this.fixedLocalBounds;
					if (this.rendererSet == null)
					{
						this.rendererSet = new HashSet<SkinnedMeshRenderer>();
					}
					if (base.FixedBoundsRef == null)
					{
						base.FixedBoundsRef = skinnedMeshRenderer;
					}
					this.rendererSet.Add(skinnedMeshRenderer);
				}
			}
		}
	}

	// Token: 0x06001016 RID: 4118 RVA: 0x00061BA0 File Offset: 0x0005FDA0
	public void UpdateSkinnedRenderers()
	{
		if (this.rendererSet != null && this.followBone != null)
		{
			Vector3 position = this.followBone.position + this.followBoneOffset;
			foreach (SkinnedMeshRenderer skinnedMeshRenderer in this.rendererSet)
			{
				if (skinnedMeshRenderer != null)
				{
					skinnedMeshRenderer.transform.position = position;
				}
			}
		}
	}

	// Token: 0x06001017 RID: 4119 RVA: 0x00061C20 File Offset: 0x0005FE20
	public override void TriggerUpdateVisibilityBounds()
	{
		if (base.enabled)
		{
			base.TriggerUpdateVisibilityBounds();
			this.UpdateSkinnedRenderers();
		}
	}

	// Token: 0x04000AD4 RID: 2772
	public Bounds fixedLocalBounds = new Bounds(new Vector3(0f, 1f, 0f), new Vector3(1.5f, 2.5f, 1.5f));

	// Token: 0x04000AD5 RID: 2773
	public Transform followBone;

	// Token: 0x04000AD6 RID: 2774
	public Vector3 followBoneOffset = new Vector3(0f, 0f, 0f);

	// Token: 0x04000AD7 RID: 2775
	public bool optimizeOnStart;

	// Token: 0x04000AD8 RID: 2776
	private HashSet<SkinnedMeshRenderer> rendererSet;
}
