﻿using System;
using UnityEngine;

// Token: 0x02000781 RID: 1921
public class FixedRateStepped
{
	// Token: 0x060023BB RID: 9147 RVA: 0x000C5A28 File Offset: 0x000C3C28
	public bool ShouldStep()
	{
		if (this.nextCall > Time.time)
		{
			return false;
		}
		if (this.nextCall == 0f)
		{
			this.nextCall = Time.time;
		}
		if (this.nextCall + this.rate * (float)this.maxSteps < Time.time)
		{
			this.nextCall = Time.time - this.rate * (float)this.maxSteps;
		}
		this.nextCall += this.rate;
		return true;
	}

	// Token: 0x04001FA1 RID: 8097
	public float rate = 0.1f;

	// Token: 0x04001FA2 RID: 8098
	public int maxSteps = 3;

	// Token: 0x04001FA3 RID: 8099
	internal float nextCall;
}
