﻿using System;
using UnityEngine;

// Token: 0x020001C1 RID: 449
[Serializable]
public struct VitalLevel
{
	// Token: 0x06000E91 RID: 3729 RVA: 0x0005BD3C File Offset: 0x00059F3C
	internal void Add(float f)
	{
		this.Level += f;
		if (this.Level > 1f)
		{
			this.Level = 1f;
		}
		if (this.Level < 0f)
		{
			this.Level = 0f;
		}
	}

	// Token: 0x170000DC RID: 220
	// (get) Token: 0x06000E92 RID: 3730 RVA: 0x0005BD90 File Offset: 0x00059F90
	public float TimeSinceUsed
	{
		get
		{
			return Time.time - this.lastUsedTime;
		}
	}

	// Token: 0x06000E93 RID: 3731 RVA: 0x0005BDA0 File Offset: 0x00059FA0
	internal void Use(float f)
	{
		if (Mathf.Approximately(f, 0f))
		{
			return;
		}
		this.Level -= Mathf.Abs(f);
		if (this.Level < 0f)
		{
			this.Level = 0f;
		}
		this.lastUsedTime = Time.time;
	}

	// Token: 0x040008A6 RID: 2214
	public float Level;

	// Token: 0x040008A7 RID: 2215
	private float lastUsedTime;
}
