﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020003E0 RID: 992
[CreateAssetMenu(menuName = "Rust/TrappableWildlife")]
[Serializable]
public class TrappableWildlife : ScriptableObject
{
	// Token: 0x040011BC RID: 4540
	public global::GameObjectRef worldObject;

	// Token: 0x040011BD RID: 4541
	public global::ItemDefinition inventoryObject;

	// Token: 0x040011BE RID: 4542
	public int minToCatch;

	// Token: 0x040011BF RID: 4543
	public int maxToCatch;

	// Token: 0x040011C0 RID: 4544
	public List<global::TrappableWildlife.BaitType> baitTypes;

	// Token: 0x040011C1 RID: 4545
	public int caloriesForInterest = 20;

	// Token: 0x040011C2 RID: 4546
	public float successRate = 1f;

	// Token: 0x040011C3 RID: 4547
	public float xpScale = 1f;

	// Token: 0x020003E1 RID: 993
	[Serializable]
	public class BaitType
	{
		// Token: 0x040011C4 RID: 4548
		public float successRate = 1f;

		// Token: 0x040011C5 RID: 4549
		public global::ItemDefinition bait;

		// Token: 0x040011C6 RID: 4550
		public int minForInterest = 1;

		// Token: 0x040011C7 RID: 4551
		public int maxToConsume = 1;
	}
}
