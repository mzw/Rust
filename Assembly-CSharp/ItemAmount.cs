﻿using System;
using UnityEngine;

// Token: 0x020004FC RID: 1276
[Serializable]
public class ItemAmount : ISerializationCallbackReceiver
{
	// Token: 0x06001B1D RID: 6941 RVA: 0x00097B70 File Offset: 0x00095D70
	public ItemAmount(global::ItemDefinition item = null, float amt = 0f)
	{
		this.itemDef = item;
		this.amount = amt;
		this.startAmount = this.amount;
	}

	// Token: 0x170001D9 RID: 473
	// (get) Token: 0x06001B1E RID: 6942 RVA: 0x00097B94 File Offset: 0x00095D94
	public int itemid
	{
		get
		{
			if (this.itemDef == null)
			{
				return 0;
			}
			return this.itemDef.itemid;
		}
	}

	// Token: 0x06001B1F RID: 6943 RVA: 0x00097BB4 File Offset: 0x00095DB4
	public virtual float GetAmount()
	{
		return this.amount;
	}

	// Token: 0x06001B20 RID: 6944 RVA: 0x00097BBC File Offset: 0x00095DBC
	public virtual void OnAfterDeserialize()
	{
		this.startAmount = this.amount;
	}

	// Token: 0x06001B21 RID: 6945 RVA: 0x00097BCC File Offset: 0x00095DCC
	public virtual void OnBeforeSerialize()
	{
	}

	// Token: 0x040015EE RID: 5614
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition itemDef;

	// Token: 0x040015EF RID: 5615
	public float amount;

	// Token: 0x040015F0 RID: 5616
	[NonSerialized]
	public float startAmount;
}
