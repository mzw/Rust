﻿using System;
using UnityEngine;

// Token: 0x020007C7 RID: 1991
public class ViewmodelSway : MonoBehaviour
{
	// Token: 0x0400205E RID: 8286
	public float positionalSwaySpeed = 1f;

	// Token: 0x0400205F RID: 8287
	public float positionalSwayAmount = 1f;

	// Token: 0x04002060 RID: 8288
	public float rotationSwaySpeed = 1f;

	// Token: 0x04002061 RID: 8289
	public float rotationSwayAmount = 1f;
}
