﻿using System;
using UnityEngine;

// Token: 0x0200077B RID: 1915
public class ClimateBlendTexture : global::ProcessedTexture
{
	// Token: 0x06002394 RID: 9108 RVA: 0x000C5218 File Offset: 0x000C3418
	public ClimateBlendTexture(int width, int height, bool linear = true)
	{
		this.material = base.CreateMaterial("Hidden/ClimateBlendLUTs");
		this.result = base.CreateRenderTexture("Climate Blend Texture", width, height, linear);
		this.result.wrapMode = 1;
	}

	// Token: 0x06002395 RID: 9109 RVA: 0x000C5254 File Offset: 0x000C3454
	public bool CheckLostData()
	{
		if (!this.result.IsCreated())
		{
			this.result.Create();
			return true;
		}
		return false;
	}

	// Token: 0x06002396 RID: 9110 RVA: 0x000C5278 File Offset: 0x000C3478
	public void Blend(Texture srcLut1, Texture dstLut1, float lerpLut1, Texture srcLut2, Texture dstLut2, float lerpLut2, float lerp, global::ClimateBlendTexture prevLut, float time)
	{
		this.material.SetTexture("_srcLut1", srcLut1);
		this.material.SetTexture("_dstLut1", dstLut1);
		this.material.SetTexture("_srcLut2", srcLut2);
		this.material.SetTexture("_dstLut2", dstLut2);
		this.material.SetTexture("_prevLut", prevLut);
		this.material.SetFloat("_lerpLut1", lerpLut1);
		this.material.SetFloat("_lerpLut2", lerpLut2);
		this.material.SetFloat("_lerp", lerp);
		this.material.SetFloat("_time", time);
		Graphics.Blit(null, this.result, this.material);
	}

	// Token: 0x06002397 RID: 9111 RVA: 0x000C533C File Offset: 0x000C353C
	public static void Swap(ref global::ClimateBlendTexture a, ref global::ClimateBlendTexture b)
	{
		global::ClimateBlendTexture climateBlendTexture = a;
		a = b;
		b = climateBlendTexture;
	}
}
