﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x0200037E RID: 894
public class BuildingBlockDecay : global::Decay
{
	// Token: 0x0600152F RID: 5423 RVA: 0x0007A6B0 File Offset: 0x000788B0
	public override float GetDecayDelay(global::BaseEntity entity)
	{
		global::BuildingBlock buildingBlock = entity as global::BuildingBlock;
		global::BuildingGrade.Enum grade = (!buildingBlock) ? global::BuildingGrade.Enum.Twigs : buildingBlock.grade;
		return base.GetDecayDelay(grade);
	}

	// Token: 0x06001530 RID: 5424 RVA: 0x0007A6E4 File Offset: 0x000788E4
	public override float GetDecayDuration(global::BaseEntity entity)
	{
		global::BuildingBlock buildingBlock = entity as global::BuildingBlock;
		global::BuildingGrade.Enum grade = (!buildingBlock) ? global::BuildingGrade.Enum.Twigs : buildingBlock.grade;
		return base.GetDecayDuration(grade);
	}

	// Token: 0x06001531 RID: 5425 RVA: 0x0007A718 File Offset: 0x00078918
	public override bool ShouldDecay(global::BaseEntity entity)
	{
		if (ConVar.Decay.upkeep)
		{
			return true;
		}
		if (this.isFoundation)
		{
			return true;
		}
		global::BuildingBlock buildingBlock = entity as global::BuildingBlock;
		global::BuildingGrade.Enum @enum = (!buildingBlock) ? global::BuildingGrade.Enum.Twigs : buildingBlock.grade;
		return @enum == global::BuildingGrade.Enum.Twigs;
	}

	// Token: 0x06001532 RID: 5426 RVA: 0x0007A764 File Offset: 0x00078964
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		this.isFoundation = name.Contains("foundation");
	}

	// Token: 0x04000FA3 RID: 4003
	private bool isFoundation;
}
