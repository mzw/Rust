﻿using System;
using System.Runtime.InteropServices;
using System.Security;

// Token: 0x0200051B RID: 1307
[SuppressUnmanagedCodeSecurity]
public static class NativeNoise
{
	// Token: 0x06001BAF RID: 7087
	[DllImport("RustNative", EntryPoint = "snoise1")]
	public static extern double Simplex1D(double x);

	// Token: 0x06001BB0 RID: 7088
	[DllImport("RustNative", EntryPoint = "sdnoise1")]
	public static extern double Simplex1D(double x, out double dx);

	// Token: 0x06001BB1 RID: 7089
	[DllImport("RustNative", EntryPoint = "snoise2")]
	public static extern double Simplex2D(double x, double y);

	// Token: 0x06001BB2 RID: 7090
	[DllImport("RustNative", EntryPoint = "sdnoise2")]
	public static extern double Simplex2D(double x, double y, out double dx, out double dy);

	// Token: 0x06001BB3 RID: 7091
	[DllImport("RustNative", EntryPoint = "turbulence")]
	public static extern double Turbulence(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BB4 RID: 7092
	[DllImport("RustNative", EntryPoint = "billow")]
	public static extern double Billow(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BB5 RID: 7093
	[DllImport("RustNative", EntryPoint = "ridge")]
	public static extern double Ridge(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BB6 RID: 7094
	[DllImport("RustNative", EntryPoint = "sharp")]
	public static extern double Sharp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BB7 RID: 7095
	[DllImport("RustNative", EntryPoint = "turbulence_iq")]
	public static extern double TurbulenceIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BB8 RID: 7096
	[DllImport("RustNative", EntryPoint = "billow_iq")]
	public static extern double BillowIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BB9 RID: 7097
	[DllImport("RustNative", EntryPoint = "ridge_iq")]
	public static extern double RidgeIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BBA RID: 7098
	[DllImport("RustNative", EntryPoint = "sharp_iq")]
	public static extern double SharpIQ(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain);

	// Token: 0x06001BBB RID: 7099
	[DllImport("RustNative", EntryPoint = "turbulence_warp")]
	public static extern double TurbulenceWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp);

	// Token: 0x06001BBC RID: 7100
	[DllImport("RustNative", EntryPoint = "billow_warp")]
	public static extern double BillowWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp);

	// Token: 0x06001BBD RID: 7101
	[DllImport("RustNative", EntryPoint = "ridge_warp")]
	public static extern double RidgeWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp);

	// Token: 0x06001BBE RID: 7102
	[DllImport("RustNative", EntryPoint = "sharp_warp")]
	public static extern double SharpWarp(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp);

	// Token: 0x06001BBF RID: 7103
	[DllImport("RustNative", EntryPoint = "jordan")]
	public static extern double Jordan(double x, double y, int octaves, double frequency, double amplitude, double lacunarity, double gain, double warp, double damp, double damp_scale);
}
