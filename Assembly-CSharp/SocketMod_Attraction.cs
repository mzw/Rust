﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000226 RID: 550
public class SocketMod_Attraction : global::SocketMod
{
	// Token: 0x06000FE7 RID: 4071 RVA: 0x00060C20 File Offset: 0x0005EE20
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = new Color(1f, 1f, 0f, 0.3f);
		Gizmos.DrawSphere(Vector3.zero, this.outerRadius);
		Gizmos.color = new Color(0f, 1f, 0f, 0.6f);
		Gizmos.DrawSphere(Vector3.zero, this.innerRadius);
	}

	// Token: 0x06000FE8 RID: 4072 RVA: 0x00060C9C File Offset: 0x0005EE9C
	public override bool DoCheck(global::Construction.Placement place)
	{
		return true;
	}

	// Token: 0x06000FE9 RID: 4073 RVA: 0x00060CA0 File Offset: 0x0005EEA0
	public override void ModifyPlacement(global::Construction.Placement place)
	{
		Vector3 vector = place.position + place.rotation * this.worldPosition;
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(vector, this.outerRadius * 2f, list, -1, 2);
		foreach (global::BaseEntity baseEntity in list)
		{
			if (baseEntity.isServer == this.isServer)
			{
				global::AttractionPoint[] array = this.prefabAttribute.FindAll<global::AttractionPoint>(baseEntity.prefabID);
				if (array != null)
				{
					foreach (global::AttractionPoint attractionPoint in array)
					{
						if (!(attractionPoint.groupName != this.groupName))
						{
							Vector3 vector2 = baseEntity.transform.position + baseEntity.transform.rotation * attractionPoint.worldPosition;
							float magnitude = (vector2 - vector).magnitude;
							if (magnitude <= this.outerRadius)
							{
								Quaternion quaternion = UnityEngine.QuaternionEx.LookRotationWithOffset(this.worldPosition, vector2 - place.position, Vector3.up);
								float num = Mathf.InverseLerp(this.outerRadius, this.innerRadius, magnitude);
								place.rotation = Quaternion.Lerp(place.rotation, quaternion, num);
								vector = place.position + place.rotation * this.worldPosition;
								Vector3 vector3 = vector2 - vector;
								place.position += vector3 * num;
							}
						}
					}
				}
			}
		}
		Pool.FreeList<global::BaseEntity>(ref list);
	}

	// Token: 0x04000A9E RID: 2718
	public float outerRadius = 1f;

	// Token: 0x04000A9F RID: 2719
	public float innerRadius = 0.1f;

	// Token: 0x04000AA0 RID: 2720
	public string groupName = "wallbottom";
}
