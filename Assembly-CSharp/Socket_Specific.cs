﻿using System;
using UnityEngine;

// Token: 0x0200021F RID: 543
public class Socket_Specific : global::Socket_Base
{
	// Token: 0x06000FCD RID: 4045 RVA: 0x00060470 File Offset: 0x0005E670
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.red;
		Gizmos.DrawLine(Vector3.zero, Vector3.forward * 0.2f);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(Vector3.zero, Vector3.right * 0.1f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(Vector3.zero, Vector3.up * 0.1f);
		Gizmos.DrawIcon(base.transform.position, "light_circle_green.png", false);
	}

	// Token: 0x06000FCE RID: 4046 RVA: 0x0006050C File Offset: 0x0005E70C
	public override bool TestTarget(global::Construction.Target target)
	{
		if (!base.TestTarget(target))
		{
			return false;
		}
		global::Socket_Specific_Female socket_Specific_Female = target.socket as global::Socket_Specific_Female;
		return !(socket_Specific_Female == null) && socket_Specific_Female.CanAccept(this);
	}

	// Token: 0x06000FCF RID: 4047 RVA: 0x0006054C File Offset: 0x0005E74C
	public override global::Construction.Placement DoPlacement(global::Construction.Target target)
	{
		Quaternion quaternion = target.socket.rotation;
		if (target.socket.male && target.socket.female)
		{
			quaternion = target.socket.rotation * Quaternion.Euler(180f, 0f, 180f);
		}
		Transform transform = target.entity.transform;
		Vector3 vector = transform.localToWorldMatrix.MultiplyPoint3x4(target.socket.position);
		Quaternion quaternion2;
		if (this.useFemaleRotation)
		{
			quaternion2 = transform.rotation * quaternion;
		}
		else
		{
			Vector3 vector2;
			vector2..ctor(vector.x, 0f, vector.z);
			Vector3 vector3;
			vector3..ctor(target.player.eyes.position.x, 0f, target.player.eyes.position.z);
			Vector3 normalized = (vector2 - vector3).normalized;
			quaternion2 = Quaternion.LookRotation(normalized) * quaternion;
		}
		global::Construction.Placement placement = new global::Construction.Placement();
		Quaternion quaternion3 = quaternion2 * Quaternion.Inverse(this.rotation);
		Vector3 vector4 = quaternion3 * this.position;
		placement.position = vector - vector4;
		placement.rotation = quaternion3;
		return placement;
	}

	// Token: 0x04000A90 RID: 2704
	public bool useFemaleRotation = true;

	// Token: 0x04000A91 RID: 2705
	public string targetSocketName;
}
