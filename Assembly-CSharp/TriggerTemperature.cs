﻿using System;
using UnityEngine;

// Token: 0x0200049D RID: 1181
public class TriggerTemperature : global::TriggerBase
{
	// Token: 0x060019B7 RID: 6583 RVA: 0x00090AD0 File Offset: 0x0008ECD0
	private void OnValidate()
	{
		this.triggerSize = base.GetComponent<SphereCollider>().radius * base.transform.localScale.y;
	}

	// Token: 0x060019B8 RID: 6584 RVA: 0x00090B04 File Offset: 0x0008ED04
	public float WorkoutTemperature(Vector3 position, float oldTemperature)
	{
		float num = Vector3.Distance(base.gameObject.transform.position, position);
		float num2 = Mathf.InverseLerp(this.triggerSize, 0f, num);
		return Mathf.Lerp(oldTemperature, this.Temperature, num2);
	}

	// Token: 0x060019B9 RID: 6585 RVA: 0x00090B48 File Offset: 0x0008ED48
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x0400144D RID: 5197
	public float Temperature = 50f;

	// Token: 0x0400144E RID: 5198
	public float triggerSize;
}
