﻿using System;
using UnityEngine;

// Token: 0x0200055A RID: 1370
public class TerrainCheck : global::PrefabAttribute
{
	// Token: 0x06001CAF RID: 7343 RVA: 0x000A0BAC File Offset: 0x0009EDAC
	protected void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		Gizmos.DrawLine(base.transform.position - Vector3.up * this.Extents, base.transform.position + Vector3.up * this.Extents);
	}

	// Token: 0x06001CB0 RID: 7344 RVA: 0x000A0C1C File Offset: 0x0009EE1C
	public bool Check(Vector3 pos)
	{
		float height = global::TerrainMeta.HeightMap.GetHeight(pos);
		return pos.y - this.Extents <= height && pos.y + this.Extents >= height;
	}

	// Token: 0x06001CB1 RID: 7345 RVA: 0x000A0C64 File Offset: 0x0009EE64
	protected override Type GetIndexedType()
	{
		return typeof(global::TerrainCheck);
	}

	// Token: 0x040017D1 RID: 6097
	public float Extents = 1f;
}
