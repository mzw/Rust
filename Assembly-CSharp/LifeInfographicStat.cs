﻿using System;
using UnityEngine;

// Token: 0x020006CD RID: 1741
public class LifeInfographicStat : MonoBehaviour
{
	// Token: 0x04001D88 RID: 7560
	public global::LifeInfographicStat.DataType dataSource;

	// Token: 0x020006CE RID: 1742
	public enum DataType
	{
		// Token: 0x04001D8A RID: 7562
		None,
		// Token: 0x04001D8B RID: 7563
		AliveTime_Short,
		// Token: 0x04001D8C RID: 7564
		SleepingTime_Short,
		// Token: 0x04001D8D RID: 7565
		KillerName,
		// Token: 0x04001D8E RID: 7566
		KillerWeapon,
		// Token: 0x04001D8F RID: 7567
		AliveTime_Long
	}
}
