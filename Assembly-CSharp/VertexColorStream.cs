﻿using System;
using UnityEngine;

// Token: 0x02000820 RID: 2080
[ExecuteInEditMode]
public class VertexColorStream : MonoBehaviour
{
	// Token: 0x060026A1 RID: 9889 RVA: 0x000D68D8 File Offset: 0x000D4AD8
	private void OnDidApplyAnimationProperties()
	{
	}

	// Token: 0x060026A2 RID: 9890 RVA: 0x000D68DC File Offset: 0x000D4ADC
	public void init(Mesh origMesh, bool destroyOld)
	{
		this.originalMesh = origMesh;
		this.paintedMesh = Object.Instantiate<Mesh>(origMesh);
		if (destroyOld)
		{
			Object.DestroyImmediate(origMesh);
		}
		this.paintedMesh.hideFlags = 0;
		this.paintedMesh.name = "vpp_" + base.gameObject.name;
		this.meshHold = new global::MeshHolder();
		this.meshHold._vertices = this.paintedMesh.vertices;
		this.meshHold._normals = this.paintedMesh.normals;
		this.meshHold._triangles = this.paintedMesh.triangles;
		this.meshHold._TrianglesOfSubs = new global::trisPerSubmesh[this.paintedMesh.subMeshCount];
		for (int i = 0; i < this.paintedMesh.subMeshCount; i++)
		{
			this.meshHold._TrianglesOfSubs[i] = new global::trisPerSubmesh();
			this.meshHold._TrianglesOfSubs[i].triangles = this.paintedMesh.GetTriangles(i);
		}
		this.meshHold._bindPoses = this.paintedMesh.bindposes;
		this.meshHold._boneWeights = this.paintedMesh.boneWeights;
		this.meshHold._bounds = this.paintedMesh.bounds;
		this.meshHold._subMeshCount = this.paintedMesh.subMeshCount;
		this.meshHold._tangents = this.paintedMesh.tangents;
		this.meshHold._uv = this.paintedMesh.uv;
		this.meshHold._uv2 = this.paintedMesh.uv2;
		this.meshHold._uv3 = this.paintedMesh.uv3;
		this.meshHold._colors = this.paintedMesh.colors;
		this.meshHold._uv4 = this.paintedMesh.uv4;
		base.GetComponent<MeshFilter>().sharedMesh = this.paintedMesh;
		if (base.GetComponent<MeshCollider>())
		{
			base.GetComponent<MeshCollider>().sharedMesh = this.paintedMesh;
		}
	}

	// Token: 0x060026A3 RID: 9891 RVA: 0x000D6AF8 File Offset: 0x000D4CF8
	public void setWholeMesh(Mesh tmpMesh)
	{
		this.paintedMesh.vertices = tmpMesh.vertices;
		this.paintedMesh.triangles = tmpMesh.triangles;
		this.paintedMesh.normals = tmpMesh.normals;
		this.paintedMesh.colors = tmpMesh.colors;
		this.paintedMesh.uv = tmpMesh.uv;
		this.paintedMesh.uv2 = tmpMesh.uv2;
		this.paintedMesh.uv3 = tmpMesh.uv3;
		this.meshHold._vertices = tmpMesh.vertices;
		this.meshHold._triangles = tmpMesh.triangles;
		this.meshHold._normals = tmpMesh.normals;
		this.meshHold._colors = tmpMesh.colors;
		this.meshHold._uv = tmpMesh.uv;
		this.meshHold._uv2 = tmpMesh.uv2;
		this.meshHold._uv3 = tmpMesh.uv3;
	}

	// Token: 0x060026A4 RID: 9892 RVA: 0x000D6BF4 File Offset: 0x000D4DF4
	public Vector3[] setVertices(Vector3[] _deformedVertices)
	{
		this.paintedMesh.vertices = _deformedVertices;
		this.meshHold._vertices = _deformedVertices;
		this.paintedMesh.RecalculateNormals();
		this.paintedMesh.RecalculateBounds();
		this.meshHold._normals = this.paintedMesh.normals;
		this.meshHold._bounds = this.paintedMesh.bounds;
		base.GetComponent<MeshCollider>().sharedMesh = null;
		if (base.GetComponent<MeshCollider>())
		{
			base.GetComponent<MeshCollider>().sharedMesh = this.paintedMesh;
		}
		return this.meshHold._normals;
	}

	// Token: 0x060026A5 RID: 9893 RVA: 0x000D6C94 File Offset: 0x000D4E94
	public Vector3[] getVertices()
	{
		return this.paintedMesh.vertices;
	}

	// Token: 0x060026A6 RID: 9894 RVA: 0x000D6CA4 File Offset: 0x000D4EA4
	public Vector3[] getNormals()
	{
		return this.paintedMesh.normals;
	}

	// Token: 0x060026A7 RID: 9895 RVA: 0x000D6CB4 File Offset: 0x000D4EB4
	public int[] getTriangles()
	{
		return this.paintedMesh.triangles;
	}

	// Token: 0x060026A8 RID: 9896 RVA: 0x000D6CC4 File Offset: 0x000D4EC4
	public void setTangents(Vector4[] _meshTangents)
	{
		this.paintedMesh.tangents = _meshTangents;
		this.meshHold._tangents = _meshTangents;
	}

	// Token: 0x060026A9 RID: 9897 RVA: 0x000D6CE0 File Offset: 0x000D4EE0
	public Vector4[] getTangents()
	{
		return this.paintedMesh.tangents;
	}

	// Token: 0x060026AA RID: 9898 RVA: 0x000D6CF0 File Offset: 0x000D4EF0
	public void setColors(Color[] _vertexColors)
	{
		this.paintedMesh.colors = _vertexColors;
		this.meshHold._colors = _vertexColors;
	}

	// Token: 0x060026AB RID: 9899 RVA: 0x000D6D0C File Offset: 0x000D4F0C
	public Color[] getColors()
	{
		return this.paintedMesh.colors;
	}

	// Token: 0x060026AC RID: 9900 RVA: 0x000D6D1C File Offset: 0x000D4F1C
	public Vector2[] getUVs()
	{
		return this.paintedMesh.uv;
	}

	// Token: 0x060026AD RID: 9901 RVA: 0x000D6D2C File Offset: 0x000D4F2C
	public void setUV4s(Vector2[] _uv4s)
	{
		this.paintedMesh.uv4 = _uv4s;
		this.meshHold._uv4 = _uv4s;
	}

	// Token: 0x060026AE RID: 9902 RVA: 0x000D6D48 File Offset: 0x000D4F48
	public Vector2[] getUV4s()
	{
		return this.paintedMesh.uv4;
	}

	// Token: 0x060026AF RID: 9903 RVA: 0x000D6D58 File Offset: 0x000D4F58
	public void unlink()
	{
		this.init(this.paintedMesh, false);
	}

	// Token: 0x060026B0 RID: 9904 RVA: 0x000D6D68 File Offset: 0x000D4F68
	public void rebuild()
	{
		if (!base.GetComponent<MeshFilter>())
		{
			return;
		}
		this.paintedMesh = new Mesh();
		this.paintedMesh.hideFlags = 61;
		this.paintedMesh.name = "vpp_" + base.gameObject.name;
		if (this.meshHold == null || this.meshHold._vertices.Length == 0 || this.meshHold._TrianglesOfSubs.Length == 0)
		{
			this.paintedMesh.subMeshCount = this._subMeshCount;
			this.paintedMesh.vertices = this._vertices;
			this.paintedMesh.normals = this._normals;
			this.paintedMesh.triangles = this._triangles;
			this.meshHold._TrianglesOfSubs = new global::trisPerSubmesh[this.paintedMesh.subMeshCount];
			for (int i = 0; i < this.paintedMesh.subMeshCount; i++)
			{
				this.meshHold._TrianglesOfSubs[i] = new global::trisPerSubmesh();
				this.meshHold._TrianglesOfSubs[i].triangles = this.paintedMesh.GetTriangles(i);
			}
			this.paintedMesh.bindposes = this._bindPoses;
			this.paintedMesh.boneWeights = this._boneWeights;
			this.paintedMesh.bounds = this._bounds;
			this.paintedMesh.tangents = this._tangents;
			this.paintedMesh.uv = this._uv;
			this.paintedMesh.uv2 = this._uv2;
			this.paintedMesh.uv3 = this._uv3;
			this.paintedMesh.colors = this._colors;
			this.paintedMesh.uv4 = this._uv4;
			this.init(this.paintedMesh, true);
		}
		else
		{
			this.paintedMesh.subMeshCount = this.meshHold._subMeshCount;
			this.paintedMesh.vertices = this.meshHold._vertices;
			this.paintedMesh.normals = this.meshHold._normals;
			for (int j = 0; j < this.meshHold._subMeshCount; j++)
			{
				this.paintedMesh.SetTriangles(this.meshHold._TrianglesOfSubs[j].triangles, j);
			}
			this.paintedMesh.bindposes = this.meshHold._bindPoses;
			this.paintedMesh.boneWeights = this.meshHold._boneWeights;
			this.paintedMesh.bounds = this.meshHold._bounds;
			this.paintedMesh.tangents = this.meshHold._tangents;
			this.paintedMesh.uv = this.meshHold._uv;
			this.paintedMesh.uv2 = this.meshHold._uv2;
			this.paintedMesh.uv3 = this.meshHold._uv3;
			this.paintedMesh.colors = this.meshHold._colors;
			this.paintedMesh.uv4 = this.meshHold._uv4;
			this.init(this.paintedMesh, true);
		}
	}

	// Token: 0x060026B1 RID: 9905 RVA: 0x000D7094 File Offset: 0x000D5294
	private void Start()
	{
		if (!this.paintedMesh || this.meshHold == null)
		{
			this.rebuild();
		}
	}

	// Token: 0x04002314 RID: 8980
	[HideInInspector]
	public Mesh originalMesh;

	// Token: 0x04002315 RID: 8981
	[HideInInspector]
	public Mesh paintedMesh;

	// Token: 0x04002316 RID: 8982
	[HideInInspector]
	public global::MeshHolder meshHold;

	// Token: 0x04002317 RID: 8983
	[HideInInspector]
	public Vector3[] _vertices;

	// Token: 0x04002318 RID: 8984
	[HideInInspector]
	public Vector3[] _normals;

	// Token: 0x04002319 RID: 8985
	[HideInInspector]
	public int[] _triangles;

	// Token: 0x0400231A RID: 8986
	[HideInInspector]
	public int[][] _Subtriangles;

	// Token: 0x0400231B RID: 8987
	[HideInInspector]
	public Matrix4x4[] _bindPoses;

	// Token: 0x0400231C RID: 8988
	[HideInInspector]
	public BoneWeight[] _boneWeights;

	// Token: 0x0400231D RID: 8989
	[HideInInspector]
	public Bounds _bounds;

	// Token: 0x0400231E RID: 8990
	[HideInInspector]
	public int _subMeshCount;

	// Token: 0x0400231F RID: 8991
	[HideInInspector]
	public Vector4[] _tangents;

	// Token: 0x04002320 RID: 8992
	[HideInInspector]
	public Vector2[] _uv;

	// Token: 0x04002321 RID: 8993
	[HideInInspector]
	public Vector2[] _uv2;

	// Token: 0x04002322 RID: 8994
	[HideInInspector]
	public Vector2[] _uv3;

	// Token: 0x04002323 RID: 8995
	[HideInInspector]
	public Color[] _colors;

	// Token: 0x04002324 RID: 8996
	[HideInInspector]
	public Vector2[] _uv4;
}
