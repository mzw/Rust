﻿using System;
using UnityEngine;

// Token: 0x0200077E RID: 1918
public static class AssetStorage
{
	// Token: 0x060023A6 RID: 9126 RVA: 0x000C55EC File Offset: 0x000C37EC
	public static void Save<T>(ref T asset, string path) where T : Object
	{
		if (!asset)
		{
			return;
		}
	}

	// Token: 0x060023A7 RID: 9127 RVA: 0x000C5604 File Offset: 0x000C3804
	public static void Save(ref Texture2D asset)
	{
	}

	// Token: 0x060023A8 RID: 9128 RVA: 0x000C5608 File Offset: 0x000C3808
	public static void Save(ref Texture2D asset, string path, bool linear, bool compress)
	{
		if (!asset)
		{
			return;
		}
	}

	// Token: 0x060023A9 RID: 9129 RVA: 0x000C5618 File Offset: 0x000C3818
	public static void Load<T>(ref T asset, string path) where T : Object
	{
	}

	// Token: 0x060023AA RID: 9130 RVA: 0x000C561C File Offset: 0x000C381C
	public static void Delete<T>(ref T asset) where T : Object
	{
		if (!asset)
		{
			return;
		}
		Object.Destroy(asset);
		asset = (T)((object)null);
	}
}
