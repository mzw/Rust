﻿using System;
using UnityEngine;

// Token: 0x020006A1 RID: 1697
public class BlueprintCraftGridRow : MonoBehaviour
{
	// Token: 0x04001CBB RID: 7355
	public GameObject amount;

	// Token: 0x04001CBC RID: 7356
	public GameObject itemName;

	// Token: 0x04001CBD RID: 7357
	public GameObject total;

	// Token: 0x04001CBE RID: 7358
	public GameObject have;

	// Token: 0x04001CBF RID: 7359
	public Color colorOK;

	// Token: 0x04001CC0 RID: 7360
	public Color colorBad;
}
