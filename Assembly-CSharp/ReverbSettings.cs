﻿using System;
using UnityEngine;

// Token: 0x020001EF RID: 495
[CreateAssetMenu(menuName = "Rust/Reverb Settings")]
public class ReverbSettings : ScriptableObject
{
	// Token: 0x040009AB RID: 2475
	[Range(-10000f, 0f)]
	public int room;

	// Token: 0x040009AC RID: 2476
	[Range(-10000f, 0f)]
	public int roomHF;

	// Token: 0x040009AD RID: 2477
	[Range(-10000f, 0f)]
	public int roomLF;

	// Token: 0x040009AE RID: 2478
	[Range(0.1f, 20f)]
	public float decayTime;

	// Token: 0x040009AF RID: 2479
	[Range(0.1f, 2f)]
	public float decayHFRatio;

	// Token: 0x040009B0 RID: 2480
	[Range(-10000f, 1000f)]
	public int reflections;

	// Token: 0x040009B1 RID: 2481
	[Range(0f, 0.3f)]
	public float reflectionsDelay;

	// Token: 0x040009B2 RID: 2482
	[Range(-10000f, 2000f)]
	public int reverb;

	// Token: 0x040009B3 RID: 2483
	[Range(0f, 0.1f)]
	public float reverbDelay;

	// Token: 0x040009B4 RID: 2484
	[Range(1000f, 20000f)]
	public float HFReference;

	// Token: 0x040009B5 RID: 2485
	[Range(20f, 1000f)]
	public float LFReference;

	// Token: 0x040009B6 RID: 2486
	[Range(0f, 100f)]
	public float diffusion;

	// Token: 0x040009B7 RID: 2487
	[Range(0f, 100f)]
	public float density;
}
