﻿using System;
using UnityEngine;

// Token: 0x020003DC RID: 988
public class BaseTrap : global::DecayEntity
{
	// Token: 0x06001713 RID: 5907 RVA: 0x00084C80 File Offset: 0x00082E80
	public virtual void ObjectEntered(GameObject obj)
	{
	}

	// Token: 0x06001714 RID: 5908 RVA: 0x00084C84 File Offset: 0x00082E84
	public virtual void Arm()
	{
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06001715 RID: 5909 RVA: 0x00084C98 File Offset: 0x00082E98
	public virtual void OnEmpty()
	{
	}
}
