﻿using System;
using UnityEngine;

// Token: 0x0200043B RID: 1083
public interface IPrefabPreProcess
{
	// Token: 0x0600186E RID: 6254
	void PreProcess(global::IPrefabProcessor preProcess, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling);
}
