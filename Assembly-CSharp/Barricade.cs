﻿using System;
using Rust;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x02000377 RID: 887
public class Barricade : global::DecayEntity
{
	// Token: 0x06001513 RID: 5395 RVA: 0x00079E50 File Offset: 0x00078050
	public override void ServerInit()
	{
		base.ServerInit();
		if (global::Barricade.nonWalkableArea < 0)
		{
			global::Barricade.nonWalkableArea = NavMesh.GetAreaFromName("Not Walkable");
		}
		if (global::Barricade.animalAgentTypeId < 0)
		{
			global::Barricade.animalAgentTypeId = NavMesh.GetSettingsByIndex(1).agentTypeID;
		}
		if (this.NavMeshVolumeAnimals == null)
		{
			this.NavMeshVolumeAnimals = base.gameObject.AddComponent<NavMeshModifierVolume>();
			this.NavMeshVolumeAnimals.area = global::Barricade.nonWalkableArea;
			this.NavMeshVolumeAnimals.AddAgentType(global::Barricade.animalAgentTypeId);
			this.NavMeshVolumeAnimals.center = Vector3.zero;
			this.NavMeshVolumeAnimals.size = Vector3.one;
		}
		if (!this.canNpcSmash)
		{
			if (global::Barricade.humanoidAgentTypeId < 0)
			{
				global::Barricade.humanoidAgentTypeId = NavMesh.GetSettingsByIndex(0).agentTypeID;
			}
			if (this.NavMeshVolumeHumanoids == null)
			{
				this.NavMeshVolumeHumanoids = base.gameObject.AddComponent<NavMeshModifierVolume>();
				this.NavMeshVolumeHumanoids.area = global::Barricade.nonWalkableArea;
				this.NavMeshVolumeHumanoids.AddAgentType(global::Barricade.humanoidAgentTypeId);
				this.NavMeshVolumeHumanoids.center = Vector3.zero;
				this.NavMeshVolumeHumanoids.size = Vector3.one;
			}
		}
		else if (this.NpcTriggerBox == null)
		{
			this.NpcTriggerBox = new GameObject("NpcTriggerBox").AddComponent<global::NPCBarricadeTriggerBox>();
			this.NpcTriggerBox.Setup(this);
		}
	}

	// Token: 0x06001514 RID: 5396 RVA: 0x00079FC0 File Offset: 0x000781C0
	public override void OnAttacked(global::HitInfo info)
	{
		if (base.isServer && info.WeaponPrefab is global::BaseMelee && !info.IsProjectile())
		{
			global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
			if (basePlayer && this.reflectDamage > 0f)
			{
				basePlayer.Hurt(this.reflectDamage * Random.Range(0.75f, 1.25f), Rust.DamageType.Stab, this, true);
				if (this.reflectEffect.isValid)
				{
					global::Effect.server.Run(this.reflectEffect.resourcePath, basePlayer, global::StringPool.closest, base.transform.position, Vector3.up, null, false);
				}
			}
		}
		base.OnAttacked(info);
	}

	// Token: 0x04000F8C RID: 3980
	public float reflectDamage = 5f;

	// Token: 0x04000F8D RID: 3981
	public global::GameObjectRef reflectEffect;

	// Token: 0x04000F8E RID: 3982
	public bool canNpcSmash = true;

	// Token: 0x04000F8F RID: 3983
	public NavMeshModifierVolume NavMeshVolumeAnimals;

	// Token: 0x04000F90 RID: 3984
	public NavMeshModifierVolume NavMeshVolumeHumanoids;

	// Token: 0x04000F91 RID: 3985
	public global::NPCBarricadeTriggerBox NpcTriggerBox;

	// Token: 0x04000F92 RID: 3986
	private static int nonWalkableArea = -1;

	// Token: 0x04000F93 RID: 3987
	private static int animalAgentTypeId = -1;

	// Token: 0x04000F94 RID: 3988
	private static int humanoidAgentTypeId = -1;
}
