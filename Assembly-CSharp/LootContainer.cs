﻿using System;
using Oxide.Core;
using Rust;
using UnityEngine;

// Token: 0x0200039C RID: 924
public class LootContainer : global::StorageContainer
{
	// Token: 0x17000188 RID: 392
	// (get) Token: 0x060015C9 RID: 5577 RVA: 0x0007D634 File Offset: 0x0007B834
	public bool shouldRefreshContents
	{
		get
		{
			return this.minSecondsBetweenRefresh > 0f && this.maxSecondsBetweenRefresh > 0f;
		}
	}

	// Token: 0x060015CA RID: 5578 RVA: 0x0007D658 File Offset: 0x0007B858
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.initialLootSpawn)
		{
			this.SpawnLoot();
		}
		if (this.BlockPlayerItemInput && !Application.isLoadingSave && this.inventory != null)
		{
			this.inventory.SetFlag(global::ItemContainer.Flag.NoItemInput, true);
		}
	}

	// Token: 0x060015CB RID: 5579 RVA: 0x0007D6B0 File Offset: 0x0007B8B0
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		if (this.BlockPlayerItemInput && this.inventory != null)
		{
			this.inventory.SetFlag(global::ItemContainer.Flag.NoItemInput, true);
		}
	}

	// Token: 0x060015CC RID: 5580 RVA: 0x0007D6E0 File Offset: 0x0007B8E0
	public virtual void SpawnLoot()
	{
		if (this.inventory == null)
		{
			Debug.Log("CONTACT DEVELOPERS! LootContainer::PopulateLoot has null inventory!!!");
			return;
		}
		this.inventory.Clear();
		global::ItemManager.DoRemoves();
		if (Interface.CallHook("OnLootSpawn", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		this.PopulateLoot();
		if (this.shouldRefreshContents)
		{
			base.Invoke(new Action(this.SpawnLoot), Random.Range(this.minSecondsBetweenRefresh, this.maxSecondsBetweenRefresh));
		}
	}

	// Token: 0x060015CD RID: 5581 RVA: 0x0007D764 File Offset: 0x0007B964
	public int ScoreForRarity(Rarity rarity)
	{
		switch (rarity)
		{
		case 1:
			return 1;
		case 2:
			return 2;
		case 3:
			return 3;
		case 4:
			return 4;
		default:
			return 5000;
		}
	}

	// Token: 0x060015CE RID: 5582 RVA: 0x0007D790 File Offset: 0x0007B990
	public virtual void PopulateLoot()
	{
		if (this.LootSpawnSlots.Length > 0)
		{
			foreach (global::LootContainer.LootSpawnSlot lootSpawnSlot in this.LootSpawnSlots)
			{
				for (int j = 0; j < lootSpawnSlot.numberToSpawn; j++)
				{
					float num = Random.Range(0f, 1f);
					if (num <= lootSpawnSlot.probability)
					{
						lootSpawnSlot.definition.SpawnIntoContainer(this.inventory);
					}
				}
			}
		}
		else if (this.lootDefinition != null)
		{
			for (int k = 0; k < this.maxDefinitionsToSpawn; k++)
			{
				this.lootDefinition.SpawnIntoContainer(this.inventory);
			}
		}
		if (this.SpawnType == global::LootContainer.spawnType.ROADSIDE || this.SpawnType == global::LootContainer.spawnType.TOWN)
		{
			foreach (global::Item item in this.inventory.itemList)
			{
				if (item.hasCondition)
				{
					item.condition = Random.Range(item.info.condition.foundCondition.fractionMin, item.info.condition.foundCondition.fractionMax) * item.info.condition.max;
				}
			}
		}
		this.GenerateScrap();
	}

	// Token: 0x060015CF RID: 5583 RVA: 0x0007D924 File Offset: 0x0007BB24
	public void GenerateScrap()
	{
		if (this.scrapAmount <= 0)
		{
			return;
		}
		if (global::LootContainer.scrapDef == null)
		{
			global::LootContainer.scrapDef = global::ItemManager.FindItemDefinition("scrap");
		}
		int num = this.scrapAmount;
		if (num > 0)
		{
			global::Item item = global::ItemManager.Create(global::LootContainer.scrapDef, num, 0UL);
			if (!item.MoveToContainer(this.inventory, -1, true))
			{
				item.Drop(base.transform.position, Vector3.zero, default(Quaternion));
			}
		}
	}

	// Token: 0x060015D0 RID: 5584 RVA: 0x0007D9AC File Offset: 0x0007BBAC
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		base.PlayerStoppedLooting(player);
		if (this.destroyOnEmpty && (this.inventory == null || this.inventory.itemList == null || this.inventory.itemList.Count == 0))
		{
			base.Kill(global::BaseNetworkable.DestroyMode.Gib);
		}
	}

	// Token: 0x060015D1 RID: 5585 RVA: 0x0007DA04 File Offset: 0x0007BC04
	public void RemoveMe()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.Gib);
	}

	// Token: 0x060015D2 RID: 5586 RVA: 0x0007DA10 File Offset: 0x0007BC10
	public override bool ShouldDropItemsIndividually()
	{
		return true;
	}

	// Token: 0x060015D3 RID: 5587 RVA: 0x0007DA14 File Offset: 0x0007BC14
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x060015D4 RID: 5588 RVA: 0x0007DA18 File Offset: 0x0007BC18
	public override void OnAttacked(global::HitInfo info)
	{
		base.OnAttacked(info);
	}

	// Token: 0x060015D5 RID: 5589 RVA: 0x0007DA24 File Offset: 0x0007BC24
	public override void InitShared()
	{
		base.InitShared();
	}

	// Token: 0x04001012 RID: 4114
	public bool destroyOnEmpty = true;

	// Token: 0x04001013 RID: 4115
	public global::LootSpawn lootDefinition;

	// Token: 0x04001014 RID: 4116
	public int maxDefinitionsToSpawn;

	// Token: 0x04001015 RID: 4117
	public float minSecondsBetweenRefresh = 3600f;

	// Token: 0x04001016 RID: 4118
	public float maxSecondsBetweenRefresh = 7200f;

	// Token: 0x04001017 RID: 4119
	public bool initialLootSpawn = true;

	// Token: 0x04001018 RID: 4120
	public float xpLootedScale = 1f;

	// Token: 0x04001019 RID: 4121
	public float xpDestroyedScale = 1f;

	// Token: 0x0400101A RID: 4122
	public bool BlockPlayerItemInput;

	// Token: 0x0400101B RID: 4123
	public int scrapAmount;

	// Token: 0x0400101C RID: 4124
	public string deathStat = string.Empty;

	// Token: 0x0400101D RID: 4125
	public global::LootContainer.spawnType SpawnType;

	// Token: 0x0400101E RID: 4126
	private static global::ItemDefinition scrapDef;

	// Token: 0x0400101F RID: 4127
	public global::LootContainer.LootSpawnSlot[] LootSpawnSlots;

	// Token: 0x0200039D RID: 925
	public enum spawnType
	{
		// Token: 0x04001021 RID: 4129
		GENERIC,
		// Token: 0x04001022 RID: 4130
		PLAYER,
		// Token: 0x04001023 RID: 4131
		TOWN,
		// Token: 0x04001024 RID: 4132
		AIRDROP,
		// Token: 0x04001025 RID: 4133
		CRASHSITE,
		// Token: 0x04001026 RID: 4134
		ROADSIDE
	}

	// Token: 0x0200039E RID: 926
	[Serializable]
	public struct LootSpawnSlot
	{
		// Token: 0x04001027 RID: 4135
		public global::LootSpawn definition;

		// Token: 0x04001028 RID: 4136
		public int numberToSpawn;

		// Token: 0x04001029 RID: 4137
		public float probability;
	}
}
