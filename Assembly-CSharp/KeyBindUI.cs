﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200070C RID: 1804
public class KeyBindUI : MonoBehaviour
{
	// Token: 0x04001EC5 RID: 7877
	public GameObject blockingCanvas;

	// Token: 0x04001EC6 RID: 7878
	public Button btnA;

	// Token: 0x04001EC7 RID: 7879
	public Button btnB;

	// Token: 0x04001EC8 RID: 7880
	public string bindString;
}
