﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Facepunch.Sqlite;
using Ionic.Crc;
using UnityEngine.Assertions;

// Token: 0x02000632 RID: 1586
public class FileStorage : IDisposable
{
	// Token: 0x06002034 RID: 8244 RVA: 0x000B85AC File Offset: 0x000B67AC
	protected FileStorage(string name, bool server)
	{
		if (server)
		{
			string rootFolder = ConVar.Server.rootFolder;
			string text = rootFolder + "/" + name + ".db";
			this.db = new Database();
			this.db.Open(text);
			if (!this.db.TableExists("data"))
			{
				this.db.Execute("CREATE TABLE data ( crc INTEGER PRIMARY KEY, data BLOB, updated INTEGER, entid INTEGER, filetype INTEGER, part INTEGER )", new object[0]);
			}
		}
	}

	// Token: 0x06002035 RID: 8245 RVA: 0x000B8638 File Offset: 0x000B6838
	~FileStorage()
	{
		this.Dispose();
	}

	// Token: 0x06002036 RID: 8246 RVA: 0x000B8668 File Offset: 0x000B6868
	public void Dispose()
	{
		if (this.db != null)
		{
			this.db.Close();
			this.db = null;
		}
	}

	// Token: 0x06002037 RID: 8247 RVA: 0x000B8688 File Offset: 0x000B6888
	private uint GetCRC(byte[] data, global::FileStorage.Type type)
	{
		this.crc.Reset();
		this.crc.SlurpBlock(data, 0, data.Length);
		this.crc.UpdateCRC((byte)type);
		return (uint)this.crc.Crc32Result;
	}

	// Token: 0x06002038 RID: 8248 RVA: 0x000B86C0 File Offset: 0x000B68C0
	public uint Store(byte[] data, global::FileStorage.Type type, uint entityID, uint numID = 0u)
	{
		uint result;
		using (TimeWarning.New("FileStorage.Store", 0.1f))
		{
			if (data.Length > 4194304)
			{
				Array.Resize<byte>(ref data, 4194304);
			}
			uint num = this.GetCRC(data, type);
			if (this.db != null)
			{
				this.db.Execute("INSERT OR REPLACE INTO data ( crc, data, entid, filetype, part ) VALUES ( ?, ?, ?, ?, ? )", new object[]
				{
					(int)num,
					data,
					(int)entityID,
					(int)type,
					(int)numID
				});
			}
			this._cache.Remove(num);
			this._cache.Add(num, new global::FileStorage.CacheData
			{
				data = data,
				entityID = entityID,
				numID = numID
			});
			result = num;
		}
		return result;
	}

	// Token: 0x06002039 RID: 8249 RVA: 0x000B87A4 File Offset: 0x000B69A4
	public byte[] Get(uint crc, global::FileStorage.Type type, uint entityID)
	{
		byte[] result;
		using (TimeWarning.New("FileStorage.Get", 0.1f))
		{
			global::FileStorage.CacheData cacheData;
			if (this._cache.TryGetValue(crc, out cacheData))
			{
				Assert.IsTrue(cacheData.data != null, "FileStorage cache contains a null texture");
				result = cacheData.data;
			}
			else if (this.db == null)
			{
				result = null;
			}
			else
			{
				byte[] array = this.db.QueryBlob("SELECT data FROM data WHERE crc = ? AND filetype = ? AND entid = ? LIMIT 1", new object[]
				{
					(int)crc,
					(int)type,
					(int)entityID
				});
				if (array == null)
				{
					result = null;
				}
				else
				{
					if (array.Length > 4194304)
					{
						Array.Resize<byte>(ref array, 4194304);
					}
					this._cache.Remove(crc);
					this._cache.Add(crc, new global::FileStorage.CacheData
					{
						data = array,
						entityID = entityID,
						numID = 0u
					});
					result = array;
				}
			}
		}
		return result;
	}

	// Token: 0x0600203A RID: 8250 RVA: 0x000B88C0 File Offset: 0x000B6AC0
	public void Remove(uint crc, global::FileStorage.Type type, uint entityID)
	{
		if (this.db != null)
		{
			this.db.Execute("DELETE FROM data WHERE crc = ? AND filetype = ? AND entid = ?", new object[]
			{
				(int)crc,
				(int)type,
				(int)entityID
			});
		}
		if (this._cache.ContainsKey(crc))
		{
			this._cache.Remove(crc);
		}
	}

	// Token: 0x0600203B RID: 8251 RVA: 0x000B8928 File Offset: 0x000B6B28
	public void RemoveEntityNum(uint entityid, uint numid)
	{
		if (this.db != null)
		{
			this.db.Execute("DELETE FROM data WHERE entid = ? AND part = ?", new object[]
			{
				(int)entityid,
				(int)numid
			});
		}
		foreach (uint key in (from x in this._cache
		where x.Value.entityID == entityid && x.Value.numID == numid
		select x.Key).ToArray<uint>())
		{
			this._cache.Remove(key);
		}
	}

	// Token: 0x0600203C RID: 8252 RVA: 0x000B89EC File Offset: 0x000B6BEC
	internal void RemoveAllByEntity(uint entityid)
	{
		if (this.db != null)
		{
			this.db.Execute("DELETE FROM data WHERE entid = ?", new object[]
			{
				(int)entityid
			});
		}
	}

	// Token: 0x04001B01 RID: 6913
	private Database db;

	// Token: 0x04001B02 RID: 6914
	public const int MaxSize = 4194304;

	// Token: 0x04001B03 RID: 6915
	private CRC32 crc = new CRC32();

	// Token: 0x04001B04 RID: 6916
	private Dictionary<uint, global::FileStorage.CacheData> _cache = new Dictionary<uint, global::FileStorage.CacheData>();

	// Token: 0x04001B05 RID: 6917
	public static global::FileStorage server = new global::FileStorage("sv.files." + 0, true);

	// Token: 0x02000633 RID: 1587
	private class CacheData
	{
		// Token: 0x04001B07 RID: 6919
		public byte[] data;

		// Token: 0x04001B08 RID: 6920
		public uint entityID;

		// Token: 0x04001B09 RID: 6921
		public uint numID;
	}

	// Token: 0x02000634 RID: 1588
	public enum Type
	{
		// Token: 0x04001B0B RID: 6923
		png,
		// Token: 0x04001B0C RID: 6924
		jpg
	}
}
