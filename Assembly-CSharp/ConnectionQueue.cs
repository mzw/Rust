﻿using System;
using System.Collections.Generic;
using Network;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000620 RID: 1568
public class ConnectionQueue
{
	// Token: 0x17000235 RID: 565
	// (get) Token: 0x06001FAD RID: 8109 RVA: 0x000B3D38 File Offset: 0x000B1F38
	public int Queued
	{
		get
		{
			return this.queue.Count;
		}
	}

	// Token: 0x17000236 RID: 566
	// (get) Token: 0x06001FAE RID: 8110 RVA: 0x000B3D48 File Offset: 0x000B1F48
	public int Joining
	{
		get
		{
			return this.joining.Count;
		}
	}

	// Token: 0x06001FAF RID: 8111 RVA: 0x000B3D58 File Offset: 0x000B1F58
	public void SkipQueue(ulong userid)
	{
		for (int i = 0; i < this.queue.Count; i++)
		{
			Connection connection = this.queue[i];
			if (connection.userid == userid)
			{
				this.JoinGame(connection);
				break;
			}
		}
	}

	// Token: 0x06001FB0 RID: 8112 RVA: 0x000B3DA8 File Offset: 0x000B1FA8
	internal void Join(Connection connection)
	{
		connection.state = 2;
		this.queue.Add(connection);
		this.nextMessageTime = 0f;
		if (this.CanJumpQueue(connection))
		{
			this.JoinGame(connection);
		}
	}

	// Token: 0x06001FB1 RID: 8113 RVA: 0x000B3DDC File Offset: 0x000B1FDC
	public void Cycle(int availableSlots)
	{
		if (this.queue.Count == 0)
		{
			return;
		}
		if (availableSlots - this.Joining > 0)
		{
			this.JoinGame(this.queue[0]);
		}
		this.SendMessages();
	}

	// Token: 0x06001FB2 RID: 8114 RVA: 0x000B3E18 File Offset: 0x000B2018
	private void SendMessages()
	{
		if (this.nextMessageTime > Time.realtimeSinceStartup)
		{
			return;
		}
		this.nextMessageTime = Time.realtimeSinceStartup + 10f;
		for (int i = 0; i < this.queue.Count; i++)
		{
			this.SendMessage(this.queue[i], i);
		}
	}

	// Token: 0x06001FB3 RID: 8115 RVA: 0x000B3E78 File Offset: 0x000B2078
	private void SendMessage(Connection c, int position)
	{
		string text = string.Empty;
		if (position > 0)
		{
			text = string.Format("{0:N0} PLAYERS AHEAD OF YOU, {1:N0} PLAYERS BEHIND", position, this.queue.Count - position - 1);
		}
		else
		{
			text = string.Format("YOU'RE NEXT - {1:N0} PLAYERS BEHIND YOU", position, this.queue.Count - position - 1);
		}
		if (Net.sv.write.Start())
		{
			Net.sv.write.PacketID(16);
			Net.sv.write.String("QUEUE");
			Net.sv.write.String(text);
			Net.sv.write.Send(new SendInfo(c));
		}
	}

	// Token: 0x06001FB4 RID: 8116 RVA: 0x000B3F40 File Offset: 0x000B2140
	public void RemoveConnection(Connection connection)
	{
		if (this.queue.Remove(connection))
		{
			this.nextMessageTime = 0f;
		}
		this.joining.Remove(connection);
	}

	// Token: 0x06001FB5 RID: 8117 RVA: 0x000B3F6C File Offset: 0x000B216C
	private void JoinGame(Connection connection)
	{
		this.queue.Remove(connection);
		connection.state = 3;
		this.nextMessageTime = 0f;
		this.joining.Add(connection);
		SingletonComponent<global::ServerMgr>.Instance.JoinGame(connection);
	}

	// Token: 0x06001FB6 RID: 8118 RVA: 0x000B3FA4 File Offset: 0x000B21A4
	public void JoinedGame(Connection connection)
	{
		this.RemoveConnection(connection);
	}

	// Token: 0x06001FB7 RID: 8119 RVA: 0x000B3FB0 File Offset: 0x000B21B0
	private bool CanJumpQueue(Connection connection)
	{
		object obj = Interface.CallHook("CanBypassQueue", new object[]
		{
			connection
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		if (global::DeveloperList.Contains(connection.userid))
		{
			return true;
		}
		global::ServerUsers.User user = global::ServerUsers.Get(connection.userid);
		return (user != null && user.group == global::ServerUsers.UserGroup.Moderator) || (user != null && user.group == global::ServerUsers.UserGroup.Owner);
	}

	// Token: 0x04001AA2 RID: 6818
	private List<Connection> queue = new List<Connection>();

	// Token: 0x04001AA3 RID: 6819
	private List<Connection> joining = new List<Connection>();

	// Token: 0x04001AA4 RID: 6820
	private float nextMessageTime;
}
