﻿using System;
using UnityEngine;

// Token: 0x020007F9 RID: 2041
[ExecuteInEditMode]
public class ExplosionsParticleSystemScaler : MonoBehaviour
{
	// Token: 0x060025BB RID: 9659 RVA: 0x000D0980 File Offset: 0x000CEB80
	private void Start()
	{
	}

	// Token: 0x060025BC RID: 9660 RVA: 0x000D0984 File Offset: 0x000CEB84
	private void Update()
	{
	}

	// Token: 0x040021DB RID: 8667
	public float particlesScale = 1f;
}
