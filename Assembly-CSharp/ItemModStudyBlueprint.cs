﻿using System;
using UnityEngine;

// Token: 0x020004F4 RID: 1268
public class ItemModStudyBlueprint : global::ItemMod
{
	// Token: 0x06001B04 RID: 6916 RVA: 0x0009731C File Offset: 0x0009551C
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (item.GetOwnerPlayer() != player)
		{
			bool flag = false;
			foreach (global::ItemContainer itemContainer in player.inventory.loot.containers)
			{
				if (item.GetRootContainer() == itemContainer)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				return;
			}
		}
		if (command == "study")
		{
			if (!item.IsBlueprint())
			{
				return;
			}
			global::ItemDefinition blueprintTargetDef = item.blueprintTargetDef;
			if (player.blueprints.IsUnlocked(blueprintTargetDef))
			{
				return;
			}
			global::Item item2 = item;
			if (item.amount > 1)
			{
				item2 = item.SplitItem(1);
			}
			item2.UseItem(1);
			player.blueprints.Unlock(blueprintTargetDef);
			if (this.studyEffect.isValid)
			{
				global::Effect.server.Run(this.studyEffect.resourcePath, player, global::StringPool.Get("head"), Vector3.zero, Vector3.zero, null, false);
			}
		}
	}

	// Token: 0x040015D4 RID: 5588
	public global::GameObjectRef studyEffect;
}
