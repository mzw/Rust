﻿using System;
using UnityEngine;

// Token: 0x02000738 RID: 1848
public class ForceChildSingletonSetup : MonoBehaviour
{
	// Token: 0x060022C3 RID: 8899 RVA: 0x000C1830 File Offset: 0x000BFA30
	[ComponentHelp("Any child objects of this object that contain SingletonComponents will be registered - even if they're not enabled")]
	private void Awake()
	{
		foreach (SingletonComponent singletonComponent in base.GetComponentsInChildren<SingletonComponent>(true))
		{
			singletonComponent.Setup();
		}
	}
}
