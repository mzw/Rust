﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000075 RID: 117
public class Landmine : global::BaseTrap
{
	// Token: 0x06000825 RID: 2085 RVA: 0x0003500C File Offset: 0x0003320C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Landmine.OnRpcMessage", 0.1f))
		{
			if (rpc == 617302696u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Disarm ");
				}
				using (TimeWarning.New("RPC_Disarm", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Disarm(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Disarm");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000826 RID: 2086 RVA: 0x00035160 File Offset: 0x00033360
	public bool Triggered()
	{
		return base.HasFlag(global::BaseEntity.Flags.Open);
	}

	// Token: 0x06000827 RID: 2087 RVA: 0x0003516C File Offset: 0x0003336C
	public bool Armed()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000828 RID: 2088 RVA: 0x00035178 File Offset: 0x00033378
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (!info.forDisk)
		{
			info.msg.landmine = Facepunch.Pool.Get<ProtoBuf.Landmine>();
			info.msg.landmine.triggeredID = this.triggerPlayerID;
		}
	}

	// Token: 0x06000829 RID: 2089 RVA: 0x000351B8 File Offset: 0x000333B8
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (!info.fromDisk && info.msg.landmine != null)
		{
			this.triggerPlayerID = info.msg.landmine.triggeredID;
		}
	}

	// Token: 0x0600082A RID: 2090 RVA: 0x000351F8 File Offset: 0x000333F8
	public override void ServerInit()
	{
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		base.Invoke(new Action(this.Arm), 1.5f);
		base.ServerInit();
	}

	// Token: 0x0600082B RID: 2091 RVA: 0x00035224 File Offset: 0x00033424
	public override void ObjectEntered(GameObject obj)
	{
		if (base.isClient)
		{
			return;
		}
		if (!this.Armed())
		{
			base.CancelInvoke(new Action(this.Arm));
			this.blocked = true;
			return;
		}
		if (Interface.CallHook("OnTrapTrigger", new object[]
		{
			this,
			obj
		}) != null)
		{
			return;
		}
		global::BasePlayer ply = obj.ToBaseEntity() as global::BasePlayer;
		this.Trigger(ply);
	}

	// Token: 0x0600082C RID: 2092 RVA: 0x00035294 File Offset: 0x00033494
	public void Trigger(global::BasePlayer ply = null)
	{
		if (ply)
		{
			this.triggerPlayerID = ply.userID;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600082D RID: 2093 RVA: 0x000352C0 File Offset: 0x000334C0
	public override void OnEmpty()
	{
		if (this.blocked)
		{
			this.Arm();
			this.blocked = false;
			return;
		}
		if (this.Triggered())
		{
			base.Invoke(new Action(this.TryExplode), 0.05f);
		}
	}

	// Token: 0x0600082E RID: 2094 RVA: 0x00035300 File Offset: 0x00033500
	public virtual void Explode()
	{
		base.health = float.PositiveInfinity;
		global::Effect.server.Run(this.explosionEffect.resourcePath, base.PivotPoint(), base.transform.up, null, true);
		global::DamageUtil.RadiusDamage(this, base.LookupPrefab(), base.CenterPoint(), this.minExplosionRadius, this.explosionRadius, this.damageTypes, 2230528, true);
		if (base.IsDestroyed)
		{
			return;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x0600082F RID: 2095 RVA: 0x00035378 File Offset: 0x00033578
	public override void OnKilled(global::HitInfo info)
	{
		base.Invoke(new Action(this.Explode), Random.Range(0.1f, 0.3f));
	}

	// Token: 0x06000830 RID: 2096 RVA: 0x0003539C File Offset: 0x0003359C
	private void OnGroundMissing()
	{
		this.Explode();
	}

	// Token: 0x06000831 RID: 2097 RVA: 0x000353A4 File Offset: 0x000335A4
	private void TryExplode()
	{
		if (this.Armed())
		{
			this.Explode();
		}
	}

	// Token: 0x06000832 RID: 2098 RVA: 0x000353B8 File Offset: 0x000335B8
	public override void Arm()
	{
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000833 RID: 2099 RVA: 0x000353CC File Offset: 0x000335CC
	[global::BaseEntity.RPC_Server]
	private void RPC_Disarm(global::BaseEntity.RPCMessage rpc)
	{
		if ((ulong)rpc.player.net.ID == this.triggerPlayerID)
		{
			return;
		}
		if (!this.Armed())
		{
			return;
		}
		if (Interface.CallHook("OnTrapDisarm", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		int num = Random.Range(0, 100);
		if (num < 15)
		{
			base.Invoke(new Action(this.TryExplode), 0.05f);
			return;
		}
		rpc.player.GiveItem(global::ItemManager.CreateByName("trap.landmine", 1, 0UL), global::BaseEntity.GiveItemReason.PickedUp);
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x040003D7 RID: 983
	public global::GameObjectRef explosionEffect;

	// Token: 0x040003D8 RID: 984
	public global::GameObjectRef triggeredEffect;

	// Token: 0x040003D9 RID: 985
	public float minExplosionRadius;

	// Token: 0x040003DA RID: 986
	public float explosionRadius;

	// Token: 0x040003DB RID: 987
	public bool blocked;

	// Token: 0x040003DC RID: 988
	private ulong triggerPlayerID;

	// Token: 0x040003DD RID: 989
	public List<Rust.DamageTypeEntry> damageTypes = new List<Rust.DamageTypeEntry>();
}
