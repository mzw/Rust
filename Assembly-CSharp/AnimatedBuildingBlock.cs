﻿using System;
using Rust;
using UnityEngine;

// Token: 0x02000363 RID: 867
public class AnimatedBuildingBlock : global::StabilityEntity
{
	// Token: 0x060014D5 RID: 5333 RVA: 0x00078D80 File Offset: 0x00076F80
	public override void ServerInit()
	{
		base.ServerInit();
		if (!Application.isLoadingSave)
		{
			this.UpdateAnimationParameters(true);
		}
	}

	// Token: 0x060014D6 RID: 5334 RVA: 0x00078D9C File Offset: 0x00076F9C
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		this.UpdateAnimationParameters(true);
	}

	// Token: 0x060014D7 RID: 5335 RVA: 0x00078DAC File Offset: 0x00076FAC
	public override void OnFlagsChanged(global::BaseEntity.Flags old, global::BaseEntity.Flags next)
	{
		base.OnFlagsChanged(old, next);
		this.UpdateAnimationParameters(false);
	}

	// Token: 0x060014D8 RID: 5336 RVA: 0x00078DC0 File Offset: 0x00076FC0
	protected void UpdateAnimationParameters(bool init)
	{
		if (!this.model)
		{
			return;
		}
		if (!this.model.animator)
		{
			return;
		}
		if (!this.model.animator.isInitialized)
		{
			return;
		}
		bool flag = this.animatorNeedsInitializing || this.animatorIsOpen != base.IsOpen() || (init && this.isAnimating);
		bool flag2 = this.animatorNeedsInitializing || init;
		if (flag)
		{
			this.isAnimating = true;
			this.model.animator.enabled = true;
			this.model.animator.SetBool("open", this.animatorIsOpen = base.IsOpen());
			if (flag2)
			{
				this.model.animator.fireEvents = false;
				int num = 0;
				while ((float)num < 20f)
				{
					this.model.animator.Update(1f);
					num++;
				}
				this.PutAnimatorToSleep();
			}
			else
			{
				this.model.animator.fireEvents = base.isClient;
				if (base.isServer)
				{
					base.SetFlag(global::BaseEntity.Flags.Busy, true, false);
				}
			}
		}
		else if (flag2)
		{
			this.PutAnimatorToSleep();
		}
		this.animatorNeedsInitializing = false;
	}

	// Token: 0x060014D9 RID: 5337 RVA: 0x00078F20 File Offset: 0x00077120
	protected void OnAnimatorFinished()
	{
		if (!this.isAnimating)
		{
			this.PutAnimatorToSleep();
		}
		this.isAnimating = false;
	}

	// Token: 0x060014DA RID: 5338 RVA: 0x00078F3C File Offset: 0x0007713C
	private void PutAnimatorToSleep()
	{
		if (!this.model || !this.model.animator)
		{
			Debug.LogWarning(base.transform.GetRecursiveName(string.Empty) + " has missing model/animator", base.gameObject);
			return;
		}
		this.model.animator.enabled = false;
		if (base.isServer)
		{
			base.SetFlag(global::BaseEntity.Flags.Busy, false, false);
		}
		this.OnAnimatorDisabled();
	}

	// Token: 0x060014DB RID: 5339 RVA: 0x00078FC4 File Offset: 0x000771C4
	protected virtual void OnAnimatorDisabled()
	{
	}

	// Token: 0x04000F55 RID: 3925
	private bool animatorNeedsInitializing = true;

	// Token: 0x04000F56 RID: 3926
	private bool animatorIsOpen = true;

	// Token: 0x04000F57 RID: 3927
	private bool isAnimating;
}
