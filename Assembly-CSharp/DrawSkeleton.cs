﻿using System;
using UnityEngine;

// Token: 0x020002EF RID: 751
public class DrawSkeleton : MonoBehaviour
{
	// Token: 0x06001313 RID: 4883 RVA: 0x000703EC File Offset: 0x0006E5EC
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.white;
		global::DrawSkeleton.DrawTransform(base.transform);
	}

	// Token: 0x06001314 RID: 4884 RVA: 0x00070404 File Offset: 0x0006E604
	private static void DrawTransform(Transform t)
	{
		for (int i = 0; i < t.childCount; i++)
		{
			Gizmos.DrawLine(t.position, t.GetChild(i).position);
			global::DrawSkeleton.DrawTransform(t.GetChild(i));
		}
	}
}
