﻿using System;
using UnityEngine;

// Token: 0x020007F0 RID: 2032
[RequireComponent(typeof(Camera))]
[ImageEffectAllowedInSceneView]
[AddComponentMenu("KriptoFX/Explosion_Bloom")]
[ExecuteInEditMode]
public class Explosion_Bloom : MonoBehaviour
{
	// Token: 0x170002B8 RID: 696
	// (get) Token: 0x06002590 RID: 9616 RVA: 0x000CFB08 File Offset: 0x000CDD08
	public Shader shader
	{
		get
		{
			if (this.m_Shader == null)
			{
				this.m_Shader = Shader.Find("Hidden/KriptoFX/PostEffects/Explosion_Bloom");
			}
			return this.m_Shader;
		}
	}

	// Token: 0x170002B9 RID: 697
	// (get) Token: 0x06002591 RID: 9617 RVA: 0x000CFB34 File Offset: 0x000CDD34
	public Material material
	{
		get
		{
			if (this.m_Material == null)
			{
				this.m_Material = global::Explosion_Bloom.CheckShaderAndCreateMaterial(this.shader);
			}
			return this.m_Material;
		}
	}

	// Token: 0x06002592 RID: 9618 RVA: 0x000CFB60 File Offset: 0x000CDD60
	public static bool IsSupported(Shader s, bool needDepth, bool needHdr, MonoBehaviour effect)
	{
		if (s == null || !s.isSupported)
		{
			Debug.LogWarningFormat("Missing shader for image effect {0}", new object[]
			{
				effect
			});
			return false;
		}
		if (!SystemInfo.supportsImageEffects)
		{
			Debug.LogWarningFormat("Image effects aren't supported on this device ({0})", new object[]
			{
				effect
			});
			return false;
		}
		if (needDepth && !SystemInfo.SupportsRenderTextureFormat(1))
		{
			Debug.LogWarningFormat("Depth textures aren't supported on this device ({0})", new object[]
			{
				effect
			});
			return false;
		}
		if (needHdr && !SystemInfo.SupportsRenderTextureFormat(2))
		{
			Debug.LogWarningFormat("Floating point textures aren't supported on this device ({0})", new object[]
			{
				effect
			});
			return false;
		}
		return true;
	}

	// Token: 0x06002593 RID: 9619 RVA: 0x000CFC0C File Offset: 0x000CDE0C
	public static Material CheckShaderAndCreateMaterial(Shader s)
	{
		if (s == null || !s.isSupported)
		{
			return null;
		}
		return new Material(s)
		{
			hideFlags = 52
		};
	}

	// Token: 0x170002BA RID: 698
	// (get) Token: 0x06002594 RID: 9620 RVA: 0x000CFC44 File Offset: 0x000CDE44
	public static bool supportsDX11
	{
		get
		{
			return SystemInfo.graphicsShaderLevel >= 50 && SystemInfo.supportsComputeShaders;
		}
	}

	// Token: 0x06002595 RID: 9621 RVA: 0x000CFC5C File Offset: 0x000CDE5C
	private void Awake()
	{
		this.m_Threshold = Shader.PropertyToID("_Threshold");
		this.m_Curve = Shader.PropertyToID("_Curve");
		this.m_PrefilterOffs = Shader.PropertyToID("_PrefilterOffs");
		this.m_SampleScale = Shader.PropertyToID("_SampleScale");
		this.m_Intensity = Shader.PropertyToID("_Intensity");
		this.m_BaseTex = Shader.PropertyToID("_BaseTex");
	}

	// Token: 0x06002596 RID: 9622 RVA: 0x000CFCCC File Offset: 0x000CDECC
	private void OnEnable()
	{
		if (!global::Explosion_Bloom.IsSupported(this.shader, true, false, this))
		{
			base.enabled = false;
		}
	}

	// Token: 0x06002597 RID: 9623 RVA: 0x000CFCE8 File Offset: 0x000CDEE8
	private void OnDisable()
	{
		if (this.m_Material != null)
		{
			Object.DestroyImmediate(this.m_Material);
		}
		this.m_Material = null;
	}

	// Token: 0x06002598 RID: 9624 RVA: 0x000CFD10 File Offset: 0x000CDF10
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		bool isMobilePlatform = Application.isMobilePlatform;
		int num = source.width;
		int num2 = source.height;
		if (!this.settings.highQuality)
		{
			num /= 2;
			num2 /= 2;
		}
		RenderTextureFormat renderTextureFormat = (!isMobilePlatform) ? 9 : 7;
		float num3 = Mathf.Log((float)num2, 2f) + this.settings.radius - 8f;
		int num4 = (int)num3;
		int num5 = Mathf.Clamp(num4, 1, 16);
		float thresholdLinear = this.settings.thresholdLinear;
		this.material.SetFloat(this.m_Threshold, thresholdLinear);
		float num6 = thresholdLinear * this.settings.softKnee + 1E-05f;
		Vector3 vector;
		vector..ctor(thresholdLinear - num6, num6 * 2f, 0.25f / num6);
		this.material.SetVector(this.m_Curve, vector);
		bool flag = !this.settings.highQuality && this.settings.antiFlicker;
		this.material.SetFloat(this.m_PrefilterOffs, (!flag) ? 0f : -0.5f);
		this.material.SetFloat(this.m_SampleScale, 0.5f + num3 - (float)num4);
		this.material.SetFloat(this.m_Intensity, Mathf.Max(0f, this.settings.intensity));
		RenderTexture temporary = RenderTexture.GetTemporary(num, num2, 0, renderTextureFormat);
		Graphics.Blit(source, temporary, this.material, (!this.settings.antiFlicker) ? 0 : 1);
		RenderTexture renderTexture = temporary;
		for (int i = 0; i < num5; i++)
		{
			this.m_blurBuffer1[i] = RenderTexture.GetTemporary(renderTexture.width / 2, renderTexture.height / 2, 0, renderTextureFormat);
			Graphics.Blit(renderTexture, this.m_blurBuffer1[i], this.material, (i != 0) ? 4 : ((!this.settings.antiFlicker) ? 2 : 3));
			renderTexture = this.m_blurBuffer1[i];
		}
		for (int j = num5 - 2; j >= 0; j--)
		{
			RenderTexture renderTexture2 = this.m_blurBuffer1[j];
			this.material.SetTexture(this.m_BaseTex, renderTexture2);
			this.m_blurBuffer2[j] = RenderTexture.GetTemporary(renderTexture2.width, renderTexture2.height, 0, renderTextureFormat);
			Graphics.Blit(renderTexture, this.m_blurBuffer2[j], this.material, (!this.settings.highQuality) ? 5 : 6);
			renderTexture = this.m_blurBuffer2[j];
		}
		int num7 = 7;
		num7 += ((!this.settings.highQuality) ? 0 : 1);
		this.material.SetTexture(this.m_BaseTex, source);
		Graphics.Blit(renderTexture, destination, this.material, num7);
		for (int k = 0; k < 16; k++)
		{
			if (this.m_blurBuffer1[k] != null)
			{
				RenderTexture.ReleaseTemporary(this.m_blurBuffer1[k]);
			}
			if (this.m_blurBuffer2[k] != null)
			{
				RenderTexture.ReleaseTemporary(this.m_blurBuffer2[k]);
			}
			this.m_blurBuffer1[k] = null;
			this.m_blurBuffer2[k] = null;
		}
		RenderTexture.ReleaseTemporary(temporary);
	}

	// Token: 0x040021A6 RID: 8614
	[SerializeField]
	public global::Explosion_Bloom.Settings settings = global::Explosion_Bloom.Settings.defaultSettings;

	// Token: 0x040021A7 RID: 8615
	[HideInInspector]
	[SerializeField]
	private Shader m_Shader;

	// Token: 0x040021A8 RID: 8616
	private Material m_Material;

	// Token: 0x040021A9 RID: 8617
	private const int kMaxIterations = 16;

	// Token: 0x040021AA RID: 8618
	private RenderTexture[] m_blurBuffer1 = new RenderTexture[16];

	// Token: 0x040021AB RID: 8619
	private RenderTexture[] m_blurBuffer2 = new RenderTexture[16];

	// Token: 0x040021AC RID: 8620
	private int m_Threshold;

	// Token: 0x040021AD RID: 8621
	private int m_Curve;

	// Token: 0x040021AE RID: 8622
	private int m_PrefilterOffs;

	// Token: 0x040021AF RID: 8623
	private int m_SampleScale;

	// Token: 0x040021B0 RID: 8624
	private int m_Intensity;

	// Token: 0x040021B1 RID: 8625
	private int m_BaseTex;

	// Token: 0x020007F1 RID: 2033
	[Serializable]
	public struct Settings
	{
		// Token: 0x170002BB RID: 699
		// (get) Token: 0x0600259A RID: 9626 RVA: 0x000D0080 File Offset: 0x000CE280
		// (set) Token: 0x06002599 RID: 9625 RVA: 0x000D0074 File Offset: 0x000CE274
		public float thresholdGamma
		{
			get
			{
				return Mathf.Max(0f, this.threshold);
			}
			set
			{
				this.threshold = value;
			}
		}

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x0600259C RID: 9628 RVA: 0x000D00A4 File Offset: 0x000CE2A4
		// (set) Token: 0x0600259B RID: 9627 RVA: 0x000D0094 File Offset: 0x000CE294
		public float thresholdLinear
		{
			get
			{
				return Mathf.GammaToLinearSpace(this.thresholdGamma);
			}
			set
			{
				this.threshold = Mathf.LinearToGammaSpace(value);
			}
		}

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x0600259D RID: 9629 RVA: 0x000D00B4 File Offset: 0x000CE2B4
		public static global::Explosion_Bloom.Settings defaultSettings
		{
			get
			{
				return new global::Explosion_Bloom.Settings
				{
					threshold = 2f,
					softKnee = 0f,
					radius = 7f,
					intensity = 0.7f,
					highQuality = true,
					antiFlicker = true
				};
			}
		}

		// Token: 0x040021B2 RID: 8626
		[Tooltip("Filters out pixels under this level of brightness.")]
		[SerializeField]
		public float threshold;

		// Token: 0x040021B3 RID: 8627
		[SerializeField]
		[Range(0f, 1f)]
		[Tooltip("Makes transition between under/over-threshold gradual.")]
		public float softKnee;

		// Token: 0x040021B4 RID: 8628
		[SerializeField]
		[Range(1f, 7f)]
		[Tooltip("Changes extent of veiling effects in a screen resolution-independent fashion.")]
		public float radius;

		// Token: 0x040021B5 RID: 8629
		[SerializeField]
		[Tooltip("Blend factor of the result image.")]
		public float intensity;

		// Token: 0x040021B6 RID: 8630
		[Tooltip("Controls filter quality and buffer resolution.")]
		[SerializeField]
		public bool highQuality;

		// Token: 0x040021B7 RID: 8631
		[Tooltip("Reduces flashing noise with an additional filter.")]
		[SerializeField]
		public bool antiFlicker;
	}
}
