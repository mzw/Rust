﻿using System;
using UnityEngine;

// Token: 0x020005D6 RID: 1494
public class TerrainSplatSet : global::TerrainModifier
{
	// Token: 0x06001EC6 RID: 7878 RVA: 0x000AD8C8 File Offset: 0x000ABAC8
	protected override void Apply(Vector3 position, float opacity, float radius, float fade)
	{
		if (!global::TerrainMeta.SplatMap)
		{
			return;
		}
		global::TerrainMeta.SplatMap.SetSplat(position, (int)this.SplatType, opacity, radius, fade);
	}

	// Token: 0x0400199C RID: 6556
	public global::TerrainSplat.Enum SplatType;
}
