﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200070D RID: 1805
public class ToggleLayer : MonoBehaviour, IClientComponent
{
	// Token: 0x06002252 RID: 8786 RVA: 0x000C07CC File Offset: 0x000BE9CC
	protected void OnEnable()
	{
		if (global::MainCamera.mainCamera)
		{
			this.toggleControl.isOn = ((global::MainCamera.mainCamera.cullingMask & this.layer.Mask) != 0);
		}
	}

	// Token: 0x06002253 RID: 8787 RVA: 0x000C0804 File Offset: 0x000BEA04
	public void OnToggleChanged()
	{
		if (global::MainCamera.mainCamera)
		{
			if (this.toggleControl.isOn)
			{
				global::MainCamera.mainCamera.cullingMask |= this.layer.Mask;
			}
			else
			{
				global::MainCamera.mainCamera.cullingMask &= ~this.layer.Mask;
			}
		}
	}

	// Token: 0x06002254 RID: 8788 RVA: 0x000C0870 File Offset: 0x000BEA70
	protected void OnValidate()
	{
		if (this.textControl)
		{
			this.textControl.text = this.layer.Name;
		}
	}

	// Token: 0x04001EC9 RID: 7881
	public Toggle toggleControl;

	// Token: 0x04001ECA RID: 7882
	public Text textControl;

	// Token: 0x04001ECB RID: 7883
	public global::LayerSelect layer;
}
