﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C2 RID: 1730
public class QuickCraftButton : MonoBehaviour
{
	// Token: 0x04001D44 RID: 7492
	public Image icon;

	// Token: 0x04001D45 RID: 7493
	public global::Tooltip tooltip;

	// Token: 0x04001D46 RID: 7494
	public Text CraftCount;
}
