﻿using System;
using UnityEngine;

// Token: 0x020001D3 RID: 467
public class AmbienceSpawnEmitters : MonoBehaviour, IClientComponent
{
	// Token: 0x040008EA RID: 2282
	public int baseEmitterCount = 5;

	// Token: 0x040008EB RID: 2283
	public int baseEmitterDistance = 10;

	// Token: 0x040008EC RID: 2284
	public GameObject emitterPrefab;
}
