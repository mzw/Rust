﻿using System;
using UnityEngine;

// Token: 0x020003F1 RID: 1009
public class TriggeredEventPrefab : global::TriggeredEvent
{
	// Token: 0x0600175E RID: 5982 RVA: 0x000861D0 File Offset: 0x000843D0
	private void RunEvent()
	{
		Debug.Log("[event] " + this.targetPrefab.resourcePath);
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.targetPrefab.resourcePath, default(Vector3), default(Quaternion), true);
		if (baseEntity)
		{
			baseEntity.Spawn();
		}
	}

	// Token: 0x040011F9 RID: 4601
	public global::GameObjectRef targetPrefab;
}
