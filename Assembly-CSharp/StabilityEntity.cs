﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x020003D7 RID: 983
public class StabilityEntity : global::DecayEntity
{
	// Token: 0x060016E9 RID: 5865 RVA: 0x00083E68 File Offset: 0x00082068
	public override void ResetState()
	{
		base.ResetState();
		this.cachedStability = 0f;
		this.cachedDistanceFromGround = int.MaxValue;
		this.supports = null;
		this.stabilityStrikes = 0;
		this.dirty = false;
	}

	// Token: 0x060016EA RID: 5866 RVA: 0x00083E9C File Offset: 0x0008209C
	public void InitializeSupports()
	{
		this.supports = new List<global::StabilityEntity.Support>();
		if (this.grounded)
		{
			return;
		}
		List<global::EntityLink> entityLinks = base.GetEntityLinks(true);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			global::EntityLink entityLink = entityLinks[i];
			if (entityLink.IsMale())
			{
				if (entityLink.socket is global::StabilitySocket)
				{
					this.supports.Add(new global::StabilityEntity.Support(this, entityLink, (entityLink.socket as global::StabilitySocket).support));
				}
				if (entityLink.socket is global::ConstructionSocket)
				{
					this.supports.Add(new global::StabilityEntity.Support(this, entityLink, (entityLink.socket as global::ConstructionSocket).support));
				}
			}
		}
	}

	// Token: 0x060016EB RID: 5867 RVA: 0x00083F5C File Offset: 0x0008215C
	public int DistanceFromGround(global::StabilityEntity ignoreEntity = null)
	{
		if (this.grounded)
		{
			return 1;
		}
		if (this.supports == null)
		{
			return 1;
		}
		if (ignoreEntity == null)
		{
			ignoreEntity = this;
		}
		int num = int.MaxValue;
		for (int i = 0; i < this.supports.Count; i++)
		{
			global::StabilityEntity.Support support = this.supports[i];
			global::StabilityEntity stabilityEntity = support.SupportEntity(ignoreEntity);
			if (!(stabilityEntity == null))
			{
				int num2 = stabilityEntity.CachedDistanceFromGround(ignoreEntity);
				if (num2 != 2147483647)
				{
					num = Mathf.Min(num, num2 + 1);
				}
			}
		}
		return num;
	}

	// Token: 0x060016EC RID: 5868 RVA: 0x00084000 File Offset: 0x00082200
	public float SupportValue(global::StabilityEntity ignoreEntity = null)
	{
		if (this.grounded)
		{
			return 1f;
		}
		if (this.supports == null)
		{
			return 1f;
		}
		if (ignoreEntity == null)
		{
			ignoreEntity = this;
		}
		float num = 0f;
		for (int i = 0; i < this.supports.Count; i++)
		{
			global::StabilityEntity.Support support = this.supports[i];
			global::StabilityEntity stabilityEntity = support.SupportEntity(ignoreEntity);
			if (!(stabilityEntity == null))
			{
				float num2 = stabilityEntity.CachedSupportValue(ignoreEntity);
				if (num2 != 0f)
				{
					num += num2 * support.factor;
				}
			}
		}
		return Mathf.Clamp01(num);
	}

	// Token: 0x060016ED RID: 5869 RVA: 0x000840B4 File Offset: 0x000822B4
	public int CachedDistanceFromGround(global::StabilityEntity ignoreEntity = null)
	{
		if (this.grounded)
		{
			return 1;
		}
		if (this.supports == null)
		{
			return 1;
		}
		if (ignoreEntity == null)
		{
			ignoreEntity = this;
		}
		int num = int.MaxValue;
		for (int i = 0; i < this.supports.Count; i++)
		{
			global::StabilityEntity.Support support = this.supports[i];
			global::StabilityEntity stabilityEntity = support.SupportEntity(ignoreEntity);
			if (!(stabilityEntity == null))
			{
				int num2 = stabilityEntity.cachedDistanceFromGround;
				if (num2 != 2147483647)
				{
					num = Mathf.Min(num, num2 + 1);
				}
			}
		}
		return num;
	}

	// Token: 0x060016EE RID: 5870 RVA: 0x00084158 File Offset: 0x00082358
	public float CachedSupportValue(global::StabilityEntity ignoreEntity = null)
	{
		if (this.grounded)
		{
			return 1f;
		}
		if (this.supports == null)
		{
			return 1f;
		}
		if (ignoreEntity == null)
		{
			ignoreEntity = this;
		}
		float num = 0f;
		for (int i = 0; i < this.supports.Count; i++)
		{
			global::StabilityEntity.Support support = this.supports[i];
			global::StabilityEntity stabilityEntity = support.SupportEntity(ignoreEntity);
			if (!(stabilityEntity == null))
			{
				float num2 = stabilityEntity.cachedStability;
				if (num2 != 0f)
				{
					num += num2 * support.factor;
				}
			}
		}
		return Mathf.Clamp01(num);
	}

	// Token: 0x060016EF RID: 5871 RVA: 0x0008420C File Offset: 0x0008240C
	public void StabilityCheck()
	{
		if (base.IsDestroyed)
		{
			return;
		}
		if (this.supports == null)
		{
			this.InitializeSupports();
		}
		bool flag = false;
		int num = this.DistanceFromGround(null);
		if (num != this.cachedDistanceFromGround)
		{
			this.cachedDistanceFromGround = num;
			flag = true;
		}
		float num2 = this.SupportValue(null);
		if (Mathf.Abs(this.cachedStability - num2) > ConVar.Stability.accuracy)
		{
			this.cachedStability = num2;
			flag = true;
		}
		if (flag)
		{
			this.dirty = true;
			this.UpdateConnectedEntities();
			this.UpdateStability();
		}
		else if (this.dirty)
		{
			this.dirty = false;
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
		if (num2 < ConVar.Stability.collapse)
		{
			if (this.stabilityStrikes < ConVar.Stability.strikes)
			{
				this.UpdateStability();
				this.stabilityStrikes++;
			}
			else
			{
				base.Kill(global::BaseNetworkable.DestroyMode.Gib);
			}
		}
		else
		{
			this.stabilityStrikes = 0;
		}
	}

	// Token: 0x060016F0 RID: 5872 RVA: 0x000842FC File Offset: 0x000824FC
	public void UpdateStability()
	{
		global::StabilityEntity.stabilityCheckQueue.Add(this);
	}

	// Token: 0x060016F1 RID: 5873 RVA: 0x0008430C File Offset: 0x0008250C
	public void UpdateSurroundingEntities()
	{
		global::StabilityEntity.updateSurroundingsQueue.Add(base.WorldSpaceBounds().ToBounds());
	}

	// Token: 0x060016F2 RID: 5874 RVA: 0x00084334 File Offset: 0x00082534
	public void UpdateConnectedEntities()
	{
		List<global::EntityLink> entityLinks = base.GetEntityLinks(true);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			global::EntityLink entityLink = entityLinks[i];
			if (entityLink.IsFemale())
			{
				for (int j = 0; j < entityLink.connections.Count; j++)
				{
					global::StabilityEntity stabilityEntity = entityLink.connections[j].owner as global::StabilityEntity;
					if (!(stabilityEntity == null))
					{
						if (!stabilityEntity.isClient)
						{
							if (!stabilityEntity.IsDestroyed)
							{
								stabilityEntity.UpdateStability();
							}
						}
					}
				}
			}
		}
	}

	// Token: 0x060016F3 RID: 5875 RVA: 0x000843E8 File Offset: 0x000825E8
	protected void OnPhysicsNeighbourChanged()
	{
		if (base.IsDestroyed)
		{
			return;
		}
		this.StabilityCheck();
	}

	// Token: 0x060016F4 RID: 5876 RVA: 0x000843FC File Offset: 0x000825FC
	protected void DebugNudge()
	{
		this.StabilityCheck();
	}

	// Token: 0x060016F5 RID: 5877 RVA: 0x00084404 File Offset: 0x00082604
	public override void ServerInit()
	{
		base.ServerInit();
		if (!Application.isLoadingSave)
		{
			this.UpdateStability();
		}
	}

	// Token: 0x060016F6 RID: 5878 RVA: 0x0008441C File Offset: 0x0008261C
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		this.UpdateSurroundingEntities();
	}

	// Token: 0x060016F7 RID: 5879 RVA: 0x0008442C File Offset: 0x0008262C
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.stabilityEntity = Facepunch.Pool.Get<ProtoBuf.StabilityEntity>();
		info.msg.stabilityEntity.stability = this.cachedStability;
		info.msg.stabilityEntity.distanceFromGround = this.cachedDistanceFromGround;
	}

	// Token: 0x060016F8 RID: 5880 RVA: 0x00084480 File Offset: 0x00082680
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.stabilityEntity != null)
		{
			this.cachedStability = info.msg.stabilityEntity.stability;
			this.cachedDistanceFromGround = info.msg.stabilityEntity.distanceFromGround;
			if (this.cachedStability <= 0f)
			{
				this.cachedStability = 0f;
			}
			if (this.cachedDistanceFromGround <= 0)
			{
				this.cachedDistanceFromGround = int.MaxValue;
			}
		}
	}

	// Token: 0x040011A3 RID: 4515
	public bool grounded;

	// Token: 0x040011A4 RID: 4516
	[NonSerialized]
	public float cachedStability;

	// Token: 0x040011A5 RID: 4517
	[NonSerialized]
	public int cachedDistanceFromGround = int.MaxValue;

	// Token: 0x040011A6 RID: 4518
	private List<global::StabilityEntity.Support> supports;

	// Token: 0x040011A7 RID: 4519
	private int stabilityStrikes;

	// Token: 0x040011A8 RID: 4520
	private bool dirty;

	// Token: 0x040011A9 RID: 4521
	public static global::StabilityEntity.StabilityCheckWorkQueue stabilityCheckQueue = new global::StabilityEntity.StabilityCheckWorkQueue();

	// Token: 0x040011AA RID: 4522
	public static global::StabilityEntity.UpdateSurroundingsQueue updateSurroundingsQueue = new global::StabilityEntity.UpdateSurroundingsQueue();

	// Token: 0x020003D8 RID: 984
	public class StabilityCheckWorkQueue : ObjectWorkQueue<global::StabilityEntity>
	{
		// Token: 0x060016FB RID: 5883 RVA: 0x00084528 File Offset: 0x00082728
		protected override void RunJob(global::StabilityEntity entity)
		{
			if (!this.ShouldAdd(entity))
			{
				return;
			}
			entity.StabilityCheck();
		}

		// Token: 0x060016FC RID: 5884 RVA: 0x00084540 File Offset: 0x00082740
		protected override bool ShouldAdd(global::StabilityEntity entity)
		{
			return ConVar.Server.stability && entity.IsValid() && entity.isServer;
		}
	}

	// Token: 0x020003D9 RID: 985
	public class UpdateSurroundingsQueue : ObjectWorkQueue<Bounds>
	{
		// Token: 0x060016FE RID: 5886 RVA: 0x00084574 File Offset: 0x00082774
		protected override void RunJob(Bounds bounds)
		{
			if (!ConVar.Server.stability)
			{
				return;
			}
			List<global::BaseEntity> list = Facepunch.Pool.GetList<global::BaseEntity>();
			global::Vis.Entities<global::BaseEntity>(bounds.center, bounds.extents.magnitude + 1f, list, 2097408, 2);
			foreach (global::BaseEntity baseEntity in list)
			{
				if (!baseEntity.IsDestroyed)
				{
					if (!baseEntity.isClient)
					{
						if (baseEntity is global::StabilityEntity)
						{
							(baseEntity as global::StabilityEntity).OnPhysicsNeighbourChanged();
						}
						else
						{
							baseEntity.BroadcastMessage("OnPhysicsNeighbourChanged", 1);
						}
					}
				}
			}
			Facepunch.Pool.FreeList<global::BaseEntity>(ref list);
		}
	}

	// Token: 0x020003DA RID: 986
	private class Support
	{
		// Token: 0x060016FF RID: 5887 RVA: 0x0008464C File Offset: 0x0008284C
		public Support(global::StabilityEntity parent, global::EntityLink link, float factor)
		{
			this.parent = parent;
			this.link = link;
			this.factor = factor;
		}

		// Token: 0x06001700 RID: 5888 RVA: 0x00084674 File Offset: 0x00082874
		public global::StabilityEntity SupportEntity(global::StabilityEntity ignoreEntity = null)
		{
			global::StabilityEntity stabilityEntity = null;
			for (int i = 0; i < this.link.connections.Count; i++)
			{
				global::StabilityEntity stabilityEntity2 = this.link.connections[i].owner as global::StabilityEntity;
				if (!(stabilityEntity2 == null))
				{
					if (!(stabilityEntity2 == this.parent))
					{
						if (!(stabilityEntity2 == ignoreEntity))
						{
							if (!stabilityEntity2.isClient)
							{
								if (!stabilityEntity2.IsDestroyed)
								{
									if (stabilityEntity == null || stabilityEntity2.cachedDistanceFromGround < stabilityEntity.cachedDistanceFromGround)
									{
										stabilityEntity = stabilityEntity2;
									}
								}
							}
						}
					}
				}
			}
			return stabilityEntity;
		}

		// Token: 0x040011AB RID: 4523
		public global::StabilityEntity parent;

		// Token: 0x040011AC RID: 4524
		public global::EntityLink link;

		// Token: 0x040011AD RID: 4525
		public float factor = 1f;
	}
}
