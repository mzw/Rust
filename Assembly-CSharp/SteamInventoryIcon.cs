﻿using System;
using Facepunch.Steamworks;
using UnityEngine;

// Token: 0x020006E8 RID: 1768
public class SteamInventoryIcon : MonoBehaviour
{
	// Token: 0x17000250 RID: 592
	// (get) Token: 0x060021AD RID: 8621 RVA: 0x000BCE1C File Offset: 0x000BB01C
	// (set) Token: 0x060021AE RID: 8622 RVA: 0x000BCE24 File Offset: 0x000BB024
	private Workshop.Item WorkshopItem { get; set; }
}
