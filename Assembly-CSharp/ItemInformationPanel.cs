﻿using System;
using UnityEngine;

// Token: 0x020006AD RID: 1709
public class ItemInformationPanel : MonoBehaviour
{
	// Token: 0x06002143 RID: 8515 RVA: 0x000BBEAC File Offset: 0x000BA0AC
	public virtual bool EligableForDisplay(global::ItemDefinition info)
	{
		Debug.LogWarning("ItemInformationPanel.EligableForDisplay");
		return false;
	}

	// Token: 0x06002144 RID: 8516 RVA: 0x000BBEBC File Offset: 0x000BA0BC
	public virtual void SetupForItem(global::ItemDefinition info, global::Item item = null)
	{
		Debug.LogWarning("ItemInformationPanel.SetupForItem");
	}
}
