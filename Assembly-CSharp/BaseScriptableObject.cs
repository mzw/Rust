﻿using System;
using UnityEngine;

// Token: 0x02000737 RID: 1847
public class BaseScriptableObject : ScriptableObject
{
	// Token: 0x060022BD RID: 8893 RVA: 0x000C17B4 File Offset: 0x000BF9B4
	public string LookupFileName()
	{
		return global::StringPool.Get(this.FilenameStringId);
	}

	// Token: 0x060022BE RID: 8894 RVA: 0x000C17C4 File Offset: 0x000BF9C4
	public static bool operator ==(global::BaseScriptableObject a, global::BaseScriptableObject b)
	{
		return object.ReferenceEquals(a, b) || (a != null && b != null && a.FilenameStringId == b.FilenameStringId);
	}

	// Token: 0x060022BF RID: 8895 RVA: 0x000C17F0 File Offset: 0x000BF9F0
	public static bool operator !=(global::BaseScriptableObject a, global::BaseScriptableObject b)
	{
		return !(a == b);
	}

	// Token: 0x060022C0 RID: 8896 RVA: 0x000C17FC File Offset: 0x000BF9FC
	public override int GetHashCode()
	{
		return (int)this.FilenameStringId;
	}

	// Token: 0x060022C1 RID: 8897 RVA: 0x000C1804 File Offset: 0x000BFA04
	public override bool Equals(object o)
	{
		return o != null && o is global::BaseScriptableObject && o as global::BaseScriptableObject == this;
	}

	// Token: 0x04001F38 RID: 7992
	[HideInInspector]
	public uint FilenameStringId;
}
