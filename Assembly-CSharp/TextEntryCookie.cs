﻿using System;
using Rust;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000707 RID: 1799
public class TextEntryCookie : MonoBehaviour
{
	// Token: 0x1700026D RID: 621
	// (get) Token: 0x0600223E RID: 8766 RVA: 0x000C046C File Offset: 0x000BE66C
	public InputField control
	{
		get
		{
			return base.GetComponent<InputField>();
		}
	}

	// Token: 0x0600223F RID: 8767 RVA: 0x000C0474 File Offset: 0x000BE674
	private void OnEnable()
	{
		string @string = PlayerPrefs.GetString("TextEntryCookie_" + base.name);
		if (!string.IsNullOrEmpty(@string))
		{
			this.control.text = @string;
		}
		this.control.onValueChanged.Invoke(this.control.text);
	}

	// Token: 0x06002240 RID: 8768 RVA: 0x000C04CC File Offset: 0x000BE6CC
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		PlayerPrefs.SetString("TextEntryCookie_" + base.name, this.control.text);
	}
}
