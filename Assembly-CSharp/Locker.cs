﻿using System;
using ConVar;
using Network;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000079 RID: 121
public class Locker : global::StorageContainer
{
	// Token: 0x06000852 RID: 2130 RVA: 0x000360C0 File Offset: 0x000342C0
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Locker.OnRpcMessage", 0.1f))
		{
			if (rpc == 2514928982u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Equip ");
				}
				using (TimeWarning.New("RPC_Equip", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_Equip", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Equip(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Equip");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000853 RID: 2131 RVA: 0x000362A0 File Offset: 0x000344A0
	public bool IsEquipping()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x06000854 RID: 2132 RVA: 0x000362B0 File Offset: 0x000344B0
	public override void ServerInit()
	{
		base.ServerInit();
		global::ItemContainer inventory = this.inventory;
		inventory.canAcceptItem = (Func<global::Item, int, bool>)Delegate.Combine(inventory.canAcceptItem, new Func<global::Item, int, bool>(this.LockerItemFilter));
		base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
	}

	// Token: 0x06000855 RID: 2133 RVA: 0x000362EC File Offset: 0x000344EC
	public bool LockerItemFilter(global::Item item, int targetSlot)
	{
		return this.equippingActive;
	}

	// Token: 0x06000856 RID: 2134 RVA: 0x000362F4 File Offset: 0x000344F4
	public void ClearEquipping()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
	}

	// Token: 0x06000857 RID: 2135 RVA: 0x00036304 File Offset: 0x00034504
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_Equip(global::BaseEntity.RPCMessage msg)
	{
		int num = msg.read.Int32();
		if (num < 0 || num > 2)
		{
			return;
		}
		if (this.IsEquipping())
		{
			return;
		}
		global::BasePlayer player = msg.player;
		int num2 = this.rowSize * this.columnSize;
		int num3 = num * num2;
		this.equippingActive = true;
		bool flag = false;
		for (int i = 0; i < player.inventory.containerWear.capacity; i++)
		{
			global::Item slot = player.inventory.containerWear.GetSlot(i);
			if (slot != null)
			{
				slot.RemoveFromContainer();
				this.clothingBuffer[i] = slot;
			}
		}
		for (int j = 0; j < this.rowSize; j++)
		{
			int num4 = num3 + j;
			int iTargetPos = j;
			global::Item slot2 = this.inventory.GetSlot(num4);
			global::Item item = this.clothingBuffer[j];
			if (slot2 != null)
			{
				flag = true;
				if (slot2.info.category != global::ItemCategory.Attire || !slot2.MoveToContainer(player.inventory.containerWear, iTargetPos, true))
				{
					slot2.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
				}
			}
			if (item != null)
			{
				flag = true;
				if (item.info.category != global::ItemCategory.Attire || !item.MoveToContainer(this.inventory, num4, true))
				{
					item.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
				}
			}
			this.clothingBuffer[j] = null;
		}
		for (int k = 0; k < this.rowSize; k++)
		{
			int num5 = num3 + k + this.rowSize;
			int iTargetPos2 = k;
			global::Item slot3 = this.inventory.GetSlot(num5);
			global::Item slot4 = player.inventory.containerBelt.GetSlot(k);
			if (slot4 != null)
			{
				slot4.RemoveFromContainer();
			}
			if (slot3 != null)
			{
				flag = true;
				if (!slot3.MoveToContainer(player.inventory.containerBelt, iTargetPos2, true))
				{
					slot3.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
				}
			}
			if (slot4 != null)
			{
				flag = true;
				if (!slot4.MoveToContainer(this.inventory, num5, true))
				{
					slot4.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
				}
			}
		}
		this.equippingActive = false;
		if (flag)
		{
			global::Effect.server.Run(this.equipSound.resourcePath, player, global::StringPool.Get("spine3"), Vector3.zero, Vector3.zero, null, false);
			base.SetFlag(global::BaseEntity.Flags.Reserved1, true, false);
			base.Invoke(new Action(this.ClearEquipping), 1.5f);
		}
	}

	// Token: 0x06000858 RID: 2136 RVA: 0x000365D0 File Offset: 0x000347D0
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x040003E1 RID: 993
	public global::GameObjectRef equipSound;

	// Token: 0x040003E2 RID: 994
	private int rowSize = 6;

	// Token: 0x040003E3 RID: 995
	private int columnSize = 2;

	// Token: 0x040003E4 RID: 996
	private global::Item[] clothingBuffer = new global::Item[6];

	// Token: 0x040003E5 RID: 997
	private bool equippingActive;

	// Token: 0x0200007A RID: 122
	public static class LockerFlags
	{
		// Token: 0x040003E6 RID: 998
		public const global::BaseEntity.Flags IsEquipping = global::BaseEntity.Flags.Reserved1;
	}
}
