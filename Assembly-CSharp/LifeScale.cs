﻿using System;
using UnityEngine;

// Token: 0x020003B8 RID: 952
public class LifeScale : global::BaseMonoBehaviour
{
	// Token: 0x06001688 RID: 5768 RVA: 0x00082394 File Offset: 0x00080594
	public void OnEnable()
	{
		this.Init();
		base.transform.localScale = this.initialScale;
	}

	// Token: 0x06001689 RID: 5769 RVA: 0x000823B0 File Offset: 0x000805B0
	public void SetProgress(float progress)
	{
		this.Init();
		this.targetLerpScale = Vector3.Lerp(this.initialScale, this.finalScale, progress);
		base.InvokeRepeating(new Action(this.UpdateScale), 0f, 0.015f);
	}

	// Token: 0x0600168A RID: 5770 RVA: 0x000823EC File Offset: 0x000805EC
	public void Init()
	{
		if (!this.initialized)
		{
			this.initialScale = base.transform.localScale;
			this.initialized = true;
		}
	}

	// Token: 0x0600168B RID: 5771 RVA: 0x00082414 File Offset: 0x00080614
	public void UpdateScale()
	{
		base.transform.localScale = Vector3.Lerp(base.transform.localScale, this.targetLerpScale, Time.deltaTime);
		if (base.transform.localScale == this.targetLerpScale)
		{
			this.targetLerpScale = Vector3.zero;
			base.CancelInvoke(new Action(this.UpdateScale));
		}
	}

	// Token: 0x040010FD RID: 4349
	[NonSerialized]
	private bool initialized;

	// Token: 0x040010FE RID: 4350
	[NonSerialized]
	private Vector3 initialScale;

	// Token: 0x040010FF RID: 4351
	public Vector3 finalScale = Vector3.one;

	// Token: 0x04001100 RID: 4352
	private Vector3 targetLerpScale = Vector3.zero;
}
