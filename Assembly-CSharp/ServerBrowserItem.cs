﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006E0 RID: 1760
public class ServerBrowserItem : MonoBehaviour
{
	// Token: 0x04001DE1 RID: 7649
	public Text serverName;

	// Token: 0x04001DE2 RID: 7650
	public Text mapName;

	// Token: 0x04001DE3 RID: 7651
	public Text playerCount;

	// Token: 0x04001DE4 RID: 7652
	public Text ping;

	// Token: 0x04001DE5 RID: 7653
	public Toggle favourited;
}
