﻿using System;
using Rust;
using UnityEngine;

// Token: 0x02000601 RID: 1537
public class SubsurfaceProfile : ScriptableObject
{
	// Token: 0x1700022B RID: 555
	// (get) Token: 0x06001F48 RID: 8008 RVA: 0x000B1C00 File Offset: 0x000AFE00
	public static Texture2D Texture
	{
		get
		{
			return (global::SubsurfaceProfile.profileTexture == null) ? null : global::SubsurfaceProfile.profileTexture.Texture;
		}
	}

	// Token: 0x1700022C RID: 556
	// (get) Token: 0x06001F49 RID: 8009 RVA: 0x000B1C1C File Offset: 0x000AFE1C
	public int Id
	{
		get
		{
			return this.id;
		}
	}

	// Token: 0x06001F4A RID: 8010 RVA: 0x000B1C24 File Offset: 0x000AFE24
	private void OnEnable()
	{
		this.id = global::SubsurfaceProfile.profileTexture.AddProfile(this.Data, this);
	}

	// Token: 0x06001F4B RID: 8011 RVA: 0x000B1C40 File Offset: 0x000AFE40
	private void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		global::SubsurfaceProfile.profileTexture.RemoveProfile(this.id);
	}

	// Token: 0x06001F4C RID: 8012 RVA: 0x000B1C60 File Offset: 0x000AFE60
	public void Update()
	{
		global::SubsurfaceProfile.profileTexture.UpdateProfile(this.id, this.Data);
	}

	// Token: 0x04001A4B RID: 6731
	private static global::SubsurfaceProfileTexture profileTexture = new global::SubsurfaceProfileTexture();

	// Token: 0x04001A4C RID: 6732
	public global::SubsurfaceProfileData Data = global::SubsurfaceProfileData.Default;

	// Token: 0x04001A4D RID: 6733
	private int id = -1;
}
