﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200002E RID: 46
public class BaseLauncher : global::BaseProjectile
{
	// Token: 0x06000435 RID: 1077 RVA: 0x000172C8 File Offset: 0x000154C8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseLauncher.OnRpcMessage", 0.1f))
		{
			if (rpc == 3687951631u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SV_Launch ");
				}
				using (TimeWarning.New("SV_Launch", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("SV_Launch", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SV_Launch(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in SV_Launch");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000436 RID: 1078 RVA: 0x000174A4 File Offset: 0x000156A4
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void SV_Launch(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.VerifyClientAttack(player))
		{
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		if (this.reloadFinished && base.HasReloadCooldown())
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Reloading (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "reload_cooldown");
			return;
		}
		this.reloadStarted = false;
		this.reloadFinished = false;
		if (this.primaryMagazine.contents <= 0)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Magazine empty (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "magazine_empty");
			return;
		}
		this.primaryMagazine.contents--;
		Vector3 vector = msg.read.Vector3();
		Vector3 vector2 = msg.read.Vector3().normalized;
		if (!base.ValidateEyePos(player, vector))
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Eye position (" + base.ShortPrefabName + ")");
			return;
		}
		global::ItemModProjectile component = this.primaryMagazine.ammoType.GetComponent<global::ItemModProjectile>();
		if (!component)
		{
			global::AntiHack.Log(player, global::AntiHackType.ProjectileHack, "Item mod not found (" + base.ShortPrefabName + ")");
			player.stats.combat.Log(this, "mod_missing");
			return;
		}
		float num = this.GetAimCone() + component.projectileSpread;
		if (num > 0f)
		{
			vector2 = global::AimConeUtil.GetModifiedAimConeDirection(num, vector2, true);
		}
		float num2 = 1f;
		RaycastHit raycastHit;
		if (UnityEngine.Physics.Raycast(vector, vector2, ref raycastHit, num2, 1101212417))
		{
			num2 = raycastHit.distance - 0.1f;
		}
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(component.projectileObject.resourcePath, vector + vector2 * num2, default(Quaternion), true);
		if (baseEntity == null)
		{
			return;
		}
		baseEntity.creatorEntity = player;
		baseEntity.SendMessage("InitializeVelocity", vector2 * 1f);
		baseEntity.Spawn();
		Interface.CallHook("OnRocketLaunched", new object[]
		{
			msg.player,
			baseEntity
		});
		base.StartAttackCooldown(base.ScaleRepeatDelay(this.repeatDelay));
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem == null)
		{
			return;
		}
		ownerItem.LoseCondition(Random.Range(1f, 2f));
	}
}
