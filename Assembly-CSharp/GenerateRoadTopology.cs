﻿using System;
using System.Collections.Generic;

// Token: 0x020005B7 RID: 1463
public class GenerateRoadTopology : global::ProceduralComponent
{
	// Token: 0x06001E70 RID: 7792 RVA: 0x000AA500 File Offset: 0x000A8700
	public override void Process(uint seed)
	{
		global::GenerateRoadTopology.<Process>c__AnonStorey1 <Process>c__AnonStorey = new global::GenerateRoadTopology.<Process>c__AnonStorey1();
		List<global::PathList> roads = global::TerrainMeta.Path.Roads;
		<Process>c__AnonStorey.heightmap = global::TerrainMeta.HeightMap;
		<Process>c__AnonStorey.topomap = global::TerrainMeta.TopologyMap;
		foreach (global::PathList pathList in roads)
		{
			pathList.Path.RecalculateTangents();
		}
		<Process>c__AnonStorey.heightmap.Push();
		foreach (global::PathList pathList2 in roads)
		{
			pathList2.AdjustTerrainHeight();
			pathList2.AdjustTerrainTexture();
			pathList2.AdjustTerrainTopology();
		}
		<Process>c__AnonStorey.heightmap.Pop();
		int[,] map = <Process>c__AnonStorey.topomap.dst;
		global::ImageProcessing.Dilate2D(map, 6144, 6, delegate(int x, int y)
		{
			if ((map[x, y] & 49) != 0)
			{
				map[x, y] |= 4096;
			}
			float normX = <Process>c__AnonStorey.topomap.Coordinate(x);
			float normZ = <Process>c__AnonStorey.topomap.Coordinate(y);
			if (<Process>c__AnonStorey.heightmap.GetSlope(normX, normZ) > 40f)
			{
				map[x, y] |= 2;
			}
		});
	}
}
