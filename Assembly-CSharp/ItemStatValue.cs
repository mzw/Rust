﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006B3 RID: 1715
public class ItemStatValue : MonoBehaviour
{
	// Token: 0x04001D10 RID: 7440
	public Text text;

	// Token: 0x04001D11 RID: 7441
	public Slider slider;

	// Token: 0x04001D12 RID: 7442
	public bool selectedItem;

	// Token: 0x04001D13 RID: 7443
	public bool smallerIsBetter;

	// Token: 0x04001D14 RID: 7444
	public bool asPercentage;
}
