﻿using System;
using UnityEngine;

// Token: 0x020000D4 RID: 212
public class EffectMount : global::EntityComponent<global::BaseEntity>, IClientComponent
{
	// Token: 0x06000B01 RID: 2817 RVA: 0x0004A174 File Offset: 0x00048374
	public void SetOn(bool isOn)
	{
		if (this.spawnedEffect)
		{
			global::GameManager.Destroy(this.spawnedEffect, 0f);
		}
		this.spawnedEffect = null;
		if (isOn)
		{
			this.spawnedEffect = Object.Instantiate<GameObject>(this.effectPrefab);
			this.spawnedEffect.transform.rotation = this.mountBone.transform.rotation;
			this.spawnedEffect.transform.position = this.mountBone.transform.position;
			this.spawnedEffect.transform.parent = this.mountBone.transform;
			this.spawnedEffect.SetActive(true);
		}
	}

	// Token: 0x040005B0 RID: 1456
	public GameObject effectPrefab;

	// Token: 0x040005B1 RID: 1457
	public GameObject spawnedEffect;

	// Token: 0x040005B2 RID: 1458
	public GameObject mountBone;
}
