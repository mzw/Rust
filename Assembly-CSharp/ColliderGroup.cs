﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000293 RID: 659
public class ColliderGroup : Pool.IPooled
{
	// Token: 0x1700012D RID: 301
	// (get) Token: 0x06001108 RID: 4360 RVA: 0x00066034 File Offset: 0x00064234
	public float Size
	{
		get
		{
			return this.grid.CellSize;
		}
	}

	// Token: 0x1700012E RID: 302
	// (get) Token: 0x06001109 RID: 4361 RVA: 0x00066044 File Offset: 0x00064244
	public Vector3 Position
	{
		get
		{
			return this.cell.position;
		}
	}

	// Token: 0x1700012F RID: 303
	// (get) Token: 0x0600110A RID: 4362 RVA: 0x00066054 File Offset: 0x00064254
	public int Count
	{
		get
		{
			return this.Colliders.Count;
		}
	}

	// Token: 0x0600110B RID: 4363 RVA: 0x00066064 File Offset: 0x00064264
	public void Initialize(global::ColliderGrid grid, global::ColliderCell cell, global::ColliderKey key)
	{
		this.grid = grid;
		this.cell = cell;
		this.key = key;
	}

	// Token: 0x0600110C RID: 4364 RVA: 0x0006607C File Offset: 0x0006427C
	public void EnterPool()
	{
		this.Invalidated = false;
		this.NeedsRefresh = false;
		this.Processing = false;
		this.Preserving = false;
		this.Colliders.Clear();
		this.TempColliders.Clear();
		this.Batches.Clear();
		this.TempBatches.Clear();
		this.TempInstances.Clear();
		this.grid = null;
		this.cell = null;
		this.key = default(global::ColliderKey);
	}

	// Token: 0x0600110D RID: 4365 RVA: 0x000660FC File Offset: 0x000642FC
	public void LeavePool()
	{
	}

	// Token: 0x0600110E RID: 4366 RVA: 0x00066100 File Offset: 0x00064300
	public void Add(global::ColliderBatch collider)
	{
		this.Colliders.Add(collider);
		this.NeedsRefresh = true;
	}

	// Token: 0x0600110F RID: 4367 RVA: 0x00066118 File Offset: 0x00064318
	public void Remove(global::ColliderBatch collider)
	{
		this.Colliders.Remove(collider);
		this.NeedsRefresh = true;
	}

	// Token: 0x06001110 RID: 4368 RVA: 0x00066130 File Offset: 0x00064330
	public void Invalidate()
	{
		if (!this.Invalidated)
		{
			for (int i = 0; i < this.Batches.Count; i++)
			{
				this.Batches[i].Invalidate();
			}
			this.Invalidated = true;
		}
		this.cell.interrupt = true;
	}

	// Token: 0x06001111 RID: 4369 RVA: 0x00066188 File Offset: 0x00064388
	public void Add(global::MeshColliderInstance instance)
	{
		this.TempInstances.Add(instance);
	}

	// Token: 0x06001112 RID: 4370 RVA: 0x00066198 File Offset: 0x00064398
	public void UpdateData()
	{
		this.TempInstances.Clear();
		int num = 0;
		while (num < this.TempColliders.Count && !this.cell.interrupt)
		{
			this.TempColliders[num].AddBatch(this);
			num++;
		}
	}

	// Token: 0x06001113 RID: 4371 RVA: 0x000661F0 File Offset: 0x000643F0
	public void CreateBatches()
	{
		if (this.TempInstances.Count == 0)
		{
			return;
		}
		global::MeshColliderBatch meshColliderBatch = this.CreateBatch();
		for (int i = 0; i < this.TempInstances.Count; i++)
		{
			global::MeshColliderInstance instance = this.TempInstances[i];
			if (meshColliderBatch.AvailableVertices < instance.mesh.vertexCount)
			{
				meshColliderBatch = this.CreateBatch();
			}
			meshColliderBatch.Add(instance);
		}
		this.TempInstances.Clear();
	}

	// Token: 0x06001114 RID: 4372 RVA: 0x00066270 File Offset: 0x00064470
	public void RefreshBatches()
	{
		int num = 0;
		while (num < this.TempBatches.Count && !this.cell.interrupt)
		{
			this.TempBatches[num].Refresh();
			num++;
		}
	}

	// Token: 0x06001115 RID: 4373 RVA: 0x000662BC File Offset: 0x000644BC
	public void ApplyBatches()
	{
		int num = 0;
		while (num < this.TempBatches.Count && !this.cell.interrupt)
		{
			this.TempBatches[num].Apply();
			num++;
		}
	}

	// Token: 0x06001116 RID: 4374 RVA: 0x00066308 File Offset: 0x00064508
	public void DisplayBatches()
	{
		int num = 0;
		while (num < this.TempBatches.Count && !this.cell.interrupt)
		{
			this.TempBatches[num].Display();
			num++;
		}
	}

	// Token: 0x06001117 RID: 4375 RVA: 0x00066354 File Offset: 0x00064554
	public IEnumerator UpdateDataAsync()
	{
		if (this.updateData == null)
		{
			this.updateData = new Action(this.UpdateData);
		}
		return Parallel.Coroutine(this.updateData);
	}

	// Token: 0x06001118 RID: 4376 RVA: 0x00066380 File Offset: 0x00064580
	public IEnumerator RefreshBatchesAsync()
	{
		if (this.refreshBatches == null)
		{
			this.refreshBatches = new Action(this.RefreshBatches);
		}
		return Parallel.Coroutine(this.refreshBatches);
	}

	// Token: 0x06001119 RID: 4377 RVA: 0x000663AC File Offset: 0x000645AC
	public void Start()
	{
		if (this.NeedsRefresh)
		{
			this.Processing = true;
			this.TempColliders.Clear();
			this.TempColliders.AddRange(this.Colliders.Values);
			this.NeedsRefresh = false;
		}
		else
		{
			this.Preserving = true;
		}
	}

	// Token: 0x0600111A RID: 4378 RVA: 0x00066400 File Offset: 0x00064600
	public void End()
	{
		if (this.Processing)
		{
			if (!this.cell.interrupt)
			{
				this.Clear();
				for (int i = 0; i < this.TempBatches.Count; i++)
				{
					this.TempBatches[i].Free();
				}
				List<global::MeshColliderBatch> batches = this.Batches;
				this.Batches = this.TempBatches;
				this.TempBatches = batches;
				this.Invalidated = false;
			}
			else
			{
				this.Cancel();
			}
			this.TempColliders.Clear();
			this.Processing = false;
		}
		else
		{
			this.Preserving = false;
		}
	}

	// Token: 0x0600111B RID: 4379 RVA: 0x000664A8 File Offset: 0x000646A8
	public void Clear()
	{
		for (int i = 0; i < this.Batches.Count; i++)
		{
			this.grid.RecycleInstance(this.Batches[i]);
		}
		this.Batches.Clear();
	}

	// Token: 0x0600111C RID: 4380 RVA: 0x000664F4 File Offset: 0x000646F4
	public void Cancel()
	{
		for (int i = 0; i < this.TempBatches.Count; i++)
		{
			this.grid.RecycleInstance(this.TempBatches[i]);
		}
		this.TempBatches.Clear();
	}

	// Token: 0x0600111D RID: 4381 RVA: 0x00066540 File Offset: 0x00064740
	public int MeshCount()
	{
		int num = 0;
		for (int i = 0; i < this.Batches.Count; i++)
		{
			num += this.Batches[i].Count;
		}
		return num;
	}

	// Token: 0x0600111E RID: 4382 RVA: 0x00066580 File Offset: 0x00064780
	public int BatchedMeshCount()
	{
		int num = 0;
		for (int i = 0; i < this.Batches.Count; i++)
		{
			num += this.Batches[i].BatchedCount;
		}
		return num;
	}

	// Token: 0x0600111F RID: 4383 RVA: 0x000665C0 File Offset: 0x000647C0
	public global::MeshColliderBatch CreateBatch()
	{
		global::MeshColliderBatch meshColliderBatch = this.grid.CreateInstance();
		meshColliderBatch.Setup(this.cell.position, this.key.layer, this.key.material);
		meshColliderBatch.Alloc();
		this.TempBatches.Add(meshColliderBatch);
		return meshColliderBatch;
	}

	// Token: 0x04000C1F RID: 3103
	public bool Invalidated;

	// Token: 0x04000C20 RID: 3104
	public bool NeedsRefresh;

	// Token: 0x04000C21 RID: 3105
	public bool Processing;

	// Token: 0x04000C22 RID: 3106
	public bool Preserving;

	// Token: 0x04000C23 RID: 3107
	public ListHashSet<global::ColliderBatch> Colliders = new ListHashSet<global::ColliderBatch>(8);

	// Token: 0x04000C24 RID: 3108
	public List<global::ColliderBatch> TempColliders = new List<global::ColliderBatch>();

	// Token: 0x04000C25 RID: 3109
	public List<global::MeshColliderBatch> Batches = new List<global::MeshColliderBatch>();

	// Token: 0x04000C26 RID: 3110
	public List<global::MeshColliderBatch> TempBatches = new List<global::MeshColliderBatch>();

	// Token: 0x04000C27 RID: 3111
	public List<global::MeshColliderInstance> TempInstances = new List<global::MeshColliderInstance>();

	// Token: 0x04000C28 RID: 3112
	private global::ColliderGrid grid;

	// Token: 0x04000C29 RID: 3113
	private global::ColliderCell cell;

	// Token: 0x04000C2A RID: 3114
	private global::ColliderKey key;

	// Token: 0x04000C2B RID: 3115
	private Action updateData;

	// Token: 0x04000C2C RID: 3116
	private Action refreshBatches;
}
