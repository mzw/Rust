﻿using System;
using System.Collections.Generic;
using Facepunch.Math;
using Facepunch.Sqlite;
using ProtoBuf;
using UnityEngine;

// Token: 0x0200062D RID: 1581
public class UserPersistance : IDisposable
{
	// Token: 0x06002021 RID: 8225 RVA: 0x000B8018 File Offset: 0x000B6218
	public UserPersistance(string strFolder)
	{
		global::UserPersistance.blueprints = new Database();
		global::UserPersistance.blueprints.Open(string.Concat(new object[]
		{
			strFolder,
			"/player.blueprints.",
			2,
			".db"
		}));
		if (!global::UserPersistance.blueprints.TableExists("data"))
		{
			global::UserPersistance.blueprints.Execute("CREATE TABLE data ( userid TEXT PRIMARY KEY, info BLOB, updated INTEGER )", new object[0]);
		}
		global::UserPersistance.deaths = new Database();
		global::UserPersistance.deaths.Open(string.Concat(new object[]
		{
			strFolder,
			"/player.deaths.",
			2,
			".db"
		}));
		if (!global::UserPersistance.deaths.TableExists("data"))
		{
			global::UserPersistance.deaths.Execute("CREATE TABLE data ( userid TEXT, born INTEGER, died INTEGER, info BLOB )", new object[0]);
			global::UserPersistance.deaths.Execute("CREATE INDEX IF NOT EXISTS userindex ON data ( userid )", new object[0]);
			global::UserPersistance.deaths.Execute("CREATE INDEX IF NOT EXISTS diedindex ON data ( died )", new object[0]);
		}
	}

	// Token: 0x06002022 RID: 8226 RVA: 0x000B8120 File Offset: 0x000B6320
	public virtual void Dispose()
	{
		if (global::UserPersistance.blueprints != null)
		{
			global::UserPersistance.blueprints.Close();
			global::UserPersistance.blueprints = null;
		}
		if (global::UserPersistance.deaths != null)
		{
			global::UserPersistance.deaths.Close();
			global::UserPersistance.deaths = null;
		}
	}

	// Token: 0x06002023 RID: 8227 RVA: 0x000B8158 File Offset: 0x000B6358
	public PersistantPlayer GetPlayerInfo(ulong playerID)
	{
		PersistantPlayer persistantPlayer = this.FetchFromDatabase(playerID);
		if (persistantPlayer == null)
		{
			persistantPlayer = new PersistantPlayer();
		}
		persistantPlayer.ShouldPool = false;
		if (persistantPlayer.unlockedItems == null)
		{
			persistantPlayer.unlockedItems = new List<int>();
		}
		return persistantPlayer;
	}

	// Token: 0x06002024 RID: 8228 RVA: 0x000B8198 File Offset: 0x000B6398
	private PersistantPlayer FetchFromDatabase(ulong playerID)
	{
		try
		{
			Row row = global::UserPersistance.blueprints.QueryRow("SELECT info FROM data WHERE userid = ?", new object[]
			{
				playerID.ToString()
			});
			if (row != null)
			{
				byte[] blob = row.GetBlob("info");
				return PersistantPlayer.Deserialize(blob);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError("Error loading player blueprints: (" + ex.Message + ")");
		}
		return null;
	}

	// Token: 0x06002025 RID: 8229 RVA: 0x000B8224 File Offset: 0x000B6424
	public void SetPlayerInfo(ulong playerID, PersistantPlayer info)
	{
		using (TimeWarning.New("SetPlayerInfo", 0.1f))
		{
			byte[] array;
			using (TimeWarning.New("ToProtoBytes", 0.1f))
			{
				array = info.ToProtoBytes();
			}
			global::UserPersistance.blueprints.Execute("INSERT OR REPLACE INTO data ( userid, info, updated ) VALUES ( ?, ?, ? )", new object[]
			{
				playerID.ToString(),
				array,
				Epoch.Current
			});
		}
	}

	// Token: 0x06002026 RID: 8230 RVA: 0x000B82D0 File Offset: 0x000B64D0
	public void AddLifeStory(ulong playerID, PlayerLifeStory lifeStory)
	{
		if (global::UserPersistance.deaths == null)
		{
			return;
		}
		if (lifeStory == null)
		{
			return;
		}
		using (TimeWarning.New("AddLifeStory", 0.1f))
		{
			byte[] array;
			using (TimeWarning.New("ToProtoBytes", 0.1f))
			{
				array = lifeStory.ToProtoBytes();
			}
			global::UserPersistance.deaths.Execute("INSERT INTO data ( userid, born, died, info ) VALUES ( ?, ?, ?, ? )", new object[]
			{
				playerID.ToString(),
				(int)lifeStory.timeBorn,
				(int)lifeStory.timeDied,
				array
			});
		}
	}

	// Token: 0x06002027 RID: 8231 RVA: 0x000B839C File Offset: 0x000B659C
	public PlayerLifeStory GetLastLifeStory(ulong playerID)
	{
		if (global::UserPersistance.deaths == null)
		{
			return null;
		}
		PlayerLifeStory result;
		using (TimeWarning.New("GetLastLifeStory", 0.1f))
		{
			try
			{
				byte[] array = global::UserPersistance.deaths.QueryBlob("SELECT info FROM data WHERE userid = ? ORDER BY died DESC LIMIT 1", new object[]
				{
					playerID.ToString()
				});
				if (array == null)
				{
					return null;
				}
				PlayerLifeStory playerLifeStory = PlayerLifeStory.Deserialize(array);
				playerLifeStory.ShouldPool = false;
				return playerLifeStory;
			}
			catch (Exception ex)
			{
				Debug.LogError("Error loading lifestory from database: (" + ex.Message + ")");
			}
			result = null;
		}
		return result;
	}

	// Token: 0x04001AEC RID: 6892
	public static Database blueprints;

	// Token: 0x04001AED RID: 6893
	private static Database deaths;
}
