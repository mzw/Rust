﻿using System;
using UnityEngine;

// Token: 0x0200043D RID: 1085
public interface IPrefabProcessor
{
	// Token: 0x0600186F RID: 6255
	void RemoveComponent(Component component);

	// Token: 0x06001870 RID: 6256
	void NominateForDeletion(GameObject obj);
}
