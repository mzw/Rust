﻿using System;
using UnityEngine;

// Token: 0x020004E7 RID: 1255
public class ItemModCycle : global::ItemMod
{
	// Token: 0x06001ADA RID: 6874 RVA: 0x000966EC File Offset: 0x000948EC
	public override void OnItemCreated(global::Item itemcreated)
	{
		float timeTaken = this.timerStart;
		itemcreated.onCycle += delegate(global::Item item, float delta)
		{
			if (this.onlyAdvanceTimerWhenPass && !this.CanCycle(item))
			{
				return;
			}
			timeTaken += delta;
			if (timeTaken < this.timeBetweenCycles)
			{
				return;
			}
			timeTaken = 0f;
			if (!this.onlyAdvanceTimerWhenPass && !this.CanCycle(item))
			{
				return;
			}
			this.CustomCycle(item, delta);
		};
	}

	// Token: 0x06001ADB RID: 6875 RVA: 0x00096724 File Offset: 0x00094924
	private bool CanCycle(global::Item item)
	{
		foreach (global::ItemMod itemMod in this.actions)
		{
			if (!itemMod.CanDoAction(item, item.GetOwnerPlayer()))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06001ADC RID: 6876 RVA: 0x00096768 File Offset: 0x00094968
	public void CustomCycle(global::Item item, float delta)
	{
		global::BasePlayer ownerPlayer = item.GetOwnerPlayer();
		foreach (global::ItemMod itemMod in this.actions)
		{
			itemMod.DoAction(item, ownerPlayer);
		}
	}

	// Token: 0x06001ADD RID: 6877 RVA: 0x000967A4 File Offset: 0x000949A4
	private void OnValidate()
	{
		if (this.actions == null)
		{
			Debug.LogWarning("ItemModMenuOption: actions is null", base.gameObject);
		}
	}

	// Token: 0x040015A4 RID: 5540
	public global::ItemMod[] actions;

	// Token: 0x040015A5 RID: 5541
	public float timeBetweenCycles = 1f;

	// Token: 0x040015A6 RID: 5542
	public float timerStart;

	// Token: 0x040015A7 RID: 5543
	public bool onlyAdvanceTimerWhenPass;
}
