﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000051 RID: 81
public class BearTrap : global::BaseTrap
{
	// Token: 0x06000681 RID: 1665 RVA: 0x00027BAC File Offset: 0x00025DAC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BearTrap.OnRpcMessage", 0.1f))
		{
			if (rpc == 1397025026u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Arm ");
				}
				using (TimeWarning.New("RPC_Arm", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Arm", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Arm(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Arm");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000682 RID: 1666 RVA: 0x00027D8C File Offset: 0x00025F8C
	public bool Armed()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000683 RID: 1667 RVA: 0x00027D98 File Offset: 0x00025F98
	public override void InitShared()
	{
		this.animator = base.GetComponent<Animator>();
		base.InitShared();
	}

	// Token: 0x06000684 RID: 1668 RVA: 0x00027DAC File Offset: 0x00025FAC
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.CanPickup(player) && !this.Armed() && player.CanBuild();
	}

	// Token: 0x06000685 RID: 1669 RVA: 0x00027DD0 File Offset: 0x00025FD0
	public override void ServerInit()
	{
		base.ServerInit();
		this.Arm();
	}

	// Token: 0x06000686 RID: 1670 RVA: 0x00027DE0 File Offset: 0x00025FE0
	public override void Arm()
	{
		base.Arm();
		this.RadialResetCorpses(120f);
	}

	// Token: 0x06000687 RID: 1671 RVA: 0x00027DF4 File Offset: 0x00025FF4
	public void Fire()
	{
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000688 RID: 1672 RVA: 0x00027E08 File Offset: 0x00026008
	public override void ObjectEntered(GameObject obj)
	{
		if (!this.Armed())
		{
			return;
		}
		if (Interface.CallHook("OnTrapTrigger", new object[]
		{
			this,
			obj
		}) != null)
		{
			return;
		}
		this.hurtTarget = obj;
		base.Invoke(new Action(this.DelayedFire), 0.05f);
	}

	// Token: 0x06000689 RID: 1673 RVA: 0x00027E60 File Offset: 0x00026060
	public void DelayedFire()
	{
		if (this.hurtTarget)
		{
			global::BaseEntity baseEntity = this.hurtTarget.ToBaseEntity();
			if (baseEntity != null)
			{
				global::HitInfo hitInfo = new global::HitInfo(this, baseEntity, Rust.DamageType.Bite, 50f, base.transform.position);
				hitInfo.damageTypes.Add(Rust.DamageType.Stab, 30f);
				baseEntity.OnAttacked(hitInfo);
			}
			this.hurtTarget = null;
		}
		this.RadialResetCorpses(1800f);
		this.Fire();
		this.Hurt(25f);
	}

	// Token: 0x0600068A RID: 1674 RVA: 0x00027EEC File Offset: 0x000260EC
	public void RadialResetCorpses(float duration)
	{
		List<global::BaseCorpse> list = Facepunch.Pool.GetList<global::BaseCorpse>();
		global::Vis.Entities<global::BaseCorpse>(base.transform.position, 5f, list, 512, 2);
		foreach (global::BaseCorpse baseCorpse in list)
		{
			baseCorpse.ResetRemovalTime(duration);
		}
		Facepunch.Pool.FreeList<global::BaseCorpse>(ref list);
	}

	// Token: 0x0600068B RID: 1675 RVA: 0x00027F6C File Offset: 0x0002616C
	public override void OnAttacked(global::HitInfo info)
	{
		float num = info.damageTypes.Total();
		if ((info.damageTypes.IsMeleeType() && num > 20f) || num > 30f)
		{
			this.Fire();
		}
		base.OnAttacked(info);
	}

	// Token: 0x0600068C RID: 1676 RVA: 0x00027FB8 File Offset: 0x000261B8
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_Arm(global::BaseEntity.RPCMessage rpc)
	{
		if (this.Armed())
		{
			return;
		}
		if (Interface.CallHook("OnTrapArm", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		this.Arm();
	}

	// Token: 0x0600068D RID: 1677 RVA: 0x00027FFC File Offset: 0x000261FC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (!base.isServer && this.animator.isInitialized)
		{
			this.animator.SetBool("armed", this.Armed());
		}
	}

	// Token: 0x040002C8 RID: 712
	protected Animator animator;

	// Token: 0x040002C9 RID: 713
	private GameObject hurtTarget;
}
