﻿using System;
using UnityEngine;

// Token: 0x02000541 RID: 1345
public abstract class DecorComponent : global::PrefabAttribute
{
	// Token: 0x06001C80 RID: 7296
	public abstract void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale);

	// Token: 0x06001C81 RID: 7297 RVA: 0x0009FB44 File Offset: 0x0009DD44
	protected override Type GetIndexedType()
	{
		return typeof(global::DecorComponent);
	}
}
