﻿using System;
using ConVar;
using Facepunch;
using Network;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000067 RID: 103
public class DudTimedExplosive : global::TimedExplosive
{
	// Token: 0x060007A6 RID: 1958 RVA: 0x000319E4 File Offset: 0x0002FBE4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("DudTimedExplosive.OnRpcMessage", 0.1f))
		{
			if (rpc == 960385558u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Pickup ");
				}
				using (TimeWarning.New("RPC_Pickup", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Pickup", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Pickup(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Pickup");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060007A7 RID: 1959 RVA: 0x00031BC4 File Offset: 0x0002FDC4
	private bool IsWickBurning()
	{
		return base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x060007A8 RID: 1960 RVA: 0x00031BD0 File Offset: 0x0002FDD0
	public override float GetRandomTimerTime()
	{
		float randomTimerTime = base.GetRandomTimerTime();
		float num = 1f;
		float num2 = Random.Range(0f, 1f);
		if (num2 <= 0.15f)
		{
			num = 0.334f;
		}
		else if (Random.Range(0f, 1f) <= 0.15f)
		{
			num = 3f;
		}
		return randomTimerTime * num;
	}

	// Token: 0x060007A9 RID: 1961 RVA: 0x00031C34 File Offset: 0x0002FE34
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_Pickup(global::BaseEntity.RPCMessage msg)
	{
		if (this.IsWickBurning())
		{
			return;
		}
		global::BasePlayer player = msg.player;
		if (Random.Range(0f, 1f) >= 0.5f && base.HasParent())
		{
			this.SetFuse(Random.Range(2.5f, 3f));
		}
		else
		{
			player.GiveItem(global::ItemManager.Create(this.itemToGive, 1, 0UL), global::BaseEntity.GiveItemReason.Generic);
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
	}

	// Token: 0x060007AA RID: 1962 RVA: 0x00031CB0 File Offset: 0x0002FEB0
	public override void SetFuse(float fuseLength)
	{
		base.SetFuse(fuseLength);
		this.explodeTime = UnityEngine.Time.realtimeSinceStartup + fuseLength;
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		base.CancelInvoke(new Action(base.KillMessage));
	}

	// Token: 0x060007AB RID: 1963 RVA: 0x00031CE8 File Offset: 0x0002FEE8
	public override void Explode()
	{
		if (Random.Range(0f, 1f) < this.dudChance)
		{
			this.BecomeDud();
		}
		else
		{
			base.Explode();
		}
	}

	// Token: 0x060007AC RID: 1964 RVA: 0x00031D18 File Offset: 0x0002FF18
	public override bool CanStickTo(global::BaseEntity entity)
	{
		return base.CanStickTo(entity) && this.IsWickBurning();
	}

	// Token: 0x060007AD RID: 1965 RVA: 0x00031D30 File Offset: 0x0002FF30
	public virtual void BecomeDud()
	{
		Vector3 estimatedWorldPosition = base.GetEstimatedWorldPosition();
		Quaternion estimatedWorldRotation = base.GetEstimatedWorldRotation();
		bool flag = this.parentEntity.IsValid(base.isServer) && this.parentEntity.Get(base.isServer).syncPosition;
		if (flag)
		{
			base.SetParent(null, 0u);
		}
		base.transform.position = estimatedWorldPosition;
		base.transform.rotation = estimatedWorldRotation;
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		this.SetCollisionEnabled(true);
		if (flag)
		{
			this.SetMotionEnabled(true);
		}
		global::Effect.server.Run("assets/bundled/prefabs/fx/impacts/blunt/concrete/concrete1.prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		base.CancelInvoke(new Action(base.KillMessage));
		base.Invoke(new Action(base.KillMessage), 1200f);
	}

	// Token: 0x060007AE RID: 1966 RVA: 0x00031E08 File Offset: 0x00030008
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.dudExplosive = Facepunch.Pool.Get<DudExplosive>();
		info.msg.dudExplosive.fuseTimeLeft = this.explodeTime - UnityEngine.Time.realtimeSinceStartup;
	}

	// Token: 0x060007AF RID: 1967 RVA: 0x00031E40 File Offset: 0x00030040
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.dudExplosive != null)
		{
			this.explodeTime = UnityEngine.Time.realtimeSinceStartup + info.msg.dudExplosive.fuseTimeLeft;
		}
	}

	// Token: 0x0400037D RID: 893
	public global::GameObjectRef fizzleEffect;

	// Token: 0x0400037E RID: 894
	public GameObject wickSpark;

	// Token: 0x0400037F RID: 895
	public AudioSource wickSound;

	// Token: 0x04000380 RID: 896
	public float dudChance = 0.3f;

	// Token: 0x04000381 RID: 897
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition itemToGive;

	// Token: 0x04000382 RID: 898
	[NonSerialized]
	private float explodeTime;
}
