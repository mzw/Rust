﻿using System;
using UnityEngine;

// Token: 0x0200073D RID: 1853
public struct CachedTransform<T> where T : Component
{
	// Token: 0x060022CE RID: 8910 RVA: 0x000C1934 File Offset: 0x000BFB34
	public CachedTransform(T instance)
	{
		this.component = instance;
		if (this.component)
		{
			this.position = this.component.transform.position;
			this.rotation = this.component.transform.rotation;
			this.localScale = this.component.transform.localScale;
		}
		else
		{
			this.position = Vector3.zero;
			this.rotation = Quaternion.identity;
			this.localScale = Vector3.one;
		}
	}

	// Token: 0x060022CF RID: 8911 RVA: 0x000C19D8 File Offset: 0x000BFBD8
	public void Apply()
	{
		if (this.component)
		{
			this.component.transform.SetPositionAndRotation(this.position, this.rotation);
			this.component.transform.localScale = this.localScale;
		}
	}

	// Token: 0x060022D0 RID: 8912 RVA: 0x000C1A38 File Offset: 0x000BFC38
	public void RotateAround(Vector3 center, Vector3 axis, float angle)
	{
		Quaternion quaternion = Quaternion.AngleAxis(angle, axis);
		Vector3 vector = quaternion * (this.position - center);
		this.position = center + vector;
		this.rotation *= Quaternion.Inverse(this.rotation) * quaternion * this.rotation;
	}

	// Token: 0x17000273 RID: 627
	// (get) Token: 0x060022D1 RID: 8913 RVA: 0x000C1A9C File Offset: 0x000BFC9C
	public Matrix4x4 localToWorldMatrix
	{
		get
		{
			return Matrix4x4.TRS(this.position, this.rotation, this.localScale);
		}
	}

	// Token: 0x17000274 RID: 628
	// (get) Token: 0x060022D2 RID: 8914 RVA: 0x000C1AB8 File Offset: 0x000BFCB8
	public Matrix4x4 worldToLocalMatrix
	{
		get
		{
			return this.localToWorldMatrix.inverse;
		}
	}

	// Token: 0x17000275 RID: 629
	// (get) Token: 0x060022D3 RID: 8915 RVA: 0x000C1AD4 File Offset: 0x000BFCD4
	public Vector3 forward
	{
		get
		{
			return this.rotation * Vector3.forward;
		}
	}

	// Token: 0x17000276 RID: 630
	// (get) Token: 0x060022D4 RID: 8916 RVA: 0x000C1AE8 File Offset: 0x000BFCE8
	public Vector3 up
	{
		get
		{
			return this.rotation * Vector3.up;
		}
	}

	// Token: 0x17000277 RID: 631
	// (get) Token: 0x060022D5 RID: 8917 RVA: 0x000C1AFC File Offset: 0x000BFCFC
	public Vector3 right
	{
		get
		{
			return this.rotation * Vector3.right;
		}
	}

	// Token: 0x060022D6 RID: 8918 RVA: 0x000C1B10 File Offset: 0x000BFD10
	public static implicit operator bool(global::CachedTransform<T> instance)
	{
		return instance.component != null;
	}

	// Token: 0x04001F40 RID: 8000
	public T component;

	// Token: 0x04001F41 RID: 8001
	public Vector3 position;

	// Token: 0x04001F42 RID: 8002
	public Quaternion rotation;

	// Token: 0x04001F43 RID: 8003
	public Vector3 localScale;
}
