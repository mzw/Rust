﻿using System;
using Facepunch;
using ProtoBuf;
using UnityEngine;

// Token: 0x020003AD RID: 941
public class SphereEntity : global::BaseEntity
{
	// Token: 0x06001658 RID: 5720 RVA: 0x000816A8 File Offset: 0x0007F8A8
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.sphereEntity = Pool.Get<ProtoBuf.SphereEntity>();
		info.msg.sphereEntity.radius = this.currentRadius;
	}

	// Token: 0x06001659 RID: 5721 RVA: 0x000816DC File Offset: 0x0007F8DC
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (base.isServer)
		{
			if (info.msg.sphereEntity != null)
			{
				this.currentRadius = (this.lerpRadius = info.msg.sphereEntity.radius);
			}
			this.UpdateScale();
		}
	}

	// Token: 0x0600165A RID: 5722 RVA: 0x00081734 File Offset: 0x0007F934
	public void LerpRadiusTo(float radius, float speed)
	{
		this.lerpRadius = radius;
		this.lerpSpeed = speed;
	}

	// Token: 0x0600165B RID: 5723 RVA: 0x00081744 File Offset: 0x0007F944
	protected void UpdateScale()
	{
		base.transform.localScale = new Vector3(this.currentRadius, this.currentRadius, this.currentRadius);
	}

	// Token: 0x0600165C RID: 5724 RVA: 0x00081768 File Offset: 0x0007F968
	protected void Update()
	{
		if (this.currentRadius == this.lerpRadius)
		{
			return;
		}
		if (base.isServer)
		{
			this.currentRadius = Mathf.MoveTowards(this.currentRadius, this.lerpRadius, Time.deltaTime * this.lerpSpeed);
			this.UpdateScale();
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x040010D0 RID: 4304
	public float currentRadius = 1f;

	// Token: 0x040010D1 RID: 4305
	public float lerpRadius = 1f;

	// Token: 0x040010D2 RID: 4306
	public float lerpSpeed = 1f;
}
