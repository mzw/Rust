﻿using System;

// Token: 0x02000680 RID: 1664
public class UIDialog : ListComponent<global::UIDialog>
{
	// Token: 0x17000242 RID: 578
	// (get) Token: 0x060020E0 RID: 8416 RVA: 0x000BAB24 File Offset: 0x000B8D24
	public static bool isOpen
	{
		get
		{
			return ListComponent<global::UIDialog>.InstanceList.Count > 0;
		}
	}
}
