﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Token: 0x020005AF RID: 1455
public class GenerateRoadLayout : global::ProceduralComponent
{
	// Token: 0x06001E5D RID: 7773 RVA: 0x000A99D4 File Offset: 0x000A7BD4
	public override void Process(uint seed)
	{
		List<global::PathList> list = new List<global::PathList>();
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		List<global::MonumentInfo> monuments = global::TerrainMeta.Path.Monuments;
		if (monuments.Count == 0)
		{
			return;
		}
		int num = Mathf.NextPowerOfTwo((int)(global::World.Size / 10f));
		int[,] array = new int[num, num];
		float radius = 5f;
		for (int i = 0; i < num; i++)
		{
			float normZ = ((float)i + 0.5f) / (float)num;
			for (int j = 0; j < num; j++)
			{
				float normX = ((float)j + 0.5f) / (float)num;
				int num2 = SeedRandom.Range(ref seed, 100, 500);
				float slope = heightMap.GetSlope(normX, normZ);
				int topology = topologyMap.GetTopology(normX, normZ, radius);
				int num3 = 2295686;
				int num4 = 49152;
				if (slope > 20f || (topology & num3) != 0)
				{
					array[i, j] = int.MaxValue;
				}
				else if ((topology & num4) != 0)
				{
					array[i, j] = 2500;
				}
				else
				{
					array[i, j] = 1 + (int)(slope * slope * 10f) + num2;
				}
			}
		}
		global::PathFinder pathFinder = new global::PathFinder(array, true);
		List<global::GenerateRoadLayout.PathSegment> list2 = new List<global::GenerateRoadLayout.PathSegment>();
		List<global::GenerateRoadLayout.PathNode> list3 = new List<global::GenerateRoadLayout.PathNode>();
		List<global::GenerateRoadLayout.PathNode> list4 = new List<global::GenerateRoadLayout.PathNode>();
		List<global::PathFinder.Point> list5 = new List<global::PathFinder.Point>();
		List<global::PathFinder.Point> list6 = new List<global::PathFinder.Point>();
		List<global::PathFinder.Point> list7 = new List<global::PathFinder.Point>();
		foreach (global::MonumentInfo monumentInfo in monuments)
		{
			bool flag = list3.Count == 0;
			foreach (global::TerrainPathConnect terrainPathConnect in monumentInfo.GetTargets(global::InfrastructureType.Road))
			{
				global::PathFinder.Point point = terrainPathConnect.GetPoint(num);
				global::PathFinder.Node node = pathFinder.FindClosestWalkable(point, 100000);
				if (node != null)
				{
					global::GenerateRoadLayout.PathNode pathNode = new global::GenerateRoadLayout.PathNode();
					pathNode.monument = monumentInfo;
					pathNode.target = terrainPathConnect;
					pathNode.node = node;
					if (flag)
					{
						list3.Add(pathNode);
					}
					else
					{
						list4.Add(pathNode);
					}
				}
			}
		}
		while (list4.Count != 0)
		{
			list6.Clear();
			list7.Clear();
			list6.AddRange(from x in list3
			select x.node.point);
			list6.AddRange(list5);
			list7.AddRange(from x in list4
			select x.node.point);
			global::PathFinder.Node node2 = pathFinder.FindPathUndirected(list6, list7, 100000);
			if (node2 == null)
			{
				global::GenerateRoadLayout.PathNode copy = list4[0];
				list3.AddRange(from x in list4
				where x.monument == copy.monument
				select x);
				list4.RemoveAll((global::GenerateRoadLayout.PathNode x) => x.monument == copy.monument);
			}
			else
			{
				global::GenerateRoadLayout.PathSegment segment = new global::GenerateRoadLayout.PathSegment();
				for (global::PathFinder.Node node3 = node2; node3 != null; node3 = node3.next)
				{
					if (node3 == node2)
					{
						segment.start = node3;
					}
					if (node3.next == null)
					{
						segment.end = node3;
					}
				}
				list2.Add(segment);
				global::GenerateRoadLayout.PathNode copy = list4.Find((global::GenerateRoadLayout.PathNode x) => x.node.point == segment.start.point || x.node.point == segment.end.point);
				list3.AddRange(from x in list4
				where x.monument == copy.monument
				select x);
				list4.RemoveAll((global::GenerateRoadLayout.PathNode x) => x.monument == copy.monument);
				int num5 = 1;
				for (global::PathFinder.Node node4 = node2; node4 != null; node4 = node4.next)
				{
					if (num5 % 8 == 0)
					{
						list5.Add(node4.point);
					}
					num5++;
				}
			}
		}
		using (List<global::GenerateRoadLayout.PathNode>.Enumerator enumerator3 = list3.GetEnumerator())
		{
			while (enumerator3.MoveNext())
			{
				global::GenerateRoadLayout.PathNode target = enumerator3.Current;
				global::GenerateRoadLayout.PathSegment pathSegment = list2.Find((global::GenerateRoadLayout.PathSegment x) => x.start.point == target.node.point || x.end.point == target.node.point);
				if (pathSegment != null)
				{
					if (pathSegment.start.point == target.node.point)
					{
						global::PathFinder.Node node5 = target.node;
						global::PathFinder.Node start = pathFinder.Reverse(target.node);
						node5.next = pathSegment.start;
						pathSegment.start = start;
						pathSegment.origin = target.target;
					}
					else if (pathSegment.end.point == target.node.point)
					{
						pathSegment.end.next = target.node;
						pathSegment.end = pathFinder.FindEnd(target.node);
						pathSegment.target = target.target;
					}
				}
			}
		}
		List<Vector3> list8 = new List<Vector3>();
		foreach (global::GenerateRoadLayout.PathSegment pathSegment2 in list2)
		{
			bool start2 = false;
			bool end = false;
			for (global::PathFinder.Node node6 = pathSegment2.start; node6 != null; node6 = node6.next)
			{
				float normX2 = ((float)node6.point.x + 0.5f) / (float)num;
				float normZ2 = ((float)node6.point.y + 0.5f) / (float)num;
				if (pathSegment2.start == node6 && pathSegment2.origin != null)
				{
					start2 = true;
					normX2 = global::TerrainMeta.NormalizeX(pathSegment2.origin.transform.position.x);
					normZ2 = global::TerrainMeta.NormalizeZ(pathSegment2.origin.transform.position.z);
				}
				else if (pathSegment2.end == node6 && pathSegment2.target != null)
				{
					end = true;
					normX2 = global::TerrainMeta.NormalizeX(pathSegment2.target.transform.position.x);
					normZ2 = global::TerrainMeta.NormalizeZ(pathSegment2.target.transform.position.z);
				}
				float num6 = global::TerrainMeta.DenormalizeX(normX2);
				float num7 = global::TerrainMeta.DenormalizeZ(normZ2);
				float num8 = Mathf.Max(heightMap.GetHeight(normX2, normZ2), 1f);
				list8.Add(new Vector3(num6, num8, num7));
			}
			if (list8.Count != 0)
			{
				if (list8.Count >= 2)
				{
					list.Add(new global::PathList("Road " + list.Count, list8.ToArray())
					{
						Width = 10f,
						InnerPadding = 1f,
						OuterPadding = 1f,
						InnerFade = 1f,
						OuterFade = 8f,
						RandomScale = 0.75f,
						MeshOffset = -0f,
						TerrainOffset = -0.5f,
						Topology = 2048,
						Splat = 128,
						Start = start2,
						End = end
					});
				}
				list8.Clear();
			}
		}
		foreach (global::PathList pathList in list)
		{
			pathList.Path.Smoothen(2);
		}
		global::TerrainMeta.Path.Roads.AddRange(list);
	}

	// Token: 0x04001929 RID: 6441
	public const float Width = 10f;

	// Token: 0x0400192A RID: 6442
	public const float InnerPadding = 1f;

	// Token: 0x0400192B RID: 6443
	public const float OuterPadding = 1f;

	// Token: 0x0400192C RID: 6444
	public const float InnerFade = 1f;

	// Token: 0x0400192D RID: 6445
	public const float OuterFade = 8f;

	// Token: 0x0400192E RID: 6446
	public const float RandomScale = 0.75f;

	// Token: 0x0400192F RID: 6447
	public const float MeshOffset = -0f;

	// Token: 0x04001930 RID: 6448
	public const float TerrainOffset = -0.5f;

	// Token: 0x04001931 RID: 6449
	private const int MaxDepth = 100000;

	// Token: 0x020005B0 RID: 1456
	private class PathNode
	{
		// Token: 0x04001934 RID: 6452
		public global::MonumentInfo monument;

		// Token: 0x04001935 RID: 6453
		public global::TerrainPathConnect target;

		// Token: 0x04001936 RID: 6454
		public global::PathFinder.Node node;
	}

	// Token: 0x020005B1 RID: 1457
	private class PathSegment
	{
		// Token: 0x04001937 RID: 6455
		public global::PathFinder.Node start;

		// Token: 0x04001938 RID: 6456
		public global::PathFinder.Node end;

		// Token: 0x04001939 RID: 6457
		public global::TerrainPathConnect origin;

		// Token: 0x0400193A RID: 6458
		public global::TerrainPathConnect target;
	}
}
