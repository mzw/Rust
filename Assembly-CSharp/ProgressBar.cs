﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x020006F9 RID: 1785
public class ProgressBar : UIBehaviour
{
	// Token: 0x04001E6C RID: 7788
	public static global::ProgressBar Instance;

	// Token: 0x04001E6D RID: 7789
	private Action<global::BasePlayer> action;

	// Token: 0x04001E6E RID: 7790
	private float timeFinished;

	// Token: 0x04001E6F RID: 7791
	private float timeCounter;

	// Token: 0x04001E70 RID: 7792
	public GameObject scaleTarget;

	// Token: 0x04001E71 RID: 7793
	public Image progressField;

	// Token: 0x04001E72 RID: 7794
	public Image iconField;

	// Token: 0x04001E73 RID: 7795
	public Text leftField;

	// Token: 0x04001E74 RID: 7796
	public Text rightField;

	// Token: 0x04001E75 RID: 7797
	public AudioClip clipOpen;

	// Token: 0x04001E76 RID: 7798
	public AudioClip clipCancel;

	// Token: 0x04001E77 RID: 7799
	public bool IsOpen;
}
