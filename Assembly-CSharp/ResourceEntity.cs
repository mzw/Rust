﻿using System;
using Facepunch;
using ProtoBuf;
using UnityEngine.Serialization;

// Token: 0x020003D4 RID: 980
public class ResourceEntity : global::BaseEntity
{
	// Token: 0x060016D8 RID: 5848 RVA: 0x00083AA4 File Offset: 0x00081CA4
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.resource == null)
		{
			return;
		}
		this.health = info.msg.resource.health;
	}

	// Token: 0x060016D9 RID: 5849 RVA: 0x00083AD8 File Offset: 0x00081CD8
	public override void InitShared()
	{
		base.InitShared();
		if (base.isServer)
		{
			global::DecorComponent[] components = global::PrefabAttribute.server.FindAll<global::DecorComponent>(this.prefabID);
			base.transform.ApplyDecorComponentsScaleOnly(components);
		}
	}

	// Token: 0x060016DA RID: 5850 RVA: 0x00083B14 File Offset: 0x00081D14
	public override void ServerInit()
	{
		base.ServerInit();
		this.resourceDispenser = base.GetComponent<global::ResourceDispenser>();
		if (this.health == 0f)
		{
			this.health = this.startHealth;
		}
	}

	// Token: 0x060016DB RID: 5851 RVA: 0x00083B44 File Offset: 0x00081D44
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (info.forDisk)
		{
			info.msg.resource = Pool.Get<BaseResource>();
			info.msg.resource.health = this.Health();
		}
	}

	// Token: 0x060016DC RID: 5852 RVA: 0x00083B84 File Offset: 0x00081D84
	public override float MaxHealth()
	{
		return this.startHealth;
	}

	// Token: 0x060016DD RID: 5853 RVA: 0x00083B8C File Offset: 0x00081D8C
	public override float Health()
	{
		return this.health;
	}

	// Token: 0x060016DE RID: 5854 RVA: 0x00083B94 File Offset: 0x00081D94
	protected virtual void OnHealthChanged()
	{
	}

	// Token: 0x060016DF RID: 5855 RVA: 0x00083B98 File Offset: 0x00081D98
	public override void OnAttacked(global::HitInfo info)
	{
		if (base.isServer && !this.isKilled)
		{
			if (this.resourceDispenser != null)
			{
				this.resourceDispenser.OnAttacked(info);
			}
			if (!info.DidGather)
			{
				if (this.baseProtection)
				{
					this.baseProtection.Scale(info.damageTypes, 1f);
				}
				float num = info.damageTypes.Total();
				this.health -= num;
				if (this.health <= 0f)
				{
					this.OnKilled(info);
					return;
				}
				this.OnHealthChanged();
			}
		}
	}

	// Token: 0x060016E0 RID: 5856 RVA: 0x00083C44 File Offset: 0x00081E44
	public virtual void OnKilled(global::HitInfo info)
	{
		this.isKilled = true;
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x060016E1 RID: 5857 RVA: 0x00083C54 File Offset: 0x00081E54
	public override float BoundsPadding()
	{
		return 1f;
	}

	// Token: 0x060016E2 RID: 5858 RVA: 0x00083C5C File Offset: 0x00081E5C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x04001198 RID: 4504
	[FormerlySerializedAs("health")]
	public float startHealth;

	// Token: 0x04001199 RID: 4505
	[FormerlySerializedAs("protection")]
	public global::ProtectionProperties baseProtection;

	// Token: 0x0400119A RID: 4506
	protected float health;

	// Token: 0x0400119B RID: 4507
	internal global::ResourceDispenser resourceDispenser;

	// Token: 0x0400119C RID: 4508
	[NonSerialized]
	protected bool isKilled;
}
