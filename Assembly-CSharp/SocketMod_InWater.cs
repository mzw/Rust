﻿using System;
using UnityEngine;

// Token: 0x0200022B RID: 555
public class SocketMod_InWater : global::SocketMod
{
	// Token: 0x06000FF6 RID: 4086 RVA: 0x0006125C File Offset: 0x0005F45C
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.cyan;
		Gizmos.DrawSphere(Vector3.zero, 0.1f);
	}

	// Token: 0x06000FF7 RID: 4087 RVA: 0x00061288 File Offset: 0x0005F488
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 pos = place.position + place.rotation * this.worldPosition;
		bool flag = global::WaterLevel.Test(pos);
		if (flag == this.wantsInWater)
		{
			return true;
		}
		global::Construction.lastPlacementError = "Failed Check: InWater (" + this.hierachyName + ")";
		return false;
	}

	// Token: 0x04000AAD RID: 2733
	public bool wantsInWater = true;
}
