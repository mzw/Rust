﻿using System;

// Token: 0x0200039B RID: 923
public interface ISplashable
{
	// Token: 0x060015C6 RID: 5574
	bool wantsSplash(global::ItemDefinition splashType, int amount);

	// Token: 0x060015C7 RID: 5575
	int DoSplash(global::ItemDefinition splashType, int amount);
}
