﻿using System;
using UnityStandardAssets.ImageEffects;

// Token: 0x0200023F RID: 575
public class RadiationOverlay : ImageEffectLayer
{
	// Token: 0x04000AE8 RID: 2792
	public global::SoundDefinition[] geigerSounds;

	// Token: 0x04000AE9 RID: 2793
	private global::Sound sound;

	// Token: 0x04000AEA RID: 2794
	private ColorCorrectionCurves colourCorrection;

	// Token: 0x04000AEB RID: 2795
	private NoiseAndGrain noiseAndGrain;
}
