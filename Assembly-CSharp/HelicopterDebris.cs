﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x020003A3 RID: 931
public class HelicopterDebris : global::ServerGib
{
	// Token: 0x060015EA RID: 5610 RVA: 0x0007E874 File Offset: 0x0007CA74
	public override void ServerInit()
	{
		base.ServerInit();
		this.tooHotUntil = Time.realtimeSinceStartup + 480f;
	}

	// Token: 0x060015EB RID: 5611 RVA: 0x0007E890 File Offset: 0x0007CA90
	public override void PhysicsInit(Mesh mesh)
	{
		base.PhysicsInit(mesh);
		if (base.isServer)
		{
			this.resourceDispenser = base.GetComponent<global::ResourceDispenser>();
			Rigidbody component = base.GetComponent<Rigidbody>();
			float num = Mathf.Clamp01(component.mass / 5f);
			this.resourceDispenser.containedItems = new List<global::ItemAmount>();
			if (num > 0.75f)
			{
				this.resourceDispenser.containedItems.Add(new global::ItemAmount(this.hqMetal, 7f * num));
			}
			if (num > 0f)
			{
				this.resourceDispenser.containedItems.Add(new global::ItemAmount(this.metalFragments, 150f * num));
				this.resourceDispenser.containedItems.Add(new global::ItemAmount(this.charcoal, 80f * num));
			}
			this.resourceDispenser.Initialize();
		}
	}

	// Token: 0x060015EC RID: 5612 RVA: 0x0007E96C File Offset: 0x0007CB6C
	public bool IsTooHot()
	{
		return this.tooHotUntil > Time.realtimeSinceStartup;
	}

	// Token: 0x060015ED RID: 5613 RVA: 0x0007E97C File Offset: 0x0007CB7C
	public override void OnAttacked(global::HitInfo info)
	{
		if (this.IsTooHot() && info.WeaponPrefab is global::BaseMelee)
		{
			if (info.Initiator is global::BasePlayer)
			{
				global::HitInfo hitInfo = new global::HitInfo();
				hitInfo.damageTypes.Add(Rust.DamageType.Heat, 5f);
				hitInfo.DoHitEffects = true;
				hitInfo.DidHit = true;
				hitInfo.HitBone = 0u;
				hitInfo.Initiator = this;
				hitInfo.PointStart = base.transform.position;
				global::Effect.server.Run("assets/bundled/prefabs/fx/impacts/additive/fire.prefab", info.Initiator, 0u, new Vector3(0f, 1f, 0f), Vector3.up, null, false);
			}
		}
		else
		{
			if (this.resourceDispenser)
			{
				this.resourceDispenser.OnAttacked(info);
			}
			base.OnAttacked(info);
		}
	}

	// Token: 0x04001069 RID: 4201
	public global::ItemDefinition metalFragments;

	// Token: 0x0400106A RID: 4202
	public global::ItemDefinition hqMetal;

	// Token: 0x0400106B RID: 4203
	public global::ItemDefinition charcoal;

	// Token: 0x0400106C RID: 4204
	private global::ResourceDispenser resourceDispenser;

	// Token: 0x0400106D RID: 4205
	private float tooHotUntil;
}
