﻿using System;
using UnityEngine;

// Token: 0x02000310 RID: 784
public class DoorAnimEvents : MonoBehaviour, IClientComponent
{
	// Token: 0x1700016B RID: 363
	// (get) Token: 0x0600136D RID: 4973 RVA: 0x0007251C File Offset: 0x0007071C
	public Animator animator
	{
		get
		{
			return base.GetComponent<Animator>();
		}
	}

	// Token: 0x0600136E RID: 4974 RVA: 0x00072524 File Offset: 0x00070724
	private void DoorOpenStart()
	{
		if (!this.openStart.isValid)
		{
			return;
		}
		if (this.animator.IsInTransition(0))
		{
			return;
		}
		if (this.animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f)
		{
			return;
		}
		global::Effect.client.Run(this.openStart.resourcePath, base.gameObject);
	}

	// Token: 0x0600136F RID: 4975 RVA: 0x0007258C File Offset: 0x0007078C
	private void DoorOpenEnd()
	{
		if (!this.openEnd.isValid)
		{
			return;
		}
		if (this.animator.IsInTransition(0))
		{
			return;
		}
		if (this.animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f)
		{
			return;
		}
		global::Effect.client.Run(this.openEnd.resourcePath, base.gameObject);
	}

	// Token: 0x06001370 RID: 4976 RVA: 0x000725F4 File Offset: 0x000707F4
	private void DoorCloseStart()
	{
		if (!this.closeStart.isValid)
		{
			return;
		}
		if (this.animator.IsInTransition(0))
		{
			return;
		}
		if (this.animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f)
		{
			return;
		}
		global::Effect.client.Run(this.closeStart.resourcePath, base.gameObject);
	}

	// Token: 0x06001371 RID: 4977 RVA: 0x0007265C File Offset: 0x0007085C
	private void DoorCloseEnd()
	{
		if (!this.closeEnd.isValid)
		{
			return;
		}
		if (this.animator.IsInTransition(0))
		{
			return;
		}
		if (this.animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f)
		{
			return;
		}
		global::Effect.client.Run(this.closeEnd.resourcePath, base.gameObject);
	}

	// Token: 0x04000E24 RID: 3620
	public global::GameObjectRef openStart;

	// Token: 0x04000E25 RID: 3621
	public global::GameObjectRef openEnd;

	// Token: 0x04000E26 RID: 3622
	public global::GameObjectRef closeStart;

	// Token: 0x04000E27 RID: 3623
	public global::GameObjectRef closeEnd;
}
