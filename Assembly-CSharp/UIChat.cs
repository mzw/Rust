﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200066B RID: 1643
public class UIChat : SingletonComponent<global::UIChat>
{
	// Token: 0x04001C04 RID: 7172
	public GameObject inputArea;

	// Token: 0x04001C05 RID: 7173
	public GameObject chatArea;

	// Token: 0x04001C06 RID: 7174
	public InputField inputField;

	// Token: 0x04001C07 RID: 7175
	public ScrollRect scrollRect;

	// Token: 0x04001C08 RID: 7176
	public CanvasGroup canvasGroup;

	// Token: 0x04001C09 RID: 7177
	public global::GameObjectRef chatItemPlayer;

	// Token: 0x04001C0A RID: 7178
	public static bool isOpen;
}
