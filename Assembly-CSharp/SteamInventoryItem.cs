﻿using System;
using UnityEngine;

// Token: 0x0200065A RID: 1626
public class SteamInventoryItem : ScriptableObject
{
	// Token: 0x1700023F RID: 575
	// (get) Token: 0x06002092 RID: 8338 RVA: 0x000BA004 File Offset: 0x000B8204
	public global::ItemDefinition itemDefinition
	{
		get
		{
			return global::ItemManager.FindItemDefinition(this.itemname);
		}
	}

	// Token: 0x04001BC3 RID: 7107
	public int id;

	// Token: 0x04001BC4 RID: 7108
	public Sprite icon;

	// Token: 0x04001BC5 RID: 7109
	public global::Translate.Phrase displayName;

	// Token: 0x04001BC6 RID: 7110
	public global::Translate.Phrase displayDescription;

	// Token: 0x04001BC7 RID: 7111
	[Header("Steam Inventory")]
	public global::SteamInventoryItem.Category category;

	// Token: 0x04001BC8 RID: 7112
	public global::SteamInventoryItem.SubCategory subcategory;

	// Token: 0x04001BC9 RID: 7113
	public global::SteamInventoryCategory steamCategory;

	// Token: 0x04001BCA RID: 7114
	[Tooltip("Dtop this item being broken down into cloth etc")]
	public bool PreventBreakingDown;

	// Token: 0x04001BCB RID: 7115
	[Header("Meta")]
	public string itemname;

	// Token: 0x04001BCC RID: 7116
	public ulong workshopID;

	// Token: 0x0200065B RID: 1627
	public enum Category
	{
		// Token: 0x04001BCE RID: 7118
		None,
		// Token: 0x04001BCF RID: 7119
		Clothing,
		// Token: 0x04001BD0 RID: 7120
		Weapon,
		// Token: 0x04001BD1 RID: 7121
		Decoration,
		// Token: 0x04001BD2 RID: 7122
		Crate,
		// Token: 0x04001BD3 RID: 7123
		Resource
	}

	// Token: 0x0200065C RID: 1628
	public enum SubCategory
	{
		// Token: 0x04001BD5 RID: 7125
		None,
		// Token: 0x04001BD6 RID: 7126
		Shirt,
		// Token: 0x04001BD7 RID: 7127
		Pants,
		// Token: 0x04001BD8 RID: 7128
		Jacket,
		// Token: 0x04001BD9 RID: 7129
		Hat,
		// Token: 0x04001BDA RID: 7130
		Mask,
		// Token: 0x04001BDB RID: 7131
		Footwear,
		// Token: 0x04001BDC RID: 7132
		Weapon,
		// Token: 0x04001BDD RID: 7133
		Misc,
		// Token: 0x04001BDE RID: 7134
		Crate,
		// Token: 0x04001BDF RID: 7135
		Resource
	}
}
