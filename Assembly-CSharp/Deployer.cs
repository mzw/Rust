﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000063 RID: 99
public class Deployer : global::HeldEntity
{
	// Token: 0x06000773 RID: 1907 RVA: 0x0002FCA4 File Offset: 0x0002DEA4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Deployer.OnRpcMessage", 0.1f))
		{
			if (rpc == 875669810u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoDeploy ");
				}
				using (TimeWarning.New("DoDeploy", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoDeploy", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoDeploy(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoDeploy");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000774 RID: 1908 RVA: 0x0002FE80 File Offset: 0x0002E080
	public global::ItemModDeployable GetModDeployable()
	{
		global::ItemDefinition ownerItemDefinition = base.GetOwnerItemDefinition();
		if (ownerItemDefinition == null)
		{
			return null;
		}
		return ownerItemDefinition.GetComponent<global::ItemModDeployable>();
	}

	// Token: 0x06000775 RID: 1909 RVA: 0x0002FEA8 File Offset: 0x0002E0A8
	public global::Deployable GetDeployable()
	{
		global::ItemModDeployable modDeployable = this.GetModDeployable();
		if (modDeployable == null)
		{
			return null;
		}
		return modDeployable.GetDeployable(this);
	}

	// Token: 0x06000776 RID: 1910 RVA: 0x0002FED4 File Offset: 0x0002E0D4
	public Quaternion GetDeployedRotation(Vector3 normal, Vector3 placeDir)
	{
		return Quaternion.LookRotation(normal, placeDir) * Quaternion.Euler(90f, 0f, 0f);
	}

	// Token: 0x06000777 RID: 1911 RVA: 0x0002FEF8 File Offset: 0x0002E0F8
	public bool IsPlacementAngleAcceptable(Vector3 pos, Quaternion rot)
	{
		Vector3 vector = rot * Vector3.up;
		float num = Vector3.Dot(vector, Vector3.up);
		float num2 = Mathf.Acos(num);
		return num2 <= 0.610865235f;
	}

	// Token: 0x06000778 RID: 1912 RVA: 0x0002FF34 File Offset: 0x0002E134
	public bool CheckPlacement(global::Deployable deployable, Ray ray, float fDistance)
	{
		using (TimeWarning.New("Deploy.CheckPlacement", 0.1f))
		{
			RaycastHit raycastHit;
			if (!UnityEngine.Physics.Raycast(ray, ref raycastHit, fDistance, 1101070337))
			{
				return false;
			}
			global::DeployVolume[] volumes = global::PrefabAttribute.server.FindAll<global::DeployVolume>(deployable.prefabID);
			Vector3 point = raycastHit.point;
			Quaternion deployedRotation = this.GetDeployedRotation(raycastHit.normal, ray.direction);
			if (global::DeployVolume.Check(point, deployedRotation, volumes, -1))
			{
				return false;
			}
			if (!this.IsPlacementAngleAcceptable(raycastHit.point, deployedRotation))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000779 RID: 1913 RVA: 0x0002FFF4 File Offset: 0x0002E1F4
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void DoDeploy(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		global::Deployable deployable = this.GetDeployable();
		if (deployable == null)
		{
			return;
		}
		Ray ray = msg.read.Ray();
		uint entityID = msg.read.UInt32();
		if (deployable.toSlot)
		{
			this.DoDeploy_Slot(deployable, ray, entityID);
			return;
		}
		this.DoDeploy_Regular(deployable, ray);
	}

	// Token: 0x0600077A RID: 1914 RVA: 0x00030060 File Offset: 0x0002E260
	public void DoDeploy_Slot(global::Deployable deployable, Ray ray, uint entityID)
	{
		if (!base.HasItemAmount())
		{
			return;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		if (!ownerPlayer.CanBuild())
		{
			return;
		}
		global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find(entityID) as global::BaseEntity;
		if (baseEntity == null)
		{
			return;
		}
		if (!baseEntity.HasSlot(deployable.slot))
		{
			return;
		}
		if (baseEntity.GetSlot(deployable.slot) != null)
		{
			return;
		}
		global::ItemModDeployable modDeployable = this.GetModDeployable();
		global::BaseEntity baseEntity2 = global::GameManager.server.CreateEntity(modDeployable.entityPrefab.resourcePath, default(Vector3), default(Quaternion), true);
		if (baseEntity2 != null)
		{
			baseEntity2.SetParent(baseEntity, baseEntity.GetSlotAnchorName(deployable.slot));
			baseEntity2.OwnerID = ownerPlayer.userID;
			baseEntity2.OnDeployed(baseEntity);
			baseEntity2.Spawn();
			baseEntity.SetSlot(deployable.slot, baseEntity2);
			if (deployable.placeEffect.isValid)
			{
				global::Effect.server.Run(deployable.placeEffect.resourcePath, baseEntity.transform.position, Vector3.up, null, false);
			}
		}
		modDeployable.OnDeployed(baseEntity2, ownerPlayer);
		Interface.CallHook("OnItemDeployed", new object[]
		{
			this,
			baseEntity
		});
		base.UseItemAmount(1);
	}

	// Token: 0x0600077B RID: 1915 RVA: 0x000301B8 File Offset: 0x0002E3B8
	public void DoDeploy_Regular(global::Deployable deployable, Ray ray)
	{
		if (!base.HasItemAmount())
		{
			return;
		}
		global::BasePlayer ownerPlayer = base.GetOwnerPlayer();
		if (!ownerPlayer)
		{
			return;
		}
		if (!ownerPlayer.CanBuild())
		{
			ownerPlayer.ChatMessage("Building is blocked!");
			return;
		}
		if (ConVar.AntiHack.objectplacement && ownerPlayer.TriggeredAntiHack(1f, float.PositiveInfinity))
		{
			ownerPlayer.ChatMessage("AntiHack!");
			return;
		}
		if (!this.CheckPlacement(deployable, ray, 8f))
		{
			return;
		}
		RaycastHit raycastHit;
		if (!UnityEngine.Physics.Raycast(ray, ref raycastHit, 8f, 1101070337))
		{
			return;
		}
		Quaternion deployedRotation = this.GetDeployedRotation(raycastHit.normal, ray.direction);
		global::Item ownerItem = base.GetOwnerItem();
		global::ItemModDeployable modDeployable = this.GetModDeployable();
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(modDeployable.entityPrefab.resourcePath, raycastHit.point, deployedRotation, true);
		if (!baseEntity)
		{
			Debug.LogWarning("Couldn't create prefab:" + modDeployable.entityPrefab.resourcePath);
			return;
		}
		baseEntity.skinID = ownerItem.skin;
		baseEntity.SendMessage("SetDeployedBy", ownerPlayer, 1);
		baseEntity.OwnerID = ownerPlayer.userID;
		baseEntity.Spawn();
		modDeployable.OnDeployed(baseEntity, ownerPlayer);
		Interface.CallHook("OnItemDeployed", new object[]
		{
			this,
			baseEntity
		});
		base.UseItemAmount(1);
	}
}
