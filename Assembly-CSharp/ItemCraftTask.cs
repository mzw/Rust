﻿using System;
using System.Collections.Generic;
using ProtoBuf;

// Token: 0x020004BA RID: 1210
public class ItemCraftTask
{
	// Token: 0x040014BB RID: 5307
	public global::ItemBlueprint blueprint;

	// Token: 0x040014BC RID: 5308
	public float endTime;

	// Token: 0x040014BD RID: 5309
	public int taskUID;

	// Token: 0x040014BE RID: 5310
	public global::BasePlayer owner;

	// Token: 0x040014BF RID: 5311
	public bool cancelled;

	// Token: 0x040014C0 RID: 5312
	public ProtoBuf.Item.InstanceData instanceData;

	// Token: 0x040014C1 RID: 5313
	public int amount = 1;

	// Token: 0x040014C2 RID: 5314
	public int skinID;

	// Token: 0x040014C3 RID: 5315
	public List<ulong> potentialOwners;

	// Token: 0x040014C4 RID: 5316
	public List<global::Item> takenItems;

	// Token: 0x040014C5 RID: 5317
	public int numCrafted;

	// Token: 0x040014C6 RID: 5318
	public float conditionScale = 1f;

	// Token: 0x040014C7 RID: 5319
	public float workSecondsComplete;

	// Token: 0x040014C8 RID: 5320
	public float worksecondsRequired;
}
