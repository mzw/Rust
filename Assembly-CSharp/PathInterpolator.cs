﻿using System;
using UnityEngine;

// Token: 0x0200079F RID: 1951
public class PathInterpolator
{
	// Token: 0x0600244D RID: 9293 RVA: 0x000C8244 File Offset: 0x000C6444
	public PathInterpolator(Vector3[] points)
	{
		if (points.Length < 2)
		{
			throw new ArgumentException("Point list too short.");
		}
		this.Points = points;
		this.MinIndex = this.DefaultMinIndex;
		this.MaxIndex = this.DefaultMaxIndex;
	}

	// Token: 0x1700028B RID: 651
	// (get) Token: 0x0600244E RID: 9294 RVA: 0x000C8280 File Offset: 0x000C6480
	// (set) Token: 0x0600244F RID: 9295 RVA: 0x000C8288 File Offset: 0x000C6488
	public int MinIndex { get; set; }

	// Token: 0x1700028C RID: 652
	// (get) Token: 0x06002450 RID: 9296 RVA: 0x000C8294 File Offset: 0x000C6494
	// (set) Token: 0x06002451 RID: 9297 RVA: 0x000C829C File Offset: 0x000C649C
	public int MaxIndex { get; set; }

	// Token: 0x1700028D RID: 653
	// (get) Token: 0x06002452 RID: 9298 RVA: 0x000C82A8 File Offset: 0x000C64A8
	// (set) Token: 0x06002453 RID: 9299 RVA: 0x000C82B0 File Offset: 0x000C64B0
	public float Length { get; private set; }

	// Token: 0x1700028E RID: 654
	// (get) Token: 0x06002454 RID: 9300 RVA: 0x000C82BC File Offset: 0x000C64BC
	// (set) Token: 0x06002455 RID: 9301 RVA: 0x000C82C4 File Offset: 0x000C64C4
	public float StepSize { get; private set; }

	// Token: 0x1700028F RID: 655
	// (get) Token: 0x06002456 RID: 9302 RVA: 0x000C82D0 File Offset: 0x000C64D0
	public int DefaultMinIndex
	{
		get
		{
			return 0;
		}
	}

	// Token: 0x17000290 RID: 656
	// (get) Token: 0x06002457 RID: 9303 RVA: 0x000C82D4 File Offset: 0x000C64D4
	public int DefaultMaxIndex
	{
		get
		{
			return this.Points.Length - 1;
		}
	}

	// Token: 0x17000291 RID: 657
	// (get) Token: 0x06002458 RID: 9304 RVA: 0x000C82E0 File Offset: 0x000C64E0
	public float StartOffset
	{
		get
		{
			return this.Length * (float)(this.MinIndex - this.DefaultMinIndex) / (float)(this.DefaultMaxIndex - this.DefaultMinIndex);
		}
	}

	// Token: 0x17000292 RID: 658
	// (get) Token: 0x06002459 RID: 9305 RVA: 0x000C8308 File Offset: 0x000C6508
	public float EndOffset
	{
		get
		{
			return this.Length * (float)(this.DefaultMaxIndex - this.MaxIndex) / (float)(this.DefaultMaxIndex - this.DefaultMinIndex);
		}
	}

	// Token: 0x0600245A RID: 9306 RVA: 0x000C8330 File Offset: 0x000C6530
	public void RecalculateTangents()
	{
		if (this.Tangents == null || this.Tangents.Length != this.Points.Length)
		{
			this.Tangents = new Vector3[this.Points.Length];
		}
		float num = 0f;
		for (int i = 0; i < this.Tangents.Length; i++)
		{
			Vector3 vector = this.Points[Mathf.Max(i - 1, 0)];
			Vector3 vector2 = this.Points[Mathf.Min(i + 1, this.Tangents.Length - 1)];
			Vector3 vector3 = vector2 - vector;
			float magnitude = vector3.magnitude;
			num += magnitude;
			this.Tangents[i] = vector3 / magnitude;
		}
		this.Length = num;
		this.StepSize = num / (float)this.Points.Length;
		this.initialized = true;
	}

	// Token: 0x0600245B RID: 9307 RVA: 0x000C841C File Offset: 0x000C661C
	public void Resample(float distance)
	{
		if (!this.initialized)
		{
			throw new Exception("Tangents have not been calculated yet or are outdated.");
		}
		Vector3[] array = new Vector3[Mathf.RoundToInt(this.Length / distance)];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = this.GetPointCubicHermite((float)i * distance);
		}
		this.Points = array;
		this.initialized = false;
	}

	// Token: 0x0600245C RID: 9308 RVA: 0x000C848C File Offset: 0x000C668C
	public void Smoothen(int iterations = 1)
	{
		float num = 0.25f;
		for (int i = 0; i < iterations; i++)
		{
			Vector3 vector = this.Points[0];
			for (int j = 1; j < this.Points.Length - 1; j++)
			{
				Vector3 vector2 = this.Points[j];
				Vector3 vector3 = this.Points[j + 1];
				this.Points[j] = (vector + vector2 + vector2 + vector3) * num;
				vector = vector2;
			}
		}
		this.initialized = false;
	}

	// Token: 0x0600245D RID: 9309 RVA: 0x000C8540 File Offset: 0x000C6740
	public Vector3 GetStartPoint()
	{
		return this.Points[this.MinIndex];
	}

	// Token: 0x0600245E RID: 9310 RVA: 0x000C8558 File Offset: 0x000C6758
	public Vector3 GetEndPoint()
	{
		return this.Points[this.MaxIndex];
	}

	// Token: 0x0600245F RID: 9311 RVA: 0x000C8570 File Offset: 0x000C6770
	public Vector3 GetStartTangent()
	{
		if (!this.initialized)
		{
			throw new Exception("Tangents have not been calculated yet or are outdated.");
		}
		return this.Tangents[this.MinIndex];
	}

	// Token: 0x06002460 RID: 9312 RVA: 0x000C85A0 File Offset: 0x000C67A0
	public Vector3 GetEndTangent()
	{
		if (!this.initialized)
		{
			throw new Exception("Tangents have not been calculated yet or are outdated.");
		}
		return this.Tangents[this.MaxIndex];
	}

	// Token: 0x06002461 RID: 9313 RVA: 0x000C85D0 File Offset: 0x000C67D0
	public Vector3 GetPoint(float distance)
	{
		float num = distance / this.Length * (float)this.Points.Length;
		int num2 = (int)num;
		if (num <= (float)this.MinIndex)
		{
			return this.GetStartPoint();
		}
		if (num >= (float)this.MaxIndex)
		{
			return this.GetEndPoint();
		}
		Vector3 vector = this.Points[num2];
		Vector3 vector2 = this.Points[num2 + 1];
		float num3 = num - (float)num2;
		return Vector3.Lerp(vector, vector2, num3);
	}

	// Token: 0x06002462 RID: 9314 RVA: 0x000C8650 File Offset: 0x000C6850
	public Vector3 GetTangent(float distance)
	{
		if (!this.initialized)
		{
			throw new Exception("Tangents have not been calculated yet or are outdated.");
		}
		float num = distance / this.Length * (float)this.Tangents.Length;
		int num2 = (int)num;
		if (num <= (float)this.MinIndex)
		{
			return this.GetStartTangent();
		}
		if (num >= (float)this.MaxIndex)
		{
			return this.GetEndTangent();
		}
		Vector3 vector = this.Tangents[num2];
		Vector3 vector2 = this.Tangents[num2 + 1];
		float num3 = num - (float)num2;
		return Vector3.Lerp(vector, vector2, num3);
	}

	// Token: 0x06002463 RID: 9315 RVA: 0x000C86E8 File Offset: 0x000C68E8
	public Vector3 GetPointCubicHermite(float distance)
	{
		if (!this.initialized)
		{
			throw new Exception("Tangents have not been calculated yet or are outdated.");
		}
		float num = distance / this.Length * (float)this.Points.Length;
		int num2 = (int)num;
		if (num <= (float)this.MinIndex)
		{
			return this.GetStartPoint();
		}
		if (num >= (float)this.MaxIndex)
		{
			return this.GetEndPoint();
		}
		Vector3 vector = this.Points[num2];
		Vector3 vector2 = this.Points[num2 + 1];
		Vector3 vector3 = this.Tangents[num2] * this.StepSize;
		Vector3 vector4 = this.Tangents[num2 + 1] * this.StepSize;
		float num3 = num - (float)num2;
		float num4 = num3 * num3;
		float num5 = num3 * num4;
		return (2f * num5 - 3f * num4 + 1f) * vector + (num5 - 2f * num4 + num3) * vector3 + (-2f * num5 + 3f * num4) * vector2 + (num5 - num4) * vector4;
	}

	// Token: 0x04001FD7 RID: 8151
	public Vector3[] Points;

	// Token: 0x04001FD8 RID: 8152
	public Vector3[] Tangents;

	// Token: 0x04001FDD RID: 8157
	private bool initialized;
}
