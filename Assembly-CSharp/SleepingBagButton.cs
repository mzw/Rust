﻿using System;
using ProtoBuf;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000700 RID: 1792
public class SleepingBagButton : MonoBehaviour
{
	// Token: 0x1700026B RID: 619
	// (get) Token: 0x0600222E RID: 8750 RVA: 0x000C00C8 File Offset: 0x000BE2C8
	public float timerSeconds
	{
		get
		{
			return Mathf.Clamp(this.releaseTime - Time.realtimeSinceStartup, 0f, 216000f);
		}
	}

	// Token: 0x1700026C RID: 620
	// (get) Token: 0x0600222F RID: 8751 RVA: 0x000C00E8 File Offset: 0x000BE2E8
	public string friendlyName
	{
		get
		{
			if (this.spawnOptions == null || string.IsNullOrEmpty(this.spawnOptions.name))
			{
				return "Null Sleeping Bag";
			}
			return this.spawnOptions.name;
		}
	}

	// Token: 0x06002230 RID: 8752 RVA: 0x000C011C File Offset: 0x000BE31C
	public void Setup(RespawnInformation.SpawnOptions options)
	{
		this.button = base.GetComponent<Button>();
		this.spawnOptions = options;
		if (options.unlockSeconds > 0f)
		{
			this.button.interactable = false;
			this.timerInfo.SetActive(true);
			this.releaseTime = Time.realtimeSinceStartup + options.unlockSeconds;
		}
		else
		{
			this.button.interactable = true;
			this.timerInfo.SetActive(false);
			this.releaseTime = 0f;
		}
		this.BagName.text = this.friendlyName;
	}

	// Token: 0x06002231 RID: 8753 RVA: 0x000C01B0 File Offset: 0x000BE3B0
	public void Update()
	{
		if (this.releaseTime == 0f)
		{
			return;
		}
		if (this.releaseTime < Time.realtimeSinceStartup)
		{
			this.releaseTime = 0f;
			this.timerInfo.SetActive(false);
			this.button.interactable = true;
		}
		else
		{
			this.LockTime.text = this.timerSeconds.ToString("N0");
		}
	}

	// Token: 0x06002232 RID: 8754 RVA: 0x000C0224 File Offset: 0x000BE424
	public void DoSpawn()
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Client, "respawn_sleepingbag", new object[]
		{
			this.spawnOptions.id
		});
	}

	// Token: 0x06002233 RID: 8755 RVA: 0x000C0250 File Offset: 0x000BE450
	public void DeleteBag()
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Client, "respawn_sleepingbag_remove", new object[]
		{
			this.spawnOptions.id
		});
	}

	// Token: 0x04001EAB RID: 7851
	public GameObject timerInfo;

	// Token: 0x04001EAC RID: 7852
	public Text BagName;

	// Token: 0x04001EAD RID: 7853
	public Text LockTime;

	// Token: 0x04001EAE RID: 7854
	internal Button button;

	// Token: 0x04001EAF RID: 7855
	internal RespawnInformation.SpawnOptions spawnOptions;

	// Token: 0x04001EB0 RID: 7856
	internal float releaseTime;
}
