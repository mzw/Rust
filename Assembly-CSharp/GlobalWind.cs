﻿using System;
using UnityEngine;

// Token: 0x0200060B RID: 1547
[RequireComponent(typeof(WindZone))]
[ExecuteInEditMode]
public class GlobalWind : MonoBehaviour
{
	// Token: 0x06001F64 RID: 8036 RVA: 0x000B225C File Offset: 0x000B045C
	private void Start()
	{
		this.windZone = base.GetComponent<WindZone>();
	}

	// Token: 0x06001F65 RID: 8037 RVA: 0x000B226C File Offset: 0x000B046C
	private void Update()
	{
		if (this.windZone.mode == null)
		{
			Vector3 forward = base.transform.forward;
			Shader.SetGlobalVector("global_Wind", new Vector4(forward.x, forward.y, forward.z, this.windZone.windMain));
		}
		else
		{
			Shader.SetGlobalVector("global_Wind", Vector3.zero);
		}
	}

	// Token: 0x04001A60 RID: 6752
	private WindZone windZone;
}
