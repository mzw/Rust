﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ConVar;
using Facepunch.Extend;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000626 RID: 1574
public static class ServerUsers
{
	// Token: 0x06002008 RID: 8200 RVA: 0x000B744C File Offset: 0x000B564C
	public static void Remove(ulong uid)
	{
		Interface.CallHook("IOnServerUsersRemove", new object[]
		{
			uid
		});
		if (!global::ServerUsers.users.ContainsKey(uid))
		{
			return;
		}
		global::ServerUsers.users.Remove(uid);
	}

	// Token: 0x06002009 RID: 8201 RVA: 0x000B7494 File Offset: 0x000B5694
	public static void Set(ulong uid, global::ServerUsers.UserGroup group, string username, string notes)
	{
		Interface.CallHook("IOnServerUsersSet", new object[]
		{
			uid,
			group,
			username,
			notes
		});
		global::ServerUsers.Remove(uid);
		global::ServerUsers.User value = new global::ServerUsers.User
		{
			steamid = uid,
			group = group,
			username = username,
			notes = notes
		};
		global::ServerUsers.users.Add(uid, value);
	}

	// Token: 0x0600200A RID: 8202 RVA: 0x000B7504 File Offset: 0x000B5704
	public static global::ServerUsers.User Get(ulong uid)
	{
		global::ServerUsers.User result = null;
		if (global::ServerUsers.users.TryGetValue(uid, out result))
		{
			return result;
		}
		return null;
	}

	// Token: 0x0600200B RID: 8203 RVA: 0x000B7528 File Offset: 0x000B5728
	public static bool Is(ulong uid, global::ServerUsers.UserGroup group)
	{
		global::ServerUsers.User user = global::ServerUsers.Get(uid);
		return user != null && user.group == group;
	}

	// Token: 0x0600200C RID: 8204 RVA: 0x000B7550 File Offset: 0x000B5750
	public static IEnumerable<global::ServerUsers.User> GetAll(global::ServerUsers.UserGroup group)
	{
		return from x in global::ServerUsers.users
		where x.Value.@group == @group
		select x.Value;
	}

	// Token: 0x0600200D RID: 8205 RVA: 0x000B75A4 File Offset: 0x000B57A4
	public static void Clear()
	{
		global::ServerUsers.users.Clear();
	}

	// Token: 0x0600200E RID: 8206 RVA: 0x000B75B0 File Offset: 0x000B57B0
	public static void Load()
	{
		global::ServerUsers.Clear();
		string serverFolder = ConVar.Server.GetServerFolder("cfg");
		if (File.Exists(serverFolder + "/bans.cfg"))
		{
			string text = File.ReadAllText(serverFolder + "/bans.cfg");
			if (!string.IsNullOrEmpty(text))
			{
				Debug.Log("Running " + serverFolder + "/bans.cfg");
				ConsoleSystem.RunFile(ConsoleSystem.Option.Server.Quiet(), text);
			}
		}
		if (File.Exists(serverFolder + "/users.cfg"))
		{
			string text2 = File.ReadAllText(serverFolder + "/users.cfg");
			if (!string.IsNullOrEmpty(text2))
			{
				Debug.Log("Running " + serverFolder + "/users.cfg");
				ConsoleSystem.RunFile(ConsoleSystem.Option.Server.Quiet(), text2);
			}
		}
	}

	// Token: 0x0600200F RID: 8207 RVA: 0x000B7680 File Offset: 0x000B5880
	public static void Save()
	{
		string serverFolder = ConVar.Server.GetServerFolder("cfg");
		string text = string.Empty;
		IEnumerable<global::ServerUsers.User> all = global::ServerUsers.GetAll(global::ServerUsers.UserGroup.Banned);
		foreach (global::ServerUsers.User user in all)
		{
			if (!(user.notes == "EAC"))
			{
				string text2 = text;
				text = string.Concat(new string[]
				{
					text2,
					"banid ",
					user.steamid.ToString(),
					" ",
					StringExtensions.QuoteSafe(user.username),
					" ",
					StringExtensions.QuoteSafe(user.notes),
					"\r\n"
				});
			}
		}
		File.WriteAllText(serverFolder + "/bans.cfg", text);
		string text3 = string.Empty;
		foreach (global::ServerUsers.User user2 in global::ServerUsers.GetAll(global::ServerUsers.UserGroup.Owner))
		{
			string text2 = text3;
			text3 = string.Concat(new string[]
			{
				text2,
				"ownerid ",
				user2.steamid.ToString(),
				" ",
				StringExtensions.QuoteSafe(user2.username),
				" ",
				StringExtensions.QuoteSafe(user2.notes),
				"\r\n"
			});
		}
		foreach (global::ServerUsers.User user3 in global::ServerUsers.GetAll(global::ServerUsers.UserGroup.Moderator))
		{
			string text2 = text3;
			text3 = string.Concat(new string[]
			{
				text2,
				"moderatorid ",
				user3.steamid.ToString(),
				" ",
				StringExtensions.QuoteSafe(user3.username),
				" ",
				StringExtensions.QuoteSafe(user3.notes),
				"\r\n"
			});
		}
		File.WriteAllText(serverFolder + "/users.cfg", text3);
	}

	// Token: 0x06002010 RID: 8208 RVA: 0x000B78F4 File Offset: 0x000B5AF4
	public static string BanListString(bool bHeader = false)
	{
		IEnumerable<global::ServerUsers.User> all = global::ServerUsers.GetAll(global::ServerUsers.UserGroup.Banned);
		string text = string.Empty;
		if (bHeader)
		{
			if (all.Count<global::ServerUsers.User>() == 0)
			{
				return "ID filter list: empty\n";
			}
			if (all.Count<global::ServerUsers.User>() == 1)
			{
				text = "ID filter list: 1 entry\n";
			}
			else
			{
				text = "ID filter list: " + all.Count<global::ServerUsers.User>().ToString() + " entries\n";
			}
		}
		int num = 1;
		foreach (global::ServerUsers.User user in all)
		{
			string text2 = text;
			text = string.Concat(new string[]
			{
				text2,
				num.ToString(),
				" ",
				user.steamid.ToString(),
				" : permanent\n"
			});
			num++;
		}
		return text;
	}

	// Token: 0x06002011 RID: 8209 RVA: 0x000B79F8 File Offset: 0x000B5BF8
	public static string BanListStringEx()
	{
		IEnumerable<global::ServerUsers.User> all = global::ServerUsers.GetAll(global::ServerUsers.UserGroup.Banned);
		string text = string.Empty;
		int num = 1;
		foreach (global::ServerUsers.User user in all)
		{
			string text2 = text;
			text = string.Concat(new string[]
			{
				text2,
				num.ToString(),
				" ",
				user.steamid.ToString(),
				" ",
				StringExtensions.QuoteSafe(user.username),
				" ",
				StringExtensions.QuoteSafe(user.notes),
				"\n"
			});
			num++;
		}
		return text;
	}

	// Token: 0x04001ACB RID: 6859
	private static Dictionary<ulong, global::ServerUsers.User> users = new Dictionary<ulong, global::ServerUsers.User>();

	// Token: 0x02000627 RID: 1575
	public enum UserGroup
	{
		// Token: 0x04001ACE RID: 6862
		None,
		// Token: 0x04001ACF RID: 6863
		Owner,
		// Token: 0x04001AD0 RID: 6864
		Moderator,
		// Token: 0x04001AD1 RID: 6865
		Banned
	}

	// Token: 0x02000628 RID: 1576
	public class User
	{
		// Token: 0x04001AD2 RID: 6866
		public ulong steamid;

		// Token: 0x04001AD3 RID: 6867
		[JsonConverter(typeof(StringEnumConverter))]
		public global::ServerUsers.UserGroup group;

		// Token: 0x04001AD4 RID: 6868
		public string username;

		// Token: 0x04001AD5 RID: 6869
		public string notes;
	}
}
