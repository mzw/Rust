﻿using System;
using UnityEngine;

// Token: 0x02000440 RID: 1088
public class LakeInfo : MonoBehaviour
{
	// Token: 0x06001874 RID: 6260 RVA: 0x0008A704 File Offset: 0x00088904
	protected void Awake()
	{
		if (global::TerrainMeta.Path)
		{
			global::TerrainMeta.Path.LakeObjs.Add(this);
		}
	}
}
