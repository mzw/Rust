﻿using System;
using UnityEngine;

// Token: 0x020005F0 RID: 1520
[Serializable]
public class CausticsAnimation
{
	// Token: 0x04001A17 RID: 6679
	public float frameRate = 15f;

	// Token: 0x04001A18 RID: 6680
	public Texture2D[] framesShallow = new Texture2D[0];

	// Token: 0x04001A19 RID: 6681
	public Texture2D[] framesDeep = new Texture2D[0];
}
