﻿using System;
using UnityEngine;

// Token: 0x02000212 RID: 530
public class ConstructionSocket : global::Socket_Base
{
	// Token: 0x06000F99 RID: 3993 RVA: 0x0005F4D4 File Offset: 0x0005D6D4
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.red;
		Gizmos.DrawLine(Vector3.zero, Vector3.forward * 0.6f);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(Vector3.zero, Vector3.right * 0.1f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(Vector3.zero, Vector3.up * 0.1f);
		Gizmos.DrawIcon(base.transform.position, "light_circle_green.png", false);
	}

	// Token: 0x06000F9A RID: 3994 RVA: 0x0005F570 File Offset: 0x0005D770
	private void OnDrawGizmosSelected()
	{
		if (this.female)
		{
			Gizmos.matrix = base.transform.localToWorldMatrix;
			Gizmos.DrawWireCube(this.selectCenter, this.selectSize);
		}
	}

	// Token: 0x06000F9B RID: 3995 RVA: 0x0005F5A0 File Offset: 0x0005D7A0
	public override bool TestTarget(global::Construction.Target target)
	{
		return base.TestTarget(target) && this.IsCompatible(target.socket);
	}

	// Token: 0x06000F9C RID: 3996 RVA: 0x0005F5C0 File Offset: 0x0005D7C0
	public override bool IsCompatible(global::Socket_Base socket)
	{
		if (!base.IsCompatible(socket))
		{
			return false;
		}
		global::ConstructionSocket constructionSocket = socket as global::ConstructionSocket;
		return !(constructionSocket == null) && constructionSocket.socketType != global::ConstructionSocket.Type.None && this.socketType != global::ConstructionSocket.Type.None && constructionSocket.socketType == this.socketType;
	}

	// Token: 0x06000F9D RID: 3997 RVA: 0x0005F61C File Offset: 0x0005D81C
	public override bool CanConnect(Vector3 position, Quaternion rotation, global::Socket_Base socket, Vector3 socketPosition, Quaternion socketRotation)
	{
		if (!base.CanConnect(position, rotation, socket, socketPosition, socketRotation))
		{
			return false;
		}
		Matrix4x4 matrix4x = Matrix4x4.TRS(position, rotation, Vector3.one);
		Matrix4x4 matrix4x2 = Matrix4x4.TRS(socketPosition, socketRotation, Vector3.one);
		Vector3 vector = matrix4x.MultiplyPoint3x4(this.worldPosition);
		Vector3 vector2 = matrix4x2.MultiplyPoint3x4(socket.worldPosition);
		float num = Vector3.Distance(vector, vector2);
		if (num > 0.05f)
		{
			return false;
		}
		Vector3 vector3 = matrix4x.MultiplyVector(this.worldRotation * Vector3.forward);
		Vector3 vector4 = matrix4x2.MultiplyVector(socket.worldRotation * Vector3.forward);
		float num2 = Vector3.Angle(vector3, vector4);
		if (this.male && this.female)
		{
			num2 = Mathf.Min(num2, Vector3.Angle(-vector3, vector4));
		}
		if (socket.male && socket.female)
		{
			num2 = Mathf.Min(num2, Vector3.Angle(vector3, -vector4));
		}
		return num2 <= 5f;
	}

	// Token: 0x06000F9E RID: 3998 RVA: 0x0005F734 File Offset: 0x0005D934
	public bool TestRestrictedAngles(Vector3 suggestedPos, Quaternion suggestedAng, global::Construction.Target target)
	{
		if (this.restrictPlacementAngle)
		{
			Quaternion quaternion = Quaternion.Euler(0f, this.faceAngle, 0f);
			Quaternion quaternion2 = quaternion * suggestedAng;
			Vector3 vector = Vector3Ex.XZ3D(target.ray.direction);
			float num = Vector3Ex.DotDegrees(vector, quaternion2 * Vector3.forward);
			if (num > this.angleAllowed * 0.5f)
			{
				return false;
			}
			if (num < this.angleAllowed * -0.5f)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000F9F RID: 3999 RVA: 0x0005F7B8 File Offset: 0x0005D9B8
	public override global::Construction.Placement DoPlacement(global::Construction.Target target)
	{
		if (!target.entity || !target.entity.transform)
		{
			return null;
		}
		Vector3 worldPosition = target.GetWorldPosition();
		Quaternion worldRotation = target.GetWorldRotation(true);
		if (this.rotationDegrees > 0)
		{
			global::Construction.Placement placement = new global::Construction.Placement();
			float num = float.MaxValue;
			float num2 = 0f;
			for (int i = 0; i < 360; i += this.rotationDegrees)
			{
				Quaternion quaternion = Quaternion.Euler(0f, (float)(this.rotationOffset + i), 0f);
				Vector3 direction = target.ray.direction;
				Vector3 vector = quaternion * worldRotation * Vector3.up;
				float num3 = Vector3.Angle(direction, vector);
				if (num3 < num)
				{
					num = num3;
					num2 = (float)i;
				}
			}
			for (int j = 0; j < 360; j += this.rotationDegrees)
			{
				Quaternion quaternion2 = worldRotation * Quaternion.Inverse(this.rotation);
				Quaternion quaternion3 = Quaternion.Euler(target.rotation);
				Quaternion quaternion4 = Quaternion.Euler(0f, (float)(this.rotationOffset + j) + num2, 0f);
				Quaternion quaternion5 = quaternion3 * quaternion4 * quaternion2;
				Vector3 vector2 = quaternion5 * this.position;
				placement.position = worldPosition - vector2;
				placement.rotation = quaternion5;
				if (this.CheckSocketMods(placement))
				{
					return placement;
				}
			}
		}
		global::Construction.Placement placement2 = new global::Construction.Placement();
		Quaternion quaternion6 = worldRotation * Quaternion.Inverse(this.rotation);
		Vector3 vector3 = quaternion6 * this.position;
		placement2.position = worldPosition - vector3;
		placement2.rotation = quaternion6;
		if (!this.TestRestrictedAngles(worldPosition, worldRotation, target))
		{
			return null;
		}
		return placement2;
	}

	// Token: 0x04000A5C RID: 2652
	public global::ConstructionSocket.Type socketType;

	// Token: 0x04000A5D RID: 2653
	public int rotationDegrees;

	// Token: 0x04000A5E RID: 2654
	public int rotationOffset;

	// Token: 0x04000A5F RID: 2655
	public bool restrictPlacementAngle;

	// Token: 0x04000A60 RID: 2656
	public float faceAngle;

	// Token: 0x04000A61 RID: 2657
	public float angleAllowed = 150f;

	// Token: 0x04000A62 RID: 2658
	[Range(0f, 1f)]
	public float support = 1f;

	// Token: 0x02000213 RID: 531
	public enum Type
	{
		// Token: 0x04000A64 RID: 2660
		None,
		// Token: 0x04000A65 RID: 2661
		Foundation,
		// Token: 0x04000A66 RID: 2662
		Floor,
		// Token: 0x04000A67 RID: 2663
		Doorway = 4,
		// Token: 0x04000A68 RID: 2664
		Wall,
		// Token: 0x04000A69 RID: 2665
		Block,
		// Token: 0x04000A6A RID: 2666
		Window = 11,
		// Token: 0x04000A6B RID: 2667
		Shutters,
		// Token: 0x04000A6C RID: 2668
		WallFrame,
		// Token: 0x04000A6D RID: 2669
		FloorFrame,
		// Token: 0x04000A6E RID: 2670
		WindowDressing,
		// Token: 0x04000A6F RID: 2671
		DoorDressing
	}
}
