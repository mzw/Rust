﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000074 RID: 116
public class KeyLock : global::BaseLock
{
	// Token: 0x06000816 RID: 2070 RVA: 0x000346D4 File Offset: 0x000328D4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("KeyLock.OnRpcMessage", 0.1f))
		{
			if (rpc == 4084065801u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_CreateKey ");
				}
				using (TimeWarning.New("RPC_CreateKey", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_CreateKey", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_CreateKey(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_CreateKey");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 358427461u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Lock ");
				}
				using (TimeWarning.New("RPC_Lock", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Lock", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Lock(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_Lock");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 1108420318u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Unlock ");
				}
				using (TimeWarning.New("RPC_Unlock", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Unlock", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Unlock(rpc4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in RPC_Unlock");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000817 RID: 2071 RVA: 0x00034BD0 File Offset: 0x00032DD0
	public override bool HasLockPermission(global::BasePlayer player)
	{
		if (player.IsDead())
		{
			return false;
		}
		if (player.userID == base.OwnerID)
		{
			return true;
		}
		List<global::Item> list = player.inventory.FindItemIDs(this.keyItemType.itemid);
		foreach (global::Item key in list)
		{
			if (this.CanKeyUnlockUs(key))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x06000818 RID: 2072 RVA: 0x00034C70 File Offset: 0x00032E70
	private bool CanKeyUnlockUs(global::Item key)
	{
		return key.instanceData != null && key.instanceData.dataInt == this.keyCode;
	}

	// Token: 0x06000819 RID: 2073 RVA: 0x00034C98 File Offset: 0x00032E98
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.keyLock != null)
		{
			this.keyCode = info.msg.keyLock.code;
		}
	}

	// Token: 0x0600081A RID: 2074 RVA: 0x00034CCC File Offset: 0x00032ECC
	public override bool ShouldNetworkOwnerInfo()
	{
		return true;
	}

	// Token: 0x0600081B RID: 2075 RVA: 0x00034CD0 File Offset: 0x00032ED0
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		if (base.OwnerID == 0UL && base.GetParentEntity())
		{
			base.OwnerID = base.GetParentEntity().OwnerID;
		}
	}

	// Token: 0x0600081C RID: 2076 RVA: 0x00034D08 File Offset: 0x00032F08
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.keyLock = Facepunch.Pool.Get<ProtoBuf.KeyLock>();
		info.msg.keyLock.code = this.keyCode;
	}

	// Token: 0x0600081D RID: 2077 RVA: 0x00034D3C File Offset: 0x00032F3C
	public override void OnDeployed(global::BaseEntity parent)
	{
		base.OnDeployed(parent);
		this.keyCode = Random.Range(1, 100000);
	}

	// Token: 0x0600081E RID: 2078 RVA: 0x00034D58 File Offset: 0x00032F58
	public override bool OnTryToOpen(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanUseLockedEntity", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.HasLockPermission(player) || !base.IsLocked();
	}

	// Token: 0x0600081F RID: 2079 RVA: 0x00034DA8 File Offset: 0x00032FA8
	public override bool OnTryToClose(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanUseLockedEntity", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.HasLockPermission(player) || !base.IsLocked();
	}

	// Token: 0x06000820 RID: 2080 RVA: 0x00034DF8 File Offset: 0x00032FF8
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_Unlock(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!base.IsLocked())
		{
			return;
		}
		if (Interface.CallHook("CanUnlock", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		if (!this.HasLockPermission(rpc.player))
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Locked, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000821 RID: 2081 RVA: 0x00034E6C File Offset: 0x0003306C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_Lock(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (base.IsLocked())
		{
			return;
		}
		if (Interface.CallHook("CanLock", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		if (!this.HasLockPermission(rpc.player))
		{
			return;
		}
		this.LockLock(rpc.player);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000822 RID: 2082 RVA: 0x00034EE4 File Offset: 0x000330E4
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_CreateKey(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (base.IsLocked() && !this.HasLockPermission(rpc.player))
		{
			return;
		}
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(this.keyItemType.itemid);
		if (itemDefinition == null)
		{
			Debug.LogWarning("RPC_CreateKey: Itemdef is missing! " + this.keyItemType);
			return;
		}
		global::ItemBlueprint bp = global::ItemManager.FindBlueprint(itemDefinition);
		if (rpc.player.inventory.crafting.CanCraft(bp, 1))
		{
			ProtoBuf.Item.InstanceData instanceData = Facepunch.Pool.Get<ProtoBuf.Item.InstanceData>();
			instanceData.dataInt = this.keyCode;
			rpc.player.inventory.crafting.CraftItem(bp, rpc.player, instanceData, 1, 0, null);
			if (!this.firstKeyCreated)
			{
				this.LockLock(rpc.player);
				base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
				this.firstKeyCreated = true;
			}
			return;
		}
	}

	// Token: 0x06000823 RID: 2083 RVA: 0x00034FD4 File Offset: 0x000331D4
	public void LockLock(global::BasePlayer player)
	{
		base.SetFlag(global::BaseEntity.Flags.Locked, true, false);
		if (player.IsValid())
		{
			player.GiveAchievement("LOCK_LOCK");
		}
	}

	// Token: 0x040003D4 RID: 980
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition keyItemType;

	// Token: 0x040003D5 RID: 981
	public int keyCode;

	// Token: 0x040003D6 RID: 982
	public bool firstKeyCreated;
}
