﻿using System;
using ConVar;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000797 RID: 1943
public class MeshDataBatch : global::MeshBatch
{
	// Token: 0x17000284 RID: 644
	// (get) Token: 0x0600240E RID: 9230 RVA: 0x000C6D8C File Offset: 0x000C4F8C
	public override int VertexCapacity
	{
		get
		{
			return ConVar.Batching.renderer_capacity;
		}
	}

	// Token: 0x17000285 RID: 645
	// (get) Token: 0x0600240F RID: 9231 RVA: 0x000C6D94 File Offset: 0x000C4F94
	public override int VertexCutoff
	{
		get
		{
			return ConVar.Batching.renderer_vertices;
		}
	}

	// Token: 0x06002410 RID: 9232 RVA: 0x000C6D9C File Offset: 0x000C4F9C
	public static GameObject CreateInstance()
	{
		GameObject gameObject = new GameObject("MeshDataBatch");
		gameObject.AddComponent<MeshFilter>();
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<global::MeshDataBatch>();
		return gameObject;
	}

	// Token: 0x06002411 RID: 9233 RVA: 0x000C6DCC File Offset: 0x000C4FCC
	protected void Awake()
	{
		this.meshFilter = base.GetComponent<MeshFilter>();
		this.meshRenderer = base.GetComponent<MeshRenderer>();
		this.meshData = new global::MeshData();
		this.meshGroup = new global::MeshGroup();
	}

	// Token: 0x06002412 RID: 9234 RVA: 0x000C6DFC File Offset: 0x000C4FFC
	public void Setup(Vector3 position, Material material, ShadowCastingMode shadows, int layer)
	{
		base.transform.position = position;
		this.position = position;
		base.gameObject.layer = layer;
		this.meshRenderer.sharedMaterial = material;
		this.meshRenderer.shadowCastingMode = shadows;
		if (shadows == 3)
		{
			this.meshRenderer.receiveShadows = false;
			this.meshRenderer.motionVectors = false;
			this.meshRenderer.lightProbeUsage = 0;
			this.meshRenderer.reflectionProbeUsage = 0;
		}
		else
		{
			this.meshRenderer.receiveShadows = true;
			this.meshRenderer.motionVectors = true;
			this.meshRenderer.lightProbeUsage = 1;
			this.meshRenderer.reflectionProbeUsage = 1;
		}
	}

	// Token: 0x06002413 RID: 9235 RVA: 0x000C6EB0 File Offset: 0x000C50B0
	public void Add(global::MeshInstance instance)
	{
		instance.position -= this.position;
		this.meshGroup.data.Add(instance);
		base.AddVertices(instance.mesh.vertexCount);
	}

	// Token: 0x06002414 RID: 9236 RVA: 0x000C6EF0 File Offset: 0x000C50F0
	protected override void AllocMemory()
	{
		this.meshGroup.Alloc();
		this.meshData.Alloc();
	}

	// Token: 0x06002415 RID: 9237 RVA: 0x000C6F08 File Offset: 0x000C5108
	protected override void FreeMemory()
	{
		this.meshGroup.Free();
		this.meshData.Free();
	}

	// Token: 0x06002416 RID: 9238 RVA: 0x000C6F20 File Offset: 0x000C5120
	protected override void RefreshMesh()
	{
		this.meshData.Clear();
		this.meshData.Combine(this.meshGroup);
	}

	// Token: 0x06002417 RID: 9239 RVA: 0x000C6F40 File Offset: 0x000C5140
	protected override void ApplyMesh()
	{
		if (!this.meshBatch)
		{
			this.meshBatch = AssetPool.Get<UnityEngine.Mesh>();
		}
		this.meshData.Apply(this.meshBatch);
		this.meshBatch.UploadMeshData(false);
	}

	// Token: 0x06002418 RID: 9240 RVA: 0x000C6F7C File Offset: 0x000C517C
	protected override void ToggleMesh(bool state)
	{
		if (state)
		{
			if (this.meshFilter)
			{
				this.meshFilter.sharedMesh = this.meshBatch;
			}
			if (this.meshRenderer)
			{
				this.meshRenderer.enabled = true;
			}
		}
		else
		{
			if (this.meshFilter)
			{
				this.meshFilter.sharedMesh = null;
			}
			if (this.meshRenderer)
			{
				this.meshRenderer.enabled = false;
			}
		}
	}

	// Token: 0x06002419 RID: 9241 RVA: 0x000C700C File Offset: 0x000C520C
	protected override void OnPooled()
	{
		if (this.meshFilter)
		{
			this.meshFilter.sharedMesh = null;
		}
		if (this.meshBatch)
		{
			AssetPool.Free(ref this.meshBatch);
		}
		this.meshData.Free();
		this.meshGroup.Free();
	}

	// Token: 0x04001FBD RID: 8125
	private Vector3 position;

	// Token: 0x04001FBE RID: 8126
	private UnityEngine.Mesh meshBatch;

	// Token: 0x04001FBF RID: 8127
	private MeshFilter meshFilter;

	// Token: 0x04001FC0 RID: 8128
	private MeshRenderer meshRenderer;

	// Token: 0x04001FC1 RID: 8129
	private global::MeshData meshData;

	// Token: 0x04001FC2 RID: 8130
	private global::MeshGroup meshGroup;
}
