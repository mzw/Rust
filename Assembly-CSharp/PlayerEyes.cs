﻿using System;
using UnityEngine;

// Token: 0x020003C1 RID: 961
public class PlayerEyes : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x1700018B RID: 395
	// (get) Token: 0x0600169C RID: 5788 RVA: 0x00082BD4 File Offset: 0x00080DD4
	public Vector3 position
	{
		get
		{
			if (base.baseEntity && base.baseEntity.isMounted)
			{
				Vector3 vector = base.baseEntity.GetMounted().EyePositionForPlayer(base.baseEntity);
				if (vector != Vector3.zero)
				{
					return vector;
				}
			}
			return base.transform.position + base.transform.up * (global::PlayerEyes.EyeOffset.y + this.viewOffset.y);
		}
	}

	// Token: 0x0600169D RID: 5789 RVA: 0x00082C64 File Offset: 0x00080E64
	public void NetworkUpdate(Quaternion rot)
	{
		this.viewOffset = ((!base.baseEntity.IsDucked()) ? Vector3.zero : global::PlayerEyes.DuckOffset);
		this.rotation = rot;
	}

	// Token: 0x0600169E RID: 5790 RVA: 0x00082C94 File Offset: 0x00080E94
	public Ray BodyRay()
	{
		return new Ray(this.position, this.BodyForward());
	}

	// Token: 0x0600169F RID: 5791 RVA: 0x00082CA8 File Offset: 0x00080EA8
	public Vector3 BodyForward()
	{
		return this.rotation * Vector3.forward;
	}

	// Token: 0x060016A0 RID: 5792 RVA: 0x00082CBC File Offset: 0x00080EBC
	public Vector3 BodyRight()
	{
		return this.rotation * Vector3.right;
	}

	// Token: 0x060016A1 RID: 5793 RVA: 0x00082CD0 File Offset: 0x00080ED0
	public Vector3 BodyUp()
	{
		return this.rotation * Vector3.up;
	}

	// Token: 0x060016A2 RID: 5794 RVA: 0x00082CE4 File Offset: 0x00080EE4
	public Ray HeadRay()
	{
		return new Ray(this.position, this.HeadForward());
	}

	// Token: 0x060016A3 RID: 5795 RVA: 0x00082CF8 File Offset: 0x00080EF8
	public Vector3 HeadForward()
	{
		return this.rotation * this.headRotation * Vector3.forward;
	}

	// Token: 0x060016A4 RID: 5796 RVA: 0x00082D18 File Offset: 0x00080F18
	public Vector3 HeadRight()
	{
		return this.rotation * this.headRotation * Vector3.right;
	}

	// Token: 0x060016A5 RID: 5797 RVA: 0x00082D38 File Offset: 0x00080F38
	public Vector3 HeadUp()
	{
		return this.rotation * this.headRotation * Vector3.up;
	}

	// Token: 0x04001126 RID: 4390
	public static readonly Vector3 EyeOffset = new Vector3(0f, 1.5f, 0f);

	// Token: 0x04001127 RID: 4391
	public static readonly Vector3 DuckOffset = new Vector3(0f, -0.6f, 0f);

	// Token: 0x04001128 RID: 4392
	public Vector3 thirdPersonSleepingOffset = new Vector3(0.43f, 1.25f, 0.7f);

	// Token: 0x04001129 RID: 4393
	public global::LazyAimProperties defaultLazyAim;

	// Token: 0x0400112A RID: 4394
	[NonSerialized]
	private Vector3 viewOffset = Vector3.zero;

	// Token: 0x0400112B RID: 4395
	[NonSerialized]
	public Quaternion rotation = Quaternion.identity;

	// Token: 0x0400112C RID: 4396
	[NonSerialized]
	public Quaternion headRotation = Quaternion.identity;

	// Token: 0x0400112D RID: 4397
	[NonSerialized]
	public Quaternion rotationLook = Quaternion.identity;
}
