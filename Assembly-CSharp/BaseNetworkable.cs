﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConVar;
using Facepunch;
using Network;
using Network.Visibility;
using Oxide.Core;
using ProtoBuf;
using Rust;
using Rust.Registry;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200035C RID: 860
public abstract class BaseNetworkable : global::BaseMonoBehaviour, IEntity, NetworkHandler
{
	// Token: 0x17000175 RID: 373
	// (get) Token: 0x0600147D RID: 5245 RVA: 0x00077678 File Offset: 0x00075878
	// (set) Token: 0x0600147E RID: 5246 RVA: 0x00077680 File Offset: 0x00075880
	public bool IsDestroyed { get; private set; }

	// Token: 0x17000176 RID: 374
	// (get) Token: 0x0600147F RID: 5247 RVA: 0x0007768C File Offset: 0x0007588C
	public string PrefabName
	{
		get
		{
			if (this._prefabName == null)
			{
				this._prefabName = global::StringPool.Get(this.prefabID);
			}
			return this._prefabName;
		}
	}

	// Token: 0x17000177 RID: 375
	// (get) Token: 0x06001480 RID: 5248 RVA: 0x000776B0 File Offset: 0x000758B0
	public string ShortPrefabName
	{
		get
		{
			if (this._prefabNameWithoutExtension == null)
			{
				this._prefabNameWithoutExtension = Path.GetFileNameWithoutExtension(this.PrefabName);
			}
			return this._prefabNameWithoutExtension;
		}
	}

	// Token: 0x06001481 RID: 5249 RVA: 0x000776D4 File Offset: 0x000758D4
	public string InvokeString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		List<InvokeAction> list = Facepunch.Pool.GetList<InvokeAction>();
		InvokeHandler.FindInvokes(this, list);
		foreach (InvokeAction invokeAction in list)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(", ");
			}
			stringBuilder.Append(invokeAction.action.Method.Name);
		}
		Facepunch.Pool.FreeList<InvokeAction>(ref list);
		return stringBuilder.ToString();
	}

	// Token: 0x06001482 RID: 5250 RVA: 0x00077774 File Offset: 0x00075974
	public global::BaseEntity LookupPrefab()
	{
		return this.gameManager.FindPrefab(this.PrefabName).ToBaseEntity();
	}

	// Token: 0x06001483 RID: 5251 RVA: 0x0007778C File Offset: 0x0007598C
	public bool EqualNetID(global::BaseNetworkable other)
	{
		return other != null && other.net != null && this.net != null && other.net.ID == this.net.ID;
	}

	// Token: 0x06001484 RID: 5252 RVA: 0x000777CC File Offset: 0x000759CC
	public virtual bool SupportsPooling()
	{
		return false;
	}

	// Token: 0x06001485 RID: 5253 RVA: 0x000777D0 File Offset: 0x000759D0
	public virtual void ResetState()
	{
		if (this.children.Count > 0)
		{
			this.children.Clear();
		}
	}

	// Token: 0x06001486 RID: 5254 RVA: 0x000777F0 File Offset: 0x000759F0
	public virtual void InitShared()
	{
	}

	// Token: 0x06001487 RID: 5255 RVA: 0x000777F4 File Offset: 0x000759F4
	public virtual void PreInitShared()
	{
	}

	// Token: 0x06001488 RID: 5256 RVA: 0x000777F8 File Offset: 0x000759F8
	public virtual void PostInitShared()
	{
	}

	// Token: 0x06001489 RID: 5257 RVA: 0x000777FC File Offset: 0x000759FC
	public virtual void DestroyShared()
	{
		using (TimeWarning.New("Registry.Entity.Unregister", 0.1f))
		{
			Rust.Registry.Entity.Unregister(base.gameObject);
		}
	}

	// Token: 0x0600148A RID: 5258 RVA: 0x00077848 File Offset: 0x00075A48
	public virtual void OnNetworkGroupEnter(Group group)
	{
	}

	// Token: 0x0600148B RID: 5259 RVA: 0x0007784C File Offset: 0x00075A4C
	public virtual void OnNetworkGroupLeave(Group group)
	{
	}

	// Token: 0x0600148C RID: 5260 RVA: 0x00077850 File Offset: 0x00075A50
	public void OnNetworkGroupChange()
	{
		if (this.children != null)
		{
			foreach (global::BaseEntity baseEntity in this.children)
			{
				baseEntity.net.SwitchGroup(this.net.group);
			}
		}
	}

	// Token: 0x0600148D RID: 5261 RVA: 0x000778C8 File Offset: 0x00075AC8
	public void OnNetworkSubscribersEnter(List<Connection> connections)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		foreach (Connection connection in connections)
		{
			global::BasePlayer basePlayer = connection.player as global::BasePlayer;
			if (!(basePlayer == null))
			{
				basePlayer.QueueUpdate(global::BasePlayer.NetworkQueue.Update, this as global::BaseEntity);
			}
		}
	}

	// Token: 0x0600148E RID: 5262 RVA: 0x00077954 File Offset: 0x00075B54
	public void OnNetworkSubscribersLeave(List<Connection> connections)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "LeaveVisibility");
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(6);
			Network.Net.sv.write.EntityID(this.net.ID);
			Network.Net.sv.write.UInt8(0);
			Network.Net.sv.write.Send(new SendInfo(connections));
		}
	}

	// Token: 0x0600148F RID: 5263 RVA: 0x000779E4 File Offset: 0x00075BE4
	private void EntityDestroy()
	{
		if (base.gameObject)
		{
			this.ResetState();
			this.gameManager.Retire(base.gameObject);
		}
	}

	// Token: 0x06001490 RID: 5264 RVA: 0x00077A10 File Offset: 0x00075C10
	private void DoEntityDestroy()
	{
		if (this.IsDestroyed)
		{
			return;
		}
		this.IsDestroyed = true;
		if (Application.isQuitting)
		{
			return;
		}
		this.DestroyShared();
		if (this.isServer)
		{
			this.DoServerDestroy();
		}
	}

	// Token: 0x06001491 RID: 5265 RVA: 0x00077A48 File Offset: 0x00075C48
	private void SpawnShared()
	{
		this.IsDestroyed = false;
		using (TimeWarning.New("Registry.Entity.Register", 0.1f))
		{
			Rust.Registry.Entity.Register(base.gameObject, this);
		}
	}

	// Token: 0x06001492 RID: 5266 RVA: 0x00077A9C File Offset: 0x00075C9C
	public virtual void Save(global::BaseNetworkable.SaveInfo info)
	{
		if (this.prefabID == 0u)
		{
			Debug.LogError("PrefabID is 0! " + base.transform.GetRecursiveName(string.Empty), base.gameObject);
		}
		info.msg.baseNetworkable = Facepunch.Pool.Get<ProtoBuf.BaseNetworkable>();
		info.msg.baseNetworkable.uid = this.net.ID;
		info.msg.baseNetworkable.prefabID = this.prefabID;
		if (this.net.group != null)
		{
			info.msg.baseNetworkable.group = this.net.group.ID;
		}
		if (!info.forDisk)
		{
			info.msg.createdThisFrame = (this.creationFrame == UnityEngine.Time.frameCount);
		}
	}

	// Token: 0x06001493 RID: 5267 RVA: 0x00077B74 File Offset: 0x00075D74
	public virtual void PostSave(global::BaseNetworkable.SaveInfo info)
	{
	}

	// Token: 0x06001494 RID: 5268 RVA: 0x00077B78 File Offset: 0x00075D78
	public void InitLoad(uint entityID)
	{
		this.net = Network.Net.sv.CreateNetworkable(entityID);
		global::BaseNetworkable.serverEntities.RegisterID(this);
		this.PreServerLoad();
	}

	// Token: 0x06001495 RID: 5269 RVA: 0x00077B9C File Offset: 0x00075D9C
	public virtual void PreServerLoad()
	{
	}

	// Token: 0x06001496 RID: 5270 RVA: 0x00077BA0 File Offset: 0x00075DA0
	public virtual void Load(global::BaseNetworkable.LoadInfo info)
	{
		if (info.msg.baseNetworkable == null)
		{
			return;
		}
		ProtoBuf.BaseNetworkable baseNetworkable = info.msg.baseNetworkable;
		if (this.prefabID != baseNetworkable.prefabID)
		{
			Debug.LogError(string.Concat(new object[]
			{
				"Prefab IDs don't match! ",
				this.prefabID,
				"/",
				baseNetworkable.prefabID,
				" -> ",
				base.gameObject
			}), base.gameObject);
		}
	}

	// Token: 0x06001497 RID: 5271 RVA: 0x00077C30 File Offset: 0x00075E30
	public virtual void PostServerLoad()
	{
		base.gameObject.SendOnSendNetworkUpdate(this as global::BaseEntity);
	}

	// Token: 0x17000178 RID: 376
	// (get) Token: 0x06001498 RID: 5272 RVA: 0x00077C44 File Offset: 0x00075E44
	public bool isServer
	{
		get
		{
			return true;
		}
	}

	// Token: 0x17000179 RID: 377
	// (get) Token: 0x06001499 RID: 5273 RVA: 0x00077C48 File Offset: 0x00075E48
	public bool isClient
	{
		get
		{
			return false;
		}
	}

	// Token: 0x0600149A RID: 5274 RVA: 0x00077C4C File Offset: 0x00075E4C
	public T ToServer<T>() where T : global::BaseNetworkable
	{
		if (this.isServer)
		{
			return this as T;
		}
		return (T)((object)null);
	}

	// Token: 0x0600149B RID: 5275 RVA: 0x00077C6C File Offset: 0x00075E6C
	public virtual bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		return false;
	}

	// Token: 0x0600149C RID: 5276 RVA: 0x00077C70 File Offset: 0x00075E70
	public static IEnumerable<Connection> GetConnectionsWithin(Vector3 position, float distance)
	{
		for (int i = 0; i < Network.Net.sv.connections.Count; i++)
		{
			Connection c = Network.Net.sv.connections[i];
			if (c.active)
			{
				global::BasePlayer player = c.player as global::BasePlayer;
				if (!(player == null))
				{
					if (player.Distance(position) <= distance)
					{
						yield return c;
					}
				}
			}
		}
		yield break;
	}

	// Token: 0x1700017A RID: 378
	// (get) Token: 0x0600149D RID: 5277 RVA: 0x00077C9C File Offset: 0x00075E9C
	// (set) Token: 0x0600149E RID: 5278 RVA: 0x00077CA4 File Offset: 0x00075EA4
	public bool limitNetworking
	{
		get
		{
			return this._limitedNetworking;
		}
		set
		{
			if (value == this._limitedNetworking)
			{
				return;
			}
			this._limitedNetworking = value;
			if (this._limitedNetworking)
			{
				this.OnNetworkLimitStart();
			}
			else
			{
				this.OnNetworkLimitEnd();
			}
		}
	}

	// Token: 0x0600149F RID: 5279 RVA: 0x00077CD8 File Offset: 0x00075ED8
	private void OnNetworkLimitStart()
	{
		base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "OnNetworkLimitStart");
		List<Connection> list = this.GetSubscribers();
		if (list == null)
		{
			return;
		}
		list = list.ToList<Connection>();
		list.RemoveAll((Connection x) => this.ShouldNetworkTo(x.player as global::BasePlayer));
		this.OnNetworkSubscribersLeave(list);
		if (this.children != null)
		{
			foreach (global::BaseEntity baseEntity in this.children)
			{
				baseEntity.OnNetworkLimitStart();
			}
		}
	}

	// Token: 0x060014A0 RID: 5280 RVA: 0x00077D7C File Offset: 0x00075F7C
	private void OnNetworkLimitEnd()
	{
		base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "OnNetworkLimitEnd");
		List<Connection> subscribers = this.GetSubscribers();
		if (subscribers == null)
		{
			return;
		}
		this.OnNetworkSubscribersEnter(subscribers);
		if (this.children != null)
		{
			foreach (global::BaseEntity baseEntity in this.children)
			{
				baseEntity.OnNetworkLimitEnd();
			}
		}
	}

	// Token: 0x060014A1 RID: 5281 RVA: 0x00077E04 File Offset: 0x00076004
	public void AddChild(global::BaseEntity child)
	{
		if (this.children.Contains(child))
		{
			return;
		}
		this.children.Add(child);
	}

	// Token: 0x060014A2 RID: 5282 RVA: 0x00077E24 File Offset: 0x00076024
	public void RemoveChild(global::BaseEntity child)
	{
		this.children.Remove(child);
	}

	// Token: 0x1700017B RID: 379
	// (get) Token: 0x060014A3 RID: 5283 RVA: 0x00077E34 File Offset: 0x00076034
	public global::GameManager gameManager
	{
		get
		{
			if (this.isServer)
			{
				return global::GameManager.server;
			}
			throw new NotImplementedException("Missing gameManager path");
		}
	}

	// Token: 0x1700017C RID: 380
	// (get) Token: 0x060014A4 RID: 5284 RVA: 0x00077E54 File Offset: 0x00076054
	public global::PrefabAttribute.Library prefabAttribute
	{
		get
		{
			if (this.isServer)
			{
				return global::PrefabAttribute.server;
			}
			throw new NotImplementedException("Missing prefabAttribute path");
		}
	}

	// Token: 0x1700017D RID: 381
	// (get) Token: 0x060014A5 RID: 5285 RVA: 0x00077E74 File Offset: 0x00076074
	public static Group GlobalNetworkGroup
	{
		get
		{
			return Network.Net.sv.visibility.Get(0u);
		}
	}

	// Token: 0x1700017E RID: 382
	// (get) Token: 0x060014A6 RID: 5286 RVA: 0x00077E88 File Offset: 0x00076088
	public static Group LimboNetworkGroup
	{
		get
		{
			return Network.Net.sv.visibility.Get(1u);
		}
	}

	// Token: 0x060014A7 RID: 5287 RVA: 0x00077E9C File Offset: 0x0007609C
	public virtual void Spawn()
	{
		this.SpawnShared();
		if (this.net == null)
		{
			this.net = Network.Net.sv.CreateNetworkable();
		}
		this.creationFrame = UnityEngine.Time.frameCount;
		this.PreInitShared();
		this.InitShared();
		this.ServerInit();
		this.PostInitShared();
		this.UpdateNetworkGroup();
		this.isSpawned = true;
		Interface.CallHook("OnEntitySpawned", new object[]
		{
			this
		});
		this.SendNetworkUpdateImmediate(true);
		if (Application.isLoading && !Application.isLoadingSave)
		{
			base.gameObject.SendOnSendNetworkUpdate(this as global::BaseEntity);
		}
	}

	// Token: 0x060014A8 RID: 5288 RVA: 0x00077F40 File Offset: 0x00076140
	public virtual void ServerInit()
	{
		global::BaseNetworkable.serverEntities.RegisterID(this);
		if (this.net != null)
		{
			this.net.handler = this;
		}
	}

	// Token: 0x060014A9 RID: 5289 RVA: 0x00077F64 File Offset: 0x00076164
	protected List<Connection> GetSubscribers()
	{
		if (this.net == null)
		{
			return null;
		}
		if (this.net.group == null)
		{
			return Network.Net.sv.connections;
		}
		return this.net.group.subscribers;
	}

	// Token: 0x060014AA RID: 5290 RVA: 0x00077FA0 File Offset: 0x000761A0
	public void KillMessage()
	{
		this.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x060014AB RID: 5291 RVA: 0x00077FAC File Offset: 0x000761AC
	public void Kill(global::BaseNetworkable.DestroyMode mode = global::BaseNetworkable.DestroyMode.None)
	{
		if (this.IsDestroyed)
		{
			Debug.LogWarning("Calling kill - but already IsDestroyed!? " + this);
			return;
		}
		Interface.CallHook("OnEntityKill", new object[]
		{
			this
		});
		base.gameObject.BroadcastOnParentDestroying();
		this.Term(mode);
		this.DoEntityDestroy();
		this.EntityDestroy();
	}

	// Token: 0x060014AC RID: 5292 RVA: 0x0007800C File Offset: 0x0007620C
	private void Term(global::BaseNetworkable.DestroyMode mode)
	{
		if (this.net == null)
		{
			return;
		}
		if (this.net.group == null)
		{
			return;
		}
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "Term {0}", mode);
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(6);
			Network.Net.sv.write.EntityID(this.net.ID);
			Network.Net.sv.write.UInt8((byte)mode);
			Network.Net.sv.write.Send(new SendInfo(this.net.group.subscribers));
		}
	}

	// Token: 0x060014AD RID: 5293 RVA: 0x000780CC File Offset: 0x000762CC
	internal virtual void DoServerDestroy()
	{
		this.isSpawned = false;
		if (this.net == null)
		{
			return;
		}
		this.InvalidateNetworkCache();
		global::BaseNetworkable.serverEntities.UnregisterID(this);
		Network.Net.sv.DestroyNetworkable(ref this.net);
		base.StopAllCoroutines();
		base.gameObject.SetActive(false);
	}

	// Token: 0x060014AE RID: 5294 RVA: 0x00078120 File Offset: 0x00076320
	public virtual bool ShouldNetworkTo(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanNetworkTo", new object[]
		{
			this,
			player
		});
		return !(obj is bool) || (bool)obj;
	}

	// Token: 0x060014AF RID: 5295 RVA: 0x0007815C File Offset: 0x0007635C
	protected void SendNetworkGroupChange()
	{
		if (!this.isSpawned)
		{
			return;
		}
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		if (this.net.group == null)
		{
			Debug.LogWarning(this.ToString() + " changed its network group to null");
			return;
		}
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(7);
			Network.Net.sv.write.EntityID(this.net.ID);
			Network.Net.sv.write.GroupID(this.net.group.ID);
			Network.Net.sv.write.Send(new SendInfo(this.net.group.subscribers));
		}
	}

	// Token: 0x060014B0 RID: 5296 RVA: 0x0007822C File Offset: 0x0007642C
	protected void SendAsSnapshot(Connection connection, bool justCreated = false)
	{
		if (Network.Net.sv.write.Start())
		{
			connection.validate.entityUpdates = connection.validate.entityUpdates + 1u;
			global::BaseNetworkable.SaveInfo saveInfo = new global::BaseNetworkable.SaveInfo
			{
				forConnection = connection,
				forDisk = false
			};
			Network.Net.sv.write.PacketID(5);
			Network.Net.sv.write.UInt32(connection.validate.entityUpdates);
			this.ToStreamForNetwork(Network.Net.sv.write, saveInfo);
			Network.Net.sv.write.Send(new SendInfo(connection));
		}
	}

	// Token: 0x060014B1 RID: 5297 RVA: 0x000782CC File Offset: 0x000764CC
	public void SendNetworkUpdate(global::BasePlayer.NetworkQueue queue = global::BasePlayer.NetworkQueue.Update)
	{
		if (Application.isLoading)
		{
			return;
		}
		if (Application.isLoadingSave)
		{
			return;
		}
		if (this.IsDestroyed)
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (!this.isSpawned)
		{
			return;
		}
		using (TimeWarning.New("SendNetworkUpdate", 0.1f))
		{
			base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "SendNetworkUpdate");
			this.InvalidateNetworkCache();
			List<Connection> subscribers = this.GetSubscribers();
			if (subscribers != null && subscribers.Count > 0)
			{
				for (int i = 0; i < subscribers.Count; i++)
				{
					Connection connection = subscribers[i];
					global::BasePlayer basePlayer = connection.player as global::BasePlayer;
					if (!(basePlayer == null))
					{
						if (this.ShouldNetworkTo(basePlayer))
						{
							basePlayer.QueueUpdate(queue, this);
						}
					}
				}
			}
		}
		base.gameObject.SendOnSendNetworkUpdate(this as global::BaseEntity);
	}

	// Token: 0x060014B2 RID: 5298 RVA: 0x000783DC File Offset: 0x000765DC
	public void SendNetworkUpdateImmediate(bool justCreated = false)
	{
		if (Application.isLoading)
		{
			return;
		}
		if (Application.isLoadingSave)
		{
			return;
		}
		if (this.IsDestroyed)
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (!this.isSpawned)
		{
			return;
		}
		using (TimeWarning.New("SendNetworkUpdateImmediate", 0.1f))
		{
			base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "SendNetworkUpdateImmediate");
			this.InvalidateNetworkCache();
			List<Connection> subscribers = this.GetSubscribers();
			if (subscribers != null && subscribers.Count > 0)
			{
				for (int i = 0; i < subscribers.Count; i++)
				{
					Connection connection = subscribers[i];
					global::BasePlayer basePlayer = connection.player as global::BasePlayer;
					if (!(basePlayer == null))
					{
						if (this.ShouldNetworkTo(basePlayer))
						{
							this.SendAsSnapshot(connection, justCreated);
						}
					}
				}
			}
		}
		base.gameObject.SendOnSendNetworkUpdate(this as global::BaseEntity);
	}

	// Token: 0x060014B3 RID: 5299 RVA: 0x000784EC File Offset: 0x000766EC
	protected void SendNetworkUpdate_Position()
	{
		if (Application.isLoading)
		{
			return;
		}
		if (Application.isLoadingSave)
		{
			return;
		}
		if (this.IsDestroyed)
		{
			return;
		}
		if (this.net == null)
		{
			return;
		}
		if (!this.isSpawned)
		{
			return;
		}
		List<Connection> subscribers = this.GetSubscribers();
		if (subscribers == null || subscribers.Count == 0)
		{
			return;
		}
		base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 2, "SendNetworkUpdate_Position");
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(10);
			Network.Net.sv.write.EntityID(this.net.ID);
			Network.Net.sv.write.Vector3(this.GetNetworkPosition());
			Network.Net.sv.write.Vector3(this.GetNetworkRotation());
			Write write = Network.Net.sv.write;
			SendInfo sendInfo = new SendInfo(subscribers);
			sendInfo.method = 1;
			sendInfo.priority = 0;
			write.Send(sendInfo);
		}
	}

	// Token: 0x060014B4 RID: 5300 RVA: 0x000785EC File Offset: 0x000767EC
	public virtual Vector3 GetNetworkPosition()
	{
		return base.transform.localPosition;
	}

	// Token: 0x060014B5 RID: 5301 RVA: 0x000785FC File Offset: 0x000767FC
	public virtual Vector3 GetNetworkRotation()
	{
		return base.transform.localRotation.eulerAngles;
	}

	// Token: 0x060014B6 RID: 5302 RVA: 0x0007861C File Offset: 0x0007681C
	private void ToStream(Stream stream, global::BaseNetworkable.SaveInfo saveInfo)
	{
		using (saveInfo.msg = Facepunch.Pool.Get<ProtoBuf.Entity>())
		{
			this.Save(saveInfo);
			if (saveInfo.msg.baseEntity == null)
			{
				Debug.LogError(this + ": ToStream - no BaseEntity!?");
			}
			if (saveInfo.msg.baseNetworkable == null)
			{
				Debug.LogError(this + ": ToStream - no baseNetworkable!?");
			}
			saveInfo.msg.ToProto(stream);
			this.PostSave(saveInfo);
		}
	}

	// Token: 0x060014B7 RID: 5303 RVA: 0x000786B8 File Offset: 0x000768B8
	public virtual bool CanUseNetworkCache(Connection connection)
	{
		return ConVar.Server.netcache;
	}

	// Token: 0x060014B8 RID: 5304 RVA: 0x000786C0 File Offset: 0x000768C0
	public void ToStreamForNetwork(Stream stream, global::BaseNetworkable.SaveInfo saveInfo)
	{
		if (!this.CanUseNetworkCache(saveInfo.forConnection))
		{
			this.ToStream(stream, saveInfo);
			return;
		}
		if (this._NetworkCache == null)
		{
			this._NetworkCache = ((global::BaseNetworkable.EntityMemoryStreamPool.Count <= 0) ? new MemoryStream(8) : (this._NetworkCache = global::BaseNetworkable.EntityMemoryStreamPool.Dequeue()));
			this.ToStream(this._NetworkCache, saveInfo);
			ConVar.Server.netcachesize += (int)this._NetworkCache.Length;
		}
		this._NetworkCache.WriteTo(stream);
	}

	// Token: 0x060014B9 RID: 5305 RVA: 0x00078758 File Offset: 0x00076958
	public void InvalidateNetworkCache()
	{
		using (TimeWarning.New("InvalidateNetworkCache", 0.1f))
		{
			if (this._SaveCache != null)
			{
				ConVar.Server.savecachesize -= (int)this._SaveCache.Length;
				this._SaveCache.SetLength(0L);
				this._SaveCache.Position = 0L;
				global::BaseNetworkable.EntityMemoryStreamPool.Enqueue(this._SaveCache);
				this._SaveCache = null;
			}
			if (this._NetworkCache != null)
			{
				ConVar.Server.netcachesize -= (int)this._NetworkCache.Length;
				this._NetworkCache.SetLength(0L);
				this._NetworkCache.Position = 0L;
				global::BaseNetworkable.EntityMemoryStreamPool.Enqueue(this._NetworkCache);
				this._NetworkCache = null;
			}
			base.LogEntry(global::BaseMonoBehaviour.LogEntryType.Network, 3, "InvalidateNetworkCache");
		}
	}

	// Token: 0x060014BA RID: 5306 RVA: 0x0007884C File Offset: 0x00076A4C
	public MemoryStream GetSaveCache()
	{
		if (this._SaveCache == null)
		{
			if (global::BaseNetworkable.EntityMemoryStreamPool.Count > 0)
			{
				this._SaveCache = global::BaseNetworkable.EntityMemoryStreamPool.Dequeue();
			}
			else
			{
				this._SaveCache = new MemoryStream(8);
			}
			global::BaseNetworkable.SaveInfo saveInfo = new global::BaseNetworkable.SaveInfo
			{
				forDisk = true
			};
			this.ToStream(this._SaveCache, saveInfo);
			ConVar.Server.savecachesize += (int)this._SaveCache.Length;
		}
		return this._SaveCache;
	}

	// Token: 0x060014BB RID: 5307 RVA: 0x000788D4 File Offset: 0x00076AD4
	public virtual void UpdateNetworkGroup()
	{
		Assert.IsTrue(this.isServer, "UpdateNetworkGroup called on clientside entity!");
		if (this.net == null)
		{
			return;
		}
		using (TimeWarning.New("UpdateGroups", 0.1f))
		{
			if (this.net.UpdateGroups(base.transform.position))
			{
				this.SendNetworkGroupChange();
			}
		}
	}

	// Token: 0x04000F35 RID: 3893
	[ReadOnly]
	[Header("BaseNetworkable")]
	public uint prefabID;

	// Token: 0x04000F36 RID: 3894
	[Tooltip("If enabled the entity will send to everyone on the server - regardless of position")]
	public bool globalBroadcast;

	// Token: 0x04000F37 RID: 3895
	[NonSerialized]
	public Networkable net;

	// Token: 0x04000F39 RID: 3897
	private string _prefabName;

	// Token: 0x04000F3A RID: 3898
	private string _prefabNameWithoutExtension;

	// Token: 0x04000F3B RID: 3899
	public static global::BaseNetworkable.EntityRealm serverEntities = new global::BaseNetworkable.EntityRealmServer();

	// Token: 0x04000F3C RID: 3900
	private const bool isServersideEntity = true;

	// Token: 0x04000F3D RID: 3901
	private bool _limitedNetworking;

	// Token: 0x04000F3E RID: 3902
	[NonSerialized]
	public List<global::BaseEntity> children = new List<global::BaseEntity>();

	// Token: 0x04000F3F RID: 3903
	private int creationFrame;

	// Token: 0x04000F40 RID: 3904
	private bool isSpawned;

	// Token: 0x04000F41 RID: 3905
	private MemoryStream _NetworkCache;

	// Token: 0x04000F42 RID: 3906
	public static Queue<MemoryStream> EntityMemoryStreamPool = new Queue<MemoryStream>();

	// Token: 0x04000F43 RID: 3907
	private MemoryStream _SaveCache;

	// Token: 0x0200035D RID: 861
	public struct SaveInfo
	{
		// Token: 0x060014BE RID: 5310 RVA: 0x0007897C File Offset: 0x00076B7C
		internal bool SendingTo(Connection ownerConnection)
		{
			return ownerConnection != null && this.forConnection != null && this.forConnection == ownerConnection;
		}

		// Token: 0x04000F44 RID: 3908
		public ProtoBuf.Entity msg;

		// Token: 0x04000F45 RID: 3909
		public bool forDisk;

		// Token: 0x04000F46 RID: 3910
		public Connection forConnection;
	}

	// Token: 0x0200035E RID: 862
	public struct LoadInfo
	{
		// Token: 0x04000F47 RID: 3911
		public ProtoBuf.Entity msg;

		// Token: 0x04000F48 RID: 3912
		public bool fromDisk;
	}

	// Token: 0x0200035F RID: 863
	public class EntityRealmServer : global::BaseNetworkable.EntityRealm
	{
		// Token: 0x1700017F RID: 383
		// (get) Token: 0x060014C0 RID: 5312 RVA: 0x000789A4 File Offset: 0x00076BA4
		protected override Manager visibilityManager
		{
			get
			{
				return (Network.Net.sv == null) ? null : Network.Net.sv.visibility;
			}
		}
	}

	// Token: 0x02000360 RID: 864
	public abstract class EntityRealm : IEnumerable<global::BaseNetworkable>, IEnumerable
	{
		// Token: 0x17000180 RID: 384
		// (get) Token: 0x060014C2 RID: 5314 RVA: 0x000789D4 File Offset: 0x00076BD4
		public int Count
		{
			get
			{
				return this.entityList.Count;
			}
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x060014C3 RID: 5315
		protected abstract Manager visibilityManager { get; }

		// Token: 0x060014C4 RID: 5316 RVA: 0x000789E4 File Offset: 0x00076BE4
		public global::BaseNetworkable Find(uint uid)
		{
			global::BaseNetworkable result = null;
			if (!this.entityList.TryGetValue(uid, ref result))
			{
				return null;
			}
			return result;
		}

		// Token: 0x060014C5 RID: 5317 RVA: 0x00078A0C File Offset: 0x00076C0C
		public void RegisterID(global::BaseNetworkable ent)
		{
			if (ent.net != null)
			{
				if (this.entityList.Contains(ent.net.ID))
				{
					this.entityList[ent.net.ID] = ent;
				}
				else
				{
					this.entityList.Add(ent.net.ID, ent);
				}
			}
		}

		// Token: 0x060014C6 RID: 5318 RVA: 0x00078A74 File Offset: 0x00076C74
		public void UnregisterID(global::BaseNetworkable ent)
		{
			if (ent.net != null)
			{
				this.entityList.Remove(ent.net.ID);
			}
		}

		// Token: 0x060014C7 RID: 5319 RVA: 0x00078A98 File Offset: 0x00076C98
		public Group FindGroup(uint uid)
		{
			Manager visibilityManager = this.visibilityManager;
			return (visibilityManager == null) ? null : visibilityManager.Get(uid);
		}

		// Token: 0x060014C8 RID: 5320 RVA: 0x00078AC4 File Offset: 0x00076CC4
		public Group TryFindGroup(uint uid)
		{
			Manager visibilityManager = this.visibilityManager;
			return (visibilityManager == null) ? null : visibilityManager.TryGet(uid);
		}

		// Token: 0x060014C9 RID: 5321 RVA: 0x00078AF0 File Offset: 0x00076CF0
		public void FindInGroup(uint uid, List<global::BaseNetworkable> list)
		{
			Group group = this.TryFindGroup(uid);
			if (group == null)
			{
				return;
			}
			int count = group.networkables.Values.Count;
			Networkable[] buffer = group.networkables.Values.Buffer;
			for (int i = 0; i < count; i++)
			{
				Networkable networkable = buffer[i];
				global::BaseNetworkable baseNetworkable = this.Find(networkable.ID);
				if (!(baseNetworkable == null))
				{
					if (baseNetworkable.net != null)
					{
						if (baseNetworkable.net.group != null)
						{
							if (baseNetworkable.net.group.ID != uid)
							{
								Debug.LogWarning("Group ID mismatch: " + baseNetworkable.ToString());
							}
							else
							{
								list.Add(baseNetworkable);
							}
						}
					}
				}
			}
		}

		// Token: 0x060014CA RID: 5322 RVA: 0x00078BC8 File Offset: 0x00076DC8
		public IEnumerator<global::BaseNetworkable> GetEnumerator()
		{
			return this.entityList.Values.GetEnumerator();
		}

		// Token: 0x060014CB RID: 5323 RVA: 0x00078BDC File Offset: 0x00076DDC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x04000F49 RID: 3913
		private ListDictionary<uint, global::BaseNetworkable> entityList = new ListDictionary<uint, global::BaseNetworkable>(8);
	}

	// Token: 0x02000361 RID: 865
	public enum DestroyMode : byte
	{
		// Token: 0x04000F4B RID: 3915
		None,
		// Token: 0x04000F4C RID: 3916
		Gib
	}
}
