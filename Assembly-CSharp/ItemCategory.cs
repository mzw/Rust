﻿using System;

// Token: 0x020004D0 RID: 1232
public enum ItemCategory
{
	// Token: 0x04001557 RID: 5463
	Weapon,
	// Token: 0x04001558 RID: 5464
	Construction,
	// Token: 0x04001559 RID: 5465
	Items,
	// Token: 0x0400155A RID: 5466
	Resources,
	// Token: 0x0400155B RID: 5467
	Attire,
	// Token: 0x0400155C RID: 5468
	Tool,
	// Token: 0x0400155D RID: 5469
	Medical,
	// Token: 0x0400155E RID: 5470
	Food,
	// Token: 0x0400155F RID: 5471
	Ammunition,
	// Token: 0x04001560 RID: 5472
	Traps,
	// Token: 0x04001561 RID: 5473
	Misc,
	// Token: 0x04001562 RID: 5474
	All,
	// Token: 0x04001563 RID: 5475
	Common,
	// Token: 0x04001564 RID: 5476
	Component,
	// Token: 0x04001565 RID: 5477
	Search
}
