﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200072F RID: 1839
public class VitalInfo : MonoBehaviour, IClientComponent
{
	// Token: 0x04001F16 RID: 7958
	public global::VitalInfo.Vital VitalType;

	// Token: 0x04001F17 RID: 7959
	public Animator animator;

	// Token: 0x04001F18 RID: 7960
	public Text text;

	// Token: 0x02000730 RID: 1840
	public enum Vital
	{
		// Token: 0x04001F1A RID: 7962
		BuildingBlocked,
		// Token: 0x04001F1B RID: 7963
		CanBuild,
		// Token: 0x04001F1C RID: 7964
		Crafting,
		// Token: 0x04001F1D RID: 7965
		CraftLevel1,
		// Token: 0x04001F1E RID: 7966
		CraftLevel2,
		// Token: 0x04001F1F RID: 7967
		CraftLevel3,
		// Token: 0x04001F20 RID: 7968
		DecayProtected,
		// Token: 0x04001F21 RID: 7969
		Decaying
	}
}
