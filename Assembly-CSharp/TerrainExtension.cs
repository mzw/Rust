﻿using System;
using Facepunch.Extend;
using UnityEngine;

// Token: 0x02000565 RID: 1381
[RequireComponent(typeof(global::TerrainMeta))]
public abstract class TerrainExtension : MonoBehaviour
{
	// Token: 0x06001CCE RID: 7374 RVA: 0x000A1528 File Offset: 0x0009F728
	public void Init(Terrain terrain, global::TerrainConfig config)
	{
		this.terrain = terrain;
		this.config = config;
	}

	// Token: 0x06001CCF RID: 7375 RVA: 0x000A1538 File Offset: 0x0009F738
	public virtual void Setup()
	{
	}

	// Token: 0x06001CD0 RID: 7376 RVA: 0x000A153C File Offset: 0x0009F73C
	public virtual void PostSetup()
	{
	}

	// Token: 0x06001CD1 RID: 7377 RVA: 0x000A1540 File Offset: 0x0009F740
	public void LogSize(object obj, ulong size)
	{
		Debug.Log(obj.GetType() + " allocated: " + NumberExtensions.FormatBytes<ulong>(size, false));
	}

	// Token: 0x040017FC RID: 6140
	[NonSerialized]
	public bool isInitialized;

	// Token: 0x040017FD RID: 6141
	internal Terrain terrain;

	// Token: 0x040017FE RID: 6142
	internal global::TerrainConfig config;
}
