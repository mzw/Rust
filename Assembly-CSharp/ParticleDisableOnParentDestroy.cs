﻿using System;
using UnityEngine;

// Token: 0x02000271 RID: 625
public class ParticleDisableOnParentDestroy : MonoBehaviour, global::IOnParentDestroying
{
	// Token: 0x060010A1 RID: 4257 RVA: 0x00064368 File Offset: 0x00062568
	public void OnParentDestroying()
	{
		base.transform.parent = null;
		base.GetComponent<ParticleSystem>().enableEmission = false;
		if (this.destroyAfterSeconds > 0f)
		{
			global::GameManager.Destroy(base.gameObject, this.destroyAfterSeconds);
		}
	}

	// Token: 0x04000B92 RID: 2962
	public float destroyAfterSeconds;
}
