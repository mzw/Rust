﻿using System;
using UnityEngine;

// Token: 0x020007F6 RID: 2038
public class ExplosionsBillboard : MonoBehaviour
{
	// Token: 0x060025B0 RID: 9648 RVA: 0x000D06D4 File Offset: 0x000CE8D4
	private void Awake()
	{
		if (this.AutoInitCamera)
		{
			this.Camera = Camera.main;
			this.Active = true;
		}
		this.t = base.transform;
		Vector3 localScale = this.t.parent.transform.localScale;
		localScale.z = localScale.x;
		this.t.parent.transform.localScale = localScale;
		this.camT = this.Camera.transform;
		Transform parent = this.t.parent;
		this.myContainer = new GameObject
		{
			name = "Billboard_" + this.t.gameObject.name
		};
		this.contT = this.myContainer.transform;
		this.contT.position = this.t.position;
		this.t.parent = this.myContainer.transform;
		this.contT.parent = parent;
	}

	// Token: 0x060025B1 RID: 9649 RVA: 0x000D07D8 File Offset: 0x000CE9D8
	private void Update()
	{
		if (this.Active)
		{
			this.contT.LookAt(this.contT.position + this.camT.rotation * Vector3.back, this.camT.rotation * Vector3.up);
		}
	}

	// Token: 0x040021CC RID: 8652
	public Camera Camera;

	// Token: 0x040021CD RID: 8653
	public bool Active = true;

	// Token: 0x040021CE RID: 8654
	public bool AutoInitCamera = true;

	// Token: 0x040021CF RID: 8655
	private GameObject myContainer;

	// Token: 0x040021D0 RID: 8656
	private Transform t;

	// Token: 0x040021D1 RID: 8657
	private Transform camT;

	// Token: 0x040021D2 RID: 8658
	private Transform contT;
}
