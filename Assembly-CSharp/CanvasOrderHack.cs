﻿using System;
using UnityEngine;

// Token: 0x02000230 RID: 560
public class CanvasOrderHack : MonoBehaviour
{
	// Token: 0x06001007 RID: 4103 RVA: 0x00061764 File Offset: 0x0005F964
	private void OnEnable()
	{
		foreach (Canvas canvas in base.GetComponentsInChildren<Canvas>(true))
		{
			if (canvas.overrideSorting)
			{
				canvas.sortingOrder++;
			}
		}
		foreach (Canvas canvas2 in base.GetComponentsInChildren<Canvas>(true))
		{
			if (canvas2.overrideSorting)
			{
				canvas2.sortingOrder--;
			}
		}
	}
}
