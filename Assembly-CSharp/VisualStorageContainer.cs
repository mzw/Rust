﻿using System;
using ProtoBuf;
using UnityEngine;

// Token: 0x020003B1 RID: 945
public class VisualStorageContainer : global::LootContainer
{
	// Token: 0x06001668 RID: 5736 RVA: 0x00081A70 File Offset: 0x0007FC70
	public override void ServerInit()
	{
		base.ServerInit();
	}

	// Token: 0x06001669 RID: 5737 RVA: 0x00081A78 File Offset: 0x0007FC78
	public override void OnItemAddedOrRemoved(global::Item item, bool added)
	{
		base.OnItemAddedOrRemoved(item, added);
	}

	// Token: 0x0600166A RID: 5738 RVA: 0x00081A84 File Offset: 0x0007FC84
	public override void PopulateLoot()
	{
		base.PopulateLoot();
		for (int i = 0; i < this.inventorySlots; i++)
		{
			global::Item slot = this.inventory.GetSlot(i);
			if (slot != null)
			{
				global::DroppedItem component = slot.Drop(this.displayNodes[i].transform.position + new Vector3(0f, 0.25f, 0f), Vector3.zero, this.displayNodes[i].transform.rotation).GetComponent<global::DroppedItem>();
				if (component)
				{
					base.ReceiveCollisionMessages(false);
					base.CancelInvoke(new Action(component.IdleDestroy));
					Rigidbody componentInChildren = component.GetComponentInChildren<Rigidbody>();
					if (componentInChildren)
					{
						componentInChildren.constraints = 10;
					}
				}
			}
		}
	}

	// Token: 0x0600166B RID: 5739 RVA: 0x00081B54 File Offset: 0x0007FD54
	public void ClearRigidBodies()
	{
		if (this.displayModels == null)
		{
			return;
		}
		foreach (global::VisualStorageContainer.DisplayModel displayModel in this.displayModels)
		{
			if (displayModel != null)
			{
				Rigidbody componentInChildren = displayModel.displayModel.GetComponentInChildren<Rigidbody>();
				Object.Destroy(componentInChildren);
			}
		}
	}

	// Token: 0x0600166C RID: 5740 RVA: 0x00081BAC File Offset: 0x0007FDAC
	public void SetItemsVisible(bool vis)
	{
		if (this.displayModels == null)
		{
			return;
		}
		foreach (global::VisualStorageContainer.DisplayModel displayModel in this.displayModels)
		{
			if (displayModel != null)
			{
				LODGroup componentInChildren = displayModel.displayModel.GetComponentInChildren<LODGroup>();
				if (componentInChildren)
				{
					componentInChildren.localReferencePoint = ((!vis) ? new Vector3(10000f, 10000f, 10000f) : Vector3.zero);
				}
				else
				{
					Debug.Log("VisualStorageContainer item missing LODGroup" + displayModel.displayModel.gameObject.name);
				}
			}
		}
	}

	// Token: 0x0600166D RID: 5741 RVA: 0x00081C54 File Offset: 0x0007FE54
	public void ItemUpdateComplete()
	{
		this.ClearRigidBodies();
		this.SetItemsVisible(true);
	}

	// Token: 0x0600166E RID: 5742 RVA: 0x00081C64 File Offset: 0x0007FE64
	public void UpdateVisibleItems(ProtoBuf.ItemContainer msg)
	{
		for (int i = 0; i < this.displayModels.Length; i++)
		{
			global::VisualStorageContainer.DisplayModel displayModel = this.displayModels[i];
			if (displayModel != null)
			{
				Object.Destroy(displayModel.displayModel);
				this.displayModels[i] = null;
			}
		}
		if (msg == null)
		{
			return;
		}
		foreach (ProtoBuf.Item item in msg.contents)
		{
			global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(item.itemid);
			GameObject gameObject;
			if (itemDefinition.worldModelPrefab != null && itemDefinition.worldModelPrefab.isValid)
			{
				gameObject = itemDefinition.worldModelPrefab.Instantiate(null);
			}
			else
			{
				gameObject = Object.Instantiate<GameObject>(this.defaultDisplayModel);
			}
			if (gameObject)
			{
				gameObject.transform.position = this.displayNodes[item.slot].transform.position + new Vector3(0f, 0.25f, 0f);
				gameObject.transform.rotation = this.displayNodes[item.slot].transform.rotation;
				Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
				rigidbody.mass = 1f;
				rigidbody.drag = 0.1f;
				rigidbody.angularDrag = 0.1f;
				rigidbody.interpolation = 1;
				rigidbody.constraints = 10;
				this.displayModels[item.slot].displayModel = gameObject;
				this.displayModels[item.slot].slot = item.slot;
				this.displayModels[item.slot].def = itemDefinition;
				gameObject.SetActive(true);
			}
		}
		this.SetItemsVisible(false);
		base.CancelInvoke(new Action(this.ItemUpdateComplete));
		base.Invoke(new Action(this.ItemUpdateComplete), 1f);
	}

	// Token: 0x040010DB RID: 4315
	public global::VisualStorageContainerNode[] displayNodes;

	// Token: 0x040010DC RID: 4316
	public global::VisualStorageContainer.DisplayModel[] displayModels;

	// Token: 0x040010DD RID: 4317
	public Transform nodeParent;

	// Token: 0x040010DE RID: 4318
	public GameObject defaultDisplayModel;

	// Token: 0x020003B2 RID: 946
	[Serializable]
	public class DisplayModel
	{
		// Token: 0x040010DF RID: 4319
		public GameObject displayModel;

		// Token: 0x040010E0 RID: 4320
		public global::ItemDefinition def;

		// Token: 0x040010E1 RID: 4321
		public int slot;
	}
}
