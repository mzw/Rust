﻿using System;
using UnityEngine;

// Token: 0x0200063E RID: 1598
[CreateAssetMenu(menuName = "Rust/Hair Dye Collection")]
public class HairDyeCollection : ScriptableObject
{
	// Token: 0x06002054 RID: 8276 RVA: 0x000B913C File Offset: 0x000B733C
	public global::HairDye Get(float seed)
	{
		if (this.Variations.Length > 0)
		{
			return this.Variations[Mathf.Clamp(Mathf.FloorToInt(seed * (float)this.Variations.Length), 0, this.Variations.Length - 1)];
		}
		return null;
	}

	// Token: 0x04001B3A RID: 6970
	public global::HairDye[] Variations;
}
