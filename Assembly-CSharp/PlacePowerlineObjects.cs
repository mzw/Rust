﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

// Token: 0x020005C6 RID: 1478
public class PlacePowerlineObjects : global::ProceduralComponent
{
	// Token: 0x06001E99 RID: 7833 RVA: 0x000AC02C File Offset: 0x000AA22C
	public override void Process(uint seed)
	{
		List<global::PathList> powerlines = global::TerrainMeta.Path.Powerlines;
		foreach (global::PathList pathList in powerlines)
		{
			foreach (global::PathList.BasicObject obj in this.Start)
			{
				pathList.TrimStart(obj);
			}
			foreach (global::PathList.BasicObject obj2 in this.End)
			{
				pathList.TrimEnd(obj2);
			}
			foreach (global::PathList.BasicObject obj3 in this.Start)
			{
				pathList.SpawnStart(ref seed, obj3);
			}
			foreach (global::PathList.BasicObject obj4 in this.End)
			{
				pathList.SpawnEnd(ref seed, obj4);
			}
			foreach (global::PathList.PathObject obj5 in this.Path)
			{
				pathList.SpawnAlong(ref seed, obj5);
			}
			foreach (global::PathList.SideObject obj6 in this.Side)
			{
				pathList.SpawnSide(ref seed, obj6);
			}
			pathList.ResetTrims();
		}
	}

	// Token: 0x0400196D RID: 6509
	public global::PathList.BasicObject[] Start;

	// Token: 0x0400196E RID: 6510
	public global::PathList.BasicObject[] End;

	// Token: 0x0400196F RID: 6511
	public global::PathList.SideObject[] Side;

	// Token: 0x04001970 RID: 6512
	[FormerlySerializedAs("PowerlineObjects")]
	public global::PathList.PathObject[] Path;
}
