﻿using System;
using UnityEngine;

// Token: 0x020001C0 RID: 448
[Serializable]
public struct StateTimer
{
	// Token: 0x06000E8F RID: 3727 RVA: 0x0005BCE4 File Offset: 0x00059EE4
	public void Activate(float seconds, Action onFinished = null)
	{
		this.ReleaseTime = Time.time + seconds;
		this.OnFinished = onFinished;
	}

	// Token: 0x170000DB RID: 219
	// (get) Token: 0x06000E90 RID: 3728 RVA: 0x0005BCFC File Offset: 0x00059EFC
	public bool IsActive
	{
		get
		{
			bool flag = this.ReleaseTime > Time.time;
			if (!flag && this.OnFinished != null)
			{
				this.OnFinished();
				this.OnFinished = null;
			}
			return flag;
		}
	}

	// Token: 0x040008A4 RID: 2212
	public float ReleaseTime;

	// Token: 0x040008A5 RID: 2213
	public Action OnFinished;
}
