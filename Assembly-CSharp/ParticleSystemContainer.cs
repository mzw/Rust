﻿using System;
using UnityEngine;

// Token: 0x02000754 RID: 1876
public class ParticleSystemContainer : MonoBehaviour
{
	// Token: 0x06002312 RID: 8978 RVA: 0x000C29F0 File Offset: 0x000C0BF0
	public void Play()
	{
	}

	// Token: 0x06002313 RID: 8979 RVA: 0x000C29F4 File Offset: 0x000C0BF4
	public void Pause()
	{
	}

	// Token: 0x06002314 RID: 8980 RVA: 0x000C29F8 File Offset: 0x000C0BF8
	public void Stop()
	{
	}

	// Token: 0x06002315 RID: 8981 RVA: 0x000C29FC File Offset: 0x000C0BFC
	public void Clear()
	{
	}
}
