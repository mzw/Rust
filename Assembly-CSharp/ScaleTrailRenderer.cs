﻿using System;
using UnityEngine;

// Token: 0x02000330 RID: 816
public class ScaleTrailRenderer : global::ScaleRenderer
{
	// Token: 0x060013B4 RID: 5044 RVA: 0x00073A20 File Offset: 0x00071C20
	public override void GatherInitialValues()
	{
		base.GatherInitialValues();
		if (this.myRenderer)
		{
			this.trailRenderer = this.myRenderer.GetComponent<TrailRenderer>();
		}
		else
		{
			this.trailRenderer = base.GetComponentInChildren<TrailRenderer>();
		}
		this.startWidth = this.trailRenderer.startWidth;
		this.endWidth = this.trailRenderer.endWidth;
		this.duration = this.trailRenderer.time;
	}

	// Token: 0x060013B5 RID: 5045 RVA: 0x00073A98 File Offset: 0x00071C98
	public override void SetScale_Internal(float scale)
	{
		base.SetScale_Internal(scale);
		this.trailRenderer.startWidth = this.startWidth * scale;
		this.trailRenderer.endWidth = this.endWidth * scale;
		this.trailRenderer.time = this.duration * scale;
	}

	// Token: 0x04000E9E RID: 3742
	private TrailRenderer trailRenderer;

	// Token: 0x04000E9F RID: 3743
	[NonSerialized]
	private float startWidth;

	// Token: 0x04000EA0 RID: 3744
	[NonSerialized]
	private float endWidth;

	// Token: 0x04000EA1 RID: 3745
	[NonSerialized]
	private float duration;
}
