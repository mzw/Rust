﻿using System;

// Token: 0x02000223 RID: 547
public class SocketMod : global::PrefabAttribute
{
	// Token: 0x06000FDC RID: 4060 RVA: 0x00060A44 File Offset: 0x0005EC44
	public virtual bool DoCheck(global::Construction.Placement place)
	{
		return false;
	}

	// Token: 0x06000FDD RID: 4061 RVA: 0x00060A48 File Offset: 0x0005EC48
	public virtual void ModifyPlacement(global::Construction.Placement place)
	{
	}

	// Token: 0x06000FDE RID: 4062 RVA: 0x00060A4C File Offset: 0x0005EC4C
	protected override Type GetIndexedType()
	{
		return typeof(global::SocketMod);
	}

	// Token: 0x04000A97 RID: 2711
	[NonSerialized]
	public global::Socket_Base baseSocket;

	// Token: 0x04000A98 RID: 2712
	public global::Translate.Phrase FailedPhrase;
}
