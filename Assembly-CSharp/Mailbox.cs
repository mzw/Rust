﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200007C RID: 124
public class Mailbox : global::StorageContainer
{
	// Token: 0x06000868 RID: 2152 RVA: 0x00036B68 File Offset: 0x00034D68
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("Mailbox.OnRpcMessage", 0.1f))
		{
			if (rpc == 1057327026u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Submit ");
				}
				using (TimeWarning.New("RPC_Submit", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Submit(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Submit");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x1700005E RID: 94
	// (get) Token: 0x06000869 RID: 2153 RVA: 0x00036CBC File Offset: 0x00034EBC
	public int mailInputSlot
	{
		get
		{
			return this.inventorySlots - 1;
		}
	}

	// Token: 0x0600086A RID: 2154 RVA: 0x00036CC8 File Offset: 0x00034EC8
	public virtual bool PlayerIsOwner(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanUseMailbox", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return player.CanBuild();
	}

	// Token: 0x0600086B RID: 2155 RVA: 0x00036D08 File Offset: 0x00034F08
	public bool IsFull()
	{
		return this.shouldMarkAsFull && base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x0600086C RID: 2156 RVA: 0x00036D24 File Offset: 0x00034F24
	public void MarkFull(bool full)
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved1, this.shouldMarkAsFull && full, false);
	}

	// Token: 0x0600086D RID: 2157 RVA: 0x00036D44 File Offset: 0x00034F44
	public override bool PlayerOpenLoot(global::BasePlayer player, string panelToOpen)
	{
		return base.PlayerOpenLoot(player, (!this.PlayerIsOwner(player)) ? panelToOpen : this.ownerPanel);
	}

	// Token: 0x0600086E RID: 2158 RVA: 0x00036D68 File Offset: 0x00034F68
	public override bool CanOpenLootPanel(global::BasePlayer player, string panelName = "")
	{
		if (panelName == this.ownerPanel)
		{
			return this.PlayerIsOwner(player) && base.CanOpenLootPanel(player, panelName);
		}
		return this.HasFreeSpace() || !this.shouldMarkAsFull;
	}

	// Token: 0x0600086F RID: 2159 RVA: 0x00036DB4 File Offset: 0x00034FB4
	private bool HasFreeSpace()
	{
		return this.GetFreeSlot() != -1;
	}

	// Token: 0x06000870 RID: 2160 RVA: 0x00036DC4 File Offset: 0x00034FC4
	private int GetFreeSlot()
	{
		for (int i = 0; i < this.mailInputSlot; i++)
		{
			if (this.inventory.GetSlot(i) == null)
			{
				return i;
			}
		}
		return -1;
	}

	// Token: 0x06000871 RID: 2161 RVA: 0x00036DFC File Offset: 0x00034FFC
	public virtual bool MoveItemToStorage(global::Item item)
	{
		item.RemoveFromContainer();
		return item.MoveToContainer(this.inventory, -1, true);
	}

	// Token: 0x06000872 RID: 2162 RVA: 0x00036E1C File Offset: 0x0003501C
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		if (this.autoSubmitWhenClosed)
		{
			this.SubmitInputItems(player);
		}
		if (this.IsFull())
		{
			global::Item slot = this.inventory.GetSlot(this.mailInputSlot);
			if (slot != null)
			{
				slot.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
			}
		}
		base.PlayerStoppedLooting(player);
		if (this.PlayerIsOwner(player))
		{
			base.SetFlag(global::BaseEntity.Flags.On, false, false);
		}
	}

	// Token: 0x06000873 RID: 2163 RVA: 0x00036E98 File Offset: 0x00035098
	[global::BaseEntity.RPC_Server]
	public void RPC_Submit(global::BaseEntity.RPCMessage msg)
	{
		if (this.IsFull())
		{
			return;
		}
		global::BasePlayer player = msg.player;
		this.SubmitInputItems(player);
	}

	// Token: 0x06000874 RID: 2164 RVA: 0x00036EC0 File Offset: 0x000350C0
	public void SubmitInputItems(global::BasePlayer fromPlayer)
	{
		global::Item slot = this.inventory.GetSlot(this.mailInputSlot);
		if (this.IsFull())
		{
			return;
		}
		if (slot != null)
		{
			if (this.MoveItemToStorage(slot))
			{
				if (slot.position != this.mailInputSlot)
				{
					global::Effect.server.Run(this.mailDropSound.resourcePath, this.GetDropPosition(), default(Vector3), null, false);
					if (fromPlayer != null && !this.PlayerIsOwner(fromPlayer))
					{
						base.SetFlag(global::BaseEntity.Flags.On, true, false);
					}
				}
			}
			else
			{
				slot.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
			}
		}
	}

	// Token: 0x06000875 RID: 2165 RVA: 0x00036F78 File Offset: 0x00035178
	public override void OnItemAddedOrRemoved(global::Item item, bool added)
	{
		this.MarkFull(!this.HasFreeSpace());
		base.OnItemAddedOrRemoved(item, added);
	}

	// Token: 0x06000876 RID: 2166 RVA: 0x00036F94 File Offset: 0x00035194
	public override bool CanMoveFrom(global::BasePlayer player, global::Item item)
	{
		bool flag = this.PlayerIsOwner(player);
		if (!flag)
		{
			flag = (item == this.inventory.GetSlot(this.mailInputSlot));
		}
		return flag && base.CanMoveFrom(player, item);
	}

	// Token: 0x040003EB RID: 1003
	public string ownerPanel;

	// Token: 0x040003EC RID: 1004
	public global::GameObjectRef mailDropSound;

	// Token: 0x040003ED RID: 1005
	public bool autoSubmitWhenClosed;

	// Token: 0x040003EE RID: 1006
	public bool shouldMarkAsFull;
}
