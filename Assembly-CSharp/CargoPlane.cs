﻿using System;
using Oxide.Core;
using UnityEngine;

// Token: 0x020003EE RID: 1006
public class CargoPlane : global::BaseEntity
{
	// Token: 0x06001751 RID: 5969 RVA: 0x00085D14 File Offset: 0x00083F14
	public void InitDropPosition(Vector3 newDropPosition)
	{
		this.dropPosition = newDropPosition;
		this.dropPosition.y = 0f;
	}

	// Token: 0x06001752 RID: 5970 RVA: 0x00085D30 File Offset: 0x00083F30
	public override void ServerInit()
	{
		base.ServerInit();
		if (this.dropPosition == Vector3.zero)
		{
			this.dropPosition = this.RandomDropPosition();
		}
		this.UpdateDropPosition(this.dropPosition);
	}

	// Token: 0x06001753 RID: 5971 RVA: 0x00085D68 File Offset: 0x00083F68
	public Vector3 RandomDropPosition()
	{
		Vector3 vector = Vector3.zero;
		float num = 100f;
		float x = global::TerrainMeta.Size.x;
		do
		{
			vector = Vector3Ex.Range(-(x / 3f), x / 3f);
		}
		while (this.filter.GetFactor(vector) == 0f && (num -= 1f) > 0f);
		vector.y = 0f;
		return vector;
	}

	// Token: 0x06001754 RID: 5972 RVA: 0x00085DDC File Offset: 0x00083FDC
	public void UpdateDropPosition(Vector3 newDropPosition)
	{
		float x = global::TerrainMeta.Size.x;
		float y = global::TerrainMeta.HighestPoint.y + 250f;
		this.startPos = Vector3Ex.Range(-1f, 1f);
		this.startPos.y = 0f;
		this.startPos.Normalize();
		this.startPos *= x * 2f;
		this.startPos.y = y;
		this.endPos = this.startPos * -1f;
		this.endPos.y = this.startPos.y;
		this.startPos += newDropPosition;
		this.endPos += newDropPosition;
		this.secondsToTake = Vector3.Distance(this.startPos, this.endPos) / 50f;
		this.secondsToTake *= Random.Range(0.95f, 1.05f);
		base.transform.position = this.startPos;
		base.transform.rotation = Quaternion.LookRotation(this.endPos - this.startPos);
		this.dropPosition = newDropPosition;
		Interface.CallHook("OnAirdrop", new object[]
		{
			this,
			newDropPosition
		});
	}

	// Token: 0x06001755 RID: 5973 RVA: 0x00085F48 File Offset: 0x00084148
	private void Update()
	{
		if (!base.isServer)
		{
			return;
		}
		this.secondsTaken += Time.deltaTime;
		float num = Mathf.InverseLerp(0f, this.secondsToTake, this.secondsTaken);
		if (!this.dropped && num >= 0.5f)
		{
			this.dropped = true;
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.prefabDrop.resourcePath, base.transform.position, default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.globalBroadcast = true;
				baseEntity.Spawn();
			}
		}
		base.transform.position = Vector3.Lerp(this.startPos, this.endPos, num);
		base.transform.hasChanged = true;
		if (num >= 1f)
		{
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
	}

	// Token: 0x040011ED RID: 4589
	public global::GameObjectRef prefabDrop;

	// Token: 0x040011EE RID: 4590
	public global::SpawnFilter filter;

	// Token: 0x040011EF RID: 4591
	private Vector3 startPos;

	// Token: 0x040011F0 RID: 4592
	private Vector3 endPos;

	// Token: 0x040011F1 RID: 4593
	private float secondsToTake;

	// Token: 0x040011F2 RID: 4594
	private float secondsTaken;

	// Token: 0x040011F3 RID: 4595
	private bool dropped;

	// Token: 0x040011F4 RID: 4596
	private Vector3 dropPosition = Vector3.zero;
}
