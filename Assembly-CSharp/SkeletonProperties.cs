﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Token: 0x02000471 RID: 1137
[CreateAssetMenu(menuName = "Rust/Skeleton Properties")]
public class SkeletonProperties : ScriptableObject
{
	// Token: 0x060018E0 RID: 6368 RVA: 0x0008C098 File Offset: 0x0008A298
	public void OnValidate()
	{
		if (this.boneReference == null)
		{
			Debug.LogWarning("boneReference is null", this);
			return;
		}
		List<global::SkeletonProperties.BoneProperty> list = this.bones.ToList<global::SkeletonProperties.BoneProperty>();
		List<Transform> allChildren = this.boneReference.transform.GetAllChildren();
		using (List<Transform>.Enumerator enumerator = allChildren.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				Transform child = enumerator.Current;
				if (list.All((global::SkeletonProperties.BoneProperty x) => x.bone != child.gameObject))
				{
					list.Add(new global::SkeletonProperties.BoneProperty
					{
						bone = child.gameObject,
						name = new global::Translate.Phrase(string.Empty, string.Empty)
						{
							token = child.name.ToLower(),
							english = child.name.ToLower()
						}
					});
				}
			}
		}
		this.bones = list.ToArray();
	}

	// Token: 0x060018E1 RID: 6369 RVA: 0x0008C1BC File Offset: 0x0008A3BC
	private void BuildDictionary()
	{
		this.quickLookup = new Dictionary<uint, global::SkeletonProperties.BoneProperty>();
		foreach (global::SkeletonProperties.BoneProperty boneProperty in this.bones)
		{
			uint num = global::StringPool.Get(boneProperty.bone.name);
			if (!this.quickLookup.ContainsKey(num))
			{
				this.quickLookup.Add(num, boneProperty);
			}
			else
			{
				string name = boneProperty.bone.name;
				string name2 = this.quickLookup[num].bone.name;
				Debug.LogWarning(string.Concat(new object[]
				{
					"Duplicate bone id ",
					num,
					" for ",
					name,
					" and ",
					name2
				}));
			}
		}
	}

	// Token: 0x060018E2 RID: 6370 RVA: 0x0008C288 File Offset: 0x0008A488
	public global::SkeletonProperties.BoneProperty FindBone(uint id)
	{
		if (this.quickLookup == null)
		{
			this.BuildDictionary();
		}
		global::SkeletonProperties.BoneProperty result = null;
		if (!this.quickLookup.TryGetValue(id, out result))
		{
			return null;
		}
		return result;
	}

	// Token: 0x04001393 RID: 5011
	public GameObject boneReference;

	// Token: 0x04001394 RID: 5012
	[global::BoneProperty]
	public global::SkeletonProperties.BoneProperty[] bones;

	// Token: 0x04001395 RID: 5013
	[NonSerialized]
	private Dictionary<uint, global::SkeletonProperties.BoneProperty> quickLookup;

	// Token: 0x02000472 RID: 1138
	[Serializable]
	public class BoneProperty
	{
		// Token: 0x04001396 RID: 5014
		public GameObject bone;

		// Token: 0x04001397 RID: 5015
		public global::Translate.Phrase name;

		// Token: 0x04001398 RID: 5016
		public global::HitArea area;
	}
}
