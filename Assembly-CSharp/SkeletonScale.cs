﻿using System;
using UnityEngine;

// Token: 0x0200027F RID: 639
public class SkeletonScale : MonoBehaviour
{
	// Token: 0x060010BB RID: 4283 RVA: 0x000649C4 File Offset: 0x00062BC4
	protected void Awake()
	{
		this.bones = base.GetComponentsInChildren<global::BoneInfoComponent>(true);
	}

	// Token: 0x060010BC RID: 4284 RVA: 0x000649D4 File Offset: 0x00062BD4
	public void UpdateBones(int seedNumber)
	{
		this.seed = seedNumber;
		foreach (global::BoneInfoComponent boneInfoComponent in this.bones)
		{
			if (!(boneInfoComponent.sizeVariation == Vector3.zero))
			{
				Random.State state = Random.state;
				Random.InitState(boneInfoComponent.sizeVariationSeed + this.seed);
				boneInfoComponent.transform.localScale = Vector3.one + boneInfoComponent.sizeVariation * Random.Range(-1f, 1f);
				Random.state = state;
			}
		}
		if (this.spine != null)
		{
			Transform transform = this.rightShoulder.transform;
			Vector3 localScale = new Vector3(1f / this.spine.transform.localScale.x, 1f / this.spine.transform.localScale.y, 1f / this.spine.transform.localScale.z);
			this.leftShoulder.transform.localScale = localScale;
			transform.localScale = localScale;
		}
	}

	// Token: 0x060010BD RID: 4285 RVA: 0x00064B08 File Offset: 0x00062D08
	public void Reset()
	{
		foreach (global::BoneInfoComponent boneInfoComponent in this.bones)
		{
			if (!(boneInfoComponent.sizeVariation == Vector3.zero))
			{
				boneInfoComponent.transform.localScale = Vector3.one;
			}
		}
	}

	// Token: 0x04000BD4 RID: 3028
	protected global::BoneInfoComponent[] bones;

	// Token: 0x04000BD5 RID: 3029
	public int seed;

	// Token: 0x04000BD6 RID: 3030
	public GameObject leftShoulder;

	// Token: 0x04000BD7 RID: 3031
	public GameObject rightShoulder;

	// Token: 0x04000BD8 RID: 3032
	public GameObject spine;
}
