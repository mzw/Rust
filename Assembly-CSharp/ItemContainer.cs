﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020004C3 RID: 1219
public sealed class ItemContainer
{
	// Token: 0x06001A58 RID: 6744 RVA: 0x000940DC File Offset: 0x000922DC
	public bool HasFlag(global::ItemContainer.Flag f)
	{
		return (this.flags & f) == f;
	}

	// Token: 0x06001A59 RID: 6745 RVA: 0x000940EC File Offset: 0x000922EC
	public void SetFlag(global::ItemContainer.Flag f, bool b)
	{
		if (b)
		{
			this.flags |= f;
		}
		else
		{
			this.flags &= ~f;
		}
	}

	// Token: 0x06001A5A RID: 6746 RVA: 0x00094118 File Offset: 0x00092318
	public bool IsLocked()
	{
		return this.HasFlag(global::ItemContainer.Flag.IsLocked);
	}

	// Token: 0x06001A5B RID: 6747 RVA: 0x00094124 File Offset: 0x00092324
	public bool PlayerItemInputBlocked()
	{
		return this.HasFlag(global::ItemContainer.Flag.NoItemInput);
	}

	// Token: 0x14000005 RID: 5
	// (add) Token: 0x06001A5C RID: 6748 RVA: 0x00094134 File Offset: 0x00092334
	// (remove) Token: 0x06001A5D RID: 6749 RVA: 0x0009416C File Offset: 0x0009236C
	public event Action onDirty;

	// Token: 0x06001A5E RID: 6750 RVA: 0x000941A4 File Offset: 0x000923A4
	public void ServerInitialize(global::Item parentItem, int iMaxCapacity)
	{
		this.parent = parentItem;
		this.capacity = iMaxCapacity;
		this.uid = 0u;
		this.isServer = true;
		if (this.allowedContents == (global::ItemContainer.ContentsType)0)
		{
			this.allowedContents = global::ItemContainer.ContentsType.Generic;
		}
		this.MarkDirty();
	}

	// Token: 0x06001A5F RID: 6751 RVA: 0x000941DC File Offset: 0x000923DC
	public void GiveUID()
	{
		Assert.IsTrue(this.uid == 0u, "Calling GiveUID - but already has a uid!");
		this.uid = Net.sv.TakeUID();
	}

	// Token: 0x06001A60 RID: 6752 RVA: 0x00094204 File Offset: 0x00092404
	public void MarkDirty()
	{
		this.dirty = true;
		if (this.parent != null)
		{
			this.parent.MarkDirty();
		}
		if (this.onDirty != null)
		{
			this.onDirty();
		}
	}

	// Token: 0x06001A61 RID: 6753 RVA: 0x0009423C File Offset: 0x0009243C
	public global::DroppedItemContainer Drop(string prefab, Vector3 pos, Quaternion rot)
	{
		if (this.itemList == null || this.itemList.Count == 0)
		{
			return null;
		}
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(prefab, pos, rot, true);
		if (baseEntity == null)
		{
			return null;
		}
		global::DroppedItemContainer droppedItemContainer = baseEntity as global::DroppedItemContainer;
		if (droppedItemContainer != null)
		{
			droppedItemContainer.TakeFrom(new global::ItemContainer[]
			{
				this
			});
		}
		droppedItemContainer.Spawn();
		return droppedItemContainer;
	}

	// Token: 0x06001A62 RID: 6754 RVA: 0x000942B8 File Offset: 0x000924B8
	public static global::DroppedItemContainer Drop(string prefab, Vector3 pos, Quaternion rot, params global::ItemContainer[] containers)
	{
		int num = 0;
		foreach (global::ItemContainer itemContainer in containers)
		{
			num += ((itemContainer.itemList == null) ? 0 : itemContainer.itemList.Count);
		}
		if (num == 0)
		{
			return null;
		}
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(prefab, pos, rot, true);
		if (baseEntity == null)
		{
			return null;
		}
		global::DroppedItemContainer droppedItemContainer = baseEntity as global::DroppedItemContainer;
		if (droppedItemContainer != null)
		{
			droppedItemContainer.TakeFrom(containers);
		}
		droppedItemContainer.Spawn();
		return droppedItemContainer;
	}

	// Token: 0x06001A63 RID: 6755 RVA: 0x0009434C File Offset: 0x0009254C
	public void OnChanged()
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			this.itemList[i].OnChanged();
		}
	}

	// Token: 0x06001A64 RID: 6756 RVA: 0x00094388 File Offset: 0x00092588
	public global::Item FindItemByUID(uint iUID)
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			global::Item item = this.itemList[i];
			if (item.IsValid())
			{
				global::Item item2 = item.FindItem(iUID);
				if (item2 != null)
				{
					return item2;
				}
			}
		}
		return null;
	}

	// Token: 0x06001A65 RID: 6757 RVA: 0x000943E0 File Offset: 0x000925E0
	public bool IsFull()
	{
		return this.itemList.Count >= this.capacity;
	}

	// Token: 0x06001A66 RID: 6758 RVA: 0x000943F8 File Offset: 0x000925F8
	public bool CanTake(global::Item item)
	{
		return !this.IsFull();
	}

	// Token: 0x06001A67 RID: 6759 RVA: 0x00094408 File Offset: 0x00092608
	public bool Insert(global::Item item)
	{
		if (this.itemList.Contains(item))
		{
			return false;
		}
		if (this.IsFull())
		{
			return false;
		}
		this.itemList.Add(item);
		item.parent = this;
		if (!this.FindPosition(item))
		{
			return false;
		}
		this.MarkDirty();
		if (this.onItemAddedRemoved != null)
		{
			this.onItemAddedRemoved(item, true);
		}
		Interface.CallHook("OnItemAddedToContainer", new object[]
		{
			this,
			item
		});
		return true;
	}

	// Token: 0x06001A68 RID: 6760 RVA: 0x00094490 File Offset: 0x00092690
	public bool SlotTaken(int i)
	{
		return this.GetSlot(i) != null;
	}

	// Token: 0x06001A69 RID: 6761 RVA: 0x000944A0 File Offset: 0x000926A0
	public global::Item GetSlot(int slot)
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			if (this.itemList[i].position == slot)
			{
				return this.itemList[i];
			}
		}
		return null;
	}

	// Token: 0x06001A6A RID: 6762 RVA: 0x000944F0 File Offset: 0x000926F0
	public bool FindPosition(global::Item item)
	{
		int position = item.position;
		item.position = -1;
		if (position >= 0 && !this.SlotTaken(position))
		{
			item.position = position;
			return true;
		}
		for (int i = 0; i < this.capacity; i++)
		{
			if (!this.SlotTaken(i))
			{
				item.position = i;
				return true;
			}
		}
		return false;
	}

	// Token: 0x06001A6B RID: 6763 RVA: 0x0009455C File Offset: 0x0009275C
	public void SetLocked(bool isLocked)
	{
		this.SetFlag(global::ItemContainer.Flag.IsLocked, isLocked);
		this.MarkDirty();
	}

	// Token: 0x06001A6C RID: 6764 RVA: 0x00094570 File Offset: 0x00092770
	public bool Remove(global::Item item)
	{
		if (!this.itemList.Contains(item))
		{
			return false;
		}
		if (this.onPreItemRemove != null)
		{
			this.onPreItemRemove(item);
		}
		this.itemList.Remove(item);
		item.parent = null;
		this.MarkDirty();
		if (this.onItemAddedRemoved != null)
		{
			this.onItemAddedRemoved(item, false);
		}
		Interface.CallHook("OnItemRemovedFromContainer", new object[]
		{
			this,
			item
		});
		return true;
	}

	// Token: 0x06001A6D RID: 6765 RVA: 0x000945F8 File Offset: 0x000927F8
	public void Clear()
	{
		foreach (global::Item item in this.itemList.ToArray())
		{
			item.Remove(0f);
		}
	}

	// Token: 0x06001A6E RID: 6766 RVA: 0x00094634 File Offset: 0x00092834
	public void Kill()
	{
		this.onDirty = null;
		this.canAcceptItem = null;
		this.onItemAddedRemoved = null;
		if (Net.sv != null)
		{
			Net.sv.ReturnUID(this.uid);
			this.uid = 0u;
		}
		foreach (global::Item item in this.itemList.ToList<global::Item>())
		{
			item.Remove(0f);
		}
		this.itemList.Clear();
	}

	// Token: 0x06001A6F RID: 6767 RVA: 0x000946DC File Offset: 0x000928DC
	public int GetAmount(int itemid, bool onlyUsableAmounts)
	{
		int num = 0;
		foreach (global::Item item in this.itemList)
		{
			if (item.info.itemid == itemid)
			{
				if (!onlyUsableAmounts || !item.IsBusy())
				{
					num += item.amount;
				}
			}
		}
		return num;
	}

	// Token: 0x06001A70 RID: 6768 RVA: 0x0009476C File Offset: 0x0009296C
	public global::Item FindItemByItemID(int itemid)
	{
		return this.itemList.FirstOrDefault((global::Item x) => x.info.itemid == itemid);
	}

	// Token: 0x06001A71 RID: 6769 RVA: 0x000947A0 File Offset: 0x000929A0
	public global::Item FindItemsByItemName(string name)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(name);
		if (itemDefinition == null)
		{
			return null;
		}
		for (int i = 0; i < this.itemList.Count; i++)
		{
			if (this.itemList[i].info == itemDefinition)
			{
				return this.itemList[i];
			}
		}
		return null;
	}

	// Token: 0x06001A72 RID: 6770 RVA: 0x00094808 File Offset: 0x00092A08
	public List<global::Item> FindItemsByItemID(int itemid)
	{
		return this.itemList.FindAll((global::Item x) => x.info.itemid == itemid);
	}

	// Token: 0x06001A73 RID: 6771 RVA: 0x0009483C File Offset: 0x00092A3C
	public ProtoBuf.ItemContainer Save()
	{
		ProtoBuf.ItemContainer itemContainer = Pool.Get<ProtoBuf.ItemContainer>();
		itemContainer.contents = Pool.GetList<ProtoBuf.Item>();
		itemContainer.UID = this.uid;
		itemContainer.slots = this.capacity;
		itemContainer.temperature = this.temperature;
		itemContainer.allowedContents = (int)this.allowedContents;
		itemContainer.allowedItem = ((!(this.onlyAllowedItem != null)) ? 0 : this.onlyAllowedItem.itemid);
		itemContainer.flags = (int)this.flags;
		itemContainer.maxStackSize = this.maxStackSize;
		if (this.availableSlots != null && this.availableSlots.Count > 0)
		{
			itemContainer.availableSlots = Pool.GetList<int>();
			for (int i = 0; i < this.availableSlots.Count; i++)
			{
				itemContainer.availableSlots.Add((int)this.availableSlots[i]);
			}
		}
		for (int j = 0; j < this.itemList.Count; j++)
		{
			global::Item item = this.itemList[j];
			if (item.IsValid())
			{
				itemContainer.contents.Add(item.Save(true, true));
			}
		}
		return itemContainer;
	}

	// Token: 0x06001A74 RID: 6772 RVA: 0x00094974 File Offset: 0x00092B74
	public void Load(ProtoBuf.ItemContainer container)
	{
		using (TimeWarning.New("ItemContainer.Load", 0.1f))
		{
			this.uid = container.UID;
			this.capacity = container.slots;
			if (container.contents != null)
			{
				List<global::Item> list = this.itemList;
				this.itemList = Pool.GetList<global::Item>();
				this.temperature = container.temperature;
				this.flags = (global::ItemContainer.Flag)container.flags;
				this.allowedContents = (global::ItemContainer.ContentsType)((container.allowedContents != 0) ? container.allowedContents : 1);
				this.onlyAllowedItem = ((container.allowedItem <= 0) ? null : global::ItemManager.FindItemDefinition(container.allowedItem));
				this.maxStackSize = container.maxStackSize;
				this.availableSlots.Clear();
				for (int i = 0; i < container.availableSlots.Count; i++)
				{
					this.availableSlots.Add((global::ItemSlot)container.availableSlots[i]);
				}
				using (TimeWarning.New("container.contents", 0.1f))
				{
					foreach (ProtoBuf.Item item in container.contents)
					{
						global::Item item2 = null;
						foreach (global::Item item3 in list)
						{
							if (item3.uid == item.UID)
							{
								item2 = item3;
								break;
							}
						}
						item2 = global::ItemManager.Load(item, item2, this.isServer);
						if (item2 != null)
						{
							item2.parent = this;
							item2.position = item.slot;
							this.Insert(item2);
						}
					}
				}
				using (TimeWarning.New("Delete old items", 0.1f))
				{
					foreach (global::Item item4 in list)
					{
						if (!this.itemList.Contains(item4))
						{
							item4.Remove(0f);
						}
					}
				}
				this.dirty = true;
				Pool.FreeList<global::Item>(ref list);
			}
		}
	}

	// Token: 0x06001A75 RID: 6773 RVA: 0x00094C88 File Offset: 0x00092E88
	public global::BasePlayer GetOwnerPlayer()
	{
		return this.playerOwner;
	}

	// Token: 0x06001A76 RID: 6774 RVA: 0x00094C90 File Offset: 0x00092E90
	public int Take(List<global::Item> collect, int itemid, int iAmount)
	{
		int num = 0;
		if (iAmount == 0)
		{
			return num;
		}
		List<global::Item> list = Pool.GetList<global::Item>();
		foreach (global::Item item in this.itemList)
		{
			if (item.info.itemid == itemid)
			{
				int num2 = iAmount - num;
				if (num2 > 0)
				{
					if (item.amount > num2)
					{
						item.MarkDirty();
						item.amount -= num2;
						num += num2;
						global::Item item2 = global::ItemManager.CreateByItemID(itemid, 1, 0UL);
						item2.amount = num2;
						item2.CollectedForCrafting(this.playerOwner);
						if (collect != null)
						{
							collect.Add(item2);
						}
						break;
					}
					if (item.amount <= num2)
					{
						num += item.amount;
						list.Add(item);
						if (collect != null)
						{
							collect.Add(item);
						}
					}
					if (num == iAmount)
					{
						break;
					}
				}
			}
		}
		foreach (global::Item item3 in list)
		{
			item3.RemoveFromContainer();
		}
		Pool.FreeList<global::Item>(ref list);
		return num;
	}

	// Token: 0x170001CE RID: 462
	// (get) Token: 0x06001A77 RID: 6775 RVA: 0x00094E00 File Offset: 0x00093000
	public Vector3 dropPosition
	{
		get
		{
			if (this.playerOwner)
			{
				return this.playerOwner.GetDropPosition();
			}
			if (this.entityOwner)
			{
				return this.entityOwner.GetDropPosition();
			}
			if (this.parent != null)
			{
				global::BaseEntity worldEntity = this.parent.GetWorldEntity();
				if (worldEntity != null)
				{
					return worldEntity.GetDropPosition();
				}
			}
			Debug.LogWarning("ItemContainer.dropPosition dropped through");
			return Vector3.zero;
		}
	}

	// Token: 0x170001CF RID: 463
	// (get) Token: 0x06001A78 RID: 6776 RVA: 0x00094E80 File Offset: 0x00093080
	public Vector3 dropVelocity
	{
		get
		{
			if (this.playerOwner)
			{
				return this.playerOwner.GetDropVelocity();
			}
			if (this.entityOwner)
			{
				return this.entityOwner.GetDropVelocity();
			}
			if (this.parent != null)
			{
				global::BaseEntity worldEntity = this.parent.GetWorldEntity();
				if (worldEntity != null)
				{
					return worldEntity.GetDropVelocity();
				}
			}
			Debug.LogWarning("ItemContainer.dropVelocity dropped through");
			return Vector3.zero;
		}
	}

	// Token: 0x06001A79 RID: 6777 RVA: 0x00094F00 File Offset: 0x00093100
	public void OnCycle(float delta)
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			if (this.itemList[i].IsValid())
			{
				this.itemList[i].OnCycle(delta);
			}
		}
	}

	// Token: 0x06001A7A RID: 6778 RVA: 0x00094F58 File Offset: 0x00093158
	public void FindAmmo(List<global::Item> list, AmmoTypes ammoType)
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			this.itemList[i].FindAmmo(list, ammoType);
		}
	}

	// Token: 0x06001A7B RID: 6779 RVA: 0x00094F94 File Offset: 0x00093194
	public bool HasAmmo(AmmoTypes ammoType)
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			if (this.itemList[i].HasAmmo(ammoType))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x06001A7C RID: 6780 RVA: 0x00094FD8 File Offset: 0x000931D8
	public void AddItem(global::ItemDefinition itemToCreate, int p)
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			if (p == 0)
			{
				return;
			}
			if (!(this.itemList[i].info != itemToCreate))
			{
				int num = this.itemList[i].MaxStackable();
				if (num > this.itemList[i].amount)
				{
					this.MarkDirty();
					this.itemList[i].amount += p;
					p -= p;
					if (this.itemList[i].amount > num)
					{
						p = this.itemList[i].amount - num;
						if (p > 0)
						{
							this.itemList[i].amount -= p;
						}
					}
				}
			}
		}
		if (p == 0)
		{
			return;
		}
		global::Item item = global::ItemManager.Create(itemToCreate, p, 0UL);
		if (!item.MoveToContainer(this, -1, true))
		{
			item.Remove(0f);
		}
	}

	// Token: 0x06001A7D RID: 6781 RVA: 0x000950F4 File Offset: 0x000932F4
	public void OnMovedToWorld()
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			global::Item item = this.itemList[i];
			item.OnMovedToWorld();
		}
	}

	// Token: 0x06001A7E RID: 6782 RVA: 0x00095130 File Offset: 0x00093330
	public void OnRemovedFromWorld()
	{
		for (int i = 0; i < this.itemList.Count; i++)
		{
			global::Item item = this.itemList[i];
			item.OnRemovedFromWorld();
		}
	}

	// Token: 0x06001A7F RID: 6783 RVA: 0x0009516C File Offset: 0x0009336C
	public uint ContentsHash()
	{
		uint num = 0u;
		for (int i = 0; i < this.capacity; i++)
		{
			global::Item slot = this.GetSlot(i);
			if (slot != null)
			{
				num = CRC.Compute32(num, slot.info.itemid);
				num = CRC.Compute32(num, slot.skin);
			}
		}
		return num;
	}

	// Token: 0x06001A80 RID: 6784 RVA: 0x000951C0 File Offset: 0x000933C0
	public global::ItemContainer FindContainer(uint id)
	{
		if (id == this.uid)
		{
			return this;
		}
		for (int i = 0; i < this.itemList.Count; i++)
		{
			global::Item item = this.itemList[i];
			if (item.contents != null)
			{
				global::ItemContainer itemContainer = item.contents.FindContainer(id);
				if (itemContainer != null)
				{
					return itemContainer;
				}
			}
		}
		return null;
	}

	// Token: 0x06001A81 RID: 6785 RVA: 0x0009522C File Offset: 0x0009342C
	public global::ItemContainer.CanAcceptResult CanAcceptItem(global::Item item, int targetPos)
	{
		if (this.canAcceptItem != null && !this.canAcceptItem(item, targetPos))
		{
			return global::ItemContainer.CanAcceptResult.CannotAccept;
		}
		if ((this.allowedContents & item.info.itemType) != item.info.itemType)
		{
			return global::ItemContainer.CanAcceptResult.CannotAccept;
		}
		if (this.onlyAllowedItem != null && this.onlyAllowedItem != item.info)
		{
			return global::ItemContainer.CanAcceptResult.CannotAccept;
		}
		if (this.availableSlots != null && this.availableSlots.Count > 0)
		{
			if (item.info.occupySlots == (global::ItemSlot)0 || item.info.occupySlots == global::ItemSlot.None)
			{
				return global::ItemContainer.CanAcceptResult.CannotAccept;
			}
			int[] array = new int[32];
			foreach (global::ItemSlot itemSlot in this.availableSlots)
			{
				array[(int)Mathf.Log((float)itemSlot, 2f)]++;
			}
			foreach (global::Item item2 in this.itemList)
			{
				for (int i = 0; i < 32; i++)
				{
					if ((item2.info.occupySlots & (global::ItemSlot)(1 << i)) != (global::ItemSlot)0)
					{
						array[i]--;
					}
				}
			}
			for (int j = 0; j < 32; j++)
			{
				if ((item.info.occupySlots & (global::ItemSlot)(1 << j)) != (global::ItemSlot)0)
				{
					if (array[j] <= 0)
					{
						return global::ItemContainer.CanAcceptResult.CannotAcceptRightNow;
					}
				}
			}
		}
		object obj = Interface.CallHook("CanAcceptItem", new object[]
		{
			this,
			item,
			targetPos
		});
		if (obj is global::ItemContainer.CanAcceptResult)
		{
			return (global::ItemContainer.CanAcceptResult)obj;
		}
		return global::ItemContainer.CanAcceptResult.CanAccept;
	}

	// Token: 0x040014F1 RID: 5361
	public global::ItemContainer.Flag flags;

	// Token: 0x040014F2 RID: 5362
	public global::ItemContainer.ContentsType allowedContents;

	// Token: 0x040014F3 RID: 5363
	public global::ItemDefinition onlyAllowedItem;

	// Token: 0x040014F4 RID: 5364
	public List<global::ItemSlot> availableSlots = new List<global::ItemSlot>();

	// Token: 0x040014F5 RID: 5365
	public int capacity = 2;

	// Token: 0x040014F6 RID: 5366
	public uint uid;

	// Token: 0x040014F7 RID: 5367
	public bool dirty;

	// Token: 0x040014F8 RID: 5368
	public List<global::Item> itemList = new List<global::Item>();

	// Token: 0x040014F9 RID: 5369
	public float temperature = 15f;

	// Token: 0x040014FA RID: 5370
	public global::Item parent;

	// Token: 0x040014FB RID: 5371
	public global::BasePlayer playerOwner;

	// Token: 0x040014FC RID: 5372
	public global::BaseEntity entityOwner;

	// Token: 0x040014FD RID: 5373
	public bool isServer;

	// Token: 0x040014FE RID: 5374
	public int maxStackSize;

	// Token: 0x04001500 RID: 5376
	public Func<global::Item, int, bool> canAcceptItem;

	// Token: 0x04001501 RID: 5377
	public Action<global::Item, bool> onItemAddedRemoved;

	// Token: 0x04001502 RID: 5378
	public Action<global::Item> onPreItemRemove;

	// Token: 0x020004C4 RID: 1220
	[Flags]
	public enum Flag
	{
		// Token: 0x04001504 RID: 5380
		IsPlayer = 1,
		// Token: 0x04001505 RID: 5381
		Clothing = 2,
		// Token: 0x04001506 RID: 5382
		Belt = 4,
		// Token: 0x04001507 RID: 5383
		SingleType = 8,
		// Token: 0x04001508 RID: 5384
		IsLocked = 16,
		// Token: 0x04001509 RID: 5385
		ShowSlotsOnIcon = 32,
		// Token: 0x0400150A RID: 5386
		NoBrokenItems = 64,
		// Token: 0x0400150B RID: 5387
		NoItemInput = 128
	}

	// Token: 0x020004C5 RID: 1221
	[Flags]
	public enum ContentsType
	{
		// Token: 0x0400150D RID: 5389
		Generic = 1,
		// Token: 0x0400150E RID: 5390
		Liquid = 2
	}

	// Token: 0x020004C6 RID: 1222
	public enum CanAcceptResult
	{
		// Token: 0x04001510 RID: 5392
		CanAccept,
		// Token: 0x04001511 RID: 5393
		CannotAccept,
		// Token: 0x04001512 RID: 5394
		CannotAcceptRightNow
	}
}
