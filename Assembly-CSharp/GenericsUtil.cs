﻿using System;

// Token: 0x02000786 RID: 1926
public static class GenericsUtil
{
	// Token: 0x060023C0 RID: 9152 RVA: 0x000C5B94 File Offset: 0x000C3D94
	public static TDst Cast<TSrc, TDst>(TSrc obj)
	{
		global::GenericsUtil.CastImpl<TSrc, TDst>.Value = obj;
		return global::GenericsUtil.CastImpl<TDst, TSrc>.Value;
	}

	// Token: 0x060023C1 RID: 9153 RVA: 0x000C5BA4 File Offset: 0x000C3DA4
	public static void Swap<T>(ref T a, ref T b)
	{
		T t = a;
		a = b;
		b = t;
	}

	// Token: 0x02000787 RID: 1927
	private static class CastImpl<TSrc, TDst>
	{
		// Token: 0x060023C2 RID: 9154 RVA: 0x000C5BCC File Offset: 0x000C3DCC
		static CastImpl()
		{
			if (typeof(TSrc) != typeof(TDst))
			{
				throw new InvalidCastException();
			}
		}

		// Token: 0x04001FAC RID: 8108
		[ThreadStatic]
		public static TSrc Value;
	}
}
