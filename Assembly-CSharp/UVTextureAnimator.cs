﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020007D1 RID: 2001
internal class UVTextureAnimator : MonoBehaviour
{
	// Token: 0x06002509 RID: 9481 RVA: 0x000CC7A0 File Offset: 0x000CA9A0
	private void Start()
	{
		this.currentRenderer = base.GetComponent<Renderer>();
		this.InitDefaultVariables();
		this.isInizialised = true;
		this.isVisible = true;
		this.Play();
	}

	// Token: 0x0600250A RID: 9482 RVA: 0x000CC7C8 File Offset: 0x000CA9C8
	private void InitDefaultVariables()
	{
		this.currentRenderer = base.GetComponent<Renderer>();
		if (this.currentRenderer == null)
		{
			throw new Exception("UvTextureAnimator can't get renderer");
		}
		if (!this.currentRenderer.enabled)
		{
			this.currentRenderer.enabled = true;
		}
		this.allCount = 0;
		this.deltaFps = 1f / this.Fps;
		this.count = this.Rows * this.Columns;
		this.index = this.Columns - 1;
		Vector3 zero = Vector3.zero;
		this.OffsetMat -= this.OffsetMat / this.count * this.count;
		Vector2 vector;
		vector..ctor(1f / (float)this.Columns, 1f / (float)this.Rows);
		if (this.currentRenderer != null)
		{
			this.instanceMaterial = this.currentRenderer.material;
			this.instanceMaterial.SetTextureScale("_MainTex", vector);
			this.instanceMaterial.SetTextureOffset("_MainTex", zero);
		}
	}

	// Token: 0x0600250B RID: 9483 RVA: 0x000CC8E4 File Offset: 0x000CAAE4
	private void Play()
	{
		if (this.isCorutineStarted)
		{
			return;
		}
		if (this.StartDelay > 0.0001f)
		{
			base.Invoke("PlayDelay", this.StartDelay);
		}
		else
		{
			base.StartCoroutine(this.UpdateCorutine());
		}
		this.isCorutineStarted = true;
	}

	// Token: 0x0600250C RID: 9484 RVA: 0x000CC938 File Offset: 0x000CAB38
	private void PlayDelay()
	{
		base.StartCoroutine(this.UpdateCorutine());
	}

	// Token: 0x0600250D RID: 9485 RVA: 0x000CC948 File Offset: 0x000CAB48
	private void OnEnable()
	{
		if (!this.isInizialised)
		{
			return;
		}
		this.InitDefaultVariables();
		this.isVisible = true;
		this.Play();
	}

	// Token: 0x0600250E RID: 9486 RVA: 0x000CC96C File Offset: 0x000CAB6C
	private void OnDisable()
	{
		this.isCorutineStarted = false;
		this.isVisible = false;
		base.StopAllCoroutines();
		base.CancelInvoke("PlayDelay");
	}

	// Token: 0x0600250F RID: 9487 RVA: 0x000CC990 File Offset: 0x000CAB90
	private IEnumerator UpdateCorutine()
	{
		while (this.isVisible && (this.IsLoop || this.allCount != this.count))
		{
			this.UpdateCorutineFrame();
			if (!this.IsLoop && this.allCount == this.count)
			{
				break;
			}
			yield return new WaitForSeconds(this.deltaFps);
		}
		this.isCorutineStarted = false;
		this.currentRenderer.enabled = false;
		yield break;
	}

	// Token: 0x06002510 RID: 9488 RVA: 0x000CC9AC File Offset: 0x000CABAC
	private void UpdateCorutineFrame()
	{
		this.allCount++;
		this.index++;
		if (this.index >= this.count)
		{
			this.index = 0;
		}
		Vector2 vector;
		vector..ctor((float)this.index / (float)this.Columns - (float)(this.index / this.Columns), 1f - (float)(this.index / this.Columns) / (float)this.Rows);
		if (this.currentRenderer != null)
		{
			this.instanceMaterial.SetTextureOffset("_MainTex", vector);
		}
	}

	// Token: 0x06002511 RID: 9489 RVA: 0x000CCA50 File Offset: 0x000CAC50
	private void OnDestroy()
	{
		if (this.instanceMaterial != null)
		{
			Object.Destroy(this.instanceMaterial);
			this.instanceMaterial = null;
		}
	}

	// Token: 0x04002104 RID: 8452
	public int Rows = 4;

	// Token: 0x04002105 RID: 8453
	public int Columns = 4;

	// Token: 0x04002106 RID: 8454
	public float Fps = 20f;

	// Token: 0x04002107 RID: 8455
	public int OffsetMat;

	// Token: 0x04002108 RID: 8456
	public bool IsLoop = true;

	// Token: 0x04002109 RID: 8457
	public float StartDelay;

	// Token: 0x0400210A RID: 8458
	private bool isInizialised;

	// Token: 0x0400210B RID: 8459
	private int index;

	// Token: 0x0400210C RID: 8460
	private int count;

	// Token: 0x0400210D RID: 8461
	private int allCount;

	// Token: 0x0400210E RID: 8462
	private float deltaFps;

	// Token: 0x0400210F RID: 8463
	private bool isVisible;

	// Token: 0x04002110 RID: 8464
	private bool isCorutineStarted;

	// Token: 0x04002111 RID: 8465
	private Renderer currentRenderer;

	// Token: 0x04002112 RID: 8466
	private Material instanceMaterial;
}
