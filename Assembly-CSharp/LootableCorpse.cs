﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200007B RID: 123
public class LootableCorpse : global::BaseCorpse
{
	// Token: 0x0600085A RID: 2138 RVA: 0x000365E8 File Offset: 0x000347E8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("LootableCorpse.OnRpcMessage", 0.1f))
		{
			if (rpc == 864456382u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_LootCorpse ");
				}
				using (TimeWarning.New("RPC_LootCorpse", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_LootCorpse", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_LootCorpse(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_LootCorpse");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x1700005D RID: 93
	// (get) Token: 0x0600085B RID: 2139 RVA: 0x000367C8 File Offset: 0x000349C8
	// (set) Token: 0x0600085C RID: 2140 RVA: 0x000367D0 File Offset: 0x000349D0
	public string playerName
	{
		get
		{
			return this._playerName;
		}
		set
		{
			this._playerName = value;
		}
	}

	// Token: 0x0600085D RID: 2141 RVA: 0x000367DC File Offset: 0x000349DC
	public override void ServerInit()
	{
		base.ServerInit();
	}

	// Token: 0x0600085E RID: 2142 RVA: 0x000367E4 File Offset: 0x000349E4
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		this.DropItems();
		if (this.containers != null)
		{
			foreach (global::ItemContainer itemContainer in this.containers)
			{
				itemContainer.Kill();
			}
		}
		this.containers = null;
	}

	// Token: 0x0600085F RID: 2143 RVA: 0x00036834 File Offset: 0x00034A34
	public void TakeFrom(params global::ItemContainer[] source)
	{
		Assert.IsTrue(this.containers == null, "Initializing Twice");
		using (TimeWarning.New("Corpse.TakeFrom", 0.1f))
		{
			this.containers = new global::ItemContainer[source.Length];
			for (int i = 0; i < source.Length; i++)
			{
				this.containers[i] = new global::ItemContainer();
				this.containers[i].ServerInitialize(null, source[i].capacity);
				this.containers[i].GiveUID();
				this.containers[i].entityOwner = this;
				foreach (global::Item item in source[i].itemList.ToArray())
				{
					if (!item.MoveToContainer(this.containers[i], -1, true))
					{
						item.Remove(0f);
					}
				}
			}
			base.ResetRemovalTime();
		}
	}

	// Token: 0x06000860 RID: 2144 RVA: 0x00036938 File Offset: 0x00034B38
	public override bool CanRemove()
	{
		return !base.IsOpen();
	}

	// Token: 0x06000861 RID: 2145 RVA: 0x00036944 File Offset: 0x00034B44
	public virtual bool CanLoot()
	{
		return true;
	}

	// Token: 0x06000862 RID: 2146 RVA: 0x00036948 File Offset: 0x00034B48
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	private void RPC_LootCorpse(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!this.CanLoot())
		{
			return;
		}
		if (this.containers == null)
		{
			return;
		}
		global::BasePlayer player = rpc.player;
		if (Interface.CallHook("CanLootEntity", new object[]
		{
			player,
			this
		}) != null)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, true, false);
		player.inventory.loot.StartLootingEntity(this, true);
		foreach (global::ItemContainer container in this.containers)
		{
			player.inventory.loot.AddContainer(container);
		}
		player.inventory.loot.SendImmediate();
		base.ClientRPCPlayer(null, player, "RPC_ClientLootCorpse");
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000863 RID: 2147 RVA: 0x00036A18 File Offset: 0x00034C18
	public void PlayerStoppedLooting(global::BasePlayer player)
	{
		Interface.CallHook("OnLootEntityEnd", new object[]
		{
			player,
			this
		});
		base.ResetRemovalTime();
		base.SetFlag(global::BaseEntity.Flags.Open, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000864 RID: 2148 RVA: 0x00036A58 File Offset: 0x00034C58
	public void DropItems()
	{
		if (this.containers != null)
		{
			global::DroppedItemContainer droppedItemContainer = global::ItemContainer.Drop("assets/prefabs/misc/item drop/item_drop_backpack.prefab", base.transform.position, Quaternion.identity, this.containers);
			if (droppedItemContainer != null)
			{
				droppedItemContainer.playerName = this.playerName;
				droppedItemContainer.playerSteamID = this.playerSteamID;
			}
		}
	}

	// Token: 0x06000865 RID: 2149 RVA: 0x00036AB8 File Offset: 0x00034CB8
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.lootableCorpse = Facepunch.Pool.Get<ProtoBuf.LootableCorpse>();
		info.msg.lootableCorpse.playerName = this.playerName;
		info.msg.lootableCorpse.playerID = this.playerSteamID;
	}

	// Token: 0x06000866 RID: 2150 RVA: 0x00036B0C File Offset: 0x00034D0C
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.lootableCorpse != null)
		{
			this.playerName = info.msg.lootableCorpse.playerName;
			this.playerSteamID = info.msg.lootableCorpse.playerID;
		}
	}

	// Token: 0x040003E7 RID: 999
	public string lootPanelName = "generic";

	// Token: 0x040003E8 RID: 1000
	[NonSerialized]
	public ulong playerSteamID;

	// Token: 0x040003E9 RID: 1001
	[NonSerialized]
	public string _playerName;

	// Token: 0x040003EA RID: 1002
	[NonSerialized]
	public global::ItemContainer[] containers;
}
