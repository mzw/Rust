﻿using System;
using System.Collections.Generic;
using Facepunch;

// Token: 0x0200038E RID: 910
public class BaseResourceExtractor : global::BaseCombatEntity
{
	// Token: 0x0600159F RID: 5535 RVA: 0x0007C31C File Offset: 0x0007A51C
	public override void ServerInit()
	{
		base.ServerInit();
		if (base.isClient)
		{
			return;
		}
		List<global::SurveyCrater> list = Pool.GetList<global::SurveyCrater>();
		global::Vis.Entities<global::SurveyCrater>(base.transform.position, 3f, list, 1, 2);
		foreach (global::SurveyCrater surveyCrater in list)
		{
			if (surveyCrater.isServer)
			{
				surveyCrater.Kill(global::BaseNetworkable.DestroyMode.None);
			}
		}
		Pool.FreeList<global::SurveyCrater>(ref list);
	}

	// Token: 0x04000FE4 RID: 4068
	public bool canExtractLiquid;

	// Token: 0x04000FE5 RID: 4069
	public bool canExtractSolid = true;
}
