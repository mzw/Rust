﻿using System;
using System.IO;

// Token: 0x02000772 RID: 1906
public static class StreamEx
{
	// Token: 0x06002368 RID: 9064 RVA: 0x000C4054 File Offset: 0x000C2254
	public static void WriteToOtherStream(this Stream self, Stream target)
	{
		int count;
		while ((count = self.Read(global::StreamEx.StaticBuffer, 0, global::StreamEx.StaticBuffer.Length)) > 0)
		{
			target.Write(global::StreamEx.StaticBuffer, 0, count);
		}
	}

	// Token: 0x04001F98 RID: 8088
	private static readonly byte[] StaticBuffer = new byte[16384];
}
