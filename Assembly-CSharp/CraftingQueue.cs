﻿using System;
using UnityEngine;

// Token: 0x020006A4 RID: 1700
public class CraftingQueue : SingletonComponent<global::CraftingQueue>
{
	// Token: 0x04001CC7 RID: 7367
	public GameObject queueContainer;

	// Token: 0x04001CC8 RID: 7368
	public GameObject queueItemPrefab;
}
