﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

// Token: 0x02000742 RID: 1858
public class ConvarWater : MonoBehaviour
{
	// Token: 0x04001F51 RID: 8017
	[FormerlySerializedAs("waterEx")]
	public global::Water water;
}
