﻿using System;
using UnityEngine;

// Token: 0x02000598 RID: 1432
public class GenerateCliffSplat : global::ProceduralComponent
{
	// Token: 0x06001E29 RID: 7721 RVA: 0x000A7A20 File Offset: 0x000A5C20
	public static void Process(int x, int z)
	{
		global::TerrainSplatMap splatMap = global::TerrainMeta.SplatMap;
		float normZ = splatMap.Coordinate(z);
		float normX = splatMap.Coordinate(x);
		int topology = global::TerrainMeta.TopologyMap.GetTopology(normX, normZ);
		if ((topology & 8389632) == 0)
		{
			float slope = global::TerrainMeta.HeightMap.GetSlope(normX, normZ);
			if (slope > 30f)
			{
				splatMap.SetSplat(x, z, 8, Mathf.InverseLerp(30f, 50f, slope));
			}
		}
	}

	// Token: 0x06001E2A RID: 7722 RVA: 0x000A7A90 File Offset: 0x000A5C90
	public override void Process(uint seed)
	{
		global::TerrainSplatMap splatMap = global::TerrainMeta.SplatMap;
		int splatres = splatMap.res;
		Parallel.For(0, splatres, delegate(int z)
		{
			for (int i = 0; i < splatres; i++)
			{
				global::GenerateCliffSplat.Process(i, z);
			}
		});
	}

	// Token: 0x040018C0 RID: 6336
	private const int filter = 8389632;
}
