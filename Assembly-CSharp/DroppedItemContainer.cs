﻿using System;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000066 RID: 102
public class DroppedItemContainer : global::BaseCombatEntity
{
	// Token: 0x06000796 RID: 1942 RVA: 0x000312CC File Offset: 0x0002F4CC
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("DroppedItemContainer.OnRpcMessage", 0.1f))
		{
			if (rpc == 4042031372u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_OpenLoot ");
				}
				using (TimeWarning.New("RPC_OpenLoot", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_OpenLoot", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_OpenLoot(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_OpenLoot");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x1700005B RID: 91
	// (get) Token: 0x06000797 RID: 1943 RVA: 0x000314AC File Offset: 0x0002F6AC
	// (set) Token: 0x06000798 RID: 1944 RVA: 0x000314B4 File Offset: 0x0002F6B4
	public string playerName
	{
		get
		{
			return this._playerName;
		}
		set
		{
			this._playerName = value;
		}
	}

	// Token: 0x06000799 RID: 1945 RVA: 0x000314C0 File Offset: 0x0002F6C0
	public override void ServerInit()
	{
		this.ResetRemovalTime();
		base.ServerInit();
	}

	// Token: 0x0600079A RID: 1946 RVA: 0x000314D0 File Offset: 0x0002F6D0
	public void RemoveMe()
	{
		if (base.IsOpen())
		{
			this.ResetRemovalTime();
			return;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x0600079B RID: 1947 RVA: 0x000314EC File Offset: 0x0002F6EC
	public void ResetRemovalTime(float dur)
	{
		using (TimeWarning.New("ResetRemovalTime", 0.1f))
		{
			base.Invoke(new Action(this.RemoveMe), dur);
		}
	}

	// Token: 0x0600079C RID: 1948 RVA: 0x00031540 File Offset: 0x0002F740
	public void ResetRemovalTime()
	{
		this.ResetRemovalTime(this.CalculateRemovalTime());
	}

	// Token: 0x0600079D RID: 1949 RVA: 0x00031550 File Offset: 0x0002F750
	public float CalculateRemovalTime()
	{
		int num = 1;
		if (this.inventory != null)
		{
			foreach (global::Item item in this.inventory.itemList)
			{
				num = Mathf.Max(num, item.despawnMultiplier);
			}
		}
		return (float)num * ConVar.Server.itemdespawn;
	}

	// Token: 0x0600079E RID: 1950 RVA: 0x000315CC File Offset: 0x0002F7CC
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		if (this.inventory != null)
		{
			this.inventory.Kill();
			this.inventory = null;
		}
	}

	// Token: 0x0600079F RID: 1951 RVA: 0x000315F4 File Offset: 0x0002F7F4
	public void TakeFrom(params global::ItemContainer[] source)
	{
		Assert.IsTrue(this.inventory == null, "Initializing Twice");
		using (TimeWarning.New("DroppedItemContainer.TakeFrom", 0.1f))
		{
			int num = 0;
			foreach (global::ItemContainer itemContainer in source)
			{
				num += itemContainer.itemList.Count;
			}
			this.inventory = new global::ItemContainer();
			this.inventory.ServerInitialize(null, num);
			this.inventory.GiveUID();
			this.inventory.entityOwner = this;
			this.inventory.SetFlag(global::ItemContainer.Flag.NoItemInput, true);
			foreach (global::ItemContainer itemContainer2 in source)
			{
				foreach (global::Item item in itemContainer2.itemList.ToArray())
				{
					if (!item.MoveToContainer(this.inventory, -1, true))
					{
						item.Remove(0f);
					}
				}
			}
			this.ResetRemovalTime();
		}
	}

	// Token: 0x060007A0 RID: 1952 RVA: 0x0003172C File Offset: 0x0002F92C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	private void RPC_OpenLoot(global::BaseEntity.RPCMessage rpc)
	{
		if (this.inventory == null)
		{
			return;
		}
		global::BasePlayer player = rpc.player;
		if (!player.CanInteract())
		{
			return;
		}
		if (Interface.CallHook("CanLootEntity", new object[]
		{
			player,
			this
		}) != null)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Open, true, false);
		player.inventory.loot.StartLootingEntity(this, true);
		player.inventory.loot.AddContainer(this.inventory);
		player.inventory.loot.SendImmediate();
		player.ClientRPCPlayer<string>(null, player, "RPC_OpenLootPanel", this.lootPanelName);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060007A1 RID: 1953 RVA: 0x000317D4 File Offset: 0x0002F9D4
	public void PlayerStoppedLooting(global::BasePlayer player)
	{
		if (this.inventory == null || this.inventory.itemList == null || this.inventory.itemList.Count == 0)
		{
			base.Kill(global::BaseNetworkable.DestroyMode.None);
		}
		else
		{
			this.ResetRemovalTime();
			base.SetFlag(global::BaseEntity.Flags.Open, false, false);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x060007A2 RID: 1954 RVA: 0x00031834 File Offset: 0x0002FA34
	public override void PreServerLoad()
	{
		base.PreServerLoad();
		this.inventory = new global::ItemContainer();
		this.inventory.entityOwner = this;
		this.inventory.ServerInitialize(null, 0);
		this.inventory.SetFlag(global::ItemContainer.Flag.NoItemInput, true);
	}

	// Token: 0x060007A3 RID: 1955 RVA: 0x00031874 File Offset: 0x0002FA74
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.lootableCorpse = Facepunch.Pool.Get<ProtoBuf.LootableCorpse>();
		info.msg.lootableCorpse.playerName = this.playerName;
		info.msg.lootableCorpse.playerID = this.playerSteamID;
		if (info.forDisk)
		{
			if (this.inventory != null)
			{
				info.msg.storageBox = Facepunch.Pool.Get<StorageBox>();
				info.msg.storageBox.contents = this.inventory.Save();
			}
			else
			{
				Debug.LogWarning("Dropped item container without inventory: " + this.ToString());
			}
		}
	}

	// Token: 0x060007A4 RID: 1956 RVA: 0x00031928 File Offset: 0x0002FB28
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.lootableCorpse != null)
		{
			this.playerName = info.msg.lootableCorpse.playerName;
			this.playerSteamID = info.msg.lootableCorpse.playerID;
		}
		if (info.msg.storageBox != null)
		{
			if (this.inventory != null)
			{
				this.inventory.Load(info.msg.storageBox.contents);
			}
			else
			{
				Debug.LogWarning("Dropped item container without inventory: " + this.ToString());
			}
		}
	}

	// Token: 0x04000379 RID: 889
	public string lootPanelName = "generic";

	// Token: 0x0400037A RID: 890
	[NonSerialized]
	public ulong playerSteamID;

	// Token: 0x0400037B RID: 891
	[NonSerialized]
	public string _playerName;

	// Token: 0x0400037C RID: 892
	public global::ItemContainer inventory;
}
