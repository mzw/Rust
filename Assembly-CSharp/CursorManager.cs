﻿using System;
using UnityEngine;

// Token: 0x02000673 RID: 1651
public class CursorManager : SingletonComponent<global::CursorManager>
{
	// Token: 0x060020C8 RID: 8392 RVA: 0x000BA9E8 File Offset: 0x000B8BE8
	private void Update()
	{
		if (SingletonComponent<global::CursorManager>.Instance != this)
		{
			return;
		}
		if (global::CursorManager.iHoldOpen == 0 && global::CursorManager.iPreviousOpen == 0)
		{
			this.SwitchToGame();
		}
		else
		{
			this.SwitchToUI();
		}
		global::CursorManager.iPreviousOpen = global::CursorManager.iHoldOpen;
		global::CursorManager.iHoldOpen = 0;
	}

	// Token: 0x060020C9 RID: 8393 RVA: 0x000BAA3C File Offset: 0x000B8C3C
	private void SwitchToGame()
	{
		if (Cursor.lockState != 1)
		{
			Cursor.lockState = 1;
		}
		if (Cursor.visible)
		{
			Cursor.visible = false;
		}
	}

	// Token: 0x060020CA RID: 8394 RVA: 0x000BAA60 File Offset: 0x000B8C60
	private void SwitchToUI()
	{
		if (Cursor.lockState != null)
		{
			Cursor.lockState = 0;
		}
		if (!Cursor.visible)
		{
			Cursor.visible = true;
		}
	}

	// Token: 0x060020CB RID: 8395 RVA: 0x000BAA84 File Offset: 0x000B8C84
	public static void HoldOpen(bool cursorVisible = false)
	{
		global::CursorManager.iHoldOpen++;
	}

	// Token: 0x04001C27 RID: 7207
	private static int iHoldOpen;

	// Token: 0x04001C28 RID: 7208
	private static int iPreviousOpen;
}
