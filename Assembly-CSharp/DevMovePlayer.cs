﻿using System;
using UnityEngine;

// Token: 0x02000305 RID: 773
public class DevMovePlayer : global::BaseMonoBehaviour
{
	// Token: 0x06001357 RID: 4951 RVA: 0x00071CB8 File Offset: 0x0006FEB8
	public void Awake()
	{
		this.randRun = Random.Range(5f, 10f);
		this.player = base.GetComponent<global::BasePlayer>();
		if (this.Waypoints.Length > 0)
		{
			this.destination = this.Waypoints[0].position;
		}
		else
		{
			this.destination = base.transform.position;
		}
		if (this.player.isClient)
		{
			return;
		}
		if (this.player.eyes == null)
		{
			this.player.eyes = this.player.GetComponent<global::PlayerEyes>();
		}
		base.Invoke(new Action(this.LateSpawn), 1f);
	}

	// Token: 0x06001358 RID: 4952 RVA: 0x00071D74 File Offset: 0x0006FF74
	public void LateSpawn()
	{
		global::Item item = global::ItemManager.CreateByName("rifle.semiauto", 1, 0UL);
		this.player.inventory.GiveItem(item, this.player.inventory.containerBelt);
		this.player.UpdateActiveItem(item.uid);
		this.player.health = 100f;
	}

	// Token: 0x06001359 RID: 4953 RVA: 0x00071DD4 File Offset: 0x0006FFD4
	public void SetWaypoints(Transform[] wps)
	{
		this.Waypoints = wps;
		this.destination = wps[0].position;
	}

	// Token: 0x0600135A RID: 4954 RVA: 0x00071DEC File Offset: 0x0006FFEC
	public void Update()
	{
		if (this.player.isClient)
		{
			return;
		}
		if (!this.player.IsAlive() || this.player.IsWounded())
		{
			return;
		}
		if (Vector3.Distance(this.destination, base.transform.position) < 0.25f)
		{
			if (this.moveRandomly)
			{
				this.waypointIndex = Random.Range(0, this.Waypoints.Length);
			}
			else
			{
				this.waypointIndex++;
			}
			if (this.waypointIndex >= this.Waypoints.Length)
			{
				this.waypointIndex = 0;
			}
		}
		this.destination = this.Waypoints[this.waypointIndex].position;
		Vector3 normalized = (this.destination - base.transform.position).normalized;
		float running = Mathf.Sin(Time.time + this.randRun);
		float speed = this.player.GetSpeed(running, 0f);
		Vector3 position = base.transform.position;
		float range = 1f;
		LayerMask mask = 1403068673;
		RaycastHit raycastHit;
		if (global::TransformUtil.GetGroundInfo(base.transform.position + normalized * speed * Time.deltaTime, out raycastHit, range, mask, this.player.transform))
		{
			position = raycastHit.point;
		}
		base.transform.position = position;
		Vector3 normalized2 = (new Vector3(this.destination.x, 0f, this.destination.z) - new Vector3(this.player.GetEstimatedWorldPosition().x, 0f, this.player.GetEstimatedWorldPosition().z)).normalized;
		this.player.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x04000E06 RID: 3590
	public global::BasePlayer player;

	// Token: 0x04000E07 RID: 3591
	public Transform[] Waypoints;

	// Token: 0x04000E08 RID: 3592
	public bool moveRandomly;

	// Token: 0x04000E09 RID: 3593
	public Vector3 destination = Vector3.zero;

	// Token: 0x04000E0A RID: 3594
	public Vector3 lookPoint = Vector3.zero;

	// Token: 0x04000E0B RID: 3595
	private int waypointIndex;

	// Token: 0x04000E0C RID: 3596
	private float randRun;
}
