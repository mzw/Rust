﻿using System;
using UnityEngine;

// Token: 0x0200041E RID: 1054
public class EnvironmentVolume : MonoBehaviour
{
	// Token: 0x1700019B RID: 411
	// (get) Token: 0x0600180F RID: 6159 RVA: 0x00089074 File Offset: 0x00087274
	// (set) Token: 0x06001810 RID: 6160 RVA: 0x0008907C File Offset: 0x0008727C
	public BoxCollider trigger { get; private set; }

	// Token: 0x06001811 RID: 6161 RVA: 0x00089088 File Offset: 0x00087288
	protected void OnDrawGizmos()
	{
		if (this.StickyGizmos)
		{
			this.DrawGizmos();
		}
	}

	// Token: 0x06001812 RID: 6162 RVA: 0x0008909C File Offset: 0x0008729C
	protected void OnDrawGizmosSelected()
	{
		if (!this.StickyGizmos)
		{
			this.DrawGizmos();
		}
	}

	// Token: 0x06001813 RID: 6163 RVA: 0x000890B0 File Offset: 0x000872B0
	private void DrawGizmos()
	{
		Vector3 lossyScale = base.transform.lossyScale;
		Quaternion rotation = base.transform.rotation;
		Vector3 pos = base.transform.position + rotation * Vector3.Scale(lossyScale, this.Center);
		Vector3 size = Vector3.Scale(lossyScale, this.Size);
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
		global::GizmosUtil.DrawCube(pos, size, rotation);
		global::GizmosUtil.DrawWireCube(pos, size, rotation);
	}

	// Token: 0x06001814 RID: 6164 RVA: 0x00089134 File Offset: 0x00087334
	protected void Awake()
	{
		this.UpdateTrigger();
	}

	// Token: 0x06001815 RID: 6165 RVA: 0x0008913C File Offset: 0x0008733C
	public void UpdateTrigger()
	{
		if (!this.trigger)
		{
			this.trigger = base.gameObject.AddComponent<BoxCollider>();
		}
		this.trigger.isTrigger = true;
		this.trigger.center = this.Center;
		this.trigger.size = this.Size;
	}

	// Token: 0x0400129C RID: 4764
	public bool StickyGizmos;

	// Token: 0x0400129D RID: 4765
	public global::EnvironmentType Type = global::EnvironmentType.Underground;

	// Token: 0x0400129E RID: 4766
	public Vector3 Center = Vector3.zero;

	// Token: 0x0400129F RID: 4767
	public Vector3 Size = Vector3.one;
}
