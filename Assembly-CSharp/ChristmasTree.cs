﻿using System;
using UnityEngine;

// Token: 0x020000BF RID: 191
public class ChristmasTree : global::StorageContainer
{
	// Token: 0x06000A9E RID: 2718 RVA: 0x00048818 File Offset: 0x00046A18
	public override bool ItemFilter(global::Item item, int targetSlot)
	{
		global::ItemModXMasTreeDecoration component = item.info.GetComponent<global::ItemModXMasTreeDecoration>();
		if (component == null)
		{
			return false;
		}
		foreach (global::Item item2 in this.inventory.itemList)
		{
			if (item2.info == item.info)
			{
				return false;
			}
		}
		return base.ItemFilter(item, targetSlot);
	}

	// Token: 0x06000A9F RID: 2719 RVA: 0x000488B4 File Offset: 0x00046AB4
	public override void OnItemAddedOrRemoved(global::Item item, bool added)
	{
		global::ItemModXMasTreeDecoration component = item.info.GetComponent<global::ItemModXMasTreeDecoration>();
		if (component != null)
		{
			base.SetFlag((global::BaseEntity.Flags)component.flagsToChange, added, false);
		}
		base.OnItemAddedOrRemoved(item, added);
	}

	// Token: 0x0400055B RID: 1371
	public GameObject[] decorations;
}
