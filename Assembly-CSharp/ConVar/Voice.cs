﻿using System;

namespace ConVar
{
	// Token: 0x020002E4 RID: 740
	[ConsoleSystem.Factory("voice")]
	public class Voice : ConsoleSystem
	{
		// Token: 0x04000D8E RID: 3470
		[ClientVar(Saved = true)]
		public static bool loopback;

		// Token: 0x04000D8F RID: 3471
		[ClientVar]
		public static float ui_scale = 1f;

		// Token: 0x04000D90 RID: 3472
		[ClientVar]
		public static float ui_cut;

		// Token: 0x04000D91 RID: 3473
		[ClientVar]
		public static int ui_samples = 20;

		// Token: 0x04000D92 RID: 3474
		[ClientVar]
		public static float ui_lerp = 0.2f;
	}
}
