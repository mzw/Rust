﻿using System;
using System.Text;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002CF RID: 719
	[ConsoleSystem.Factory("music")]
	public class Music : ConsoleSystem
	{
		// Token: 0x0600125F RID: 4703 RVA: 0x0006C8BC File Offset: 0x0006AABC
		[ClientVar]
		public static void info(ConsoleSystem.Arg arg)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (global::MusicManager.instance == null)
			{
				stringBuilder.Append("No music manager was found");
			}
			else
			{
				stringBuilder.Append("Current music info: ");
				stringBuilder.AppendLine();
				stringBuilder.Append("  theme: " + global::MusicManager.instance.currentTheme);
				stringBuilder.AppendLine();
				stringBuilder.Append("  intensity: " + global::MusicManager.instance.intensity);
				stringBuilder.AppendLine();
				stringBuilder.Append("  next music: " + global::MusicManager.instance.nextMusic);
				stringBuilder.AppendLine();
				stringBuilder.Append("  current time: " + Time.time);
				stringBuilder.AppendLine();
			}
			arg.ReplyWith(stringBuilder.ToString());
		}

		// Token: 0x04000D22 RID: 3362
		[ClientVar]
		public static bool enabled = true;

		// Token: 0x04000D23 RID: 3363
		[ClientVar]
		public static int songGapMin = 240;

		// Token: 0x04000D24 RID: 3364
		[ClientVar]
		public static int songGapMax = 480;
	}
}
