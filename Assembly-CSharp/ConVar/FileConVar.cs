﻿using System;

namespace ConVar
{
	// Token: 0x020002BE RID: 702
	[ConsoleSystem.Factory("file")]
	public class FileConVar : ConsoleSystem
	{
		// Token: 0x04000CF8 RID: 3320
		[ClientVar]
		public static bool debug;

		// Token: 0x04000CF9 RID: 3321
		[ClientVar]
		public static bool time;
	}
}
