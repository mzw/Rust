﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002BF RID: 703
	[ConsoleSystem.Factory("fps")]
	public class FPS : ConsoleSystem
	{
		// Token: 0x1700013F RID: 319
		// (get) Token: 0x060011EF RID: 4591 RVA: 0x0006AAC4 File Offset: 0x00068CC4
		// (set) Token: 0x060011F0 RID: 4592 RVA: 0x0006AACC File Offset: 0x00068CCC
		[ServerVar(Saved = true)]
		[ClientVar(Saved = true)]
		public static int limit
		{
			get
			{
				return Application.targetFrameRate;
			}
			set
			{
				Application.targetFrameRate = value;
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x060011F1 RID: 4593 RVA: 0x0006AAD4 File Offset: 0x00068CD4
		// (set) Token: 0x060011F2 RID: 4594 RVA: 0x0006AADC File Offset: 0x00068CDC
		[ClientVar]
		public static int graph
		{
			get
			{
				return FPS.m_graph;
			}
			set
			{
				FPS.m_graph = value;
				if (!global::MainCamera.mainCamera)
				{
					return;
				}
				global::FPSGraph component = global::MainCamera.mainCamera.GetComponent<global::FPSGraph>();
				if (!component)
				{
					return;
				}
				component.Refresh();
			}
		}

		// Token: 0x04000CFA RID: 3322
		private static int m_graph;
	}
}
