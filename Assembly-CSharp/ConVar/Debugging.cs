﻿using System;
using System.Globalization;
using Facepunch.Unity;
using Rust;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002AE RID: 686
	[ConsoleSystem.Factory("debug")]
	public class Debugging : ConsoleSystem
	{
		// Token: 0x060011AE RID: 4526 RVA: 0x00069B18 File Offset: 0x00067D18
		[ServerVar]
		[ClientVar]
		public static void renderinfo(ConsoleSystem.Arg arg)
		{
			Facepunch.Unity.RenderInfo.GenerateReport();
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x060011B0 RID: 4528 RVA: 0x00069B30 File Offset: 0x00067D30
		// (set) Token: 0x060011AF RID: 4527 RVA: 0x00069B20 File Offset: 0x00067D20
		[ServerVar]
		[ClientVar]
		public static bool log
		{
			get
			{
				return Debug.unityLogger.logEnabled;
			}
			set
			{
				Debug.unityLogger.logEnabled = value;
			}
		}

		// Token: 0x060011B1 RID: 4529 RVA: 0x00069B3C File Offset: 0x00067D3C
		[ServerVar(Help = "Takes you in and out of your current network group, causing you to delete and then download all entities in your PVS again")]
		public static void flushgroup(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (basePlayer == null)
			{
				return;
			}
			basePlayer.net.SwitchGroup(global::BaseNetworkable.LimboNetworkGroup);
			basePlayer.UpdateNetworkGroup();
		}

		// Token: 0x060011B2 RID: 4530 RVA: 0x00069B74 File Offset: 0x00067D74
		[ServerVar(Help = "Break the current held object")]
		public static void breakheld(ConsoleSystem.Arg arg)
		{
			global::Item activeItem = arg.Player().GetActiveItem();
			if (activeItem == null)
			{
				return;
			}
			activeItem.LoseCondition(activeItem.condition * 2f);
		}

		// Token: 0x060011B3 RID: 4531 RVA: 0x00069BA8 File Offset: 0x00067DA8
		[ServerVar(Help = "Break all the items in your inventory whose name match the passed string")]
		public static void breakitem(ConsoleSystem.Arg arg)
		{
			string @string = arg.GetString(0, string.Empty);
			foreach (global::Item item in arg.Player().inventory.containerMain.itemList)
			{
				if (StringEx.Contains(item.info.shortname, @string, CompareOptions.IgnoreCase))
				{
					if (item.hasCondition)
					{
						item.LoseCondition(item.condition * 2f);
					}
				}
			}
		}

		// Token: 0x060011B4 RID: 4532 RVA: 0x00069C58 File Offset: 0x00067E58
		[ServerVar]
		public static void hurt(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			global::HitInfo hitInfo = new global::HitInfo(basePlayer, basePlayer, Rust.DamageType.Bullet, (float)arg.GetInt(0, 1));
			string @string = arg.GetString(1, string.Empty);
			if (!string.IsNullOrEmpty(@string))
			{
				hitInfo.HitBone = global::StringPool.Get(@string);
			}
			basePlayer.OnAttacked(hitInfo);
		}

		// Token: 0x060011B5 RID: 4533 RVA: 0x00069CAC File Offset: 0x00067EAC
		[ServerVar]
		public static void eat(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			basePlayer.metabolism.ApplyChange(global::MetabolismAttribute.Type.Calories, (float)arg.GetInt(0, 1), (float)arg.GetInt(1, 1));
		}

		// Token: 0x060011B6 RID: 4534 RVA: 0x00069CE0 File Offset: 0x00067EE0
		[ServerVar]
		public static void drink(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			basePlayer.metabolism.ApplyChange(global::MetabolismAttribute.Type.Hydration, (float)arg.GetInt(0, 1), (float)arg.GetInt(1, 1));
		}

		// Token: 0x04000CC9 RID: 3273
		[ServerVar]
		[ClientVar]
		public static bool checktriggers;

		// Token: 0x04000CCA RID: 3274
		[ServerVar(Help = "Do not damage any items")]
		public static bool disablecondition;
	}
}
