﻿using System;

namespace ConVar
{
	// Token: 0x020002E5 RID: 741
	[ConsoleSystem.Factory("water")]
	public class Water : ConsoleSystem
	{
		// Token: 0x04000D93 RID: 3475
		[ClientVar(Saved = true)]
		public static int quality = 1;

		// Token: 0x04000D94 RID: 3476
		[ClientVar(Saved = true)]
		public static int reflections = 1;
	}
}
