﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x0200029E RID: 670
	[ConsoleSystem.Factory("audio")]
	public class Audio : ConsoleSystem
	{
		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06001178 RID: 4472 RVA: 0x00068D94 File Offset: 0x00066F94
		// (set) Token: 0x06001179 RID: 4473 RVA: 0x00068D9C File Offset: 0x00066F9C
		[ClientVar(Help = "Volume", Saved = true)]
		public static int speakers
		{
			get
			{
				return UnityEngine.AudioSettings.speakerMode;
			}
			set
			{
				value = Mathf.Clamp(value, 2, 7);
				AudioConfiguration configuration = UnityEngine.AudioSettings.GetConfiguration();
				configuration.speakerMode = value;
				using (TimeWarning.New("Audio Settings Reset", 0.25f))
				{
					UnityEngine.AudioSettings.Reset(configuration);
				}
			}
		}

		// Token: 0x04000CA2 RID: 3234
		[ClientVar(Help = "Volume", Saved = true)]
		public static float master = 1f;

		// Token: 0x04000CA3 RID: 3235
		[ClientVar(Help = "Volume", Saved = true)]
		public static float musicvolume = 1f;

		// Token: 0x04000CA4 RID: 3236
		[ClientVar(Help = "Volume", Saved = true)]
		public static float musicvolumemenu = 1f;

		// Token: 0x04000CA5 RID: 3237
		[ClientVar(Help = "Volume", Saved = true)]
		public static float game = 1f;

		// Token: 0x04000CA6 RID: 3238
		[ClientVar(Help = "Volume", Saved = true)]
		public static float voices = 1f;

		// Token: 0x04000CA7 RID: 3239
		[ClientVar(Help = "Ambience System")]
		public static bool ambience = true;

		// Token: 0x04000CA8 RID: 3240
		[ClientVar(Help = "Max ms per frame to spend updating sounds")]
		public static float framebudget = 0.3f;

		// Token: 0x04000CA9 RID: 3241
		[ClientVar(Help = "Use more advanced sound occlusion", Saved = true)]
		public static bool advancedocclusion;
	}
}
