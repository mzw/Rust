﻿using System;

namespace ConVar
{
	// Token: 0x020002AF RID: 687
	[ConsoleSystem.Factory("decay")]
	public class Decay : ConsoleSystem
	{
		// Token: 0x04000CCB RID: 3275
		[ServerVar(Help = "Maximum distance to test to see if a structure is outside, higher values are slower but accurate for huge buildings")]
		public static float outside_test_range = 50f;

		// Token: 0x04000CCC RID: 3276
		[ServerVar]
		public static float tick = 600f;

		// Token: 0x04000CCD RID: 3277
		[ServerVar]
		public static float scale = 1f;

		// Token: 0x04000CCE RID: 3278
		[ServerVar]
		public static bool debug;

		// Token: 0x04000CCF RID: 3279
		[ServerVar(Help = "Is upkeep enabled")]
		public static bool upkeep = true;

		// Token: 0x04000CD0 RID: 3280
		[ServerVar(Help = "How many minutes does the upkeep cost last? default : 1440 (24 hours)")]
		public static float upkeep_period_minutes = 1440f;

		// Token: 0x04000CD1 RID: 3281
		[ServerVar(Help = "How many minutes can the upkeep cost last after the cupboard was destroyed? default : 1440 (24 hours)")]
		public static float upkeep_grief_protection = 1440f;

		// Token: 0x04000CD2 RID: 3282
		[ServerVar(Help = "Scale at which objects heal when upkeep conditions are met, default of 1 is same rate at which they decay")]
		public static float upkeep_heal_scale = 1f;

		// Token: 0x04000CD3 RID: 3283
		[ServerVar(Help = "Scale at which objects decay when they are inside, default of 0.1")]
		public static float upkeep_inside_decay_scale = 0.1f;

		// Token: 0x04000CD4 RID: 3284
		[ServerVar(Help = "When set to a value above 0 everything will decay with this delay")]
		public static float delay_override;

		// Token: 0x04000CD5 RID: 3285
		[ServerVar(Help = "How long should this building grade decay be delayed when not protected by upkeep, in hours")]
		public static float delay_twig;

		// Token: 0x04000CD6 RID: 3286
		[ServerVar(Help = "How long should this building grade decay be delayed when not protected by upkeep, in hours")]
		public static float delay_wood;

		// Token: 0x04000CD7 RID: 3287
		[ServerVar(Help = "How long should this building grade decay be delayed when not protected by upkeep, in hours")]
		public static float delay_stone;

		// Token: 0x04000CD8 RID: 3288
		[ServerVar(Help = "How long should this building grade decay be delayed when not protected by upkeep, in hours")]
		public static float delay_metal;

		// Token: 0x04000CD9 RID: 3289
		[ServerVar(Help = "How long should this building grade decay be delayed when not protected by upkeep, in hours")]
		public static float delay_toptier;

		// Token: 0x04000CDA RID: 3290
		[ServerVar(Help = "When set to a value above 0 everything will decay with this duration")]
		public static float duration_override;

		// Token: 0x04000CDB RID: 3291
		[ServerVar(Help = "How long should this building grade take to decay when not protected by upkeep, in hours")]
		public static float duration_twig = 1f;

		// Token: 0x04000CDC RID: 3292
		[ServerVar(Help = "How long should this building grade take to decay when not protected by upkeep, in hours")]
		public static float duration_wood = 3f;

		// Token: 0x04000CDD RID: 3293
		[ServerVar(Help = "How long should this building grade take to decay when not protected by upkeep, in hours")]
		public static float duration_stone = 5f;

		// Token: 0x04000CDE RID: 3294
		[ServerVar(Help = "How long should this building grade take to decay when not protected by upkeep, in hours")]
		public static float duration_metal = 8f;

		// Token: 0x04000CDF RID: 3295
		[ServerVar(Help = "How long should this building grade take to decay when not protected by upkeep, in hours")]
		public static float duration_toptier = 12f;

		// Token: 0x04000CE0 RID: 3296
		[ServerVar(Help = "Between 0 and this value are considered bracket 0 and will cost bracket_0_costfraction per upkeep period to maintain")]
		public static int bracket_0_blockcount = 15;

		// Token: 0x04000CE1 RID: 3297
		[ServerVar(Help = "blocks within bracket 0 will cost this fraction per upkeep period to maintain")]
		public static float bracket_0_costfraction = 0.1f;

		// Token: 0x04000CE2 RID: 3298
		[ServerVar(Help = "Between bracket_0_blockcount and this value are considered bracket 1 and will cost bracket_1_costfraction per upkeep period to maintain")]
		public static int bracket_1_blockcount = 50;

		// Token: 0x04000CE3 RID: 3299
		[ServerVar(Help = "blocks within bracket 1 will cost this fraction per upkeep period to maintain")]
		public static float bracket_1_costfraction = 0.15f;

		// Token: 0x04000CE4 RID: 3300
		[ServerVar(Help = "Between bracket_1_blockcount and this value are considered bracket 2 and will cost bracket_2_costfraction per upkeep period to maintain")]
		public static int bracket_2_blockcount = 125;

		// Token: 0x04000CE5 RID: 3301
		[ServerVar(Help = "blocks within bracket 2 will cost this fraction per upkeep period to maintain")]
		public static float bracket_2_costfraction = 0.2f;

		// Token: 0x04000CE6 RID: 3302
		[ServerVar(Help = "Between bracket_2_blockcount and this value (and beyond) are considered bracket 3 and will cost bracket_3_costfraction per upkeep period to maintain")]
		public static int bracket_3_blockcount = 200;

		// Token: 0x04000CE7 RID: 3303
		[ServerVar(Help = "blocks within bracket 3 will cost this fraction per upkeep period to maintain")]
		public static float bracket_3_costfraction = 0.333f;
	}
}
