﻿using System;
using Facepunch.Steamworks;
using Rust;

namespace ConVar
{
	// Token: 0x020002E7 RID: 743
	[ConsoleSystem.Factory("workshop")]
	public class Workshop : ConsoleSystem
	{
		// Token: 0x060012CC RID: 4812 RVA: 0x0006E0EC File Offset: 0x0006C2EC
		[ServerVar]
		public static void print_approved_skins(ConsoleSystem.Arg arg)
		{
			if (Global.SteamServer != null && Global.SteamServer.Inventory.Definitions != null)
			{
				TextTable textTable = new TextTable();
				textTable.AddColumn("name");
				textTable.AddColumn("itemshortname");
				textTable.AddColumn("workshopid");
				textTable.AddColumn("workshopdownload");
				foreach (Inventory.Definition definition in Global.SteamServer.Inventory.Definitions)
				{
					string name = definition.Name;
					string stringProperty = definition.GetStringProperty("itemshortname");
					string stringProperty2 = definition.GetStringProperty("workshopid");
					string stringProperty3 = definition.GetStringProperty("workshopdownload");
					textTable.AddRow(new string[]
					{
						name,
						stringProperty,
						stringProperty2,
						stringProperty3
					});
				}
				arg.ReplyWith(textTable.ToString());
			}
		}
	}
}
