﻿using System;

namespace ConVar
{
	// Token: 0x020002E8 RID: 744
	[ConsoleSystem.Factory("world")]
	public class World : ConsoleSystem
	{
		// Token: 0x060012CE RID: 4814 RVA: 0x0006E1D4 File Offset: 0x0006C3D4
		[ClientVar]
		public static void monuments(ConsoleSystem.Arg arg)
		{
			if (!global::TerrainMeta.Path)
			{
				return;
			}
			TextTable textTable = new TextTable();
			textTable.AddColumn("type");
			textTable.AddColumn("name");
			textTable.AddColumn("pos");
			foreach (global::MonumentInfo monumentInfo in global::TerrainMeta.Path.Monuments)
			{
				textTable.AddRow(new string[]
				{
					monumentInfo.Type.ToString(),
					monumentInfo.name,
					monumentInfo.transform.position.ToString()
				});
			}
			arg.ReplyWith(textTable.ToString());
		}

		// Token: 0x04000D95 RID: 3477
		[ServerVar]
		[ClientVar]
		public static bool cache = true;
	}
}
