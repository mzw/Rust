﻿using System;

namespace ConVar
{
	// Token: 0x020002DC RID: 732
	[ConsoleSystem.Factory("SSS")]
	public class SSS : ConsoleSystem
	{
		// Token: 0x04000D77 RID: 3447
		[ClientVar(Saved = true)]
		public static bool enabled = true;

		// Token: 0x04000D78 RID: 3448
		[ClientVar(Saved = true)]
		public static int quality;

		// Token: 0x04000D79 RID: 3449
		[ClientVar(Saved = true)]
		public static bool halfres = true;

		// Token: 0x04000D7A RID: 3450
		[ClientVar(Saved = true)]
		public static float scale = 1f;
	}
}
