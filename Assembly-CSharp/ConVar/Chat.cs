﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using Facepunch;
using Facepunch.Math;
using Oxide.Core;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002A3 RID: 675
	[ConsoleSystem.Factory("chat")]
	public class Chat : ConsoleSystem
	{
		// Token: 0x0600118A RID: 4490 RVA: 0x00069070 File Offset: 0x00067270
		public static void Broadcast(string message, string username = "SERVER", string color = "#eee", ulong userid = 0UL)
		{
			if (Interface.CallHook("OnServerMessage", new object[]
			{
				message,
				username,
				color,
				userid
			}) != null)
			{
				return;
			}
			global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
			{
				0,
				string.Concat(new string[]
				{
					"<color=",
					color,
					">",
					username,
					"</color> ",
					message
				})
			});
			Chat.ChatEntry chatEntry = new Chat.ChatEntry
			{
				Message = message,
				UserId = userid,
				Username = username,
				Color = color,
				Time = Epoch.Current
			};
			Chat.History.Add(chatEntry);
			Facepunch.RCon.Broadcast(Facepunch.RCon.LogType.Chat, chatEntry);
		}

		// Token: 0x0600118B RID: 4491 RVA: 0x00069140 File Offset: 0x00067340
		[ServerUserVar]
		public static void say(ConsoleSystem.Arg arg)
		{
			if (!Chat.enabled)
			{
				arg.ReplyWith("Chat is disabled.");
				return;
			}
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.HasPlayerFlag(global::BasePlayer.PlayerFlags.ChatMute))
			{
				return;
			}
			if (!basePlayer.IsAdmin && !basePlayer.IsDeveloper)
			{
				if (basePlayer.NextChatTime == 0f)
				{
					basePlayer.NextChatTime = Time.realtimeSinceStartup - 30f;
				}
				if (basePlayer.NextChatTime > Time.realtimeSinceStartup)
				{
					basePlayer.NextChatTime += 2f;
					float num = basePlayer.NextChatTime - Time.realtimeSinceStartup;
					global::ConsoleNetwork.SendClientCommand(basePlayer.net.connection, "chat.add", new object[]
					{
						0,
						"You're chatting too fast - try again in " + (num + 0.5f).ToString("0") + " seconds"
					});
					if (num > 120f)
					{
						basePlayer.Kick("Chatting too fast");
					}
					return;
				}
			}
			string text = arg.GetString(0, "text").Trim();
			if (text.Length > 128)
			{
				text = text.Substring(0, 128);
			}
			if (text.Length <= 0)
			{
				return;
			}
			if (Interface.CallHook("IOnPlayerChat", new object[]
			{
				arg
			}) != null)
			{
				return;
			}
			if (text.StartsWith("/") || text.StartsWith("\\"))
			{
				return;
			}
			if (text.Contains("<"))
			{
				if (StringEx.Contains(text, "<size", CompareOptions.IgnoreCase))
				{
					return;
				}
				if (StringEx.Contains(text, "<color", CompareOptions.IgnoreCase))
				{
					return;
				}
				if (StringEx.Contains(text, "<material", CompareOptions.IgnoreCase))
				{
					return;
				}
				if (StringEx.Contains(text, "<quad", CompareOptions.IgnoreCase))
				{
					return;
				}
				if (StringEx.Contains(text, "<b>", CompareOptions.IgnoreCase))
				{
					return;
				}
				if (StringEx.Contains(text, "<i>", CompareOptions.IgnoreCase))
				{
					return;
				}
			}
			if (Chat.serverlog)
			{
				global::ServerConsole.PrintColoured(new object[]
				{
					ConsoleColor.DarkYellow,
					basePlayer.displayName + ": ",
					ConsoleColor.DarkGreen,
					text
				});
				DebugEx.Log(string.Format("[CHAT] {0} : {1}", basePlayer.ToString(), text), 0);
			}
			string text2 = "#5af";
			if (basePlayer.IsAdmin)
			{
				text2 = "#af5";
			}
			if (basePlayer.IsDeveloper)
			{
				text2 = "#fa5";
			}
			string text3 = basePlayer.displayName;
			text3 = text3.Replace('<', '[').Replace('>', ']');
			basePlayer.NextChatTime = Time.realtimeSinceStartup + 1.5f;
			Chat.ChatEntry chatEntry = new Chat.ChatEntry
			{
				Message = text,
				UserId = basePlayer.userID,
				Username = basePlayer.displayName,
				Color = text2,
				Time = Epoch.Current
			};
			Chat.History.Add(chatEntry);
			Facepunch.RCon.Broadcast(Facepunch.RCon.LogType.Chat, chatEntry);
			if (Server.globalchat)
			{
				global::ConsoleNetwork.BroadcastToAllClients("chat.add2", new object[]
				{
					basePlayer.userID,
					text,
					text3,
					text2,
					1f
				});
				arg.ReplyWith(string.Empty);
				return;
			}
			float num2 = 2500f;
			foreach (global::BasePlayer basePlayer2 in global::BasePlayer.activePlayerList)
			{
				float sqrMagnitude = (basePlayer2.transform.position - basePlayer.transform.position).sqrMagnitude;
				if (sqrMagnitude <= num2)
				{
					global::ConsoleNetwork.SendClientCommand(basePlayer2.net.connection, "chat.add2", new object[]
					{
						basePlayer.userID,
						text,
						text3,
						text2,
						Mathf.Clamp01(num2 - sqrMagnitude + 0.2f)
					});
				}
			}
			arg.ReplyWith(string.Empty);
		}

		// Token: 0x0600118C RID: 4492 RVA: 0x00069580 File Offset: 0x00067780
		[ServerVar]
		[Help("Return the last x lines of the console. Default is 200")]
		public static IEnumerable<Chat.ChatEntry> tail(ConsoleSystem.Arg arg)
		{
			int @int = arg.GetInt(0, 200);
			int num = Chat.History.Count - @int;
			if (num < 0)
			{
				num = 0;
			}
			return Chat.History.Skip(num);
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x000695BC File Offset: 0x000677BC
		[ServerVar]
		[Help("Search the console for a particular string")]
		public static IEnumerable<Chat.ChatEntry> search(ConsoleSystem.Arg arg)
		{
			string search = arg.GetString(0, null);
			if (search == null)
			{
				return Enumerable.Empty<Chat.ChatEntry>();
			}
			return from x in Chat.History
			where x.Message.Length < 4096 && StringEx.Contains(x.Message, search, CompareOptions.IgnoreCase)
			select x;
		}

		// Token: 0x04000CBA RID: 3258
		private const float textRange = 50f;

		// Token: 0x04000CBB RID: 3259
		private const float textVolumeBoost = 0.2f;

		// Token: 0x04000CBC RID: 3260
		[ServerVar]
		[ClientVar]
		public static bool enabled = true;

		// Token: 0x04000CBD RID: 3261
		private static List<Chat.ChatEntry> History = new List<Chat.ChatEntry>();

		// Token: 0x04000CBE RID: 3262
		[ServerVar]
		public static bool serverlog = true;

		// Token: 0x020002A4 RID: 676
		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct ChatEntry
		{
			// Token: 0x17000134 RID: 308
			// (get) Token: 0x0600118F RID: 4495 RVA: 0x0006961C File Offset: 0x0006781C
			// (set) Token: 0x06001190 RID: 4496 RVA: 0x00069624 File Offset: 0x00067824
			public string Message { get; set; }

			// Token: 0x17000135 RID: 309
			// (get) Token: 0x06001191 RID: 4497 RVA: 0x00069630 File Offset: 0x00067830
			// (set) Token: 0x06001192 RID: 4498 RVA: 0x00069638 File Offset: 0x00067838
			public ulong UserId { get; set; }

			// Token: 0x17000136 RID: 310
			// (get) Token: 0x06001193 RID: 4499 RVA: 0x00069644 File Offset: 0x00067844
			// (set) Token: 0x06001194 RID: 4500 RVA: 0x0006964C File Offset: 0x0006784C
			public string Username { get; set; }

			// Token: 0x17000137 RID: 311
			// (get) Token: 0x06001195 RID: 4501 RVA: 0x00069658 File Offset: 0x00067858
			// (set) Token: 0x06001196 RID: 4502 RVA: 0x00069660 File Offset: 0x00067860
			public string Color { get; set; }

			// Token: 0x17000138 RID: 312
			// (get) Token: 0x06001197 RID: 4503 RVA: 0x0006966C File Offset: 0x0006786C
			// (set) Token: 0x06001198 RID: 4504 RVA: 0x00069674 File Offset: 0x00067874
			public int Time { get; set; }
		}
	}
}
