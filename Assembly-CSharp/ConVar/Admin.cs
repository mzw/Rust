﻿using System;
using System.Globalization;
using System.Linq;
using Facepunch;
using Facepunch.Extend;
using Network;
using UnityEngine;

namespace ConVar
{
	// Token: 0x02000298 RID: 664
	[ConsoleSystem.Factory("global")]
	public class Admin : ConsoleSystem
	{
		// Token: 0x0600114A RID: 4426 RVA: 0x00066F04 File Offset: 0x00065104
		[ServerVar(Help = "Print out currently connected clients")]
		public static void status(ConsoleSystem.Arg arg)
		{
			string @string = arg.GetString(0, string.Empty);
			string text = string.Empty;
			if (@string.Length == 0)
			{
				text = text + "hostname: " + Server.hostname + "\n";
				text = text + "version : " + 2065.ToString() + " secure (secure mode enabled, connected to Steam3)\n";
				text = text + "map     : " + Server.level + "\n";
				string text2 = text;
				text = string.Concat(new object[]
				{
					text2,
					"players : ",
					global::BasePlayer.activePlayerList.Count<global::BasePlayer>(),
					" (",
					Server.maxplayers,
					" max) (",
					SingletonComponent<global::ServerMgr>.Instance.connectionQueue.Queued,
					" queued) (",
					SingletonComponent<global::ServerMgr>.Instance.connectionQueue.Joining,
					" joining)\n\n"
				});
			}
			TextTable textTable = new TextTable();
			textTable.AddColumn("id");
			textTable.AddColumn("name");
			textTable.AddColumn("ping");
			textTable.AddColumn("connected");
			textTable.AddColumn("addr");
			textTable.AddColumn("owner");
			textTable.AddColumn("violation");
			textTable.AddColumn("kicks");
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				try
				{
					if (basePlayer.IsValid())
					{
						string userIDString = basePlayer.UserIDString;
						if (basePlayer.net.connection == null)
						{
							textTable.AddRow(new string[]
							{
								userIDString,
								"NO CONNECTION"
							});
						}
						else
						{
							string text3 = basePlayer.net.connection.ownerid.ToString();
							string text4 = StringExtensions.QuoteSafe(basePlayer.GetSubName(32));
							string text5 = Net.sv.GetAveragePing(basePlayer.net.connection).ToString();
							string text6 = basePlayer.net.connection.ipaddress;
							string text7 = basePlayer.violationLevel.ToString("0.0");
							string text8 = basePlayer.GetAntiHackKicks().ToString();
							if (!arg.IsAdmin && !arg.IsRcon)
							{
								text6 = "xx.xxx.xx.xxx";
							}
							string text9 = basePlayer.net.connection.GetSecondsConnected().ToString() + "s";
							if (@string.Length <= 0 || StringEx.Contains(text4, @string, CompareOptions.IgnoreCase) || userIDString.Contains(@string) || text3.Contains(@string) || text6.Contains(@string))
							{
								textTable.AddRow(new string[]
								{
									userIDString,
									text4,
									text5,
									text9,
									text6,
									(!(text3 == userIDString)) ? text3 : string.Empty,
									text7,
									text8
								});
							}
						}
					}
				}
				catch (Exception ex)
				{
					textTable.AddRow(new string[]
					{
						basePlayer.UserIDString,
						StringExtensions.QuoteSafe(ex.Message)
					});
				}
			}
			arg.ReplyWith(text + textTable.ToString());
		}

		// Token: 0x0600114B RID: 4427 RVA: 0x000672DC File Offset: 0x000654DC
		[ServerVar(Help = "Print out stats of currently connected clients")]
		public static void stats(ConsoleSystem.Arg arg)
		{
			TextTable table = new TextTable();
			table.AddColumn("id");
			table.AddColumn("name");
			table.AddColumn("time");
			table.AddColumn("kills");
			table.AddColumn("deaths");
			table.AddColumn("suicides");
			table.AddColumn("player");
			table.AddColumn("building");
			table.AddColumn("entity");
			Action<ulong, string> action = delegate(ulong id, string name)
			{
				global::ServerStatistics.Storage storage = global::ServerStatistics.Get(id);
				string text2 = TimeSpan.FromSeconds((double)storage.Get("time")).ToShortString();
				string text3 = storage.Get("kill_player").ToString();
				string text4 = (storage.Get("deaths") - storage.Get("death_suicide")).ToString();
				string text5 = storage.Get("death_suicide").ToString();
				string str = storage.Get("hit_player_direct_los").ToString();
				string str2 = storage.Get("hit_player_indirect_los").ToString();
				string str3 = storage.Get("hit_building_direct_los").ToString();
				string str4 = storage.Get("hit_building_indirect_los").ToString();
				string str5 = storage.Get("hit_entity_direct_los").ToString();
				string str6 = storage.Get("hit_entity_indirect_los").ToString();
				table.AddRow(new string[]
				{
					id.ToString(),
					name,
					text2,
					text3,
					text4,
					text5,
					str + " / " + str2,
					str3 + " / " + str4,
					str5 + " / " + str6
				});
			};
			ulong filterID = arg.GetUInt64(0, 0UL);
			if (filterID == 0UL)
			{
				string @string = arg.GetString(0, string.Empty);
				foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
				{
					try
					{
						if (basePlayer.IsValid())
						{
							string text = StringExtensions.QuoteSafe(basePlayer.GetSubName(32));
							if (@string.Length <= 0 || StringEx.Contains(text, @string, CompareOptions.IgnoreCase))
							{
								action(basePlayer.userID, text);
							}
						}
					}
					catch (Exception ex)
					{
						table.AddRow(new string[]
						{
							basePlayer.UserIDString,
							StringExtensions.QuoteSafe(ex.Message)
						});
					}
				}
			}
			else
			{
				string arg2 = "N/A";
				global::BasePlayer basePlayer2 = global::BasePlayer.activePlayerList.Find((global::BasePlayer p) => p.userID == filterID);
				if (basePlayer2)
				{
					arg2 = StringExtensions.QuoteSafe(basePlayer2.GetSubName(32));
				}
				action(filterID, arg2);
			}
			arg.ReplyWith(table.ToString());
		}

		// Token: 0x0600114C RID: 4428 RVA: 0x000674F4 File Offset: 0x000656F4
		[ServerVar]
		public static void kick(ConsoleSystem.Arg arg)
		{
			global::BasePlayer player = arg.GetPlayer(0);
			if (!player || player.net == null || player.net.connection == null)
			{
				arg.ReplyWith("Player not found");
				return;
			}
			string @string = arg.GetString(1, "no reason given");
			arg.ReplyWith("Kicked: " + player.displayName);
			Chat.Broadcast(string.Concat(new string[]
			{
				"Kicking ",
				player.displayName,
				" (",
				@string,
				")"
			}), "SERVER", "#eee", 0UL);
			player.Kick("Kicked: " + arg.GetString(1, "No Reason Given"));
		}

		// Token: 0x0600114D RID: 4429 RVA: 0x000675BC File Offset: 0x000657BC
		[ServerVar]
		public static void kickall(ConsoleSystem.Arg arg)
		{
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList.ToArray())
			{
				basePlayer.Kick("Kicked: " + arg.GetString(1, "No Reason Given"));
			}
		}

		// Token: 0x0600114E RID: 4430 RVA: 0x00067608 File Offset: 0x00065808
		[ServerVar]
		public static void ban(ConsoleSystem.Arg arg)
		{
			global::BasePlayer player = arg.GetPlayer(0);
			if (!player || player.net == null || player.net.connection == null)
			{
				arg.ReplyWith("Player not found");
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(player.userID);
			if (user != null && user.group == global::ServerUsers.UserGroup.Banned)
			{
				arg.ReplyWith("User " + player.userID + " is already banned");
				return;
			}
			string @string = arg.GetString(1, "No Reason Given");
			global::ServerUsers.Set(player.userID, global::ServerUsers.UserGroup.Banned, player.displayName, @string);
			string text = string.Empty;
			if (player.IsConnected && player.net.connection.ownerid != player.net.connection.userid)
			{
				text = text + " and also banned ownerid " + player.net.connection.ownerid;
				global::ServerUsers.Set(player.net.connection.ownerid, global::ServerUsers.UserGroup.Banned, player.displayName, arg.GetString(1, "Family share owner of " + player.net.connection.userid));
			}
			global::ServerUsers.Save();
			arg.ReplyWith(string.Concat(new object[]
			{
				"Kickbanned User: ",
				player.userID,
				" - ",
				player.displayName,
				text
			}));
			Chat.Broadcast(string.Concat(new string[]
			{
				"Kickbanning ",
				player.displayName,
				" (",
				@string,
				")"
			}), "SERVER", "#eee", 0UL);
			Net.sv.Kick(player.net.connection, "Banned: " + @string);
		}

		// Token: 0x0600114F RID: 4431 RVA: 0x000677EC File Offset: 0x000659EC
		[ServerVar]
		public static void moderatorid(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			string @string = arg.GetString(1, "unnamed");
			string string2 = arg.GetString(2, "no reason");
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(@uint);
			if (user != null && user.group == global::ServerUsers.UserGroup.Moderator)
			{
				arg.ReplyWith("User " + @uint + " is already a Moderator");
				return;
			}
			global::ServerUsers.Set(@uint, global::ServerUsers.UserGroup.Moderator, @string, string2);
			arg.ReplyWith(string.Concat(new object[]
			{
				"Added moderator ",
				@string,
				", steamid ",
				@uint
			}));
		}

		// Token: 0x06001150 RID: 4432 RVA: 0x000678B0 File Offset: 0x00065AB0
		[ServerVar]
		public static void ownerid(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			string @string = arg.GetString(1, "unnamed");
			string string2 = arg.GetString(2, "no reason");
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(@uint);
			if (user != null && user.group == global::ServerUsers.UserGroup.Owner)
			{
				arg.ReplyWith("User " + @uint + " is already an Owner");
				return;
			}
			global::ServerUsers.Set(@uint, global::ServerUsers.UserGroup.Owner, @string, string2);
			arg.ReplyWith(string.Concat(new object[]
			{
				"Added owner ",
				@string,
				", steamid ",
				@uint
			}));
		}

		// Token: 0x06001151 RID: 4433 RVA: 0x00067974 File Offset: 0x00065B74
		[ServerVar]
		public static void removemoderator(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(@uint);
			if (user == null || user.group != global::ServerUsers.UserGroup.Moderator)
			{
				arg.ReplyWith("User " + @uint + " isn't a moderator");
				return;
			}
			global::ServerUsers.Remove(@uint);
			arg.ReplyWith("Removed Moderator: " + @uint);
		}

		// Token: 0x06001152 RID: 4434 RVA: 0x00067A04 File Offset: 0x00065C04
		[ServerVar]
		public static void removeowner(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(@uint);
			if (user == null || user.group != global::ServerUsers.UserGroup.Owner)
			{
				arg.ReplyWith("User " + @uint + " isn't an owner");
				return;
			}
			global::ServerUsers.Remove(@uint);
			arg.ReplyWith("Removed Owner: " + @uint);
		}

		// Token: 0x06001153 RID: 4435 RVA: 0x00067A94 File Offset: 0x00065C94
		[ServerVar]
		public static void banid(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			string @string = arg.GetString(1, "unnamed");
			string string2 = arg.GetString(2, "no reason");
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(@uint);
			if (user != null && user.group == global::ServerUsers.UserGroup.Banned)
			{
				arg.ReplyWith("User " + @uint + " is already banned");
				return;
			}
			global::ServerUsers.Set(@uint, global::ServerUsers.UserGroup.Banned, @string, string2);
			arg.ReplyWith(string.Concat(new object[]
			{
				"Banned User: ",
				@uint,
				" - ",
				@string
			}));
		}

		// Token: 0x06001154 RID: 4436 RVA: 0x00067B58 File Offset: 0x00065D58
		[ServerVar]
		public static void unban(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			global::ServerUsers.User user = global::ServerUsers.Get(@uint);
			if (user == null || user.group != global::ServerUsers.UserGroup.Banned)
			{
				arg.ReplyWith("User " + @uint + " isn't banned");
				return;
			}
			global::ServerUsers.Remove(@uint);
			arg.ReplyWith("Unbanned User: " + @uint);
		}

		// Token: 0x06001155 RID: 4437 RVA: 0x00067BE8 File Offset: 0x00065DE8
		[ServerVar]
		public static void skipqueue(ConsoleSystem.Arg arg)
		{
			ulong @uint = arg.GetUInt64(0, 0UL);
			if (@uint < 70000000000000000UL)
			{
				arg.ReplyWith("This doesn't appear to be a 64bit steamid: " + @uint);
				return;
			}
			SingletonComponent<global::ServerMgr>.Instance.connectionQueue.SkipQueue(@uint);
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x00067C38 File Offset: 0x00065E38
		[ServerVar(Help = "Print out currently connected clients etc")]
		public static void players(ConsoleSystem.Arg arg)
		{
			TextTable textTable = new TextTable();
			textTable.AddColumn("id");
			textTable.AddColumn("name");
			textTable.AddColumn("ping");
			textTable.AddColumn("snap");
			textTable.AddColumn("updt");
			textTable.AddColumn("posi");
			textTable.AddColumn("dist");
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				string userIDString = basePlayer.UserIDString;
				string text = basePlayer.displayName.ToString();
				if (text.Length >= 14)
				{
					text = text.Substring(0, 14) + "..";
				}
				string text2 = text;
				string text3 = Net.sv.GetAveragePing(basePlayer.net.connection).ToString();
				string text4 = basePlayer.GetQueuedUpdateCount(global::BasePlayer.NetworkQueue.Update).ToString();
				string text5 = basePlayer.GetQueuedUpdateCount(global::BasePlayer.NetworkQueue.UpdateDistance).ToString();
				textTable.AddRow(new string[]
				{
					userIDString,
					text2,
					text3,
					string.Empty,
					text4,
					string.Empty,
					text5
				});
			}
			arg.ReplyWith(textTable.ToString());
		}

		// Token: 0x06001157 RID: 4439 RVA: 0x00067DB4 File Offset: 0x00065FB4
		[ServerVar(Help = "Sends a message in chat")]
		public static void say(ConsoleSystem.Arg arg)
		{
			Chat.Broadcast(arg.FullString, "SERVER", "#eee", 0UL);
		}

		// Token: 0x06001158 RID: 4440 RVA: 0x00067DD0 File Offset: 0x00065FD0
		[ServerVar(Help = "Show user info for players on server.")]
		public static void users(ConsoleSystem.Arg arg)
		{
			string text = "<slot:userid:\"name\">\n";
			int num = 0;
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				string text2 = text;
				text = string.Concat(new object[]
				{
					text2,
					basePlayer.userID,
					":\"",
					basePlayer.displayName,
					"\"\n"
				});
				num++;
			}
			text = text + num.ToString() + "users\n";
			arg.ReplyWith(text);
		}

		// Token: 0x06001159 RID: 4441 RVA: 0x00067E8C File Offset: 0x0006608C
		[ServerVar(Help = "List of banned users (sourceds compat)")]
		public static void banlist(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(global::ServerUsers.BanListString(false));
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x00067E9C File Offset: 0x0006609C
		[ServerVar(Help = "List of banned users - shows reasons and usernames")]
		public static void banlistex(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(global::ServerUsers.BanListStringEx());
		}

		// Token: 0x0600115B RID: 4443 RVA: 0x00067EAC File Offset: 0x000660AC
		[ServerVar(Help = "List of banned users, by ID (sourceds compat)")]
		public static void listid(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(global::ServerUsers.BanListString(true));
		}

		// Token: 0x0600115C RID: 4444 RVA: 0x00067EBC File Offset: 0x000660BC
		[ServerVar]
		public static void mutevoice(ConsoleSystem.Arg arg)
		{
			global::BasePlayer player = arg.GetPlayer(0);
			if (!player || player.net == null || player.net.connection == null)
			{
				arg.ReplyWith("Player not found");
				return;
			}
			player.SetPlayerFlag(global::BasePlayer.PlayerFlags.VoiceMuted, true);
		}

		// Token: 0x0600115D RID: 4445 RVA: 0x00067F10 File Offset: 0x00066110
		[ServerVar]
		public static void unmutevoice(ConsoleSystem.Arg arg)
		{
			global::BasePlayer player = arg.GetPlayer(0);
			if (!player || player.net == null || player.net.connection == null)
			{
				arg.ReplyWith("Player not found");
				return;
			}
			player.SetPlayerFlag(global::BasePlayer.PlayerFlags.VoiceMuted, false);
		}

		// Token: 0x0600115E RID: 4446 RVA: 0x00067F64 File Offset: 0x00066164
		[ServerVar]
		public static void mutechat(ConsoleSystem.Arg arg)
		{
			global::BasePlayer player = arg.GetPlayer(0);
			if (!player || player.net == null || player.net.connection == null)
			{
				arg.ReplyWith("Player not found");
				return;
			}
			player.SetPlayerFlag(global::BasePlayer.PlayerFlags.ChatMute, true);
		}

		// Token: 0x0600115F RID: 4447 RVA: 0x00067FB8 File Offset: 0x000661B8
		[ServerVar]
		public static void unmutechat(ConsoleSystem.Arg arg)
		{
			global::BasePlayer player = arg.GetPlayer(0);
			if (!player || player.net == null || player.net.connection == null)
			{
				arg.ReplyWith("Player not found");
				return;
			}
			player.SetPlayerFlag(global::BasePlayer.PlayerFlags.ChatMute, false);
		}

		// Token: 0x06001160 RID: 4448 RVA: 0x0006800C File Offset: 0x0006620C
		[ServerVar]
		public static void clientperf(ConsoleSystem.Arg arg)
		{
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				basePlayer.ClientRPCPlayer(null, basePlayer, "GetPerformanceReport");
			}
		}

		// Token: 0x06001161 RID: 4449 RVA: 0x00068070 File Offset: 0x00066270
		[ServerVar]
		public static void entid(ConsoleSystem.Arg arg)
		{
			global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find(arg.GetUInt(1, 0u)) as global::BaseEntity;
			if (baseEntity == null)
			{
				return;
			}
			if (baseEntity is global::BasePlayer)
			{
				return;
			}
			string @string = arg.GetString(0, string.Empty);
			if (arg.Player() != null)
			{
				Debug.Log(string.Concat(new object[]
				{
					"[ENTCMD] ",
					arg.Player().displayName,
					"/",
					arg.Player().userID,
					" used *",
					@string,
					"* on ent: ",
					baseEntity.name
				}));
			}
			if (@string != null)
			{
				if (@string == "kill")
				{
					baseEntity.Kill(global::BaseNetworkable.DestroyMode.Gib);
					return;
				}
				if (@string == "lock")
				{
					baseEntity.SetFlag(global::BaseEntity.Flags.Locked, true, false);
					return;
				}
				if (@string == "unlock")
				{
					baseEntity.SetFlag(global::BaseEntity.Flags.Locked, false, false);
					return;
				}
				if (@string == "debug")
				{
					baseEntity.SetFlag(global::BaseEntity.Flags.Debugging, true, false);
					return;
				}
				if (@string == "undebug")
				{
					baseEntity.SetFlag(global::BaseEntity.Flags.Debugging, false, false);
					return;
				}
				if (@string == "who")
				{
					arg.ReplyWith("Owner ID: " + baseEntity.OwnerID);
					return;
				}
			}
			arg.ReplyWith("Unknown command");
		}

		// Token: 0x06001162 RID: 4450 RVA: 0x000681F0 File Offset: 0x000663F0
		[ServerVar(Help = "Get a list of players")]
		public static Admin.PlayerInfo[] playerlist()
		{
			return (from x in global::BasePlayer.activePlayerList
			select new Admin.PlayerInfo
			{
				SteamID = x.userID.ToString(),
				OwnerSteamID = x.OwnerID.ToString(),
				DisplayName = x.displayName,
				Ping = Net.sv.GetAveragePing(x.net.connection),
				Address = x.net.connection.ipaddress,
				ConnectedSeconds = (int)x.net.connection.GetSecondsConnected(),
				VoiationLevel = x.violationLevel,
				Health = x.Health()
			}).ToArray<Admin.PlayerInfo>();
		}

		// Token: 0x06001163 RID: 4451 RVA: 0x00068220 File Offset: 0x00066420
		[ServerVar(Help = "List of banned users")]
		public static global::ServerUsers.User[] Bans()
		{
			return global::ServerUsers.GetAll(global::ServerUsers.UserGroup.Banned).ToArray<global::ServerUsers.User>();
		}

		// Token: 0x06001164 RID: 4452 RVA: 0x00068230 File Offset: 0x00066430
		[ServerVar(Help = "Get a list of information about the server")]
		public static Admin.ServerInfoOutput ServerInfo()
		{
			return new Admin.ServerInfoOutput
			{
				Hostname = Server.hostname,
				MaxPlayers = Server.maxplayers,
				Players = global::BasePlayer.activePlayerList.Count,
				Queued = SingletonComponent<global::ServerMgr>.Instance.connectionQueue.Queued,
				Joining = SingletonComponent<global::ServerMgr>.Instance.connectionQueue.Joining,
				EntityCount = global::BaseNetworkable.serverEntities.Count,
				GameTime = ((!(TOD_Sky.Instance != null)) ? DateTime.UtcNow.ToString() : TOD_Sky.Instance.Cycle.DateTime.ToString()),
				Uptime = (int)Time.realtimeSinceStartup,
				Map = Server.level,
				Framerate = (float)global::Performance.report.frameRate,
				Memory = (int)global::Performance.report.memoryAllocations,
				Collections = (int)global::Performance.report.memoryCollections,
				NetworkIn = ((Net.sv != null) ? ((int)Net.sv.GetStat(null, 3)) : 0),
				NetworkOut = ((Net.sv != null) ? ((int)Net.sv.GetStat(null, 1)) : 0),
				Restarting = SingletonComponent<global::ServerMgr>.Instance.Restarting,
				SaveCreatedTime = global::SaveRestore.SaveCreatedTime.ToString()
			};
		}

		// Token: 0x06001165 RID: 4453 RVA: 0x000683B8 File Offset: 0x000665B8
		[ServerVar(Help = "Get information about this build")]
		public static BuildInfo BuildInfo()
		{
			return Facepunch.BuildInfo.Current;
		}

		// Token: 0x02000299 RID: 665
		public struct PlayerInfo
		{
			// Token: 0x04000C36 RID: 3126
			public string SteamID;

			// Token: 0x04000C37 RID: 3127
			public string OwnerSteamID;

			// Token: 0x04000C38 RID: 3128
			public string DisplayName;

			// Token: 0x04000C39 RID: 3129
			public int Ping;

			// Token: 0x04000C3A RID: 3130
			public string Address;

			// Token: 0x04000C3B RID: 3131
			public int ConnectedSeconds;

			// Token: 0x04000C3C RID: 3132
			public float VoiationLevel;

			// Token: 0x04000C3D RID: 3133
			public float CurrentLevel;

			// Token: 0x04000C3E RID: 3134
			public float UnspentXp;

			// Token: 0x04000C3F RID: 3135
			public float Health;
		}

		// Token: 0x0200029A RID: 666
		public struct ServerInfoOutput
		{
			// Token: 0x04000C40 RID: 3136
			public string Hostname;

			// Token: 0x04000C41 RID: 3137
			public int MaxPlayers;

			// Token: 0x04000C42 RID: 3138
			public int Players;

			// Token: 0x04000C43 RID: 3139
			public int Queued;

			// Token: 0x04000C44 RID: 3140
			public int Joining;

			// Token: 0x04000C45 RID: 3141
			public int EntityCount;

			// Token: 0x04000C46 RID: 3142
			public string GameTime;

			// Token: 0x04000C47 RID: 3143
			public int Uptime;

			// Token: 0x04000C48 RID: 3144
			public string Map;

			// Token: 0x04000C49 RID: 3145
			public float Framerate;

			// Token: 0x04000C4A RID: 3146
			public int Memory;

			// Token: 0x04000C4B RID: 3147
			public int Collections;

			// Token: 0x04000C4C RID: 3148
			public int NetworkIn;

			// Token: 0x04000C4D RID: 3149
			public int NetworkOut;

			// Token: 0x04000C4E RID: 3150
			public bool Restarting;

			// Token: 0x04000C4F RID: 3151
			public string SaveCreatedTime;
		}
	}
}
