﻿using System;
using System.IO;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002AC RID: 684
	[ConsoleSystem.Factory("data")]
	public class Data : ConsoleSystem
	{
		// Token: 0x060011AB RID: 4523 RVA: 0x00069970 File Offset: 0x00067B70
		[ClientVar]
		[ServerVar]
		public static void export(ConsoleSystem.Arg args)
		{
			string @string = args.GetString(0, "none");
			string text = Path.Combine(Application.persistentDataPath, @string + ".raw");
			if (@string != null)
			{
				if (!(@string == "splatmap"))
				{
					if (!(@string == "heightmap"))
					{
						if (!(@string == "biomemap"))
						{
							if (!(@string == "topologymap"))
							{
								if (!(@string == "alphamap"))
								{
									if (!(@string == "watermap"))
									{
										goto IL_166;
									}
									if (global::TerrainMeta.WaterMap)
									{
										global::RawWriter.Write(global::TerrainMeta.WaterMap.ToEnumerable(), text);
									}
								}
								else if (global::TerrainMeta.AlphaMap)
								{
									global::RawWriter.Write(global::TerrainMeta.AlphaMap.ToEnumerable(), text);
								}
							}
							else if (global::TerrainMeta.TopologyMap)
							{
								global::RawWriter.Write(global::TerrainMeta.TopologyMap.ToEnumerable(), text);
							}
						}
						else if (global::TerrainMeta.BiomeMap)
						{
							global::RawWriter.Write(global::TerrainMeta.BiomeMap.ToEnumerable(), text);
						}
					}
					else if (global::TerrainMeta.HeightMap)
					{
						global::RawWriter.Write(global::TerrainMeta.HeightMap.ToEnumerable(), text);
					}
				}
				else if (global::TerrainMeta.SplatMap)
				{
					global::RawWriter.Write(global::TerrainMeta.SplatMap.ToEnumerable(), text);
				}
				args.ReplyWith("Export written to " + text);
				return;
			}
			IL_166:
			args.ReplyWith("Unknown export source: " + @string);
		}
	}
}
