﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002DF RID: 735
	[ConsoleSystem.Factory("supply")]
	public class Supply : ConsoleSystem
	{
		// Token: 0x060012B3 RID: 4787 RVA: 0x0006DBB8 File Offset: 0x0006BDB8
		[ServerVar]
		public static void drop(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			Debug.Log("Supply Drop Inbound");
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/npc/cargo plane/cargo_plane.prefab", default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				global::CargoPlane component = baseEntity.GetComponent<global::CargoPlane>();
				component.InitDropPosition(basePlayer.transform.position + new Vector3(0f, 10f, 0f));
				baseEntity.Spawn();
			}
		}

		// Token: 0x060012B4 RID: 4788 RVA: 0x0006DC4C File Offset: 0x0006BE4C
		[ServerVar]
		public static void call(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			Debug.Log("Supply Drop Inbound");
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/npc/cargo plane/cargo_plane.prefab", default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.Spawn();
			}
		}

		// Token: 0x04000D81 RID: 3457
		private const string path = "assets/prefabs/npc/cargo plane/cargo_plane.prefab";
	}
}
