﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002C6 RID: 710
	[ConsoleSystem.Factory("hierarchy")]
	public class Hierarchy : ConsoleSystem
	{
		// Token: 0x06001242 RID: 4674 RVA: 0x0006BAD8 File Offset: 0x00069CD8
		private static Transform[] GetCurrent()
		{
			if (Hierarchy.currentDir == null)
			{
				return global::TransformUtil.GetRootObjects().ToArray<Transform>();
			}
			List<Transform> list = new List<Transform>();
			for (int i = 0; i < Hierarchy.currentDir.transform.childCount; i++)
			{
				list.Add(Hierarchy.currentDir.transform.GetChild(i));
			}
			return list.ToArray();
		}

		// Token: 0x06001243 RID: 4675 RVA: 0x0006BB44 File Offset: 0x00069D44
		[ServerVar]
		public static void ls(ConsoleSystem.Arg args)
		{
			string text = string.Empty;
			string filter = args.GetString(0, string.Empty);
			if (Hierarchy.currentDir)
			{
				text = text + "Listing " + Hierarchy.currentDir.transform.GetRecursiveName(string.Empty) + "\n\n";
			}
			else
			{
				text += "Listing .\n\n";
			}
			foreach (Transform transform in (from x in Hierarchy.GetCurrent()
			where string.IsNullOrEmpty(filter) || x.name.Contains(filter)
			select x).Take(40))
			{
				text += string.Format("   {0} [{1}]\n", transform.name, transform.childCount);
			}
			text += "\n";
			args.ReplyWith(text);
		}

		// Token: 0x06001244 RID: 4676 RVA: 0x0006BC48 File Offset: 0x00069E48
		[ServerVar]
		public static void cd(ConsoleSystem.Arg args)
		{
			if (args.FullString == ".")
			{
				Hierarchy.currentDir = null;
				args.ReplyWith("Changed to .");
				return;
			}
			if (args.FullString == "..")
			{
				if (Hierarchy.currentDir)
				{
					Hierarchy.currentDir = ((!Hierarchy.currentDir.transform.parent) ? null : Hierarchy.currentDir.transform.parent.gameObject);
				}
				Hierarchy.currentDir = null;
				if (Hierarchy.currentDir)
				{
					args.ReplyWith("Changed to " + Hierarchy.currentDir.transform.GetRecursiveName(string.Empty));
				}
				else
				{
					args.ReplyWith("Changed to .");
				}
				return;
			}
			Transform transform = Hierarchy.GetCurrent().FirstOrDefault((Transform x) => x.name.ToLower() == args.FullString.ToLower());
			if (transform == null)
			{
				transform = Hierarchy.GetCurrent().FirstOrDefault((Transform x) => x.name.StartsWith(args.FullString, StringComparison.CurrentCultureIgnoreCase));
			}
			if (transform)
			{
				Hierarchy.currentDir = transform.gameObject;
				args.ReplyWith("Changed to " + Hierarchy.currentDir.transform.GetRecursiveName(string.Empty));
				return;
			}
			args.ReplyWith("Couldn't find \"" + args.FullString + "\"");
		}

		// Token: 0x06001245 RID: 4677 RVA: 0x0006BDE8 File Offset: 0x00069FE8
		[ServerVar]
		public static void del(ConsoleSystem.Arg args)
		{
			if (!args.HasArgs(1))
			{
				return;
			}
			IEnumerable<Transform> enumerable = from x in Hierarchy.GetCurrent()
			where x.name.ToLower() == args.FullString.ToLower()
			select x;
			if (enumerable.Count<Transform>() == 0)
			{
				enumerable = from x in Hierarchy.GetCurrent()
				where x.name.StartsWith(args.FullString, StringComparison.CurrentCultureIgnoreCase)
				select x;
			}
			if (enumerable.Count<Transform>() == 0)
			{
				args.ReplyWith("Couldn't find  " + args.FullString);
				return;
			}
			foreach (Transform transform in enumerable)
			{
				global::BaseEntity baseEntity = transform.gameObject.ToBaseEntity();
				if (baseEntity.IsValid())
				{
					if (baseEntity.isServer)
					{
						baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
					}
				}
				else
				{
					global::GameManager.Destroy(transform.gameObject, 0f);
				}
			}
			args.ReplyWith("Deleted " + enumerable.Count<Transform>() + " objects");
		}

		// Token: 0x04000D1E RID: 3358
		private static GameObject currentDir;
	}
}
