﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002E1 RID: 737
	[ConsoleSystem.Factory("time")]
	public class Time : ConsoleSystem
	{
		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060012B8 RID: 4792 RVA: 0x0006DCD0 File Offset: 0x0006BED0
		// (set) Token: 0x060012B9 RID: 4793 RVA: 0x0006DCD8 File Offset: 0x0006BED8
		[Help("Fixed delta time in seconds")]
		[ServerVar]
		public static float fixeddelta
		{
			get
			{
				return Time.fixedDeltaTime;
			}
			set
			{
				Time.fixedDeltaTime = value;
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060012BA RID: 4794 RVA: 0x0006DCE0 File Offset: 0x0006BEE0
		// (set) Token: 0x060012BB RID: 4795 RVA: 0x0006DCE8 File Offset: 0x0006BEE8
		[ServerVar]
		[Help("The minimum amount of times to tick per frame")]
		public static float maxdelta
		{
			get
			{
				return Time.maximumDeltaTime;
			}
			set
			{
				Time.maximumDeltaTime = value;
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060012BC RID: 4796 RVA: 0x0006DCF0 File Offset: 0x0006BEF0
		// (set) Token: 0x060012BD RID: 4797 RVA: 0x0006DCF8 File Offset: 0x0006BEF8
		[Help("The time scale")]
		[ServerVar]
		public static float timescale
		{
			get
			{
				return Time.timeScale;
			}
			set
			{
				Time.timeScale = value;
			}
		}

		// Token: 0x04000D84 RID: 3460
		[ServerVar]
		[Help("Pause time while loading")]
		public static bool pausewhileloading = true;
	}
}
