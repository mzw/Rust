﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002E9 RID: 745
	[ConsoleSystem.Factory("xmas")]
	public class XMas : ConsoleSystem
	{
		// Token: 0x060012D1 RID: 4817 RVA: 0x0006E2C8 File Offset: 0x0006C4C8
		[ServerVar]
		public static void refill(ConsoleSystem.Arg arg)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/misc/xmas/xmasrefill.prefab", default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.Spawn();
			}
		}

		// Token: 0x04000D96 RID: 3478
		private const string path = "assets/prefabs/misc/xmas/xmasrefill.prefab";

		// Token: 0x04000D97 RID: 3479
		[ServerVar]
		public static bool enabled;

		// Token: 0x04000D98 RID: 3480
		[ServerVar]
		public static float spawnRange = 40f;

		// Token: 0x04000D99 RID: 3481
		[ServerVar]
		public static int spawnAttempts = 5;

		// Token: 0x04000D9A RID: 3482
		[ServerVar]
		public static int giftsPerPlayer = 2;
	}
}
