﻿using System;
using Facepunch;

namespace ConVar
{
	// Token: 0x020002CD RID: 717
	public class Manifest
	{
		// Token: 0x0600125B RID: 4699 RVA: 0x0006C89C File Offset: 0x0006AA9C
		[ServerVar]
		[ClientVar]
		public static object PrintManifest()
		{
			return Application.Manifest;
		}

		// Token: 0x0600125C RID: 4700 RVA: 0x0006C8A4 File Offset: 0x0006AAA4
		[ServerVar]
		[ClientVar]
		public static object PrintManifestRaw()
		{
			return Manifest.Contents;
		}
	}
}
