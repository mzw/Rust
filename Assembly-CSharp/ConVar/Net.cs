﻿using System;

namespace ConVar
{
	// Token: 0x020002D0 RID: 720
	[ConsoleSystem.Factory("net")]
	public class Net : ConsoleSystem
	{
		// Token: 0x04000D25 RID: 3365
		[ServerVar]
		public static bool visdebug;

		// Token: 0x04000D26 RID: 3366
		[ClientVar]
		public static bool debug;
	}
}
