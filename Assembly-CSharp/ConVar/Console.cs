﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Facepunch;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002A7 RID: 679
	[ConsoleSystem.Factory("console")]
	public class Console : ConsoleSystem
	{
		// Token: 0x0600119E RID: 4510 RVA: 0x000696D4 File Offset: 0x000678D4
		[ServerVar]
		[Help("Return the last x lines of the console. Default is 200")]
		public static IEnumerable<Facepunch.Output.Entry> tail(ConsoleSystem.Arg arg)
		{
			int @int = arg.GetInt(0, 200);
			int num = Facepunch.Output.HistoryOutput.Count - @int;
			if (num < 0)
			{
				num = 0;
			}
			return Facepunch.Output.HistoryOutput.Skip(num);
		}

		// Token: 0x0600119F RID: 4511 RVA: 0x00069710 File Offset: 0x00067910
		[ServerVar]
		[Help("Search the console for a particular string")]
		public static IEnumerable<Facepunch.Output.Entry> search(ConsoleSystem.Arg arg)
		{
			string search = arg.GetString(0, null);
			if (search == null)
			{
				return Enumerable.Empty<Facepunch.Output.Entry>();
			}
			return from x in Facepunch.Output.HistoryOutput
			where x.Message.Length < 4096 && StringEx.Contains(x.Message, search, CompareOptions.IgnoreCase)
			select x;
		}
	}
}
