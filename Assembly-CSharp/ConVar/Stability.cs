﻿using System;
using System.Linq;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002DD RID: 733
	[ConsoleSystem.Factory("stability")]
	public class Stability : ConsoleSystem
	{
		// Token: 0x060012AF RID: 4783 RVA: 0x0006DB1C File Offset: 0x0006BD1C
		[ServerVar]
		public static void refresh_stability(ConsoleSystem.Arg args)
		{
			global::StabilityEntity[] array = global::BaseNetworkable.serverEntities.OfType<global::StabilityEntity>().ToArray<global::StabilityEntity>();
			Debug.Log("Refreshing stability on " + array.Length + " entities...");
			for (int i = 0; i < array.Length; i++)
			{
				array[i].UpdateStability();
			}
		}

		// Token: 0x04000D7B RID: 3451
		[ServerVar]
		public static int verbose;

		// Token: 0x04000D7C RID: 3452
		[ServerVar]
		public static int strikes = 10;

		// Token: 0x04000D7D RID: 3453
		[ServerVar]
		public static float collapse = 0.05f;

		// Token: 0x04000D7E RID: 3454
		[ServerVar]
		public static float accuracy = 0.001f;

		// Token: 0x04000D7F RID: 3455
		[ServerVar]
		public static float stabilityqueue = 9f;

		// Token: 0x04000D80 RID: 3456
		[ServerVar]
		public static float surroundingsqueue = 3f;
	}
}
