﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002C0 RID: 704
	[ConsoleSystem.Factory("gc")]
	public class GC : ConsoleSystem
	{
		// Token: 0x060011F4 RID: 4596 RVA: 0x0006AB24 File Offset: 0x00068D24
		[ServerVar]
		[ClientVar]
		public static void collect()
		{
			GC.Collect();
		}

		// Token: 0x060011F5 RID: 4597 RVA: 0x0006AB2C File Offset: 0x00068D2C
		[ServerVar]
		[ClientVar]
		public static void unload()
		{
			Resources.UnloadUnusedAssets();
		}
	}
}
