﻿using System;

namespace ConVar
{
	// Token: 0x020002BD RID: 701
	[ConsoleSystem.Factory("env")]
	public class Env : ConsoleSystem
	{
		// Token: 0x1700013A RID: 314
		// (get) Token: 0x060011E2 RID: 4578 RVA: 0x0006A914 File Offset: 0x00068B14
		// (set) Token: 0x060011E1 RID: 4577 RVA: 0x0006A8EC File Offset: 0x00068AEC
		[ServerVar]
		public static bool progresstime
		{
			get
			{
				return !(TOD_Sky.Instance == null) && TOD_Sky.Instance.Components.Time.ProgressTime;
			}
			set
			{
				if (TOD_Sky.Instance == null)
				{
					return;
				}
				TOD_Sky.Instance.Components.Time.ProgressTime = value;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x060011E4 RID: 4580 RVA: 0x0006A960 File Offset: 0x00068B60
		// (set) Token: 0x060011E3 RID: 4579 RVA: 0x0006A93C File Offset: 0x00068B3C
		[ServerVar]
		public static float time
		{
			get
			{
				if (TOD_Sky.Instance == null)
				{
					return 0f;
				}
				return TOD_Sky.Instance.Cycle.Hour;
			}
			set
			{
				if (TOD_Sky.Instance == null)
				{
					return;
				}
				TOD_Sky.Instance.Cycle.Hour = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x060011E6 RID: 4582 RVA: 0x0006A9AC File Offset: 0x00068BAC
		// (set) Token: 0x060011E5 RID: 4581 RVA: 0x0006A988 File Offset: 0x00068B88
		[ServerVar]
		public static int day
		{
			get
			{
				if (TOD_Sky.Instance == null)
				{
					return 0;
				}
				return TOD_Sky.Instance.Cycle.Day;
			}
			set
			{
				if (TOD_Sky.Instance == null)
				{
					return;
				}
				TOD_Sky.Instance.Cycle.Day = value;
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x060011E8 RID: 4584 RVA: 0x0006A9F4 File Offset: 0x00068BF4
		// (set) Token: 0x060011E7 RID: 4583 RVA: 0x0006A9D0 File Offset: 0x00068BD0
		[ServerVar]
		public static int month
		{
			get
			{
				if (TOD_Sky.Instance == null)
				{
					return 0;
				}
				return TOD_Sky.Instance.Cycle.Month;
			}
			set
			{
				if (TOD_Sky.Instance == null)
				{
					return;
				}
				TOD_Sky.Instance.Cycle.Month = value;
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x060011EA RID: 4586 RVA: 0x0006AA3C File Offset: 0x00068C3C
		// (set) Token: 0x060011E9 RID: 4585 RVA: 0x0006AA18 File Offset: 0x00068C18
		[ServerVar]
		public static int year
		{
			get
			{
				if (TOD_Sky.Instance == null)
				{
					return 0;
				}
				return TOD_Sky.Instance.Cycle.Year;
			}
			set
			{
				if (TOD_Sky.Instance == null)
				{
					return;
				}
				TOD_Sky.Instance.Cycle.Year = value;
			}
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x0006AA60 File Offset: 0x00068C60
		[ServerVar]
		public static void addtime(ConsoleSystem.Arg arg)
		{
			if (TOD_Sky.Instance == null)
			{
				return;
			}
			DateTime dateTime = TOD_Sky.Instance.Cycle.DateTime.Add(arg.GetTimeSpan(0));
			TOD_Sky.Instance.Cycle.DateTime = dateTime;
		}
	}
}
