﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002D4 RID: 724
	[ConsoleSystem.Factory("physics")]
	public class Physics : ConsoleSystem
	{
		// Token: 0x17000150 RID: 336
		// (get) Token: 0x06001267 RID: 4711 RVA: 0x0006C9EC File Offset: 0x0006ABEC
		// (set) Token: 0x06001268 RID: 4712 RVA: 0x0006C9F4 File Offset: 0x0006ABF4
		[ServerVar]
		public static float bouncethreshold
		{
			get
			{
				return Physics.bounceThreshold;
			}
			set
			{
				Physics.bounceThreshold = value;
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x06001269 RID: 4713 RVA: 0x0006C9FC File Offset: 0x0006ABFC
		// (set) Token: 0x0600126A RID: 4714 RVA: 0x0006CA04 File Offset: 0x0006AC04
		[ServerVar]
		public static float sleepthreshold
		{
			get
			{
				return Physics.sleepThreshold;
			}
			set
			{
				Physics.sleepThreshold = value;
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x0600126B RID: 4715 RVA: 0x0006CA0C File Offset: 0x0006AC0C
		// (set) Token: 0x0600126C RID: 4716 RVA: 0x0006CA14 File Offset: 0x0006AC14
		[ServerVar(Help = "The default solver iteration count permitted for any rigid bodies (default 7). Must be positive")]
		public static int solveriterationcount
		{
			get
			{
				return Physics.defaultSolverIterations;
			}
			set
			{
				Physics.defaultSolverIterations = value;
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x0600126D RID: 4717 RVA: 0x0006CA1C File Offset: 0x0006AC1C
		// (set) Token: 0x0600126E RID: 4718 RVA: 0x0006CA3C File Offset: 0x0006AC3C
		[ServerVar(Help = "Gravity multiplier")]
		public static float gravity
		{
			get
			{
				return Physics.gravity.y / -9.81f;
			}
			set
			{
				Physics.gravity = new Vector3(0f, value * -9.81f, 0f);
			}
		}

		// Token: 0x0600126F RID: 4719 RVA: 0x0006CA5C File Offset: 0x0006AC5C
		internal static void ApplyDropped(Rigidbody rigidBody, global::BaseEntity entity)
		{
			if (Physics.droppedmode == "good")
			{
				rigidBody.collisionDetectionMode = 2;
			}
			if (Physics.droppedmode == "tempgood")
			{
				rigidBody.collisionDetectionMode = 2;
				entity.Invoke(new Action(entity.SwitchToFastPhysics), 10f);
			}
			if (Physics.droppedmode == "fast")
			{
				rigidBody.collisionDetectionMode = 0;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06001270 RID: 4720 RVA: 0x0006CAD4 File Offset: 0x0006ACD4
		// (set) Token: 0x06001271 RID: 4721 RVA: 0x0006CAE4 File Offset: 0x0006ACE4
		[ClientVar]
		[ServerVar(Help = "The amount of physics steps per second")]
		public static float steps
		{
			get
			{
				return 1f / Time.fixedDeltaTime;
			}
			set
			{
				if (value < 10f)
				{
					value = 10f;
				}
				if (value > 60f)
				{
					value = 60f;
				}
				Time.fixedDeltaTime = 1f / value;
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x06001272 RID: 4722 RVA: 0x0006CB18 File Offset: 0x0006AD18
		// (set) Token: 0x06001273 RID: 4723 RVA: 0x0006CB28 File Offset: 0x0006AD28
		[ServerVar(Help = "The slowest physics steps will operate")]
		[ClientVar]
		public static float minsteps
		{
			get
			{
				return 1f / Time.maximumDeltaTime;
			}
			set
			{
				if (value < 1f)
				{
					value = 1f;
				}
				if (value > 60f)
				{
					value = 60f;
				}
				Time.maximumDeltaTime = 1f / value;
			}
		}

		// Token: 0x04000D27 RID: 3367
		private const float baseGravity = -9.81f;

		// Token: 0x04000D28 RID: 3368
		[ServerVar(Help = "The physics mode that dropped items and corpses should use. good, tempgood or fast. fast + tempgood might cause objects to fall through other objects.")]
		public static string droppedmode = "good";

		// Token: 0x04000D29 RID: 3369
		[ServerVar(Help = "Send effects to clients when physics objects collide")]
		public static bool sendeffects = true;
	}
}
