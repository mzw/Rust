﻿using System;

namespace ConVar
{
	// Token: 0x020002E0 RID: 736
	[ConsoleSystem.Factory("terrain")]
	public class Terrain : ConsoleSystem
	{
		// Token: 0x04000D82 RID: 3458
		[ClientVar(Saved = true)]
		public static float quality = 100f;

		// Token: 0x04000D83 RID: 3459
		[ClientVar(Saved = true)]
		public static bool basetexcomp = true;
	}
}
