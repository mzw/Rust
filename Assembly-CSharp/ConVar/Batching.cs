﻿using System;
using System.Text;
using UnityEngine;

namespace ConVar
{
	// Token: 0x0200029F RID: 671
	[ConsoleSystem.Factory("batching")]
	public class Batching : ConsoleSystem
	{
		// Token: 0x0600117C RID: 4476 RVA: 0x00068E54 File Offset: 0x00067054
		[ServerVar]
		public static void refresh_colliders(ConsoleSystem.Arg args)
		{
			foreach (global::ColliderBatch colliderBatch in Object.FindObjectsOfType<global::ColliderBatch>())
			{
				colliderBatch.Refresh();
			}
			if (SingletonComponent<global::ColliderGrid>.Instance)
			{
				SingletonComponent<global::ColliderGrid>.Instance.Refresh();
			}
		}

		// Token: 0x0600117D RID: 4477 RVA: 0x00068EA0 File Offset: 0x000670A0
		[ServerVar]
		public static void print_colliders(ConsoleSystem.Arg args)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (SingletonComponent<global::ColliderGrid>.Instance)
			{
				stringBuilder.AppendFormat("Mesh Collider Batching: {0:N0}/{0:N0}", SingletonComponent<global::ColliderGrid>.Instance.BatchedMeshCount(), SingletonComponent<global::ColliderGrid>.Instance.MeshCount());
				stringBuilder.AppendLine();
			}
			args.ReplyWith(stringBuilder.ToString());
		}

		// Token: 0x04000CAA RID: 3242
		[ServerVar]
		[ClientVar]
		public static bool colliders = true;

		// Token: 0x04000CAB RID: 3243
		[ServerVar]
		[ClientVar]
		public static bool collider_threading = true;

		// Token: 0x04000CAC RID: 3244
		[ClientVar]
		[ServerVar]
		public static int collider_capacity = 30000;

		// Token: 0x04000CAD RID: 3245
		[ClientVar]
		[ServerVar]
		public static int collider_vertices = 1000;

		// Token: 0x04000CAE RID: 3246
		[ServerVar]
		[ClientVar]
		public static int collider_submeshes = 1;

		// Token: 0x04000CAF RID: 3247
		[ClientVar]
		public static bool renderers = true;

		// Token: 0x04000CB0 RID: 3248
		[ClientVar]
		public static bool renderer_threading = true;

		// Token: 0x04000CB1 RID: 3249
		[ClientVar]
		public static int renderer_capacity = 30000;

		// Token: 0x04000CB2 RID: 3250
		[ClientVar]
		public static int renderer_vertices = 1000;

		// Token: 0x04000CB3 RID: 3251
		[ClientVar]
		public static int renderer_submeshes = 1;

		// Token: 0x04000CB4 RID: 3252
		[ServerVar]
		[ClientVar]
		public static int verbose;
	}
}
