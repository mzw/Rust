﻿using System;

namespace ConVar
{
	// Token: 0x020002D9 RID: 729
	[ConsoleSystem.Factory("sentry")]
	public class Sentry : ConsoleSystem
	{
		// Token: 0x04000D2D RID: 3373
		[ServerVar(Help = "target everyone regardless of authorization")]
		public static bool targetall;

		// Token: 0x04000D2E RID: 3374
		[ServerVar(Help = "how long until something is considered hostile after it attacked")]
		public static float hostileduration = 120f;
	}
}
