﻿using System;
using Rust.Workshop;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002C2 RID: 706
	[ConsoleSystem.Factory("graphics")]
	public class Graphics : ConsoleSystem
	{
		// Token: 0x17000143 RID: 323
		// (get) Token: 0x0600121C RID: 4636 RVA: 0x0006B53C File Offset: 0x0006973C
		// (set) Token: 0x0600121D RID: 4637 RVA: 0x0006B544 File Offset: 0x00069744
		[ClientVar(Help = "The currently selected quality level")]
		public static int quality
		{
			get
			{
				return QualitySettings.GetQualityLevel();
			}
			set
			{
				int shadowcascades = Graphics.shadowcascades;
				QualitySettings.SetQualityLevel(value, true);
				Graphics.shadowcascades = shadowcascades;
			}
		}

		// Token: 0x0600121E RID: 4638 RVA: 0x0006B564 File Offset: 0x00069764
		public static float EnforceShadowDistanceBounds(float distance)
		{
			if (QualitySettings.shadowCascades == 1)
			{
				distance = Mathf.Clamp(distance, 40f, 40f);
			}
			else if (QualitySettings.shadowCascades == 2)
			{
				distance = Mathf.Clamp(distance, 40f, 180f);
			}
			else
			{
				distance = Mathf.Clamp(distance, 40f, 800f);
			}
			return distance;
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x0600121F RID: 4639 RVA: 0x0006B5C8 File Offset: 0x000697C8
		// (set) Token: 0x06001220 RID: 4640 RVA: 0x0006B5D0 File Offset: 0x000697D0
		[ClientVar(Saved = true)]
		public static float shadowdistance
		{
			get
			{
				return Graphics._shadowdistance;
			}
			set
			{
				Graphics._shadowdistance = value;
				QualitySettings.shadowDistance = Graphics.EnforceShadowDistanceBounds(Graphics._shadowdistance);
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06001221 RID: 4641 RVA: 0x0006B5E8 File Offset: 0x000697E8
		// (set) Token: 0x06001222 RID: 4642 RVA: 0x0006B5F0 File Offset: 0x000697F0
		[ClientVar(Saved = true)]
		public static int shadowcascades
		{
			get
			{
				return QualitySettings.shadowCascades;
			}
			set
			{
				QualitySettings.shadowCascades = value;
				QualitySettings.shadowDistance = Graphics.EnforceShadowDistanceBounds(Graphics.shadowdistance);
			}
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06001223 RID: 4643 RVA: 0x0006B608 File Offset: 0x00069808
		// (set) Token: 0x06001224 RID: 4644 RVA: 0x0006B610 File Offset: 0x00069810
		[ClientVar(Saved = true)]
		public static int shadowquality
		{
			get
			{
				return Graphics._shadowquality;
			}
			set
			{
				Graphics._shadowquality = Mathf.Clamp(value, 0, 2);
				Graphics.shadowmode = Graphics._shadowquality + 1;
				if (Graphics._shadowquality < 2)
				{
					Shader.DisableKeyword("SHADOW_QUALITY_HIGH");
				}
				else
				{
					Shader.EnableKeyword("SHADOW_QUALITY_HIGH");
				}
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x06001225 RID: 4645 RVA: 0x0006B650 File Offset: 0x00069850
		// (set) Token: 0x06001226 RID: 4646 RVA: 0x0006B658 File Offset: 0x00069858
		[ClientVar(Saved = true)]
		public static float fov
		{
			get
			{
				return Graphics._fov;
			}
			set
			{
				Graphics._fov = Mathf.Clamp(value, 60f, 90f);
			}
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x06001227 RID: 4647 RVA: 0x0006B670 File Offset: 0x00069870
		// (set) Token: 0x06001228 RID: 4648 RVA: 0x0006B678 File Offset: 0x00069878
		[ClientVar]
		public static float lodbias
		{
			get
			{
				return QualitySettings.lodBias;
			}
			set
			{
				QualitySettings.lodBias = Mathf.Clamp(value, 0.25f, 5f);
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x06001229 RID: 4649 RVA: 0x0006B690 File Offset: 0x00069890
		// (set) Token: 0x0600122A RID: 4650 RVA: 0x0006B698 File Offset: 0x00069898
		[ClientVar(Saved = true)]
		public static int shaderlod
		{
			get
			{
				return Shader.globalMaximumLOD;
			}
			set
			{
				Shader.globalMaximumLOD = Mathf.Clamp(value, 100, 600);
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x0600122B RID: 4651 RVA: 0x0006B6AC File Offset: 0x000698AC
		// (set) Token: 0x0600122C RID: 4652 RVA: 0x0006B6B4 File Offset: 0x000698B4
		[ClientVar(Saved = true)]
		public static float uiscale
		{
			get
			{
				return Graphics._uiscale;
			}
			set
			{
				Graphics._uiscale = Mathf.Clamp(value, 0.5f, 1f);
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x0600122D RID: 4653 RVA: 0x0006B6CC File Offset: 0x000698CC
		// (set) Token: 0x0600122E RID: 4654 RVA: 0x0006B6D4 File Offset: 0x000698D4
		[ClientVar(Saved = true)]
		public static int af
		{
			get
			{
				return Graphics._anisotropic;
			}
			set
			{
				value = Mathf.Clamp(value, 1, 16);
				Texture.SetGlobalAnisotropicFilteringLimits(1, value);
				if (value <= 1)
				{
					Texture.anisotropicFiltering = 0;
				}
				if (value > 1)
				{
					Texture.anisotropicFiltering = 1;
				}
				Graphics._anisotropic = value;
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x0600122F RID: 4655 RVA: 0x0006B708 File Offset: 0x00069908
		// (set) Token: 0x06001230 RID: 4656 RVA: 0x0006B710 File Offset: 0x00069910
		[ClientVar(Saved = true)]
		public static int parallax
		{
			get
			{
				return Graphics._parallax;
			}
			set
			{
				if (value != 1)
				{
					if (value != 2)
					{
						Shader.DisableKeyword("TERRAIN_PARALLAX_OFFSET");
						Shader.DisableKeyword("TERRAIN_PARALLAX_OCCLUSION");
					}
					else
					{
						Shader.DisableKeyword("TERRAIN_PARALLAX_OFFSET");
						Shader.EnableKeyword("TERRAIN_PARALLAX_OCCLUSION");
					}
				}
				else
				{
					Shader.EnableKeyword("TERRAIN_PARALLAX_OFFSET");
					Shader.DisableKeyword("TERRAIN_PARALLAX_OCCLUSION");
				}
				Graphics._parallax = value;
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x06001231 RID: 4657 RVA: 0x0006B784 File Offset: 0x00069984
		// (set) Token: 0x06001232 RID: 4658 RVA: 0x0006B78C File Offset: 0x0006998C
		[ClientVar]
		public static bool itemskins
		{
			get
			{
				return Rust.Workshop.WorkshopSkin.AllowApply;
			}
			set
			{
				Rust.Workshop.WorkshopSkin.AllowApply = value;
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x06001233 RID: 4659 RVA: 0x0006B794 File Offset: 0x00069994
		// (set) Token: 0x06001234 RID: 4660 RVA: 0x0006B79C File Offset: 0x0006999C
		[ClientVar]
		public static bool itemskincache
		{
			get
			{
				return Skin.Cache;
			}
			set
			{
				Skin.Cache = value;
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x06001235 RID: 4661 RVA: 0x0006B7A4 File Offset: 0x000699A4
		// (set) Token: 0x06001236 RID: 4662 RVA: 0x0006B7AC File Offset: 0x000699AC
		[ClientVar]
		public static float itemskintimeout
		{
			get
			{
				return Rust.Workshop.WorkshopSkin.DownloadTimeout;
			}
			set
			{
				Rust.Workshop.WorkshopSkin.DownloadTimeout = value;
			}
		}

		// Token: 0x04000D03 RID: 3331
		private const float MinShadowDistance = 40f;

		// Token: 0x04000D04 RID: 3332
		private const float MaxShadowDistance2Split = 180f;

		// Token: 0x04000D05 RID: 3333
		private const float MaxShadowDistance4Split = 800f;

		// Token: 0x04000D06 RID: 3334
		private static float _shadowdistance = 800f;

		// Token: 0x04000D07 RID: 3335
		[ClientVar(Saved = true)]
		public static int shadowmode = 2;

		// Token: 0x04000D08 RID: 3336
		[ClientVar(Saved = true)]
		public static int shadowlights = 1;

		// Token: 0x04000D09 RID: 3337
		private static int _shadowquality = 1;

		// Token: 0x04000D0A RID: 3338
		[ClientVar(Saved = true)]
		public static bool grassshadows;

		// Token: 0x04000D0B RID: 3339
		[ClientVar(Saved = true)]
		public static bool contactshadows;

		// Token: 0x04000D0C RID: 3340
		[ClientVar(Saved = true)]
		public static float drawdistance = 2500f;

		// Token: 0x04000D0D RID: 3341
		private static float _fov = 75f;

		// Token: 0x04000D0E RID: 3342
		[ClientVar]
		public static bool hud = true;

		// Token: 0x04000D0F RID: 3343
		[ClientVar(Saved = true)]
		public static bool chat = true;

		// Token: 0x04000D10 RID: 3344
		[ClientVar(Saved = true)]
		public static bool branding = true;

		// Token: 0x04000D11 RID: 3345
		[ClientVar(Saved = true)]
		public static bool dof;

		// Token: 0x04000D12 RID: 3346
		[ClientVar(Saved = true)]
		public static float dof_aper = 12f;

		// Token: 0x04000D13 RID: 3347
		[ClientVar(Saved = true)]
		public static float dof_blur = 1f;

		// Token: 0x04000D14 RID: 3348
		private static float _uiscale = 1f;

		// Token: 0x04000D15 RID: 3349
		private static int _anisotropic = 1;

		// Token: 0x04000D16 RID: 3350
		private static int _parallax;
	}
}
