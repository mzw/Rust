﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002E6 RID: 742
	[ConsoleSystem.Factory("weather")]
	public class Weather : ConsoleSystem
	{
		// Token: 0x060012C7 RID: 4807 RVA: 0x0006DD64 File Offset: 0x0006BF64
		[ServerVar]
		public static void clouds(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::Climate>.Instance == null)
			{
				return;
			}
			float clouds = SingletonComponent<global::Climate>.Instance.Overrides.Clouds;
			float @float = args.GetFloat(0, -1f);
			string text = (clouds >= 0f) ? (Mathf.RoundToInt(100f * clouds) + "%") : "automatic";
			string text2 = (@float >= 0f) ? (Mathf.RoundToInt(100f * @float) + "%") : "automatic";
			args.ReplyWith(string.Concat(new string[]
			{
				"Clouds: ",
				text2,
				" (was ",
				text,
				")"
			}));
			SingletonComponent<global::Climate>.Instance.Overrides.Clouds = @float;
		}

		// Token: 0x060012C8 RID: 4808 RVA: 0x0006DE44 File Offset: 0x0006C044
		[ServerVar]
		public static void fog(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::Climate>.Instance == null)
			{
				return;
			}
			float fog = SingletonComponent<global::Climate>.Instance.Overrides.Fog;
			float @float = args.GetFloat(0, -1f);
			string text = (fog >= 0f) ? (Mathf.RoundToInt(100f * fog) + "%") : "automatic";
			string text2 = (@float >= 0f) ? (Mathf.RoundToInt(100f * @float) + "%") : "automatic";
			args.ReplyWith(string.Concat(new string[]
			{
				"Fog: ",
				text2,
				" (was ",
				text,
				")"
			}));
			SingletonComponent<global::Climate>.Instance.Overrides.Fog = @float;
		}

		// Token: 0x060012C9 RID: 4809 RVA: 0x0006DF24 File Offset: 0x0006C124
		[ServerVar]
		public static void wind(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::Climate>.Instance == null)
			{
				return;
			}
			float wind = SingletonComponent<global::Climate>.Instance.Overrides.Wind;
			float @float = args.GetFloat(0, -1f);
			string text = (wind >= 0f) ? (Mathf.RoundToInt(100f * wind) + "%") : "automatic";
			string text2 = (@float >= 0f) ? (Mathf.RoundToInt(100f * @float) + "%") : "automatic";
			args.ReplyWith(string.Concat(new string[]
			{
				"Wind: ",
				text2,
				" (was ",
				text,
				")"
			}));
			SingletonComponent<global::Climate>.Instance.Overrides.Wind = @float;
		}

		// Token: 0x060012CA RID: 4810 RVA: 0x0006E004 File Offset: 0x0006C204
		[ServerVar]
		public static void rain(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::Climate>.Instance == null)
			{
				return;
			}
			float rain = SingletonComponent<global::Climate>.Instance.Overrides.Rain;
			float @float = args.GetFloat(0, -1f);
			string text = (rain >= 0f) ? (Mathf.RoundToInt(100f * rain) + "%") : "automatic";
			string text2 = (@float >= 0f) ? (Mathf.RoundToInt(100f * @float) + "%") : "automatic";
			args.ReplyWith(string.Concat(new string[]
			{
				"Rain: ",
				text2,
				" (was ",
				text,
				")"
			}));
			SingletonComponent<global::Climate>.Instance.Overrides.Rain = @float;
		}
	}
}
