﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002B3 RID: 691
	[ConsoleSystem.Factory("entity")]
	public class Entity : ConsoleSystem
	{
		// Token: 0x060011BE RID: 4542 RVA: 0x00069E14 File Offset: 0x00068014
		private static TextTable GetEntityTable(Func<Entity.EntityInfo, bool> filter)
		{
			TextTable textTable = new TextTable();
			textTable.AddColumn("realm");
			textTable.AddColumn("entity");
			textTable.AddColumn("group");
			textTable.AddColumn("parent");
			textTable.AddColumn("name");
			textTable.AddColumn("position");
			textTable.AddColumn("rotation");
			textTable.AddColumn("invokes");
			foreach (global::BaseNetworkable baseNetworkable in global::BaseNetworkable.serverEntities)
			{
				if (!(baseNetworkable == null))
				{
					Entity.EntityInfo arg = new Entity.EntityInfo(baseNetworkable);
					if (filter(arg))
					{
						textTable.AddRow(new string[]
						{
							"sv",
							arg.entityID.ToString(),
							arg.groupID.ToString(),
							arg.parentID.ToString(),
							arg.entity.ShortPrefabName,
							arg.entity.transform.position.ToString(),
							arg.entity.transform.rotation.eulerAngles.ToString(),
							arg.entity.InvokeString()
						});
					}
				}
			}
			return textTable;
		}

		// Token: 0x060011BF RID: 4543 RVA: 0x00069FBC File Offset: 0x000681BC
		[ClientVar]
		[ServerVar]
		public static void find_entity(ConsoleSystem.Arg args)
		{
			string filter = args.GetString(0, string.Empty);
			TextTable entityTable = Entity.GetEntityTable((Entity.EntityInfo info) => string.IsNullOrEmpty(filter) || info.entity.PrefabName.Contains(filter));
			args.ReplyWith(entityTable.ToString());
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x0006A000 File Offset: 0x00068200
		[ServerVar]
		[ClientVar]
		public static void find_id(ConsoleSystem.Arg args)
		{
			uint filter = args.GetUInt(0, 0u);
			TextTable entityTable = Entity.GetEntityTable((Entity.EntityInfo info) => info.entityID == filter);
			args.ReplyWith(entityTable.ToString());
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x0006A040 File Offset: 0x00068240
		[ClientVar]
		[ServerVar]
		public static void find_group(ConsoleSystem.Arg args)
		{
			uint filter = args.GetUInt(0, 0u);
			TextTable entityTable = Entity.GetEntityTable((Entity.EntityInfo info) => info.groupID == filter);
			args.ReplyWith(entityTable.ToString());
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x0006A080 File Offset: 0x00068280
		[ServerVar]
		[ClientVar]
		public static void find_parent(ConsoleSystem.Arg args)
		{
			uint filter = args.GetUInt(0, 0u);
			TextTable entityTable = Entity.GetEntityTable((Entity.EntityInfo info) => info.parentID == filter);
			args.ReplyWith(entityTable.ToString());
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x0006A0C0 File Offset: 0x000682C0
		[ClientVar]
		[ServerVar]
		public static void find_radius(ConsoleSystem.Arg args)
		{
			global::BasePlayer player = args.Player();
			if (player == null)
			{
				return;
			}
			uint filter = args.GetUInt(0, 10u);
			TextTable entityTable = Entity.GetEntityTable((Entity.EntityInfo info) => Vector3.Distance(info.entity.transform.position, player.transform.position) <= filter);
			args.ReplyWith(entityTable.ToString());
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x0006A120 File Offset: 0x00068320
		[ClientVar]
		[ServerVar]
		public static void find_self(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (basePlayer == null)
			{
				return;
			}
			if (basePlayer.net == null)
			{
				return;
			}
			uint filter = basePlayer.net.ID;
			TextTable entityTable = Entity.GetEntityTable((Entity.EntityInfo info) => info.entityID == filter);
			args.ReplyWith(entityTable.ToString());
		}

		// Token: 0x060011C5 RID: 4549 RVA: 0x0006A184 File Offset: 0x00068384
		[ServerVar]
		public static void debug_toggle(ConsoleSystem.Arg args)
		{
			int @int = args.GetInt(0, 0);
			if (@int == 0)
			{
				return;
			}
			global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find((uint)@int) as global::BaseEntity;
			if (baseEntity == null)
			{
				return;
			}
			baseEntity.SetFlag(global::BaseEntity.Flags.Debugging, !baseEntity.IsDebugging(), false);
			if (baseEntity.IsDebugging())
			{
				baseEntity.OnDebugStart();
			}
			args.ReplyWith(string.Concat(new object[]
			{
				"Debugging for ",
				baseEntity.net.ID,
				" ",
				(!baseEntity.IsDebugging()) ? "disabled" : "enabled"
			}));
		}

		// Token: 0x060011C6 RID: 4550 RVA: 0x0006A234 File Offset: 0x00068434
		[ServerVar]
		public static void nudge(int entID)
		{
			if (entID == 0)
			{
				return;
			}
			global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find((uint)entID) as global::BaseEntity;
			if (baseEntity == null)
			{
				return;
			}
			baseEntity.BroadcastMessage("DebugNudge", 1);
		}

		// Token: 0x060011C7 RID: 4551 RVA: 0x0006A274 File Offset: 0x00068474
		[ServerVar(Name = "spawn")]
		public static string svspawn(string name, Vector3 pos)
		{
			if (string.IsNullOrEmpty(name))
			{
				return "No entity name provided";
			}
			string[] array = (from x in global::GameManifest.Current.entities
			where StringEx.Contains(Path.GetFileNameWithoutExtension(x), name, CompareOptions.IgnoreCase)
			select x.ToLower()).ToArray<string>();
			if (array.Length == 0)
			{
				return "Entity type not found";
			}
			if (array.Length > 1)
			{
				string text = array.FirstOrDefault((string x) => string.Compare(Path.GetFileNameWithoutExtension(x), name, StringComparison.OrdinalIgnoreCase) == 0);
				if (text == null)
				{
					string str = "Unknown entity - could be:\n\n";
					string separator = "\n";
					IEnumerable<string> source = array;
					if (Entity.<>f__mg$cache0 == null)
					{
						Entity.<>f__mg$cache0 = new Func<string, string>(Path.GetFileNameWithoutExtension);
					}
					return str + string.Join(separator, source.Select(Entity.<>f__mg$cache0).ToArray<string>());
				}
				array[0] = text;
			}
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(array[0], pos, default(Quaternion), true);
			if (baseEntity == null)
			{
				return "Couldn't spawn " + name;
			}
			baseEntity.Spawn();
			return string.Concat(new object[]
			{
				"spawned ",
				baseEntity,
				" at ",
				pos
			});
		}

		// Token: 0x060011C8 RID: 4552 RVA: 0x0006A3C4 File Offset: 0x000685C4
		[ServerVar(Name = "spawnitem")]
		public static string svspawnitem(string name, Vector3 pos)
		{
			if (string.IsNullOrEmpty(name))
			{
				return "No entity name provided";
			}
			string[] array = (from x in global::ItemManager.itemList
			select x.shortname into x
			where StringEx.Contains(x, name, CompareOptions.IgnoreCase)
			select x).ToArray<string>();
			if (array.Length == 0)
			{
				return "Entity type not found";
			}
			if (array.Length > 1)
			{
				string text = array.FirstOrDefault((string x) => string.Compare(x, name, StringComparison.OrdinalIgnoreCase) == 0);
				if (text == null)
				{
					return "Unknown entity - could be:\n\n" + string.Join("\n", array);
				}
				array[0] = text;
			}
			global::Item item = global::ItemManager.CreateByName(array[0], 1, 0UL);
			if (item == null)
			{
				return "Couldn't spawn " + name;
			}
			item.CreateWorldObject(pos, default(Quaternion));
			return string.Concat(new object[]
			{
				"spawned ",
				item,
				" at ",
				pos
			});
		}

		// Token: 0x060011C9 RID: 4553 RVA: 0x0006A4E0 File Offset: 0x000686E0
		[ServerVar]
		public static void spawnlootfrom(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			string @string = args.GetString(0, string.Empty);
			int @int = args.GetInt(1, 1);
			Vector3 vector = args.GetVector3(1, (!basePlayer) ? Vector3.zero : basePlayer.CenterPoint());
			if (string.IsNullOrEmpty(@string))
			{
				return;
			}
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(@string, vector, default(Quaternion), true);
			if (baseEntity == null)
			{
				return;
			}
			baseEntity.Spawn();
			basePlayer.ChatMessage(string.Concat(new object[]
			{
				"Contents of ",
				@string,
				" spawned ",
				@int,
				" times"
			}));
			global::LootContainer component = baseEntity.GetComponent<global::LootContainer>();
			if (component != null)
			{
				for (int i = 0; i < @int * component.maxDefinitionsToSpawn; i++)
				{
					component.lootDefinition.SpawnIntoContainer(basePlayer.inventory.containerMain);
				}
			}
			baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
		}

		// Token: 0x060011CA RID: 4554 RVA: 0x0006A5F0 File Offset: 0x000687F0
		[ServerVar(Help = "Destroy all entities created by this user")]
		public static int DeleteBy(ulong SteamId)
		{
			if (SteamId == 0UL)
			{
				return 0;
			}
			int num = 0;
			foreach (global::BaseNetworkable baseNetworkable in global::BaseNetworkable.serverEntities)
			{
				global::BaseEntity baseEntity = (global::BaseEntity)baseNetworkable;
				if (!(baseEntity == null))
				{
					if (baseEntity.OwnerID == SteamId)
					{
						baseEntity.Invoke(new Action(baseEntity.KillMessage), (float)num * 0.2f);
						num++;
					}
				}
			}
			return num;
		}

		// Token: 0x04000CE8 RID: 3304
		[CompilerGenerated]
		private static Func<string, string> <>f__mg$cache0;

		// Token: 0x020002B4 RID: 692
		private struct EntityInfo
		{
			// Token: 0x060011CD RID: 4557 RVA: 0x0006A6A4 File Offset: 0x000688A4
			public EntityInfo(global::BaseNetworkable src)
			{
				this.entity = src;
				global::BaseEntity baseEntity = this.entity as global::BaseEntity;
				global::BaseEntity baseEntity2 = (!(baseEntity != null)) ? null : baseEntity.GetParentEntity();
				this.entityID = ((!(this.entity != null) || this.entity.net == null) ? 0u : this.entity.net.ID);
				this.parentID = ((!(baseEntity2 != null) || baseEntity2.net == null) ? 0u : baseEntity2.net.ID);
				this.groupID = ((!(this.entity != null) || this.entity.net == null || this.entity.net.group == null) ? 0u : this.entity.net.group.ID);
			}

			// Token: 0x04000CEB RID: 3307
			public global::BaseNetworkable entity;

			// Token: 0x04000CEC RID: 3308
			public uint entityID;

			// Token: 0x04000CED RID: 3309
			public uint parentID;

			// Token: 0x04000CEE RID: 3310
			public uint groupID;
		}
	}
}
