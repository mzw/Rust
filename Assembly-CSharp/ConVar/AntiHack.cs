﻿using System;

namespace ConVar
{
	// Token: 0x0200029D RID: 669
	[ConsoleSystem.Factory("antihack")]
	public class AntiHack : ConsoleSystem
	{
		// Token: 0x04000C6F RID: 3183
		[ServerVar]
		[Help("report violations to the anti cheat backend")]
		public static bool reporting = true;

		// Token: 0x04000C70 RID: 3184
		[ServerVar]
		[Help("are admins allowed to use their admin cheat")]
		public static bool admincheat = true;

		// Token: 0x04000C71 RID: 3185
		[ServerVar]
		[Help("use antihack to verify object placement by players")]
		public static bool objectplacement = true;

		// Token: 0x04000C72 RID: 3186
		[Help("use antihack to verify model state sent by players")]
		[ServerVar]
		public static bool modelstate = true;

		// Token: 0x04000C73 RID: 3187
		[ServerVar]
		[Help("0 == users, 1 == admins, 2 == developers")]
		public static int userlevel = 2;

		// Token: 0x04000C74 RID: 3188
		[ServerVar]
		[Help("0 == no enforcement, 1 == kick, 2 == ban (DISABLED)")]
		public static int enforcementlevel = 1;

		// Token: 0x04000C75 RID: 3189
		[ServerVar]
		[Help("max allowed client desync, lower value = more false positives")]
		public static float maxdesync = 1f;

		// Token: 0x04000C76 RID: 3190
		[ServerVar]
		[Help("max allowed client tick interval delta time, lower value = more false positives")]
		public static float maxdeltatime = 1f;

		// Token: 0x04000C77 RID: 3191
		[Help("the rate at which violation values go back down")]
		[ServerVar]
		public static float relaxationrate = 0.1f;

		// Token: 0x04000C78 RID: 3192
		[ServerVar]
		[Help("the time before violation values go back down")]
		public static float relaxationpause = 10f;

		// Token: 0x04000C79 RID: 3193
		[ServerVar]
		[Help("violation value above this results in enforcement")]
		public static float maxviolation = 100f;

		// Token: 0x04000C7A RID: 3194
		[ServerVar]
		[Help("0 == disabled, 1 == ray, 2 == sphere, 3 == curve")]
		public static int noclip_protection = 3;

		// Token: 0x04000C7B RID: 3195
		[Help("whether or not to reject movement when noclip is detected")]
		[ServerVar]
		public static bool noclip_reject = true;

		// Token: 0x04000C7C RID: 3196
		[Help("violation penalty to hand out when noclip is detected")]
		[ServerVar]
		public static float noclip_penalty;

		// Token: 0x04000C7D RID: 3197
		[ServerVar]
		[Help("collider margin when checking for noclipping")]
		public static float noclip_margin = 0.09f;

		// Token: 0x04000C7E RID: 3198
		[ServerVar]
		[Help("collider backtracking when checking for noclipping")]
		public static float noclip_backtracking = 0.01f;

		// Token: 0x04000C7F RID: 3199
		[ServerVar]
		[Help("movement curve step size, lower value = less false positives")]
		public static float noclip_stepsize = 0.1f;

		// Token: 0x04000C80 RID: 3200
		[ServerVar]
		[Help("movement curve max steps, lower value = more false positives")]
		public static int noclip_maxsteps = 15;

		// Token: 0x04000C81 RID: 3201
		[Help("0 == disabled, 1 == enabled")]
		[ServerVar]
		public static int speedhack_protection = 1;

		// Token: 0x04000C82 RID: 3202
		[ServerVar]
		[Help("whether or not to reject movement when speedhack is detected")]
		public static bool speedhack_reject = true;

		// Token: 0x04000C83 RID: 3203
		[ServerVar]
		[Help("violation penalty to hand out when speedhack is detected")]
		public static float speedhack_penalty;

		// Token: 0x04000C84 RID: 3204
		[ServerVar]
		[Help("speed threshold to assume speedhacking, lower value = more false positives")]
		public static float speedhack_forgiveness = 0.5f;

		// Token: 0x04000C85 RID: 3205
		[ServerVar]
		[Help("time interval to calculate speed in, lower value = more false positives")]
		public static float speedhack_deltatime = 0.1f;

		// Token: 0x04000C86 RID: 3206
		[Help("speed forgiveness when moving down slopes, lower value = more false positives")]
		[ServerVar]
		public static float speedhack_slopespeed = 10f;

		// Token: 0x04000C87 RID: 3207
		[ServerVar]
		[Help("required number of speeding tickets to trigger a violation")]
		public static int speedhack_tickets = 15;

		// Token: 0x04000C88 RID: 3208
		[ServerVar]
		[Help("speeding ticket history length")]
		public static int speedhack_history = 20;

		// Token: 0x04000C89 RID: 3209
		[ServerVar]
		[Help("0 == disabled, 1 == client, 2 == capsule, 3 == curve")]
		public static int flyhack_protection = 3;

		// Token: 0x04000C8A RID: 3210
		[ServerVar]
		[Help("whether or not to reject movement when flyhack is detected")]
		public static bool flyhack_reject;

		// Token: 0x04000C8B RID: 3211
		[Help("violation penalty to hand out when flyhack is detected")]
		[ServerVar]
		public static float flyhack_penalty = 100f;

		// Token: 0x04000C8C RID: 3212
		[ServerVar]
		[Help("distance threshold to assume flyhacking, lower value = more false positives")]
		public static float flyhack_forgiveness_vertical = 1.5f;

		// Token: 0x04000C8D RID: 3213
		[ServerVar]
		[Help("distance threshold to assume flyhacking, lower value = more false positives")]
		public static float flyhack_forgiveness_horizontal = 1.5f;

		// Token: 0x04000C8E RID: 3214
		[ServerVar]
		[Help("collider downwards extrusion when checking for flyhacking")]
		public static float flyhack_extrusion = 2f;

		// Token: 0x04000C8F RID: 3215
		[ServerVar]
		[Help("collider margin when checking for flyhacking")]
		public static float flyhack_margin = 0.05f;

		// Token: 0x04000C90 RID: 3216
		[ServerVar]
		[Help("movement curve step size, lower value = less false positives")]
		public static float flyhack_stepsize = 0.1f;

		// Token: 0x04000C91 RID: 3217
		[ServerVar]
		[Help("movement curve max steps, lower value = more false positives")]
		public static int flyhack_maxsteps = 15;

		// Token: 0x04000C92 RID: 3218
		[ServerVar]
		[Help("0 == disabled, 1 == speed, 2 == speed + entity, 3 == speed + entity + LOS")]
		public static int projectile_protection = 3;

		// Token: 0x04000C93 RID: 3219
		[ServerVar]
		[Help("violation penalty to hand out when projectile hack is detected")]
		public static float projectile_penalty;

		// Token: 0x04000C94 RID: 3220
		[Help("projectile speed forgiveness in percent, lower value = more false positives")]
		[ServerVar]
		public static float projectile_forgiveness = 0.5f;

		// Token: 0x04000C95 RID: 3221
		[Help("projectile server frames to include in delay, lower value = more false positives")]
		[ServerVar]
		public static float projectile_serverframes = 2f;

		// Token: 0x04000C96 RID: 3222
		[ServerVar]
		[Help("projectile client frames to include in delay, lower value = more false positives")]
		public static float projectile_clientframes = 2f;

		// Token: 0x04000C97 RID: 3223
		[ServerVar]
		[Help("0 == disabled, 1 == initiator, 2 == initiator + target, 3 == initiator + target + LOS")]
		public static int melee_protection = 3;

		// Token: 0x04000C98 RID: 3224
		[ServerVar]
		[Help("violation penalty to hand out when melee hack is detected")]
		public static float melee_penalty;

		// Token: 0x04000C99 RID: 3225
		[ServerVar]
		[Help("melee distance forgiveness in percent, lower value = more false positives")]
		public static float melee_forgiveness = 0.5f;

		// Token: 0x04000C9A RID: 3226
		[ServerVar]
		[Help("melee server frames to include in delay, lower value = more false positives")]
		public static float melee_serverframes = 2f;

		// Token: 0x04000C9B RID: 3227
		[ServerVar]
		[Help("melee client frames to include in delay, lower value = more false positives")]
		public static float melee_clientframes = 2f;

		// Token: 0x04000C9C RID: 3228
		[ServerVar]
		[Help("0 == disabled, 1 == distance, 2 == distance + LOS")]
		public static int eye_protection = 2;

		// Token: 0x04000C9D RID: 3229
		[ServerVar]
		[Help("violation penalty to hand out when eye hack is detected")]
		public static float eye_penalty;

		// Token: 0x04000C9E RID: 3230
		[Help("eye speed forgiveness in percent, lower value = more false positives")]
		[ServerVar]
		public static float eye_forgiveness = 0.5f;

		// Token: 0x04000C9F RID: 3231
		[ServerVar]
		[Help("eye server frames to include in delay, lower value = more false positives")]
		public static float eye_serverframes = 2f;

		// Token: 0x04000CA0 RID: 3232
		[Help("eye client frames to include in delay, lower value = more false positives")]
		[ServerVar]
		public static float eye_clientframes = 2f;

		// Token: 0x04000CA1 RID: 3233
		[ServerVar]
		[Help("0 == silent, 1 == print max violation, 2 == print nonzero violation, 3 == print any violation")]
		public static int debuglevel = 1;
	}
}
