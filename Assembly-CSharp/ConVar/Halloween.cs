﻿using System;

namespace ConVar
{
	// Token: 0x020002C4 RID: 708
	[ConsoleSystem.Factory("halloween")]
	public class Halloween : ConsoleSystem
	{
		// Token: 0x04000D17 RID: 3351
		[ServerVar]
		public static bool enabled;

		// Token: 0x04000D18 RID: 3352
		[ServerVar(Help = "Population active on the server, per square km")]
		public static float murdererpopulation;
	}
}
