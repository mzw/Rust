﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Facepunch;
using Facepunch.Extend;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002D6 RID: 726
	[ConsoleSystem.Factory("pool")]
	public class Pool : ConsoleSystem
	{
		// Token: 0x06001277 RID: 4727 RVA: 0x0006CB80 File Offset: 0x0006AD80
		[ServerVar]
		[ClientVar]
		public static void print_memory(ConsoleSystem.Arg arg)
		{
			if (Pool.directory.Count == 0)
			{
				arg.ReplyWith("Memory pool is empty.");
				return;
			}
			TextTable textTable = new TextTable();
			textTable.AddColumn("type");
			textTable.AddColumn("pooled");
			textTable.AddColumn("active");
			textTable.AddColumn("hits");
			textTable.AddColumn("misses");
			textTable.AddColumn("spills");
			foreach (KeyValuePair<Type, object> keyValuePair in from x in Pool.directory
			orderby (x.Value as Pool.ICollection).ItemsCreated descending
			select x)
			{
				string text = keyValuePair.Key.ToString().Replace("System.Collections.Generic.", string.Empty);
				Pool.ICollection collection = keyValuePair.Value as Pool.ICollection;
				textTable.AddRow(new string[]
				{
					text,
					NumberExtensions.FormatNumberShort(collection.ItemsInStack),
					NumberExtensions.FormatNumberShort(collection.ItemsInUse),
					NumberExtensions.FormatNumberShort(collection.ItemsTaken),
					NumberExtensions.FormatNumberShort(collection.ItemsCreated),
					NumberExtensions.FormatNumberShort(collection.ItemsSpilled)
				});
			}
			arg.ReplyWith(textTable.ToString());
		}

		// Token: 0x06001278 RID: 4728 RVA: 0x0006CCE8 File Offset: 0x0006AEE8
		[ServerVar]
		[ClientVar]
		public static void print_prefabs(ConsoleSystem.Arg arg)
		{
			global::PrefabPoolCollection pool = global::GameManager.server.pool;
			if (pool.storage.Count == 0)
			{
				arg.ReplyWith("Prefab pool is empty.");
				return;
			}
			string @string = arg.GetString(0, string.Empty);
			TextTable textTable = new TextTable();
			textTable.AddColumn("id");
			textTable.AddColumn("name");
			textTable.AddColumn("count");
			foreach (KeyValuePair<uint, global::PrefabPool> keyValuePair in pool.storage)
			{
				string text = keyValuePair.Key.ToString();
				string text2 = global::StringPool.Get(keyValuePair.Key);
				string text3 = keyValuePair.Value.Count.ToString();
				if (string.IsNullOrEmpty(@string) || StringEx.Contains(text2, @string, CompareOptions.IgnoreCase))
				{
					textTable.AddRow(new string[]
					{
						text,
						Path.GetFileNameWithoutExtension(text2),
						text3
					});
				}
			}
			arg.ReplyWith(textTable.ToString());
		}

		// Token: 0x06001279 RID: 4729 RVA: 0x0006CE24 File Offset: 0x0006B024
		[ServerVar]
		[ClientVar]
		public static void print_assets(ConsoleSystem.Arg arg)
		{
			if (AssetPool.storage.Count == 0)
			{
				arg.ReplyWith("Asset pool is empty.");
				return;
			}
			string @string = arg.GetString(0, string.Empty);
			TextTable textTable = new TextTable();
			textTable.AddColumn("type");
			textTable.AddColumn("allocated");
			textTable.AddColumn("available");
			foreach (KeyValuePair<Type, AssetPool.Pool> keyValuePair in AssetPool.storage)
			{
				string text = keyValuePair.Key.ToString();
				string text2 = keyValuePair.Value.allocated.ToString();
				string text3 = keyValuePair.Value.available.ToString();
				if (string.IsNullOrEmpty(@string) || StringEx.Contains(text, @string, CompareOptions.IgnoreCase))
				{
					textTable.AddRow(new string[]
					{
						text,
						text2,
						text3
					});
				}
			}
			arg.ReplyWith(textTable.ToString());
		}

		// Token: 0x0600127A RID: 4730 RVA: 0x0006CF48 File Offset: 0x0006B148
		[ServerVar]
		[ClientVar]
		public static void clear_memory(ConsoleSystem.Arg arg)
		{
			Pool.Clear();
		}

		// Token: 0x0600127B RID: 4731 RVA: 0x0006CF50 File Offset: 0x0006B150
		[ServerVar]
		[ClientVar]
		public static void clear_prefabs(ConsoleSystem.Arg arg)
		{
			global::GameManager.server.pool.Clear();
		}

		// Token: 0x0600127C RID: 4732 RVA: 0x0006CF64 File Offset: 0x0006B164
		[ServerVar]
		[ClientVar]
		public static void clear_assets(ConsoleSystem.Arg arg)
		{
			AssetPool.Clear();
		}

		// Token: 0x04000D2A RID: 3370
		[ServerVar]
		[ClientVar]
		public static int mode = 2;

		// Token: 0x04000D2B RID: 3371
		[ServerVar]
		public static bool collider_batches;
	}
}
