﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002AA RID: 682
	[ConsoleSystem.Factory("craft")]
	public class Craft : ConsoleSystem
	{
		// Token: 0x060011A5 RID: 4517 RVA: 0x000697AC File Offset: 0x000679AC
		[ServerUserVar]
		public static void add(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			int @int = args.GetInt(0, 0);
			int int2 = args.GetInt(1, 1);
			int num = (int)args.GetUInt64(2, 0UL);
			if (int2 < 1)
			{
				return;
			}
			global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(@int);
			if (itemDefinition == null)
			{
				args.ReplyWith("Item not found");
				return;
			}
			global::ItemBlueprint itemBlueprint = global::ItemManager.FindBlueprint(itemDefinition);
			if (!itemBlueprint)
			{
				args.ReplyWith("Blueprint not found");
				return;
			}
			if (!itemBlueprint.userCraftable)
			{
				args.ReplyWith("Item is not craftable");
				return;
			}
			if (!basePlayer.blueprints.CanCraft(@int, num))
			{
				num = 0;
				if (!basePlayer.blueprints.CanCraft(@int, num))
				{
					args.ReplyWith("You can't craft this item");
					return;
				}
				args.ReplyWith("You don't have permission to use this skin, so crafting unskinned");
			}
			if (!basePlayer.inventory.crafting.CraftItem(itemBlueprint, basePlayer, null, int2, num, null))
			{
				args.ReplyWith("Couldn't craft!");
				return;
			}
		}

		// Token: 0x060011A6 RID: 4518 RVA: 0x000698BC File Offset: 0x00067ABC
		[ServerUserVar]
		public static void canceltask(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			int @int = args.GetInt(0, 0);
			if (!basePlayer.inventory.crafting.CancelTask(@int, true))
			{
				args.ReplyWith("Couldn't cancel task!");
				return;
			}
		}

		// Token: 0x060011A7 RID: 4519 RVA: 0x00069914 File Offset: 0x00067B14
		[ServerUserVar]
		public static void cancel(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			int @int = args.GetInt(0, 0);
			basePlayer.inventory.crafting.CancelBlueprint(@int);
		}

		// Token: 0x04000CC8 RID: 3272
		[ServerVar]
		public static bool instant;
	}
}
