﻿using System;

namespace ConVar
{
	// Token: 0x020002DB RID: 731
	[ConsoleSystem.Factory("spawn")]
	public class Spawn : ConsoleSystem
	{
		// Token: 0x060012A7 RID: 4775 RVA: 0x0006D90C File Offset: 0x0006BB0C
		[ServerVar]
		public static void fill_populations(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::SpawnHandler>.Instance)
			{
				SingletonComponent<global::SpawnHandler>.Instance.FillPopulations();
			}
		}

		// Token: 0x060012A8 RID: 4776 RVA: 0x0006D928 File Offset: 0x0006BB28
		[ServerVar]
		public static void fill_groups(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::SpawnHandler>.Instance)
			{
				SingletonComponent<global::SpawnHandler>.Instance.FillGroups();
			}
		}

		// Token: 0x060012A9 RID: 4777 RVA: 0x0006D944 File Offset: 0x0006BB44
		[ServerVar]
		public static void report(ConsoleSystem.Arg args)
		{
			if (SingletonComponent<global::SpawnHandler>.Instance)
			{
				args.ReplyWith(SingletonComponent<global::SpawnHandler>.Instance.GetReport(false));
			}
			else
			{
				args.ReplyWith("No spawn handler found.");
			}
		}

		// Token: 0x060012AA RID: 4778 RVA: 0x0006D978 File Offset: 0x0006BB78
		[ServerVar]
		public static void scalars(ConsoleSystem.Arg args)
		{
			TextTable textTable = new TextTable();
			textTable.AddColumn("Type");
			textTable.AddColumn("Value");
			textTable.AddRow(new string[]
			{
				"Player Fraction",
				global::SpawnHandler.PlayerFraction().ToString()
			});
			textTable.AddRow(new string[]
			{
				"Player Excess",
				global::SpawnHandler.PlayerExcess().ToString()
			});
			textTable.AddRow(new string[]
			{
				"Population Rate",
				global::SpawnHandler.PlayerLerp(Spawn.min_rate, Spawn.max_rate).ToString()
			});
			textTable.AddRow(new string[]
			{
				"Population Density",
				global::SpawnHandler.PlayerLerp(Spawn.min_density, Spawn.max_density).ToString()
			});
			textTable.AddRow(new string[]
			{
				"Group Rate",
				global::SpawnHandler.PlayerScale(Spawn.player_scale).ToString()
			});
			args.ReplyWith(textTable.ToString());
		}

		// Token: 0x04000D6F RID: 3439
		[ServerVar]
		public static float min_rate = 0.5f;

		// Token: 0x04000D70 RID: 3440
		[ServerVar]
		public static float max_rate = 1f;

		// Token: 0x04000D71 RID: 3441
		[ServerVar]
		public static float min_density = 0.5f;

		// Token: 0x04000D72 RID: 3442
		[ServerVar]
		public static float max_density = 1f;

		// Token: 0x04000D73 RID: 3443
		[ServerVar]
		public static float player_base = 100f;

		// Token: 0x04000D74 RID: 3444
		[ServerVar]
		public static float player_scale = 2f;

		// Token: 0x04000D75 RID: 3445
		[ServerVar]
		public static bool respawn_populations = true;

		// Token: 0x04000D76 RID: 3446
		[ServerVar]
		public static bool respawn_groups = true;
	}
}
