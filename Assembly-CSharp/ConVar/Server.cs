﻿using System;
using System.Diagnostics;
using System.IO;
using Facepunch.Network.Raknet;
using Network;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002DA RID: 730
	[ConsoleSystem.Factory("server")]
	public class Server : ConsoleSystem
	{
		// Token: 0x06001286 RID: 4742 RVA: 0x0006CFBC File Offset: 0x0006B1BC
		public static float TickDelta()
		{
			return 1f / (float)Server.tickrate;
		}

		// Token: 0x06001287 RID: 4743 RVA: 0x0006CFCC File Offset: 0x0006B1CC
		public static float TickTime(uint tick)
		{
			return (float)((double)Server.TickDelta() * tick);
		}

		// Token: 0x06001288 RID: 4744 RVA: 0x0006CFDC File Offset: 0x0006B1DC
		[ServerVar(Help = "Show holstered items on player bodies")]
		public static void setshowholstereditems(ConsoleSystem.Arg arg)
		{
			Server.showHolsteredItems = arg.GetBool(0, Server.showHolsteredItems);
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				basePlayer.inventory.UpdatedVisibleHolsteredItems();
			}
			foreach (global::BasePlayer basePlayer2 in global::BasePlayer.sleepingPlayerList)
			{
				basePlayer2.inventory.UpdatedVisibleHolsteredItems();
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x06001289 RID: 4745 RVA: 0x0006D09C File Offset: 0x0006B29C
		// (set) Token: 0x0600128A RID: 4746 RVA: 0x0006D0A4 File Offset: 0x0006B2A4
		[ServerVar]
		public static float maxreceivetime
		{
			get
			{
				return Server.MaxReceiveTime;
			}
			set
			{
				Server.MaxReceiveTime = Mathf.Clamp(value, 10f, 10000f);
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x0600128B RID: 4747 RVA: 0x0006D0BC File Offset: 0x0006B2BC
		// (set) Token: 0x0600128C RID: 4748 RVA: 0x0006D0C4 File Offset: 0x0006B2C4
		[ServerVar]
		public static int maxpacketspersecond
		{
			get
			{
				return (int)Server.MaxPacketsPerSecond;
			}
			set
			{
				Server.MaxPacketsPerSecond = (ulong)((long)Mathf.Clamp(value, 1, 1000000));
			}
		}

		// Token: 0x0600128D RID: 4749 RVA: 0x0006D0D8 File Offset: 0x0006B2D8
		[ServerVar(Help = "Starts a server")]
		public static void start(ConsoleSystem.Arg arg)
		{
			if (Net.sv.IsConnected())
			{
				arg.ReplyWith("There is already a server running!");
				return;
			}
			string @string = arg.GetString(0, Server.level);
			if (!global::LevelManager.IsValid(@string))
			{
				arg.ReplyWith("Level '" + @string + "' isn't valid!");
				return;
			}
			if (Object.FindObjectOfType<global::ServerMgr>())
			{
				arg.ReplyWith("There is already a server running!");
				return;
			}
			Object.DontDestroyOnLoad(global::GameManager.server.CreatePrefab("assets/bundled/prefabs/system/server.prefab", true));
			global::LevelManager.LoadLevel(@string, true);
		}

		// Token: 0x0600128E RID: 4750 RVA: 0x0006D168 File Offset: 0x0006B368
		[ServerVar(Help = "Stops a server")]
		public static void stop(ConsoleSystem.Arg arg)
		{
			if (!Net.sv.IsConnected())
			{
				arg.ReplyWith("There isn't a server running!");
				return;
			}
			Net.sv.Stop(arg.GetString(0, "Stopping Server"));
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x0600128F RID: 4751 RVA: 0x0006D19C File Offset: 0x0006B39C
		public static string rootFolder
		{
			get
			{
				return "server/" + Server.identity;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x06001290 RID: 4752 RVA: 0x0006D1B0 File Offset: 0x0006B3B0
		public static string backupFolder
		{
			get
			{
				return "backup/0/" + Server.identity;
			}
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x06001291 RID: 4753 RVA: 0x0006D1C4 File Offset: 0x0006B3C4
		public static string backupFolder1
		{
			get
			{
				return "backup/1/" + Server.identity;
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x06001292 RID: 4754 RVA: 0x0006D1D8 File Offset: 0x0006B3D8
		public static string backupFolder2
		{
			get
			{
				return "backup/2/" + Server.identity;
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x06001293 RID: 4755 RVA: 0x0006D1EC File Offset: 0x0006B3EC
		public static string backupFolder3
		{
			get
			{
				return "backup/3/" + Server.identity;
			}
		}

		// Token: 0x06001294 RID: 4756 RVA: 0x0006D200 File Offset: 0x0006B400
		[ServerVar(Help = "Backup server folder")]
		public static void backup()
		{
			global::DirectoryEx.Backup(new string[]
			{
				Server.backupFolder,
				Server.backupFolder1,
				Server.backupFolder2,
				Server.backupFolder3
			});
			global::DirectoryEx.CopyAll(Server.rootFolder, Server.backupFolder);
		}

		// Token: 0x06001295 RID: 4757 RVA: 0x0006D23C File Offset: 0x0006B43C
		public static string GetServerFolder(string folder)
		{
			string text = Server.rootFolder + "/" + folder;
			if (Directory.Exists(text))
			{
				return text;
			}
			Directory.CreateDirectory(text);
			return text;
		}

		// Token: 0x06001296 RID: 4758 RVA: 0x0006D270 File Offset: 0x0006B470
		[ServerVar(Help = "Writes config files")]
		public static void writecfg(ConsoleSystem.Arg arg)
		{
			string contents = ConsoleSystem.SaveToConfigString(true);
			string serverFolder = Server.GetServerFolder("cfg");
			File.WriteAllText(serverFolder + "/serverauto.cfg", contents);
			global::ServerUsers.Save();
			arg.ReplyWith("Config Saved");
		}

		// Token: 0x06001297 RID: 4759 RVA: 0x0006D2B0 File Offset: 0x0006B4B0
		[ServerVar]
		public static void fps(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(global::Performance.report.frameRate.ToString() + " FPS");
		}

		// Token: 0x06001298 RID: 4760 RVA: 0x0006D2D8 File Offset: 0x0006B4D8
		[ServerVar(Help = "Force save the current game")]
		public static void save(ConsoleSystem.Arg arg)
		{
			Stopwatch stopwatch = Stopwatch.StartNew();
			foreach (global::BaseEntity baseEntity in global::BaseEntity.saveList)
			{
				baseEntity.InvalidateNetworkCache();
			}
			Debug.Log("Invalidate Network Cache took " + stopwatch.Elapsed.TotalSeconds.ToString("0.00") + " seconds");
			global::SaveRestore.Save(true);
		}

		// Token: 0x06001299 RID: 4761 RVA: 0x0006D370 File Offset: 0x0006B570
		[ServerVar]
		public static string readcfg(ConsoleSystem.Arg arg)
		{
			string serverFolder = Server.GetServerFolder("cfg");
			if (File.Exists(serverFolder + "/serverauto.cfg"))
			{
				string text = File.ReadAllText(serverFolder + "/serverauto.cfg");
				ConsoleSystem.RunFile(ConsoleSystem.Option.Server.Quiet(), text);
			}
			if (File.Exists(serverFolder + "/server.cfg"))
			{
				string text2 = File.ReadAllText(serverFolder + "/server.cfg");
				ConsoleSystem.RunFile(ConsoleSystem.Option.Server.Quiet(), text2);
			}
			return "Server Config Loaded";
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x0600129A RID: 4762 RVA: 0x0006D400 File Offset: 0x0006B600
		// (set) Token: 0x0600129B RID: 4763 RVA: 0x0006D418 File Offset: 0x0006B618
		[ServerVar]
		public static bool compression
		{
			get
			{
				return Net.sv != null && Net.sv.compressionEnabled;
			}
			set
			{
				Net.sv.compressionEnabled = value;
			}
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x0600129C RID: 4764 RVA: 0x0006D428 File Offset: 0x0006B628
		// (set) Token: 0x0600129D RID: 4765 RVA: 0x0006D440 File Offset: 0x0006B640
		[ServerVar]
		public static bool netlog
		{
			get
			{
				return Net.sv != null && Net.sv.logging;
			}
			set
			{
				Net.sv.logging = value;
			}
		}

		// Token: 0x0600129E RID: 4766 RVA: 0x0006D450 File Offset: 0x0006B650
		[ServerUserVar]
		public static void cheatreport(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (basePlayer == null)
			{
				return;
			}
			ulong @uint = arg.GetUInt64(0, 0UL);
			string @string = arg.GetString(1, string.Empty);
			Debug.LogWarning(string.Concat(new object[]
			{
				basePlayer,
				" reported ",
				@uint,
				": ",
				StringEx.ToPrintable(@string, 140)
			}));
			global::EACServer.eacScout.SendPlayerReport(@uint.ToString(), basePlayer.net.connection.userid.ToString(), 1, @string);
		}

		// Token: 0x0600129F RID: 4767 RVA: 0x0006D4F8 File Offset: 0x0006B6F8
		[ServerUserVar(Help = "Get the player combat log")]
		public static string combatlog(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (arg.HasArgs(1) && basePlayer != null && basePlayer.IsAdmin)
			{
				basePlayer = arg.GetPlayerOrSleeper(0);
			}
			if (basePlayer == null)
			{
				return "invalid player";
			}
			return basePlayer.stats.combat.Get(Server.combatlogsize);
		}

		// Token: 0x060012A0 RID: 4768 RVA: 0x0006D560 File Offset: 0x0006B760
		[ServerVar(Help = "Print the current player position.")]
		public static string printpos(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (arg.HasArgs(1))
			{
				basePlayer = arg.GetPlayerOrSleeper(0);
			}
			return (!(basePlayer == null)) ? basePlayer.transform.position.ToString() : "invalid player";
		}

		// Token: 0x060012A1 RID: 4769 RVA: 0x0006D5B8 File Offset: 0x0006B7B8
		[ServerVar(Help = "Print the current player rotation.")]
		public static string printrot(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (arg.HasArgs(1))
			{
				basePlayer = arg.GetPlayerOrSleeper(0);
			}
			return (!(basePlayer == null)) ? basePlayer.transform.rotation.eulerAngles.ToString() : "invalid player";
		}

		// Token: 0x060012A2 RID: 4770 RVA: 0x0006D618 File Offset: 0x0006B818
		[ServerVar(Help = "Print the current player eyes.")]
		public static string printeyes(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (arg.HasArgs(1))
			{
				basePlayer = arg.GetPlayerOrSleeper(0);
			}
			return (!(basePlayer == null)) ? basePlayer.eyes.rotation.eulerAngles.ToString() : "invalid player";
		}

		// Token: 0x060012A3 RID: 4771 RVA: 0x0006D674 File Offset: 0x0006B874
		[ServerVar(ServerAdmin = false, Help = "This sends a snapshot of all the entities in the client's pvs. This is mostly redundant, but we request this when the client starts recording a demo.. so they get all the information.")]
		public static void snapshot(ConsoleSystem.Arg arg)
		{
			if (arg.Player() == null)
			{
				return;
			}
			Debug.Log("Sending full snapshot to " + arg.Player());
			arg.Player().SendNetworkUpdateImmediate(false);
			arg.Player().SendGlobalSnapshot();
			arg.Player().SendFullSnapshot();
		}

		// Token: 0x060012A4 RID: 4772 RVA: 0x0006D6CC File Offset: 0x0006B8CC
		[ServerVar(Help = "Send network update for all players")]
		public static void sendnetworkupdate(ConsoleSystem.Arg arg)
		{
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				basePlayer.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			}
		}

		// Token: 0x04000D2F RID: 3375
		[ServerVar]
		public static string ip = string.Empty;

		// Token: 0x04000D30 RID: 3376
		[ServerVar]
		public static int port = 28015;

		// Token: 0x04000D31 RID: 3377
		[ServerVar]
		public static int queryport;

		// Token: 0x04000D32 RID: 3378
		[ServerVar]
		public static int maxplayers = 500;

		// Token: 0x04000D33 RID: 3379
		[ServerVar]
		public static string hostname = "My Untitled Rust Server";

		// Token: 0x04000D34 RID: 3380
		[ServerVar]
		public static string identity = "my_server_identity";

		// Token: 0x04000D35 RID: 3381
		[ServerVar]
		public static string level = "Procedural Map";

		// Token: 0x04000D36 RID: 3382
		[ServerVar]
		public static int seed;

		// Token: 0x04000D37 RID: 3383
		[ServerVar]
		public static int salt;

		// Token: 0x04000D38 RID: 3384
		[ServerVar]
		public static int worldsize;

		// Token: 0x04000D39 RID: 3385
		[ServerVar]
		public static int saveinterval = 600;

		// Token: 0x04000D3A RID: 3386
		[ServerVar]
		public static bool secure = true;

		// Token: 0x04000D3B RID: 3387
		[ServerVar]
		public static int encryption = 2;

		// Token: 0x04000D3C RID: 3388
		[ServerVar]
		public static int tickrate = 10;

		// Token: 0x04000D3D RID: 3389
		[ServerVar]
		public static int entityrate = 16;

		// Token: 0x04000D3E RID: 3390
		[ServerVar]
		public static float schematime = 1800f;

		// Token: 0x04000D3F RID: 3391
		[ServerVar]
		public static float cycletime = 500f;

		// Token: 0x04000D40 RID: 3392
		[ServerVar]
		public static bool official;

		// Token: 0x04000D41 RID: 3393
		[ServerVar]
		public static bool stats;

		// Token: 0x04000D42 RID: 3394
		[ServerVar]
		public static bool globalchat = true;

		// Token: 0x04000D43 RID: 3395
		[ServerVar]
		public static bool stability = true;

		// Token: 0x04000D44 RID: 3396
		[ServerVar]
		public static bool radiation = true;

		// Token: 0x04000D45 RID: 3397
		[ServerVar]
		public static float itemdespawn = 300f;

		// Token: 0x04000D46 RID: 3398
		[ServerVar]
		public static float corpsedespawn = 300f;

		// Token: 0x04000D47 RID: 3399
		[ServerVar]
		public static bool pve;

		// Token: 0x04000D48 RID: 3400
		[ServerVar]
		public static string description = "No server description has been provided.";

		// Token: 0x04000D49 RID: 3401
		[ServerVar]
		public static string headerimage = string.Empty;

		// Token: 0x04000D4A RID: 3402
		[ServerVar]
		public static string url = string.Empty;

		// Token: 0x04000D4B RID: 3403
		[ServerVar]
		public static string branch = string.Empty;

		// Token: 0x04000D4C RID: 3404
		[ServerVar]
		public static int queriesPerSecond = 2000;

		// Token: 0x04000D4D RID: 3405
		[ServerVar]
		public static int ipQueriesPerMin = 30;

		// Token: 0x04000D4E RID: 3406
		[ServerVar(Saved = true)]
		public static float meleedamage = 1f;

		// Token: 0x04000D4F RID: 3407
		[ServerVar(Saved = true)]
		public static float arrowdamage = 1f;

		// Token: 0x04000D50 RID: 3408
		[ServerVar(Saved = true)]
		public static float bulletdamage = 1f;

		// Token: 0x04000D51 RID: 3409
		[ServerVar(Saved = true)]
		public static float bleedingdamage = 1f;

		// Token: 0x04000D52 RID: 3410
		[ServerVar(Saved = true)]
		public static float meleearmor = 1f;

		// Token: 0x04000D53 RID: 3411
		[ServerVar(Saved = true)]
		public static float arrowarmor = 1f;

		// Token: 0x04000D54 RID: 3412
		[ServerVar(Saved = true)]
		public static float bulletarmor = 1f;

		// Token: 0x04000D55 RID: 3413
		[ServerVar(Saved = true)]
		public static float bleedingarmor = 1f;

		// Token: 0x04000D56 RID: 3414
		[ServerVar]
		public static int updatebatch = 512;

		// Token: 0x04000D57 RID: 3415
		[ServerVar]
		public static int updatebatchspawn = 1024;

		// Token: 0x04000D58 RID: 3416
		[ServerVar]
		public static float planttick = 60f;

		// Token: 0x04000D59 RID: 3417
		[ServerVar]
		public static float planttickscale = 1f;

		// Token: 0x04000D5A RID: 3418
		[ServerVar]
		public static float metabolismtick = 1f;

		// Token: 0x04000D5B RID: 3419
		[ServerVar(Saved = true)]
		public static bool woundingenabled = true;

		// Token: 0x04000D5C RID: 3420
		[ServerVar]
		public static bool plantlightdetection = true;

		// Token: 0x04000D5D RID: 3421
		[ServerVar]
		public static float respawnresetrange = 50f;

		// Token: 0x04000D5E RID: 3422
		[ServerVar]
		public static int maxunack = 4;

		// Token: 0x04000D5F RID: 3423
		[ServerVar]
		public static bool netcache = true;

		// Token: 0x04000D60 RID: 3424
		[ServerVar]
		public static bool corpses = true;

		// Token: 0x04000D61 RID: 3425
		[ServerVar]
		public static bool events = true;

		// Token: 0x04000D62 RID: 3426
		[ServerVar]
		public static bool dropitems = true;

		// Token: 0x04000D63 RID: 3427
		[ServerVar]
		public static int netcachesize;

		// Token: 0x04000D64 RID: 3428
		[ServerVar]
		public static int savecachesize;

		// Token: 0x04000D65 RID: 3429
		[ServerVar]
		public static int combatlogsize = 30;

		// Token: 0x04000D66 RID: 3430
		[ServerVar]
		public static int authtimeout = 60;

		// Token: 0x04000D67 RID: 3431
		[ServerVar]
		public static int playertimeout = 60;

		// Token: 0x04000D68 RID: 3432
		[ServerVar]
		public static int idlekick = 30;

		// Token: 0x04000D69 RID: 3433
		[ServerVar]
		public static int idlekickmode = 1;

		// Token: 0x04000D6A RID: 3434
		[ServerVar]
		public static int idlekickadmins;

		// Token: 0x04000D6B RID: 3435
		[ServerVar(Saved = true)]
		public static bool showHolsteredItems = true;

		// Token: 0x04000D6C RID: 3436
		[ServerVar]
		public static int maxrpcspersecond = 200;

		// Token: 0x04000D6D RID: 3437
		[ServerVar]
		public static int maxcommandspersecond = 100;

		// Token: 0x04000D6E RID: 3438
		[ServerVar]
		public static int maxtickspersecond = 300;
	}
}
