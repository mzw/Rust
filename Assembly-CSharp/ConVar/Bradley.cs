﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002A0 RID: 672
	[ConsoleSystem.Factory("bradley")]
	public class Bradley : ConsoleSystem
	{
		// Token: 0x06001180 RID: 4480 RVA: 0x00068F64 File Offset: 0x00067164
		[ServerVar]
		public static void quickrespawn(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			global::BradleySpawner singleton = global::BradleySpawner.singleton;
			if (singleton == null)
			{
				Debug.LogWarning("No Spawner");
				return;
			}
			if (singleton.spawned)
			{
				singleton.spawned.Kill(global::BaseNetworkable.DestroyMode.None);
			}
			singleton.spawned = null;
			singleton.DoRespawn();
		}

		// Token: 0x04000CB5 RID: 3253
		[ServerVar]
		public static float respawnDelayMinutes = 60f;

		// Token: 0x04000CB6 RID: 3254
		[ServerVar]
		public static float respawnDelayVariance = 1f;

		// Token: 0x04000CB7 RID: 3255
		[ServerVar]
		public static bool enabled = true;
	}
}
