﻿using System;

namespace ConVar
{
	// Token: 0x020002E3 RID: 739
	[ConsoleSystem.Factory("vis")]
	public class Vis : ConsoleSystem
	{
		// Token: 0x04000D85 RID: 3461
		[Help("Turns on debug display of lerp")]
		[ClientVar]
		public static bool lerp;

		// Token: 0x04000D86 RID: 3462
		[ServerVar]
		[Help("Turns on debug display of damages")]
		public static bool damage;

		// Token: 0x04000D87 RID: 3463
		[ServerVar]
		[Help("Turns on debug display of attacks")]
		[ClientVar]
		public static bool attack;

		// Token: 0x04000D88 RID: 3464
		[Help("Turns on debug display of protection")]
		[ClientVar]
		[ServerVar]
		public static bool protection;

		// Token: 0x04000D89 RID: 3465
		[Help("Turns on debug display of weakspots")]
		[ServerVar]
		public static bool weakspots;

		// Token: 0x04000D8A RID: 3466
		[ServerVar]
		[Help("Show trigger entries")]
		public static bool triggers;

		// Token: 0x04000D8B RID: 3467
		[Help("Turns on debug display of hitboxes")]
		[ServerVar]
		public static bool hitboxes;

		// Token: 0x04000D8C RID: 3468
		[ServerVar]
		[Help("Turns on debug display of line of sight checks")]
		public static bool lineofsight;

		// Token: 0x04000D8D RID: 3469
		[ServerVar]
		[Help("Turns on debug display of senses, which are received by Ai")]
		public static bool sense;
	}
}
