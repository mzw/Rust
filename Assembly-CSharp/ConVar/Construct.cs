﻿using System;

namespace ConVar
{
	// Token: 0x020002A9 RID: 681
	[ConsoleSystem.Factory("construct")]
	public class Construct : ConsoleSystem
	{
		// Token: 0x04000CC7 RID: 3271
		[ServerVar]
		[Help("How many minutes before a placed frame gets destroyed")]
		public static float frameminutes = 30f;
	}
}
