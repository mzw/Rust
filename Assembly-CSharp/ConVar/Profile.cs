﻿using System;

namespace ConVar
{
	// Token: 0x020002D7 RID: 727
	[ConsoleSystem.Factory("profile")]
	public class Profile : ConsoleSystem
	{
		// Token: 0x06001280 RID: 4736 RVA: 0x0006CF90 File Offset: 0x0006B190
		[ServerVar]
		[ClientVar]
		public static void start(ConsoleSystem.Arg arg)
		{
		}

		// Token: 0x06001281 RID: 4737 RVA: 0x0006CF94 File Offset: 0x0006B194
		[ServerVar]
		[ClientVar]
		public static void stop(ConsoleSystem.Arg arg)
		{
		}
	}
}
