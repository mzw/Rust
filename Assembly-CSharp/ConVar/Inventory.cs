﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002CB RID: 715
	[ConsoleSystem.Factory("inventory")]
	public class Inventory : ConsoleSystem
	{
		// Token: 0x06001250 RID: 4688 RVA: 0x0006BFF0 File Offset: 0x0006A1F0
		[ServerUserVar]
		public static void lighttoggle(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			if (basePlayer.IsSleeping())
			{
				return;
			}
			basePlayer.LightToggle();
		}

		// Token: 0x06001251 RID: 4689 RVA: 0x0006C030 File Offset: 0x0006A230
		[ServerUserVar]
		public static void endloot(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			if (basePlayer.IsSleeping())
			{
				return;
			}
			basePlayer.inventory.loot.Clear();
		}

		// Token: 0x06001252 RID: 4690 RVA: 0x0006C078 File Offset: 0x0006A278
		[ServerVar]
		public static void give(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			global::Item item = global::ItemManager.CreateByPartialName(arg.GetString(0, string.Empty), 1);
			if (item == null)
			{
				arg.ReplyWith("Invalid Item!");
				return;
			}
			item.amount = arg.GetInt(1, 1);
			item.OnVirginSpawn();
			if (!basePlayer.inventory.GiveItem(item, null))
			{
				item.Remove(0f);
				arg.ReplyWith("Couldn't give item (inventory full?)");
				return;
			}
			basePlayer.Command("note.inv", new object[]
			{
				item.info.itemid,
				item.amount
			});
			Debug.Log(string.Concat(new object[]
			{
				"giving ",
				basePlayer.displayName,
				" ",
				item.amount,
				" x ",
				item.info.displayName.english
			}));
			Chat.Broadcast(string.Concat(new object[]
			{
				basePlayer.displayName,
				" gave themselves ",
				item.amount,
				" x ",
				item.info.displayName.english
			}), "SERVER", "#eee", 0UL);
		}

		// Token: 0x06001253 RID: 4691 RVA: 0x0006C1D4 File Offset: 0x0006A3D4
		[ServerVar]
		public static void resetbp(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			basePlayer.blueprints.Reset();
		}

		// Token: 0x06001254 RID: 4692 RVA: 0x0006C200 File Offset: 0x0006A400
		[ServerVar]
		public static void unlockall(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			basePlayer.blueprints.UnlockAll();
		}

		// Token: 0x06001255 RID: 4693 RVA: 0x0006C22C File Offset: 0x0006A42C
		[ServerVar]
		public static void giveall(ConsoleSystem.Arg arg)
		{
			global::Item item = null;
			string text = "SERVER";
			if (arg.Player() != null)
			{
				text = arg.Player().displayName;
			}
			foreach (global::BasePlayer basePlayer in global::BasePlayer.activePlayerList)
			{
				item = global::ItemManager.CreateByPartialName(arg.GetString(0, string.Empty), 1);
				if (item == null)
				{
					arg.ReplyWith("Invalid Item!");
					return;
				}
				item.amount = arg.GetInt(1, 1);
				item.OnVirginSpawn();
				if (!basePlayer.inventory.GiveItem(item, null))
				{
					item.Remove(0f);
					arg.ReplyWith("Couldn't give item (inventory full?)");
				}
				else
				{
					basePlayer.Command("note.inv", new object[]
					{
						item.info.itemid,
						item.amount
					});
					Debug.Log(string.Concat(new object[]
					{
						" [ServerVar] giving ",
						basePlayer.displayName,
						" ",
						item.amount,
						" x ",
						item.info.displayName.english
					}));
				}
			}
			if (item != null)
			{
				Chat.Broadcast(string.Concat(new object[]
				{
					text,
					" gave everyone ",
					item.amount,
					" x ",
					item.info.displayName.english
				}), "SERVER", "#eee", 0UL);
			}
		}

		// Token: 0x06001256 RID: 4694 RVA: 0x0006C3F8 File Offset: 0x0006A5F8
		[ServerVar]
		public static void giveto(ConsoleSystem.Arg arg)
		{
			string text = "SERVER";
			if (arg.Player() != null)
			{
				text = arg.Player().displayName;
			}
			global::BasePlayer basePlayer = global::BasePlayer.Find(arg.GetString(0, string.Empty));
			if (basePlayer == null)
			{
				arg.ReplyWith("Couldn't find player!");
				return;
			}
			global::Item item = global::ItemManager.CreateByPartialName(arg.GetString(1, string.Empty), 1);
			if (item == null)
			{
				arg.ReplyWith("Invalid Item!");
				return;
			}
			item.amount = arg.GetInt(2, 1);
			item.OnVirginSpawn();
			if (!basePlayer.inventory.GiveItem(item, null))
			{
				item.Remove(0f);
				arg.ReplyWith("Couldn't give item (inventory full?)");
				return;
			}
			basePlayer.Command("note.inv", new object[]
			{
				item.info.itemid,
				item.amount
			});
			Debug.Log(string.Concat(new object[]
			{
				" [ServerVar] giving ",
				basePlayer.displayName,
				" ",
				item.amount,
				" x ",
				item.info.displayName.english
			}));
			Chat.Broadcast(string.Concat(new object[]
			{
				text,
				" gave ",
				basePlayer.displayName,
				" ",
				item.amount,
				" x ",
				item.info.displayName.english
			}), "SERVER", "#eee", 0UL);
		}

		// Token: 0x06001257 RID: 4695 RVA: 0x0006C59C File Offset: 0x0006A79C
		[ServerVar]
		public static void giveid(ConsoleSystem.Arg arg)
		{
			string text = "SERVER";
			if (arg.Player() != null)
			{
				text = arg.Player().displayName;
			}
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			global::Item item = global::ItemManager.CreateByItemID(arg.GetInt(0, 0), 1, 0UL);
			if (item == null)
			{
				arg.ReplyWith("Invalid Item!");
				return;
			}
			item.amount = arg.GetInt(1, 1);
			item.OnVirginSpawn();
			if (!basePlayer.inventory.GiveItem(item, null))
			{
				item.Remove(0f);
				arg.ReplyWith("Couldn't give item (inventory full?)");
				return;
			}
			basePlayer.Command("note.inv", new object[]
			{
				item.info.itemid,
				item.amount
			});
			Debug.Log(string.Concat(new object[]
			{
				" [ServerVar] giving ",
				basePlayer.displayName,
				" ",
				item.amount,
				" x ",
				item.info.displayName.english
			}));
			Chat.Broadcast(string.Concat(new object[]
			{
				text,
				" gave ",
				basePlayer.displayName,
				" ",
				item.amount,
				" x ",
				item.info.displayName.english
			}), "SERVER", "#eee", 0UL);
		}

		// Token: 0x06001258 RID: 4696 RVA: 0x0006C728 File Offset: 0x0006A928
		[ServerVar]
		public static void givearm(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			global::Item item = global::ItemManager.CreateByItemID(arg.GetInt(0, 0), 1, 0UL);
			if (item == null)
			{
				arg.ReplyWith("Invalid Item!");
				return;
			}
			item.amount = arg.GetInt(1, 1);
			item.OnVirginSpawn();
			if (!basePlayer.inventory.GiveItem(item, basePlayer.inventory.containerBelt))
			{
				item.Remove(0f);
				arg.ReplyWith("Couldn't give item (inventory full?)");
				return;
			}
			basePlayer.Command("note.inv", new object[]
			{
				item.info.itemid,
				item.amount
			});
			Debug.Log(string.Concat(new object[]
			{
				" [ServerVar] giving ",
				basePlayer.displayName,
				" ",
				item.amount,
				" x ",
				item.info.displayName.english
			}));
			Chat.Broadcast(string.Concat(new object[]
			{
				basePlayer.displayName,
				" gave themselves ",
				item.amount,
				" x ",
				item.info.displayName.english
			}), "SERVER", "#eee", 0UL);
		}
	}
}
