﻿using System;
using Apex.AI;
using Apex.LoadBalancing;
using UnityEngine;

namespace ConVar
{
	// Token: 0x0200029C RID: 668
	[ConsoleSystem.Factory("ai")]
	public class AI : ConsoleSystem
	{
		// Token: 0x0600116B RID: 4459 RVA: 0x00068638 File Offset: 0x00066838
		public static float TickDelta()
		{
			return 1f / AI.tickrate;
		}

		// Token: 0x0600116C RID: 4460 RVA: 0x00068648 File Offset: 0x00066848
		[ServerVar(Help = "Set the update interval for the agency of dormant and active animals and npcs. (Default: 2.0)")]
		public static void aiManagerLoadBalancerUpdateInterval(ConsoleSystem.Arg args)
		{
			LoadBalancedQueue loadBalancedQueue = global::AiManagerLoadBalancer.aiManagerLoadBalancer as LoadBalancedQueue;
			if (loadBalancedQueue != null)
			{
				loadBalancedQueue.defaultUpdateInterval = args.GetFloat(0, loadBalancedQueue.defaultUpdateInterval);
			}
		}

		// Token: 0x0600116D RID: 4461 RVA: 0x0006867C File Offset: 0x0006687C
		[ServerVar(Help = "Set the update interval for the default load balancer, currently used for cover point generation. (Default: 2.5)")]
		public static void defaultLoadBalancerUpdateInterval(ConsoleSystem.Arg args)
		{
			LoadBalancedQueue loadBalancedQueue = Apex.LoadBalancing.LoadBalancer.defaultBalancer as LoadBalancedQueue;
			if (loadBalancedQueue != null)
			{
				loadBalancedQueue.defaultUpdateInterval = args.GetFloat(0, loadBalancedQueue.defaultUpdateInterval);
			}
		}

		// Token: 0x0600116E RID: 4462 RVA: 0x000686B0 File Offset: 0x000668B0
		[ServerVar(Help = "Set the update interval for the behaviour ai of animals and npcs. (Default: 0.25)")]
		public static void aiLoadBalancerUpdateInterval(ConsoleSystem.Arg args)
		{
			LoadBalancedQueue loadBalancedQueue = AILoadBalancer.aiLoadBalancer as LoadBalancedQueue;
			if (loadBalancedQueue != null)
			{
				loadBalancedQueue.defaultUpdateInterval = args.GetFloat(0, loadBalancedQueue.defaultUpdateInterval);
			}
		}

		// Token: 0x0600116F RID: 4463 RVA: 0x000686E4 File Offset: 0x000668E4
		[ServerVar(Help = "Set the update interval for npc senses that updates the knowledge gathering of npcs. (Default: 0.2)")]
		public static void NpcSenseLoadBalancerUpdateInterval(ConsoleSystem.Arg args)
		{
			LoadBalancedQueue loadBalancedQueue = global::NPCSensesLoadBalancer.NpcSensesLoadBalancer as LoadBalancedQueue;
			if (loadBalancedQueue != null)
			{
				loadBalancedQueue.defaultUpdateInterval = args.GetFloat(0, loadBalancedQueue.defaultUpdateInterval);
			}
		}

		// Token: 0x06001170 RID: 4464 RVA: 0x00068718 File Offset: 0x00066918
		[ServerVar(Help = "Set the update interval for animal senses that updates the knowledge gathering of animals. (Default: 0.2)")]
		public static void AnimalSenseLoadBalancerUpdateInterval(ConsoleSystem.Arg args)
		{
			LoadBalancedQueue loadBalancedQueue = global::AnimalSensesLoadBalancer.animalSensesLoadBalancer as LoadBalancedQueue;
			if (loadBalancedQueue != null)
			{
				loadBalancedQueue.defaultUpdateInterval = args.GetFloat(0, loadBalancedQueue.defaultUpdateInterval);
			}
		}

		// Token: 0x06001171 RID: 4465 RVA: 0x0006874C File Offset: 0x0006694C
		[ServerVar]
		public static void aiDebug_toggle(ConsoleSystem.Arg args)
		{
			int @int = args.GetInt(0, 0);
			if (@int == 0)
			{
				return;
			}
			global::BaseEntity baseEntity = global::BaseNetworkable.serverEntities.Find((uint)@int) as global::BaseEntity;
			if (baseEntity == null)
			{
				return;
			}
			global::NPCPlayerApex npcplayerApex = baseEntity as global::NPCPlayerApex;
			if (npcplayerApex != null)
			{
				TextTable textTable = new TextTable();
				textTable.AddColumn("type");
				textTable.AddColumn("state");
				textTable.AddColumn("health");
				textTable.AddColumn("stuckTime");
				textTable.AddColumn("hasPath");
				textTable.AddColumn("hasEnemyTarget");
				textTable.AddRow(new string[]
				{
					npcplayerApex.Family.ToString(),
					(!npcplayerApex.IsDormant) ? "awake" : "dormant",
					npcplayerApex.health.ToString("N2"),
					npcplayerApex.stuckDuration.ToString("N2"),
					npcplayerApex.HasPath.ToString(),
					(npcplayerApex.AttackTarget != null).ToString()
				});
				args.ReplyWith(textTable.ToString());
				return;
			}
			global::BaseAnimalNPC baseAnimalNPC = baseEntity as global::BaseAnimalNPC;
			if (baseAnimalNPC != null)
			{
				TextTable textTable2 = new TextTable();
				textTable2.AddColumn("type");
				textTable2.AddColumn("state");
				textTable2.AddColumn("health");
				textTable2.AddColumn("stuckTime");
				textTable2.AddColumn("hasPath");
				textTable2.AddColumn("hasEnemyTarget");
				textTable2.AddColumn("hasFoodTarget");
				textTable2.AddRow(new string[]
				{
					baseAnimalNPC.Stats.Family.ToString(),
					(!baseAnimalNPC.IsDormant) ? "awake" : "dormant",
					baseAnimalNPC.health.ToString("N2"),
					baseAnimalNPC.stuckDuration.ToString("N2"),
					baseAnimalNPC.HasPath.ToString(),
					(baseAnimalNPC.AttackTarget != null).ToString(),
					(baseAnimalNPC.FoodTarget != null).ToString()
				});
				args.ReplyWith(textTable2.ToString());
				return;
			}
		}

		// Token: 0x06001172 RID: 4466 RVA: 0x000689D8 File Offset: 0x00066BD8
		[ServerVar]
		public static void aiDebug_LoadBalanceOverdueReportServer(ConsoleSystem.Arg args)
		{
			TextTable textTable = new TextTable();
			textTable.AddColumn("type");
			textTable.AddColumn("count");
			textTable.AddColumn("overdue");
			AI.AddLBTableEntry(ref textTable, "Default", Apex.LoadBalancing.LoadBalancer.defaultBalancer as LoadBalancedQueue);
			AI.AddLBTableEntry(ref textTable, "Ai Manager", global::AiManagerLoadBalancer.aiManagerLoadBalancer as LoadBalancedQueue);
			AI.AddLBTableEntry(ref textTable, "Ai Behaviour", AILoadBalancer.aiLoadBalancer as LoadBalancedQueue);
			AI.AddLBTableEntry(ref textTable, "Npc Senses", global::NPCSensesLoadBalancer.NpcSensesLoadBalancer as LoadBalancedQueue);
			AI.AddLBTableEntry(ref textTable, "Animal Senses", global::AnimalSensesLoadBalancer.animalSensesLoadBalancer as LoadBalancedQueue);
			args.ReplyWith(textTable.ToString());
		}

		// Token: 0x06001173 RID: 4467 RVA: 0x00068A88 File Offset: 0x00066C88
		private static void AddLBTableEntry(ref TextTable table, string name, LoadBalancedQueue lb)
		{
			if (lb == null)
			{
				return;
			}
			float num = 0f;
			if (lb.updatedItemsCount > 0)
			{
				num = Mathf.Clamp(lb.updatesOverdueByTotal / (float)lb.updatedItemsCount - 0.02f, 0f, float.MaxValue);
			}
			table.AddRow(new string[]
			{
				name,
				lb.itemCount.ToString(),
				num.ToString("N2")
			});
		}

		// Token: 0x04000C52 RID: 3154
		[ServerVar]
		public static bool think = true;

		// Token: 0x04000C53 RID: 3155
		[ServerVar]
		public static bool ignoreplayers;

		// Token: 0x04000C54 RID: 3156
		[ServerVar]
		public static bool move = true;

		// Token: 0x04000C55 RID: 3157
		[ServerVar]
		public static float sensetime = 1f;

		// Token: 0x04000C56 RID: 3158
		[ServerVar]
		public static float frametime = 5f;

		// Token: 0x04000C57 RID: 3159
		[ServerVar(Help = "If npc_enable is set to false then npcs won't spawn. (default: true)")]
		public static bool npc_enable = true;

		// Token: 0x04000C58 RID: 3160
		[ServerVar(Help = "npc_max_population_military_tunnels defines the size of the npc population at military tunnels. (default: 3)")]
		public static int npc_max_population_military_tunnels = 3;

		// Token: 0x04000C59 RID: 3161
		[ServerVar(Help = "npc_spawn_per_tick_max_military_tunnels defines how many can maximum spawn at once at military tunnels. (default: 1)")]
		public static int npc_spawn_per_tick_max_military_tunnels = 1;

		// Token: 0x04000C5A RID: 3162
		[ServerVar(Help = "npc_spawn_per_tick_min_military_tunnels defineshow many will minimum spawn at once at military tunnels. (default: 1)")]
		public static int npc_spawn_per_tick_min_military_tunnels = 1;

		// Token: 0x04000C5B RID: 3163
		[ServerVar(Help = "npc_respawn_delay_max_military_tunnels defines the maximum delay between spawn ticks at military tunnels. (default: 1920)")]
		public static float npc_respawn_delay_max_military_tunnels = 1920f;

		// Token: 0x04000C5C RID: 3164
		[ServerVar(Help = "npc_respawn_delay_min_military_tunnels defines the minimum delay between spawn ticks at military tunnels. (default: 480)")]
		public static float npc_respawn_delay_min_military_tunnels = 480f;

		// Token: 0x04000C5D RID: 3165
		[ServerVar(Help = "npc_valid_aim_cone defines how close their aim needs to be on target in order to fire. (default: 5)")]
		public static float npc_valid_aim_cone = 0.8f;

		// Token: 0x04000C5E RID: 3166
		[ServerVar(Help = "npc_cover_compromised_cooldown defines how long a cover point is marked as compromised before it's cleared again for selection. (default: 10)")]
		public static float npc_cover_compromised_cooldown = 10f;

		// Token: 0x04000C5F RID: 3167
		[ServerVar(Help = "If npc_cover_use_path_distance is set to true then npcs will look at the distance between the cover point and their target using the path between the two, rather than the straight-line distance.")]
		public static bool npc_cover_use_path_distance = true;

		// Token: 0x04000C60 RID: 3168
		[ServerVar(Help = "npc_cover_path_vs_straight_dist_max_diff defines what the maximum difference between straight-line distance and path distance can be when evaluating cover points. (default: 2)")]
		public static float npc_cover_path_vs_straight_dist_max_diff = 2f;

		// Token: 0x04000C61 RID: 3169
		[ServerVar(Help = "npc_door_trigger_size defines the size of the trigger box on doors that opens the door as npcs walk close to it (default: 1.5)")]
		public static float npc_door_trigger_size = 1.5f;

		// Token: 0x04000C62 RID: 3170
		[ServerVar(Help = "npc_patrol_point_cooldown defines the cooldown time on a patrol point until it's available again (default: 5)")]
		public static float npc_patrol_point_cooldown = 5f;

		// Token: 0x04000C63 RID: 3171
		[ServerVar(Help = "npc_speed_walk define the speed of an npc when in the walk state, and should be a number between 0 and 1. (Default: 0.18)")]
		public static float npc_speed_walk = 0.18f;

		// Token: 0x04000C64 RID: 3172
		[ServerVar(Help = "npc_speed_walk define the speed of an npc when in the run state, and should be a number between 0 and 1. (Default: 0.4)")]
		public static float npc_speed_run = 0.4f;

		// Token: 0x04000C65 RID: 3173
		[ServerVar(Help = "npc_speed_walk define the speed of an npc when in the sprint state, and should be a number between 0 and 1. (Default: 1.0)")]
		public static float npc_speed_sprint = 1f;

		// Token: 0x04000C66 RID: 3174
		[ServerVar(Help = "npc_speed_walk define the speed of an npc when in the crouched walk state, and should be a number between 0 and 1. (Default: 0.1)")]
		public static float npc_speed_crouch_walk = 0.1f;

		// Token: 0x04000C67 RID: 3175
		[ServerVar(Help = "npc_speed_crouch_run define the speed of an npc when in the crouched run state, and should be a number between 0 and 1. (Default: 0.25)")]
		public static float npc_speed_crouch_run = 0.25f;

		// Token: 0x04000C68 RID: 3176
		[ServerVar(Help = "npc_alertness_drain_rate define the rate at which we drain the alertness level of an NPC when there are no enemies in sight. (Default: 0.01)")]
		public static float npc_alertness_drain_rate = 0.01f;

		// Token: 0x04000C69 RID: 3177
		[ServerVar(Help = "npc_alertness_zero_detection_mod define the threshold of visibility required to detect an enemy when alertness is zero. (Default: 0.5)")]
		public static float npc_alertness_zero_detection_mod = 0.5f;

		// Token: 0x04000C6A RID: 3178
		[ServerVar(Help = "npc_junkpile_a_spawn_chance define the chance for scientists to spawn at junkpile a. (Default: 0)")]
		public static float npc_junkpile_a_spawn_chance;

		// Token: 0x04000C6B RID: 3179
		[ServerVar(Help = "npc_junkpile_g_spawn_chance define the chance for scientists to spawn at junkpile g. (Default: 0)")]
		public static float npc_junkpile_g_spawn_chance;

		// Token: 0x04000C6C RID: 3180
		[ServerVar(Help = "npc_max_junkpile_count define how many npcs can spawn into the world at junkpiles at the same time (does not include monuments) (Default: 20)")]
		public static int npc_max_junkpile_count = 50;

		// Token: 0x04000C6D RID: 3181
		[ServerVar(Help = "If npc_families_no_hurt is true, npcs of the same family won't be able to hurt each other. (default: true)")]
		public static bool npc_families_no_hurt = true;

		// Token: 0x04000C6E RID: 3182
		[ServerVar]
		public static float tickrate = 5f;
	}
}
