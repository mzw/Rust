﻿using System;
using UnityEngine;

namespace ConVar
{
	// Token: 0x020002C5 RID: 709
	[ConsoleSystem.Factory("heli")]
	public class PatrolHelicopter : ConsoleSystem
	{
		// Token: 0x0600123C RID: 4668 RVA: 0x0006B844 File Offset: 0x00069A44
		[ServerVar]
		public static void drop(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			Debug.Log("heli called to : " + basePlayer.transform.position);
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/npc/patrol helicopter/patrolhelicopter.prefab", default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				global::PatrolHelicopterAI component = baseEntity.GetComponent<global::PatrolHelicopterAI>();
				component.SetInitialDestination(basePlayer.transform.position + new Vector3(0f, 10f, 0f), 0f);
				baseEntity.Spawn();
			}
		}

		// Token: 0x0600123D RID: 4669 RVA: 0x0006B8F0 File Offset: 0x00069AF0
		[ServerVar]
		public static void calltome(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			Debug.Log("heli called to : " + basePlayer.transform.position);
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/npc/patrol helicopter/patrolhelicopter.prefab", default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				global::PatrolHelicopterAI component = baseEntity.GetComponent<global::PatrolHelicopterAI>();
				component.SetInitialDestination(basePlayer.transform.position + new Vector3(0f, 10f, 0f), 0.25f);
				baseEntity.Spawn();
			}
		}

		// Token: 0x0600123E RID: 4670 RVA: 0x0006B99C File Offset: 0x00069B9C
		[ServerVar]
		public static void call(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			Debug.Log("Helicopter inbound");
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity("assets/prefabs/npc/patrol helicopter/patrolhelicopter.prefab", default(Vector3), default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.Spawn();
			}
		}

		// Token: 0x0600123F RID: 4671 RVA: 0x0006B9FC File Offset: 0x00069BFC
		[ServerVar]
		public static void strafe(ConsoleSystem.Arg arg)
		{
			global::BasePlayer basePlayer = arg.Player();
			if (!basePlayer)
			{
				return;
			}
			global::PatrolHelicopterAI heliInstance = global::PatrolHelicopterAI.heliInstance;
			if (heliInstance == null)
			{
				Debug.Log("no heli instance");
				return;
			}
			RaycastHit raycastHit;
			if (Physics.Raycast(basePlayer.eyes.HeadRay(), ref raycastHit, 1000f, 1084434689))
			{
				Debug.Log("strafing :" + raycastHit.point);
				heliInstance.interestZoneOrigin = raycastHit.point;
				heliInstance.ExitCurrentState();
				heliInstance.State_Strafe_Enter(raycastHit.point, false);
			}
			else
			{
				Debug.Log("strafe ray missed");
			}
		}

		// Token: 0x04000D19 RID: 3353
		private const string path = "assets/prefabs/npc/patrol helicopter/patrolhelicopter.prefab";

		// Token: 0x04000D1A RID: 3354
		[ServerVar]
		public static float lifetimeMinutes = 15f;

		// Token: 0x04000D1B RID: 3355
		[ServerVar]
		public static int guns = 1;

		// Token: 0x04000D1C RID: 3356
		[ServerVar]
		public static float bulletDamageScale = 1f;

		// Token: 0x04000D1D RID: 3357
		[ServerVar]
		public static float bulletAccuracy = 2f;
	}
}
