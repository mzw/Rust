﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Facepunch;
using Facepunch.Extend;
using Network;
using Network.Visibility;
using Rust;
using UnityEngine;
using UnityEngine.Profiling;

namespace ConVar
{
	// Token: 0x020002C1 RID: 705
	[ConsoleSystem.Factory("global")]
	public class Global : ConsoleSystem
	{
		// Token: 0x17000141 RID: 321
		// (get) Token: 0x060011F7 RID: 4599 RVA: 0x0006AB3C File Offset: 0x00068D3C
		// (set) Token: 0x060011F8 RID: 4600 RVA: 0x0006AB44 File Offset: 0x00068D44
		[ClientVar]
		[ServerVar]
		public static bool timewarning
		{
			get
			{
				return TimeWarning.Enabled;
			}
			set
			{
				TimeWarning.Enabled = value;
			}
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x060011FA RID: 4602 RVA: 0x0006AB54 File Offset: 0x00068D54
		// (set) Token: 0x060011F9 RID: 4601 RVA: 0x0006AB4C File Offset: 0x00068D4C
		[ClientVar]
		[ServerVar]
		public static int developer
		{
			get
			{
				return Global._developer;
			}
			set
			{
				Global._developer = value;
			}
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x0006AB5C File Offset: 0x00068D5C
		[ServerVar]
		public static void restart(ConsoleSystem.Arg args)
		{
			global::ServerMgr.RestartServer(args.GetString(1, string.Empty), args.GetInt(0, 300));
		}

		// Token: 0x060011FC RID: 4604 RVA: 0x0006AB7C File Offset: 0x00068D7C
		[ServerVar]
		[ClientVar]
		public static void quit(ConsoleSystem.Arg args)
		{
			SingletonComponent<global::ServerMgr>.Instance.Shutdown();
			Application.isQuitting = true;
			Net.sv.Stop("quit");
			Process.GetCurrentProcess().Kill();
			Debug.Log("Quitting");
			Application.Quit();
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x0006ABB8 File Offset: 0x00068DB8
		[ServerVar]
		public static void report(ConsoleSystem.Arg args)
		{
			global::ServerPerformance.DoReport();
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x0006ABC0 File Offset: 0x00068DC0
		[ServerVar]
		[ClientVar]
		public static void objects(ConsoleSystem.Arg args)
		{
			Object[] array = Object.FindObjectsOfType<Object>();
			string text = string.Empty;
			Dictionary<Type, int> dictionary = new Dictionary<Type, int>();
			Dictionary<Type, long> dictionary2 = new Dictionary<Type, long>();
			foreach (Object @object in array)
			{
				int runtimeMemorySize = Profiler.GetRuntimeMemorySize(@object);
				if (dictionary.ContainsKey(@object.GetType()))
				{
					Dictionary<Type, int> dictionary3;
					Type type;
					(dictionary3 = dictionary)[type = @object.GetType()] = dictionary3[type] + 1;
				}
				else
				{
					dictionary.Add(@object.GetType(), 1);
				}
				if (dictionary2.ContainsKey(@object.GetType()))
				{
					Dictionary<Type, long> dictionary4;
					Type type2;
					(dictionary4 = dictionary2)[type2 = @object.GetType()] = dictionary4[type2] + (long)runtimeMemorySize;
				}
				else
				{
					dictionary2.Add(@object.GetType(), (long)runtimeMemorySize);
				}
			}
			IOrderedEnumerable<KeyValuePair<Type, long>> orderedEnumerable = from x in dictionary2
			orderby x.Value descending
			select x;
			foreach (KeyValuePair<Type, long> keyValuePair in orderedEnumerable)
			{
				string text2 = text;
				text = string.Concat(new object[]
				{
					text2,
					dictionary[keyValuePair.Key].ToString().PadLeft(10),
					" ",
					NumberExtensions.FormatBytes<long>(keyValuePair.Value, false).PadLeft(15),
					"\t",
					keyValuePair.Key,
					"\n"
				});
			}
			args.ReplyWith(text);
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x0006AD84 File Offset: 0x00068F84
		[ServerVar]
		[ClientVar]
		public static void textures(ConsoleSystem.Arg args)
		{
			Texture[] array = Object.FindObjectsOfType<Texture>();
			string text = string.Empty;
			foreach (Texture texture in array)
			{
				string text2 = NumberExtensions.FormatBytes<int>(Profiler.GetRuntimeMemorySize(texture), false);
				string text3 = text;
				text = string.Concat(new string[]
				{
					text3,
					texture.ToString().PadRight(30),
					texture.name.PadRight(30),
					text2,
					"\n"
				});
			}
			args.ReplyWith(text);
		}

		// Token: 0x06001200 RID: 4608 RVA: 0x0006AE14 File Offset: 0x00069014
		[ServerVar]
		[ClientVar]
		public static void colliders(ConsoleSystem.Arg args)
		{
			int num = (from x in Object.FindObjectsOfType<Collider>()
			where x.enabled
			select x).Count<Collider>();
			int num2 = (from x in Object.FindObjectsOfType<Collider>()
			where !x.enabled
			select x).Count<Collider>();
			string text = string.Concat(new object[]
			{
				num,
				" colliders enabled, ",
				num2,
				" disabled"
			});
			args.ReplyWith(text);
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x0006AEB0 File Offset: 0x000690B0
		[ServerVar]
		[ClientVar]
		public static void error(ConsoleSystem.Arg args)
		{
			GameObject gameObject = null;
			gameObject.transform.position = Vector3.zero;
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x0006AED0 File Offset: 0x000690D0
		[ServerVar]
		[ClientVar]
		public static void queue(ConsoleSystem.Arg args)
		{
			string text = string.Empty;
			text = text + "stabilityCheckQueue:\t\t" + global::StabilityEntity.stabilityCheckQueue.Info() + "\n";
			text = text + "updateSurroundingsQueue:\t" + global::StabilityEntity.updateSurroundingsQueue.Info() + "\n";
			args.ReplyWith(text);
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x0006AF20 File Offset: 0x00069120
		[ServerUserVar]
		public static void setinfo(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			string @string = args.GetString(0, null);
			string string2 = args.GetString(1, null);
			if (@string == null || string2 == null)
			{
				return;
			}
			basePlayer.SetInfo(@string, string2);
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x0006AF68 File Offset: 0x00069168
		[ServerVar]
		public static void sleep(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsSleeping())
			{
				return;
			}
			if (basePlayer.IsSpectating())
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			basePlayer.StartSleeping();
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x0006AFB4 File Offset: 0x000691B4
		[ServerUserVar]
		public static void kill(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsSpectating())
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			if (basePlayer.CanSuicide())
			{
				basePlayer.MarkSuicide();
				basePlayer.Hurt(1000f, Rust.DamageType.Suicide, basePlayer, false);
			}
			else
			{
				basePlayer.ConsoleMessage("You can't suicide again so quickly, wait a while");
			}
		}

		// Token: 0x06001206 RID: 4614 RVA: 0x0006B01C File Offset: 0x0006921C
		[ServerUserVar]
		public static void respawn(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (!basePlayer.IsDead() && !basePlayer.IsSpectating())
			{
				if (Global.developer > 0)
				{
					Debug.LogWarning(basePlayer + " wanted to respawn but isn't dead or spectating");
				}
				basePlayer.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
				return;
			}
			basePlayer.Respawn();
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x0006B07C File Offset: 0x0006927C
		[ServerVar]
		public static void injure(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (basePlayer.IsDead())
			{
				return;
			}
			basePlayer.StartWounded();
		}

		// Token: 0x06001208 RID: 4616 RVA: 0x0006B0B0 File Offset: 0x000692B0
		[ServerVar]
		public static void spectate(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (!basePlayer.IsDead())
			{
				basePlayer.DieInstantly();
			}
			string @string = args.GetString(0, string.Empty);
			if (basePlayer.IsDead())
			{
				basePlayer.StartSpectating();
				basePlayer.UpdateSpectateTarget(@string);
			}
		}

		// Token: 0x06001209 RID: 4617 RVA: 0x0006B108 File Offset: 0x00069308
		[ServerUserVar]
		public static void respawn_sleepingbag(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (!basePlayer.IsDead())
			{
				return;
			}
			uint @uint = args.GetUInt(0, 0u);
			if (@uint == 0u)
			{
				args.ReplyWith("Missing sleeping bag ID");
				return;
			}
			if (!global::SleepingBag.SpawnPlayer(basePlayer, @uint))
			{
				args.ReplyWith("Couldn't spawn in sleeping bag!");
			}
		}

		// Token: 0x0600120A RID: 4618 RVA: 0x0006B168 File Offset: 0x00069368
		[ServerUserVar]
		public static void respawn_sleepingbag_remove(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			uint @uint = args.GetUInt(0, 0u);
			if (@uint == 0u)
			{
				args.ReplyWith("Missing sleeping bag ID");
				return;
			}
			global::SleepingBag.DestroyBag(basePlayer, @uint);
		}

		// Token: 0x0600120B RID: 4619 RVA: 0x0006B1AC File Offset: 0x000693AC
		[ServerUserVar]
		public static void status_sv(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			args.ReplyWith(basePlayer.GetDebugStatus());
		}

		// Token: 0x0600120C RID: 4620 RVA: 0x0006B1D8 File Offset: 0x000693D8
		[ClientVar]
		public static void status_cl(ConsoleSystem.Arg args)
		{
		}

		// Token: 0x0600120D RID: 4621 RVA: 0x0006B1DC File Offset: 0x000693DC
		[ServerVar]
		public static void teleport(ConsoleSystem.Arg args)
		{
			if (args.HasArgs(2))
			{
				global::BasePlayer player = args.GetPlayer(0);
				if (!player)
				{
					return;
				}
				if (!player.IsAlive())
				{
					return;
				}
				global::BasePlayer playerOrSleeper = args.GetPlayerOrSleeper(1);
				if (!playerOrSleeper)
				{
					return;
				}
				if (!playerOrSleeper.IsAlive())
				{
					return;
				}
				player.Teleport(playerOrSleeper);
			}
			else
			{
				global::BasePlayer basePlayer = args.Player();
				if (!basePlayer)
				{
					return;
				}
				if (!basePlayer.IsAlive())
				{
					return;
				}
				global::BasePlayer playerOrSleeper2 = args.GetPlayerOrSleeper(0);
				if (!playerOrSleeper2)
				{
					return;
				}
				if (!playerOrSleeper2.IsAlive())
				{
					return;
				}
				basePlayer.Teleport(playerOrSleeper2);
			}
		}

		// Token: 0x0600120E RID: 4622 RVA: 0x0006B288 File Offset: 0x00069488
		[ServerVar]
		public static void teleport2me(ConsoleSystem.Arg args)
		{
			global::BasePlayer player = args.GetPlayer(0);
			if (!player)
			{
				return;
			}
			if (!player.IsAlive())
			{
				return;
			}
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (!basePlayer.IsAlive())
			{
				return;
			}
			player.Teleport(basePlayer);
		}

		// Token: 0x0600120F RID: 4623 RVA: 0x0006B2DC File Offset: 0x000694DC
		[ServerVar]
		public static void teleportany(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (!basePlayer.IsAlive())
			{
				return;
			}
			basePlayer.Teleport(args.GetString(0, string.Empty), false);
		}

		// Token: 0x06001210 RID: 4624 RVA: 0x0006B31C File Offset: 0x0006951C
		[ServerVar]
		public static void teleportpos(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			if (!basePlayer.IsAlive())
			{
				return;
			}
			basePlayer.Teleport(args.GetVector3(0, Vector3.zero));
		}

		// Token: 0x06001211 RID: 4625 RVA: 0x0006B35C File Offset: 0x0006955C
		[ClientVar]
		[ServerVar]
		public static void free(ConsoleSystem.Arg args)
		{
			Pool.clear_prefabs(args);
			Pool.clear_assets(args);
			Pool.clear_memory(args);
			GC.collect();
			GC.unload();
		}

		// Token: 0x06001212 RID: 4626 RVA: 0x0006B37C File Offset: 0x0006957C
		[ClientVar]
		[ServerVar(ServerUser = true)]
		public static void version(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(string.Format("Protocol: {0}\nBuild Date: {1}\nUnity Version: {2}\nChangeset: {3}\nBranch: {4}", new object[]
			{
				Rust.Protocol.printable,
				BuildInfo.Current.BuildDate,
				Application.unityVersion,
				BuildInfo.Current.Scm.ChangeId,
				BuildInfo.Current.Scm.Branch
			}));
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x0006B3E8 File Offset: 0x000695E8
		[ServerVar]
		[ClientVar]
		public static void sysinfo(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(global::SystemInfoGeneralText.currentInfo);
		}

		// Token: 0x06001214 RID: 4628 RVA: 0x0006B3F8 File Offset: 0x000695F8
		[ServerVar]
		[ClientVar]
		public static void sysuid(ConsoleSystem.Arg arg)
		{
			arg.ReplyWith(SystemInfo.deviceUniqueIdentifier);
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x0006B408 File Offset: 0x00069608
		[ServerVar]
		public static void breakitem(ConsoleSystem.Arg args)
		{
			global::BasePlayer basePlayer = args.Player();
			if (!basePlayer)
			{
				return;
			}
			global::Item activeItem = basePlayer.GetActiveItem();
			if (activeItem != null)
			{
				activeItem.LoseCondition(activeItem.condition);
			}
		}

		// Token: 0x06001216 RID: 4630 RVA: 0x0006B444 File Offset: 0x00069644
		[ClientVar]
		[ServerVar]
		public static void subscriptions(ConsoleSystem.Arg arg)
		{
			TextTable textTable = new TextTable();
			textTable.AddColumn("realm");
			textTable.AddColumn("group");
			global::BasePlayer basePlayer = arg.Player();
			if (basePlayer)
			{
				foreach (Group group in basePlayer.net.subscriber.subscribed)
				{
					textTable.AddRow(new string[]
					{
						"sv",
						group.ID.ToString()
					});
				}
			}
			arg.ReplyWith(textTable.ToString());
		}

		// Token: 0x04000CFB RID: 3323
		private static int _developer;

		// Token: 0x04000CFC RID: 3324
		[ServerVar]
		[ClientVar]
		public static int maxthreads = 8;

		// Token: 0x04000CFD RID: 3325
		[ServerVar(Saved = true)]
		[ClientVar(Saved = true)]
		public static int perf;

		// Token: 0x04000CFE RID: 3326
		[ClientVar(ClientInfo = true, Saved = true, Help = "If you're an admin this will enable god mode")]
		public static bool god;

		// Token: 0x04000CFF RID: 3327
		[ClientVar(ClientInfo = true, Saved = true, Help = "If enabled you will be networked when you're spectating. This means that you will hear audio chat, but also means that cheaters will potentially be able to detect you watching them.")]
		public static bool specnet;
	}
}
