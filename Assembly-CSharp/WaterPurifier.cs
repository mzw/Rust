﻿using System;
using Facepunch;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x0200037C RID: 892
public class WaterPurifier : global::LiquidContainer
{
	// Token: 0x06001522 RID: 5410 RVA: 0x0007A26C File Offset: 0x0007846C
	public override void ServerInit()
	{
		base.ServerInit();
		if (!Application.isLoadingSave)
		{
			this.SpawnStorageEnt();
		}
	}

	// Token: 0x06001523 RID: 5411 RVA: 0x0007A284 File Offset: 0x00078484
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		this.SpawnStorageEnt();
	}

	// Token: 0x06001524 RID: 5412 RVA: 0x0007A294 File Offset: 0x00078494
	public void SpawnStorageEnt()
	{
		this.waterStorage = (global::GameManager.server.CreateEntity(this.storagePrefab.resourcePath, this.storagePrefabAnchor.localPosition, this.storagePrefabAnchor.localRotation, true) as global::LiquidContainer);
		this.waterStorage.SetParent(base.GetParentEntity(), 0u);
		this.waterStorage.Spawn();
	}

	// Token: 0x06001525 RID: 5413 RVA: 0x0007A2F8 File Offset: 0x000784F8
	internal override void OnParentRemoved()
	{
		base.Kill(global::BaseNetworkable.DestroyMode.Gib);
	}

	// Token: 0x06001526 RID: 5414 RVA: 0x0007A304 File Offset: 0x00078504
	public override void OnKilled(global::HitInfo info)
	{
		base.OnKilled(info);
		this.waterStorage.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06001527 RID: 5415 RVA: 0x0007A31C File Offset: 0x0007851C
	public void ParentTemperatureUpdate(float temp)
	{
	}

	// Token: 0x06001528 RID: 5416 RVA: 0x0007A320 File Offset: 0x00078520
	public void CheckCoolDown()
	{
		if (!base.GetParentEntity() || !base.GetParentEntity().IsOn() || !this.HasDirtyWater())
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
			base.CancelInvoke(new Action(this.CheckCoolDown));
		}
	}

	// Token: 0x06001529 RID: 5417 RVA: 0x0007A378 File Offset: 0x00078578
	public bool HasDirtyWater()
	{
		global::Item slot = this.inventory.GetSlot(0);
		return slot != null && slot.info.itemType == global::ItemContainer.ContentsType.Liquid && slot.amount > 0;
	}

	// Token: 0x0600152A RID: 5418 RVA: 0x0007A3B8 File Offset: 0x000785B8
	public void Cook(float timeCooked)
	{
		if (this.waterStorage == null)
		{
			return;
		}
		bool flag = this.HasDirtyWater();
		if (!this.IsBoiling() && flag)
		{
			base.InvokeRepeating(new Action(this.CheckCoolDown), 2f, 2f);
			base.SetFlag(global::BaseEntity.Flags.Reserved1, true, false);
		}
		if (!this.IsBoiling())
		{
			return;
		}
		if (flag)
		{
			float num = timeCooked * ((float)this.waterToProcessPerMinute / 60f);
			this.dirtyWaterProcssed += num;
			this.pendingFreshWater += num / (float)this.freshWaterRatio;
			if (this.dirtyWaterProcssed >= 1f)
			{
				int num2 = Mathf.FloorToInt(this.dirtyWaterProcssed);
				global::Item slot = this.inventory.GetSlot(0);
				slot.UseItem(num2);
				this.dirtyWaterProcssed -= (float)num2;
				base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			}
			if (this.pendingFreshWater >= 1f)
			{
				int num3 = Mathf.FloorToInt(this.pendingFreshWater);
				this.pendingFreshWater -= (float)num3;
				global::Item slot2 = this.waterStorage.inventory.GetSlot(0);
				if (slot2 != null && slot2.info != this.freshWater)
				{
					slot2.RemoveFromContainer();
					slot2.Remove(0f);
				}
				if (slot2 == null)
				{
					global::Item item = global::ItemManager.Create(this.freshWater, num3, 0UL);
					if (!item.MoveToContainer(this.waterStorage.inventory, -1, true))
					{
						item.Remove(0f);
					}
				}
				else
				{
					slot2.amount += num3;
					slot2.amount = Mathf.Clamp(slot2.amount, 0, this.waterStorage.maxStackSize);
					this.waterStorage.inventory.MarkDirty();
				}
				this.waterStorage.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			}
		}
	}

	// Token: 0x0600152B RID: 5419 RVA: 0x0007A5A4 File Offset: 0x000787A4
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		if (info.forDisk && this.waterStorage != null)
		{
			info.msg.miningQuarry = Pool.Get<ProtoBuf.MiningQuarry>();
			info.msg.miningQuarry.extractor = Pool.Get<ResourceExtractor>();
			info.msg.miningQuarry.extractor.outputContents = this.waterStorage.inventory.Save();
		}
	}

	// Token: 0x0600152C RID: 5420 RVA: 0x0007A624 File Offset: 0x00078824
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.fromDisk)
		{
			base.SetFlag(global::BaseEntity.Flags.On, false, false);
			if (info.msg.miningQuarry != null && this.waterStorage != null)
			{
				this.waterStorage.inventory.Load(info.msg.miningQuarry.extractor.outputContents);
			}
		}
	}

	// Token: 0x0600152D RID: 5421 RVA: 0x0007A698 File Offset: 0x00078898
	public bool IsBoiling()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x04000F9A RID: 3994
	public global::GameObjectRef storagePrefab;

	// Token: 0x04000F9B RID: 3995
	public Transform storagePrefabAnchor;

	// Token: 0x04000F9C RID: 3996
	public global::ItemDefinition freshWater;

	// Token: 0x04000F9D RID: 3997
	public int waterToProcessPerMinute = 120;

	// Token: 0x04000F9E RID: 3998
	public int freshWaterRatio = 4;

	// Token: 0x04000F9F RID: 3999
	private global::LiquidContainer waterStorage;

	// Token: 0x04000FA0 RID: 4000
	private float dirtyWaterProcssed;

	// Token: 0x04000FA1 RID: 4001
	private float pendingFreshWater;

	// Token: 0x0200037D RID: 893
	public static class WaterPurifierFlags
	{
		// Token: 0x04000FA2 RID: 4002
		public const global::BaseEntity.Flags Boiling = global::BaseEntity.Flags.Reserved1;
	}
}
