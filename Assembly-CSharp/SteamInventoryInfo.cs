﻿using System;
using Rust.Workshop.Game;
using UnityEngine;

// Token: 0x020006E9 RID: 1769
public class SteamInventoryInfo : SingletonComponent<global::SteamInventoryInfo>
{
	// Token: 0x04001E03 RID: 7683
	public GameObject inventoryItemPrefab;

	// Token: 0x04001E04 RID: 7684
	public GameObject inventoryCanvas;

	// Token: 0x04001E05 RID: 7685
	public GameObject missingItems;

	// Token: 0x04001E06 RID: 7686
	public WorkshopInventoryCraftingControls CraftControl;
}
