﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

// Token: 0x020001F4 RID: 500
public class SoundDefinition : ScriptableObject
{
	// Token: 0x06000F5A RID: 3930 RVA: 0x0005DE9C File Offset: 0x0005C09C
	public float GetLength()
	{
		float result = 0f;
		for (int i = 0; i < this.weightedAudioClips.Count; i++)
		{
			AudioClip audioClip = this.weightedAudioClips[i].audioClip;
			if (audioClip)
			{
				result = Mathf.Max(new float[]
				{
					audioClip.length
				});
			}
		}
		for (int j = 0; j < this.distanceAudioClips.Count; j++)
		{
			List<global::WeightedAudioClip> audioClips = this.distanceAudioClips[j].audioClips;
			for (int k = 0; k < audioClips.Count; k++)
			{
				AudioClip audioClip2 = audioClips[k].audioClip;
				if (audioClip2)
				{
					result = Mathf.Max(new float[]
					{
						audioClip2.length
					});
				}
			}
		}
		return result;
	}

	// Token: 0x06000F5B RID: 3931 RVA: 0x0005DF7C File Offset: 0x0005C17C
	public void OnValidate()
	{
		GameObject gameObject = this.template.Get();
		AudioSource component = gameObject.GetComponent<AudioSource>();
		if (!this.useCustomFalloffCurve)
		{
			this.falloffCurve = component.GetCustomCurve(0);
		}
		if (!this.useCustomSpatialBlendCurve)
		{
			this.spatialBlendCurve = component.GetCustomCurve(1);
		}
		if (!this.useCustomSpreadCurve)
		{
			this.spreadCurve = component.GetCustomCurve(3);
		}
	}

	// Token: 0x040009E0 RID: 2528
	public global::GameObjectRef template;

	// Token: 0x040009E1 RID: 2529
	[Horizontal(2, -1)]
	public List<global::WeightedAudioClip> weightedAudioClips = new List<global::WeightedAudioClip>
	{
		new global::WeightedAudioClip()
	};

	// Token: 0x040009E2 RID: 2530
	public List<global::SoundDefinition.DistanceAudioClipList> distanceAudioClips;

	// Token: 0x040009E3 RID: 2531
	public AudioMixerGroup output;

	// Token: 0x040009E4 RID: 2532
	public bool defaultToFirstPerson;

	// Token: 0x040009E5 RID: 2533
	public bool loop;

	// Token: 0x040009E6 RID: 2534
	public bool randomizeStartPosition;

	// Token: 0x040009E7 RID: 2535
	[Range(0f, 1f)]
	public float volume = 1f;

	// Token: 0x040009E8 RID: 2536
	[Range(0f, 1f)]
	public float volumeVariation;

	// Token: 0x040009E9 RID: 2537
	[Range(-3f, 3f)]
	public float pitch = 1f;

	// Token: 0x040009EA RID: 2538
	[Range(0f, 1f)]
	public float pitchVariation;

	// Token: 0x040009EB RID: 2539
	[Header("Voice limiting")]
	public bool dontVoiceLimit;

	// Token: 0x040009EC RID: 2540
	public int globalVoiceMaxCount = 100;

	// Token: 0x040009ED RID: 2541
	public int localVoiceMaxCount = 100;

	// Token: 0x040009EE RID: 2542
	public float localVoiceRange = 10f;

	// Token: 0x040009EF RID: 2543
	public float voiceLimitFadeOutTime = 0.05f;

	// Token: 0x040009F0 RID: 2544
	public float localVoiceDebounceTime = 0.1f;

	// Token: 0x040009F1 RID: 2545
	[Header("Occlusion Settings")]
	public bool enableOcclusion;

	// Token: 0x040009F2 RID: 2546
	public bool playIfOccluded = true;

	// Token: 0x040009F3 RID: 2547
	public float occlusionFilterFreq = 20000f;

	// Token: 0x040009F4 RID: 2548
	public float occlusionGain = 1f;

	// Token: 0x040009F5 RID: 2549
	public bool forceOccludedPlayback;

	// Token: 0x040009F6 RID: 2550
	[Tooltip("Use this mixer group when the sound is occluded to save DSP CPU usage. Only works for non-looping sounds.")]
	public AudioMixerGroup occludedOutput;

	// Token: 0x040009F7 RID: 2551
	[Header("Custom curves")]
	public AnimationCurve falloffCurve;

	// Token: 0x040009F8 RID: 2552
	public bool useCustomFalloffCurve;

	// Token: 0x040009F9 RID: 2553
	public AnimationCurve spatialBlendCurve;

	// Token: 0x040009FA RID: 2554
	public bool useCustomSpatialBlendCurve;

	// Token: 0x040009FB RID: 2555
	public AnimationCurve spreadCurve;

	// Token: 0x040009FC RID: 2556
	public bool useCustomSpreadCurve;

	// Token: 0x020001F5 RID: 501
	[Serializable]
	public class DistanceAudioClipList
	{
		// Token: 0x040009FD RID: 2557
		public int distance;

		// Token: 0x040009FE RID: 2558
		[Horizontal(2, -1)]
		public List<global::WeightedAudioClip> audioClips;
	}
}
