﻿using System;
using UnityEngine;

// Token: 0x02000388 RID: 904
public class FireSpot : global::BaseEntity
{
	// Token: 0x06001583 RID: 5507 RVA: 0x0007BCF0 File Offset: 0x00079EF0
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
	}

	// Token: 0x04000FC7 RID: 4039
	public GameObject flameEffect;
}
