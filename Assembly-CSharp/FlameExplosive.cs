﻿using System;
using UnityEngine;

// Token: 0x02000389 RID: 905
public class FlameExplosive : global::TimedExplosive
{
	// Token: 0x06001585 RID: 5509 RVA: 0x0007BD30 File Offset: 0x00079F30
	public override void Explode()
	{
		this.Explode(-base.transform.forward);
	}

	// Token: 0x06001586 RID: 5510 RVA: 0x0007BD48 File Offset: 0x00079F48
	public void Explode(Vector3 surfaceNormal)
	{
		if (!base.isServer)
		{
			return;
		}
		int num = 0;
		while ((float)num < this.numToCreate)
		{
			global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.createOnExplode.resourcePath, base.transform.position, default(Quaternion), true);
			if (baseEntity)
			{
				baseEntity.transform.position = base.transform.position;
				Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(this.spreadAngle, surfaceNormal, true);
				baseEntity.transform.rotation = Quaternion.LookRotation(modifiedAimConeDirection);
				baseEntity.creatorEntity = ((!(this.creatorEntity == null)) ? this.creatorEntity : baseEntity);
				baseEntity.Spawn();
				baseEntity.SetVelocity(modifiedAimConeDirection * Random.Range(this.minVelocity, this.maxVelocity));
			}
			num++;
		}
		base.Explode();
	}

	// Token: 0x06001587 RID: 5511 RVA: 0x0007BE30 File Offset: 0x0007A030
	public override void ProjectileImpact(RaycastHit info)
	{
		this.Explode(info.normal);
	}

	// Token: 0x04000FC8 RID: 4040
	public global::GameObjectRef createOnExplode;

	// Token: 0x04000FC9 RID: 4041
	public float numToCreate = 10f;

	// Token: 0x04000FCA RID: 4042
	public float minVelocity = 2f;

	// Token: 0x04000FCB RID: 4043
	public float maxVelocity = 5f;

	// Token: 0x04000FCC RID: 4044
	public float spreadAngle = 90f;
}
