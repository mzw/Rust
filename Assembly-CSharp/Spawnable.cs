﻿using System;
using Facepunch;
using ProtoBuf;
using Rust;
using UnityEngine;

// Token: 0x02000486 RID: 1158
public class Spawnable : MonoBehaviour, IServerComponent
{
	// Token: 0x06001923 RID: 6435 RVA: 0x0008DC38 File Offset: 0x0008BE38
	protected void OnEnable()
	{
		if (Application.isLoadingSave)
		{
			return;
		}
		this.Add();
	}

	// Token: 0x06001924 RID: 6436 RVA: 0x0008DC4C File Offset: 0x0008BE4C
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (Application.isLoadingSave)
		{
			return;
		}
		this.Remove();
	}

	// Token: 0x06001925 RID: 6437 RVA: 0x0008DC6C File Offset: 0x0008BE6C
	private void Add()
	{
		this.SpawnPosition = base.transform.position;
		if (this.Population == null)
		{
			Debug.LogWarning(this.ToString() + " Population is null, entity spawned from outside the spawn handler?");
			return;
		}
		if (SingletonComponent<global::SpawnHandler>.Instance)
		{
			SingletonComponent<global::SpawnHandler>.Instance.AddInstance(this);
		}
	}

	// Token: 0x06001926 RID: 6438 RVA: 0x0008DCCC File Offset: 0x0008BECC
	private void Remove()
	{
		if (this.Population == null)
		{
			return;
		}
		if (SingletonComponent<global::SpawnHandler>.Instance)
		{
			SingletonComponent<global::SpawnHandler>.Instance.RemoveInstance(this);
		}
	}

	// Token: 0x06001927 RID: 6439 RVA: 0x0008DCFC File Offset: 0x0008BEFC
	private void Kill()
	{
		global::BaseEntity baseEntity = base.gameObject.ToBaseEntity();
		baseEntity.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06001928 RID: 6440 RVA: 0x0008DD1C File Offset: 0x0008BF1C
	internal void Save(global::BaseNetworkable.SaveInfo info)
	{
		if (this.Population == null)
		{
			return;
		}
		info.msg.spawnable = Pool.Get<ProtoBuf.Spawnable>();
		info.msg.spawnable.population = this.Population.FilenameStringId;
	}

	// Token: 0x06001929 RID: 6441 RVA: 0x0008DD68 File Offset: 0x0008BF68
	internal void Load(global::BaseNetworkable.LoadInfo info)
	{
		if (info.msg.spawnable != null)
		{
			this.Population = global::FileSystem.Load<global::SpawnPopulation>(global::StringPool.Get(info.msg.spawnable.population), true);
		}
		if (this.Population != null)
		{
			this.Add();
		}
		else
		{
			Debug.LogWarning(base.gameObject + " has no population - should be removed");
			this.Kill();
		}
	}

	// Token: 0x0600192A RID: 6442 RVA: 0x0008DDE0 File Offset: 0x0008BFE0
	protected void OnValidate()
	{
		this.Population = null;
	}

	// Token: 0x040013DC RID: 5084
	[ReadOnly]
	public global::SpawnPopulation Population;

	// Token: 0x040013DD RID: 5085
	internal Vector3 SpawnPosition;
}
