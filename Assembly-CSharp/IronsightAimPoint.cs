﻿using System;
using UnityEngine;

// Token: 0x020007B9 RID: 1977
public class IronsightAimPoint : MonoBehaviour
{
	// Token: 0x060024BE RID: 9406 RVA: 0x000CA138 File Offset: 0x000C8338
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Vector3 normalized = (this.targetPoint.position - base.transform.position).normalized;
		Gizmos.color = Color.red;
		this.DrawArrow(base.transform.position, base.transform.position + normalized * 0.1f, 0.1f);
		Gizmos.color = Color.cyan;
		this.DrawArrow(base.transform.position, this.targetPoint.position, 0.02f);
		Gizmos.color = Color.yellow;
		this.DrawArrow(this.targetPoint.position, this.targetPoint.position + normalized * 3f, 0.02f);
	}

	// Token: 0x060024BF RID: 9407 RVA: 0x000CA214 File Offset: 0x000C8414
	private void DrawArrow(Vector3 start, Vector3 end, float arrowLength)
	{
		Vector3 normalized = (end - start).normalized;
		Vector3 up = Camera.current.transform.up;
		Gizmos.DrawLine(start, end);
		Gizmos.DrawLine(end, end + up * arrowLength - normalized * arrowLength);
		Gizmos.DrawLine(end, end - up * arrowLength - normalized * arrowLength);
		Gizmos.DrawLine(end + up * arrowLength - normalized * arrowLength, end - up * arrowLength - normalized * arrowLength);
	}

	// Token: 0x04002035 RID: 8245
	public Transform targetPoint;
}
