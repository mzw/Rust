﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000033 RID: 51
public class BaseMountable : global::BaseCombatEntity
{
	// Token: 0x0600046B RID: 1131 RVA: 0x00019A04 File Offset: 0x00017C04
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseMountable.OnRpcMessage", 0.1f))
		{
			if (rpc == 1291113684u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_WantsDismount ");
				}
				using (TimeWarning.New("RPC_WantsDismount", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_WantsDismount(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_WantsDismount");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 1245658160u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_WantsMount ");
				}
				using (TimeWarning.New("RPC_WantsMount", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_WantsMount(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_WantsMount");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600046C RID: 1132 RVA: 0x00019CC0 File Offset: 0x00017EC0
	public virtual Vector3 EyePositionForPlayer(global::BasePlayer player)
	{
		if (player.GetMounted() == this)
		{
			return this.eyeOverride.transform.position;
		}
		return Vector3.zero;
	}

	// Token: 0x0600046D RID: 1133 RVA: 0x00019CEC File Offset: 0x00017EEC
	public override float MaxVelocity()
	{
		global::BaseEntity parentEntity = base.GetParentEntity();
		if (parentEntity)
		{
			return parentEntity.MaxVelocity();
		}
		return base.MaxVelocity();
	}

	// Token: 0x0600046E RID: 1134 RVA: 0x00019D18 File Offset: 0x00017F18
	public bool IsMounted()
	{
		return this._mounted != null;
	}

	// Token: 0x17000011 RID: 17
	// (get) Token: 0x0600046F RID: 1135 RVA: 0x00019D28 File Offset: 0x00017F28
	protected override float PositionTickRate
	{
		get
		{
			return 0.05f;
		}
	}

	// Token: 0x06000470 RID: 1136 RVA: 0x00019D30 File Offset: 0x00017F30
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.CanPickup(player) && !this.IsMounted();
	}

	// Token: 0x06000471 RID: 1137 RVA: 0x00019D4C File Offset: 0x00017F4C
	public override void OnKilled(global::HitInfo info)
	{
		this.DismountAllPlayers();
		base.OnKilled(info);
	}

	// Token: 0x06000472 RID: 1138 RVA: 0x00019D5C File Offset: 0x00017F5C
	[global::BaseEntity.RPC_Server]
	public void RPC_WantsMount(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (this._mounted != null)
		{
			return;
		}
		if (!this.NearMountPoint(player))
		{
			return;
		}
		this.MountPlayer(player);
	}

	// Token: 0x06000473 RID: 1139 RVA: 0x00019D98 File Offset: 0x00017F98
	[global::BaseEntity.RPC_Server]
	public void RPC_WantsDismount(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (player != this._mounted)
		{
			return;
		}
		this.DismountPlayer(player);
	}

	// Token: 0x06000474 RID: 1140 RVA: 0x00019DC8 File Offset: 0x00017FC8
	public void MountPlayer(global::BasePlayer player)
	{
		if (this._mounted != null)
		{
			return;
		}
		if (Interface.CallHook("CanMountEntity", new object[]
		{
			this,
			player
		}) != null)
		{
			return;
		}
		player.EnsureDismounted();
		this._mounted = player;
		player.MountObject(this, 0);
		player.MovePosition(this.mountAnchor.transform.position);
		player.transform.rotation = this.mountAnchor.transform.rotation;
		player.ServerRotation = this.mountAnchor.transform.rotation;
		player.OverrideViewAngles(this.mountAnchor.transform.rotation.eulerAngles);
		this._mounted.eyes.NetworkUpdate(this.mountAnchor.transform.rotation);
		player.ClientRPCPlayer<Vector3>(null, player, "ForcePositionTo", player.transform.position);
		base.SetFlag(global::BaseEntity.Flags.Busy, true, false);
		Interface.CallHook("OnEntityMounted", new object[]
		{
			this,
			player
		});
	}

	// Token: 0x06000475 RID: 1141 RVA: 0x00019EE0 File Offset: 0x000180E0
	public void DismountAllPlayers()
	{
		if (this._mounted)
		{
			this.DismountPlayer(this._mounted);
		}
	}

	// Token: 0x06000476 RID: 1142 RVA: 0x00019F00 File Offset: 0x00018100
	public void DismountPlayer(global::BasePlayer player)
	{
		if (this._mounted == null)
		{
			return;
		}
		if (Interface.CallHook("CanDismountEntity", new object[]
		{
			this,
			player
		}) != null)
		{
			return;
		}
		this._mounted.DismountObject();
		Vector3 dismountPosition = this.GetDismountPosition(player);
		this._mounted.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
		this._mounted.MovePosition(dismountPosition);
		this._mounted.eyes.NetworkUpdate(this.dismountAnchor.transform.rotation);
		this._mounted.SendNetworkUpdateImmediate(false);
		this._mounted.ClientRPCPlayer<Vector3>(null, this._mounted, "ForcePositionTo", dismountPosition);
		this._mounted = null;
		base.SetFlag(global::BaseEntity.Flags.Busy, false, false);
		Interface.CallHook("OnEntityDismounted", new object[]
		{
			this,
			player
		});
	}

	// Token: 0x06000477 RID: 1143 RVA: 0x00019FF0 File Offset: 0x000181F0
	public Vector3 GetDismountPosition(global::BasePlayer player)
	{
		int num = 0;
		foreach (Transform transform in this.dismountPositions)
		{
			if (!UnityEngine.Physics.CheckCapsule(transform.transform.position + new Vector3(0f, player.GetRadius(), 0f), transform.transform.position + new Vector3(0f, player.GetHeight() - player.GetRadius(), 0f), player.GetRadius(), 1403068673))
			{
				Vector3 vector = transform.transform.position + new Vector3(0f, player.GetHeight() * 0.5f, 0f);
				if (base.IsVisible(vector) && !UnityEngine.Physics.Linecast(base.transform.position + new Vector3(0f, 1f, 0f), vector, 1075904513))
				{
					return transform.transform.position;
				}
			}
			num++;
		}
		Debug.LogWarning(string.Concat(new object[]
		{
			"Failed to find dismount position for player :",
			player.displayName,
			" / ",
			player.userID,
			" on obj : ",
			base.gameObject.name
		}));
		return base.transform.position + new Vector3(0f, 1f, 0f);
	}

	// Token: 0x06000478 RID: 1144 RVA: 0x0001A174 File Offset: 0x00018374
	public override void ServerInit()
	{
		base.ServerInit();
	}

	// Token: 0x06000479 RID: 1145 RVA: 0x0001A17C File Offset: 0x0001837C
	public void FixedUpdate()
	{
		if (base.isClient)
		{
			return;
		}
		if (!this.isMobile)
		{
			return;
		}
		if (this._mounted)
		{
			this._mounted.MovePosition(this.mountAnchor.transform.position);
		}
	}

	// Token: 0x0600047A RID: 1146 RVA: 0x0001A1CC File Offset: 0x000183CC
	public virtual void PlayerServerInput(global::InputState inputState, global::BasePlayer player)
	{
		if (player != this._mounted)
		{
			return;
		}
	}

	// Token: 0x0600047B RID: 1147 RVA: 0x0001A1E0 File Offset: 0x000183E0
	public virtual float GetComfort()
	{
		return 1f;
	}

	// Token: 0x0600047C RID: 1148 RVA: 0x0001A1E8 File Offset: 0x000183E8
	public bool NearMountPoint(global::BasePlayer player)
	{
		float num = Vector3.Distance(player.transform.position, base.transform.position);
		if (num <= this.maxMountDistance)
		{
			RaycastHit hit;
			if (!UnityEngine.Physics.SphereCast(player.eyes.HeadRay(), 0.25f, ref hit, 2f, 1084434689))
			{
				return false;
			}
			if (hit.GetEntity() && hit.GetEntity().net.ID == this.net.ID)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x0600047D RID: 1149 RVA: 0x0001A280 File Offset: 0x00018480
	public Vector3 ConvertVector(Vector3 vec)
	{
		for (int i = 0; i < 3; i++)
		{
			if (vec[i] > 180f)
			{
				int num;
				vec[num = i] = vec[num] - 360f;
			}
			else if (vec[i] < -180f)
			{
				int num2;
				vec[num2 = i] = vec[num2] + 360f;
			}
		}
		return vec;
	}

	// Token: 0x04000143 RID: 323
	protected global::BasePlayer _mounted;

	// Token: 0x04000144 RID: 324
	public Transform mountAnchor;

	// Token: 0x04000145 RID: 325
	public Transform dismountAnchor;

	// Token: 0x04000146 RID: 326
	public bool isMobile;

	// Token: 0x04000147 RID: 327
	public Transform[] dismountPositions;

	// Token: 0x04000148 RID: 328
	public Transform eyeOverride;

	// Token: 0x04000149 RID: 329
	public float maxMountDistance = 1.5f;
}
