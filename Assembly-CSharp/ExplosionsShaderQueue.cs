﻿using System;
using UnityEngine;

// Token: 0x020007FD RID: 2045
public class ExplosionsShaderQueue : MonoBehaviour
{
	// Token: 0x060025CA RID: 9674 RVA: 0x000D0DE4 File Offset: 0x000CEFE4
	private void Start()
	{
		this.rend = base.GetComponent<Renderer>();
		if (this.rend != null)
		{
			this.rend.sharedMaterial.renderQueue += this.AddQueue;
		}
		else
		{
			base.Invoke("SetProjectorQueue", 0.1f);
		}
	}

	// Token: 0x060025CB RID: 9675 RVA: 0x000D0E40 File Offset: 0x000CF040
	private void SetProjectorQueue()
	{
		base.GetComponent<Projector>().material.renderQueue += this.AddQueue;
	}

	// Token: 0x060025CC RID: 9676 RVA: 0x000D0E60 File Offset: 0x000CF060
	private void OnDisable()
	{
		if (this.rend != null)
		{
			this.rend.sharedMaterial.renderQueue = -1;
		}
	}

	// Token: 0x040021F8 RID: 8696
	public int AddQueue = 1;

	// Token: 0x040021F9 RID: 8697
	private Renderer rend;
}
