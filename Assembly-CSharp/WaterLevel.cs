﻿using System;
using UnityEngine;

// Token: 0x020004A1 RID: 1185
public static class WaterLevel
{
	// Token: 0x060019C6 RID: 6598 RVA: 0x00090EBC File Offset: 0x0008F0BC
	public static float Factor(Bounds bounds)
	{
		float result;
		using (TimeWarning.New("WaterLevel.Factor", 0.1f))
		{
			if (bounds.size == Vector3.zero)
			{
				bounds.size = new Vector3(0.1f, 0.1f, 0.1f);
			}
			global::WaterLevel.WaterInfo waterInfo = global::WaterLevel.GetWaterInfo(bounds);
			result = ((!waterInfo.isValid) ? 0f : Mathf.InverseLerp(bounds.min.y, bounds.max.y, waterInfo.surfaceLevel));
		}
		return result;
	}

	// Token: 0x060019C7 RID: 6599 RVA: 0x00090F78 File Offset: 0x0008F178
	public static bool Test(Vector3 pos)
	{
		bool isValid;
		using (TimeWarning.New("WaterLevel.Test", 0.1f))
		{
			isValid = global::WaterLevel.GetWaterInfo(pos).isValid;
		}
		return isValid;
	}

	// Token: 0x060019C8 RID: 6600 RVA: 0x00090FC8 File Offset: 0x0008F1C8
	public static float GetWaterDepth(Vector3 pos)
	{
		float currentDepth;
		using (TimeWarning.New("WaterLevel.GetWaterDepth", 0.1f))
		{
			currentDepth = global::WaterLevel.GetWaterInfo(pos).currentDepth;
		}
		return currentDepth;
	}

	// Token: 0x060019C9 RID: 6601 RVA: 0x00091018 File Offset: 0x0008F218
	public static global::WaterLevel.WaterInfo GetWaterInfo(Vector3 pos)
	{
		global::WaterLevel.WaterInfo result;
		using (TimeWarning.New("WaterLevel.GetWaterInfo", 0.1f))
		{
			global::WaterLevel.WaterInfo waterInfo = default(global::WaterLevel.WaterInfo);
			float num = (!global::TerrainMeta.WaterMap) ? 0f : global::TerrainMeta.WaterMap.GetHeight(pos);
			if (pos.y > num)
			{
				result = waterInfo;
			}
			else
			{
				float num2 = (!global::TerrainMeta.HeightMap) ? 0f : global::TerrainMeta.HeightMap.GetHeight(pos);
				if (pos.y < num2 - 1f)
				{
					num = 0f;
					if (pos.y > num)
					{
						return waterInfo;
					}
				}
				if (global::Water.Collision && global::Water.Collision.GetIgnore(pos, 0.01f))
				{
					result = waterInfo;
				}
				else
				{
					waterInfo.isValid = true;
					waterInfo.currentDepth = Mathf.Max(0f, num - pos.y);
					waterInfo.overallDepth = Mathf.Max(0f, num - num2);
					waterInfo.surfaceLevel = num;
					result = waterInfo;
				}
			}
		}
		return result;
	}

	// Token: 0x060019CA RID: 6602 RVA: 0x00091168 File Offset: 0x0008F368
	public static global::WaterLevel.WaterInfo GetWaterInfo(Bounds bounds)
	{
		global::WaterLevel.WaterInfo result;
		using (TimeWarning.New("WaterLevel.GetWaterInfo", 0.1f))
		{
			global::WaterLevel.WaterInfo waterInfo = default(global::WaterLevel.WaterInfo);
			float num = (!global::TerrainMeta.WaterMap) ? 0f : global::TerrainMeta.WaterMap.GetHeight(bounds.center);
			if (bounds.min.y > num)
			{
				result = waterInfo;
			}
			else
			{
				float num2 = (!global::TerrainMeta.HeightMap) ? 0f : global::TerrainMeta.HeightMap.GetHeight(bounds.center);
				if (bounds.max.y < num2 - 1f)
				{
					num = 0f;
					if (bounds.min.y > num)
					{
						return waterInfo;
					}
				}
				if (global::Water.Collision && global::Water.Collision.GetIgnore(bounds))
				{
					result = waterInfo;
				}
				else
				{
					waterInfo.isValid = true;
					waterInfo.currentDepth = Mathf.Max(0f, num - bounds.min.y);
					waterInfo.overallDepth = Mathf.Max(0f, num - num2);
					waterInfo.surfaceLevel = num;
					result = waterInfo;
				}
			}
		}
		return result;
	}

	// Token: 0x020004A2 RID: 1186
	public struct WaterInfo
	{
		// Token: 0x04001452 RID: 5202
		public bool isValid;

		// Token: 0x04001453 RID: 5203
		public float currentDepth;

		// Token: 0x04001454 RID: 5204
		public float overallDepth;

		// Token: 0x04001455 RID: 5205
		public float surfaceLevel;
	}
}
