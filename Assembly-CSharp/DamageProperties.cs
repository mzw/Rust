﻿using System;
using UnityEngine;

// Token: 0x0200046E RID: 1134
[CreateAssetMenu(menuName = "Rust/Damage Properties")]
public class DamageProperties : ScriptableObject
{
	// Token: 0x060018DB RID: 6363 RVA: 0x0008BFB8 File Offset: 0x0008A1B8
	public float GetMultiplier(global::HitArea area)
	{
		for (int i = 0; i < this.bones.Length; i++)
		{
			global::DamageProperties.HitAreaProperty hitAreaProperty = this.bones[i];
			if (hitAreaProperty.area == area)
			{
				return hitAreaProperty.damage;
			}
		}
		return (!this.fallback) ? 1f : this.fallback.GetMultiplier(area);
	}

	// Token: 0x060018DC RID: 6364 RVA: 0x0008C020 File Offset: 0x0008A220
	public void ScaleDamage(global::HitInfo info)
	{
		global::HitArea boneArea = info.boneArea;
		if (boneArea == (global::HitArea)-1 || boneArea == (global::HitArea)0)
		{
			return;
		}
		info.damageTypes.ScaleAll(this.GetMultiplier(boneArea));
	}

	// Token: 0x0400138D RID: 5005
	public global::DamageProperties fallback;

	// Token: 0x0400138E RID: 5006
	[Horizontal(1, 0)]
	public global::DamageProperties.HitAreaProperty[] bones;

	// Token: 0x0200046F RID: 1135
	[Serializable]
	public class HitAreaProperty
	{
		// Token: 0x0400138F RID: 5007
		public global::HitArea area = global::HitArea.Head;

		// Token: 0x04001390 RID: 5008
		public float damage = 1f;
	}
}
