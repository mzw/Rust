﻿using System;
using UnityEngine;

// Token: 0x0200072C RID: 1836
public class LocalizeText : MonoBehaviour, global::ILanguageChanged
{
	// Token: 0x04001F0E RID: 7950
	public string token;

	// Token: 0x04001F0F RID: 7951
	[TextArea]
	public string english;

	// Token: 0x04001F10 RID: 7952
	public string append;

	// Token: 0x04001F11 RID: 7953
	public global::LocalizeText.SpecialMode specialMode;

	// Token: 0x0200072D RID: 1837
	public enum SpecialMode
	{
		// Token: 0x04001F13 RID: 7955
		None,
		// Token: 0x04001F14 RID: 7956
		AllUppercase,
		// Token: 0x04001F15 RID: 7957
		AllLowercase
	}
}
