﻿using System;
using UnityEngine;

// Token: 0x02000547 RID: 1351
public class DecorRotate : global::DecorComponent
{
	// Token: 0x06001C8C RID: 7308 RVA: 0x000A0008 File Offset: 0x0009E208
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		uint num = SeedEx.Seed(pos, global::World.Seed) + 2u;
		float num2 = SeedRandom.Range(ref num, this.MinRotation.x, this.MaxRotation.x);
		float num3 = SeedRandom.Range(ref num, this.MinRotation.y, this.MaxRotation.y);
		float num4 = SeedRandom.Range(ref num, this.MinRotation.z, this.MaxRotation.z);
		rot = Quaternion.Euler(num2, num3, num4) * rot;
	}

	// Token: 0x04001797 RID: 6039
	public Vector3 MinRotation = new Vector3(0f, -180f, 0f);

	// Token: 0x04001798 RID: 6040
	public Vector3 MaxRotation = new Vector3(0f, 180f, 0f);
}
