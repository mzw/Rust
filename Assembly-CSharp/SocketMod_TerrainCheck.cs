﻿using System;
using UnityEngine;

// Token: 0x0200022E RID: 558
public class SocketMod_TerrainCheck : global::SocketMod
{
	// Token: 0x06000FFF RID: 4095 RVA: 0x0006156C File Offset: 0x0005F76C
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		bool flag = global::SocketMod_TerrainCheck.IsInTerrain(base.transform.position);
		if (!this.wantsInTerrain)
		{
			flag = !flag;
		}
		Gizmos.color = ((!flag) ? Color.red : Color.green);
		Gizmos.DrawSphere(Vector3.zero, 0.1f);
	}

	// Token: 0x06001000 RID: 4096 RVA: 0x000615D4 File Offset: 0x0005F7D4
	public static bool IsInTerrain(Vector3 vPoint)
	{
		if (!global::TerrainMeta.Collision || !global::TerrainMeta.Collision.GetIgnore(vPoint, 0.01f))
		{
			foreach (Terrain terrain in Terrain.activeTerrains)
			{
				float num = terrain.SampleHeight(vPoint) + terrain.transform.position.y;
				if (num > vPoint.y)
				{
					return true;
				}
			}
		}
		return Physics.Raycast(new Ray(vPoint + Vector3.up * 3f, Vector3.down), 3f, 65536);
	}

	// Token: 0x06001001 RID: 4097 RVA: 0x00061688 File Offset: 0x0005F888
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 vPoint = place.position + place.rotation * this.worldPosition;
		bool flag = global::SocketMod_TerrainCheck.IsInTerrain(vPoint) == this.wantsInTerrain;
		if (flag)
		{
			return true;
		}
		global::Construction.lastPlacementError = this.fullName + ": not in terrain";
		return false;
	}

	// Token: 0x04000AB5 RID: 2741
	public bool wantsInTerrain = true;
}
