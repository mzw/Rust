﻿using System;
using UnityEngine;

// Token: 0x0200056B RID: 1387
public class TerrainBlendMap : global::TerrainMap2D<byte>
{
	// Token: 0x06001CFE RID: 7422 RVA: 0x000A22D8 File Offset: 0x000A04D8
	public override void Setup()
	{
		if (this.BlendTexture != null)
		{
			if (this.BlendTexture.width == this.BlendTexture.height)
			{
				this.res = this.BlendTexture.width;
				this.src = (this.dst = new byte[this.res, this.res]);
				Color32[] pixels = this.BlendTexture.GetPixels32();
				int i = 0;
				int num = 0;
				while (i < this.res)
				{
					int j = 0;
					while (j < this.res)
					{
						this.dst[i, j] = pixels[num].a;
						j++;
						num++;
					}
					i++;
				}
			}
			else
			{
				Debug.LogError("Invalid alpha texture: " + this.BlendTexture.name);
			}
		}
		else
		{
			this.res = this.terrain.terrainData.alphamapResolution;
			this.src = (this.dst = new byte[this.res, this.res]);
			for (int k = 0; k < this.res; k++)
			{
				for (int l = 0; l < this.res; l++)
				{
					this.dst[k, l] = 0;
				}
			}
		}
	}

	// Token: 0x06001CFF RID: 7423 RVA: 0x000A2440 File Offset: 0x000A0640
	public void GenerateTextures()
	{
		this.BlendTexture = new Texture2D(this.res, this.res, 1, true, true);
		this.BlendTexture.name = "BlendTexture";
		this.BlendTexture.wrapMode = 1;
		Color32[] col = new Color32[this.res * this.res];
		Parallel.For(0, this.res, delegate(int z)
		{
			for (int i = 0; i < this.res; i++)
			{
				byte b = this.src[z, i];
				col[z * this.res + i] = new Color32(b, b, b, b);
			}
		});
		this.BlendTexture.SetPixels32(col);
	}

	// Token: 0x06001D00 RID: 7424 RVA: 0x000A24D4 File Offset: 0x000A06D4
	public void ApplyTextures()
	{
		this.BlendTexture.Apply(true, false);
		this.BlendTexture.Compress(false);
		this.BlendTexture.Apply(false, true);
	}

	// Token: 0x06001D01 RID: 7425 RVA: 0x000A24FC File Offset: 0x000A06FC
	public float GetAlpha(Vector3 worldPos)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		return this.GetAlpha(normX, normZ);
	}

	// Token: 0x06001D02 RID: 7426 RVA: 0x000A252C File Offset: 0x000A072C
	public float GetAlpha(float normX, float normZ)
	{
		int num = this.res - 1;
		float num2 = normX * (float)num;
		float num3 = normZ * (float)num;
		int num4 = Mathf.Clamp((int)num2, 0, num);
		int num5 = Mathf.Clamp((int)num3, 0, num);
		int x = Mathf.Min(num4 + 1, num);
		int z = Mathf.Min(num5 + 1, num);
		float num6 = Mathf.Lerp(this.GetAlpha(num4, num5), this.GetAlpha(x, num5), num2 - (float)num4);
		float num7 = Mathf.Lerp(this.GetAlpha(num4, z), this.GetAlpha(x, z), num2 - (float)num4);
		return Mathf.Lerp(num6, num7, num3 - (float)num5);
	}

	// Token: 0x06001D03 RID: 7427 RVA: 0x000A25C4 File Offset: 0x000A07C4
	public float GetAlpha(int x, int z)
	{
		return global::TextureData.Byte2Float((int)this.src[z, x]);
	}

	// Token: 0x06001D04 RID: 7428 RVA: 0x000A25D8 File Offset: 0x000A07D8
	public void SetAlpha(Vector3 worldPos, float a)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetAlpha(normX, normZ, a);
	}

	// Token: 0x06001D05 RID: 7429 RVA: 0x000A2608 File Offset: 0x000A0808
	public void SetAlpha(float normX, float normZ, float a)
	{
		int x = base.Index(normX);
		int z = base.Index(normZ);
		this.SetAlpha(x, z, a);
	}

	// Token: 0x06001D06 RID: 7430 RVA: 0x000A2630 File Offset: 0x000A0830
	public void SetAlpha(int x, int z, float a)
	{
		this.dst[z, x] = global::TextureData.Float2Byte(a);
	}

	// Token: 0x06001D07 RID: 7431 RVA: 0x000A2648 File Offset: 0x000A0848
	public void SetAlpha(int x, int z, float a, float opacity)
	{
		this.SetAlpha(x, z, Mathf.Lerp(this.GetAlpha(x, z), a, opacity));
	}

	// Token: 0x06001D08 RID: 7432 RVA: 0x000A2664 File Offset: 0x000A0864
	public void SetAlpha(Vector3 worldPos, float a, float opacity, float radius, float fade = 0f)
	{
		float normX = global::TerrainMeta.NormalizeX(worldPos.x);
		float normZ = global::TerrainMeta.NormalizeZ(worldPos.z);
		this.SetAlpha(normX, normZ, a, opacity, radius, fade);
	}

	// Token: 0x06001D09 RID: 7433 RVA: 0x000A269C File Offset: 0x000A089C
	public void SetAlpha(float normX, float normZ, float a, float opacity, float radius, float fade = 0f)
	{
		Action<int, int, float> action = delegate(int x, int z, float lerp)
		{
			lerp *= opacity;
			if (lerp > 0f)
			{
				this.SetAlpha(x, z, a, lerp);
			}
		};
		base.ApplyFilter(normX, normZ, radius, fade, action);
	}

	// Token: 0x04001809 RID: 6153
	public Texture2D BlendTexture;
}
