﻿using System;
using UnityEngine;

// Token: 0x020002F4 RID: 756
public class ObjectSpam : MonoBehaviour
{
	// Token: 0x06001328 RID: 4904 RVA: 0x0007094C File Offset: 0x0006EB4C
	private void Start()
	{
		for (int i = 0; i < this.amount; i++)
		{
			GameObject gameObject = Object.Instantiate<GameObject>(this.source);
			gameObject.transform.position = base.transform.position + Vector3Ex.Range(-this.radius, this.radius);
			gameObject.hideFlags = 3;
		}
	}

	// Token: 0x04000DD7 RID: 3543
	public GameObject source;

	// Token: 0x04000DD8 RID: 3544
	public int amount = 1000;

	// Token: 0x04000DD9 RID: 3545
	public float radius;
}
