﻿using System;
using UnityEngine;

// Token: 0x02000607 RID: 1543
[CreateAssetMenu(menuName = "Rust/Environment Volume Properties")]
public class EnvironmentVolumeProperties : ScriptableObject
{
	// Token: 0x06001F5D RID: 8029 RVA: 0x000B2164 File Offset: 0x000B0364
	public float FindReflectionMultiplier(global::EnvironmentType type)
	{
		foreach (global::EnvironmentMultiplier environmentMultiplier in this.ReflectionMultipliers)
		{
			if ((type & environmentMultiplier.Type) != (global::EnvironmentType)0)
			{
				return environmentMultiplier.Multiplier;
			}
		}
		return 1f;
	}

	// Token: 0x06001F5E RID: 8030 RVA: 0x000B21AC File Offset: 0x000B03AC
	public float FindAmbientMultiplier(global::EnvironmentType type)
	{
		foreach (global::EnvironmentMultiplier environmentMultiplier in this.AmbientMultipliers)
		{
			if ((type & environmentMultiplier.Type) != (global::EnvironmentType)0)
			{
				return environmentMultiplier.Multiplier;
			}
		}
		return 1f;
	}

	// Token: 0x04001A56 RID: 6742
	public int ReflectionQuality;

	// Token: 0x04001A57 RID: 6743
	public LayerMask ReflectionCullingFlags;

	// Token: 0x04001A58 RID: 6744
	[Horizontal(1, 0)]
	public global::EnvironmentMultiplier[] ReflectionMultipliers;

	// Token: 0x04001A59 RID: 6745
	[Horizontal(1, 0)]
	public global::EnvironmentMultiplier[] AmbientMultipliers;
}
