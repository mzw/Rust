﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using ConVar;
using EasyAntiCheat.Server.Hydra.Cerberus;
using Facepunch;
using Facepunch.Extend;
using Facepunch.Math;
using Facepunch.Rust;
using Network;
using Network.Visibility;
using Oxide.Core;
using Oxide.Core.Libraries.Covalence;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000042 RID: 66
public class BasePlayer : global::BaseCombatEntity
{
	// Token: 0x0600053B RID: 1339 RVA: 0x0001E038 File Offset: 0x0001C238
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BasePlayer.OnRpcMessage", 0.1f))
		{
			if (rpc == 2177997023u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ClientKeepConnectionAlive ");
				}
				using (TimeWarning.New("ClientKeepConnectionAlive", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("ClientKeepConnectionAlive", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ClientKeepConnectionAlive(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in ClientKeepConnectionAlive");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 618836810u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - ClientLoadingComplete ");
				}
				using (TimeWarning.New("ClientLoadingComplete", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("ClientLoadingComplete", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.ClientLoadingComplete(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in ClientLoadingComplete");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 2248815946u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - OnPlayerLanded ");
				}
				using (TimeWarning.New("OnPlayerLanded", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("OnPlayerLanded", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.OnPlayerLanded(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in OnPlayerLanded");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 3322107216u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - OnProjectileAttack ");
				}
				using (TimeWarning.New("OnProjectileAttack", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("OnProjectileAttack", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.OnProjectileAttack(msg5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in OnProjectileAttack");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
			if (rpc == 3595933759u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - OnProjectileRicochet ");
				}
				using (TimeWarning.New("OnProjectileRicochet", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("OnProjectileRicochet", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg6 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.OnProjectileRicochet(msg6);
						}
					}
					catch (Exception ex5)
					{
						player.Kick("RPC Error in OnProjectileRicochet");
						Debug.LogException(ex5);
					}
				}
				return true;
			}
			if (rpc == 3890520017u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - OnProjectileUpdate ");
				}
				using (TimeWarning.New("OnProjectileUpdate", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.FromOwner.Test("OnProjectileUpdate", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg7 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.OnProjectileUpdate(msg7);
						}
					}
					catch (Exception ex6)
					{
						player.Kick("RPC Error in OnProjectileUpdate");
						Debug.LogException(ex6);
					}
				}
				return true;
			}
			if (rpc == 2695790948u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - PerformanceReport ");
				}
				using (TimeWarning.New("PerformanceReport", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg8 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.PerformanceReport(msg8);
						}
					}
					catch (Exception ex7)
					{
						player.Kick("RPC Error in PerformanceReport");
						Debug.LogException(ex7);
					}
				}
				return true;
			}
			if (rpc == 540658179u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Assist ");
				}
				using (TimeWarning.New("RPC_Assist", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_Assist", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg9 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Assist(msg9);
						}
					}
					catch (Exception ex8)
					{
						player.Kick("RPC Error in RPC_Assist");
						Debug.LogException(ex8);
					}
				}
				return true;
			}
			if (rpc == 1739731598u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_KeepAlive ");
				}
				using (TimeWarning.New("RPC_KeepAlive", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_KeepAlive", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg10 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_KeepAlive(msg10);
						}
					}
					catch (Exception ex9)
					{
						player.Kick("RPC Error in RPC_KeepAlive");
						Debug.LogException(ex9);
					}
				}
				return true;
			}
			if (rpc == 1233366563u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_LootPlayer ");
				}
				using (TimeWarning.New("RPC_LootPlayer", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_LootPlayer", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg11 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_LootPlayer(msg11);
						}
					}
					catch (Exception ex10)
					{
						player.Kick("RPC Error in RPC_LootPlayer");
						Debug.LogException(ex10);
					}
				}
				return true;
			}
			if (rpc == 3021566908u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - SV_Drink ");
				}
				using (TimeWarning.New("SV_Drink", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg12 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.SV_Drink(msg12);
						}
					}
					catch (Exception ex11)
					{
						player.Kick("RPC Error in SV_Drink");
						Debug.LogException(ex11);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600053C RID: 1340 RVA: 0x0001F0D0 File Offset: 0x0001D2D0
	public override global::BasePlayer ToPlayer()
	{
		return this;
	}

	// Token: 0x1700003E RID: 62
	// (get) Token: 0x0600053D RID: 1341 RVA: 0x0001F0D4 File Offset: 0x0001D2D4
	public Connection Connection
	{
		get
		{
			return (this.net != null) ? this.net.connection : null;
		}
	}

	// Token: 0x1700003F RID: 63
	// (get) Token: 0x0600053E RID: 1342 RVA: 0x0001F0F4 File Offset: 0x0001D2F4
	// (set) Token: 0x0600053F RID: 1343 RVA: 0x0001F0FC File Offset: 0x0001D2FC
	public string displayName
	{
		get
		{
			return this._displayName;
		}
		set
		{
			this._displayName = value;
		}
	}

	// Token: 0x06000540 RID: 1344 RVA: 0x0001F108 File Offset: 0x0001D308
	public string GetSubName(int maxlen = 32)
	{
		string text = this.displayName;
		if (text.Length > maxlen)
		{
			text = text.Substring(0, maxlen) + "..";
		}
		return text;
	}

	// Token: 0x06000541 RID: 1345 RVA: 0x0001F13C File Offset: 0x0001D33C
	public bool CanInteract()
	{
		return !this.IsDead() && !this.IsSleeping() && !this.IsWounded();
	}

	// Token: 0x06000542 RID: 1346 RVA: 0x0001F160 File Offset: 0x0001D360
	public override float StartHealth()
	{
		return Random.Range(50f, 60f);
	}

	// Token: 0x06000543 RID: 1347 RVA: 0x0001F174 File Offset: 0x0001D374
	public override float StartMaxHealth()
	{
		return 100f;
	}

	// Token: 0x06000544 RID: 1348 RVA: 0x0001F17C File Offset: 0x0001D37C
	public override float MaxHealth()
	{
		return 100f;
	}

	// Token: 0x06000545 RID: 1349 RVA: 0x0001F184 File Offset: 0x0001D384
	public override float MaxVelocity()
	{
		if (this.IsSleeping())
		{
			return 0f;
		}
		if (this.isMounted)
		{
			return this.GetMounted().MaxVelocity();
		}
		return this.GetMaxSpeed();
	}

	// Token: 0x06000546 RID: 1350 RVA: 0x0001F1B4 File Offset: 0x0001D3B4
	public override void InitShared()
	{
		this.Belt = new global::PlayerBelt(this);
		this.cachedProtection = ScriptableObject.CreateInstance<global::ProtectionProperties>();
		this.baseProtection = ScriptableObject.CreateInstance<global::ProtectionProperties>();
		this.inventory = base.GetComponent<global::PlayerInventory>();
		this.blueprints = base.GetComponent<global::PlayerBlueprints>();
		this.metabolism = base.GetComponent<global::PlayerMetabolism>();
		this.eyes = base.GetComponent<global::PlayerEyes>();
		this.input = base.GetComponent<global::PlayerInput>();
		base.InitShared();
	}

	// Token: 0x06000547 RID: 1351 RVA: 0x0001F228 File Offset: 0x0001D428
	public override void DestroyShared()
	{
		Object.Destroy(this.cachedProtection);
		Object.Destroy(this.baseProtection);
		base.DestroyShared();
	}

	// Token: 0x06000548 RID: 1352 RVA: 0x0001F248 File Offset: 0x0001D448
	public static void ServerCycle(float deltaTime)
	{
		global::BasePlayer.activePlayerList.RemoveAll((global::BasePlayer x) => x == null);
		List<global::BasePlayer> list = Facepunch.Pool.Get<List<global::BasePlayer>>();
		list.AddRange(global::BasePlayer.activePlayerList);
		for (int i = 0; i < list.Count; i++)
		{
			if (!(list[i] == null))
			{
				list[i].ServerUpdate(deltaTime);
			}
		}
		if (ConVar.Server.idlekick > 0 && ((global::ServerMgr.AvailableSlots <= 0 && ConVar.Server.idlekickmode == 1) || ConVar.Server.idlekickmode == 2))
		{
			for (int j = 0; j < list.Count; j++)
			{
				if (list[j].IdleTime >= (float)(ConVar.Server.idlekick * 60))
				{
					if (!list[j].IsAdmin || ConVar.Server.idlekickadmins != 0)
					{
						if (!list[j].IsDeveloper || ConVar.Server.idlekickadmins != 0)
						{
							list[j].Kick("Idle for " + ConVar.Server.idlekick + " minutes");
						}
					}
				}
			}
		}
		Facepunch.Pool.FreeList<global::BasePlayer>(ref list);
	}

	// Token: 0x06000549 RID: 1353 RVA: 0x0001F398 File Offset: 0x0001D598
	public Bounds GetBounds(bool ducked)
	{
		return new Bounds(base.transform.position + this.GetOffset(ducked), this.GetSize(ducked));
	}

	// Token: 0x0600054A RID: 1354 RVA: 0x0001F3C0 File Offset: 0x0001D5C0
	public Bounds GetBounds()
	{
		return this.GetBounds(this.modelState.ducked);
	}

	// Token: 0x0600054B RID: 1355 RVA: 0x0001F3D4 File Offset: 0x0001D5D4
	public Vector3 GetCenter(bool ducked)
	{
		return base.transform.position + this.GetOffset(ducked);
	}

	// Token: 0x0600054C RID: 1356 RVA: 0x0001F3F0 File Offset: 0x0001D5F0
	public Vector3 GetCenter()
	{
		return this.GetCenter(this.modelState.ducked);
	}

	// Token: 0x0600054D RID: 1357 RVA: 0x0001F404 File Offset: 0x0001D604
	public Vector3 GetOffset(bool ducked)
	{
		if (ducked)
		{
			return new Vector3(0f, 0.55f, 0f);
		}
		return new Vector3(0f, 0.9f, 0f);
	}

	// Token: 0x0600054E RID: 1358 RVA: 0x0001F438 File Offset: 0x0001D638
	public Vector3 GetOffset()
	{
		return this.GetOffset(this.modelState.ducked);
	}

	// Token: 0x0600054F RID: 1359 RVA: 0x0001F44C File Offset: 0x0001D64C
	public Vector3 GetSize(bool ducked)
	{
		if (ducked)
		{
			return new Vector3(1f, 1.1f, 1f);
		}
		return new Vector3(1f, 1.8f, 1f);
	}

	// Token: 0x06000550 RID: 1360 RVA: 0x0001F480 File Offset: 0x0001D680
	public Vector3 GetSize()
	{
		return this.GetSize(this.modelState.ducked);
	}

	// Token: 0x06000551 RID: 1361 RVA: 0x0001F494 File Offset: 0x0001D694
	public float GetHeight(bool ducked)
	{
		if (ducked)
		{
			return 1.1f;
		}
		return 1.8f;
	}

	// Token: 0x06000552 RID: 1362 RVA: 0x0001F4A8 File Offset: 0x0001D6A8
	public float GetHeight()
	{
		return this.GetHeight(this.modelState.ducked);
	}

	// Token: 0x06000553 RID: 1363 RVA: 0x0001F4BC File Offset: 0x0001D6BC
	public float GetRadius()
	{
		return 0.5f;
	}

	// Token: 0x06000554 RID: 1364 RVA: 0x0001F4C4 File Offset: 0x0001D6C4
	public float GetJumpHeight()
	{
		return 1.5f;
	}

	// Token: 0x06000555 RID: 1365 RVA: 0x0001F4CC File Offset: 0x0001D6CC
	public float MaxDeployDistance(global::Item item)
	{
		return 8f;
	}

	// Token: 0x06000556 RID: 1366 RVA: 0x0001F4D4 File Offset: 0x0001D6D4
	public float GetMinSpeed()
	{
		return this.GetSpeed(0f, 1f);
	}

	// Token: 0x06000557 RID: 1367 RVA: 0x0001F4E8 File Offset: 0x0001D6E8
	public float GetMaxSpeed()
	{
		return this.GetSpeed(1f, 0f) * (1f - this.clothingMoveSpeedReduction);
	}

	// Token: 0x06000558 RID: 1368 RVA: 0x0001F508 File Offset: 0x0001D708
	public float GetSpeed(float running, float ducking)
	{
		return Mathf.Lerp(Mathf.Lerp(2.8f, 5.5f, running), 1.7f, ducking);
	}

	// Token: 0x06000559 RID: 1369 RVA: 0x0001F528 File Offset: 0x0001D728
	public override void OnAttacked(global::HitInfo info)
	{
		if (Interface.CallHook("IOnBasePlayerAttacked", new object[]
		{
			this,
			info
		}) != null)
		{
			return;
		}
		float health = base.health;
		if (base.isServer)
		{
			global::HitArea boneArea = info.boneArea;
			if (boneArea != (global::HitArea)-1)
			{
				List<global::Item> list = Facepunch.Pool.GetList<global::Item>();
				list.AddRange(this.inventory.containerWear.itemList);
				for (int i = 0; i < list.Count; i++)
				{
					global::Item item = list[i];
					if (item != null)
					{
						global::ItemModWearable component = item.info.GetComponent<global::ItemModWearable>();
						if (!(component == null))
						{
							if (component.ProtectsArea(boneArea))
							{
								item.OnAttacked(info);
							}
						}
					}
				}
				Facepunch.Pool.FreeList<global::Item>(ref list);
				this.inventory.ServerUpdate(0f);
			}
		}
		base.OnAttacked(info);
		if (base.isServer && base.isServer && info.hasDamage)
		{
			if (!info.damageTypes.Has(Rust.DamageType.Bleeding) && info.damageTypes.IsBleedCausing() && !this.IsWounded() && !this.IsImmortal())
			{
				this.metabolism.bleeding.Add(info.damageTypes.Total() * 0.2f);
			}
			this.CheckDeathCondition(info);
			if (this.net != null && this.net.connection != null)
			{
				global::Effect effect = new global::Effect();
				effect.Init(global::Effect.Type.Generic, base.transform.position, base.transform.forward, null);
				effect.pooledString = "assets/bundled/prefabs/fx/takedamage_hit.prefab";
				global::EffectNetwork.Send(effect, this.net.connection);
			}
			string a = global::StringPool.Get(info.HitBone);
			Vector3 normalized = (info.PointEnd - info.PointStart).normalized;
			bool flag = Vector3.Dot(normalized, this.eyes.BodyForward()) > 0.4f;
			if (info.isHeadshot)
			{
				if (flag)
				{
					base.SignalBroadcast(global::BaseEntity.Signal.Flinch_RearHead, string.Empty, null);
				}
				else
				{
					base.SignalBroadcast(global::BaseEntity.Signal.Flinch_Head, string.Empty, null);
				}
				global::BasePlayer initiatorPlayer = info.InitiatorPlayer;
				global::Effect.server.Run("assets/bundled/prefabs/fx/headshot.prefab", this, 0u, new Vector3(0f, 2f, 0f), Vector3.zero, (!(initiatorPlayer != null)) ? null : initiatorPlayer.net.connection, false);
				if (initiatorPlayer)
				{
					initiatorPlayer.stats.Add("headshot", 1, global::Stats.Steam);
				}
			}
			else if (flag)
			{
				base.SignalBroadcast(global::BaseEntity.Signal.Flinch_RearTorso, string.Empty, null);
			}
			else if (a == "spine" || a == "spine2")
			{
				base.SignalBroadcast(global::BaseEntity.Signal.Flinch_Stomach, string.Empty, null);
			}
			else
			{
				base.SignalBroadcast(global::BaseEntity.Signal.Flinch_Chest, string.Empty, null);
			}
		}
		if (this.stats != null)
		{
			if (this.IsWounded())
			{
				this.stats.combat.Log(info, health, base.health, "wounded");
			}
			else if (this.IsDead())
			{
				this.stats.combat.Log(info, health, base.health, "killed");
			}
			else
			{
				this.stats.combat.Log(info, health, base.health, null);
			}
		}
	}

	// Token: 0x0600055A RID: 1370 RVA: 0x0001F8C0 File Offset: 0x0001DAC0
	public void UpdatePlayerCollider(bool state)
	{
		if (this.triggerCollider == null)
		{
			this.triggerCollider = base.gameObject.GetComponent<Collider>();
		}
		if (this.triggerCollider.enabled != state)
		{
			base.RemoveFromTriggers();
		}
		this.triggerCollider.enabled = state;
	}

	// Token: 0x0600055B RID: 1371 RVA: 0x0001F914 File Offset: 0x0001DB14
	public void UpdatePlayerRigidbody(bool state)
	{
		if (this.physicsRigidbody == null)
		{
			this.physicsRigidbody = base.gameObject.GetComponent<Rigidbody>();
		}
		if (state)
		{
			if (this.physicsRigidbody == null)
			{
				this.physicsRigidbody = base.gameObject.AddComponent<Rigidbody>();
				this.physicsRigidbody.useGravity = false;
				this.physicsRigidbody.isKinematic = true;
				this.physicsRigidbody.mass = 1f;
				this.physicsRigidbody.interpolation = 0;
				this.physicsRigidbody.collisionDetectionMode = 0;
			}
		}
		else
		{
			base.RemoveFromTriggers();
			if (this.physicsRigidbody != null)
			{
				global::GameManager.Destroy(this.physicsRigidbody, 0f);
				this.physicsRigidbody = null;
			}
		}
	}

	// Token: 0x0600055C RID: 1372 RVA: 0x0001F9E0 File Offset: 0x0001DBE0
	public bool CanAttack()
	{
		global::HeldEntity heldEntity = this.GetHeldEntity();
		if (heldEntity == null)
		{
			return false;
		}
		bool flag = this.IsSwimming();
		bool flag2 = heldEntity.CanBeUsedInWater();
		return !this.modelState.onLadder && (flag || this.modelState.onground) && (!flag || flag2);
	}

	// Token: 0x0600055D RID: 1373 RVA: 0x0001FA4C File Offset: 0x0001DC4C
	public bool OnLadder()
	{
		return this.modelState.onLadder;
	}

	// Token: 0x0600055E RID: 1374 RVA: 0x0001FA5C File Offset: 0x0001DC5C
	public bool IsSwimming()
	{
		return this.modelState.waterLevel >= 0.65f;
	}

	// Token: 0x0600055F RID: 1375 RVA: 0x0001FA74 File Offset: 0x0001DC74
	public bool IsHeadUnderwater()
	{
		return this.modelState.waterLevel > 0.75f;
	}

	// Token: 0x06000560 RID: 1376 RVA: 0x0001FA88 File Offset: 0x0001DC88
	public bool IsOnGround()
	{
		return this.modelState.onground;
	}

	// Token: 0x06000561 RID: 1377 RVA: 0x0001FA98 File Offset: 0x0001DC98
	public bool IsRunning()
	{
		return this.modelState != null && this.modelState.sprinting;
	}

	// Token: 0x06000562 RID: 1378 RVA: 0x0001FAB4 File Offset: 0x0001DCB4
	public bool IsDucked()
	{
		return this.modelState != null && this.modelState.ducked;
	}

	// Token: 0x06000563 RID: 1379 RVA: 0x0001FAD0 File Offset: 0x0001DCD0
	public void ChatMessage(string msg)
	{
		if (!base.isServer)
		{
			return;
		}
		if (Interface.CallHook("OnMessagePlayer", new object[]
		{
			msg,
			this
		}) != null)
		{
			return;
		}
		this.SendConsoleCommand("chat.add", new object[]
		{
			0,
			msg
		});
	}

	// Token: 0x06000564 RID: 1380 RVA: 0x0001FB28 File Offset: 0x0001DD28
	public void ConsoleMessage(string msg)
	{
		if (base.isServer)
		{
			this.SendConsoleCommand("echo " + msg, new object[0]);
			return;
		}
	}

	// Token: 0x06000565 RID: 1381 RVA: 0x0001FB50 File Offset: 0x0001DD50
	public override float PenetrationResistance(global::HitInfo info)
	{
		return 100f;
	}

	// Token: 0x06000566 RID: 1382 RVA: 0x0001FB58 File Offset: 0x0001DD58
	public override void ScaleDamage(global::HitInfo info)
	{
		if (info.UseProtection)
		{
			global::HitArea boneArea = info.boneArea;
			if (boneArea != (global::HitArea)-1)
			{
				this.cachedProtection.Clear();
				this.cachedProtection.Add(this.inventory.containerWear.itemList, boneArea);
				this.cachedProtection.Multiply(Rust.DamageType.Arrow, ConVar.Server.arrowarmor);
				this.cachedProtection.Multiply(Rust.DamageType.Bullet, ConVar.Server.bulletarmor);
				this.cachedProtection.Multiply(Rust.DamageType.Slash, ConVar.Server.meleearmor);
				this.cachedProtection.Multiply(Rust.DamageType.Blunt, ConVar.Server.meleearmor);
				this.cachedProtection.Multiply(Rust.DamageType.Stab, ConVar.Server.meleearmor);
				this.cachedProtection.Multiply(Rust.DamageType.Bleeding, ConVar.Server.bleedingarmor);
				this.cachedProtection.Scale(info.damageTypes, 1f);
			}
			else
			{
				this.baseProtection.Scale(info.damageTypes, 1f);
			}
		}
		if (info.damageProperties)
		{
			info.damageProperties.ScaleDamage(info);
		}
	}

	// Token: 0x06000567 RID: 1383 RVA: 0x0001FC60 File Offset: 0x0001DE60
	private void UpdateMoveSpeedFromClothing()
	{
		float num = 0f;
		float num2 = 0f;
		bool flag = false;
		foreach (global::Item item in this.inventory.containerWear.itemList)
		{
			global::ItemModWearable component = item.info.GetComponent<global::ItemModWearable>();
			if (component)
			{
				if (component.blocksAiming)
				{
					flag = true;
				}
				if (component.movementProperties != null)
				{
					num2 += component.movementProperties.speedReduction;
					num = Mathf.Max(num, component.movementProperties.minSpeedReduction);
				}
			}
		}
		this.clothingMoveSpeedReduction = Mathf.Max(num2, num);
		this.clothingBlocksAiming = flag;
	}

	// Token: 0x06000568 RID: 1384 RVA: 0x0001FD40 File Offset: 0x0001DF40
	private void UpdateProtectionFromClothing()
	{
		this.baseProtection.Clear();
		this.baseProtection.Add(this.inventory.containerWear.itemList, (global::HitArea)-1);
		float num = 1f / (float)this.inventory.containerWear.capacity;
		for (int i = 0; i < this.baseProtection.amounts.Length; i++)
		{
			if (i != 17)
			{
				this.baseProtection.amounts[i] *= num;
			}
		}
	}

	// Token: 0x06000569 RID: 1385 RVA: 0x0001FDD0 File Offset: 0x0001DFD0
	public override string Categorize()
	{
		return "player";
	}

	// Token: 0x0600056A RID: 1386 RVA: 0x0001FDD8 File Offset: 0x0001DFD8
	public override string ToString()
	{
		if (this._name == null)
		{
			if (base.isServer)
			{
				this._name = string.Format("{1}[{0}/{2}]", (this.net == null) ? 0u : this.net.ID, this.displayName, this.userID);
			}
			else
			{
				this._name = base.ShortPrefabName;
			}
		}
		return this._name;
	}

	// Token: 0x0600056B RID: 1387 RVA: 0x0001FE54 File Offset: 0x0001E054
	public string GetDebugStatus()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendFormat("Entity: {0}\n", this.ToString());
		stringBuilder.AppendFormat("Name: {0}\n", this.displayName);
		stringBuilder.AppendFormat("SteamID: {0}\n", this.userID);
		IEnumerator enumerator = Enum.GetValues(typeof(global::BasePlayer.PlayerFlags)).GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				global::BasePlayer.PlayerFlags playerFlags = (global::BasePlayer.PlayerFlags)obj;
				stringBuilder.AppendFormat("{1}: {0}\n", this.HasPlayerFlag(playerFlags), playerFlags);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return stringBuilder.ToString();
	}

	// Token: 0x0600056C RID: 1388 RVA: 0x0001FF20 File Offset: 0x0001E120
	public override global::Item GetItem(uint itemId)
	{
		if (this.inventory == null)
		{
			return null;
		}
		return this.inventory.FindItemUID(itemId);
	}

	// Token: 0x17000040 RID: 64
	// (get) Token: 0x0600056D RID: 1389 RVA: 0x0001FF44 File Offset: 0x0001E144
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return base.Traits | global::BaseEntity.TraitFlag.Human | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat | global::BaseEntity.TraitFlag.Alive;
		}
	}

	// Token: 0x0600056E RID: 1390 RVA: 0x0001FF58 File Offset: 0x0001E158
	public bool TriggeredAntiHack(float seconds = 1f, float score = float.PositiveInfinity)
	{
		return UnityEngine.Time.realtimeSinceStartup - this.lastViolationTime < seconds || this.violationLevel > score;
	}

	// Token: 0x0600056F RID: 1391 RVA: 0x0001FF78 File Offset: 0x0001E178
	public bool UsedAdminCheat(float seconds = 1f)
	{
		return UnityEngine.Time.realtimeSinceStartup - this.lastAdminCheatTime < seconds;
	}

	// Token: 0x06000570 RID: 1392 RVA: 0x0001FF8C File Offset: 0x0001E18C
	public int GetAntiHackKicks()
	{
		return global::AntiHack.GetKickRecord(this);
	}

	// Token: 0x06000571 RID: 1393 RVA: 0x0001FF94 File Offset: 0x0001E194
	public void ResetAntiHack()
	{
		this.violationLevel = 0f;
		this.lastViolationTime = 0f;
		this.speedhackDeltaTime = 0f;
		this.speedhackDistance = 0f;
		this.speedhackTickets = 0;
		this.speedhackHistory.Clear();
		this.flyhackDistanceVertical = 0f;
		this.flyhackDistanceHorizontal = 0f;
	}

	// Token: 0x06000572 RID: 1394 RVA: 0x0001FFF8 File Offset: 0x0001E1F8
	public override bool CanBeLooted(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanLootPlayer", new object[]
		{
			this,
			player
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return !(player == this) && (this.IsWounded() || this.IsSleeping());
	}

	// Token: 0x06000573 RID: 1395 RVA: 0x00020054 File Offset: 0x0001E254
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_LootPlayer(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (!this.CanBeLooted(msg.player))
		{
			return;
		}
		msg.player.inventory.loot.StartLootingEntity(this, true);
		msg.player.inventory.loot.AddContainer(this.inventory.containerMain);
		msg.player.inventory.loot.AddContainer(this.inventory.containerWear);
		msg.player.inventory.loot.AddContainer(this.inventory.containerBelt);
		msg.player.inventory.loot.SendImmediate();
		msg.player.ClientRPCPlayer<string>(null, msg.player, "RPC_OpenLootPanel", "player_corpse");
	}

	// Token: 0x06000574 RID: 1396 RVA: 0x00020138 File Offset: 0x0001E338
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_Assist(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (msg.player == this)
		{
			return;
		}
		if (!this.IsWounded())
		{
			return;
		}
		this.StopWounded();
		msg.player.stats.Add("wounded_assisted", 1, global::Stats.Steam);
		this.stats.Add("wounded_healed", 1, global::Stats.Steam);
	}

	// Token: 0x06000575 RID: 1397 RVA: 0x000201A8 File Offset: 0x0001E3A8
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_KeepAlive(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (msg.player == this)
		{
			return;
		}
		if (!this.IsWounded())
		{
			return;
		}
		this.ProlongWounding(10f);
	}

	// Token: 0x06000576 RID: 1398 RVA: 0x000201E8 File Offset: 0x0001E3E8
	[global::BaseEntity.RPC_Server]
	private void SV_Drink(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		Vector3 vector = msg.read.Vector3();
		if (Vector3Ex.IsNaNOrInfinity(vector))
		{
			return;
		}
		if (!player)
		{
			return;
		}
		if (!player.metabolism.CanConsume())
		{
			return;
		}
		if (Vector3.Distance(player.transform.position, vector) > 5f)
		{
			return;
		}
		if (!global::WaterLevel.Test(vector))
		{
			return;
		}
		global::ItemDefinition atPoint = global::WaterResource.GetAtPoint(vector);
		if (atPoint == null)
		{
			return;
		}
		global::ItemModConsumable component = atPoint.GetComponent<global::ItemModConsumable>();
		global::Item item = global::ItemManager.Create(atPoint, component.amountToConsume, 0UL);
		global::ItemModConsume component2 = item.info.GetComponent<global::ItemModConsume>();
		if (component2.CanDoAction(item, player))
		{
			component2.DoAction(item, player);
		}
		if (item != null)
		{
			item.Remove(0f);
		}
		player.metabolism.MarkConsumption();
	}

	// Token: 0x06000577 RID: 1399 RVA: 0x000202CC File Offset: 0x0001E4CC
	public int GetQueuedUpdateCount(global::BasePlayer.NetworkQueue queue)
	{
		return this.networkQueue[(int)queue].Length;
	}

	// Token: 0x06000578 RID: 1400 RVA: 0x000202DC File Offset: 0x0001E4DC
	public void SendSnapshots(ListHashSet<Networkable> ents)
	{
		using (TimeWarning.New("SendSnapshots", 0.1f))
		{
			int count = ents.Values.Count;
			Networkable[] buffer = ents.Values.Buffer;
			for (int i = 0; i < count; i++)
			{
				this.SnapshotQueue.Add(buffer[i].handler as global::BaseNetworkable);
			}
		}
	}

	// Token: 0x06000579 RID: 1401 RVA: 0x00020360 File Offset: 0x0001E560
	public void QueueUpdate(global::BasePlayer.NetworkQueue queue, global::BaseNetworkable ent)
	{
		if (!this.IsConnected)
		{
			return;
		}
		if (queue == global::BasePlayer.NetworkQueue.Update)
		{
			this.networkQueue[0].Add(ent);
			return;
		}
		if (queue != global::BasePlayer.NetworkQueue.UpdateDistance)
		{
			return;
		}
		if (this.IsReceivingSnapshot)
		{
			return;
		}
		if (this.networkQueue[1].Contains(ent))
		{
			return;
		}
		if (this.networkQueue[0].Contains(ent))
		{
			return;
		}
		global::BasePlayer.NetworkQueueList networkQueueList = this.networkQueue[1];
		float num = base.Distance(ent as global::BaseEntity);
		if (num < 20f)
		{
			this.QueueUpdate(global::BasePlayer.NetworkQueue.Update, ent);
		}
		else
		{
			networkQueueList.Add(ent);
		}
	}

	// Token: 0x0600057A RID: 1402 RVA: 0x00020404 File Offset: 0x0001E604
	public void SendEntityUpdate()
	{
		using (TimeWarning.New("SendEntityUpdate", 0.1f))
		{
			this.SendEntityUpdates(this.SnapshotQueue);
			this.SendEntityUpdates(this.networkQueue[0]);
			this.SendEntityUpdates(this.networkQueue[1]);
		}
	}

	// Token: 0x0600057B RID: 1403 RVA: 0x0002046C File Offset: 0x0001E66C
	public void ClearEntityQueue(Group group = null)
	{
		this.SnapshotQueue.Clear(group);
		this.networkQueue[0].Clear(group);
		this.networkQueue[1].Clear(group);
	}

	// Token: 0x0600057C RID: 1404 RVA: 0x00020498 File Offset: 0x0001E698
	private void SendEntityUpdates(global::BasePlayer.NetworkQueueList queue)
	{
		if (queue.queueInternal.Count == 0)
		{
			return;
		}
		int num = (!this.IsReceivingSnapshot) ? ConVar.Server.updatebatch : ConVar.Server.updatebatchspawn;
		List<global::BaseNetworkable> list = Facepunch.Pool.GetList<global::BaseNetworkable>();
		using (TimeWarning.New("SendEntityUpdates.SendEntityUpdates", 0.1f))
		{
			int num2 = 0;
			foreach (global::BaseNetworkable baseNetworkable in queue.queueInternal)
			{
				this.SendEntitySnapshot(baseNetworkable);
				list.Add(baseNetworkable);
				num2++;
				if (num2 > num)
				{
					break;
				}
			}
		}
		if (num > queue.queueInternal.Count)
		{
			queue.queueInternal.Clear();
		}
		else
		{
			using (TimeWarning.New("SendEntityUpdates.Remove", 0.1f))
			{
				for (int i = 0; i < list.Count; i++)
				{
					queue.queueInternal.Remove(list[i]);
				}
			}
		}
		if (queue.queueInternal.Count == 0 && queue.MaxLength > 2048)
		{
			queue.queueInternal.Clear();
			queue.queueInternal = new HashSet<global::BaseNetworkable>();
			queue.MaxLength = 0;
		}
		Facepunch.Pool.FreeList<global::BaseNetworkable>(ref list);
	}

	// Token: 0x0600057D RID: 1405 RVA: 0x00020638 File Offset: 0x0001E838
	public void SendEntitySnapshot(global::BaseNetworkable ent)
	{
		using (TimeWarning.New("SendEntitySnapshot", 0.1f))
		{
			if (!(ent == null))
			{
				if (ent.net != null)
				{
					if (ent.ShouldNetworkTo(this))
					{
						if (Network.Net.sv.write.Start())
						{
							Connection connection = this.net.connection;
							connection.validate.entityUpdates = connection.validate.entityUpdates + 1u;
							global::BaseNetworkable.SaveInfo saveInfo = new global::BaseNetworkable.SaveInfo
							{
								forConnection = this.net.connection,
								forDisk = false
							};
							Network.Net.sv.write.PacketID(5);
							Network.Net.sv.write.UInt32(this.net.connection.validate.entityUpdates);
							ent.ToStreamForNetwork(Network.Net.sv.write, saveInfo);
							Network.Net.sv.write.Send(new SendInfo(this.net.connection));
						}
					}
				}
			}
		}
	}

	// Token: 0x0600057E RID: 1406 RVA: 0x00020764 File Offset: 0x0001E964
	public bool HasPlayerFlag(global::BasePlayer.PlayerFlags f)
	{
		return (this.playerFlags & f) == f;
	}

	// Token: 0x17000041 RID: 65
	// (get) Token: 0x0600057F RID: 1407 RVA: 0x00020774 File Offset: 0x0001E974
	public bool IsReceivingSnapshot
	{
		get
		{
			return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.ReceivingSnapshot);
		}
	}

	// Token: 0x17000042 RID: 66
	// (get) Token: 0x06000580 RID: 1408 RVA: 0x00020780 File Offset: 0x0001E980
	public bool IsAdmin
	{
		get
		{
			return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.IsAdmin);
		}
	}

	// Token: 0x17000043 RID: 67
	// (get) Token: 0x06000581 RID: 1409 RVA: 0x0002078C File Offset: 0x0001E98C
	public bool IsDeveloper
	{
		get
		{
			return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.IsDeveloper);
		}
	}

	// Token: 0x17000044 RID: 68
	// (get) Token: 0x06000582 RID: 1410 RVA: 0x0002079C File Offset: 0x0001E99C
	public bool IsAiming
	{
		get
		{
			return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.Aiming);
		}
	}

	// Token: 0x17000045 RID: 69
	// (get) Token: 0x06000583 RID: 1411 RVA: 0x000207AC File Offset: 0x0001E9AC
	public bool IsFlying
	{
		get
		{
			return this.modelState != null && this.modelState.flying;
		}
	}

	// Token: 0x17000046 RID: 70
	// (get) Token: 0x06000584 RID: 1412 RVA: 0x000207CC File Offset: 0x0001E9CC
	public bool IsConnected
	{
		get
		{
			return base.isServer && Network.Net.sv != null && this.net != null && this.net.connection != null;
		}
	}

	// Token: 0x06000585 RID: 1413 RVA: 0x00020808 File Offset: 0x0001EA08
	public void SetPlayerFlag(global::BasePlayer.PlayerFlags f, bool b)
	{
		if (b)
		{
			if (this.HasPlayerFlag(f))
			{
				return;
			}
			this.playerFlags |= f;
		}
		else
		{
			if (!this.HasPlayerFlag(f))
			{
				return;
			}
			this.playerFlags &= ~f;
		}
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000586 RID: 1414 RVA: 0x00020860 File Offset: 0x0001EA60
	public void LightToggle()
	{
		this.lightsOn = !this.lightsOn;
		this.SetLightsOn(this.lightsOn);
	}

	// Token: 0x06000587 RID: 1415 RVA: 0x00020880 File Offset: 0x0001EA80
	public void SetLightsOn(bool isOn)
	{
		global::Item activeItem = this.GetActiveItem();
		if (activeItem != null)
		{
			global::BaseEntity heldEntity = activeItem.GetHeldEntity();
			if (heldEntity != null)
			{
				global::HeldEntity component = heldEntity.GetComponent<global::HeldEntity>();
				if (component)
				{
					component.SendMessage("SetLightsOn", !component.LightsOn(), 1);
				}
			}
		}
		foreach (global::Item item in this.inventory.containerWear.itemList)
		{
			global::ItemModWearable component2 = item.info.GetComponent<global::ItemModWearable>();
			if (component2 && component2.emissive)
			{
				item.SetFlag(global::Item.Flag.IsOn, !item.HasFlag(global::Item.Flag.IsOn));
				item.MarkDirty();
			}
		}
		if (this.isMounted)
		{
			this.GetMounted().SendMessage("LightToggle", 1);
		}
	}

	// Token: 0x06000588 RID: 1416 RVA: 0x00020984 File Offset: 0x0001EB84
	public global::HeldEntity GetHeldEntity()
	{
		if (!base.isServer)
		{
			return null;
		}
		global::Item activeItem = this.GetActiveItem();
		if (activeItem == null)
		{
			return null;
		}
		return activeItem.GetHeldEntity() as global::HeldEntity;
	}

	// Token: 0x06000589 RID: 1417 RVA: 0x000209B8 File Offset: 0x0001EBB8
	public bool IsHoldingEntity<T>()
	{
		global::HeldEntity heldEntity = this.GetHeldEntity();
		return !(heldEntity == null) && heldEntity is T;
	}

	// Token: 0x0600058A RID: 1418 RVA: 0x000209E4 File Offset: 0x0001EBE4
	private void UpdateModelState()
	{
		if (this.IsDead())
		{
			return;
		}
		if (this.IsSpectating())
		{
			return;
		}
		this.wantsSendModelState = true;
	}

	// Token: 0x0600058B RID: 1419 RVA: 0x00020A08 File Offset: 0x0001EC08
	private void SendModelState()
	{
		if (!this.wantsSendModelState)
		{
			return;
		}
		if (this.nextModelStateUpdate > UnityEngine.Time.time)
		{
			return;
		}
		this.wantsSendModelState = false;
		this.nextModelStateUpdate = UnityEngine.Time.time + 0.1f;
		if (this.IsDead())
		{
			return;
		}
		if (this.IsSpectating())
		{
			return;
		}
		this.modelState.sleeping = this.IsSleeping();
		this.modelState.sitting = this.isMounted;
		this.modelState.relaxed = this.IsRelaxed();
		base.ClientRPC<ModelState>(null, "OnModelState", this.modelState);
	}

	// Token: 0x17000047 RID: 71
	// (get) Token: 0x0600058C RID: 1420 RVA: 0x00020AA8 File Offset: 0x0001ECA8
	public bool isMounted
	{
		get
		{
			return this.mounted.IsValid(base.isServer);
		}
	}

	// Token: 0x0600058D RID: 1421 RVA: 0x00020ABC File Offset: 0x0001ECBC
	public global::BaseMountable GetMounted()
	{
		return this.mounted.Get(base.isServer) as global::BaseMountable;
	}

	// Token: 0x0600058E RID: 1422 RVA: 0x00020AD4 File Offset: 0x0001ECD4
	public void MountObject(global::BaseMountable mount, int desiredSeat = 0)
	{
		this.mounted.Set(mount);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x0600058F RID: 1423 RVA: 0x00020AEC File Offset: 0x0001ECEC
	public void EnsureDismounted()
	{
		if (this.isMounted)
		{
			this.GetMounted().DismountPlayer(this);
		}
	}

	// Token: 0x06000590 RID: 1424 RVA: 0x00020B08 File Offset: 0x0001ED08
	public void DismountObject()
	{
		this.mounted.Set(null);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000591 RID: 1425 RVA: 0x00020B20 File Offset: 0x0001ED20
	public bool IsSleeping()
	{
		return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.Sleeping);
	}

	// Token: 0x06000592 RID: 1426 RVA: 0x00020B2C File Offset: 0x0001ED2C
	public bool IsSpectating()
	{
		return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.Spectating);
	}

	// Token: 0x06000593 RID: 1427 RVA: 0x00020B38 File Offset: 0x0001ED38
	public bool IsRelaxed()
	{
		return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.Relaxed);
	}

	// Token: 0x06000594 RID: 1428 RVA: 0x00020B48 File Offset: 0x0001ED48
	public bool CanBuild()
	{
		global::BuildingPrivlidge buildingPrivilege = this.GetBuildingPrivilege();
		return buildingPrivilege == null || buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x06000595 RID: 1429 RVA: 0x00020B74 File Offset: 0x0001ED74
	public bool CanBuild(Vector3 position, Quaternion rotation, Bounds bounds)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(new OBB(position, rotation, bounds));
		return buildingPrivilege == null || buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x06000596 RID: 1430 RVA: 0x00020BA8 File Offset: 0x0001EDA8
	public bool CanBuild(OBB obb)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(obb);
		return buildingPrivilege == null || buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x06000597 RID: 1431 RVA: 0x00020BD4 File Offset: 0x0001EDD4
	public bool IsBuildingBlocked()
	{
		global::BuildingPrivlidge buildingPrivilege = this.GetBuildingPrivilege();
		return !(buildingPrivilege == null) && !buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x06000598 RID: 1432 RVA: 0x00020C00 File Offset: 0x0001EE00
	public bool IsBuildingBlocked(Vector3 position, Quaternion rotation, Bounds bounds)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(new OBB(position, rotation, bounds));
		return !(buildingPrivilege == null) && !buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x06000599 RID: 1433 RVA: 0x00020C34 File Offset: 0x0001EE34
	public bool IsBuildingBlocked(OBB obb)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(obb);
		return !(buildingPrivilege == null) && !buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x0600059A RID: 1434 RVA: 0x00020C64 File Offset: 0x0001EE64
	public bool IsBuildingAuthed()
	{
		global::BuildingPrivlidge buildingPrivilege = this.GetBuildingPrivilege();
		return !(buildingPrivilege == null) && buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x0600059B RID: 1435 RVA: 0x00020C90 File Offset: 0x0001EE90
	public bool IsBuildingAuthed(Vector3 position, Quaternion rotation, Bounds bounds)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(new OBB(position, rotation, bounds));
		return !(buildingPrivilege == null) && buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x0600059C RID: 1436 RVA: 0x00020CC4 File Offset: 0x0001EEC4
	public bool IsBuildingAuthed(OBB obb)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(obb);
		return !(buildingPrivilege == null) && buildingPrivilege.IsAuthed(this);
	}

	// Token: 0x0600059D RID: 1437 RVA: 0x00020CF0 File Offset: 0x0001EEF0
	public bool CanPlaceBuildingPrivilege()
	{
		global::BuildingPrivlidge buildingPrivilege = this.GetBuildingPrivilege();
		return buildingPrivilege == null;
	}

	// Token: 0x0600059E RID: 1438 RVA: 0x00020D0C File Offset: 0x0001EF0C
	public bool CanPlaceBuildingPrivilege(Vector3 position, Quaternion rotation, Bounds bounds)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(new OBB(position, rotation, bounds));
		return buildingPrivilege == null;
	}

	// Token: 0x0600059F RID: 1439 RVA: 0x00020D30 File Offset: 0x0001EF30
	public bool CanPlaceBuildingPrivilege(OBB obb)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(obb);
		return buildingPrivilege == null;
	}

	// Token: 0x060005A0 RID: 1440 RVA: 0x00020D4C File Offset: 0x0001EF4C
	public bool IsNearEnemyBase()
	{
		global::BuildingPrivlidge buildingPrivilege = this.GetBuildingPrivilege();
		return !(buildingPrivilege == null) && !buildingPrivilege.IsAuthed(this) && buildingPrivilege.AnyAuthed();
	}

	// Token: 0x060005A1 RID: 1441 RVA: 0x00020D84 File Offset: 0x0001EF84
	public bool IsNearEnemyBase(Vector3 position, Quaternion rotation, Bounds bounds)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(new OBB(position, rotation, bounds));
		return !(buildingPrivilege == null) && !buildingPrivilege.IsAuthed(this) && buildingPrivilege.AnyAuthed();
	}

	// Token: 0x060005A2 RID: 1442 RVA: 0x00020DC4 File Offset: 0x0001EFC4
	public bool IsNearEnemyBase(OBB obb)
	{
		global::BuildingPrivlidge buildingPrivilege = base.GetBuildingPrivilege(obb);
		return !(buildingPrivilege == null) && !buildingPrivilege.IsAuthed(this) && buildingPrivilege.AnyAuthed();
	}

	// Token: 0x060005A3 RID: 1443 RVA: 0x00020DFC File Offset: 0x0001EFFC
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	public void OnProjectileAttack(global::BaseEntity.RPCMessage msg)
	{
		PlayerProjectileAttack playerProjectileAttack = PlayerProjectileAttack.Deserialize(msg.read);
		if (playerProjectileAttack == null)
		{
			return;
		}
		PlayerAttack playerAttack = playerProjectileAttack.playerAttack;
		global::HitInfo hitInfo = new global::HitInfo();
		hitInfo.LoadFromAttack(playerAttack.attack, true);
		hitInfo.Initiator = this;
		hitInfo.ProjectileID = playerAttack.projectileID;
		hitInfo.ProjectileDistance = playerProjectileAttack.hitDistance;
		hitInfo.ProjectileVelocity = playerProjectileAttack.hitVelocity;
		hitInfo.Predicted = msg.connection;
		if (hitInfo.IsNaNOrInfinity())
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Contains NaN (" + playerAttack.projectileID + ")");
			playerProjectileAttack.ResetToPool();
			this.stats.combat.Log(hitInfo, "projectile_nan");
			return;
		}
		global::BasePlayer.FiredProjectile value;
		if (!this.firedProjectiles.TryGetValue(playerAttack.projectileID, out value))
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Missing ID (" + playerAttack.projectileID + ")");
			playerProjectileAttack.ResetToPool();
			this.stats.combat.Log(hitInfo, "projectile_invalid");
			return;
		}
		if (value.integrity <= 0f)
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Integrity is zero (" + playerAttack.projectileID + ")");
			playerProjectileAttack.ResetToPool();
			this.stats.combat.Log(hitInfo, "projectile_integrity");
			return;
		}
		if (value.firedTime < UnityEngine.Time.realtimeSinceStartup - 8f)
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Lifetime is zero (" + playerAttack.projectileID + ")");
			playerProjectileAttack.ResetToPool();
			this.stats.combat.Log(hitInfo, "projectile_lifetime");
			return;
		}
		hitInfo.Weapon = value.weaponSource;
		hitInfo.WeaponPrefab = value.weaponPrefab;
		hitInfo.ProjectilePrefab = value.projectilePrefab;
		hitInfo.damageProperties = value.projectilePrefab.damageProperties;
		if (ConVar.AntiHack.projectile_protection > 0 && hitInfo.HitEntity)
		{
			bool flag = true;
			float num = 1f + ConVar.AntiHack.projectile_forgiveness;
			float projectile_clientframes = ConVar.AntiHack.projectile_clientframes;
			float projectile_serverframes = ConVar.AntiHack.projectile_serverframes;
			float num2 = Mathx.Decrement(value.firedTime);
			float num3 = Mathx.Increment(UnityEngine.Time.realtimeSinceStartup);
			float num4 = num3 - num2;
			float num5 = projectile_clientframes / 60f;
			float num6 = projectile_serverframes * Mathx.Max(UnityEngine.Time.deltaTime, UnityEngine.Time.smoothDeltaTime, UnityEngine.Time.fixedDeltaTime);
			float num7 = (this.desyncTime + num4 + num5 + num6) * num;
			if (ConVar.AntiHack.projectile_protection >= 2)
			{
				float num8 = hitInfo.HitEntity.MaxVelocity();
				float num9 = hitInfo.HitEntity.BoundsPadding() + num7 * num8;
				float num10 = hitInfo.HitEntity.Distance(hitInfo.HitPositionWorld);
				if (num10 > num9)
				{
					string name = hitInfo.ProjectilePrefab.name;
					string shortPrefabName = hitInfo.HitEntity.ShortPrefabName;
					global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, string.Concat(new object[]
					{
						"Entity too far away (",
						name,
						" on ",
						shortPrefabName,
						" with ",
						num10,
						"m > ",
						num9,
						"m in ",
						num7,
						"s)"
					}));
					this.stats.combat.Log(hitInfo, "projectile_distance");
					flag = false;
				}
			}
			if (ConVar.AntiHack.projectile_protection >= 1)
			{
				float num11 = value.itemMod.GetMaxVelocity();
				global::BaseProjectile baseProjectile = hitInfo.Weapon as global::BaseProjectile;
				if (baseProjectile)
				{
					num11 *= baseProjectile.projectileVelocityScale;
				}
				float num12 = hitInfo.ProjectilePrefab.initialDistance + num7 * num11;
				float num13 = Vector3.Distance(value.origin, hitInfo.HitPositionWorld);
				if (num13 > num12)
				{
					string name2 = hitInfo.ProjectilePrefab.name;
					string shortPrefabName2 = hitInfo.HitEntity.ShortPrefabName;
					global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, string.Concat(new object[]
					{
						"Traveled too fast (",
						name2,
						" on ",
						shortPrefabName2,
						" with ",
						num13,
						"m > ",
						num12,
						"m in ",
						num7,
						"s)"
					}));
					this.stats.combat.Log(hitInfo, "projectile_speed");
					flag = false;
				}
			}
			if (ConVar.AntiHack.projectile_protection >= 3)
			{
				Vector3 pointStart = hitInfo.PointStart;
				Vector3 vector = hitInfo.HitPositionWorld + hitInfo.HitNormalWorld.normalized * 0.001f;
				Vector3 position = value.position;
				Vector3 vector2 = pointStart;
				Vector3 vector3 = hitInfo.PositionOnRay(vector);
				Vector3 vector4 = vector;
				bool flag2 = global::GamePhysics.LineOfSight(position, vector2, vector3, vector4, 2162688, 0f);
				if (!flag2)
				{
					this.stats.Add("hit_" + hitInfo.HitEntity.Categorize() + "_indirect_los", 1, global::Stats.Server);
				}
				else
				{
					this.stats.Add("hit_" + hitInfo.HitEntity.Categorize() + "_direct_los", 1, global::Stats.Server);
				}
				if (!flag2)
				{
					string name3 = hitInfo.ProjectilePrefab.name;
					string shortPrefabName3 = hitInfo.HitEntity.ShortPrefabName;
					global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, string.Concat(new object[]
					{
						"Line of sight (",
						name3,
						" on ",
						shortPrefabName3,
						") ",
						position,
						" ",
						vector2,
						" ",
						vector3,
						" ",
						vector4
					}));
					this.stats.combat.Log(hitInfo, "projectile_los");
					flag = false;
				}
			}
			if (!flag)
			{
				global::AntiHack.AddViolation(this, global::AntiHackType.ProjectileHack, ConVar.AntiHack.projectile_penalty);
				playerProjectileAttack.ResetToPool();
				return;
			}
		}
		value.position = hitInfo.HitPositionWorld + hitInfo.ProjectileVelocity.normalized * 0.001f;
		hitInfo.ProjectilePrefab.CalculateDamage(hitInfo, value.projectileModifier, value.integrity);
		if (hitInfo.ProjectilePrefab.penetrationPower <= 0f || hitInfo.HitEntity == null)
		{
			value.integrity = 0f;
		}
		else
		{
			float num14 = hitInfo.HitEntity.PenetrationResistance(hitInfo) / hitInfo.ProjectilePrefab.penetrationPower;
			value.integrity = Mathf.Clamp01(value.integrity - num14);
		}
		value.itemMod.ServerProjectileHit(hitInfo);
		if (hitInfo.HitEntity)
		{
			this.stats.Add(value.itemMod.category + "_hit_" + hitInfo.HitEntity.Categorize(), 1, global::Stats.Steam);
		}
		if (value.integrity <= 0f && hitInfo.ProjectilePrefab.remainInWorld)
		{
			this.CreateWorldProjectile(hitInfo, value.itemDef, value.itemMod, hitInfo.ProjectilePrefab, value.pickupItem);
		}
		this.firedProjectiles[playerAttack.projectileID] = value;
		if (Interface.CallHook("OnPlayerAttack", new object[]
		{
			this,
			hitInfo
		}) != null)
		{
			return;
		}
		if (hitInfo.HitEntity)
		{
			hitInfo.HitEntity.OnAttacked(hitInfo);
		}
		global::Effect.server.ImpactEffect(hitInfo);
		playerProjectileAttack.ResetToPool();
	}

	// Token: 0x060005A4 RID: 1444 RVA: 0x000215A0 File Offset: 0x0001F7A0
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	public void OnProjectileRicochet(global::BaseEntity.RPCMessage msg)
	{
		PlayerProjectileRicochet playerProjectileRicochet = PlayerProjectileRicochet.Deserialize(msg.read);
		if (playerProjectileRicochet == null)
		{
			return;
		}
		if (Vector3Ex.IsNaNOrInfinity(playerProjectileRicochet.hitPosition) || Vector3Ex.IsNaNOrInfinity(playerProjectileRicochet.inVelocity) || Vector3Ex.IsNaNOrInfinity(playerProjectileRicochet.outVelocity))
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Contains NaN (" + playerProjectileRicochet.projectileID + ")");
			playerProjectileRicochet.ResetToPool();
			return;
		}
		global::BasePlayer.FiredProjectile value;
		if (!this.firedProjectiles.TryGetValue(playerProjectileRicochet.projectileID, out value))
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Missing ID (" + playerProjectileRicochet.projectileID + ")");
			playerProjectileRicochet.ResetToPool();
			return;
		}
		if (value.firedTime < UnityEngine.Time.realtimeSinceStartup - 8f)
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Lifetime is zero (" + playerProjectileRicochet.projectileID + ")");
			playerProjectileRicochet.ResetToPool();
			return;
		}
		Vector3 normalized = playerProjectileRicochet.inVelocity.normalized;
		Vector3 normalized2 = playerProjectileRicochet.outVelocity.normalized;
		if (ConVar.AntiHack.projectile_protection >= 3)
		{
			Vector3 position = value.position;
			Vector3 vector = playerProjectileRicochet.hitPosition - normalized * 0.001f;
			Vector3 p = playerProjectileRicochet.hitPosition + normalized2 * 0.001f;
			if (!global::GamePhysics.LineOfSight(position, vector, p, 2162688, 0f))
			{
				string name = value.projectilePrefab.name;
				global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, string.Concat(new object[]
				{
					"Line of sight (",
					name,
					" on ricochet) ",
					position,
					" ",
					vector
				}));
				playerProjectileRicochet.ResetToPool();
				return;
			}
		}
		value.position = playerProjectileRicochet.hitPosition + normalized2 * 0.001f;
		this.firedProjectiles[playerProjectileRicochet.projectileID] = value;
		playerProjectileRicochet.ResetToPool();
	}

	// Token: 0x060005A5 RID: 1445 RVA: 0x000217AC File Offset: 0x0001F9AC
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	public void OnProjectileUpdate(global::BaseEntity.RPCMessage msg)
	{
		PlayerProjectileUpdate playerProjectileUpdate = PlayerProjectileUpdate.Deserialize(msg.read);
		if (playerProjectileUpdate == null)
		{
			return;
		}
		if (Vector3Ex.IsNaNOrInfinity(playerProjectileUpdate.curPosition))
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Contains NaN (" + playerProjectileUpdate.projectileID + ")");
			playerProjectileUpdate.ResetToPool();
			return;
		}
		global::BasePlayer.FiredProjectile value;
		if (!this.firedProjectiles.TryGetValue(playerProjectileUpdate.projectileID, out value))
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Missing ID (" + playerProjectileUpdate.projectileID + ")");
			playerProjectileUpdate.ResetToPool();
			return;
		}
		if (value.firedTime < UnityEngine.Time.realtimeSinceStartup - 8f)
		{
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Lifetime is zero (" + playerProjectileUpdate.projectileID + ")");
			playerProjectileUpdate.ResetToPool();
			return;
		}
		if (ConVar.AntiHack.projectile_protection >= 3)
		{
			Vector3 position = value.position;
			Vector3 curPosition = playerProjectileUpdate.curPosition;
			if (!global::GamePhysics.LineOfSight(position, curPosition, 2162688, 0f))
			{
				string name = value.projectilePrefab.name;
				global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, string.Concat(new object[]
				{
					"Line of sight (",
					name,
					" on update) ",
					position,
					" ",
					curPosition
				}));
				playerProjectileUpdate.ResetToPool();
				return;
			}
		}
		value.position = playerProjectileUpdate.curPosition;
		this.firedProjectiles[playerProjectileUpdate.projectileID] = value;
		playerProjectileUpdate.ResetToPool();
	}

	// Token: 0x060005A6 RID: 1446 RVA: 0x00021940 File Offset: 0x0001FB40
	protected virtual void CreateWorldProjectile(global::HitInfo info, global::ItemDefinition itemDef, global::ItemModProjectile itemMod, global::Projectile projectilePrefab, global::Item recycleItem)
	{
		if (Interface.CallHook("CanCreateWorldProjectile", new object[]
		{
			info,
			itemDef
		}) != null)
		{
			return;
		}
		Vector3 projectileVelocity = info.ProjectileVelocity;
		global::Item item = (recycleItem == null) ? global::ItemManager.Create(itemDef, 1, 0UL) : recycleItem;
		global::BaseEntity baseEntity = item.CreateWorldObject(info.HitPositionWorld, Quaternion.LookRotation(projectileVelocity.normalized));
		Rigidbody component = baseEntity.GetComponent<Rigidbody>();
		if (Interface.CallHook("OnCreateWorldProjectile", new object[]
		{
			info,
			item
		}) != null)
		{
			return;
		}
		if (!info.DidHit)
		{
			baseEntity.Kill(global::BaseNetworkable.DestroyMode.Gib);
			return;
		}
		if (projectilePrefab.breakProbability > 0f && Random.value <= projectilePrefab.breakProbability)
		{
			baseEntity.Kill(global::BaseNetworkable.DestroyMode.Gib);
			return;
		}
		if (projectilePrefab.conditionLoss > 0f)
		{
			item.LoseCondition(projectilePrefab.conditionLoss * 100f);
			if (item.isBroken)
			{
				baseEntity.Kill(global::BaseNetworkable.DestroyMode.Gib);
				return;
			}
		}
		if (projectilePrefab.stickProbability <= 0f || Random.value > projectilePrefab.stickProbability)
		{
			component.AddForce(projectileVelocity.normalized * 200f);
			component.WakeUp();
			return;
		}
		if (info.HitEntity == null)
		{
			component.isKinematic = true;
			return;
		}
		Quaternion localRotation;
		if (info.HitBone == 0u)
		{
			localRotation = Quaternion.LookRotation(info.HitEntity.transform.InverseTransformDirection(projectileVelocity.normalized));
		}
		else
		{
			localRotation = Quaternion.LookRotation(info.HitNormalLocal * -1f);
		}
		component.isKinematic = true;
		baseEntity.SetParent(info.HitEntity, info.HitBone);
		baseEntity.transform.localPosition = info.HitPositionLocal;
		baseEntity.transform.localRotation = localRotation;
	}

	// Token: 0x060005A7 RID: 1447 RVA: 0x00021B1C File Offset: 0x0001FD1C
	public void CleanupExpiredProjectiles()
	{
		foreach (KeyValuePair<int, global::BasePlayer.FiredProjectile> keyValuePair in (from x in this.firedProjectiles
		where x.Value.firedTime < UnityEngine.Time.realtimeSinceStartup - 8f - 1f
		select x).ToList<KeyValuePair<int, global::BasePlayer.FiredProjectile>>())
		{
			this.firedProjectiles.Remove(keyValuePair.Key);
		}
	}

	// Token: 0x060005A8 RID: 1448 RVA: 0x00021BAC File Offset: 0x0001FDAC
	public bool HasFiredProjectile(int id)
	{
		return this.firedProjectiles.ContainsKey(id);
	}

	// Token: 0x060005A9 RID: 1449 RVA: 0x00021BBC File Offset: 0x0001FDBC
	public void NoteFiredProjectile(int projectileid, Vector3 startPos, Vector3 startVel, global::AttackEntity attackEnt, global::ItemDefinition firedItemDef, global::Item pickupItem = null)
	{
		global::BaseProjectile baseProjectile = attackEnt as global::BaseProjectile;
		global::ItemModProjectile component = firedItemDef.GetComponent<global::ItemModProjectile>();
		global::Projectile component2 = component.projectileObject.Get().GetComponent<global::Projectile>();
		if (Vector3Ex.IsNaNOrInfinity(startPos) || Vector3Ex.IsNaNOrInfinity(startVel))
		{
			string name = component2.name;
			global::AntiHack.Log(this, global::AntiHackType.ProjectileHack, "Contains NaN (" + name + ")");
			this.stats.combat.Log(baseProjectile, "projectile_nan");
			return;
		}
		global::BasePlayer.FiredProjectile value = new global::BasePlayer.FiredProjectile
		{
			itemDef = firedItemDef,
			itemMod = component,
			projectilePrefab = component2,
			firedTime = UnityEngine.Time.realtimeSinceStartup,
			weaponSource = attackEnt,
			weaponPrefab = ((!(attackEnt == null)) ? global::GameManager.server.FindPrefab(global::StringPool.Get(attackEnt.prefabID)).GetComponent<global::AttackEntity>() : null),
			projectileModifier = ((!(baseProjectile == null)) ? baseProjectile.GetProjectileModifier() : global::Projectile.Modifier.Default),
			pickupItem = pickupItem,
			integrity = 1f,
			origin = startPos,
			position = startPos,
			velocity = startVel
		};
		this.firedProjectiles.Add(projectileid, value);
	}

	// Token: 0x060005AA RID: 1450 RVA: 0x00021D04 File Offset: 0x0001FF04
	public override bool CanUseNetworkCache(Connection connection)
	{
		return this.net == null || this.net.connection != connection;
	}

	// Token: 0x060005AB RID: 1451 RVA: 0x00021D28 File Offset: 0x0001FF28
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		bool flag = this.net != null && this.net.connection == info.forConnection;
		info.msg.basePlayer = Facepunch.Pool.Get<ProtoBuf.BasePlayer>();
		info.msg.basePlayer.userid = this.userID;
		info.msg.basePlayer.name = this.displayName;
		info.msg.basePlayer.playerFlags = (int)this.playerFlags;
		info.msg.basePlayer.heldEntity = this.svActiveItemID;
		if (this.IsConnected && (this.IsAdmin || this.IsDeveloper))
		{
			info.msg.basePlayer.skinCol = this.net.connection.info.GetFloat("global.skincol", -1f);
			info.msg.basePlayer.skinTex = this.net.connection.info.GetFloat("global.skintex", -1f);
			info.msg.basePlayer.skinMesh = this.net.connection.info.GetFloat("global.skinmesh", -1f);
		}
		else
		{
			info.msg.basePlayer.skinCol = -1f;
			info.msg.basePlayer.skinTex = -1f;
			info.msg.basePlayer.skinMesh = -1f;
		}
		if (info.forDisk || flag)
		{
			info.msg.basePlayer.metabolism = this.metabolism.Save();
		}
		if (!info.forDisk && !flag)
		{
			info.msg.basePlayer.playerFlags &= -5;
			info.msg.basePlayer.playerFlags &= -129;
		}
		info.msg.basePlayer.inventory = this.inventory.Save(info.forDisk || flag);
		this.modelState.sleeping = this.IsSleeping();
		this.modelState.relaxed = this.IsRelaxed();
		info.msg.basePlayer.modelState = this.modelState.Copy();
		if (!info.forDisk)
		{
			info.msg.basePlayer.mounted = this.mounted.uid;
		}
		if (flag)
		{
			info.msg.basePlayer.persistantData = SingletonComponent<global::ServerMgr>.Instance.persistance.GetPlayerInfo(this.userID);
		}
		if (info.forDisk)
		{
			info.msg.basePlayer.currentLife = this.lifeStory;
			info.msg.basePlayer.previousLife = this.previousLifeStory;
		}
	}

	// Token: 0x060005AC RID: 1452 RVA: 0x00022034 File Offset: 0x00020234
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.basePlayer != null)
		{
			ProtoBuf.BasePlayer basePlayer = info.msg.basePlayer;
			this.userID = basePlayer.userid;
			this.UserIDString = this.userID.ToString();
			if (basePlayer.name != null)
			{
				this._displayName = basePlayer.name;
				if (string.IsNullOrEmpty(this._displayName.Trim()))
				{
					this._displayName = "Blaster :D";
				}
			}
			this.playerFlags = (global::BasePlayer.PlayerFlags)basePlayer.playerFlags;
			if (basePlayer.metabolism != null)
			{
				this.metabolism.Load(basePlayer.metabolism);
			}
			if (basePlayer.inventory != null)
			{
				this.inventory.Load(basePlayer.inventory);
			}
			if (basePlayer.modelState != null)
			{
				if (this.modelState != null)
				{
					this.modelState.ResetToPool();
					this.modelState = null;
				}
				this.modelState = basePlayer.modelState;
				basePlayer.modelState = null;
			}
		}
		if (info.fromDisk)
		{
			this.lifeStory = info.msg.basePlayer.currentLife;
			if (this.lifeStory != null)
			{
				this.lifeStory.ShouldPool = false;
			}
			this.previousLifeStory = info.msg.basePlayer.previousLife;
			if (this.previousLifeStory != null)
			{
				this.previousLifeStory.ShouldPool = false;
			}
			this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Sleeping, false);
			this.StartSleeping();
			this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Connected, false);
			if (this.lifeStory == null && this.IsAlive())
			{
				this.LifeStoryStart();
			}
		}
	}

	// Token: 0x17000048 RID: 72
	// (get) Token: 0x060005AD RID: 1453 RVA: 0x000221E0 File Offset: 0x000203E0
	public virtual global::BaseNpc.AiStatistics.FamilyEnum Family
	{
		get
		{
			return global::BaseNpc.AiStatistics.FamilyEnum.Player;
		}
	}

	// Token: 0x17000049 RID: 73
	// (get) Token: 0x060005AE RID: 1454 RVA: 0x000221E4 File Offset: 0x000203E4
	protected override float PositionTickRate
	{
		get
		{
			return -1f;
		}
	}

	// Token: 0x060005AF RID: 1455 RVA: 0x000221EC File Offset: 0x000203EC
	public bool CanSuicide()
	{
		return this.IsAdmin || this.IsDeveloper || UnityEngine.Time.realtimeSinceStartup > this.nextSuicideTime;
	}

	// Token: 0x060005B0 RID: 1456 RVA: 0x00022214 File Offset: 0x00020414
	public void MarkSuicide()
	{
		this.nextSuicideTime = UnityEngine.Time.realtimeSinceStartup + 60f;
	}

	// Token: 0x060005B1 RID: 1457 RVA: 0x00022228 File Offset: 0x00020428
	public global::Item GetActiveItem()
	{
		if (this.svActiveItemID == 0u)
		{
			return null;
		}
		if (this.IsDead())
		{
			return null;
		}
		if (this.inventory == null || this.inventory.containerBelt == null)
		{
			return null;
		}
		return this.inventory.containerBelt.FindItemByUID(this.svActiveItemID);
	}

	// Token: 0x060005B2 RID: 1458 RVA: 0x00022288 File Offset: 0x00020488
	public void MovePosition(Vector3 newPos)
	{
		base.transform.position = newPos;
		this.tickInterpolator.Reset(newPos);
		base.NetworkPositionTick();
	}

	// Token: 0x1700004A RID: 74
	// (get) Token: 0x060005B3 RID: 1459 RVA: 0x000222A8 File Offset: 0x000204A8
	// (set) Token: 0x060005B4 RID: 1460 RVA: 0x000222B0 File Offset: 0x000204B0
	public Vector3 estimatedVelocity { get; private set; }

	// Token: 0x1700004B RID: 75
	// (get) Token: 0x060005B5 RID: 1461 RVA: 0x000222BC File Offset: 0x000204BC
	// (set) Token: 0x060005B6 RID: 1462 RVA: 0x000222C4 File Offset: 0x000204C4
	public float estimatedSpeed { get; private set; }

	// Token: 0x1700004C RID: 76
	// (get) Token: 0x060005B7 RID: 1463 RVA: 0x000222D0 File Offset: 0x000204D0
	// (set) Token: 0x060005B8 RID: 1464 RVA: 0x000222D8 File Offset: 0x000204D8
	public float estimatedSpeed2D { get; private set; }

	// Token: 0x1700004D RID: 77
	// (get) Token: 0x060005B9 RID: 1465 RVA: 0x000222E4 File Offset: 0x000204E4
	// (set) Token: 0x060005BA RID: 1466 RVA: 0x000222EC File Offset: 0x000204EC
	public int secondsConnected { get; private set; }

	// Token: 0x1700004E RID: 78
	// (get) Token: 0x060005BB RID: 1467 RVA: 0x000222F8 File Offset: 0x000204F8
	// (set) Token: 0x060005BC RID: 1468 RVA: 0x00022300 File Offset: 0x00020500
	public float desyncTime { get; private set; }

	// Token: 0x060005BD RID: 1469 RVA: 0x0002230C File Offset: 0x0002050C
	public void OverrideViewAngles(Vector3 newAng)
	{
		this.viewAngles = newAng;
	}

	// Token: 0x060005BE RID: 1470 RVA: 0x00022318 File Offset: 0x00020518
	public override void ServerInit()
	{
		this.stats = new global::PlayerStatistics(this);
		if (this.userID == 0UL)
		{
			this.userID = (ulong)((long)Random.Range(0, 10000000));
			this.UserIDString = this.userID.ToString();
		}
		this.UpdatePlayerCollider(true);
		this.UpdatePlayerRigidbody(!this.IsSleeping());
		base.ServerInit();
		global::BaseEntity.Query.Server.AddPlayer(this);
		this.inventory.ServerInit(this);
		this.metabolism.ServerInit(this);
	}

	// Token: 0x060005BF RID: 1471 RVA: 0x000223A8 File Offset: 0x000205A8
	internal override void DoServerDestroy()
	{
		base.DoServerDestroy();
		global::BaseEntity.Query.Server.RemovePlayer(this);
		if (this.inventory)
		{
			this.inventory.DoDestroy();
		}
		global::BasePlayer.sleepingPlayerList.Remove(this);
	}

	// Token: 0x060005C0 RID: 1472 RVA: 0x000223E4 File Offset: 0x000205E4
	protected void ServerUpdate(float deltaTime)
	{
		if (!Network.Net.sv.IsConnected())
		{
			return;
		}
		this.LifeStoryUpdate(deltaTime);
		this.FinalizeTick(deltaTime);
		this.desyncTime = Mathf.Max(this.timeSinceLastTick - deltaTime, 0f);
		if (UnityEngine.Time.realtimeSinceStartup < this.lastPlayerTick + 0.0625f)
		{
			return;
		}
		if (this.lastPlayerTick < UnityEngine.Time.realtimeSinceStartup - 6.25f)
		{
			this.lastPlayerTick = UnityEngine.Time.realtimeSinceStartup - Random.Range(0f, 0.0625f);
		}
		while (this.lastPlayerTick < UnityEngine.Time.realtimeSinceStartup)
		{
			this.lastPlayerTick += 0.0625f;
		}
		if (this.IsConnected)
		{
			this.ConnectedPlayerUpdate(0.0625f);
		}
	}

	// Token: 0x060005C1 RID: 1473 RVA: 0x000224AC File Offset: 0x000206AC
	private void ConnectedPlayerUpdate(float deltaTime)
	{
		this.SendEntityUpdate();
		if (this.IsReceivingSnapshot)
		{
			if (this.SnapshotQueue.Length == 0 && global::EACServer.IsAuthenticated(this.net.connection))
			{
				this.EnterGame();
			}
			return;
		}
		if (this.IsAlive())
		{
			this.metabolism.ServerUpdate(this, deltaTime);
			if (this.timeSinceLastTick > (float)ConVar.Server.playertimeout)
			{
				this.lastTickTime = 0f;
				this.Kick("Unresponsive");
				return;
			}
		}
		int num = (int)this.net.connection.GetSecondsConnected();
		int num2 = num - this.secondsConnected;
		if (num2 > 0)
		{
			this.stats.Add("time", num2, global::Stats.Server);
			this.secondsConnected = num;
		}
		this.SendModelState();
	}

	// Token: 0x060005C2 RID: 1474 RVA: 0x00022578 File Offset: 0x00020778
	private void EnterGame()
	{
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.ReceivingSnapshot, false);
		base.ClientRPCPlayer(null, this, "FinishLoading");
		if (this.net != null)
		{
			global::EACServer.OnFinishLoading(this.net.connection);
		}
		Debug.LogFormat("{0} has entered the game", new object[]
		{
			this
		});
	}

	// Token: 0x060005C3 RID: 1475 RVA: 0x000225CC File Offset: 0x000207CC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.FromOwner]
	private void ClientKeepConnectionAlive(global::BaseEntity.RPCMessage msg)
	{
		this.lastTickTime = UnityEngine.Time.time;
	}

	// Token: 0x060005C4 RID: 1476 RVA: 0x000225DC File Offset: 0x000207DC
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	private void ClientLoadingComplete(global::BaseEntity.RPCMessage msg)
	{
	}

	// Token: 0x060005C5 RID: 1477 RVA: 0x000225E0 File Offset: 0x000207E0
	public void PlayerInit(Connection c)
	{
		using (TimeWarning.New("PlayerInit", 10L))
		{
			base.CancelInvoke(new Action(base.KillMessage));
			this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Connected, true);
			global::BasePlayer.activePlayerList.Add(this);
			this.userID = c.userid;
			this.UserIDString = this.userID.ToString();
			this._displayName = StringEx.ToPrintable(c.username, 32);
			c.player = this;
			this.tickInterpolator.Reset(base.transform.position);
			this.lastTickTime = 0f;
			this.lastInputTime = 0f;
			this.SetPlayerFlag(global::BasePlayer.PlayerFlags.ReceivingSnapshot, true);
			this.stats.Init();
			this.previousLifeStory = SingletonComponent<global::ServerMgr>.Instance.persistance.GetLastLifeStory(this.userID);
			this.SetPlayerFlag(global::BasePlayer.PlayerFlags.IsAdmin, c.authLevel > 0u);
			this.SetPlayerFlag(global::BasePlayer.PlayerFlags.IsDeveloper, global::DeveloperList.IsDeveloper(this));
			if (this.IsDead() && this.net.SwitchGroup(global::BaseNetworkable.LimboNetworkGroup))
			{
				base.SendNetworkGroupChange();
			}
			this.net.OnConnected(c);
			this.net.StartSubscriber();
			base.SendAsSnapshot(this.net.connection, false);
			base.ClientRPCPlayer(null, this, "StartLoading");
			if (this.net != null)
			{
				global::EACServer.OnStartLoading(this.net.connection);
			}
			this.SendGlobalSnapshot();
			this.SendFullSnapshot();
			if (this.IsAdmin)
			{
				if (ConVar.AntiHack.noclip_protection <= 0)
				{
					this.ChatMessage("antihack.noclip_protection is disabled!");
				}
				if (ConVar.AntiHack.speedhack_protection <= 0)
				{
					this.ChatMessage("antihack.speedhack_protection is disabled!");
				}
				if (ConVar.AntiHack.flyhack_protection <= 0)
				{
					this.ChatMessage("antihack.flyhack_protection is disabled!");
				}
				if (ConVar.AntiHack.projectile_protection <= 0)
				{
					this.ChatMessage("antihack.projectile_protection is disabled!");
				}
				if (ConVar.AntiHack.melee_protection <= 0)
				{
					this.ChatMessage("antihack.melee_protection is disabled!");
				}
				if (ConVar.AntiHack.eye_protection <= 0)
				{
					this.ChatMessage("antihack.eye_protection is disabled!");
				}
			}
			Interface.CallHook("OnPlayerInit", new object[]
			{
				this
			});
		}
	}

	// Token: 0x060005C6 RID: 1478 RVA: 0x00022834 File Offset: 0x00020A34
	public void SendDeathInformation()
	{
		base.ClientRPCPlayer(null, this, "OnDied");
	}

	// Token: 0x060005C7 RID: 1479 RVA: 0x00022844 File Offset: 0x00020A44
	public void SendRespawnOptions()
	{
		using (RespawnInformation respawnInformation = Facepunch.Pool.Get<RespawnInformation>())
		{
			respawnInformation.spawnOptions = Facepunch.Pool.Get<List<RespawnInformation.SpawnOptions>>();
			foreach (global::SleepingBag sleepingBag in global::SleepingBag.FindForPlayer(this.userID, true))
			{
				RespawnInformation.SpawnOptions spawnOptions = Facepunch.Pool.Get<RespawnInformation.SpawnOptions>();
				spawnOptions.id = sleepingBag.net.ID;
				spawnOptions.name = sleepingBag.niceName;
				spawnOptions.type = 1;
				spawnOptions.unlockSeconds = sleepingBag.unlockSeconds;
				respawnInformation.spawnOptions.Add(spawnOptions);
			}
			respawnInformation.previousLife = this.previousLifeStory;
			respawnInformation.fadeIn = (this.previousLifeStory != null && (ulong)this.previousLifeStory.timeDied > (ulong)((long)(Epoch.Current - 5)));
			base.ClientRPCPlayer<RespawnInformation>(null, this, "OnRespawnInformation", respawnInformation);
		}
	}

	// Token: 0x060005C8 RID: 1480 RVA: 0x00022938 File Offset: 0x00020B38
	public virtual void StartSleeping()
	{
		if (this.IsSleeping())
		{
			return;
		}
		if (Interface.CallHook("OnPlayerSleep", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		this.EnsureDismounted();
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Sleeping, true);
		if (!global::BasePlayer.sleepingPlayerList.Contains(this))
		{
			global::BasePlayer.sleepingPlayerList.Add(this);
		}
		base.CancelInvoke(new Action(this.InventoryUpdate));
		this.inventory.loot.Clear();
		this.inventory.crafting.CancelAll(true);
		this.UpdatePlayerCollider(true);
		this.UpdatePlayerRigidbody(false);
	}

	// Token: 0x060005C9 RID: 1481 RVA: 0x000229D8 File Offset: 0x00020BD8
	public void DelayedRigidbodyDisable()
	{
		this.UpdatePlayerRigidbody(false);
	}

	// Token: 0x060005CA RID: 1482 RVA: 0x000229E4 File Offset: 0x00020BE4
	public virtual void EndSleeping()
	{
		if (!this.IsSleeping())
		{
			return;
		}
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Sleeping, false);
		global::BasePlayer.sleepingPlayerList.Remove(this);
		base.InvokeRepeating(new Action(this.InventoryUpdate), 1f, 0.1f * Random.Range(0.99f, 1.01f));
		this.UpdatePlayerCollider(true);
		this.UpdatePlayerRigidbody(true);
		Interface.CallHook("OnPlayerSleepEnded", new object[]
		{
			this
		});
	}

	// Token: 0x060005CB RID: 1483 RVA: 0x00022A64 File Offset: 0x00020C64
	public virtual void EndLooting()
	{
		if (this.inventory.loot)
		{
			this.inventory.loot.Clear();
		}
	}

	// Token: 0x060005CC RID: 1484 RVA: 0x00022A8C File Offset: 0x00020C8C
	public virtual void OnDisconnected()
	{
		this.stats.Save();
		this.EndLooting();
		if (this.IsAlive() || this.IsSleeping())
		{
			this.StartSleeping();
		}
		else
		{
			base.Invoke(new Action(base.KillMessage), 0f);
		}
		global::BasePlayer.activePlayerList.Remove(this);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Connected, false);
		if (this.net != null)
		{
			this.net.OnDisconnected();
		}
		this.ResetAntiHack();
	}

	// Token: 0x060005CD RID: 1485 RVA: 0x00022B18 File Offset: 0x00020D18
	private void InventoryUpdate()
	{
		if (this.IsConnected && !this.IsDead())
		{
			this.inventory.ServerUpdate(0.1f);
		}
	}

	// Token: 0x060005CE RID: 1486 RVA: 0x00022B40 File Offset: 0x00020D40
	[global::BaseEntity.RPC_Server.FromOwner]
	[global::BaseEntity.RPC_Server]
	private void OnPlayerLanded(global::BaseEntity.RPCMessage msg)
	{
		float num = msg.read.Float();
		if (float.IsNaN(num) || float.IsInfinity(num))
		{
			return;
		}
		float num2 = Mathf.InverseLerp(-15f, -100f, num);
		if (num2 == 0f)
		{
			return;
		}
		if (Interface.CallHook("OnPlayerLand", new object[]
		{
			this,
			num2
		}) != null)
		{
			return;
		}
		this.metabolism.bleeding.Add(num2 * 0.5f);
		float num3 = num2 * 500f;
		this.Hurt(num3, Rust.DamageType.Fall, null, true);
		if (num3 > 20f && this.fallDamageEffect.isValid)
		{
			global::Effect.server.Run(this.fallDamageEffect.resourcePath, base.transform.position, Vector3.zero, null, false);
		}
		Interface.CallHook("OnPlayerLanded", new object[]
		{
			this,
			num2
		});
	}

	// Token: 0x060005CF RID: 1487 RVA: 0x00022C3C File Offset: 0x00020E3C
	private void SV_StartLootingPlayer(uint entityID)
	{
		global::BasePlayer basePlayer = global::BaseNetworkable.serverEntities.Find(entityID) as global::BasePlayer;
		if (basePlayer == null)
		{
			return;
		}
		this.inventory.loot.StartLootingPlayer(basePlayer);
	}

	// Token: 0x060005D0 RID: 1488 RVA: 0x00022C78 File Offset: 0x00020E78
	public void SendGlobalSnapshot()
	{
		using (TimeWarning.New("SendGlobalSnapshot", 10L))
		{
			this.EnterVisibility(Network.Net.sv.visibility.Get(0u));
		}
	}

	// Token: 0x060005D1 RID: 1489 RVA: 0x00022CCC File Offset: 0x00020ECC
	public void SendFullSnapshot()
	{
		using (TimeWarning.New("SendFullSnapshot", 0.1f))
		{
			foreach (Group group in this.net.subscriber.subscribed)
			{
				if (group.ID != 0u)
				{
					this.EnterVisibility(group);
				}
			}
		}
	}

	// Token: 0x060005D2 RID: 1490 RVA: 0x00022D70 File Offset: 0x00020F70
	public override Vector3 GetNetworkRotation()
	{
		return this.viewAngles;
	}

	// Token: 0x060005D3 RID: 1491 RVA: 0x00022D78 File Offset: 0x00020F78
	public override void OnNetworkGroupLeave(Group group)
	{
		base.OnNetworkGroupLeave(group);
		this.LeaveVisibility(group);
	}

	// Token: 0x060005D4 RID: 1492 RVA: 0x00022D88 File Offset: 0x00020F88
	private void LeaveVisibility(Group group)
	{
		global::ServerMgr.OnLeaveVisibility(this.net.connection, group);
		this.ClearEntityQueue(group);
	}

	// Token: 0x060005D5 RID: 1493 RVA: 0x00022DA4 File Offset: 0x00020FA4
	public override void OnNetworkGroupEnter(Group group)
	{
		base.OnNetworkGroupEnter(group);
		if (this.IsReceivingSnapshot)
		{
			return;
		}
		this.EnterVisibility(group);
	}

	// Token: 0x060005D6 RID: 1494 RVA: 0x00022DC0 File Offset: 0x00020FC0
	private void EnterVisibility(Group group)
	{
		global::ServerMgr.OnEnterVisibility(this.net.connection, group);
		this.SendSnapshots(group.networkables);
	}

	// Token: 0x060005D7 RID: 1495 RVA: 0x00022DE0 File Offset: 0x00020FE0
	public void CheckDeathCondition(global::HitInfo info = null)
	{
		Assert.IsTrue(base.isServer, "CheckDeathCondition called on client!");
		if (this.IsSpectating())
		{
			return;
		}
		if (this.IsDead())
		{
			return;
		}
		if (this.metabolism.ShouldDie())
		{
			this.Die(info);
		}
	}

	// Token: 0x060005D8 RID: 1496 RVA: 0x00022E2C File Offset: 0x0002102C
	public virtual void CreateCorpse()
	{
		using (TimeWarning.New("Create corpse", 0.1f))
		{
			global::LootableCorpse lootableCorpse = base.DropCorpse("assets/prefabs/player/player_corpse.prefab") as global::LootableCorpse;
			if (lootableCorpse)
			{
				lootableCorpse.SetFlag(global::BaseEntity.Flags.Reserved5, this.HasPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash), false);
				lootableCorpse.TakeFrom(new global::ItemContainer[]
				{
					this.inventory.containerMain,
					this.inventory.containerWear,
					this.inventory.containerBelt
				});
				lootableCorpse.playerName = this.displayName;
				lootableCorpse.playerSteamID = this.userID;
				lootableCorpse.Spawn();
				lootableCorpse.TakeChildren(this);
				global::ResourceDispenser component = lootableCorpse.GetComponent<global::ResourceDispenser>();
				int num = 2;
				if (this.lifeStory != null)
				{
					num += Mathf.Clamp(Mathf.FloorToInt(this.lifeStory.secondsAlive / 180f), 0, 20);
				}
				component.containedItems.Add(new global::ItemAmount(global::ItemManager.FindItemDefinition("fat.animal"), (float)num));
			}
		}
	}

	// Token: 0x060005D9 RID: 1497 RVA: 0x00022F4C File Offset: 0x0002114C
	public override void OnKilled(global::HitInfo info)
	{
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Unused2, false);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Unused1, false);
		this.EnsureDismounted();
		this.EndSleeping();
		this.EndLooting();
		this.ClearHostileTime();
		this.stats.Add("deaths", 1, global::Stats.All);
		this.UpdatePlayerCollider(false);
		this.UpdatePlayerRigidbody(false);
		this.StopWounded();
		this.inventory.crafting.CancelAll(true);
		if (global::EACServer.playerTracker != null && this.net.connection != null)
		{
			global::BasePlayer basePlayer = (info == null || !(info.Initiator != null)) ? null : info.Initiator.ToPlayer();
			if (basePlayer != null && basePlayer.net.connection != null)
			{
				using (TimeWarning.New("playerTracker.LogPlayerKill", 0.1f))
				{
					PlayerKill playerKill = default(PlayerKill);
					playerKill.ClientKiller = global::EACServer.GetClient(basePlayer.net.connection);
					playerKill.ClientVictim = global::EACServer.GetClient(this.net.connection);
					global::EACServer.playerTracker.LogPlayerKill(playerKill);
				}
			}
			else
			{
				using (TimeWarning.New("playerTracker.LogPlayerDespawn", 0.1f))
				{
					PlayerDespawn playerDespawn = default(PlayerDespawn);
					playerDespawn.Client = global::EACServer.GetClient(this.net.connection);
					global::EACServer.playerTracker.LogPlayerDespawn(playerDespawn);
				}
			}
		}
		this.CreateCorpse();
		this.inventory.Strip();
		if (this.lastDamage == Rust.DamageType.Fall)
		{
			this.stats.Add("death_fall", 1, global::Stats.Steam);
		}
		string text = string.Empty;
		string msg = string.Empty;
		if (info != null)
		{
			if (info.Initiator)
			{
				if (info.Initiator == this)
				{
					text = this.ToString() + " was suicide by " + this.lastDamage;
					msg = "You died: suicide by " + this.lastDamage;
					if (this.lastDamage == Rust.DamageType.Suicide)
					{
						Facepunch.Rust.Analytics.Death("suicide");
						this.stats.Add("death_suicide", 1, global::Stats.All);
					}
					else
					{
						Facepunch.Rust.Analytics.Death("selfinflicted");
						this.stats.Add("death_selfinflicted", 1, global::Stats.Steam);
					}
				}
				else if (info.Initiator is global::BasePlayer)
				{
					global::BasePlayer basePlayer2 = info.Initiator.ToPlayer();
					text = this.ToString() + " was killed by " + basePlayer2.ToString();
					msg = string.Concat(new object[]
					{
						"You died: killed by ",
						basePlayer2.displayName,
						" (",
						basePlayer2.userID,
						")"
					});
					basePlayer2.stats.Add("kill_player", 1, global::Stats.All);
					if (info.WeaponPrefab != null)
					{
						Facepunch.Rust.Analytics.Death(info.WeaponPrefab.ShortPrefabName);
					}
					else
					{
						Facepunch.Rust.Analytics.Death("player");
					}
				}
				else
				{
					text = string.Concat(new string[]
					{
						this.ToString(),
						" was killed by ",
						info.Initiator.ShortPrefabName,
						" (",
						info.Initiator.Categorize(),
						")"
					});
					msg = "You died: killed by " + info.Initiator.Categorize();
					this.stats.Add("death_" + info.Initiator.Categorize(), 1, global::Stats.Steam);
					Facepunch.Rust.Analytics.Death(info.Initiator.Categorize());
				}
			}
			else if (this.lastDamage == Rust.DamageType.Fall)
			{
				text = this.ToString() + " was killed by fall!";
				msg = "You died: killed by fall!";
				Facepunch.Rust.Analytics.Death("fall");
			}
			else
			{
				text = this.ToString() + " was killed by " + info.damageTypes.GetMajorityDamageType().ToString();
				msg = "You died: " + info.damageTypes.GetMajorityDamageType().ToString();
			}
		}
		else
		{
			text = string.Concat(new object[]
			{
				this.ToString(),
				" died (",
				this.lastDamage,
				")"
			});
			msg = "You died: " + this.lastDamage.ToString();
		}
		using (TimeWarning.New("LogMessage", 0.1f))
		{
			DebugEx.Log(text, 0);
			this.ConsoleMessage(msg);
		}
		base.SendNetworkUpdateImmediate(false);
		this.LifeStoryLogDeath(info, this.lastDamage);
		this.LifeStoryEnd();
		if (this.net.connection == null)
		{
			base.Invoke(new Action(base.KillMessage), 0f);
		}
		else
		{
			this.SendRespawnOptions();
			this.SendDeathInformation();
			this.stats.Save();
		}
	}

	// Token: 0x060005DA RID: 1498 RVA: 0x000234A0 File Offset: 0x000216A0
	public void RespawnAt(Vector3 position, Quaternion rotation)
	{
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Wounded, false);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Unused2, false);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Unused1, false);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.ReceivingSnapshot, true);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash, false);
		global::ServerPerformance.spawns += 1UL;
		base.transform.position = position;
		base.transform.rotation = rotation;
		this.tickInterpolator.Reset(position);
		this.lastTickTime = 0f;
		this.StopWounded();
		this.StopSpectating();
		this.UpdateNetworkGroup();
		this.UpdatePlayerCollider(true);
		this.UpdatePlayerRigidbody(false);
		this.StartSleeping();
		this.LifeStoryStart();
		this.metabolism.Reset();
		this.InitializeHealth(this.StartHealth(), this.StartMaxHealth());
		this.inventory.GiveDefaultItems();
		base.SendNetworkUpdateImmediate(false);
		this.ClearEntityQueue(null);
		base.ClientRPCPlayer(null, this, "StartLoading");
		if (this.net != null)
		{
			global::EACServer.OnStartLoading(this.net.connection);
		}
		Interface.CallHook("OnPlayerRespawned", new object[]
		{
			this
		});
		this.SendFullSnapshot();
	}

	// Token: 0x060005DB RID: 1499 RVA: 0x000235C0 File Offset: 0x000217C0
	public void Respawn()
	{
		global::BasePlayer.SpawnPoint spawnPoint = global::ServerMgr.FindSpawnPoint();
		object obj = Interface.CallHook("OnPlayerRespawn", new object[]
		{
			this
		});
		if (obj is global::BasePlayer.SpawnPoint)
		{
			spawnPoint = (global::BasePlayer.SpawnPoint)obj;
		}
		this.RespawnAt(spawnPoint.pos, spawnPoint.rot);
	}

	// Token: 0x060005DC RID: 1500 RVA: 0x0002360C File Offset: 0x0002180C
	public bool IsImmortal()
	{
		return ((this.IsAdmin || this.IsDeveloper) && this.IsConnected && this.net.connection != null && this.net.connection.info.GetBool("global.god", false)) || this.WoundingCausingImmportality();
	}

	// Token: 0x060005DD RID: 1501 RVA: 0x0002367C File Offset: 0x0002187C
	public float TimeAlive()
	{
		return this.lifeStory.secondsAlive;
	}

	// Token: 0x060005DE RID: 1502 RVA: 0x0002368C File Offset: 0x0002188C
	public override void Hurt(global::HitInfo info)
	{
		if (this.IsDead())
		{
			return;
		}
		if (this.IsImmortal())
		{
			return;
		}
		if (Interface.CallHook("IOnBasePlayerHurt", new object[]
		{
			this,
			info
		}) != null)
		{
			return;
		}
		if (ConVar.Server.pve && info.Initiator && info.Initiator is global::BasePlayer && info.Initiator != this)
		{
			global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
			basePlayer.Hurt(info.damageTypes.Total(), Rust.DamageType.Generic, null, true);
			return;
		}
		this.metabolism.pending_health.Subtract(info.damageTypes.Total() * 10f);
		base.Hurt(info);
		if (global::EACServer.playerTracker != null && info.Initiator != null && info.Initiator is global::BasePlayer)
		{
			global::BasePlayer basePlayer2 = info.Initiator.ToPlayer();
			if (this.net.connection != null && basePlayer2.net.connection != null)
			{
				PlayerTakeDamage playerTakeDamage = default(PlayerTakeDamage);
				playerTakeDamage.VictimClient = global::EACServer.GetClient(this.net.connection);
				playerTakeDamage.AttackerClient = global::EACServer.GetClient(basePlayer2.net.connection);
				playerTakeDamage.HitBoneID = (int)info.HitBone;
				if (info.isHeadshot)
				{
					playerTakeDamage.Flags |= 1;
				}
				playerTakeDamage.WeaponID = 0;
				if (info.Weapon != null)
				{
					global::Item item = info.Weapon.GetItem();
					if (item != null)
					{
						playerTakeDamage.WeaponID = item.info.itemid;
					}
				}
				Vector3 position = basePlayer2.eyes.position;
				Vector3 eulerAngles = basePlayer2.eyes.rotation.eulerAngles;
				Vector3 position2 = this.eyes.position;
				Vector3 eulerAngles2 = this.eyes.rotation.eulerAngles;
				playerTakeDamage.AttackerPosition = new Vector3(position.x, position.y, position.z);
				playerTakeDamage.AttackerViewAngles = new Vector3(eulerAngles.x, eulerAngles.y, eulerAngles.z);
				playerTakeDamage.VictimPosition = new Vector3(position2.x, position2.y, position2.z);
				playerTakeDamage.VictimViewAngles = new Vector3(eulerAngles2.x, eulerAngles2.y, eulerAngles2.z);
				global::EACServer.playerTracker.LogPlayerTakeDamage(playerTakeDamage);
			}
		}
		this.metabolism.SendChangesToClient();
		if (info.PointStart != Vector3.zero)
		{
			base.ClientRPCPlayer<Vector3, int>(null, this, "DirectionalDamage", info.PointStart, (int)info.damageTypes.GetMajorityDamageType());
		}
	}

	// Token: 0x060005DF RID: 1503 RVA: 0x00023954 File Offset: 0x00021B54
	public static global::BasePlayer FindByID(ulong userID)
	{
		global::BasePlayer result;
		using (TimeWarning.New("BasePlayer.FindByID", 0.1f))
		{
			result = global::BasePlayer.activePlayerList.Find((global::BasePlayer x) => x.userID == userID);
		}
		return result;
	}

	// Token: 0x060005E0 RID: 1504 RVA: 0x000239B8 File Offset: 0x00021BB8
	public static global::BasePlayer FindSleeping(ulong userID)
	{
		global::BasePlayer result;
		using (TimeWarning.New("BasePlayer.FindSleeping", 0.1f))
		{
			result = global::BasePlayer.sleepingPlayerList.Find((global::BasePlayer x) => x.userID == userID);
		}
		return result;
	}

	// Token: 0x060005E1 RID: 1505 RVA: 0x00023A1C File Offset: 0x00021C1C
	public void Command(string strCommand, params object[] arguments)
	{
		if (this.net.connection == null)
		{
			return;
		}
		global::ConsoleNetwork.SendClientCommand(this.net.connection, strCommand, arguments);
	}

	// Token: 0x060005E2 RID: 1506 RVA: 0x00023A44 File Offset: 0x00021C44
	public override void OnInvalidPosition()
	{
		if (this.IsDead())
		{
			return;
		}
		this.Die(null);
	}

	// Token: 0x060005E3 RID: 1507 RVA: 0x00023A5C File Offset: 0x00021C5C
	private static global::BasePlayer Find(string strNameOrIDOrIP, List<global::BasePlayer> list)
	{
		global::BasePlayer basePlayer = list.Find((global::BasePlayer x) => x.UserIDString == strNameOrIDOrIP);
		if (basePlayer)
		{
			return basePlayer;
		}
		global::BasePlayer basePlayer2 = list.Find((global::BasePlayer x) => x.displayName.StartsWith(strNameOrIDOrIP, StringComparison.CurrentCultureIgnoreCase));
		if (basePlayer2)
		{
			return basePlayer2;
		}
		global::BasePlayer basePlayer3 = list.Find((global::BasePlayer x) => x.net != null && x.net.connection != null && x.net.connection.ipaddress == strNameOrIDOrIP);
		if (basePlayer3)
		{
			return basePlayer3;
		}
		return null;
	}

	// Token: 0x060005E4 RID: 1508 RVA: 0x00023AD8 File Offset: 0x00021CD8
	public static global::BasePlayer Find(string strNameOrIDOrIP)
	{
		return global::BasePlayer.Find(strNameOrIDOrIP, global::BasePlayer.activePlayerList);
	}

	// Token: 0x060005E5 RID: 1509 RVA: 0x00023AE8 File Offset: 0x00021CE8
	public static global::BasePlayer FindSleeping(string strNameOrIDOrIP)
	{
		return global::BasePlayer.Find(strNameOrIDOrIP, global::BasePlayer.sleepingPlayerList);
	}

	// Token: 0x060005E6 RID: 1510 RVA: 0x00023AF8 File Offset: 0x00021CF8
	public void SendConsoleCommand(string command, params object[] obj)
	{
		global::ConsoleNetwork.SendClientCommand(this.net.connection, command, obj);
	}

	// Token: 0x060005E7 RID: 1511 RVA: 0x00023B0C File Offset: 0x00021D0C
	public void UpdateRadiation(float fAmount)
	{
		this.metabolism.radiation_level.Increase(fAmount);
	}

	// Token: 0x060005E8 RID: 1512 RVA: 0x00023B20 File Offset: 0x00021D20
	public override float RadiationExposureFraction()
	{
		float num = Mathf.Clamp(this.baseProtection.amounts[17], 0f, 1f);
		return 1f - num;
	}

	// Token: 0x060005E9 RID: 1513 RVA: 0x00023B54 File Offset: 0x00021D54
	public override float RadiationProtection()
	{
		return this.baseProtection.amounts[17] * 100f;
	}

	// Token: 0x060005EA RID: 1514 RVA: 0x00023B6C File Offset: 0x00021D6C
	public override void OnHealthChanged(float oldvalue, float newvalue)
	{
		if (Interface.CallHook("OnPlayerHealthChange", new object[]
		{
			this,
			oldvalue,
			newvalue
		}) != null)
		{
			return;
		}
		base.OnHealthChanged(oldvalue, newvalue);
		this.metabolism.isDirty = true;
	}

	// Token: 0x060005EB RID: 1515 RVA: 0x00023BBC File Offset: 0x00021DBC
	public void SV_ClothingChanged()
	{
		this.UpdateProtectionFromClothing();
		this.UpdateMoveSpeedFromClothing();
	}

	// Token: 0x060005EC RID: 1516 RVA: 0x00023BCC File Offset: 0x00021DCC
	public bool IsNoob()
	{
		return !this.HasPlayerFlag(global::BasePlayer.PlayerFlags.DisplaySash);
	}

	// Token: 0x060005ED RID: 1517 RVA: 0x00023BDC File Offset: 0x00021DDC
	public bool HasHostileItem()
	{
		bool result;
		using (TimeWarning.New("BasePlayer.HasHostileItem", 0.1f))
		{
			foreach (global::Item item in this.inventory.containerBelt.itemList)
			{
				if (this.IsHostileItem(item))
				{
					return true;
				}
			}
			foreach (global::Item item2 in this.inventory.containerMain.itemList)
			{
				if (this.IsHostileItem(item2))
				{
					return true;
				}
			}
			result = false;
		}
		return result;
	}

	// Token: 0x060005EE RID: 1518 RVA: 0x00023CE4 File Offset: 0x00021EE4
	public bool IsHostileItem(global::Item item)
	{
		if (!item.info.isHoldable)
		{
			return false;
		}
		global::ItemModEntity component = item.info.GetComponent<global::ItemModEntity>();
		if (component == null)
		{
			return false;
		}
		GameObject gameObject = component.entityPrefab.Get();
		if (gameObject == null)
		{
			return false;
		}
		global::AttackEntity component2 = gameObject.GetComponent<global::AttackEntity>();
		return !(component2 == null) && component2.hostile;
	}

	// Token: 0x060005EF RID: 1519 RVA: 0x00023D54 File Offset: 0x00021F54
	public override void GiveItem(global::Item item, global::BaseEntity.GiveItemReason reason = global::BaseEntity.GiveItemReason.Generic)
	{
		if (reason == global::BaseEntity.GiveItemReason.ResourceHarvested)
		{
			this.stats.Add(string.Format("harvest.{0}", item.info.shortname), item.amount, global::Stats.Steam);
		}
		if (this.inventory.GiveItem(item, null))
		{
			if (!string.IsNullOrEmpty(item.name))
			{
				this.Command("note.inv", new object[]
				{
					item.info.itemid,
					item.amount,
					item.name,
					(int)reason
				});
			}
			else
			{
				this.Command("note.inv", new object[]
				{
					item.info.itemid,
					item.amount,
					string.Empty,
					(int)reason
				});
			}
		}
		else
		{
			item.Drop(this.inventory.containerMain.dropPosition, this.inventory.containerMain.dropVelocity, default(Quaternion));
		}
	}

	// Token: 0x060005F0 RID: 1520 RVA: 0x00023E74 File Offset: 0x00022074
	public override void AttackerInfo(PlayerLifeStory.DeathInfo info)
	{
		info.attackerName = this.displayName;
		info.attackerSteamID = this.userID;
	}

	// Token: 0x1700004F RID: 79
	// (get) Token: 0x060005F1 RID: 1521 RVA: 0x00023E90 File Offset: 0x00022090
	public float currentCraftLevel
	{
		get
		{
			if (this.triggers == null)
			{
				return 0f;
			}
			if (this.nextCheckTime > UnityEngine.Time.realtimeSinceStartup)
			{
				return this.cachedCraftLevel;
			}
			this.nextCheckTime = UnityEngine.Time.realtimeSinceStartup + Random.Range(0.4f, 0.5f);
			float num = 0f;
			for (int i = 0; i < this.triggers.Count; i++)
			{
				global::TriggerWorkbench triggerWorkbench = this.triggers[i] as global::TriggerWorkbench;
				if (!(triggerWorkbench == null))
				{
					if (!(triggerWorkbench.parentBench == null))
					{
						if (triggerWorkbench.parentBench.IsVisible(this.eyes.position))
						{
							float num2 = triggerWorkbench.WorkbenchLevel();
							if (num2 > num)
							{
								num = num2;
							}
						}
					}
				}
			}
			this.cachedCraftLevel = num;
			return num;
		}
	}

	// Token: 0x17000050 RID: 80
	// (get) Token: 0x060005F2 RID: 1522 RVA: 0x00023F78 File Offset: 0x00022178
	public float currentComfort
	{
		get
		{
			float num = 0f;
			if (this.isMounted)
			{
				num = this.GetMounted().GetComfort();
			}
			if (this.triggers == null)
			{
				return num;
			}
			for (int i = 0; i < this.triggers.Count; i++)
			{
				global::TriggerComfort triggerComfort = this.triggers[i] as global::TriggerComfort;
				if (!(triggerComfort == null))
				{
					float num2 = triggerComfort.CalculateComfort(base.transform.position, this);
					if (num2 > num)
					{
						num = num2;
					}
				}
			}
			return num;
		}
	}

	// Token: 0x060005F3 RID: 1523 RVA: 0x0002400C File Offset: 0x0002220C
	public virtual bool ShouldDropActiveItem()
	{
		object obj = Interface.CallHook("CanDropActiveItem", new object[]
		{
			this
		});
		return !(obj is bool) || (bool)obj;
	}

	// Token: 0x060005F4 RID: 1524 RVA: 0x00024044 File Offset: 0x00022244
	public override void Die(global::HitInfo info = null)
	{
		using (TimeWarning.New("Player.Die", 0.1f))
		{
			if (!this.IsDead())
			{
				if (this.Belt != null && this.ShouldDropActiveItem())
				{
					Vector3 vector;
					vector..ctor(Random.Range(-2f, 2f), 0.2f, Random.Range(-2f, 2f));
					this.Belt.DropActive(vector.normalized * 3f);
				}
				if (!this.WoundInsteadOfDying(info))
				{
					if (Interface.CallHook("OnPlayerDie", new object[]
					{
						this,
						info
					}) != null)
					{
						return;
					}
					base.Die(info);
				}
			}
		}
	}

	// Token: 0x060005F5 RID: 1525 RVA: 0x00024128 File Offset: 0x00022328
	public void Kick(string reason)
	{
		if (!this.IsConnected)
		{
			return;
		}
		Network.Net.sv.Kick(this.net.connection, reason);
		Interface.CallHook("OnPlayerKicked", new object[]
		{
			this,
			reason
		});
	}

	// Token: 0x060005F6 RID: 1526 RVA: 0x00024174 File Offset: 0x00022374
	public override Vector3 GetDropPosition()
	{
		return this.eyes.position;
	}

	// Token: 0x060005F7 RID: 1527 RVA: 0x00024184 File Offset: 0x00022384
	public override Vector3 GetDropVelocity()
	{
		return this.eyes.BodyForward() * 3f * Random.Range(1f, 1.5f);
	}

	// Token: 0x060005F8 RID: 1528 RVA: 0x000241B0 File Offset: 0x000223B0
	public virtual void SetInfo(string key, string val)
	{
		if (!this.IsConnected)
		{
			return;
		}
		this.net.connection.info.Set(key, val);
	}

	// Token: 0x060005F9 RID: 1529 RVA: 0x000241D8 File Offset: 0x000223D8
	public virtual int GetInfoInt(string key, int defaultVal)
	{
		if (!this.IsConnected)
		{
			return defaultVal;
		}
		return this.net.connection.info.GetInt(key, defaultVal);
	}

	// Token: 0x060005FA RID: 1530 RVA: 0x00024200 File Offset: 0x00022400
	[global::BaseEntity.RPC_Server]
	public void PerformanceReport(global::BaseEntity.RPCMessage msg)
	{
		int num = msg.read.Int32();
		int num2 = msg.read.Int32();
		float num3 = msg.read.Float();
		int num4 = msg.read.Int32();
		Debug.LogFormat("{0}{1}{2}{3}{4}", new object[]
		{
			(num + "MB").PadRight(9),
			(num2 + "MB").PadRight(9),
			(num3.ToString("0") + "FPS").PadRight(8),
			NumberExtensions.FormatSeconds((long)num4).PadRight(9),
			this.displayName
		});
	}

	// Token: 0x060005FB RID: 1531 RVA: 0x000242C0 File Offset: 0x000224C0
	public override bool ShouldNetworkTo(global::BasePlayer player)
	{
		return (!this.IsSpectating() || !(player != this) || player.net.connection.info.GetBool("global.specnet", false)) && base.ShouldNetworkTo(player);
	}

	// Token: 0x060005FC RID: 1532 RVA: 0x00024310 File Offset: 0x00022510
	internal void GiveAchievement(string name)
	{
		if (GameInfo.HasAchievements)
		{
			base.ClientRPCPlayer<string>(null, this, "RecieveAchievement", name);
		}
	}

	// Token: 0x17000051 RID: 81
	// (get) Token: 0x060005FD RID: 1533 RVA: 0x0002432C File Offset: 0x0002252C
	public bool hasPreviousLife
	{
		get
		{
			return this.previousLifeStory != null;
		}
	}

	// Token: 0x060005FE RID: 1534 RVA: 0x0002433C File Offset: 0x0002253C
	internal void LifeStoryStart()
	{
		Assert.IsTrue(this.lifeStory == null, "Stomping old lifeStory");
		this.lifeStory = new PlayerLifeStory
		{
			ShouldPool = false
		};
		this.lifeStory.timeBorn = (uint)Epoch.Current;
	}

	// Token: 0x060005FF RID: 1535 RVA: 0x00024380 File Offset: 0x00022580
	internal void LifeStoryEnd()
	{
		SingletonComponent<global::ServerMgr>.Instance.persistance.AddLifeStory(this.userID, this.lifeStory);
		this.previousLifeStory = this.lifeStory;
		this.lifeStory = null;
	}

	// Token: 0x06000600 RID: 1536 RVA: 0x000243B0 File Offset: 0x000225B0
	internal void LifeStoryUpdate(float deltaTime)
	{
		if (this.lifeStory == null)
		{
			return;
		}
		this.lifeStory.secondsAlive += deltaTime;
		if (this.IsSleeping())
		{
			this.lifeStory.secondsSleeping += deltaTime;
		}
	}

	// Token: 0x06000601 RID: 1537 RVA: 0x000243F0 File Offset: 0x000225F0
	internal void LifeStoryLogDeath(global::HitInfo deathBlow, Rust.DamageType lastDamage)
	{
		if (this.lifeStory == null)
		{
			return;
		}
		this.lifeStory.timeDied = (uint)Epoch.Current;
		PlayerLifeStory.DeathInfo deathInfo = Facepunch.Pool.Get<PlayerLifeStory.DeathInfo>();
		deathInfo.lastDamageType = (int)lastDamage;
		if (deathBlow != null)
		{
			if (deathBlow.Initiator != null)
			{
				deathBlow.Initiator.AttackerInfo(deathInfo);
			}
			if (deathBlow.WeaponPrefab != null)
			{
				deathInfo.inflictorName = deathBlow.WeaponPrefab.ShortPrefabName;
			}
			if (deathBlow.HitBone > 0u)
			{
				deathInfo.hitBone = global::StringPool.Get(deathBlow.HitBone);
			}
			else
			{
				deathInfo.hitBone = string.Empty;
			}
		}
		else if (base.SecondsSinceAttacked <= 60f && this.lastAttacker != null)
		{
			this.lastAttacker.AttackerInfo(deathInfo);
		}
		this.lifeStory.deathInfo = deathInfo;
	}

	// Token: 0x06000602 RID: 1538 RVA: 0x000244D8 File Offset: 0x000226D8
	private void Tick_Spectator()
	{
		int num = 0;
		if (this.serverInput.WasJustPressed(global::BUTTON.JUMP))
		{
			num++;
		}
		if (this.serverInput.WasJustPressed(global::BUTTON.DUCK))
		{
			num--;
		}
		if (num != 0)
		{
			this.SpectateOffset += num;
			using (TimeWarning.New("UpdateSpectateTarget", 0.1f))
			{
				this.UpdateSpectateTarget(this.spectateFilter);
			}
		}
	}

	// Token: 0x06000603 RID: 1539 RVA: 0x00024568 File Offset: 0x00022768
	public void UpdateSpectateTarget(string strName)
	{
		this.spectateFilter = strName;
		IEnumerable<global::BaseEntity> source2;
		if (this.spectateFilter.StartsWith("@"))
		{
			string filter = this.spectateFilter.Substring(1);
			IEnumerable<global::BaseNetworkable> source = from x in global::BaseNetworkable.serverEntities
			where StringEx.Contains(x.name, filter, CompareOptions.IgnoreCase)
			where x != this
			select x;
			source2 = source.Cast<global::BaseEntity>();
		}
		else
		{
			IEnumerable<global::BasePlayer> source3 = from x in global::BasePlayer.activePlayerList
			where !x.IsSpectating() && !x.IsDead() && !x.IsSleeping()
			select x;
			if (strName.Length > 0)
			{
				source3 = from x in source3
				where StringEx.Contains(x.displayName, this.spectateFilter, CompareOptions.IgnoreCase) || x.UserIDString.Contains(this.spectateFilter)
				where x != this
				select x;
			}
			source3 = from x in source3
			orderby x.displayName
			select x;
			source2 = source3.Cast<global::BaseEntity>();
		}
		global::BaseEntity[] array = source2.ToArray<global::BaseEntity>();
		if (array.Length == 0)
		{
			this.ChatMessage("No valid spectate targets!");
			return;
		}
		global::BaseEntity baseEntity = array[this.SpectateOffset % array.Length];
		if (baseEntity != null)
		{
			if (baseEntity is global::BasePlayer)
			{
				this.ChatMessage("Spectating: " + (baseEntity as global::BasePlayer).displayName);
			}
			else
			{
				this.ChatMessage("Spectating: " + baseEntity.ToString());
			}
			this.ClearEntityQueue(null);
			using (TimeWarning.New("SendEntitySnapshot", 0.1f))
			{
				this.SendEntitySnapshot(baseEntity);
			}
			base.gameObject.Identity();
			using (TimeWarning.New("SetParent", 0.1f))
			{
				base.SetParent(baseEntity, 0u);
			}
		}
	}

	// Token: 0x06000604 RID: 1540 RVA: 0x00024770 File Offset: 0x00022970
	public void StartSpectating()
	{
		if (this.IsSpectating())
		{
			return;
		}
		if (Interface.CallHook("OnPlayerSpectate", new object[]
		{
			this,
			this.spectateFilter
		}) != null)
		{
			return;
		}
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Spectating, true);
		base.gameObject.SetLayerRecursive(10);
		base.CancelInvoke(new Action(this.InventoryUpdate));
		this.ChatMessage("Becoming Spectator");
		this.UpdateSpectateTarget(this.spectateFilter);
	}

	// Token: 0x06000605 RID: 1541 RVA: 0x000247EC File Offset: 0x000229EC
	public void StopSpectating()
	{
		if (!this.IsSpectating())
		{
			return;
		}
		if (Interface.CallHook("OnPlayerSpectateEnd", new object[]
		{
			this,
			this.spectateFilter
		}) != null)
		{
			return;
		}
		base.SetParent(null, 0u);
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Spectating, false);
		base.gameObject.SetLayerRecursive(17);
	}

	// Token: 0x06000606 RID: 1542 RVA: 0x00024848 File Offset: 0x00022A48
	public void Teleport(global::BasePlayer player)
	{
		this.Teleport(player.transform.position);
	}

	// Token: 0x06000607 RID: 1543 RVA: 0x0002485C File Offset: 0x00022A5C
	public void Teleport(string strName, bool playersOnly)
	{
		global::BaseEntity[] array = global::BaseEntity.Util.FindTargets(strName, playersOnly);
		if (array == null || array.Length == 0)
		{
			return;
		}
		global::BaseEntity baseEntity = array[Random.Range(0, array.Length)];
		this.Teleport(baseEntity.transform.position);
	}

	// Token: 0x06000608 RID: 1544 RVA: 0x000248A0 File Offset: 0x00022AA0
	public void Teleport(Vector3 position)
	{
		this.MovePosition(position);
		base.ClientRPCPlayer<Vector3>(null, this, "ForcePositionTo", position);
	}

	// Token: 0x06000609 RID: 1545 RVA: 0x000248B8 File Offset: 0x00022AB8
	public override float GetThreatLevel()
	{
		this.EnsureUpdated();
		return this.cachedThreatLevel;
	}

	// Token: 0x0600060A RID: 1546 RVA: 0x000248C8 File Offset: 0x00022AC8
	public void EnsureUpdated()
	{
		if (UnityEngine.Time.realtimeSinceStartup - this.lastUpdateTime < 30f)
		{
			return;
		}
		this.lastUpdateTime = UnityEngine.Time.realtimeSinceStartup;
		this.cachedThreatLevel = 0f;
		if (!this.IsSleeping())
		{
			if (this.inventory.containerWear.itemList.Count > 2)
			{
				this.cachedThreatLevel += 1f;
			}
			foreach (global::Item item in this.inventory.containerBelt.itemList)
			{
				global::BaseEntity heldEntity = item.GetHeldEntity();
				if (heldEntity && heldEntity is global::BaseProjectile && !(heldEntity is global::BowWeapon))
				{
					this.cachedThreatLevel += 2f;
					break;
				}
			}
		}
	}

	// Token: 0x17000052 RID: 82
	// (get) Token: 0x0600060B RID: 1547 RVA: 0x000249CC File Offset: 0x00022BCC
	public float timeSinceLastTick
	{
		get
		{
			if (this.lastTickTime == 0f)
			{
				return 0f;
			}
			return UnityEngine.Time.time - this.lastTickTime;
		}
	}

	// Token: 0x17000053 RID: 83
	// (get) Token: 0x0600060C RID: 1548 RVA: 0x000249F0 File Offset: 0x00022BF0
	public float IdleTime
	{
		get
		{
			if (this.lastInputTime == 0f)
			{
				return 0f;
			}
			return UnityEngine.Time.time - this.lastInputTime;
		}
	}

	// Token: 0x17000054 RID: 84
	// (get) Token: 0x0600060D RID: 1549 RVA: 0x00024A14 File Offset: 0x00022C14
	public bool isStalled
	{
		get
		{
			return !this.IsDead() && !this.IsSleeping() && this.timeSinceLastTick > 1f;
		}
	}

	// Token: 0x17000055 RID: 85
	// (get) Token: 0x0600060E RID: 1550 RVA: 0x00024A40 File Offset: 0x00022C40
	public bool wasStalled
	{
		get
		{
			if (this.isStalled)
			{
				this.lastStallTime = UnityEngine.Time.time;
			}
			return UnityEngine.Time.time - this.lastStallTime < 1f;
		}
	}

	// Token: 0x0600060F RID: 1551 RVA: 0x00024A6C File Offset: 0x00022C6C
	public void OnReceivedTick(Stream stream)
	{
		using (TimeWarning.New("OnReceiveTickFromStream", 0.1f))
		{
			PlayerTick playerTick = null;
			using (TimeWarning.New("lastReceivedTick = data.Copy", 0.1f))
			{
				playerTick = PlayerTick.Deserialize(stream, this.lastReceivedTick, true);
			}
			using (TimeWarning.New("lastReceivedTick = data.Copy", 0.1f))
			{
				this.lastReceivedTick = playerTick.Copy();
			}
			using (TimeWarning.New("OnReceiveTick", 0.1f))
			{
				this.OnReceiveTick(playerTick, this.wasStalled);
			}
			using (TimeWarning.New("EACStateUpdate", 0.1f))
			{
				this.EACStateUpdate();
			}
			this.lastTickTime = UnityEngine.Time.time;
			playerTick.Dispose();
		}
	}

	// Token: 0x06000610 RID: 1552 RVA: 0x00024BAC File Offset: 0x00022DAC
	public void OnReceivedVoice(byte[] data)
	{
		if (Interface.CallHook("OnPlayerVoice", new object[]
		{
			this,
			data
		}) != null)
		{
			return;
		}
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(21);
			Network.Net.sv.write.UInt32(this.net.ID);
			Network.Net.sv.write.BytesWithSize(data);
			Write write = Network.Net.sv.write;
			SendInfo sendInfo = new SendInfo(global::BaseNetworkable.GetConnectionsWithin(base.transform.position, 100f));
			sendInfo.priority = 0;
			write.Send(sendInfo);
		}
	}

	// Token: 0x06000611 RID: 1553 RVA: 0x00024C5C File Offset: 0x00022E5C
	private void EACStateUpdate()
	{
		if (global::EACServer.playerTracker == null)
		{
			return;
		}
		if (this.IsReceivingSnapshot)
		{
			return;
		}
		Vector3 position = this.eyes.position;
		Vector3 eulerAngles = this.eyes.rotation.eulerAngles;
		PlayerTick playerTick = default(PlayerTick);
		playerTick.Client = global::EACServer.GetClient(this.net.connection);
		playerTick.PlayerPosition = new Vector3(position.x, position.y, position.z);
		playerTick.PlayerViewAngles = new Vector3(eulerAngles.x, eulerAngles.y, eulerAngles.z);
		if (!this.IsOnGround())
		{
			playerTick.Flags |= 1;
		}
		using (TimeWarning.New("playerTracker.LogPlayerState", 0.1f))
		{
			try
			{
				global::EACServer.playerTracker.LogPlayerTick(playerTick);
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Disabling EAC Logging due to exception");
				global::EACServer.playerTracker = null;
				Debug.LogException(ex);
			}
		}
	}

	// Token: 0x06000612 RID: 1554 RVA: 0x00024D84 File Offset: 0x00022F84
	private void OnReceiveTick(PlayerTick msg, bool wasPlayerStalled)
	{
		if (msg.inputState != null)
		{
			this.serverInput.Flip(msg.inputState);
		}
		if (Interface.CallHook("OnPlayerTick", new object[]
		{
			this,
			msg,
			wasPlayerStalled
		}) != null)
		{
			return;
		}
		if (Interface.CallHook("OnPlayerInput", new object[]
		{
			this,
			this.serverInput
		}) != null)
		{
			return;
		}
		if (this.serverInput.current.buttons != this.serverInput.previous.buttons)
		{
			this.lastInputTime = UnityEngine.Time.time;
		}
		if (this.IsReceivingSnapshot)
		{
			return;
		}
		if (this.IsSpectating())
		{
			using (TimeWarning.New("Tick_Spectator", 0.1f))
			{
				this.Tick_Spectator();
			}
			return;
		}
		if (this.IsDead())
		{
			return;
		}
		if (this.IsSleeping())
		{
			if (this.serverInput.WasJustPressed(global::BUTTON.FIRE_PRIMARY) || this.serverInput.WasJustPressed(global::BUTTON.FIRE_SECONDARY) || this.serverInput.WasJustPressed(global::BUTTON.JUMP) || this.serverInput.WasJustPressed(global::BUTTON.DUCK))
			{
				this.EndSleeping();
				base.SendNetworkUpdateImmediate(false);
			}
			this.UpdateActiveItem(0u);
			return;
		}
		this.UpdateActiveItem(msg.activeItem);
		this.UpdateModelStateFromTick(msg);
		if (this.IsWounded())
		{
			return;
		}
		if (this.isMounted)
		{
			this.GetMounted().PlayerServerInput(this.serverInput, this);
		}
		this.UpdatePositionFromTick(msg, wasPlayerStalled);
		this.UpdateRotationFromTick(msg);
	}

	// Token: 0x06000613 RID: 1555 RVA: 0x00024F3C File Offset: 0x0002313C
	internal void UpdateActiveItem(uint itemID)
	{
		Assert.IsTrue(base.isServer, "Realm should be server!");
		if (this.svActiveItemID == itemID)
		{
			return;
		}
		global::Item activeItem = this.GetActiveItem();
		this.svActiveItemID = 0u;
		if (activeItem != null)
		{
			global::HeldEntity heldEntity = activeItem.GetHeldEntity() as global::HeldEntity;
			if (heldEntity != null)
			{
				heldEntity.SetHeld(false);
			}
		}
		this.svActiveItemID = itemID;
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		global::Item activeItem2 = this.GetActiveItem();
		if (activeItem2 != null)
		{
			global::HeldEntity heldEntity2 = activeItem2.GetHeldEntity() as global::HeldEntity;
			if (heldEntity2 != null)
			{
				heldEntity2.SetHeld(true);
			}
		}
		this.inventory.UpdatedVisibleHolsteredItems();
		Interface.CallHook("OnPlayerActiveItemChanged", new object[]
		{
			this,
			activeItem,
			activeItem2
		});
	}

	// Token: 0x06000614 RID: 1556 RVA: 0x00025004 File Offset: 0x00023204
	internal void UpdateModelStateFromTick(PlayerTick tick)
	{
		if (tick.modelState == null)
		{
			return;
		}
		if (ModelState.Equal(this.modelStateTick, tick.modelState))
		{
			return;
		}
		if (this.modelStateTick != null)
		{
			this.modelStateTick.ResetToPool();
		}
		this.modelStateTick = tick.modelState;
		tick.modelState = null;
		this.tickNeedsFinalizing = true;
	}

	// Token: 0x06000615 RID: 1557 RVA: 0x00025064 File Offset: 0x00023264
	internal void UpdatePositionFromTick(PlayerTick tick, bool wasPlayerStalled)
	{
		if (Vector3Ex.IsNaNOrInfinity(tick.position) || Vector3Ex.IsNaNOrInfinity(tick.eyePos))
		{
			this.Kick("Kicked: Invalid Position");
			return;
		}
		if (this.isMounted || (this.modelState != null && this.modelState.sitting) || (tick.modelState != null && tick.modelState.sitting))
		{
			return;
		}
		if (wasPlayerStalled)
		{
			float num = Vector3.Distance(tick.position, this.tickInterpolator.EndPoint);
			if (num > 0.01f)
			{
				global::AntiHack.ResetTimer(this);
			}
			if (num > 0.5f)
			{
				base.ClientRPCPlayer<Vector3>(null, this, "ForcePositionTo", this.tickInterpolator.EndPoint);
			}
			return;
		}
		if (this.modelState == null || !this.modelState.flying || (!this.IsAdmin && !this.IsDeveloper))
		{
			float num2 = Vector3.Distance(tick.position, this.tickInterpolator.EndPoint);
			if (num2 > 5f)
			{
				global::AntiHack.ResetTimer(this);
				base.ClientRPCPlayer<Vector3>(null, this, "ForcePositionTo", this.tickInterpolator.EndPoint);
				return;
			}
		}
		this.tickInterpolator.AddPoint(tick.position);
		this.tickNeedsFinalizing = true;
	}

	// Token: 0x06000616 RID: 1558 RVA: 0x000251BC File Offset: 0x000233BC
	internal void UpdateRotationFromTick(PlayerTick tick)
	{
		if (tick.inputState == null)
		{
			return;
		}
		if (Vector3Ex.IsNaNOrInfinity(tick.inputState.aimAngles))
		{
			this.Kick("Kicked: Invalid Rotation");
			return;
		}
		this.tickViewAngles = tick.inputState.aimAngles;
		this.tickNeedsFinalizing = true;
	}

	// Token: 0x06000617 RID: 1559 RVA: 0x00025210 File Offset: 0x00023410
	public void UpdateEstimatedVelocity(Vector3 lastPos, Vector3 currentPos, float deltaTime)
	{
		this.estimatedVelocity = (currentPos - lastPos) / deltaTime;
		this.estimatedSpeed = this.estimatedVelocity.magnitude;
		this.estimatedSpeed2D = Vector3Ex.Magnitude2D(this.estimatedVelocity);
		if (this.estimatedSpeed < 0.01f)
		{
			this.estimatedSpeed = 0f;
		}
		if (this.estimatedSpeed2D < 0.01f)
		{
			this.estimatedSpeed2D = 0f;
		}
	}

	// Token: 0x06000618 RID: 1560 RVA: 0x0002528C File Offset: 0x0002348C
	private void FinalizeTick(float deltaTime)
	{
		this.tickDeltaTime += deltaTime;
		if (this.IsReceivingSnapshot)
		{
			return;
		}
		if (!this.tickNeedsFinalizing)
		{
			return;
		}
		this.tickNeedsFinalizing = false;
		using (TimeWarning.New("ModelState", 0.1f))
		{
			if (this.modelStateTick != null)
			{
				if (this.modelState != null)
				{
					if (this.modelStateTick.flying && !this.IsAdmin && !this.IsDeveloper)
					{
						global::AntiHack.NoteAdminHack(this);
					}
					if (ConVar.AntiHack.modelstate && this.TriggeredAntiHack(1f, float.PositiveInfinity))
					{
						this.modelStateTick.ducked = this.modelState.ducked;
					}
					this.modelState.ResetToPool();
					this.modelState = null;
				}
				this.modelState = this.modelStateTick;
				this.modelStateTick = null;
				this.UpdateModelState();
			}
		}
		using (TimeWarning.New("Transform", 0.1f))
		{
			this.UpdateEstimatedVelocity(this.tickInterpolator.StartPoint, this.tickInterpolator.EndPoint, this.tickDeltaTime);
			bool flag = this.tickInterpolator.StartPoint != this.tickInterpolator.EndPoint;
			bool flag2 = Vector3.Angle(this.tickViewAngles, this.viewAngles) > 0.01f;
			if (flag)
			{
				if (global::AntiHack.ValidateMove(this, this.tickInterpolator, this.tickDeltaTime))
				{
					base.transform.position = this.tickInterpolator.EndPoint;
					global::AntiHack.FadeViolations(this, this.tickDeltaTime);
				}
				else
				{
					flag = false;
					base.ClientRPCPlayer<Vector3>(null, this, "ForcePositionTo", base.transform.position);
				}
			}
			this.tickInterpolator.Reset(base.transform.position);
			if (flag2)
			{
				this.viewAngles = this.tickViewAngles;
				base.transform.hasChanged = true;
			}
			if (flag || flag2)
			{
				this.eyes.NetworkUpdate(Quaternion.Euler(this.viewAngles));
				base.NetworkPositionTick();
			}
		}
		using (TimeWarning.New("AntiHack.EnforceViolations", 0.1f))
		{
			global::AntiHack.EnforceViolations(this);
		}
		this.tickDeltaTime = 0f;
	}

	// Token: 0x06000619 RID: 1561 RVA: 0x00025540 File Offset: 0x00023740
	public bool IsWounded()
	{
		return this.HasPlayerFlag(global::BasePlayer.PlayerFlags.Wounded);
	}

	// Token: 0x17000056 RID: 86
	// (get) Token: 0x0600061A RID: 1562 RVA: 0x0002554C File Offset: 0x0002374C
	public float secondsSinceWoundedStarted
	{
		get
		{
			return UnityEngine.Time.realtimeSinceStartup - this.woundedStartTime;
		}
	}

	// Token: 0x0600061B RID: 1563 RVA: 0x0002555C File Offset: 0x0002375C
	private bool WoundInsteadOfDying(global::HitInfo info)
	{
		if (this.IsWounded())
		{
			return false;
		}
		if (!this.EligibleForWounding(info))
		{
			return false;
		}
		this.lastWoundedTime = UnityEngine.Time.realtimeSinceStartup;
		base.health = (float)Random.Range(2, 6);
		this.metabolism.bleeding.value = 0f;
		this.StartWounded();
		return true;
	}

	// Token: 0x0600061C RID: 1564 RVA: 0x000255BC File Offset: 0x000237BC
	public virtual bool EligibleForWounding(global::HitInfo info)
	{
		object obj = Interface.CallHook("CanBeWounded", new object[]
		{
			this,
			info
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		if (!ConVar.Server.woundingenabled)
		{
			return false;
		}
		if (this.IsSleeping())
		{
			return false;
		}
		if (this.isMounted)
		{
			return false;
		}
		if (info == null)
		{
			return false;
		}
		if (UnityEngine.Time.realtimeSinceStartup - this.lastWoundedTime < 60f)
		{
			return false;
		}
		if (info.WeaponPrefab is global::BaseMelee)
		{
			return true;
		}
		if (info.WeaponPrefab is global::BaseProjectile)
		{
			return !info.isHeadshot;
		}
		Rust.DamageType majorityDamageType = info.damageTypes.GetMajorityDamageType();
		return majorityDamageType != Rust.DamageType.Suicide && (majorityDamageType == Rust.DamageType.Fall || majorityDamageType == Rust.DamageType.Bite || majorityDamageType == Rust.DamageType.Bleeding || majorityDamageType == Rust.DamageType.Hunger || majorityDamageType == Rust.DamageType.Thirst || majorityDamageType == Rust.DamageType.Poison);
	}

	// Token: 0x0600061D RID: 1565 RVA: 0x000256B4 File Offset: 0x000238B4
	public void StartWounded()
	{
		if (this.IsWounded())
		{
			return;
		}
		if (Interface.CallHook("OnPlayerWound", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		this.stats.Add("wounded", 1, global::Stats.Steam);
		this.woundedDuration = Random.Range(40f, 50f);
		this.woundedStartTime = UnityEngine.Time.realtimeSinceStartup;
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Wounded, true);
		base.SendNetworkUpdateImmediate(false);
		base.Invoke(new Action(this.WoundingTick), 1f);
	}

	// Token: 0x0600061E RID: 1566 RVA: 0x00025740 File Offset: 0x00023940
	public void StopWounded()
	{
		if (Interface.CallHook("OnPlayerRecover", new object[]
		{
			this
		}) != null)
		{
			return;
		}
		this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Wounded, false);
		base.CancelInvoke(new Action(this.WoundingTick));
	}

	// Token: 0x0600061F RID: 1567 RVA: 0x00025784 File Offset: 0x00023984
	public void ProlongWounding(float delay)
	{
		this.woundedDuration = Mathf.Max(this.woundedDuration, Mathf.Min(this.secondsSinceWoundedStarted + delay, this.woundedDuration + delay));
	}

	// Token: 0x06000620 RID: 1568 RVA: 0x000257AC File Offset: 0x000239AC
	private void WoundingTick()
	{
		using (TimeWarning.New("WoundingTick", 0.1f))
		{
			if (!this.IsDead())
			{
				if (this.secondsSinceWoundedStarted >= this.woundedDuration)
				{
					if (Random.Range(0, 100) < 20)
					{
						if (Interface.CallHook("OnPlayerRecover", new object[]
						{
							this
						}) != null)
						{
							return;
						}
						this.SetPlayerFlag(global::BasePlayer.PlayerFlags.Wounded, false);
					}
					else
					{
						this.Die(null);
					}
				}
				else
				{
					base.Invoke(new Action(this.WoundingTick), 1f);
				}
			}
		}
	}

	// Token: 0x06000621 RID: 1569 RVA: 0x00025868 File Offset: 0x00023A68
	private bool WoundingCausingImmportality()
	{
		return this.IsWounded() && this.secondsSinceWoundedStarted <= 0.25f;
	}

	// Token: 0x04000204 RID: 516
	[Header("BasePlayer")]
	public global::GameObjectRef fallDamageEffect;

	// Token: 0x04000205 RID: 517
	[InspectorFlags]
	public global::BasePlayer.PlayerFlags playerFlags;

	// Token: 0x04000206 RID: 518
	[NonSerialized]
	public global::PlayerEyes eyes;

	// Token: 0x04000207 RID: 519
	[NonSerialized]
	public global::PlayerInventory inventory;

	// Token: 0x04000208 RID: 520
	[NonSerialized]
	public global::PlayerBlueprints blueprints;

	// Token: 0x04000209 RID: 521
	[NonSerialized]
	public global::PlayerMetabolism metabolism;

	// Token: 0x0400020A RID: 522
	[NonSerialized]
	public global::PlayerInput input;

	// Token: 0x0400020B RID: 523
	[NonSerialized]
	public global::BaseMovement movement;

	// Token: 0x0400020C RID: 524
	[NonSerialized]
	public global::BaseCollision collision;

	// Token: 0x0400020D RID: 525
	public global::PlayerBelt Belt;

	// Token: 0x0400020E RID: 526
	[NonSerialized]
	private Collider triggerCollider;

	// Token: 0x0400020F RID: 527
	[NonSerialized]
	private Rigidbody physicsRigidbody;

	// Token: 0x04000210 RID: 528
	[NonSerialized]
	public ulong userID;

	// Token: 0x04000211 RID: 529
	[NonSerialized]
	public string UserIDString;

	// Token: 0x04000212 RID: 530
	protected string _displayName;

	// Token: 0x04000213 RID: 531
	private global::ProtectionProperties cachedProtection;

	// Token: 0x04000214 RID: 532
	private const int displayNameMaxLength = 32;

	// Token: 0x04000215 RID: 533
	public bool clothingBlocksAiming;

	// Token: 0x04000216 RID: 534
	public float clothingMoveSpeedReduction;

	// Token: 0x04000217 RID: 535
	[NonSerialized]
	public bool isInAir;

	// Token: 0x04000218 RID: 536
	[NonSerialized]
	public bool isOnPlayer;

	// Token: 0x04000219 RID: 537
	[NonSerialized]
	public float violationLevel;

	// Token: 0x0400021A RID: 538
	[NonSerialized]
	public float lastViolationTime;

	// Token: 0x0400021B RID: 539
	[NonSerialized]
	public float lastAdminCheatTime;

	// Token: 0x0400021C RID: 540
	[NonSerialized]
	public global::AntiHackType lastViolationType;

	// Token: 0x0400021D RID: 541
	[NonSerialized]
	public float speedhackDeltaTime;

	// Token: 0x0400021E RID: 542
	[NonSerialized]
	public float speedhackDistance;

	// Token: 0x0400021F RID: 543
	[NonSerialized]
	public int speedhackTickets;

	// Token: 0x04000220 RID: 544
	[NonSerialized]
	public Queue<float> speedhackHistory = new Queue<float>();

	// Token: 0x04000221 RID: 545
	[NonSerialized]
	public float flyhackDistanceVertical;

	// Token: 0x04000222 RID: 546
	[NonSerialized]
	public float flyhackDistanceHorizontal;

	// Token: 0x04000223 RID: 547
	[NonSerialized]
	public global::PlayerModel playerModel;

	// Token: 0x04000224 RID: 548
	private const float drinkRange = 1.5f;

	// Token: 0x04000225 RID: 549
	private const float drinkMovementSpeed = 0.1f;

	// Token: 0x04000226 RID: 550
	[NonSerialized]
	private global::BasePlayer.NetworkQueueList[] networkQueue = new global::BasePlayer.NetworkQueueList[]
	{
		new global::BasePlayer.NetworkQueueList(),
		new global::BasePlayer.NetworkQueueList()
	};

	// Token: 0x04000227 RID: 551
	[NonSerialized]
	private global::BasePlayer.NetworkQueueList SnapshotQueue = new global::BasePlayer.NetworkQueueList();

	// Token: 0x04000228 RID: 552
	[NonSerialized]
	private bool lightsOn = true;

	// Token: 0x04000229 RID: 553
	[NonSerialized]
	public ModelState modelState = new ModelState
	{
		onground = true
	};

	// Token: 0x0400022A RID: 554
	[NonSerialized]
	private ModelState modelStateTick;

	// Token: 0x0400022B RID: 555
	[NonSerialized]
	private bool wantsSendModelState;

	// Token: 0x0400022C RID: 556
	[NonSerialized]
	private float nextModelStateUpdate;

	// Token: 0x0400022D RID: 557
	[NonSerialized]
	private global::EntityRef mounted;

	// Token: 0x0400022E RID: 558
	private Dictionary<int, global::BasePlayer.FiredProjectile> firedProjectiles = new Dictionary<int, global::BasePlayer.FiredProjectile>();

	// Token: 0x0400022F RID: 559
	[NonSerialized]
	public global::PlayerStatistics stats;

	// Token: 0x04000230 RID: 560
	[NonSerialized]
	public uint svActiveItemID;

	// Token: 0x04000231 RID: 561
	[NonSerialized]
	public float NextChatTime;

	// Token: 0x04000232 RID: 562
	[NonSerialized]
	public float nextSuicideTime;

	// Token: 0x04000238 RID: 568
	protected Vector3 viewAngles = default(Vector3);

	// Token: 0x04000239 RID: 569
	private float lastPlayerTick;

	// Token: 0x0400023A RID: 570
	private const float playerTickRate = 0.0625f;

	// Token: 0x0400023B RID: 571
	public static List<global::BasePlayer> activePlayerList = new List<global::BasePlayer>();

	// Token: 0x0400023C RID: 572
	public static List<global::BasePlayer> sleepingPlayerList = new List<global::BasePlayer>();

	// Token: 0x0400023D RID: 573
	private float cachedCraftLevel;

	// Token: 0x0400023E RID: 574
	private float nextCheckTime;

	// Token: 0x0400023F RID: 575
	[NonSerialized]
	public PlayerLifeStory lifeStory;

	// Token: 0x04000240 RID: 576
	[NonSerialized]
	public PlayerLifeStory previousLifeStory;

	// Token: 0x04000241 RID: 577
	private int SpectateOffset = 1000000;

	// Token: 0x04000242 RID: 578
	private string spectateFilter = string.Empty;

	// Token: 0x04000243 RID: 579
	private float lastUpdateTime = float.NegativeInfinity;

	// Token: 0x04000244 RID: 580
	private float cachedThreatLevel;

	// Token: 0x04000245 RID: 581
	[NonSerialized]
	public global::InputState serverInput = new global::InputState();

	// Token: 0x04000246 RID: 582
	[NonSerialized]
	private float lastTickTime;

	// Token: 0x04000247 RID: 583
	[NonSerialized]
	private float lastStallTime;

	// Token: 0x04000248 RID: 584
	[NonSerialized]
	private float lastInputTime;

	// Token: 0x04000249 RID: 585
	private PlayerTick lastReceivedTick = new PlayerTick();

	// Token: 0x0400024A RID: 586
	private float tickDeltaTime;

	// Token: 0x0400024B RID: 587
	private bool tickNeedsFinalizing;

	// Token: 0x0400024C RID: 588
	private Vector3 tickViewAngles;

	// Token: 0x0400024D RID: 589
	private global::TickInterpolator tickInterpolator = new global::TickInterpolator();

	// Token: 0x0400024E RID: 590
	private float woundedDuration;

	// Token: 0x0400024F RID: 591
	private float woundedStartTime;

	// Token: 0x04000250 RID: 592
	private float lastWoundedTime = float.NegativeInfinity;

	// Token: 0x04000255 RID: 597
	[NonSerialized]
	public IPlayer IPlayer;

	// Token: 0x02000043 RID: 67
	public enum CameraMode
	{
		// Token: 0x04000257 RID: 599
		FirstPerson,
		// Token: 0x04000258 RID: 600
		ThirdPerson,
		// Token: 0x04000259 RID: 601
		Eyes
	}

	// Token: 0x02000044 RID: 68
	public enum NetworkQueue
	{
		// Token: 0x0400025B RID: 603
		Update,
		// Token: 0x0400025C RID: 604
		UpdateDistance,
		// Token: 0x0400025D RID: 605
		Count
	}

	// Token: 0x02000045 RID: 69
	private class NetworkQueueList
	{
		// Token: 0x0600062A RID: 1578 RVA: 0x0002595C File Offset: 0x00023B5C
		public bool Contains(global::BaseNetworkable ent)
		{
			return this.queueInternal.Contains(ent);
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x0002596C File Offset: 0x00023B6C
		public void Add(global::BaseNetworkable ent)
		{
			if (!this.Contains(ent))
			{
				this.queueInternal.Add(ent);
			}
			this.MaxLength = Mathf.Max(this.MaxLength, this.queueInternal.Count);
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x000259A4 File Offset: 0x00023BA4
		public void Add(global::BaseNetworkable[] ent)
		{
			foreach (global::BaseNetworkable ent2 in ent)
			{
				this.Add(ent2);
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600062D RID: 1581 RVA: 0x000259D4 File Offset: 0x00023BD4
		public int Length
		{
			get
			{
				return this.queueInternal.Count;
			}
		}

		// Token: 0x0600062E RID: 1582 RVA: 0x000259E4 File Offset: 0x00023BE4
		public void Clear(Group group)
		{
			using (TimeWarning.New("NetworkQueueList.Clear", 0.1f))
			{
				if (group != null)
				{
					if (!group.isGlobal)
					{
						this.queueInternal.RemoveWhere((global::BaseNetworkable x) => x == null || x.net == null || x.net.group == null || x.net.group == group);
					}
				}
				else
				{
					this.queueInternal.RemoveWhere((global::BaseNetworkable x) => x == null || x.net == null || x.net.group == null || !x.net.group.isGlobal);
				}
			}
		}

		// Token: 0x0400025E RID: 606
		public HashSet<global::BaseNetworkable> queueInternal = new HashSet<global::BaseNetworkable>();

		// Token: 0x0400025F RID: 607
		public int MaxLength;
	}

	// Token: 0x02000047 RID: 71
	[Flags]
	public enum PlayerFlags
	{
		// Token: 0x04000263 RID: 611
		Unused1 = 1,
		// Token: 0x04000264 RID: 612
		Unused2 = 2,
		// Token: 0x04000265 RID: 613
		IsAdmin = 4,
		// Token: 0x04000266 RID: 614
		ReceivingSnapshot = 8,
		// Token: 0x04000267 RID: 615
		Sleeping = 16,
		// Token: 0x04000268 RID: 616
		Spectating = 32,
		// Token: 0x04000269 RID: 617
		Wounded = 64,
		// Token: 0x0400026A RID: 618
		IsDeveloper = 128,
		// Token: 0x0400026B RID: 619
		Connected = 256,
		// Token: 0x0400026C RID: 620
		VoiceMuted = 512,
		// Token: 0x0400026D RID: 621
		ThirdPersonViewmode = 1024,
		// Token: 0x0400026E RID: 622
		EyesViewmode = 2048,
		// Token: 0x0400026F RID: 623
		ChatMute = 4096,
		// Token: 0x04000270 RID: 624
		NoSprint = 8192,
		// Token: 0x04000271 RID: 625
		Aiming = 16384,
		// Token: 0x04000272 RID: 626
		DisplaySash = 32768,
		// Token: 0x04000273 RID: 627
		Relaxed = 65536,
		// Token: 0x04000274 RID: 628
		Workbench1 = 1048576,
		// Token: 0x04000275 RID: 629
		Workbench2 = 2097152,
		// Token: 0x04000276 RID: 630
		Workbench3 = 4194304
	}

	// Token: 0x02000048 RID: 72
	private struct FiredProjectile
	{
		// Token: 0x04000277 RID: 631
		public global::ItemDefinition itemDef;

		// Token: 0x04000278 RID: 632
		public global::ItemModProjectile itemMod;

		// Token: 0x04000279 RID: 633
		public global::Projectile projectilePrefab;

		// Token: 0x0400027A RID: 634
		public float firedTime;

		// Token: 0x0400027B RID: 635
		public global::AttackEntity weaponSource;

		// Token: 0x0400027C RID: 636
		public global::AttackEntity weaponPrefab;

		// Token: 0x0400027D RID: 637
		public global::Projectile.Modifier projectileModifier;

		// Token: 0x0400027E RID: 638
		public global::Item pickupItem;

		// Token: 0x0400027F RID: 639
		public float integrity;

		// Token: 0x04000280 RID: 640
		public Vector3 origin;

		// Token: 0x04000281 RID: 641
		public Vector3 position;

		// Token: 0x04000282 RID: 642
		public Vector3 velocity;
	}

	// Token: 0x02000049 RID: 73
	public class SpawnPoint
	{
		// Token: 0x04000283 RID: 643
		public Vector3 pos;

		// Token: 0x04000284 RID: 644
		public Quaternion rot;
	}
}
