﻿using System;

// Token: 0x020005E5 RID: 1509
public enum WaterType
{
	// Token: 0x040019EA RID: 6634
	Ocean = 1,
	// Token: 0x040019EB RID: 6635
	River,
	// Token: 0x040019EC RID: 6636
	Lake = 4
}
