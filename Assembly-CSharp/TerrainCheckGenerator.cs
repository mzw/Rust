﻿using System;
using UnityEngine;

// Token: 0x0200055C RID: 1372
public class TerrainCheckGenerator : MonoBehaviour, IEditorComponent
{
	// Token: 0x040017D2 RID: 6098
	public float PlacementRadius = 32f;

	// Token: 0x040017D3 RID: 6099
	public float PlacementPadding;

	// Token: 0x040017D4 RID: 6100
	public float PlacementFade = 16f;

	// Token: 0x040017D5 RID: 6101
	public float PlacementDistance = 8f;

	// Token: 0x040017D6 RID: 6102
	public float CheckExtentsMin = 8f;

	// Token: 0x040017D7 RID: 6103
	public float CheckExtentsMax = 16f;
}
