﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000715 RID: 1813
public class TweakUIToggle : MonoBehaviour
{
	// Token: 0x06002279 RID: 8825 RVA: 0x000C0EF8 File Offset: 0x000BF0F8
	protected void Awake()
	{
		this.conVar = ConsoleSystem.Index.Client.Find(this.convarName);
		if (this.conVar != null)
		{
			this.UpdateToggleState();
		}
		else
		{
			Debug.LogWarning("Tweak Toggle Convar Missing: " + this.convarName);
		}
	}

	// Token: 0x0600227A RID: 8826 RVA: 0x000C0F38 File Offset: 0x000BF138
	protected void OnEnable()
	{
		this.UpdateToggleState();
	}

	// Token: 0x0600227B RID: 8827 RVA: 0x000C0F40 File Offset: 0x000BF140
	public void OnToggleChanged()
	{
		this.UpdateConVar();
	}

	// Token: 0x0600227C RID: 8828 RVA: 0x000C0F48 File Offset: 0x000BF148
	private void UpdateConVar()
	{
		if (this.conVar == null)
		{
			return;
		}
		bool flag = this.toggleControl.isOn;
		if (this.inverse)
		{
			flag = !flag;
		}
		if (this.conVar.AsBool == flag)
		{
			return;
		}
		this.conVar.Set(flag);
	}

	// Token: 0x0600227D RID: 8829 RVA: 0x000C0F9C File Offset: 0x000BF19C
	private void UpdateToggleState()
	{
		if (this.conVar == null)
		{
			return;
		}
		bool flag = this.conVar.AsBool;
		if (this.inverse)
		{
			flag = !flag;
		}
		this.toggleControl.isOn = flag;
	}

	// Token: 0x04001EE5 RID: 7909
	public Toggle toggleControl;

	// Token: 0x04001EE6 RID: 7910
	public string convarName = "effects.motionblur";

	// Token: 0x04001EE7 RID: 7911
	public bool inverse;

	// Token: 0x04001EE8 RID: 7912
	internal ConsoleSystem.Command conVar;
}
