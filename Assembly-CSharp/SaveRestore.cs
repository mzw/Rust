﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ConVar;
using Facepunch;
using Facepunch.Math;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x0200047F RID: 1151
public class SaveRestore : SingletonComponent<global::SaveRestore>
{
	// Token: 0x060018F6 RID: 6390 RVA: 0x0008C628 File Offset: 0x0008A828
	public static IEnumerator Save(string strFilename, bool AndWait = false)
	{
		if (Application.isQuitting)
		{
			yield break;
		}
		Stopwatch timerSerialize = new Stopwatch();
		Stopwatch timerWrite = new Stopwatch();
		Stopwatch timerDisk = new Stopwatch();
		Stopwatch timerTotal = new Stopwatch();
		int iEnts = 0;
		Stopwatch sw = Stopwatch.StartNew();
		foreach (global::BaseEntity entity in global::BaseEntity.saveList.ToArray<global::BaseEntity>())
		{
			if (!(entity == null) && entity.IsValid())
			{
				try
				{
					entity.GetSaveCache();
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
				}
				if (sw.Elapsed.TotalMilliseconds > 5.0)
				{
					if (!AndWait)
					{
						yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
					}
					sw.Reset();
					sw.Start();
				}
			}
		}
		global::SaveRestore.SaveBuffer.Position = 0L;
		global::SaveRestore.SaveBuffer.SetLength(0L);
		timerTotal.Start();
		using (TimeWarning.New("Save", 100L))
		{
			BinaryWriter writer = new BinaryWriter(global::SaveRestore.SaveBuffer);
			writer.Write(83);
			writer.Write(65);
			writer.Write(86);
			writer.Write(82);
			writer.Write(68);
			writer.Write(Epoch.FromDateTime(global::SaveRestore.SaveCreatedTime));
			writer.Write(158u);
			default(global::BaseNetworkable.SaveInfo).forDisk = true;
			if (!AndWait)
			{
				yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
			}
			foreach (global::BaseEntity baseEntity in global::BaseEntity.saveList)
			{
				if (baseEntity == null || baseEntity.IsDestroyed)
				{
					Debug.LogWarning("Entity is NULL but is still in saveList - not destroyed properly? " + baseEntity, baseEntity);
				}
				else
				{
					MemoryStream memoryStream = null;
					try
					{
						timerSerialize.Start();
						memoryStream = baseEntity.GetSaveCache();
						timerSerialize.Stop();
					}
					catch (Exception ex2)
					{
						Debug.LogException(ex2);
					}
					if (memoryStream == null || memoryStream.Length <= 0L)
					{
						Debug.LogWarningFormat("Skipping saving entity {0} - because {1}", new object[]
						{
							baseEntity,
							(memoryStream != null) ? "savecache is 0" : "savecache is null"
						});
					}
					else
					{
						timerWrite.Start();
						writer.Write((uint)memoryStream.Length);
						writer.Write(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
						timerWrite.Stop();
						iEnts++;
					}
				}
			}
		}
		timerTotal.Stop();
		if (!AndWait)
		{
			yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		}
		timerDisk.Start();
		try
		{
			if (File.Exists(strFilename + ".new"))
			{
				File.Delete(strFilename + ".new");
			}
			try
			{
				File.WriteAllBytes(strFilename + ".new", global::SaveRestore.SaveBuffer.ToArray());
			}
			catch (Exception arg)
			{
				Debug.LogWarning("Couldn't write save file! We got an exception: " + arg);
				if (File.Exists(strFilename + ".new"))
				{
					File.Delete(strFilename + ".new");
				}
			}
			if (File.Exists(strFilename))
			{
				File.Delete(strFilename);
			}
			File.Move(strFilename + ".new", strFilename);
		}
		catch (Exception arg2)
		{
			Debug.LogWarning("Error when saving to disk: " + arg2);
		}
		timerDisk.Stop();
		Debug.LogFormat("Saved {0} ents, serialization({1}), write({2}), disk({3}) totalstall({4}).", new object[]
		{
			iEnts.ToString("N0"),
			timerSerialize.Elapsed.TotalSeconds.ToString("0.00"),
			timerWrite.Elapsed.TotalSeconds.ToString("0.00"),
			timerDisk.Elapsed.TotalSeconds.ToString("0.00"),
			timerTotal.Elapsed.TotalSeconds.ToString("0.00")
		});
		yield break;
	}

	// Token: 0x060018F7 RID: 6391 RVA: 0x0008C64C File Offset: 0x0008A84C
	private void Start()
	{
		base.StartCoroutine(this.SaveRegularly());
	}

	// Token: 0x060018F8 RID: 6392 RVA: 0x0008C65C File Offset: 0x0008A85C
	private IEnumerator SaveRegularly()
	{
		for (;;)
		{
			yield return UnityEngine.CoroutineEx.waitForSeconds((float)ConVar.Server.saveinterval);
			if (this.timedSave && this.timedSavePause <= 0)
			{
				yield return base.StartCoroutine(this.DoAutomatedSave(false));
			}
		}
		yield break;
	}

	// Token: 0x060018F9 RID: 6393 RVA: 0x0008C678 File Offset: 0x0008A878
	[DebuggerHidden]
	private IEnumerator DoAutomatedSave(bool AndWait = false)
	{
		Interface.CallHook("OnServerSave", null);
		global::SaveRestore.<DoAutomatedSave>c__Iterator2 <DoAutomatedSave>c__Iterator = new global::SaveRestore.<DoAutomatedSave>c__Iterator2();
		<DoAutomatedSave>c__Iterator.AndWait = AndWait;
		<DoAutomatedSave>c__Iterator.$this = this;
		return <DoAutomatedSave>c__Iterator;
	}

	// Token: 0x060018FA RID: 6394 RVA: 0x0008C6A8 File Offset: 0x0008A8A8
	public static bool Save(bool AndWait)
	{
		if (SingletonComponent<global::SaveRestore>.Instance == null)
		{
			return false;
		}
		if (global::SaveRestore.IsSaving)
		{
			return false;
		}
		IEnumerator enumerator = SingletonComponent<global::SaveRestore>.Instance.DoAutomatedSave(true);
		while (enumerator.MoveNext())
		{
		}
		return true;
	}

	// Token: 0x170001B2 RID: 434
	// (get) Token: 0x060018FB RID: 6395 RVA: 0x0008C6F0 File Offset: 0x0008A8F0
	public static string SaveFileName
	{
		get
		{
			return global::World.FileName + ".sav";
		}
	}

	// Token: 0x060018FC RID: 6396 RVA: 0x0008C704 File Offset: 0x0008A904
	internal static void ClearMapEntities()
	{
		global::BaseEntity[] array = Object.FindObjectsOfType<global::BaseEntity>();
		if (array.Length > 0)
		{
			DebugEx.Log("Destroying " + array.Length + " old entities", 0);
			Stopwatch stopwatch = Stopwatch.StartNew();
			for (int i = 0; i < array.Length; i++)
			{
				array[i].Kill(global::BaseNetworkable.DestroyMode.None);
				if (stopwatch.Elapsed.TotalMilliseconds > 2000.0)
				{
					stopwatch.Reset();
					stopwatch.Start();
					DebugEx.Log(string.Concat(new object[]
					{
						"\t",
						i + 1,
						" / ",
						array.Length
					}), 0);
				}
			}
			global::ItemManager.Heartbeat();
			DebugEx.Log("\tdone.", 0);
		}
	}

	// Token: 0x060018FD RID: 6397 RVA: 0x0008C7D0 File Offset: 0x0008A9D0
	public static bool Load(string strFilename = "", bool allowOutOfDateSaves = false)
	{
		global::SaveRestore.SaveCreatedTime = DateTime.UtcNow;
		bool result;
		try
		{
			if (strFilename == string.Empty)
			{
				strFilename = ConVar.Server.rootFolder + "/" + global::SaveRestore.SaveFileName;
			}
			if (!File.Exists(strFilename))
			{
				if (!File.Exists("TestSaves/" + strFilename))
				{
					Debug.LogWarning("Couldn't load " + strFilename + " - file doesn't exist");
					Interface.CallHook("OnNewSave", new object[]
					{
						strFilename
					});
					return false;
				}
				strFilename = "TestSaves/" + strFilename;
			}
			Dictionary<global::BaseEntity, ProtoBuf.Entity> dictionary = new Dictionary<global::BaseEntity, ProtoBuf.Entity>();
			using (FileStream fileStream = File.OpenRead(strFilename))
			{
				using (BinaryReader binaryReader = new BinaryReader(fileStream))
				{
					global::SaveRestore.SaveCreatedTime = File.GetCreationTime(strFilename);
					if ((int)binaryReader.ReadSByte() != 83 || (int)binaryReader.ReadSByte() != 65 || (int)binaryReader.ReadSByte() != 86 || (int)binaryReader.ReadSByte() != 82)
					{
						Debug.LogWarning("Invalid save (missing header)");
						return false;
					}
					if (binaryReader.PeekChar() == 68)
					{
						binaryReader.ReadChar();
						global::SaveRestore.SaveCreatedTime = Epoch.ToDateTime(binaryReader.ReadInt32());
					}
					if (binaryReader.ReadUInt32() != 158u)
					{
						if (allowOutOfDateSaves)
						{
							Debug.LogWarning("This save is from an older (possibly incompatible) version!");
						}
						else
						{
							Debug.LogWarning("This save is from an older version. It might not load properly.");
						}
					}
					global::SaveRestore.ClearMapEntities();
					Assert.IsTrue(global::BaseEntity.saveList.Count == 0, "BaseEntity.saveList isn't empty!");
					Network.Net.sv.Reset();
					Application.isLoadingSave = true;
					HashSet<uint> hashSet = new HashSet<uint>();
					while (fileStream.Position < fileStream.Length)
					{
						Facepunch.RCon.Update();
						uint num = binaryReader.ReadUInt32();
						ProtoBuf.Entity entData = ProtoBuf.Entity.DeserializeLength(fileStream, (int)num);
						if (entData.basePlayer != null && dictionary.Any((KeyValuePair<global::BaseEntity, ProtoBuf.Entity> x) => x.Value.basePlayer != null && x.Value.basePlayer.userid == entData.basePlayer.userid))
						{
							Debug.LogWarning(string.Concat(new object[]
							{
								"Skipping entity ",
								entData.baseNetworkable.uid,
								" - it's a player ",
								entData.basePlayer.userid,
								" who is in the save multiple times"
							}));
						}
						else if (entData.baseNetworkable.uid > 0u && hashSet.Contains(entData.baseNetworkable.uid))
						{
							Debug.LogWarning(string.Concat(new object[]
							{
								"Skipping entity ",
								entData.baseNetworkable.uid,
								" ",
								global::StringPool.Get(entData.baseNetworkable.prefabID),
								" - uid is used multiple times"
							}));
						}
						else
						{
							if (entData.baseNetworkable.uid > 0u)
							{
								hashSet.Add(entData.baseNetworkable.uid);
							}
							global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(global::StringPool.Get(entData.baseNetworkable.prefabID), entData.baseEntity.pos, Quaternion.Euler(entData.baseEntity.rot), true);
							if (baseEntity)
							{
								baseEntity.InitLoad(entData.baseNetworkable.uid);
								dictionary.Add(baseEntity, entData);
							}
						}
					}
				}
			}
			DebugEx.Log("Spawning " + dictionary.Count + " entities", 0);
			object obj = Interface.CallHook("OnSaveLoad", new object[]
			{
				dictionary
			});
			if (obj is bool)
			{
				return (bool)obj;
			}
			global::BaseNetworkable.LoadInfo info = default(global::BaseNetworkable.LoadInfo);
			info.fromDisk = true;
			Stopwatch stopwatch = Stopwatch.StartNew();
			int num2 = 0;
			foreach (KeyValuePair<global::BaseEntity, ProtoBuf.Entity> keyValuePair in dictionary)
			{
				global::BaseEntity key = keyValuePair.Key;
				if (!(key == null))
				{
					Facepunch.RCon.Update();
					info.msg = keyValuePair.Value;
					key.Spawn();
					key.Load(info);
					if (key.IsValid())
					{
						key.PostServerLoad();
						num2++;
						if (stopwatch.Elapsed.TotalMilliseconds > 2000.0)
						{
							stopwatch.Reset();
							stopwatch.Start();
							DebugEx.Log(string.Concat(new object[]
							{
								"\t",
								num2,
								" / ",
								dictionary.Count
							}), 0);
						}
					}
				}
			}
			DebugEx.Log("\tdone.", 0);
			if (SingletonComponent<global::SpawnHandler>.Instance)
			{
				DebugEx.Log("Enforcing SpawnPopulation Limits", 0);
				SingletonComponent<global::SpawnHandler>.Instance.EnforceLimits(false);
				DebugEx.Log("\tdone.", 0);
			}
			Application.isLoadingSave = false;
			result = true;
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Error loading save (" + strFilename + "): " + ex.Message);
			result = false;
		}
		return result;
	}

	// Token: 0x060018FE RID: 6398 RVA: 0x0008CDBC File Offset: 0x0008AFBC
	public static void GetSaveCache()
	{
		global::BaseEntity[] array = global::BaseEntity.saveList.ToArray<global::BaseEntity>();
		if (array.Length > 0)
		{
			DebugEx.Log("Initializing " + array.Length + " entity save caches", 0);
			Stopwatch stopwatch = Stopwatch.StartNew();
			for (int i = 0; i < array.Length; i++)
			{
				global::BaseEntity baseEntity = array[i];
				if (baseEntity.IsValid())
				{
					baseEntity.GetSaveCache();
					if (stopwatch.Elapsed.TotalMilliseconds > 2000.0)
					{
						stopwatch.Reset();
						stopwatch.Start();
						DebugEx.Log(string.Concat(new object[]
						{
							"\t",
							i + 1,
							" / ",
							array.Length
						}), 0);
					}
				}
			}
			DebugEx.Log("\tdone.", 0);
		}
	}

	// Token: 0x060018FF RID: 6399 RVA: 0x0008CE9C File Offset: 0x0008B09C
	public static void InitializeEntityLinks()
	{
		global::BaseEntity[] array = (from x in global::BaseNetworkable.serverEntities
		where x is global::BaseEntity
		select x as global::BaseEntity).ToArray<global::BaseEntity>();
		if (array.Length > 0)
		{
			DebugEx.Log("Initializing " + array.Length + " entity links", 0);
			Stopwatch stopwatch = Stopwatch.StartNew();
			for (int i = 0; i < array.Length; i++)
			{
				Facepunch.RCon.Update();
				array[i].RefreshEntityLinks();
				if (stopwatch.Elapsed.TotalMilliseconds > 2000.0)
				{
					stopwatch.Reset();
					stopwatch.Start();
					DebugEx.Log(string.Concat(new object[]
					{
						"\t",
						i + 1,
						" / ",
						array.Length
					}), 0);
				}
			}
			DebugEx.Log("\tdone.", 0);
		}
	}

	// Token: 0x06001900 RID: 6400 RVA: 0x0008CFB0 File Offset: 0x0008B1B0
	public static void InitializeEntitySupports()
	{
		if (!ConVar.Server.stability)
		{
			return;
		}
		global::StabilityEntity[] array = (from x in global::BaseNetworkable.serverEntities
		where x is global::StabilityEntity
		select x as global::StabilityEntity).ToArray<global::StabilityEntity>();
		if (array.Length > 0)
		{
			DebugEx.Log("Initializing " + array.Length + " stability supports", 0);
			Stopwatch stopwatch = Stopwatch.StartNew();
			for (int i = 0; i < array.Length; i++)
			{
				Facepunch.RCon.Update();
				array[i].InitializeSupports();
				if (stopwatch.Elapsed.TotalMilliseconds > 2000.0)
				{
					stopwatch.Reset();
					stopwatch.Start();
					DebugEx.Log(string.Concat(new object[]
					{
						"\t",
						i + 1,
						" / ",
						array.Length
					}), 0);
				}
			}
			DebugEx.Log("\tdone.", 0);
		}
	}

	// Token: 0x06001901 RID: 6401 RVA: 0x0008D0D0 File Offset: 0x0008B2D0
	public static void InitializeEntityConditionals()
	{
		global::BuildingBlock[] array = (from x in global::BaseNetworkable.serverEntities
		where x is global::BuildingBlock
		select x as global::BuildingBlock).ToArray<global::BuildingBlock>();
		if (array.Length > 0)
		{
			DebugEx.Log("Initializing " + array.Length + " conditional models", 0);
			Stopwatch stopwatch = Stopwatch.StartNew();
			for (int i = 0; i < array.Length; i++)
			{
				Facepunch.RCon.Update();
				array[i].UpdateSkin(true);
				if (stopwatch.Elapsed.TotalMilliseconds > 2000.0)
				{
					stopwatch.Reset();
					stopwatch.Start();
					DebugEx.Log(string.Concat(new object[]
					{
						"\t",
						i + 1,
						" / ",
						array.Length
					}), 0);
				}
			}
			DebugEx.Log("\tdone.", 0);
		}
	}

	// Token: 0x040013B0 RID: 5040
	public static bool IsSaving = false;

	// Token: 0x040013B1 RID: 5041
	public bool timedSave = true;

	// Token: 0x040013B2 RID: 5042
	public int timedSavePause;

	// Token: 0x040013B3 RID: 5043
	public static DateTime SaveCreatedTime;

	// Token: 0x040013B4 RID: 5044
	private static MemoryStream SaveBuffer = new MemoryStream(33554432);
}
