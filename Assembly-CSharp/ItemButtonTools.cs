﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020002F0 RID: 752
public class ItemButtonTools : MonoBehaviour
{
	// Token: 0x06001316 RID: 4886 RVA: 0x00070454 File Offset: 0x0006E654
	public void GiveSelf(int amount)
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Client, "inventory.giveid", new object[]
		{
			this.itemDef.itemid,
			amount
		});
	}

	// Token: 0x06001317 RID: 4887 RVA: 0x00070488 File Offset: 0x0006E688
	public void GiveArmed()
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Client, "inventory.givearm", new object[]
		{
			this.itemDef.itemid
		});
	}

	// Token: 0x06001318 RID: 4888 RVA: 0x000704B4 File Offset: 0x0006E6B4
	public void GiveBlueprint()
	{
	}

	// Token: 0x04000DCB RID: 3531
	public Image image;

	// Token: 0x04000DCC RID: 3532
	public global::ItemDefinition itemDef;
}
