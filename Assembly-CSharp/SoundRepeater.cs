﻿using System;
using UnityEngine;

// Token: 0x02000200 RID: 512
[RequireComponent(typeof(global::SoundPlayer))]
public class SoundRepeater : MonoBehaviour
{
	// Token: 0x04000A10 RID: 2576
	public float interval = 5f;

	// Token: 0x04000A11 RID: 2577
	public global::SoundPlayer player;
}
