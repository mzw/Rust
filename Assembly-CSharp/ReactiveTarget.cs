﻿using System;
using ConVar;
using Network;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000089 RID: 137
public class ReactiveTarget : global::DecayEntity
{
	// Token: 0x06000912 RID: 2322 RVA: 0x0003CF60 File Offset: 0x0003B160
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("ReactiveTarget.OnRpcMessage", 0.1f))
		{
			if (rpc == 2521335847u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Lower ");
				}
				using (TimeWarning.New("RPC_Lower", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Lower(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Lower");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 2526575221u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Reset ");
				}
				using (TimeWarning.New("RPC_Reset", 0.1f))
				{
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Reset(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_Reset");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000913 RID: 2323 RVA: 0x0003D21C File Offset: 0x0003B41C
	public void OnHitShared(global::HitInfo info)
	{
		if (this.IsKnockedDown())
		{
			return;
		}
		bool flag = info.HitBone == global::StringPool.Get("target_collider");
		bool flag2 = info.HitBone == global::StringPool.Get("target_collider_bullseye");
		if (!flag && !flag2)
		{
			return;
		}
		if (base.isServer)
		{
			float num = info.damageTypes.Total();
			if (flag2)
			{
				num *= 2f;
				global::Effect.server.Run(this.bullseyeEffect.resourcePath, this, global::StringPool.Get("target_collider_bullseye"), Vector3.zero, Vector3.zero, null, false);
			}
			this.knockdownHealth -= num;
			bool flag3 = this.knockdownHealth <= 0f;
			if (flag3)
			{
				global::Effect.server.Run(this.knockdownEffect.resourcePath, this, global::StringPool.Get("target_collider_bullseye"), Vector3.zero, Vector3.zero, null, false);
				base.SetFlag(global::BaseEntity.Flags.On, false, false);
				this.QueueReset();
				base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			}
			else
			{
				base.ClientRPC<uint>(null, "HitEffect", info.Initiator.net.ID);
			}
			this.Hurt(1f, Rust.DamageType.Suicide, info.Initiator, false);
		}
	}

	// Token: 0x06000914 RID: 2324 RVA: 0x0003D34C File Offset: 0x0003B54C
	public bool IsKnockedDown()
	{
		return !base.HasFlag(global::BaseEntity.Flags.On);
	}

	// Token: 0x06000915 RID: 2325 RVA: 0x0003D358 File Offset: 0x0003B558
	public override void OnAttacked(global::HitInfo info)
	{
		this.OnHitShared(info);
		base.OnAttacked(info);
	}

	// Token: 0x06000916 RID: 2326 RVA: 0x0003D368 File Offset: 0x0003B568
	public override bool CanPickup(global::BasePlayer player)
	{
		return base.CanPickup(player) && this.CanToggle();
	}

	// Token: 0x06000917 RID: 2327 RVA: 0x0003D380 File Offset: 0x0003B580
	public bool CanToggle()
	{
		return UnityEngine.Time.realtimeSinceStartup > this.lastToggleTime + 1f;
	}

	// Token: 0x06000918 RID: 2328 RVA: 0x0003D398 File Offset: 0x0003B598
	public void QueueReset()
	{
		base.Invoke(new Action(this.ResetTarget), 6f);
	}

	// Token: 0x06000919 RID: 2329 RVA: 0x0003D3B4 File Offset: 0x0003B5B4
	public void ResetTarget()
	{
		if (base.HasFlag(global::BaseEntity.Flags.On) || !this.CanToggle())
		{
			return;
		}
		base.CancelInvoke(new Action(this.ResetTarget));
		this.lastToggleTime = UnityEngine.Time.realtimeSinceStartup;
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		this.knockdownHealth = 100f;
	}

	// Token: 0x0600091A RID: 2330 RVA: 0x0003D40C File Offset: 0x0003B60C
	[global::BaseEntity.RPC_Server]
	public void RPC_Reset(global::BaseEntity.RPCMessage msg)
	{
		this.ResetTarget();
	}

	// Token: 0x0600091B RID: 2331 RVA: 0x0003D414 File Offset: 0x0003B614
	[global::BaseEntity.RPC_Server]
	public void RPC_Lower(global::BaseEntity.RPCMessage msg)
	{
		if (!base.HasFlag(global::BaseEntity.Flags.On) || !this.CanToggle())
		{
			return;
		}
		this.lastToggleTime = UnityEngine.Time.realtimeSinceStartup;
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
	}

	// Token: 0x04000430 RID: 1072
	public Animator myAnimator;

	// Token: 0x04000431 RID: 1073
	public global::GameObjectRef bullseyeEffect;

	// Token: 0x04000432 RID: 1074
	public global::GameObjectRef knockdownEffect;

	// Token: 0x04000433 RID: 1075
	private float lastToggleTime = float.NegativeInfinity;

	// Token: 0x04000434 RID: 1076
	private float knockdownHealth = 100f;
}
