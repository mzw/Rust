﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006A2 RID: 1698
public class BlueprintHeader : MonoBehaviour
{
	// Token: 0x06002132 RID: 8498 RVA: 0x000BBD80 File Offset: 0x000B9F80
	public void Setup(global::ItemCategory name, int unlocked, int total)
	{
		this.categoryName.text = name.ToString().ToUpper();
		this.unlockCount.text = string.Format("UNLOCKED {0}/{1}", unlocked, total);
	}

	// Token: 0x04001CC1 RID: 7361
	public Text categoryName;

	// Token: 0x04001CC2 RID: 7362
	public Text unlockCount;
}
