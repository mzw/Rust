﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch.Steamworks;
using Rust;
using UnityEngine;

// Token: 0x020004CA RID: 1226
public class ItemDefinition : MonoBehaviour
{
	// Token: 0x170001D1 RID: 465
	// (get) Token: 0x06001A89 RID: 6793 RVA: 0x000954F8 File Offset: 0x000936F8
	public Inventory.Definition[] skins2
	{
		get
		{
			if (this._skins2 != null)
			{
				return this._skins2;
			}
			if (Global.SteamServer != null && Global.SteamServer.Inventory.Definitions != null)
			{
				string prefabname = base.name;
				this._skins2 = (from x in Global.SteamServer.Inventory.Definitions
				where (x.GetStringProperty("itemshortname") == this.shortname || x.GetStringProperty("itemshortname") == prefabname) && !string.IsNullOrEmpty(x.GetStringProperty("workshopdownload"))
				select x).ToArray<Inventory.Definition>();
			}
			return this._skins2;
		}
	}

	// Token: 0x06001A8A RID: 6794 RVA: 0x00095580 File Offset: 0x00093780
	public void InvalidateWorkshopSkinCache()
	{
		this._skins2 = null;
	}

	// Token: 0x06001A8B RID: 6795 RVA: 0x0009558C File Offset: 0x0009378C
	public static ulong FindSkin(int itemID, int skinID)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(itemID);
		if (itemDefinition == null)
		{
			return 0UL;
		}
		Inventory.Definition definition = Global.SteamServer.Inventory.FindDefinition(skinID);
		if (definition != null)
		{
			ulong property = definition.GetProperty<ulong>("workshopdownload");
			if (property != 0UL)
			{
				string property2 = definition.GetProperty<string>("itemshortname");
				if (property2 == itemDefinition.shortname || property2 == itemDefinition.name)
				{
					return property;
				}
			}
		}
		for (int i = 0; i < itemDefinition.skins.Length; i++)
		{
			global::ItemSkinDirectory.Skin skin = itemDefinition.skins[i];
			if (skin.id == skinID)
			{
				return (ulong)((long)skinID);
			}
		}
		return 0UL;
	}

	// Token: 0x170001D2 RID: 466
	// (get) Token: 0x06001A8C RID: 6796 RVA: 0x00095650 File Offset: 0x00093850
	public global::ItemBlueprint Blueprint
	{
		get
		{
			return base.GetComponent<global::ItemBlueprint>();
		}
	}

	// Token: 0x06001A8D RID: 6797 RVA: 0x00095658 File Offset: 0x00093858
	public bool HasFlag(global::ItemDefinition.Flag f)
	{
		return (this.flags & f) == f;
	}

	// Token: 0x06001A8E RID: 6798 RVA: 0x00095668 File Offset: 0x00093868
	public void Initialize(List<global::ItemDefinition> itemList)
	{
		if (this.itemMods != null)
		{
			Debug.LogError("Item Definition Initializing twice: " + base.name);
		}
		this.skins = global::ItemSkinDirectory.ForItem(this);
		this.itemMods = base.GetComponentsInChildren<global::ItemMod>(true);
		foreach (global::ItemMod itemMod in this.itemMods)
		{
			itemMod.ModInit();
		}
		this.Children = (from x in itemList
		where x.Parent == this
		select x).ToArray<global::ItemDefinition>();
		this.isWearable = (base.GetComponent<global::ItemModWearable>() != null);
		this.isHoldable = (base.GetComponent<global::ItemModEntity>() != null);
		this.isUsable = (base.GetComponent<global::ItemModEntity>() != null || base.GetComponent<global::ItemModConsume>() != null);
	}

	// Token: 0x170001D3 RID: 467
	// (get) Token: 0x06001A8F RID: 6799 RVA: 0x0009573C File Offset: 0x0009393C
	// (set) Token: 0x06001A90 RID: 6800 RVA: 0x00095744 File Offset: 0x00093944
	public bool isWearable { get; private set; }

	// Token: 0x170001D4 RID: 468
	// (get) Token: 0x06001A91 RID: 6801 RVA: 0x00095750 File Offset: 0x00093950
	// (set) Token: 0x06001A92 RID: 6802 RVA: 0x00095758 File Offset: 0x00093958
	public bool isHoldable { get; private set; }

	// Token: 0x170001D5 RID: 469
	// (get) Token: 0x06001A93 RID: 6803 RVA: 0x00095764 File Offset: 0x00093964
	// (set) Token: 0x06001A94 RID: 6804 RVA: 0x0009576C File Offset: 0x0009396C
	public bool isUsable { get; private set; }

	// Token: 0x170001D6 RID: 470
	// (get) Token: 0x06001A95 RID: 6805 RVA: 0x00095778 File Offset: 0x00093978
	public bool HasSkins
	{
		get
		{
			return (this.skins2 != null && this.skins2.Length > 0) || (this.skins != null && this.skins.Length > 0);
		}
	}

	// Token: 0x170001D7 RID: 471
	// (get) Token: 0x06001A96 RID: 6806 RVA: 0x000957B4 File Offset: 0x000939B4
	// (set) Token: 0x06001A97 RID: 6807 RVA: 0x000957BC File Offset: 0x000939BC
	public bool CraftableWithSkin { get; private set; }

	// Token: 0x04001522 RID: 5410
	[ReadOnly]
	[Header("Item")]
	public int itemid;

	// Token: 0x04001523 RID: 5411
	[Tooltip("The shortname should be unique. A hash will be generated from it to identify the item type. If this name changes at any point it will make all saves incompatible")]
	public string shortname;

	// Token: 0x04001524 RID: 5412
	[Header("Appearance")]
	public global::Translate.Phrase displayName;

	// Token: 0x04001525 RID: 5413
	public global::Translate.Phrase displayDescription;

	// Token: 0x04001526 RID: 5414
	public Sprite iconSprite;

	// Token: 0x04001527 RID: 5415
	public global::ItemCategory category;

	// Token: 0x04001528 RID: 5416
	public global::ItemSelectionPanel selectionPanel;

	// Token: 0x04001529 RID: 5417
	[Header("Containment")]
	public int maxDraggable;

	// Token: 0x0400152A RID: 5418
	public global::ItemContainer.ContentsType itemType = global::ItemContainer.ContentsType.Generic;

	// Token: 0x0400152B RID: 5419
	public global::ItemDefinition.AmountType amountType;

	// Token: 0x0400152C RID: 5420
	[InspectorFlags]
	public global::ItemSlot occupySlots = global::ItemSlot.None;

	// Token: 0x0400152D RID: 5421
	public int stackable;

	// Token: 0x0400152E RID: 5422
	public bool quickDespawn;

	// Token: 0x0400152F RID: 5423
	[Header("Spawn Tables")]
	public Rarity rarity;

	// Token: 0x04001530 RID: 5424
	public bool spawnAsBlueprint;

	// Token: 0x04001531 RID: 5425
	[Header("Sounds")]
	public global::SoundDefinition inventorySelectSound;

	// Token: 0x04001532 RID: 5426
	public global::SoundDefinition inventoryGrabSound;

	// Token: 0x04001533 RID: 5427
	public global::SoundDefinition inventoryDropSound;

	// Token: 0x04001534 RID: 5428
	public global::SoundDefinition physImpactSoundDef;

	// Token: 0x04001535 RID: 5429
	public global::ItemDefinition.Condition condition;

	// Token: 0x04001536 RID: 5430
	[Header("Misc")]
	public bool hidden;

	// Token: 0x04001537 RID: 5431
	[InspectorFlags]
	public global::ItemDefinition.Flag flags;

	// Token: 0x04001538 RID: 5432
	[Tooltip("User can craft this item on any server if they have this steam item")]
	public global::SteamInventoryItem steamItem;

	// Token: 0x04001539 RID: 5433
	[Tooltip("Can only craft this item if the parent is craftable (tech tree)")]
	public global::ItemDefinition Parent;

	// Token: 0x0400153A RID: 5434
	public global::GameObjectRef worldModelPrefab;

	// Token: 0x0400153B RID: 5435
	[NonSerialized]
	public global::ItemMod[] itemMods;

	// Token: 0x0400153C RID: 5436
	public global::BaseEntity.TraitFlag Traits;

	// Token: 0x0400153D RID: 5437
	[NonSerialized]
	public global::ItemSkinDirectory.Skin[] skins;

	// Token: 0x0400153E RID: 5438
	[NonSerialized]
	private Inventory.Definition[] _skins2;

	// Token: 0x0400153F RID: 5439
	[Tooltip("Panel to show in the inventory menu when selected")]
	public GameObject panel;

	// Token: 0x04001544 RID: 5444
	[NonSerialized]
	public global::ItemDefinition[] Children = new global::ItemDefinition[0];

	// Token: 0x020004CB RID: 1227
	[Serializable]
	public struct Condition
	{
		// Token: 0x04001545 RID: 5445
		public bool enabled;

		// Token: 0x04001546 RID: 5446
		[Tooltip("The maximum condition this item type can have, new items will start with this value")]
		public float max;

		// Token: 0x04001547 RID: 5447
		[Tooltip("If false then item will destroy when condition reaches 0")]
		public bool repairable;

		// Token: 0x04001548 RID: 5448
		[Tooltip("If true, never lose max condition when repaired")]
		public bool maintainMaxCondition;

		// Token: 0x04001549 RID: 5449
		public global::ItemDefinition.Condition.WorldSpawnCondition foundCondition;

		// Token: 0x020004CC RID: 1228
		[Serializable]
		public class WorldSpawnCondition
		{
			// Token: 0x0400154A RID: 5450
			public float fractionMin = 1f;

			// Token: 0x0400154B RID: 5451
			public float fractionMax = 1f;
		}
	}

	// Token: 0x020004CD RID: 1229
	[Flags]
	public enum Flag
	{
		// Token: 0x0400154D RID: 5453
		NoDropping = 1,
		// Token: 0x0400154E RID: 5454
		NotStraightToBelt = 2
	}

	// Token: 0x020004CE RID: 1230
	public enum AmountType
	{
		// Token: 0x04001550 RID: 5456
		Count,
		// Token: 0x04001551 RID: 5457
		Millilitre,
		// Token: 0x04001552 RID: 5458
		Feet,
		// Token: 0x04001553 RID: 5459
		Genetics
	}
}
