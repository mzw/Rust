﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006B1 RID: 1713
public class ItemSplitter : MonoBehaviour
{
	// Token: 0x04001D0C RID: 7436
	public Slider slider;

	// Token: 0x04001D0D RID: 7437
	public Text textValue;

	// Token: 0x04001D0E RID: 7438
	public Text splitAmountText;
}
