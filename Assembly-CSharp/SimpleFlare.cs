﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020000BC RID: 188
public class SimpleFlare : global::BaseMonoBehaviour, IClientComponent
{
	// Token: 0x04000545 RID: 1349
	public bool timeShimmer;

	// Token: 0x04000546 RID: 1350
	public bool positionalShimmer;

	// Token: 0x04000547 RID: 1351
	public bool rotate;

	// Token: 0x04000548 RID: 1352
	public float fadeSpeed = 35f;

	// Token: 0x04000549 RID: 1353
	public Collider checkCollider;

	// Token: 0x0400054A RID: 1354
	public float maxVisibleDistance = 30f;

	// Token: 0x0400054B RID: 1355
	public bool lightScaled;

	// Token: 0x0400054C RID: 1356
	public bool alignToCameraViaScript;

	// Token: 0x0400054D RID: 1357
	protected float tickRate = 0.33f;

	// Token: 0x0400054E RID: 1358
	private Vector3 fullSize;

	// Token: 0x0400054F RID: 1359
	public bool faceCameraPos = true;

	// Token: 0x04000550 RID: 1360
	public bool billboardViaShader;

	// Token: 0x04000551 RID: 1361
	private float artificialLightExposure;

	// Token: 0x04000552 RID: 1362
	private float privateRand;

	// Token: 0x04000553 RID: 1363
	private List<global::BasePlayer> players;

	// Token: 0x04000554 RID: 1364
	private Renderer myRenderer;

	// Token: 0x04000555 RID: 1365
	private static MaterialPropertyBlock block;

	// Token: 0x04000556 RID: 1366
	public float dotMin = -1f;

	// Token: 0x04000557 RID: 1367
	public float dotMax = -1f;
}
