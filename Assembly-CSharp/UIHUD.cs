﻿using System;

// Token: 0x0200069A RID: 1690
public class UIHUD : SingletonComponent<global::UIHUD>, global::IUIScreen
{
	// Token: 0x04001C91 RID: 7313
	public global::UIChat chatPanel;

	// Token: 0x04001C92 RID: 7314
	public global::HudElement Hunger;

	// Token: 0x04001C93 RID: 7315
	public global::HudElement Thirst;

	// Token: 0x04001C94 RID: 7316
	public global::HudElement Health;

	// Token: 0x04001C95 RID: 7317
	public global::HudElement PendingHealth;
}
