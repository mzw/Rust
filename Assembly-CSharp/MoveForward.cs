﻿using System;
using UnityEngine;

// Token: 0x02000751 RID: 1873
public class MoveForward : MonoBehaviour
{
	// Token: 0x0600230C RID: 8972 RVA: 0x000C2948 File Offset: 0x000C0B48
	protected void Update()
	{
		base.GetComponent<Rigidbody>().velocity = this.Speed * base.transform.forward;
	}

	// Token: 0x04001F78 RID: 8056
	public float Speed = 2f;
}
