﻿using System;
using UnityEngine;

// Token: 0x020007F7 RID: 2039
public class ExplosionsDeactivateRendererByTime : MonoBehaviour
{
	// Token: 0x060025B3 RID: 9651 RVA: 0x000D084C File Offset: 0x000CEA4C
	private void Awake()
	{
		this.rend = base.GetComponent<Renderer>();
	}

	// Token: 0x060025B4 RID: 9652 RVA: 0x000D085C File Offset: 0x000CEA5C
	private void DeactivateRenderer()
	{
		this.rend.enabled = false;
	}

	// Token: 0x060025B5 RID: 9653 RVA: 0x000D086C File Offset: 0x000CEA6C
	private void OnEnable()
	{
		this.rend.enabled = true;
		base.Invoke("DeactivateRenderer", this.TimeDelay);
	}

	// Token: 0x040021D3 RID: 8659
	public float TimeDelay = 1f;

	// Token: 0x040021D4 RID: 8660
	private Renderer rend;
}
