﻿using System;
using UnityStandardAssets.CinematicEffects;

// Token: 0x02000241 RID: 577
public class TonemappingOverlay : ImageEffectLayer
{
	// Token: 0x04000AED RID: 2797
	public UnityStandardAssets.CinematicEffects.TonemappingColorGrading tonemapping;
}
