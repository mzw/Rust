﻿using System;
using UnityEngine;

// Token: 0x020005CB RID: 1483
public class AddToHeightMap : global::ProceduralObject
{
	// Token: 0x06001EA5 RID: 7845 RVA: 0x000AC5C4 File Offset: 0x000AA7C4
	public override void Process()
	{
		Collider component = base.GetComponent<Collider>();
		Bounds bounds = component.bounds;
		int num = global::TerrainMeta.HeightMap.Index(global::TerrainMeta.NormalizeX(bounds.min.x));
		int num2 = global::TerrainMeta.HeightMap.Index(global::TerrainMeta.NormalizeZ(bounds.max.x));
		int num3 = global::TerrainMeta.HeightMap.Index(global::TerrainMeta.NormalizeX(bounds.min.z));
		int num4 = global::TerrainMeta.HeightMap.Index(global::TerrainMeta.NormalizeZ(bounds.max.z));
		for (int i = num3; i <= num4; i++)
		{
			float normZ = global::TerrainMeta.HeightMap.Coordinate(i);
			for (int j = num; j <= num2; j++)
			{
				float normX = global::TerrainMeta.HeightMap.Coordinate(j);
				Vector3 vector;
				vector..ctor(global::TerrainMeta.DenormalizeX(normX), bounds.max.y, global::TerrainMeta.DenormalizeZ(normZ));
				Ray ray;
				ray..ctor(vector, Vector3.down);
				RaycastHit raycastHit;
				if (component.Raycast(ray, ref raycastHit, bounds.size.y))
				{
					float y = raycastHit.point.y;
					float num5 = global::TerrainMeta.NormalizeY(y);
					float height = global::TerrainMeta.HeightMap.GetHeight01(j, i);
					if (num5 > height)
					{
						global::TerrainMeta.HeightMap.SetHeight(j, i, num5);
					}
				}
			}
		}
		global::GameManager.Destroy(this, 0f);
	}
}
