﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000267 RID: 615
public class IconSkin : MonoBehaviour
{
	// Token: 0x04000B48 RID: 2888
	public Image icon;

	// Token: 0x04000B49 RID: 2889
	public Text text;
}
