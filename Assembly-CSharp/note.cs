﻿using System;
using Facepunch.Extend;
using UnityEngine;

// Token: 0x020000B8 RID: 184
[ConsoleSystem.Factory("note")]
public class note : ConsoleSystem
{
	// Token: 0x06000A85 RID: 2693 RVA: 0x0004825C File Offset: 0x0004645C
	[ServerUserVar]
	public static void update(ConsoleSystem.Arg arg)
	{
		uint @uint = arg.GetUInt(0, 0u);
		string @string = arg.GetString(1, string.Empty);
		global::Item item = arg.Player().inventory.FindItemUID(@uint);
		if (item == null)
		{
			return;
		}
		item.text = StringExtensions.Truncate(@string, 1024, null);
		item.MarkDirty();
	}
}
