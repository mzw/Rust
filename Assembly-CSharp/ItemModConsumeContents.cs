﻿using System;

// Token: 0x020004E1 RID: 1249
public class ItemModConsumeContents : global::ItemMod
{
	// Token: 0x06001ACC RID: 6860 RVA: 0x000960C4 File Offset: 0x000942C4
	public override void DoAction(global::Item item, global::BasePlayer player)
	{
		foreach (global::Item item2 in item.contents.itemList)
		{
			global::ItemModConsume component = item2.info.GetComponent<global::ItemModConsume>();
			if (!(component == null))
			{
				if (component.CanDoAction(item2, player))
				{
					component.DoAction(item2, player);
					break;
				}
			}
		}
	}

	// Token: 0x06001ACD RID: 6861 RVA: 0x0009615C File Offset: 0x0009435C
	public override bool CanDoAction(global::Item item, global::BasePlayer player)
	{
		if (!player.metabolism.CanConsume())
		{
			return false;
		}
		if (item.contents == null)
		{
			return false;
		}
		foreach (global::Item item2 in item.contents.itemList)
		{
			global::ItemModConsume component = item2.info.GetComponent<global::ItemModConsume>();
			if (!(component == null))
			{
				if (component.CanDoAction(item2, player))
				{
					return true;
				}
			}
		}
		return false;
	}

	// Token: 0x0400158F RID: 5519
	public global::GameObjectRef consumeEffect;
}
