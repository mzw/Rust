﻿using System;
using UnityEngine;

// Token: 0x020004EB RID: 1259
public class ItemModMenuOption : global::ItemMod
{
	// Token: 0x06001AEC RID: 6892 RVA: 0x00096B6C File Offset: 0x00094D6C
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (command != this.commandName)
		{
			return;
		}
		if (!this.actionTarget.CanDoAction(item, player))
		{
			return;
		}
		this.actionTarget.DoAction(item, player);
	}

	// Token: 0x06001AED RID: 6893 RVA: 0x00096BA0 File Offset: 0x00094DA0
	private void OnValidate()
	{
		if (this.actionTarget == null)
		{
			Debug.LogWarning("ItemModMenuOption: actionTarget is null!", base.gameObject);
		}
		if (string.IsNullOrEmpty(this.commandName))
		{
			Debug.LogWarning("ItemModMenuOption: commandName can't be empty!", base.gameObject);
		}
		if (this.option.icon == null)
		{
			Debug.LogWarning("No icon set for ItemModMenuOption " + base.gameObject.name, base.gameObject);
		}
	}

	// Token: 0x040015AF RID: 5551
	public string commandName;

	// Token: 0x040015B0 RID: 5552
	public global::ItemMod actionTarget;

	// Token: 0x040015B1 RID: 5553
	public global::BaseEntity.Menu.Option option;

	// Token: 0x040015B2 RID: 5554
	[Tooltip("If true, this is the command that will run when an item is 'selected' on the toolbar")]
	public bool isPrimaryOption = true;
}
