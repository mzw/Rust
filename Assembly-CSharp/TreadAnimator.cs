﻿using System;
using UnityEngine;

// Token: 0x020000CE RID: 206
public class TreadAnimator : MonoBehaviour, IClientComponent
{
	// Token: 0x0400059D RID: 1437
	public Animator mainBodyAnimator;

	// Token: 0x0400059E RID: 1438
	public Transform[] wheelBones;

	// Token: 0x0400059F RID: 1439
	public Vector3[] vecShocksOffsetPosition;

	// Token: 0x040005A0 RID: 1440
	public Vector3[] wheelBoneOrigin;

	// Token: 0x040005A1 RID: 1441
	public float wheelBoneDistMax = 0.26f;

	// Token: 0x040005A2 RID: 1442
	public Renderer treadRenderer;

	// Token: 0x040005A3 RID: 1443
	public Material leftTread;

	// Token: 0x040005A4 RID: 1444
	public Material rightTread;

	// Token: 0x040005A5 RID: 1445
	public global::TreadEffects treadEffects;
}
