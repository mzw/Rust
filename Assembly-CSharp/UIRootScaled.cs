﻿using System;
using ConVar;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000720 RID: 1824
public class UIRootScaled : global::UIRoot
{
	// Token: 0x17000271 RID: 625
	// (get) Token: 0x06002292 RID: 8850 RVA: 0x000C1228 File Offset: 0x000BF428
	public static Canvas DragOverlayCanvas
	{
		get
		{
			return global::UIRootScaled.Instance.overlayCanvas;
		}
	}

	// Token: 0x06002293 RID: 8851 RVA: 0x000C1234 File Offset: 0x000BF434
	protected override void Awake()
	{
		global::UIRootScaled.Instance = this;
		base.Awake();
	}

	// Token: 0x06002294 RID: 8852 RVA: 0x000C1244 File Offset: 0x000BF444
	protected override void Refresh()
	{
		Vector2 vector;
		vector..ctor(1280f / ConVar.Graphics.uiscale, 720f / ConVar.Graphics.uiscale);
		if (this.scaler.referenceResolution != vector)
		{
			this.scaler.referenceResolution = vector;
		}
	}

	// Token: 0x04001F00 RID: 7936
	private static global::UIRootScaled Instance;

	// Token: 0x04001F01 RID: 7937
	public CanvasScaler scaler;
}
