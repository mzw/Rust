﻿using System;
using UnityEngine;

// Token: 0x020000BD RID: 189
public class ItemModUnwrap : global::ItemMod
{
	// Token: 0x06000A96 RID: 2710 RVA: 0x00048654 File Offset: 0x00046854
	public override void ServerCommand(global::Item item, string command, global::BasePlayer player)
	{
		if (command == "unwrap")
		{
			if (item.amount <= 0)
			{
				return;
			}
			item.UseItem(1);
			this.revealList.SpawnIntoContainer(player.inventory.containerMain);
			if (this.successEffect.isValid)
			{
				global::Effect.server.Run(this.successEffect.resourcePath, player.eyes.position, default(Vector3), null, false);
			}
		}
	}

	// Token: 0x04000558 RID: 1368
	public global::LootSpawn revealList;

	// Token: 0x04000559 RID: 1369
	public global::GameObjectRef successEffect;
}
