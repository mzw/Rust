﻿using System;
using UnityEngine;

// Token: 0x02000369 RID: 873
public class EntityComponent<T> : global::EntityComponentBase where T : global::BaseEntity
{
	// Token: 0x17000184 RID: 388
	// (get) Token: 0x060014E4 RID: 5348 RVA: 0x00079084 File Offset: 0x00077284
	protected T baseEntity
	{
		get
		{
			if (this._baseEntity == null)
			{
				this.UpdateBaseEntity();
			}
			return this._baseEntity;
		}
	}

	// Token: 0x060014E5 RID: 5349 RVA: 0x000790A8 File Offset: 0x000772A8
	protected void UpdateBaseEntity()
	{
		if (!this)
		{
			return;
		}
		if (!base.gameObject)
		{
			return;
		}
		this._baseEntity = (base.gameObject.ToBaseEntity() as T);
	}

	// Token: 0x060014E6 RID: 5350 RVA: 0x000790E4 File Offset: 0x000772E4
	protected override global::BaseEntity GetBaseEntity()
	{
		return this.baseEntity;
	}

	// Token: 0x04000F59 RID: 3929
	[NonSerialized]
	private T _baseEntity;
}
