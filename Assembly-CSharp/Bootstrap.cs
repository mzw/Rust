﻿using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using ConVar;
using Facepunch;
using Facepunch.Network.Raknet;
using Network;
using Oxide.Core;
using Rust;
using Rust.Ai;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020003F2 RID: 1010
public class Bootstrap : SingletonComponent<global::Bootstrap>
{
	// Token: 0x1700018F RID: 399
	// (get) Token: 0x06001760 RID: 5984 RVA: 0x00086248 File Offset: 0x00084448
	public static bool needsSetup
	{
		get
		{
			return !global::Bootstrap.bootstrapInitRun;
		}
	}

	// Token: 0x17000190 RID: 400
	// (get) Token: 0x06001761 RID: 5985 RVA: 0x00086254 File Offset: 0x00084454
	public static bool isPresent
	{
		get
		{
			return global::Bootstrap.bootstrapInitRun || Object.FindObjectsOfType<global::GameSetup>().Count<global::GameSetup>() > 0;
		}
	}

	// Token: 0x06001762 RID: 5986 RVA: 0x00086278 File Offset: 0x00084478
	public static void RunDefaults()
	{
		Application.targetFrameRate = 256;
		UnityEngine.Time.fixedDeltaTime = 0.0625f;
		UnityEngine.Time.maximumDeltaTime = 0.5f;
	}

	// Token: 0x06001763 RID: 5987 RVA: 0x00086298 File Offset: 0x00084498
	public static void Init_Tier0()
	{
		global::Bootstrap.RunDefaults();
		global::GameSetup.RunOnce = true;
		global::Bootstrap.bootstrapInitRun = true;
		ConsoleSystem.Index.Initialize(global::ConsoleGen.All);
		UnityButtons.Register();
		Facepunch.Output.Install();
		Facepunch.Pool.ResizeBuffer<Networkable>(65536);
		Facepunch.Pool.ResizeBuffer<global::EntityLink>(262144);
		Facepunch.Pool.FillBuffer<Networkable>(int.MaxValue);
		Facepunch.Pool.FillBuffer<global::EntityLink>(int.MaxValue);
		global::Bootstrap.NetworkInit();
		global::Noise.ConnectToNativeBackend();
		string str = CommandLine.Full.Replace(CommandLine.GetSwitch("-rcon.password", CommandLine.GetSwitch("+rcon.password", "RCONPASSWORD")), "******");
		DebugEx.Log("Command Line: " + str, 0);
		Interface.Initialize();
	}

	// Token: 0x06001764 RID: 5988 RVA: 0x0008633C File Offset: 0x0008453C
	public static void Init_Systems()
	{
		Application.Initialize(new Integration());
		if (global::Bootstrap.<>f__mg$cache0 == null)
		{
			global::Bootstrap.<>f__mg$cache0 = new Func<int>(global::SystemInfoEx.get_systemMemoryUsed);
		}
		Facepunch.Performance.GetMemoryUsage = global::Bootstrap.<>f__mg$cache0;
	}

	// Token: 0x06001765 RID: 5989 RVA: 0x0008636C File Offset: 0x0008456C
	public static void Init_Config()
	{
		global::ConsoleNetwork.Init();
		ConsoleSystem.UpdateValuesFromCommandLine();
		ConsoleSystem.Run(ConsoleSystem.Option.Server, "server.readcfg", new object[0]);
		global::ServerUsers.Load();
	}

	// Token: 0x06001766 RID: 5990 RVA: 0x00086394 File Offset: 0x00084594
	public static void NetworkInit()
	{
		Network.Net.sv = new Facepunch.Network.Raknet.Server();
	}

	// Token: 0x06001767 RID: 5991 RVA: 0x000863A0 File Offset: 0x000845A0
	private IEnumerator Start()
	{
		DebugEx.Log("Bootstrap Startup", 0);
		DebugEx.Log(global::SystemInfoGeneralText.currentInfo, 0);
		Texture.SetGlobalAnisotropicFilteringLimits(1, 16);
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Loading Bundles"));
		global::FileSystem.iface = new global::FileSystem_AssetBundles("Bundles/Bundles");
		if (global::FileSystem_AssetBundles.isError)
		{
			this.ThrowError(global::FileSystem_AssetBundles.loadingError);
		}
		if (global::Bootstrap.isErrored)
		{
			yield break;
		}
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Loading Game Manifest"));
		global::GameManifest.Load();
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("DONE!"));
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Running Self Check"));
		global::SelfCheck.Run();
		if (global::Bootstrap.isErrored)
		{
			yield break;
		}
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Bootstrap Tier0"));
		global::Bootstrap.Init_Tier0();
		ConsoleSystem.UpdateValuesFromCommandLine();
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Bootstrap Systems"));
		global::Bootstrap.Init_Systems();
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Bootstrap Config"));
		global::Bootstrap.Init_Config();
		if (global::Bootstrap.isErrored)
		{
			yield break;
		}
		yield return base.StartCoroutine(global::Bootstrap.LoadingUpdate("Loading Items"));
		global::ItemManager.Initialize();
		if (global::Bootstrap.isErrored)
		{
			yield break;
		}
		yield return base.StartCoroutine(this.DedicatedServerStartup());
		global::GameManager.Destroy(base.gameObject, 0f);
		yield break;
	}

	// Token: 0x06001768 RID: 5992 RVA: 0x000863BC File Offset: 0x000845BC
	private IEnumerator DedicatedServerStartup()
	{
		Application.isLoading = true;
		yield return base.StartCoroutine(global::Bootstrap.GameInit_Warmup(null));
		UnityEngine.Physics.solverIterationCount = 3;
		QualitySettings.SetQualityLevel(0);
		Object.DontDestroyOnLoad(base.gameObject);
		Object.DontDestroyOnLoad(global::GameManager.server.CreatePrefab("assets/bundled/prefabs/system/server_console.prefab", true));
		this.StartupShared();
		global::World.InitSize(ConVar.Server.worldsize);
		global::World.InitSeed(ConVar.Server.seed);
		global::World.InitSalt(ConVar.Server.salt);
		global::LevelManager.LoadLevel(ConVar.Server.level, true);
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return base.StartCoroutine(global::Bootstrap.StartServer(!CommandLine.HasSwitch("-skipload"), string.Empty, false));
		if (!Object.FindObjectOfType<global::Performance>())
		{
			Object.DontDestroyOnLoad(global::GameManager.server.CreatePrefab("assets/bundled/prefabs/system/performance.prefab", true));
		}
		Facepunch.Pool.Clear();
		System.GC.Collect();
		Application.isLoading = false;
		yield break;
	}

	// Token: 0x06001769 RID: 5993 RVA: 0x000863D8 File Offset: 0x000845D8
	public static IEnumerator StartServer(bool doLoad, string saveFileOverride, bool allowOutOfDateSaves)
	{
		float timeScale = UnityEngine.Time.timeScale;
		if (ConVar.Time.pausewhileloading)
		{
			UnityEngine.Time.timeScale = 0f;
		}
		Facepunch.RCon.Initialize();
		global::BaseEntity.Query.Server = new global::BaseEntity.Query.EntityTree(8096f);
		if (SingletonComponent<global::WorldSetup>.Instance)
		{
			yield return SingletonComponent<global::WorldSetup>.Instance.StartCoroutine(SingletonComponent<global::WorldSetup>.Instance.InitCoroutine());
		}
		if (SingletonComponent<global::DynamicNavMesh>.Instance && SingletonComponent<global::DynamicNavMesh>.Instance.enabled && !Rust.Ai.AiManager.nav_disable && !Rust.Ai.AiManager.nav_grid)
		{
			yield return SingletonComponent<global::DynamicNavMesh>.Instance.StartCoroutine(SingletonComponent<global::DynamicNavMesh>.Instance.UpdateNavMeshAndWait());
		}
		if (SingletonComponent<Rust.Ai.AiManager>.Instance && SingletonComponent<Rust.Ai.AiManager>.Instance.enabled)
		{
			SingletonComponent<Rust.Ai.AiManager>.Instance.Initialize();
			if (!Rust.Ai.AiManager.nav_disable && ConVar.AI.npc_enable && !Rust.Ai.AiManager.nav_grid && global::TerrainMeta.Path != null)
			{
				foreach (global::MonumentInfo monument in global::TerrainMeta.Path.Monuments)
				{
					if (monument.HasNavmesh)
					{
						yield return monument.StartCoroutine(monument.GetMonumentNavMesh().UpdateNavMeshAndWait());
					}
				}
			}
		}
		GameObject server = global::GameManager.server.CreatePrefab("assets/bundled/prefabs/system/server.prefab", true);
		Object.DontDestroyOnLoad(server);
		global::ServerMgr serverMgr = server.GetComponent<global::ServerMgr>();
		serverMgr.Initialize(doLoad, saveFileOverride, allowOutOfDateSaves, false);
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		global::ColliderGrid.RefreshAll();
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		global::SaveRestore.InitializeEntityLinks();
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		global::SaveRestore.InitializeEntitySupports();
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		global::SaveRestore.InitializeEntityConditionals();
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		global::ColliderGrid.RefreshAll();
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		global::SaveRestore.GetSaveCache();
		yield return UnityEngine.CoroutineEx.waitForSecondsRealtime(0.1f);
		serverMgr.OpenConnection();
		if (ConVar.Time.pausewhileloading)
		{
			UnityEngine.Time.timeScale = timeScale;
		}
		DebugEx.Log("Server startup complete", 0);
		yield break;
	}

	// Token: 0x0600176A RID: 5994 RVA: 0x00086404 File Offset: 0x00084604
	private void StartupShared()
	{
		Interface.CallHook("InitLogging", null);
		global::ItemManager.Initialize();
	}

	// Token: 0x0600176B RID: 5995 RVA: 0x00086418 File Offset: 0x00084618
	public void ThrowError(string error)
	{
		Debug.Log("ThrowError: " + error);
		this.errorPanel.SetActive(true);
		this.errorText.text = error;
		global::Bootstrap.isErrored = true;
	}

	// Token: 0x0600176C RID: 5996 RVA: 0x00086448 File Offset: 0x00084648
	public void ExitGame()
	{
		Debug.Log("Exiting due to Exit Game button on bootstrap error panel");
		Application.Quit();
	}

	// Token: 0x0600176D RID: 5997 RVA: 0x0008645C File Offset: 0x0008465C
	public static IEnumerator LoadingUpdate(string str)
	{
		if (!SingletonComponent<global::Bootstrap>.Instance)
		{
			yield break;
		}
		SingletonComponent<global::Bootstrap>.Instance.messageString = str;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield break;
	}

	// Token: 0x0600176E RID: 5998 RVA: 0x00086478 File Offset: 0x00084678
	public static IEnumerator GameInit_Warmup(Action<string> statusFunction = null)
	{
		if (global::Bootstrap.isGameInitRun)
		{
			yield break;
		}
		if (statusFunction != null)
		{
			statusFunction("GameManager Warmup");
		}
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		global::GameManager.Warmup();
		if (statusFunction != null)
		{
			statusFunction("Skinnable Warmup");
		}
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		yield return UnityEngine.CoroutineEx.waitForEndOfFrame;
		global::GameManifest.LoadAssets();
		global::Bootstrap.isGameInitRun = true;
		yield break;
	}

	// Token: 0x040011FA RID: 4602
	internal static bool bootstrapInitRun;

	// Token: 0x040011FB RID: 4603
	public static bool isErrored;

	// Token: 0x040011FC RID: 4604
	public string messageString = "Loading...";

	// Token: 0x040011FD RID: 4605
	public GameObject errorPanel;

	// Token: 0x040011FE RID: 4606
	public Text errorText;

	// Token: 0x040011FF RID: 4607
	public Text statusText;

	// Token: 0x04001200 RID: 4608
	public static bool isGameInitRun;

	// Token: 0x04001201 RID: 4609
	[CompilerGenerated]
	private static Func<int> <>f__mg$cache0;
}
