﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020005C4 RID: 1476
public class PlaceMonuments : global::ProceduralComponent
{
	// Token: 0x06001E96 RID: 7830 RVA: 0x000ABC30 File Offset: 0x000A9E30
	public override void Process(uint seed)
	{
		if ((ulong)global::World.Size < (ulong)((long)this.MinSize))
		{
			return;
		}
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::Prefab<global::MonumentInfo>[] array = global::Prefab.Load<global::MonumentInfo>("assets/bundled/prefabs/autospawn/" + this.ResourceFolder, null, null, true);
		if (array == null || array.Length == 0)
		{
			return;
		}
		array.Shuffle(seed);
		array.BubbleSort<global::Prefab<global::MonumentInfo>>();
		Vector3 position = global::TerrainMeta.Position;
		Vector3 size = global::TerrainMeta.Size;
		List<global::PlaceMonuments.SpawnInfo> list = new List<global::PlaceMonuments.SpawnInfo>();
		int num = 0;
		List<global::PlaceMonuments.SpawnInfo> list2 = new List<global::PlaceMonuments.SpawnInfo>();
		for (int i = 0; i < 10; i++)
		{
			int num2 = 0;
			list.Clear();
			foreach (global::Prefab<global::MonumentInfo> prefab in array)
			{
				int num3 = (int)((!prefab.Parameters) ? global::PrefabPriority.Low : (prefab.Parameters.Priority + 1));
				int num4 = num3 * num3 * num3 * num3;
				for (int k = 0; k < 10000; k++)
				{
					float num5 = SeedRandom.Value(ref seed);
					float num6 = SeedRandom.Value(ref seed);
					float num7 = SeedRandom.Range(ref seed, 0f, 1f);
					float factor = this.Filter.GetFactor(num5, num6);
					if (factor * factor >= num7)
					{
						float height = heightMap.GetHeight(num5, num6);
						float num8 = position.x + num5 * size.x;
						float num9 = position.z + num6 * size.z;
						Vector3 vector;
						vector..ctor(num8, height, num9);
						Quaternion localRotation = prefab.Object.transform.localRotation;
						Vector3 localScale = prefab.Object.transform.localScale;
						if (!this.CheckRadius(list, vector, (float)this.Distance))
						{
							prefab.ApplyDecorComponents(ref vector, ref localRotation, ref localScale);
							if (!prefab.Component || prefab.Component.CheckPlacement(vector, localRotation, localScale))
							{
								if (prefab.ApplyTerrainAnchors(ref vector, localRotation, localScale, this.Filter))
								{
									if (prefab.ApplyTerrainChecks(vector, localRotation, localScale, this.Filter))
									{
										if (prefab.ApplyTerrainFilters(vector, localRotation, localScale, null))
										{
											if (!prefab.CheckEnvironmentVolumes(vector, localRotation, localScale, global::EnvironmentType.Underground))
											{
												global::PlaceMonuments.SpawnInfo item = new global::PlaceMonuments.SpawnInfo
												{
													prefab = prefab,
													position = vector,
													rotation = localRotation,
													scale = localScale
												};
												list.Add(item);
												num2 += num4;
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (num2 > num)
			{
				num = num2;
				global::GenericsUtil.Swap<List<global::PlaceMonuments.SpawnInfo>>(ref list, ref list2);
			}
		}
		foreach (global::PlaceMonuments.SpawnInfo spawnInfo in list2)
		{
			global::Prefab prefab2 = spawnInfo.prefab;
			Vector3 position2 = spawnInfo.position;
			Quaternion rotation = spawnInfo.rotation;
			Vector3 scale = spawnInfo.scale;
			prefab2.ApplyTerrainPlacements(position2, rotation, scale);
			prefab2.ApplyTerrainModifiers(position2, rotation, scale);
			global::World.Serialization.AddPrefab("Monument", prefab2.ID, position2, rotation, scale);
		}
	}

	// Token: 0x06001E97 RID: 7831 RVA: 0x000ABFA4 File Offset: 0x000AA1A4
	private bool CheckRadius(List<global::PlaceMonuments.SpawnInfo> spawns, Vector3 pos, float radius)
	{
		float num = radius * radius;
		foreach (global::PlaceMonuments.SpawnInfo spawnInfo in spawns)
		{
			float sqrMagnitude = (spawnInfo.position - pos).sqrMagnitude;
			if (sqrMagnitude < num)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x04001963 RID: 6499
	public global::SpawnFilter Filter;

	// Token: 0x04001964 RID: 6500
	public string ResourceFolder = string.Empty;

	// Token: 0x04001965 RID: 6501
	public int Distance = 500;

	// Token: 0x04001966 RID: 6502
	public int MinSize;

	// Token: 0x04001967 RID: 6503
	private const int Candidates = 10;

	// Token: 0x04001968 RID: 6504
	private const int Attempts = 10000;

	// Token: 0x020005C5 RID: 1477
	private struct SpawnInfo
	{
		// Token: 0x04001969 RID: 6505
		public global::Prefab prefab;

		// Token: 0x0400196A RID: 6506
		public Vector3 position;

		// Token: 0x0400196B RID: 6507
		public Quaternion rotation;

		// Token: 0x0400196C RID: 6508
		public Vector3 scale;
	}
}
