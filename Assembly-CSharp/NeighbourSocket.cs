﻿using System;
using UnityEngine;

// Token: 0x0200021C RID: 540
public class NeighbourSocket : global::Socket_Base
{
	// Token: 0x06000FBB RID: 4027 RVA: 0x0005FFB8 File Offset: 0x0005E1B8
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.DrawWireCube(this.selectCenter, this.selectSize);
	}

	// Token: 0x06000FBC RID: 4028 RVA: 0x0005FFDC File Offset: 0x0005E1DC
	public override bool TestTarget(global::Construction.Target target)
	{
		return false;
	}

	// Token: 0x06000FBD RID: 4029 RVA: 0x0005FFE0 File Offset: 0x0005E1E0
	public override bool CanConnect(Vector3 position, Quaternion rotation, global::Socket_Base socket, Vector3 socketPosition, Quaternion socketRotation)
	{
		if (!base.CanConnect(position, rotation, socket, socketPosition, socketRotation))
		{
			return false;
		}
		OBB selectBounds = base.GetSelectBounds(position, rotation);
		OBB selectBounds2 = socket.GetSelectBounds(socketPosition, socketRotation);
		return selectBounds.Intersects(selectBounds2);
	}
}
