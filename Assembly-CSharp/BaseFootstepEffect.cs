﻿using System;
using UnityEngine;

// Token: 0x02000312 RID: 786
public abstract class BaseFootstepEffect : MonoBehaviour, IClientComponent
{
	// Token: 0x04000E2D RID: 3629
	public LayerMask validImpactLayers = -1;
}
