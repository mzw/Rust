﻿using System;
using UnityEngine;

// Token: 0x02000222 RID: 546
public class SocketHandle : global::PrefabAttribute
{
	// Token: 0x06000FD9 RID: 4057 RVA: 0x000609CC File Offset: 0x0005EBCC
	protected override Type GetIndexedType()
	{
		return typeof(global::SocketHandle);
	}

	// Token: 0x06000FDA RID: 4058 RVA: 0x000609D8 File Offset: 0x0005EBD8
	internal void AdjustTarget(ref global::Construction.Target target, float maxplaceDistance)
	{
		Vector3 worldPosition = this.worldPosition;
		Vector3 vector = target.ray.origin + target.ray.direction * maxplaceDistance;
		Vector3 vector2 = vector - worldPosition;
		target.ray.direction = (vector2 - target.ray.origin).normalized;
	}
}
