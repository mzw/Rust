﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C8 RID: 1736
public class UIIntegerEntry : MonoBehaviour
{
	// Token: 0x14000006 RID: 6
	// (add) Token: 0x06002166 RID: 8550 RVA: 0x000BC1D8 File Offset: 0x000BA3D8
	// (remove) Token: 0x06002167 RID: 8551 RVA: 0x000BC210 File Offset: 0x000BA410
	public event Action textChanged;

	// Token: 0x06002168 RID: 8552 RVA: 0x000BC248 File Offset: 0x000BA448
	public void OnAmountTextChanged()
	{
		this.textChanged();
	}

	// Token: 0x06002169 RID: 8553 RVA: 0x000BC258 File Offset: 0x000BA458
	public void SetAmount(int amount)
	{
		if (amount == this.GetIntAmount())
		{
			return;
		}
		this.textEntry.text = amount.ToString();
	}

	// Token: 0x0600216A RID: 8554 RVA: 0x000BC280 File Offset: 0x000BA480
	public int GetIntAmount()
	{
		int result = 0;
		int.TryParse(this.textEntry.text, out result);
		return result;
	}

	// Token: 0x0600216B RID: 8555 RVA: 0x000BC2A4 File Offset: 0x000BA4A4
	public void PlusMinus(int delta)
	{
		this.SetAmount(this.GetIntAmount() + delta);
	}

	// Token: 0x04001D6C RID: 7532
	public InputField textEntry;
}
