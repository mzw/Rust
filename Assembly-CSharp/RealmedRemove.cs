﻿using System;
using System.Linq;
using UnityEngine;

// Token: 0x02000476 RID: 1142
public class RealmedRemove : MonoBehaviour, global::IPrefabPreProcess
{
	// Token: 0x060018EA RID: 6378 RVA: 0x0008C38C File Offset: 0x0008A58C
	public void PreProcess(global::IPrefabProcessor process, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		if (clientside)
		{
			foreach (GameObject gameObject in this.removedFromClient)
			{
				Object.DestroyImmediate(gameObject, true);
			}
			foreach (Component component in this.removedComponentFromClient)
			{
				Object.DestroyImmediate(component, true);
			}
		}
		if (serverside)
		{
			foreach (GameObject gameObject2 in this.removedFromServer)
			{
				Object.DestroyImmediate(gameObject2, true);
			}
			foreach (Component component2 in this.removedComponentFromServer)
			{
				Object.DestroyImmediate(component2, true);
			}
		}
		if (!bundling)
		{
			process.RemoveComponent(this);
		}
	}

	// Token: 0x060018EB RID: 6379 RVA: 0x0008C46C File Offset: 0x0008A66C
	public bool ShouldDelete(Component comp, bool client, bool server)
	{
		return (!client || this.doNotRemoveFromClient == null || !this.doNotRemoveFromClient.Contains(comp)) && (!server || this.doNotRemoveFromServer == null || !this.doNotRemoveFromServer.Contains(comp));
	}

	// Token: 0x0400139C RID: 5020
	public GameObject[] removedFromClient;

	// Token: 0x0400139D RID: 5021
	public Component[] removedComponentFromClient;

	// Token: 0x0400139E RID: 5022
	public GameObject[] removedFromServer;

	// Token: 0x0400139F RID: 5023
	public Component[] removedComponentFromServer;

	// Token: 0x040013A0 RID: 5024
	public Component[] doNotRemoveFromServer;

	// Token: 0x040013A1 RID: 5025
	public Component[] doNotRemoveFromClient;
}
