﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

// Token: 0x02000728 RID: 1832
public class DragMe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	// Token: 0x060022A0 RID: 8864 RVA: 0x000C135C File Offset: 0x000BF55C
	public virtual void OnBeginDrag(PointerEventData eventData)
	{
	}

	// Token: 0x060022A1 RID: 8865 RVA: 0x000C1360 File Offset: 0x000BF560
	public virtual void OnDrag(PointerEventData eventData)
	{
	}

	// Token: 0x060022A2 RID: 8866 RVA: 0x000C1364 File Offset: 0x000BF564
	public virtual void OnEndDrag(PointerEventData eventData)
	{
	}

	// Token: 0x04001F09 RID: 7945
	public static global::DragMe dragging;

	// Token: 0x04001F0A RID: 7946
	public static GameObject dragIcon;

	// Token: 0x04001F0B RID: 7947
	public static object data;

	// Token: 0x04001F0C RID: 7948
	[NonSerialized]
	public string dragType = "generic";
}
