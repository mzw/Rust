﻿using System;
using UnityEngine;

// Token: 0x020005D9 RID: 1497
public class Monument : global::TerrainPlacement
{
	// Token: 0x06001ECC RID: 7884 RVA: 0x000AD97C File Offset: 0x000ABB7C
	protected void OnDrawGizmosSelected()
	{
		if (this.Radius == 0f)
		{
			this.Radius = this.extents.x;
		}
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		global::GizmosUtil.DrawWireCircleY(base.transform.position, this.Radius);
		global::GizmosUtil.DrawWireCircleY(base.transform.position, this.Radius - this.Fade);
	}

	// Token: 0x06001ECD RID: 7885 RVA: 0x000AD9FC File Offset: 0x000ABBFC
	protected override void ApplyHeight(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		if (this.Radius == 0f)
		{
			this.Radius = this.extents.x;
		}
		bool useBlendMap = this.blendmap != null;
		Vector3 position = localToWorld.MultiplyPoint3x4(Vector3.zero);
		global::TextureData heightdata = new global::TextureData(this.heightmap);
		global::TextureData blenddata = (!useBlendMap) ? default(global::TextureData) : new global::TextureData(this.blendmap);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, -this.Radius));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, -this.Radius));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, this.Radius));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, this.Radius));
		global::TerrainMeta.HeightMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			float normZ = global::TerrainMeta.HeightMap.Coordinate(z);
			float normX = global::TerrainMeta.HeightMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			float num;
			if (useBlendMap)
			{
				num = blenddata.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z).w;
			}
			else
			{
				float num2 = global::Noise.Billow((double)vector.x, (double)vector.z, 4, 0.004999999888241291, (double)(0.25f * this.Fade), 2.0, 0.5);
				num = Mathf.InverseLerp(this.Radius, this.Radius - this.Fade + num2, Vector3Ex.Magnitude2D(vector2));
			}
			if (num == 0f)
			{
				return;
			}
			float y = position.y + this.offset.y + heightdata.GetInterpolatedHalf((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z) * this.size.y;
			float num3 = global::TerrainMeta.NormalizeY(y);
			float height = global::TerrainMeta.HeightMap.GetHeight01(x, z);
			num3 = Mathf.SmoothStep(height, num3, num);
			global::TerrainMeta.HeightMap.SetHeight(x, z, num3);
		});
	}

	// Token: 0x06001ECE RID: 7886 RVA: 0x000ADB64 File Offset: 0x000ABD64
	protected override void ApplySplat(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		if (this.Radius == 0f)
		{
			this.Radius = this.extents.x;
		}
		global::TextureData splat0data = new global::TextureData(this.splatmap0);
		global::TextureData splat1data = new global::TextureData(this.splatmap1);
		bool should0 = base.ShouldSplat(1);
		bool should1 = base.ShouldSplat(2);
		bool should2 = base.ShouldSplat(4);
		bool should3 = base.ShouldSplat(8);
		bool should4 = base.ShouldSplat(16);
		bool should5 = base.ShouldSplat(32);
		bool should6 = base.ShouldSplat(64);
		bool should7 = base.ShouldSplat(128);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, -this.Radius));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, -this.Radius));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, this.Radius));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, this.Radius));
		global::TerrainMeta.SplatMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			global::GenerateCliffSplat.Process(x, z);
			float normZ = global::TerrainMeta.SplatMap.Coordinate(z);
			float normX = global::TerrainMeta.SplatMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			float num = global::Noise.Billow((double)vector.x, (double)vector.z, 4, 0.004999999888241291, (double)(0.25f * this.Fade), 2.0, 0.5);
			float num2 = Mathf.InverseLerp(this.Radius, this.Radius - this.Fade + num, Vector3Ex.Magnitude2D(vector2));
			if (num2 == 0f)
			{
				return;
			}
			Vector4 interpolatedVector = splat0data.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			Vector4 interpolatedVector2 = splat1data.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			if (!should0)
			{
				interpolatedVector.x = 0f;
			}
			if (!should1)
			{
				interpolatedVector.y = 0f;
			}
			if (!should2)
			{
				interpolatedVector.z = 0f;
			}
			if (!should3)
			{
				interpolatedVector.w = 0f;
			}
			if (!should4)
			{
				interpolatedVector2.x = 0f;
			}
			if (!should5)
			{
				interpolatedVector2.y = 0f;
			}
			if (!should6)
			{
				interpolatedVector2.z = 0f;
			}
			if (!should7)
			{
				interpolatedVector2.w = 0f;
			}
			global::TerrainMeta.SplatMap.SetSplatRaw(x, z, interpolatedVector, interpolatedVector2, num2);
		});
	}

	// Token: 0x06001ECF RID: 7887 RVA: 0x000ADCFC File Offset: 0x000ABEFC
	protected override void ApplyAlpha(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		if (this.Radius == 0f)
		{
			this.Radius = this.extents.x;
		}
		global::TextureData alphadata = new global::TextureData(this.alphamap);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, -this.Radius));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, -this.Radius));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, this.Radius));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, this.Radius));
		global::TerrainMeta.AlphaMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			float normZ = global::TerrainMeta.AlphaMap.Coordinate(z);
			float normX = global::TerrainMeta.AlphaMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			float num = global::Noise.Billow((double)vector.x, (double)vector.z, 4, 0.004999999888241291, (double)(0.25f * this.Fade), 2.0, 0.5);
			float num2 = Mathf.InverseLerp(this.Radius, this.Radius - this.Fade + num, Vector3Ex.Magnitude2D(vector2));
			if (num2 == 0f)
			{
				return;
			}
			float w = alphadata.GetInterpolatedVector((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z).w;
			global::TerrainMeta.AlphaMap.SetAlpha(x, z, w, num2);
		});
	}

	// Token: 0x06001ED0 RID: 7888 RVA: 0x000ADE14 File Offset: 0x000AC014
	protected override void ApplyBiome(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
	}

	// Token: 0x06001ED1 RID: 7889 RVA: 0x000ADE18 File Offset: 0x000AC018
	protected override void ApplyTopology(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		if (this.Radius == 0f)
		{
			this.Radius = this.extents.x;
		}
		global::TextureData topologydata = new global::TextureData(this.topologymap);
		Vector3 v = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, -this.Radius));
		Vector3 v2 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, -this.Radius));
		Vector3 v3 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(-this.Radius, 0f, this.Radius));
		Vector3 v4 = localToWorld.MultiplyPoint3x4(this.offset + new Vector3(this.Radius, 0f, this.Radius));
		global::TerrainMeta.TopologyMap.ForEachParallel(v, v2, v3, v4, delegate(int x, int z)
		{
			global::GenerateCliffTopology.Process(x, z);
			float normZ = global::TerrainMeta.TopologyMap.Coordinate(z);
			float normX = global::TerrainMeta.TopologyMap.Coordinate(x);
			Vector3 vector;
			vector..ctor(global::TerrainMeta.DenormalizeX(normX), 0f, global::TerrainMeta.DenormalizeZ(normZ));
			Vector3 vector2 = worldToLocal.MultiplyPoint3x4(vector) - this.offset;
			int interpolatedInt = topologydata.GetInterpolatedInt((vector2.x + this.extents.x) / this.size.x, (vector2.z + this.extents.z) / this.size.z);
			if (this.ShouldTopology(interpolatedInt))
			{
				global::TerrainMeta.TopologyMap.AddTopology(x, z, interpolatedInt & (int)this.TopologyMask);
			}
		});
	}

	// Token: 0x06001ED2 RID: 7890 RVA: 0x000ADF30 File Offset: 0x000AC130
	protected override void ApplyWater(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
	}

	// Token: 0x0400199F RID: 6559
	public float Radius;

	// Token: 0x040019A0 RID: 6560
	public float Fade = 10f;
}
