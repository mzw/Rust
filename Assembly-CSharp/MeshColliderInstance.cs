﻿using System;
using UnityEngine;

// Token: 0x0200024C RID: 588
public struct MeshColliderInstance
{
	// Token: 0x17000114 RID: 276
	// (get) Token: 0x0600103E RID: 4158 RVA: 0x00062354 File Offset: 0x00060554
	// (set) Token: 0x0600103F RID: 4159 RVA: 0x00062364 File Offset: 0x00060564
	public Mesh mesh
	{
		get
		{
			return this.data.mesh;
		}
		set
		{
			this.data = global::MeshCache.Get(value);
		}
	}

	// Token: 0x04000B02 RID: 2818
	public Transform transform;

	// Token: 0x04000B03 RID: 2819
	public Rigidbody rigidbody;

	// Token: 0x04000B04 RID: 2820
	public Collider collider;

	// Token: 0x04000B05 RID: 2821
	public OBB bounds;

	// Token: 0x04000B06 RID: 2822
	public Vector3 position;

	// Token: 0x04000B07 RID: 2823
	public Quaternion rotation;

	// Token: 0x04000B08 RID: 2824
	public Vector3 scale;

	// Token: 0x04000B09 RID: 2825
	public global::MeshCache.Data data;
}
