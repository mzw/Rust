﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x02000687 RID: 1671
public class FPSGraph : global::Graph
{
	// Token: 0x060020EE RID: 8430 RVA: 0x000BAEB4 File Offset: 0x000B90B4
	public void Refresh()
	{
		base.enabled = (ConVar.FPS.graph > 0);
		this.Area.width = (float)(this.Resolution = Mathf.Clamp(ConVar.FPS.graph, 0, Screen.width));
	}

	// Token: 0x060020EF RID: 8431 RVA: 0x000BAEF4 File Offset: 0x000B90F4
	protected void OnEnable()
	{
		this.Refresh();
	}

	// Token: 0x060020F0 RID: 8432 RVA: 0x000BAEFC File Offset: 0x000B90FC
	protected override float GetValue()
	{
		return 1f / UnityEngine.Time.deltaTime;
	}

	// Token: 0x060020F1 RID: 8433 RVA: 0x000BAF0C File Offset: 0x000B910C
	protected override Color GetColor(float value)
	{
		return (value >= 10f) ? ((value >= 30f) ? Color.green : Color.yellow) : Color.red;
	}
}
