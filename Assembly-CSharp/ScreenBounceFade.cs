﻿using System;
using UnityEngine;

// Token: 0x02000334 RID: 820
public class ScreenBounceFade : global::BaseScreenShake
{
	// Token: 0x060013C5 RID: 5061 RVA: 0x00073DE8 File Offset: 0x00071FE8
	public override void Setup()
	{
		this.bounceTime = Random.Range(0f, 1000f);
	}

	// Token: 0x060013C6 RID: 5062 RVA: 0x00073E00 File Offset: 0x00072000
	public override void Run(float delta, ref global::CachedTransform<Camera> cam, ref global::CachedTransform<global::BaseViewModel> vm)
	{
		float num = Vector3.Distance(cam.position, base.transform.position);
		float num2 = 1f - Mathf.InverseLerp(0f, this.maxDistance, num);
		this.bounceTime += Time.deltaTime * this.bounceSpeed.Evaluate(delta);
		float num3 = this.distanceFalloff.Evaluate(num2);
		float num4 = this.bounceScale.Evaluate(delta) * 0.1f * num3 * this.scale * this.timeFalloff.Evaluate(delta);
		this.bounceVelocity.x = Mathf.Sin(this.bounceTime * 20f) * num4;
		this.bounceVelocity.y = Mathf.Cos(this.bounceTime * 25f) * num4;
		this.bounceVelocity.z = 0f;
		Vector3 vector = Vector3.zero;
		vector += this.bounceVelocity.x * cam.right;
		vector += this.bounceVelocity.y * cam.up;
		vector *= num2;
		if (cam)
		{
			cam.position += vector;
		}
		if (vm)
		{
			vm.position += vector * -1f * this.bounceViewmodel.Evaluate(delta);
		}
	}

	// Token: 0x04000EAC RID: 3756
	public AnimationCurve bounceScale;

	// Token: 0x04000EAD RID: 3757
	public AnimationCurve bounceSpeed;

	// Token: 0x04000EAE RID: 3758
	public AnimationCurve bounceViewmodel;

	// Token: 0x04000EAF RID: 3759
	public AnimationCurve distanceFalloff;

	// Token: 0x04000EB0 RID: 3760
	public AnimationCurve timeFalloff;

	// Token: 0x04000EB1 RID: 3761
	private float bounceTime;

	// Token: 0x04000EB2 RID: 3762
	private Vector3 bounceVelocity = Vector3.zero;

	// Token: 0x04000EB3 RID: 3763
	public float maxDistance = 10f;

	// Token: 0x04000EB4 RID: 3764
	public float scale = 1f;
}
