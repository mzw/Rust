﻿using System;
using UnityEngine;

// Token: 0x020007B5 RID: 1973
public class AlternateAttack : StateMachineBehaviour
{
	// Token: 0x060024B4 RID: 9396 RVA: 0x000C9F80 File Offset: 0x000C8180
	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (this.random)
		{
			string text = this.targetTransitions[Random.Range(0, this.targetTransitions.Length)];
			animator.Play(text, layerIndex, 0f);
		}
		else
		{
			int integer = animator.GetInteger("lastAttack");
			string text2 = this.targetTransitions[integer % this.targetTransitions.Length];
			animator.Play(text2, layerIndex, 0f);
			if (!this.dontIncrement)
			{
				animator.SetInteger("lastAttack", integer + 1);
			}
		}
	}

	// Token: 0x0400201D RID: 8221
	public bool random;

	// Token: 0x0400201E RID: 8222
	public bool dontIncrement;

	// Token: 0x0400201F RID: 8223
	public string[] targetTransitions;
}
