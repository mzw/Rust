﻿using System;
using UnityEngine;

// Token: 0x020000DD RID: 221
public class FlameJet : MonoBehaviour
{
	// Token: 0x06000B15 RID: 2837 RVA: 0x0004A7B4 File Offset: 0x000489B4
	private void Initialize()
	{
		this.currentColor = this.startColor;
	}

	// Token: 0x06000B16 RID: 2838 RVA: 0x0004A7C4 File Offset: 0x000489C4
	private void Awake()
	{
		this.Initialize();
	}

	// Token: 0x06000B17 RID: 2839 RVA: 0x0004A7CC File Offset: 0x000489CC
	public void LateUpdate()
	{
		this.UpdateLine();
	}

	// Token: 0x06000B18 RID: 2840 RVA: 0x0004A7D4 File Offset: 0x000489D4
	public void SetOn(bool isOn)
	{
		this.on = isOn;
	}

	// Token: 0x06000B19 RID: 2841 RVA: 0x0004A7E0 File Offset: 0x000489E0
	private float curve(float x)
	{
		return x * x;
	}

	// Token: 0x06000B1A RID: 2842 RVA: 0x0004A7F4 File Offset: 0x000489F4
	private void UpdateLine()
	{
		this.currentColor.a = Mathf.Lerp(this.currentColor.a, (!this.on) ? 0f : 1f, Time.deltaTime * 40f);
		this.line.SetColors(this.currentColor, this.endColor);
		this.tesselation = 0.1f;
		this.numSegments = Mathf.CeilToInt(this.maxLength / this.tesselation);
		float num = this.maxLength / (float)this.numSegments;
		Vector3[] array = new Vector3[this.numSegments];
		for (int i = 0; i < array.Length; i++)
		{
			float num2 = 0f;
			float num3 = 0f;
			if (this.lastWorldSegments != null && this.lastWorldSegments[i] != Vector3.zero)
			{
				Vector3 vector = base.transform.InverseTransformPoint(this.lastWorldSegments[i]);
				float num4 = (float)i / (float)array.Length;
				Vector3 vector2 = Vector3.Lerp(vector, Vector3.zero, Time.deltaTime * this.drag);
				vector2 = Vector3.Lerp(Vector3.zero, vector2, Mathf.Sqrt(num4));
				num2 = vector2.x;
				num3 = vector2.y;
			}
			if (i == 0)
			{
				num3 = (num2 = 0f);
			}
			Vector3 vector3;
			vector3..ctor(num2, num3, (float)i * num);
			array[i] = vector3;
			if (this.lastWorldSegments == null)
			{
				this.lastWorldSegments = new Vector3[this.numSegments];
			}
			this.lastWorldSegments[i] = base.transform.TransformPoint(vector3);
		}
		this.line.SetVertexCount(this.numSegments);
		this.line.SetPositions(array);
	}

	// Token: 0x040005DB RID: 1499
	public LineRenderer line;

	// Token: 0x040005DC RID: 1500
	public float tesselation = 0.025f;

	// Token: 0x040005DD RID: 1501
	private float length;

	// Token: 0x040005DE RID: 1502
	public float maxLength = 2f;

	// Token: 0x040005DF RID: 1503
	public float drag;

	// Token: 0x040005E0 RID: 1504
	private int numSegments;

	// Token: 0x040005E1 RID: 1505
	public bool on;

	// Token: 0x040005E2 RID: 1506
	private Vector3[] lastWorldSegments;

	// Token: 0x040005E3 RID: 1507
	private Vector3[] currentSegments;

	// Token: 0x040005E4 RID: 1508
	public Color startColor;

	// Token: 0x040005E5 RID: 1509
	public Color endColor;

	// Token: 0x040005E6 RID: 1510
	public Color currentColor;
}
