﻿using System;
using UnityEngine;
using UnityEngine.Events;

// Token: 0x02000701 RID: 1793
public class SteamFriendsList : MonoBehaviour
{
	// Token: 0x04001EB1 RID: 7857
	public RectTransform targetPanel;

	// Token: 0x04001EB2 RID: 7858
	public global::SteamUserButton userButton;

	// Token: 0x04001EB3 RID: 7859
	public bool IncludeFriendsList = true;

	// Token: 0x04001EB4 RID: 7860
	public bool IncludeRecentlySeen;

	// Token: 0x04001EB5 RID: 7861
	public global::SteamFriendsList.onFriendSelectedEvent onFriendSelected;

	// Token: 0x02000702 RID: 1794
	[Serializable]
	public class onFriendSelectedEvent : UnityEvent<ulong>
	{
	}
}
