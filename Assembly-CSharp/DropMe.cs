﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

// Token: 0x0200072A RID: 1834
public class DropMe : MonoBehaviour, IDropHandler, IEventSystemHandler
{
	// Token: 0x060022A4 RID: 8868 RVA: 0x000C1370 File Offset: 0x000BF570
	public virtual void OnDrop(PointerEventData eventData)
	{
	}

	// Token: 0x04001F0D RID: 7949
	public string[] droppableTypes;
}
