﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x02000244 RID: 580
public class CameraSettings : MonoBehaviour, IClientComponent
{
	// Token: 0x0600102B RID: 4139 RVA: 0x00061E24 File Offset: 0x00060024
	private void OnEnable()
	{
		this.cam = base.GetComponent<Camera>();
	}

	// Token: 0x0600102C RID: 4140 RVA: 0x00061E34 File Offset: 0x00060034
	private void Update()
	{
		this.cam.farClipPlane = Mathf.Clamp(ConVar.Graphics.drawdistance, 500f, 2500f);
	}

	// Token: 0x04000AF2 RID: 2802
	private Camera cam;
}
