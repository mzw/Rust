﻿using System;
using UnityEngine;

// Token: 0x020000DC RID: 220
public class v_chainsaw : MonoBehaviour
{
	// Token: 0x06000B0F RID: 2831 RVA: 0x0004A3E8 File Offset: 0x000485E8
	public void OnEnable()
	{
		if (this.block == null)
		{
			this.block = new MaterialPropertyBlock();
		}
		this.saveST = this.chainRenderer.sharedMaterial.GetVector("_MainTex_ST");
	}

	// Token: 0x06000B10 RID: 2832 RVA: 0x0004A420 File Offset: 0x00048620
	private void Awake()
	{
		this.chainlink = this.chainRenderer.sharedMaterial;
	}

	// Token: 0x06000B11 RID: 2833 RVA: 0x0004A434 File Offset: 0x00048634
	private void Start()
	{
	}

	// Token: 0x06000B12 RID: 2834 RVA: 0x0004A438 File Offset: 0x00048638
	private void ScrollChainTexture()
	{
		float num = this.chainAmount = (this.chainAmount + Time.deltaTime * this.chainSpeed) % 1f;
		this.block.Clear();
		this.block.SetVector("_MainTex_ST", new Vector4(this.saveST.x, this.saveST.y, num, 0f));
		this.chainRenderer.SetPropertyBlock(this.block);
	}

	// Token: 0x06000B13 RID: 2835 RVA: 0x0004A4B8 File Offset: 0x000486B8
	private void Update()
	{
		this.chainsawAnimator.SetBool("attacking", this.bAttacking);
		this.smokeEffect.enableEmission = this.bEngineOn;
		if (this.bHitMetal)
		{
			this.chainsawAnimator.SetBool("attackHit", true);
			foreach (ParticleSystem particleSystem in this.hitMetalFX)
			{
				particleSystem.enableEmission = true;
			}
			foreach (ParticleSystem particleSystem2 in this.hitWoodFX)
			{
				particleSystem2.enableEmission = false;
			}
			foreach (ParticleSystem particleSystem3 in this.hitFleshFX)
			{
				particleSystem3.enableEmission = false;
			}
		}
		else if (this.bHitWood)
		{
			this.chainsawAnimator.SetBool("attackHit", true);
			foreach (ParticleSystem particleSystem4 in this.hitMetalFX)
			{
				particleSystem4.enableEmission = false;
			}
			foreach (ParticleSystem particleSystem5 in this.hitWoodFX)
			{
				particleSystem5.enableEmission = true;
			}
			foreach (ParticleSystem particleSystem6 in this.hitFleshFX)
			{
				particleSystem6.enableEmission = false;
			}
		}
		else if (this.bHitFlesh)
		{
			this.chainsawAnimator.SetBool("attackHit", true);
			foreach (ParticleSystem particleSystem7 in this.hitMetalFX)
			{
				particleSystem7.enableEmission = false;
			}
			foreach (ParticleSystem particleSystem8 in this.hitWoodFX)
			{
				particleSystem8.enableEmission = false;
			}
			foreach (ParticleSystem particleSystem9 in this.hitFleshFX)
			{
				particleSystem9.enableEmission = true;
			}
		}
		else
		{
			this.chainsawAnimator.SetBool("attackHit", false);
			foreach (ParticleSystem particleSystem10 in this.hitMetalFX)
			{
				particleSystem10.enableEmission = false;
			}
			foreach (ParticleSystem particleSystem11 in this.hitWoodFX)
			{
				particleSystem11.enableEmission = false;
			}
			foreach (ParticleSystem particleSystem12 in this.hitFleshFX)
			{
				particleSystem12.enableEmission = false;
			}
		}
	}

	// Token: 0x040005C7 RID: 1479
	public bool bAttacking;

	// Token: 0x040005C8 RID: 1480
	public bool bHitMetal;

	// Token: 0x040005C9 RID: 1481
	public bool bHitWood;

	// Token: 0x040005CA RID: 1482
	public bool bHitFlesh;

	// Token: 0x040005CB RID: 1483
	public bool bEngineOn;

	// Token: 0x040005CC RID: 1484
	public ParticleSystem[] hitMetalFX;

	// Token: 0x040005CD RID: 1485
	public ParticleSystem[] hitWoodFX;

	// Token: 0x040005CE RID: 1486
	public ParticleSystem[] hitFleshFX;

	// Token: 0x040005CF RID: 1487
	public ParticleSystem smokeEffect;

	// Token: 0x040005D0 RID: 1488
	public AudioClip chainsawAttacking;

	// Token: 0x040005D1 RID: 1489
	public AudioClip chainsawIdle;

	// Token: 0x040005D2 RID: 1490
	public Animator chainsawAnimator;

	// Token: 0x040005D3 RID: 1491
	public Renderer chainRenderer;

	// Token: 0x040005D4 RID: 1492
	public Material chainlink;

	// Token: 0x040005D5 RID: 1493
	private MaterialPropertyBlock block;

	// Token: 0x040005D6 RID: 1494
	private Vector2 saveST;

	// Token: 0x040005D7 RID: 1495
	private float chainSpeed;

	// Token: 0x040005D8 RID: 1496
	private float chainAmount;

	// Token: 0x040005D9 RID: 1497
	public float temp1;

	// Token: 0x040005DA RID: 1498
	public float temp2;
}
