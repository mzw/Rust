﻿using System;
using UnityEngine;

// Token: 0x020004E0 RID: 1248
public class ItemModConsumeChance : global::ItemModConsume
{
	// Token: 0x06001AC8 RID: 6856 RVA: 0x00096044 File Offset: 0x00094244
	private bool GetChance()
	{
		Random.State state = Random.state;
		Random.InitState(Time.frameCount);
		bool result = Random.Range(0f, 1f) <= this.chanceForSecondaryConsume;
		Random.state = state;
		return result;
	}

	// Token: 0x06001AC9 RID: 6857 RVA: 0x00096084 File Offset: 0x00094284
	public override global::ItemModConsumable GetConsumable()
	{
		if (this.GetChance())
		{
			return this.secondaryConsumable;
		}
		return base.GetConsumable();
	}

	// Token: 0x06001ACA RID: 6858 RVA: 0x000960A0 File Offset: 0x000942A0
	public override global::GameObjectRef GetConsumeEffect()
	{
		if (this.GetChance())
		{
			return this.secondaryConsumeEffect;
		}
		return base.GetConsumeEffect();
	}

	// Token: 0x0400158C RID: 5516
	public float chanceForSecondaryConsume = 0.5f;

	// Token: 0x0400158D RID: 5517
	public global::GameObjectRef secondaryConsumeEffect;

	// Token: 0x0400158E RID: 5518
	public global::ItemModConsumable secondaryConsumable;
}
