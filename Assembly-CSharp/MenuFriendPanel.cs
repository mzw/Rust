﻿using System;
using Facepunch.Steamworks;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006D5 RID: 1749
public class MenuFriendPanel : MonoBehaviour
{
	// Token: 0x04001DAB RID: 7595
	public Text friendName;

	// Token: 0x04001DAC RID: 7596
	public Text friendSubtitle;

	// Token: 0x04001DAD RID: 7597
	public RawImage friendAvatar;

	// Token: 0x04001DAE RID: 7598
	public SteamFriend friend;
}
