﻿using System;
using UnityEngine;

// Token: 0x020000CF RID: 207
public class TreadEffects : MonoBehaviour
{
	// Token: 0x040005A6 RID: 1446
	public ParticleSystem[] rearTreadDirt;

	// Token: 0x040005A7 RID: 1447
	public ParticleSystem[] rearTreadSmoke;

	// Token: 0x040005A8 RID: 1448
	public ParticleSystem[] middleTreadSmoke;
}
