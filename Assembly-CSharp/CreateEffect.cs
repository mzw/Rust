﻿using System;
using UnityEngine;

// Token: 0x02000408 RID: 1032
public class CreateEffect : MonoBehaviour
{
	// Token: 0x060017CE RID: 6094 RVA: 0x000883F0 File Offset: 0x000865F0
	public void OnEnable()
	{
		global::Effect.client.Run(this.EffectToCreate.resourcePath, base.transform.position, base.transform.up, base.transform.forward);
	}

	// Token: 0x04001256 RID: 4694
	public global::GameObjectRef EffectToCreate;
}
