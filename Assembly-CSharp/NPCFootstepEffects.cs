﻿using System;
using UnityEngine;

// Token: 0x02000329 RID: 809
public class NPCFootstepEffects : global::BaseFootstepEffect
{
	// Token: 0x04000E76 RID: 3702
	public string impactEffectDirectory = "footstep/stag";

	// Token: 0x04000E77 RID: 3703
	public Transform frontLeftFoot;

	// Token: 0x04000E78 RID: 3704
	public Transform frontRightFoot;

	// Token: 0x04000E79 RID: 3705
	public Transform backLeftFoot;

	// Token: 0x04000E7A RID: 3706
	public Transform backRightFoot;
}
