﻿using System;

// Token: 0x020000F4 RID: 244
public class Stag : global::BaseAnimalNPC
{
	// Token: 0x170000AD RID: 173
	// (get) Token: 0x06000C25 RID: 3109 RVA: 0x0004FED4 File Offset: 0x0004E0D4
	public override global::BaseEntity.TraitFlag Traits
	{
		get
		{
			return global::BaseEntity.TraitFlag.Alive | global::BaseEntity.TraitFlag.Animal | global::BaseEntity.TraitFlag.Food | global::BaseEntity.TraitFlag.Meat;
		}
	}

	// Token: 0x06000C26 RID: 3110 RVA: 0x0004FED8 File Offset: 0x0004E0D8
	public override bool WantsToEat(global::BaseEntity best)
	{
		if (best.HasTrait(global::BaseEntity.TraitFlag.Alive))
		{
			return false;
		}
		if (best.HasTrait(global::BaseEntity.TraitFlag.Meat))
		{
			return false;
		}
		global::CollectibleEntity collectibleEntity = best as global::CollectibleEntity;
		if (collectibleEntity != null)
		{
			foreach (global::ItemAmount itemAmount in collectibleEntity.itemList)
			{
				if (itemAmount.itemDef.category == global::ItemCategory.Food)
				{
					return true;
				}
			}
		}
		return base.WantsToEat(best);
	}

	// Token: 0x06000C27 RID: 3111 RVA: 0x0004FF50 File Offset: 0x0004E150
	public override string Categorize()
	{
		return "Stag";
	}

	// Token: 0x040006B6 RID: 1718
	[ServerVar(Help = "Population active on the server, per square km")]
	public static float Population = 3f;
}
