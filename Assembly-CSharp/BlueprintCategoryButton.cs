﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006A0 RID: 1696
public class BlueprintCategoryButton : MonoBehaviour, global::IInventoryChanged
{
	// Token: 0x04001CB6 RID: 7350
	public Text amountLabel;

	// Token: 0x04001CB7 RID: 7351
	public global::ItemCategory Category;

	// Token: 0x04001CB8 RID: 7352
	public GameObject BackgroundHighlight;

	// Token: 0x04001CB9 RID: 7353
	public AudioClip clickSound;

	// Token: 0x04001CBA RID: 7354
	public AudioClip hoverSound;
}
