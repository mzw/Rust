﻿using System;
using UnityEngine;

// Token: 0x020007F5 RID: 2037
public class ExplosionsFPS : MonoBehaviour
{
	// Token: 0x060025AC RID: 9644 RVA: 0x000D05E8 File Offset: 0x000CE7E8
	private void Awake()
	{
		this.guiStyleHeader.fontSize = 14;
		this.guiStyleHeader.normal.textColor = new Color(1f, 1f, 1f);
	}

	// Token: 0x060025AD RID: 9645 RVA: 0x000D061C File Offset: 0x000CE81C
	private void OnGUI()
	{
		GUI.Label(new Rect(0f, 0f, 30f, 30f), "FPS: " + (int)this.fps, this.guiStyleHeader);
	}

	// Token: 0x060025AE RID: 9646 RVA: 0x000D0658 File Offset: 0x000CE858
	private void Update()
	{
		this.timeleft -= Time.deltaTime;
		this.frames++;
		if ((double)this.timeleft <= 0.0)
		{
			this.fps = (float)this.frames;
			this.timeleft = 1f;
			this.frames = 0;
		}
	}

	// Token: 0x040021C8 RID: 8648
	private readonly GUIStyle guiStyleHeader = new GUIStyle();

	// Token: 0x040021C9 RID: 8649
	private float timeleft;

	// Token: 0x040021CA RID: 8650
	private float fps;

	// Token: 0x040021CB RID: 8651
	private int frames;
}
