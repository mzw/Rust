﻿using System;
using Rust.Ai;
using UnityEngine;
using UnityEngine.AI;

// Token: 0x020001BE RID: 446
public class RealmedNavMeshObstacle : global::BasePrefab
{
	// Token: 0x06000E8C RID: 3724 RVA: 0x0005BBE4 File Offset: 0x00059DE4
	public override void PreProcess(global::IPrefabProcessor process, GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		if (bundling)
		{
			return;
		}
		base.PreProcess(process, rootObj, name, serverside, clientside, false);
		if (base.isServer && this.Obstacle)
		{
			if (Rust.Ai.AiManager.nav_disable)
			{
				process.RemoveComponent(this.Obstacle);
				this.Obstacle = null;
			}
			else if (Rust.Ai.AiManager.nav_obstacles_carve_state >= 2)
			{
				this.Obstacle.carving = true;
			}
			else if (Rust.Ai.AiManager.nav_obstacles_carve_state == 1)
			{
				this.Obstacle.carving = (this.Obstacle.gameObject.layer == 21);
			}
			else
			{
				this.Obstacle.carving = false;
			}
		}
		process.RemoveComponent(this);
	}

	// Token: 0x040008A3 RID: 2211
	public NavMeshObstacle Obstacle;
}
