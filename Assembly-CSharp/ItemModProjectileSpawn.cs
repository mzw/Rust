﻿using System;
using UnityEngine;

// Token: 0x020004EF RID: 1263
public class ItemModProjectileSpawn : global::ItemModProjectile
{
	// Token: 0x06001AFC RID: 6908 RVA: 0x00096F78 File Offset: 0x00095178
	public override void ServerProjectileHit(global::HitInfo info)
	{
		for (int i = 0; i < this.numToCreateChances; i++)
		{
			if (this.createOnImpact.isValid && Random.Range(0f, 1f) < this.createOnImpactChance)
			{
				global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.createOnImpact.resourcePath, default(Vector3), default(Quaternion), true);
				if (baseEntity)
				{
					baseEntity.transform.position = info.HitPositionWorld + info.HitNormalWorld * 0.1f;
					baseEntity.transform.rotation = Quaternion.LookRotation(info.HitNormalWorld);
					baseEntity.Spawn();
					if (this.spreadAngle > 0f)
					{
						Vector3 modifiedAimConeDirection = global::AimConeUtil.GetModifiedAimConeDirection(this.spreadAngle, info.HitNormalWorld, true);
						baseEntity.SetVelocity(modifiedAimConeDirection * Random.Range(1f, 3f));
					}
				}
			}
		}
		base.ServerProjectileHit(info);
	}

	// Token: 0x040015C1 RID: 5569
	public float createOnImpactChance;

	// Token: 0x040015C2 RID: 5570
	public global::GameObjectRef createOnImpact = new global::GameObjectRef();

	// Token: 0x040015C3 RID: 5571
	public float spreadAngle = 30f;

	// Token: 0x040015C4 RID: 5572
	public float spreadVelocityMin = 1f;

	// Token: 0x040015C5 RID: 5573
	public float spreadVelocityMax = 3f;

	// Token: 0x040015C6 RID: 5574
	public int numToCreateChances = 1;
}
