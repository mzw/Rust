﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200051E RID: 1310
public class PathList
{
	// Token: 0x06001BD3 RID: 7123 RVA: 0x0009BB7C File Offset: 0x00099D7C
	public PathList(string name, Vector3[] points)
	{
		this.Name = name;
		this.Path = new global::PathInterpolator(points);
	}

	// Token: 0x06001BD4 RID: 7124 RVA: 0x0009BB98 File Offset: 0x00099D98
	private void SpawnObjectsNeighborAligned(ref uint seed, global::Prefab[] prefabs, List<Vector3> positions, global::SpawnFilter filter = null)
	{
		if (positions.Count < 2)
		{
			return;
		}
		for (int i = 0; i < positions.Count; i++)
		{
			int index = Mathf.Max(i - 1, 0);
			int index2 = Mathf.Min(i + 1, positions.Count - 1);
			Vector3 position = positions[i];
			Vector3 vector = Vector3Ex.XZ3D(positions[index2] - positions[index]);
			Quaternion rotation = Quaternion.LookRotation(vector);
			this.SpawnObject(ref seed, prefabs, position, rotation, filter);
		}
	}

	// Token: 0x06001BD5 RID: 7125 RVA: 0x0009BC20 File Offset: 0x00099E20
	private bool SpawnObject(ref uint seed, global::Prefab[] prefabs, Vector3 position, Quaternion rotation, global::SpawnFilter filter = null)
	{
		global::Prefab random = prefabs.GetRandom(ref seed);
		Vector3 vector = position;
		Quaternion quaternion = rotation;
		Vector3 localScale = random.Object.transform.localScale;
		random.ApplyDecorComponents(ref vector, ref quaternion, ref localScale);
		if (!random.ApplyTerrainAnchors(ref vector, quaternion, localScale, filter))
		{
			return false;
		}
		random.ApplyTerrainPlacements(vector, quaternion, localScale);
		random.ApplyTerrainModifiers(vector, quaternion, localScale);
		global::World.Serialization.AddPrefab(this.Name, random.ID, vector, quaternion, localScale);
		return true;
	}

	// Token: 0x06001BD6 RID: 7126 RVA: 0x0009BC98 File Offset: 0x00099E98
	private bool CheckObjects(global::Prefab[] prefabs, Vector3 position, Quaternion rotation, global::SpawnFilter filter = null)
	{
		foreach (global::Prefab prefab in prefabs)
		{
			Vector3 vector = position;
			Vector3 localScale = prefab.Object.transform.localScale;
			if (!prefab.ApplyTerrainAnchors(ref vector, rotation, localScale, filter))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06001BD7 RID: 7127 RVA: 0x0009BCE8 File Offset: 0x00099EE8
	private void SpawnObject(ref uint seed, global::Prefab[] prefabs, Vector3 pos, Vector3 dir, global::PathList.BasicObject obj)
	{
		if (!obj.AlignToNormal)
		{
			dir = Vector3Ex.XZ3D(dir).normalized;
		}
		global::SpawnFilter filter = obj.Filter;
		Vector3 vector = (this.Width * 0.5f + obj.Offset) * (global::PathList.rot90 * dir);
		for (int i = 0; i < global::PathList.placements.Length; i++)
		{
			if (obj.Placement != global::PathList.Placement.Center || i == 0)
			{
				if (obj.Placement != global::PathList.Placement.Side || i != 0)
				{
					Vector3 vector2 = pos + global::PathList.placements[i] * vector;
					if (obj.HeightToTerrain)
					{
						vector2.y = global::TerrainMeta.HeightMap.GetHeight(vector2);
					}
					if (filter.Test(vector2))
					{
						Quaternion rotation = (i != 2) ? Quaternion.LookRotation(dir) : Quaternion.LookRotation(global::PathList.rot180 * dir);
						if (this.SpawnObject(ref seed, prefabs, vector2, rotation, filter))
						{
							break;
						}
					}
				}
			}
		}
	}

	// Token: 0x06001BD8 RID: 7128 RVA: 0x0009BE0C File Offset: 0x0009A00C
	private bool CheckObjects(global::Prefab[] prefabs, Vector3 pos, Vector3 dir, global::PathList.BasicObject obj)
	{
		if (!obj.AlignToNormal)
		{
			dir = Vector3Ex.XZ3D(dir).normalized;
		}
		global::SpawnFilter filter = obj.Filter;
		Vector3 vector = (this.Width * 0.5f + obj.Offset) * (global::PathList.rot90 * dir);
		for (int i = 0; i < global::PathList.placements.Length; i++)
		{
			if (obj.Placement != global::PathList.Placement.Center || i == 0)
			{
				if (obj.Placement != global::PathList.Placement.Side || i != 0)
				{
					Vector3 vector2 = pos + global::PathList.placements[i] * vector;
					if (obj.HeightToTerrain)
					{
						vector2.y = global::TerrainMeta.HeightMap.GetHeight(vector2);
					}
					if (filter.Test(vector2))
					{
						Quaternion rotation = (i != 2) ? Quaternion.LookRotation(dir) : Quaternion.LookRotation(global::PathList.rot180 * dir);
						if (this.CheckObjects(prefabs, vector2, rotation, filter))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	// Token: 0x06001BD9 RID: 7129 RVA: 0x0009BF2C File Offset: 0x0009A12C
	public void SpawnSide(ref uint seed, global::PathList.SideObject obj)
	{
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		global::PathList.Side side = obj.Side;
		global::SpawnFilter filter = obj.Filter;
		float density = obj.Density;
		float distance = obj.Distance;
		float num = this.Width * 0.5f + obj.Offset;
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		float[] array2 = new float[]
		{
			-num,
			num
		};
		int num2 = 0;
		Vector3 vector = this.Path.GetStartPoint();
		List<Vector3> list = new List<Vector3>();
		float num3 = distance * 0.25f;
		float num4 = distance * 0.5f;
		float num5 = this.Path.StartOffset + num4;
		float num6 = this.Path.Length - this.Path.EndOffset - num4;
		for (float num7 = num5; num7 <= num6; num7 += num3)
		{
			Vector3 vector2 = (!this.Spline) ? this.Path.GetPoint(num7) : this.Path.GetPointCubicHermite(num7);
			if ((vector2 - vector).magnitude >= distance)
			{
				Vector3 tangent = this.Path.GetTangent(num7);
				Vector3 vector3 = global::PathList.rot90 * tangent;
				for (int i = 0; i < array2.Length; i++)
				{
					int num8 = (num2 + i) % array2.Length;
					if (side != global::PathList.Side.Left || num8 == 0)
					{
						if (side != global::PathList.Side.Right || num8 == 1)
						{
							float num9 = array2[num8];
							Vector3 vector4 = vector2;
							vector4.x += vector3.x * num9;
							vector4.z += vector3.z * num9;
							float normX = global::TerrainMeta.NormalizeX(vector4.x);
							float normZ = global::TerrainMeta.NormalizeZ(vector4.z);
							if (filter.GetFactor(normX, normZ) >= SeedRandom.Value(ref seed))
							{
								if (density >= SeedRandom.Value(ref seed))
								{
									vector4.y = heightMap.GetHeight(normX, normZ);
									if (obj.Alignment == global::PathList.Alignment.None)
									{
										if (!this.SpawnObject(ref seed, array, vector4, Quaternion.identity, filter))
										{
											goto IL_2DF;
										}
									}
									else if (obj.Alignment == global::PathList.Alignment.Forward)
									{
										if (!this.SpawnObject(ref seed, array, vector4, Quaternion.LookRotation(tangent * num9), filter))
										{
											goto IL_2DF;
										}
									}
									else if (obj.Alignment == global::PathList.Alignment.Inward)
									{
										if (!this.SpawnObject(ref seed, array, vector4, Quaternion.LookRotation(-vector3 * num9), filter))
										{
											goto IL_2DF;
										}
									}
									else
									{
										list.Add(vector4);
									}
								}
								num2 = num8;
								vector = vector2;
								if (side == global::PathList.Side.Any)
								{
									break;
								}
							}
						}
					}
					IL_2DF:;
				}
			}
		}
		if (list.Count > 0)
		{
			this.SpawnObjectsNeighborAligned(ref seed, array, list, filter);
		}
	}

	// Token: 0x06001BDA RID: 7130 RVA: 0x0009C254 File Offset: 0x0009A454
	public void SpawnAlong(ref uint seed, global::PathList.PathObject obj)
	{
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		global::SpawnFilter filter = obj.Filter;
		float density = obj.Density;
		float distance = obj.Distance;
		float dithering = obj.Dithering;
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		Vector3 vector = this.Path.GetStartPoint();
		List<Vector3> list = new List<Vector3>();
		float num = distance * 0.25f;
		float num2 = distance * 0.5f;
		float num3 = this.Path.StartOffset + num2;
		float num4 = this.Path.Length - this.Path.EndOffset - num2;
		for (float num5 = num3; num5 <= num4; num5 += num)
		{
			Vector3 vector2 = (!this.Spline) ? this.Path.GetPoint(num5) : this.Path.GetPointCubicHermite(num5);
			if ((vector2 - vector).magnitude >= distance)
			{
				Vector3 tangent = this.Path.GetTangent(num5);
				Vector3 vector3 = global::PathList.rot90 * tangent;
				Vector3 vector4 = vector2;
				vector4.x += SeedRandom.Range(ref seed, -dithering, dithering);
				vector4.z += SeedRandom.Range(ref seed, -dithering, dithering);
				float normX = global::TerrainMeta.NormalizeX(vector4.x);
				float normZ = global::TerrainMeta.NormalizeZ(vector4.z);
				if (filter.GetFactor(normX, normZ) >= SeedRandom.Value(ref seed))
				{
					if (density >= SeedRandom.Value(ref seed))
					{
						vector4.y = heightMap.GetHeight(normX, normZ);
						if (obj.Alignment == global::PathList.Alignment.None)
						{
							if (!this.SpawnObject(ref seed, array, vector4, Quaternion.identity, filter))
							{
								goto IL_24E;
							}
						}
						else if (obj.Alignment == global::PathList.Alignment.Forward)
						{
							if (!this.SpawnObject(ref seed, array, vector4, Quaternion.LookRotation(tangent), filter))
							{
								goto IL_24E;
							}
						}
						else if (obj.Alignment == global::PathList.Alignment.Inward)
						{
							if (!this.SpawnObject(ref seed, array, vector4, Quaternion.LookRotation(vector3), filter))
							{
								goto IL_24E;
							}
						}
						else
						{
							list.Add(vector4);
						}
					}
					vector = vector2;
				}
			}
			IL_24E:;
		}
		if (list.Count > 0)
		{
			this.SpawnObjectsNeighborAligned(ref seed, array, list, filter);
		}
	}

	// Token: 0x06001BDB RID: 7131 RVA: 0x0009C4D8 File Offset: 0x0009A6D8
	public void SpawnBridge(ref uint seed, global::PathList.BridgeObject obj)
	{
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		Vector3 startPoint = this.Path.GetStartPoint();
		Vector3 endPoint = this.Path.GetEndPoint();
		Vector3 vector = endPoint - startPoint;
		float magnitude = vector.magnitude;
		Vector3 vector2 = vector / magnitude;
		float num = magnitude / obj.Distance;
		int num2 = Mathf.RoundToInt(num);
		float num3 = 0.5f * (num - (float)num2);
		Vector3 vector3 = obj.Distance * vector2;
		Vector3 vector4 = startPoint + (0.5f + num3) * vector3;
		Quaternion rotation = Quaternion.LookRotation(vector2);
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		global::TerrainWaterMap waterMap = global::TerrainMeta.WaterMap;
		for (int i = 0; i < num2; i++)
		{
			float num4 = Mathf.Max(heightMap.GetHeight(vector4), waterMap.GetHeight(vector4)) - 1f;
			if (vector4.y > num4)
			{
				this.SpawnObject(ref seed, array, vector4, rotation, null);
			}
			vector4 += vector3;
		}
	}

	// Token: 0x06001BDC RID: 7132 RVA: 0x0009C624 File Offset: 0x0009A824
	public void SpawnStart(ref uint seed, global::PathList.BasicObject obj)
	{
		if (!this.Start)
		{
			return;
		}
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		Vector3 startPoint = this.Path.GetStartPoint();
		Vector3 startTangent = this.Path.GetStartTangent();
		this.SpawnObject(ref seed, array, startPoint, startTangent, obj);
	}

	// Token: 0x06001BDD RID: 7133 RVA: 0x0009C6B0 File Offset: 0x0009A8B0
	public void SpawnEnd(ref uint seed, global::PathList.BasicObject obj)
	{
		if (!this.End)
		{
			return;
		}
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		Vector3 endPoint = this.Path.GetEndPoint();
		Vector3 dir = -this.Path.GetEndTangent();
		this.SpawnObject(ref seed, array, endPoint, dir, obj);
	}

	// Token: 0x06001BDE RID: 7134 RVA: 0x0009C740 File Offset: 0x0009A940
	public void TrimStart(global::PathList.BasicObject obj)
	{
		if (!this.Start)
		{
			return;
		}
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		Vector3[] points = this.Path.Points;
		Vector3[] tangents = this.Path.Tangents;
		int num = points.Length / 4;
		for (int i = 0; i < num; i++)
		{
			Vector3 pos = points[this.Path.MinIndex + i];
			Vector3 dir = tangents[this.Path.MinIndex + i];
			if (this.CheckObjects(array, pos, dir, obj))
			{
				this.Path.MinIndex += i;
				break;
			}
		}
	}

	// Token: 0x06001BDF RID: 7135 RVA: 0x0009C83C File Offset: 0x0009AA3C
	public void TrimEnd(global::PathList.BasicObject obj)
	{
		if (!this.End)
		{
			return;
		}
		if (string.IsNullOrEmpty(obj.Folder))
		{
			return;
		}
		global::Prefab[] array = global::Prefab.Load("assets/bundled/prefabs/autospawn/" + obj.Folder, null, null, true);
		if (array == null || array.Length == 0)
		{
			Debug.LogError("Empty decor folder: " + obj.Folder);
			return;
		}
		Vector3[] points = this.Path.Points;
		Vector3[] tangents = this.Path.Tangents;
		int num = points.Length / 4;
		for (int i = 0; i < num; i++)
		{
			Vector3 pos = points[this.Path.MaxIndex - i];
			Vector3 dir = -tangents[this.Path.MaxIndex - i];
			if (this.CheckObjects(array, pos, dir, obj))
			{
				this.Path.MaxIndex -= i;
				break;
			}
		}
	}

	// Token: 0x06001BE0 RID: 7136 RVA: 0x0009C93C File Offset: 0x0009AB3C
	public void ResetTrims()
	{
		this.Path.MinIndex = this.Path.DefaultMinIndex;
		this.Path.MaxIndex = this.Path.DefaultMaxIndex;
	}

	// Token: 0x06001BE1 RID: 7137 RVA: 0x0009C96C File Offset: 0x0009AB6C
	public void AdjustTerrainHeight()
	{
		global::TerrainHeightMap heightmap = global::TerrainMeta.HeightMap;
		global::TerrainTopologyMap topomap = global::TerrainMeta.TopologyMap;
		float num = 1f;
		float randomScale = this.RandomScale;
		float outerPadding = this.OuterPadding;
		float innerPadding = this.InnerPadding;
		float outerFade = this.OuterFade;
		float innerFade = this.InnerFade;
		float offset01 = this.TerrainOffset * global::TerrainMeta.OneOverSize.y;
		float num2 = this.Width * 0.5f;
		Vector3 startPoint = this.Path.GetStartPoint();
		Vector3 endPoint = this.Path.GetEndPoint();
		Vector3 vector = this.Path.GetStartTangent();
		Vector3 vector2 = global::PathList.rot90 * vector;
		Vector3 v = startPoint - vector2 * (num2 + outerPadding + outerFade);
		Vector3 v2 = startPoint + vector2 * (num2 + outerPadding + outerFade);
		float num3 = this.Path.Length + num;
		for (float num4 = 0f; num4 < num3; num4 += num)
		{
			Vector3 vector3 = (!this.Spline) ? this.Path.GetPoint(num4) : this.Path.GetPointCubicHermite(num4);
			float num5 = Vector3Ex.Magnitude2D(startPoint - vector3);
			float num6 = Vector3Ex.Magnitude2D(endPoint - vector3);
			float opacity = Mathf.InverseLerp(0f, num2, Mathf.Min(num5, num6));
			float radius = Mathf.Lerp(num2, num2 * randomScale, global::Noise.Billow((double)vector3.x, (double)vector3.z, 2, 0.004999999888241291, 1.0, 2.0, 0.5));
			vector = Vector3Ex.XZ3D(this.Path.GetTangent(num4)).normalized;
			vector2 = global::PathList.rot90 * vector;
			Ray ray = new Ray(vector3, vector);
			Vector3 vector4 = vector3 - vector2 * (radius + outerPadding + outerFade);
			Vector3 vector5 = vector3 + vector2 * (radius + outerPadding + outerFade);
			float yn = global::TerrainMeta.NormalizeY(vector3.y);
			heightmap.ForEach(v, v2, vector4, vector5, delegate(int x, int z)
			{
				float num7 = heightmap.Coordinate(x);
				float num8 = heightmap.Coordinate(z);
				if ((topomap.GetTopology(num7, num8) & this.Topology) != 0)
				{
					return;
				}
				Vector3 vector6 = global::TerrainMeta.Denormalize(new Vector3(num7, yn, num8));
				Vector3 vector7 = ray.ClosestPoint(vector6);
				float num9 = Vector3Ex.Magnitude2D(vector6 - vector7);
				float num10 = Mathf.InverseLerp(radius + outerPadding + outerFade, radius + outerPadding, num9);
				float num11 = Mathf.InverseLerp(radius - innerPadding, radius - innerPadding - innerFade, num9);
				float num12 = global::TerrainMeta.NormalizeY(vector7.y);
				num10 = Mathf.SmoothStep(0f, 1f, num10);
				num11 = Mathf.SmoothStep(0f, 1f, num11);
				heightmap.SetHeight(x, z, num12 + offset01 * num11, opacity * num10);
			});
			v = vector4;
			v2 = vector5;
		}
	}

	// Token: 0x06001BE2 RID: 7138 RVA: 0x0009CC24 File Offset: 0x0009AE24
	public void AdjustTerrainTexture()
	{
		if (this.Splat == 0)
		{
			return;
		}
		global::TerrainSplatMap splatmap = global::TerrainMeta.SplatMap;
		float num = 1f;
		float randomScale = this.RandomScale;
		float outerPadding = this.OuterPadding;
		float innerPadding = this.InnerPadding;
		float num2 = this.Width * 0.5f;
		Vector3 startPoint = this.Path.GetStartPoint();
		Vector3 endPoint = this.Path.GetEndPoint();
		Vector3 vector = this.Path.GetStartTangent();
		Vector3 vector2 = global::PathList.rot90 * vector;
		Vector3 v = startPoint - vector2 * (num2 + outerPadding);
		Vector3 v2 = startPoint + vector2 * (num2 + outerPadding);
		float num3 = this.Path.Length + num;
		for (float num4 = 0f; num4 < num3; num4 += num)
		{
			Vector3 vector3 = (!this.Spline) ? this.Path.GetPoint(num4) : this.Path.GetPointCubicHermite(num4);
			float num5 = Vector3Ex.Magnitude2D(startPoint - vector3);
			float num6 = Vector3Ex.Magnitude2D(endPoint - vector3);
			float opacity = Mathf.InverseLerp(0f, num2, Mathf.Min(num5, num6));
			float radius = Mathf.Lerp(num2, num2 * randomScale, global::Noise.Billow((double)vector3.x, (double)vector3.z, 2, 0.004999999888241291, 1.0, 2.0, 0.5));
			vector = Vector3Ex.XZ3D(this.Path.GetTangent(num4)).normalized;
			vector2 = global::PathList.rot90 * vector;
			Ray ray = new Ray(vector3, vector);
			Vector3 vector4 = vector3 - vector2 * (radius + outerPadding);
			Vector3 vector5 = vector3 + vector2 * (radius + outerPadding);
			float yn = global::TerrainMeta.NormalizeY(vector3.y);
			splatmap.ForEach(v, v2, vector4, vector5, delegate(int x, int z)
			{
				float num7 = splatmap.Coordinate(x);
				float num8 = splatmap.Coordinate(z);
				Vector3 vector6 = global::TerrainMeta.Denormalize(new Vector3(num7, yn, num8));
				Vector3 vector7 = ray.ClosestPoint(vector6);
				float num9 = Vector3Ex.Magnitude2D(vector6 - vector7);
				float num10 = Mathf.InverseLerp(radius + outerPadding, radius - innerPadding, num9);
				splatmap.SetSplat(x, z, this.Splat, num10 * opacity);
			});
			v = vector4;
			v2 = vector5;
		}
	}

	// Token: 0x06001BE3 RID: 7139 RVA: 0x0009CE88 File Offset: 0x0009B088
	public void AdjustTerrainTopology()
	{
		if (this.Topology == 0)
		{
			return;
		}
		global::TerrainTopologyMap topomap = global::TerrainMeta.TopologyMap;
		float num = 1f;
		float randomScale = this.RandomScale;
		float outerPadding = this.OuterPadding;
		float innerPadding = this.InnerPadding;
		float num2 = this.Width * 0.5f;
		Vector3 startPoint = this.Path.GetStartPoint();
		Vector3 endPoint = this.Path.GetEndPoint();
		Vector3 vector = this.Path.GetStartTangent();
		Vector3 vector2 = global::PathList.rot90 * vector;
		Vector3 v = startPoint - vector2 * (num2 + outerPadding);
		Vector3 v2 = startPoint + vector2 * (num2 + outerPadding);
		float num3 = this.Path.Length + num;
		for (float num4 = 0f; num4 < num3; num4 += num)
		{
			Vector3 vector3 = (!this.Spline) ? this.Path.GetPoint(num4) : this.Path.GetPointCubicHermite(num4);
			float num5 = Vector3Ex.Magnitude2D(startPoint - vector3);
			float num6 = Vector3Ex.Magnitude2D(endPoint - vector3);
			float opacity = Mathf.InverseLerp(0f, num2, Mathf.Min(num5, num6));
			float radius = Mathf.Lerp(num2, num2 * randomScale, global::Noise.Billow((double)vector3.x, (double)vector3.z, 2, 0.004999999888241291, 1.0, 2.0, 0.5));
			vector = Vector3Ex.XZ3D(this.Path.GetTangent(num4)).normalized;
			vector2 = global::PathList.rot90 * vector;
			Ray ray = new Ray(vector3, vector);
			Vector3 vector4 = vector3 - vector2 * (radius + outerPadding);
			Vector3 vector5 = vector3 + vector2 * (radius + outerPadding);
			float yn = global::TerrainMeta.NormalizeY(vector3.y);
			topomap.ForEach(v, v2, vector4, vector5, delegate(int x, int z)
			{
				float num7 = topomap.Coordinate(x);
				float num8 = topomap.Coordinate(z);
				Vector3 vector6 = global::TerrainMeta.Denormalize(new Vector3(num7, yn, num8));
				Vector3 vector7 = ray.ClosestPoint(vector6);
				float num9 = Vector3Ex.Magnitude2D(vector6 - vector7);
				float num10 = Mathf.InverseLerp(radius + outerPadding, radius - innerPadding, num9);
				if (num10 * opacity > 0.3f)
				{
					topomap.SetTopology(x, z, this.Topology);
				}
			});
			v = vector4;
			v2 = vector5;
		}
	}

	// Token: 0x06001BE4 RID: 7140 RVA: 0x0009D0EC File Offset: 0x0009B2EC
	public List<Mesh> CreateMesh()
	{
		List<Mesh> list = new List<Mesh>();
		float num = 8f;
		float num2 = 64f;
		float randomScale = this.RandomScale;
		float meshOffset = this.MeshOffset;
		float num3 = this.Width * 0.5f;
		int capacity = (int)(this.Path.Length / num) * 2;
		int capacity2 = (int)(this.Path.Length / num) * 3;
		List<Vector3> list2 = new List<Vector3>(capacity);
		List<Color> list3 = new List<Color>(capacity);
		List<Vector2> list4 = new List<Vector2>(capacity);
		List<Vector3> list5 = new List<Vector3>(capacity);
		List<Vector4> list6 = new List<Vector4>(capacity);
		List<int> list7 = new List<int>(capacity2);
		global::TerrainHeightMap heightMap = global::TerrainMeta.HeightMap;
		Vector2 item;
		item..ctor(0f, 0f);
		Vector2 item2;
		item2..ctor(1f, 0f);
		Vector3 vector = Vector3.zero;
		Vector3 vector2 = Vector3.zero;
		Vector3 vector3 = Vector3.zero;
		Vector3 vector4 = Vector3.zero;
		int num4 = -1;
		int num5 = -1;
		float num6 = this.Path.Length + num;
		for (float num7 = 0f; num7 < num6; num7 += num)
		{
			Vector3 vector5 = (!this.Spline) ? this.Path.GetPoint(num7) : this.Path.GetPointCubicHermite(num7);
			float num8 = Mathf.Lerp(num3, num3 * randomScale, global::Noise.Billow((double)vector5.x, (double)vector5.z, 2, 0.004999999888241291, 1.0, 2.0, 0.5));
			Vector3 tangent = this.Path.GetTangent(num7);
			Vector3 normalized = Vector3Ex.XZ3D(tangent).normalized;
			Vector3 vector6 = global::PathList.rot90 * normalized;
			Vector4 item3;
			item3..ctor(vector6.x, vector6.y, vector6.z, 1f);
			Vector3 item4 = Vector3.Slerp(Vector3.Cross(tangent, vector6), Vector3.up, 0.1f);
			Vector3 vector7;
			vector7..ctor(vector5.x - vector6.x * num8, 0f, vector5.z - vector6.z * num8);
			vector7.y = Mathf.Min(vector5.y, heightMap.GetHeight(vector7)) + meshOffset;
			Vector3 vector8;
			vector8..ctor(vector5.x + vector6.x * num8, 0f, vector5.z + vector6.z * num8);
			vector8.y = Mathf.Min(vector5.y, heightMap.GetHeight(vector8)) + meshOffset;
			if (num7 != 0f)
			{
				float num9 = Vector3Ex.Magnitude2D(vector5 - vector3) / (2f * num8);
				item.y += num9;
				item2.y += num9;
				Vector3 vector9 = Vector3Ex.XZ3D(vector7 - vector);
				if (Vector3.Dot(vector9, vector4) <= 0f)
				{
					vector7 = vector;
				}
				Vector3 vector10 = Vector3Ex.XZ3D(vector8 - vector2);
				if (Vector3.Dot(vector10, vector4) <= 0f)
				{
					vector8 = vector2;
				}
			}
			Color item5 = (num7 <= 0f || num7 + num >= num6) ? new Color(1f, 1f, 1f, 0f) : new Color(1f, 1f, 1f, 1f);
			list4.Add(item);
			list3.Add(item5);
			list2.Add(vector7);
			list5.Add(item4);
			list6.Add(item3);
			int num10 = list2.Count - 1;
			if (num4 != -1 && num5 != -1)
			{
				list7.Add(num10);
				list7.Add(num5);
				list7.Add(num4);
			}
			num4 = num10;
			vector = vector7;
			list4.Add(item2);
			list3.Add(item5);
			list2.Add(vector8);
			list5.Add(item4);
			list6.Add(item3);
			int num11 = list2.Count - 1;
			if (num4 != -1 && num5 != -1)
			{
				list7.Add(num11);
				list7.Add(num5);
				list7.Add(num4);
			}
			num5 = num11;
			vector2 = vector8;
			vector3 = vector5;
			vector4 = normalized;
			if (list2.Count >= 100 && this.Path.Length - num7 > num2)
			{
				Mesh mesh = new Mesh();
				mesh.SetVertices(list2);
				mesh.SetColors(list3);
				mesh.SetUVs(0, list4);
				mesh.SetTriangles(list7, 0);
				mesh.SetNormals(list5);
				mesh.SetTangents(list6);
				list.Add(mesh);
				list2.Clear();
				list3.Clear();
				list4.Clear();
				list5.Clear();
				list6.Clear();
				list7.Clear();
				num4 = -1;
				num5 = -1;
				num7 -= num;
			}
		}
		if (list7.Count > 0)
		{
			Mesh mesh2 = new Mesh();
			mesh2.SetVertices(list2);
			mesh2.SetColors(list3);
			mesh2.SetUVs(0, list4);
			mesh2.SetTriangles(list7, 0);
			mesh2.SetNormals(list5);
			mesh2.SetTangents(list6);
			list.Add(mesh2);
		}
		return list;
	}

	// Token: 0x04001660 RID: 5728
	private static Quaternion rot90 = Quaternion.Euler(0f, 90f, 0f);

	// Token: 0x04001661 RID: 5729
	private static Quaternion rot180 = Quaternion.Euler(0f, 180f, 0f);

	// Token: 0x04001662 RID: 5730
	public string Name;

	// Token: 0x04001663 RID: 5731
	public global::PathInterpolator Path;

	// Token: 0x04001664 RID: 5732
	public bool Spline;

	// Token: 0x04001665 RID: 5733
	public bool Start;

	// Token: 0x04001666 RID: 5734
	public bool End;

	// Token: 0x04001667 RID: 5735
	public float Width;

	// Token: 0x04001668 RID: 5736
	public float InnerPadding;

	// Token: 0x04001669 RID: 5737
	public float OuterPadding;

	// Token: 0x0400166A RID: 5738
	public float InnerFade;

	// Token: 0x0400166B RID: 5739
	public float OuterFade;

	// Token: 0x0400166C RID: 5740
	public float RandomScale;

	// Token: 0x0400166D RID: 5741
	public float MeshOffset;

	// Token: 0x0400166E RID: 5742
	public float TerrainOffset;

	// Token: 0x0400166F RID: 5743
	public int Topology;

	// Token: 0x04001670 RID: 5744
	public int Splat;

	// Token: 0x04001671 RID: 5745
	public const float StepSize = 1f;

	// Token: 0x04001672 RID: 5746
	public const float MeshStepSize = 8f;

	// Token: 0x04001673 RID: 5747
	public const float MeshNormalSmoothing = 0.1f;

	// Token: 0x04001674 RID: 5748
	public const int SubMeshVerts = 100;

	// Token: 0x04001675 RID: 5749
	private static float[] placements = new float[]
	{
		0f,
		-1f,
		1f
	};

	// Token: 0x0200051F RID: 1311
	public enum Side
	{
		// Token: 0x04001677 RID: 5751
		Both,
		// Token: 0x04001678 RID: 5752
		Left,
		// Token: 0x04001679 RID: 5753
		Right,
		// Token: 0x0400167A RID: 5754
		Any
	}

	// Token: 0x02000520 RID: 1312
	public enum Placement
	{
		// Token: 0x0400167C RID: 5756
		Center,
		// Token: 0x0400167D RID: 5757
		Side
	}

	// Token: 0x02000521 RID: 1313
	public enum Alignment
	{
		// Token: 0x0400167F RID: 5759
		None,
		// Token: 0x04001680 RID: 5760
		Neighbor,
		// Token: 0x04001681 RID: 5761
		Forward,
		// Token: 0x04001682 RID: 5762
		Inward
	}

	// Token: 0x02000522 RID: 1314
	[Serializable]
	public class BasicObject
	{
		// Token: 0x04001683 RID: 5763
		public string Folder;

		// Token: 0x04001684 RID: 5764
		public global::SpawnFilter Filter;

		// Token: 0x04001685 RID: 5765
		public global::PathList.Placement Placement;

		// Token: 0x04001686 RID: 5766
		public bool AlignToNormal = true;

		// Token: 0x04001687 RID: 5767
		public bool HeightToTerrain = true;

		// Token: 0x04001688 RID: 5768
		public float Offset;
	}

	// Token: 0x02000523 RID: 1315
	[Serializable]
	public class SideObject
	{
		// Token: 0x04001689 RID: 5769
		public string Folder;

		// Token: 0x0400168A RID: 5770
		public global::SpawnFilter Filter;

		// Token: 0x0400168B RID: 5771
		public global::PathList.Side Side;

		// Token: 0x0400168C RID: 5772
		public global::PathList.Alignment Alignment;

		// Token: 0x0400168D RID: 5773
		public float Density = 1f;

		// Token: 0x0400168E RID: 5774
		public float Distance = 25f;

		// Token: 0x0400168F RID: 5775
		public float Offset = 2f;
	}

	// Token: 0x02000524 RID: 1316
	[Serializable]
	public class PathObject
	{
		// Token: 0x04001690 RID: 5776
		public string Folder;

		// Token: 0x04001691 RID: 5777
		public global::SpawnFilter Filter;

		// Token: 0x04001692 RID: 5778
		public global::PathList.Alignment Alignment;

		// Token: 0x04001693 RID: 5779
		public float Density = 1f;

		// Token: 0x04001694 RID: 5780
		public float Distance = 5f;

		// Token: 0x04001695 RID: 5781
		public float Dithering = 5f;
	}

	// Token: 0x02000525 RID: 1317
	[Serializable]
	public class BridgeObject
	{
		// Token: 0x04001696 RID: 5782
		public string Folder;

		// Token: 0x04001697 RID: 5783
		public float Distance = 10f;
	}
}
