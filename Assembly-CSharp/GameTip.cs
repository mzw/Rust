﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200068A RID: 1674
public class GameTip : SingletonComponent<global::GameTip>
{
	// Token: 0x04001C5B RID: 7259
	public CanvasGroup canvasGroup;

	// Token: 0x04001C5C RID: 7260
	public Text text;
}
