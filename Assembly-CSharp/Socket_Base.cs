﻿using System;
using UnityEngine;

// Token: 0x0200021D RID: 541
public class Socket_Base : global::PrefabAttribute
{
	// Token: 0x06000FBF RID: 4031 RVA: 0x00060070 File Offset: 0x0005E270
	public Vector3 GetSelectPivot(Vector3 position, Quaternion rotation)
	{
		return position + rotation * this.worldPosition;
	}

	// Token: 0x06000FC0 RID: 4032 RVA: 0x00060084 File Offset: 0x0005E284
	public OBB GetSelectBounds(Vector3 position, Quaternion rotation)
	{
		return new OBB(position + rotation * this.worldPosition, Vector3.one, rotation * this.worldRotation, new Bounds(this.selectCenter, this.selectSize));
	}

	// Token: 0x06000FC1 RID: 4033 RVA: 0x000600C0 File Offset: 0x0005E2C0
	protected override Type GetIndexedType()
	{
		return typeof(global::Socket_Base);
	}

	// Token: 0x06000FC2 RID: 4034 RVA: 0x000600CC File Offset: 0x0005E2CC
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		base.AttributeSetup(rootObj, name, serverside, clientside, bundling);
		this.position = base.transform.position;
		this.rotation = base.transform.rotation;
		this.socketMods = base.GetComponentsInChildren<global::SocketMod>(true);
		foreach (global::SocketMod socketMod in this.socketMods)
		{
			socketMod.baseSocket = this;
		}
	}

	// Token: 0x06000FC3 RID: 4035 RVA: 0x0006013C File Offset: 0x0005E33C
	public virtual bool TestTarget(global::Construction.Target target)
	{
		return target.socket != null;
	}

	// Token: 0x06000FC4 RID: 4036 RVA: 0x0006014C File Offset: 0x0005E34C
	public virtual bool IsCompatible(global::Socket_Base socket)
	{
		return !(socket == null) && (socket.male || this.male) && (socket.female || this.female) && socket.GetType() == base.GetType();
	}

	// Token: 0x06000FC5 RID: 4037 RVA: 0x000601A8 File Offset: 0x0005E3A8
	public virtual bool CanConnect(Vector3 position, Quaternion rotation, global::Socket_Base socket, Vector3 socketPosition, Quaternion socketRotation)
	{
		return this.IsCompatible(socket);
	}

	// Token: 0x06000FC6 RID: 4038 RVA: 0x000601B4 File Offset: 0x0005E3B4
	public virtual global::Construction.Placement DoPlacement(global::Construction.Target target)
	{
		Quaternion quaternion = Quaternion.Euler(target.rotation) * Quaternion.LookRotation(target.normal, Vector3.up);
		Vector3 vector = target.position;
		vector -= quaternion * this.position;
		return new global::Construction.Placement
		{
			rotation = quaternion,
			position = vector
		};
	}

	// Token: 0x06000FC7 RID: 4039 RVA: 0x00060214 File Offset: 0x0005E414
	public virtual bool CheckSocketMods(global::Construction.Placement placement)
	{
		foreach (global::SocketMod socketMod in this.socketMods)
		{
			socketMod.ModifyPlacement(placement);
		}
		foreach (global::SocketMod socketMod2 in this.socketMods)
		{
			if (!socketMod2.DoCheck(placement))
			{
				if (socketMod2.FailedPhrase.IsValid())
				{
					global::Construction.lastPlacementError = "Failed Check: (" + socketMod2.FailedPhrase.translated + ")";
				}
				return false;
			}
		}
		return true;
	}

	// Token: 0x04000A83 RID: 2691
	public bool male = true;

	// Token: 0x04000A84 RID: 2692
	public bool maleDummy;

	// Token: 0x04000A85 RID: 2693
	public bool female;

	// Token: 0x04000A86 RID: 2694
	public bool femaleDummy;

	// Token: 0x04000A87 RID: 2695
	public bool monogamous;

	// Token: 0x04000A88 RID: 2696
	[NonSerialized]
	public Vector3 position;

	// Token: 0x04000A89 RID: 2697
	[NonSerialized]
	public Quaternion rotation;

	// Token: 0x04000A8A RID: 2698
	public Vector3 selectSize = new Vector3(2f, 0.1f, 2f);

	// Token: 0x04000A8B RID: 2699
	public Vector3 selectCenter = new Vector3(0f, 0f, 1f);

	// Token: 0x04000A8C RID: 2700
	[HideInInspector]
	public string socketName;

	// Token: 0x04000A8D RID: 2701
	[NonSerialized]
	public global::SocketMod[] socketMods;
}
