﻿using System;
using UnityEngine;

// Token: 0x020005E3 RID: 1507
public abstract class TerrainPlacement : global::PrefabAttribute
{
	// Token: 0x06001EEC RID: 7916 RVA: 0x000AF3AC File Offset: 0x000AD5AC
	protected void OnValidate()
	{
		if (!Application.isPlaying && Terrain.activeTerrain)
		{
			TerrainData terrainData = Terrain.activeTerrain.terrainData;
			global::TerrainHeightMap component = Terrain.activeTerrain.GetComponent<global::TerrainHeightMap>();
			if (component)
			{
				this.heightmap = component.HeightTexture;
			}
			global::TerrainSplatMap component2 = Terrain.activeTerrain.GetComponent<global::TerrainSplatMap>();
			if (component2)
			{
				this.splatmap0 = component2.SplatTexture0;
				this.splatmap1 = component2.SplatTexture1;
			}
			global::TerrainAlphaMap component3 = Terrain.activeTerrain.GetComponent<global::TerrainAlphaMap>();
			if (component3)
			{
				this.alphamap = component3.AlphaTexture;
			}
			global::TerrainBiomeMap component4 = Terrain.activeTerrain.GetComponent<global::TerrainBiomeMap>();
			if (component4)
			{
				this.biomemap = component4.BiomeTexture;
			}
			global::TerrainTopologyMap component5 = Terrain.activeTerrain.GetComponent<global::TerrainTopologyMap>();
			if (component5)
			{
				this.topologymap = component5.TopologyTexture;
			}
			global::TerrainWaterMap component6 = Terrain.activeTerrain.GetComponent<global::TerrainWaterMap>();
			if (component6)
			{
				this.watermap = component6.WaterTexture;
			}
			global::TerrainBlendMap component7 = Terrain.activeTerrain.GetComponent<global::TerrainBlendMap>();
			if (component7)
			{
				this.blendmap = component7.BlendTexture;
			}
			this.size = terrainData.size;
			this.extents = terrainData.size * 0.5f;
			this.offset = Terrain.activeTerrain.GetPosition() + Vector3Ex.XZ3D(terrainData.size) * 0.5f - base.transform.position;
		}
	}

	// Token: 0x06001EED RID: 7917 RVA: 0x000AF540 File Offset: 0x000AD740
	public void Apply(Matrix4x4 localToWorld, Matrix4x4 worldToLocal)
	{
		if (this.ShouldHeight())
		{
			this.ApplyHeight(localToWorld, worldToLocal);
		}
		if (this.ShouldSplat(-1))
		{
			this.ApplySplat(localToWorld, worldToLocal);
		}
		if (this.ShouldAlpha())
		{
			this.ApplyAlpha(localToWorld, worldToLocal);
		}
		if (this.ShouldBiome(-1))
		{
			this.ApplyBiome(localToWorld, worldToLocal);
		}
		if (this.ShouldTopology(-1))
		{
			this.ApplyTopology(localToWorld, worldToLocal);
		}
		if (this.ShouldWater())
		{
			this.ApplyWater(localToWorld, worldToLocal);
		}
	}

	// Token: 0x06001EEE RID: 7918 RVA: 0x000AF5C4 File Offset: 0x000AD7C4
	protected bool ShouldHeight()
	{
		return this.heightmap != null && this.HeightMap;
	}

	// Token: 0x06001EEF RID: 7919 RVA: 0x000AF5E0 File Offset: 0x000AD7E0
	protected bool ShouldSplat(int id = -1)
	{
		return this.splatmap0 != null && this.splatmap1 != null && (this.SplatMask & (global::TerrainSplat.Enum)id) != (global::TerrainSplat.Enum)0;
	}

	// Token: 0x06001EF0 RID: 7920 RVA: 0x000AF618 File Offset: 0x000AD818
	protected bool ShouldAlpha()
	{
		return this.alphamap != null && this.AlphaMap;
	}

	// Token: 0x06001EF1 RID: 7921 RVA: 0x000AF634 File Offset: 0x000AD834
	protected bool ShouldBiome(int id = -1)
	{
		return this.biomemap != null && (this.BiomeMask & (global::TerrainBiome.Enum)id) != (global::TerrainBiome.Enum)0;
	}

	// Token: 0x06001EF2 RID: 7922 RVA: 0x000AF658 File Offset: 0x000AD858
	protected bool ShouldTopology(int id = -1)
	{
		return this.topologymap != null && (this.TopologyMask & (global::TerrainTopology.Enum)id) != (global::TerrainTopology.Enum)0;
	}

	// Token: 0x06001EF3 RID: 7923 RVA: 0x000AF67C File Offset: 0x000AD87C
	protected bool ShouldWater()
	{
		return this.watermap != null && this.WaterMap;
	}

	// Token: 0x06001EF4 RID: 7924
	protected abstract void ApplyHeight(Matrix4x4 localToWorld, Matrix4x4 worldToLocal);

	// Token: 0x06001EF5 RID: 7925
	protected abstract void ApplySplat(Matrix4x4 localToWorld, Matrix4x4 worldToLocal);

	// Token: 0x06001EF6 RID: 7926
	protected abstract void ApplyAlpha(Matrix4x4 localToWorld, Matrix4x4 worldToLocal);

	// Token: 0x06001EF7 RID: 7927
	protected abstract void ApplyBiome(Matrix4x4 localToWorld, Matrix4x4 worldToLocal);

	// Token: 0x06001EF8 RID: 7928
	protected abstract void ApplyTopology(Matrix4x4 localToWorld, Matrix4x4 worldToLocal);

	// Token: 0x06001EF9 RID: 7929
	protected abstract void ApplyWater(Matrix4x4 localToWorld, Matrix4x4 worldToLocal);

	// Token: 0x06001EFA RID: 7930 RVA: 0x000AF698 File Offset: 0x000AD898
	protected override Type GetIndexedType()
	{
		return typeof(global::TerrainPlacement);
	}

	// Token: 0x040019D8 RID: 6616
	[ReadOnly]
	public Vector3 size = Vector3.zero;

	// Token: 0x040019D9 RID: 6617
	[ReadOnly]
	public Vector3 extents = Vector3.zero;

	// Token: 0x040019DA RID: 6618
	[ReadOnly]
	public Vector3 offset = Vector3.zero;

	// Token: 0x040019DB RID: 6619
	public bool HeightMap = true;

	// Token: 0x040019DC RID: 6620
	public bool AlphaMap = true;

	// Token: 0x040019DD RID: 6621
	public bool WaterMap;

	// Token: 0x040019DE RID: 6622
	[InspectorFlags]
	public global::TerrainSplat.Enum SplatMask;

	// Token: 0x040019DF RID: 6623
	[InspectorFlags]
	public global::TerrainBiome.Enum BiomeMask;

	// Token: 0x040019E0 RID: 6624
	[InspectorFlags]
	public global::TerrainTopology.Enum TopologyMask;

	// Token: 0x040019E1 RID: 6625
	[HideInInspector]
	public Texture2D heightmap;

	// Token: 0x040019E2 RID: 6626
	[HideInInspector]
	public Texture2D splatmap0;

	// Token: 0x040019E3 RID: 6627
	[HideInInspector]
	public Texture2D splatmap1;

	// Token: 0x040019E4 RID: 6628
	[HideInInspector]
	public Texture2D alphamap;

	// Token: 0x040019E5 RID: 6629
	[HideInInspector]
	public Texture2D biomemap;

	// Token: 0x040019E6 RID: 6630
	[HideInInspector]
	public Texture2D topologymap;

	// Token: 0x040019E7 RID: 6631
	[HideInInspector]
	public Texture2D watermap;

	// Token: 0x040019E8 RID: 6632
	[HideInInspector]
	public Texture2D blendmap;
}
