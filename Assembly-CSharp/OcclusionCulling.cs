﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using RustNative;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000807 RID: 2055
[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(Camera))]
public class OcclusionCulling : MonoBehaviour
{
	// Token: 0x170002C2 RID: 706
	// (get) Token: 0x060025FA RID: 9722 RVA: 0x000D1C38 File Offset: 0x000CFE38
	public static global::OcclusionCulling Instance
	{
		get
		{
			return global::OcclusionCulling.instance;
		}
	}

	// Token: 0x170002C3 RID: 707
	// (get) Token: 0x060025FB RID: 9723 RVA: 0x000D1C40 File Offset: 0x000CFE40
	public static bool Supported
	{
		get
		{
			return global::OcclusionCulling.supportedDeviceTypes.Contains(SystemInfo.graphicsDeviceType);
		}
	}

	// Token: 0x170002C4 RID: 708
	// (get) Token: 0x060025FC RID: 9724 RVA: 0x000D1C54 File Offset: 0x000CFE54
	// (set) Token: 0x060025FD RID: 9725 RVA: 0x000D1C5C File Offset: 0x000CFE5C
	public static bool Enabled
	{
		get
		{
			return global::OcclusionCulling._enabled;
		}
		set
		{
			global::OcclusionCulling._enabled = value;
			if (global::OcclusionCulling.instance != null)
			{
				global::OcclusionCulling.instance.enabled = value;
			}
		}
	}

	// Token: 0x170002C5 RID: 709
	// (get) Token: 0x060025FE RID: 9726 RVA: 0x000D1C80 File Offset: 0x000CFE80
	// (set) Token: 0x060025FF RID: 9727 RVA: 0x000D1C88 File Offset: 0x000CFE88
	public static bool SafeMode
	{
		get
		{
			return global::OcclusionCulling._safeMode;
		}
		set
		{
			global::OcclusionCulling._safeMode = value;
		}
	}

	// Token: 0x170002C6 RID: 710
	// (get) Token: 0x06002600 RID: 9728 RVA: 0x000D1C90 File Offset: 0x000CFE90
	// (set) Token: 0x06002601 RID: 9729 RVA: 0x000D1C98 File Offset: 0x000CFE98
	public static global::OcclusionCulling.DebugFilter DebugShow
	{
		get
		{
			return global::OcclusionCulling._debugShow;
		}
		set
		{
			global::OcclusionCulling._debugShow = value;
		}
	}

	// Token: 0x06002602 RID: 9730 RVA: 0x000D1CA0 File Offset: 0x000CFEA0
	private static void GrowStatePool()
	{
		for (int i = 0; i < 2048; i++)
		{
			global::OcclusionCulling.statePool.Enqueue(new global::OccludeeState());
		}
	}

	// Token: 0x06002603 RID: 9731 RVA: 0x000D1CD4 File Offset: 0x000CFED4
	private static global::OccludeeState Allocate()
	{
		if (global::OcclusionCulling.statePool.Count == 0)
		{
			global::OcclusionCulling.GrowStatePool();
		}
		return global::OcclusionCulling.statePool.Dequeue();
	}

	// Token: 0x06002604 RID: 9732 RVA: 0x000D1CF4 File Offset: 0x000CFEF4
	private static void Release(global::OccludeeState state)
	{
		global::OcclusionCulling.statePool.Enqueue(state);
	}

	// Token: 0x06002605 RID: 9733 RVA: 0x000D1D04 File Offset: 0x000CFF04
	private void Awake()
	{
		global::OcclusionCulling.instance = this;
		this.camera = base.GetComponent<Camera>();
		for (int i = 0; i < 6; i++)
		{
			this.frustumPropNames[i] = "_FrustumPlane" + i;
		}
	}

	// Token: 0x06002606 RID: 9734 RVA: 0x000D1D50 File Offset: 0x000CFF50
	private void OnEnable()
	{
		if (!global::OcclusionCulling.Enabled)
		{
			global::OcclusionCulling.Enabled = false;
			return;
		}
		if (!global::OcclusionCulling.Supported)
		{
			Debug.LogWarning("[OcclusionCulling] HiZ culling disabled due to graphics device type " + SystemInfo.graphicsDeviceType + " not supported.");
			global::OcclusionCulling.Enabled = false;
			return;
		}
		this.usingFallback = (this.debugSettings.forceFallback || !SystemInfo.supportsComputeShaders || this.computeShader == null || !this.computeShader.HasKernel("compute_cull"));
		for (int i = 0; i < global::OcclusionCulling.staticOccludees.Count; i++)
		{
			global::OcclusionCulling.staticChanged.Add(i);
		}
		for (int j = 0; j < global::OcclusionCulling.dynamicOccludees.Count; j++)
		{
			global::OcclusionCulling.dynamicChanged.Add(j);
		}
		if (this.usingFallback)
		{
			this.fallbackMat = new Material(Shader.Find("Hidden/OcclusionCulling/Culling"))
			{
				hideFlags = 61
			};
			global::OcclusionCulling.staticSet.Attach(this.fallbackMat);
			global::OcclusionCulling.dynamicSet.Attach(this.fallbackMat);
			global::OcclusionCulling.gridSet.Attach(this.fallbackMat);
		}
		else
		{
			global::OcclusionCulling.staticSet.Attach(this.computeShader);
			global::OcclusionCulling.dynamicSet.Attach(this.computeShader);
			global::OcclusionCulling.gridSet.Attach(this.computeShader);
		}
		this.depthCopyMat = new Material(Shader.Find("Hidden/OcclusionCulling/DepthCopy"))
		{
			hideFlags = 61
		};
		this.InitializeHiZMap();
		this.UpdateCameraMatrices(true);
		this.TestNativePath();
	}

	// Token: 0x06002607 RID: 9735 RVA: 0x000D1EF4 File Offset: 0x000D00F4
	private void TestNativePath()
	{
		try
		{
			global::OccludeeState.State state = default(global::OccludeeState.State);
			Color32 color;
			color..ctor(0, 0, 0, 0);
			Vector4 zero = Vector4.zero;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			global::OcclusionCulling.ProcessOccludees_Native(ref state, ref num, 0, ref color, 0, ref num2, ref num3, ref zero, 0f, 0u);
		}
		catch (EntryPointNotFoundException)
		{
			Debug.Log("[OcclusionCulling] Fast native path not available. Reverting to managed fallback.");
			this.useNativePath = false;
		}
	}

	// Token: 0x06002608 RID: 9736 RVA: 0x000D1F68 File Offset: 0x000D0168
	private void OnDisable()
	{
		if (this.fallbackMat != null)
		{
			Object.DestroyImmediate(this.fallbackMat);
			this.fallbackMat = null;
		}
		if (this.depthCopyMat != null)
		{
			Object.DestroyImmediate(this.depthCopyMat);
			this.depthCopyMat = null;
		}
		global::OcclusionCulling.staticSet.Dispose(true);
		global::OcclusionCulling.dynamicSet.Dispose(true);
		global::OcclusionCulling.gridSet.Dispose(true);
		this.FinalizeHiZMap();
	}

	// Token: 0x06002609 RID: 9737 RVA: 0x000D1FE4 File Offset: 0x000D01E4
	public static void MakeAllVisible()
	{
		for (int i = 0; i < global::OcclusionCulling.staticOccludees.Count; i++)
		{
			if (global::OcclusionCulling.staticOccludees[i] != null)
			{
				global::OcclusionCulling.staticOccludees[i].MakeVisible();
			}
		}
		for (int j = 0; j < global::OcclusionCulling.dynamicOccludees.Count; j++)
		{
			if (global::OcclusionCulling.dynamicOccludees[j] != null)
			{
				global::OcclusionCulling.dynamicOccludees[j].MakeVisible();
			}
		}
	}

	// Token: 0x0600260A RID: 9738 RVA: 0x000D2068 File Offset: 0x000D0268
	private void Update()
	{
		if (!global::OcclusionCulling.Enabled)
		{
			base.enabled = false;
			return;
		}
		this.CheckResizeHiZMap();
		this.DebugUpdate();
		this.DebugDraw();
	}

	// Token: 0x0600260B RID: 9739 RVA: 0x000D2090 File Offset: 0x000D0290
	public static void RecursiveAddOccludees<T>(Transform transform, float minTimeVisible = 0.1f, bool isStatic = true, bool stickyGizmos = false) where T : global::Occludee
	{
		Renderer component = transform.GetComponent<Renderer>();
		Collider component2 = transform.GetComponent<Collider>();
		if (component != null && component2 != null)
		{
			T t = component.gameObject.GetComponent<T>();
			t = ((!(t == null)) ? t : component.gameObject.AddComponent<T>());
			t.minTimeVisible = minTimeVisible;
			t.isStatic = isStatic;
			t.stickyGizmos = stickyGizmos;
			t.Register();
		}
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform2 = (Transform)obj;
				global::OcclusionCulling.RecursiveAddOccludees<T>(transform2, minTimeVisible, isStatic, stickyGizmos);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	// Token: 0x0600260C RID: 9740 RVA: 0x000D2180 File Offset: 0x000D0380
	private static int FindFreeSlot(global::OcclusionCulling.SimpleList<global::OccludeeState> occludees, global::OcclusionCulling.SimpleList<global::OccludeeState.State> states, Queue<int> recycled)
	{
		int result;
		if (recycled.Count > 0)
		{
			result = recycled.Dequeue();
		}
		else
		{
			if (occludees.Count == occludees.Capacity)
			{
				int num = Mathf.Min(occludees.Capacity + 2048, 1048576);
				if (num > 0)
				{
					occludees.Capacity = num;
					states.Capacity = num;
				}
			}
			if (occludees.Count < occludees.Capacity)
			{
				result = occludees.Count;
				occludees.Add(null);
				states.Add(default(global::OccludeeState.State));
			}
			else
			{
				result = -1;
			}
		}
		return result;
	}

	// Token: 0x0600260D RID: 9741 RVA: 0x000D2218 File Offset: 0x000D0418
	public static global::OccludeeState GetStateById(int id)
	{
		if (id < 0 || id >= 2097152)
		{
			return null;
		}
		bool flag = id < 1048576;
		int index = (!flag) ? (id - 1048576) : id;
		if (flag)
		{
			return global::OcclusionCulling.staticOccludees[index];
		}
		return global::OcclusionCulling.dynamicOccludees[index];
	}

	// Token: 0x0600260E RID: 9742 RVA: 0x000D2274 File Offset: 0x000D0474
	public static int RegisterOccludee(Vector3 center, float radius, bool isVisible, float minTimeVisible, bool isStatic, global::OcclusionCulling.OnVisibilityChanged onVisibilityChanged = null)
	{
		int num;
		if (isStatic)
		{
			num = global::OcclusionCulling.RegisterOccludee(center, radius, isVisible, minTimeVisible, isStatic, onVisibilityChanged, global::OcclusionCulling.staticOccludees, global::OcclusionCulling.staticStates, global::OcclusionCulling.staticRecycled, global::OcclusionCulling.staticChanged, global::OcclusionCulling.staticSet, global::OcclusionCulling.staticVisibilityChanged);
		}
		else
		{
			num = global::OcclusionCulling.RegisterOccludee(center, radius, isVisible, minTimeVisible, isStatic, onVisibilityChanged, global::OcclusionCulling.dynamicOccludees, global::OcclusionCulling.dynamicStates, global::OcclusionCulling.dynamicRecycled, global::OcclusionCulling.dynamicChanged, global::OcclusionCulling.dynamicSet, global::OcclusionCulling.dynamicVisibilityChanged);
		}
		return (num >= 0 && !isStatic) ? (num + 1048576) : num;
	}

	// Token: 0x0600260F RID: 9743 RVA: 0x000D2304 File Offset: 0x000D0504
	private static int RegisterOccludee(Vector3 center, float radius, bool isVisible, float minTimeVisible, bool isStatic, global::OcclusionCulling.OnVisibilityChanged onVisibilityChanged, global::OcclusionCulling.SimpleList<global::OccludeeState> occludees, global::OcclusionCulling.SimpleList<global::OccludeeState.State> states, Queue<int> recycled, List<int> changed, global::OcclusionCulling.BufferSet set, global::OcclusionCulling.SimpleList<int> visibilityChanged)
	{
		int num = global::OcclusionCulling.FindFreeSlot(occludees, states, recycled);
		if (num >= 0)
		{
			Vector4 sphereBounds;
			sphereBounds..ctor(center.x, center.y, center.z, radius);
			global::OccludeeState occludeeState = global::OcclusionCulling.Allocate().Initialize(states, set, num, sphereBounds, isVisible, minTimeVisible, isStatic, onVisibilityChanged);
			occludeeState.cell = global::OcclusionCulling.RegisterToGrid(occludeeState);
			occludees[num] = occludeeState;
			changed.Add(num);
			if (states.array[num].isVisible != occludeeState.cell.isVisible)
			{
				visibilityChanged.Add(num);
			}
		}
		return num;
	}

	// Token: 0x06002610 RID: 9744 RVA: 0x000D23A0 File Offset: 0x000D05A0
	public static void UnregisterOccludee(int id)
	{
		if (id >= 0 && id < 2097152)
		{
			bool flag = id < 1048576;
			int slot = (!flag) ? (id - 1048576) : id;
			if (flag)
			{
				global::OcclusionCulling.UnregisterOccludee(slot, global::OcclusionCulling.staticOccludees, global::OcclusionCulling.staticRecycled, global::OcclusionCulling.staticChanged);
			}
			else
			{
				global::OcclusionCulling.UnregisterOccludee(slot, global::OcclusionCulling.dynamicOccludees, global::OcclusionCulling.dynamicRecycled, global::OcclusionCulling.dynamicChanged);
			}
		}
	}

	// Token: 0x06002611 RID: 9745 RVA: 0x000D2414 File Offset: 0x000D0614
	private static void UnregisterOccludee(int slot, global::OcclusionCulling.SimpleList<global::OccludeeState> occludees, Queue<int> recycled, List<int> changed)
	{
		global::OccludeeState occludeeState = occludees[slot];
		global::OcclusionCulling.UnregisterFromGrid(occludeeState);
		recycled.Enqueue(slot);
		changed.Add(slot);
		global::OcclusionCulling.Release(occludeeState);
		occludees[slot] = null;
		occludeeState.Invalidate();
	}

	// Token: 0x06002612 RID: 9746 RVA: 0x000D2454 File Offset: 0x000D0654
	public static void UpdateDynamicOccludee(int id, Vector3 center, float radius)
	{
		int num = id - 1048576;
		if (num >= 0 && num < 1048576)
		{
			global::OcclusionCulling.dynamicStates.array[num].sphereBounds = new Vector4(center.x, center.y, center.z, radius);
			global::OcclusionCulling.dynamicChanged.Add(num);
		}
	}

	// Token: 0x06002613 RID: 9747 RVA: 0x000D24B8 File Offset: 0x000D06B8
	private void UpdateBuffers(global::OcclusionCulling.SimpleList<global::OccludeeState> occludees, global::OcclusionCulling.SimpleList<global::OccludeeState.State> states, global::OcclusionCulling.BufferSet set, List<int> changed, bool isStatic)
	{
		int count = occludees.Count;
		bool flag = changed.Count > 0;
		set.CheckResize(count, 2048);
		for (int i = 0; i < changed.Count; i++)
		{
			int num = changed[i];
			global::OccludeeState occludeeState = occludees[num];
			if (occludeeState != null)
			{
				if (!isStatic)
				{
					global::OcclusionCulling.UpdateInGrid(occludeeState);
				}
				set.inputData[num] = states[num].sphereBounds;
			}
			else
			{
				set.inputData[num] = Vector4.zero;
			}
		}
		changed.Clear();
		if (flag)
		{
			set.UploadData();
		}
	}

	// Token: 0x06002614 RID: 9748 RVA: 0x000D257C File Offset: 0x000D077C
	private void UpdateCameraMatrices(bool starting = false)
	{
		if (!starting)
		{
			this.prevViewProjMatrix = this.viewProjMatrix;
		}
		Matrix4x4 matrix4x = Matrix4x4.Perspective(this.camera.fieldOfView, this.camera.aspect, this.camera.nearClipPlane, this.camera.farClipPlane);
		this.viewMatrix = this.camera.worldToCameraMatrix;
		this.projMatrix = GL.GetGPUProjectionMatrix(matrix4x, false);
		this.viewProjMatrix = this.projMatrix * this.viewMatrix;
		this.invViewProjMatrix = Matrix4x4.Inverse(this.viewProjMatrix);
		if (starting)
		{
			this.prevViewProjMatrix = this.viewProjMatrix;
		}
	}

	// Token: 0x06002615 RID: 9749 RVA: 0x000D2628 File Offset: 0x000D0828
	private void OnPreCull()
	{
		this.UpdateCameraMatrices(false);
		this.GenerateHiZMipChain();
		this.PrepareAndDispatch();
		this.IssueRead();
		if (global::OcclusionCulling.grid.Size <= global::OcclusionCulling.gridSet.resultData.Length)
		{
			this.RetrieveAndApplyVisibility();
		}
		else
		{
			Debug.LogWarning(string.Concat(new object[]
			{
				"[OcclusionCulling] Grid size and result capacity are out of sync: ",
				global::OcclusionCulling.grid.Size,
				", ",
				global::OcclusionCulling.gridSet.resultData.Length
			}));
		}
	}

	// Token: 0x06002616 RID: 9750 RVA: 0x000D26BC File Offset: 0x000D08BC
	private void OnPostRender()
	{
		bool sRGBWrite = GL.sRGBWrite;
		RenderBuffer activeColorBuffer = Graphics.activeColorBuffer;
		RenderBuffer activeDepthBuffer = Graphics.activeDepthBuffer;
		this.GrabDepthTexture();
		Graphics.SetRenderTarget(activeColorBuffer, activeDepthBuffer);
		GL.sRGBWrite = sRGBWrite;
	}

	// Token: 0x06002617 RID: 9751 RVA: 0x000D26F0 File Offset: 0x000D08F0
	private float[] MatrixToFloatArray(Matrix4x4 m)
	{
		int i = 0;
		int num = 0;
		while (i < 4)
		{
			for (int j = 0; j < 4; j++)
			{
				this.matrixToFloatTemp[num++] = m[j, i];
			}
			i++;
		}
		return this.matrixToFloatTemp;
	}

	// Token: 0x06002618 RID: 9752 RVA: 0x000D2740 File Offset: 0x000D0940
	private void PrepareAndDispatch()
	{
		Vector2 vector;
		vector..ctor((float)this.hiZWidth, (float)this.hiZHeight);
		global::OcclusionCulling.ExtractFrustum(this.viewProjMatrix, ref this.frustumPlanes);
		if (this.usingFallback)
		{
			this.fallbackMat.SetTexture("_HiZMap", this.hiZTexture);
			this.fallbackMat.SetFloat("_HiZMaxLod", (float)(this.hiZLevelCount - 1));
			this.fallbackMat.SetMatrix("_ViewMatrix", this.viewMatrix);
			this.fallbackMat.SetMatrix("_ProjMatrix", this.projMatrix);
			this.fallbackMat.SetMatrix("_ViewProjMatrix", this.viewProjMatrix);
			this.fallbackMat.SetVector("_CameraWorldPos", base.transform.position);
			this.fallbackMat.SetVector("_ViewportSize", vector);
			for (int i = 0; i < 6; i++)
			{
				this.fallbackMat.SetVector(this.frustumPropNames[i], this.frustumPlanes[i]);
			}
		}
		else
		{
			this.computeShader.SetTexture(0, "_HiZMap", this.hiZTexture);
			this.computeShader.SetFloat("_HiZMaxLod", (float)(this.hiZLevelCount - 1));
			this.computeShader.SetFloats("_ViewMatrix", this.MatrixToFloatArray(this.viewMatrix));
			this.computeShader.SetFloats("_ProjMatrix", this.MatrixToFloatArray(this.projMatrix));
			this.computeShader.SetFloats("_ViewProjMatrix", this.MatrixToFloatArray(this.viewProjMatrix));
			this.computeShader.SetVector("_CameraWorldPos", base.transform.position);
			this.computeShader.SetVector("_ViewportSize", vector);
			for (int j = 0; j < 6; j++)
			{
				this.computeShader.SetVector(this.frustumPropNames[j], this.frustumPlanes[j]);
			}
		}
		if (global::OcclusionCulling.staticOccludees.Count > 0)
		{
			this.UpdateBuffers(global::OcclusionCulling.staticOccludees, global::OcclusionCulling.staticStates, global::OcclusionCulling.staticSet, global::OcclusionCulling.staticChanged, true);
			global::OcclusionCulling.staticSet.Dispatch(global::OcclusionCulling.staticOccludees.Count);
		}
		if (global::OcclusionCulling.dynamicOccludees.Count > 0)
		{
			this.UpdateBuffers(global::OcclusionCulling.dynamicOccludees, global::OcclusionCulling.dynamicStates, global::OcclusionCulling.dynamicSet, global::OcclusionCulling.dynamicChanged, false);
			global::OcclusionCulling.dynamicSet.Dispatch(global::OcclusionCulling.dynamicOccludees.Count);
		}
		this.UpdateGridBuffers();
		global::OcclusionCulling.gridSet.Dispatch(global::OcclusionCulling.grid.Size);
	}

	// Token: 0x06002619 RID: 9753 RVA: 0x000D29E8 File Offset: 0x000D0BE8
	private void IssueRead()
	{
		if (global::OcclusionCulling.staticOccludees.Count > 0)
		{
			global::OcclusionCulling.staticSet.IssueRead();
		}
		if (global::OcclusionCulling.dynamicOccludees.Count > 0)
		{
			global::OcclusionCulling.dynamicSet.IssueRead();
		}
		if (global::OcclusionCulling.grid.Count > 0)
		{
			global::OcclusionCulling.gridSet.IssueRead();
		}
		GL.IssuePluginEvent(Graphics.GetRenderEventFunc(), 2);
	}

	// Token: 0x0600261A RID: 9754 RVA: 0x000D2A50 File Offset: 0x000D0C50
	public void ResetTiming(global::OcclusionCulling.SmartList bucket)
	{
		for (int i = 0; i < bucket.Size; i++)
		{
			global::OccludeeState occludeeState = bucket[i];
			if (occludeeState != null)
			{
				occludeeState.states.array[occludeeState.slot].waitTime = 0f;
			}
		}
	}

	// Token: 0x0600261B RID: 9755 RVA: 0x000D2AA4 File Offset: 0x000D0CA4
	public void ResetTiming()
	{
		for (int i = 0; i < global::OcclusionCulling.grid.Size; i++)
		{
			global::OcclusionCulling.Cell cell = global::OcclusionCulling.grid[i];
			if (cell != null)
			{
				this.ResetTiming(cell.staticBucket);
				this.ResetTiming(cell.dynamicBucket);
			}
		}
	}

	// Token: 0x0600261C RID: 9756 RVA: 0x000D2AF8 File Offset: 0x000D0CF8
	private static bool FrustumCull(Vector4[] planes, Vector4 testSphere)
	{
		for (int i = 0; i < 6; i++)
		{
			float num = planes[i].x * testSphere.x + planes[i].y * testSphere.y + planes[i].z * testSphere.z + planes[i].w;
			if (num < -testSphere.w)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x0600261D RID: 9757 RVA: 0x000D2B74 File Offset: 0x000D0D74
	private static int ProcessOccludees_Safe(global::OcclusionCulling.SimpleList<global::OccludeeState.State> states, global::OcclusionCulling.SmartList bucket, Color32[] results, global::OcclusionCulling.SimpleList<int> changed, Vector4[] frustumPlanes, float time, uint frame)
	{
		int num = 0;
		for (int i = 0; i < bucket.Size; i++)
		{
			global::OccludeeState occludeeState = bucket[i];
			if (occludeeState != null && occludeeState.slot < results.Length)
			{
				int slot = occludeeState.slot;
				global::OccludeeState.State value = states[slot];
				bool flag = global::OcclusionCulling.FrustumCull(frustumPlanes, value.sphereBounds);
				bool flag2 = results[slot].r > 0 && flag;
				if (flag2 || frame < value.waitFrame)
				{
					value.waitTime = time + value.minTimeVisible;
				}
				if (!flag2)
				{
					flag2 = (time < value.waitTime);
				}
				if (flag2 != value.isVisible)
				{
					if (value.callback)
					{
						changed.Add(slot);
					}
					else
					{
						value.isVisible = flag2;
					}
				}
				states[slot] = value;
				num += ((!value.isVisible) ? 1 : 0);
			}
		}
		return num;
	}

	// Token: 0x0600261E RID: 9758 RVA: 0x000D2C78 File Offset: 0x000D0E78
	private static int ProcessOccludees_Fast(global::OccludeeState.State[] states, int[] bucket, int bucketCount, Color32[] results, int resultCount, int[] changed, ref int changedCount, Vector4[] frustumPlanes, float time, uint frame)
	{
		int num = 0;
		for (int i = 0; i < bucketCount; i++)
		{
			int num2 = bucket[i];
			if (num2 >= 0 && num2 < resultCount && states[num2].active)
			{
				global::OccludeeState.State state = states[num2];
				bool flag = global::OcclusionCulling.FrustumCull(frustumPlanes, state.sphereBounds);
				bool flag2 = results[num2].r > 0 && flag;
				if (flag2 || frame < state.waitFrame)
				{
					state.waitTime = time + state.minTimeVisible;
				}
				if (!flag2)
				{
					flag2 = (time < state.waitTime);
				}
				if (flag2 != state.isVisible)
				{
					if (state.callback)
					{
						changed[changedCount++] = num2;
					}
					else
					{
						state.isVisible = flag2;
					}
				}
				states[num2] = state;
				num += ((!flag2) ? 1 : 0);
			}
		}
		return num;
	}

	// Token: 0x0600261F RID: 9759
	[DllImport("Renderer", EntryPoint = "CULL_ProcessOccludees")]
	private static extern int ProcessOccludees_Native(ref global::OccludeeState.State states, ref int bucket, int bucketCount, ref Color32 results, int resultCount, ref int changed, ref int changedCount, ref Vector4 frustumPlanes, float time, uint frame);

	// Token: 0x06002620 RID: 9760 RVA: 0x000D2D88 File Offset: 0x000D0F88
	private void ApplyVisibility_Safe(float time, uint frame)
	{
		bool ready = global::OcclusionCulling.staticSet.Ready;
		bool ready2 = global::OcclusionCulling.dynamicSet.Ready;
		for (int i = 0; i < global::OcclusionCulling.grid.Size; i++)
		{
			global::OcclusionCulling.Cell cell = global::OcclusionCulling.grid[i];
			if (cell != null && global::OcclusionCulling.gridSet.resultData.Length > 0)
			{
				bool flag = global::OcclusionCulling.FrustumCull(this.frustumPlanes, cell.sphereBounds);
				bool flag2 = global::OcclusionCulling.gridSet.resultData[i].r > 0 && flag;
				if (cell.isVisible || flag2)
				{
					int num = 0;
					int num2 = 0;
					if (ready && cell.staticBucket.Count > 0)
					{
						num = global::OcclusionCulling.ProcessOccludees_Safe(global::OcclusionCulling.staticStates, cell.staticBucket, global::OcclusionCulling.staticSet.resultData, global::OcclusionCulling.staticVisibilityChanged, this.frustumPlanes, time, frame);
					}
					if (ready2 && cell.dynamicBucket.Count > 0)
					{
						num2 = global::OcclusionCulling.ProcessOccludees_Safe(global::OcclusionCulling.dynamicStates, cell.dynamicBucket, global::OcclusionCulling.dynamicSet.resultData, global::OcclusionCulling.dynamicVisibilityChanged, this.frustumPlanes, time, frame);
					}
					cell.isVisible = (flag2 || num < cell.staticBucket.Count || num2 < cell.dynamicBucket.Count);
				}
			}
		}
	}

	// Token: 0x06002621 RID: 9761 RVA: 0x000D2EE8 File Offset: 0x000D10E8
	private void ApplyVisibility_Fast(float time, uint frame)
	{
		bool ready = global::OcclusionCulling.staticSet.Ready;
		bool ready2 = global::OcclusionCulling.dynamicSet.Ready;
		for (int i = 0; i < global::OcclusionCulling.grid.Size; i++)
		{
			global::OcclusionCulling.Cell cell = global::OcclusionCulling.grid[i];
			if (cell != null && global::OcclusionCulling.gridSet.resultData.Length > 0)
			{
				bool flag = global::OcclusionCulling.FrustumCull(this.frustumPlanes, cell.sphereBounds);
				bool flag2 = global::OcclusionCulling.gridSet.resultData[i].r > 0 && flag;
				if (cell.isVisible || flag2)
				{
					int num = 0;
					int num2 = 0;
					if (ready && cell.staticBucket.Count > 0)
					{
						num = global::OcclusionCulling.ProcessOccludees_Fast(global::OcclusionCulling.staticStates.array, cell.staticBucket.Slots, cell.staticBucket.Size, global::OcclusionCulling.staticSet.resultData, global::OcclusionCulling.staticSet.resultData.Length, global::OcclusionCulling.staticVisibilityChanged.array, ref global::OcclusionCulling.staticVisibilityChanged.count, this.frustumPlanes, time, frame);
					}
					if (ready2 && cell.dynamicBucket.Count > 0)
					{
						num2 = global::OcclusionCulling.ProcessOccludees_Fast(global::OcclusionCulling.dynamicStates.array, cell.dynamicBucket.Slots, cell.dynamicBucket.Size, global::OcclusionCulling.dynamicSet.resultData, global::OcclusionCulling.dynamicSet.resultData.Length, global::OcclusionCulling.dynamicVisibilityChanged.array, ref global::OcclusionCulling.dynamicVisibilityChanged.count, this.frustumPlanes, time, frame);
					}
					cell.isVisible = (flag2 || num < cell.staticBucket.Count || num2 < cell.dynamicBucket.Count);
				}
			}
		}
	}

	// Token: 0x06002622 RID: 9762 RVA: 0x000D30A8 File Offset: 0x000D12A8
	private void ApplyVisibility_Native(float time, uint frame)
	{
		bool ready = global::OcclusionCulling.staticSet.Ready;
		bool ready2 = global::OcclusionCulling.dynamicSet.Ready;
		for (int i = 0; i < global::OcclusionCulling.grid.Size; i++)
		{
			global::OcclusionCulling.Cell cell = global::OcclusionCulling.grid[i];
			if (cell != null && global::OcclusionCulling.gridSet.resultData.Length > 0)
			{
				bool flag = global::OcclusionCulling.FrustumCull(this.frustumPlanes, cell.sphereBounds);
				bool flag2 = global::OcclusionCulling.gridSet.resultData[i].r > 0 && flag;
				if (cell.isVisible || flag2)
				{
					int num = 0;
					int num2 = 0;
					if (ready && cell.staticBucket.Count > 0)
					{
						num = global::OcclusionCulling.ProcessOccludees_Native(ref global::OcclusionCulling.staticStates.array[0], ref cell.staticBucket.Slots[0], cell.staticBucket.Size, ref global::OcclusionCulling.staticSet.resultData[0], global::OcclusionCulling.staticSet.resultData.Length, ref global::OcclusionCulling.staticVisibilityChanged.array[0], ref global::OcclusionCulling.staticVisibilityChanged.count, ref this.frustumPlanes[0], time, frame);
					}
					if (ready2 && cell.dynamicBucket.Count > 0)
					{
						num2 = global::OcclusionCulling.ProcessOccludees_Native(ref global::OcclusionCulling.dynamicStates.array[0], ref cell.dynamicBucket.Slots[0], cell.dynamicBucket.Size, ref global::OcclusionCulling.dynamicSet.resultData[0], global::OcclusionCulling.dynamicSet.resultData.Length, ref global::OcclusionCulling.dynamicVisibilityChanged.array[0], ref global::OcclusionCulling.dynamicVisibilityChanged.count, ref this.frustumPlanes[0], time, frame);
					}
					cell.isVisible = (flag2 || num < cell.staticBucket.Count || num2 < cell.dynamicBucket.Count);
				}
			}
		}
	}

	// Token: 0x06002623 RID: 9763 RVA: 0x000D32A4 File Offset: 0x000D14A4
	private void ProcessCallbacks(global::OcclusionCulling.SimpleList<global::OccludeeState> occludees, global::OcclusionCulling.SimpleList<global::OccludeeState.State> states, global::OcclusionCulling.SimpleList<int> changed)
	{
		for (int i = 0; i < changed.Count; i++)
		{
			int num = changed[i];
			global::OccludeeState occludeeState = occludees[num];
			if (occludeeState != null)
			{
				bool flag = !states.array[num].isVisible;
				global::OcclusionCulling.OnVisibilityChanged onVisibilityChanged = occludeeState.onVisibilityChanged;
				if (onVisibilityChanged != null && (Object)onVisibilityChanged.Target != null)
				{
					onVisibilityChanged(flag);
				}
				if (occludeeState.slot >= 0)
				{
					states.array[occludeeState.slot].isVisible = flag;
				}
			}
		}
		changed.Clear();
	}

	// Token: 0x06002624 RID: 9764 RVA: 0x000D334C File Offset: 0x000D154C
	public void RetrieveAndApplyVisibility()
	{
		if (global::OcclusionCulling.staticOccludees.Count > 0)
		{
			global::OcclusionCulling.staticSet.GetResults();
		}
		if (global::OcclusionCulling.dynamicOccludees.Count > 0)
		{
			global::OcclusionCulling.dynamicSet.GetResults();
		}
		if (global::OcclusionCulling.grid.Count > 0)
		{
			global::OcclusionCulling.gridSet.GetResults();
		}
		if (this.debugSettings.showAllVisible)
		{
			for (int i = 0; i < global::OcclusionCulling.staticSet.resultData.Length; i++)
			{
				global::OcclusionCulling.staticSet.resultData[i].r = 1;
			}
			for (int j = 0; j < global::OcclusionCulling.dynamicSet.resultData.Length; j++)
			{
				global::OcclusionCulling.dynamicSet.resultData[j].r = 1;
			}
			for (int k = 0; k < global::OcclusionCulling.gridSet.resultData.Length; k++)
			{
				global::OcclusionCulling.gridSet.resultData[k].r = 1;
			}
		}
		global::OcclusionCulling.staticVisibilityChanged.EnsureCapacity(global::OcclusionCulling.staticOccludees.Count);
		global::OcclusionCulling.dynamicVisibilityChanged.EnsureCapacity(global::OcclusionCulling.dynamicOccludees.Count);
		float time = Time.time;
		uint frameCount = (uint)Time.frameCount;
		if (this.useNativePath)
		{
			this.ApplyVisibility_Native(time, frameCount);
		}
		else
		{
			this.ApplyVisibility_Fast(time, frameCount);
		}
		this.ProcessCallbacks(global::OcclusionCulling.staticOccludees, global::OcclusionCulling.staticStates, global::OcclusionCulling.staticVisibilityChanged);
		this.ProcessCallbacks(global::OcclusionCulling.dynamicOccludees, global::OcclusionCulling.dynamicStates, global::OcclusionCulling.dynamicVisibilityChanged);
	}

	// Token: 0x06002625 RID: 9765 RVA: 0x000D34D4 File Offset: 0x000D16D4
	public static bool DebugFilterIsDynamic(int filter)
	{
		return filter == 1 || filter == 4;
	}

	// Token: 0x06002626 RID: 9766 RVA: 0x000D34E4 File Offset: 0x000D16E4
	public static bool DebugFilterIsStatic(int filter)
	{
		return filter == 2 || filter == 4;
	}

	// Token: 0x06002627 RID: 9767 RVA: 0x000D34F4 File Offset: 0x000D16F4
	public static bool DebugFilterIsGrid(int filter)
	{
		return filter == 3 || filter == 4;
	}

	// Token: 0x06002628 RID: 9768 RVA: 0x000D3504 File Offset: 0x000D1704
	private void DebugInitialize()
	{
		this.debugMipMat = new Material(Shader.Find("Hidden/OcclusionCulling/DebugMip"))
		{
			hideFlags = 61
		};
	}

	// Token: 0x06002629 RID: 9769 RVA: 0x000D3530 File Offset: 0x000D1730
	private void DebugShutdown()
	{
		if (this.debugMipMat != null)
		{
			Object.DestroyImmediate(this.debugMipMat);
			this.debugMipMat = null;
		}
	}

	// Token: 0x0600262A RID: 9770 RVA: 0x000D3558 File Offset: 0x000D1758
	private void DebugUpdate()
	{
		if (this.HiZReady)
		{
			this.debugSettings.showMainLod = Mathf.Clamp(this.debugSettings.showMainLod, 0, this.hiZLevels.Length - 1);
		}
	}

	// Token: 0x0600262B RID: 9771 RVA: 0x000D358C File Offset: 0x000D178C
	private void DebugDraw()
	{
	}

	// Token: 0x0600262C RID: 9772 RVA: 0x000D3590 File Offset: 0x000D1790
	public static void NormalizePlane(ref Vector4 plane)
	{
		float num = Mathf.Sqrt(plane.x * plane.x + plane.y * plane.y + plane.z * plane.z);
		plane.x /= num;
		plane.y /= num;
		plane.z /= num;
		plane.w /= num;
	}

	// Token: 0x0600262D RID: 9773 RVA: 0x000D3604 File Offset: 0x000D1804
	public static void ExtractFrustum(Matrix4x4 viewProjMatrix, ref Vector4[] planes)
	{
		planes[0].x = viewProjMatrix.m30 + viewProjMatrix.m00;
		planes[0].y = viewProjMatrix.m31 + viewProjMatrix.m01;
		planes[0].z = viewProjMatrix.m32 + viewProjMatrix.m02;
		planes[0].w = viewProjMatrix.m33 + viewProjMatrix.m03;
		global::OcclusionCulling.NormalizePlane(ref planes[0]);
		planes[1].x = viewProjMatrix.m30 - viewProjMatrix.m00;
		planes[1].y = viewProjMatrix.m31 - viewProjMatrix.m01;
		planes[1].z = viewProjMatrix.m32 - viewProjMatrix.m02;
		planes[1].w = viewProjMatrix.m33 - viewProjMatrix.m03;
		global::OcclusionCulling.NormalizePlane(ref planes[1]);
		planes[2].x = viewProjMatrix.m30 - viewProjMatrix.m10;
		planes[2].y = viewProjMatrix.m31 - viewProjMatrix.m11;
		planes[2].z = viewProjMatrix.m32 - viewProjMatrix.m12;
		planes[2].w = viewProjMatrix.m33 - viewProjMatrix.m13;
		global::OcclusionCulling.NormalizePlane(ref planes[2]);
		planes[3].x = viewProjMatrix.m30 + viewProjMatrix.m10;
		planes[3].y = viewProjMatrix.m31 + viewProjMatrix.m11;
		planes[3].z = viewProjMatrix.m32 + viewProjMatrix.m12;
		planes[3].w = viewProjMatrix.m33 + viewProjMatrix.m13;
		global::OcclusionCulling.NormalizePlane(ref planes[3]);
		planes[4].x = viewProjMatrix.m20;
		planes[4].y = viewProjMatrix.m21;
		planes[4].z = viewProjMatrix.m22;
		planes[4].w = viewProjMatrix.m23;
		global::OcclusionCulling.NormalizePlane(ref planes[4]);
		planes[5].x = viewProjMatrix.m30 - viewProjMatrix.m20;
		planes[5].y = viewProjMatrix.m31 - viewProjMatrix.m21;
		planes[5].z = viewProjMatrix.m32 - viewProjMatrix.m22;
		planes[5].w = viewProjMatrix.m33 - viewProjMatrix.m23;
		global::OcclusionCulling.NormalizePlane(ref planes[5]);
	}

	// Token: 0x170002C7 RID: 711
	// (get) Token: 0x0600262E RID: 9774 RVA: 0x000D38E0 File Offset: 0x000D1AE0
	public bool HiZReady
	{
		get
		{
			return this.hiZTexture != null && this.hiZWidth > 0 && this.hiZHeight > 0;
		}
	}

	// Token: 0x0600262F RID: 9775 RVA: 0x000D390C File Offset: 0x000D1B0C
	public void CheckResizeHiZMap()
	{
		int pixelWidth = this.camera.pixelWidth;
		int pixelHeight = this.camera.pixelHeight;
		if (pixelWidth > 0 && pixelHeight > 0)
		{
			int num = pixelWidth / 4;
			int num2 = pixelHeight / 4;
			if (this.hiZLevels == null || this.hiZWidth != num || this.hiZHeight != num2)
			{
				this.InitializeHiZMap(num, num2);
				this.hiZWidth = num;
				this.hiZHeight = num2;
				if (this.debugSettings.log)
				{
					Debug.Log(string.Concat(new object[]
					{
						"[OcclusionCulling] Resized HiZ Map to ",
						this.hiZWidth,
						" x ",
						this.hiZHeight
					}));
				}
			}
		}
	}

	// Token: 0x06002630 RID: 9776 RVA: 0x000D39CC File Offset: 0x000D1BCC
	private void InitializeHiZMap()
	{
		Shader shader = Shader.Find("Hidden/OcclusionCulling/DepthDownscale");
		Shader shader2 = Shader.Find("Hidden/OcclusionCulling/BlitCopy");
		this.downscaleMat = new Material(shader)
		{
			hideFlags = 61
		};
		this.blitCopyMat = new Material(shader2)
		{
			hideFlags = 61
		};
		this.CheckResizeHiZMap();
	}

	// Token: 0x06002631 RID: 9777 RVA: 0x000D3A24 File Offset: 0x000D1C24
	private void FinalizeHiZMap()
	{
		this.DestroyHiZMap();
		if (this.downscaleMat != null)
		{
			Object.DestroyImmediate(this.downscaleMat);
			this.downscaleMat = null;
		}
		if (this.blitCopyMat != null)
		{
			Object.DestroyImmediate(this.blitCopyMat);
			this.blitCopyMat = null;
		}
	}

	// Token: 0x06002632 RID: 9778 RVA: 0x000D3A80 File Offset: 0x000D1C80
	private void InitializeHiZMap(int width, int height)
	{
		this.DestroyHiZMap();
		int num = Mathf.Min(width, height);
		this.hiZLevelCount = (int)(Mathf.Log((float)num, 2f) + 1f);
		this.hiZLevels = new RenderTexture[this.hiZLevelCount];
		this.depthTexture = this.CreateDepthTexture("DepthTex", width, height, false);
		this.hiZTexture = this.CreateDepthTexture("HiZMapTex", width, height, true);
		for (int i = 0; i < this.hiZLevelCount; i++)
		{
			this.hiZLevels[i] = this.CreateDepthTextureMip("HiZMap" + i, width, height, i);
		}
	}

	// Token: 0x06002633 RID: 9779 RVA: 0x000D3B28 File Offset: 0x000D1D28
	private void DestroyHiZMap()
	{
		if (this.depthTexture != null)
		{
			RenderTexture.active = null;
			Object.DestroyImmediate(this.depthTexture);
			this.depthTexture = null;
		}
		if (this.hiZTexture != null)
		{
			RenderTexture.active = null;
			Object.DestroyImmediate(this.hiZTexture);
			this.hiZTexture = null;
		}
		if (this.hiZLevels != null)
		{
			for (int i = 0; i < this.hiZLevels.Length; i++)
			{
				Object.DestroyImmediate(this.hiZLevels[i]);
			}
			this.hiZLevels = null;
		}
	}

	// Token: 0x06002634 RID: 9780 RVA: 0x000D3BC0 File Offset: 0x000D1DC0
	private RenderTexture CreateDepthTexture(string name, int width, int height, bool mips = false)
	{
		RenderTexture renderTexture = new RenderTexture(width, height, 0, 14, 1);
		renderTexture.name = name;
		renderTexture.useMipMap = mips;
		renderTexture.autoGenerateMips = false;
		renderTexture.wrapMode = 1;
		renderTexture.filterMode = 0;
		renderTexture.Create();
		return renderTexture;
	}

	// Token: 0x06002635 RID: 9781 RVA: 0x000D3C08 File Offset: 0x000D1E08
	private RenderTexture CreateDepthTextureMip(string name, int width, int height, int mip)
	{
		int num = width >> mip;
		int num2 = height >> mip;
		int num3 = (mip != 0) ? 0 : 24;
		RenderTexture renderTexture = new RenderTexture(num, num2, num3, 14, 1);
		renderTexture.name = name;
		renderTexture.useMipMap = false;
		renderTexture.wrapMode = 1;
		renderTexture.filterMode = 0;
		renderTexture.Create();
		return renderTexture;
	}

	// Token: 0x06002636 RID: 9782 RVA: 0x000D3C68 File Offset: 0x000D1E68
	public void GrabDepthTexture()
	{
		if (this.depthTexture != null)
		{
			Graphics.Blit(null, this.depthTexture, this.depthCopyMat, 0);
		}
	}

	// Token: 0x06002637 RID: 9783 RVA: 0x000D3C90 File Offset: 0x000D1E90
	public void GenerateHiZMipChain()
	{
		if (this.HiZReady)
		{
			this.depthCopyMat.SetMatrix("_CameraReprojection", this.prevViewProjMatrix * this.invViewProjMatrix);
			Graphics.Blit(this.depthTexture, this.hiZLevels[0], this.depthCopyMat, 1);
			for (int i = 1; i < this.hiZLevels.Length; i++)
			{
				RenderTexture renderTexture = this.hiZLevels[i - 1];
				RenderTexture renderTexture2 = this.hiZLevels[i];
				int num = ((renderTexture.width & 1) != 0 || (renderTexture.height & 1) != 0) ? 1 : 0;
				this.downscaleMat.SetTexture("_MainTex", renderTexture);
				Graphics.Blit(renderTexture, renderTexture2, this.downscaleMat, num);
			}
			for (int j = 0; j < this.hiZLevels.Length; j++)
			{
				Graphics.SetRenderTarget(this.hiZTexture, j);
				Graphics.Blit(this.hiZLevels[j], this.blitCopyMat);
			}
		}
	}

	// Token: 0x06002638 RID: 9784 RVA: 0x000D3D90 File Offset: 0x000D1F90
	private void DebugDrawGizmos()
	{
		Camera component = base.GetComponent<Camera>();
		Gizmos.color = new Color(0.75f, 0.75f, 0f, 0.5f);
		Gizmos.matrix = Matrix4x4.TRS(base.transform.position, base.transform.rotation, Vector3.one);
		Gizmos.DrawFrustum(Vector3.zero, component.fieldOfView, component.farClipPlane, component.nearClipPlane, component.aspect);
		Gizmos.color = Color.red;
		Gizmos.matrix = Matrix4x4.identity;
		Matrix4x4 worldToCameraMatrix = component.worldToCameraMatrix;
		Matrix4x4 gpuprojectionMatrix = GL.GetGPUProjectionMatrix(component.projectionMatrix, false);
		Matrix4x4 matrix4x = gpuprojectionMatrix * worldToCameraMatrix;
		Vector4[] array = new Vector4[6];
		global::OcclusionCulling.ExtractFrustum(matrix4x, ref array);
		for (int i = 0; i < array.Length; i++)
		{
			Vector3 vector;
			vector..ctor(array[i].x, array[i].y, array[i].z);
			float w = array[i].w;
			Vector3 vector2 = -vector * w;
			Gizmos.DrawLine(vector2, vector2 * 2f);
		}
	}

	// Token: 0x06002639 RID: 9785 RVA: 0x000D3EC8 File Offset: 0x000D20C8
	private static int floor(float x)
	{
		int num = (int)x;
		return (x >= (float)num) ? num : (num - 1);
	}

	// Token: 0x0600263A RID: 9786 RVA: 0x000D3EEC File Offset: 0x000D20EC
	public static global::OcclusionCulling.Cell RegisterToGrid(global::OccludeeState occludee)
	{
		int num = global::OcclusionCulling.floor(occludee.states.array[occludee.slot].sphereBounds.x * 0.01f);
		int num2 = global::OcclusionCulling.floor(occludee.states.array[occludee.slot].sphereBounds.y * 0.01f);
		int num3 = global::OcclusionCulling.floor(occludee.states.array[occludee.slot].sphereBounds.z * 0.01f);
		int num4 = Mathf.Clamp(num, -1048575, 1048575);
		int num5 = Mathf.Clamp(num2, -1048575, 1048575);
		int num6 = Mathf.Clamp(num3, -1048575, 1048575);
		ulong num7 = (ulong)((long)((num4 < 0) ? (num4 + 1048575) : num4));
		ulong num8 = (ulong)((long)((num5 < 0) ? (num5 + 1048575) : num5));
		ulong num9 = (ulong)((long)((num6 < 0) ? (num6 + 1048575) : num6));
		ulong key = num7 << 42 | num8 << 21 | num9;
		global::OcclusionCulling.Cell cell;
		bool flag = global::OcclusionCulling.grid.TryGetValue(key, out cell);
		if (!flag)
		{
			Vector3 vector = default(Vector3);
			vector.x = (float)num * 100f + 50f;
			vector.y = (float)num2 * 100f + 50f;
			vector.z = (float)num3 * 100f + 50f;
			Vector3 vector2;
			vector2..ctor(100f, 100f, 100f);
			cell = global::OcclusionCulling.grid.Add(key, 16).Initialize(num, num2, num3, new Bounds(vector, vector2));
		}
		global::OcclusionCulling.SmartList smartList = (!occludee.isStatic) ? cell.dynamicBucket : cell.staticBucket;
		if (!flag || !smartList.Contains(occludee))
		{
			occludee.cell = cell;
			smartList.Add(occludee, 16);
			global::OcclusionCulling.gridChanged.Enqueue(cell);
		}
		return cell;
	}

	// Token: 0x0600263B RID: 9787 RVA: 0x000D40F8 File Offset: 0x000D22F8
	public static void UpdateInGrid(global::OccludeeState occludee)
	{
		int num = global::OcclusionCulling.floor(occludee.states.array[occludee.slot].sphereBounds.x * 0.01f);
		int num2 = global::OcclusionCulling.floor(occludee.states.array[occludee.slot].sphereBounds.y * 0.01f);
		int num3 = global::OcclusionCulling.floor(occludee.states.array[occludee.slot].sphereBounds.z * 0.01f);
		if (num != occludee.cell.x || num2 != occludee.cell.y || num3 != occludee.cell.z)
		{
			global::OcclusionCulling.UnregisterFromGrid(occludee);
			global::OcclusionCulling.RegisterToGrid(occludee);
		}
	}

	// Token: 0x0600263C RID: 9788 RVA: 0x000D41CC File Offset: 0x000D23CC
	public static void UnregisterFromGrid(global::OccludeeState occludee)
	{
		global::OcclusionCulling.Cell cell = occludee.cell;
		global::OcclusionCulling.SmartList smartList = (!occludee.isStatic) ? cell.dynamicBucket : cell.staticBucket;
		global::OcclusionCulling.gridChanged.Enqueue(cell);
		smartList.Remove(occludee);
		if (cell.staticBucket.Count == 0 && cell.dynamicBucket.Count == 0)
		{
			global::OcclusionCulling.grid.Remove(cell);
			cell.Reset();
		}
		occludee.cell = null;
	}

	// Token: 0x0600263D RID: 9789 RVA: 0x000D4248 File Offset: 0x000D2448
	public void UpdateGridBuffers()
	{
		if (global::OcclusionCulling.gridSet.CheckResize(global::OcclusionCulling.grid.Size, 256))
		{
			if (this.debugSettings.log)
			{
				Debug.Log("[OcclusionCulling] Resized grid to " + global::OcclusionCulling.grid.Size);
			}
			for (int i = 0; i < global::OcclusionCulling.grid.Size; i++)
			{
				if (global::OcclusionCulling.grid[i] != null)
				{
					global::OcclusionCulling.gridChanged.Enqueue(global::OcclusionCulling.grid[i]);
				}
			}
		}
		bool flag = global::OcclusionCulling.gridChanged.Count > 0;
		while (global::OcclusionCulling.gridChanged.Count > 0)
		{
			global::OcclusionCulling.Cell cell = global::OcclusionCulling.gridChanged.Dequeue();
			global::OcclusionCulling.gridSet.inputData[cell.hashedPoolIndex] = cell.sphereBounds;
		}
		if (flag)
		{
			global::OcclusionCulling.gridSet.UploadData();
		}
	}

	// Token: 0x0400223F RID: 8767
	public ComputeShader computeShader;

	// Token: 0x04002240 RID: 8768
	private Camera camera;

	// Token: 0x04002241 RID: 8769
	private const int ComputeThreadsPerGroup = 64;

	// Token: 0x04002242 RID: 8770
	private const int InputBufferStride = 16;

	// Token: 0x04002243 RID: 8771
	private const int ResultBufferStride = 4;

	// Token: 0x04002244 RID: 8772
	private const int OccludeeMaxSlotsPerPool = 1048576;

	// Token: 0x04002245 RID: 8773
	private const int OccludeePoolGranularity = 2048;

	// Token: 0x04002246 RID: 8774
	private const int StateBufferGranularity = 2048;

	// Token: 0x04002247 RID: 8775
	private const int GridBufferGranularity = 256;

	// Token: 0x04002248 RID: 8776
	private static Queue<global::OccludeeState> statePool = new Queue<global::OccludeeState>();

	// Token: 0x04002249 RID: 8777
	private static global::OcclusionCulling.SimpleList<global::OccludeeState> staticOccludees = new global::OcclusionCulling.SimpleList<global::OccludeeState>(2048);

	// Token: 0x0400224A RID: 8778
	private static global::OcclusionCulling.SimpleList<global::OccludeeState.State> staticStates = new global::OcclusionCulling.SimpleList<global::OccludeeState.State>(2048);

	// Token: 0x0400224B RID: 8779
	private static global::OcclusionCulling.SimpleList<int> staticVisibilityChanged = new global::OcclusionCulling.SimpleList<int>(1024);

	// Token: 0x0400224C RID: 8780
	private static global::OcclusionCulling.SimpleList<global::OccludeeState> dynamicOccludees = new global::OcclusionCulling.SimpleList<global::OccludeeState>(2048);

	// Token: 0x0400224D RID: 8781
	private static global::OcclusionCulling.SimpleList<global::OccludeeState.State> dynamicStates = new global::OcclusionCulling.SimpleList<global::OccludeeState.State>(2048);

	// Token: 0x0400224E RID: 8782
	private static global::OcclusionCulling.SimpleList<int> dynamicVisibilityChanged = new global::OcclusionCulling.SimpleList<int>(1024);

	// Token: 0x0400224F RID: 8783
	private static List<int> staticChanged = new List<int>(256);

	// Token: 0x04002250 RID: 8784
	private static Queue<int> staticRecycled = new Queue<int>();

	// Token: 0x04002251 RID: 8785
	private static List<int> dynamicChanged = new List<int>(1024);

	// Token: 0x04002252 RID: 8786
	private static Queue<int> dynamicRecycled = new Queue<int>();

	// Token: 0x04002253 RID: 8787
	private static global::OcclusionCulling.BufferSet staticSet = new global::OcclusionCulling.BufferSet();

	// Token: 0x04002254 RID: 8788
	private static global::OcclusionCulling.BufferSet dynamicSet = new global::OcclusionCulling.BufferSet();

	// Token: 0x04002255 RID: 8789
	private static global::OcclusionCulling.BufferSet gridSet = new global::OcclusionCulling.BufferSet();

	// Token: 0x04002256 RID: 8790
	private Vector4[] frustumPlanes = new Vector4[6];

	// Token: 0x04002257 RID: 8791
	private string[] frustumPropNames = new string[6];

	// Token: 0x04002258 RID: 8792
	private float[] matrixToFloatTemp = new float[16];

	// Token: 0x04002259 RID: 8793
	private bool usingFallback = true;

	// Token: 0x0400225A RID: 8794
	private Material fallbackMat;

	// Token: 0x0400225B RID: 8795
	private Material depthCopyMat;

	// Token: 0x0400225C RID: 8796
	private Matrix4x4 viewMatrix;

	// Token: 0x0400225D RID: 8797
	private Matrix4x4 projMatrix;

	// Token: 0x0400225E RID: 8798
	private Matrix4x4 viewProjMatrix;

	// Token: 0x0400225F RID: 8799
	private Matrix4x4 prevViewProjMatrix;

	// Token: 0x04002260 RID: 8800
	private Matrix4x4 invViewProjMatrix;

	// Token: 0x04002261 RID: 8801
	private bool useNativePath = true;

	// Token: 0x04002262 RID: 8802
	private static global::OcclusionCulling instance;

	// Token: 0x04002263 RID: 8803
	private static GraphicsDeviceType[] supportedDeviceTypes = new GraphicsDeviceType[]
	{
		2,
		17
	};

	// Token: 0x04002264 RID: 8804
	private static bool _enabled = false;

	// Token: 0x04002265 RID: 8805
	private static bool _safeMode = false;

	// Token: 0x04002266 RID: 8806
	private static global::OcclusionCulling.DebugFilter _debugShow = global::OcclusionCulling.DebugFilter.Off;

	// Token: 0x04002267 RID: 8807
	public global::OcclusionCulling.DebugSettings debugSettings = new global::OcclusionCulling.DebugSettings();

	// Token: 0x04002268 RID: 8808
	private Material debugMipMat;

	// Token: 0x04002269 RID: 8809
	private const float debugDrawDuration = 0.0334f;

	// Token: 0x0400226A RID: 8810
	private Material downscaleMat;

	// Token: 0x0400226B RID: 8811
	private Material blitCopyMat;

	// Token: 0x0400226C RID: 8812
	private int hiZLevelCount;

	// Token: 0x0400226D RID: 8813
	private int hiZWidth;

	// Token: 0x0400226E RID: 8814
	private int hiZHeight;

	// Token: 0x0400226F RID: 8815
	private RenderTexture depthTexture;

	// Token: 0x04002270 RID: 8816
	private RenderTexture hiZTexture;

	// Token: 0x04002271 RID: 8817
	private RenderTexture[] hiZLevels;

	// Token: 0x04002272 RID: 8818
	private const int GridCellsPerAxis = 2097152;

	// Token: 0x04002273 RID: 8819
	private const int GridHalfCellsPerAxis = 1048576;

	// Token: 0x04002274 RID: 8820
	private const int GridMinHalfCellsPerAxis = -1048575;

	// Token: 0x04002275 RID: 8821
	private const int GridMaxHalfCellsPerAxis = 1048575;

	// Token: 0x04002276 RID: 8822
	private const float GridCellSize = 100f;

	// Token: 0x04002277 RID: 8823
	private const float GridHalfCellSize = 50f;

	// Token: 0x04002278 RID: 8824
	private const float GridRcpCellSize = 0.01f;

	// Token: 0x04002279 RID: 8825
	private const int GridPoolCapacity = 16384;

	// Token: 0x0400227A RID: 8826
	private const int GridPoolGranularity = 4096;

	// Token: 0x0400227B RID: 8827
	private static global::OcclusionCulling.HashedPool<global::OcclusionCulling.Cell> grid = new global::OcclusionCulling.HashedPool<global::OcclusionCulling.Cell>(16384, 4096);

	// Token: 0x0400227C RID: 8828
	private static Queue<global::OcclusionCulling.Cell> gridChanged = new Queue<global::OcclusionCulling.Cell>();

	// Token: 0x02000808 RID: 2056
	public class BufferSet
	{
		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06002640 RID: 9792 RVA: 0x000D4480 File Offset: 0x000D2680
		public bool Ready
		{
			get
			{
				return this.resultData.Length > 0;
			}
		}

		// Token: 0x06002641 RID: 9793 RVA: 0x000D4490 File Offset: 0x000D2690
		public void Attach(Material fallbackMat)
		{
			this.fallbackMat = fallbackMat;
			this.usingFallback = true;
		}

		// Token: 0x06002642 RID: 9794 RVA: 0x000D44A0 File Offset: 0x000D26A0
		public void Attach(ComputeShader computeShader)
		{
			this.compute = computeShader;
			this.usingFallback = false;
		}

		// Token: 0x06002643 RID: 9795 RVA: 0x000D44B0 File Offset: 0x000D26B0
		public void Dispose(bool data = true)
		{
			if (this.inputBuffer != null)
			{
				this.inputBuffer.Dispose();
				this.inputBuffer = null;
			}
			if (this.resultBuffer != null)
			{
				this.resultBuffer.Dispose();
				this.resultBuffer = null;
			}
			if (this.inputTexture != null)
			{
				Object.DestroyImmediate(this.inputTexture);
				this.inputTexture = null;
			}
			if (this.resultTexture != null)
			{
				RenderTexture.active = null;
				Object.DestroyImmediate(this.resultTexture);
				this.resultTexture = null;
			}
			if (this.resultReadTexture != null)
			{
				Object.DestroyImmediate(this.resultReadTexture);
				this.resultReadTexture = null;
			}
			if (this.readbackInst != IntPtr.Zero)
			{
				Graphics.BufferReadback.Destroy(this.readbackInst);
				this.readbackInst = IntPtr.Zero;
			}
			if (data)
			{
				this.inputData = new Color[0];
				this.resultData = new Color32[0];
				this.capacity = 0;
				this.count = 0;
			}
		}

		// Token: 0x06002644 RID: 9796 RVA: 0x000D45C0 File Offset: 0x000D27C0
		public bool CheckResize(int count, int granularity)
		{
			if (count > this.capacity || (this.usingFallback && this.resultTexture != null && !this.resultTexture.IsCreated()))
			{
				this.Dispose(false);
				int num = count / granularity * granularity + granularity;
				if (this.usingFallback)
				{
					this.width = Mathf.CeilToInt(Mathf.Sqrt((float)num));
					this.height = Mathf.CeilToInt((float)num / (float)this.width);
					this.inputTexture = new Texture2D(this.width, this.height, 20, false, true);
					this.inputTexture.name = "_Input";
					this.inputTexture.filterMode = 0;
					this.inputTexture.wrapMode = 1;
					this.resultTexture = new RenderTexture(this.width, this.height, 0, 0, 1);
					this.resultTexture.name = "_Result";
					this.resultTexture.filterMode = 0;
					this.resultTexture.wrapMode = 1;
					this.resultTexture.useMipMap = false;
					this.resultTexture.Create();
					this.resultReadTexture = new Texture2D(this.width, this.height, 5, false, true);
					this.resultReadTexture.name = "_ResultRead";
					this.resultReadTexture.filterMode = 0;
					this.resultReadTexture.wrapMode = 1;
					this.readbackInst = Graphics.BufferReadback.CreateForTexture(this.resultTexture.GetNativeTexturePtr(), (uint)this.width, (uint)this.height, this.resultTexture.format);
					this.capacity = this.width * this.height;
				}
				else
				{
					this.inputBuffer = new ComputeBuffer(num, 16);
					this.resultBuffer = new ComputeBuffer(num, 4);
					uint num2 = (uint)(num * 4);
					this.readbackInst = Graphics.BufferReadback.CreateForBuffer(this.resultBuffer.GetNativeBufferPtr(), num2);
					this.capacity = num;
				}
				Array.Resize<Color>(ref this.inputData, this.capacity);
				Array.Resize<Color32>(ref this.resultData, this.capacity);
				this.count = count;
				return true;
			}
			return false;
		}

		// Token: 0x06002645 RID: 9797 RVA: 0x000D47D4 File Offset: 0x000D29D4
		public void UploadData()
		{
			if (this.usingFallback)
			{
				this.inputTexture.SetPixels(this.inputData);
				this.inputTexture.Apply();
			}
			else
			{
				this.inputBuffer.SetData(this.inputData);
			}
		}

		// Token: 0x06002646 RID: 9798 RVA: 0x000D4814 File Offset: 0x000D2A14
		private int AlignDispatchSize(int dispatchSize)
		{
			return (dispatchSize + 63) / 64;
		}

		// Token: 0x06002647 RID: 9799 RVA: 0x000D4820 File Offset: 0x000D2A20
		public void Dispatch(int count)
		{
			if (this.usingFallback)
			{
				RenderBuffer activeColorBuffer = Graphics.activeColorBuffer;
				RenderBuffer activeDepthBuffer = Graphics.activeDepthBuffer;
				this.fallbackMat.SetTexture("_Input", this.inputTexture);
				Graphics.Blit(this.inputTexture, this.resultTexture, this.fallbackMat, 0);
				Graphics.SetRenderTarget(activeColorBuffer, activeDepthBuffer);
			}
			else
			{
				this.compute.SetBuffer(0, "_Input", this.inputBuffer);
				this.compute.SetBuffer(0, "_Result", this.resultBuffer);
				this.compute.Dispatch(0, this.AlignDispatchSize(count), 1, 1);
			}
		}

		// Token: 0x06002648 RID: 9800 RVA: 0x000D48C4 File Offset: 0x000D2AC4
		public void IssueRead()
		{
			if (!global::OcclusionCulling.SafeMode)
			{
				Graphics.BufferReadback.IssueRead(this.readbackInst);
			}
		}

		// Token: 0x06002649 RID: 9801 RVA: 0x000D48DC File Offset: 0x000D2ADC
		public void GetResults()
		{
			if (this.resultData != null && this.resultData.Length > 0)
			{
				if (!global::OcclusionCulling.SafeMode && this.readbackInst != IntPtr.Zero)
				{
					Graphics.BufferReadback.GetData(this.readbackInst, ref this.resultData[0]);
				}
				else if (this.usingFallback)
				{
					RenderTexture.active = this.resultTexture;
					this.resultReadTexture.ReadPixels(new Rect(0f, 0f, (float)this.width, (float)this.height), 0, 0);
					this.resultReadTexture.Apply();
					Color32[] pixels = this.resultReadTexture.GetPixels32();
					Array.Copy(pixels, this.resultData, this.resultData.Length);
				}
				else
				{
					this.resultBuffer.GetData(this.resultData);
				}
			}
		}

		// Token: 0x0400227D RID: 8829
		public ComputeBuffer inputBuffer;

		// Token: 0x0400227E RID: 8830
		public ComputeBuffer resultBuffer;

		// Token: 0x0400227F RID: 8831
		public int width;

		// Token: 0x04002280 RID: 8832
		public int height;

		// Token: 0x04002281 RID: 8833
		public int capacity;

		// Token: 0x04002282 RID: 8834
		public int count;

		// Token: 0x04002283 RID: 8835
		public Texture2D inputTexture;

		// Token: 0x04002284 RID: 8836
		public RenderTexture resultTexture;

		// Token: 0x04002285 RID: 8837
		public Texture2D resultReadTexture;

		// Token: 0x04002286 RID: 8838
		public IntPtr readbackInst = IntPtr.Zero;

		// Token: 0x04002287 RID: 8839
		public Color[] inputData = new Color[0];

		// Token: 0x04002288 RID: 8840
		public Color32[] resultData = new Color32[0];

		// Token: 0x04002289 RID: 8841
		private bool usingFallback = true;

		// Token: 0x0400228A RID: 8842
		private Material fallbackMat;

		// Token: 0x0400228B RID: 8843
		private ComputeShader compute;
	}

	// Token: 0x02000809 RID: 2057
	// (Invoke) Token: 0x0600264B RID: 9803
	public delegate void OnVisibilityChanged(bool visible);

	// Token: 0x0200080A RID: 2058
	public enum DebugFilter
	{
		// Token: 0x0400228D RID: 8845
		Off,
		// Token: 0x0400228E RID: 8846
		Dynamic,
		// Token: 0x0400228F RID: 8847
		Static,
		// Token: 0x04002290 RID: 8848
		Grid,
		// Token: 0x04002291 RID: 8849
		All
	}

	// Token: 0x0200080B RID: 2059
	[Flags]
	public enum DebugMask
	{
		// Token: 0x04002293 RID: 8851
		Off = 0,
		// Token: 0x04002294 RID: 8852
		Dynamic = 1,
		// Token: 0x04002295 RID: 8853
		Static = 2,
		// Token: 0x04002296 RID: 8854
		Grid = 4,
		// Token: 0x04002297 RID: 8855
		All = 7
	}

	// Token: 0x0200080C RID: 2060
	[Serializable]
	public class DebugSettings
	{
		// Token: 0x04002298 RID: 8856
		public bool forceFallback;

		// Token: 0x04002299 RID: 8857
		public bool log;

		// Token: 0x0400229A RID: 8858
		public bool showAllVisible;

		// Token: 0x0400229B RID: 8859
		public bool showMipChain;

		// Token: 0x0400229C RID: 8860
		public bool showMain;

		// Token: 0x0400229D RID: 8861
		public int showMainLod;

		// Token: 0x0400229E RID: 8862
		public bool showFallback;

		// Token: 0x0400229F RID: 8863
		public bool showStats;

		// Token: 0x040022A0 RID: 8864
		public bool showScreenBounds;

		// Token: 0x040022A1 RID: 8865
		public global::OcclusionCulling.DebugMask showMask;
	}

	// Token: 0x0200080D RID: 2061
	public class HashedPoolValue
	{
		// Token: 0x040022A2 RID: 8866
		public ulong hashedPoolKey = ulong.MaxValue;

		// Token: 0x040022A3 RID: 8867
		public int hashedPoolIndex = -1;
	}

	// Token: 0x0200080E RID: 2062
	public class HashedPool<ValueType> where ValueType : global::OcclusionCulling.HashedPoolValue, new()
	{
		// Token: 0x06002650 RID: 9808 RVA: 0x000D49E0 File Offset: 0x000D2BE0
		public HashedPool(int capacity, int granularity)
		{
			this.granularity = granularity;
			this.dict = new Dictionary<ulong, ValueType>(capacity);
			this.pool = new List<ValueType>(capacity);
			this.list = new List<ValueType>(capacity);
			this.recycled = new Queue<ValueType>();
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06002651 RID: 9809 RVA: 0x000D4A20 File Offset: 0x000D2C20
		public int Size
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06002652 RID: 9810 RVA: 0x000D4A30 File Offset: 0x000D2C30
		public int Count
		{
			get
			{
				return this.dict.Count;
			}
		}

		// Token: 0x170002CB RID: 715
		public ValueType this[int i]
		{
			get
			{
				return this.list[i];
			}
			set
			{
				this.list[i] = value;
			}
		}

		// Token: 0x06002655 RID: 9813 RVA: 0x000D4A60 File Offset: 0x000D2C60
		public void Clear()
		{
			this.dict.Clear();
			this.pool.Clear();
			this.list.Clear();
			this.recycled.Clear();
		}

		// Token: 0x06002656 RID: 9814 RVA: 0x000D4A90 File Offset: 0x000D2C90
		public ValueType Add(ulong key, int capacityGranularity = 16)
		{
			ValueType valueType;
			if (this.recycled.Count > 0)
			{
				valueType = this.recycled.Dequeue();
				this.list[valueType.hashedPoolIndex] = valueType;
			}
			else
			{
				int count = this.pool.Count;
				if (count == this.pool.Capacity)
				{
					this.pool.Capacity += this.granularity;
				}
				valueType = Activator.CreateInstance<ValueType>();
				valueType.hashedPoolIndex = count;
				this.pool.Add(valueType);
				this.list.Add(valueType);
			}
			valueType.hashedPoolKey = key;
			this.dict.Add(key, valueType);
			return valueType;
		}

		// Token: 0x06002657 RID: 9815 RVA: 0x000D4B50 File Offset: 0x000D2D50
		public void Remove(ValueType value)
		{
			this.dict.Remove(value.hashedPoolKey);
			this.list[value.hashedPoolIndex] = (ValueType)((object)null);
			this.recycled.Enqueue(value);
			value.hashedPoolKey = ulong.MaxValue;
		}

		// Token: 0x06002658 RID: 9816 RVA: 0x000D4BAC File Offset: 0x000D2DAC
		public bool TryGetValue(ulong key, out ValueType value)
		{
			return this.dict.TryGetValue(key, out value);
		}

		// Token: 0x06002659 RID: 9817 RVA: 0x000D4BBC File Offset: 0x000D2DBC
		public bool ContainsKey(ulong key)
		{
			return this.dict.ContainsKey(key);
		}

		// Token: 0x040022A4 RID: 8868
		private int granularity;

		// Token: 0x040022A5 RID: 8869
		private Dictionary<ulong, ValueType> dict;

		// Token: 0x040022A6 RID: 8870
		private List<ValueType> pool;

		// Token: 0x040022A7 RID: 8871
		private List<ValueType> list;

		// Token: 0x040022A8 RID: 8872
		private Queue<ValueType> recycled;
	}

	// Token: 0x0200080F RID: 2063
	public class SimpleList<T>
	{
		// Token: 0x0600265A RID: 9818 RVA: 0x000D4BCC File Offset: 0x000D2DCC
		public SimpleList()
		{
			this.array = global::OcclusionCulling.SimpleList<T>.emptyArray;
		}

		// Token: 0x0600265B RID: 9819 RVA: 0x000D4BE0 File Offset: 0x000D2DE0
		public SimpleList(int capacity)
		{
			this.array = ((capacity != 0) ? new T[capacity] : global::OcclusionCulling.SimpleList<T>.emptyArray);
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x0600265C RID: 9820 RVA: 0x000D4C04 File Offset: 0x000D2E04
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x0600265D RID: 9821 RVA: 0x000D4C0C File Offset: 0x000D2E0C
		// (set) Token: 0x0600265E RID: 9822 RVA: 0x000D4C18 File Offset: 0x000D2E18
		public int Capacity
		{
			get
			{
				return this.array.Length;
			}
			set
			{
				if (value != this.array.Length)
				{
					if (value > 0)
					{
						T[] destinationArray = new T[value];
						if (this.count > 0)
						{
							Array.Copy(this.array, 0, destinationArray, 0, this.count);
						}
						this.array = destinationArray;
					}
					else
					{
						this.array = global::OcclusionCulling.SimpleList<T>.emptyArray;
					}
				}
			}
		}

		// Token: 0x170002CE RID: 718
		public T this[int index]
		{
			get
			{
				return this.array[index];
			}
			set
			{
				this.array[index] = value;
			}
		}

		// Token: 0x06002661 RID: 9825 RVA: 0x000D4C98 File Offset: 0x000D2E98
		public void Add(T item)
		{
			if (this.count == this.array.Length)
			{
				this.EnsureCapacity(this.count + 1);
			}
			this.array[this.count++] = item;
		}

		// Token: 0x06002662 RID: 9826 RVA: 0x000D4CE4 File Offset: 0x000D2EE4
		public void Clear()
		{
			if (this.count > 0)
			{
				Array.Clear(this.array, 0, this.count);
				this.count = 0;
			}
		}

		// Token: 0x06002663 RID: 9827 RVA: 0x000D4D0C File Offset: 0x000D2F0C
		public bool Contains(T item)
		{
			for (int i = 0; i < this.count; i++)
			{
				if (this.array[i].Equals(item))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002664 RID: 9828 RVA: 0x000D4D58 File Offset: 0x000D2F58
		public void CopyTo(T[] array)
		{
			Array.Copy(this.array, 0, array, 0, this.count);
		}

		// Token: 0x06002665 RID: 9829 RVA: 0x000D4D70 File Offset: 0x000D2F70
		public void EnsureCapacity(int min)
		{
			if (this.array.Length < min)
			{
				int num = (this.array.Length != 0) ? (this.array.Length * 2) : 16;
				num = ((num >= min) ? num : min);
				this.Capacity = num;
			}
		}

		// Token: 0x040022A9 RID: 8873
		private const int defaultCapacity = 16;

		// Token: 0x040022AA RID: 8874
		private static readonly T[] emptyArray = new T[0];

		// Token: 0x040022AB RID: 8875
		public T[] array;

		// Token: 0x040022AC RID: 8876
		public int count;
	}

	// Token: 0x02000810 RID: 2064
	public class SmartListValue
	{
		// Token: 0x040022AD RID: 8877
		public int hashedListIndex = -1;
	}

	// Token: 0x02000811 RID: 2065
	public class SmartList
	{
		// Token: 0x06002668 RID: 9832 RVA: 0x000D4DE0 File Offset: 0x000D2FE0
		public SmartList(int capacity)
		{
			this.list = new global::OccludeeState[capacity];
			this.slots = new int[capacity];
			this.recycled = new Queue<int>();
			this.count = 0;
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06002669 RID: 9833 RVA: 0x000D4E14 File Offset: 0x000D3014
		public global::OccludeeState[] List
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x0600266A RID: 9834 RVA: 0x000D4E1C File Offset: 0x000D301C
		public int[] Slots
		{
			get
			{
				return this.slots;
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x0600266B RID: 9835 RVA: 0x000D4E24 File Offset: 0x000D3024
		public int Size
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x0600266C RID: 9836 RVA: 0x000D4E2C File Offset: 0x000D302C
		public int Count
		{
			get
			{
				return this.count - this.recycled.Count;
			}
		}

		// Token: 0x170002D3 RID: 723
		public global::OccludeeState this[int i]
		{
			get
			{
				return this.list[i];
			}
			set
			{
				this.list[i] = value;
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x0600266F RID: 9839 RVA: 0x000D4E58 File Offset: 0x000D3058
		// (set) Token: 0x06002670 RID: 9840 RVA: 0x000D4E64 File Offset: 0x000D3064
		public int Capacity
		{
			get
			{
				return this.list.Length;
			}
			set
			{
				if (value != this.list.Length)
				{
					if (value > 0)
					{
						global::OccludeeState[] destinationArray = new global::OccludeeState[value];
						int[] destinationArray2 = new int[value];
						if (this.count > 0)
						{
							Array.Copy(this.list, destinationArray, this.count);
							Array.Copy(this.slots, destinationArray2, this.count);
						}
						this.list = destinationArray;
						this.slots = destinationArray2;
					}
					else
					{
						this.list = global::OcclusionCulling.SmartList.emptyList;
						this.slots = global::OcclusionCulling.SmartList.emptySlots;
					}
				}
			}
		}

		// Token: 0x06002671 RID: 9841 RVA: 0x000D4EF0 File Offset: 0x000D30F0
		public void Add(global::OccludeeState value, int capacityGranularity = 16)
		{
			int num;
			if (this.recycled.Count > 0)
			{
				num = this.recycled.Dequeue();
				this.list[num] = value;
				this.slots[num] = value.slot;
			}
			else
			{
				num = this.count;
				if (num == this.list.Length)
				{
					this.EnsureCapacity(this.count + 1);
				}
				this.list[num] = value;
				this.slots[num] = value.slot;
				this.count++;
			}
			value.hashedListIndex = num;
		}

		// Token: 0x06002672 RID: 9842 RVA: 0x000D4F88 File Offset: 0x000D3188
		public void Remove(global::OccludeeState value)
		{
			int hashedListIndex = value.hashedListIndex;
			this.list[hashedListIndex] = null;
			this.slots[hashedListIndex] = -1;
			this.recycled.Enqueue(hashedListIndex);
			value.hashedListIndex = -1;
		}

		// Token: 0x06002673 RID: 9843 RVA: 0x000D4FC4 File Offset: 0x000D31C4
		public bool Contains(global::OccludeeState value)
		{
			int hashedListIndex = value.hashedListIndex;
			return hashedListIndex >= 0 && this.list[hashedListIndex] != null;
		}

		// Token: 0x06002674 RID: 9844 RVA: 0x000D4FF0 File Offset: 0x000D31F0
		public void EnsureCapacity(int min)
		{
			if (this.list.Length < min)
			{
				int num = (this.list.Length != 0) ? (this.list.Length * 2) : 16;
				num = ((num >= min) ? num : min);
				this.Capacity = num;
			}
		}

		// Token: 0x040022AE RID: 8878
		private const int defaultCapacity = 16;

		// Token: 0x040022AF RID: 8879
		private static readonly global::OccludeeState[] emptyList = new global::OccludeeState[0];

		// Token: 0x040022B0 RID: 8880
		private static readonly int[] emptySlots = new int[0];

		// Token: 0x040022B1 RID: 8881
		private global::OccludeeState[] list;

		// Token: 0x040022B2 RID: 8882
		private int[] slots;

		// Token: 0x040022B3 RID: 8883
		private Queue<int> recycled;

		// Token: 0x040022B4 RID: 8884
		private int count;
	}

	// Token: 0x02000812 RID: 2066
	[Serializable]
	public class Cell : global::OcclusionCulling.HashedPoolValue
	{
		// Token: 0x06002677 RID: 9847 RVA: 0x000D5060 File Offset: 0x000D3260
		public void Reset()
		{
			this.x = (this.y = (this.z = 0));
			this.bounds = default(Bounds);
			this.sphereBounds = Vector4.zero;
			this.isVisible = true;
			this.staticBucket = null;
			this.dynamicBucket = null;
		}

		// Token: 0x06002678 RID: 9848 RVA: 0x000D50B8 File Offset: 0x000D32B8
		public global::OcclusionCulling.Cell Initialize(int x, int y, int z, Bounds bounds)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.bounds = bounds;
			this.sphereBounds = new Vector4(bounds.center.x, bounds.center.y, bounds.center.z, bounds.extents.magnitude);
			this.isVisible = true;
			this.staticBucket = new global::OcclusionCulling.SmartList(32);
			this.dynamicBucket = new global::OcclusionCulling.SmartList(32);
			return this;
		}

		// Token: 0x040022B5 RID: 8885
		public int x;

		// Token: 0x040022B6 RID: 8886
		public int y;

		// Token: 0x040022B7 RID: 8887
		public int z;

		// Token: 0x040022B8 RID: 8888
		public Bounds bounds;

		// Token: 0x040022B9 RID: 8889
		public Vector4 sphereBounds;

		// Token: 0x040022BA RID: 8890
		public bool isVisible;

		// Token: 0x040022BB RID: 8891
		public global::OcclusionCulling.SmartList staticBucket;

		// Token: 0x040022BC RID: 8892
		public global::OcclusionCulling.SmartList dynamicBucket;
	}

	// Token: 0x02000813 RID: 2067
	public struct Sphere
	{
		// Token: 0x06002679 RID: 9849 RVA: 0x000D514C File Offset: 0x000D334C
		public Sphere(Vector3 position, float radius)
		{
			this.position = position;
			this.radius = radius;
		}

		// Token: 0x040022BD RID: 8893
		public Vector3 position;

		// Token: 0x040022BE RID: 8894
		public float radius;
	}
}
