﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200022C RID: 556
public class SocketMod_PlantCheck : global::SocketMod
{
	// Token: 0x06000FF9 RID: 4089 RVA: 0x000612F8 File Offset: 0x0005F4F8
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = ((!this.wantsCollide) ? new Color(1f, 0f, 0f, 0.7f) : new Color(0f, 1f, 0f, 0.7f));
		Gizmos.DrawSphere(Vector3.zero, this.sphereRadius);
	}

	// Token: 0x06000FFA RID: 4090 RVA: 0x0006136C File Offset: 0x0005F56C
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 position = place.position + place.rotation * this.worldPosition;
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(position, this.sphereRadius, list, this.layerMask.value, this.queryTriggers);
		foreach (global::BaseEntity baseEntity in list)
		{
			global::PlantEntity component = baseEntity.GetComponent<global::PlantEntity>();
			if (component && this.wantsCollide)
			{
				Pool.FreeList<global::BaseEntity>(ref list);
				return true;
			}
			if (component && !this.wantsCollide)
			{
				Pool.FreeList<global::BaseEntity>(ref list);
				return false;
			}
		}
		Pool.FreeList<global::BaseEntity>(ref list);
		return !this.wantsCollide;
	}

	// Token: 0x04000AAE RID: 2734
	public float sphereRadius = 1f;

	// Token: 0x04000AAF RID: 2735
	public LayerMask layerMask;

	// Token: 0x04000AB0 RID: 2736
	public QueryTriggerInteraction queryTriggers;

	// Token: 0x04000AB1 RID: 2737
	public bool wantsCollide;
}
