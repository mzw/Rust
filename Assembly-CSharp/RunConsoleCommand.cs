﻿using System;
using UnityEngine;

// Token: 0x0200075A RID: 1882
public class RunConsoleCommand : MonoBehaviour
{
	// Token: 0x06002333 RID: 9011 RVA: 0x000C2E8C File Offset: 0x000C108C
	public void ClientRun(string command)
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Client, command, new object[0]);
	}
}
