﻿using System;
using UnityEngine;

// Token: 0x0200054C RID: 1356
public class DecorSpawn : MonoBehaviour, IClientComponent
{
	// Token: 0x0400179E RID: 6046
	public global::SpawnFilter Filter;

	// Token: 0x0400179F RID: 6047
	public string ResourceFolder = string.Empty;

	// Token: 0x040017A0 RID: 6048
	public uint Seed;

	// Token: 0x040017A1 RID: 6049
	public float ObjectCutoff = 0.2f;

	// Token: 0x040017A2 RID: 6050
	public float ObjectTapering = 0.2f;

	// Token: 0x040017A3 RID: 6051
	public int ObjectsPerPatch = 10;

	// Token: 0x040017A4 RID: 6052
	public float ClusterRadius = 2f;

	// Token: 0x040017A5 RID: 6053
	public int ClusterSizeMin = 1;

	// Token: 0x040017A6 RID: 6054
	public int ClusterSizeMax = 10;

	// Token: 0x040017A7 RID: 6055
	public int PatchCount = 8;

	// Token: 0x040017A8 RID: 6056
	public int PatchSize = 100;

	// Token: 0x040017A9 RID: 6057
	public bool LOD = true;
}
