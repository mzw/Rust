﻿using System;
using Facepunch;
using ProtoBuf;

// Token: 0x0200039A RID: 922
public class EnvSync : global::PointEntity
{
	// Token: 0x060015C2 RID: 5570 RVA: 0x0007D464 File Offset: 0x0007B664
	public override void ServerInit()
	{
		base.ServerInit();
		base.InvokeRepeating(new Action(this.UpdateNetwork), 5f, 5f);
	}

	// Token: 0x060015C3 RID: 5571 RVA: 0x0007D488 File Offset: 0x0007B688
	private void UpdateNetwork()
	{
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060015C4 RID: 5572 RVA: 0x0007D494 File Offset: 0x0007B694
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.environment = Pool.Get<Environment>();
		if (TOD_Sky.Instance)
		{
			info.msg.environment.dateTime = TOD_Sky.Instance.Cycle.DateTime.ToBinary();
		}
		if (SingletonComponent<global::Climate>.Instance)
		{
			info.msg.environment.clouds = SingletonComponent<global::Climate>.Instance.Overrides.Clouds;
			info.msg.environment.fog = SingletonComponent<global::Climate>.Instance.Overrides.Fog;
			info.msg.environment.wind = SingletonComponent<global::Climate>.Instance.Overrides.Wind;
			info.msg.environment.rain = SingletonComponent<global::Climate>.Instance.Overrides.Rain;
		}
	}

	// Token: 0x060015C5 RID: 5573 RVA: 0x0007D580 File Offset: 0x0007B780
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.environment == null)
		{
			return;
		}
		if (!TOD_Sky.Instance)
		{
			return;
		}
		TOD_Sky.Instance.Cycle.DateTime = DateTime.FromBinary(info.msg.environment.dateTime);
	}

	// Token: 0x04001010 RID: 4112
	private const float syncInterval = 5f;

	// Token: 0x04001011 RID: 4113
	private const float syncIntervalInv = 0.2f;
}
