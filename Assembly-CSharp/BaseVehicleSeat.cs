﻿using System;

// Token: 0x020003E7 RID: 999
public class BaseVehicleSeat : global::BaseMountable
{
	// Token: 0x06001741 RID: 5953 RVA: 0x000857D4 File Offset: 0x000839D4
	public global::BaseVehicle GetVehicleParent()
	{
		return base.GetParentEntity() as global::BaseVehicle;
	}
}
