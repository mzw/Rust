﻿using System;
using UnityEngine;

// Token: 0x020005E4 RID: 1508
public static class TerrainPlacementEx
{
	// Token: 0x06001EFB RID: 7931 RVA: 0x000AF6A4 File Offset: 0x000AD8A4
	public static void ApplyTerrainPlacements(this Transform transform, global::TerrainPlacement[] placements, Vector3 pos, Quaternion rot, Vector3 scale)
	{
		if (placements.Length == 0)
		{
			return;
		}
		Matrix4x4 localToWorld = Matrix4x4.TRS(pos, rot, scale);
		Matrix4x4 inverse = localToWorld.inverse;
		for (int i = 0; i < placements.Length; i++)
		{
			placements[i].Apply(localToWorld, inverse);
		}
	}

	// Token: 0x06001EFC RID: 7932 RVA: 0x000AF6EC File Offset: 0x000AD8EC
	public static void ApplyTerrainPlacements(this Transform transform, global::TerrainPlacement[] placements)
	{
		transform.ApplyTerrainPlacements(placements, transform.position, transform.rotation, transform.lossyScale);
	}
}
