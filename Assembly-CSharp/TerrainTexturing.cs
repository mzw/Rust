﻿using System;
using UnityEngine;

// Token: 0x0200058F RID: 1423
[ExecuteInEditMode]
public class TerrainTexturing : global::TerrainExtension
{
	// Token: 0x040018AE RID: 6318
	private const int coarseHeightDownscale = 1;

	// Token: 0x040018AF RID: 6319
	public bool debugFoliageDisplacement;

	// Token: 0x040018B0 RID: 6320
	private const int CoarseSlopeBlurPasses = 4;

	// Token: 0x040018B1 RID: 6321
	private const float CoarseSlopeBlurRadius = 1f;
}
