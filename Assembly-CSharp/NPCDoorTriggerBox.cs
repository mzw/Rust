﻿using System;
using ConVar;
using UnityEngine;

// Token: 0x020001BD RID: 445
public class NPCDoorTriggerBox : MonoBehaviour
{
	// Token: 0x06000E88 RID: 3720 RVA: 0x0005BAB8 File Offset: 0x00059CB8
	public void Setup(global::Door d)
	{
		this.door = d;
		base.transform.SetParent(this.door.transform, false);
		base.gameObject.layer = 18;
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		boxCollider.isTrigger = true;
		boxCollider.center = Vector3.zero;
		boxCollider.size = Vector3.one * ConVar.AI.npc_door_trigger_size;
	}

	// Token: 0x06000E89 RID: 3721 RVA: 0x0005BB24 File Offset: 0x00059D24
	private void OnTriggerEnter(Collider other)
	{
		if (this.door == null || this.door.isClient || this.door.IsOpen() || this.door.IsLocked())
		{
			return;
		}
		if (global::NPCDoorTriggerBox.playerServerLayer < 0)
		{
			global::NPCDoorTriggerBox.playerServerLayer = LayerMask.NameToLayer("Player (Server)");
		}
		if ((other.gameObject.layer & global::NPCDoorTriggerBox.playerServerLayer) > 0)
		{
			global::BasePlayer component = other.gameObject.GetComponent<global::BasePlayer>();
			if (component != null && component is global::NPCPlayer)
			{
				this.door.SetOpen(true);
			}
		}
	}

	// Token: 0x040008A1 RID: 2209
	private global::Door door;

	// Token: 0x040008A2 RID: 2210
	private static int playerServerLayer = -1;
}
