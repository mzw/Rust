﻿using System;
using UnityEngine;

// Token: 0x020005FE RID: 1534
[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class DeferredExtensionMesh : MonoBehaviour
{
	// Token: 0x04001A47 RID: 6727
	public global::SubsurfaceProfile subsurfaceProfile;
}
