﻿using System;
using UnityEngine;

// Token: 0x02000210 RID: 528
[ExecuteInEditMode]
public class ConstructionPlaceholder : global::PrefabAttribute, global::IPrefabPreProcess
{
	// Token: 0x06000F8F RID: 3983 RVA: 0x0005F240 File Offset: 0x0005D440
	protected override void AttributeSetup(GameObject rootObj, string name, bool serverside, bool clientside, bool bundling)
	{
		base.AttributeSetup(rootObj, name, serverside, clientside, bundling);
		if (clientside)
		{
			if (this.renderer)
			{
				MeshFilter meshFilter = rootObj.GetComponent<MeshFilter>();
				MeshRenderer meshRenderer = rootObj.GetComponent<MeshRenderer>();
				if (!meshFilter)
				{
					meshFilter = rootObj.AddComponent<MeshFilter>();
					meshFilter.sharedMesh = this.mesh;
				}
				if (!meshRenderer)
				{
					meshRenderer = rootObj.AddComponent<MeshRenderer>();
					meshRenderer.sharedMaterial = this.material;
					meshRenderer.shadowCastingMode = 0;
				}
			}
			if (this.collider)
			{
				MeshCollider meshCollider = rootObj.GetComponent<MeshCollider>();
				if (!meshCollider)
				{
					meshCollider = rootObj.AddComponent<MeshCollider>();
					meshCollider.sharedMesh = this.mesh;
				}
			}
		}
	}

	// Token: 0x06000F90 RID: 3984 RVA: 0x0005F2F0 File Offset: 0x0005D4F0
	protected override Type GetIndexedType()
	{
		return typeof(global::ConstructionPlaceholder);
	}

	// Token: 0x04000A56 RID: 2646
	public Mesh mesh;

	// Token: 0x04000A57 RID: 2647
	public Material material;

	// Token: 0x04000A58 RID: 2648
	public bool renderer;

	// Token: 0x04000A59 RID: 2649
	public bool collider;
}
