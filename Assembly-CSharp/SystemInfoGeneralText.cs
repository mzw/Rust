﻿using System;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020002FA RID: 762
public class SystemInfoGeneralText : MonoBehaviour
{
	// Token: 0x1700016A RID: 362
	// (get) Token: 0x0600133E RID: 4926 RVA: 0x000712D4 File Offset: 0x0006F4D4
	public static string currentInfo
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("System");
			stringBuilder.AppendLine();
			stringBuilder.Append("\tName: ");
			stringBuilder.Append(SystemInfo.deviceName);
			stringBuilder.AppendLine();
			stringBuilder.Append("\tOS:   ");
			stringBuilder.Append(SystemInfo.operatingSystem);
			stringBuilder.AppendLine();
			stringBuilder.AppendLine();
			stringBuilder.Append("CPU");
			stringBuilder.AppendLine();
			stringBuilder.Append("\tModel:  ");
			stringBuilder.Append(SystemInfo.processorType);
			stringBuilder.AppendLine();
			stringBuilder.Append("\tCores:  ");
			stringBuilder.Append(SystemInfo.processorCount);
			stringBuilder.AppendLine();
			stringBuilder.Append("\tMemory: ");
			stringBuilder.Append(SystemInfo.systemMemorySize);
			stringBuilder.Append(" MB");
			stringBuilder.AppendLine();
			stringBuilder.AppendLine();
			stringBuilder.Append("GPU");
			stringBuilder.AppendLine();
			stringBuilder.Append("\tModel:  ");
			stringBuilder.Append(SystemInfo.graphicsDeviceName);
			stringBuilder.AppendLine();
			stringBuilder.Append("\tAPI:    ");
			stringBuilder.Append(SystemInfo.graphicsDeviceVersion);
			stringBuilder.AppendLine();
			stringBuilder.Append("\tMemory: ");
			stringBuilder.Append(SystemInfo.graphicsMemorySize);
			stringBuilder.Append(" MB");
			stringBuilder.AppendLine();
			stringBuilder.Append("\tSM:     ");
			stringBuilder.Append(SystemInfo.graphicsShaderLevel);
			stringBuilder.AppendLine();
			stringBuilder.AppendLine();
			stringBuilder.Append("Process");
			stringBuilder.AppendLine();
			Process currentProcess = Process.GetCurrentProcess();
			if (currentProcess != null && currentProcess.WorkingSet64 > 0L)
			{
				stringBuilder.Append("\tPriority: ");
				stringBuilder.Append(currentProcess.BasePriority);
				stringBuilder.AppendLine();
				stringBuilder.Append("\tThreads:  ");
				stringBuilder.Append(currentProcess.Threads.Count);
				stringBuilder.AppendLine();
				stringBuilder.Append("\tMemory:   ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB(currentProcess.WorkingSet64));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tPeak:     ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB(currentProcess.PeakWorkingSet64));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
			}
			else
			{
				stringBuilder.Append("\tMemory:   ");
				stringBuilder.Append(global::SystemInfoEx.systemMemoryUsed);
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
			}
			stringBuilder.AppendLine();
			stringBuilder.Append("Mono");
			stringBuilder.AppendLine();
			stringBuilder.Append("\tCollects: ");
			stringBuilder.Append(GC.CollectionCount(0));
			stringBuilder.AppendLine();
			stringBuilder.Append("\tMemory:   ");
			stringBuilder.Append(global::SystemInfoGeneralText.MB(GC.GetTotalMemory(false)));
			stringBuilder.Append(" MB");
			stringBuilder.AppendLine();
			stringBuilder.AppendLine();
			if (global::World.Seed > 0u && global::World.Size > 0u)
			{
				stringBuilder.Append("World");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tSeed:        ");
				stringBuilder.Append(global::World.Seed);
				stringBuilder.AppendLine();
				stringBuilder.Append("\tSize:        ");
				stringBuilder.Append(global::SystemInfoGeneralText.KM2(global::World.Size));
				stringBuilder.Append(" km²");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tHeightmap:   ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB((!global::TerrainMeta.HeightMap) ? 0L : global::TerrainMeta.HeightMap.GetMemoryUsage()));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tWatermap:    ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB((!global::TerrainMeta.WaterMap) ? 0L : global::TerrainMeta.WaterMap.GetMemoryUsage()));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tSplatmap:    ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB((!global::TerrainMeta.SplatMap) ? 0L : global::TerrainMeta.SplatMap.GetMemoryUsage()));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tBiomemap:    ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB((!global::TerrainMeta.BiomeMap) ? 0L : global::TerrainMeta.BiomeMap.GetMemoryUsage()));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tTopologymap: ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB((!global::TerrainMeta.TopologyMap) ? 0L : global::TerrainMeta.TopologyMap.GetMemoryUsage()));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
				stringBuilder.Append("\tAlphamap:    ");
				stringBuilder.Append(global::SystemInfoGeneralText.MB((!global::TerrainMeta.AlphaMap) ? 0L : global::TerrainMeta.AlphaMap.GetMemoryUsage()));
				stringBuilder.Append(" MB");
				stringBuilder.AppendLine();
			}
			stringBuilder.AppendLine();
			if (!string.IsNullOrEmpty(global::World.Checksum))
			{
				stringBuilder.AppendLine("Checksum");
				stringBuilder.Append('\t');
				stringBuilder.AppendLine(global::World.Checksum);
			}
			return stringBuilder.ToString();
		}
	}

	// Token: 0x0600133F RID: 4927 RVA: 0x00071854 File Offset: 0x0006FA54
	protected void Update()
	{
		this.text.text = global::SystemInfoGeneralText.currentInfo;
	}

	// Token: 0x06001340 RID: 4928 RVA: 0x00071868 File Offset: 0x0006FA68
	private static long MB(long bytes)
	{
		return bytes / 1048576L;
	}

	// Token: 0x06001341 RID: 4929 RVA: 0x00071874 File Offset: 0x0006FA74
	private static long MB(ulong bytes)
	{
		return global::SystemInfoGeneralText.MB((long)bytes);
	}

	// Token: 0x06001342 RID: 4930 RVA: 0x0007187C File Offset: 0x0006FA7C
	private static int KM2(float meters)
	{
		return Mathf.RoundToInt(meters * meters * 1E-06f);
	}

	// Token: 0x04000DF3 RID: 3571
	public Text text;
}
