﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using ConVar;
using EasyAntiCheat.Server;
using EasyAntiCheat.Server.Hydra;
using EasyAntiCheat.Server.Hydra.Cerberus;
using EasyAntiCheat.Server.Scout;
using Network;
using Oxide.Core;
using UnityEngine;

// Token: 0x02000621 RID: 1569
public class EACServer
{
	// Token: 0x06001FB9 RID: 8121 RVA: 0x000B4034 File Offset: 0x000B2234
	public static void Encrypt(Connection connection, MemoryStream src, int srcOffset, MemoryStream dst, int dstOffset)
	{
		global::EACServer.easyAntiCheat.NetProtect.ProtectMessage(global::EACServer.GetClient(connection), src, (long)srcOffset, dst, (long)dstOffset);
	}

	// Token: 0x06001FBA RID: 8122 RVA: 0x000B4054 File Offset: 0x000B2254
	public static void Decrypt(Connection connection, MemoryStream src, int srcOffset, MemoryStream dst, int dstOffset)
	{
		global::EACServer.easyAntiCheat.NetProtect.UnprotectMessage(global::EACServer.GetClient(connection), src, (long)srcOffset, dst, (long)dstOffset);
	}

	// Token: 0x06001FBB RID: 8123 RVA: 0x000B4074 File Offset: 0x000B2274
	public static EasyAntiCheat.Server.Hydra.Client GetClient(Connection connection)
	{
		EasyAntiCheat.Server.Hydra.Client result;
		global::EACServer.connection2client.TryGetValue(connection, out result);
		return result;
	}

	// Token: 0x06001FBC RID: 8124 RVA: 0x000B4090 File Offset: 0x000B2290
	public static Connection GetConnection(EasyAntiCheat.Server.Hydra.Client client)
	{
		Connection result;
		global::EACServer.client2connection.TryGetValue(client, out result);
		return result;
	}

	// Token: 0x06001FBD RID: 8125 RVA: 0x000B40AC File Offset: 0x000B22AC
	public static bool IsAuthenticated(Connection connection)
	{
		ClientStatus clientStatus;
		global::EACServer.connection2status.TryGetValue(connection, out clientStatus);
		return clientStatus == 5;
	}

	// Token: 0x06001FBE RID: 8126 RVA: 0x000B40CC File Offset: 0x000B22CC
	private static void OnAuthenticatedLocal(Connection connection)
	{
		if (connection.authStatus == string.Empty)
		{
			connection.authStatus = "ok";
		}
		global::EACServer.connection2status[connection] = 2;
	}

	// Token: 0x06001FBF RID: 8127 RVA: 0x000B40FC File Offset: 0x000B22FC
	private static void OnAuthenticatedRemote(Connection connection)
	{
		global::EACServer.connection2status[connection] = 5;
	}

	// Token: 0x06001FC0 RID: 8128 RVA: 0x000B410C File Offset: 0x000B230C
	public static bool ShouldIgnore(Connection connection)
	{
		return !ConVar.Server.secure || connection.authLevel >= 3u;
	}

	// Token: 0x06001FC1 RID: 8129 RVA: 0x000B412C File Offset: 0x000B232C
	private static void HandleClientUpdate(ClientStatusUpdate<EasyAntiCheat.Server.Hydra.Client> clientStatus)
	{
		using (TimeWarning.New("AntiCheatKickPlayer", 10L))
		{
			EasyAntiCheat.Server.Hydra.Client clientObject = clientStatus.ClientObject;
			Connection connection = global::EACServer.GetConnection(clientObject);
			if (connection == null)
			{
				Debug.LogError("EAC status update for invalid client: " + clientObject.ClientID);
			}
			else if (!global::EACServer.ShouldIgnore(connection))
			{
				if (clientStatus.RequiresKick)
				{
					string text = clientStatus.Message;
					if (string.IsNullOrEmpty(text))
					{
						text = clientStatus.Status.ToString();
					}
					Debug.Log(string.Concat(new object[]
					{
						"[EAC] Kicking ",
						connection.userid,
						" (",
						text,
						")"
					}));
					connection.authStatus = "eac";
					Network.Net.sv.Kick(connection, "EAC: " + text);
					if (clientStatus.Status == 3)
					{
						connection.authStatus = "eacbanned";
						Interface.CallHook("OnPlayerBanned", new object[]
						{
							connection,
							connection.authStatus
						});
						global::ServerUsers.Set(connection.userid, global::ServerUsers.UserGroup.Banned, connection.username, "EAC");
						global::ConsoleNetwork.BroadcastToAllClients("chat.add", new object[]
						{
							0,
							"<color=#fff>SERVER</color> Kicking " + connection.username + " (banned by anticheat)"
						});
						ConVar.Entity.DeleteBy(connection.userid);
					}
					global::EACServer.easyAntiCheat.UnregisterClient(clientObject);
					global::EACServer.client2connection.Remove(clientObject);
					global::EACServer.connection2client.Remove(connection);
					global::EACServer.connection2status.Remove(connection);
				}
				else if (clientStatus.Status == 2)
				{
					global::EACServer.OnAuthenticatedLocal(connection);
					global::EACServer.easyAntiCheat.SetClientNetworkState(clientObject, false);
				}
				else if (clientStatus.Status == 5)
				{
					global::EACServer.OnAuthenticatedRemote(connection);
				}
			}
		}
	}

	// Token: 0x06001FC2 RID: 8130 RVA: 0x000B4340 File Offset: 0x000B2540
	private static void SendToClient(EasyAntiCheat.Server.Hydra.Client client, byte[] message, int messageLength)
	{
		Connection connection = global::EACServer.GetConnection(client);
		if (connection == null)
		{
			Debug.LogError("EAC network packet for invalid client: " + client.ClientID);
			return;
		}
		if (Network.Net.sv.write.Start())
		{
			Network.Net.sv.write.PacketID(22);
			Network.Net.sv.write.UInt32((uint)messageLength);
			Network.Net.sv.write.Write(message, 0, messageLength);
			Network.Net.sv.write.Send(new SendInfo(connection));
		}
	}

	// Token: 0x06001FC3 RID: 8131 RVA: 0x000B43D4 File Offset: 0x000B25D4
	public static void DoStartup()
	{
		global::EACServer.client2connection.Clear();
		global::EACServer.connection2client.Clear();
		global::EACServer.connection2status.Clear();
		Log.SetOut(new StreamWriter(ConVar.Server.rootFolder + "/Log.EAC.txt", true)
		{
			AutoFlush = true
		});
		Log.Prefix = string.Empty;
		Log.Level = 3;
		if (global::EACServer.<>f__mg$cache0 == null)
		{
			global::EACServer.<>f__mg$cache0 = new EasyAntiCheatServer<EasyAntiCheat.Server.Hydra.Client>.ClientStatusHandler(global::EACServer.HandleClientUpdate);
		}
		global::EACServer.easyAntiCheat = new EasyAntiCheatServer<EasyAntiCheat.Server.Hydra.Client>(global::EACServer.<>f__mg$cache0, 20, ConVar.Server.hostname);
		global::EACServer.playerTracker = global::EACServer.easyAntiCheat.Cerberus;
		global::EACServer.playerTracker.LogGameRoundStart(string.Concat(new object[]
		{
			Application.loadedLevelName,
			"_",
			global::World.Size,
			"_",
			global::World.Seed
		}));
		global::EACServer.eacScout = new Scout();
	}

	// Token: 0x06001FC4 RID: 8132 RVA: 0x000B44C0 File Offset: 0x000B26C0
	public static void DoUpdate()
	{
		if (global::EACServer.easyAntiCheat != null)
		{
			global::EACServer.easyAntiCheat.HandleClientUpdates();
			if (Network.Net.sv != null && Network.Net.sv.IsConnected())
			{
				EasyAntiCheat.Server.Hydra.Client client;
				byte[] message;
				int messageLength;
				while (global::EACServer.easyAntiCheat.PopNetworkMessage(ref client, ref message, ref messageLength))
				{
					global::EACServer.SendToClient(client, message, messageLength);
				}
			}
		}
	}

	// Token: 0x06001FC5 RID: 8133 RVA: 0x000B451C File Offset: 0x000B271C
	public static void DoShutdown()
	{
		global::EACServer.client2connection.Clear();
		global::EACServer.connection2client.Clear();
		global::EACServer.connection2status.Clear();
		if (global::EACServer.eacScout != null)
		{
			Debug.Log("EasyAntiCheat Scout Shutting Down");
			global::EACServer.eacScout.Dispose();
			global::EACServer.eacScout = null;
		}
		if (global::EACServer.easyAntiCheat != null)
		{
			Debug.Log("EasyAntiCheat Server Shutting Down");
			global::EACServer.easyAntiCheat.Dispose();
			global::EACServer.easyAntiCheat = null;
		}
	}

	// Token: 0x06001FC6 RID: 8134 RVA: 0x000B4590 File Offset: 0x000B2790
	public static void OnLeaveGame(Connection connection)
	{
		if (global::EACServer.easyAntiCheat != null)
		{
			EasyAntiCheat.Server.Hydra.Client client = global::EACServer.GetClient(connection);
			global::EACServer.easyAntiCheat.UnregisterClient(client);
			global::EACServer.client2connection.Remove(client);
			global::EACServer.connection2client.Remove(connection);
			global::EACServer.connection2status.Remove(connection);
		}
	}

	// Token: 0x06001FC7 RID: 8135 RVA: 0x000B45E0 File Offset: 0x000B27E0
	public static void OnJoinGame(Connection connection)
	{
		if (global::EACServer.easyAntiCheat != null)
		{
			EasyAntiCheat.Server.Hydra.Client client = global::EACServer.easyAntiCheat.GenerateCompatibilityClient();
			global::EACServer.easyAntiCheat.RegisterClient(client, connection.userid.ToString(), connection.ipaddress, connection.ownerid.ToString(), connection.username, (connection.authLevel <= 0u) ? 0 : 1);
			global::EACServer.client2connection.Add(client, connection);
			global::EACServer.connection2client.Add(connection, client);
			global::EACServer.connection2status.Add(connection, 0);
			if (global::EACServer.ShouldIgnore(connection))
			{
				global::EACServer.OnAuthenticatedLocal(connection);
				global::EACServer.OnAuthenticatedRemote(connection);
			}
		}
		else
		{
			global::EACServer.OnAuthenticatedLocal(connection);
			global::EACServer.OnAuthenticatedRemote(connection);
		}
	}

	// Token: 0x06001FC8 RID: 8136 RVA: 0x000B469C File Offset: 0x000B289C
	public static void OnStartLoading(Connection connection)
	{
		if (global::EACServer.easyAntiCheat != null)
		{
			EasyAntiCheat.Server.Hydra.Client client = global::EACServer.GetClient(connection);
			global::EACServer.easyAntiCheat.SetClientNetworkState(client, false);
		}
	}

	// Token: 0x06001FC9 RID: 8137 RVA: 0x000B46C8 File Offset: 0x000B28C8
	public static void OnFinishLoading(Connection connection)
	{
		if (global::EACServer.easyAntiCheat != null)
		{
			EasyAntiCheat.Server.Hydra.Client client = global::EACServer.GetClient(connection);
			global::EACServer.easyAntiCheat.SetClientNetworkState(client, true);
		}
	}

	// Token: 0x06001FCA RID: 8138 RVA: 0x000B46F4 File Offset: 0x000B28F4
	public static void OnMessageReceived(Message message)
	{
		if (!global::EACServer.connection2client.ContainsKey(message.connection))
		{
			Debug.LogError("EAC network packet from invalid connection: " + message.connection.userid);
			return;
		}
		EasyAntiCheat.Server.Hydra.Client client = global::EACServer.GetClient(message.connection);
		MemoryStream memoryStream = message.read.MemoryStreamWithSize();
		global::EACServer.easyAntiCheat.PushNetworkMessage(client, memoryStream.GetBuffer(), (int)memoryStream.Length);
	}

	// Token: 0x04001AA5 RID: 6821
	public static ICerberus playerTracker;

	// Token: 0x04001AA6 RID: 6822
	public static Scout eacScout;

	// Token: 0x04001AA7 RID: 6823
	private static Dictionary<EasyAntiCheat.Server.Hydra.Client, Connection> client2connection = new Dictionary<EasyAntiCheat.Server.Hydra.Client, Connection>();

	// Token: 0x04001AA8 RID: 6824
	private static Dictionary<Connection, EasyAntiCheat.Server.Hydra.Client> connection2client = new Dictionary<Connection, EasyAntiCheat.Server.Hydra.Client>();

	// Token: 0x04001AA9 RID: 6825
	private static Dictionary<Connection, ClientStatus> connection2status = new Dictionary<Connection, ClientStatus>();

	// Token: 0x04001AAA RID: 6826
	private static EasyAntiCheatServer<EasyAntiCheat.Server.Hydra.Client> easyAntiCheat = null;

	// Token: 0x04001AAB RID: 6827
	[CompilerGenerated]
	private static EasyAntiCheatServer<EasyAntiCheat.Server.Hydra.Client>.ClientStatusHandler <>f__mg$cache0;
}
