﻿using System;

// Token: 0x020006AA RID: 1706
public class GunModInformationPanel : global::ItemInformationPanel
{
	// Token: 0x04001CE8 RID: 7400
	public global::ItemTextValue fireRateDisplay;

	// Token: 0x04001CE9 RID: 7401
	public global::ItemTextValue velocityDisplay;

	// Token: 0x04001CEA RID: 7402
	public global::ItemTextValue damageDisplay;

	// Token: 0x04001CEB RID: 7403
	public global::ItemTextValue accuracyDisplay;

	// Token: 0x04001CEC RID: 7404
	public global::ItemTextValue recoilDisplay;

	// Token: 0x04001CED RID: 7405
	public global::ItemTextValue zoomDisplay;
}
