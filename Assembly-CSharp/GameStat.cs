﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000685 RID: 1669
public class GameStat : MonoBehaviour
{
	// Token: 0x04001C40 RID: 7232
	public float refreshTime = 5f;

	// Token: 0x04001C41 RID: 7233
	public Text title;

	// Token: 0x04001C42 RID: 7234
	public Text globalStat;

	// Token: 0x04001C43 RID: 7235
	public Text localStat;

	// Token: 0x04001C44 RID: 7236
	private long globalValue;

	// Token: 0x04001C45 RID: 7237
	private long localValue;

	// Token: 0x04001C46 RID: 7238
	private long oldGlobalValue;

	// Token: 0x04001C47 RID: 7239
	private long oldLocalValue;

	// Token: 0x04001C48 RID: 7240
	private float secondsSinceRefresh;

	// Token: 0x04001C49 RID: 7241
	private float secondsUntilUpdate;

	// Token: 0x04001C4A RID: 7242
	private float secondsUntilChange;

	// Token: 0x04001C4B RID: 7243
	public global::GameStat.Stat[] stats;

	// Token: 0x02000686 RID: 1670
	[Serializable]
	public struct Stat
	{
		// Token: 0x04001C4C RID: 7244
		public string statName;

		// Token: 0x04001C4D RID: 7245
		public string statTitle;
	}
}
