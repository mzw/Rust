﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Token: 0x0200066E RID: 1646
public class ConnectionScreen : SingletonComponent<global::ConnectionScreen>
{
	// Token: 0x04001C15 RID: 7189
	public Text statusText;

	// Token: 0x04001C16 RID: 7190
	public GameObject disconnectButton;

	// Token: 0x04001C17 RID: 7191
	public GameObject retryButton;

	// Token: 0x04001C18 RID: 7192
	public global::ServerBrowserInfo browserInfo;

	// Token: 0x04001C19 RID: 7193
	public UnityEvent onShowConnectionScreen = new UnityEvent();
}
