﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000060 RID: 96
public class CodeLock : global::BaseLock
{
	// Token: 0x0600075E RID: 1886 RVA: 0x0002EAA0 File Offset: 0x0002CCA0
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("CodeLock.OnRpcMessage", 0.1f))
		{
			if (rpc == 2845594679u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_ChangeCode ");
				}
				using (TimeWarning.New("RPC_ChangeCode", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_ChangeCode", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_ChangeCode(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_ChangeCode");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3250008422u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - TryLock ");
				}
				using (TimeWarning.New("TryLock", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("TryLock", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.TryLock(rpc3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in TryLock");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 1073883327u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - TryUnlock ");
				}
				using (TimeWarning.New("TryUnlock", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("TryUnlock", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.TryUnlock(rpc4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in TryUnlock");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 3183142647u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - UnlockWithCode ");
				}
				using (TimeWarning.New("UnlockWithCode", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("UnlockWithCode", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.UnlockWithCode(rpc5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in UnlockWithCode");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x0600075F RID: 1887 RVA: 0x0002F128 File Offset: 0x0002D328
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.codeLock != null)
		{
			this.hasCode = info.msg.codeLock.hasCode;
			this.hasGuestCode = info.msg.codeLock.hasGuestCode;
			if (info.msg.codeLock.pv != null)
			{
				this.code = info.msg.codeLock.pv.code;
				this.whitelistPlayers = info.msg.codeLock.pv.users;
				this.guestCode = info.msg.codeLock.pv.guestCode;
				this.guestPlayers = info.msg.codeLock.pv.guestUsers;
				if (this.guestCode == null || this.guestCode.Length != 4)
				{
					this.hasGuestCode = false;
					this.guestCode = string.Empty;
					this.guestPlayers.Clear();
				}
			}
		}
	}

	// Token: 0x06000760 RID: 1888 RVA: 0x0002F23C File Offset: 0x0002D43C
	internal void DoEffect(string effect)
	{
		global::Effect.server.Run(effect, this, 0u, Vector3.zero, Vector3.forward, null, false);
	}

	// Token: 0x06000761 RID: 1889 RVA: 0x0002F254 File Offset: 0x0002D454
	public override bool OnTryToOpen(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanUseLockedEntity", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		if (!base.IsLocked())
		{
			return true;
		}
		if (this.whitelistPlayers.Contains(player.userID) || this.guestPlayers.Contains(player.userID))
		{
			this.DoEffect(this.effectUnlocked.resourcePath);
			return true;
		}
		this.DoEffect(this.effectDenied.resourcePath);
		return false;
	}

	// Token: 0x06000762 RID: 1890 RVA: 0x0002F2EC File Offset: 0x0002D4EC
	public override bool OnTryToClose(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanUseLockedEntity", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		if (!base.IsLocked())
		{
			return true;
		}
		if (this.whitelistPlayers.Contains(player.userID) || this.guestPlayers.Contains(player.userID))
		{
			this.DoEffect(this.effectUnlocked.resourcePath);
			return true;
		}
		this.DoEffect(this.effectDenied.resourcePath);
		return false;
	}

	// Token: 0x06000763 RID: 1891 RVA: 0x0002F384 File Offset: 0x0002D584
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.codeLock = Facepunch.Pool.Get<ProtoBuf.CodeLock>();
		info.msg.codeLock.hasGuestCode = (this.guestCode.Length > 0);
		info.msg.codeLock.hasCode = (this.code.Length > 0);
		if (info.forDisk)
		{
			info.msg.codeLock.pv = Facepunch.Pool.Get<ProtoBuf.CodeLock.Private>();
			info.msg.codeLock.pv.code = this.code;
			info.msg.codeLock.pv.users = Facepunch.Pool.Get<List<ulong>>();
			info.msg.codeLock.pv.users.AddRange(this.whitelistPlayers);
			info.msg.codeLock.pv.guestCode = this.guestCode;
			info.msg.codeLock.pv.guestUsers = Facepunch.Pool.Get<List<ulong>>();
			info.msg.codeLock.pv.guestUsers.AddRange(this.guestPlayers);
		}
	}

	// Token: 0x06000764 RID: 1892 RVA: 0x0002F4BC File Offset: 0x0002D6BC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void RPC_ChangeCode(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		string text = rpc.read.String();
		bool flag = rpc.read.Bit();
		if (base.IsLocked())
		{
			return;
		}
		if (text.Length != 4)
		{
			return;
		}
		if (!this.hasCode && flag)
		{
			return;
		}
		if (!this.hasCode && !flag)
		{
			base.SetFlag(global::BaseEntity.Flags.Locked, true, false);
		}
		if (Interface.CallHook("CanChangeCode", new object[]
		{
			this,
			rpc.player,
			text,
			flag
		}) != null)
		{
			return;
		}
		if (!flag)
		{
			this.code = text;
			this.hasCode = (this.code.Length > 0);
			this.whitelistPlayers.Clear();
			this.whitelistPlayers.Add(rpc.player.userID);
		}
		else
		{
			this.guestCode = text;
			this.hasGuestCode = (this.guestCode.Length > 0);
			this.guestPlayers.Clear();
			this.guestPlayers.Add(rpc.player.userID);
		}
		this.DoEffect(this.effectCodeChanged.resourcePath);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000765 RID: 1893 RVA: 0x0002F608 File Offset: 0x0002D808
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void TryUnlock(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!base.IsLocked())
		{
			return;
		}
		if (Interface.CallHook("CanUnlock", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		if (this.whitelistPlayers.Contains(rpc.player.userID))
		{
			this.DoEffect(this.effectUnlocked.resourcePath);
			base.SetFlag(global::BaseEntity.Flags.Locked, false, false);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			return;
		}
		base.ClientRPCPlayer(null, rpc.player, "EnterUnlockCode");
	}

	// Token: 0x06000766 RID: 1894 RVA: 0x0002F6A8 File Offset: 0x0002D8A8
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void TryLock(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (base.IsLocked())
		{
			return;
		}
		if (this.code.Length != 4)
		{
			return;
		}
		if (Interface.CallHook("CanLock", new object[]
		{
			this,
			rpc.player
		}) != null)
		{
			return;
		}
		if (!this.whitelistPlayers.Contains(rpc.player.userID))
		{
			return;
		}
		this.DoEffect(this.effectLocked.resourcePath);
		base.SetFlag(global::BaseEntity.Flags.Locked, true, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000767 RID: 1895 RVA: 0x0002F748 File Offset: 0x0002D948
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void UnlockWithCode(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (!base.IsLocked())
		{
			return;
		}
		string text = rpc.read.String();
		if (Interface.CallHook("OnCodeEntered", new object[]
		{
			this,
			rpc.player,
			text
		}) != null)
		{
			return;
		}
		bool flag = text == this.guestCode;
		bool flag2 = text == this.code;
		if (!(text == this.code) && (!this.hasGuestCode || !(text == this.guestCode)))
		{
			if (UnityEngine.Time.realtimeSinceStartup > this.lastWrongTime + 10f)
			{
				this.wrongCodes = 0;
			}
			this.DoEffect(this.effectDenied.resourcePath);
			this.DoEffect(this.effectShock.resourcePath);
			rpc.player.Hurt((float)(this.wrongCodes + 1) * 5f, Rust.DamageType.ElectricShock, this, false);
			this.wrongCodes++;
			this.lastWrongTime = UnityEngine.Time.realtimeSinceStartup;
			return;
		}
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		if (flag2)
		{
			if (!this.whitelistPlayers.Contains(rpc.player.userID))
			{
				this.DoEffect(this.effectCodeChanged.resourcePath);
				this.whitelistPlayers.Add(rpc.player.userID);
			}
		}
		else if (flag && !this.guestPlayers.Contains(rpc.player.userID))
		{
			this.DoEffect(this.effectCodeChanged.resourcePath);
			this.guestPlayers.Add(rpc.player.userID);
		}
	}

	// Token: 0x04000356 RID: 854
	public GameObject keyEnterDialog;

	// Token: 0x04000357 RID: 855
	public global::GameObjectRef effectUnlocked;

	// Token: 0x04000358 RID: 856
	public global::GameObjectRef effectLocked;

	// Token: 0x04000359 RID: 857
	public global::GameObjectRef effectDenied;

	// Token: 0x0400035A RID: 858
	public global::GameObjectRef effectCodeChanged;

	// Token: 0x0400035B RID: 859
	public global::GameObjectRef effectShock;

	// Token: 0x0400035C RID: 860
	public bool hasCode;

	// Token: 0x0400035D RID: 861
	public bool hasGuestCode;

	// Token: 0x0400035E RID: 862
	public string code = string.Empty;

	// Token: 0x0400035F RID: 863
	public string guestCode = string.Empty;

	// Token: 0x04000360 RID: 864
	public List<ulong> whitelistPlayers = new List<ulong>();

	// Token: 0x04000361 RID: 865
	public List<ulong> guestPlayers = new List<ulong>();

	// Token: 0x04000362 RID: 866
	public int wrongCodes;

	// Token: 0x04000363 RID: 867
	public float lastWrongTime = float.NegativeInfinity;
}
