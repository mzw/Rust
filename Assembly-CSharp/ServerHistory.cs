﻿using System;
using Facepunch.Steamworks;
using UnityEngine;

// Token: 0x020006E5 RID: 1765
public class ServerHistory : MonoBehaviour
{
	// Token: 0x04001DFA RID: 7674
	public global::ServerHistoryItem prefab;

	// Token: 0x04001DFB RID: 7675
	public GameObject panelList;

	// Token: 0x04001DFC RID: 7676
	internal ServerList.Request Request;
}
