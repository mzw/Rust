﻿using System;
using UnityEngine;

// Token: 0x0200032D RID: 813
public class Sandstorm : MonoBehaviour
{
	// Token: 0x060013A7 RID: 5031 RVA: 0x00073784 File Offset: 0x00071984
	private void Start()
	{
	}

	// Token: 0x060013A8 RID: 5032 RVA: 0x00073788 File Offset: 0x00071988
	private void Update()
	{
		base.transform.RotateAround(base.transform.position, Vector3.up, Time.deltaTime * this.m_flSwirl);
		Vector3 eulerAngles = base.transform.eulerAngles;
		eulerAngles.x = -7f + Mathf.Sin(Time.time * 2.5f) * 7f;
		base.transform.eulerAngles = eulerAngles;
		if (this.m_psSandStorm != null)
		{
			this.m_psSandStorm.startSpeed = this.m_flSpeed;
			this.m_psSandStorm.startSpeed += Mathf.Sin(Time.time * 0.4f) * (this.m_flSpeed * 0.75f);
			this.m_psSandStorm.emissionRate = this.m_flEmissionRate + Mathf.Sin(Time.time * 1f) * (this.m_flEmissionRate * 0.3f);
		}
	}

	// Token: 0x04000E8E RID: 3726
	public ParticleSystem m_psSandStorm;

	// Token: 0x04000E8F RID: 3727
	public float m_flSpeed;

	// Token: 0x04000E90 RID: 3728
	public float m_flSwirl;

	// Token: 0x04000E91 RID: 3729
	public float m_flEmissionRate;
}
