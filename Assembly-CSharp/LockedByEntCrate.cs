﻿using System;
using UnityEngine;

// Token: 0x020003A5 RID: 933
public class LockedByEntCrate : global::LootContainer
{
	// Token: 0x060015FD RID: 5629 RVA: 0x0007EEF0 File Offset: 0x0007D0F0
	public void SetLockingEnt(GameObject ent)
	{
		base.CancelInvoke(new Action(this.Think));
		this.SetLocked(false);
		this.lockingEnt = ent;
		if (this.lockingEnt != null)
		{
			base.InvokeRepeating(new Action(this.Think), Random.Range(0f, 1f), 1f);
			this.SetLocked(true);
		}
	}

	// Token: 0x060015FE RID: 5630 RVA: 0x0007EF5C File Offset: 0x0007D15C
	public void SetLocked(bool isLocked)
	{
		base.SetFlag(global::BaseEntity.Flags.OnFire, isLocked, false);
		base.SetFlag(global::BaseEntity.Flags.Locked, isLocked, false);
	}

	// Token: 0x060015FF RID: 5631 RVA: 0x0007EF74 File Offset: 0x0007D174
	public void Think()
	{
		if (this.lockingEnt == null && base.IsLocked())
		{
			this.SetLockingEnt(null);
		}
	}

	// Token: 0x0400107D RID: 4221
	public GameObject lockingEnt;
}
