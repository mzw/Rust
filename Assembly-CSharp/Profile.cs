﻿using System;
using System.Diagnostics;
using UnityEngine;

// Token: 0x02000345 RID: 837
public class Profile
{
	// Token: 0x06001414 RID: 5140 RVA: 0x000755E0 File Offset: 0x000737E0
	public Profile(string cat, string nam, float WarnTime = 1f)
	{
		this.category = cat;
		this.name = nam;
		this.warnTime = WarnTime;
	}

	// Token: 0x06001415 RID: 5141 RVA: 0x00075608 File Offset: 0x00073808
	public void Start()
	{
		this.watch.Reset();
		this.watch.Start();
	}

	// Token: 0x06001416 RID: 5142 RVA: 0x00075620 File Offset: 0x00073820
	public void Stop()
	{
		this.watch.Stop();
		if ((float)this.watch.Elapsed.Seconds > this.warnTime)
		{
			Debug.Log(string.Concat(new object[]
			{
				this.category,
				".",
				this.name,
				": Took ",
				this.watch.Elapsed.Seconds,
				" seconds"
			}));
		}
	}

	// Token: 0x04000EE4 RID: 3812
	public Stopwatch watch = new Stopwatch();

	// Token: 0x04000EE5 RID: 3813
	public string category;

	// Token: 0x04000EE6 RID: 3814
	public string name;

	// Token: 0x04000EE7 RID: 3815
	public float warnTime;
}
