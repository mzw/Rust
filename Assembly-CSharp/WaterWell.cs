﻿using System;
using ConVar;
using Facepunch;
using Network;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000A8 RID: 168
public class WaterWell : global::LiquidContainer
{
	// Token: 0x06000A3D RID: 2621 RVA: 0x0004690C File Offset: 0x00044B0C
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("WaterWell.OnRpcMessage", 0.1f))
		{
			if (rpc == 358552706u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Pump ");
				}
				using (TimeWarning.New("RPC_Pump", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_Pump", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Pump(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_Pump");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000A3E RID: 2622 RVA: 0x00046AEC File Offset: 0x00044CEC
	public override void ServerInit()
	{
		base.ServerInit();
		base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
		base.SetFlag(global::BaseEntity.Flags.Reserved3, false, false);
	}

	// Token: 0x06000A3F RID: 2623 RVA: 0x00046B10 File Offset: 0x00044D10
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_Pump(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (player == null || player.IsDead() || player.IsSleeping())
		{
			return;
		}
		if (player.metabolism.calories.value < this.caloriesPerPump)
		{
			return;
		}
		if (base.HasFlag(global::BaseEntity.Flags.Reserved2))
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved2, true, false);
		player.metabolism.calories.value -= this.caloriesPerPump;
		player.metabolism.SendChangesToClient();
		this.currentPressure = Mathf.Clamp01(this.currentPressure + this.pressurePerPump);
		base.Invoke(new Action(this.StopPump), 1.8f);
		if (this.currentPressure >= 0f)
		{
			base.CancelInvoke(new Action(this.Produce));
			base.Invoke(new Action(this.Produce), 1f);
		}
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000A40 RID: 2624 RVA: 0x00046C1C File Offset: 0x00044E1C
	public void StopPump()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000A41 RID: 2625 RVA: 0x00046C34 File Offset: 0x00044E34
	protected override void OnInventoryDirty()
	{
		base.OnInventoryDirty();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000A42 RID: 2626 RVA: 0x00046C44 File Offset: 0x00044E44
	public void Produce()
	{
		this.inventory.AddItem(this.defaultLiquid, this.waterPerPump);
		base.SetFlag(global::BaseEntity.Flags.Reserved3, true, false);
		this.ScheduleTapOff();
		base.SendNetworkUpdateImmediate(false);
	}

	// Token: 0x06000A43 RID: 2627 RVA: 0x00046C78 File Offset: 0x00044E78
	public void ScheduleTapOff()
	{
		base.CancelInvoke(new Action(this.TapOff));
		base.Invoke(new Action(this.TapOff), 1f);
	}

	// Token: 0x06000A44 RID: 2628 RVA: 0x00046CA4 File Offset: 0x00044EA4
	private void TapOff()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved3, false, false);
	}

	// Token: 0x06000A45 RID: 2629 RVA: 0x00046CB4 File Offset: 0x00044EB4
	public void ReducePressure()
	{
		float num = Random.Range(0.1f, 0.2f);
		this.currentPressure = Mathf.Clamp01(this.currentPressure - num);
	}

	// Token: 0x06000A46 RID: 2630 RVA: 0x00046CE4 File Offset: 0x00044EE4
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.waterwell = Facepunch.Pool.Get<ProtoBuf.WaterWell>();
		info.msg.waterwell.pressure = this.currentPressure;
		info.msg.waterwell.waterLevel = this.GetWaterAmount();
	}

	// Token: 0x06000A47 RID: 2631 RVA: 0x00046D38 File Offset: 0x00044F38
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.waterwell != null)
		{
			this.currentPressure = info.msg.waterwell.pressure;
		}
	}

	// Token: 0x06000A48 RID: 2632 RVA: 0x00046D6C File Offset: 0x00044F6C
	public float GetWaterAmount()
	{
		if (!base.isServer)
		{
			return 0f;
		}
		global::Item slot = this.inventory.GetSlot(0);
		if (slot == null)
		{
			return 0f;
		}
		return (float)slot.amount;
	}

	// Token: 0x040004CF RID: 1231
	public Animator animator;

	// Token: 0x040004D0 RID: 1232
	private const global::BaseEntity.Flags Pumping = global::BaseEntity.Flags.Reserved2;

	// Token: 0x040004D1 RID: 1233
	private const global::BaseEntity.Flags WaterFlow = global::BaseEntity.Flags.Reserved3;

	// Token: 0x040004D2 RID: 1234
	public float caloriesPerPump = 5f;

	// Token: 0x040004D3 RID: 1235
	public float pressurePerPump = 0.2f;

	// Token: 0x040004D4 RID: 1236
	public float pressureForProduction = 1f;

	// Token: 0x040004D5 RID: 1237
	public float currentPressure;

	// Token: 0x040004D6 RID: 1238
	public int waterPerPump = 50;

	// Token: 0x040004D7 RID: 1239
	public GameObject waterLevelObj;

	// Token: 0x040004D8 RID: 1240
	public float waterLevelObjFullOffset;
}
