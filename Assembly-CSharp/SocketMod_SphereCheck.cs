﻿using System;
using UnityEngine;

// Token: 0x0200022D RID: 557
public class SocketMod_SphereCheck : global::SocketMod
{
	// Token: 0x06000FFC RID: 4092 RVA: 0x00061478 File Offset: 0x0005F678
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = ((!this.wantsCollide) ? new Color(1f, 0f, 0f, 0.7f) : new Color(0f, 1f, 0f, 0.7f));
		Gizmos.DrawSphere(Vector3.zero, this.sphereRadius);
	}

	// Token: 0x06000FFD RID: 4093 RVA: 0x000614EC File Offset: 0x0005F6EC
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 position = place.position + place.rotation * this.worldPosition;
		bool flag = this.wantsCollide == global::GamePhysics.CheckSphere(position, this.sphereRadius, this.layerMask.value, 0);
		if (flag)
		{
			return true;
		}
		global::Construction.lastPlacementError = "Failed Check: Sphere Test (" + this.hierachyName + ")";
		return false;
	}

	// Token: 0x04000AB2 RID: 2738
	public float sphereRadius = 1f;

	// Token: 0x04000AB3 RID: 2739
	public LayerMask layerMask;

	// Token: 0x04000AB4 RID: 2740
	public bool wantsCollide;
}
