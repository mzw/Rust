﻿using System;

// Token: 0x02000431 RID: 1073
public static class HitAreaConst
{
	// Token: 0x04001320 RID: 4896
	public const global::HitArea Nothing = (global::HitArea)0;

	// Token: 0x04001321 RID: 4897
	public const global::HitArea Everything = (global::HitArea)-1;
}
