﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

// Token: 0x02000422 RID: 1058
[CreateAssetMenu(menuName = "Rust/Foliage Placement")]
public class FoliagePlacement : ScriptableObject
{
	// Token: 0x040012AA RID: 4778
	[Header("Placement")]
	public float Density = 2f;

	// Token: 0x040012AB RID: 4779
	[Header("Filter")]
	public global::SpawnFilter Filter;

	// Token: 0x040012AC RID: 4780
	[FormerlySerializedAs("Cutoff")]
	public float FilterCutoff = 0.5f;

	// Token: 0x040012AD RID: 4781
	public float FilterFade = 0.1f;

	// Token: 0x040012AE RID: 4782
	[FormerlySerializedAs("Scaling")]
	public float FilterScaling = 1f;

	// Token: 0x040012AF RID: 4783
	[Header("Randomization")]
	public float RandomScaling = 0.2f;

	// Token: 0x040012B0 RID: 4784
	[Header("Placement Range")]
	[global::MinMax(0f, 1f)]
	public global::MinMax Range = new global::MinMax(0f, 1f);

	// Token: 0x040012B1 RID: 4785
	public float RangeFade = 0.1f;

	// Token: 0x040012B2 RID: 4786
	[Header("LOD")]
	[Range(0f, 1f)]
	public float DistanceDensity;

	// Token: 0x040012B3 RID: 4787
	[Range(1f, 2f)]
	public float DistanceScaling = 2f;

	// Token: 0x040012B4 RID: 4788
	[Header("Visuals")]
	public Material material;

	// Token: 0x040012B5 RID: 4789
	public Mesh mesh;

	// Token: 0x040012B6 RID: 4790
	public const int octaves = 1;

	// Token: 0x040012B7 RID: 4791
	public const float frequency = 0.05f;

	// Token: 0x040012B8 RID: 4792
	public const float amplitude = 0.5f;

	// Token: 0x040012B9 RID: 4793
	public const float offset = 0.5f;
}
