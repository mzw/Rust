﻿using System;
using UnityEngine;

// Token: 0x020005D7 RID: 1495
public class TerrainTopologyAdd : global::TerrainModifier
{
	// Token: 0x06001EC8 RID: 7880 RVA: 0x000AD904 File Offset: 0x000ABB04
	protected override void Apply(Vector3 position, float opacity, float radius, float fade)
	{
		if (!global::TerrainMeta.TopologyMap)
		{
			return;
		}
		global::TerrainMeta.TopologyMap.AddTopology(position, (int)this.TopologyType, radius, fade);
	}

	// Token: 0x0400199D RID: 6557
	[InspectorFlags]
	public global::TerrainTopology.Enum TopologyType = global::TerrainTopology.Enum.Decor;
}
