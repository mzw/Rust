﻿using System;
using UnityEngine;

// Token: 0x02000326 RID: 806
public class MaterialSwap : MonoBehaviour, IClientComponent
{
	// Token: 0x04000E6E RID: 3694
	public int materialIndex;

	// Token: 0x04000E6F RID: 3695
	public Renderer myRenderer;

	// Token: 0x04000E70 RID: 3696
	public Material OverrideMaterial;
}
