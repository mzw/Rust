﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x020001DD RID: 477
public class GranularAudioClip : MonoBehaviour
{
	// Token: 0x06000F0D RID: 3853 RVA: 0x0005CB58 File Offset: 0x0005AD58
	private void Update()
	{
		if (!this.inited && this.sourceClip.loadState == 2)
		{
			this.sampleRate = this.sourceClip.frequency;
			this.sourceAudioData = new float[this.sourceClip.samples * this.sourceClip.channels];
			this.sourceClip.GetData(this.sourceAudioData, 0);
			this.InitAudioClip();
			AudioSource component = base.GetComponent<AudioSource>();
			component.clip = this.granularClip;
			component.loop = true;
			component.Play();
			this.inited = true;
		}
		this.RefreshCachedData();
	}

	// Token: 0x06000F0E RID: 3854 RVA: 0x0005CBFC File Offset: 0x0005ADFC
	private void RefreshCachedData()
	{
		this.grainAttackSamples = Mathf.FloorToInt(this.grainAttack * (float)this.sampleRate * (float)this.sourceChannels);
		this.grainSustainSamples = Mathf.FloorToInt(this.grainSustain * (float)this.sampleRate * (float)this.sourceChannels);
		this.grainReleaseSamples = Mathf.FloorToInt(this.grainRelease * (float)this.sampleRate * (float)this.sourceChannels);
		this.grainFrequencySamples = Mathf.FloorToInt(this.grainFrequency * (float)this.sampleRate * (float)this.sourceChannels);
	}

	// Token: 0x06000F0F RID: 3855 RVA: 0x0005CC90 File Offset: 0x0005AE90
	private void InitAudioClip()
	{
		int num = 1;
		int num2 = 1;
		UnityEngine.AudioSettings.GetDSPBufferSize(ref num, ref num2);
		this.granularClip = AudioClip.Create(this.sourceClip.name + " (granular)", num, this.sourceClip.channels, this.sampleRate, true, new AudioClip.PCMReaderCallback(this.OnAudioRead));
		this.sourceChannels = this.sourceClip.channels;
	}

	// Token: 0x06000F10 RID: 3856 RVA: 0x0005CCFC File Offset: 0x0005AEFC
	private void OnAudioRead(float[] data)
	{
		for (int i = 0; i < data.Length; i++)
		{
			if (this.samplesUntilNextGrain <= 0)
			{
				this.SpawnGrain();
			}
			float num = 0f;
			for (int j = 0; j < this.grains.Count; j++)
			{
				num += this.grains[j].GetSample();
			}
			data[i] = num;
			this.samplesUntilNextGrain--;
		}
		this.CleanupFinishedGrains();
	}

	// Token: 0x06000F11 RID: 3857 RVA: 0x0005CD80 File Offset: 0x0005AF80
	private void SpawnGrain()
	{
		if (this.grainFrequencySamples == 0)
		{
			return;
		}
		float num = (float)(this.random.NextDouble() * (double)this.sourceTimeVariation * 2.0) - this.sourceTimeVariation;
		float num2 = this.sourceTime + num;
		int start = Mathf.FloorToInt(num2 * (float)this.sampleRate / (float)this.sourceChannels);
		global::GranularAudioClip.Grain grain = Pool.Get<global::GranularAudioClip.Grain>();
		grain.Init(this.sourceAudioData, start, this.grainAttackSamples, this.grainSustainSamples, this.grainReleaseSamples);
		this.grains.Add(grain);
		this.samplesUntilNextGrain = this.grainFrequencySamples;
	}

	// Token: 0x06000F12 RID: 3858 RVA: 0x0005CE1C File Offset: 0x0005B01C
	private void CleanupFinishedGrains()
	{
		for (int i = this.grains.Count - 1; i >= 0; i--)
		{
			global::GranularAudioClip.Grain grain = this.grains[i];
			if (grain.finished)
			{
				Pool.Free<global::GranularAudioClip.Grain>(ref grain);
				this.grains.RemoveAt(i);
			}
		}
	}

	// Token: 0x04000932 RID: 2354
	public AudioClip sourceClip;

	// Token: 0x04000933 RID: 2355
	private float[] sourceAudioData;

	// Token: 0x04000934 RID: 2356
	private int sourceChannels = 1;

	// Token: 0x04000935 RID: 2357
	public AudioClip granularClip;

	// Token: 0x04000936 RID: 2358
	public int sampleRate = 44100;

	// Token: 0x04000937 RID: 2359
	public float sourceTime = 0.5f;

	// Token: 0x04000938 RID: 2360
	public float sourceTimeVariation = 0.1f;

	// Token: 0x04000939 RID: 2361
	public float grainAttack = 0.1f;

	// Token: 0x0400093A RID: 2362
	public float grainSustain = 0.1f;

	// Token: 0x0400093B RID: 2363
	public float grainRelease = 0.1f;

	// Token: 0x0400093C RID: 2364
	public float grainFrequency = 0.1f;

	// Token: 0x0400093D RID: 2365
	public int grainAttackSamples;

	// Token: 0x0400093E RID: 2366
	public int grainSustainSamples;

	// Token: 0x0400093F RID: 2367
	public int grainReleaseSamples;

	// Token: 0x04000940 RID: 2368
	public int grainFrequencySamples;

	// Token: 0x04000941 RID: 2369
	public int samplesUntilNextGrain;

	// Token: 0x04000942 RID: 2370
	public List<global::GranularAudioClip.Grain> grains = new List<global::GranularAudioClip.Grain>();

	// Token: 0x04000943 RID: 2371
	private Random random = new Random();

	// Token: 0x04000944 RID: 2372
	private bool inited;

	// Token: 0x020001DE RID: 478
	public class Grain
	{
		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000F14 RID: 3860 RVA: 0x0005CE7C File Offset: 0x0005B07C
		public bool finished
		{
			get
			{
				return this.currentSample >= this.endSample;
			}
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x0005CE90 File Offset: 0x0005B090
		public void Init(float[] source, int start, int attack, int sustain, int release)
		{
			this.sourceData = source;
			this.sourceDataLength = this.sourceData.Length;
			this.startSample = start;
			this.currentSample = start;
			this.attackTimeSamples = attack;
			this.sustainTimeSamples = sustain;
			this.releaseTimeSamples = release;
			this.gainPerSampleAttack = 1f / (float)this.attackTimeSamples;
			this.gainPerSampleRelease = -1f / (float)this.releaseTimeSamples;
			this.attackEndSample = this.startSample + this.attackTimeSamples;
			this.releaseStartSample = this.attackEndSample + this.sustainTimeSamples;
			this.endSample = this.releaseStartSample + this.releaseTimeSamples;
			this.gain = 0f;
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x0005CF44 File Offset: 0x0005B144
		public float GetSample()
		{
			int num = this.currentSample % this.sourceDataLength;
			if (num < 0)
			{
				num += this.sourceDataLength;
			}
			float num2 = this.sourceData[num];
			if (this.currentSample <= this.attackEndSample)
			{
				this.gain += this.gainPerSampleAttack;
			}
			else if (this.currentSample >= this.releaseStartSample)
			{
				this.gain += this.gainPerSampleRelease;
			}
			this.currentSample++;
			return num2 * this.gain;
		}

		// Token: 0x04000945 RID: 2373
		private float[] sourceData;

		// Token: 0x04000946 RID: 2374
		private int sourceDataLength;

		// Token: 0x04000947 RID: 2375
		private int startSample;

		// Token: 0x04000948 RID: 2376
		private int currentSample;

		// Token: 0x04000949 RID: 2377
		private int attackTimeSamples;

		// Token: 0x0400094A RID: 2378
		private int sustainTimeSamples;

		// Token: 0x0400094B RID: 2379
		private int releaseTimeSamples;

		// Token: 0x0400094C RID: 2380
		private float gain;

		// Token: 0x0400094D RID: 2381
		private float gainPerSampleAttack;

		// Token: 0x0400094E RID: 2382
		private float gainPerSampleRelease;

		// Token: 0x0400094F RID: 2383
		private int attackEndSample;

		// Token: 0x04000950 RID: 2384
		private int releaseStartSample;

		// Token: 0x04000951 RID: 2385
		private int endSample;
	}
}
