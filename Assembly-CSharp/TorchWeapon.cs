﻿using System;
using ConVar;
using Network;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000A2 RID: 162
public class TorchWeapon : global::BaseMelee
{
	// Token: 0x060009F3 RID: 2547 RVA: 0x00043C10 File Offset: 0x00041E10
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("TorchWeapon.OnRpcMessage", 0.1f))
		{
			if (rpc == 1729863050u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Extinguish ");
				}
				using (TimeWarning.New("Extinguish", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("Extinguish", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Extinguish(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in Extinguish");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3104565130u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - Ignite ");
				}
				using (TimeWarning.New("Ignite", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("Ignite", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.Ignite(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in Ignite");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009F4 RID: 2548 RVA: 0x00043F74 File Offset: 0x00042174
	public override void GetAttackStats(global::HitInfo info)
	{
		base.GetAttackStats(info);
		if (base.HasFlag(global::BaseEntity.Flags.On))
		{
			info.damageTypes.Add(Rust.DamageType.Heat, 1f);
		}
	}

	// Token: 0x060009F5 RID: 2549 RVA: 0x00043F9C File Offset: 0x0004219C
	public override float GetConditionLoss()
	{
		return base.GetConditionLoss() + ((!base.HasFlag(global::BaseEntity.Flags.On)) ? 0f : 6f);
	}

	// Token: 0x060009F6 RID: 2550 RVA: 0x00043FC0 File Offset: 0x000421C0
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void Ignite(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.On, true, false);
		base.InvokeRepeating(new Action(this.UseFuel), 1f, 1f);
	}

	// Token: 0x060009F7 RID: 2551 RVA: 0x00043FFC File Offset: 0x000421FC
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void Extinguish(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.On, false, false);
		base.CancelInvoke(new Action(this.UseFuel));
	}

	// Token: 0x060009F8 RID: 2552 RVA: 0x0004402C File Offset: 0x0004222C
	public void UseFuel()
	{
		global::Item ownerItem = base.GetOwnerItem();
		if (ownerItem == null)
		{
			return;
		}
		ownerItem.LoseCondition(this.fuelTickAmount);
	}

	// Token: 0x060009F9 RID: 2553 RVA: 0x00044054 File Offset: 0x00042254
	public override void OnHeldChanged()
	{
		if (base.IsDisabled())
		{
			base.SetFlag(global::BaseEntity.Flags.On, false, false);
			base.CancelInvoke(new Action(this.UseFuel));
		}
	}

	// Token: 0x040004A0 RID: 1184
	[NonSerialized]
	public float fuelTickAmount = 0.166666672f;

	// Token: 0x040004A1 RID: 1185
	[Header("TorchWeapon")]
	public AnimatorOverrideController LitHoldAnimationOverride;
}
