﻿using System;
using UnityEngine;

// Token: 0x02000221 RID: 545
public class Socket_Terrain : global::Socket_Base
{
	// Token: 0x06000FD5 RID: 4053 RVA: 0x000607C8 File Offset: 0x0005E9C8
	private void OnDrawGizmos()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = Color.red;
		Gizmos.DrawLine(Vector3.zero, Vector3.forward * 0.2f);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(Vector3.zero, Vector3.right * 0.1f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(Vector3.zero, Vector3.up * 0.1f);
		Gizmos.color = new Color(0f, 1f, 0f, 0.2f);
		Gizmos.DrawCube(Vector3.zero, new Vector3(0.1f, 0.1f, this.placementHeight));
		Gizmos.color = new Color(0f, 1f, 0f, 0.5f);
		Gizmos.DrawWireCube(Vector3.zero, new Vector3(0.1f, 0.1f, this.placementHeight));
		Gizmos.DrawIcon(base.transform.position, "light_circle_green.png", false);
	}

	// Token: 0x06000FD6 RID: 4054 RVA: 0x000608E0 File Offset: 0x0005EAE0
	public override bool TestTarget(global::Construction.Target target)
	{
		return target.onTerrain;
	}

	// Token: 0x06000FD7 RID: 4055 RVA: 0x000608EC File Offset: 0x0005EAEC
	public override global::Construction.Placement DoPlacement(global::Construction.Target target)
	{
		Vector3 eulerAngles = this.rotation.eulerAngles;
		eulerAngles.x = 0f;
		eulerAngles.z = 0f;
		Vector3 direction = target.ray.direction;
		direction.y = 0f;
		direction.Normalize();
		Vector3 vector = Vector3.up;
		if (this.alignToNormal)
		{
			vector = target.normal;
		}
		Quaternion quaternion = Quaternion.Euler(target.rotation) * Quaternion.LookRotation(direction, vector) * Quaternion.Euler(0f, eulerAngles.y, 0f);
		Vector3 vector2 = target.position;
		vector2 -= quaternion * this.position;
		return new global::Construction.Placement
		{
			rotation = quaternion,
			position = vector2
		};
	}

	// Token: 0x04000A95 RID: 2709
	public float placementHeight;

	// Token: 0x04000A96 RID: 2710
	public bool alignToNormal;
}
