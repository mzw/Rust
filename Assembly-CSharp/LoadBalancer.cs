﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Rust;
using UnityEngine;

// Token: 0x02000747 RID: 1863
public class LoadBalancer : SingletonComponent<global::LoadBalancer>
{
	// Token: 0x060022ED RID: 8941 RVA: 0x000C2218 File Offset: 0x000C0418
	protected void LateUpdate()
	{
		if (Application.isReceiving)
		{
			return;
		}
		if (Application.isLoading)
		{
			return;
		}
		if (global::LoadBalancer.Paused)
		{
			return;
		}
		int num = global::LoadBalancer.Count();
		float num2 = Mathf.InverseLerp(1000f, 100000f, (float)num);
		float num3 = Mathf.SmoothStep(1f, 100f, num2);
		this.watch.Reset();
		this.watch.Start();
		for (int i = 0; i < this.queues.Length; i++)
		{
			Queue<global::DeferredAction> queue = this.queues[i];
			while (queue.Count > 0)
			{
				global::DeferredAction deferredAction = queue.Dequeue();
				deferredAction.Action();
				if (this.watch.Elapsed.TotalMilliseconds > (double)num3)
				{
					return;
				}
			}
		}
	}

	// Token: 0x060022EE RID: 8942 RVA: 0x000C22E8 File Offset: 0x000C04E8
	public static int Count()
	{
		if (!SingletonComponent<global::LoadBalancer>.Instance)
		{
			return 0;
		}
		Queue<global::DeferredAction>[] array = SingletonComponent<global::LoadBalancer>.Instance.queues;
		int num = 0;
		for (int i = 0; i < array.Length; i++)
		{
			num += array[i].Count;
		}
		return num;
	}

	// Token: 0x060022EF RID: 8943 RVA: 0x000C2334 File Offset: 0x000C0534
	public static void ProcessAll()
	{
		if (!SingletonComponent<global::LoadBalancer>.Instance)
		{
			global::LoadBalancer.CreateInstance();
		}
		foreach (Queue<global::DeferredAction> queue in SingletonComponent<global::LoadBalancer>.Instance.queues)
		{
			while (queue.Count > 0)
			{
				global::DeferredAction deferredAction = queue.Dequeue();
				deferredAction.Action();
			}
		}
	}

	// Token: 0x060022F0 RID: 8944 RVA: 0x000C2398 File Offset: 0x000C0598
	public static void Enqueue(global::DeferredAction action)
	{
		if (!SingletonComponent<global::LoadBalancer>.Instance)
		{
			global::LoadBalancer.CreateInstance();
		}
		Queue<global::DeferredAction>[] array = SingletonComponent<global::LoadBalancer>.Instance.queues;
		Queue<global::DeferredAction> queue = array[action.Index];
		queue.Enqueue(action);
	}

	// Token: 0x060022F1 RID: 8945 RVA: 0x000C23D4 File Offset: 0x000C05D4
	private static void CreateInstance()
	{
		GameObject gameObject = new GameObject();
		gameObject.name = "LoadBalancer";
		gameObject.AddComponent<global::LoadBalancer>();
		Object.DontDestroyOnLoad(gameObject);
	}

	// Token: 0x04001F58 RID: 8024
	public static bool Paused;

	// Token: 0x04001F59 RID: 8025
	private const float MinMilliseconds = 1f;

	// Token: 0x04001F5A RID: 8026
	private const float MaxMilliseconds = 100f;

	// Token: 0x04001F5B RID: 8027
	private const int MinBacklog = 1000;

	// Token: 0x04001F5C RID: 8028
	private const int MaxBacklog = 100000;

	// Token: 0x04001F5D RID: 8029
	private Queue<global::DeferredAction>[] queues = new Queue<global::DeferredAction>[]
	{
		new Queue<global::DeferredAction>(),
		new Queue<global::DeferredAction>(),
		new Queue<global::DeferredAction>(),
		new Queue<global::DeferredAction>(),
		new Queue<global::DeferredAction>()
	};

	// Token: 0x04001F5E RID: 8030
	private Stopwatch watch = Stopwatch.StartNew();
}
