﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200079C RID: 1948
public class PathFinder
{
	// Token: 0x06002430 RID: 9264 RVA: 0x000C7824 File Offset: 0x000C5A24
	public PathFinder(int[,] costmap, bool diagonals = true)
	{
		this.costmap = costmap;
		this.neighbors = ((!diagonals) ? global::PathFinder.neumannNeighbors : global::PathFinder.mooreNeighbors);
	}

	// Token: 0x06002431 RID: 9265 RVA: 0x000C7850 File Offset: 0x000C5A50
	public global::PathFinder.Node FindPath(global::PathFinder.Point start, global::PathFinder.Point end, int depth = 2147483647)
	{
		return this.FindPathReversed(end, start, depth);
	}

	// Token: 0x06002432 RID: 9266 RVA: 0x000C785C File Offset: 0x000C5A5C
	private global::PathFinder.Node FindPathReversed(global::PathFinder.Point start, global::PathFinder.Point end, int depth = 2147483647)
	{
		if (this.visited == null)
		{
			this.visited = new bool[this.costmap.GetLength(0), this.costmap.GetLength(1)];
		}
		else
		{
			Array.Clear(this.visited, 0, this.visited.Length);
		}
		int num = 0;
		int num2 = this.costmap.GetLength(0) - 1;
		int num3 = 0;
		int num4 = this.costmap.GetLength(1) - 1;
		IntrusiveMinHeap<global::PathFinder.Node> intrusiveMinHeap = default(IntrusiveMinHeap<global::PathFinder.Node>);
		int cost = this.costmap[start.y, start.x];
		int heuristic = this.Heuristic(start, end);
		intrusiveMinHeap.Add(new global::PathFinder.Node(start, cost, heuristic, null));
		this.visited[start.y, start.x] = true;
		while (!intrusiveMinHeap.Empty && depth-- > 0)
		{
			global::PathFinder.Node node = intrusiveMinHeap.Pop();
			if (node.heuristic == 0)
			{
				return node;
			}
			for (int i = 0; i < this.neighbors.Length; i++)
			{
				global::PathFinder.Point point = node.point + this.neighbors[i];
				if (point.x >= num && point.x <= num2 && point.y >= num3 && point.y <= num4 && !this.visited[point.y, point.x])
				{
					this.visited[point.y, point.x] = true;
					int num5 = this.costmap[point.y, point.x];
					if (num5 != 2147483647)
					{
						int cost2 = node.cost + num5;
						int heuristic2 = this.Heuristic(point, end);
						intrusiveMinHeap.Add(new global::PathFinder.Node(point, cost2, heuristic2, node));
					}
				}
			}
		}
		return null;
	}

	// Token: 0x06002433 RID: 9267 RVA: 0x000C7A60 File Offset: 0x000C5C60
	public global::PathFinder.Node FindPathDirected(List<global::PathFinder.Point> startList, List<global::PathFinder.Point> endList, int depth = 2147483647)
	{
		return this.FindPathReversed(endList, startList, depth);
	}

	// Token: 0x06002434 RID: 9268 RVA: 0x000C7A6C File Offset: 0x000C5C6C
	public global::PathFinder.Node FindPathUndirected(List<global::PathFinder.Point> startList, List<global::PathFinder.Point> endList, int depth = 2147483647)
	{
		if (startList.Count > endList.Count)
		{
			return this.FindPathReversed(endList, startList, depth);
		}
		return this.FindPathReversed(startList, endList, depth);
	}

	// Token: 0x06002435 RID: 9269 RVA: 0x000C7A94 File Offset: 0x000C5C94
	private global::PathFinder.Node FindPathReversed(List<global::PathFinder.Point> startList, List<global::PathFinder.Point> endList, int depth = 2147483647)
	{
		if (this.visited == null)
		{
			this.visited = new bool[this.costmap.GetLength(0), this.costmap.GetLength(1)];
		}
		else
		{
			Array.Clear(this.visited, 0, this.visited.Length);
		}
		int num = 0;
		int num2 = this.costmap.GetLength(0) - 1;
		int num3 = 0;
		int num4 = this.costmap.GetLength(1) - 1;
		IntrusiveMinHeap<global::PathFinder.Node> intrusiveMinHeap = default(IntrusiveMinHeap<global::PathFinder.Node>);
		foreach (global::PathFinder.Point point in startList)
		{
			int cost = this.costmap[point.y, point.x];
			int heuristic = this.Heuristic(point, endList);
			intrusiveMinHeap.Add(new global::PathFinder.Node(point, cost, heuristic, null));
			this.visited[point.y, point.x] = true;
		}
		while (!intrusiveMinHeap.Empty && depth-- > 0)
		{
			global::PathFinder.Node node = intrusiveMinHeap.Pop();
			if (node.heuristic == 0)
			{
				return node;
			}
			for (int i = 0; i < this.neighbors.Length; i++)
			{
				global::PathFinder.Point point2 = node.point + this.neighbors[i];
				if (point2.x >= num && point2.x <= num2 && point2.y >= num3 && point2.y <= num4 && !this.visited[point2.y, point2.x])
				{
					this.visited[point2.y, point2.x] = true;
					int num5 = this.costmap[point2.y, point2.x];
					if (num5 != 2147483647)
					{
						int cost2 = node.cost + num5;
						int heuristic2 = this.Heuristic(point2, endList);
						intrusiveMinHeap.Add(new global::PathFinder.Node(point2, cost2, heuristic2, node));
					}
				}
			}
		}
		return null;
	}

	// Token: 0x06002436 RID: 9270 RVA: 0x000C7CE0 File Offset: 0x000C5EE0
	public global::PathFinder.Node FindClosestWalkable(global::PathFinder.Point start, int depth = 2147483647)
	{
		if (this.visited == null)
		{
			this.visited = new bool[this.costmap.GetLength(0), this.costmap.GetLength(1)];
		}
		else
		{
			Array.Clear(this.visited, 0, this.visited.Length);
		}
		int num = 0;
		int num2 = this.costmap.GetLength(0) - 1;
		int num3 = 0;
		int num4 = this.costmap.GetLength(1) - 1;
		IntrusiveMinHeap<global::PathFinder.Node> intrusiveMinHeap = default(IntrusiveMinHeap<global::PathFinder.Node>);
		int cost = 1;
		int heuristic = this.Heuristic(start);
		intrusiveMinHeap.Add(new global::PathFinder.Node(start, cost, heuristic, null));
		this.visited[start.y, start.x] = true;
		while (!intrusiveMinHeap.Empty && depth-- > 0)
		{
			global::PathFinder.Node node = intrusiveMinHeap.Pop();
			if (node.heuristic == 0)
			{
				return node;
			}
			for (int i = 0; i < this.neighbors.Length; i++)
			{
				global::PathFinder.Point point = node.point + this.neighbors[i];
				if (point.x >= num && point.x <= num2 && point.y >= num3 && point.y <= num4 && !this.visited[point.y, point.x])
				{
					this.visited[point.y, point.x] = true;
					int cost2 = node.cost + 1;
					int heuristic2 = this.Heuristic(point);
					intrusiveMinHeap.Add(new global::PathFinder.Node(point, cost2, heuristic2, node));
				}
			}
		}
		return null;
	}

	// Token: 0x06002437 RID: 9271 RVA: 0x000C7EA4 File Offset: 0x000C60A4
	public global::PathFinder.Node Reverse(global::PathFinder.Node start)
	{
		global::PathFinder.Node node = null;
		global::PathFinder.Node next = null;
		for (global::PathFinder.Node node2 = start; node2 != null; node2 = node2.next)
		{
			if (node != null)
			{
				node.next = next;
			}
			next = node;
			node = node2;
		}
		if (node != null)
		{
			node.next = next;
		}
		return node;
	}

	// Token: 0x06002438 RID: 9272 RVA: 0x000C7EE8 File Offset: 0x000C60E8
	public global::PathFinder.Node FindEnd(global::PathFinder.Node start)
	{
		for (global::PathFinder.Node node = start; node != null; node = node.next)
		{
			if (node.next == null)
			{
				return node;
			}
		}
		return start;
	}

	// Token: 0x06002439 RID: 9273 RVA: 0x000C7F18 File Offset: 0x000C6118
	public int Heuristic(global::PathFinder.Point a)
	{
		return (this.costmap[a.y, a.x] != int.MaxValue) ? 0 : 1;
	}

	// Token: 0x0600243A RID: 9274 RVA: 0x000C7F44 File Offset: 0x000C6144
	public int Heuristic(global::PathFinder.Point a, global::PathFinder.Point b)
	{
		int num = a.x - b.x;
		int num2 = a.y - b.y;
		return num * num + num2 * num2;
	}

	// Token: 0x0600243B RID: 9275 RVA: 0x000C7F78 File Offset: 0x000C6178
	public int Heuristic(global::PathFinder.Point a, List<global::PathFinder.Point> b)
	{
		int num = int.MaxValue;
		for (int i = 0; i < b.Count; i++)
		{
			num = Mathf.Min(num, this.Heuristic(a, b[i]));
		}
		return num;
	}

	// Token: 0x04001FCB RID: 8139
	private int[,] costmap;

	// Token: 0x04001FCC RID: 8140
	private bool[,] visited;

	// Token: 0x04001FCD RID: 8141
	private global::PathFinder.Point[] neighbors;

	// Token: 0x04001FCE RID: 8142
	private static global::PathFinder.Point[] mooreNeighbors = new global::PathFinder.Point[]
	{
		new global::PathFinder.Point(0, 1),
		new global::PathFinder.Point(-1, 0),
		new global::PathFinder.Point(1, 0),
		new global::PathFinder.Point(0, -1),
		new global::PathFinder.Point(-1, 1),
		new global::PathFinder.Point(1, 1),
		new global::PathFinder.Point(-1, -1),
		new global::PathFinder.Point(1, -1)
	};

	// Token: 0x04001FCF RID: 8143
	private static global::PathFinder.Point[] neumannNeighbors = new global::PathFinder.Point[]
	{
		new global::PathFinder.Point(0, 1),
		new global::PathFinder.Point(-1, 0),
		new global::PathFinder.Point(1, 0),
		new global::PathFinder.Point(0, -1)
	};

	// Token: 0x0200079D RID: 1949
	public struct Point : IEquatable<global::PathFinder.Point>
	{
		// Token: 0x0600243D RID: 9277 RVA: 0x000C80C0 File Offset: 0x000C62C0
		public Point(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		// Token: 0x0600243E RID: 9278 RVA: 0x000C80D0 File Offset: 0x000C62D0
		public static global::PathFinder.Point operator +(global::PathFinder.Point a, global::PathFinder.Point b)
		{
			return new global::PathFinder.Point(a.x + b.x, a.y + b.y);
		}

		// Token: 0x0600243F RID: 9279 RVA: 0x000C80F8 File Offset: 0x000C62F8
		public static global::PathFinder.Point operator -(global::PathFinder.Point a, global::PathFinder.Point b)
		{
			return new global::PathFinder.Point(a.x - b.x, a.y - b.y);
		}

		// Token: 0x06002440 RID: 9280 RVA: 0x000C8120 File Offset: 0x000C6320
		public static global::PathFinder.Point operator *(global::PathFinder.Point p, int i)
		{
			return new global::PathFinder.Point(p.x * i, p.y * i);
		}

		// Token: 0x06002441 RID: 9281 RVA: 0x000C813C File Offset: 0x000C633C
		public static global::PathFinder.Point operator /(global::PathFinder.Point p, int i)
		{
			return new global::PathFinder.Point(p.x / i, p.y / i);
		}

		// Token: 0x06002442 RID: 9282 RVA: 0x000C8158 File Offset: 0x000C6358
		public static bool operator ==(global::PathFinder.Point a, global::PathFinder.Point b)
		{
			return a.Equals(b);
		}

		// Token: 0x06002443 RID: 9283 RVA: 0x000C8164 File Offset: 0x000C6364
		public static bool operator !=(global::PathFinder.Point a, global::PathFinder.Point b)
		{
			return !a.Equals(b);
		}

		// Token: 0x06002444 RID: 9284 RVA: 0x000C8174 File Offset: 0x000C6374
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode();
		}

		// Token: 0x06002445 RID: 9285 RVA: 0x000C819C File Offset: 0x000C639C
		public override bool Equals(object other)
		{
			return other is global::PathFinder.Point && this.Equals((global::PathFinder.Point)other);
		}

		// Token: 0x06002446 RID: 9286 RVA: 0x000C81BC File Offset: 0x000C63BC
		public bool Equals(global::PathFinder.Point other)
		{
			return this.x == other.x && this.y == other.y;
		}

		// Token: 0x04001FD0 RID: 8144
		public int x;

		// Token: 0x04001FD1 RID: 8145
		public int y;
	}

	// Token: 0x0200079E RID: 1950
	public class Node : IMinHeapNode<global::PathFinder.Node>, ILinkedListNode<global::PathFinder.Node>
	{
		// Token: 0x06002447 RID: 9287 RVA: 0x000C81E4 File Offset: 0x000C63E4
		public Node(global::PathFinder.Point point, int cost, int heuristic, global::PathFinder.Node next = null)
		{
			this.point = point;
			this.cost = cost;
			this.heuristic = heuristic;
			this.next = next;
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06002448 RID: 9288 RVA: 0x000C820C File Offset: 0x000C640C
		// (set) Token: 0x06002449 RID: 9289 RVA: 0x000C8214 File Offset: 0x000C6414
		public global::PathFinder.Node next { get; set; }

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x0600244A RID: 9290 RVA: 0x000C8220 File Offset: 0x000C6420
		// (set) Token: 0x0600244B RID: 9291 RVA: 0x000C8228 File Offset: 0x000C6428
		public global::PathFinder.Node child { get; set; }

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x0600244C RID: 9292 RVA: 0x000C8234 File Offset: 0x000C6434
		public int order
		{
			get
			{
				return this.cost + this.heuristic;
			}
		}

		// Token: 0x04001FD2 RID: 8146
		public global::PathFinder.Point point;

		// Token: 0x04001FD3 RID: 8147
		public int cost;

		// Token: 0x04001FD4 RID: 8148
		public int heuristic;
	}
}
