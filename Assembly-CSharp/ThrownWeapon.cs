﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using Rust.Ai;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000A1 RID: 161
public class ThrownWeapon : global::AttackEntity
{
	// Token: 0x060009EF RID: 2543 RVA: 0x00043528 File Offset: 0x00041728
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("ThrownWeapon.OnRpcMessage", 0.1f))
		{
			if (rpc == 2968518138u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoDrop ");
				}
				using (TimeWarning.New("DoDrop", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoDrop", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoDrop(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoDrop");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 1844230459u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoThrow ");
				}
				using (TimeWarning.New("DoThrow", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsActiveItem.Test("DoThrow", this, player))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoThrow(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in DoThrow");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060009F0 RID: 2544 RVA: 0x0004388C File Offset: 0x00041A8C
	[global::BaseEntity.RPC_Server.IsActiveItem]
	[global::BaseEntity.RPC_Server]
	private void DoThrow(global::BaseEntity.RPCMessage msg)
	{
		if (!base.HasItemAmount() || base.HasAttackCooldown())
		{
			return;
		}
		Vector3 vector = msg.read.Vector3();
		Vector3 normalized = msg.read.Vector3().normalized;
		float num = Mathf.Clamp01(msg.read.Float());
		if (!base.ValidateEyePos(msg.player, vector))
		{
			vector = msg.player.eyes.position;
		}
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.prefabToThrow.resourcePath, vector, Quaternion.LookRotation((!(this.overrideAngle == Vector3.zero)) ? this.overrideAngle : (-normalized)), true);
		if (baseEntity == null)
		{
			return;
		}
		baseEntity.creatorEntity = msg.player;
		baseEntity.SetVelocity(normalized * this.maxThrowVelocity * num + msg.player.estimatedVelocity * 0.5f);
		if (this.tumbleVelocity > 0f)
		{
			baseEntity.SetAngularVelocity(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * this.tumbleVelocity);
		}
		baseEntity.Spawn();
		Interface.CallHook("OnExplosiveThrown", new object[]
		{
			msg.player,
			baseEntity
		});
		base.StartAttackCooldown(this.repeatDelay);
		base.UseItemAmount(1);
		global::BasePlayer player = msg.player;
		if (player != null)
		{
			Rust.Ai.Sense.Stimulate(new Rust.Ai.Sensation
			{
				Type = Rust.Ai.SensationType.ThrownWeapon,
				Position = player.GetNetworkPosition(),
				Radius = 50f
			});
		}
	}

	// Token: 0x060009F1 RID: 2545 RVA: 0x00043A78 File Offset: 0x00041C78
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsActiveItem]
	private void DoDrop(global::BaseEntity.RPCMessage msg)
	{
		if (!base.HasItemAmount() || base.HasAttackCooldown())
		{
			return;
		}
		Vector3 vector = msg.read.Vector3();
		Vector3 normalized = msg.read.Vector3().normalized;
		if (!base.ValidateEyePos(msg.player, vector))
		{
			vector = msg.player.eyes.position;
		}
		global::BaseEntity baseEntity = global::GameManager.server.CreateEntity(this.prefabToThrow.resourcePath, vector, Quaternion.LookRotation(Vector3.up), true);
		if (baseEntity == null)
		{
			return;
		}
		RaycastHit hit;
		if (UnityEngine.Physics.SphereCast(new Ray(vector, normalized), 0.05f, ref hit, 1.5f, 1101212417))
		{
			Vector3 point = hit.point;
			Vector3 normal = hit.normal;
			global::BaseEntity baseEntity2 = hit.GetEntity();
			if (baseEntity2 && baseEntity2 is global::StabilityEntity && baseEntity is global::TimedExplosive)
			{
				baseEntity2 = baseEntity2.ToServer<global::BaseEntity>();
				global::TimedExplosive timedExplosive = baseEntity as global::TimedExplosive;
				timedExplosive.onlyDamageParent = true;
				timedExplosive.DoStick(point, normal, baseEntity2);
			}
			else
			{
				baseEntity.SetVelocity(normalized);
			}
		}
		else
		{
			baseEntity.SetVelocity(normalized);
		}
		baseEntity.creatorEntity = msg.player;
		baseEntity.Spawn();
		Interface.CallHook("OnExplosiveDropped", new object[]
		{
			msg.player,
			baseEntity
		});
		base.StartAttackCooldown(this.repeatDelay);
		base.UseItemAmount(1);
	}

	// Token: 0x0400049C RID: 1180
	[Header("Throw Weapon")]
	public global::GameObjectRef prefabToThrow;

	// Token: 0x0400049D RID: 1181
	public float maxThrowVelocity = 10f;

	// Token: 0x0400049E RID: 1182
	public float tumbleVelocity;

	// Token: 0x0400049F RID: 1183
	public Vector3 overrideAngle = Vector3.zero;
}
