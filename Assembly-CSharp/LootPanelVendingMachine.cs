﻿using System;
using UnityEngine;

// Token: 0x020000B2 RID: 178
public class LootPanelVendingMachine : global::LootPanel
{
	// Token: 0x04000511 RID: 1297
	public GameObject sellOrderPrefab;

	// Token: 0x04000512 RID: 1298
	public GameObject sellOrderContainer;

	// Token: 0x04000513 RID: 1299
	public GameObject busyOverlayPrefab;

	// Token: 0x04000514 RID: 1300
	private GameObject busyOverlayInstance;
}
