﻿using System;
using Rust;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C0 RID: 1728
public class ProtectionValue : MonoBehaviour, global::IClothingChanged
{
	// Token: 0x04001D3D RID: 7485
	public CanvasGroup group;

	// Token: 0x04001D3E RID: 7486
	public Text text;

	// Token: 0x04001D3F RID: 7487
	public Rust.DamageType damageType;

	// Token: 0x04001D40 RID: 7488
	public bool selectedItem;

	// Token: 0x04001D41 RID: 7489
	public bool displayBaseProtection;
}
