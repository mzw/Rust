﻿using System;
using UnityEngine;

// Token: 0x02000272 RID: 626
public class ParticleRandomLifetime : MonoBehaviour
{
	// Token: 0x060010A3 RID: 4259 RVA: 0x000643C4 File Offset: 0x000625C4
	public void Awake()
	{
		if (!this.mySystem)
		{
			return;
		}
		float startLifetime = Random.Range(this.minScale, this.maxScale);
		this.mySystem.startLifetime = startLifetime;
	}

	// Token: 0x04000B93 RID: 2963
	public ParticleSystem mySystem;

	// Token: 0x04000B94 RID: 2964
	public float minScale = 0.5f;

	// Token: 0x04000B95 RID: 2965
	public float maxScale = 1f;
}
