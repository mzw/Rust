﻿using System;
using UnityEngine;

// Token: 0x0200049B RID: 1179
public class TriggerRadiation : global::TriggerBase
{
	// Token: 0x060019B1 RID: 6577 RVA: 0x00090900 File Offset: 0x0008EB00
	public float GetRadiationAmount()
	{
		if (this.RadiationAmountOverride > 0f)
		{
			return this.RadiationAmountOverride;
		}
		if (this.radiationTier == global::TriggerRadiation.RadiationTier.MINIMAL)
		{
			return 2f;
		}
		if (this.radiationTier == global::TriggerRadiation.RadiationTier.LOW)
		{
			return 10f;
		}
		if (this.radiationTier == global::TriggerRadiation.RadiationTier.MEDIUM)
		{
			return 25f;
		}
		if (this.radiationTier == global::TriggerRadiation.RadiationTier.HIGH)
		{
			return 51f;
		}
		return 1f;
	}

	// Token: 0x060019B2 RID: 6578 RVA: 0x00090970 File Offset: 0x0008EB70
	private void OnValidate()
	{
		this.radiationSize = base.GetComponent<SphereCollider>().radius * base.transform.localScale.y;
	}

	// Token: 0x060019B3 RID: 6579 RVA: 0x000909A4 File Offset: 0x0008EBA4
	public float GetRadiation(Vector3 position, float radProtection)
	{
		float radiationAmount = this.GetRadiationAmount();
		float num = Vector3.Distance(base.gameObject.transform.position, position);
		float num2 = Mathf.InverseLerp(this.radiationSize, this.radiationSize * (1f - this.falloff), num);
		float num3 = Mathf.Clamp(radiationAmount - radProtection, 0f, radiationAmount);
		return num3 * num2;
	}

	// Token: 0x060019B4 RID: 6580 RVA: 0x00090A04 File Offset: 0x0008EC04
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		if (!(baseEntity is global::BaseCombatEntity))
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x060019B5 RID: 6581 RVA: 0x00090A60 File Offset: 0x0008EC60
	public void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(base.transform.position, this.radiationSize);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(base.transform.position, this.radiationSize * (1f - this.falloff));
	}

	// Token: 0x04001444 RID: 5188
	public global::TriggerRadiation.RadiationTier radiationTier = global::TriggerRadiation.RadiationTier.LOW;

	// Token: 0x04001445 RID: 5189
	public float RadiationAmountOverride;

	// Token: 0x04001446 RID: 5190
	public float radiationSize;

	// Token: 0x04001447 RID: 5191
	public float falloff = 0.1f;

	// Token: 0x0200049C RID: 1180
	public enum RadiationTier
	{
		// Token: 0x04001449 RID: 5193
		MINIMAL,
		// Token: 0x0400144A RID: 5194
		LOW,
		// Token: 0x0400144B RID: 5195
		MEDIUM,
		// Token: 0x0400144C RID: 5196
		HIGH
	}
}
