﻿using System;
using System.Collections.Generic;
using Rust;
using UnityEngine;

// Token: 0x020004C9 RID: 1225
public class ItemBlueprint : MonoBehaviour
{
	// Token: 0x170001D0 RID: 464
	// (get) Token: 0x06001A87 RID: 6791 RVA: 0x000954CC File Offset: 0x000936CC
	public global::ItemDefinition targetItem
	{
		get
		{
			return base.GetComponent<global::ItemDefinition>();
		}
	}

	// Token: 0x04001515 RID: 5397
	public List<global::ItemAmount> ingredients = new List<global::ItemAmount>();

	// Token: 0x04001516 RID: 5398
	public bool defaultBlueprint;

	// Token: 0x04001517 RID: 5399
	public bool userCraftable = true;

	// Token: 0x04001518 RID: 5400
	public bool isResearchable = true;

	// Token: 0x04001519 RID: 5401
	public Rarity rarity;

	// Token: 0x0400151A RID: 5402
	[Header("Workbench")]
	public int workbenchLevelRequired;

	// Token: 0x0400151B RID: 5403
	[Header("Scrap")]
	public int scrapRequired;

	// Token: 0x0400151C RID: 5404
	public int scrapFromRecycle;

	// Token: 0x0400151D RID: 5405
	[Tooltip("This item won't show anywhere unless you have the corresponding SteamItem in your inventory - which is defined on the ItemDefinition")]
	[Header("Unlocking")]
	public bool NeedsSteamItem;

	// Token: 0x0400151E RID: 5406
	public int blueprintStackSize = -1;

	// Token: 0x0400151F RID: 5407
	public float time = 1f;

	// Token: 0x04001520 RID: 5408
	public int amountToCreate = 1;

	// Token: 0x04001521 RID: 5409
	public string UnlockAchievment;
}
