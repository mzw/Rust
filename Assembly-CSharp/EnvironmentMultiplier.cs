﻿using System;

// Token: 0x02000606 RID: 1542
[Serializable]
public class EnvironmentMultiplier
{
	// Token: 0x04001A54 RID: 6740
	public global::EnvironmentType Type;

	// Token: 0x04001A55 RID: 6741
	public float Multiplier;
}
