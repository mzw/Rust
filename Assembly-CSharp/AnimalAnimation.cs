﻿using System;
using UnityEngine;

// Token: 0x02000233 RID: 563
public class AnimalAnimation : MonoBehaviour, IClientComponent
{
	// Token: 0x04000ABC RID: 2748
	public global::BaseNpc Target;

	// Token: 0x04000ABD RID: 2749
	public Animator Animator;

	// Token: 0x04000ABE RID: 2750
	public global::MaterialEffect FootstepEffects;

	// Token: 0x04000ABF RID: 2751
	public Transform[] Feet;

	// Token: 0x04000AC0 RID: 2752
	[ReadOnly]
	public string BaseFolder;
}
