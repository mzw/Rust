﻿using System;
using UnityEngine;

// Token: 0x020001F0 RID: 496
public class ReverbZoneTrigger : global::TriggerBase, global::IClientComponentEx, global::ILOD
{
	// Token: 0x06000F4D RID: 3917 RVA: 0x0005DB74 File Offset: 0x0005BD74
	public virtual void PreClientComponentCull(global::IPrefabProcessor p)
	{
		p.RemoveComponent(this.trigger);
		p.RemoveComponent(this.reverbZone);
		p.RemoveComponent(this);
		p.NominateForDeletion(base.gameObject);
	}

	// Token: 0x040009B8 RID: 2488
	public Collider trigger;

	// Token: 0x040009B9 RID: 2489
	public AudioReverbZone reverbZone;

	// Token: 0x040009BA RID: 2490
	public float lodDistance = 100f;

	// Token: 0x040009BB RID: 2491
	public bool inRange;

	// Token: 0x040009BC RID: 2492
	public global::ReverbSettings reverbSettings;
}
