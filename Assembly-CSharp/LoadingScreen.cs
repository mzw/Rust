﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006D3 RID: 1747
public class LoadingScreen : SingletonComponent<global::LoadingScreen>
{
	// Token: 0x17000249 RID: 585
	// (get) Token: 0x06002178 RID: 8568 RVA: 0x000BC314 File Offset: 0x000BA514
	public static bool isOpen
	{
		get
		{
			return SingletonComponent<global::LoadingScreen>.Instance && SingletonComponent<global::LoadingScreen>.Instance.panel && SingletonComponent<global::LoadingScreen>.Instance.panel.gameObject.activeSelf;
		}
	}

	// Token: 0x1700024A RID: 586
	// (get) Token: 0x06002179 RID: 8569 RVA: 0x000BC350 File Offset: 0x000BA550
	// (set) Token: 0x0600217A RID: 8570 RVA: 0x000BC358 File Offset: 0x000BA558
	public static bool WantsSkip { get; private set; }

	// Token: 0x1700024B RID: 587
	// (get) Token: 0x0600217C RID: 8572 RVA: 0x000BC368 File Offset: 0x000BA568
	// (set) Token: 0x0600217B RID: 8571 RVA: 0x000BC360 File Offset: 0x000BA560
	public static string Text { get; private set; }

	// Token: 0x0600217D RID: 8573 RVA: 0x000BC370 File Offset: 0x000BA570
	protected override void Awake()
	{
		base.Awake();
		global::LoadingScreen.HideSkip();
		global::LoadingScreen.Hide();
	}

	// Token: 0x0600217E RID: 8574 RVA: 0x000BC384 File Offset: 0x000BA584
	public static void Show()
	{
		if (!SingletonComponent<global::LoadingScreen>.Instance)
		{
			Debug.LogWarning("Wanted to show loading screen but not ready");
			return;
		}
		if (SingletonComponent<global::LoadingScreen>.Instance.panel.gameObject.activeSelf)
		{
			return;
		}
		SingletonComponent<global::LoadingScreen>.Instance.panel.gameObject.SetActive(true);
		SingletonComponent<global::LoadingScreen>.Instance.gameObject.SetActive(false);
		SingletonComponent<global::LoadingScreen>.Instance.gameObject.SetActive(true);
		global::MusicManager.RaiseIntensityTo(0.5f, 999);
	}

	// Token: 0x0600217F RID: 8575 RVA: 0x000BC40C File Offset: 0x000BA60C
	public static void Hide()
	{
		if (!SingletonComponent<global::LoadingScreen>.Instance)
		{
			return;
		}
		if (!SingletonComponent<global::LoadingScreen>.Instance.panel)
		{
			return;
		}
		if (!SingletonComponent<global::LoadingScreen>.Instance.panel.gameObject)
		{
			return;
		}
		if (!SingletonComponent<global::LoadingScreen>.Instance.panel.gameObject.activeSelf)
		{
			return;
		}
		SingletonComponent<global::LoadingScreen>.Instance.panel.gameObject.SetActive(false);
		SingletonComponent<global::LoadingScreen>.Instance.gameObject.SetActive(false);
		SingletonComponent<global::LoadingScreen>.Instance.gameObject.SetActive(true);
		if (global::LevelManager.isLoaded && global::MusicManager.instance != null)
		{
			global::MusicManager.instance.StopMusic();
		}
	}

	// Token: 0x06002180 RID: 8576 RVA: 0x000BC4CC File Offset: 0x000BA6CC
	public static void ShowSkip()
	{
		global::LoadingScreen.WantsSkip = false;
		if (!SingletonComponent<global::LoadingScreen>.Instance)
		{
			return;
		}
		if (!SingletonComponent<global::LoadingScreen>.Instance.skipButton)
		{
			return;
		}
		SingletonComponent<global::LoadingScreen>.Instance.skipButton.gameObject.SetActive(true);
	}

	// Token: 0x06002181 RID: 8577 RVA: 0x000BC51C File Offset: 0x000BA71C
	public static void HideSkip()
	{
		global::LoadingScreen.WantsSkip = false;
		if (!SingletonComponent<global::LoadingScreen>.Instance)
		{
			return;
		}
		if (!SingletonComponent<global::LoadingScreen>.Instance.skipButton)
		{
			return;
		}
		SingletonComponent<global::LoadingScreen>.Instance.skipButton.gameObject.SetActive(false);
	}

	// Token: 0x06002182 RID: 8578 RVA: 0x000BC56C File Offset: 0x000BA76C
	public static void Update(string strType)
	{
		if (global::LoadingScreen.Text == strType)
		{
			return;
		}
		global::LoadingScreen.Text = strType;
		if (!SingletonComponent<global::LoadingScreen>.Instance)
		{
			return;
		}
		SingletonComponent<global::LoadingScreen>.Instance.subtitle.text = strType.ToUpper();
		GameObject gameObject = GameObject.Find("MenuMusic");
		if (gameObject)
		{
			AudioSource component = gameObject.GetComponent<AudioSource>();
			if (component)
			{
				component.Pause();
			}
		}
	}

	// Token: 0x06002183 RID: 8579 RVA: 0x000BC5E4 File Offset: 0x000BA7E4
	public void UpdateFromServer(string strTitle, string strSubtitle)
	{
		this.title.text = strTitle;
		this.subtitle.text = strSubtitle;
	}

	// Token: 0x06002184 RID: 8580 RVA: 0x000BC600 File Offset: 0x000BA800
	public void CancelLoading()
	{
		ConsoleSystem.Run(ConsoleSystem.Option.Client.Quiet(), "client.disconnect", new object[0]);
	}

	// Token: 0x06002185 RID: 8581 RVA: 0x000BC62C File Offset: 0x000BA82C
	public void SkipLoading()
	{
		global::LoadingScreen.WantsSkip = true;
	}

	// Token: 0x04001DA5 RID: 7589
	public CanvasRenderer panel;

	// Token: 0x04001DA6 RID: 7590
	public Text title;

	// Token: 0x04001DA7 RID: 7591
	public Text subtitle;

	// Token: 0x04001DA8 RID: 7592
	public Button skipButton;

	// Token: 0x04001DA9 RID: 7593
	public AudioSource music;
}
