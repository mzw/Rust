﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000671 RID: 1649
public class ConvarToggleChildren : MonoBehaviour
{
	// Token: 0x060020C2 RID: 8386 RVA: 0x000BA8B4 File Offset: 0x000B8AB4
	protected void Awake()
	{
		this.Command = ConsoleSystem.Index.Client.Find(this.ConvarName);
		if (this.Command == null)
		{
			this.Command = ConsoleSystem.Index.Server.Find(this.ConvarName);
		}
		if (this.Command != null)
		{
			this.SetState(this.Command.String == this.ConvarEnabled);
		}
	}

	// Token: 0x060020C3 RID: 8387 RVA: 0x000BA918 File Offset: 0x000B8B18
	protected void Update()
	{
		if (this.Command != null)
		{
			bool flag = this.Command.String == this.ConvarEnabled;
			if (this.state != flag)
			{
				this.SetState(flag);
			}
		}
	}

	// Token: 0x060020C4 RID: 8388 RVA: 0x000BA95C File Offset: 0x000B8B5C
	private void SetState(bool newState)
	{
		IEnumerator enumerator = base.transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform = (Transform)obj;
				transform.gameObject.SetActive(newState);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		this.state = newState;
	}

	// Token: 0x04001C22 RID: 7202
	public string ConvarName;

	// Token: 0x04001C23 RID: 7203
	public string ConvarEnabled = "True";

	// Token: 0x04001C24 RID: 7204
	private bool state;

	// Token: 0x04001C25 RID: 7205
	private ConsoleSystem.Command Command;
}
