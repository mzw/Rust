﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020006C3 RID: 1731
public class RepairBenchPanel : global::LootPanel
{
	// Token: 0x04001D47 RID: 7495
	public Text infoText;

	// Token: 0x04001D48 RID: 7496
	public Button repairButton;

	// Token: 0x04001D49 RID: 7497
	public Color gotColor;

	// Token: 0x04001D4A RID: 7498
	public Color notGotColor;

	// Token: 0x04001D4B RID: 7499
	public global::Translate.Phrase phraseEmpty;

	// Token: 0x04001D4C RID: 7500
	public global::Translate.Phrase phraseNotRepairable;

	// Token: 0x04001D4D RID: 7501
	public global::Translate.Phrase phraseRepairNotNeeded;

	// Token: 0x04001D4E RID: 7502
	public global::Translate.Phrase phraseNoBlueprint;

	// Token: 0x04001D4F RID: 7503
	public GameObject skinsPanel;

	// Token: 0x04001D50 RID: 7504
	public GameObject changeSkinDialog;

	// Token: 0x04001D51 RID: 7505
	public global::IconSkinPicker picker;
}
