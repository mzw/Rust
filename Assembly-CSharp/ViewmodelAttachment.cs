﻿using System;

// Token: 0x020007C3 RID: 1987
public class ViewmodelAttachment : global::EntityComponent<global::BaseEntity>, IClientComponent, global::IViewModeChanged, global::IViewModelUpdated
{
	// Token: 0x0400205B RID: 8283
	public global::GameObjectRef modelObject;

	// Token: 0x0400205C RID: 8284
	public string targetBone;

	// Token: 0x0400205D RID: 8285
	public bool hideViewModelIronSights;
}
