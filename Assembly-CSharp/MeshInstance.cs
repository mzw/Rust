﻿using System;
using UnityEngine;

// Token: 0x02000252 RID: 594
public struct MeshInstance
{
	// Token: 0x17000115 RID: 277
	// (get) Token: 0x06001050 RID: 4176 RVA: 0x00062C00 File Offset: 0x00060E00
	// (set) Token: 0x06001051 RID: 4177 RVA: 0x00062C10 File Offset: 0x00060E10
	public Mesh mesh
	{
		get
		{
			return this.data.mesh;
		}
		set
		{
			this.data = global::MeshCache.Get(value);
		}
	}

	// Token: 0x04000B1A RID: 2842
	public Vector3 position;

	// Token: 0x04000B1B RID: 2843
	public Quaternion rotation;

	// Token: 0x04000B1C RID: 2844
	public Vector3 scale;

	// Token: 0x04000B1D RID: 2845
	public global::MeshCache.Data data;
}
