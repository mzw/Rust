﻿using System;
using UnityEngine.UI;

// Token: 0x0200069D RID: 1693
public class ArmorInformationPanel : global::ItemInformationPanel
{
	// Token: 0x04001C99 RID: 7321
	public global::ItemTextValue projectileDisplay;

	// Token: 0x04001C9A RID: 7322
	public global::ItemTextValue meleeDisplay;

	// Token: 0x04001C9B RID: 7323
	public global::ItemTextValue coldDisplay;

	// Token: 0x04001C9C RID: 7324
	public global::ItemTextValue explosionDisplay;

	// Token: 0x04001C9D RID: 7325
	public global::ItemTextValue radiationDisplay;

	// Token: 0x04001C9E RID: 7326
	public global::ItemTextValue biteDisplay;

	// Token: 0x04001C9F RID: 7327
	public global::ItemTextValue spacer;

	// Token: 0x04001CA0 RID: 7328
	public Text areaProtectionText;

	// Token: 0x04001CA1 RID: 7329
	public global::Translate.Phrase LegText;

	// Token: 0x04001CA2 RID: 7330
	public global::Translate.Phrase ChestText;

	// Token: 0x04001CA3 RID: 7331
	public global::Translate.Phrase HeadText;

	// Token: 0x04001CA4 RID: 7332
	public global::Translate.Phrase ChestLegsText;

	// Token: 0x04001CA5 RID: 7333
	public global::Translate.Phrase WholeBodyText;
}
