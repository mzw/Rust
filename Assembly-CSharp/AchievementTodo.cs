﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000006 RID: 6
public class AchievementTodo : global::BaseMonoBehaviour
{
	// Token: 0x0400000F RID: 15
	public Text text;

	// Token: 0x04000010 RID: 16
	public RectTransform checkIcon;

	// Token: 0x04000011 RID: 17
	public RectTransform checkBox;

	// Token: 0x04000012 RID: 18
	public Color AliveColor;

	// Token: 0x04000013 RID: 19
	public Color DeadColor;

	// Token: 0x04000014 RID: 20
	public Color HighlightColor;
}
