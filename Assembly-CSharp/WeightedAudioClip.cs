﻿using System;
using UnityEngine;

// Token: 0x02000206 RID: 518
[Serializable]
public class WeightedAudioClip
{
	// Token: 0x04000A1E RID: 2590
	public AudioClip audioClip;

	// Token: 0x04000A1F RID: 2591
	public int weight = 1;
}
