﻿using System;

// Token: 0x020004E3 RID: 1251
public class ItemModContainerRestriction : global::ItemMod
{
	// Token: 0x06001AD3 RID: 6867 RVA: 0x00096434 File Offset: 0x00094634
	public bool CanExistWith(global::ItemModContainerRestriction other)
	{
		return other == null || (this.slotFlags & other.slotFlags) == (global::ItemModContainerRestriction.SlotFlags)0;
	}

	// Token: 0x04001599 RID: 5529
	[InspectorFlags]
	public global::ItemModContainerRestriction.SlotFlags slotFlags;

	// Token: 0x020004E4 RID: 1252
	[Flags]
	public enum SlotFlags
	{
		// Token: 0x0400159B RID: 5531
		Map = 1
	}
}
