﻿using System;
using ConVar;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000800 RID: 2048
[ExecuteInEditMode]
[RequireComponent(typeof(Light))]
public class NGSS_Directional : MonoBehaviour
{
	// Token: 0x060025DF RID: 9695 RVA: 0x000D14B4 File Offset: 0x000CF6B4
	private void OnDestroy()
	{
		this.Shutdown();
	}

	// Token: 0x060025E0 RID: 9696 RVA: 0x000D14BC File Offset: 0x000CF6BC
	private void OnDisable()
	{
		this.Shutdown();
	}

	// Token: 0x060025E1 RID: 9697 RVA: 0x000D14C4 File Offset: 0x000CF6C4
	private void OnApplicationQuit()
	{
		this.Shutdown();
	}

	// Token: 0x060025E2 RID: 9698 RVA: 0x000D14CC File Offset: 0x000CF6CC
	private void OnEnable()
	{
		this.Init();
	}

	// Token: 0x060025E3 RID: 9699 RVA: 0x000D14D4 File Offset: 0x000CF6D4
	private void Init()
	{
		if (!this.isInitialized)
		{
			this.m_Light = base.GetComponent<Light>();
			int num = (QualitySettings.shadowResolution != 3) ? ((QualitySettings.shadowResolution != 2) ? ((QualitySettings.shadowResolution != 1) ? 512 : 1024) : 2048) : 4096;
			this.m_ShadowmapCopy = null;
			this.m_ShadowmapCopy = new RenderTexture(num, num, 0, 14);
			this.m_ShadowmapCopy.filterMode = 1;
			this.m_ShadowmapCopy.useMipMap = false;
			this.rawShadowDepthCB = new CommandBuffer
			{
				name = "NGSS Directional PCSS buffer"
			};
			this.rawShadowDepthCB.Clear();
			this.rawShadowDepthCB.SetShadowSamplingMode(1, 1);
			this.rawShadowDepthCB.Blit(1, this.m_ShadowmapCopy);
			this.rawShadowDepthCB.SetGlobalTexture("NGSS_DirectionalRawDepth", this.m_ShadowmapCopy);
			foreach (CommandBuffer commandBuffer in this.m_Light.GetCommandBuffers(1))
			{
				if (commandBuffer.name == this.rawShadowDepthCB.name)
				{
					this.isInitialized = true;
					return;
				}
			}
			this.m_Light.AddCommandBuffer(1, this.rawShadowDepthCB);
			this.isInitialized = true;
		}
	}

	// Token: 0x060025E4 RID: 9700 RVA: 0x000D1640 File Offset: 0x000CF840
	private void Shutdown()
	{
		if (this.isInitialized)
		{
			this.m_Light.RemoveCommandBuffer(1, this.rawShadowDepthCB);
			this.m_ShadowmapCopy = null;
			this.isInitialized = false;
		}
	}

	// Token: 0x060025E5 RID: 9701 RVA: 0x000D1670 File Offset: 0x000CF870
	private void Update()
	{
		if (ConVar.Graphics.shadowquality < 2)
		{
			this.Shutdown();
		}
		else
		{
			this.Init();
			this.SetGlobalSettings();
		}
	}

	// Token: 0x060025E6 RID: 9702 RVA: 0x000D1694 File Offset: 0x000CF894
	private void SetGlobalSettings()
	{
		Shader.SetGlobalFloat("NGSS_PCSS_GLOBAL_SOFTNESS", this.PCSS_GLOBAL_SOFTNESS);
		Shader.SetGlobalFloat("NGSS_PCSS_FILTER_DIR_MIN", (this.PCSS_FILTER_DIR_MIN <= this.PCSS_FILTER_DIR_MAX) ? this.PCSS_FILTER_DIR_MIN : this.PCSS_FILTER_DIR_MAX);
		Shader.SetGlobalFloat("NGSS_PCSS_FILTER_DIR_MAX", (this.PCSS_FILTER_DIR_MAX >= this.PCSS_FILTER_DIR_MIN) ? this.PCSS_FILTER_DIR_MAX : this.PCSS_FILTER_DIR_MIN);
		Shader.SetGlobalFloat("NGSS_POISSON_SAMPLING_NOISE_DIR", this.BANDING_NOISE_AMOUNT);
		Shader.EnableKeyword("NGSS_PCSS_FILTER_DIR");
		if (this.EARLY_BAILOUT_OPTIMIZATION)
		{
			Shader.EnableKeyword("NGSS_USE_EARLY_BAILOUT_OPTIMIZATION_DIR");
		}
		else
		{
			Shader.DisableKeyword("NGSS_USE_EARLY_BAILOUT_OPTIMIZATION_DIR");
		}
		if (this.USE_BIAS_FADE)
		{
			Shader.EnableKeyword("NGSS_USE_BIAS_FADE_DIR");
		}
		else
		{
			Shader.DisableKeyword("NGSS_USE_BIAS_FADE_DIR");
		}
		Shader.DisableKeyword("DIR_POISSON_64");
		Shader.DisableKeyword("DIR_POISSON_32");
		Shader.DisableKeyword("DIR_POISSON_25");
		Shader.DisableKeyword("DIR_POISSON_16");
		string text;
		switch (this.SAMPLERS_COUNT)
		{
		case global::NGSS_Directional.SAMPLER_COUNT.SAMPLERS_25:
			text = "DIR_POISSON_25";
			break;
		case global::NGSS_Directional.SAMPLER_COUNT.SAMPLERS_32:
			text = "DIR_POISSON_32";
			break;
		case global::NGSS_Directional.SAMPLER_COUNT.SAMPLERS_64:
			text = "DIR_POISSON_64";
			break;
		default:
			text = "DIR_POISSON_16";
			break;
		}
		Shader.EnableKeyword(text);
	}

	// Token: 0x04002214 RID: 8724
	[Tooltip("Optimize shadows performance by skipping fragments that are 100% lit or 100% shadowed. Some tiny noisy artefacts can be seen if shadows are too soft.")]
	public bool EARLY_BAILOUT_OPTIMIZATION = true;

	// Token: 0x04002215 RID: 8725
	[Tooltip("Help with bias problems but can leads to some noisy artefacts if Early Bailout Optimization is enabled.\nRequires PCSS in order to work.")]
	public bool USE_BIAS_FADE;

	// Token: 0x04002216 RID: 8726
	[Range(0f, 0.02f)]
	[Tooltip("Overall softness for both PCF and PCSS shadows.\nRecommended value: 0.01.")]
	public float PCSS_GLOBAL_SOFTNESS = 0.01f;

	// Token: 0x04002217 RID: 8727
	[Range(0f, 1f)]
	[Tooltip("PCSS softness when shadows is close to caster.\nRecommended value: 0.05.")]
	public float PCSS_FILTER_DIR_MIN = 0.05f;

	// Token: 0x04002218 RID: 8728
	[Range(0f, 0.5f)]
	[Tooltip("PCSS softness when shadows is far from caster.\nRecommended value: 0.25.\nIf too high can lead to visible artifacts when early bailout is enabled.")]
	public float PCSS_FILTER_DIR_MAX = 0.25f;

	// Token: 0x04002219 RID: 8729
	[Range(0f, 10f)]
	[Tooltip("Amount of banding or noise. Example: 0.0 gives 100 % Banding and 10.0 gives 100 % Noise.")]
	public float BANDING_NOISE_AMOUNT = 1f;

	// Token: 0x0400221A RID: 8730
	[Tooltip("Recommended values: Mobile = 16, Consoles = 25, Desktop Low = 32, Desktop High = 64")]
	public global::NGSS_Directional.SAMPLER_COUNT SAMPLERS_COUNT;

	// Token: 0x0400221B RID: 8731
	private bool isInitialized;

	// Token: 0x0400221C RID: 8732
	private Light m_Light;

	// Token: 0x0400221D RID: 8733
	private CommandBuffer rawShadowDepthCB;

	// Token: 0x0400221E RID: 8734
	private RenderTexture m_ShadowmapCopy;

	// Token: 0x02000801 RID: 2049
	public enum SAMPLER_COUNT
	{
		// Token: 0x04002220 RID: 8736
		SAMPLERS_16,
		// Token: 0x04002221 RID: 8737
		SAMPLERS_25,
		// Token: 0x04002222 RID: 8738
		SAMPLERS_32,
		// Token: 0x04002223 RID: 8739
		SAMPLERS_64
	}
}
