﻿using System;

// Token: 0x02000430 RID: 1072
[Flags]
public enum HitArea
{
	// Token: 0x04001319 RID: 4889
	Head = 1,
	// Token: 0x0400131A RID: 4890
	Chest = 2,
	// Token: 0x0400131B RID: 4891
	Stomach = 4,
	// Token: 0x0400131C RID: 4892
	Arm = 8,
	// Token: 0x0400131D RID: 4893
	Hand = 16,
	// Token: 0x0400131E RID: 4894
	Leg = 32,
	// Token: 0x0400131F RID: 4895
	Foot = 64
}
