﻿using System;
using Rust;
using UnityEngine;

// Token: 0x0200047E RID: 1150
public class SavePause : MonoBehaviour, IServerComponent
{
	// Token: 0x060018F3 RID: 6387 RVA: 0x0008C594 File Offset: 0x0008A794
	protected void OnEnable()
	{
		if (!SingletonComponent<global::SaveRestore>.Instance || this.tracked)
		{
			return;
		}
		this.tracked = true;
		SingletonComponent<global::SaveRestore>.Instance.timedSavePause++;
	}

	// Token: 0x060018F4 RID: 6388 RVA: 0x0008C5CC File Offset: 0x0008A7CC
	protected void OnDisable()
	{
		if (Application.isQuitting)
		{
			return;
		}
		if (!SingletonComponent<global::SaveRestore>.Instance || !this.tracked)
		{
			return;
		}
		this.tracked = false;
		SingletonComponent<global::SaveRestore>.Instance.timedSavePause--;
	}

	// Token: 0x040013AF RID: 5039
	private bool tracked;
}
