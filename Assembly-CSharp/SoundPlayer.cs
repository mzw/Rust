﻿using System;
using UnityEngine;

// Token: 0x020001FD RID: 509
public class SoundPlayer : global::BaseMonoBehaviour, IClientComponent
{
	// Token: 0x04000A09 RID: 2569
	public global::SoundDefinition soundDefinition;

	// Token: 0x04000A0A RID: 2570
	public bool playImmediately = true;

	// Token: 0x04000A0B RID: 2571
	public bool debugRepeat;

	// Token: 0x04000A0C RID: 2572
	public bool pending;

	// Token: 0x04000A0D RID: 2573
	public Vector3 soundOffset = Vector3.zero;
}
