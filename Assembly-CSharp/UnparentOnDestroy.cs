﻿using System;
using UnityEngine;

// Token: 0x0200028A RID: 650
public class UnparentOnDestroy : MonoBehaviour, global::IOnParentDestroying
{
	// Token: 0x060010D2 RID: 4306 RVA: 0x00064FEC File Offset: 0x000631EC
	public void OnParentDestroying()
	{
		base.transform.parent = null;
		global::GameManager.Destroy(base.gameObject, (this.destroyAfterSeconds > 0f) ? this.destroyAfterSeconds : 1f);
	}

	// Token: 0x060010D3 RID: 4307 RVA: 0x00065028 File Offset: 0x00063228
	protected void OnValidate()
	{
		if (this.destroyAfterSeconds <= 0f)
		{
			this.destroyAfterSeconds = 1f;
		}
	}

	// Token: 0x04000BF4 RID: 3060
	public float destroyAfterSeconds = 1f;
}
