﻿using System;
using UnityEngine;

// Token: 0x0200042D RID: 1069
public class HitTest
{
	// Token: 0x06001850 RID: 6224 RVA: 0x00089F14 File Offset: 0x00088114
	public Vector3 HitPointWorld()
	{
		if (this.HitEntity != null)
		{
			Transform transform = this.HitTransform;
			if (!transform)
			{
				transform = this.HitEntity.transform;
			}
			return transform.TransformPoint(this.HitPoint);
		}
		return this.HitPoint;
	}

	// Token: 0x06001851 RID: 6225 RVA: 0x00089F64 File Offset: 0x00088164
	public Vector3 HitNormalWorld()
	{
		if (this.HitEntity != null)
		{
			Transform transform = this.HitTransform;
			if (!transform)
			{
				transform = this.HitEntity.transform;
			}
			return transform.TransformDirection(this.HitNormal);
		}
		return this.HitNormal;
	}

	// Token: 0x06001852 RID: 6226 RVA: 0x00089FB4 File Offset: 0x000881B4
	public void Clear()
	{
		this.type = global::HitTest.Type.Generic;
		this.AttackRay = default(Ray);
		this.Radius = 0f;
		this.Forgiveness = 0f;
		this.MaxDistance = 0f;
		this.RayHit = default(RaycastHit);
		this.MultiHit = false;
		this.BestHit = false;
		this.DidHit = false;
		this.damageProperties = null;
		this.gameObject = null;
		this.collider = null;
		this.ignoreEntity = null;
		this.HitEntity = null;
		this.HitPoint = default(Vector3);
		this.HitNormal = default(Vector3);
		this.HitDistance = 0f;
		this.HitTransform = null;
		this.HitPart = 0u;
		this.HitMaterial = null;
	}

	// Token: 0x040012E1 RID: 4833
	public global::HitTest.Type type;

	// Token: 0x040012E2 RID: 4834
	public Ray AttackRay;

	// Token: 0x040012E3 RID: 4835
	public float Radius;

	// Token: 0x040012E4 RID: 4836
	public float Forgiveness;

	// Token: 0x040012E5 RID: 4837
	public float MaxDistance;

	// Token: 0x040012E6 RID: 4838
	public RaycastHit RayHit;

	// Token: 0x040012E7 RID: 4839
	public bool MultiHit;

	// Token: 0x040012E8 RID: 4840
	public bool BestHit;

	// Token: 0x040012E9 RID: 4841
	public bool DidHit;

	// Token: 0x040012EA RID: 4842
	public global::DamageProperties damageProperties;

	// Token: 0x040012EB RID: 4843
	public GameObject gameObject;

	// Token: 0x040012EC RID: 4844
	public Collider collider;

	// Token: 0x040012ED RID: 4845
	public global::BaseEntity ignoreEntity;

	// Token: 0x040012EE RID: 4846
	public global::BaseEntity HitEntity;

	// Token: 0x040012EF RID: 4847
	public Vector3 HitPoint;

	// Token: 0x040012F0 RID: 4848
	public Vector3 HitNormal;

	// Token: 0x040012F1 RID: 4849
	public float HitDistance;

	// Token: 0x040012F2 RID: 4850
	public Transform HitTransform;

	// Token: 0x040012F3 RID: 4851
	public uint HitPart;

	// Token: 0x040012F4 RID: 4852
	public string HitMaterial;

	// Token: 0x0200042E RID: 1070
	public enum Type
	{
		// Token: 0x040012F6 RID: 4854
		Generic,
		// Token: 0x040012F7 RID: 4855
		ProjectileEffect,
		// Token: 0x040012F8 RID: 4856
		Projectile,
		// Token: 0x040012F9 RID: 4857
		MeleeAttack,
		// Token: 0x040012FA RID: 4858
		Use
	}
}
