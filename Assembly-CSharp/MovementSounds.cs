﻿using System;
using UnityEngine;

// Token: 0x02000509 RID: 1289
public class MovementSounds : MonoBehaviour
{
	// Token: 0x04001610 RID: 5648
	public global::SoundDefinition waterMovementDef;

	// Token: 0x04001611 RID: 5649
	public float waterMovementFadeInSpeed = 1f;

	// Token: 0x04001612 RID: 5650
	public float waterMovementFadeOutSpeed = 1f;

	// Token: 0x04001613 RID: 5651
	private global::Sound waterMovement;

	// Token: 0x04001614 RID: 5652
	private global::SoundModulation.Modulator waterGainMod;

	// Token: 0x04001615 RID: 5653
	private Vector3 lastPos;
}
