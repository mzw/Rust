﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x0200041F RID: 1055
public static class EnvironmentVolumeEx
{
	// Token: 0x06001816 RID: 6166 RVA: 0x00089198 File Offset: 0x00087398
	public static bool CheckEnvironmentVolumes(this Transform transform, Vector3 pos, Quaternion rot, Vector3 scale, global::EnvironmentType type)
	{
		List<global::EnvironmentVolume> list = Pool.GetList<global::EnvironmentVolume>();
		transform.GetComponentsInChildren<global::EnvironmentVolume>(true, list);
		for (int i = 0; i < list.Count; i++)
		{
			global::EnvironmentVolume environmentVolume = list[i];
			OBB obb;
			obb..ctor(environmentVolume.transform, new Bounds(environmentVolume.Center, environmentVolume.Size));
			obb.Transform(pos, scale, rot);
			if (global::EnvironmentManager.Check(obb, type))
			{
				Pool.FreeList<global::EnvironmentVolume>(ref list);
				return true;
			}
		}
		Pool.FreeList<global::EnvironmentVolume>(ref list);
		return false;
	}

	// Token: 0x06001817 RID: 6167 RVA: 0x00089218 File Offset: 0x00087418
	public static bool CheckEnvironmentVolumes(this Transform transform, global::EnvironmentType type)
	{
		return transform.CheckEnvironmentVolumes(transform.position, transform.rotation, transform.lossyScale, type);
	}
}
