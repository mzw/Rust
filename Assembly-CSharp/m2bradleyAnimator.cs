﻿using System;
using UnityEngine;

// Token: 0x0200039F RID: 927
public class m2bradleyAnimator : MonoBehaviour
{
	// Token: 0x060015D8 RID: 5592 RVA: 0x0007DB38 File Offset: 0x0007BD38
	private void Start()
	{
		this.mainRigidbody = base.GetComponent<Rigidbody>();
		for (int i = 0; i < this.ShocksBones.Length; i++)
		{
			this.vecShocksOffsetPosition[i] = this.ShocksBones[i].localPosition;
		}
	}

	// Token: 0x060015D9 RID: 5593 RVA: 0x0007DB88 File Offset: 0x0007BD88
	private void Update()
	{
		this.TrackTurret();
		this.TrackSpotLight();
		this.TrackSideGuns();
		this.AnimateWheelsTreads();
		this.AdjustShocksHeight();
		this.m2Animator.SetBool("rocketpods", this.rocketsOpen);
	}

	// Token: 0x060015DA RID: 5594 RVA: 0x0007DBC0 File Offset: 0x0007BDC0
	private void AnimateWheelsTreads()
	{
		float num = 0f;
		if (this.mainRigidbody != null)
		{
			num = Vector3.Dot(this.mainRigidbody.velocity, base.transform.forward);
		}
		float num2 = Time.time * -1f * num * this.treadConstant % 1f;
		this.treadLeftMaterial.SetTextureOffset("_MainTex", new Vector2(num2, 0f));
		this.treadLeftMaterial.SetTextureOffset("_BumpMap", new Vector2(num2, 0f));
		this.treadLeftMaterial.SetTextureOffset("_SpecGlossMap", new Vector2(num2, 0f));
		this.treadRightMaterial.SetTextureOffset("_MainTex", new Vector2(num2, 0f));
		this.treadRightMaterial.SetTextureOffset("_BumpMap", new Vector2(num2, 0f));
		this.treadRightMaterial.SetTextureOffset("_SpecGlossMap", new Vector2(num2, 0f));
		if (num >= 0f)
		{
			this.wheelAngle = (this.wheelAngle + Time.deltaTime * num * this.wheelSpinConstant) % 360f;
		}
		else
		{
			this.wheelAngle += Time.deltaTime * num * this.wheelSpinConstant;
			if (this.wheelAngle <= 0f)
			{
				this.wheelAngle = 360f;
			}
		}
		this.m2Animator.SetFloat("wheel_spin", this.wheelAngle);
		this.m2Animator.SetFloat("speed", num);
	}

	// Token: 0x060015DB RID: 5595 RVA: 0x0007DD4C File Offset: 0x0007BF4C
	private void AdjustShocksHeight()
	{
		Ray ray = default(Ray);
		int mask = LayerMask.GetMask(new string[]
		{
			"Terrain",
			"World",
			"Construction"
		});
		int num = this.ShocksBones.Length;
		float num2 = 0.55f;
		float num3 = 0.79f;
		for (int i = 0; i < num; i++)
		{
			ray.origin = this.ShockTraceLineBegin[i].position;
			ray.direction = base.transform.up * -1f;
			RaycastHit raycastHit;
			bool flag = Physics.SphereCast(ray, 0.15f, ref raycastHit, num3, mask);
			float num4;
			if (flag)
			{
				num4 = raycastHit.distance - num2;
			}
			else
			{
				num4 = 0.26f;
			}
			this.vecShocksOffsetPosition[i].y = Mathf.Lerp(this.vecShocksOffsetPosition[i].y, Mathf.Clamp(num4 * -1f, -0.26f, 0f), Time.deltaTime * 5f);
			this.ShocksBones[i].localPosition = this.vecShocksOffsetPosition[i];
		}
	}

	// Token: 0x060015DC RID: 5596 RVA: 0x0007DE8C File Offset: 0x0007C08C
	private void TrackTurret()
	{
		if (this.targetTurret != null)
		{
			Vector3 normalized = (this.targetTurret.position - this.turret.position).normalized;
			float num;
			float num2;
			this.CalculateYawPitchOffset(this.turret, this.turret.position, this.targetTurret.position, out num, out num2);
			num = this.NormalizeYaw(num);
			float num3 = Time.deltaTime * this.turretTurnSpeed;
			if (num < -0.5f)
			{
				this.vecTurret.y = (this.vecTurret.y - num3) % 360f;
			}
			else if (num > 0.5f)
			{
				this.vecTurret.y = (this.vecTurret.y + num3) % 360f;
			}
			this.turret.localEulerAngles = this.vecTurret;
			float num4 = Time.deltaTime * this.cannonPitchSpeed;
			this.CalculateYawPitchOffset(this.mainCannon, this.mainCannon.position, this.targetTurret.position, out num, out num2);
			if (num2 < -0.5f)
			{
				this.vecMainCannon.x = this.vecMainCannon.x - num4;
			}
			else if (num2 > 0.5f)
			{
				this.vecMainCannon.x = this.vecMainCannon.x + num4;
			}
			this.vecMainCannon.x = Mathf.Clamp(this.vecMainCannon.x, -55f, 5f);
			this.mainCannon.localEulerAngles = this.vecMainCannon;
			if (num2 < -0.5f)
			{
				this.vecCoaxGun.x = this.vecCoaxGun.x - num4;
			}
			else if (num2 > 0.5f)
			{
				this.vecCoaxGun.x = this.vecCoaxGun.x + num4;
			}
			this.vecCoaxGun.x = Mathf.Clamp(this.vecCoaxGun.x, -65f, 15f);
			this.coaxGun.localEulerAngles = this.vecCoaxGun;
			if (this.rocketsOpen)
			{
				num4 = Time.deltaTime * this.rocketPitchSpeed;
				this.CalculateYawPitchOffset(this.rocketsPitch, this.rocketsPitch.position, this.targetTurret.position, out num, out num2);
				if (num2 < -0.5f)
				{
					this.vecRocketsPitch.x = this.vecRocketsPitch.x - num4;
				}
				else if (num2 > 0.5f)
				{
					this.vecRocketsPitch.x = this.vecRocketsPitch.x + num4;
				}
				this.vecRocketsPitch.x = Mathf.Clamp(this.vecRocketsPitch.x, -45f, 45f);
			}
			else
			{
				this.vecRocketsPitch.x = Mathf.Lerp(this.vecRocketsPitch.x, 0f, Time.deltaTime * 1.7f);
			}
			this.rocketsPitch.localEulerAngles = this.vecRocketsPitch;
		}
	}

	// Token: 0x060015DD RID: 5597 RVA: 0x0007E198 File Offset: 0x0007C398
	private void TrackSpotLight()
	{
		if (this.targetSpotLight != null)
		{
			Vector3 normalized = (this.targetSpotLight.position - this.spotLightYaw.position).normalized;
			float num;
			float num2;
			this.CalculateYawPitchOffset(this.spotLightYaw, this.spotLightYaw.position, this.targetSpotLight.position, out num, out num2);
			num = this.NormalizeYaw(num);
			float num3 = Time.deltaTime * this.spotLightTurnSpeed;
			if (num < -0.5f)
			{
				this.vecSpotLightBase.y = (this.vecSpotLightBase.y - num3) % 360f;
			}
			else if (num > 0.5f)
			{
				this.vecSpotLightBase.y = (this.vecSpotLightBase.y + num3) % 360f;
			}
			this.spotLightYaw.localEulerAngles = this.vecSpotLightBase;
			this.CalculateYawPitchOffset(this.spotLightPitch, this.spotLightPitch.position, this.targetSpotLight.position, out num, out num2);
			if (num2 < -0.5f)
			{
				this.vecSpotLight.x = this.vecSpotLight.x - num3;
			}
			else if (num2 > 0.5f)
			{
				this.vecSpotLight.x = this.vecSpotLight.x + num3;
			}
			this.vecSpotLight.x = Mathf.Clamp(this.vecSpotLight.x, -50f, 50f);
			this.spotLightPitch.localEulerAngles = this.vecSpotLight;
			this.m2Animator.SetFloat("sideMG_pitch", this.vecSpotLight.x, 0.5f, Time.deltaTime);
		}
	}

	// Token: 0x060015DE RID: 5598 RVA: 0x0007E344 File Offset: 0x0007C544
	private void TrackSideGuns()
	{
		for (int i = 0; i < this.sideguns.Length; i++)
		{
			if (!(this.targetSideguns[i] == null))
			{
				Vector3 normalized = (this.targetSideguns[i].position - this.sideguns[i].position).normalized;
				float num;
				float num2;
				this.CalculateYawPitchOffset(this.sideguns[i], this.sideguns[i].position, this.targetSideguns[i].position, out num, out num2);
				num = this.NormalizeYaw(num);
				float num3 = Time.deltaTime * this.sidegunsTurnSpeed;
				if (num < -0.5f)
				{
					Vector3[] array = this.vecSideGunRotation;
					int num4 = i;
					array[num4].y = array[num4].y - num3;
				}
				else if (num > 0.5f)
				{
					Vector3[] array2 = this.vecSideGunRotation;
					int num5 = i;
					array2[num5].y = array2[num5].y + num3;
				}
				if (num2 < -0.5f)
				{
					Vector3[] array3 = this.vecSideGunRotation;
					int num6 = i;
					array3[num6].x = array3[num6].x - num3;
				}
				else if (num2 > 0.5f)
				{
					Vector3[] array4 = this.vecSideGunRotation;
					int num7 = i;
					array4[num7].x = array4[num7].x + num3;
				}
				this.vecSideGunRotation[i].x = Mathf.Clamp(this.vecSideGunRotation[i].x, -45f, 45f);
				this.vecSideGunRotation[i].y = Mathf.Clamp(this.vecSideGunRotation[i].y, -45f, 45f);
				this.sideguns[i].localEulerAngles = this.vecSideGunRotation[i];
			}
		}
	}

	// Token: 0x060015DF RID: 5599 RVA: 0x0007E50C File Offset: 0x0007C70C
	public void CalculateYawPitchOffset(Transform objectTransform, Vector3 vecStart, Vector3 vecEnd, out float yaw, out float pitch)
	{
		Vector3 vector = objectTransform.InverseTransformDirection(vecEnd - vecStart);
		float num = Mathf.Sqrt(vector.x * vector.x + vector.z * vector.z);
		pitch = -Mathf.Atan2(vector.y, num) * 57.2957764f;
		vector = (vecEnd - vecStart).normalized;
		Vector3 forward = objectTransform.forward;
		forward.y = 0f;
		forward.Normalize();
		float num2 = Vector3.Dot(vector, forward);
		float num3 = Vector3.Dot(vector, objectTransform.right);
		float num4 = 360f * num3;
		float num5 = 360f * -num2;
		yaw = (Mathf.Atan2(num4, num5) + 3.14159274f) * 57.2957764f;
	}

	// Token: 0x060015E0 RID: 5600 RVA: 0x0007E5D4 File Offset: 0x0007C7D4
	public float NormalizeYaw(float flYaw)
	{
		float result;
		if (flYaw > 180f)
		{
			result = 360f - flYaw;
		}
		else
		{
			result = flYaw * -1f;
		}
		return result;
	}

	// Token: 0x0400102A RID: 4138
	public Animator m2Animator;

	// Token: 0x0400102B RID: 4139
	public Material treadLeftMaterial;

	// Token: 0x0400102C RID: 4140
	public Material treadRightMaterial;

	// Token: 0x0400102D RID: 4141
	private Rigidbody mainRigidbody;

	// Token: 0x0400102E RID: 4142
	[Header("GunBones")]
	public Transform turret;

	// Token: 0x0400102F RID: 4143
	public Transform mainCannon;

	// Token: 0x04001030 RID: 4144
	public Transform coaxGun;

	// Token: 0x04001031 RID: 4145
	public Transform rocketsPitch;

	// Token: 0x04001032 RID: 4146
	public Transform spotLightYaw;

	// Token: 0x04001033 RID: 4147
	public Transform spotLightPitch;

	// Token: 0x04001034 RID: 4148
	public Transform sideMG;

	// Token: 0x04001035 RID: 4149
	public Transform[] sideguns;

	// Token: 0x04001036 RID: 4150
	[Header("WheelBones")]
	public Transform[] ShocksBones;

	// Token: 0x04001037 RID: 4151
	public Transform[] ShockTraceLineBegin;

	// Token: 0x04001038 RID: 4152
	public Vector3[] vecShocksOffsetPosition;

	// Token: 0x04001039 RID: 4153
	[Header("Targeting")]
	public Transform targetTurret;

	// Token: 0x0400103A RID: 4154
	public Transform targetSpotLight;

	// Token: 0x0400103B RID: 4155
	public Transform[] targetSideguns;

	// Token: 0x0400103C RID: 4156
	private Vector3 vecTurret = new Vector3(0f, 0f, 0f);

	// Token: 0x0400103D RID: 4157
	private Vector3 vecMainCannon = new Vector3(0f, 0f, 0f);

	// Token: 0x0400103E RID: 4158
	private Vector3 vecCoaxGun = new Vector3(0f, 0f, 0f);

	// Token: 0x0400103F RID: 4159
	private Vector3 vecRocketsPitch = new Vector3(0f, 0f, 0f);

	// Token: 0x04001040 RID: 4160
	private Vector3 vecSpotLightBase = new Vector3(0f, 0f, 0f);

	// Token: 0x04001041 RID: 4161
	private Vector3 vecSpotLight = new Vector3(0f, 0f, 0f);

	// Token: 0x04001042 RID: 4162
	private float sideMGPitchValue;

	// Token: 0x04001043 RID: 4163
	[Header("MuzzleFlash locations")]
	public GameObject muzzleflashCannon;

	// Token: 0x04001044 RID: 4164
	public GameObject muzzleflashCoaxGun;

	// Token: 0x04001045 RID: 4165
	public GameObject muzzleflashSideMG;

	// Token: 0x04001046 RID: 4166
	public GameObject[] muzzleflashRockets;

	// Token: 0x04001047 RID: 4167
	public GameObject spotLightHaloSawnpoint;

	// Token: 0x04001048 RID: 4168
	public GameObject[] muzzleflashSideguns;

	// Token: 0x04001049 RID: 4169
	[Header("MuzzleFlash Particle Systems")]
	public global::GameObjectRef machineGunMuzzleFlashFX;

	// Token: 0x0400104A RID: 4170
	public global::GameObjectRef mainCannonFireFX;

	// Token: 0x0400104B RID: 4171
	public global::GameObjectRef rocketLaunchFX;

	// Token: 0x0400104C RID: 4172
	[Header("Misc")]
	public bool rocketsOpen;

	// Token: 0x0400104D RID: 4173
	public Vector3[] vecSideGunRotation;

	// Token: 0x0400104E RID: 4174
	public float treadConstant = 0.14f;

	// Token: 0x0400104F RID: 4175
	public float wheelSpinConstant = 80f;

	// Token: 0x04001050 RID: 4176
	[Header("Gun Movement speeds")]
	public float sidegunsTurnSpeed = 30f;

	// Token: 0x04001051 RID: 4177
	public float turretTurnSpeed = 6f;

	// Token: 0x04001052 RID: 4178
	public float cannonPitchSpeed = 10f;

	// Token: 0x04001053 RID: 4179
	public float rocketPitchSpeed = 20f;

	// Token: 0x04001054 RID: 4180
	public float spotLightTurnSpeed = 60f;

	// Token: 0x04001055 RID: 4181
	public float machineGunSpeed = 20f;

	// Token: 0x04001056 RID: 4182
	private float wheelAngle;
}
