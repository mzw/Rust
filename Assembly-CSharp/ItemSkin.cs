﻿using System;
using Rust.Workshop;
using UnityEngine;

// Token: 0x02000645 RID: 1605
public class ItemSkin : global::SteamInventoryItem
{
	// Token: 0x06002063 RID: 8291 RVA: 0x000B94E0 File Offset: 0x000B76E0
	public void ApplySkin(GameObject obj)
	{
		if (this.Skinnable == null)
		{
			return;
		}
		Skin.Apply(obj, this.Skinnable, this.Materials);
	}

	// Token: 0x04001B52 RID: 6994
	public Skinnable Skinnable;

	// Token: 0x04001B53 RID: 6995
	public Material[] Materials;
}
