﻿using System;
using Oxide.Core;
using UnityEngine;

// Token: 0x020003DD RID: 989
public class BaseTrapTrigger : global::TriggerBase
{
	// Token: 0x06001717 RID: 5911 RVA: 0x00084CA4 File Offset: 0x00082EA4
	internal override GameObject InterestedInObject(GameObject obj)
	{
		obj = base.InterestedInObject(obj);
		if (obj == null)
		{
			return null;
		}
		global::BaseEntity baseEntity = obj.ToBaseEntity();
		if (baseEntity == null)
		{
			return null;
		}
		if (baseEntity.isClient)
		{
			return null;
		}
		return baseEntity.gameObject;
	}

	// Token: 0x06001718 RID: 5912 RVA: 0x00084CF0 File Offset: 0x00082EF0
	internal override void OnObjectAdded(GameObject obj)
	{
		Interface.CallHook("OnTrapSnapped", new object[]
		{
			this,
			obj
		});
		base.OnObjectAdded(obj);
		this._trap.ObjectEntered(obj);
	}

	// Token: 0x06001719 RID: 5913 RVA: 0x00084D2C File Offset: 0x00082F2C
	internal override void OnEmpty()
	{
		base.OnEmpty();
		this._trap.OnEmpty();
	}

	// Token: 0x040011BB RID: 4539
	public global::BaseTrap _trap;
}
