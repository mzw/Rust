﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConVar;
using Network;
using Oxide.Core;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x020000A4 RID: 164
public class VendingMachine : global::StorageContainer
{
	// Token: 0x06000A0A RID: 2570 RVA: 0x000446D8 File Offset: 0x000428D8
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("VendingMachine.OnRpcMessage", 0.1f))
		{
			if (rpc == 245647001u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - BuyItem ");
				}
				using (TimeWarning.New("BuyItem", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("BuyItem", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.BuyItem(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in BuyItem");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 3305100245u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_AddSellOrder ");
				}
				using (TimeWarning.New("RPC_AddSellOrder", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_AddSellOrder", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_AddSellOrder(msg2);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in RPC_AddSellOrder");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 1866810087u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_Broadcast ");
				}
				using (TimeWarning.New("RPC_Broadcast", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_Broadcast", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_Broadcast(msg3);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in RPC_Broadcast");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 3657747351u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_DeleteSellOrder ");
				}
				using (TimeWarning.New("RPC_DeleteSellOrder", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_DeleteSellOrder", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_DeleteSellOrder(msg4);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in RPC_DeleteSellOrder");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
			if (rpc == 738432363u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_OpenAdmin ");
				}
				using (TimeWarning.New("RPC_OpenAdmin", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_OpenAdmin", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_OpenAdmin(msg5);
						}
					}
					catch (Exception ex5)
					{
						player.Kick("RPC Error in RPC_OpenAdmin");
						Debug.LogException(ex5);
					}
				}
				return true;
			}
			if (rpc == 4042233178u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_OpenShop ");
				}
				using (TimeWarning.New("RPC_OpenShop", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_OpenShop", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg6 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_OpenShop(msg6);
						}
					}
					catch (Exception ex6)
					{
						player.Kick("RPC Error in RPC_OpenShop");
						Debug.LogException(ex6);
					}
				}
				return true;
			}
			if (rpc == 210684524u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_RotateVM ");
				}
				using (TimeWarning.New("RPC_RotateVM", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_RotateVM", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg7 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_RotateVM(msg7);
						}
					}
					catch (Exception ex7)
					{
						player.Kick("RPC Error in RPC_RotateVM");
						Debug.LogException(ex7);
					}
				}
				return true;
			}
			if (rpc == 2190956516u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_UpdateShopName ");
				}
				using (TimeWarning.New("RPC_UpdateShopName", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("RPC_UpdateShopName", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg8 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_UpdateShopName(msg8);
						}
					}
					catch (Exception ex8)
					{
						player.Kick("RPC Error in RPC_UpdateShopName");
						Debug.LogException(ex8);
					}
				}
				return true;
			}
			if (rpc == 527502404u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - TransactionStart ");
				}
				using (TimeWarning.New("TransactionStart", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.IsVisible.Test("TransactionStart", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.TransactionStart(rpc3);
						}
					}
					catch (Exception ex9)
					{
						player.Kick("RPC Error in TransactionStart");
						Debug.LogException(ex9);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000A0B RID: 2571 RVA: 0x00045528 File Offset: 0x00043728
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.vendingMachine != null)
		{
			this.shopName = info.msg.vendingMachine.shopName;
			if (info.msg.vendingMachine.sellOrderContainer != null)
			{
				this.sellOrders = info.msg.vendingMachine.sellOrderContainer;
				this.sellOrders.ShouldPool = false;
			}
			if (info.fromDisk && base.isServer)
			{
				this.RefreshSellOrderStockLevel(null);
			}
		}
	}

	// Token: 0x06000A0C RID: 2572 RVA: 0x000455BC File Offset: 0x000437BC
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.vendingMachine = new ProtoBuf.VendingMachine();
		info.msg.vendingMachine.ShouldPool = false;
		info.msg.vendingMachine.shopName = this.shopName;
		if (this.sellOrders != null)
		{
			info.msg.vendingMachine.sellOrderContainer = new ProtoBuf.VendingMachine.SellOrderContainer();
			info.msg.vendingMachine.sellOrderContainer.ShouldPool = false;
			info.msg.vendingMachine.sellOrderContainer.sellOrders = new List<ProtoBuf.VendingMachine.SellOrder>();
			foreach (ProtoBuf.VendingMachine.SellOrder sellOrder in this.sellOrders.sellOrders)
			{
				ProtoBuf.VendingMachine.SellOrder sellOrder2 = new ProtoBuf.VendingMachine.SellOrder();
				sellOrder2.ShouldPool = false;
				sellOrder.CopyTo(sellOrder2);
				info.msg.vendingMachine.sellOrderContainer.sellOrders.Add(sellOrder2);
			}
		}
	}

	// Token: 0x06000A0D RID: 2573 RVA: 0x000456DC File Offset: 0x000438DC
	public override void ServerInit()
	{
		base.ServerInit();
		if (base.isServer)
		{
			this.InstallDefaultSellOrders();
			base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
			this.inventory.onItemAddedRemoved = new Action<global::Item, bool>(this.OnItemAddedOrRemoved);
			this.RefreshSellOrderStockLevel(null);
			global::ItemContainer inventory = this.inventory;
			inventory.canAcceptItem = (Func<global::Item, int, bool>)Delegate.Combine(inventory.canAcceptItem, new Func<global::Item, int, bool>(this.CanAcceptItem));
			this.UpdateMapMarker();
		}
	}

	// Token: 0x06000A0E RID: 2574 RVA: 0x0004575C File Offset: 0x0004395C
	public override void DestroyShared()
	{
		if (this.myMarker)
		{
			this.myMarker.Kill(global::BaseNetworkable.DestroyMode.None);
			this.myMarker = null;
		}
		base.DestroyShared();
	}

	// Token: 0x06000A0F RID: 2575 RVA: 0x00045788 File Offset: 0x00043988
	public override void OnItemAddedOrRemoved(global::Item item, bool added)
	{
		base.OnItemAddedOrRemoved(item, added);
	}

	// Token: 0x06000A10 RID: 2576 RVA: 0x00045794 File Offset: 0x00043994
	public void FullUpdate()
	{
		this.RefreshSellOrderStockLevel(null);
		this.UpdateMapMarker();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000A11 RID: 2577 RVA: 0x000457AC File Offset: 0x000439AC
	protected override void OnInventoryDirty()
	{
		base.OnInventoryDirty();
		base.CancelInvoke(new Action(this.FullUpdate));
		base.Invoke(new Action(this.FullUpdate), 0.2f);
	}

	// Token: 0x06000A12 RID: 2578 RVA: 0x000457E0 File Offset: 0x000439E0
	public void RefreshSellOrderStockLevel(global::ItemDefinition itemDef = null)
	{
		using (List<ProtoBuf.VendingMachine.SellOrder>.Enumerator enumerator = this.sellOrders.sellOrders.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				ProtoBuf.VendingMachine.SellOrder so = enumerator.Current;
				if (itemDef == null || itemDef.itemid == so.itemToSellID)
				{
					if (so.itemToSellIsBP)
					{
						List<global::Item> source = this.inventory.FindItemsByItemID(this.blueprintBaseDef.itemid);
						List<global::Item> list = (from x in source
						where x.blueprintTarget == so.itemToSellID
						select x).ToList<global::Item>();
						ProtoBuf.VendingMachine.SellOrder so3 = so;
						int inStock;
						if (list != null && list.Count<global::Item>() >= 0)
						{
							Interface.CallHook("OnRefreshVendingStock", new object[]
							{
								this,
								itemDef
							});
							inStock = list.Sum((global::Item x) => x.amount) / so.itemToSellAmount;
						}
						else
						{
							inStock = 0;
						}
						so3.inStock = inStock;
					}
					else
					{
						List<global::Item> list2 = this.inventory.FindItemsByItemID(so.itemToSellID);
						ProtoBuf.VendingMachine.SellOrder so2 = so;
						int inStock2;
						if (list2 != null && list2.Count >= 0)
						{
							Interface.CallHook("OnRefreshVendingStock", new object[]
							{
								this,
								itemDef
							});
							inStock2 = list2.Sum((global::Item x) => x.amount) / so.itemToSellAmount;
						}
						else
						{
							inStock2 = 0;
						}
						so2.inStock = inStock2;
					}
				}
			}
		}
	}

	// Token: 0x06000A13 RID: 2579 RVA: 0x000459B4 File Offset: 0x00043BB4
	public bool OutOfStock()
	{
		foreach (ProtoBuf.VendingMachine.SellOrder sellOrder in this.sellOrders.sellOrders)
		{
			if (sellOrder.inStock > 0)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x06000A14 RID: 2580 RVA: 0x00045A24 File Offset: 0x00043C24
	public override void PostServerLoad()
	{
		base.PostServerLoad();
		this.RefreshSellOrderStockLevel(null);
		this.UpdateMapMarker();
	}

	// Token: 0x06000A15 RID: 2581 RVA: 0x00045A3C File Offset: 0x00043C3C
	public void UpdateEmptyFlag()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved1, this.inventory.itemList.Count == 0, false);
	}

	// Token: 0x06000A16 RID: 2582 RVA: 0x00045A60 File Offset: 0x00043C60
	public override void PlayerStoppedLooting(global::BasePlayer player)
	{
		base.PlayerStoppedLooting(player);
		this.UpdateEmptyFlag();
		if (this.vend_Player != null && this.vend_Player == player)
		{
			this.ClearPendingOrder();
		}
	}

	// Token: 0x06000A17 RID: 2583 RVA: 0x00045A98 File Offset: 0x00043C98
	public void InstallDefaultSellOrders()
	{
		this.sellOrders = new ProtoBuf.VendingMachine.SellOrderContainer();
		this.sellOrders.ShouldPool = false;
		this.sellOrders.sellOrders = new List<ProtoBuf.VendingMachine.SellOrder>();
	}

	// Token: 0x06000A18 RID: 2584 RVA: 0x00045AC4 File Offset: 0x00043CC4
	public void SetPendingOrder(global::BasePlayer buyer, int sellOrderId, int numberOfTransactions)
	{
		this.ClearPendingOrder();
		this.vend_Player = buyer;
		this.vend_sellOrderID = sellOrderId;
		this.vend_numberOfTransactions = numberOfTransactions;
		base.SetFlag(global::BaseEntity.Flags.Reserved2, true, false);
		base.ClientRPC<int>(null, "CLIENT_StartVendingSounds", sellOrderId);
	}

	// Token: 0x06000A19 RID: 2585 RVA: 0x00045AFC File Offset: 0x00043CFC
	public void ClearPendingOrder()
	{
		base.CancelInvoke(new Action(this.CompletePendingOrder));
		this.vend_Player = null;
		this.vend_sellOrderID = -1;
		this.vend_numberOfTransactions = -1;
		base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
		base.ClientRPC(null, "CLIENT_CancelVendingSounds");
	}

	// Token: 0x06000A1A RID: 2586 RVA: 0x00045B4C File Offset: 0x00043D4C
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void BuyItem(global::BaseEntity.RPCMessage rpc)
	{
		int num = rpc.read.Int32();
		int num2 = rpc.read.Int32();
		Interface.CallHook("OnBuyVendingItem", new object[]
		{
			this,
			rpc.player,
			num,
			num2
		});
		this.SetPendingOrder(rpc.player, num, num2);
		base.Invoke(new Action(this.CompletePendingOrder), 2.5f);
	}

	// Token: 0x06000A1B RID: 2587 RVA: 0x00045BCC File Offset: 0x00043DCC
	public void CompletePendingOrder()
	{
		this.DoTransaction(this.vend_Player, this.vend_sellOrderID, this.vend_numberOfTransactions);
		this.ClearPendingOrder();
		global::Decay.RadialDecayTouch(base.transform.position, 40f, 2097408);
	}

	// Token: 0x06000A1C RID: 2588 RVA: 0x00045C08 File Offset: 0x00043E08
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void TransactionStart(global::BaseEntity.RPCMessage rpc)
	{
	}

	// Token: 0x06000A1D RID: 2589 RVA: 0x00045C0C File Offset: 0x00043E0C
	public bool DoTransaction(global::BasePlayer buyer, int sellOrderId, int numberOfTransactions = 1)
	{
		if (sellOrderId < 0 || sellOrderId > this.sellOrders.sellOrders.Count)
		{
			return false;
		}
		if (Vector3.Distance(buyer.GetEstimatedWorldPosition(), base.transform.position) > 4f)
		{
			return false;
		}
		object obj = Interface.CallHook("OnVendingTransaction", new object[]
		{
			this,
			buyer,
			sellOrderId,
			numberOfTransactions
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		ProtoBuf.VendingMachine.SellOrder sellOrder = this.sellOrders.sellOrders[sellOrderId];
		List<global::Item> list = this.inventory.FindItemsByItemID(sellOrder.itemToSellID);
		if (sellOrder.itemToSellIsBP)
		{
			list = (from x in this.inventory.FindItemsByItemID(this.blueprintBaseDef.itemid)
			where x.blueprintTarget == sellOrder.itemToSellID
			select x).ToList<global::Item>();
		}
		if (list == null || list.Count == 0)
		{
			return false;
		}
		numberOfTransactions = Mathf.Clamp(numberOfTransactions, 1, (!list[0].hasCondition) ? 1000000 : 1);
		int num = sellOrder.itemToSellAmount * numberOfTransactions;
		int num2 = list.Sum((global::Item x) => x.amount);
		if (num > num2)
		{
			return false;
		}
		List<global::Item> list2 = buyer.inventory.FindItemIDs(sellOrder.currencyID);
		if (sellOrder.currencyIsBP)
		{
			list2 = (from x in buyer.inventory.FindItemIDs(this.blueprintBaseDef.itemid)
			where x.blueprintTarget == sellOrder.currencyID
			select x).ToList<global::Item>();
		}
		list2 = (from x in list2
		where !x.hasCondition || (x.conditionNormalized >= 0.5f && x.maxConditionNormalized > 0.5f)
		select x).ToList<global::Item>();
		if (list2.Count == 0)
		{
			return false;
		}
		int num3 = list2.Sum((global::Item x) => x.amount);
		int num4 = sellOrder.currencyAmountPerItem * numberOfTransactions;
		if (num3 < num4)
		{
			return false;
		}
		this.transactionActive = true;
		int num5 = 0;
		foreach (global::Item item in list2)
		{
			int num6 = Mathf.Min(num4 - num5, item.amount);
			global::Item item2;
			if (item.amount <= num6)
			{
				item2 = item;
			}
			else
			{
				item2 = item.SplitItem(num6);
			}
			if (!item2.MoveToContainer(this.inventory, -1, true))
			{
				item2.Drop(this.inventory.dropPosition, Vector3.zero, default(Quaternion));
			}
			num5 += num6;
			if (num5 >= num4)
			{
				break;
			}
		}
		int num7 = 0;
		foreach (global::Item item3 in list)
		{
			global::Item item4;
			if (item3.amount <= num)
			{
				item4 = item3;
			}
			else
			{
				item4 = item3.SplitItem(num);
			}
			num7 += item4.amount;
			buyer.GiveItem(item4, global::BaseEntity.GiveItemReason.PickedUp);
			if (num7 >= num)
			{
				break;
			}
		}
		this.UpdateEmptyFlag();
		this.transactionActive = false;
		return true;
	}

	// Token: 0x06000A1E RID: 2590 RVA: 0x00045FB8 File Offset: 0x000441B8
	public void SendSellOrders(global::BasePlayer player = null)
	{
		if (player)
		{
			base.ClientRPCPlayer<ProtoBuf.VendingMachine.SellOrderContainer>(null, player, "CLIENT_ReceiveSellOrders", this.sellOrders);
		}
		else
		{
			base.ClientRPC<ProtoBuf.VendingMachine.SellOrderContainer>(null, "CLIENT_ReceiveSellOrders", this.sellOrders);
		}
	}

	// Token: 0x06000A1F RID: 2591 RVA: 0x00045FF0 File Offset: 0x000441F0
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_Broadcast(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		bool b = msg.read.Bit();
		if (this.CanPlayerAdmin(player))
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved4, b, false);
			Interface.CallHook("OnToggleVendingBroadcast", new object[]
			{
				this,
				player
			});
			this.UpdateMapMarker();
		}
	}

	// Token: 0x06000A20 RID: 2592 RVA: 0x0004604C File Offset: 0x0004424C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_UpdateShopName(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		string text = StringEx.ToPrintable(msg.read.String(), 32);
		if (this.CanPlayerAdmin(player))
		{
			this.shopName = text;
			this.UpdateMapMarker();
		}
	}

	// Token: 0x06000A21 RID: 2593 RVA: 0x00046090 File Offset: 0x00044290
	public void UpdateMapMarker()
	{
		if (this.IsBroadcasting())
		{
			bool flag = false;
			if (this.myMarker == null)
			{
				this.myMarker = (global::GameManager.server.CreateEntity(this.mapMarkerPrefab.resourcePath, base.transform.position, Quaternion.identity, true) as global::VendingMachineMapMarker);
				flag = true;
			}
			this.myMarker.SetFlag(global::BaseEntity.Flags.Busy, this.OutOfStock(), false);
			this.myMarker.markerShopName = this.shopName;
			this.myMarker.server_vendingMachine = this;
			if (flag)
			{
				this.myMarker.Spawn();
			}
			else
			{
				this.myMarker.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			}
		}
		else if (this.myMarker)
		{
			this.myMarker.Kill(global::BaseNetworkable.DestroyMode.None);
			this.myMarker = null;
		}
	}

	// Token: 0x06000A22 RID: 2594 RVA: 0x0004616C File Offset: 0x0004436C
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_OpenShop(global::BaseEntity.RPCMessage msg)
	{
		this.SendSellOrders(msg.player);
		this.PlayerOpenLoot(msg.player, this.customerPanel);
		Interface.CallHook("OnOpenVendingShop", new object[]
		{
			this,
			msg.player
		});
	}

	// Token: 0x06000A23 RID: 2595 RVA: 0x000461BC File Offset: 0x000443BC
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_OpenAdmin(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.CanPlayerAdmin(player))
		{
			return;
		}
		this.SendSellOrders(player);
		this.PlayerOpenLoot(player);
		base.ClientRPCPlayer(null, player, "CLIENT_OpenAdminMenu");
		Interface.CallHook("OnOpenVendingAdmin", new object[]
		{
			this,
			player
		});
	}

	// Token: 0x06000A24 RID: 2596 RVA: 0x00046218 File Offset: 0x00044418
	public bool CanAcceptItem(global::Item item, int targetSlot)
	{
		object obj = Interface.CallHook("CanVendingAcceptItem", new object[]
		{
			this,
			item,
			targetSlot
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		global::BasePlayer ownerPlayer = item.GetOwnerPlayer();
		return this.transactionActive || item.parent == null || this.inventory.itemList.Contains(item) || (!(ownerPlayer == null) && this.CanPlayerAdmin(ownerPlayer));
	}

	// Token: 0x06000A25 RID: 2597 RVA: 0x000462A8 File Offset: 0x000444A8
	public override bool CanMoveFrom(global::BasePlayer player, global::Item item)
	{
		return this.CanPlayerAdmin(player);
	}

	// Token: 0x06000A26 RID: 2598 RVA: 0x000462B4 File Offset: 0x000444B4
	public override bool CanOpenLootPanel(global::BasePlayer player, string panelName = "")
	{
		object obj = Interface.CallHook("CanUseVending", new object[]
		{
			this,
			player
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return panelName == this.customerPanel || (base.CanOpenLootPanel(player, panelName) && this.CanPlayerAdmin(player));
	}

	// Token: 0x06000A27 RID: 2599 RVA: 0x00046318 File Offset: 0x00044518
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	public void RPC_DeleteSellOrder(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.CanPlayerAdmin(player))
		{
			return;
		}
		int num = msg.read.Int32();
		if (num >= 0 && num < this.sellOrders.sellOrders.Count)
		{
			this.sellOrders.sellOrders.RemoveAt(num);
		}
		Interface.CallHook("OnDeleteVendingOffer", new object[]
		{
			this,
			player
		});
		this.RefreshSellOrderStockLevel(null);
		this.UpdateMapMarker();
		this.SendSellOrders(player);
	}

	// Token: 0x06000A28 RID: 2600 RVA: 0x000463A4 File Offset: 0x000445A4
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_RotateVM(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (Interface.CallHook("OnRotateVendingMachine", new object[]
		{
			this,
			player
		}) != null)
		{
			return;
		}
		if (player.CanBuild() && this.IsInventoryEmpty())
		{
			base.transform.rotation = Quaternion.LookRotation(-base.transform.forward, base.transform.up);
			base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		}
	}

	// Token: 0x06000A29 RID: 2601 RVA: 0x00046420 File Offset: 0x00044620
	[global::BaseEntity.RPC_Server.IsVisible(3f)]
	[global::BaseEntity.RPC_Server]
	public void RPC_AddSellOrder(global::BaseEntity.RPCMessage msg)
	{
		global::BasePlayer player = msg.player;
		if (!this.CanPlayerAdmin(player))
		{
			return;
		}
		if (this.sellOrders.sellOrders.Count >= 7)
		{
			player.ChatMessage("Too many sell orders - remove some");
			return;
		}
		int num = msg.read.Int32();
		int num2 = msg.read.Int32();
		int num3 = msg.read.Int32();
		int num4 = msg.read.Int32();
		byte b = msg.read.UInt8();
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(num);
		global::ItemDefinition itemDefinition2 = global::ItemManager.FindItemDefinition(num3);
		if (itemDefinition == null || itemDefinition2 == null)
		{
			return;
		}
		num4 = Mathf.Clamp(num4, 1, 10000);
		num2 = Mathf.Clamp(num2, 1, itemDefinition.stackable);
		ProtoBuf.VendingMachine.SellOrder sellOrder = new ProtoBuf.VendingMachine.SellOrder();
		sellOrder.ShouldPool = false;
		sellOrder.itemToSellID = num;
		sellOrder.itemToSellAmount = num2;
		sellOrder.currencyID = num3;
		sellOrder.currencyAmountPerItem = num4;
		sellOrder.currencyIsBP = (b == 3 || b == 2);
		sellOrder.itemToSellIsBP = (b == 3 || b == 1);
		this.sellOrders.sellOrders.Add(sellOrder);
		this.RefreshSellOrderStockLevel(itemDefinition);
		Interface.CallHook("OnAddVendingOffer", new object[]
		{
			this,
			player,
			itemDefinition2
		});
		this.UpdateMapMarker();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000A2A RID: 2602 RVA: 0x0004659C File Offset: 0x0004479C
	public void RefreshAndSendNetworkUpdate()
	{
		this.RefreshSellOrderStockLevel(null);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000A2B RID: 2603 RVA: 0x000465AC File Offset: 0x000447AC
	public void UpdateOrCreateSalesSheet()
	{
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition("note");
		List<global::Item> list = this.inventory.FindItemsByItemID(itemDefinition.itemid);
		global::Item item = null;
		foreach (global::Item item2 in list)
		{
			if (item2.text.Length == 0)
			{
				item = item2;
				break;
			}
		}
		if (item == null)
		{
			global::ItemDefinition itemDefinition2 = global::ItemManager.FindItemDefinition("paper");
			global::Item item3 = this.inventory.FindItemByItemID(itemDefinition2.itemid);
			if (item3 != null)
			{
				item = global::ItemManager.CreateByItemID(itemDefinition.itemid, 1, 0UL);
				if (!item.MoveToContainer(this.inventory, -1, true))
				{
					item.Drop(this.GetDropPosition(), this.GetDropVelocity(), default(Quaternion));
				}
				item3.UseItem(1);
			}
		}
		if (item != null)
		{
			foreach (ProtoBuf.VendingMachine.SellOrder sellOrder in this.sellOrders.sellOrders)
			{
				global::ItemDefinition itemDefinition3 = global::ItemManager.FindItemDefinition(sellOrder.itemToSellID);
				global::Item item4 = item;
				item4.text = item4.text + itemDefinition3.displayName.translated + "\n";
			}
			item.MarkDirty();
		}
	}

	// Token: 0x06000A2C RID: 2604 RVA: 0x00046730 File Offset: 0x00044930
	public bool IsBroadcasting()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved4);
	}

	// Token: 0x06000A2D RID: 2605 RVA: 0x00046740 File Offset: 0x00044940
	public bool IsInventoryEmpty()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x06000A2E RID: 2606 RVA: 0x00046750 File Offset: 0x00044950
	public bool IsVending()
	{
		return base.HasFlag(global::BaseEntity.Flags.Reserved2);
	}

	// Token: 0x06000A2F RID: 2607 RVA: 0x00046760 File Offset: 0x00044960
	public bool PlayerBehind(global::BasePlayer player)
	{
		return Vector3.Dot(base.transform.forward, (player.GetEstimatedWorldPosition() - base.transform.position).normalized) <= -0.7f;
	}

	// Token: 0x06000A30 RID: 2608 RVA: 0x000467A8 File Offset: 0x000449A8
	public bool PlayerInfront(global::BasePlayer player)
	{
		return Vector3.Dot(base.transform.forward, (player.GetEstimatedWorldPosition() - base.transform.position).normalized) >= 0.7f;
	}

	// Token: 0x06000A31 RID: 2609 RVA: 0x000467F0 File Offset: 0x000449F0
	public bool CanPlayerAdmin(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanAdministerVending", new object[]
		{
			this,
			player
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.PlayerBehind(player);
	}

	// Token: 0x040004B8 RID: 1208
	[Header("VendingMachine")]
	public GameObject adminMenuPrefab;

	// Token: 0x040004B9 RID: 1209
	public string customerPanel = string.Empty;

	// Token: 0x040004BA RID: 1210
	public ProtoBuf.VendingMachine.SellOrderContainer sellOrders;

	// Token: 0x040004BB RID: 1211
	public global::SoundPlayer buySound;

	// Token: 0x040004BC RID: 1212
	public string shopName = "A Shop";

	// Token: 0x040004BD RID: 1213
	public global::GameObjectRef mapMarkerPrefab;

	// Token: 0x040004BE RID: 1214
	public global::ItemDefinition blueprintBaseDef;

	// Token: 0x040004BF RID: 1215
	private global::BasePlayer vend_Player;

	// Token: 0x040004C0 RID: 1216
	private int vend_sellOrderID;

	// Token: 0x040004C1 RID: 1217
	private int vend_numberOfTransactions;

	// Token: 0x040004C2 RID: 1218
	public bool transactionActive;

	// Token: 0x040004C3 RID: 1219
	private global::VendingMachineMapMarker myMarker;

	// Token: 0x020000A5 RID: 165
	public static class VendingMachineFlags
	{
		// Token: 0x040004C9 RID: 1225
		public const global::BaseEntity.Flags EmptyInv = global::BaseEntity.Flags.Reserved1;

		// Token: 0x040004CA RID: 1226
		public const global::BaseEntity.Flags IsVending = global::BaseEntity.Flags.Reserved2;

		// Token: 0x040004CB RID: 1227
		public const global::BaseEntity.Flags Broadcasting = global::BaseEntity.Flags.Reserved4;

		// Token: 0x040004CC RID: 1228
		public const global::BaseEntity.Flags OutOfStock = global::BaseEntity.Flags.Reserved5;
	}
}
