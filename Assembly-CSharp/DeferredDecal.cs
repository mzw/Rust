﻿using System;
using UnityEngine;

// Token: 0x020005F6 RID: 1526
[ExecuteInEditMode]
public class DeferredDecal : MonoBehaviour
{
	// Token: 0x04001A2F RID: 6703
	public bool StickyGizmos;

	// Token: 0x04001A30 RID: 6704
	public Mesh mesh;

	// Token: 0x04001A31 RID: 6705
	public Material material;

	// Token: 0x04001A32 RID: 6706
	public global::DeferredDecalQueue queue;
}
