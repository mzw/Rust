﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000308 RID: 776
public class GameContentList : MonoBehaviour
{
	// Token: 0x04000E0D RID: 3597
	public global::GameContentList.ResourceType resourceType;

	// Token: 0x04000E0E RID: 3598
	public List<Object> foundObjects;

	// Token: 0x02000309 RID: 777
	public enum ResourceType
	{
		// Token: 0x04000E10 RID: 3600
		Audio,
		// Token: 0x04000E11 RID: 3601
		Textures,
		// Token: 0x04000E12 RID: 3602
		Models
	}
}
