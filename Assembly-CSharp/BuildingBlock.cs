﻿using System;
using System.Collections.Generic;
using ConVar;
using Facepunch;
using Network;
using Oxide.Core;
using ProtoBuf;
using Rust;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000055 RID: 85
public class BuildingBlock : global::StabilityEntity
{
	// Token: 0x060006D3 RID: 1747 RVA: 0x0002ADB4 File Offset: 0x00028FB4
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BuildingBlock.OnRpcMessage", 0.1f))
		{
			if (rpc == 3916846720u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoDemolish ");
				}
				using (TimeWarning.New("DoDemolish", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("DoDemolish", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoDemolish(msg2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in DoDemolish");
						Debug.LogException(ex);
					}
				}
				return true;
			}
			if (rpc == 848803451u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoImmediateDemolish ");
				}
				using (TimeWarning.New("DoImmediateDemolish", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("DoImmediateDemolish", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg3 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoImmediateDemolish(msg3);
						}
					}
					catch (Exception ex2)
					{
						player.Kick("RPC Error in DoImmediateDemolish");
						Debug.LogException(ex2);
					}
				}
				return true;
			}
			if (rpc == 3019202825u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoRotation ");
				}
				using (TimeWarning.New("DoRotation", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("DoRotation", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg4 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoRotation(msg4);
						}
					}
					catch (Exception ex3)
					{
						player.Kick("RPC Error in DoRotation");
						Debug.LogException(ex3);
					}
				}
				return true;
			}
			if (rpc == 1059213803u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - DoUpgradeToGrade ");
				}
				using (TimeWarning.New("DoUpgradeToGrade", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("DoUpgradeToGrade", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage msg5 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.DoUpgradeToGrade(msg5);
						}
					}
					catch (Exception ex4)
					{
						player.Kick("RPC Error in DoUpgradeToGrade");
						Debug.LogException(ex4);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x060006D4 RID: 1748 RVA: 0x0002B43C File Offset: 0x0002963C
	public override void ResetState()
	{
		base.ResetState();
		this.blockDefinition = null;
		this.modelState = 0;
		this.lastModelState = 0;
		this.grade = global::BuildingGrade.Enum.Twigs;
		this.lastGrade = global::BuildingGrade.Enum.None;
		this.DestroySkin();
		this.UpdatePlaceholder(true);
	}

	// Token: 0x060006D5 RID: 1749 RVA: 0x0002B474 File Offset: 0x00029674
	public override void InitShared()
	{
		base.InitShared();
		this.placeholderRenderer = base.GetComponent<MeshRenderer>();
		this.placeholderCollider = base.GetComponent<MeshCollider>();
	}

	// Token: 0x060006D6 RID: 1750 RVA: 0x0002B494 File Offset: 0x00029694
	public override void PostInitShared()
	{
		this.baseProtection = this.currentGrade.gradeBase.damageProtecton;
		this.grade = this.currentGrade.gradeBase.type;
		base.PostInitShared();
	}

	// Token: 0x060006D7 RID: 1751 RVA: 0x0002B4C8 File Offset: 0x000296C8
	public override void DestroyShared()
	{
		if (base.isServer)
		{
			this.RefreshNeighbours(false);
		}
		base.DestroyShared();
	}

	// Token: 0x060006D8 RID: 1752 RVA: 0x0002B4E4 File Offset: 0x000296E4
	public override string Categorize()
	{
		return "building";
	}

	// Token: 0x060006D9 RID: 1753 RVA: 0x0002B4EC File Offset: 0x000296EC
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x060006DA RID: 1754 RVA: 0x0002B4F0 File Offset: 0x000296F0
	public override float BoundsPadding()
	{
		return 1f;
	}

	// Token: 0x060006DB RID: 1755 RVA: 0x0002B4F8 File Offset: 0x000296F8
	public override bool IsOutside()
	{
		float outside_test_range = ConVar.Decay.outside_test_range;
		Vector3 vector = base.PivotPoint();
		for (int i = 0; i < global::BuildingBlock.outsideLookupOffsets.Length; i++)
		{
			Vector3 vector2 = global::BuildingBlock.outsideLookupOffsets[i];
			Vector3 vector3 = vector + vector2 * outside_test_range;
			Ray ray;
			ray..ctor(vector3, -vector2);
			if (!UnityEngine.Physics.Raycast(ray, outside_test_range - 0.5f, 2097152))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060006DC RID: 1756 RVA: 0x0002B574 File Offset: 0x00029774
	private bool CanDemolish(global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanDemolish", new object[]
		{
			player,
			this
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.IsDemolishable() && this.HasDemolishPrivilege(player);
	}

	// Token: 0x060006DD RID: 1757 RVA: 0x0002B5C4 File Offset: 0x000297C4
	private bool IsDemolishable()
	{
		return ConVar.Server.pve || base.HasFlag(global::BaseEntity.Flags.Reserved2);
	}

	// Token: 0x060006DE RID: 1758 RVA: 0x0002B5E4 File Offset: 0x000297E4
	private bool HasDemolishPrivilege(global::BasePlayer player)
	{
		return player.IsBuildingAuthed(base.transform.position, base.transform.rotation, this.bounds);
	}

	// Token: 0x060006DF RID: 1759 RVA: 0x0002B608 File Offset: 0x00029808
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void DoDemolish(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (!this.CanDemolish(msg.player))
		{
			return;
		}
		if (Interface.CallHook("OnStructureDemolish", new object[]
		{
			this,
			msg.player,
			false
		}) != null)
		{
			return;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.Gib);
	}

	// Token: 0x060006E0 RID: 1760 RVA: 0x0002B670 File Offset: 0x00029870
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void DoImmediateDemolish(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (!msg.player.IsAdmin)
		{
			return;
		}
		if (Interface.CallHook("OnStructureDemolish", new object[]
		{
			this,
			msg.player,
			true
		}) != null)
		{
			return;
		}
		base.Kill(global::BaseNetworkable.DestroyMode.Gib);
	}

	// Token: 0x060006E1 RID: 1761 RVA: 0x0002B6D4 File Offset: 0x000298D4
	public void StopBeingDemolishable()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x060006E2 RID: 1762 RVA: 0x0002B6EC File Offset: 0x000298EC
	public void StartBeingDemolishable()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved2, true, false);
		base.Invoke(new Action(this.StopBeingDemolishable), 600f);
	}

	// Token: 0x060006E3 RID: 1763 RVA: 0x0002B714 File Offset: 0x00029914
	public void SetConditionalModel(int state)
	{
		this.modelState = state;
	}

	// Token: 0x060006E4 RID: 1764 RVA: 0x0002B720 File Offset: 0x00029920
	public bool GetConditionalModel(int index)
	{
		return (this.modelState & 1 << index) != 0;
	}

	// Token: 0x1700005A RID: 90
	// (get) Token: 0x060006E5 RID: 1765 RVA: 0x0002B738 File Offset: 0x00029938
	public global::ConstructionGrade currentGrade
	{
		get
		{
			global::ConstructionGrade constructionGrade = this.GetGrade(this.grade);
			if (constructionGrade != null)
			{
				return constructionGrade;
			}
			for (int i = 0; i < this.blockDefinition.grades.Length; i++)
			{
				if (this.blockDefinition.grades[i] != null)
				{
					return this.blockDefinition.grades[i];
				}
			}
			Debug.LogWarning("Building block grade not found: " + this.grade);
			return null;
		}
	}

	// Token: 0x060006E6 RID: 1766 RVA: 0x0002B7C0 File Offset: 0x000299C0
	private global::ConstructionGrade GetGrade(global::BuildingGrade.Enum iGrade)
	{
		if (this.grade >= (global::BuildingGrade.Enum)this.blockDefinition.grades.Length)
		{
			Debug.LogWarning(string.Concat(new object[]
			{
				"Grade out of range ",
				base.gameObject,
				" ",
				this.grade,
				" / ",
				this.blockDefinition.grades.Length
			}));
			return this.blockDefinition.defaultGrade;
		}
		return this.blockDefinition.grades[(int)iGrade];
	}

	// Token: 0x060006E7 RID: 1767 RVA: 0x0002B854 File Offset: 0x00029A54
	private bool CanChangeToGrade(global::BuildingGrade.Enum iGrade, global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanChangeGrade", new object[]
		{
			player,
			this,
			iGrade
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return this.HasUpgradePrivilege(iGrade, player) && !this.IsUpgradeBlocked();
	}

	// Token: 0x060006E8 RID: 1768 RVA: 0x0002B8B0 File Offset: 0x00029AB0
	private bool HasUpgradePrivilege(global::BuildingGrade.Enum iGrade, global::BasePlayer player)
	{
		return iGrade != this.grade && iGrade < (global::BuildingGrade.Enum)this.blockDefinition.grades.Length && iGrade >= global::BuildingGrade.Enum.Twigs && iGrade >= this.grade && !player.IsBuildingBlocked(base.transform.position, base.transform.rotation, this.bounds);
	}

	// Token: 0x060006E9 RID: 1769 RVA: 0x0002B91C File Offset: 0x00029B1C
	private bool IsUpgradeBlocked()
	{
		if (!this.blockDefinition.checkVolumeOnUpgrade)
		{
			return false;
		}
		global::DeployVolume[] volumes = global::PrefabAttribute.server.FindAll<global::DeployVolume>(this.prefabID);
		return global::DeployVolume.Check(base.transform.position, base.transform.rotation, volumes, ~(1 << base.gameObject.layer));
	}

	// Token: 0x060006EA RID: 1770 RVA: 0x0002B97C File Offset: 0x00029B7C
	private bool CanAffordUpgrade(global::BuildingGrade.Enum iGrade, global::BasePlayer player)
	{
		object obj = Interface.CallHook("CanAffordUpgrade", new object[]
		{
			player,
			this,
			iGrade
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		global::ConstructionGrade constructionGrade = this.GetGrade(iGrade);
		foreach (global::ItemAmount itemAmount in constructionGrade.costToBuild)
		{
			if ((float)player.inventory.GetAmount(itemAmount.itemid) < itemAmount.amount)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x060006EB RID: 1771 RVA: 0x0002BA3C File Offset: 0x00029C3C
	public void SetGrade(global::BuildingGrade.Enum iGradeID)
	{
		if (this.blockDefinition.grades == null || iGradeID >= (global::BuildingGrade.Enum)this.blockDefinition.grades.Length)
		{
			Debug.LogError("Tried to set to undefined grade! " + this.blockDefinition.fullName, base.gameObject);
			return;
		}
		this.grade = iGradeID;
		this.grade = this.currentGrade.gradeBase.type;
		this.UpdateGrade();
	}

	// Token: 0x060006EC RID: 1772 RVA: 0x0002BAB0 File Offset: 0x00029CB0
	private void UpdateGrade()
	{
		this.baseProtection = this.currentGrade.gradeBase.damageProtecton;
	}

	// Token: 0x060006ED RID: 1773 RVA: 0x0002BAC8 File Offset: 0x00029CC8
	public void SetHealthToMax()
	{
		base.health = this.MaxHealth();
	}

	// Token: 0x060006EE RID: 1774 RVA: 0x0002BAD8 File Offset: 0x00029CD8
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	private void DoUpgradeToGrade(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		global::BuildingGrade.Enum @enum = (global::BuildingGrade.Enum)msg.read.Int32();
		global::ConstructionGrade constructionGrade = this.GetGrade(@enum);
		if (constructionGrade == null)
		{
			return;
		}
		if (!this.CanChangeToGrade(@enum, msg.player))
		{
			return;
		}
		if (!this.CanAffordUpgrade(@enum, msg.player))
		{
			return;
		}
		if (base.SecondsSinceAttacked < 30f)
		{
			return;
		}
		if (Interface.CallHook("OnStructureUpgrade", new object[]
		{
			this,
			msg.player,
			@enum
		}) != null)
		{
			return;
		}
		this.PayForUpgrade(constructionGrade, msg.player);
		this.SetGrade(@enum);
		this.SetHealthToMax();
		this.StartBeingRotatable();
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
		this.UpdateSkin(false);
		base.ResetUpkeepTime();
		global::BuildingManager.Building building = global::BuildingManager.server.GetBuilding(this.buildingID);
		if (building != null)
		{
			building.Dirty();
		}
		global::Effect.server.Run("assets/bundled/prefabs/fx/build/promote_" + @enum.ToString().ToLower() + ".prefab", this, 0u, Vector3.zero, Vector3.zero, null, false);
	}

	// Token: 0x060006EF RID: 1775 RVA: 0x0002BC04 File Offset: 0x00029E04
	private void PayForUpgrade(global::ConstructionGrade g, global::BasePlayer player)
	{
		List<global::Item> list = new List<global::Item>();
		foreach (global::ItemAmount itemAmount in g.costToBuild)
		{
			player.inventory.Take(list, itemAmount.itemid, (int)itemAmount.amount);
			player.Command(string.Concat(new object[]
			{
				"note.inv ",
				itemAmount.itemid,
				" ",
				itemAmount.amount * -1f
			}), new object[0]);
		}
		foreach (global::Item item in list)
		{
			item.Remove(0f);
		}
	}

	// Token: 0x060006F0 RID: 1776 RVA: 0x0002BD10 File Offset: 0x00029F10
	private bool NeedsSkinChange(bool force = false)
	{
		return this.currentSkin == null || force || this.lastGrade != this.grade || this.lastModelState != this.modelState;
	}

	// Token: 0x060006F1 RID: 1777 RVA: 0x0002BD60 File Offset: 0x00029F60
	public void UpdateSkin(bool force = false)
	{
		if (!this.NeedsSkinChange(force))
		{
			return;
		}
		if (this.cachedStability <= 0f || base.isServer)
		{
			this.ChangeSkin();
			return;
		}
		if (!this.skinChange)
		{
			this.skinChange = new global::DeferredAction(this, new Action(this.ChangeSkin), global::ActionPriority.Medium);
		}
		if (!this.skinChange.Idle)
		{
			return;
		}
		this.skinChange.Invoke();
	}

	// Token: 0x060006F2 RID: 1778 RVA: 0x0002BDE4 File Offset: 0x00029FE4
	private void DestroySkin()
	{
		if (this.currentSkin != null)
		{
			this.currentSkin.Destroy(this);
			this.currentSkin = null;
		}
	}

	// Token: 0x060006F3 RID: 1779 RVA: 0x0002BE0C File Offset: 0x0002A00C
	private void RefreshNeighbours(bool linkToNeighbours)
	{
		List<global::EntityLink> entityLinks = base.GetEntityLinks(linkToNeighbours);
		for (int i = 0; i < entityLinks.Count; i++)
		{
			global::EntityLink entityLink = entityLinks[i];
			for (int j = 0; j < entityLink.connections.Count; j++)
			{
				global::BuildingBlock buildingBlock = entityLink.connections[j].owner as global::BuildingBlock;
				if (!(buildingBlock == null))
				{
					if (Application.isLoading)
					{
						buildingBlock.UpdateSkin(true);
					}
					else
					{
						global::BuildingBlock.updateSkinQueueServer.Add(buildingBlock);
					}
				}
			}
		}
	}

	// Token: 0x060006F4 RID: 1780 RVA: 0x0002BEAC File Offset: 0x0002A0AC
	private void UpdatePlaceholder(bool state)
	{
		if (this.placeholderRenderer)
		{
			this.placeholderRenderer.enabled = state;
		}
		if (this.placeholderCollider)
		{
			this.placeholderCollider.enabled = state;
		}
	}

	// Token: 0x060006F5 RID: 1781 RVA: 0x0002BEE8 File Offset: 0x0002A0E8
	private void ChangeSkin()
	{
		if (base.IsDestroyed)
		{
			return;
		}
		global::ConstructionGrade currentGrade = this.currentGrade;
		if (currentGrade.skinObject.isValid)
		{
			this.ChangeSkin(currentGrade.skinObject);
			return;
		}
		foreach (global::ConstructionGrade constructionGrade in this.blockDefinition.grades)
		{
			if (constructionGrade.skinObject.isValid)
			{
				this.ChangeSkin(constructionGrade.skinObject);
				return;
			}
		}
		Debug.LogWarning("No skins found for " + base.gameObject);
	}

	// Token: 0x060006F6 RID: 1782 RVA: 0x0002BF80 File Offset: 0x0002A180
	private void ChangeSkin(global::GameObjectRef prefab)
	{
		bool flag = this.lastGrade != this.grade;
		this.lastGrade = this.grade;
		if (flag)
		{
			if (this.currentSkin == null)
			{
				this.UpdatePlaceholder(false);
			}
			else
			{
				this.DestroySkin();
			}
			GameObject gameObject = base.gameManager.CreatePrefab(prefab.resourcePath, base.transform, false);
			gameObject.AwakeFromInstantiate();
			this.currentSkin = gameObject.GetComponent<global::ConstructionSkin>();
			global::Model component = this.currentSkin.GetComponent<global::Model>();
			base.SetModel(component);
			Assert.IsTrue(this.model == component, "Didn't manage to set model successfully!");
		}
		if (base.isServer)
		{
			this.modelState = this.currentSkin.DetermineConditionalModelState(this);
		}
		bool flag2 = this.lastModelState != this.modelState;
		this.lastModelState = this.modelState;
		if (flag || flag2)
		{
			this.currentSkin.Refresh(this);
		}
		if (base.isServer)
		{
			if (flag)
			{
				this.RefreshNeighbours(true);
			}
			if (flag2)
			{
				base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
			}
		}
	}

	// Token: 0x060006F7 RID: 1783 RVA: 0x0002C0A0 File Offset: 0x0002A2A0
	public override bool ShouldBlockProjectiles()
	{
		return this.grade != global::BuildingGrade.Enum.Twigs;
	}

	// Token: 0x060006F8 RID: 1784 RVA: 0x0002C0B0 File Offset: 0x0002A2B0
	private void OnHammered()
	{
	}

	// Token: 0x060006F9 RID: 1785 RVA: 0x0002C0B4 File Offset: 0x0002A2B4
	public override float MaxHealth()
	{
		return this.currentGrade.maxHealth;
	}

	// Token: 0x060006FA RID: 1786 RVA: 0x0002C0C4 File Offset: 0x0002A2C4
	public override List<global::ItemAmount> BuildCost()
	{
		return this.currentGrade.costToBuild;
	}

	// Token: 0x060006FB RID: 1787 RVA: 0x0002C0D4 File Offset: 0x0002A2D4
	public override void OnHealthChanged(float oldvalue, float newvalue)
	{
		base.OnHealthChanged(oldvalue, newvalue);
		if (Mathf.RoundToInt(oldvalue) == Mathf.RoundToInt(newvalue))
		{
			return;
		}
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.UpdateDistance);
	}

	// Token: 0x060006FC RID: 1788 RVA: 0x0002C0F8 File Offset: 0x0002A2F8
	public override float RepairCostFraction()
	{
		return 1f;
	}

	// Token: 0x060006FD RID: 1789 RVA: 0x0002C100 File Offset: 0x0002A300
	private bool CanRotate(global::BasePlayer player)
	{
		return this.IsRotatable() && this.HasRotationPrivilege(player) && !this.IsRotationBlocked();
	}

	// Token: 0x060006FE RID: 1790 RVA: 0x0002C128 File Offset: 0x0002A328
	private bool IsRotatable()
	{
		return this.blockDefinition.grades != null && this.blockDefinition.canRotate && base.HasFlag(global::BaseEntity.Flags.Reserved1);
	}

	// Token: 0x060006FF RID: 1791 RVA: 0x0002C164 File Offset: 0x0002A364
	private bool IsRotationBlocked()
	{
		if (!this.blockDefinition.checkVolumeOnRotate)
		{
			return false;
		}
		global::DeployVolume[] volumes = global::PrefabAttribute.server.FindAll<global::DeployVolume>(this.prefabID);
		return global::DeployVolume.Check(base.transform.position, base.transform.rotation, volumes, ~(1 << base.gameObject.layer));
	}

	// Token: 0x06000700 RID: 1792 RVA: 0x0002C1C4 File Offset: 0x0002A3C4
	private bool HasRotationPrivilege(global::BasePlayer player)
	{
		return !player.IsBuildingBlocked(base.transform.position, base.transform.rotation, this.bounds);
	}

	// Token: 0x06000701 RID: 1793 RVA: 0x0002C1EC File Offset: 0x0002A3EC
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	[global::BaseEntity.RPC_Server]
	private void DoRotation(global::BaseEntity.RPCMessage msg)
	{
		if (!msg.player.CanInteract())
		{
			return;
		}
		if (!this.CanRotate(msg.player))
		{
			return;
		}
		if (!this.blockDefinition.canRotate)
		{
			return;
		}
		if (Interface.CallHook("OnStructureRotate", new object[]
		{
			this,
			msg.player
		}) != null)
		{
			return;
		}
		base.transform.localRotation *= Quaternion.Euler(this.blockDefinition.rotationAmount);
		base.RefreshEntityLinks();
		base.UpdateSurroundingEntities();
		if (this.currentSkin)
		{
			this.currentSkin.Refresh(this);
		}
		base.SendNetworkUpdateImmediate(false);
		base.ClientRPC(null, "RefreshSkin");
	}

	// Token: 0x06000702 RID: 1794 RVA: 0x0002C2B4 File Offset: 0x0002A4B4
	public void StopBeingRotatable()
	{
		base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
		base.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06000703 RID: 1795 RVA: 0x0002C2CC File Offset: 0x0002A4CC
	public void StartBeingRotatable()
	{
		if (this.blockDefinition.grades == null)
		{
			return;
		}
		if (!this.blockDefinition.canRotate)
		{
			return;
		}
		base.SetFlag(global::BaseEntity.Flags.Reserved1, true, false);
		base.Invoke(new Action(this.StopBeingRotatable), 600f);
	}

	// Token: 0x06000704 RID: 1796 RVA: 0x0002C320 File Offset: 0x0002A520
	public override void Save(global::BaseNetworkable.SaveInfo info)
	{
		base.Save(info);
		info.msg.buildingBlock = Facepunch.Pool.Get<ProtoBuf.BuildingBlock>();
		info.msg.buildingBlock.model = this.modelState;
		info.msg.buildingBlock.grade = (int)this.grade;
	}

	// Token: 0x06000705 RID: 1797 RVA: 0x0002C374 File Offset: 0x0002A574
	public override void Load(global::BaseNetworkable.LoadInfo info)
	{
		base.Load(info);
		if (info.msg.buildingBlock != null)
		{
			this.SetConditionalModel(info.msg.buildingBlock.model);
			this.SetGrade((global::BuildingGrade.Enum)info.msg.buildingBlock.grade);
		}
		if (info.fromDisk)
		{
			base.SetFlag(global::BaseEntity.Flags.Reserved2, false, false);
			base.SetFlag(global::BaseEntity.Flags.Reserved1, false, false);
			this.UpdateSkin(false);
		}
	}

	// Token: 0x06000706 RID: 1798 RVA: 0x0002C3F4 File Offset: 0x0002A5F4
	public override void ServerInit()
	{
		this.blockDefinition = global::PrefabAttribute.server.Find<global::Construction>(this.prefabID);
		if (this.blockDefinition == null)
		{
			Debug.LogError("Couldn't find Construction for prefab " + this.prefabID);
		}
		base.ServerInit();
		this.UpdateSkin(false);
		if (base.HasFlag(global::BaseEntity.Flags.Reserved1) || !Application.isLoadingSave)
		{
			this.StartBeingRotatable();
		}
		if (base.HasFlag(global::BaseEntity.Flags.Reserved2) || !Application.isLoadingSave)
		{
			this.StartBeingDemolishable();
		}
	}

	// Token: 0x06000707 RID: 1799 RVA: 0x0002C490 File Offset: 0x0002A690
	public override void Hurt(global::HitInfo info)
	{
		if (ConVar.Server.pve && info.Initiator && info.Initiator is global::BasePlayer)
		{
			global::BasePlayer basePlayer = info.Initiator as global::BasePlayer;
			basePlayer.Hurt(info.damageTypes.Total(), Rust.DamageType.Generic, null, true);
			return;
		}
		base.Hurt(info);
	}

	// Token: 0x06000708 RID: 1800 RVA: 0x0002C4F0 File Offset: 0x0002A6F0
	public override void OnKilled(global::HitInfo info)
	{
		base.Kill(global::BaseNetworkable.DestroyMode.Gib);
	}

	// Token: 0x06000709 RID: 1801 RVA: 0x0002C4FC File Offset: 0x0002A6FC
	static BuildingBlock()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Vector3[] array = new Vector3[5];
		int num = 0;
		Vector3 vector;
		vector..ctor(0f, 1f, 0f);
		array[num] = vector.normalized;
		int num2 = 1;
		Vector3 vector2;
		vector2..ctor(1f, 1f, 0f);
		array[num2] = vector2.normalized;
		int num3 = 2;
		Vector3 vector3;
		vector3..ctor(-1f, 1f, 0f);
		array[num3] = vector3.normalized;
		int num4 = 3;
		Vector3 vector4;
		vector4..ctor(0f, 1f, 1f);
		array[num4] = vector4.normalized;
		int num5 = 4;
		Vector3 vector5;
		vector5..ctor(0f, 1f, -1f);
		array[num5] = vector5.normalized;
		global::BuildingBlock.outsideLookupOffsets = array;
		global::BuildingBlock.updateSkinQueueServer = new global::BuildingBlock.UpdateSkinWorkQueue();
	}

	// Token: 0x0400032C RID: 812
	[NonSerialized]
	public global::Construction blockDefinition;

	// Token: 0x0400032D RID: 813
	private static Vector3[] outsideLookupOffsets;

	// Token: 0x0400032E RID: 814
	private int modelState;

	// Token: 0x0400032F RID: 815
	private int lastModelState;

	// Token: 0x04000330 RID: 816
	public global::BuildingGrade.Enum grade;

	// Token: 0x04000331 RID: 817
	private global::BuildingGrade.Enum lastGrade = global::BuildingGrade.Enum.None;

	// Token: 0x04000332 RID: 818
	private global::ConstructionSkin currentSkin;

	// Token: 0x04000333 RID: 819
	private global::DeferredAction skinChange;

	// Token: 0x04000334 RID: 820
	private MeshRenderer placeholderRenderer;

	// Token: 0x04000335 RID: 821
	private MeshCollider placeholderCollider;

	// Token: 0x04000336 RID: 822
	public static global::BuildingBlock.UpdateSkinWorkQueue updateSkinQueueServer;

	// Token: 0x02000056 RID: 86
	public static class BlockFlags
	{
		// Token: 0x04000337 RID: 823
		public const global::BaseEntity.Flags CanRotate = global::BaseEntity.Flags.Reserved1;

		// Token: 0x04000338 RID: 824
		public const global::BaseEntity.Flags CanDemolish = global::BaseEntity.Flags.Reserved2;
	}

	// Token: 0x02000057 RID: 87
	public class UpdateSkinWorkQueue : ObjectWorkQueue<global::BuildingBlock>
	{
		// Token: 0x0600070B RID: 1803 RVA: 0x0002C5F4 File Offset: 0x0002A7F4
		protected override void RunJob(global::BuildingBlock entity)
		{
			if (!this.ShouldAdd(entity))
			{
				return;
			}
			entity.UpdateSkin(true);
		}

		// Token: 0x0600070C RID: 1804 RVA: 0x0002C60C File Offset: 0x0002A80C
		protected override bool ShouldAdd(global::BuildingBlock entity)
		{
			return entity.IsValid();
		}
	}
}
