﻿using System;
using UnityEngine;

// Token: 0x02000593 RID: 1427
public static class TerrainFilterEx
{
	// Token: 0x06001E1F RID: 7711 RVA: 0x000A7654 File Offset: 0x000A5854
	public static bool ApplyTerrainFilters(this Transform transform, global::TerrainFilter[] filters, Vector3 pos, Quaternion rot, Vector3 scale, global::SpawnFilter globalFilter = null)
	{
		if (filters.Length <= 0)
		{
			return true;
		}
		foreach (global::TerrainFilter terrainFilter in filters)
		{
			Vector3 vector = Vector3.Scale(terrainFilter.worldPosition, scale);
			Vector3 vector2 = pos + rot * vector;
			if (global::TerrainMeta.OutOfBounds(vector2))
			{
				return false;
			}
			if (globalFilter != null && globalFilter.GetFactor(vector2) == 0f)
			{
				return false;
			}
			if (!terrainFilter.Check(vector2))
			{
				return false;
			}
		}
		return true;
	}
}
