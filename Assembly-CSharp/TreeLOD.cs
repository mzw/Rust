﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x0200045E RID: 1118
public class TreeLOD : global::LODComponent
{
	// Token: 0x04001356 RID: 4950
	[Horizontal(1, 0)]
	public global::TreeLOD.State[] States;

	// Token: 0x0200045F RID: 1119
	[Serializable]
	public class State
	{
		// Token: 0x04001357 RID: 4951
		public float distance;

		// Token: 0x04001358 RID: 4952
		public Renderer renderer;

		// Token: 0x04001359 RID: 4953
		[NonSerialized]
		public MeshFilter filter;

		// Token: 0x0400135A RID: 4954
		[NonSerialized]
		public ShadowCastingMode shadowMode;
	}
}
