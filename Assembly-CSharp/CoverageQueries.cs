﻿using System;
using System.Collections.Generic;
using RustNative;
using UnityEngine;

// Token: 0x020007E8 RID: 2024
[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(Camera))]
public class CoverageQueries : MonoBehaviour
{
	// Token: 0x170002B5 RID: 693
	// (get) Token: 0x0600255A RID: 9562 RVA: 0x000CE6D8 File Offset: 0x000CC8D8
	public static global::CoverageQueries Instance
	{
		get
		{
			return global::CoverageQueries.instance;
		}
	}

	// Token: 0x170002B6 RID: 694
	// (get) Token: 0x0600255B RID: 9563 RVA: 0x000CE6E0 File Offset: 0x000CC8E0
	// (set) Token: 0x0600255C RID: 9564 RVA: 0x000CE6E8 File Offset: 0x000CC8E8
	public static bool Debug
	{
		get
		{
			return global::CoverageQueries._debug;
		}
		set
		{
			global::CoverageQueries._debug = value;
		}
	}

	// Token: 0x0600255D RID: 9565 RVA: 0x000CE6F0 File Offset: 0x000CC8F0
	private void Awake()
	{
		global::CoverageQueries.instance = this;
		this.camera = base.GetComponent<Camera>();
	}

	// Token: 0x0600255E RID: 9566 RVA: 0x000CE704 File Offset: 0x000CC904
	private void OnEnable()
	{
		this.coverageMat = new Material(Shader.Find("Hidden/CoverageQueries/Coverage"))
		{
			hideFlags = 61
		};
		global::CoverageQueries.buffer.Attach(this.coverageMat);
	}

	// Token: 0x0600255F RID: 9567 RVA: 0x000CE740 File Offset: 0x000CC940
	private void OnDisable()
	{
		if (this.coverageMat != null)
		{
			Object.DestroyImmediate(this.coverageMat);
			this.coverageMat = null;
		}
		global::CoverageQueries.buffer.Dispose(true);
	}

	// Token: 0x06002560 RID: 9568 RVA: 0x000CE770 File Offset: 0x000CC970
	private void Update()
	{
		this.FetchAndAnalyseResults();
		this.UpdateCollection();
	}

	// Token: 0x06002561 RID: 9569 RVA: 0x000CE780 File Offset: 0x000CC980
	private void OnPostRender()
	{
		this.PrepareAndDispatch();
		this.IssueRead();
	}

	// Token: 0x06002562 RID: 9570 RVA: 0x000CE790 File Offset: 0x000CC990
	private void UpdateCollection()
	{
		if (global::CoverageQueries.reused.Count > 0)
		{
			foreach (global::CoverageQueries.Query query in global::CoverageQueries.reused)
			{
				int id = query.intern.id;
				UnityEngine.Debug.Assert(id >= 0 && id < global::CoverageQueries.pool.Count, "Reusing invalid query id ");
				global::CoverageQueries.pool[id] = query;
				global::CoverageQueries.changed.Add(id);
			}
			global::CoverageQueries.reused.Clear();
		}
		if (global::CoverageQueries.added.Count > 0)
		{
			foreach (global::CoverageQueries.Query query2 in global::CoverageQueries.added)
			{
				int id2 = query2.intern.id;
				UnityEngine.Debug.Assert(id2 >= 0 && id2 <= global::CoverageQueries.pool.Count + global::CoverageQueries.added.Count, "Adding invalid query id");
				global::CoverageQueries.pool.Add(query2);
				global::CoverageQueries.changed.Add(id2);
			}
			global::CoverageQueries.added.Clear();
		}
		if (global::CoverageQueries.removed.Count > 0)
		{
			for (int i = 0; i < global::CoverageQueries.removed.Count; i++)
			{
				int num = global::CoverageQueries.removed[i];
				UnityEngine.Debug.Assert(num >= 0 && num < global::CoverageQueries.pool.Count, "Removing invalid query id");
				global::CoverageQueries.pool[num].intern.Reset();
				global::CoverageQueries.pool[num].result.Reset();
				global::CoverageQueries.pool[num] = null;
				global::CoverageQueries.freed.Enqueue(num);
			}
			global::CoverageQueries.removed.Clear();
		}
		global::CoverageQueries.buffer.CheckResize(global::CoverageQueries.pool.Count);
	}

	// Token: 0x06002563 RID: 9571 RVA: 0x000CE9BC File Offset: 0x000CCBBC
	private void PrepareAndDispatch()
	{
		if (global::CoverageQueries.pool.Count > 0 && global::CoverageQueries.pool.Count <= global::CoverageQueries.buffer.inputData.Length)
		{
			Matrix4x4 worldToCameraMatrix = this.camera.worldToCameraMatrix;
			Matrix4x4 gpuprojectionMatrix = GL.GetGPUProjectionMatrix(this.camera.projectionMatrix, false);
			Matrix4x4 matrix4x = gpuprojectionMatrix * worldToCameraMatrix;
			this.coverageMat.SetMatrix("_ViewProjMatrix", matrix4x);
			this.coverageMat.SetFloat("_DepthBias", this.depthBias);
			if (global::CoverageQueries.changed.Count > 0)
			{
				for (int i = 0; i < global::CoverageQueries.changed.Count; i++)
				{
					int num = global::CoverageQueries.changed[i];
					UnityEngine.Debug.Assert(global::CoverageQueries.changed[i] >= 0 && global::CoverageQueries.changed[i] < global::CoverageQueries.pool.Count);
					global::CoverageQueries.Query query = global::CoverageQueries.pool[num];
					if (query != null)
					{
						float x = query.input.position.x;
						float y = query.input.position.y;
						float z = query.input.position.z;
						float num2 = (float)Mathf.RoundToInt(query.input.radius * 10000f) + (float)query.input.sampleCount / 255f;
						global::CoverageQueries.buffer.inputData[num] = new Vector4(x, y, z, num2);
					}
				}
				global::CoverageQueries.changed.Clear();
			}
			global::CoverageQueries.buffer.UploadData();
			global::CoverageQueries.buffer.Dispatch(global::CoverageQueries.pool.Count);
		}
	}

	// Token: 0x06002564 RID: 9572 RVA: 0x000CEB7C File Offset: 0x000CCD7C
	private void IssueRead()
	{
		if (global::CoverageQueries.pool.Count > 0)
		{
			global::CoverageQueries.buffer.IssueRead();
			GL.IssuePluginEvent(Graphics.GetRenderEventFunc(), 2);
		}
	}

	// Token: 0x06002565 RID: 9573 RVA: 0x000CEBA4 File Offset: 0x000CCDA4
	private void FetchAndAnalyseResults()
	{
		if (global::CoverageQueries.pool.Count > 0 && global::CoverageQueries.pool.Count <= global::CoverageQueries.buffer.resultData.Length)
		{
			global::CoverageQueries.buffer.GetResults();
			for (int i = 0; i < global::CoverageQueries.pool.Count; i++)
			{
				global::CoverageQueries.Query query = global::CoverageQueries.pool[i];
				if (query != null)
				{
					UnityEngine.Debug.Assert(query.intern.id == i);
					query.result.passed = (int)global::CoverageQueries.buffer.resultData[i].r;
					query.result.coverage = (float)global::CoverageQueries.buffer.resultData[i].g / 255f;
					query.result.weightedCoverage = (float)global::CoverageQueries.buffer.resultData[i].b / 255f;
					query.result.originOccluded = (global::CoverageQueries.buffer.resultData[i].a != 0);
					query.result.originVisibility = 2f * Mathf.Clamp01(0.5f - query.result.coverage) * (float)((!query.result.originOccluded) ? 1 : 0);
					if (query.result.frame < 0)
					{
						query.result.smoothCoverage = query.result.coverage;
						query.result.weightedSmoothCoverage = query.result.weightedCoverage;
						query.result.originSmoothVisibility = query.result.originVisibility;
					}
					else
					{
						float num = Time.deltaTime * query.input.smoothingSpeed;
						query.result.smoothCoverage = Mathf.Lerp(query.result.smoothCoverage, query.result.coverage, num);
						query.result.weightedSmoothCoverage = Mathf.Lerp(query.result.weightedSmoothCoverage, query.result.weightedCoverage, num);
						query.result.originSmoothVisibility = Mathf.Lerp(query.result.originSmoothVisibility, query.result.originVisibility, num);
					}
					query.result.frame = Time.frameCount;
				}
			}
		}
	}

	// Token: 0x06002566 RID: 9574 RVA: 0x000CEDEC File Offset: 0x000CCFEC
	public static void RegisterQuery(global::CoverageQueries.Query query)
	{
		UnityEngine.Debug.Assert(query.input.sampleCount >= 1 && query.input.sampleCount <= 256, "RegisterQuery failed sample count check");
		UnityEngine.Debug.Assert(query.input.radius >= 0f, "RegisterQuery failed with negative radius");
		int num = query.intern.id;
		if (num < 0)
		{
			if (global::CoverageQueries.freed.Count > 0)
			{
				num = global::CoverageQueries.freed.Dequeue();
				global::CoverageQueries.reused.Add(query);
			}
			else
			{
				num = global::CoverageQueries.pool.Count + global::CoverageQueries.added.Count;
				global::CoverageQueries.added.Add(query);
			}
			query.intern.id = num;
		}
	}

	// Token: 0x06002567 RID: 9575 RVA: 0x000CEEB8 File Offset: 0x000CD0B8
	public static void UnregisterQuery(global::CoverageQueries.Query query)
	{
		int id = query.intern.id;
		if (id >= 0 && id < global::CoverageQueries.pool.Count)
		{
			global::CoverageQueries.removed.Add(id);
		}
	}

	// Token: 0x06002568 RID: 9576 RVA: 0x000CEEF4 File Offset: 0x000CD0F4
	public static void UpdateQuery(global::CoverageQueries.Query query)
	{
		int id = query.intern.id;
		if (id >= 0 && id < global::CoverageQueries.pool.Count)
		{
			global::CoverageQueries.changed.Add(id);
		}
	}

	// Token: 0x0400217C RID: 8572
	public float depthBias = -0.1f;

	// Token: 0x0400217D RID: 8573
	private static List<global::CoverageQueries.Query> pool = new List<global::CoverageQueries.Query>(128);

	// Token: 0x0400217E RID: 8574
	private static List<global::CoverageQueries.Query> added = new List<global::CoverageQueries.Query>(32);

	// Token: 0x0400217F RID: 8575
	private static List<global::CoverageQueries.Query> reused = new List<global::CoverageQueries.Query>(32);

	// Token: 0x04002180 RID: 8576
	private static List<int> removed = new List<int>(128);

	// Token: 0x04002181 RID: 8577
	private static List<int> changed = new List<int>(128);

	// Token: 0x04002182 RID: 8578
	private static Queue<int> freed = new Queue<int>(16);

	// Token: 0x04002183 RID: 8579
	private static global::CoverageQueries.BufferSet buffer = new global::CoverageQueries.BufferSet();

	// Token: 0x04002184 RID: 8580
	private Camera camera;

	// Token: 0x04002185 RID: 8581
	private Material coverageMat;

	// Token: 0x04002186 RID: 8582
	private static global::CoverageQueries instance;

	// Token: 0x04002187 RID: 8583
	private static bool _debug = false;

	// Token: 0x04002188 RID: 8584
	public bool debug;

	// Token: 0x020007E9 RID: 2025
	public class BufferSet
	{
		// Token: 0x0600256B RID: 9579 RVA: 0x000CEFCC File Offset: 0x000CD1CC
		public void Attach(Material coverageMat)
		{
			this.coverageMat = coverageMat;
		}

		// Token: 0x0600256C RID: 9580 RVA: 0x000CEFD8 File Offset: 0x000CD1D8
		public void Dispose(bool data = true)
		{
			if (this.inputTexture != null)
			{
				Object.DestroyImmediate(this.inputTexture);
				this.inputTexture = null;
			}
			if (this.resultTexture != null)
			{
				RenderTexture.active = null;
				Object.DestroyImmediate(this.resultTexture);
				this.resultTexture = null;
			}
			if (this.readbackInst != IntPtr.Zero)
			{
				Graphics.BufferReadback.Destroy(this.readbackInst);
				this.readbackInst = IntPtr.Zero;
			}
			if (data)
			{
				this.inputData = new Color[0];
				this.resultData = new Color32[0];
			}
		}

		// Token: 0x0600256D RID: 9581 RVA: 0x000CF07C File Offset: 0x000CD27C
		public bool CheckResize(int count)
		{
			if (count > this.inputData.Length || (this.resultTexture != null && !this.resultTexture.IsCreated()))
			{
				this.Dispose(false);
				this.width = Mathf.CeilToInt(Mathf.Sqrt((float)count));
				this.height = Mathf.CeilToInt((float)count / (float)this.width);
				this.inputTexture = new Texture2D(this.width, this.height, 20, false, true);
				this.inputTexture.name = "_Input";
				this.inputTexture.filterMode = 0;
				this.inputTexture.wrapMode = 1;
				this.resultTexture = new RenderTexture(this.width, this.height, 0, 0, 1);
				this.resultTexture.name = "_Result";
				this.resultTexture.filterMode = 0;
				this.resultTexture.wrapMode = 1;
				this.resultTexture.useMipMap = false;
				this.resultTexture.Create();
				this.readbackInst = Graphics.BufferReadback.CreateForTexture(this.resultTexture.GetNativeTexturePtr(), (uint)this.width, (uint)this.height, this.resultTexture.format);
				int newSize = this.width * this.height;
				Array.Resize<Color>(ref this.inputData, newSize);
				Array.Resize<Color32>(ref this.resultData, newSize);
				return true;
			}
			return false;
		}

		// Token: 0x0600256E RID: 9582 RVA: 0x000CF1DC File Offset: 0x000CD3DC
		public void UploadData()
		{
			if (this.inputData.Length > 0)
			{
				this.inputTexture.SetPixels(this.inputData);
				this.inputTexture.Apply();
			}
		}

		// Token: 0x0600256F RID: 9583 RVA: 0x000CF208 File Offset: 0x000CD408
		public void Dispatch(int count)
		{
			if (this.inputData.Length > 0)
			{
				RenderBuffer activeColorBuffer = Graphics.activeColorBuffer;
				RenderBuffer activeDepthBuffer = Graphics.activeDepthBuffer;
				this.coverageMat.SetTexture("_Input", this.inputTexture);
				Graphics.Blit(this.inputTexture, this.resultTexture, this.coverageMat, 0);
				Graphics.SetRenderTarget(activeColorBuffer, activeDepthBuffer);
			}
		}

		// Token: 0x06002570 RID: 9584 RVA: 0x000CF264 File Offset: 0x000CD464
		public void IssueRead()
		{
			if (this.readbackInst != IntPtr.Zero)
			{
				Graphics.BufferReadback.IssueRead(this.readbackInst);
			}
		}

		// Token: 0x06002571 RID: 9585 RVA: 0x000CF288 File Offset: 0x000CD488
		public void GetResults()
		{
			if (this.readbackInst != IntPtr.Zero && this.resultData.Length > 0)
			{
				Graphics.BufferReadback.GetData(this.readbackInst, ref this.resultData[0]);
			}
		}

		// Token: 0x04002189 RID: 8585
		public int width;

		// Token: 0x0400218A RID: 8586
		public int height;

		// Token: 0x0400218B RID: 8587
		public Texture2D inputTexture;

		// Token: 0x0400218C RID: 8588
		public RenderTexture resultTexture;

		// Token: 0x0400218D RID: 8589
		public IntPtr readbackInst = IntPtr.Zero;

		// Token: 0x0400218E RID: 8590
		public Color[] inputData = new Color[0];

		// Token: 0x0400218F RID: 8591
		public Color32[] resultData = new Color32[0];

		// Token: 0x04002190 RID: 8592
		private Material coverageMat;
	}

	// Token: 0x020007EA RID: 2026
	public class Query
	{
		// Token: 0x06002572 RID: 9586 RVA: 0x000CF2C4 File Offset: 0x000CD4C4
		public Query()
		{
			this.Reset();
		}

		// Token: 0x06002573 RID: 9587 RVA: 0x000CF30C File Offset: 0x000CD50C
		public Query(Vector3 position, float radius, int sampleCount, float smoothingSpeed = 15f)
		{
			this.Reset();
			this.input.position = position;
			this.input.radius = radius;
			this.input.sampleCount = sampleCount;
			this.input.smoothingSpeed = smoothingSpeed;
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06002574 RID: 9588 RVA: 0x000CF384 File Offset: 0x000CD584
		public bool IsRegistered
		{
			get
			{
				return this.intern.id >= 0;
			}
		}

		// Token: 0x06002575 RID: 9589 RVA: 0x000CF398 File Offset: 0x000CD598
		private void Reset()
		{
			this.intern.Reset();
			this.result.Reset();
		}

		// Token: 0x06002576 RID: 9590 RVA: 0x000CF3B0 File Offset: 0x000CD5B0
		public void Register()
		{
			global::CoverageQueries.RegisterQuery(this);
		}

		// Token: 0x06002577 RID: 9591 RVA: 0x000CF3B8 File Offset: 0x000CD5B8
		public void Update(Vector3 position, float radius)
		{
			if (this.intern.id >= 0)
			{
				this.input.position = position;
				this.input.radius = radius;
				global::CoverageQueries.UpdateQuery(this);
			}
		}

		// Token: 0x06002578 RID: 9592 RVA: 0x000CF3EC File Offset: 0x000CD5EC
		public void UpdatePosition(Vector3 position)
		{
			this.input.position = position;
			global::CoverageQueries.UpdateQuery(this);
		}

		// Token: 0x06002579 RID: 9593 RVA: 0x000CF400 File Offset: 0x000CD600
		public void UpdateRadius(float radius)
		{
			this.input.radius = radius;
			global::CoverageQueries.UpdateQuery(this);
		}

		// Token: 0x0600257A RID: 9594 RVA: 0x000CF414 File Offset: 0x000CD614
		public void Unregister()
		{
			global::CoverageQueries.UnregisterQuery(this);
		}

		// Token: 0x04002191 RID: 8593
		public global::CoverageQueries.Query.Input input = default(global::CoverageQueries.Query.Input);

		// Token: 0x04002192 RID: 8594
		public global::CoverageQueries.Query.Internal intern = default(global::CoverageQueries.Query.Internal);

		// Token: 0x04002193 RID: 8595
		public global::CoverageQueries.Query.Result result = default(global::CoverageQueries.Query.Result);

		// Token: 0x020007EB RID: 2027
		public struct Input
		{
			// Token: 0x04002194 RID: 8596
			public Vector3 position;

			// Token: 0x04002195 RID: 8597
			public float radius;

			// Token: 0x04002196 RID: 8598
			public int sampleCount;

			// Token: 0x04002197 RID: 8599
			public float smoothingSpeed;
		}

		// Token: 0x020007EC RID: 2028
		public struct Internal
		{
			// Token: 0x0600257B RID: 9595 RVA: 0x000CF41C File Offset: 0x000CD61C
			public void Reset()
			{
				this.id = -1;
			}

			// Token: 0x04002198 RID: 8600
			public int id;
		}

		// Token: 0x020007ED RID: 2029
		public struct Result
		{
			// Token: 0x0600257C RID: 9596 RVA: 0x000CF428 File Offset: 0x000CD628
			public void Reset()
			{
				this.passed = 0;
				this.coverage = 1f;
				this.smoothCoverage = 1f;
				this.weightedCoverage = 1f;
				this.weightedSmoothCoverage = 1f;
				this.originOccluded = false;
				this.frame = -1;
				this.originVisibility = 0f;
				this.originSmoothVisibility = 0f;
			}

			// Token: 0x04002199 RID: 8601
			public int passed;

			// Token: 0x0400219A RID: 8602
			public float coverage;

			// Token: 0x0400219B RID: 8603
			public float smoothCoverage;

			// Token: 0x0400219C RID: 8604
			public float weightedCoverage;

			// Token: 0x0400219D RID: 8605
			public float weightedSmoothCoverage;

			// Token: 0x0400219E RID: 8606
			public bool originOccluded;

			// Token: 0x0400219F RID: 8607
			public int frame;

			// Token: 0x040021A0 RID: 8608
			public float originVisibility;

			// Token: 0x040021A1 RID: 8609
			public float originSmoothVisibility;
		}
	}
}
