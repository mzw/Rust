﻿using System;
using UnityEngine;

// Token: 0x02000643 RID: 1603
[CreateAssetMenu(menuName = "Rust/Hair Set Collection")]
public class HairSetCollection : ScriptableObject
{
	// Token: 0x0600205E RID: 8286 RVA: 0x000B9428 File Offset: 0x000B7628
	public global::HairSetCollection.HairSetEntry[] GetListByType(global::HairType hairType)
	{
		switch (hairType)
		{
		case global::HairType.Head:
			return this.Head;
		case global::HairType.Eyebrow:
			return this.Eyebrow;
		case global::HairType.Facial:
			return this.Facial;
		case global::HairType.Armpit:
			return this.Armpit;
		case global::HairType.Pubic:
			return this.Pubic;
		default:
			return null;
		}
	}

	// Token: 0x0600205F RID: 8287 RVA: 0x000B9478 File Offset: 0x000B7678
	public int GetIndex(global::HairSetCollection.HairSetEntry[] list, float typeNum)
	{
		return Mathf.Clamp(Mathf.FloorToInt(typeNum * (float)list.Length), 0, list.Length - 1);
	}

	// Token: 0x06002060 RID: 8288 RVA: 0x000B9490 File Offset: 0x000B7690
	public int GetIndex(global::HairType hairType, float typeNum)
	{
		global::HairSetCollection.HairSetEntry[] listByType = this.GetListByType(hairType);
		return this.GetIndex(listByType, typeNum);
	}

	// Token: 0x06002061 RID: 8289 RVA: 0x000B94B0 File Offset: 0x000B76B0
	public global::HairSetCollection.HairSetEntry Get(global::HairType hairType, float typeNum)
	{
		global::HairSetCollection.HairSetEntry[] listByType = this.GetListByType(hairType);
		return listByType[this.GetIndex(listByType, typeNum)];
	}

	// Token: 0x04001B4B RID: 6987
	public global::HairSetCollection.HairSetEntry[] Head;

	// Token: 0x04001B4C RID: 6988
	public global::HairSetCollection.HairSetEntry[] Eyebrow;

	// Token: 0x04001B4D RID: 6989
	public global::HairSetCollection.HairSetEntry[] Facial;

	// Token: 0x04001B4E RID: 6990
	public global::HairSetCollection.HairSetEntry[] Armpit;

	// Token: 0x04001B4F RID: 6991
	public global::HairSetCollection.HairSetEntry[] Pubic;

	// Token: 0x02000644 RID: 1604
	[Serializable]
	public struct HairSetEntry
	{
		// Token: 0x04001B50 RID: 6992
		public global::HairSet HairSet;

		// Token: 0x04001B51 RID: 6993
		public global::HairDyeCollection HairDyeCollection;
	}
}
