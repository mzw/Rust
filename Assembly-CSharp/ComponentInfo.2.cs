﻿using System;
using UnityEngine;

// Token: 0x02000247 RID: 583
public abstract class ComponentInfo : MonoBehaviour
{
	// Token: 0x06001031 RID: 4145
	public abstract void Setup();

	// Token: 0x06001032 RID: 4146
	public abstract void Reset();
}
