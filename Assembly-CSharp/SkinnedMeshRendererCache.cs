﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Token: 0x0200025A RID: 602
public static class SkinnedMeshRendererCache
{
	// Token: 0x06001069 RID: 4201 RVA: 0x000637A0 File Offset: 0x000619A0
	public static void Add(Mesh mesh, global::SkinnedMeshRendererCache.RigInfo info)
	{
		if (!global::SkinnedMeshRendererCache.dictionary.ContainsKey(mesh))
		{
			global::SkinnedMeshRendererCache.dictionary.Add(mesh, info);
		}
	}

	// Token: 0x0600106A RID: 4202 RVA: 0x000637C0 File Offset: 0x000619C0
	public static global::SkinnedMeshRendererCache.RigInfo Get(SkinnedMeshRenderer renderer)
	{
		global::SkinnedMeshRendererCache.RigInfo rigInfo;
		if (!global::SkinnedMeshRendererCache.dictionary.TryGetValue(renderer.sharedMesh, out rigInfo))
		{
			rigInfo = new global::SkinnedMeshRendererCache.RigInfo();
			Transform rootBone = renderer.rootBone;
			Transform[] bones = renderer.bones;
			if (rootBone == null)
			{
				Debug.LogWarning("Renderer without a valid root bone: " + renderer.name + " " + renderer.sharedMesh.name, renderer.gameObject);
				return rigInfo;
			}
			renderer.transform.position = Vector3.zero;
			renderer.transform.rotation = Quaternion.identity;
			renderer.transform.localScale = Vector3.one;
			rigInfo.root = rootBone.name;
			rigInfo.bones = (from x in bones
			select x.name).ToArray<string>();
			rigInfo.transforms = (from x in bones
			select x.transform.localToWorldMatrix).ToArray<Matrix4x4>();
			rigInfo.rootTransform = renderer.rootBone.transform.localToWorldMatrix;
			global::SkinnedMeshRendererCache.dictionary.Add(renderer.sharedMesh, rigInfo);
		}
		return rigInfo;
	}

	// Token: 0x04000B33 RID: 2867
	public static Dictionary<Mesh, global::SkinnedMeshRendererCache.RigInfo> dictionary = new Dictionary<Mesh, global::SkinnedMeshRendererCache.RigInfo>();

	// Token: 0x0200025B RID: 603
	[Serializable]
	public class RigInfo
	{
		// Token: 0x04000B36 RID: 2870
		public string root;

		// Token: 0x04000B37 RID: 2871
		public string[] bones;

		// Token: 0x04000B38 RID: 2872
		public Matrix4x4[] transforms;

		// Token: 0x04000B39 RID: 2873
		public Matrix4x4 rootTransform;
	}
}
