﻿using System;

// Token: 0x02000353 RID: 851
public static class BaseEntityEx
{
	// Token: 0x0600145F RID: 5215 RVA: 0x00076DE0 File Offset: 0x00074FE0
	public static bool IsValid(this global::BaseEntity ent)
	{
		return !(ent == null) && ent.net != null;
	}
}
