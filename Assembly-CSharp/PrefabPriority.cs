﻿using System;

// Token: 0x02000468 RID: 1128
public enum PrefabPriority
{
	// Token: 0x0400137B RID: 4987
	Lowest,
	// Token: 0x0400137C RID: 4988
	Low,
	// Token: 0x0400137D RID: 4989
	Default,
	// Token: 0x0400137E RID: 4990
	High,
	// Token: 0x0400137F RID: 4991
	Highest
}
