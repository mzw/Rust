﻿using System;
using UnityEngine;

// Token: 0x02000327 RID: 807
public class Muzzleflash_AlphaRandom : MonoBehaviour
{
	// Token: 0x0600139C RID: 5020 RVA: 0x000733F0 File Offset: 0x000715F0
	private void Start()
	{
	}

	// Token: 0x0600139D RID: 5021 RVA: 0x000733F4 File Offset: 0x000715F4
	private void OnEnable()
	{
		this.gck[0].color = Color.white;
		this.gck[0].time = 0f;
		this.gck[1].color = Color.white;
		this.gck[1].time = 0.6f;
		this.gck[2].color = Color.black;
		this.gck[2].time = 0.75f;
		float alpha = Random.Range(0.2f, 0.85f);
		this.gak[0].alpha = alpha;
		this.gak[0].time = 0f;
		this.gak[1].alpha = alpha;
		this.gak[1].time = 0.45f;
		this.gak[2].alpha = 0f;
		this.gak[2].time = 0.5f;
		this.grad.SetKeys(this.gck, this.gak);
		foreach (ParticleSystem particleSystem in this.muzzleflashParticles)
		{
			if (particleSystem == null)
			{
				Debug.LogWarning("Muzzleflash_AlphaRandom : null particle system in " + base.gameObject.name);
			}
			else
			{
				particleSystem.colorOverLifetime.color = this.grad;
			}
		}
	}

	// Token: 0x04000E71 RID: 3697
	public ParticleSystem[] muzzleflashParticles;

	// Token: 0x04000E72 RID: 3698
	private Gradient grad = new Gradient();

	// Token: 0x04000E73 RID: 3699
	private GradientColorKey[] gck = new GradientColorKey[3];

	// Token: 0x04000E74 RID: 3700
	private GradientAlphaKey[] gak = new GradientAlphaKey[3];
}
