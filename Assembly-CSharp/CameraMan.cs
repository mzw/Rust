﻿using System;

// Token: 0x02000243 RID: 579
public class CameraMan : SingletonComponent<global::CameraMan>
{
	// Token: 0x04000AEE RID: 2798
	public bool OnlyControlWhenCursorHidden = true;

	// Token: 0x04000AEF RID: 2799
	public bool NeedBothMouseButtonsToZoom;

	// Token: 0x04000AF0 RID: 2800
	public float LookSensitivity = 1f;

	// Token: 0x04000AF1 RID: 2801
	public float MoveSpeed = 1f;
}
