﻿using System;
using UnityEngine;

// Token: 0x02000592 RID: 1426
public class TerrainFilter : global::PrefabAttribute
{
	// Token: 0x06001E1C RID: 7708 RVA: 0x000A7598 File Offset: 0x000A5798
	protected void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		Gizmos.DrawCube(base.transform.position + Vector3.up * 50f * 0.5f, new Vector3(0.5f, 50f, 0.5f));
		Gizmos.DrawSphere(base.transform.position + Vector3.up * 50f, 2f);
	}

	// Token: 0x06001E1D RID: 7709 RVA: 0x000A7630 File Offset: 0x000A5830
	public bool Check(Vector3 pos)
	{
		return this.Filter.GetFactor(pos) > 0f;
	}

	// Token: 0x06001E1E RID: 7710 RVA: 0x000A7648 File Offset: 0x000A5848
	protected override Type GetIndexedType()
	{
		return typeof(global::TerrainFilter);
	}

	// Token: 0x040018B2 RID: 6322
	public global::SpawnFilter Filter;
}
