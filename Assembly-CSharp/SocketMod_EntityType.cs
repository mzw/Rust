﻿using System;
using System.Collections.Generic;
using Facepunch;
using UnityEngine;

// Token: 0x02000229 RID: 553
public class SocketMod_EntityType : global::SocketMod
{
	// Token: 0x06000FF0 RID: 4080 RVA: 0x00061050 File Offset: 0x0005F250
	private void OnDrawGizmosSelected()
	{
		Gizmos.matrix = base.transform.localToWorldMatrix;
		Gizmos.color = ((!this.wantsCollide) ? new Color(1f, 0f, 0f, 0.7f) : new Color(0f, 1f, 0f, 0.7f));
		Gizmos.DrawSphere(Vector3.zero, this.sphereRadius);
	}

	// Token: 0x06000FF1 RID: 4081 RVA: 0x000610C4 File Offset: 0x0005F2C4
	public override bool DoCheck(global::Construction.Placement place)
	{
		Vector3 position = place.position + place.rotation * this.worldPosition;
		List<global::BaseEntity> list = Pool.GetList<global::BaseEntity>();
		global::Vis.Entities<global::BaseEntity>(position, this.sphereRadius, list, this.layerMask.value, this.queryTriggers);
		foreach (global::BaseEntity baseEntity in list)
		{
			bool flag = baseEntity.GetType().IsAssignableFrom(this.searchType.GetType());
			if (flag && this.wantsCollide)
			{
				Pool.FreeList<global::BaseEntity>(ref list);
				return true;
			}
			if (flag && !this.wantsCollide)
			{
				Pool.FreeList<global::BaseEntity>(ref list);
				return false;
			}
		}
		Pool.FreeList<global::BaseEntity>(ref list);
		return !this.wantsCollide;
	}

	// Token: 0x04000AA7 RID: 2727
	public float sphereRadius = 1f;

	// Token: 0x04000AA8 RID: 2728
	public LayerMask layerMask;

	// Token: 0x04000AA9 RID: 2729
	public QueryTriggerInteraction queryTriggers;

	// Token: 0x04000AAA RID: 2730
	public global::BaseEntity searchType;

	// Token: 0x04000AAB RID: 2731
	public bool wantsCollide;
}
