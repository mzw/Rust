﻿using System;
using ConVar;
using Network;
using Oxide.Core;
using UnityEngine;
using UnityEngine.Assertions;

// Token: 0x02000030 RID: 48
public class BaseLock : global::BaseEntity
{
	// Token: 0x06000450 RID: 1104 RVA: 0x00018400 File Offset: 0x00016600
	public override bool OnRpcMessage(global::BasePlayer player, uint rpc, Message msg)
	{
		using (TimeWarning.New("BaseLock.OnRpcMessage", 0.1f))
		{
			if (rpc == 3901957964u && player != null)
			{
				Assert.IsTrue(player.isServer, "SV_RPC Message is using a clientside player!");
				if (ConVar.Global.developer > 2)
				{
					Debug.Log("SV_RPCMessage: " + player + " - RPC_TakeLock ");
				}
				using (TimeWarning.New("RPC_TakeLock", 0.1f))
				{
					using (TimeWarning.New("Conditions", 0.1f))
					{
						if (!global::BaseEntity.RPC_Server.MaxDistance.Test("RPC_TakeLock", this, player, 3f))
						{
							return true;
						}
					}
					try
					{
						using (TimeWarning.New("Call", 0.1f))
						{
							global::BaseEntity.RPCMessage rpc2 = new global::BaseEntity.RPCMessage
							{
								connection = msg.connection,
								player = player,
								read = msg.read
							};
							this.RPC_TakeLock(rpc2);
						}
					}
					catch (Exception ex)
					{
						player.Kick("RPC Error in RPC_TakeLock");
						Debug.LogException(ex);
					}
				}
				return true;
			}
		}
		return base.OnRpcMessage(player, rpc, msg);
	}

	// Token: 0x06000451 RID: 1105 RVA: 0x000185E0 File Offset: 0x000167E0
	public virtual bool GetPlayerLockPermission(global::BasePlayer player)
	{
		return this.OnTryToOpen(player);
	}

	// Token: 0x06000452 RID: 1106 RVA: 0x000185EC File Offset: 0x000167EC
	public virtual bool OnTryToOpen(global::BasePlayer player)
	{
		return !base.IsLocked();
	}

	// Token: 0x06000453 RID: 1107 RVA: 0x000185F8 File Offset: 0x000167F8
	public virtual bool OnTryToClose(global::BasePlayer player)
	{
		return true;
	}

	// Token: 0x06000454 RID: 1108 RVA: 0x000185FC File Offset: 0x000167FC
	public virtual bool HasLockPermission(global::BasePlayer player)
	{
		return true;
	}

	// Token: 0x06000455 RID: 1109 RVA: 0x00018600 File Offset: 0x00016800
	[global::BaseEntity.RPC_Server]
	[global::BaseEntity.RPC_Server.MaxDistance(3f)]
	public void RPC_TakeLock(global::BaseEntity.RPCMessage rpc)
	{
		if (!rpc.player.CanInteract())
		{
			return;
		}
		if (base.IsLocked())
		{
			return;
		}
		if (Interface.CallHook("CanPickupLock", new object[]
		{
			rpc.player,
			this
		}) != null)
		{
			return;
		}
		global::Item item = global::ItemManager.Create(this.itemType, 1, this.skinID);
		if (item != null)
		{
			rpc.player.GiveItem(item, global::BaseEntity.GiveItemReason.Generic);
		}
		base.Kill(global::BaseNetworkable.DestroyMode.None);
	}

	// Token: 0x06000456 RID: 1110 RVA: 0x0001867C File Offset: 0x0001687C
	public override bool SupportsPooling()
	{
		return true;
	}

	// Token: 0x06000457 RID: 1111 RVA: 0x00018680 File Offset: 0x00016880
	public override float BoundsPadding()
	{
		return 2f;
	}

	// Token: 0x04000134 RID: 308
	[global::ItemSelector(global::ItemCategory.All)]
	public global::ItemDefinition itemType;
}
