﻿using System;
using UnityEngine;

// Token: 0x02000285 RID: 645
public class TimedRemoval : MonoBehaviour
{
	// Token: 0x060010C9 RID: 4297 RVA: 0x00064D4C File Offset: 0x00062F4C
	private void OnEnable()
	{
		Object.Destroy(this.objectToDestroy, this.removeDelay);
	}

	// Token: 0x04000BEF RID: 3055
	public Object objectToDestroy;

	// Token: 0x04000BF0 RID: 3056
	public float removeDelay = 1f;
}
