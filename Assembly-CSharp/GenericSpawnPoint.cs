﻿using System;
using UnityEngine;
using UnityEngine.Events;

// Token: 0x0200048A RID: 1162
public class GenericSpawnPoint : global::BaseSpawnPoint
{
	// Token: 0x06001944 RID: 6468 RVA: 0x0008E714 File Offset: 0x0008C914
	public override void GetLocation(out Vector3 pos, out Quaternion rot)
	{
		pos = base.transform.position;
		if (this.randomRot)
		{
			rot = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
		}
		else
		{
			rot = base.transform.rotation;
		}
		if (this.dropToGround)
		{
			base.DropToGround(ref pos, ref rot);
		}
	}

	// Token: 0x06001945 RID: 6469 RVA: 0x0008E78C File Offset: 0x0008C98C
	public override void ObjectSpawned(global::SpawnPointInstance instance)
	{
		if (this.spawnEffect.isValid)
		{
			global::Effect.server.Run(this.spawnEffect.resourcePath, instance.GetComponent<global::BaseEntity>(), 0u, Vector3.zero, Vector3.up, null, false);
		}
		this.OnObjectSpawnedEvent.Invoke();
		base.gameObject.SetActive(false);
	}

	// Token: 0x06001946 RID: 6470 RVA: 0x0008E7E4 File Offset: 0x0008C9E4
	public override void ObjectRetired(global::SpawnPointInstance instance)
	{
		this.OnObjectRetiredEvent.Invoke();
		base.gameObject.SetActive(true);
	}

	// Token: 0x040013F8 RID: 5112
	public bool dropToGround = true;

	// Token: 0x040013F9 RID: 5113
	public bool randomRot;

	// Token: 0x040013FA RID: 5114
	public global::GameObjectRef spawnEffect;

	// Token: 0x040013FB RID: 5115
	public UnityEvent OnObjectSpawnedEvent = new UnityEvent();

	// Token: 0x040013FC RID: 5116
	public UnityEvent OnObjectRetiredEvent = new UnityEvent();
}
