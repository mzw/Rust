﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020001D4 RID: 468
public class RainSurfaceAmbience : MonoBehaviour
{
	// Token: 0x040008ED RID: 2285
	public float tickRate = 1f;

	// Token: 0x040008EE RID: 2286
	public float gridSize = 20f;

	// Token: 0x040008EF RID: 2287
	public float gridSamples = 10f;

	// Token: 0x040008F0 RID: 2288
	public float startHeight = 100f;

	// Token: 0x040008F1 RID: 2289
	public float rayLength = 250f;

	// Token: 0x040008F2 RID: 2290
	public LayerMask layerMask;

	// Token: 0x040008F3 RID: 2291
	public float spreadScale = 8f;

	// Token: 0x040008F4 RID: 2292
	public float maxDistance = 10f;

	// Token: 0x040008F5 RID: 2293
	public float lerpSpeed = 5f;

	// Token: 0x040008F6 RID: 2294
	public List<global::RainSurfaceAmbience.SurfaceSound> surfaces = new List<global::RainSurfaceAmbience.SurfaceSound>();

	// Token: 0x020001D5 RID: 469
	[Serializable]
	public class SurfaceSound
	{
		// Token: 0x040008F7 RID: 2295
		public global::SoundDefinition soundDef;

		// Token: 0x040008F8 RID: 2296
		public List<PhysicMaterial> materials = new List<PhysicMaterial>();

		// Token: 0x040008F9 RID: 2297
		[HideInInspector]
		public global::Sound sound;

		// Token: 0x040008FA RID: 2298
		[HideInInspector]
		public float amount;

		// Token: 0x040008FB RID: 2299
		[HideInInspector]
		public Vector3 position = Vector3.zero;

		// Token: 0x040008FC RID: 2300
		[HideInInspector]
		public Bounds bounds;

		// Token: 0x040008FD RID: 2301
		[HideInInspector]
		public List<Vector3> points = new List<Vector3>();

		// Token: 0x040008FE RID: 2302
		[HideInInspector]
		public global::SoundModulation.Modulator gainMod;

		// Token: 0x040008FF RID: 2303
		[HideInInspector]
		public global::SoundModulation.Modulator spreadMod;
	}
}
