﻿using System;

// Token: 0x020004D7 RID: 1239
public class ItemModAlterCondition : global::ItemMod
{
	// Token: 0x06001AB4 RID: 6836 RVA: 0x00095A10 File Offset: 0x00093C10
	public override void DoAction(global::Item item, global::BasePlayer player)
	{
		if (item.amount < 1)
		{
			return;
		}
		if (this.conditionChange < 0f)
		{
			item.LoseCondition(this.conditionChange * -1f);
		}
		else
		{
			item.RepairCondition(this.conditionChange);
		}
	}

	// Token: 0x04001575 RID: 5493
	public float conditionChange;
}
