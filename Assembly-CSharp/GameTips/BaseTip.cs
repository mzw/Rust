﻿using System;

namespace GameTips
{
	// Token: 0x0200068B RID: 1675
	public abstract class BaseTip
	{
		// Token: 0x060020FE RID: 8446
		public abstract global::Translate.Phrase GetPhrase();

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x060020FF RID: 8447
		public abstract bool ShouldShow { get; }

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06002100 RID: 8448 RVA: 0x000BB320 File Offset: 0x000B9520
		public string Type
		{
			get
			{
				return base.GetType().Name;
			}
		}
	}
}
