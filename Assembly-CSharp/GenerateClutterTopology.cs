﻿using System;

// Token: 0x0200059D RID: 1437
public class GenerateClutterTopology : global::ProceduralComponent
{
	// Token: 0x06001E36 RID: 7734 RVA: 0x000A7D50 File Offset: 0x000A5F50
	public override void Process(uint seed)
	{
		global::TerrainTopologyMap topologyMap = global::TerrainMeta.TopologyMap;
		int[,] map = topologyMap.dst;
		global::ImageProcessing.Dilate2D(map, 16777728, 3, delegate(int x, int y)
		{
			if ((map[x, y] & 512) == 0)
			{
				map[x, y] |= 16777216;
			}
		});
	}
}
