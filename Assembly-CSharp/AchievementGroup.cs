﻿using System;
using System.Linq;
using Facepunch.Steamworks;

// Token: 0x02000003 RID: 3
public class AchievementGroup
{
	// Token: 0x06000003 RID: 3 RVA: 0x000020A0 File Offset: 0x000002A0
	public AchievementGroup(string token = "", string english = "")
	{
		this.groupTitle.token = token;
		this.groupTitle.english = english;
	}

	// Token: 0x17000001 RID: 1
	// (get) Token: 0x06000004 RID: 4 RVA: 0x000020D8 File Offset: 0x000002D8
	public bool Unlocked
	{
		get
		{
			return this.Items.All((global::AchievementGroup.AchievementItem x) => x.Unlocked);
		}
	}

	// Token: 0x04000006 RID: 6
	public global::Translate.Phrase groupTitle = new global::Translate.Phrase(string.Empty, string.Empty);

	// Token: 0x04000007 RID: 7
	public static global::AchievementGroup[] All = new global::AchievementGroup[]
	{
		new global::AchievementGroup("list_getting_started", "Getting Started")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("COLLECT_100_WOOD", "Harvest 100 Wood"),
				new global::AchievementGroup.AchievementItem("CRAFT_CAMPFIRE", "Craft a camp fire"),
				new global::AchievementGroup.AchievementItem("PLACE_CAMPFIRE", "Place a camp fire")
			}
		},
		new global::AchievementGroup("list_tools_weaps", "Craft Tools & Weapons")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("COLLECT_700_WOOD", "Harvest 700 Wood"),
				new global::AchievementGroup.AchievementItem("COLLECT_200_STONE", "Harvest 200 Stone"),
				new global::AchievementGroup.AchievementItem("CRAFT_STONE_HATCHET", "Craft a Stone Hatchet"),
				new global::AchievementGroup.AchievementItem("CRAFT_STONE_PICKAXE", "Craft a Stone Pickaxe"),
				new global::AchievementGroup.AchievementItem("CRAFT_SPEAR", "Craft a Wooden Spear")
			}
		},
		new global::AchievementGroup("list_respawn_point", "Create a respawn point")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("COLLECT_30_CLOTH", "Collect 30 Cloth"),
				new global::AchievementGroup.AchievementItem("CRAFT_SLEEPINGBAG", "Craft a sleeping bag"),
				new global::AchievementGroup.AchievementItem("PLACE_SLEEPINGBAG", "Place a sleeping bag")
			}
		},
		new global::AchievementGroup("list_base_building", "Build a Base")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_BUILDING_PLAN", "Craft a Building Plan"),
				new global::AchievementGroup.AchievementItem("CRAFT_HAMMER", "Craft a hammer"),
				new global::AchievementGroup.AchievementItem("CONSTRUCT_BASE", "Construct a Base"),
				new global::AchievementGroup.AchievementItem("UPGRADE_BASE", "Upgrade your base")
			}
		},
		new global::AchievementGroup("list_secure_base", "Secure your Base")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_WOODEN_DOOR", "Craft a Wooden Door"),
				new global::AchievementGroup.AchievementItem("CRAFT_LOCK", "Craft a lock"),
				new global::AchievementGroup.AchievementItem("PLACE_WOODEN_DOOR", "Place Wooden Door"),
				new global::AchievementGroup.AchievementItem("PLACE_LOCK", "Place lock on Door"),
				new global::AchievementGroup.AchievementItem("LOCK_LOCK", "Lock the Lock")
			}
		},
		new global::AchievementGroup("list_create_storage", "Create Storage")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_WOODEN_BOX", "Craft a Wooden Box"),
				new global::AchievementGroup.AchievementItem("PLACE_WOODEN_BOX", "Place Wooden Box in Base")
			}
		},
		new global::AchievementGroup("list_craft_toolcupboard", "Claim an Area")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_TOOL_CUPBOARD", "Craft a Tool Cupboard"),
				new global::AchievementGroup.AchievementItem("PLACE_TOOL_CUPBOARD", "Place tool cupboard in base")
			}
		},
		new global::AchievementGroup("list_hunt", "Going Hunting")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("COLLECT_50_CLOTH", "Gather 50 Cloth"),
				new global::AchievementGroup.AchievementItem("CRAFT_HUNTING_BOW", "Craft a Hunting Bow"),
				new global::AchievementGroup.AchievementItem("CRAFT_ARROWS", "Craft some Arrows"),
				new global::AchievementGroup.AchievementItem("KILL_ANIMAL", "Kill an Animal"),
				new global::AchievementGroup.AchievementItem("SKIN_ANIMAL", "Harvest an Animal")
			}
		},
		new global::AchievementGroup("list_gear_up", "Craft & Equip Clothing")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_BURLAP_HEADWRAP", "Craft a Burlap Headwrap"),
				new global::AchievementGroup.AchievementItem("CRAFT_BURLAP_SHIRT", "Craft a Burlap Shirt"),
				new global::AchievementGroup.AchievementItem("CRAFT_BURLAP_PANTS", "Craft Burlap Pants"),
				new global::AchievementGroup.AchievementItem("EQUIP_CLOTHING", "Equip Clothing")
			}
		},
		new global::AchievementGroup("list_furnace", "Create a Furnace")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("COLLECT_50_LGF", "Collect or Craft 50 Low Grade Fuel"),
				new global::AchievementGroup.AchievementItem("CRAFT_FURNACE", "Craft a Furnace"),
				new global::AchievementGroup.AchievementItem("PLACE_FURNACE", "Place a Furnace")
			}
		},
		new global::AchievementGroup("list_machete", "Craft a Metal Weapon")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("COLLECT_300_METAL_ORE", "Collect 300 Metal Ore"),
				new global::AchievementGroup.AchievementItem("CRAFT_MACHETE", "Craft a Machete")
			}
		},
		new global::AchievementGroup("list_explore_1", "Exploring")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("VISIT_ROAD", "Visit a Road"),
				new global::AchievementGroup.AchievementItem("DESTROY_10_BARRELS", "Destroy 10 Barrels"),
				new global::AchievementGroup.AchievementItem("COLLECT_65_SCRAP", "Collect 65 Scrap")
			}
		},
		new global::AchievementGroup("list_workbench", "Workbenches")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_WORKBENCH", "Craft a Workbench"),
				new global::AchievementGroup.AchievementItem("PLACE_WORKBENCH", "Place Workbench in base"),
				new global::AchievementGroup.AchievementItem("CRAFT_NAILGUN", "Craft a Nailgun"),
				new global::AchievementGroup.AchievementItem("CRAFT_NAILGUN_NAILS", "Craft Nailgun Nails")
			}
		},
		new global::AchievementGroup("list_research", "Researching")
		{
			Items = new global::AchievementGroup.AchievementItem[]
			{
				new global::AchievementGroup.AchievementItem("CRAFT_RESEARCH_TABLE", "Craft a Research Table"),
				new global::AchievementGroup.AchievementItem("PLACE_RESEARCH_TABLE", "Place Research Table in base"),
				new global::AchievementGroup.AchievementItem("RESEARCH_ITEM", "Research an Item")
			}
		}
	};

	// Token: 0x04000008 RID: 8
	public global::AchievementGroup.AchievementItem[] Items;

	// Token: 0x02000004 RID: 4
	public class AchievementItem
	{
		// Token: 0x06000007 RID: 7 RVA: 0x0000264C File Offset: 0x0000084C
		public AchievementItem(string name, string phrase)
		{
			this.Name = name;
			this.Phrase = new global::Translate.Phrase(("achievement_" + name).ToLower(), phrase);
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000008 RID: 8 RVA: 0x00002678 File Offset: 0x00000878
		public Achievement Achievement
		{
			get
			{
				return (Facepunch.Steamworks.Client.Instance != null) ? Facepunch.Steamworks.Client.Instance.Achievements.Find(this.Name) : null;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000009 RID: 9 RVA: 0x000026AC File Offset: 0x000008AC
		public bool Unlocked
		{
			get
			{
				return this.Achievement != null && this.Achievement.State;
			}
		}

		// Token: 0x0400000A RID: 10
		public string Name;

		// Token: 0x0400000B RID: 11
		public global::Translate.Phrase Phrase;
	}
}
