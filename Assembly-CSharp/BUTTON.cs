﻿using System;

// Token: 0x020004B8 RID: 1208
public enum BUTTON
{
	// Token: 0x040014AC RID: 5292
	FORWARD = 2,
	// Token: 0x040014AD RID: 5293
	BACKWARD = 4,
	// Token: 0x040014AE RID: 5294
	LEFT = 8,
	// Token: 0x040014AF RID: 5295
	RIGHT = 16,
	// Token: 0x040014B0 RID: 5296
	JUMP = 32,
	// Token: 0x040014B1 RID: 5297
	DUCK = 64,
	// Token: 0x040014B2 RID: 5298
	SPRINT = 128,
	// Token: 0x040014B3 RID: 5299
	USE = 256,
	// Token: 0x040014B4 RID: 5300
	FIRE_PRIMARY = 1024,
	// Token: 0x040014B5 RID: 5301
	FIRE_SECONDARY = 2048,
	// Token: 0x040014B6 RID: 5302
	RELOAD = 8192,
	// Token: 0x040014B7 RID: 5303
	FIRE_THIRD = 134217728
}
