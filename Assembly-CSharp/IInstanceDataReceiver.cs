﻿using System;
using ProtoBuf;

// Token: 0x02000433 RID: 1075
public interface IInstanceDataReceiver
{
	// Token: 0x06001863 RID: 6243
	void ReceiveInstanceData(ProtoBuf.Item.InstanceData data);
}
