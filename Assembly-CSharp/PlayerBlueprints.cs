﻿using System;
using System.Collections.Generic;
using Facepunch.Steamworks;
using Oxide.Core;
using ProtoBuf;

// Token: 0x020003C0 RID: 960
public class PlayerBlueprints : global::EntityComponent<global::BasePlayer>
{
	// Token: 0x06001695 RID: 5781 RVA: 0x0008272C File Offset: 0x0008092C
	public void Reset()
	{
		PersistantPlayer playerInfo = SingletonComponent<global::ServerMgr>.Instance.persistance.GetPlayerInfo(base.baseEntity.userID);
		playerInfo.unlockedItems = new List<int>();
		SingletonComponent<global::ServerMgr>.Instance.persistance.SetPlayerInfo(base.baseEntity.userID, playerInfo);
		base.baseEntity.SendNetworkUpdate(global::BasePlayer.NetworkQueue.Update);
	}

	// Token: 0x06001696 RID: 5782 RVA: 0x00082788 File Offset: 0x00080988
	public void UnlockAll()
	{
		foreach (global::ItemBlueprint itemBlueprint in global::ItemManager.bpList)
		{
			if (itemBlueprint.userCraftable && !itemBlueprint.defaultBlueprint)
			{
				PersistantPlayer playerInfo = SingletonComponent<global::ServerMgr>.Instance.persistance.GetPlayerInfo(base.baseEntity.userID);
				if (!playerInfo.unlockedItems.Contains(itemBlueprint.targetItem.itemid))
				{
					playerInfo.unlockedItems.Add(itemBlueprint.targetItem.itemid);
					SingletonComponent<global::ServerMgr>.Instance.persistance.SetPlayerInfo(base.baseEntity.userID, playerInfo);
				}
			}
		}
		base.baseEntity.SendNetworkUpdateImmediate(false);
		base.baseEntity.ClientRPCPlayer<int>(null, base.baseEntity, "UnlockedBlueprint", 0);
	}

	// Token: 0x06001697 RID: 5783 RVA: 0x00082884 File Offset: 0x00080A84
	public bool IsUnlocked(global::ItemDefinition itemDef)
	{
		PersistantPlayer playerInfo = SingletonComponent<global::ServerMgr>.Instance.persistance.GetPlayerInfo(base.baseEntity.userID);
		return playerInfo.unlockedItems != null && playerInfo.unlockedItems.Contains(itemDef.itemid);
	}

	// Token: 0x06001698 RID: 5784 RVA: 0x000828CC File Offset: 0x00080ACC
	public void Unlock(global::ItemDefinition itemDef)
	{
		PersistantPlayer playerInfo = SingletonComponent<global::ServerMgr>.Instance.persistance.GetPlayerInfo(base.baseEntity.userID);
		if (!playerInfo.unlockedItems.Contains(itemDef.itemid))
		{
			playerInfo.unlockedItems.Add(itemDef.itemid);
			SingletonComponent<global::ServerMgr>.Instance.persistance.SetPlayerInfo(base.baseEntity.userID, playerInfo);
			base.baseEntity.SendNetworkUpdateImmediate(false);
			base.baseEntity.ClientRPCPlayer<int>(null, base.baseEntity, "UnlockedBlueprint", itemDef.itemid);
			base.baseEntity.stats.Add("blueprint_studied", 1, global::Stats.Steam);
		}
	}

	// Token: 0x06001699 RID: 5785 RVA: 0x00082978 File Offset: 0x00080B78
	public bool HasUnlocked(global::ItemDefinition targetItem)
	{
		if (!targetItem.Blueprint || !targetItem.Blueprint.NeedsSteamItem)
		{
			foreach (int num in global::ItemManager.defaultBlueprints)
			{
				if (num == targetItem.itemid)
				{
					return true;
				}
			}
			return base.baseEntity.isServer && this.IsUnlocked(targetItem);
		}
		if (targetItem.steamItem != null && !this.steamInventory.HasItem(targetItem.steamItem.id))
		{
			return false;
		}
		if (targetItem.steamItem == null)
		{
			bool flag = false;
			foreach (global::ItemSkinDirectory.Skin skin in targetItem.skins)
			{
				if (this.steamInventory.HasItem(skin.id))
				{
					flag = true;
					break;
				}
			}
			if (!flag && targetItem.skins2 != null)
			{
				foreach (Inventory.Definition definition in targetItem.skins2)
				{
					if (this.steamInventory.HasItem(definition.Id))
					{
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x0600169A RID: 5786 RVA: 0x00082ADC File Offset: 0x00080CDC
	public bool CanCraft(int itemid, int skinItemId)
	{
		global::ItemDefinition itemDefinition = global::ItemManager.FindItemDefinition(itemid);
		if (itemDefinition == null)
		{
			return false;
		}
		object obj = Interface.CallHook("CanCraft", new object[]
		{
			this,
			itemDefinition,
			skinItemId
		});
		if (obj is bool)
		{
			return (bool)obj;
		}
		return (skinItemId == 0 || this.steamInventory.HasItem(skinItemId)) && base.baseEntity.currentCraftLevel >= (float)itemDefinition.Blueprint.workbenchLevelRequired && this.HasUnlocked(itemDefinition);
	}

	// Token: 0x04001125 RID: 4389
	public global::SteamInventory steamInventory;
}
