﻿using System;
using UnityEngine;

// Token: 0x02000652 RID: 1618
[CreateAssetMenu(menuName = "Rust/Recoil Properties")]
public class RecoilProperties : ScriptableObject
{
	// Token: 0x04001B70 RID: 7024
	public float recoilYawMin;

	// Token: 0x04001B71 RID: 7025
	public float recoilYawMax;

	// Token: 0x04001B72 RID: 7026
	public float recoilPitchMin;

	// Token: 0x04001B73 RID: 7027
	public float recoilPitchMax;

	// Token: 0x04001B74 RID: 7028
	public float timeToTakeMin;

	// Token: 0x04001B75 RID: 7029
	public float timeToTakeMax = 0.1f;

	// Token: 0x04001B76 RID: 7030
	public float ADSScale = 0.5f;

	// Token: 0x04001B77 RID: 7031
	public float movementPenalty;

	// Token: 0x04001B78 RID: 7032
	public float clampPitch = float.NegativeInfinity;

	// Token: 0x04001B79 RID: 7033
	public AnimationCurve pitchCurve = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 1f),
		new Keyframe(1f, 1f)
	});

	// Token: 0x04001B7A RID: 7034
	public AnimationCurve yawCurve = new AnimationCurve(new Keyframe[]
	{
		new Keyframe(0f, 1f),
		new Keyframe(1f, 1f)
	});

	// Token: 0x04001B7B RID: 7035
	public bool useCurves;

	// Token: 0x04001B7C RID: 7036
	public int shotsUntilMax = 30;
}
