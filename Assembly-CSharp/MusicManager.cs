﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

// Token: 0x020001E7 RID: 487
public class MusicManager : MonoBehaviour
{
	// Token: 0x17000105 RID: 261
	// (get) Token: 0x06000F25 RID: 3877 RVA: 0x0005D3CC File Offset: 0x0005B5CC
	public double currentThemeTime
	{
		get
		{
			return UnityEngine.AudioSettings.dspTime - this.themeStartTime;
		}
	}

	// Token: 0x06000F26 RID: 3878 RVA: 0x0005D3DC File Offset: 0x0005B5DC
	public static void RaiseIntensityTo(float amount, int holdLengthBars = 0)
	{
	}

	// Token: 0x06000F27 RID: 3879 RVA: 0x0005D3E0 File Offset: 0x0005B5E0
	public void StopMusic()
	{
	}

	// Token: 0x0400096D RID: 2413
	public AudioMixerGroup mixerGroup;

	// Token: 0x0400096E RID: 2414
	public List<global::MusicTheme> themes;

	// Token: 0x0400096F RID: 2415
	public global::MusicTheme currentTheme;

	// Token: 0x04000970 RID: 2416
	public List<AudioSource> sources = new List<AudioSource>();

	// Token: 0x04000971 RID: 2417
	public double nextMusic;

	// Token: 0x04000972 RID: 2418
	public double nextMusicForced;

	// Token: 0x04000973 RID: 2419
	[Range(0f, 1f)]
	public float intensity;

	// Token: 0x04000974 RID: 2420
	public Dictionary<global::MusicTheme.PositionedClip, global::MusicManager.ClipPlaybackData> clipPlaybackData = new Dictionary<global::MusicTheme.PositionedClip, global::MusicManager.ClipPlaybackData>();

	// Token: 0x04000975 RID: 2421
	public static global::MusicManager instance;

	// Token: 0x04000976 RID: 2422
	public int holdIntensityUntilBar;

	// Token: 0x04000977 RID: 2423
	public bool musicPlaying;

	// Token: 0x04000978 RID: 2424
	public bool loadingFirstClips;

	// Token: 0x04000979 RID: 2425
	public global::MusicTheme nextTheme;

	// Token: 0x0400097A RID: 2426
	public double lastClipUpdate;

	// Token: 0x0400097B RID: 2427
	public float clipUpdateInterval = 0.1f;

	// Token: 0x0400097C RID: 2428
	public double themeStartTime;

	// Token: 0x0400097D RID: 2429
	public int lastActiveClipRefresh = -10;

	// Token: 0x0400097E RID: 2430
	public int activeClipRefreshInterval = 1;

	// Token: 0x0400097F RID: 2431
	public bool forceThemeChange;

	// Token: 0x04000980 RID: 2432
	public float randomIntensityJumpChance = 0.001f;

	// Token: 0x04000981 RID: 2433
	public List<global::MusicTheme.PositionedClip> activeClips = new List<global::MusicTheme.PositionedClip>();

	// Token: 0x04000982 RID: 2434
	public List<global::MusicTheme.PositionedClip> activeMusicClips = new List<global::MusicTheme.PositionedClip>();

	// Token: 0x04000983 RID: 2435
	public List<global::MusicTheme.PositionedClip> activeControlClips = new List<global::MusicTheme.PositionedClip>();

	// Token: 0x04000984 RID: 2436
	public int currentBar;

	// Token: 0x020001E8 RID: 488
	[Serializable]
	public class ClipPlaybackData
	{
		// Token: 0x04000985 RID: 2437
		public AudioSource source;

		// Token: 0x04000986 RID: 2438
		public global::MusicTheme.PositionedClip positionedClip;

		// Token: 0x04000987 RID: 2439
		public bool isActive;

		// Token: 0x04000988 RID: 2440
		public bool fadingIn;

		// Token: 0x04000989 RID: 2441
		public bool fadingOut;

		// Token: 0x0400098A RID: 2442
		public double fadeStarted;

		// Token: 0x0400098B RID: 2443
		public bool needsSync;
	}
}
