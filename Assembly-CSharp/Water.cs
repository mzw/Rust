﻿using System;
using UnityEngine;

// Token: 0x020005E9 RID: 1513
public class Water : MonoBehaviour
{
	// Token: 0x17000220 RID: 544
	// (get) Token: 0x06001F00 RID: 7936 RVA: 0x000AF7B0 File Offset: 0x000AD9B0
	// (set) Token: 0x06001F01 RID: 7937 RVA: 0x000AF7B8 File Offset: 0x000AD9B8
	public static global::WaterCollision Collision { get; private set; }

	// Token: 0x17000221 RID: 545
	// (get) Token: 0x06001F02 RID: 7938 RVA: 0x000AF7C0 File Offset: 0x000AD9C0
	public bool IsInitialized
	{
		get
		{
			return this.initialized;
		}
	}

	// Token: 0x17000222 RID: 546
	// (get) Token: 0x06001F03 RID: 7939 RVA: 0x000AF7C8 File Offset: 0x000AD9C8
	public static global::Water Instance
	{
		get
		{
			return global::Water.instance;
		}
	}

	// Token: 0x06001F04 RID: 7940 RVA: 0x000AF7D0 File Offset: 0x000AD9D0
	private void Awake()
	{
		global::Water.instance = this;
		global::Water.Collision = base.GetComponent<global::WaterCollision>();
	}

	// Token: 0x040019F7 RID: 6647
	public global::WaterSurfaceDesc ocean = new global::WaterSurfaceDesc();

	// Token: 0x040019F8 RID: 6648
	public global::WaterSurfaceDesc rivers = new global::WaterSurfaceDesc();

	// Token: 0x040019F9 RID: 6649
	public global::WaterSurfaceDesc lakes = new global::WaterSurfaceDesc();

	// Token: 0x040019FA RID: 6650
	public Vector3 wind = new Vector3(6f, 0f, 1f);

	// Token: 0x040019FB RID: 6651
	public bool showDebug;

	// Token: 0x040019FC RID: 6652
	public bool showGizmos;

	// Token: 0x040019FD RID: 6653
	public global::WaterQuality Quality = global::WaterQuality.High;

	// Token: 0x040019FE RID: 6654
	public global::WaterSimulation simulation = new global::WaterSimulation();

	// Token: 0x040019FF RID: 6655
	public global::WaterRendering rendering = new global::WaterRendering();

	// Token: 0x04001A01 RID: 6657
	private bool initialized;

	// Token: 0x04001A02 RID: 6658
	private static global::Water instance;
}
