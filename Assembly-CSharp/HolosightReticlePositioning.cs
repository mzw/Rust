﻿using System;
using UnityEngine;

// Token: 0x020000D7 RID: 215
public class HolosightReticlePositioning : MonoBehaviour
{
	// Token: 0x1700006E RID: 110
	// (get) Token: 0x06000B06 RID: 2822 RVA: 0x0004A26C File Offset: 0x0004846C
	public RectTransform rectTransform
	{
		get
		{
			return base.transform as RectTransform;
		}
	}

	// Token: 0x06000B07 RID: 2823 RVA: 0x0004A27C File Offset: 0x0004847C
	private void Update()
	{
		if (global::MainCamera.isValid)
		{
			this.UpdatePosition(global::MainCamera.mainCamera);
		}
	}

	// Token: 0x06000B08 RID: 2824 RVA: 0x0004A294 File Offset: 0x00048494
	private void UpdatePosition(Camera cam)
	{
		Vector3 position = this.aimPoint.targetPoint.transform.position;
		Vector2 vector = RectTransformUtility.WorldToScreenPoint(cam, position);
		RectTransformUtility.ScreenPointToLocalPointInRectangle(this.rectTransform.parent as RectTransform, vector, cam, ref vector);
		vector.x /= (this.rectTransform.parent as RectTransform).rect.width * 0.5f;
		vector.y /= (this.rectTransform.parent as RectTransform).rect.height * 0.5f;
		this.rectTransform.anchoredPosition = vector;
	}

	// Token: 0x040005B6 RID: 1462
	public global::IronsightAimPoint aimPoint;
}
