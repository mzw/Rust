﻿using System;
using UnityEngine;

// Token: 0x0200030B RID: 779
public class Performance : SingletonComponent<global::Performance>
{
	// Token: 0x06001364 RID: 4964 RVA: 0x000722C4 File Offset: 0x000704C4
	private void Update()
	{
		using (TimeWarning.New("FPSTimer", 0.1f))
		{
			this.FPSTimer();
		}
	}

	// Token: 0x06001365 RID: 4965 RVA: 0x0007230C File Offset: 0x0007050C
	private void FPSTimer()
	{
		this.frames++;
		this.time += Time.unscaledDeltaTime;
		if (this.time < 1f)
		{
			return;
		}
		global::Performance.current.frameRate = this.frames;
		global::Performance.current.frameTime = this.time / (float)this.frames * 1000f;
		checked
		{
			global::Performance.frameRateHistory[(int)((IntPtr)(global::Performance.cycles % unchecked((long)global::Performance.frameRateHistory.Length)))] = global::Performance.current.frameRate;
			global::Performance.frameTimeHistory[(int)((IntPtr)(global::Performance.cycles % unchecked((long)global::Performance.frameTimeHistory.Length)))] = global::Performance.current.frameTime;
			global::Performance.current.frameRateAverage = this.AverageFrameRate();
			global::Performance.current.frameTimeAverage = this.AverageFrameTime();
		}
		global::Performance.current.memoryUsageSystem = (long)global::SystemInfoEx.systemMemoryUsed;
		global::Performance.current.memoryAllocations = GC.GetTotalMemory(false) / 1048576L;
		global::Performance.current.memoryCollections = (long)GC.CollectionCount(0);
		global::Performance.current.loadBalancerTasks = (long)global::LoadBalancer.Count();
		global::Performance.current.invokeHandlerTasks = (long)InvokeHandler.Count();
		this.frames = 0;
		this.time = 0f;
		global::Performance.cycles += 1L;
		global::Performance.report = global::Performance.current;
	}

	// Token: 0x06001366 RID: 4966 RVA: 0x00072458 File Offset: 0x00070658
	private float AverageFrameRate()
	{
		float num = 0f;
		for (int i = 0; i < global::Performance.frameRateHistory.Length; i++)
		{
			num += (float)global::Performance.frameRateHistory[i];
		}
		return num / (float)global::Performance.frameRateHistory.Length;
	}

	// Token: 0x06001367 RID: 4967 RVA: 0x00072498 File Offset: 0x00070698
	private float AverageFrameTime()
	{
		float num = 0f;
		for (int i = 0; i < global::Performance.frameTimeHistory.Length; i++)
		{
			num += global::Performance.frameTimeHistory[i];
		}
		return num / (float)global::Performance.frameTimeHistory.Length;
	}

	// Token: 0x04000E13 RID: 3603
	public static global::Performance.Tick current;

	// Token: 0x04000E14 RID: 3604
	public static global::Performance.Tick report;

	// Token: 0x04000E15 RID: 3605
	private static long cycles = 0L;

	// Token: 0x04000E16 RID: 3606
	private static int[] frameRateHistory = new int[60];

	// Token: 0x04000E17 RID: 3607
	private static float[] frameTimeHistory = new float[60];

	// Token: 0x04000E18 RID: 3608
	private int frames;

	// Token: 0x04000E19 RID: 3609
	private float time;

	// Token: 0x0200030C RID: 780
	public struct Tick
	{
		// Token: 0x04000E1A RID: 3610
		public int frameRate;

		// Token: 0x04000E1B RID: 3611
		public float frameTime;

		// Token: 0x04000E1C RID: 3612
		public float frameRateAverage;

		// Token: 0x04000E1D RID: 3613
		public float frameTimeAverage;

		// Token: 0x04000E1E RID: 3614
		public long memoryUsageSystem;

		// Token: 0x04000E1F RID: 3615
		public long memoryAllocations;

		// Token: 0x04000E20 RID: 3616
		public long memoryCollections;

		// Token: 0x04000E21 RID: 3617
		public long loadBalancerTasks;

		// Token: 0x04000E22 RID: 3618
		public long invokeHandlerTasks;

		// Token: 0x04000E23 RID: 3619
		public int ping;
	}
}
