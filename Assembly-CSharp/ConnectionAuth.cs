﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Network;
using Oxide.Core;
using UnityEngine;

// Token: 0x0200061D RID: 1565
public class ConnectionAuth : MonoBehaviour
{
	// Token: 0x06001F9D RID: 8093 RVA: 0x000B3934 File Offset: 0x000B1B34
	public bool IsConnected(ulong iSteamID)
	{
		return global::BasePlayer.FindByID(iSteamID) || global::ConnectionAuth.m_AuthConnection.Any((Connection item) => item.userid == iSteamID);
	}

	// Token: 0x06001F9E RID: 8094 RVA: 0x000B397C File Offset: 0x000B1B7C
	public static void Reject(Connection connection, string strReason)
	{
		DebugEx.Log(connection.ToString() + " Rejecting connection - " + strReason, 0);
		Net.sv.Kick(connection, strReason);
		global::ConnectionAuth.m_AuthConnection.Remove(connection);
	}

	// Token: 0x06001F9F RID: 8095 RVA: 0x000B39B0 File Offset: 0x000B1BB0
	public static void OnDisconnect(Connection connection)
	{
		global::ConnectionAuth.m_AuthConnection.Remove(connection);
	}

	// Token: 0x06001FA0 RID: 8096 RVA: 0x000B39C0 File Offset: 0x000B1BC0
	public void Approve(Connection connection)
	{
		global::ConnectionAuth.m_AuthConnection.Remove(connection);
		SingletonComponent<global::ServerMgr>.Instance.connectionQueue.Join(connection);
	}

	// Token: 0x06001FA1 RID: 8097 RVA: 0x000B39E0 File Offset: 0x000B1BE0
	public void OnNewConnection(Connection connection)
	{
		connection.connected = false;
		if (connection.token == null || connection.token.Length < 32)
		{
			global::ConnectionAuth.Reject(connection, "Invalid Token");
			return;
		}
		if (connection.userid == 0UL)
		{
			global::ConnectionAuth.Reject(connection, "Invalid SteamID");
			return;
		}
		if (connection.protocol != 2065u)
		{
			if (!global::DeveloperList.Contains(connection.userid))
			{
				global::ConnectionAuth.Reject(connection, "Incompatible Version");
				return;
			}
			DebugEx.Log("Not kicking " + connection.userid + " for incompatible protocol (is a developer)", 0);
		}
		if (global::ServerUsers.Is(connection.userid, global::ServerUsers.UserGroup.Banned))
		{
			global::ConnectionAuth.Reject(connection, "You are banned from this server");
			return;
		}
		if (global::ServerUsers.Is(connection.userid, global::ServerUsers.UserGroup.Moderator))
		{
			DebugEx.Log(connection.ToString() + " has auth level 1", 0);
			connection.authLevel = 1u;
		}
		if (global::ServerUsers.Is(connection.userid, global::ServerUsers.UserGroup.Owner))
		{
			DebugEx.Log(connection.ToString() + " has auth level 2", 0);
			connection.authLevel = 2u;
		}
		if (global::DeveloperList.Contains(connection.userid))
		{
			DebugEx.Log(connection.ToString() + " is a developer", 0);
			connection.authLevel = 3u;
		}
		if (this.IsConnected(connection.userid))
		{
			global::ConnectionAuth.Reject(connection, "You are already connected!");
			return;
		}
		if (Interface.CallHook("IOnUserApprove", new object[]
		{
			connection
		}) != null)
		{
			return;
		}
		global::ConnectionAuth.m_AuthConnection.Add(connection);
		base.StartCoroutine(this.AuthorisationRoutine(connection));
	}

	// Token: 0x06001FA2 RID: 8098 RVA: 0x000B3B7C File Offset: 0x000B1D7C
	public IEnumerator AuthorisationRoutine(Connection connection)
	{
		yield return base.StartCoroutine(global::Auth_Steam.Run(connection));
		yield return base.StartCoroutine(global::Auth_EAC.Run(connection));
		if (connection.rejected || !connection.active)
		{
			yield break;
		}
		global::BasePlayer playerBody = global::BasePlayer.FindByID(connection.userid);
		if (playerBody && playerBody.net.connection != null)
		{
			global::ConnectionAuth.Reject(connection, "You are already connected as a player!");
			yield break;
		}
		this.Approve(connection);
		yield break;
	}

	// Token: 0x04001A9A RID: 6810
	[NonSerialized]
	public static List<Connection> m_AuthConnection = new List<Connection>();
}
