﻿using System;
using UnityEngine;

// Token: 0x020007AC RID: 1964
public struct Half3
{
	// Token: 0x06002490 RID: 9360 RVA: 0x000C96C0 File Offset: 0x000C78C0
	public Half3(Vector3 vec)
	{
		this.x = Mathf.FloatToHalf(vec.x);
		this.y = Mathf.FloatToHalf(vec.y);
		this.z = Mathf.FloatToHalf(vec.z);
	}

	// Token: 0x06002491 RID: 9361 RVA: 0x000C96F8 File Offset: 0x000C78F8
	public static explicit operator Vector3(global::Half3 vec)
	{
		return new Vector3(Mathf.HalfToFloat(vec.x), Mathf.HalfToFloat(vec.y), Mathf.HalfToFloat(vec.z));
	}

	// Token: 0x04002005 RID: 8197
	public ushort x;

	// Token: 0x04002006 RID: 8198
	public ushort y;

	// Token: 0x04002007 RID: 8199
	public ushort z;
}
