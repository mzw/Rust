﻿using System;
using Facepunch;
using UnityEngine;

// Token: 0x0200071D RID: 1821
public class UIPrefab : MonoBehaviour
{
	// Token: 0x06002287 RID: 8839 RVA: 0x000C10F0 File Offset: 0x000BF2F0
	private void Awake()
	{
		if (this.prefabSource == null)
		{
			return;
		}
		if (this.createdGameObject != null)
		{
			return;
		}
		this.createdGameObject = Instantiate.GameObject(this.prefabSource, null);
		this.createdGameObject.name = this.prefabSource.name;
		this.createdGameObject.transform.SetParent(base.transform, false);
		this.createdGameObject.Identity();
	}

	// Token: 0x06002288 RID: 8840 RVA: 0x000C116C File Offset: 0x000BF36C
	public void SetVisible(bool visible)
	{
		if (this.createdGameObject == null)
		{
			return;
		}
		if (this.createdGameObject.activeSelf == visible)
		{
			return;
		}
		this.createdGameObject.SetActive(visible);
	}

	// Token: 0x04001EFC RID: 7932
	public GameObject prefabSource;

	// Token: 0x04001EFD RID: 7933
	internal GameObject createdGameObject;
}
