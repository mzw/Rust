﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000711 RID: 1809
public class TweakUIDropdown : MonoBehaviour
{
	// Token: 0x06002263 RID: 8803 RVA: 0x000C0A3C File Offset: 0x000BEC3C
	protected void Awake()
	{
		this.conVar = ConsoleSystem.Index.Client.Find(this.convarName);
		if (this.conVar == null)
		{
			Debug.LogWarning("TweakUIDropDown Convar Missing: " + this.convarName);
			return;
		}
		this.UpdateState();
	}

	// Token: 0x06002264 RID: 8804 RVA: 0x000C0A78 File Offset: 0x000BEC78
	protected void OnEnable()
	{
		this.UpdateState();
	}

	// Token: 0x06002265 RID: 8805 RVA: 0x000C0A80 File Offset: 0x000BEC80
	public void OnValueChanged()
	{
		this.UpdateConVar();
	}

	// Token: 0x06002266 RID: 8806 RVA: 0x000C0A88 File Offset: 0x000BEC88
	public void ChangeValue(int change)
	{
		this.currentValue += change;
		if (this.currentValue < 0)
		{
			this.currentValue = 0;
		}
		if (this.currentValue > this.nameValues.Length - 1)
		{
			this.currentValue = this.nameValues.Length - 1;
		}
		this.Left.interactable = (this.currentValue > 0);
		this.Right.interactable = (this.currentValue < this.nameValues.Length - 1);
		this.UpdateConVar();
	}

	// Token: 0x06002267 RID: 8807 RVA: 0x000C0B14 File Offset: 0x000BED14
	private void UpdateConVar()
	{
		global::TweakUIDropdown.NameValue nameValue = this.nameValues[this.currentValue];
		if (this.conVar == null)
		{
			return;
		}
		if (this.conVar.String == nameValue.value)
		{
			return;
		}
		this.conVar.Set(nameValue.value);
		this.UpdateState();
	}

	// Token: 0x06002268 RID: 8808 RVA: 0x000C0B70 File Offset: 0x000BED70
	private void UpdateState()
	{
		if (this.conVar == null)
		{
			return;
		}
		string @string = this.conVar.String;
		for (int i = 0; i < this.nameValues.Length; i++)
		{
			if (!(this.nameValues[i].value != @string))
			{
				this.Current.text = this.nameValues[i].label.translated;
				this.currentValue = i;
				if (this.assignImageColor)
				{
					this.BackgroundImage.color = this.nameValues[i].imageColor;
				}
			}
		}
	}

	// Token: 0x04001ED1 RID: 7889
	public Button Left;

	// Token: 0x04001ED2 RID: 7890
	public Button Right;

	// Token: 0x04001ED3 RID: 7891
	public Text Current;

	// Token: 0x04001ED4 RID: 7892
	public Image BackgroundImage;

	// Token: 0x04001ED5 RID: 7893
	public global::TweakUIDropdown.NameValue[] nameValues;

	// Token: 0x04001ED6 RID: 7894
	public string convarName = "effects.motionblur";

	// Token: 0x04001ED7 RID: 7895
	public bool assignImageColor;

	// Token: 0x04001ED8 RID: 7896
	internal ConsoleSystem.Command conVar;

	// Token: 0x04001ED9 RID: 7897
	public int currentValue;

	// Token: 0x02000712 RID: 1810
	[Serializable]
	public class NameValue
	{
		// Token: 0x04001EDA RID: 7898
		public string value;

		// Token: 0x04001EDB RID: 7899
		public Color imageColor;

		// Token: 0x04001EDC RID: 7900
		public global::Translate.Phrase label;
	}
}
