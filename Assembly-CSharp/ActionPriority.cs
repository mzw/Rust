﻿using System;

// Token: 0x02000748 RID: 1864
public enum ActionPriority
{
	// Token: 0x04001F60 RID: 8032
	Highest,
	// Token: 0x04001F61 RID: 8033
	High,
	// Token: 0x04001F62 RID: 8034
	Medium,
	// Token: 0x04001F63 RID: 8035
	Low,
	// Token: 0x04001F64 RID: 8036
	Lowest
}
