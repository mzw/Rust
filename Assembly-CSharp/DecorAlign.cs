﻿using System;
using UnityEngine;

// Token: 0x02000543 RID: 1347
public class DecorAlign : global::DecorComponent
{
	// Token: 0x06001C86 RID: 7302 RVA: 0x0009FC30 File Offset: 0x0009DE30
	public override void Apply(ref Vector3 pos, ref Quaternion rot, ref Vector3 scale)
	{
		Vector3 normal = global::TerrainMeta.HeightMap.GetNormal(pos);
		Vector3 vector = (!(normal == Vector3.up)) ? Vector3.Cross(normal, Vector3.up) : Vector3.forward;
		Vector3 vector2 = Vector3.Cross(normal, vector);
		if (this.SlopeOffset != Vector3.zero || this.SlopeScale != Vector3.one)
		{
			float slope = global::TerrainMeta.HeightMap.GetSlope01(pos);
			if (this.SlopeOffset != Vector3.zero)
			{
				Vector3 vector3 = this.SlopeOffset * slope;
				pos += vector3.x * vector;
				pos += vector3.y * normal;
				pos -= vector3.z * vector2;
			}
			if (this.SlopeScale != Vector3.one)
			{
				Vector3 vector4 = Vector3.Lerp(Vector3.one, Vector3.one + Quaternion.Inverse(rot) * (this.SlopeScale - Vector3.one), slope);
				scale.x *= vector4.x;
				scale.y *= vector4.y;
				scale.z *= vector4.z;
			}
		}
		Vector3 up = Vector3.Lerp(rot * Vector3.up, normal, this.NormalAlignment);
		Vector3 forward = Vector3.Lerp(rot * Vector3.forward, vector2, this.GradientAlignment);
		Quaternion quaternion = UnityEngine.QuaternionEx.LookRotationForcedUp(forward, up);
		rot = quaternion * rot;
	}

	// Token: 0x0400178C RID: 6028
	public float NormalAlignment = 1f;

	// Token: 0x0400178D RID: 6029
	public float GradientAlignment = 1f;

	// Token: 0x0400178E RID: 6030
	public Vector3 SlopeOffset = Vector3.zero;

	// Token: 0x0400178F RID: 6031
	public Vector3 SlopeScale = Vector3.one;
}
